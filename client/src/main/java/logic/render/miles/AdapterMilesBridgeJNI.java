package logic.render.miles;


import utaikodom.render.miles.MilesBridgeJNI;

import java.nio.Buffer;

/* renamed from: a.afz  reason: case insensitive filesystem */
/* compiled from: a */
public class AdapterMilesBridgeJNI implements IAdapterMilesBridgeJNI {
    /* renamed from: a */
    public static int m21668a(C1467Vb vb, C5297aDx adx, float f) {
        return MilesBridgeJNI.AIL_sample_stage_property_RAMP_TO(C1467Vb.m10693c(vb), adx.swigValue(), f);
    }

    /* renamed from: b */
    public static int m21800b(C1467Vb vb, C5297aDx adx, float f) {
        return MilesBridgeJNI.AIL_sample_stage_property_RAMP_TIME(C1467Vb.m10693c(vb), adx.swigValue(), f);
    }

    public static void VOIDFUNC() {
        MilesBridgeJNI.VOIDFUNC();
    }

    public static long RIB_alloc_provider_handle(int i) {
        return MilesBridgeJNI.RIB_alloc_provider_handle(i);
    }

    public static void RIB_free_provider_handle(long j) {
        MilesBridgeJNI.RIB_free_provider_handle(j);
    }

    public static long RIB_load_provider_library(String str) {
        return MilesBridgeJNI.RIB_load_provider_library(str);
    }

    public static void RIB_free_provider_library(long j) {
        MilesBridgeJNI.RIB_free_provider_library(j);
    }

    /* renamed from: a */
    public static int m21663a(long j, String str, int i, C5373aGv agv) {
        return MilesBridgeJNI.RIB_register_interface(j, str, i, C5373aGv.m15023a(agv));
    }

    /* renamed from: b */
    public static int m21799b(long j, String str, int i, C5373aGv agv) {
        return MilesBridgeJNI.RIB_unregister_interface(j, str, i, C5373aGv.m15023a(agv));
    }

    /* renamed from: c */
    public static int m21837c(long j, String str, int i, C5373aGv agv) {
        return MilesBridgeJNI.RIB_request_interface(j, str, i, C5373aGv.m15023a(agv));
    }

    /* renamed from: a */
    public static int m21664a(long j, String str, C3536sH sHVar, C5758aVq avq, C5373aGv agv) {
        return MilesBridgeJNI.RIB_enumerate_interface(j, str, sHVar.swigValue(), C5758aVq.m19143h(avq), C5373aGv.m15023a(agv));
    }

    /* renamed from: a */
    public static int m21701a(String str, C5758aVq avq, C5758aVq avq2) {
        return MilesBridgeJNI.RIB_enumerate_providers(str, C5758aVq.m19143h(avq), C5758aVq.m19143h(avq2));
    }

    /* renamed from: a */
    public static String m21735a(Buffer buffer, aHB ahb) {
        return MilesBridgeJNI.RIB_type_string(buffer, ahb.swigValue());
    }

    public static long RIB_find_file_provider(String str, String str2, String str3) {
        return MilesBridgeJNI.RIB_find_file_provider(str, str2, str3);
    }

    /* renamed from: a */
    public static long m21710a(String str, String str2, aBO abo) {
        return MilesBridgeJNI.RIB_find_provider(str, str2, aBO.m12884g(abo));
    }

    /* renamed from: a */
    public static long m21708a(C5551aNr anr, String str) {
        return MilesBridgeJNI.RIB_load_static_provider_library(C5551aNr.m16779a(anr), str);
    }

    public static long RIB_find_files_provider(String str, String str2, String str3, String str4, String str5) {
        return MilesBridgeJNI.RIB_find_files_provider(str, str2, str3, str4, str5);
    }

    public static long RIB_find_file_dec_provider(String str, String str2, long j, String str3, String str4) {
        return MilesBridgeJNI.RIB_find_file_dec_provider(str, str2, j, str3, str4);
    }

    public static int RIB_load_application_providers(String str) {
        return MilesBridgeJNI.RIB_load_application_providers(str);
    }

    public static void RIB_set_provider_user_data(long j, long j2, int i) {
        MilesBridgeJNI.RIB_set_provider_user_data(j, j2, i);
    }

    public static int RIB_provider_user_data(long j, long j2) {
        return MilesBridgeJNI.RIB_provider_user_data(j, j2);
    }

    public static void RIB_set_provider_system_data(long j, long j2, int i) {
        MilesBridgeJNI.RIB_set_provider_system_data(j, j2, i);
    }

    public static int RIB_provider_system_data(long j, long j2) {
        return MilesBridgeJNI.RIB_provider_system_data(j, j2);
    }

    public static String RIB_error() {
        return MilesBridgeJNI.RIB_error();
    }

    public static long AIL_get_timer_highest_delay() {
        return MilesBridgeJNI.AIL_get_timer_highest_delay();
    }

    public static void AIL_serve() {
        MilesBridgeJNI.AIL_serve();
    }

    /* renamed from: hb */
    public static aBO m21902hb(long j) {
        long AIL_mem_alloc_lock = MilesBridgeJNI.AIL_mem_alloc_lock(j);
        if (AIL_mem_alloc_lock == 0) {
            return null;
        }
        return new aBO(AIL_mem_alloc_lock, false);
    }

    /* renamed from: c */
    public static void m21849c(aBO abo) {
        MilesBridgeJNI.AIL_mem_free_lock(aBO.m12884g(abo));
    }

    public static int AIL_file_error() {
        return MilesBridgeJNI.AIL_file_error();
    }

    public static int AIL_file_size(String str) {
        return MilesBridgeJNI.AIL_file_size(str);
    }

    public static Buffer AIL_file_read(String str, Buffer buffer) {
        return MilesBridgeJNI.AIL_file_read(str, buffer);
    }

    /* renamed from: a */
    public static int m21699a(String str, aBO abo, long j) {
        return MilesBridgeJNI.AIL_file_write(str, aBO.m12884g(abo), j);
    }

    /* renamed from: a */
    public static int m21700a(String str, aBO abo, long j, int i, int i2) {
        return MilesBridgeJNI.AIL_WAV_file_write(str, aBO.m12884g(abo), j, i, i2);
    }

    /* renamed from: b */
    public static C0569Hs m21807b(C0569Hs hs) {
        long AIL_mem_use_malloc = MilesBridgeJNI.AIL_mem_use_malloc(C0569Hs.m5277a(hs));
        if (AIL_mem_use_malloc == 0) {
            return null;
        }
        return new C0569Hs(AIL_mem_use_malloc, false);
    }

    /* renamed from: b */
    public static C1385UI m21808b(C1385UI ui) {
        long AIL_mem_use_free = MilesBridgeJNI.AIL_mem_use_free(C1385UI.m10211a(ui));
        if (AIL_mem_use_free == 0) {
            return null;
        }
        return new C1385UI(AIL_mem_use_free, false);
    }

    public static String AIL_ftoa(float f) {
        return MilesBridgeJNI.AIL_ftoa(f);
    }

    public static int AIL_startup() {
        return MilesBridgeJNI.AIL_startup();
    }

    public static int AIL_get_preference(long j) {
        return MilesBridgeJNI.AIL_get_preference(j);
    }

    public static void AIL_shutdown() {
        MilesBridgeJNI.AIL_shutdown();
    }

    public static int AIL_set_preference(long j, int i) {
        return MilesBridgeJNI.AIL_set_preference(j, i);
    }

    public static String AIL_last_error() {
        return MilesBridgeJNI.AIL_last_error();
    }

    public static void AIL_set_error(String str) {
        MilesBridgeJNI.AIL_set_error(str);
    }

    public static void AIL_debug_printf(String str) {
        MilesBridgeJNI.AIL_debug_printf(str);
    }

    public static long AIL_MMX_available() {
        return MilesBridgeJNI.AIL_MMX_available();
    }

    public static void AIL_lock() {
        MilesBridgeJNI.AIL_lock();
    }

    public static void AIL_unlock() {
        MilesBridgeJNI.AIL_unlock();
    }

    public static void AIL_lock_mutex() {
        MilesBridgeJNI.AIL_lock_mutex();
    }

    public static void AIL_unlock_mutex() {
        MilesBridgeJNI.AIL_unlock_mutex();
    }

    public static void AIL_delay(int i) {
        MilesBridgeJNI.AIL_delay(i);
    }

    public static int AIL_background() {
        return MilesBridgeJNI.AIL_background();
    }

    /* renamed from: a */
    public static C1824aS m21720a(C1824aS aSVar, int i) {
        long AIL_register_trace_callback = MilesBridgeJNI.AIL_register_trace_callback(C1824aS.m18147a(aSVar), i);
        if (AIL_register_trace_callback == 0) {
            return null;
        }
        return new C1824aS(AIL_register_trace_callback, false);
    }

    /* renamed from: b */
    public static int m21803b(C5828abI abi) {
        return MilesBridgeJNI.AIL_register_timer(C5828abI.m19941a(abi));
    }

    public static long AIL_set_timer_user(int i, long j) {
        return MilesBridgeJNI.AIL_set_timer_user(i, j);
    }

    public static void AIL_set_timer_period(int i, long j) {
        MilesBridgeJNI.AIL_set_timer_period(i, j);
    }

    public static void AIL_set_timer_frequency(int i, long j) {
        MilesBridgeJNI.AIL_set_timer_frequency(i, j);
    }

    public static void AIL_set_timer_divisor(int i, long j) {
        MilesBridgeJNI.AIL_set_timer_divisor(i, j);
    }

    public static void AIL_start_timer(int i) {
        MilesBridgeJNI.AIL_start_timer(i);
    }

    public static void AIL_start_all_timers() {
        MilesBridgeJNI.AIL_start_all_timers();
    }

    public static void AIL_stop_timer(int i) {
        MilesBridgeJNI.AIL_stop_timer(i);
    }

    public static void AIL_stop_all_timers() {
        MilesBridgeJNI.AIL_stop_all_timers();
    }

    public static void AIL_release_timer_handle(int i) {
        MilesBridgeJNI.AIL_release_timer_handle(i);
    }

    public static void AIL_release_all_timers() {
        MilesBridgeJNI.AIL_release_all_timers();
    }

    public static C1266Si bVA() {
        long AIL_HWND = MilesBridgeJNI.AIL_HWND();
        if (AIL_HWND == 0) {
            return null;
        }
        return new C1266Si(AIL_HWND, false);
    }

    /* renamed from: a */
    public static aNZ m21718a(long j, int i, int i2, long j2) {
        long AIL_open_digital_driver = MilesBridgeJNI.AIL_open_digital_driver(j, i, i2, j2);
        if (AIL_open_digital_driver == 0) {
            return null;
        }
        return new aNZ(AIL_open_digital_driver, false);
    }

    /* renamed from: c */
    public static void m21851c(aNZ anz) {
        MilesBridgeJNI.AIL_close_digital_driver(aNZ.m16621q(anz));
    }

    /* renamed from: d */
    public static int m21861d(aNZ anz) {
        return MilesBridgeJNI.AIL_digital_handle_release(aNZ.m16621q(anz));
    }

    /* renamed from: e */
    public static int m21873e(aNZ anz) {
        return MilesBridgeJNI.AIL_digital_handle_reacquire(aNZ.m16621q(anz));
    }

    public static String AIL_set_redist_directory(String str) {
        return MilesBridgeJNI.AIL_set_redist_directory(str);
    }

    public static int AIL_background_CPU_percent() {
        return MilesBridgeJNI.AIL_background_CPU_percent();
    }

    /* renamed from: f */
    public static int m21883f(aNZ anz) {
        return MilesBridgeJNI.AIL_digital_CPU_percent(aNZ.m16621q(anz));
    }

    /* renamed from: g */
    public static int m21891g(aNZ anz) {
        return MilesBridgeJNI.AIL_digital_latency(aNZ.m16621q(anz));
    }

    /* renamed from: h */
    public static C1467Vb m21900h(aNZ anz) {
        long AIL_allocate_sample_handle = MilesBridgeJNI.AIL_allocate_sample_handle(aNZ.m16621q(anz));
        if (AIL_allocate_sample_handle == 0) {
            return null;
        }
        return new C1467Vb(AIL_allocate_sample_handle, false);
    }

    /* renamed from: a */
    public static void m21768a(aNZ anz, aMJ amj, int i, float f) {
        MilesBridgeJNI.AIL_set_speaker_configuration(aNZ.m16621q(anz), aMJ.m16344m(amj), i, f);
    }

    /* renamed from: a */
    public static aMJ m21717a(aNZ anz, C2480fe feVar, C2480fe feVar2, C2512gC gCVar, C0266DP dp) {
        long AIL_speaker_configuration = MilesBridgeJNI.AIL_speaker_configuration(aNZ.m16621q(anz), C2480fe.m31244a(feVar), C2480fe.m31244a(feVar2), C2512gC.m31656a(gCVar), C0266DP.m2393a(dp));
        if (AIL_speaker_configuration == 0) {
            return null;
        }
        return new aMJ(AIL_speaker_configuration, false);
    }

    /* renamed from: a */
    public static void m21769a(aNZ anz, C5853abh abh, int i) {
        MilesBridgeJNI.AIL_set_listener_relative_receiver_array(aNZ.m16621q(anz), C5853abh.m20059a(abh), i);
    }

    /* renamed from: a */
    public static C5853abh m21723a(aNZ anz, C2480fe feVar) {
        long AIL_listener_relative_receiver_array = MilesBridgeJNI.AIL_listener_relative_receiver_array(aNZ.m16621q(anz), C2480fe.m31244a(feVar));
        if (AIL_listener_relative_receiver_array == 0) {
            return null;
        }
        return new C5853abh(AIL_listener_relative_receiver_array, false);
    }

    /* renamed from: a */
    public static void m21772a(aNZ anz, C2512gC gCVar, C2512gC gCVar2, C6426ami ami, int i) {
        MilesBridgeJNI.AIL_set_speaker_reverb_levels(aNZ.m16621q(anz), C2512gC.m31656a(gCVar), C2512gC.m31656a(gCVar2), C6426ami.m23976a(ami), i);
    }

    /* renamed from: e */
    public static void m21877e(C1467Vb vb) {
        MilesBridgeJNI.AIL_release_sample_handle(C1467Vb.m10693c(vb));
    }

    /* renamed from: a */
    public static int m21665a(C1467Vb vb, int i, long j) {
        return MilesBridgeJNI.AIL_init_sample(C1467Vb.m10693c(vb), i, j);
    }

    /* renamed from: a */
    public static int m21675a(C1467Vb vb, Buffer buffer, int i) {
        return MilesBridgeJNI.AIL_set_sample_file(C1467Vb.m10693c(vb), buffer, i);
    }

    /* renamed from: a */
    public static int m21671a(C1467Vb vb, aHS ahs) {
        return MilesBridgeJNI.AIL_set_sample_info(C1467Vb.m10693c(vb), aHS.m15166b(ahs));
    }

    /* renamed from: a */
    public static int m21674a(C1467Vb vb, String str, Buffer buffer, long j, int i) {
        return MilesBridgeJNI.AIL_set_named_sample_file(C1467Vb.m10693c(vb), str, buffer, j, i);
    }

    /* renamed from: a */
    public static long m21705a(C1467Vb vb, C5297aDx adx, long j) {
        return MilesBridgeJNI.AIL_set_sample_processor(C1467Vb.m10693c(vb), adx.swigValue(), j);
    }

    /* renamed from: a */
    public static long m21707a(aNZ anz, aMG amg, long j) {
        return MilesBridgeJNI.AIL_set_digital_driver_processor(aNZ.m16621q(anz), amg.swigValue(), j);
    }

    /* renamed from: a */
    public static long m21704a(C1467Vb vb, C5297aDx adx) {
        return MilesBridgeJNI.AIL_sample_processor(C1467Vb.m10693c(vb), adx.swigValue());
    }

    /* renamed from: a */
    public static long m21706a(aNZ anz, aMG amg) {
        return MilesBridgeJNI.AIL_digital_driver_processor(aNZ.m16621q(anz), amg.swigValue());
    }

    /* renamed from: a */
    public static void m21748a(C1467Vb vb, long j) {
        MilesBridgeJNI.AIL_set_sample_adpcm_block_size(C1467Vb.m10693c(vb), j);
    }

    /* renamed from: a */
    public static void m21757a(C1467Vb vb, Buffer buffer, long j) {
        MilesBridgeJNI.AIL_set_sample_address(C1467Vb.m10693c(vb), buffer, j);
    }

    /* renamed from: f */
    public static void m21888f(C1467Vb vb) {
        MilesBridgeJNI.AIL_start_sample(C1467Vb.m10693c(vb));
    }

    /* renamed from: g */
    public static void m21896g(C1467Vb vb) {
        MilesBridgeJNI.AIL_stop_sample(C1467Vb.m10693c(vb));
    }

    /* renamed from: h */
    public static void m21901h(C1467Vb vb) {
        MilesBridgeJNI.AIL_resume_sample(C1467Vb.m10693c(vb));
    }

    /* renamed from: i */
    public static void m21909i(C1467Vb vb) {
        MilesBridgeJNI.AIL_end_sample(C1467Vb.m10693c(vb));
    }

    /* renamed from: a */
    public static void m21746a(C1467Vb vb, int i) {
        MilesBridgeJNI.AIL_set_sample_playback_rate(C1467Vb.m10693c(vb), i);
    }

    /* renamed from: a */
    public static void m21740a(C1467Vb vb, float f, float f2) {
        MilesBridgeJNI.AIL_set_sample_volume_pan(C1467Vb.m10693c(vb), f, f2);
    }

    /* renamed from: b */
    public static void m21813b(C1467Vb vb, float f, float f2) {
        MilesBridgeJNI.AIL_set_sample_volume_levels(C1467Vb.m10693c(vb), f, f2);
    }

    /* renamed from: c */
    public static void m21846c(C1467Vb vb, float f, float f2) {
        MilesBridgeJNI.AIL_set_sample_reverb_levels(C1467Vb.m10693c(vb), f, f2);
    }

    /* renamed from: a */
    public static void m21739a(C1467Vb vb, float f) {
        MilesBridgeJNI.AIL_set_sample_low_pass_cut_off(C1467Vb.m10693c(vb), f);
    }

    /* renamed from: b */
    public static void m21816b(C1467Vb vb, int i) {
        MilesBridgeJNI.AIL_set_sample_loop_count(C1467Vb.m10693c(vb), i);
    }

    /* renamed from: a */
    public static void m21747a(C1467Vb vb, int i, int i2) {
        MilesBridgeJNI.AIL_set_sample_loop_block(C1467Vb.m10693c(vb), i, i2);
    }

    /* renamed from: a */
    public static int m21673a(C1467Vb vb, C2480fe feVar, C2480fe feVar2) {
        return MilesBridgeJNI.AIL_sample_loop_block(C1467Vb.m10693c(vb), C2480fe.m31244a(feVar), C2480fe.m31244a(feVar2));
    }

    /* renamed from: j */
    public static long m21912j(C1467Vb vb) {
        return MilesBridgeJNI.AIL_sample_status(C1467Vb.m10693c(vb));
    }

    /* renamed from: k */
    public static int m21914k(C1467Vb vb) {
        return MilesBridgeJNI.AIL_sample_playback_rate(C1467Vb.m10693c(vb));
    }

    /* renamed from: a */
    public static void m21752a(C1467Vb vb, C2512gC gCVar, C2512gC gCVar2) {
        MilesBridgeJNI.AIL_sample_volume_pan(C1467Vb.m10693c(vb), C2512gC.m31656a(gCVar), C2512gC.m31656a(gCVar2));
    }

    /* renamed from: a */
    public static int m21672a(C1467Vb vb, C5758aVq avq) {
        return MilesBridgeJNI.AIL_sample_channel_count(C1467Vb.m10693c(vb), C5758aVq.m19143h(avq));
    }

    /* renamed from: a */
    public static void m21758a(C1467Vb vb, Buffer buffer, Buffer buffer2) {
        MilesBridgeJNI.AIL_sample_volume_levels(C1467Vb.m10693c(vb), buffer, buffer2);
    }

    /* renamed from: b */
    public static void m21819b(C1467Vb vb, C2512gC gCVar, C2512gC gCVar2) {
        MilesBridgeJNI.AIL_sample_reverb_levels(C1467Vb.m10693c(vb), C2512gC.m31656a(gCVar), C2512gC.m31656a(gCVar2));
    }

    /* renamed from: l */
    public static float m21917l(C1467Vb vb) {
        return MilesBridgeJNI.AIL_sample_low_pass_cut_off(C1467Vb.m10693c(vb));
    }

    /* renamed from: m */
    public static int m21921m(C1467Vb vb) {
        return MilesBridgeJNI.AIL_sample_loop_count(C1467Vb.m10693c(vb));
    }

    /* renamed from: a */
    public static void m21762a(aNZ anz, float f) {
        MilesBridgeJNI.AIL_set_digital_master_volume_level(aNZ.m16621q(anz), f);
    }

    /* renamed from: i */
    public static float m21905i(aNZ anz) {
        return MilesBridgeJNI.AIL_digital_master_volume_level(aNZ.m16621q(anz));
    }

    /* renamed from: a */
    public static void m21743a(C1467Vb vb, float f, float f2, float f3, float f4, float f5) {
        MilesBridgeJNI.AIL_set_sample_51_volume_pan(C1467Vb.m10693c(vb), f, f2, f3, f4, f5);
    }

    /* renamed from: a */
    public static void m21755a(C1467Vb vb, C2512gC gCVar, C2512gC gCVar2, C2512gC gCVar3, C2512gC gCVar4, C2512gC gCVar5) {
        MilesBridgeJNI.AIL_sample_51_volume_pan(C1467Vb.m10693c(vb), C2512gC.m31656a(gCVar), C2512gC.m31656a(gCVar2), C2512gC.m31656a(gCVar3), C2512gC.m31656a(gCVar4), C2512gC.m31656a(gCVar5));
    }

    /* renamed from: a */
    public static void m21744a(C1467Vb vb, float f, float f2, float f3, float f4, float f5, float f6) {
        MilesBridgeJNI.AIL_set_sample_51_volume_levels(C1467Vb.m10693c(vb), f, f2, f3, f4, f5, f6);
    }

    /* renamed from: a */
    public static void m21756a(C1467Vb vb, C2512gC gCVar, C2512gC gCVar2, C2512gC gCVar3, C2512gC gCVar4, C2512gC gCVar5, C2512gC gCVar6) {
        MilesBridgeJNI.AIL_sample_51_volume_levels(C1467Vb.m10693c(vb), C2512gC.m31656a(gCVar), C2512gC.m31656a(gCVar2), C2512gC.m31656a(gCVar3), C2512gC.m31656a(gCVar4), C2512gC.m31656a(gCVar5), C2512gC.m31656a(gCVar6));
    }

    /* renamed from: a */
    public static void m21764a(aNZ anz, float f, float f2, float f3) {
        MilesBridgeJNI.AIL_set_digital_master_reverb(aNZ.m16621q(anz), f, f2, f3);
    }

    /* renamed from: a */
    public static void m21773a(aNZ anz, C2512gC gCVar, C2512gC gCVar2, C2512gC gCVar3) {
        MilesBridgeJNI.AIL_digital_master_reverb(aNZ.m16621q(anz), C2512gC.m31656a(gCVar), C2512gC.m31656a(gCVar2), C2512gC.m31656a(gCVar3));
    }

    /* renamed from: a */
    public static void m21763a(aNZ anz, float f, float f2) {
        MilesBridgeJNI.AIL_set_digital_master_reverb_levels(aNZ.m16621q(anz), f, f2);
    }

    /* renamed from: a */
    public static void m21771a(aNZ anz, C2512gC gCVar, C2512gC gCVar2) {
        MilesBridgeJNI.AIL_digital_master_reverb_levels(aNZ.m16621q(anz), C2512gC.m31656a(gCVar), C2512gC.m31656a(gCVar2));
    }

    /* renamed from: a */
    public static int m21689a(aNZ anz, int i, int i2) {
        return MilesBridgeJNI.AIL_minimum_sample_buffer_size(aNZ.m16621q(anz), i, i2);
    }

    /* renamed from: c */
    public static int m21838c(C1467Vb vb, int i) {
        return MilesBridgeJNI.AIL_set_sample_buffer_count(C1467Vb.m10693c(vb), i);
    }

    /* renamed from: n */
    public static int m21923n(C1467Vb vb) {
        return MilesBridgeJNI.AIL_sample_buffer_count(C1467Vb.m10693c(vb));
    }

    /* renamed from: o */
    public static int m21925o(C1467Vb vb) {
        return MilesBridgeJNI.AIL_sample_buffer_available(C1467Vb.m10693c(vb));
    }

    /* renamed from: a */
    public static int m21666a(C1467Vb vb, int i, aBO abo, long j) {
        return MilesBridgeJNI.AIL_load_sample_buffer(C1467Vb.m10693c(vb), i, aBO.m12884g(abo), j);
    }

    /* renamed from: a */
    public static void m21749a(C1467Vb vb, long j, int i) {
        MilesBridgeJNI.AIL_request_EOB_ASI_reset(C1467Vb.m10693c(vb), j, i);
    }

    /* renamed from: a */
    public static int m21667a(C1467Vb vb, int i, C5758aVq avq, C5758aVq avq2, C2480fe feVar, C2480fe feVar2) {
        return MilesBridgeJNI.AIL_sample_buffer_info(C1467Vb.m10693c(vb), i, C5758aVq.m19143h(avq), C5758aVq.m19143h(avq2), C2480fe.m31244a(feVar), C2480fe.m31244a(feVar2));
    }

    /* renamed from: p */
    public static long m21926p(C1467Vb vb) {
        return MilesBridgeJNI.AIL_sample_granularity(C1467Vb.m10693c(vb));
    }

    /* renamed from: b */
    public static void m21817b(C1467Vb vb, long j) {
        MilesBridgeJNI.AIL_set_sample_position(C1467Vb.m10693c(vb), j);
    }

    /* renamed from: q */
    public static long m21928q(C1467Vb vb) {
        return MilesBridgeJNI.AIL_sample_position(C1467Vb.m10693c(vb));
    }

    /* renamed from: a */
    public static C3346qY m21731a(C1467Vb vb, C3346qY qYVar) {
        long AIL_register_SOB_callback = MilesBridgeJNI.AIL_register_SOB_callback(C1467Vb.m10693c(vb), C3346qY.m37645a(qYVar));
        if (AIL_register_SOB_callback == 0) {
            return null;
        }
        return new C3346qY(AIL_register_SOB_callback, false);
    }

    /* renamed from: b */
    public static C3346qY m21810b(C1467Vb vb, C3346qY qYVar) {
        long AIL_register_EOB_callback = MilesBridgeJNI.AIL_register_EOB_callback(C1467Vb.m10693c(vb), C3346qY.m37645a(qYVar));
        if (AIL_register_EOB_callback == 0) {
            return null;
        }
        return new C3346qY(AIL_register_EOB_callback, false);
    }

    /* renamed from: c */
    public static C3346qY m21844c(C1467Vb vb, C3346qY qYVar) {
        long AIL_register_EOS_callback = MilesBridgeJNI.AIL_register_EOS_callback(C1467Vb.m10693c(vb), C3346qY.m37645a(qYVar));
        if (AIL_register_EOS_callback == 0) {
            return null;
        }
        return new C3346qY(AIL_register_EOS_callback, false);
    }

    /* renamed from: a */
    public static C6266aje m21725a(C1467Vb vb, C6266aje aje) {
        long AIL_register_falloff_function_callback = MilesBridgeJNI.AIL_register_falloff_function_callback(C1467Vb.m10693c(vb), C6266aje.m23016b(aje));
        if (AIL_register_falloff_function_callback == 0) {
            return null;
        }
        return new C6266aje(AIL_register_falloff_function_callback, false);
    }

    /* renamed from: b */
    public static void m21818b(C1467Vb vb, long j, int i) {
        MilesBridgeJNI.AIL_set_sample_user_data(C1467Vb.m10693c(vb), j, i);
    }

    /* renamed from: c */
    public static int m21839c(C1467Vb vb, long j) {
        return MilesBridgeJNI.AIL_sample_user_data(C1467Vb.m10693c(vb), j);
    }

    /* renamed from: j */
    public static int m21911j(aNZ anz) {
        return MilesBridgeJNI.AIL_active_sample_count(aNZ.m16621q(anz));
    }

    /* renamed from: a */
    public static void m21770a(aNZ anz, C2480fe feVar, C2480fe feVar2, String str) {
        MilesBridgeJNI.AIL_digital_configuration(aNZ.m16621q(anz), C2480fe.m31244a(feVar), C2480fe.m31244a(feVar2), str);
    }

    /* renamed from: a */
    public static int m21684a(aBO abo, C2938lt ltVar, Buffer buffer, Buffer buffer2, Buffer buffer3) {
        return MilesBridgeJNI.AIL_platform_property(aBO.m12884g(abo), ltVar.swigValue(), buffer, buffer2, buffer3);
    }

    /* renamed from: a */
    public static void m21751a(C1467Vb vb, C2224cz czVar, C2224cz czVar2) {
        MilesBridgeJNI.AIL_get_DirectSound_info(C1467Vb.m10693c(vb), C2224cz.m28711a(czVar), C2224cz.m28711a(czVar2));
    }

    /* renamed from: a */
    public static int m21690a(aNZ anz, C1266Si si) {
        return MilesBridgeJNI.AIL_set_DirectSound_HWND(aNZ.m16621q(anz), C1266Si.m9549a(si));
    }

    /* renamed from: d */
    public static void m21865d(C1467Vb vb, int i) {
        MilesBridgeJNI.AIL_set_sample_ms_position(C1467Vb.m10693c(vb), i);
    }

    /* renamed from: b */
    public static void m21821b(C1467Vb vb, Buffer buffer, Buffer buffer2) {
        MilesBridgeJNI.AIL_sample_ms_position(C1467Vb.m10693c(vb), buffer, buffer2);
    }

    /* renamed from: a */
    public static ayV m21727a(C5740aUy auy) {
        long AIL_open_input = MilesBridgeJNI.AIL_open_input(C5740aUy.m18768c(auy));
        if (AIL_open_input == 0) {
            return null;
        }
        return new ayV(AIL_open_input, false);
    }

    /* renamed from: a */
    public static void m21790a(ayV ayv) {
        MilesBridgeJNI.AIL_close_input(ayV.m27430c(ayv));
    }

    /* renamed from: b */
    public static C5740aUy m21809b(ayV ayv) {
        long AIL_get_input_info = MilesBridgeJNI.AIL_get_input_info(ayV.m27430c(ayv));
        if (AIL_get_input_info == 0) {
            return null;
        }
        return new C5740aUy(AIL_get_input_info, false);
    }

    /* renamed from: a */
    public static int m21696a(ayV ayv, int i) {
        return MilesBridgeJNI.AIL_set_input_state(ayV.m27430c(ayv), i);
    }

    /* renamed from: hc */
    public static C6297akJ m21903hc(long j) {
        long AIL_open_XMIDI_driver = MilesBridgeJNI.AIL_open_XMIDI_driver(j);
        if (AIL_open_XMIDI_driver == 0) {
            return null;
        }
        return new C6297akJ(AIL_open_XMIDI_driver, false);
    }

    /* renamed from: b */
    public static void m21828b(C6297akJ akj) {
        MilesBridgeJNI.AIL_close_XMIDI_driver(C6297akJ.m23148j(akj));
    }

    /* renamed from: a */
    public static int m21694a(C6683arf arf, C3130oH oHVar, int i) {
        return MilesBridgeJNI.AIL_midiOutOpen(C6683arf.m25535a(arf), C3130oH.m36604a(oHVar), i);
    }

    /* renamed from: c */
    public static void m21855c(C6297akJ akj) {
        MilesBridgeJNI.AIL_midiOutClose(C6297akJ.m23148j(akj));
    }

    /* renamed from: d */
    public static int m21862d(C6297akJ akj) {
        return MilesBridgeJNI.AIL_MIDI_handle_release(C6297akJ.m23148j(akj));
    }

    /* renamed from: e */
    public static int m21874e(C6297akJ akj) {
        return MilesBridgeJNI.AIL_MIDI_handle_reacquire(C6297akJ.m23148j(akj));
    }

    /* renamed from: f */
    public static C6481anl m21887f(C6297akJ akj) {
        long AIL_allocate_sequence_handle = MilesBridgeJNI.AIL_allocate_sequence_handle(C6297akJ.m23148j(akj));
        if (AIL_allocate_sequence_handle == 0) {
            return null;
        }
        return new C6481anl(AIL_allocate_sequence_handle, false);
    }

    /* renamed from: a */
    public static void m21779a(C6481anl anl) {
        MilesBridgeJNI.AIL_release_sequence_handle(C6481anl.m24294k(anl));
    }

    /* renamed from: a */
    public static int m21693a(C6481anl anl, Buffer buffer, int i) {
        return MilesBridgeJNI.AIL_init_sequence(C6481anl.m24294k(anl), buffer, i);
    }

    /* renamed from: b */
    public static void m21830b(C6481anl anl) {
        MilesBridgeJNI.AIL_start_sequence(C6481anl.m24294k(anl));
    }

    /* renamed from: c */
    public static void m21856c(C6481anl anl) {
        MilesBridgeJNI.AIL_stop_sequence(C6481anl.m24294k(anl));
    }

    /* renamed from: d */
    public static void m21867d(C6481anl anl) {
        MilesBridgeJNI.AIL_resume_sequence(C6481anl.m24294k(anl));
    }

    /* renamed from: e */
    public static void m21880e(C6481anl anl) {
        MilesBridgeJNI.AIL_end_sequence(C6481anl.m24294k(anl));
    }

    /* renamed from: a */
    public static void m21781a(C6481anl anl, int i, int i2) {
        MilesBridgeJNI.AIL_set_sequence_tempo(C6481anl.m24294k(anl), i, i2);
    }

    /* renamed from: b */
    public static void m21831b(C6481anl anl, int i, int i2) {
        MilesBridgeJNI.AIL_set_sequence_volume(C6481anl.m24294k(anl), i, i2);
    }

    /* renamed from: a */
    public static void m21780a(C6481anl anl, int i) {
        MilesBridgeJNI.AIL_set_sequence_loop_count(C6481anl.m24294k(anl), i);
    }

    /* renamed from: f */
    public static long m21885f(C6481anl anl) {
        return MilesBridgeJNI.AIL_sequence_status(C6481anl.m24294k(anl));
    }

    /* renamed from: g */
    public static int m21893g(C6481anl anl) {
        return MilesBridgeJNI.AIL_sequence_tempo(C6481anl.m24294k(anl));
    }

    /* renamed from: h */
    public static int m21898h(C6481anl anl) {
        return MilesBridgeJNI.AIL_sequence_volume(C6481anl.m24294k(anl));
    }

    /* renamed from: i */
    public static int m21907i(C6481anl anl) {
        return MilesBridgeJNI.AIL_sequence_loop_count(C6481anl.m24294k(anl));
    }

    /* renamed from: a */
    public static void m21776a(C6297akJ akj, int i) {
        MilesBridgeJNI.AIL_set_XMIDI_master_volume(C6297akJ.m23148j(akj), i);
    }

    /* renamed from: g */
    public static int m21892g(C6297akJ akj) {
        return MilesBridgeJNI.AIL_XMIDI_master_volume(C6297akJ.m23148j(akj));
    }

    /* renamed from: h */
    public static int m21897h(C6297akJ akj) {
        return MilesBridgeJNI.AIL_active_sequence_count(C6297akJ.m23148j(akj));
    }

    /* renamed from: c */
    public static int m21841c(C6481anl anl, int i, int i2) {
        return MilesBridgeJNI.AIL_controller_value(C6481anl.m24294k(anl), i, i2);
    }

    /* renamed from: b */
    public static int m21804b(C6481anl anl, int i) {
        return MilesBridgeJNI.AIL_channel_notes(C6481anl.m24294k(anl), i);
    }

    /* renamed from: a */
    public static void m21785a(C6481anl anl, C2480fe feVar, C2480fe feVar2) {
        MilesBridgeJNI.AIL_sequence_position(C6481anl.m24294k(anl), C2480fe.m31244a(feVar), C2480fe.m31244a(feVar2));
    }

    /* renamed from: a */
    public static void m21782a(C6481anl anl, long j) {
        MilesBridgeJNI.AIL_branch_index(C6481anl.m24294k(anl), j);
    }

    /* renamed from: a */
    public static C5687aSx m21721a(C6481anl anl, C5687aSx asx) {
        long AIL_register_prefix_callback = MilesBridgeJNI.AIL_register_prefix_callback(C6481anl.m24294k(anl), C5687aSx.m18319b(asx));
        if (AIL_register_prefix_callback == 0) {
            return null;
        }
        return new C5687aSx(AIL_register_prefix_callback, false);
    }

    /* renamed from: a */
    public static C5332aFg m21716a(C6481anl anl, C5332aFg afg) {
        long AIL_register_trigger_callback = MilesBridgeJNI.AIL_register_trigger_callback(C6481anl.m24294k(anl), C5332aFg.m14623b(afg));
        if (AIL_register_trigger_callback == 0) {
            return null;
        }
        return new C5332aFg(AIL_register_trigger_callback, false);
    }

    /* renamed from: a */
    public static C6362alW m21726a(C6481anl anl, C6362alW alw) {
        long AIL_register_sequence_callback = MilesBridgeJNI.AIL_register_sequence_callback(C6481anl.m24294k(anl), C6362alW.m23767a(alw));
        if (AIL_register_sequence_callback == 0) {
            return null;
        }
        return new C6362alW(AIL_register_sequence_callback, false);
    }

    /* renamed from: a */
    public static C3656tL m21732a(C6481anl anl, C3656tL tLVar) {
        long AIL_register_beat_callback = MilesBridgeJNI.AIL_register_beat_callback(C6481anl.m24294k(anl), C3656tL.m39583a(tLVar));
        if (AIL_register_beat_callback == 0) {
            return null;
        }
        return new C3656tL(AIL_register_beat_callback, false);
    }

    /* renamed from: a */
    public static C5550aNq m21719a(C6297akJ akj, C5550aNq anq) {
        long AIL_register_event_callback = MilesBridgeJNI.AIL_register_event_callback(C6297akJ.m23148j(akj), C5550aNq.m16778c(anq));
        if (AIL_register_event_callback == 0) {
            return null;
        }
        return new C5550aNq(AIL_register_event_callback, false);
    }

    /* renamed from: a */
    public static C5944adU m21724a(C6297akJ akj, C5944adU adu) {
        long AIL_register_timbre_callback = MilesBridgeJNI.AIL_register_timbre_callback(C6297akJ.m23148j(akj), C5944adU.m20899b(adu));
        if (AIL_register_timbre_callback == 0) {
            return null;
        }
        return new C5944adU(AIL_register_timbre_callback, false);
    }

    /* renamed from: a */
    public static void m21783a(C6481anl anl, long j, int i) {
        MilesBridgeJNI.AIL_set_sequence_user_data(C6481anl.m24294k(anl), j, i);
    }

    /* renamed from: b */
    public static int m21805b(C6481anl anl, long j) {
        return MilesBridgeJNI.AIL_sequence_user_data(C6481anl.m24294k(anl), j);
    }

    /* renamed from: a */
    public static void m21784a(C6481anl anl, C6626aqa aqa) {
        MilesBridgeJNI.AIL_register_ICA_array(C6481anl.m24294k(anl), C6626aqa.m25149e(aqa));
    }

    /* renamed from: i */
    public static int m21906i(C6297akJ akj) {
        return MilesBridgeJNI.AIL_lock_channel(C6297akJ.m23148j(akj));
    }

    /* renamed from: b */
    public static void m21829b(C6297akJ akj, int i) {
        MilesBridgeJNI.AIL_release_channel(C6297akJ.m23148j(akj), i);
    }

    /* renamed from: d */
    public static void m21869d(C6481anl anl, int i, int i2) {
        MilesBridgeJNI.AIL_map_sequence_channel(C6481anl.m24294k(anl), i, i2);
    }

    /* renamed from: c */
    public static int m21840c(C6481anl anl, int i) {
        return MilesBridgeJNI.AIL_true_sequence_channel(C6481anl.m24294k(anl), i);
    }

    /* renamed from: a */
    public static void m21778a(C6297akJ akj, C6481anl anl, int i, int i2, int i3) {
        MilesBridgeJNI.AIL_send_channel_voice_message(C6297akJ.m23148j(akj), C6481anl.m24294k(anl), i, i2, i3);
    }

    /* renamed from: a */
    public static void m21777a(C6297akJ akj, aBO abo) {
        MilesBridgeJNI.AIL_send_sysex_message(C6297akJ.m23148j(akj), aBO.m12884g(abo));
    }

    /* renamed from: a */
    public static C0589IK m21711a(aNZ anz, C6297akJ akj, aBO abo, int i) {
        long AIL_create_wave_synthesizer = MilesBridgeJNI.AIL_create_wave_synthesizer(aNZ.m16621q(anz), C6297akJ.m23148j(akj), aBO.m12884g(abo), i);
        if (AIL_create_wave_synthesizer == 0) {
            return null;
        }
        return new C0589IK(AIL_create_wave_synthesizer, false);
    }

    /* renamed from: b */
    public static void m21811b(C0589IK ik) {
        MilesBridgeJNI.AIL_destroy_wave_synthesizer(C0589IK.m5329a(ik));
    }

    /* renamed from: d */
    public static void m21868d(C6481anl anl, int i) {
        MilesBridgeJNI.AIL_set_sequence_ms_position(C6481anl.m24294k(anl), i);
    }

    /* renamed from: a */
    public static void m21786a(C6481anl anl, Buffer buffer, Buffer buffer2) {
        MilesBridgeJNI.AIL_sequence_ms_position(C6481anl.m24294k(anl), buffer, buffer2);
    }

    /* renamed from: hd */
    public static C6761atF m21904hd(long j) {
        long AIL_redbook_open = MilesBridgeJNI.AIL_redbook_open(j);
        if (AIL_redbook_open == 0) {
            return null;
        }
        return new C6761atF(AIL_redbook_open, false);
    }

    /* renamed from: qy */
    public static C6761atF m21929qy(int i) {
        long AIL_redbook_open_drive = MilesBridgeJNI.AIL_redbook_open_drive(i);
        if (AIL_redbook_open_drive == 0) {
            return null;
        }
        return new C6761atF(AIL_redbook_open_drive, false);
    }

    /* renamed from: a */
    public static void m21787a(C6761atF atf) {
        MilesBridgeJNI.AIL_redbook_close(C6761atF.m25826m(atf));
    }

    /* renamed from: b */
    public static void m21832b(C6761atF atf) {
        MilesBridgeJNI.AIL_redbook_eject(C6761atF.m25826m(atf));
    }

    /* renamed from: c */
    public static void m21857c(C6761atF atf) {
        MilesBridgeJNI.AIL_redbook_retract(C6761atF.m25826m(atf));
    }

    /* renamed from: d */
    public static long m21863d(C6761atF atf) {
        return MilesBridgeJNI.AIL_redbook_status(C6761atF.m25826m(atf));
    }

    /* renamed from: e */
    public static long m21876e(C6761atF atf) {
        return MilesBridgeJNI.AIL_redbook_tracks(C6761atF.m25826m(atf));
    }

    /* renamed from: f */
    public static long m21886f(C6761atF atf) {
        return MilesBridgeJNI.AIL_redbook_track(C6761atF.m25826m(atf));
    }

    /* renamed from: a */
    public static void m21788a(C6761atF atf, long j, C5758aVq avq, C5758aVq avq2) {
        MilesBridgeJNI.AIL_redbook_track_info(C6761atF.m25826m(atf), j, C5758aVq.m19143h(avq), C5758aVq.m19143h(avq2));
    }

    /* renamed from: g */
    public static long m21895g(C6761atF atf) {
        return MilesBridgeJNI.AIL_redbook_id(C6761atF.m25826m(atf));
    }

    /* renamed from: h */
    public static long m21899h(C6761atF atf) {
        return MilesBridgeJNI.AIL_redbook_position(C6761atF.m25826m(atf));
    }

    /* renamed from: a */
    public static long m21709a(C6761atF atf, long j, long j2) {
        return MilesBridgeJNI.AIL_redbook_play(C6761atF.m25826m(atf), j, j2);
    }

    /* renamed from: i */
    public static long m21908i(C6761atF atf) {
        return MilesBridgeJNI.AIL_redbook_stop(C6761atF.m25826m(atf));
    }

    /* renamed from: j */
    public static long m21913j(C6761atF atf) {
        return MilesBridgeJNI.AIL_redbook_pause(C6761atF.m25826m(atf));
    }

    /* renamed from: k */
    public static long m21915k(C6761atF atf) {
        return MilesBridgeJNI.AIL_redbook_resume(C6761atF.m25826m(atf));
    }

    /* renamed from: l */
    public static float m21918l(C6761atF atf) {
        return MilesBridgeJNI.AIL_redbook_volume_level(C6761atF.m25826m(atf));
    }

    /* renamed from: a */
    public static float m21659a(C6761atF atf, float f) {
        return MilesBridgeJNI.AIL_redbook_set_volume_level(C6761atF.m25826m(atf), f);
    }

    public static long AIL_ms_count() {
        return MilesBridgeJNI.AIL_ms_count();
    }

    public static long AIL_us_count() {
        return MilesBridgeJNI.AIL_us_count();
    }

    /* renamed from: a */
    public static C2689ic m21728a(aNZ anz, String str, int i) {
        long AIL_open_stream = MilesBridgeJNI.AIL_open_stream(aNZ.m16621q(anz), str, i);
        if (AIL_open_stream == 0) {
            return null;
        }
        return new C2689ic(AIL_open_stream, false);
    }

    /* renamed from: b */
    public static void m21833b(C2689ic icVar) {
        MilesBridgeJNI.AIL_close_stream(C2689ic.m33438a(icVar));
    }

    /* renamed from: c */
    public static C1467Vb m21842c(C2689ic icVar) {
        long AIL_stream_sample_handle = MilesBridgeJNI.AIL_stream_sample_handle(C2689ic.m33438a(icVar));
        if (AIL_stream_sample_handle == 0) {
            return null;
        }
        return new C1467Vb(AIL_stream_sample_handle, false);
    }

    /* renamed from: a */
    public static int m21697a(C2689ic icVar, int i) {
        return MilesBridgeJNI.AIL_service_stream(C2689ic.m33438a(icVar), i);
    }

    /* renamed from: d */
    public static void m21870d(C2689ic icVar) {
        MilesBridgeJNI.AIL_start_stream(C2689ic.m33438a(icVar));
    }

    /* renamed from: b */
    public static void m21834b(C2689ic icVar, int i) {
        MilesBridgeJNI.AIL_pause_stream(C2689ic.m33438a(icVar), i);
    }

    /* renamed from: e */
    public static int m21875e(C2689ic icVar) {
        return MilesBridgeJNI.AIL_stream_loop_count(C2689ic.m33438a(icVar));
    }

    /* renamed from: c */
    public static void m21858c(C2689ic icVar, int i) {
        MilesBridgeJNI.AIL_set_stream_loop_count(C2689ic.m33438a(icVar), i);
    }

    /* renamed from: a */
    public static void m21791a(C2689ic icVar, int i, int i2) {
        MilesBridgeJNI.AIL_set_stream_loop_block(C2689ic.m33438a(icVar), i, i2);
    }

    /* renamed from: f */
    public static int m21884f(C2689ic icVar) {
        return MilesBridgeJNI.AIL_stream_status(C2689ic.m33438a(icVar));
    }

    /* renamed from: d */
    public static void m21871d(C2689ic icVar, int i) {
        MilesBridgeJNI.AIL_set_stream_position(C2689ic.m33438a(icVar), i);
    }

    /* renamed from: g */
    public static int m21894g(C2689ic icVar) {
        return MilesBridgeJNI.AIL_stream_position(C2689ic.m33438a(icVar));
    }

    /* renamed from: a */
    public static void m21793a(C2689ic icVar, C2480fe feVar, C2480fe feVar2, C2480fe feVar3, C2480fe feVar4) {
        MilesBridgeJNI.AIL_stream_info(C2689ic.m33438a(icVar), C2480fe.m31244a(feVar), C2480fe.m31244a(feVar2), C2480fe.m31244a(feVar3), C2480fe.m31244a(feVar4));
    }

    /* renamed from: a */
    public static aVC m21722a(C2689ic icVar, aVC avc) {
        long AIL_register_stream_callback = MilesBridgeJNI.AIL_register_stream_callback(C2689ic.m33438a(icVar), aVC.m18785b(avc));
        if (AIL_register_stream_callback == 0) {
            return null;
        }
        return new aVC(AIL_register_stream_callback, false);
    }

    /* renamed from: e */
    public static void m21881e(C2689ic icVar, int i) {
        MilesBridgeJNI.AIL_auto_service_stream(C2689ic.m33438a(icVar), i);
    }

    /* renamed from: a */
    public static void m21792a(C2689ic icVar, long j, int i) {
        MilesBridgeJNI.AIL_set_stream_user_data(C2689ic.m33438a(icVar), j, i);
    }

    /* renamed from: a */
    public static int m21698a(C2689ic icVar, long j) {
        return MilesBridgeJNI.AIL_stream_user_data(C2689ic.m33438a(icVar), j);
    }

    /* renamed from: f */
    public static void m21889f(C2689ic icVar, int i) {
        MilesBridgeJNI.AIL_set_stream_ms_position(C2689ic.m33438a(icVar), i);
    }

    /* renamed from: a */
    public static void m21794a(C2689ic icVar, Buffer buffer, Buffer buffer2) {
        MilesBridgeJNI.AIL_stream_ms_position(C2689ic.m33438a(icVar), buffer, buffer2);
    }

    /* renamed from: a */
    public static void m21736a(C0523HP hp, C5828abI abi, aDQ adq, aNM anm) {
        MilesBridgeJNI.AIL_set_file_callbacks(C0523HP.m5141a(hp), C5828abI.m19941a(abi), aDQ.m13599a(adq), aNM.m16554a(anm));
    }

    /* renamed from: a */
    public static void m21737a(C0523HP hp, C5828abI abi, aDQ adq, C3207pC pCVar, aEA aea) {
        MilesBridgeJNI.AIL_set_file_async_callbacks(C0523HP.m5141a(hp), C5828abI.m19941a(abi), aDQ.m13599a(adq), C3207pC.m36986a(pCVar), aEA.m13926a(aea));
    }

    /* renamed from: a */
    public static C3955xL m21733a(C6297akJ akj, aNZ anz, String str, long j, long j2, int i, int i2) {
        long AIL_DLS_open = MilesBridgeJNI.AIL_DLS_open(C6297akJ.m23148j(akj), aNZ.m16621q(anz), str, j, j2, i, i2);
        if (AIL_DLS_open == 0) {
            return null;
        }
        return new C3955xL(AIL_DLS_open, false);
    }

    /* renamed from: a */
    public static void m21795a(C3955xL xLVar, long j) {
        MilesBridgeJNI.AIL_DLS_close(C3955xL.m40885a(xLVar), j);
    }

    /* renamed from: a */
    public static C3336qQ m21730a(C3955xL xLVar, String str, long j) {
        long AIL_DLS_load_file = MilesBridgeJNI.AIL_DLS_load_file(C3955xL.m40885a(xLVar), str, j);
        if (AIL_DLS_load_file == 0) {
            return null;
        }
        return new C3336qQ(AIL_DLS_load_file, false);
    }

    /* renamed from: a */
    public static C3336qQ m21729a(C3955xL xLVar, aBO abo, long j) {
        long AIL_DLS_load_memory = MilesBridgeJNI.AIL_DLS_load_memory(C3955xL.m40885a(xLVar), aBO.m12884g(abo), j);
        if (AIL_DLS_load_memory == 0) {
            return null;
        }
        return new C3336qQ(AIL_DLS_load_memory, false);
    }

    /* renamed from: a */
    public static void m21797a(C3955xL xLVar, C3336qQ qQVar) {
        MilesBridgeJNI.AIL_DLS_unload(C3955xL.m40885a(xLVar), C3336qQ.m37519a(qQVar));
    }

    /* renamed from: b */
    public static void m21835b(C3955xL xLVar) {
        MilesBridgeJNI.AIL_DLS_compact(C3955xL.m40885a(xLVar));
    }

    /* renamed from: a */
    public static void m21796a(C3955xL xLVar, C5937adN adn, C2480fe feVar) {
        MilesBridgeJNI.AIL_DLS_get_info(C3955xL.m40885a(xLVar), C5937adN.m20815a(adn), C2480fe.m31244a(feVar));
    }

    /* renamed from: c */
    public static C1467Vb m21843c(C3955xL xLVar) {
        long AIL_DLS_sample_handle = MilesBridgeJNI.AIL_DLS_sample_handle(C3955xL.m40885a(xLVar));
        if (AIL_DLS_sample_handle == 0) {
            return null;
        }
        return new C1467Vb(AIL_DLS_sample_handle, false);
    }

    /* renamed from: a */
    public static void m21775a(C6282aju aju) {
        MilesBridgeJNI.AIL_close_library(C6282aju.m23101b(aju));
    }

    /* renamed from: v */
    public static C6282aju m21933v(String str, int i) {
        long AIL_open_library = MilesBridgeJNI.AIL_open_library(str, i);
        if (AIL_open_library == 0) {
            return null;
        }
        return new C6282aju(AIL_open_library, false);
    }

    /* renamed from: a */
    public static String m21734a(C6282aju aju, String str, String str2, int i) {
        return MilesBridgeJNI.AIL_library_resource_filename(C6282aju.m23101b(aju), str, str2, i);
    }

    public static int AIL_quick_startup(int i, int i2, long j, int i3, int i4) {
        return MilesBridgeJNI.AIL_quick_startup(i, i2, j, i3, i4);
    }

    public static void AIL_quick_shutdown() {
        MilesBridgeJNI.AIL_quick_shutdown();
    }

    /* renamed from: a */
    public static void m21738a(C0885Mq mq, C6683arf arf, C1512WF wf) {
        MilesBridgeJNI.AIL_quick_handles(C0885Mq.m7170a(mq), C6683arf.m25535a(arf), C1512WF.m11064a(wf));
    }

    /* renamed from: iE */
    public static aFA m21910iE(String str) {
        long AIL_quick_load = MilesBridgeJNI.AIL_quick_load(str);
        if (AIL_quick_load == 0) {
            return null;
        }
        return new aFA(AIL_quick_load, false);
    }

    /* renamed from: a */
    public static aFA m21712a(aBO abo, long j) {
        long AIL_quick_load_mem = MilesBridgeJNI.AIL_quick_load_mem(aBO.m12884g(abo), j);
        if (AIL_quick_load_mem == 0) {
            return null;
        }
        return new aFA(AIL_quick_load_mem, false);
    }

    /* renamed from: a */
    public static aFA m21713a(aBO abo, String str, long j) {
        long AIL_quick_load_named_mem = MilesBridgeJNI.AIL_quick_load_named_mem(aBO.m12884g(abo), str, j);
        if (AIL_quick_load_named_mem == 0) {
            return null;
        }
        return new aFA(AIL_quick_load_named_mem, false);
    }

    /* renamed from: a */
    public static aFA m21714a(aFA afa) {
        long AIL_quick_copy = MilesBridgeJNI.AIL_quick_copy(aFA.m14479h(afa));
        if (AIL_quick_copy == 0) {
            return null;
        }
        return new aFA(AIL_quick_copy, false);
    }

    /* renamed from: b */
    public static void m21822b(aFA afa) {
        MilesBridgeJNI.AIL_quick_unload(aFA.m14479h(afa));
    }

    /* renamed from: a */
    public static int m21686a(aFA afa, long j) {
        return MilesBridgeJNI.AIL_quick_play(aFA.m14479h(afa), j);
    }

    /* renamed from: c */
    public static void m21850c(aFA afa) {
        MilesBridgeJNI.AIL_quick_halt(aFA.m14479h(afa));
    }

    /* renamed from: d */
    public static int m21860d(aFA afa) {
        return MilesBridgeJNI.AIL_quick_status(aFA.m14479h(afa));
    }

    /* renamed from: a */
    public static aFA m21715a(String str, long j, int i) {
        long AIL_quick_load_and_play = MilesBridgeJNI.AIL_quick_load_and_play(str, j, i);
        if (AIL_quick_load_and_play == 0) {
            return null;
        }
        return new aFA(AIL_quick_load_and_play, false);
    }

    /* renamed from: a */
    public static void m21761a(aFA afa, int i) {
        MilesBridgeJNI.AIL_quick_set_speed(aFA.m14479h(afa), i);
    }

    /* renamed from: a */
    public static void m21760a(aFA afa, float f, float f2) {
        MilesBridgeJNI.AIL_quick_set_volume(aFA.m14479h(afa), f, f2);
    }

    /* renamed from: b */
    public static void m21823b(aFA afa, float f, float f2) {
        MilesBridgeJNI.AIL_quick_set_reverb_levels(aFA.m14479h(afa), f, f2);
    }

    /* renamed from: a */
    public static void m21759a(aFA afa, float f) {
        MilesBridgeJNI.AIL_quick_set_low_pass_cut_off(aFA.m14479h(afa), f);
    }

    /* renamed from: b */
    public static void m21824b(aFA afa, int i) {
        MilesBridgeJNI.AIL_quick_set_ms_position(aFA.m14479h(afa), i);
    }

    /* renamed from: e */
    public static int m21872e(aFA afa) {
        return MilesBridgeJNI.AIL_quick_ms_position(aFA.m14479h(afa));
    }

    /* renamed from: f */
    public static int m21882f(aFA afa) {
        return MilesBridgeJNI.AIL_quick_ms_length(aFA.m14479h(afa));
    }

    /* renamed from: g */
    public static int m21890g(aFA afa) {
        return MilesBridgeJNI.AIL_quick_type(aFA.m14479h(afa));
    }

    /* renamed from: a */
    public static void m21750a(C1467Vb vb, aCS acs) {
        MilesBridgeJNI.AIL_save_sample_attributes(C1467Vb.m10693c(vb), aCS.m13229a(acs));
    }

    /* renamed from: b */
    public static long m21806b(C1467Vb vb, aCS acs) {
        return MilesBridgeJNI.AIL_load_sample_attributes(C1467Vb.m10693c(vb), aCS.m13229a(acs));
    }

    /* renamed from: a */
    public static int m21703a(Buffer buffer, aHS ahs) {
        return MilesBridgeJNI.AIL_WAV_info(buffer, aHS.m15166b(ahs));
    }

    /* renamed from: a */
    public static int m21660a(long j, long j2, int i, C1491Vx vx) {
        return MilesBridgeJNI.AIL_size_processed_digital_audio(j, j2, i, C1491Vx.m10941a(vx));
    }

    /* renamed from: a */
    public static int m21676a(aBO abo, int i, long j, long j2, int i2, C1491Vx vx) {
        return MilesBridgeJNI.AIL_process_digital_audio(aBO.m12884g(abo), i, j, j2, i2, C1491Vx.m10941a(vx));
    }

    /* renamed from: a */
    public static int m21688a(aHS ahs, String str, C2224cz czVar, C5758aVq avq, C5551aNr anr) {
        return MilesBridgeJNI.AIL_compress_ASI(aHS.m15166b(ahs), str, C2224cz.m28711a(czVar), C5758aVq.m19143h(avq), C5551aNr.m16779a(anr));
    }

    /* renamed from: a */
    public static int m21680a(aBO abo, long j, String str, C2224cz czVar, C5758aVq avq, C5551aNr anr) {
        return MilesBridgeJNI.AIL_decompress_ASI(aBO.m12884g(abo), j, str, C2224cz.m28711a(czVar), C5758aVq.m19143h(avq), C5551aNr.m16779a(anr));
    }

    /* renamed from: a */
    public static int m21687a(aHS ahs, C2224cz czVar, C5758aVq avq) {
        return MilesBridgeJNI.AIL_compress_ADPCM(aHS.m15166b(ahs), C2224cz.m28711a(czVar), C5758aVq.m19143h(avq));
    }

    /* renamed from: b */
    public static int m21802b(aHS ahs, C2224cz czVar, C5758aVq avq) {
        return MilesBridgeJNI.AIL_decompress_ADPCM(aHS.m15166b(ahs), C2224cz.m28711a(czVar), C5758aVq.m19143h(avq));
    }

    /* renamed from: a */
    public static int m21685a(aBO abo, String str, C2224cz czVar, C5758aVq avq, C5551aNr anr) {
        return MilesBridgeJNI.AIL_compress_DLS(aBO.m12884g(abo), str, C2224cz.m28711a(czVar), C5758aVq.m19143h(avq), C5551aNr.m16779a(anr));
    }

    /* renamed from: a */
    public static int m21682a(aBO abo, aBO abo2, C2224cz czVar, C5758aVq avq) {
        return MilesBridgeJNI.AIL_merge_DLS_with_XMI(aBO.m12884g(abo), aBO.m12884g(abo2), C2224cz.m28711a(czVar), C5758aVq.m19143h(avq));
    }

    /* renamed from: a */
    public static int m21679a(aBO abo, long j, C2224cz czVar, C5758aVq avq, C2224cz czVar2, C5758aVq avq2, C5551aNr anr) {
        return MilesBridgeJNI.AIL_extract_DLS(aBO.m12884g(abo), j, C2224cz.m28711a(czVar), C5758aVq.m19143h(avq), C2224cz.m28711a(czVar2), C5758aVq.m19143h(avq2), C5551aNr.m16779a(anr));
    }

    /* renamed from: a */
    public static int m21683a(aBO abo, aBO abo2, C2224cz czVar, C5758aVq avq, int i, C5551aNr anr) {
        return MilesBridgeJNI.AIL_filter_DLS_with_XMI(aBO.m12884g(abo), aBO.m12884g(abo2), C2224cz.m28711a(czVar), C5758aVq.m19143h(avq), i, C5551aNr.m16779a(anr));
    }

    /* renamed from: a */
    public static int m21678a(aBO abo, long j, C2224cz czVar, C5758aVq avq, int i) {
        return MilesBridgeJNI.AIL_MIDI_to_XMI(aBO.m12884g(abo), j, C2224cz.m28711a(czVar), C5758aVq.m19143h(avq), i);
    }

    /* renamed from: a */
    public static int m21681a(aBO abo, C1490Vw vw, C5758aVq avq, int i, String str) {
        return MilesBridgeJNI.AIL_list_DLS(aBO.m12884g(abo), C1490Vw.m10940a(vw), C5758aVq.m19143h(avq), i, str);
    }

    /* renamed from: a */
    public static int m21677a(aBO abo, long j, C1490Vw vw, C5758aVq avq, int i) {
        return MilesBridgeJNI.AIL_list_MIDI(aBO.m12884g(abo), j, C1490Vw.m10940a(vw), C5758aVq.m19143h(avq), i);
    }

    public static int AIL_file_type(Buffer buffer, long j) {
        return MilesBridgeJNI.AIL_file_type(buffer, j);
    }

    public static int AIL_file_type_named(Buffer buffer, String str, long j) {
        return MilesBridgeJNI.AIL_file_type_named(buffer, str, j);
    }

    /* renamed from: a */
    public static int m21702a(Buffer buffer, long j, C2224cz czVar, C5758aVq avq, C2224cz czVar2, C5758aVq avq2) {
        return MilesBridgeJNI.AIL_find_DLS(buffer, j, C2224cz.m28711a(czVar), C5758aVq.m19143h(avq), C2224cz.m28711a(czVar2), C5758aVq.m19143h(avq2));
    }

    /* renamed from: a */
    public static void m21789a(C6867avH avh, C6626aqa aqa, int i) {
        MilesBridgeJNI.AIL_inspect_MP3(C6867avH.m26487b(avh), C6626aqa.m25149e(aqa), i);
    }

    /* renamed from: a */
    public static int m21695a(C6867avH avh) {
        return MilesBridgeJNI.AIL_enumerate_MP3_frames(C6867avH.m26487b(avh));
    }

    /* renamed from: k */
    public static aNZ m21916k(aNZ anz) {
        long AIL_primary_digital_driver = MilesBridgeJNI.AIL_primary_digital_driver(aNZ.m16621q(anz));
        if (AIL_primary_digital_driver == 0) {
            return null;
        }
        return new aNZ(AIL_primary_digital_driver, false);
    }

    /* renamed from: l */
    public static int m21919l(aNZ anz) {
        return MilesBridgeJNI.AIL_room_type(aNZ.m16621q(anz));
    }

    /* renamed from: a */
    public static void m21767a(aNZ anz, int i) {
        MilesBridgeJNI.AIL_set_room_type(aNZ.m16621q(anz), i);
    }

    /* renamed from: m */
    public static float m21920m(aNZ anz) {
        return MilesBridgeJNI.AIL_3D_rolloff_factor(aNZ.m16621q(anz));
    }

    /* renamed from: b */
    public static void m21825b(aNZ anz, float f) {
        MilesBridgeJNI.AIL_set_3D_rolloff_factor(aNZ.m16621q(anz), f);
    }

    /* renamed from: n */
    public static float m21922n(aNZ anz) {
        return MilesBridgeJNI.AIL_3D_doppler_factor(aNZ.m16621q(anz));
    }

    /* renamed from: c */
    public static void m21852c(aNZ anz, float f) {
        MilesBridgeJNI.AIL_set_3D_doppler_factor(aNZ.m16621q(anz), f);
    }

    /* renamed from: o */
    public static float m21924o(aNZ anz) {
        return MilesBridgeJNI.AIL_3D_distance_factor(aNZ.m16621q(anz));
    }

    /* renamed from: d */
    public static void m21866d(aNZ anz, float f) {
        MilesBridgeJNI.AIL_set_3D_distance_factor(aNZ.m16621q(anz), f);
    }

    /* renamed from: b */
    public static void m21812b(C1467Vb vb, float f) {
        MilesBridgeJNI.AIL_set_sample_obstruction(C1467Vb.m10693c(vb), f);
    }

    /* renamed from: c */
    public static void m21845c(C1467Vb vb, float f) {
        MilesBridgeJNI.AIL_set_sample_occlusion(C1467Vb.m10693c(vb), f);
    }

    /* renamed from: d */
    public static void m21864d(C1467Vb vb, float f) {
        MilesBridgeJNI.AIL_set_sample_exclusion(C1467Vb.m10693c(vb), f);
    }

    /* renamed from: r */
    public static float m21930r(C1467Vb vb) {
        return MilesBridgeJNI.AIL_sample_obstruction(C1467Vb.m10693c(vb));
    }

    /* renamed from: s */
    public static float m21931s(C1467Vb vb) {
        return MilesBridgeJNI.AIL_sample_occlusion(C1467Vb.m10693c(vb));
    }

    /* renamed from: t */
    public static float m21932t(C1467Vb vb) {
        return MilesBridgeJNI.AIL_sample_exclusion(C1467Vb.m10693c(vb));
    }

    /* renamed from: a */
    public static void m21745a(C1467Vb vb, float f, float f2, int i) {
        MilesBridgeJNI.AIL_set_sample_3D_distances(C1467Vb.m10693c(vb), f, f2, i);
    }

    /* renamed from: a */
    public static void m21753a(C1467Vb vb, C2512gC gCVar, C2512gC gCVar2, C2480fe feVar) {
        MilesBridgeJNI.AIL_sample_3D_distances(C1467Vb.m10693c(vb), C2512gC.m31656a(gCVar), C2512gC.m31656a(gCVar2), C2480fe.m31244a(feVar));
    }

    /* renamed from: a */
    public static void m21741a(C1467Vb vb, float f, float f2, float f3) {
        MilesBridgeJNI.AIL_set_sample_3D_cone(C1467Vb.m10693c(vb), f, f2, f3);
    }

    /* renamed from: a */
    public static void m21754a(C1467Vb vb, C2512gC gCVar, C2512gC gCVar2, C2512gC gCVar3) {
        MilesBridgeJNI.AIL_sample_3D_cone(C1467Vb.m10693c(vb), C2512gC.m31656a(gCVar), C2512gC.m31656a(gCVar2), C2512gC.m31656a(gCVar3));
    }

    /* renamed from: b */
    public static void m21814b(C1467Vb vb, float f, float f2, float f3) {
        MilesBridgeJNI.AIL_set_sample_3D_position(C1467Vb.m10693c(vb), f, f2, f3);
    }

    /* renamed from: a */
    public static void m21742a(C1467Vb vb, float f, float f2, float f3, float f4) {
        MilesBridgeJNI.AIL_set_sample_3D_velocity(C1467Vb.m10693c(vb), f, f2, f3, f4);
    }

    /* renamed from: c */
    public static void m21847c(C1467Vb vb, float f, float f2, float f3) {
        MilesBridgeJNI.AIL_set_sample_3D_velocity_vector(C1467Vb.m10693c(vb), f, f2, f3);
    }

    /* renamed from: b */
    public static void m21815b(C1467Vb vb, float f, float f2, float f3, float f4, float f5, float f6) {
        MilesBridgeJNI.AIL_set_sample_3D_orientation(C1467Vb.m10693c(vb), f, f2, f3, f4, f5, f6);
    }

    /* renamed from: b */
    public static int m21801b(C1467Vb vb, C2512gC gCVar, C2512gC gCVar2, C2512gC gCVar3) {
        return MilesBridgeJNI.AIL_sample_3D_position(C1467Vb.m10693c(vb), C2512gC.m31656a(gCVar), C2512gC.m31656a(gCVar2), C2512gC.m31656a(gCVar3));
    }

    /* renamed from: c */
    public static void m21848c(C1467Vb vb, C2512gC gCVar, C2512gC gCVar2, C2512gC gCVar3) {
        MilesBridgeJNI.AIL_sample_3D_velocity(C1467Vb.m10693c(vb), C2512gC.m31656a(gCVar), C2512gC.m31656a(gCVar2), C2512gC.m31656a(gCVar3));
    }

    /* renamed from: b */
    public static void m21820b(C1467Vb vb, C2512gC gCVar, C2512gC gCVar2, C2512gC gCVar3, C2512gC gCVar4, C2512gC gCVar5, C2512gC gCVar6) {
        MilesBridgeJNI.AIL_sample_3D_orientation(C1467Vb.m10693c(vb), C2512gC.m31656a(gCVar), C2512gC.m31656a(gCVar2), C2512gC.m31656a(gCVar3), C2512gC.m31656a(gCVar4), C2512gC.m31656a(gCVar5), C2512gC.m31656a(gCVar6));
    }

    /* renamed from: e */
    public static void m21878e(C1467Vb vb, float f) {
        MilesBridgeJNI.AIL_update_sample_3D_position(C1467Vb.m10693c(vb), f);
    }

    /* renamed from: b */
    public static void m21826b(aNZ anz, float f, float f2, float f3) {
        MilesBridgeJNI.AIL_set_listener_3D_position(aNZ.m16621q(anz), f, f2, f3);
    }

    /* renamed from: a */
    public static void m21765a(aNZ anz, float f, float f2, float f3, float f4) {
        MilesBridgeJNI.AIL_set_listener_3D_velocity(aNZ.m16621q(anz), f, f2, f3, f4);
    }

    /* renamed from: c */
    public static void m21853c(aNZ anz, float f, float f2, float f3) {
        MilesBridgeJNI.AIL_set_listener_3D_velocity_vector(aNZ.m16621q(anz), f, f2, f3);
    }

    /* renamed from: a */
    public static void m21766a(aNZ anz, float f, float f2, float f3, float f4, float f5, float f6) {
        MilesBridgeJNI.AIL_set_listener_3D_orientation(aNZ.m16621q(anz), f, f2, f3, f4, f5, f6);
    }

    /* renamed from: b */
    public static void m21827b(aNZ anz, C2512gC gCVar, C2512gC gCVar2, C2512gC gCVar3) {
        MilesBridgeJNI.AIL_listener_3D_position(aNZ.m16621q(anz), C2512gC.m31656a(gCVar), C2512gC.m31656a(gCVar2), C2512gC.m31656a(gCVar3));
    }

    /* renamed from: c */
    public static void m21854c(aNZ anz, C2512gC gCVar, C2512gC gCVar2, C2512gC gCVar3) {
        MilesBridgeJNI.AIL_listener_3D_velocity(aNZ.m16621q(anz), C2512gC.m31656a(gCVar), C2512gC.m31656a(gCVar2), C2512gC.m31656a(gCVar3));
    }

    /* renamed from: a */
    public static void m21774a(aNZ anz, C2512gC gCVar, C2512gC gCVar2, C2512gC gCVar3, C2512gC gCVar4, C2512gC gCVar5, C2512gC gCVar6) {
        MilesBridgeJNI.AIL_listener_3D_orientation(aNZ.m16621q(anz), C2512gC.m31656a(gCVar), C2512gC.m31656a(gCVar2), C2512gC.m31656a(gCVar3), C2512gC.m31656a(gCVar4), C2512gC.m31656a(gCVar5), C2512gC.m31656a(gCVar6));
    }

    /* renamed from: e */
    public static void m21879e(aNZ anz, float f) {
        MilesBridgeJNI.AIL_update_listener_3D_position(aNZ.m16621q(anz), f);
    }

    /* renamed from: p */
    public static long m21927p(aNZ anz) {
        return MilesBridgeJNI.AIL_digital_output_filter(aNZ.m16621q(anz));
    }

    /* renamed from: a */
    public static int m21692a(C5758aVq avq, C5758aVq avq2, C1490Vw vw) {
        return MilesBridgeJNI.AIL_enumerate_filters(C5758aVq.m19143h(avq), C5758aVq.m19143h(avq2), C1490Vw.m10940a(vw));
    }

    /* renamed from: a */
    public static int m21661a(long j, aNZ anz) {
        return MilesBridgeJNI.AIL_open_filter(j, aNZ.m16621q(anz));
    }

    public static void AIL_close_filter(int i) {
        MilesBridgeJNI.AIL_close_filter(i);
    }

    public static int AIL_find_filter(String str, Buffer buffer) {
        return MilesBridgeJNI.AIL_find_filter(str, buffer);
    }

    /* renamed from: a */
    public static int m21662a(long j, C5758aVq avq, C5373aGv agv) {
        return MilesBridgeJNI.AIL_enumerate_filter_properties(j, C5758aVq.m19143h(avq), C5373aGv.m15023a(agv));
    }

    public static int AIL_filter_property(long j, String str, Buffer buffer, Buffer buffer2, Buffer buffer3) {
        return MilesBridgeJNI.AIL_filter_property(j, str, buffer, buffer2, buffer3);
    }

    /* renamed from: b */
    public static int m21798b(long j, C5758aVq avq, C5373aGv agv) {
        return MilesBridgeJNI.AIL_enumerate_output_filter_driver_properties(j, C5758aVq.m19143h(avq), C5373aGv.m15023a(agv));
    }

    /* renamed from: a */
    public static int m21691a(aNZ anz, String str, Buffer buffer, Buffer buffer2, Buffer buffer3) {
        return MilesBridgeJNI.AIL_output_filter_driver_property(aNZ.m16621q(anz), str, buffer, buffer2, buffer3);
    }

    /* renamed from: c */
    public static int m21836c(long j, C5758aVq avq, C5373aGv agv) {
        return MilesBridgeJNI.AIL_enumerate_output_filter_sample_properties(j, C5758aVq.m19143h(avq), C5373aGv.m15023a(agv));
    }

    /* renamed from: d */
    public static int m21859d(long j, C5758aVq avq, C5373aGv agv) {
        return MilesBridgeJNI.AIL_enumerate_filter_sample_properties(j, C5758aVq.m19143h(avq), C5373aGv.m15023a(agv));
    }

    /* renamed from: a */
    public static int m21669a(C1467Vb vb, C5297aDx adx, C5758aVq avq, C5373aGv agv) {
        return MilesBridgeJNI.AIL_enumerate_sample_stage_properties(C1467Vb.m10693c(vb), adx.swigValue(), C5758aVq.m19143h(avq), C5373aGv.m15023a(agv));
    }

    /* renamed from: a */
    public static int m21670a(C1467Vb vb, C5297aDx adx, String str, Buffer buffer, Buffer buffer2, Buffer buffer3) {
        return MilesBridgeJNI.AIL_sample_stage_property(C1467Vb.m10693c(vb), adx.swigValue(), str, buffer, buffer2, buffer3);
    }
}
