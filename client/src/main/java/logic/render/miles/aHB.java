package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aHB */
/* compiled from: a */
public final class aHB {
    public static final aHB hXT = new aHB("RIB_NONE", MilesBridgeJNI.RIB_NONE_get());
    public static final aHB hXU = new aHB("RIB_CUSTOM");
    public static final aHB hXV = new aHB("RIB_DEC");
    public static final aHB hXW = new aHB("RIB_HEX");
    public static final aHB hXX = new aHB("RIB_FLOAT");
    public static final aHB hXY = new aHB("RIB_PERCENT");
    public static final aHB hXZ = new aHB("RIB_BOOL");
    public static final aHB hYa = new aHB("RIB_STRING");
    public static final aHB hYb = new aHB("RIB_READONLY", MilesBridgeJNI.RIB_READONLY_get());
    private static aHB[] hYc = {hXT, hXU, hXV, hXW, hXX, hXY, hXZ, hYa, hYb};

    /* renamed from: pF */
    private static int f2910pF = 0;

    /* renamed from: pG */
    private final int f2911pG;

    /* renamed from: pH */
    private final String f2912pH;

    private aHB(String str) {
        this.f2912pH = str;
        int i = f2910pF;
        f2910pF = i + 1;
        this.f2911pG = i;
    }

    private aHB(String str, int i) {
        this.f2912pH = str;
        this.f2911pG = i;
        f2910pF = i + 1;
    }

    private aHB(String str, aHB ahb) {
        this.f2912pH = str;
        this.f2911pG = ahb.f2911pG;
        f2910pF = this.f2911pG + 1;
    }

    /* renamed from: yk */
    public static aHB m15117yk(int i) {
        if (i < hYc.length && i >= 0 && hYc[i].f2911pG == i) {
            return hYc[i];
        }
        for (int i2 = 0; i2 < hYc.length; i2++) {
            if (hYc[i2].f2911pG == i) {
                return hYc[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + aHB.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f2911pG;
    }

    public String toString() {
        return this.f2912pH;
    }
}
