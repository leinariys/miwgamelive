package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

import java.nio.Buffer;

/* renamed from: a.ayW */
/* compiled from: a */
public class ayW {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public ayW(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public ayW() {
        this(MilesBridgeJNI.new_SMPBUF(), true);
    }

    /* renamed from: b */
    public static long m27440b(ayW ayw) {
        if (ayw == null) {
            return 0;
        }
        return ayw.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_SMPBUF(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo16998b(Buffer buffer) {
        MilesBridgeJNI.SMPBUF_start_set(this.swigCPtr, buffer);
    }

    public Buffer cEE() {
        return MilesBridgeJNI.SMPBUF_start_get(this.swigCPtr);
    }

    /* renamed from: jk */
    public void mo17007jk(long j) {
        MilesBridgeJNI.SMPBUF_len_set(this.swigCPtr, j);
    }

    public long cEF() {
        return MilesBridgeJNI.SMPBUF_len_get(this.swigCPtr);
    }

    /* renamed from: jl */
    public void mo17008jl(long j) {
        MilesBridgeJNI.SMPBUF_pos_set(this.swigCPtr, j);
    }

    public long cEG() {
        return MilesBridgeJNI.SMPBUF_pos_get(this.swigCPtr);
    }

    public long getDone() {
        return MilesBridgeJNI.SMPBUF_done_get(this.swigCPtr);
    }

    public void setDone(long j) {
        MilesBridgeJNI.SMPBUF_done_set(this.swigCPtr, j);
    }

    /* renamed from: wq */
    public void mo17010wq(int i) {
        MilesBridgeJNI.SMPBUF_reset_ASI_set(this.swigCPtr, i);
    }

    public int cEH() {
        return MilesBridgeJNI.SMPBUF_reset_ASI_get(this.swigCPtr);
    }

    /* renamed from: wr */
    public void mo17011wr(int i) {
        MilesBridgeJNI.SMPBUF_reset_seek_pos_set(this.swigCPtr, i);
    }

    public int cEI() {
        return MilesBridgeJNI.SMPBUF_reset_seek_pos_get(this.swigCPtr);
    }
}
