package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.iI */
/* compiled from: a */
public final class C2666iI {

    /* renamed from: YR */
    public static final C2666iI f8135YR = new C2666iI("MSS_SPEAKER_FRONT_LEFT", MilesBridgeJNI.MSS_SPEAKER_FRONT_LEFT_get());

    /* renamed from: YS */
    public static final C2666iI f8136YS = new C2666iI("MSS_SPEAKER_FRONT_RIGHT", MilesBridgeJNI.MSS_SPEAKER_FRONT_RIGHT_get());

    /* renamed from: YT */
    public static final C2666iI f8137YT = new C2666iI("MSS_SPEAKER_FRONT_CENTER", MilesBridgeJNI.MSS_SPEAKER_FRONT_CENTER_get());

    /* renamed from: YU */
    public static final C2666iI f8138YU = new C2666iI("MSS_SPEAKER_LOW_FREQUENCY", MilesBridgeJNI.MSS_SPEAKER_LOW_FREQUENCY_get());

    /* renamed from: YV */
    public static final C2666iI f8139YV = new C2666iI("MSS_SPEAKER_BACK_LEFT", MilesBridgeJNI.MSS_SPEAKER_BACK_LEFT_get());

    /* renamed from: YW */
    public static final C2666iI f8140YW = new C2666iI("MSS_SPEAKER_BACK_RIGHT", MilesBridgeJNI.MSS_SPEAKER_BACK_RIGHT_get());

    /* renamed from: YX */
    public static final C2666iI f8141YX = new C2666iI("MSS_SPEAKER_FRONT_LEFT_OF_CENTER", MilesBridgeJNI.MSS_SPEAKER_FRONT_LEFT_OF_CENTER_get());

    /* renamed from: YY */
    public static final C2666iI f8142YY = new C2666iI("MSS_SPEAKER_FRONT_RIGHT_OF_CENTER", MilesBridgeJNI.MSS_SPEAKER_FRONT_RIGHT_OF_CENTER_get());

    /* renamed from: YZ */
    public static final C2666iI f8143YZ = new C2666iI("MSS_SPEAKER_BACK_CENTER", MilesBridgeJNI.MSS_SPEAKER_BACK_CENTER_get());

    /* renamed from: Za */
    public static final C2666iI f8144Za = new C2666iI("MSS_SPEAKER_SIDE_LEFT", MilesBridgeJNI.MSS_SPEAKER_SIDE_LEFT_get());

    /* renamed from: Zb */
    public static final C2666iI f8145Zb = new C2666iI("MSS_SPEAKER_SIDE_RIGHT", MilesBridgeJNI.MSS_SPEAKER_SIDE_RIGHT_get());

    /* renamed from: Zc */
    public static final C2666iI f8146Zc = new C2666iI("MSS_SPEAKER_TOP_CENTER", MilesBridgeJNI.MSS_SPEAKER_TOP_CENTER_get());

    /* renamed from: Zd */
    public static final C2666iI f8147Zd = new C2666iI("MSS_SPEAKER_TOP_FRONT_LEFT", MilesBridgeJNI.MSS_SPEAKER_TOP_FRONT_LEFT_get());

    /* renamed from: Ze */
    public static final C2666iI f8148Ze = new C2666iI("MSS_SPEAKER_TOP_FRONT_CENTER", MilesBridgeJNI.MSS_SPEAKER_TOP_FRONT_CENTER_get());

    /* renamed from: Zf */
    public static final C2666iI f8149Zf = new C2666iI("MSS_SPEAKER_TOP_FRONT_RIGHT", MilesBridgeJNI.MSS_SPEAKER_TOP_FRONT_RIGHT_get());

    /* renamed from: Zg */
    public static final C2666iI f8150Zg = new C2666iI("MSS_SPEAKER_TOP_BACK_LEFT", MilesBridgeJNI.MSS_SPEAKER_TOP_BACK_LEFT_get());

    /* renamed from: Zh */
    public static final C2666iI f8151Zh = new C2666iI("MSS_SPEAKER_TOP_BACK_CENTER", MilesBridgeJNI.MSS_SPEAKER_TOP_BACK_CENTER_get());

    /* renamed from: Zi */
    public static final C2666iI f8152Zi = new C2666iI("MSS_SPEAKER_TOP_BACK_RIGHT", MilesBridgeJNI.MSS_SPEAKER_TOP_BACK_RIGHT_get());

    /* renamed from: Zj */
    public static final C2666iI f8153Zj = new C2666iI("MSS_SPEAKER_MAX_INDEX", MilesBridgeJNI.MSS_SPEAKER_MAX_INDEX_get());

    /* renamed from: Zk */
    private static C2666iI[] f8154Zk = {f8135YR, f8136YS, f8137YT, f8138YU, f8139YV, f8140YW, f8141YX, f8142YY, f8143YZ, f8144Za, f8145Zb, f8146Zc, f8147Zd, f8148Ze, f8149Zf, f8150Zg, f8151Zh, f8152Zi, f8153Zj};

    /* renamed from: pF */
    private static int f8155pF = 0;

    /* renamed from: pG */
    private final int f8156pG;

    /* renamed from: pH */
    private final String f8157pH;

    private C2666iI(String str) {
        this.f8157pH = str;
        int i = f8155pF;
        f8155pF = i + 1;
        this.f8156pG = i;
    }

    private C2666iI(String str, int i) {
        this.f8157pH = str;
        this.f8156pG = i;
        f8155pF = i + 1;
    }

    private C2666iI(String str, C2666iI iIVar) {
        this.f8157pH = str;
        this.f8156pG = iIVar.f8156pG;
        f8155pF = this.f8156pG + 1;
    }

    /* renamed from: bS */
    public static C2666iI m33097bS(int i) {
        if (i < f8154Zk.length && i >= 0 && f8154Zk[i].f8156pG == i) {
            return f8154Zk[i];
        }
        for (int i2 = 0; i2 < f8154Zk.length; i2++) {
            if (f8154Zk[i2].f8156pG == i) {
                return f8154Zk[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C2666iI.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f8156pG;
    }

    public String toString() {
        return this.f8157pH;
    }
}
