package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.adw  reason: case insensitive filesystem */
/* compiled from: a */
public class C5972adw {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5972adw(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5972adw() {
        this(MilesBridgeJNI.new_MWAVEFORMATEXTENSIBLE(), true);
    }

    /* renamed from: a */
    public static long m21074a(C5972adw adw) {
        if (adw == null) {
            return 0;
        }
        return adw.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_MWAVEFORMATEXTENSIBLE(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo12957a(aMW amw) {
        MilesBridgeJNI.MWAVEFORMATEXTENSIBLE_Format_set(this.swigCPtr, aMW.m16390b(amw));
    }

    public aMW bRk() {
        long MWAVEFORMATEXTENSIBLE_Format_get = MilesBridgeJNI.MWAVEFORMATEXTENSIBLE_Format_get(this.swigCPtr);
        if (MWAVEFORMATEXTENSIBLE_Format_get == 0) {
            return null;
        }
        return new aMW(MWAVEFORMATEXTENSIBLE_Format_get, false);
    }

    /* renamed from: gN */
    public void mo12965gN(long j) {
        MilesBridgeJNI.MWAVEFORMATEXTENSIBLE_dwChannelMask_set(this.swigCPtr, j);
    }

    public long bRl() {
        return MilesBridgeJNI.MWAVEFORMATEXTENSIBLE_dwChannelMask_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo12958a(C6626aqa aqa) {
        MilesBridgeJNI.MWAVEFORMATEXTENSIBLE_SubFormat_set(this.swigCPtr, C6626aqa.m25149e(aqa));
    }

    public C6626aqa bRm() {
        long MWAVEFORMATEXTENSIBLE_SubFormat_get = MilesBridgeJNI.MWAVEFORMATEXTENSIBLE_SubFormat_get(this.swigCPtr);
        if (MWAVEFORMATEXTENSIBLE_SubFormat_get == 0) {
            return null;
        }
        return new C6626aqa(MWAVEFORMATEXTENSIBLE_SubFormat_get, false);
    }

    public C5504aLw bRn() {
        long MWAVEFORMATEXTENSIBLE_Samples_get = MilesBridgeJNI.MWAVEFORMATEXTENSIBLE_Samples_get(this.swigCPtr);
        if (MWAVEFORMATEXTENSIBLE_Samples_get == 0) {
            return null;
        }
        return new C5504aLw(MWAVEFORMATEXTENSIBLE_Samples_get, false);
    }
}
