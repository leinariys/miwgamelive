package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aQi  reason: case insensitive filesystem */
/* compiled from: a */
public class C5620aQi {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5620aQi(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5620aQi() {
        this(MilesBridgeJNI.new_EAX_AUTOWAH(), true);
    }

    /* renamed from: a */
    public static long m17625a(C5620aQi aqi) {
        if (aqi == null) {
            return 0;
        }
        return aqi.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_EAX_AUTOWAH(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: aD */
    public void mo11051aD(int i) {
        MilesBridgeJNI.EAX_AUTOWAH_Effect_set(this.swigCPtr, i);
    }

    /* renamed from: ma */
    public int mo11059ma() {
        return MilesBridgeJNI.EAX_AUTOWAH_Effect_get(this.swigCPtr);
    }

    public int getVolume() {
        return MilesBridgeJNI.EAX_AUTOWAH_Volume_get(this.swigCPtr);
    }

    public void setVolume(int i) {
        MilesBridgeJNI.EAX_AUTOWAH_Volume_set(this.swigCPtr, i);
    }

    /* renamed from: nR */
    public void mo11060nR(float f) {
        MilesBridgeJNI.EAX_AUTOWAH_AttackTime_set(this.swigCPtr, f);
    }

    public float dpF() {
        return MilesBridgeJNI.EAX_AUTOWAH_AttackTime_get(this.swigCPtr);
    }

    /* renamed from: nS */
    public void mo11061nS(float f) {
        MilesBridgeJNI.EAX_AUTOWAH_ReleaseTime_set(this.swigCPtr, f);
    }

    public float dpG() {
        return MilesBridgeJNI.EAX_AUTOWAH_ReleaseTime_get(this.swigCPtr);
    }

    /* renamed from: Aj */
    public void mo11049Aj(int i) {
        MilesBridgeJNI.EAX_AUTOWAH_Resonance_set(this.swigCPtr, i);
    }

    public int dpH() {
        return MilesBridgeJNI.EAX_AUTOWAH_Resonance_get(this.swigCPtr);
    }

    /* renamed from: Ak */
    public void mo11050Ak(int i) {
        MilesBridgeJNI.EAX_AUTOWAH_PeakLevel_set(this.swigCPtr, i);
    }

    public int dpI() {
        return MilesBridgeJNI.EAX_AUTOWAH_PeakLevel_get(this.swigCPtr);
    }
}
