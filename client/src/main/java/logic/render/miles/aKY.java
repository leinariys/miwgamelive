package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aKY */
/* compiled from: a */
public class aKY {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aKY(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aKY() {
        this(MilesBridgeJNI.new_LOWPASS_CONSTANT_INFO(), true);
    }

    /* renamed from: b */
    public static long m16009b(aKY aky) {
        if (aky == null) {
            return 0;
        }
        return aky.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_LOWPASS_CONSTANT_INFO(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: yx */
    public void mo9744yx(int i) {
        MilesBridgeJNI.LOWPASS_CONSTANT_INFO_A_set(this.swigCPtr, i);
    }

    public int dgO() {
        return MilesBridgeJNI.LOWPASS_CONSTANT_INFO_A_get(this.swigCPtr);
    }

    /* renamed from: yy */
    public void mo9745yy(int i) {
        MilesBridgeJNI.LOWPASS_CONSTANT_INFO_B0_set(this.swigCPtr, i);
    }

    public int dgP() {
        return MilesBridgeJNI.LOWPASS_CONSTANT_INFO_B0_get(this.swigCPtr);
    }

    /* renamed from: yz */
    public void mo9746yz(int i) {
        MilesBridgeJNI.LOWPASS_CONSTANT_INFO_B1_set(this.swigCPtr, i);
    }

    public int dgQ() {
        return MilesBridgeJNI.LOWPASS_CONSTANT_INFO_B1_get(this.swigCPtr);
    }
}
