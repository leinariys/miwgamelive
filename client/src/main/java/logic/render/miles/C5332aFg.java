package logic.render.miles;

/* renamed from: a.aFg  reason: case insensitive filesystem */
/* compiled from: a */
public class C5332aFg {
    private long swigCPtr;

    public C5332aFg(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5332aFg() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m14623b(C5332aFg afg) {
        if (afg == null) {
            return 0;
        }
        return afg.swigCPtr;
    }
}
