package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.ic */
/* compiled from: a */
public class C2689ic {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2689ic(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C2689ic() {
        this(MilesBridgeJNI.new_MSTREAM_TYPE(), true);
    }

    /* renamed from: a */
    public static long m33438a(C2689ic icVar) {
        if (icVar == null) {
            return 0;
        }
        return icVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_MSTREAM_TYPE(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: bs */
    public void mo19735bs(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_block_oriented_set(this.swigCPtr, i);
    }

    /* renamed from: Ah */
    public int mo19679Ah() {
        return MilesBridgeJNI.MSTREAM_TYPE_block_oriented_get(this.swigCPtr);
    }

    /* renamed from: bt */
    public void mo19737bt(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_using_ASI_set(this.swigCPtr, i);
    }

    /* renamed from: Ai */
    public int mo19680Ai() {
        return MilesBridgeJNI.MSTREAM_TYPE_using_ASI_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo19703a(C0270DT dt) {
        MilesBridgeJNI.MSTREAM_TYPE_ASI_set(this.swigCPtr, C0270DT.m2424b(dt));
    }

    /* renamed from: Aj */
    public C0270DT mo19681Aj() {
        long MSTREAM_TYPE_ASI_get = MilesBridgeJNI.MSTREAM_TYPE_ASI_get(this.swigCPtr);
        if (MSTREAM_TYPE_ASI_get == 0) {
            return null;
        }
        return new C0270DT(MSTREAM_TYPE_ASI_get, false);
    }

    /* renamed from: a */
    public void mo19704a(C1467Vb vb) {
        MilesBridgeJNI.MSTREAM_TYPE_samp_set(this.swigCPtr, C1467Vb.m10693c(vb));
    }

    /* renamed from: Ak */
    public C1467Vb mo19682Ak() {
        long MSTREAM_TYPE_samp_get = MilesBridgeJNI.MSTREAM_TYPE_samp_get(this.swigCPtr);
        if (MSTREAM_TYPE_samp_get == 0) {
            return null;
        }
        return new C1467Vb(MSTREAM_TYPE_samp_get, false);
    }

    /* renamed from: bk */
    public void mo19727bk(long j) {
        MilesBridgeJNI.MSTREAM_TYPE_fileh_set(this.swigCPtr, j);
    }

    /* renamed from: Al */
    public long mo19683Al() {
        return MilesBridgeJNI.MSTREAM_TYPE_fileh_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo19706a(aUW auw) {
        MilesBridgeJNI.MSTREAM_TYPE_bufs_set(this.swigCPtr, aUW.m18601b(auw));
    }

    /* renamed from: Am */
    public aUW mo19684Am() {
        long MSTREAM_TYPE_bufs_get = MilesBridgeJNI.MSTREAM_TYPE_bufs_get(this.swigCPtr);
        if (MSTREAM_TYPE_bufs_get == 0) {
            return null;
        }
        return new aUW(MSTREAM_TYPE_bufs_get, false);
    }

    /* renamed from: a */
    public void mo19708a(C5758aVq avq) {
        MilesBridgeJNI.MSTREAM_TYPE_bufsizes_set(this.swigCPtr, C5758aVq.m19143h(avq));
    }

    /* renamed from: An */
    public C5758aVq mo19685An() {
        long MSTREAM_TYPE_bufsizes_get = MilesBridgeJNI.MSTREAM_TYPE_bufsizes_get(this.swigCPtr);
        if (MSTREAM_TYPE_bufsizes_get == 0) {
            return null;
        }
        return new C5758aVq(MSTREAM_TYPE_bufsizes_get, false);
    }

    /* renamed from: b */
    public void mo19710b(C2480fe feVar) {
        MilesBridgeJNI.MSTREAM_TYPE_reset_ASI_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    /* renamed from: Ao */
    public C2480fe mo19686Ao() {
        long MSTREAM_TYPE_reset_ASI_get = MilesBridgeJNI.MSTREAM_TYPE_reset_ASI_get(this.swigCPtr);
        if (MSTREAM_TYPE_reset_ASI_get == 0) {
            return null;
        }
        return new C2480fe(MSTREAM_TYPE_reset_ASI_get, false);
    }

    /* renamed from: c */
    public void mo19748c(C2480fe feVar) {
        MilesBridgeJNI.MSTREAM_TYPE_reset_seek_pos_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    /* renamed from: Ap */
    public C2480fe mo19687Ap() {
        long MSTREAM_TYPE_reset_seek_pos_get = MilesBridgeJNI.MSTREAM_TYPE_reset_seek_pos_get(this.swigCPtr);
        if (MSTREAM_TYPE_reset_seek_pos_get == 0) {
            return null;
        }
        return new C2480fe(MSTREAM_TYPE_reset_seek_pos_get, false);
    }

    /* renamed from: d */
    public void mo19749d(C2480fe feVar) {
        MilesBridgeJNI.MSTREAM_TYPE_bufstart_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    /* renamed from: Aq */
    public C2480fe mo19688Aq() {
        long MSTREAM_TYPE_bufstart_get = MilesBridgeJNI.MSTREAM_TYPE_bufstart_get(this.swigCPtr);
        if (MSTREAM_TYPE_bufstart_get == 0) {
            return null;
        }
        return new C2480fe(MSTREAM_TYPE_bufstart_get, false);
    }

    /* renamed from: b */
    public void mo19709b(C2224cz czVar) {
        MilesBridgeJNI.MSTREAM_TYPE_asyncs_set(this.swigCPtr, C2224cz.m28711a(czVar));
    }

    /* renamed from: Ar */
    public C2224cz mo19689Ar() {
        long MSTREAM_TYPE_asyncs_get = MilesBridgeJNI.MSTREAM_TYPE_asyncs_get(this.swigCPtr);
        if (MSTREAM_TYPE_asyncs_get == 0) {
            return null;
        }
        return new C2224cz(MSTREAM_TYPE_asyncs_get, false);
    }

    /* renamed from: e */
    public void mo19751e(C2480fe feVar) {
        MilesBridgeJNI.MSTREAM_TYPE_loadedbufstart_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    /* renamed from: As */
    public C2480fe mo19690As() {
        long MSTREAM_TYPE_loadedbufstart_get = MilesBridgeJNI.MSTREAM_TYPE_loadedbufstart_get(this.swigCPtr);
        if (MSTREAM_TYPE_loadedbufstart_get == 0) {
            return null;
        }
        return new C2480fe(MSTREAM_TYPE_loadedbufstart_get, false);
    }

    /* renamed from: f */
    public void mo19752f(C2480fe feVar) {
        MilesBridgeJNI.MSTREAM_TYPE_loadedorder_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    /* renamed from: At */
    public C2480fe mo19691At() {
        long MSTREAM_TYPE_loadedorder_get = MilesBridgeJNI.MSTREAM_TYPE_loadedorder_get(this.swigCPtr);
        if (MSTREAM_TYPE_loadedorder_get == 0) {
            return null;
        }
        return new C2480fe(MSTREAM_TYPE_loadedorder_get, false);
    }

    /* renamed from: bu */
    public void mo19739bu(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_loadorder_set(this.swigCPtr, i);
    }

    /* renamed from: Au */
    public int mo19692Au() {
        return MilesBridgeJNI.MSTREAM_TYPE_loadorder_get(this.swigCPtr);
    }

    /* renamed from: bv */
    public void mo19741bv(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_bufsize_set(this.swigCPtr, i);
    }

    /* renamed from: Av */
    public int mo19693Av() {
        return MilesBridgeJNI.MSTREAM_TYPE_bufsize_get(this.swigCPtr);
    }

    /* renamed from: bw */
    public void mo19743bw(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_readsize_set(this.swigCPtr, i);
    }

    /* renamed from: Aw */
    public int mo19694Aw() {
        return MilesBridgeJNI.MSTREAM_TYPE_readsize_get(this.swigCPtr);
    }

    /* renamed from: bl */
    public void mo19728bl(long j) {
        MilesBridgeJNI.MSTREAM_TYPE_buf1_set(this.swigCPtr, j);
    }

    /* renamed from: Ax */
    public long mo19695Ax() {
        return MilesBridgeJNI.MSTREAM_TYPE_buf1_get(this.swigCPtr);
    }

    /* renamed from: bx */
    public void mo19745bx(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_size1_set(this.swigCPtr, i);
    }

    /* renamed from: Ay */
    public int mo19696Ay() {
        return MilesBridgeJNI.MSTREAM_TYPE_size1_get(this.swigCPtr);
    }

    /* renamed from: bm */
    public void mo19729bm(long j) {
        MilesBridgeJNI.MSTREAM_TYPE_buf2_set(this.swigCPtr, j);
    }

    /* renamed from: Az */
    public long mo19697Az() {
        return MilesBridgeJNI.MSTREAM_TYPE_buf2_get(this.swigCPtr);
    }

    /* renamed from: by */
    public void mo19746by(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_size2_set(this.swigCPtr, i);
    }

    /* renamed from: AA */
    public int mo19653AA() {
        return MilesBridgeJNI.MSTREAM_TYPE_size2_get(this.swigCPtr);
    }

    /* renamed from: bn */
    public void mo19730bn(long j) {
        MilesBridgeJNI.MSTREAM_TYPE_buf3_set(this.swigCPtr, j);
    }

    /* renamed from: AB */
    public long mo19654AB() {
        return MilesBridgeJNI.MSTREAM_TYPE_buf3_get(this.swigCPtr);
    }

    /* renamed from: bz */
    public void mo19747bz(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_size3_set(this.swigCPtr, i);
    }

    /* renamed from: AC */
    public int mo19655AC() {
        return MilesBridgeJNI.MSTREAM_TYPE_size3_get(this.swigCPtr);
    }

    /* renamed from: bo */
    public void mo19731bo(long j) {
        MilesBridgeJNI.MSTREAM_TYPE_datarate_set(this.swigCPtr, j);
    }

    /* renamed from: AD */
    public long mo19656AD() {
        return MilesBridgeJNI.MSTREAM_TYPE_datarate_get(this.swigCPtr);
    }

    /* renamed from: bA */
    public void mo19711bA(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_filerate_set(this.swigCPtr, i);
    }

    /* renamed from: AE */
    public int mo19657AE() {
        return MilesBridgeJNI.MSTREAM_TYPE_filerate_get(this.swigCPtr);
    }

    /* renamed from: bB */
    public void mo19712bB(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_filetype_set(this.swigCPtr, i);
    }

    /* renamed from: AF */
    public int mo19658AF() {
        return MilesBridgeJNI.MSTREAM_TYPE_filetype_get(this.swigCPtr);
    }

    /* renamed from: bp */
    public void mo19732bp(long j) {
        MilesBridgeJNI.MSTREAM_TYPE_filemask_set(this.swigCPtr, j);
    }

    /* renamed from: AG */
    public long mo19659AG() {
        return MilesBridgeJNI.MSTREAM_TYPE_filemask_get(this.swigCPtr);
    }

    /* renamed from: bq */
    public void mo19733bq(long j) {
        MilesBridgeJNI.MSTREAM_TYPE_fileflags_set(this.swigCPtr, j);
    }

    /* renamed from: AH */
    public long mo19660AH() {
        return MilesBridgeJNI.MSTREAM_TYPE_fileflags_get(this.swigCPtr);
    }

    /* renamed from: bC */
    public void mo19713bC(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_totallen_set(this.swigCPtr, i);
    }

    /* renamed from: AI */
    public int mo19661AI() {
        return MilesBridgeJNI.MSTREAM_TYPE_totallen_get(this.swigCPtr);
    }

    /* renamed from: bD */
    public void mo19714bD(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_substart_set(this.swigCPtr, i);
    }

    /* renamed from: AJ */
    public int mo19662AJ() {
        return MilesBridgeJNI.MSTREAM_TYPE_substart_get(this.swigCPtr);
    }

    /* renamed from: bE */
    public void mo19715bE(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_sublen_set(this.swigCPtr, i);
    }

    /* renamed from: AK */
    public int mo19663AK() {
        return MilesBridgeJNI.MSTREAM_TYPE_sublen_get(this.swigCPtr);
    }

    /* renamed from: bF */
    public void mo19716bF(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_subpadding_set(this.swigCPtr, i);
    }

    /* renamed from: AL */
    public int mo19664AL() {
        return MilesBridgeJNI.MSTREAM_TYPE_subpadding_get(this.swigCPtr);
    }

    /* renamed from: br */
    public void mo19734br(long j) {
        MilesBridgeJNI.MSTREAM_TYPE_blocksize_set(this.swigCPtr, j);
    }

    /* renamed from: AM */
    public long mo19665AM() {
        return MilesBridgeJNI.MSTREAM_TYPE_blocksize_get(this.swigCPtr);
    }

    public int getPadding() {
        return MilesBridgeJNI.MSTREAM_TYPE_padding_get(this.swigCPtr);
    }

    public void setPadding(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_padding_set(this.swigCPtr, i);
    }

    /* renamed from: bG */
    public void mo19717bG(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_padded_set(this.swigCPtr, i);
    }

    /* renamed from: AN */
    public int mo19666AN() {
        return MilesBridgeJNI.MSTREAM_TYPE_padded_get(this.swigCPtr);
    }

    /* renamed from: bH */
    public void mo19718bH(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_loadedsome_set(this.swigCPtr, i);
    }

    /* renamed from: AO */
    public int mo19667AO() {
        return MilesBridgeJNI.MSTREAM_TYPE_loadedsome_get(this.swigCPtr);
    }

    /* renamed from: bs */
    public void mo19736bs(long j) {
        MilesBridgeJNI.MSTREAM_TYPE_startpos_set(this.swigCPtr, j);
    }

    /* renamed from: AP */
    public long mo19668AP() {
        return MilesBridgeJNI.MSTREAM_TYPE_startpos_get(this.swigCPtr);
    }

    /* renamed from: bt */
    public void mo19738bt(long j) {
        MilesBridgeJNI.MSTREAM_TYPE_totalread_set(this.swigCPtr, j);
    }

    /* renamed from: AQ */
    public long mo19669AQ() {
        return MilesBridgeJNI.MSTREAM_TYPE_totalread_get(this.swigCPtr);
    }

    /* renamed from: bu */
    public void mo19740bu(long j) {
        MilesBridgeJNI.MSTREAM_TYPE_loopsleft_set(this.swigCPtr, j);
    }

    /* renamed from: AR */
    public long mo19670AR() {
        return MilesBridgeJNI.MSTREAM_TYPE_loopsleft_get(this.swigCPtr);
    }

    /* renamed from: bv */
    public void mo19742bv(long j) {
        MilesBridgeJNI.MSTREAM_TYPE_error_set(this.swigCPtr, j);
    }

    /* renamed from: AS */
    public long mo19671AS() {
        return MilesBridgeJNI.MSTREAM_TYPE_error_get(this.swigCPtr);
    }

    /* renamed from: bI */
    public void mo19719bI(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_preload_set(this.swigCPtr, i);
    }

    /* renamed from: AT */
    public int mo19672AT() {
        return MilesBridgeJNI.MSTREAM_TYPE_preload_get(this.swigCPtr);
    }

    /* renamed from: bw */
    public void mo19744bw(long j) {
        MilesBridgeJNI.MSTREAM_TYPE_preloadpos_set(this.swigCPtr, j);
    }

    /* renamed from: AU */
    public long mo19673AU() {
        return MilesBridgeJNI.MSTREAM_TYPE_preloadpos_get(this.swigCPtr);
    }

    /* renamed from: bJ */
    public void mo19720bJ(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_noback_set(this.swigCPtr, i);
    }

    /* renamed from: AV */
    public int mo19674AV() {
        return MilesBridgeJNI.MSTREAM_TYPE_noback_get(this.swigCPtr);
    }

    /* renamed from: bK */
    public void mo19721bK(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_alldone_set(this.swigCPtr, i);
    }

    /* renamed from: AW */
    public int mo19675AW() {
        return MilesBridgeJNI.MSTREAM_TYPE_alldone_get(this.swigCPtr);
    }

    /* renamed from: bL */
    public void mo19722bL(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_primeamount_set(this.swigCPtr, i);
    }

    /* renamed from: AX */
    public int mo19676AX() {
        return MilesBridgeJNI.MSTREAM_TYPE_primeamount_get(this.swigCPtr);
    }

    /* renamed from: bM */
    public void mo19723bM(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_readatleast_set(this.swigCPtr, i);
    }

    /* renamed from: AY */
    public int mo19677AY() {
        return MilesBridgeJNI.MSTREAM_TYPE_readatleast_get(this.swigCPtr);
    }

    /* renamed from: bN */
    public void mo19724bN(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_playcontrol_set(this.swigCPtr, i);
    }

    /* renamed from: AZ */
    public int mo19678AZ() {
        return MilesBridgeJNI.MSTREAM_TYPE_playcontrol_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo19707a(aVC avc) {
        MilesBridgeJNI.MSTREAM_TYPE_callback_set(this.swigCPtr, aVC.m18785b(avc));
    }

    /* renamed from: Ba */
    public aVC mo19698Ba() {
        long MSTREAM_TYPE_callback_get = MilesBridgeJNI.MSTREAM_TYPE_callback_get(this.swigCPtr);
        if (MSTREAM_TYPE_callback_get == 0) {
            return null;
        }
        return new aVC(MSTREAM_TYPE_callback_get, false);
    }

    /* renamed from: g */
    public void mo19754g(C2480fe feVar) {
        MilesBridgeJNI.MSTREAM_TYPE_user_data_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    /* renamed from: Bb */
    public C2480fe mo19699Bb() {
        long MSTREAM_TYPE_user_data_get = MilesBridgeJNI.MSTREAM_TYPE_user_data_get(this.swigCPtr);
        if (MSTREAM_TYPE_user_data_get == 0) {
            return null;
        }
        return new C2480fe(MSTREAM_TYPE_user_data_get, false);
    }

    /* renamed from: a */
    public void mo19705a(aBO abo) {
        MilesBridgeJNI.MSTREAM_TYPE_next_set(this.swigCPtr, aBO.m12884g(abo));
    }

    /* renamed from: Bc */
    public aBO mo19700Bc() {
        long MSTREAM_TYPE_next_get = MilesBridgeJNI.MSTREAM_TYPE_next_get(this.swigCPtr);
        if (MSTREAM_TYPE_next_get == 0) {
            return null;
        }
        return new aBO(MSTREAM_TYPE_next_get, false);
    }

    /* renamed from: bO */
    public void mo19725bO(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_autostreaming_set(this.swigCPtr, i);
    }

    /* renamed from: Bd */
    public int mo19701Bd() {
        return MilesBridgeJNI.MSTREAM_TYPE_autostreaming_get(this.swigCPtr);
    }

    /* renamed from: bP */
    public void mo19726bP(int i) {
        MilesBridgeJNI.MSTREAM_TYPE_docallback_set(this.swigCPtr, i);
    }

    /* renamed from: Be */
    public int mo19702Be() {
        return MilesBridgeJNI.MSTREAM_TYPE_docallback_get(this.swigCPtr);
    }
}
