package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aWk */
/* compiled from: a */
public class aWk {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aWk(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aWk() {
        this(MilesBridgeJNI.new_EAX_RMODULATOR(), true);
    }

    /* renamed from: a */
    public static long m19273a(aWk awk) {
        if (awk == null) {
            return 0;
        }
        return awk.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_EAX_RMODULATOR(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: aD */
    public void mo12025aD(int i) {
        MilesBridgeJNI.EAX_RMODULATOR_Effect_set(this.swigCPtr, i);
    }

    /* renamed from: ma */
    public int mo12032ma() {
        return MilesBridgeJNI.EAX_RMODULATOR_Effect_get(this.swigCPtr);
    }

    public int getVolume() {
        return MilesBridgeJNI.EAX_RMODULATOR_Volume_get(this.swigCPtr);
    }

    public void setVolume(int i) {
        MilesBridgeJNI.EAX_RMODULATOR_Volume_set(this.swigCPtr, i);
    }

    /* renamed from: P */
    public void mo12024P(float f) {
        MilesBridgeJNI.EAX_RMODULATOR_Frequency_set(this.swigCPtr, f);
    }

    /* renamed from: mb */
    public float mo12033mb() {
        return MilesBridgeJNI.EAX_RMODULATOR_Frequency_get(this.swigCPtr);
    }

    /* renamed from: oJ */
    public void mo12034oJ(float f) {
        MilesBridgeJNI.EAX_RMODULATOR_HighPassCutOff_set(this.swigCPtr, f);
    }

    public float dDx() {
        return MilesBridgeJNI.EAX_RMODULATOR_HighPassCutOff_get(this.swigCPtr);
    }

    /* renamed from: iq */
    public void mo12031iq(long j) {
        MilesBridgeJNI.EAX_RMODULATOR_Waveform_set(this.swigCPtr, j);
    }

    public long crB() {
        return MilesBridgeJNI.EAX_RMODULATOR_Waveform_get(this.swigCPtr);
    }
}
