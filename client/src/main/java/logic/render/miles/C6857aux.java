package logic.render.miles;

/* renamed from: a.aux  reason: case insensitive filesystem */
/* compiled from: a */
public class C6857aux {
    private long swigCPtr;

    public C6857aux(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6857aux() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m26473b(C6857aux aux) {
        if (aux == null) {
            return 0;
        }
        return aux.swigCPtr;
    }
}
