package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aqS  reason: case insensitive filesystem */
/* compiled from: a */
public class C6618aqS {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6618aqS(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6618aqS() {
        this(MilesBridgeJNI.new_EAX_VMORPHER(), true);
    }

    /* renamed from: a */
    public static long m25111a(C6618aqS aqs) {
        if (aqs == null) {
            return 0;
        }
        return aqs.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_EAX_VMORPHER(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: aD */
    public void mo15566aD(int i) {
        MilesBridgeJNI.EAX_VMORPHER_Effect_set(this.swigCPtr, i);
    }

    /* renamed from: ma */
    public int mo15580ma() {
        return MilesBridgeJNI.EAX_VMORPHER_Effect_get(this.swigCPtr);
    }

    public int getVolume() {
        return MilesBridgeJNI.EAX_VMORPHER_Volume_get(this.swigCPtr);
    }

    public void setVolume(int i) {
        MilesBridgeJNI.EAX_VMORPHER_Volume_set(this.swigCPtr, i);
    }

    /* renamed from: io */
    public void mo15576io(long j) {
        MilesBridgeJNI.EAX_VMORPHER_PhonemeA_set(this.swigCPtr, j);
    }

    public long crx() {
        return MilesBridgeJNI.EAX_VMORPHER_PhonemeA_get(this.swigCPtr);
    }

    /* renamed from: tK */
    public void mo15582tK(int i) {
        MilesBridgeJNI.EAX_VMORPHER_PhonemeACoarseTuning_set(this.swigCPtr, i);
    }

    public int cry() {
        return MilesBridgeJNI.EAX_VMORPHER_PhonemeACoarseTuning_get(this.swigCPtr);
    }

    /* renamed from: ip */
    public void mo15577ip(long j) {
        MilesBridgeJNI.EAX_VMORPHER_PhonemeB_set(this.swigCPtr, j);
    }

    public long crz() {
        return MilesBridgeJNI.EAX_VMORPHER_PhonemeB_get(this.swigCPtr);
    }

    /* renamed from: tL */
    public void mo15583tL(int i) {
        MilesBridgeJNI.EAX_VMORPHER_PhonemeBCoarseTuning_set(this.swigCPtr, i);
    }

    public int crA() {
        return MilesBridgeJNI.EAX_VMORPHER_PhonemeBCoarseTuning_get(this.swigCPtr);
    }

    /* renamed from: iq */
    public void mo15578iq(long j) {
        MilesBridgeJNI.EAX_VMORPHER_Waveform_set(this.swigCPtr, j);
    }

    public long crB() {
        return MilesBridgeJNI.EAX_VMORPHER_Waveform_get(this.swigCPtr);
    }

    /* renamed from: kw */
    public void mo15579kw(float f) {
        MilesBridgeJNI.EAX_VMORPHER_Rate_set(this.swigCPtr, f);
    }

    public float crC() {
        return MilesBridgeJNI.EAX_VMORPHER_Rate_get(this.swigCPtr);
    }
}
