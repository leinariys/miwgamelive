package logic.render.gameswf;

import utaikodom.render.gameswf.GameSWFBridgeJNI;

/* renamed from: a.aX */
/* compiled from: a */
public class C1831aX {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1831aX(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    /* renamed from: a */
    public static long m19335a(C1831aX aXVar) {
        if (aXVar == null) {
            return 0;
        }
        return aXVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            GameSWFBridgeJNI.delete_movie_definition(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: fD */
    public int mo12107fD() {
        return GameSWFBridgeJNI.movie_definition_get_version(this.swigCPtr, this);
    }

    /* renamed from: fE */
    public float mo12108fE() {
        return GameSWFBridgeJNI.movie_definition_get_width_pixels(this.swigCPtr, this);
    }

    /* renamed from: fF */
    public float mo12109fF() {
        return GameSWFBridgeJNI.movie_definition_get_height_pixels(this.swigCPtr, this);
    }

    /* renamed from: fG */
    public int mo12110fG() {
        return GameSWFBridgeJNI.movie_definition_get_frame_count(this.swigCPtr, this);
    }

    /* renamed from: fH */
    public float mo12111fH() {
        return GameSWFBridgeJNI.movie_definition_get_frame_rate(this.swigCPtr, this);
    }

    /* renamed from: fI */
    public C3840ve mo12112fI() {
        long movie_definition_create_instance = GameSWFBridgeJNI.movie_definition_create_instance(this.swigCPtr, this);
        if (movie_definition_create_instance == 0) {
            return null;
        }
        return new C3840ve(movie_definition_create_instance, false);
    }

    /* renamed from: fJ */
    public void mo12113fJ() {
        GameSWFBridgeJNI.movie_definition_clear_instance(this.swigCPtr, this);
    }

    /* renamed from: a */
    public void mo12103a(C6987aya aya, C1332TW tw) {
        GameSWFBridgeJNI.movie_definition_output_cached_data(this.swigCPtr, this, C6987aya.m27466b(aya), C1332TW.m9980a(tw));
    }

    /* renamed from: a */
    public void mo12102a(C6987aya aya) {
        GameSWFBridgeJNI.movie_definition_input_cached_data(this.swigCPtr, this, C6987aya.m27466b(aya));
    }

    /* renamed from: fK */
    public void mo12114fK() {
        GameSWFBridgeJNI.movie_definition_generate_font_bitmaps(this.swigCPtr, this);
    }

    /* renamed from: a */
    public void mo12104a(C2220cv cvVar) {
        GameSWFBridgeJNI.movie_definition_visit_imported_movies(this.swigCPtr, this, C2220cv.m28643b(cvVar));
    }

    /* renamed from: a */
    public void mo12105a(String str, C1831aX aXVar) {
        GameSWFBridgeJNI.movie_definition_resolve_import(this.swigCPtr, this, str, m19335a(aXVar), aXVar);
    }

    /* renamed from: fL */
    public int mo12115fL() {
        return GameSWFBridgeJNI.movie_definition_get_bitmap_info_count(this.swigCPtr, this);
    }

    /* renamed from: M */
    public C1414Uj mo12101M(int i) {
        long movie_definition_get_bitmap_info = GameSWFBridgeJNI.movie_definition_get_bitmap_info(this.swigCPtr, this, i);
        if (movie_definition_get_bitmap_info == 0) {
            return null;
        }
        return new C1414Uj(movie_definition_get_bitmap_info, false);
    }
}
