package logic.render.gameswf;

import utaikodom.render.gameswf.GameSWFBridgeJNI;

import java.nio.Buffer;

/* renamed from: a.aSr  reason: case insensitive filesystem */
/* compiled from: a */
public class C5681aSr {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5681aSr(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5681aSr() {
        this(GameSWFBridgeJNI.new_MemoryFile(), true);
    }

    /* renamed from: a */
    public static long m18248a(C5681aSr asr) {
        if (asr == null) {
            return 0;
        }
        return asr.swigCPtr;
    }

    /* renamed from: c */
    public static void m18249c(int i, Buffer buffer) {
        GameSWFBridgeJNI.MemoryFile_setNextFile(i, buffer);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            GameSWFBridgeJNI.delete_MemoryFile(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }
}
