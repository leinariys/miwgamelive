package logic.render.gameswf;

/* renamed from: a.cv */
/* compiled from: a */
public class C2220cv {
    private long swigCPtr;

    public C2220cv(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C2220cv() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m28643b(C2220cv cvVar) {
        if (cvVar == null) {
            return 0;
        }
        return cvVar.swigCPtr;
    }
}
