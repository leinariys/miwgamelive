package logic.render.gameswf;

/* renamed from: a.afs  reason: case insensitive filesystem */
/* compiled from: a */
public class C6072afs {
    private long swigCPtr;

    public C6072afs(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6072afs() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m21648a(C6072afs afs) {
        if (afs == null) {
            return 0;
        }
        return afs.swigCPtr;
    }
}
