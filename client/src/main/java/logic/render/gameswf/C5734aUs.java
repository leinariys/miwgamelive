package logic.render.gameswf;

/* renamed from: a.aUs  reason: case insensitive filesystem */
/* compiled from: a */
public class C5734aUs {
    private long swigCPtr;

    public C5734aUs(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5734aUs() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m18752b(C5734aUs aus) {
        if (aus == null) {
            return 0;
        }
        return aus.swigCPtr;
    }
}
