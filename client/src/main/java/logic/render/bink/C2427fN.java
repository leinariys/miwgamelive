package logic.render.bink;

import utaikodom.render.bink.BinkBridgeJNI;

/* renamed from: a.fN */
/* compiled from: a */
public class C2427fN {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2427fN(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C2427fN() {
        this(BinkBridgeJNI.new_BINK(), true);
    }

    /* renamed from: a */
    public static long m30833a(C2427fN fNVar) {
        if (fNVar == null) {
            return 0;
        }
        return fNVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            BinkBridgeJNI.delete_BINK(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: S */
    public void mo18444S(long j) {
        BinkBridgeJNI.BINK_Width_set(this.swigCPtr, this, j);
    }

    /* renamed from: sN */
    public long mo18543sN() {
        return BinkBridgeJNI.BINK_Width_get(this.swigCPtr, this);
    }

    /* renamed from: T */
    public void mo18445T(long j) {
        BinkBridgeJNI.BINK_Height_set(this.swigCPtr, this, j);
    }

    /* renamed from: sO */
    public long mo18544sO() {
        return BinkBridgeJNI.BINK_Height_get(this.swigCPtr, this);
    }

    /* renamed from: U */
    public void mo18446U(long j) {
        BinkBridgeJNI.BINK_Frames_set(this.swigCPtr, this, j);
    }

    /* renamed from: sP */
    public long mo18545sP() {
        return BinkBridgeJNI.BINK_Frames_get(this.swigCPtr, this);
    }

    /* renamed from: V */
    public void mo18447V(long j) {
        BinkBridgeJNI.BINK_FrameNum_set(this.swigCPtr, this, j);
    }

    /* renamed from: sQ */
    public long mo18546sQ() {
        return BinkBridgeJNI.BINK_FrameNum_get(this.swigCPtr, this);
    }

    /* renamed from: W */
    public void mo18448W(long j) {
        BinkBridgeJNI.BINK_LastFrameNum_set(this.swigCPtr, this, j);
    }

    /* renamed from: sR */
    public long mo18547sR() {
        return BinkBridgeJNI.BINK_LastFrameNum_get(this.swigCPtr, this);
    }

    /* renamed from: X */
    public void mo18449X(long j) {
        BinkBridgeJNI.BINK_FrameRate_set(this.swigCPtr, this, j);
    }

    /* renamed from: sS */
    public long mo18548sS() {
        return BinkBridgeJNI.BINK_FrameRate_get(this.swigCPtr, this);
    }

    /* renamed from: Y */
    public void mo18450Y(long j) {
        BinkBridgeJNI.BINK_FrameRateDiv_set(this.swigCPtr, this, j);
    }

    /* renamed from: sT */
    public long mo18549sT() {
        return BinkBridgeJNI.BINK_FrameRateDiv_get(this.swigCPtr, this);
    }

    /* renamed from: Z */
    public void mo18451Z(long j) {
        BinkBridgeJNI.BINK_ReadError_set(this.swigCPtr, this, j);
    }

    /* renamed from: sU */
    public long mo18550sU() {
        return BinkBridgeJNI.BINK_ReadError_get(this.swigCPtr, this);
    }

    /* renamed from: aa */
    public void mo18486aa(long j) {
        BinkBridgeJNI.BINK_OpenFlags_set(this.swigCPtr, this, j);
    }

    /* renamed from: sV */
    public long mo18551sV() {
        return BinkBridgeJNI.BINK_OpenFlags_get(this.swigCPtr, this);
    }

    /* renamed from: ab */
    public void mo18487ab(long j) {
        BinkBridgeJNI.BINK_BinkType_set(this.swigCPtr, this, j);
    }

    /* renamed from: sW */
    public long mo18552sW() {
        return BinkBridgeJNI.BINK_BinkType_get(this.swigCPtr, this);
    }

    public long getSize() {
        return BinkBridgeJNI.BINK_Size_get(this.swigCPtr, this);
    }

    public void setSize(long j) {
        BinkBridgeJNI.BINK_Size_set(this.swigCPtr, this, j);
    }

    /* renamed from: ac */
    public void mo18488ac(long j) {
        BinkBridgeJNI.BINK_FrameSize_set(this.swigCPtr, this, j);
    }

    /* renamed from: sX */
    public long mo18553sX() {
        return BinkBridgeJNI.BINK_FrameSize_get(this.swigCPtr, this);
    }

    /* renamed from: ad */
    public void mo18489ad(long j) {
        BinkBridgeJNI.BINK_SndSize_set(this.swigCPtr, this, j);
    }

    /* renamed from: sY */
    public long mo18554sY() {
        return BinkBridgeJNI.BINK_SndSize_get(this.swigCPtr, this);
    }

    /* renamed from: ae */
    public void mo18490ae(long j) {
        BinkBridgeJNI.BINK_FrameChangePercent_set(this.swigCPtr, this, j);
    }

    /* renamed from: sZ */
    public long mo18555sZ() {
        return BinkBridgeJNI.BINK_FrameChangePercent_get(this.swigCPtr, this);
    }

    /* renamed from: a */
    public void mo18456a(C6679arb arb) {
        BinkBridgeJNI.BINK_FrameRects_set(this.swigCPtr, this, C6679arb.m25511b(arb), arb);
    }

    /* renamed from: ta */
    public C6679arb mo18583ta() {
        long BINK_FrameRects_get = BinkBridgeJNI.BINK_FrameRects_get(this.swigCPtr, this);
        if (BINK_FrameRects_get == 0) {
            return null;
        }
        return new C6679arb(BINK_FrameRects_get, false);
    }

    /* renamed from: ba */
    public void mo18516ba(int i) {
        BinkBridgeJNI.BINK_NumRects_set(this.swigCPtr, this, i);
    }

    /* renamed from: tb */
    public int mo18584tb() {
        return BinkBridgeJNI.BINK_NumRects_get(this.swigCPtr, this);
    }

    /* renamed from: a */
    public void mo18458a(C2816kW kWVar) {
        BinkBridgeJNI.BINK_FrameBuffers_set(this.swigCPtr, this, C2816kW.m34436c(kWVar), kWVar);
    }

    /* renamed from: tc */
    public C2816kW mo18585tc() {
        long BINK_FrameBuffers_get = BinkBridgeJNI.BINK_FrameBuffers_get(this.swigCPtr, this);
        if (BINK_FrameBuffers_get == 0) {
            return null;
        }
        return new C2816kW(BINK_FrameBuffers_get, false);
    }

    /* renamed from: a */
    public void mo18452a(C1263Sf sf) {
        BinkBridgeJNI.BINK_MaskPlane_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    /* renamed from: td */
    public C1263Sf mo18586td() {
        long BINK_MaskPlane_get = BinkBridgeJNI.BINK_MaskPlane_get(this.swigCPtr, this);
        if (BINK_MaskPlane_get == 0) {
            return null;
        }
        return new C1263Sf(BINK_MaskPlane_get, false);
    }

    /* renamed from: af */
    public void mo18491af(long j) {
        BinkBridgeJNI.BINK_MaskPitch_set(this.swigCPtr, this, j);
    }

    /* renamed from: te */
    public long mo18587te() {
        return BinkBridgeJNI.BINK_MaskPitch_get(this.swigCPtr, this);
    }

    /* renamed from: ag */
    public void mo18492ag(long j) {
        BinkBridgeJNI.BINK_MaskLength_set(this.swigCPtr, this, j);
    }

    /* renamed from: tf */
    public long mo18588tf() {
        return BinkBridgeJNI.BINK_MaskLength_get(this.swigCPtr, this);
    }

    /* renamed from: b */
    public void mo18512b(C1263Sf sf) {
        BinkBridgeJNI.BINK_AsyncMaskPlane_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    /* renamed from: tg */
    public C1263Sf mo18589tg() {
        long BINK_AsyncMaskPlane_get = BinkBridgeJNI.BINK_AsyncMaskPlane_get(this.swigCPtr, this);
        if (BINK_AsyncMaskPlane_get == 0) {
            return null;
        }
        return new C1263Sf(BINK_AsyncMaskPlane_get, false);
    }

    /* renamed from: c */
    public void mo18524c(C1263Sf sf) {
        BinkBridgeJNI.BINK_InUseMaskPlane_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    /* renamed from: th */
    public C1263Sf mo18590th() {
        long BINK_InUseMaskPlane_get = BinkBridgeJNI.BINK_InUseMaskPlane_get(this.swigCPtr, this);
        if (BINK_InUseMaskPlane_get == 0) {
            return null;
        }
        return new C1263Sf(BINK_InUseMaskPlane_get, false);
    }

    /* renamed from: d */
    public void mo18527d(C1263Sf sf) {
        BinkBridgeJNI.BINK_LastMaskPlane_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    /* renamed from: ti */
    public C1263Sf mo18591ti() {
        long BINK_LastMaskPlane_get = BinkBridgeJNI.BINK_LastMaskPlane_get(this.swigCPtr, this);
        if (BINK_LastMaskPlane_get == 0) {
            return null;
        }
        return new C1263Sf(BINK_LastMaskPlane_get, false);
    }

    /* renamed from: ah */
    public void mo18493ah(long j) {
        BinkBridgeJNI.BINK_LargestFrameSize_set(this.swigCPtr, this, j);
    }

    /* renamed from: tj */
    public long mo18592tj() {
        return BinkBridgeJNI.BINK_LargestFrameSize_get(this.swigCPtr, this);
    }

    /* renamed from: ai */
    public void mo18494ai(long j) {
        BinkBridgeJNI.BINK_InternalFrames_set(this.swigCPtr, this, j);
    }

    /* renamed from: tk */
    public long mo18593tk() {
        return BinkBridgeJNI.BINK_InternalFrames_get(this.swigCPtr, this);
    }

    /* renamed from: bb */
    public void mo18518bb(int i) {
        BinkBridgeJNI.BINK_NumTracks_set(this.swigCPtr, this, i);
    }

    public int getNumTracks() {
        return BinkBridgeJNI.BINK_NumTracks_get(this.swigCPtr, this);
    }

    /* renamed from: aj */
    public void mo18495aj(long j) {
        BinkBridgeJNI.BINK_Highest1SecRate_set(this.swigCPtr, this, j);
    }

    /* renamed from: tl */
    public long mo18594tl() {
        return BinkBridgeJNI.BINK_Highest1SecRate_get(this.swigCPtr, this);
    }

    /* renamed from: ak */
    public void mo18496ak(long j) {
        BinkBridgeJNI.BINK_Highest1SecFrame_set(this.swigCPtr, this, j);
    }

    /* renamed from: tm */
    public long mo18595tm() {
        return BinkBridgeJNI.BINK_Highest1SecFrame_get(this.swigCPtr, this);
    }

    /* renamed from: bc */
    public void mo18520bc(int i) {
        BinkBridgeJNI.BINK_Paused_set(this.swigCPtr, this, i);
    }

    /* renamed from: tn */
    public int mo18596tn() {
        return BinkBridgeJNI.BINK_Paused_get(this.swigCPtr, this);
    }

    /* renamed from: a */
    public void mo18455a(C6573apZ apz) {
        BinkBridgeJNI.BINK_async_in_progress_set(this.swigCPtr, this, C6573apZ.m24928d(apz));
    }

    /* renamed from: to */
    public C6573apZ mo18597to() {
        long BINK_async_in_progress_get = BinkBridgeJNI.BINK_async_in_progress_get(this.swigCPtr, this);
        if (BINK_async_in_progress_get == 0) {
            return null;
        }
        return new C6573apZ(BINK_async_in_progress_get, false);
    }

    /* renamed from: al */
    public void mo18497al(long j) {
        BinkBridgeJNI.BINK_soundon_set(this.swigCPtr, this, j);
    }

    /* renamed from: tp */
    public long mo18598tp() {
        return BinkBridgeJNI.BINK_soundon_get(this.swigCPtr, this);
    }

    /* renamed from: am */
    public void mo18498am(long j) {
        BinkBridgeJNI.BINK_videoon_set(this.swigCPtr, this, j);
    }

    /* renamed from: tq */
    public long mo18599tq() {
        return BinkBridgeJNI.BINK_videoon_get(this.swigCPtr, this);
    }

    /* renamed from: e */
    public void mo18530e(C1263Sf sf) {
        BinkBridgeJNI.BINK_compframe_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    /* renamed from: tr */
    public C1263Sf mo18600tr() {
        long BINK_compframe_get = BinkBridgeJNI.BINK_compframe_get(this.swigCPtr, this);
        if (BINK_compframe_get == 0) {
            return null;
        }
        return new C1263Sf(BINK_compframe_get, false);
    }

    /* renamed from: an */
    public void mo18499an(long j) {
        BinkBridgeJNI.BINK_compframesize_set(this.swigCPtr, this, j);
    }

    /* renamed from: ts */
    public long mo18601ts() {
        return BinkBridgeJNI.BINK_compframesize_get(this.swigCPtr, this);
    }

    /* renamed from: ao */
    public void mo18500ao(long j) {
        BinkBridgeJNI.BINK_compframeoffset_set(this.swigCPtr, this, j);
    }

    /* renamed from: tt */
    public long mo18602tt() {
        return BinkBridgeJNI.BINK_compframeoffset_get(this.swigCPtr, this);
    }

    /* renamed from: ap */
    public void mo18501ap(long j) {
        BinkBridgeJNI.BINK_compframekey_set(this.swigCPtr, this, j);
    }

    /* renamed from: tu */
    public long mo18603tu() {
        return BinkBridgeJNI.BINK_compframekey_get(this.swigCPtr, this);
    }

    /* renamed from: aq */
    public void mo18502aq(long j) {
        BinkBridgeJNI.BINK_skippedlastblit_set(this.swigCPtr, this, j);
    }

    /* renamed from: tv */
    public long mo18604tv() {
        return BinkBridgeJNI.BINK_skippedlastblit_get(this.swigCPtr, this);
    }

    /* renamed from: ar */
    public void mo18503ar(long j) {
        BinkBridgeJNI.BINK_playingtracks_set(this.swigCPtr, this, j);
    }

    /* renamed from: tw */
    public long mo18605tw() {
        return BinkBridgeJNI.BINK_playingtracks_get(this.swigCPtr, this);
    }

    /* renamed from: a */
    public void mo18457a(C2677iS iSVar) {
        BinkBridgeJNI.BINK_bsnd_set(this.swigCPtr, this, C2677iS.m33304b(iSVar), iSVar);
    }

    /* renamed from: tx */
    public C2677iS mo18606tx() {
        long BINK_bsnd_get = BinkBridgeJNI.BINK_bsnd_get(this.swigCPtr, this);
        if (BINK_bsnd_get == 0) {
            return null;
        }
        return new C2677iS(BINK_bsnd_get, false);
    }

    /* renamed from: b */
    public void mo18513b(C6573apZ apz) {
        BinkBridgeJNI.BINK_trackindexes_set(this.swigCPtr, this, C6573apZ.m24928d(apz));
    }

    /* renamed from: ty */
    public C6573apZ mo18607ty() {
        long BINK_trackindexes_get = BinkBridgeJNI.BINK_trackindexes_get(this.swigCPtr, this);
        if (BINK_trackindexes_get == 0) {
            return null;
        }
        return new C6573apZ(BINK_trackindexes_get, false);
    }

    /* renamed from: a */
    public void mo18459a(C3000mo moVar) {
        BinkBridgeJNI.BINK_bunp_set(this.swigCPtr, this, C3000mo.m35839b(moVar), moVar);
    }

    /* renamed from: tz */
    public C3000mo mo18608tz() {
        long BINK_bunp_get = BinkBridgeJNI.BINK_bunp_get(this.swigCPtr, this);
        if (BINK_bunp_get == 0) {
            return null;
        }
        return new C3000mo(BINK_bunp_get, false);
    }

    /* renamed from: as */
    public void mo18504as(long j) {
        BinkBridgeJNI.BINK_changepercent_set(this.swigCPtr, this, j);
    }

    /* renamed from: tA */
    public long mo18557tA() {
        return BinkBridgeJNI.BINK_changepercent_get(this.swigCPtr, this);
    }

    /* renamed from: f */
    public void mo18532f(C1263Sf sf) {
        BinkBridgeJNI.BINK_preloadptr_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    /* renamed from: tB */
    public C1263Sf mo18558tB() {
        long BINK_preloadptr_get = BinkBridgeJNI.BINK_preloadptr_get(this.swigCPtr, this);
        if (BINK_preloadptr_get == 0) {
            return null;
        }
        return new C1263Sf(BINK_preloadptr_get, false);
    }

    /* renamed from: b */
    public void mo18514b(C2091bv bvVar) {
        BinkBridgeJNI.BINK_frameoffsets_set(this.swigCPtr, this, C2091bv.m28102a(bvVar));
    }

    /* renamed from: tC */
    public C2091bv mo18559tC() {
        long BINK_frameoffsets_get = BinkBridgeJNI.BINK_frameoffsets_get(this.swigCPtr, this);
        if (BINK_frameoffsets_get == 0) {
            return null;
        }
        return new C2091bv(BINK_frameoffsets_get, false);
    }

    /* renamed from: a */
    public void mo18453a(C5340aFo afo) {
        BinkBridgeJNI.BINK_bio_set(this.swigCPtr, this, C5340aFo.m14647b(afo), afo);
    }

    /* renamed from: tD */
    public C5340aFo mo18560tD() {
        long BINK_bio_get = BinkBridgeJNI.BINK_bio_get(this.swigCPtr, this);
        if (BINK_bio_get == 0) {
            return null;
        }
        return new C5340aFo(BINK_bio_get, false);
    }

    /* renamed from: a */
    public void mo18454a(aTU atu) {
        BinkBridgeJNI.BINK_ioptr_set(this.swigCPtr, this, aTU.m18355h(atu));
    }

    /* renamed from: tE */
    public aTU mo18561tE() {
        long BINK_ioptr_get = BinkBridgeJNI.BINK_ioptr_get(this.swigCPtr, this);
        if (BINK_ioptr_get == 0) {
            return null;
        }
        return new aTU(BINK_ioptr_get, false);
    }

    /* renamed from: at */
    public void mo18505at(long j) {
        BinkBridgeJNI.BINK_iosize_set(this.swigCPtr, this, j);
    }

    /* renamed from: tF */
    public long mo18562tF() {
        return BinkBridgeJNI.BINK_iosize_get(this.swigCPtr, this);
    }

    /* renamed from: au */
    public void mo18506au(long j) {
        BinkBridgeJNI.BINK_decompwidth_set(this.swigCPtr, this, j);
    }

    /* renamed from: tG */
    public long mo18563tG() {
        return BinkBridgeJNI.BINK_decompwidth_get(this.swigCPtr, this);
    }

    /* renamed from: av */
    public void mo18507av(long j) {
        BinkBridgeJNI.BINK_decompheight_set(this.swigCPtr, this, j);
    }

    /* renamed from: tH */
    public long mo18564tH() {
        return BinkBridgeJNI.BINK_decompheight_get(this.swigCPtr, this);
    }

    /* renamed from: c */
    public void mo18526c(C2091bv bvVar) {
        BinkBridgeJNI.BINK_tracksizes_set(this.swigCPtr, this, C2091bv.m28102a(bvVar));
    }

    /* renamed from: tI */
    public C2091bv mo18565tI() {
        long BINK_tracksizes_get = BinkBridgeJNI.BINK_tracksizes_get(this.swigCPtr, this);
        if (BINK_tracksizes_get == 0) {
            return null;
        }
        return new C2091bv(BINK_tracksizes_get, false);
    }

    /* renamed from: d */
    public void mo18528d(C2091bv bvVar) {
        BinkBridgeJNI.BINK_tracktypes_set(this.swigCPtr, this, C2091bv.m28102a(bvVar));
    }

    /* renamed from: tJ */
    public C2091bv mo18566tJ() {
        long BINK_tracktypes_get = BinkBridgeJNI.BINK_tracktypes_get(this.swigCPtr, this);
        if (BINK_tracktypes_get == 0) {
            return null;
        }
        return new C2091bv(BINK_tracktypes_get, false);
    }

    /* renamed from: c */
    public void mo18525c(C6573apZ apz) {
        BinkBridgeJNI.BINK_trackIDs_set(this.swigCPtr, this, C6573apZ.m24928d(apz));
    }

    /* renamed from: tK */
    public C6573apZ mo18567tK() {
        long BINK_trackIDs_get = BinkBridgeJNI.BINK_trackIDs_get(this.swigCPtr, this);
        if (BINK_trackIDs_get == 0) {
            return null;
        }
        return new C6573apZ(BINK_trackIDs_get, false);
    }

    /* renamed from: aw */
    public void mo18508aw(long j) {
        BinkBridgeJNI.BINK_numrects_set(this.swigCPtr, this, j);
    }

    /* renamed from: tL */
    public long mo18568tL() {
        return BinkBridgeJNI.BINK_numrects_get(this.swigCPtr, this);
    }

    /* renamed from: ax */
    public void mo18509ax(long j) {
        BinkBridgeJNI.BINK_playedframes_set(this.swigCPtr, this, j);
    }

    /* renamed from: tM */
    public long mo18569tM() {
        return BinkBridgeJNI.BINK_playedframes_get(this.swigCPtr, this);
    }

    /* renamed from: ay */
    public void mo18510ay(long j) {
        BinkBridgeJNI.BINK_firstframetime_set(this.swigCPtr, this, j);
    }

    /* renamed from: tN */
    public long mo18570tN() {
        return BinkBridgeJNI.BINK_firstframetime_get(this.swigCPtr, this);
    }

    /* renamed from: az */
    public void mo18511az(long j) {
        BinkBridgeJNI.BINK_startblittime_set(this.swigCPtr, this, j);
    }

    /* renamed from: tO */
    public long mo18571tO() {
        return BinkBridgeJNI.BINK_startblittime_get(this.swigCPtr, this);
    }

    /* renamed from: aA */
    public void mo18460aA(long j) {
        BinkBridgeJNI.BINK_startsynctime_set(this.swigCPtr, this, j);
    }

    /* renamed from: tP */
    public long mo18572tP() {
        return BinkBridgeJNI.BINK_startsynctime_get(this.swigCPtr, this);
    }

    /* renamed from: aB */
    public void mo18461aB(long j) {
        BinkBridgeJNI.BINK_startsyncframe_set(this.swigCPtr, this, j);
    }

    /* renamed from: tQ */
    public long mo18573tQ() {
        return BinkBridgeJNI.BINK_startsyncframe_get(this.swigCPtr, this);
    }

    /* renamed from: aC */
    public void mo18462aC(long j) {
        BinkBridgeJNI.BINK_twoframestime_set(this.swigCPtr, this, j);
    }

    /* renamed from: tR */
    public long mo18574tR() {
        return BinkBridgeJNI.BINK_twoframestime_get(this.swigCPtr, this);
    }

    /* renamed from: aD */
    public void mo18463aD(long j) {
        BinkBridgeJNI.BINK_slowestframetime_set(this.swigCPtr, this, j);
    }

    /* renamed from: tS */
    public long mo18575tS() {
        return BinkBridgeJNI.BINK_slowestframetime_get(this.swigCPtr, this);
    }

    /* renamed from: aE */
    public void mo18464aE(long j) {
        BinkBridgeJNI.BINK_slowestframe_set(this.swigCPtr, this, j);
    }

    /* renamed from: tT */
    public long mo18576tT() {
        return BinkBridgeJNI.BINK_slowestframe_get(this.swigCPtr, this);
    }

    /* renamed from: aF */
    public void mo18465aF(long j) {
        BinkBridgeJNI.BINK_slowest2frametime_set(this.swigCPtr, this, j);
    }

    /* renamed from: tU */
    public long mo18577tU() {
        return BinkBridgeJNI.BINK_slowest2frametime_get(this.swigCPtr, this);
    }

    /* renamed from: aG */
    public void mo18466aG(long j) {
        BinkBridgeJNI.BINK_slowest2frame_set(this.swigCPtr, this, j);
    }

    /* renamed from: tV */
    public long mo18578tV() {
        return BinkBridgeJNI.BINK_slowest2frame_get(this.swigCPtr, this);
    }

    /* renamed from: aH */
    public void mo18467aH(long j) {
        BinkBridgeJNI.BINK_totalmem_set(this.swigCPtr, this, j);
    }

    /* renamed from: tW */
    public long mo18579tW() {
        return BinkBridgeJNI.BINK_totalmem_get(this.swigCPtr, this);
    }

    /* renamed from: aI */
    public void mo18468aI(long j) {
        BinkBridgeJNI.BINK_timevdecomp_set(this.swigCPtr, this, j);
    }

    /* renamed from: tX */
    public long mo18580tX() {
        return BinkBridgeJNI.BINK_timevdecomp_get(this.swigCPtr, this);
    }

    /* renamed from: aJ */
    public void mo18469aJ(long j) {
        BinkBridgeJNI.BINK_timeadecomp_set(this.swigCPtr, this, j);
    }

    /* renamed from: tY */
    public long mo18581tY() {
        return BinkBridgeJNI.BINK_timeadecomp_get(this.swigCPtr, this);
    }

    /* renamed from: aK */
    public void mo18470aK(long j) {
        BinkBridgeJNI.BINK_timeblit_set(this.swigCPtr, this, j);
    }

    /* renamed from: tZ */
    public long mo18582tZ() {
        return BinkBridgeJNI.BINK_timeblit_get(this.swigCPtr, this);
    }

    /* renamed from: aL */
    public void mo18471aL(long j) {
        BinkBridgeJNI.BINK_timeopen_set(this.swigCPtr, this, j);
    }

    /* renamed from: ua */
    public long mo18612ua() {
        return BinkBridgeJNI.BINK_timeopen_get(this.swigCPtr, this);
    }

    /* renamed from: aM */
    public void mo18472aM(long j) {
        BinkBridgeJNI.BINK_fileframerate_set(this.swigCPtr, this, j);
    }

    /* renamed from: ub */
    public long mo18613ub() {
        return BinkBridgeJNI.BINK_fileframerate_get(this.swigCPtr, this);
    }

    /* renamed from: aN */
    public void mo18473aN(long j) {
        BinkBridgeJNI.BINK_fileframeratediv_set(this.swigCPtr, this, j);
    }

    /* renamed from: uc */
    public long mo18614uc() {
        return BinkBridgeJNI.BINK_fileframeratediv_get(this.swigCPtr, this);
    }

    /* renamed from: aO */
    public void mo18474aO(long j) {
        BinkBridgeJNI.BINK_runtimeframes_set(this.swigCPtr, this, j);
    }

    /* renamed from: ud */
    public long mo18615ud() {
        return BinkBridgeJNI.BINK_runtimeframes_get(this.swigCPtr, this);
    }

    /* renamed from: bd */
    public void mo18522bd(int i) {
        BinkBridgeJNI.BINK_rtindex_set(this.swigCPtr, this, i);
    }

    /* renamed from: ue */
    public int mo18616ue() {
        return BinkBridgeJNI.BINK_rtindex_get(this.swigCPtr, this);
    }

    /* renamed from: e */
    public void mo18531e(C2091bv bvVar) {
        BinkBridgeJNI.BINK_rtframetimes_set(this.swigCPtr, this, C2091bv.m28102a(bvVar));
    }

    /* renamed from: uf */
    public C2091bv mo18617uf() {
        long BINK_rtframetimes_get = BinkBridgeJNI.BINK_rtframetimes_get(this.swigCPtr, this);
        if (BINK_rtframetimes_get == 0) {
            return null;
        }
        return new C2091bv(BINK_rtframetimes_get, false);
    }

    /* renamed from: f */
    public void mo18533f(C2091bv bvVar) {
        BinkBridgeJNI.BINK_rtadecomptimes_set(this.swigCPtr, this, C2091bv.m28102a(bvVar));
    }

    /* renamed from: ug */
    public C2091bv mo18618ug() {
        long BINK_rtadecomptimes_get = BinkBridgeJNI.BINK_rtadecomptimes_get(this.swigCPtr, this);
        if (BINK_rtadecomptimes_get == 0) {
            return null;
        }
        return new C2091bv(BINK_rtadecomptimes_get, false);
    }

    /* renamed from: g */
    public void mo18535g(C2091bv bvVar) {
        BinkBridgeJNI.BINK_rtvdecomptimes_set(this.swigCPtr, this, C2091bv.m28102a(bvVar));
    }

    /* renamed from: uh */
    public C2091bv mo18619uh() {
        long BINK_rtvdecomptimes_get = BinkBridgeJNI.BINK_rtvdecomptimes_get(this.swigCPtr, this);
        if (BINK_rtvdecomptimes_get == 0) {
            return null;
        }
        return new C2091bv(BINK_rtvdecomptimes_get, false);
    }

    /* renamed from: h */
    public void mo18538h(C2091bv bvVar) {
        BinkBridgeJNI.BINK_rtblittimes_set(this.swigCPtr, this, C2091bv.m28102a(bvVar));
    }

    /* renamed from: ui */
    public C2091bv mo18620ui() {
        long BINK_rtblittimes_get = BinkBridgeJNI.BINK_rtblittimes_get(this.swigCPtr, this);
        if (BINK_rtblittimes_get == 0) {
            return null;
        }
        return new C2091bv(BINK_rtblittimes_get, false);
    }

    /* renamed from: i */
    public void mo18539i(C2091bv bvVar) {
        BinkBridgeJNI.BINK_rtreadtimes_set(this.swigCPtr, this, C2091bv.m28102a(bvVar));
    }

    /* renamed from: uj */
    public C2091bv mo18621uj() {
        long BINK_rtreadtimes_get = BinkBridgeJNI.BINK_rtreadtimes_get(this.swigCPtr, this);
        if (BINK_rtreadtimes_get == 0) {
            return null;
        }
        return new C2091bv(BINK_rtreadtimes_get, false);
    }

    /* renamed from: j */
    public void mo18540j(C2091bv bvVar) {
        BinkBridgeJNI.BINK_rtidlereadtimes_set(this.swigCPtr, this, C2091bv.m28102a(bvVar));
    }

    /* renamed from: uk */
    public C2091bv mo18622uk() {
        long BINK_rtidlereadtimes_get = BinkBridgeJNI.BINK_rtidlereadtimes_get(this.swigCPtr, this);
        if (BINK_rtidlereadtimes_get == 0) {
            return null;
        }
        return new C2091bv(BINK_rtidlereadtimes_get, false);
    }

    /* renamed from: k */
    public void mo18541k(C2091bv bvVar) {
        BinkBridgeJNI.BINK_rtthreadreadtimes_set(this.swigCPtr, this, C2091bv.m28102a(bvVar));
    }

    /* renamed from: ul */
    public C2091bv mo18623ul() {
        long BINK_rtthreadreadtimes_get = BinkBridgeJNI.BINK_rtthreadreadtimes_get(this.swigCPtr, this);
        if (BINK_rtthreadreadtimes_get == 0) {
            return null;
        }
        return new C2091bv(BINK_rtthreadreadtimes_get, false);
    }

    /* renamed from: aP */
    public void mo18475aP(long j) {
        BinkBridgeJNI.BINK_lastblitflags_set(this.swigCPtr, this, j);
    }

    /* renamed from: um */
    public long mo18624um() {
        return BinkBridgeJNI.BINK_lastblitflags_get(this.swigCPtr, this);
    }

    /* renamed from: aQ */
    public void mo18476aQ(long j) {
        BinkBridgeJNI.BINK_lastdecompframe_set(this.swigCPtr, this, j);
    }

    /* renamed from: un */
    public long mo18625un() {
        return BinkBridgeJNI.BINK_lastdecompframe_get(this.swigCPtr, this);
    }

    /* renamed from: aR */
    public void mo18477aR(long j) {
        BinkBridgeJNI.BINK_lastfinisheddoframe_set(this.swigCPtr, this, j);
    }

    /* renamed from: uo */
    public long mo18626uo() {
        return BinkBridgeJNI.BINK_lastfinisheddoframe_get(this.swigCPtr, this);
    }

    /* renamed from: aS */
    public void mo18478aS(long j) {
        BinkBridgeJNI.BINK_lastresynctime_set(this.swigCPtr, this, j);
    }

    /* renamed from: up */
    public long mo18627up() {
        return BinkBridgeJNI.BINK_lastresynctime_get(this.swigCPtr, this);
    }

    /* renamed from: aT */
    public void mo18479aT(long j) {
        BinkBridgeJNI.BINK_doresync_set(this.swigCPtr, this, j);
    }

    /* renamed from: uq */
    public long mo18628uq() {
        return BinkBridgeJNI.BINK_doresync_get(this.swigCPtr, this);
    }

    /* renamed from: aU */
    public void mo18480aU(long j) {
        BinkBridgeJNI.BINK_soundskips_set(this.swigCPtr, this, j);
    }

    /* renamed from: ur */
    public long mo18629ur() {
        return BinkBridgeJNI.BINK_soundskips_get(this.swigCPtr, this);
    }

    /* renamed from: aV */
    public void mo18481aV(long j) {
        BinkBridgeJNI.BINK_skipped_status_this_frame_set(this.swigCPtr, this, j);
    }

    /* renamed from: us */
    public long mo18630us() {
        return BinkBridgeJNI.BINK_skipped_status_this_frame_get(this.swigCPtr, this);
    }

    /* renamed from: aW */
    public void mo18482aW(long j) {
        BinkBridgeJNI.BINK_very_delayed_set(this.swigCPtr, this, j);
    }

    /* renamed from: ut */
    public long mo18631ut() {
        return BinkBridgeJNI.BINK_very_delayed_get(this.swigCPtr, this);
    }

    /* renamed from: aX */
    public void mo18483aX(long j) {
        BinkBridgeJNI.BINK_skippedblits_set(this.swigCPtr, this, j);
    }

    /* renamed from: uu */
    public long mo18632uu() {
        return BinkBridgeJNI.BINK_skippedblits_get(this.swigCPtr, this);
    }

    /* renamed from: aY */
    public void mo18484aY(long j) {
        BinkBridgeJNI.BINK_skipped_in_a_row_set(this.swigCPtr, this, j);
    }

    /* renamed from: uv */
    public long mo18633uv() {
        return BinkBridgeJNI.BINK_skipped_in_a_row_get(this.swigCPtr, this);
    }

    /* renamed from: aZ */
    public void mo18485aZ(long j) {
        BinkBridgeJNI.BINK_paused_sync_diff_set(this.swigCPtr, this, j);
    }

    /* renamed from: uw */
    public long mo18634uw() {
        return BinkBridgeJNI.BINK_paused_sync_diff_get(this.swigCPtr, this);
    }

    /* renamed from: ba */
    public void mo18517ba(long j) {
        BinkBridgeJNI.BINK_last_time_almost_empty_set(this.swigCPtr, this, j);
    }

    /* renamed from: ux */
    public long mo18635ux() {
        return BinkBridgeJNI.BINK_last_time_almost_empty_get(this.swigCPtr, this);
    }

    /* renamed from: bb */
    public void mo18519bb(long j) {
        BinkBridgeJNI.BINK_last_read_count_set(this.swigCPtr, this, j);
    }

    /* renamed from: uy */
    public long mo18636uy() {
        return BinkBridgeJNI.BINK_last_read_count_get(this.swigCPtr, this);
    }

    /* renamed from: bc */
    public void mo18521bc(long j) {
        BinkBridgeJNI.BINK_last_sound_count_set(this.swigCPtr, this, j);
    }

    /* renamed from: uz */
    public long mo18637uz() {
        return BinkBridgeJNI.BINK_last_sound_count_get(this.swigCPtr, this);
    }

    /* renamed from: l */
    public void mo18542l(C2091bv bvVar) {
        BinkBridgeJNI.BINK_snd_callback_buffer_set(this.swigCPtr, this, C2091bv.m28102a(bvVar));
    }

    /* renamed from: uA */
    public C2091bv mo18609uA() {
        long BINK_snd_callback_buffer_get = BinkBridgeJNI.BINK_snd_callback_buffer_get(this.swigCPtr, this);
        if (BINK_snd_callback_buffer_get == 0) {
            return null;
        }
        return new C2091bv(BINK_snd_callback_buffer_get, false);
    }

    /* renamed from: be */
    public void mo18523be(int i) {
        BinkBridgeJNI.BINK_allkeys_set(this.swigCPtr, this, i);
    }

    /* renamed from: uB */
    public int mo18610uB() {
        return BinkBridgeJNI.BINK_allkeys_get(this.swigCPtr, this);
    }

    /* renamed from: b */
    public void mo18515b(C2816kW kWVar) {
        BinkBridgeJNI.BINK_allocatedframebuffers_set(this.swigCPtr, this, C2816kW.m34436c(kWVar), kWVar);
    }

    /* renamed from: uC */
    public C2816kW mo18611uC() {
        long BINK_allocatedframebuffers_get = BinkBridgeJNI.BINK_allocatedframebuffers_get(this.swigCPtr, this);
        if (BINK_allocatedframebuffers_get == 0) {
            return null;
        }
        return new C2816kW(BINK_allocatedframebuffers_get, false);
    }
}
