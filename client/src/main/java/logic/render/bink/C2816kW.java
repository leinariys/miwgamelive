package logic.render.bink;

import utaikodom.render.bink.BinkBridgeJNI;

/* renamed from: a.kW */
/* compiled from: a */
public class C2816kW {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2816kW(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C2816kW() {
        this(BinkBridgeJNI.new_BINKFRAMEBUFFERS(), true);
    }

    /* renamed from: c */
    public static long m34436c(C2816kW kWVar) {
        if (kWVar == null) {
            return 0;
        }
        return kWVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            BinkBridgeJNI.delete_BINKFRAMEBUFFERS(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: cJ */
    public void mo20083cJ(int i) {
        BinkBridgeJNI.BINKFRAMEBUFFERS_TotalFrames_set(this.swigCPtr, this, i);
    }

    /* renamed from: Lf */
    public int mo20075Lf() {
        return BinkBridgeJNI.BINKFRAMEBUFFERS_TotalFrames_get(this.swigCPtr, this);
    }

    /* renamed from: cf */
    public void mo20084cf(long j) {
        BinkBridgeJNI.BINKFRAMEBUFFERS_YABufferWidth_set(this.swigCPtr, this, j);
    }

    /* renamed from: Lg */
    public long mo20076Lg() {
        return BinkBridgeJNI.BINKFRAMEBUFFERS_YABufferWidth_get(this.swigCPtr, this);
    }

    /* renamed from: cg */
    public void mo20085cg(long j) {
        BinkBridgeJNI.BINKFRAMEBUFFERS_YABufferHeight_set(this.swigCPtr, this, j);
    }

    /* renamed from: Lh */
    public long mo20077Lh() {
        return BinkBridgeJNI.BINKFRAMEBUFFERS_YABufferHeight_get(this.swigCPtr, this);
    }

    /* renamed from: ch */
    public void mo20086ch(long j) {
        BinkBridgeJNI.BINKFRAMEBUFFERS_cRcBBufferWidth_set(this.swigCPtr, this, j);
    }

    /* renamed from: Li */
    public long mo20078Li() {
        return BinkBridgeJNI.BINKFRAMEBUFFERS_cRcBBufferWidth_get(this.swigCPtr, this);
    }

    /* renamed from: ci */
    public void mo20087ci(long j) {
        BinkBridgeJNI.BINKFRAMEBUFFERS_cRcBBufferHeight_set(this.swigCPtr, this, j);
    }

    /* renamed from: Lj */
    public long mo20079Lj() {
        return BinkBridgeJNI.BINKFRAMEBUFFERS_cRcBBufferHeight_get(this.swigCPtr, this);
    }

    /* renamed from: V */
    public void mo20081V(long j) {
        BinkBridgeJNI.BINKFRAMEBUFFERS_FrameNum_set(this.swigCPtr, this, j);
    }

    /* renamed from: sQ */
    public long mo20090sQ() {
        return BinkBridgeJNI.BINKFRAMEBUFFERS_FrameNum_get(this.swigCPtr, this);
    }

    /* renamed from: a */
    public void mo20082a(C3121oA oAVar) {
        BinkBridgeJNI.BINKFRAMEBUFFERS_Frames_set(this.swigCPtr, this, C3121oA.m36587b(oAVar), oAVar);
    }

    /* renamed from: Lk */
    public C3121oA mo20080Lk() {
        long BINKFRAMEBUFFERS_Frames_get = BinkBridgeJNI.BINKFRAMEBUFFERS_Frames_get(this.swigCPtr, this);
        if (BINKFRAMEBUFFERS_Frames_get == 0) {
            return null;
        }
        return new C3121oA(BINKFRAMEBUFFERS_Frames_get, false);
    }
}
