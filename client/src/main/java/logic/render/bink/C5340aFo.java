package logic.render.bink;

import utaikodom.render.bink.BinkBridgeJNI;

/* renamed from: a.aFo  reason: case insensitive filesystem */
/* compiled from: a */
public class C5340aFo {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5340aFo(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5340aFo() {
        this(BinkBridgeJNI.new_BINKIO(), true);
    }

    /* renamed from: b */
    public static long m14647b(C5340aFo afo) {
        if (afo == null) {
            return 0;
        }
        return afo.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            BinkBridgeJNI.delete_BINKIO(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo8812b(C3878wC wCVar) {
        BinkBridgeJNI.BINKIO_ReadHeader_set(this.swigCPtr, this, C3878wC.m40489a(wCVar));
    }

    public C3878wC cYn() {
        long BINKIO_ReadHeader_get = BinkBridgeJNI.BINKIO_ReadHeader_get(this.swigCPtr, this);
        if (BINKIO_ReadHeader_get == 0) {
            return null;
        }
        return new C3878wC(BINKIO_ReadHeader_get, false);
    }

    /* renamed from: b */
    public void mo8813b(C4026yH yHVar) {
        BinkBridgeJNI.BINKIO_ReadFrame_set(this.swigCPtr, this, C4026yH.m41261a(yHVar));
    }

    public C4026yH cYo() {
        long BINKIO_ReadFrame_get = BinkBridgeJNI.BINKIO_ReadFrame_get(this.swigCPtr, this);
        if (BINKIO_ReadFrame_get == 0) {
            return null;
        }
        return new C4026yH(BINKIO_ReadFrame_get, false);
    }

    /* renamed from: b */
    public void mo8808b(C0742Kb kb) {
        BinkBridgeJNI.BINKIO_GetBufferSize_set(this.swigCPtr, this, C0742Kb.m6485a(kb));
    }

    public C0742Kb cYp() {
        long BINKIO_GetBufferSize_get = BinkBridgeJNI.BINKIO_GetBufferSize_get(this.swigCPtr, this);
        if (BINKIO_GetBufferSize_get == 0) {
            return null;
        }
        return new C0742Kb(BINKIO_GetBufferSize_get, false);
    }

    /* renamed from: a */
    public void mo8807a(aWj awj) {
        BinkBridgeJNI.BINKIO_SetInfo_set(this.swigCPtr, this, aWj.m19272b(awj));
    }

    public aWj cYq() {
        long BINKIO_SetInfo_get = BinkBridgeJNI.BINKIO_SetInfo_get(this.swigCPtr, this);
        if (BINKIO_SetInfo_get == 0) {
            return null;
        }
        return new aWj(BINKIO_SetInfo_get, false);
    }

    /* renamed from: b */
    public void mo8810b(C5217aAv aav) {
        BinkBridgeJNI.BINKIO_Idle_set(this.swigCPtr, this, C5217aAv.m12687a(aav));
    }

    public C5217aAv cYr() {
        long BINKIO_Idle_get = BinkBridgeJNI.BINKIO_Idle_get(this.swigCPtr, this);
        if (BINKIO_Idle_get == 0) {
            return null;
        }
        return new C5217aAv(BINKIO_Idle_get, false);
    }

    /* renamed from: b */
    public void mo8809b(C1289Sw sw) {
        BinkBridgeJNI.BINKIO_Close_set(this.swigCPtr, this, C1289Sw.m9633a(sw));
    }

    public C1289Sw cYs() {
        long BINKIO_Close_get = BinkBridgeJNI.BINKIO_Close_get(this.swigCPtr, this);
        if (BINKIO_Close_get == 0) {
            return null;
        }
        return new C1289Sw(BINKIO_Close_get, false);
    }

    /* renamed from: b */
    public void mo8814b(C4032yN yNVar) {
        BinkBridgeJNI.BINKIO_BGControl_set(this.swigCPtr, this, C4032yN.m41269a(yNVar));
    }

    public C4032yN cYt() {
        long BINKIO_BGControl_get = BinkBridgeJNI.BINKIO_BGControl_get(this.swigCPtr, this);
        if (BINKIO_BGControl_get == 0) {
            return null;
        }
        return new C4032yN(BINKIO_BGControl_get, false);
    }

    public C2427fN getBink() {
        long BINKIO_bink_get = BinkBridgeJNI.BINKIO_bink_get(this.swigCPtr, this);
        if (BINKIO_bink_get == 0) {
            return null;
        }
        return new C2427fN(BINKIO_bink_get, false);
    }

    public void setBink(C2427fN fNVar) {
        BinkBridgeJNI.BINKIO_bink_set(this.swigCPtr, this, C2427fN.m30833a(fNVar), fNVar);
    }

    /* renamed from: Z */
    public void mo8806Z(long j) {
        BinkBridgeJNI.BINKIO_ReadError_set(this.swigCPtr, this, j);
    }

    /* renamed from: sU */
    public long mo8860sU() {
        return BinkBridgeJNI.BINKIO_ReadError_get(this.swigCPtr, this);
    }

    /* renamed from: kC */
    public void mo8848kC(long j) {
        BinkBridgeJNI.BINKIO_DoingARead_set(this.swigCPtr, this, j);
    }

    public long cYu() {
        return BinkBridgeJNI.BINKIO_DoingARead_get(this.swigCPtr, this);
    }

    /* renamed from: kD */
    public void mo8849kD(long j) {
        BinkBridgeJNI.BINKIO_BytesRead_set(this.swigCPtr, this, j);
    }

    public long getBytesRead() {
        return BinkBridgeJNI.BINKIO_BytesRead_get(this.swigCPtr, this);
    }

    /* renamed from: kE */
    public void mo8850kE(long j) {
        BinkBridgeJNI.BINKIO_Working_set(this.swigCPtr, this, j);
    }

    public long cYv() {
        return BinkBridgeJNI.BINKIO_Working_get(this.swigCPtr, this);
    }

    /* renamed from: jO */
    public void mo8847jO(long j) {
        BinkBridgeJNI.BINKIO_TotalTime_set(this.swigCPtr, this, j);
    }

    public long getTotalTime() {
        return BinkBridgeJNI.BINKIO_TotalTime_get(this.swigCPtr, this);
    }

    /* renamed from: kF */
    public void mo8851kF(long j) {
        BinkBridgeJNI.BINKIO_ForegroundTime_set(this.swigCPtr, this, j);
    }

    public long cYw() {
        return BinkBridgeJNI.BINKIO_ForegroundTime_get(this.swigCPtr, this);
    }

    /* renamed from: kG */
    public void mo8852kG(long j) {
        BinkBridgeJNI.BINKIO_IdleTime_set(this.swigCPtr, this, j);
    }

    public long cYx() {
        return BinkBridgeJNI.BINKIO_IdleTime_get(this.swigCPtr, this);
    }

    /* renamed from: kH */
    public void mo8853kH(long j) {
        BinkBridgeJNI.BINKIO_ThreadTime_set(this.swigCPtr, this, j);
    }

    public long cYy() {
        return BinkBridgeJNI.BINKIO_ThreadTime_get(this.swigCPtr, this);
    }

    /* renamed from: kI */
    public void mo8854kI(long j) {
        BinkBridgeJNI.BINKIO_BufSize_set(this.swigCPtr, this, j);
    }

    public long cYz() {
        return BinkBridgeJNI.BINKIO_BufSize_get(this.swigCPtr, this);
    }

    /* renamed from: kJ */
    public void mo8855kJ(long j) {
        BinkBridgeJNI.BINKIO_BufHighUsed_set(this.swigCPtr, this, j);
    }

    public long cYA() {
        return BinkBridgeJNI.BINKIO_BufHighUsed_get(this.swigCPtr, this);
    }

    /* renamed from: kK */
    public void mo8856kK(long j) {
        BinkBridgeJNI.BINKIO_CurBufSize_set(this.swigCPtr, this, j);
    }

    public long cYB() {
        return BinkBridgeJNI.BINKIO_CurBufSize_get(this.swigCPtr, this);
    }

    /* renamed from: kL */
    public void mo8857kL(long j) {
        BinkBridgeJNI.BINKIO_CurBufUsed_set(this.swigCPtr, this, j);
    }

    public long cYC() {
        return BinkBridgeJNI.BINKIO_CurBufUsed_get(this.swigCPtr, this);
    }

    /* renamed from: kM */
    public void mo8858kM(long j) {
        BinkBridgeJNI.BINKIO_Suspended_set(this.swigCPtr, this, j);
    }

    public long cYD() {
        return BinkBridgeJNI.BINKIO_Suspended_get(this.swigCPtr, this);
    }

    /* renamed from: g */
    public void mo8843g(aTU atu) {
        BinkBridgeJNI.BINKIO_iodata_set(this.swigCPtr, this, aTU.m18355h(atu));
    }

    public aTU cYE() {
        long BINKIO_iodata_get = BinkBridgeJNI.BINKIO_iodata_get(this.swigCPtr, this);
        if (BINKIO_iodata_get == 0) {
            return null;
        }
        return new aTU(BINKIO_iodata_get, false);
    }

    /* renamed from: c */
    public void mo8815c(C1289Sw sw) {
        BinkBridgeJNI.BINKIO_suspend_callback_set(this.swigCPtr, this, C1289Sw.m9633a(sw));
    }

    public C1289Sw cYF() {
        long BINKIO_suspend_callback_get = BinkBridgeJNI.BINKIO_suspend_callback_get(this.swigCPtr, this);
        if (BINKIO_suspend_callback_get == 0) {
            return null;
        }
        return new C1289Sw(BINKIO_suspend_callback_get, false);
    }

    /* renamed from: b */
    public void mo8811b(C6921awJ awj) {
        BinkBridgeJNI.BINKIO_try_suspend_callback_set(this.swigCPtr, this, C6921awJ.m26856a(awj));
    }

    public C6921awJ cYG() {
        long BINKIO_try_suspend_callback_get = BinkBridgeJNI.BINKIO_try_suspend_callback_get(this.swigCPtr, this);
        if (BINKIO_try_suspend_callback_get == 0) {
            return null;
        }
        return new C6921awJ(BINKIO_try_suspend_callback_get, false);
    }

    /* renamed from: d */
    public void mo8839d(C1289Sw sw) {
        BinkBridgeJNI.BINKIO_resume_callback_set(this.swigCPtr, this, C1289Sw.m9633a(sw));
    }

    public C1289Sw cYH() {
        long BINKIO_resume_callback_get = BinkBridgeJNI.BINKIO_resume_callback_get(this.swigCPtr, this);
        if (BINKIO_resume_callback_get == 0) {
            return null;
        }
        return new C1289Sw(BINKIO_resume_callback_get, false);
    }

    /* renamed from: e */
    public void mo8841e(C1289Sw sw) {
        BinkBridgeJNI.BINKIO_idle_on_callback_set(this.swigCPtr, this, C1289Sw.m9633a(sw));
    }

    public C1289Sw cYI() {
        long BINKIO_idle_on_callback_get = BinkBridgeJNI.BINKIO_idle_on_callback_get(this.swigCPtr, this);
        if (BINKIO_idle_on_callback_get == 0) {
            return null;
        }
        return new C1289Sw(BINKIO_idle_on_callback_get, false);
    }

    /* renamed from: m */
    public void mo8859m(C2091bv bvVar) {
        BinkBridgeJNI.BINKIO_callback_control_set(this.swigCPtr, this, C2091bv.m28102a(bvVar));
    }

    public C2091bv cYJ() {
        long BINKIO_callback_control_get = BinkBridgeJNI.BINKIO_callback_control_get(this.swigCPtr, this);
        if (BINKIO_callback_control_get == 0) {
            return null;
        }
        return new C2091bv(BINKIO_callback_control_get, false);
    }
}
