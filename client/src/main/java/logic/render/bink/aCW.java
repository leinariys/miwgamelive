package logic.render.bink;

import utaikodom.render.bink.BinkBridgeJNI;

/* renamed from: a.aCW */
/* compiled from: a */
public class aCW {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aCW(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aCW() {
        this(BinkBridgeJNI.new_BINKSUMMARY(), true);
    }

    /* renamed from: a */
    public static long m13300a(aCW acw) {
        if (acw == null) {
            return 0;
        }
        return acw.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            BinkBridgeJNI.delete_BINKSUMMARY(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: S */
    public void mo8129S(long j) {
        BinkBridgeJNI.BINKSUMMARY_Width_set(this.swigCPtr, this, j);
    }

    /* renamed from: sN */
    public long mo8187sN() {
        return BinkBridgeJNI.BINKSUMMARY_Width_get(this.swigCPtr, this);
    }

    /* renamed from: T */
    public void mo8130T(long j) {
        BinkBridgeJNI.BINKSUMMARY_Height_set(this.swigCPtr, this, j);
    }

    /* renamed from: sO */
    public long mo8188sO() {
        return BinkBridgeJNI.BINKSUMMARY_Height_get(this.swigCPtr, this);
    }

    /* renamed from: jO */
    public void mo8162jO(long j) {
        BinkBridgeJNI.BINKSUMMARY_TotalTime_set(this.swigCPtr, this, j);
    }

    public long getTotalTime() {
        return BinkBridgeJNI.BINKSUMMARY_TotalTime_get(this.swigCPtr, this);
    }

    /* renamed from: jP */
    public void mo8163jP(long j) {
        BinkBridgeJNI.BINKSUMMARY_FileFrameRate_set(this.swigCPtr, this, j);
    }

    public long cRo() {
        return BinkBridgeJNI.BINKSUMMARY_FileFrameRate_get(this.swigCPtr, this);
    }

    /* renamed from: jQ */
    public void mo8164jQ(long j) {
        BinkBridgeJNI.BINKSUMMARY_FileFrameRateDiv_set(this.swigCPtr, this, j);
    }

    public long cRp() {
        return BinkBridgeJNI.BINKSUMMARY_FileFrameRateDiv_get(this.swigCPtr, this);
    }

    /* renamed from: X */
    public void mo8131X(long j) {
        BinkBridgeJNI.BINKSUMMARY_FrameRate_set(this.swigCPtr, this, j);
    }

    /* renamed from: sS */
    public long mo8189sS() {
        return BinkBridgeJNI.BINKSUMMARY_FrameRate_get(this.swigCPtr, this);
    }

    /* renamed from: Y */
    public void mo8132Y(long j) {
        BinkBridgeJNI.BINKSUMMARY_FrameRateDiv_set(this.swigCPtr, this, j);
    }

    /* renamed from: sT */
    public long mo8190sT() {
        return BinkBridgeJNI.BINKSUMMARY_FrameRateDiv_get(this.swigCPtr, this);
    }

    /* renamed from: jR */
    public void mo8165jR(long j) {
        BinkBridgeJNI.BINKSUMMARY_TotalOpenTime_set(this.swigCPtr, this, j);
    }

    public long cRq() {
        return BinkBridgeJNI.BINKSUMMARY_TotalOpenTime_get(this.swigCPtr, this);
    }

    /* renamed from: jS */
    public void mo8166jS(long j) {
        BinkBridgeJNI.BINKSUMMARY_TotalFrames_set(this.swigCPtr, this, j);
    }

    public long cRr() {
        return BinkBridgeJNI.BINKSUMMARY_TotalFrames_get(this.swigCPtr, this);
    }

    /* renamed from: jT */
    public void mo8167jT(long j) {
        BinkBridgeJNI.BINKSUMMARY_TotalPlayedFrames_set(this.swigCPtr, this, j);
    }

    public long cRs() {
        return BinkBridgeJNI.BINKSUMMARY_TotalPlayedFrames_get(this.swigCPtr, this);
    }

    /* renamed from: jU */
    public void mo8168jU(long j) {
        BinkBridgeJNI.BINKSUMMARY_SkippedFrames_set(this.swigCPtr, this, j);
    }

    public long cRt() {
        return BinkBridgeJNI.BINKSUMMARY_SkippedFrames_get(this.swigCPtr, this);
    }

    /* renamed from: jV */
    public void mo8169jV(long j) {
        BinkBridgeJNI.BINKSUMMARY_SkippedBlits_set(this.swigCPtr, this, j);
    }

    public long cRu() {
        return BinkBridgeJNI.BINKSUMMARY_SkippedBlits_get(this.swigCPtr, this);
    }

    /* renamed from: jW */
    public void mo8170jW(long j) {
        BinkBridgeJNI.BINKSUMMARY_SoundSkips_set(this.swigCPtr, this, j);
    }

    public long cRv() {
        return BinkBridgeJNI.BINKSUMMARY_SoundSkips_get(this.swigCPtr, this);
    }

    /* renamed from: jX */
    public void mo8171jX(long j) {
        BinkBridgeJNI.BINKSUMMARY_TotalBlitTime_set(this.swigCPtr, this, j);
    }

    public long cRw() {
        return BinkBridgeJNI.BINKSUMMARY_TotalBlitTime_get(this.swigCPtr, this);
    }

    /* renamed from: jY */
    public void mo8172jY(long j) {
        BinkBridgeJNI.BINKSUMMARY_TotalReadTime_set(this.swigCPtr, this, j);
    }

    public long cRx() {
        return BinkBridgeJNI.BINKSUMMARY_TotalReadTime_get(this.swigCPtr, this);
    }

    /* renamed from: jZ */
    public void mo8173jZ(long j) {
        BinkBridgeJNI.BINKSUMMARY_TotalVideoDecompTime_set(this.swigCPtr, this, j);
    }

    public long cRy() {
        return BinkBridgeJNI.BINKSUMMARY_TotalVideoDecompTime_get(this.swigCPtr, this);
    }

    /* renamed from: ka */
    public void mo8174ka(long j) {
        BinkBridgeJNI.BINKSUMMARY_TotalAudioDecompTime_set(this.swigCPtr, this, j);
    }

    public long cRz() {
        return BinkBridgeJNI.BINKSUMMARY_TotalAudioDecompTime_get(this.swigCPtr, this);
    }

    /* renamed from: kb */
    public void mo8175kb(long j) {
        BinkBridgeJNI.BINKSUMMARY_TotalIdleReadTime_set(this.swigCPtr, this, j);
    }

    public long cRA() {
        return BinkBridgeJNI.BINKSUMMARY_TotalIdleReadTime_get(this.swigCPtr, this);
    }

    /* renamed from: kc */
    public void mo8176kc(long j) {
        BinkBridgeJNI.BINKSUMMARY_TotalBackReadTime_set(this.swigCPtr, this, j);
    }

    public long cRB() {
        return BinkBridgeJNI.BINKSUMMARY_TotalBackReadTime_get(this.swigCPtr, this);
    }

    /* renamed from: kd */
    public void mo8177kd(long j) {
        BinkBridgeJNI.BINKSUMMARY_TotalReadSpeed_set(this.swigCPtr, this, j);
    }

    public long cRC() {
        return BinkBridgeJNI.BINKSUMMARY_TotalReadSpeed_get(this.swigCPtr, this);
    }

    /* renamed from: ke */
    public void mo8178ke(long j) {
        BinkBridgeJNI.BINKSUMMARY_SlowestFrameTime_set(this.swigCPtr, this, j);
    }

    public long cRD() {
        return BinkBridgeJNI.BINKSUMMARY_SlowestFrameTime_get(this.swigCPtr, this);
    }

    /* renamed from: kf */
    public void mo8179kf(long j) {
        BinkBridgeJNI.BINKSUMMARY_Slowest2FrameTime_set(this.swigCPtr, this, j);
    }

    public long cRE() {
        return BinkBridgeJNI.BINKSUMMARY_Slowest2FrameTime_get(this.swigCPtr, this);
    }

    /* renamed from: kg */
    public void mo8180kg(long j) {
        BinkBridgeJNI.BINKSUMMARY_SlowestFrameNum_set(this.swigCPtr, this, j);
    }

    public long cRF() {
        return BinkBridgeJNI.BINKSUMMARY_SlowestFrameNum_get(this.swigCPtr, this);
    }

    /* renamed from: kh */
    public void mo8181kh(long j) {
        BinkBridgeJNI.BINKSUMMARY_Slowest2FrameNum_set(this.swigCPtr, this, j);
    }

    public long cRG() {
        return BinkBridgeJNI.BINKSUMMARY_Slowest2FrameNum_get(this.swigCPtr, this);
    }

    /* renamed from: ki */
    public void mo8182ki(long j) {
        BinkBridgeJNI.BINKSUMMARY_AverageDataRate_set(this.swigCPtr, this, j);
    }

    public long cRH() {
        return BinkBridgeJNI.BINKSUMMARY_AverageDataRate_get(this.swigCPtr, this);
    }

    /* renamed from: kj */
    public void mo8183kj(long j) {
        BinkBridgeJNI.BINKSUMMARY_AverageFrameSize_set(this.swigCPtr, this, j);
    }

    public long cRI() {
        return BinkBridgeJNI.BINKSUMMARY_AverageFrameSize_get(this.swigCPtr, this);
    }

    /* renamed from: kk */
    public void mo8184kk(long j) {
        BinkBridgeJNI.BINKSUMMARY_HighestMemAmount_set(this.swigCPtr, this, j);
    }

    public long cRJ() {
        return BinkBridgeJNI.BINKSUMMARY_HighestMemAmount_get(this.swigCPtr, this);
    }

    /* renamed from: kl */
    public void mo8185kl(long j) {
        BinkBridgeJNI.BINKSUMMARY_TotalIOMemory_set(this.swigCPtr, this, j);
    }

    public long cRK() {
        return BinkBridgeJNI.BINKSUMMARY_TotalIOMemory_get(this.swigCPtr, this);
    }

    /* renamed from: km */
    public void mo8186km(long j) {
        BinkBridgeJNI.BINKSUMMARY_HighestIOUsed_set(this.swigCPtr, this, j);
    }

    public long cRL() {
        return BinkBridgeJNI.BINKSUMMARY_HighestIOUsed_get(this.swigCPtr, this);
    }

    /* renamed from: aj */
    public void mo8133aj(long j) {
        BinkBridgeJNI.BINKSUMMARY_Highest1SecRate_set(this.swigCPtr, this, j);
    }

    /* renamed from: tl */
    public long mo8191tl() {
        return BinkBridgeJNI.BINKSUMMARY_Highest1SecRate_get(this.swigCPtr, this);
    }

    /* renamed from: ak */
    public void mo8134ak(long j) {
        BinkBridgeJNI.BINKSUMMARY_Highest1SecFrame_set(this.swigCPtr, this, j);
    }

    /* renamed from: tm */
    public long mo8192tm() {
        return BinkBridgeJNI.BINKSUMMARY_Highest1SecFrame_get(this.swigCPtr, this);
    }
}
