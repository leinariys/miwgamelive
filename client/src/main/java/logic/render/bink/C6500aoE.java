package logic.render.bink;

import utaikodom.render.bink.BinkBridgeJNI;

/* renamed from: a.aoE  reason: case insensitive filesystem */
/* compiled from: a */
public class C6500aoE {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6500aoE(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6500aoE() {
        this(BinkBridgeJNI.new_BINKTRACK(), true);
    }

    /* renamed from: b */
    public static long m24354b(C6500aoE aoe) {
        if (aoe == null) {
            return 0;
        }
        return aoe.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            BinkBridgeJNI.delete_BINKTRACK(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: ii */
    public void mo15139ii(long j) {
        BinkBridgeJNI.BINKTRACK_Frequency_set(this.swigCPtr, this, j);
    }

    public long cnS() {
        return BinkBridgeJNI.BINKTRACK_Frequency_get(this.swigCPtr, this);
    }

    /* renamed from: ij */
    public void mo15140ij(long j) {
        BinkBridgeJNI.BINKTRACK_Bits_set(this.swigCPtr, this, j);
    }

    public long cnT() {
        return BinkBridgeJNI.BINKTRACK_Bits_get(this.swigCPtr, this);
    }

    /* renamed from: ik */
    public void mo15141ik(long j) {
        BinkBridgeJNI.BINKTRACK_Channels_set(this.swigCPtr, this, j);
    }

    public long cnU() {
        return BinkBridgeJNI.BINKTRACK_Channels_get(this.swigCPtr, this);
    }

    /* renamed from: il */
    public void mo15142il(long j) {
        BinkBridgeJNI.BINKTRACK_MaxSize_set(this.swigCPtr, this, j);
    }

    public long cnV() {
        return BinkBridgeJNI.BINKTRACK_MaxSize_get(this.swigCPtr, this);
    }

    public C2427fN getBink() {
        long BINKTRACK_bink_get = BinkBridgeJNI.BINKTRACK_bink_get(this.swigCPtr, this);
        if (BINKTRACK_bink_get == 0) {
            return null;
        }
        return new C2427fN(BINKTRACK_bink_get, false);
    }

    public void setBink(C2427fN fNVar) {
        BinkBridgeJNI.BINKTRACK_bink_set(this.swigCPtr, this, C2427fN.m30833a(fNVar), fNVar);
    }

    /* renamed from: bC */
    public void mo15130bC(long j) {
        BinkBridgeJNI.BINKTRACK_sndcomp_set(this.swigCPtr, this, j);
    }

    /* renamed from: DC */
    public long mo15129DC() {
        return BinkBridgeJNI.BINKTRACK_sndcomp_get(this.swigCPtr, this);
    }

    /* renamed from: th */
    public void mo15144th(int i) {
        BinkBridgeJNI.BINKTRACK_trackindex_set(this.swigCPtr, this, i);
    }

    public int cnW() {
        return BinkBridgeJNI.BINKTRACK_trackindex_get(this.swigCPtr, this);
    }
}
