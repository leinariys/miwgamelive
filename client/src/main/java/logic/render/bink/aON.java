package logic.render.bink;

import utaikodom.render.bink.BinkBridgeJNI;

/* renamed from: a.aON */
/* compiled from: a */
public class aON {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aON(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aON() {
        this(BinkBridgeJNI.new_BINKHDR(), true);
    }

    /* renamed from: a */
    public static long m16821a(aON aon) {
        if (aon == null) {
            return 0;
        }
        return aon.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            BinkBridgeJNI.delete_BINKHDR(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: lH */
    public void mo10573lH(long j) {
        BinkBridgeJNI.BINKHDR_Marker_set(this.swigCPtr, this, j);
    }

    public long dnQ() {
        return BinkBridgeJNI.BINKHDR_Marker_get(this.swigCPtr, this);
    }

    public long getSize() {
        return BinkBridgeJNI.BINKHDR_Size_get(this.swigCPtr, this);
    }

    public void setSize(long j) {
        BinkBridgeJNI.BINKHDR_Size_set(this.swigCPtr, this, j);
    }

    /* renamed from: U */
    public void mo10562U(long j) {
        BinkBridgeJNI.BINKHDR_Frames_set(this.swigCPtr, this, j);
    }

    /* renamed from: sP */
    public long mo10577sP() {
        return BinkBridgeJNI.BINKHDR_Frames_get(this.swigCPtr, this);
    }

    /* renamed from: ah */
    public void mo10565ah(long j) {
        BinkBridgeJNI.BINKHDR_LargestFrameSize_set(this.swigCPtr, this, j);
    }

    /* renamed from: tj */
    public long mo10582tj() {
        return BinkBridgeJNI.BINKHDR_LargestFrameSize_get(this.swigCPtr, this);
    }

    /* renamed from: ai */
    public void mo10566ai(long j) {
        BinkBridgeJNI.BINKHDR_InternalFrames_set(this.swigCPtr, this, j);
    }

    /* renamed from: tk */
    public long mo10583tk() {
        return BinkBridgeJNI.BINKHDR_InternalFrames_get(this.swigCPtr, this);
    }

    /* renamed from: S */
    public void mo10560S(long j) {
        BinkBridgeJNI.BINKHDR_Width_set(this.swigCPtr, this, j);
    }

    /* renamed from: sN */
    public long mo10575sN() {
        return BinkBridgeJNI.BINKHDR_Width_get(this.swigCPtr, this);
    }

    /* renamed from: T */
    public void mo10561T(long j) {
        BinkBridgeJNI.BINKHDR_Height_set(this.swigCPtr, this, j);
    }

    /* renamed from: sO */
    public long mo10576sO() {
        return BinkBridgeJNI.BINKHDR_Height_get(this.swigCPtr, this);
    }

    /* renamed from: X */
    public void mo10563X(long j) {
        BinkBridgeJNI.BINKHDR_FrameRate_set(this.swigCPtr, this, j);
    }

    /* renamed from: sS */
    public long mo10578sS() {
        return BinkBridgeJNI.BINKHDR_FrameRate_get(this.swigCPtr, this);
    }

    /* renamed from: Y */
    public void mo10564Y(long j) {
        BinkBridgeJNI.BINKHDR_FrameRateDiv_set(this.swigCPtr, this, j);
    }

    /* renamed from: sT */
    public long mo10579sT() {
        return BinkBridgeJNI.BINKHDR_FrameRateDiv_get(this.swigCPtr, this);
    }

    public long getFlags() {
        return BinkBridgeJNI.BINKHDR_Flags_get(this.swigCPtr, this);
    }

    public void setFlags(long j) {
        BinkBridgeJNI.BINKHDR_Flags_set(this.swigCPtr, this, j);
    }

    /* renamed from: lI */
    public void mo10574lI(long j) {
        BinkBridgeJNI.BINKHDR_NumTracks_set(this.swigCPtr, this, j);
    }

    public long dnR() {
        return BinkBridgeJNI.BINKHDR_NumTracks_get(this.swigCPtr, this);
    }
}
