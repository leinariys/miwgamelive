package logic.render.bink;

/* renamed from: a.Ep */
/* compiled from: a */
public class C0362Ep {
    private long swigCPtr;

    public C0362Ep(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C0362Ep() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m3030b(C0362Ep ep) {
        if (ep == null) {
            return 0;
        }
        return ep.swigCPtr;
    }
}
