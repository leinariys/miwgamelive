package logic.render.bink;

import utaikodom.render.bink.BinkBridgeJNI;

/* renamed from: a.agE  reason: case insensitive filesystem */
/* compiled from: a */
public class C6084agE {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6084agE(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6084agE() {
        this(BinkBridgeJNI.new_BINKREALTIME(), true);
    }

    /* renamed from: a */
    public static long m21940a(C6084agE age) {
        if (age == null) {
            return 0;
        }
        return age.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            BinkBridgeJNI.delete_BINKREALTIME(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: V */
    public void mo13319V(long j) {
        BinkBridgeJNI.BINKREALTIME_FrameNum_set(this.swigCPtr, this, j);
    }

    /* renamed from: sQ */
    public long mo13345sQ() {
        return BinkBridgeJNI.BINKREALTIME_FrameNum_get(this.swigCPtr, this);
    }

    /* renamed from: X */
    public void mo13320X(long j) {
        BinkBridgeJNI.BINKREALTIME_FrameRate_set(this.swigCPtr, this, j);
    }

    /* renamed from: sS */
    public long mo13346sS() {
        return BinkBridgeJNI.BINKREALTIME_FrameRate_get(this.swigCPtr, this);
    }

    /* renamed from: Y */
    public void mo13321Y(long j) {
        BinkBridgeJNI.BINKREALTIME_FrameRateDiv_set(this.swigCPtr, this, j);
    }

    /* renamed from: sT */
    public long mo13347sT() {
        return BinkBridgeJNI.BINKREALTIME_FrameRateDiv_get(this.swigCPtr, this);
    }

    /* renamed from: U */
    public void mo13318U(long j) {
        BinkBridgeJNI.BINKREALTIME_Frames_set(this.swigCPtr, this, j);
    }

    /* renamed from: sP */
    public long mo13344sP() {
        return BinkBridgeJNI.BINKREALTIME_Frames_get(this.swigCPtr, this);
    }

    /* renamed from: hh */
    public void mo13334hh(long j) {
        BinkBridgeJNI.BINKREALTIME_FramesTime_set(this.swigCPtr, this, j);
    }

    public long bWX() {
        return BinkBridgeJNI.BINKREALTIME_FramesTime_get(this.swigCPtr, this);
    }

    /* renamed from: hi */
    public void mo13335hi(long j) {
        BinkBridgeJNI.BINKREALTIME_FramesVideoDecompTime_set(this.swigCPtr, this, j);
    }

    public long bWY() {
        return BinkBridgeJNI.BINKREALTIME_FramesVideoDecompTime_get(this.swigCPtr, this);
    }

    /* renamed from: hj */
    public void mo13336hj(long j) {
        BinkBridgeJNI.BINKREALTIME_FramesAudioDecompTime_set(this.swigCPtr, this, j);
    }

    public long bWZ() {
        return BinkBridgeJNI.BINKREALTIME_FramesAudioDecompTime_get(this.swigCPtr, this);
    }

    /* renamed from: hk */
    public void mo13337hk(long j) {
        BinkBridgeJNI.BINKREALTIME_FramesReadTime_set(this.swigCPtr, this, j);
    }

    public long bXa() {
        return BinkBridgeJNI.BINKREALTIME_FramesReadTime_get(this.swigCPtr, this);
    }

    /* renamed from: hl */
    public void mo13338hl(long j) {
        BinkBridgeJNI.BINKREALTIME_FramesIdleReadTime_set(this.swigCPtr, this, j);
    }

    public long bXb() {
        return BinkBridgeJNI.BINKREALTIME_FramesIdleReadTime_get(this.swigCPtr, this);
    }

    /* renamed from: hm */
    public void mo13339hm(long j) {
        BinkBridgeJNI.BINKREALTIME_FramesThreadReadTime_set(this.swigCPtr, this, j);
    }

    public long bXc() {
        return BinkBridgeJNI.BINKREALTIME_FramesThreadReadTime_get(this.swigCPtr, this);
    }

    /* renamed from: hn */
    public void mo13340hn(long j) {
        BinkBridgeJNI.BINKREALTIME_FramesBlitTime_set(this.swigCPtr, this, j);
    }

    public long bXd() {
        return BinkBridgeJNI.BINKREALTIME_FramesBlitTime_get(this.swigCPtr, this);
    }

    /* renamed from: ho */
    public void mo13341ho(long j) {
        BinkBridgeJNI.BINKREALTIME_ReadBufferSize_set(this.swigCPtr, this, j);
    }

    public long bXe() {
        return BinkBridgeJNI.BINKREALTIME_ReadBufferSize_get(this.swigCPtr, this);
    }

    /* renamed from: hp */
    public void mo13342hp(long j) {
        BinkBridgeJNI.BINKREALTIME_ReadBufferUsed_set(this.swigCPtr, this, j);
    }

    public long bXf() {
        return BinkBridgeJNI.BINKREALTIME_ReadBufferUsed_get(this.swigCPtr, this);
    }

    /* renamed from: hq */
    public void mo13343hq(long j) {
        BinkBridgeJNI.BINKREALTIME_FramesDataRate_set(this.swigCPtr, this, j);
    }

    public long bXg() {
        return BinkBridgeJNI.BINKREALTIME_FramesDataRate_get(this.swigCPtr, this);
    }
}
