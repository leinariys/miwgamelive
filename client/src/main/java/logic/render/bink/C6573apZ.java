package logic.render.bink;

/* renamed from: a.apZ  reason: case insensitive filesystem */
/* compiled from: a */
public class C6573apZ {
    private long swigCPtr;

    public C6573apZ(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6573apZ() {
        this.swigCPtr = 0;
    }

    /* renamed from: d */
    public static long m24928d(C6573apZ apz) {
        if (apz == null) {
            return 0;
        }
        return apz.swigCPtr;
    }
}
