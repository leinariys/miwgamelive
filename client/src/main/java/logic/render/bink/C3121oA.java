package logic.render.bink;

import utaikodom.render.bink.BinkBridgeJNI;

/* renamed from: a.oA */
/* compiled from: a */
public class C3121oA {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3121oA(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3121oA() {
        this(BinkBridgeJNI.new_BINKFRAMEPLANESET(), true);
    }

    /* renamed from: b */
    public static long m36587b(C3121oA oAVar) {
        if (oAVar == null) {
            return 0;
        }
        return oAVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            BinkBridgeJNI.delete_BINKFRAMEPLANESET(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo20931b(C2838kr krVar) {
        BinkBridgeJNI.BINKFRAMEPLANESET_YPlane_set(this.swigCPtr, this, C2838kr.m34578a(krVar), krVar);
    }

    /* renamed from: Um */
    public C2838kr mo20927Um() {
        long BINKFRAMEPLANESET_YPlane_get = BinkBridgeJNI.BINKFRAMEPLANESET_YPlane_get(this.swigCPtr, this);
        if (BINKFRAMEPLANESET_YPlane_get == 0) {
            return null;
        }
        return new C2838kr(BINKFRAMEPLANESET_YPlane_get, false);
    }

    /* renamed from: c */
    public void mo20932c(C2838kr krVar) {
        BinkBridgeJNI.BINKFRAMEPLANESET_cRPlane_set(this.swigCPtr, this, C2838kr.m34578a(krVar), krVar);
    }

    /* renamed from: Un */
    public C2838kr mo20928Un() {
        long BINKFRAMEPLANESET_cRPlane_get = BinkBridgeJNI.BINKFRAMEPLANESET_cRPlane_get(this.swigCPtr, this);
        if (BINKFRAMEPLANESET_cRPlane_get == 0) {
            return null;
        }
        return new C2838kr(BINKFRAMEPLANESET_cRPlane_get, false);
    }

    /* renamed from: d */
    public void mo20933d(C2838kr krVar) {
        BinkBridgeJNI.BINKFRAMEPLANESET_cBPlane_set(this.swigCPtr, this, C2838kr.m34578a(krVar), krVar);
    }

    /* renamed from: Uo */
    public C2838kr mo20929Uo() {
        long BINKFRAMEPLANESET_cBPlane_get = BinkBridgeJNI.BINKFRAMEPLANESET_cBPlane_get(this.swigCPtr, this);
        if (BINKFRAMEPLANESET_cBPlane_get == 0) {
            return null;
        }
        return new C2838kr(BINKFRAMEPLANESET_cBPlane_get, false);
    }

    /* renamed from: e */
    public void mo20935e(C2838kr krVar) {
        BinkBridgeJNI.BINKFRAMEPLANESET_APlane_set(this.swigCPtr, this, C2838kr.m34578a(krVar), krVar);
    }

    /* renamed from: Up */
    public C2838kr mo20930Up() {
        long BINKFRAMEPLANESET_APlane_get = BinkBridgeJNI.BINKFRAMEPLANESET_APlane_get(this.swigCPtr, this);
        if (BINKFRAMEPLANESET_APlane_get == 0) {
            return null;
        }
        return new C2838kr(BINKFRAMEPLANESET_APlane_get, false);
    }
}
