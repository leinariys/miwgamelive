package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aBV */
/* compiled from: a */
public class aBV {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aBV(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aBV() {
        this(grannyJNI.new_granny_material_map(), true);
    }

    /* renamed from: b */
    public static long m12973b(aBV abv) {
        if (abv == null) {
            return 0;
        }
        return abv.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_material_map(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: lq */
    public void mo7926lq(String str) {
        grannyJNI.granny_material_map_Usage_set(this.swigCPtr, str);
    }

    public String ayT() {
        return grannyJNI.granny_material_map_Usage_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo7922b(C0201CS cs) {
        grannyJNI.granny_material_map_Material_set(this.swigCPtr, C0201CS.m1752a(cs));
    }

    public C0201CS cDE() {
        long granny_material_map_Material_get = grannyJNI.granny_material_map_Material_get(this.swigCPtr);
        if (granny_material_map_Material_get == 0) {
            return null;
        }
        return new C0201CS(granny_material_map_Material_get, false);
    }

    /* renamed from: wR */
    public aBV mo7927wR(int i) {
        long granny_material_map_get = grannyJNI.granny_material_map_get(this.swigCPtr, i);
        if (granny_material_map_get == 0) {
            return null;
        }
        return new aBV(granny_material_map_get, false);
    }
}
