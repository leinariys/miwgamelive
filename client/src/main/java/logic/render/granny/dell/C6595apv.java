package logic.render.granny.dell;

/* renamed from: a.apv  reason: case insensitive filesystem */
/* compiled from: a */
public class C6595apv {
    private long swigCPtr;

    public C6595apv(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6595apv() {
        this.swigCPtr = 0;
    }

    /* renamed from: g */
    public static long m25028g(C6595apv apv) {
        if (apv == null) {
            return 0;
        }
        return apv.swigCPtr;
    }
}
