package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aIE */
/* compiled from: a */
public class aIE {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aIE(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aIE() {
        this(grannyJNI.new_granny_fixed_allocator(), true);
    }

    /* renamed from: c */
    public static long m15360c(aIE aie) {
        if (aie == null) {
            return 0;
        }
        return aie.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_fixed_allocator(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: gn */
    public void mo9333gn(int i) {
        grannyJNI.granny_fixed_allocator_UnitSize_set(this.swigCPtr, i);
    }

    public int axS() {
        return grannyJNI.granny_fixed_allocator_UnitSize_get(this.swigCPtr);
    }

    /* renamed from: go */
    public void mo9334go(int i) {
        grannyJNI.granny_fixed_allocator_UnitsPerBlock_set(this.swigCPtr, i);
    }

    public int axT() {
        return grannyJNI.granny_fixed_allocator_UnitsPerBlock_get(this.swigCPtr);
    }

    /* renamed from: d */
    public void mo9329d(C6277ajp ajp) {
        grannyJNI.granny_fixed_allocator_Sentinel_set(this.swigCPtr, C6277ajp.m23053a(ajp));
    }

    public C6277ajp dfe() {
        long granny_fixed_allocator_Sentinel_get = grannyJNI.granny_fixed_allocator_Sentinel_get(this.swigCPtr);
        if (granny_fixed_allocator_Sentinel_get == 0) {
            return null;
        }
        return new C6277ajp(granny_fixed_allocator_Sentinel_get, false);
    }

    /* renamed from: yo */
    public aIE mo9335yo(int i) {
        long granny_fixed_allocator_get = grannyJNI.granny_fixed_allocator_get(this.swigCPtr, i);
        if (granny_fixed_allocator_get == 0) {
            return null;
        }
        return new aIE(granny_fixed_allocator_get, false);
    }
}
