package logic.render.granny.dell;

import logic.render.granny.C5515aMh;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.Gb */
/* compiled from: a */
public class C0476Gb {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0476Gb(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0476Gb() {
        this(grannyJNI.new_granny_bound_transform_track(), true);
    }

    /* renamed from: a */
    public static long m3443a(C0476Gb gb) {
        if (gb == null) {
            return 0;
        }
        return gb.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_bound_transform_track(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: k */
    public void mo2379k(short s) {
        grannyJNI.granny_bound_transform_track_SourceTrackIndex_set(this.swigCPtr, s);
    }

    public short aQy() {
        return grannyJNI.granny_bound_transform_track_SourceTrackIndex_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo2373b(char c) {
        grannyJNI.granny_bound_transform_track_QuaternionMode_set(this.swigCPtr, c);
    }

    public char aQz() {
        return grannyJNI.granny_bound_transform_track_QuaternionMode_get(this.swigCPtr);
    }

    /* renamed from: c */
    public void mo2374c(char c) {
        grannyJNI.granny_bound_transform_track_Flags_set(this.swigCPtr, c);
    }

    public char aQA() {
        return grannyJNI.granny_bound_transform_track_Flags_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo2363a(C0777LF lf) {
        grannyJNI.granny_bound_transform_track_SourceTrack_set(this.swigCPtr, C0777LF.m6656f(lf));
    }

    public C0777LF aQB() {
        long granny_bound_transform_track_SourceTrack_get = grannyJNI.granny_bound_transform_track_SourceTrack_get(this.swigCPtr);
        if (granny_bound_transform_track_SourceTrack_get == 0) {
            return null;
        }
        return new C0777LF(granny_bound_transform_track_SourceTrack_get, false);
    }

    /* renamed from: a */
    public void mo2364a(C5316aEq aeq) {
        grannyJNI.granny_bound_transform_track_Sampler_set(this.swigCPtr, C5316aEq.m14387b(aeq));
    }

    public C5316aEq aQC() {
        long granny_bound_transform_track_Sampler_get = grannyJNI.granny_bound_transform_track_Sampler_get(this.swigCPtr);
        if (granny_bound_transform_track_Sampler_get == 0) {
            return null;
        }
        return new C5316aEq(granny_bound_transform_track_Sampler_get, false);
    }

    /* renamed from: fl */
    public void mo2377fl(float f) {
        grannyJNI.granny_bound_transform_track_LODError_set(this.swigCPtr, f);
    }

    public float aQD() {
        return grannyJNI.granny_bound_transform_track_LODError_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo2365a(C5515aMh amh) {
        grannyJNI.granny_bound_transform_track_LODTransform_set(this.swigCPtr, C5515aMh.m16438f(amh));
    }

    public C5515aMh aQE() {
        long granny_bound_transform_track_LODTransform_get = grannyJNI.granny_bound_transform_track_LODTransform_get(this.swigCPtr);
        if (granny_bound_transform_track_LODTransform_get == 0) {
            return null;
        }
        return new C5515aMh(granny_bound_transform_track_LODTransform_get, false);
    }

    /* renamed from: hp */
    public C0476Gb mo2378hp(int i) {
        long granny_bound_transform_track_get = grannyJNI.granny_bound_transform_track_get(this.swigCPtr, i);
        if (granny_bound_transform_track_get == 0) {
            return null;
        }
        return new C0476Gb(granny_bound_transform_track_get, false);
    }
}
