package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aMB */
/* compiled from: a */
public class aMB {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aMB(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aMB() {
        this(grannyJNI.new_granny_pn33_vertex(), true);
    }

    /* renamed from: a */
    public static long m16308a(aMB amb) {
        if (amb == null) {
            return 0;
        }
        return amb.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pn33_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo9969b(C3159oW oWVar) {
        grannyJNI.granny_pn33_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo9974s() {
        long granny_pn33_vertex_Position_get = grannyJNI.granny_pn33_vertex_Position_get(this.swigCPtr);
        if (granny_pn33_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pn33_vertex_Position_get, false);
    }

    /* renamed from: h */
    public void mo9973h(C3159oW oWVar) {
        grannyJNI.granny_pn33_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo9970cR() {
        long granny_pn33_vertex_Normal_get = grannyJNI.granny_pn33_vertex_Normal_get(this.swigCPtr);
        if (granny_pn33_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pn33_vertex_Normal_get, false);
    }

    /* renamed from: yK */
    public aMB mo9975yK(int i) {
        long granny_pn33_vertex_get = grannyJNI.granny_pn33_vertex_get(this.swigCPtr, i);
        if (granny_pn33_vertex_get == 0) {
            return null;
        }
        return new aMB(granny_pn33_vertex_get, false);
    }
}
