package logic.render.granny.dell;

import logic.render.granny.aSM;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.wi */
/* compiled from: a */
public class C3914wi {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3914wi(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3914wi() {
        this(grannyJNI.new_granny_curve2(), true);
    }

    /* renamed from: a */
    public static long m40719a(C3914wi wiVar) {
        if (wiVar == null) {
            return 0;
        }
        return wiVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve2(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo22801b(aSM asm) {
        grannyJNI.granny_curve2_CurveData_set(this.swigCPtr, aSM.m18172c(asm));
    }

    public aSM ame() {
        long granny_curve2_CurveData_get = grannyJNI.granny_curve2_CurveData_get(this.swigCPtr);
        if (granny_curve2_CurveData_get == 0) {
            return null;
        }
        return new aSM(granny_curve2_CurveData_get, false);
    }

    /* renamed from: fs */
    public C3914wi mo22804fs(int i) {
        long granny_curve2_get = grannyJNI.granny_curve2_get(this.swigCPtr, i);
        if (granny_curve2_get == 0) {
            return null;
        }
        return new C3914wi(granny_curve2_get, false);
    }
}
