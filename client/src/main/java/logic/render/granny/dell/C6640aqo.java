package logic.render.granny.dell;

import logic.render.granny.C1731Za;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aqo  reason: case insensitive filesystem */
/* compiled from: a */
public class C6640aqo {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6640aqo(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6640aqo() {
        this(grannyJNI.new_granny_compress_curve_parameters(), true);
    }

    /* renamed from: a */
    public static long m25217a(C6640aqo aqo) {
        if (aqo == null) {
            return 0;
        }
        return aqo.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_compress_curve_parameters(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: tE */
    public void mo15659tE(int i) {
        grannyJNI.granny_compress_curve_parameters_DesiredDegree_set(this.swigCPtr, i);
    }

    public int cra() {
        return grannyJNI.granny_compress_curve_parameters_DesiredDegree_get(this.swigCPtr);
    }

    /* renamed from: gc */
    public void mo15654gc(boolean z) {
        grannyJNI.granny_compress_curve_parameters_AllowDegreeReduction_set(this.swigCPtr, z);
    }

    public boolean crb() {
        return grannyJNI.granny_compress_curve_parameters_AllowDegreeReduction_get(this.swigCPtr);
    }

    /* renamed from: gd */
    public void mo15655gd(boolean z) {
        grannyJNI.m45852x324cbe8(this.swigCPtr, z);
    }

    public boolean crc() {
        return grannyJNI.m45851x3249edc(this.swigCPtr);
    }

    /* renamed from: ks */
    public void mo15656ks(float f) {
        grannyJNI.granny_compress_curve_parameters_ErrorTolerance_set(this.swigCPtr, f);
    }

    public float crd() {
        return grannyJNI.granny_compress_curve_parameters_ErrorTolerance_get(this.swigCPtr);
    }

    /* renamed from: kt */
    public void mo15657kt(float f) {
        grannyJNI.granny_compress_curve_parameters_C0Threshold_set(this.swigCPtr, f);
    }

    public float cre() {
        return grannyJNI.granny_compress_curve_parameters_C0Threshold_get(this.swigCPtr);
    }

    /* renamed from: ku */
    public void mo15658ku(float f) {
        grannyJNI.granny_compress_curve_parameters_C1Threshold_set(this.swigCPtr, f);
    }

    public float crf() {
        return grannyJNI.granny_compress_curve_parameters_C1Threshold_get(this.swigCPtr);
    }

    /* renamed from: d */
    public void mo15651d(C3339qT qTVar) {
        grannyJNI.granny_compress_curve_parameters_PossibleCompressionTypes_set(this.swigCPtr, C3339qT.m37523a(qTVar));
    }

    public C3339qT crg() {
        long granny_compress_curve_parameters_PossibleCompressionTypes_get = grannyJNI.granny_compress_curve_parameters_PossibleCompressionTypes_get(this.swigCPtr);
        if (granny_compress_curve_parameters_PossibleCompressionTypes_get == 0) {
            return null;
        }
        return new C3339qT(granny_compress_curve_parameters_PossibleCompressionTypes_get, false);
    }

    /* renamed from: tF */
    public void mo15660tF(int i) {
        grannyJNI.m45854x3e3a774a(this.swigCPtr, i);
    }

    public int crh() {
        return grannyJNI.m45853x3e3a4a3e(this.swigCPtr);
    }

    /* renamed from: bk */
    public void mo15638bk(C1731Za za) {
        grannyJNI.granny_compress_curve_parameters_ConstantCompressionType_set(this.swigCPtr, C1731Za.m12101bi(za));
    }

    public C1731Za cri() {
        long granny_compress_curve_parameters_ConstantCompressionType_get = grannyJNI.granny_compress_curve_parameters_ConstantCompressionType_get(this.swigCPtr);
        if (granny_compress_curve_parameters_ConstantCompressionType_get == 0) {
            return null;
        }
        return new C1731Za(granny_compress_curve_parameters_ConstantCompressionType_get, false);
    }

    /* renamed from: bl */
    public void mo15639bl(C1731Za za) {
        grannyJNI.granny_compress_curve_parameters_IdentityCompressionType_set(this.swigCPtr, C1731Za.m12101bi(za));
    }

    public C1731Za crj() {
        long granny_compress_curve_parameters_IdentityCompressionType_get = grannyJNI.granny_compress_curve_parameters_IdentityCompressionType_get(this.swigCPtr);
        if (granny_compress_curve_parameters_IdentityCompressionType_get == 0) {
            return null;
        }
        return new C1731Za(granny_compress_curve_parameters_IdentityCompressionType_get, false);
    }

    /* renamed from: N */
    public void mo15637N(C3159oW oWVar) {
        grannyJNI.granny_compress_curve_parameters_IdentityVector_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW crk() {
        long granny_compress_curve_parameters_IdentityVector_get = grannyJNI.granny_compress_curve_parameters_IdentityVector_get(this.swigCPtr);
        if (granny_compress_curve_parameters_IdentityVector_get == 0) {
            return null;
        }
        return new C3159oW(granny_compress_curve_parameters_IdentityVector_get, false);
    }

    /* renamed from: tG */
    public C6640aqo mo15661tG(int i) {
        long granny_compress_curve_parameters_get = grannyJNI.granny_compress_curve_parameters_get(this.swigCPtr, i);
        if (granny_compress_curve_parameters_get == 0) {
            return null;
        }
        return new C6640aqo(granny_compress_curve_parameters_get, false);
    }
}
