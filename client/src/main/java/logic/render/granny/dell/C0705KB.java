package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.KB */
/* compiled from: a */
public class C0705KB {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0705KB(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0705KB() {
        this(grannyJNI.new_granny_stat_hud_perf_point(), true);
    }

    /* renamed from: a */
    public static long m6261a(C0705KB kb) {
        if (kb == null) {
            return 0;
        }
        return kb.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_stat_hud_perf_point(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getName() {
        return grannyJNI.granny_stat_hud_perf_point_Name_get(this.swigCPtr);
    }

    public void setName(String str) {
        grannyJNI.granny_stat_hud_perf_point_Name_set(this.swigCPtr, str);
    }

    /* renamed from: lt */
    public void mo3473lt(int i) {
        grannyJNI.granny_stat_hud_perf_point_Count_set(this.swigCPtr, i);
    }

    public int getCount() {
        return grannyJNI.granny_stat_hud_perf_point_Count_get(this.swigCPtr);
    }

    /* renamed from: B */
    public void mo3465B(double d) {
        grannyJNI.granny_stat_hud_perf_point_TotalSeconds_set(this.swigCPtr, d);
    }

    public double bcS() {
        return grannyJNI.granny_stat_hud_perf_point_TotalSeconds_get(this.swigCPtr);
    }

    /* renamed from: C */
    public void mo3466C(double d) {
        grannyJNI.granny_stat_hud_perf_point_TotalCycles_set(this.swigCPtr, d);
    }

    public double bcT() {
        return grannyJNI.granny_stat_hud_perf_point_TotalCycles_get(this.swigCPtr);
    }

    /* renamed from: lu */
    public C0705KB mo3474lu(int i) {
        long granny_stat_hud_perf_point_get = grannyJNI.granny_stat_hud_perf_point_get(this.swigCPtr, i);
        if (granny_stat_hud_perf_point_get == 0) {
            return null;
        }
        return new C0705KB(granny_stat_hud_perf_point_get, false);
    }
}
