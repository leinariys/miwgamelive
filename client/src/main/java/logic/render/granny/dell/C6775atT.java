package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.atT  reason: case insensitive filesystem */
/* compiled from: a */
public class C6775atT {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6775atT(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6775atT() {
        this(grannyJNI.new_granny_pwngbt313332_vertex(), true);
    }

    /* renamed from: a */
    public static long m25883a(C6775atT att) {
        if (att == null) {
            return 0;
        }
        return att.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pwngbt313332_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo16077b(C3159oW oWVar) {
        grannyJNI.granny_pwngbt313332_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo16088s() {
        long granny_pwngbt313332_vertex_Position_get = grannyJNI.granny_pwngbt313332_vertex_Position_get(this.swigCPtr);
        if (granny_pwngbt313332_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngbt313332_vertex_Position_get, false);
    }

    /* renamed from: n */
    public void mo16087n(long j) {
        grannyJNI.granny_pwngbt313332_vertex_BoneIndex_set(this.swigCPtr, j);
    }

    /* renamed from: gC */
    public long mo16081gC() {
        return grannyJNI.granny_pwngbt313332_vertex_BoneIndex_get(this.swigCPtr);
    }

    /* renamed from: h */
    public void mo16083h(C3159oW oWVar) {
        grannyJNI.granny_pwngbt313332_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo16078cR() {
        long granny_pwngbt313332_vertex_Normal_get = grannyJNI.granny_pwngbt313332_vertex_Normal_get(this.swigCPtr);
        if (granny_pwngbt313332_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngbt313332_vertex_Normal_get, false);
    }

    /* renamed from: i */
    public void mo16084i(C3159oW oWVar) {
        grannyJNI.granny_pwngbt313332_vertex_Tangent_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: gD */
    public C3159oW mo16082gD() {
        long granny_pwngbt313332_vertex_Tangent_get = grannyJNI.granny_pwngbt313332_vertex_Tangent_get(this.swigCPtr);
        if (granny_pwngbt313332_vertex_Tangent_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngbt313332_vertex_Tangent_get, false);
    }

    /* renamed from: w */
    public void mo16090w(C3159oW oWVar) {
        grannyJNI.granny_pwngbt313332_vertex_Binormal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW amC() {
        long granny_pwngbt313332_vertex_Binormal_get = grannyJNI.granny_pwngbt313332_vertex_Binormal_get(this.swigCPtr);
        if (granny_pwngbt313332_vertex_Binormal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngbt313332_vertex_Binormal_get, false);
    }

    /* renamed from: j */
    public void mo16085j(C3159oW oWVar) {
        grannyJNI.granny_pwngbt313332_vertex_UV_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: mv */
    public C3159oW mo16086mv() {
        long granny_pwngbt313332_vertex_UV_get = grannyJNI.granny_pwngbt313332_vertex_UV_get(this.swigCPtr);
        if (granny_pwngbt313332_vertex_UV_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngbt313332_vertex_UV_get, false);
    }

    /* renamed from: uH */
    public C6775atT mo16089uH(int i) {
        long granny_pwngbt313332_vertex_get = grannyJNI.granny_pwngbt313332_vertex_get(this.swigCPtr, i);
        if (granny_pwngbt313332_vertex_get == 0) {
            return null;
        }
        return new C6775atT(granny_pwngbt313332_vertex_get, false);
    }
}
