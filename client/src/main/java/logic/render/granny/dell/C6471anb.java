package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.anb  reason: case insensitive filesystem */
/* compiled from: a */
public class C6471anb {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6471anb(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6471anb() {
        this(grannyJNI.new_granny_pwngt34332_vertex(), true);
    }

    /* renamed from: a */
    public static long m24206a(C6471anb anb) {
        if (anb == null) {
            return 0;
        }
        return anb.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pwngt34332_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo14990b(C3159oW oWVar) {
        grannyJNI.granny_pwngt34332_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo15001s() {
        long granny_pwngt34332_vertex_Position_get = grannyJNI.granny_pwngt34332_vertex_Position_get(this.swigCPtr);
        if (granny_pwngt34332_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngt34332_vertex_Position_get, false);
    }

    /* renamed from: a */
    public void mo14988a(C2382ef efVar) {
        grannyJNI.granny_pwngt34332_vertex_BoneWeights_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cP */
    public C2382ef mo14991cP() {
        long granny_pwngt34332_vertex_BoneWeights_get = grannyJNI.granny_pwngt34332_vertex_BoneWeights_get(this.swigCPtr);
        if (granny_pwngt34332_vertex_BoneWeights_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwngt34332_vertex_BoneWeights_get, false);
    }

    /* renamed from: b */
    public void mo14989b(C2382ef efVar) {
        grannyJNI.granny_pwngt34332_vertex_BoneIndices_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cQ */
    public C2382ef mo14992cQ() {
        long granny_pwngt34332_vertex_BoneIndices_get = grannyJNI.granny_pwngt34332_vertex_BoneIndices_get(this.swigCPtr);
        if (granny_pwngt34332_vertex_BoneIndices_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwngt34332_vertex_BoneIndices_get, false);
    }

    /* renamed from: h */
    public void mo14997h(C3159oW oWVar) {
        grannyJNI.granny_pwngt34332_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo14993cR() {
        long granny_pwngt34332_vertex_Normal_get = grannyJNI.granny_pwngt34332_vertex_Normal_get(this.swigCPtr);
        if (granny_pwngt34332_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngt34332_vertex_Normal_get, false);
    }

    /* renamed from: i */
    public void mo14998i(C3159oW oWVar) {
        grannyJNI.granny_pwngt34332_vertex_Tangent_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: gD */
    public C3159oW mo14996gD() {
        long granny_pwngt34332_vertex_Tangent_get = grannyJNI.granny_pwngt34332_vertex_Tangent_get(this.swigCPtr);
        if (granny_pwngt34332_vertex_Tangent_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngt34332_vertex_Tangent_get, false);
    }

    /* renamed from: j */
    public void mo14999j(C3159oW oWVar) {
        grannyJNI.granny_pwngt34332_vertex_UV_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: mv */
    public C3159oW mo15000mv() {
        long granny_pwngt34332_vertex_UV_get = grannyJNI.granny_pwngt34332_vertex_UV_get(this.swigCPtr);
        if (granny_pwngt34332_vertex_UV_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngt34332_vertex_UV_get, false);
    }

    /* renamed from: sF */
    public C6471anb mo15002sF(int i) {
        long granny_pwngt34332_vertex_get = grannyJNI.granny_pwngt34332_vertex_get(this.swigCPtr, i);
        if (granny_pwngt34332_vertex_get == 0) {
            return null;
        }
        return new C6471anb(granny_pwngt34332_vertex_get, false);
    }
}
