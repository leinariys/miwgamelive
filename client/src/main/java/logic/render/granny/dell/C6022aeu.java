package logic.render.granny.dell;

/* renamed from: a.aeu  reason: case insensitive filesystem */
/* compiled from: a */
public final class C6022aeu {
    public static final C6022aeu foq = new C6022aeu("GrannyDiffAsVectors");

    /* renamed from: for  reason: not valid java name */
    public static final C6022aeu f10384for = new C6022aeu("GrannyDiffAsQuaternions");
    public static final C6022aeu fos = new C6022aeu("GrannyManhattanMetric");
    private static C6022aeu[] fot = {foq, f10384for, fos};

    /* renamed from: pF */
    private static int f4435pF = 0;

    /* renamed from: pG */
    private final int f4436pG;

    /* renamed from: pH */
    private final String f4437pH;

    private C6022aeu(String str) {
        this.f4437pH = str;
        int i = f4435pF;
        f4435pF = i + 1;
        this.f4436pG = i;
    }

    private C6022aeu(String str, int i) {
        this.f4437pH = str;
        this.f4436pG = i;
        f4435pF = i + 1;
    }

    private C6022aeu(String str, C6022aeu aeu) {
        this.f4437pH = str;
        this.f4436pG = aeu.f4436pG;
        f4435pF = this.f4436pG + 1;
    }

    /* renamed from: qk */
    public static C6022aeu m21379qk(int i) {
        if (i < fot.length && i >= 0 && fot[i].f4436pG == i) {
            return fot[i];
        }
        for (int i2 = 0; i2 < fot.length; i2++) {
            if (fot[i2].f4436pG == i) {
                return fot[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C6022aeu.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f4436pG;
    }

    public String toString() {
        return this.f4437pH;
    }
}
