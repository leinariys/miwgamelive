package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.arq  reason: case insensitive filesystem */
/* compiled from: a */
public class C6694arq {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6694arq(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6694arq() {
        this(grannyJNI.new_granny_bspline_error(), true);
    }

    /* renamed from: a */
    public static long m25594a(C6694arq arq) {
        if (arq == null) {
            return 0;
        }
        return arq.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_bspline_error(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: tN */
    public void mo15877tN(int i) {
        grannyJNI.granny_bspline_error_MaximumSquaredErrorKnotIndex_set(this.swigCPtr, i);
    }

    public int csa() {
        return grannyJNI.granny_bspline_error_MaximumSquaredErrorKnotIndex_get(this.swigCPtr);
    }

    /* renamed from: tO */
    public void mo15878tO(int i) {
        grannyJNI.granny_bspline_error_MaximumSquaredErrorSampleIndex_set(this.swigCPtr, i);
    }

    public int csb() {
        return grannyJNI.granny_bspline_error_MaximumSquaredErrorSampleIndex_get(this.swigCPtr);
    }

    /* renamed from: ky */
    public void mo15875ky(float f) {
        grannyJNI.granny_bspline_error_MaximumSquaredError_set(this.swigCPtr, f);
    }

    public float csc() {
        return grannyJNI.granny_bspline_error_MaximumSquaredError_get(this.swigCPtr);
    }

    /* renamed from: kz */
    public void mo15876kz(float f) {
        grannyJNI.granny_bspline_error_AccumulatedSquaredError_set(this.swigCPtr, f);
    }

    public float csd() {
        return grannyJNI.granny_bspline_error_AccumulatedSquaredError_get(this.swigCPtr);
    }

    /* renamed from: tP */
    public C6694arq mo15879tP(int i) {
        long granny_bspline_error_get = grannyJNI.granny_bspline_error_get(this.swigCPtr, i);
        if (granny_bspline_error_get == 0) {
            return null;
        }
        return new C6694arq(granny_bspline_error_get, false);
    }
}
