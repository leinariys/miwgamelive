package logic.render.granny.dell;

/* renamed from: a.aDo  reason: case insensitive filesystem */
/* compiled from: a */
public final class C5288aDo {
    public static final C5288aDo hAV = new C5288aDo("GrannyExtractTrackMaskResult_AllDataPresent");
    public static final C5288aDo hAW = new C5288aDo("GrannyExtractTrackMaskResult_PartialDataPresent");
    public static final C5288aDo hAX = new C5288aDo("GrannyExtractTrackMaskResult_NoDataPresent");
    private static C5288aDo[] hAY = {hAV, hAW, hAX};

    /* renamed from: pF */
    private static int f2579pF = 0;

    /* renamed from: pG */
    private final int f2580pG;

    /* renamed from: pH */
    private final String f2581pH;

    private C5288aDo(String str) {
        this.f2581pH = str;
        int i = f2579pF;
        f2579pF = i + 1;
        this.f2580pG = i;
    }

    private C5288aDo(String str, int i) {
        this.f2581pH = str;
        this.f2580pG = i;
        f2579pF = i + 1;
    }

    private C5288aDo(String str, C5288aDo ado) {
        this.f2581pH = str;
        this.f2580pG = ado.f2580pG;
        f2579pF = this.f2580pG + 1;
    }

    /* renamed from: xz */
    public static C5288aDo m13844xz(int i) {
        if (i < hAY.length && i >= 0 && hAY[i].f2580pG == i) {
            return hAY[i];
        }
        for (int i2 = 0; i2 < hAY.length; i2++) {
            if (hAY[i2].f2580pG == i) {
                return hAY[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C5288aDo.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f2580pG;
    }

    public String toString() {
        return this.f2581pH;
    }
}
