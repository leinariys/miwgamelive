package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aKc  reason: case insensitive filesystem */
/* compiled from: a */
public class C5458aKc {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5458aKc(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5458aKc() {
        this(grannyJNI.new_granny_box_intersection(), true);
    }

    /* renamed from: a */
    public static long m16023a(C5458aKc akc) {
        if (akc == null) {
            return 0;
        }
        return akc.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_box_intersection(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: mV */
    public void mo9769mV(float f) {
        grannyJNI.granny_box_intersection_MinT_set(this.swigCPtr, f);
    }

    public float dgb() {
        return grannyJNI.granny_box_intersection_MinT_get(this.swigCPtr);
    }

    /* renamed from: O */
    public void mo9761O(C3159oW oWVar) {
        grannyJNI.granny_box_intersection_MinNormal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW dgc() {
        long granny_box_intersection_MinNormal_get = grannyJNI.granny_box_intersection_MinNormal_get(this.swigCPtr);
        if (granny_box_intersection_MinNormal_get == 0) {
            return null;
        }
        return new C3159oW(granny_box_intersection_MinNormal_get, false);
    }

    /* renamed from: mW */
    public void mo9770mW(float f) {
        grannyJNI.granny_box_intersection_MaxT_set(this.swigCPtr, f);
    }

    public float dgd() {
        return grannyJNI.granny_box_intersection_MaxT_get(this.swigCPtr);
    }

    /* renamed from: P */
    public void mo9762P(C3159oW oWVar) {
        grannyJNI.granny_box_intersection_MaxNormal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW dge() {
        long granny_box_intersection_MaxNormal_get = grannyJNI.granny_box_intersection_MaxNormal_get(this.swigCPtr);
        if (granny_box_intersection_MaxNormal_get == 0) {
            return null;
        }
        return new C3159oW(granny_box_intersection_MaxNormal_get, false);
    }

    /* renamed from: ys */
    public C5458aKc mo9771ys(int i) {
        long granny_box_intersection_get = grannyJNI.granny_box_intersection_get(this.swigCPtr, i);
        if (granny_box_intersection_get == 0) {
            return null;
        }
        return new C5458aKc(granny_box_intersection_get, false);
    }
}
