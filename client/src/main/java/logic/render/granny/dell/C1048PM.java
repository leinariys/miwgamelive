package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.PM */
/* compiled from: a */
public class C1048PM {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1048PM(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1048PM() {
        this(grannyJNI.new_granny_pwn343_vertex(), true);
    }

    /* renamed from: a */
    public static long m8401a(C1048PM pm) {
        if (pm == null) {
            return 0;
        }
        return pm.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pwn343_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo4663b(C3159oW oWVar) {
        grannyJNI.granny_pwn343_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo4671s() {
        long granny_pwn343_vertex_Position_get = grannyJNI.granny_pwn343_vertex_Position_get(this.swigCPtr);
        if (granny_pwn343_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwn343_vertex_Position_get, false);
    }

    /* renamed from: a */
    public void mo4661a(C2382ef efVar) {
        grannyJNI.granny_pwn343_vertex_BoneWeights_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cP */
    public C2382ef mo4664cP() {
        long granny_pwn343_vertex_BoneWeights_get = grannyJNI.granny_pwn343_vertex_BoneWeights_get(this.swigCPtr);
        if (granny_pwn343_vertex_BoneWeights_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwn343_vertex_BoneWeights_get, false);
    }

    /* renamed from: b */
    public void mo4662b(C2382ef efVar) {
        grannyJNI.granny_pwn343_vertex_BoneIndices_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cQ */
    public C2382ef mo4665cQ() {
        long granny_pwn343_vertex_BoneIndices_get = grannyJNI.granny_pwn343_vertex_BoneIndices_get(this.swigCPtr);
        if (granny_pwn343_vertex_BoneIndices_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwn343_vertex_BoneIndices_get, false);
    }

    /* renamed from: h */
    public void mo4669h(C3159oW oWVar) {
        grannyJNI.granny_pwn343_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo4666cR() {
        long granny_pwn343_vertex_Normal_get = grannyJNI.granny_pwn343_vertex_Normal_get(this.swigCPtr);
        if (granny_pwn343_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwn343_vertex_Normal_get, false);
    }

    /* renamed from: mI */
    public C1048PM mo4670mI(int i) {
        long granny_pwn343_vertex_get = grannyJNI.granny_pwn343_vertex_get(this.swigCPtr, i);
        if (granny_pwn343_vertex_get == 0) {
            return null;
        }
        return new C1048PM(granny_pwn343_vertex_get, false);
    }
}
