package logic.render.granny.dell;

import logic.render.granny.C2062bl;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aNX */
/* compiled from: a */
public class aNX {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aNX(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aNX() {
        this(grannyJNI.new_granny_curve_data_d9i3_k16u_c16u(), true);
    }

    /* renamed from: a */
    public static long m16607a(aNX anx) {
        if (anx == null) {
            return 0;
        }
        return anx.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve_data_d9i3_k16u_c16u(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo10299a(C6341alB alb) {
        grannyJNI.granny_curve_data_d9i3_k16u_c16u_CurveDataHeader_set(this.swigCPtr, C6341alB.m23687b(alb));
    }

    /* renamed from: Qa */
    public C6341alB mo10293Qa() {
        long granny_curve_data_d9i3_k16u_c16u_CurveDataHeader_get = grannyJNI.granny_curve_data_d9i3_k16u_c16u_CurveDataHeader_get(this.swigCPtr);
        if (granny_curve_data_d9i3_k16u_c16u_CurveDataHeader_get == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_d9i3_k16u_c16u_CurveDataHeader_get, false);
    }

    /* renamed from: dT */
    public void mo10301dT(int i) {
        grannyJNI.granny_curve_data_d9i3_k16u_c16u_OneOverKnotScaleTrunc_set(this.swigCPtr, i);
    }

    /* renamed from: WK */
    public int mo10298WK() {
        return grannyJNI.granny_curve_data_d9i3_k16u_c16u_OneOverKnotScaleTrunc_get(this.swigCPtr);
    }

    /* renamed from: n */
    public void mo10305n(C3159oW oWVar) {
        grannyJNI.granny_curve_data_d9i3_k16u_c16u_ControlScales_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: Qc */
    public C3159oW mo10294Qc() {
        long granny_curve_data_d9i3_k16u_c16u_ControlScales_get = grannyJNI.granny_curve_data_d9i3_k16u_c16u_ControlScales_get(this.swigCPtr);
        if (granny_curve_data_d9i3_k16u_c16u_ControlScales_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_d9i3_k16u_c16u_ControlScales_get, false);
    }

    /* renamed from: o */
    public void mo10306o(C3159oW oWVar) {
        grannyJNI.granny_curve_data_d9i3_k16u_c16u_ControlOffsets_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: Qd */
    public C3159oW mo10295Qd() {
        long granny_curve_data_d9i3_k16u_c16u_ControlOffsets_get = grannyJNI.granny_curve_data_d9i3_k16u_c16u_ControlOffsets_get(this.swigCPtr);
        if (granny_curve_data_d9i3_k16u_c16u_ControlOffsets_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_d9i3_k16u_c16u_ControlOffsets_get, false);
    }

    /* renamed from: di */
    public void mo10303di(int i) {
        grannyJNI.granny_curve_data_d9i3_k16u_c16u_KnotControlCount_set(this.swigCPtr, i);
    }

    /* renamed from: Qe */
    public int mo10296Qe() {
        return grannyJNI.granny_curve_data_d9i3_k16u_c16u_KnotControlCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo10300b(C2062bl blVar) {
        grannyJNI.granny_curve_data_d9i3_k16u_c16u_KnotsControls_set(this.swigCPtr, C2062bl.m27965a(blVar));
    }

    /* renamed from: Ua */
    public C2062bl mo10297Ua() {
        long granny_curve_data_d9i3_k16u_c16u_KnotsControls_get = grannyJNI.granny_curve_data_d9i3_k16u_c16u_KnotsControls_get(this.swigCPtr);
        if (granny_curve_data_d9i3_k16u_c16u_KnotsControls_get == 0) {
            return null;
        }
        return new C2062bl(granny_curve_data_d9i3_k16u_c16u_KnotsControls_get, false);
    }

    /* renamed from: za */
    public aNX mo10307za(int i) {
        long granny_curve_data_d9i3_k16u_c16u_get = grannyJNI.granny_curve_data_d9i3_k16u_c16u_get(this.swigCPtr, i);
        if (granny_curve_data_d9i3_k16u_c16u_get == 0) {
            return null;
        }
        return new aNX(granny_curve_data_d9i3_k16u_c16u_get, false);
    }
}
