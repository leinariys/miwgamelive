package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.vF */
/* compiled from: a */
public final class C3807vF {
    public static final C3807vF bzB = new C3807vF("GrannyBlendQuaternionDirectly", grannyJNI.GrannyBlendQuaternionDirectly_get());
    public static final C3807vF bzC = new C3807vF("GrannyBlendQuaternionInverted", grannyJNI.GrannyBlendQuaternionInverted_get());
    public static final C3807vF bzD = new C3807vF("GrannyBlendQuaternionNeighborhooded", grannyJNI.GrannyBlendQuaternionNeighborhooded_get());
    public static final C3807vF bzE = new C3807vF("GrannyBlendQuaternionAccumNeighborhooded", grannyJNI.GrannyBlendQuaternionAccumNeighborhooded_get());
    private static C3807vF[] bzF = {bzB, bzC, bzD, bzE};

    /* renamed from: pF */
    private static int f9397pF = 0;

    /* renamed from: pG */
    private final int f9398pG;

    /* renamed from: pH */
    private final String f9399pH;

    private C3807vF(String str) {
        this.f9399pH = str;
        int i = f9397pF;
        f9397pF = i + 1;
        this.f9398pG = i;
    }

    private C3807vF(String str, int i) {
        this.f9399pH = str;
        this.f9398pG = i;
        f9397pF = i + 1;
    }

    private C3807vF(String str, C3807vF vFVar) {
        this.f9399pH = str;
        this.f9398pG = vFVar.f9398pG;
        f9397pF = this.f9398pG + 1;
    }

    /* renamed from: eY */
    public static C3807vF m40165eY(int i) {
        if (i < bzF.length && i >= 0 && bzF[i].f9398pG == i) {
            return bzF[i];
        }
        for (int i2 = 0; i2 < bzF.length; i2++) {
            if (bzF[i2].f9398pG == i) {
                return bzF[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C3807vF.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f9398pG;
    }

    public String toString() {
        return this.f9399pH;
    }
}
