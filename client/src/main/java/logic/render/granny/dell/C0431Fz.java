package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

import java.nio.Buffer;

/* renamed from: a.Fz */
/* compiled from: a */
public class C0431Fz {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0431Fz(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0431Fz() {
        this(grannyJNI.new_granny_allocation_information(), true);
    }

    /* renamed from: a */
    public static long m3301a(C0431Fz fz) {
        if (fz == null) {
            return 0;
        }
        return fz.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_allocation_information(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo2236a(Buffer buffer) {
        grannyJNI.granny_allocation_information_Memory_set(this.swigCPtr, buffer);
    }

    public Buffer aPw() {
        return grannyJNI.granny_allocation_information_Memory_get(this.swigCPtr);
    }

    /* renamed from: hh */
    public void mo2244hh(int i) {
        grannyJNI.granny_allocation_information_RequestedSize_set(this.swigCPtr, i);
    }

    public int aPx() {
        return grannyJNI.granny_allocation_information_RequestedSize_get(this.swigCPtr);
    }

    /* renamed from: hi */
    public void mo2245hi(int i) {
        grannyJNI.granny_allocation_information_ActualSize_set(this.swigCPtr, i);
    }

    public int aPy() {
        return grannyJNI.granny_allocation_information_ActualSize_get(this.swigCPtr);
    }

    public String getSourceFileName() {
        return grannyJNI.granny_allocation_information_SourceFileName_get(this.swigCPtr);
    }

    public void setSourceFileName(String str) {
        grannyJNI.granny_allocation_information_SourceFileName_set(this.swigCPtr, str);
    }

    /* renamed from: O */
    public void mo2235O(int i) {
        grannyJNI.granny_allocation_information_SourceLineNumber_set(this.swigCPtr, i);
    }

    /* renamed from: fV */
    public int mo2241fV() {
        return grannyJNI.granny_allocation_information_SourceLineNumber_get(this.swigCPtr);
    }

    /* renamed from: hj */
    public C0431Fz mo2246hj(int i) {
        long granny_allocation_information_get = grannyJNI.granny_allocation_information_get(this.swigCPtr, i);
        if (granny_allocation_information_get == 0) {
            return null;
        }
        return new C0431Fz(granny_allocation_information_get, false);
    }
}
