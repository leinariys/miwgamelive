package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.ahx  reason: case insensitive filesystem */
/* compiled from: a */
public class C6181ahx {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6181ahx(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6181ahx() {
        this(grannyJNI.new_granny_stat_hud_perf(), true);
    }

    /* renamed from: b */
    public static long m22394b(C6181ahx ahx) {
        if (ahx == null) {
            return 0;
        }
        return ahx.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_stat_hud_perf(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: B */
    public void mo13626B(double d) {
        grannyJNI.granny_stat_hud_perf_TotalSeconds_set(this.swigCPtr, d);
    }

    public double bcS() {
        return grannyJNI.granny_stat_hud_perf_TotalSeconds_get(this.swigCPtr);
    }

    /* renamed from: C */
    public void mo13627C(double d) {
        grannyJNI.granny_stat_hud_perf_TotalCycles_set(this.swigCPtr, d);
    }

    public double bcT() {
        return grannyJNI.granny_stat_hud_perf_TotalCycles_get(this.swigCPtr);
    }

    /* renamed from: qS */
    public C6181ahx mo13632qS(int i) {
        long granny_stat_hud_perf_get = grannyJNI.granny_stat_hud_perf_get(this.swigCPtr, i);
        if (granny_stat_hud_perf_get == 0) {
            return null;
        }
        return new C6181ahx(granny_stat_hud_perf_get, false);
    }
}
