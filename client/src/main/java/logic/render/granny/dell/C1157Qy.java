package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.Qy */
/* compiled from: a */
public class C1157Qy {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1157Qy(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1157Qy() {
        this(grannyJNI.new_granny_stat_hud(), true);
    }

    /* renamed from: c */
    public static long m9055c(C1157Qy qy) {
        if (qy == null) {
            return 0;
        }
        return qy.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_stat_hud(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: mS */
    public void mo5145mS(int i) {
        grannyJNI.granny_stat_hud_FrameCount_set(this.swigCPtr, i);
    }

    public int getFrameCount() {
        return grannyJNI.granny_stat_hud_FrameCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo5128b(C1075Pj pj) {
        grannyJNI.granny_stat_hud_ModelInstances_set(this.swigCPtr, C1075Pj.m8601a(pj));
    }

    public C1075Pj bpQ() {
        long granny_stat_hud_ModelInstances_get = grannyJNI.granny_stat_hud_ModelInstances_get(this.swigCPtr);
        if (granny_stat_hud_ModelInstances_get == 0) {
            return null;
        }
        return new C1075Pj(granny_stat_hud_ModelInstances_get, false);
    }

    /* renamed from: a */
    public void mo5124a(aNT ant) {
        grannyJNI.granny_stat_hud_Controls_set(this.swigCPtr, aNT.m16576b(ant));
    }

    public aNT bpR() {
        long granny_stat_hud_Controls_get = grannyJNI.granny_stat_hud_Controls_get(this.swigCPtr);
        if (granny_stat_hud_Controls_get == 0) {
            return null;
        }
        return new aNT(granny_stat_hud_Controls_get, false);
    }

    /* renamed from: b */
    public void mo5130b(C6208aiY aiy) {
        grannyJNI.granny_stat_hud_AnimBindingCacheStatus_set(this.swigCPtr, C6208aiY.m22570c(aiy));
    }

    public C6208aiY bpS() {
        long granny_stat_hud_AnimBindingCacheStatus_get = grannyJNI.granny_stat_hud_AnimBindingCacheStatus_get(this.swigCPtr);
        if (granny_stat_hud_AnimBindingCacheStatus_get == 0) {
            return null;
        }
        return new C6208aiY(granny_stat_hud_AnimBindingCacheStatus_get, false);
    }

    /* renamed from: a */
    public void mo5125a(aOZ aoz) {
        grannyJNI.granny_stat_hud_AnimTypes_set(this.swigCPtr, aOZ.m16912b(aoz));
    }

    public aOZ bpT() {
        long granny_stat_hud_AnimTypes_get = grannyJNI.granny_stat_hud_AnimTypes_get(this.swigCPtr);
        if (granny_stat_hud_AnimTypes_get == 0) {
            return null;
        }
        return new aOZ(granny_stat_hud_AnimTypes_get, false);
    }

    /* renamed from: b */
    public void mo5131b(C3913wh whVar) {
        grannyJNI.granny_stat_hud_Footprint_set(this.swigCPtr, C3913wh.m40714a(whVar));
    }

    public C3913wh bpU() {
        long granny_stat_hud_Footprint_get = grannyJNI.granny_stat_hud_Footprint_get(this.swigCPtr);
        if (granny_stat_hud_Footprint_get == 0) {
            return null;
        }
        return new C3913wh(granny_stat_hud_Footprint_get, false);
    }

    /* renamed from: mT */
    public void mo5146mT(int i) {
        grannyJNI.granny_stat_hud_AllocPointCount_set(this.swigCPtr, i);
    }

    public int bpV() {
        return grannyJNI.granny_stat_hud_AllocPointCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo5129b(C1833aZ aZVar) {
        grannyJNI.granny_stat_hud_AllocPoints_set(this.swigCPtr, C1833aZ.m19350a(aZVar));
    }

    public C1833aZ bpW() {
        long granny_stat_hud_AllocPoints_get = grannyJNI.granny_stat_hud_AllocPoints_get(this.swigCPtr);
        if (granny_stat_hud_AllocPoints_get == 0) {
            return null;
        }
        return new C1833aZ(granny_stat_hud_AllocPoints_get, false);
    }

    /* renamed from: a */
    public void mo5126a(C6181ahx ahx) {
        grannyJNI.granny_stat_hud_Perf_set(this.swigCPtr, C6181ahx.m22394b(ahx));
    }

    public C6181ahx bpX() {
        long granny_stat_hud_Perf_get = grannyJNI.granny_stat_hud_Perf_get(this.swigCPtr);
        if (granny_stat_hud_Perf_get == 0) {
            return null;
        }
        return new C6181ahx(granny_stat_hud_Perf_get, false);
    }

    /* renamed from: mU */
    public void mo5147mU(int i) {
        grannyJNI.granny_stat_hud_PerfPointCount_set(this.swigCPtr, i);
    }

    public int bpY() {
        return grannyJNI.granny_stat_hud_PerfPointCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo5127b(C0705KB kb) {
        grannyJNI.granny_stat_hud_PerfPoints_set(this.swigCPtr, C0705KB.m6261a(kb));
    }

    public C0705KB bpZ() {
        long granny_stat_hud_PerfPoints_get = grannyJNI.granny_stat_hud_PerfPoints_get(this.swigCPtr);
        if (granny_stat_hud_PerfPoints_get == 0) {
            return null;
        }
        return new C0705KB(granny_stat_hud_PerfPoints_get, false);
    }

    /* renamed from: mV */
    public C1157Qy mo5148mV(int i) {
        long granny_stat_hud_get = grannyJNI.granny_stat_hud_get(this.swigCPtr, i);
        if (granny_stat_hud_get == 0) {
            return null;
        }
        return new C1157Qy(granny_stat_hud_get, false);
    }
}
