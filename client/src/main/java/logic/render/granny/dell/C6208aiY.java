package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aiY  reason: case insensitive filesystem */
/* compiled from: a */
public class C6208aiY {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6208aiY(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6208aiY() {
        this(grannyJNI.new_granny_animation_binding_cache_status(), true);
    }

    /* renamed from: c */
    public static long m22570c(C6208aiY aiy) {
        if (aiy == null) {
            return 0;
        }
        return aiy.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_animation_binding_cache_status(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: rl */
    public void mo13746rl(int i) {
        grannyJNI.granny_animation_binding_cache_status_TotalBindingsCreated_set(this.swigCPtr, i);
    }

    public int ccD() {
        return grannyJNI.granny_animation_binding_cache_status_TotalBindingsCreated_get(this.swigCPtr);
    }

    /* renamed from: rm */
    public void mo13747rm(int i) {
        grannyJNI.granny_animation_binding_cache_status_TotalBindingsDestroyed_set(this.swigCPtr, i);
    }

    public int ccE() {
        return grannyJNI.granny_animation_binding_cache_status_TotalBindingsDestroyed_get(this.swigCPtr);
    }

    /* renamed from: rn */
    public void mo13748rn(int i) {
        grannyJNI.granny_animation_binding_cache_status_DirectAcquireCount_set(this.swigCPtr, i);
    }

    public int ccF() {
        return grannyJNI.granny_animation_binding_cache_status_DirectAcquireCount_get(this.swigCPtr);
    }

    /* renamed from: ro */
    public void mo13749ro(int i) {
        grannyJNI.granny_animation_binding_cache_status_IndirectAcquireCount_set(this.swigCPtr, i);
    }

    public int ccG() {
        return grannyJNI.granny_animation_binding_cache_status_IndirectAcquireCount_get(this.swigCPtr);
    }

    /* renamed from: rp */
    public void mo13750rp(int i) {
        grannyJNI.granny_animation_binding_cache_status_ReleaseCount_set(this.swigCPtr, i);
    }

    public int ccH() {
        return grannyJNI.granny_animation_binding_cache_status_ReleaseCount_get(this.swigCPtr);
    }

    /* renamed from: rq */
    public void mo13751rq(int i) {
        grannyJNI.m45848x487fd557(this.swigCPtr, i);
    }

    public int ccI() {
        return grannyJNI.m45847x487fa84b(this.swigCPtr);
    }

    /* renamed from: rr */
    public void mo13752rr(int i) {
        grannyJNI.m45850xcfc32f84(this.swigCPtr, i);
    }

    public int ccJ() {
        return grannyJNI.m45849xcfc30278(this.swigCPtr);
    }

    /* renamed from: rs */
    public void mo13753rs(int i) {
        grannyJNI.granny_animation_binding_cache_status_CacheHits_set(this.swigCPtr, i);
    }

    public int ccK() {
        return grannyJNI.granny_animation_binding_cache_status_CacheHits_get(this.swigCPtr);
    }

    /* renamed from: rt */
    public void mo13754rt(int i) {
        grannyJNI.granny_animation_binding_cache_status_CacheMisses_set(this.swigCPtr, i);
    }

    public int ccL() {
        return grannyJNI.granny_animation_binding_cache_status_CacheMisses_get(this.swigCPtr);
    }

    /* renamed from: ru */
    public void mo13755ru(int i) {
        grannyJNI.granny_animation_binding_cache_status_ExplicitFlushCount_set(this.swigCPtr, i);
    }

    public int ccM() {
        return grannyJNI.granny_animation_binding_cache_status_ExplicitFlushCount_get(this.swigCPtr);
    }

    /* renamed from: rv */
    public void mo13756rv(int i) {
        grannyJNI.granny_animation_binding_cache_status_ExplicitFlushFrees_set(this.swigCPtr, i);
    }

    public int ccN() {
        return grannyJNI.granny_animation_binding_cache_status_ExplicitFlushFrees_get(this.swigCPtr);
    }

    /* renamed from: rw */
    public void mo13757rw(int i) {
        grannyJNI.granny_animation_binding_cache_status_OverflowFrees_set(this.swigCPtr, i);
    }

    public int ccO() {
        return grannyJNI.granny_animation_binding_cache_status_OverflowFrees_get(this.swigCPtr);
    }

    /* renamed from: rx */
    public C6208aiY mo13758rx(int i) {
        long granny_animation_binding_cache_status_get = grannyJNI.granny_animation_binding_cache_status_get(this.swigCPtr, i);
        if (granny_animation_binding_cache_status_get == 0) {
            return null;
        }
        return new C6208aiY(granny_animation_binding_cache_status_get, false);
    }
}
