package logic.render.granny.dell;

/* renamed from: a.aNv  reason: case insensitive filesystem */
/* compiled from: a */
public class C5555aNv {
    private long swigCPtr;

    public C5555aNv(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5555aNv() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m16789b(C5555aNv anv) {
        if (anv == null) {
            return 0;
        }
        return anv.swigCPtr;
    }
}
