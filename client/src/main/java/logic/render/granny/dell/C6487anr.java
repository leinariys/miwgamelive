package logic.render.granny.dell;

/* renamed from: a.anr  reason: case insensitive filesystem */
/* compiled from: a */
public class C6487anr {
    private long swigCPtr;

    public C6487anr(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6487anr() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m24338a(C6487anr anr) {
        if (anr == null) {
            return 0;
        }
        return anr.swigCPtr;
    }
}
