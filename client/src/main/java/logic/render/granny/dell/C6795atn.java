package logic.render.granny.dell;

/* renamed from: a.atn  reason: case insensitive filesystem */
/* compiled from: a */
public class C6795atn {
    private long swigCPtr;

    public C6795atn(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6795atn() {
        this.swigCPtr = 0;
    }

    /* renamed from: c */
    public static long m26059c(C6795atn atn) {
        if (atn == null) {
            return 0;
        }
        return atn.swigCPtr;
    }
}
