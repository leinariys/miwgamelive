package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import logic.render.granny.C6135ahD;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.akB  reason: case insensitive filesystem */
/* compiled from: a */
public class C6289akB {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6289akB(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6289akB() {
        this(grannyJNI.new_granny_vertex_weight_arrays(), true);
    }

    /* renamed from: a */
    public static long m23112a(C6289akB akb) {
        if (akb == null) {
            return 0;
        }
        return akb.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_vertex_weight_arrays(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: L */
    public void mo14227L(C3159oW oWVar) {
        grannyJNI.granny_vertex_weight_arrays_BoneWeights_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW cgt() {
        long granny_vertex_weight_arrays_BoneWeights_get = grannyJNI.granny_vertex_weight_arrays_BoneWeights_get(this.swigCPtr);
        if (granny_vertex_weight_arrays_BoneWeights_get == 0) {
            return null;
        }
        return new C3159oW(granny_vertex_weight_arrays_BoneWeights_get, false);
    }

    /* renamed from: j */
    public void mo14232j(C6135ahD ahd) {
        grannyJNI.granny_vertex_weight_arrays_BoneIndices_set(this.swigCPtr, C6135ahD.m22189i(ahd));
    }

    public C6135ahD cgu() {
        long granny_vertex_weight_arrays_BoneIndices_get = grannyJNI.granny_vertex_weight_arrays_BoneIndices_get(this.swigCPtr);
        if (granny_vertex_weight_arrays_BoneIndices_get == 0) {
            return null;
        }
        return new C6135ahD(granny_vertex_weight_arrays_BoneIndices_get, false);
    }

    /* renamed from: sf */
    public C6289akB mo14233sf(int i) {
        long granny_vertex_weight_arrays_get = grannyJNI.granny_vertex_weight_arrays_get(this.swigCPtr, i);
        if (granny_vertex_weight_arrays_get == 0) {
            return null;
        }
        return new C6289akB(granny_vertex_weight_arrays_get, false);
    }
}
