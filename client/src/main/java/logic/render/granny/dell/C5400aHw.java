package logic.render.granny.dell;

/* renamed from: a.aHw  reason: case insensitive filesystem */
/* compiled from: a */
public final class C5400aHw {
    public static final C5400aHw hXA = new C5400aHw("GrannyDisplacementTexture");
    public static final C5400aHw hXB = new C5400aHw("GrannyOnePastLastMaterialTextureType");
    public static final C5400aHw hXr = new C5400aHw("GrannyUnknownTextureType");
    public static final C5400aHw hXs = new C5400aHw("GrannyAmbientColorTexture");
    public static final C5400aHw hXt = new C5400aHw("GrannyDiffuseColorTexture");
    public static final C5400aHw hXu = new C5400aHw("GrannySpecularColorTexture");
    public static final C5400aHw hXv = new C5400aHw("GrannySelfIlluminationTexture");
    public static final C5400aHw hXw = new C5400aHw("GrannyOpacityTexture");
    public static final C5400aHw hXx = new C5400aHw("GrannyBumpHeightTexture");
    public static final C5400aHw hXy = new C5400aHw("GrannyReflectionTexture");
    public static final C5400aHw hXz = new C5400aHw("GrannyRefractionTexture");
    private static C5400aHw[] hXC = {hXr, hXs, hXt, hXu, hXv, hXw, hXx, hXy, hXz, hXA, hXB};
    /* renamed from: pF */
    private static int f3012pF = 0;

    /* renamed from: pG */
    private final int f3013pG;

    /* renamed from: pH */
    private final String f3014pH;

    private C5400aHw(String str) {
        this.f3014pH = str;
        int i = f3012pF;
        f3012pF = i + 1;
        this.f3013pG = i;
    }

    private C5400aHw(String str, int i) {
        this.f3014pH = str;
        this.f3013pG = i;
        f3012pF = i + 1;
    }

    private C5400aHw(String str, C5400aHw ahw) {
        this.f3014pH = str;
        this.f3013pG = ahw.f3013pG;
        f3012pF = this.f3013pG + 1;
    }

    /* renamed from: yg */
    public static C5400aHw m15324yg(int i) {
        if (i < hXC.length && i >= 0 && hXC[i].f3013pG == i) {
            return hXC[i];
        }
        for (int i2 = 0; i2 < hXC.length; i2++) {
            if (hXC[i2].f3013pG == i) {
                return hXC[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C5400aHw.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f3013pG;
    }

    public String toString() {
        return this.f3014pH;
    }
}
