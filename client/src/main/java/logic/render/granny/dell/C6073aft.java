package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aft  reason: case insensitive filesystem */
/* compiled from: a */
public class C6073aft {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6073aft(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6073aft() {
        this(grannyJNI.new_granny_curve_data_da_keyframes32f(), true);
    }

    /* renamed from: a */
    public static long m21649a(C6073aft aft) {
        if (aft == null) {
            return 0;
        }
        return aft.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve_data_da_keyframes32f(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo13302a(C6341alB alb) {
        grannyJNI.granny_curve_data_da_keyframes32f_CurveDataHeader_set(this.swigCPtr, C6341alB.m23687b(alb));
    }

    /* renamed from: Qa */
    public C6341alB mo13301Qa() {
        long granny_curve_data_da_keyframes32f_CurveDataHeader_get = grannyJNI.granny_curve_data_da_keyframes32f_CurveDataHeader_get(this.swigCPtr);
        if (granny_curve_data_da_keyframes32f_CurveDataHeader_get == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_da_keyframes32f_CurveDataHeader_get, false);
    }

    /* renamed from: t */
    public void mo13310t(short s) {
        grannyJNI.granny_curve_data_da_keyframes32f_Dimension_set(this.swigCPtr, s);
    }

    public short bVv() {
        return grannyJNI.granny_curve_data_da_keyframes32f_Dimension_get(this.swigCPtr);
    }

    /* renamed from: qv */
    public void mo13308qv(int i) {
        grannyJNI.granny_curve_data_da_keyframes32f_ControlCount_set(this.swigCPtr, i);
    }

    public int bVw() {
        return grannyJNI.granny_curve_data_da_keyframes32f_ControlCount_get(this.swigCPtr);
    }

    /* renamed from: K */
    public void mo13300K(C3159oW oWVar) {
        grannyJNI.granny_curve_data_da_keyframes32f_Controls_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW bEk() {
        long granny_curve_data_da_keyframes32f_Controls_get = grannyJNI.granny_curve_data_da_keyframes32f_Controls_get(this.swigCPtr);
        if (granny_curve_data_da_keyframes32f_Controls_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_da_keyframes32f_Controls_get, false);
    }

    /* renamed from: qw */
    public C6073aft mo13309qw(int i) {
        long granny_curve_data_da_keyframes32f_get = grannyJNI.granny_curve_data_da_keyframes32f_get(this.swigCPtr, i);
        if (granny_curve_data_da_keyframes32f_get == 0) {
            return null;
        }
        return new C6073aft(granny_curve_data_da_keyframes32f_get, false);
    }
}
