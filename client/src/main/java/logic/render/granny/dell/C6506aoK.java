package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aoK  reason: case insensitive filesystem */
/* compiled from: a */
public class C6506aoK {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6506aoK(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6506aoK() {
        this(grannyJNI.new_granny_counter_results(), true);
    }

    /* renamed from: a */
    public static long m24404a(C6506aoK aok) {
        if (aok == null) {
            return 0;
        }
        return aok.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_counter_results(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getName() {
        return grannyJNI.granny_counter_results_Name_get(this.swigCPtr);
    }

    public void setName(String str) {
        grannyJNI.granny_counter_results_Name_set(this.swigCPtr, str);
    }

    /* renamed from: lt */
    public void mo15191lt(int i) {
        grannyJNI.granny_counter_results_Count_set(this.swigCPtr, i);
    }

    public int getCount() {
        return grannyJNI.granny_counter_results_Count_get(this.swigCPtr);
    }

    /* renamed from: B */
    public void mo15174B(double d) {
        grannyJNI.granny_counter_results_TotalSeconds_set(this.swigCPtr, d);
    }

    public double bcS() {
        return grannyJNI.granny_counter_results_TotalSeconds_get(this.swigCPtr);
    }

    /* renamed from: P */
    public void mo15176P(double d) {
        grannyJNI.granny_counter_results_SecondsWithoutChildren_set(this.swigCPtr, d);
    }

    public double coj() {
        return grannyJNI.granny_counter_results_SecondsWithoutChildren_get(this.swigCPtr);
    }

    /* renamed from: C */
    public void mo15175C(double d) {
        grannyJNI.granny_counter_results_TotalCycles_set(this.swigCPtr, d);
    }

    public double bcT() {
        return grannyJNI.granny_counter_results_TotalCycles_get(this.swigCPtr);
    }

    /* renamed from: Q */
    public void mo15177Q(double d) {
        grannyJNI.granny_counter_results_TotalCyclesWithoutChildren_set(this.swigCPtr, d);
    }

    public double cok() {
        return grannyJNI.granny_counter_results_TotalCyclesWithoutChildren_get(this.swigCPtr);
    }

    /* renamed from: tj */
    public void mo15193tj(int i) {
        grannyJNI.granny_counter_results_PeakCount_set(this.swigCPtr, i);
    }

    public int col() {
        return grannyJNI.granny_counter_results_PeakCount_get(this.swigCPtr);
    }

    /* renamed from: R */
    public void mo15178R(double d) {
        grannyJNI.granny_counter_results_PeakTotalSeconds_set(this.swigCPtr, d);
    }

    public double com() {
        return grannyJNI.granny_counter_results_PeakTotalSeconds_get(this.swigCPtr);
    }

    /* renamed from: S */
    public void mo15179S(double d) {
        grannyJNI.granny_counter_results_PeakSecondsWithoutChildren_set(this.swigCPtr, d);
    }

    public double con() {
        return grannyJNI.granny_counter_results_PeakSecondsWithoutChildren_get(this.swigCPtr);
    }

    /* renamed from: tk */
    public C6506aoK mo15194tk(int i) {
        long granny_counter_results_get = grannyJNI.granny_counter_results_get(this.swigCPtr, i);
        if (granny_counter_results_get == 0) {
            return null;
        }
        return new C6506aoK(granny_counter_results_get, false);
    }
}
