package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aKm  reason: case insensitive filesystem */
/* compiled from: a */
public class C5468aKm {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5468aKm(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5468aKm() {
        this(grannyJNI.new_granny_pngt3332_vertex(), true);
    }

    /* renamed from: a */
    public static long m16050a(C5468aKm akm) {
        if (akm == null) {
            return 0;
        }
        return akm.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pngt3332_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo9795b(C3159oW oWVar) {
        grannyJNI.granny_pngt3332_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo9804s() {
        long granny_pngt3332_vertex_Position_get = grannyJNI.granny_pngt3332_vertex_Position_get(this.swigCPtr);
        if (granny_pngt3332_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngt3332_vertex_Position_get, false);
    }

    /* renamed from: h */
    public void mo9800h(C3159oW oWVar) {
        grannyJNI.granny_pngt3332_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo9796cR() {
        long granny_pngt3332_vertex_Normal_get = grannyJNI.granny_pngt3332_vertex_Normal_get(this.swigCPtr);
        if (granny_pngt3332_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngt3332_vertex_Normal_get, false);
    }

    /* renamed from: i */
    public void mo9801i(C3159oW oWVar) {
        grannyJNI.granny_pngt3332_vertex_Tangent_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: gD */
    public C3159oW mo9799gD() {
        long granny_pngt3332_vertex_Tangent_get = grannyJNI.granny_pngt3332_vertex_Tangent_get(this.swigCPtr);
        if (granny_pngt3332_vertex_Tangent_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngt3332_vertex_Tangent_get, false);
    }

    /* renamed from: j */
    public void mo9802j(C3159oW oWVar) {
        grannyJNI.granny_pngt3332_vertex_UV_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: mv */
    public C3159oW mo9803mv() {
        long granny_pngt3332_vertex_UV_get = grannyJNI.granny_pngt3332_vertex_UV_get(this.swigCPtr);
        if (granny_pngt3332_vertex_UV_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngt3332_vertex_UV_get, false);
    }

    /* renamed from: yt */
    public C5468aKm mo9805yt(int i) {
        long granny_pngt3332_vertex_get = grannyJNI.granny_pngt3332_vertex_get(this.swigCPtr, i);
        if (granny_pngt3332_vertex_get == 0) {
            return null;
        }
        return new C5468aKm(granny_pngt3332_vertex_get, false);
    }
}
