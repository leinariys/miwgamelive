package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.rr */
/* compiled from: a */
public class C3518rr {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3518rr(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3518rr() {
        this(grannyJNI.new_granny_pwn313_vertex(), true);
    }

    /* renamed from: a */
    public static long m38647a(C3518rr rrVar) {
        if (rrVar == null) {
            return 0;
        }
        return rrVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pwn313_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo21849b(C3159oW oWVar) {
        grannyJNI.granny_pwn313_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo21857s() {
        long granny_pwn313_vertex_Position_get = grannyJNI.granny_pwn313_vertex_Position_get(this.swigCPtr);
        if (granny_pwn313_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwn313_vertex_Position_get, false);
    }

    /* renamed from: n */
    public void mo21856n(long j) {
        grannyJNI.granny_pwn313_vertex_BoneIndex_set(this.swigCPtr, j);
    }

    /* renamed from: gC */
    public long mo21854gC() {
        return grannyJNI.granny_pwn313_vertex_BoneIndex_get(this.swigCPtr);
    }

    /* renamed from: h */
    public void mo21855h(C3159oW oWVar) {
        grannyJNI.granny_pwn313_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo21850cR() {
        long granny_pwn313_vertex_Normal_get = grannyJNI.granny_pwn313_vertex_Normal_get(this.swigCPtr);
        if (granny_pwn313_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwn313_vertex_Normal_get, false);
    }

    /* renamed from: ej */
    public C3518rr mo21852ej(int i) {
        long granny_pwn313_vertex_get = grannyJNI.granny_pwn313_vertex_get(this.swigCPtr, i);
        if (granny_pwn313_vertex_get == 0) {
            return null;
        }
        return new C3518rr(granny_pwn313_vertex_get, false);
    }
}
