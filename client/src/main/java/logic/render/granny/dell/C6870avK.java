package logic.render.granny.dell;

import logic.render.granny.C5501aLt;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.avK  reason: case insensitive filesystem */
/* compiled from: a */
public class C6870avK {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6870avK(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6870avK() {
        this(grannyJNI.new_granny_log_callback(), true);
    }

    /* renamed from: c */
    public static long m26571c(C6870avK avk) {
        if (avk == null) {
            return 0;
        }
        return avk.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_log_callback(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo16562b(C1611Xa xa) {
        grannyJNI.granny_log_callback_Function_set(this.swigCPtr, C1611Xa.m11540a(xa));
    }

    public C1611Xa cAA() {
        long granny_log_callback_Function_get = grannyJNI.granny_log_callback_Function_get(this.swigCPtr);
        if (granny_log_callback_Function_get == 0) {
            return null;
        }
        return new C1611Xa(granny_log_callback_Function_get, false);
    }

    /* renamed from: g */
    public void mo16567g(C5501aLt alt) {
        grannyJNI.granny_log_callback_UserData_set(this.swigCPtr, C5501aLt.m16286h(alt));
    }

    public C5501aLt cAB() {
        long granny_log_callback_UserData_get = grannyJNI.granny_log_callback_UserData_get(this.swigCPtr);
        if (granny_log_callback_UserData_get == 0) {
            return null;
        }
        return new C5501aLt(granny_log_callback_UserData_get, false);
    }

    /* renamed from: vH */
    public C6870avK mo16568vH(int i) {
        long granny_log_callback_get = grannyJNI.granny_log_callback_get(this.swigCPtr, i);
        if (granny_log_callback_get == 0) {
            return null;
        }
        return new C6870avK(granny_log_callback_get, false);
    }
}
