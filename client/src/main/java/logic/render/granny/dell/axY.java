package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.axY */
/* compiled from: a */
public class axY {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public axY(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public axY() {
        this(grannyJNI.new_granny_curve_data_d9i1_k8u_c8u(), true);
    }

    /* renamed from: a */
    public static long m27086a(axY axy) {
        if (axy == null) {
            return 0;
        }
        return axy.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve_data_d9i1_k8u_c8u(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo16769a(C6341alB alb) {
        grannyJNI.granny_curve_data_d9i1_k8u_c8u_CurveDataHeader_set(this.swigCPtr, C6341alB.m23687b(alb));
    }

    /* renamed from: Qa */
    public C6341alB mo16764Qa() {
        long granny_curve_data_d9i1_k8u_c8u_CurveDataHeader_get = grannyJNI.granny_curve_data_d9i1_k8u_c8u_CurveDataHeader_get(this.swigCPtr);
        if (granny_curve_data_d9i1_k8u_c8u_CurveDataHeader_get == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_d9i1_k8u_c8u_CurveDataHeader_get, false);
    }

    /* renamed from: dT */
    public void mo16774dT(int i) {
        grannyJNI.granny_curve_data_d9i1_k8u_c8u_OneOverKnotScaleTrunc_set(this.swigCPtr, i);
    }

    /* renamed from: WK */
    public int mo16766WK() {
        return grannyJNI.granny_curve_data_d9i1_k8u_c8u_OneOverKnotScaleTrunc_get(this.swigCPtr);
    }

    /* renamed from: cU */
    public void mo16771cU(float f) {
        grannyJNI.granny_curve_data_d9i1_k8u_c8u_ControlScale_set(this.swigCPtr, f);
    }

    /* renamed from: WL */
    public float mo16767WL() {
        return grannyJNI.granny_curve_data_d9i1_k8u_c8u_ControlScale_get(this.swigCPtr);
    }

    /* renamed from: cV */
    public void mo16772cV(float f) {
        grannyJNI.granny_curve_data_d9i1_k8u_c8u_ControlOffset_set(this.swigCPtr, f);
    }

    /* renamed from: WM */
    public float mo16768WM() {
        return grannyJNI.granny_curve_data_d9i1_k8u_c8u_ControlOffset_get(this.swigCPtr);
    }

    /* renamed from: di */
    public void mo16776di(int i) {
        grannyJNI.granny_curve_data_d9i1_k8u_c8u_KnotControlCount_set(this.swigCPtr, i);
    }

    /* renamed from: Qe */
    public int mo16765Qe() {
        return grannyJNI.granny_curve_data_d9i1_k8u_c8u_KnotControlCount_get(this.swigCPtr);
    }

    /* renamed from: d */
    public void mo16773d(C2382ef efVar) {
        grannyJNI.granny_curve_data_d9i1_k8u_c8u_KnotsControls_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    public C2382ef ano() {
        long granny_curve_data_d9i1_k8u_c8u_KnotsControls_get = grannyJNI.granny_curve_data_d9i1_k8u_c8u_KnotsControls_get(this.swigCPtr);
        if (granny_curve_data_d9i1_k8u_c8u_KnotsControls_get == 0) {
            return null;
        }
        return new C2382ef(granny_curve_data_d9i1_k8u_c8u_KnotsControls_get, false);
    }

    /* renamed from: vZ */
    public axY mo16778vZ(int i) {
        long granny_curve_data_d9i1_k8u_c8u_get = grannyJNI.granny_curve_data_d9i1_k8u_c8u_get(this.swigCPtr, i);
        if (granny_curve_data_d9i1_k8u_c8u_get == 0) {
            return null;
        }
        return new axY(granny_curve_data_d9i1_k8u_c8u_get, false);
    }
}
