package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.nt */
/* compiled from: a */
public class C3106nt {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3106nt(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3106nt() {
        this(grannyJNI.new_granny_p3_vertex(), true);
    }

    /* renamed from: a */
    public static long m36512a(C3106nt ntVar) {
        if (ntVar == null) {
            return 0;
        }
        return ntVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_p3_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo20858b(C3159oW oWVar) {
        grannyJNI.granny_p3_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo20862s() {
        long granny_p3_vertex_Position_get = grannyJNI.granny_p3_vertex_Position_get(this.swigCPtr);
        if (granny_p3_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_p3_vertex_Position_get, false);
    }

    /* renamed from: dk */
    public C3106nt mo20860dk(int i) {
        long granny_p3_vertex_get = grannyJNI.granny_p3_vertex_get(this.swigCPtr, i);
        if (granny_p3_vertex_get == 0) {
            return null;
        }
        return new C3106nt(granny_p3_vertex_get, false);
    }
}
