package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.alg  reason: case insensitive filesystem */
/* compiled from: a */
public class C6372alg {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6372alg(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6372alg() {
        this(grannyJNI.new_granny_fixed_allocator_unit(), true);
    }

    /* renamed from: b */
    public static long m23797b(C6372alg alg) {
        if (alg == null) {
            return 0;
        }
        return alg.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_fixed_allocator_unit(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: c */
    public void mo14736c(C6372alg alg) {
        grannyJNI.granny_fixed_allocator_unit_Next_set(this.swigCPtr, m23797b(alg));
    }

    public C6372alg chP() {
        long granny_fixed_allocator_unit_Next_get = grannyJNI.granny_fixed_allocator_unit_Next_get(this.swigCPtr);
        if (granny_fixed_allocator_unit_Next_get == 0) {
            return null;
        }
        return new C6372alg(granny_fixed_allocator_unit_Next_get, false);
    }

    /* renamed from: ss */
    public C6372alg mo14740ss(int i) {
        long granny_fixed_allocator_unit_get = grannyJNI.granny_fixed_allocator_unit_get(this.swigCPtr, i);
        if (granny_fixed_allocator_unit_get == 0) {
            return null;
        }
        return new C6372alg(granny_fixed_allocator_unit_get, false);
    }
}
