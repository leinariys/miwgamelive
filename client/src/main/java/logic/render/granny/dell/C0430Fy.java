package logic.render.granny.dell;

/* renamed from: a.Fy */
/* compiled from: a */
public final class C0430Fy {
    public static final C0430Fy cVA = new C0430Fy("GrannyCubicPixelFilter");
    public static final C0430Fy cVB = new C0430Fy("GrannyLinearPixelFilter");
    public static final C0430Fy cVC = new C0430Fy("GrannyBoxPixelFilter");
    public static final C0430Fy cVD = new C0430Fy("GrannyOnePastLastPixelFilterType");
    private static C0430Fy[] cVE = {cVA, cVB, cVC, cVD};

    /* renamed from: pF */
    private static int f592pF = 0;

    /* renamed from: pG */
    private final int f593pG;

    /* renamed from: pH */
    private final String f594pH;

    private C0430Fy(String str) {
        this.f594pH = str;
        int i = f592pF;
        f592pF = i + 1;
        this.f593pG = i;
    }

    private C0430Fy(String str, int i) {
        this.f594pH = str;
        this.f593pG = i;
        f592pF = i + 1;
    }

    private C0430Fy(String str, C0430Fy fy) {
        this.f594pH = str;
        this.f593pG = fy.f593pG;
        f592pF = this.f593pG + 1;
    }

    /* renamed from: hg */
    public static C0430Fy m3300hg(int i) {
        if (i < cVE.length && i >= 0 && cVE[i].f593pG == i) {
            return cVE[i];
        }
        for (int i2 = 0; i2 < cVE.length; i2++) {
            if (cVE[i2].f593pG == i) {
                return cVE[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C0430Fy.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f593pG;
    }

    public String toString() {
        return this.f594pH;
    }
}
