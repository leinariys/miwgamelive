package logic.render.granny.dell;

import logic.render.granny.C6900avo;
import logic.render.granny.aSM;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aPJ */
/* compiled from: a */
public class aPJ {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aPJ(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aPJ() {
        this(grannyJNI.new_granny_string_database(), true);
    }

    /* renamed from: a */
    public static long m17153a(aPJ apj) {
        if (apj == null) {
            return 0;
        }
        return apj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_string_database(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: Aa */
    public void mo10733Aa(int i) {
        grannyJNI.granny_string_database_StringCount_set(this.swigCPtr, i);
    }

    public int dot() {
        return grannyJNI.granny_string_database_StringCount_get(this.swigCPtr);
    }

    /* renamed from: e */
    public void mo10740e(C6900avo avo) {
        grannyJNI.granny_string_database_Strings_set(this.swigCPtr, C6900avo.m26731d(avo));
    }

    public C6900avo dou() {
        long granny_string_database_Strings_get = grannyJNI.granny_string_database_Strings_get(this.swigCPtr);
        if (granny_string_database_Strings_get == 0) {
            return null;
        }
        return new C6900avo(granny_string_database_Strings_get, false);
    }

    /* renamed from: lJ */
    public void mo10743lJ(long j) {
        grannyJNI.granny_string_database_DatabaseCRC_set(this.swigCPtr, j);
    }

    public long dov() {
        return grannyJNI.granny_string_database_DatabaseCRC_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo10735a(aSM asm) {
        grannyJNI.granny_string_database_ExtendedData_set(this.swigCPtr, aSM.m18172c(asm));
    }

    /* renamed from: kM */
    public aSM mo10742kM() {
        long granny_string_database_ExtendedData_get = grannyJNI.granny_string_database_ExtendedData_get(this.swigCPtr);
        if (granny_string_database_ExtendedData_get == 0) {
            return null;
        }
        return new aSM(granny_string_database_ExtendedData_get, false);
    }

    /* renamed from: Ab */
    public aPJ mo10734Ab(int i) {
        long granny_string_database_get = grannyJNI.granny_string_database_get(this.swigCPtr, i);
        if (granny_string_database_get == 0) {
            return null;
        }
        return new aPJ(granny_string_database_get, false);
    }
}
