package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.ma */
/* compiled from: a */
public class C2982ma {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2982ma(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C2982ma() {
        this(grannyJNI.new_granny_text_track(), true);
    }

    /* renamed from: a */
    public static long m35792a(C2982ma maVar) {
        if (maVar == null) {
            return 0;
        }
        return maVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_text_track(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getName() {
        return grannyJNI.granny_text_track_Name_get(this.swigCPtr);
    }

    public void setName(String str) {
        grannyJNI.granny_text_track_Name_set(this.swigCPtr, str);
    }

    /* renamed from: cT */
    public void mo20564cT(int i) {
        grannyJNI.granny_text_track_EntryCount_set(this.swigCPtr, i);
    }

    public int getEntryCount() {
        return grannyJNI.granny_text_track_EntryCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo20563a(C5324aEy aey) {
        grannyJNI.granny_text_track_Entries_set(this.swigCPtr, C5324aEy.m14469b(aey));
    }

    /* renamed from: MV */
    public C5324aEy mo20562MV() {
        long granny_text_track_Entries_get = grannyJNI.granny_text_track_Entries_get(this.swigCPtr);
        if (granny_text_track_Entries_get == 0) {
            return null;
        }
        return new C5324aEy(granny_text_track_Entries_get, false);
    }

    /* renamed from: cU */
    public C2982ma mo20565cU(int i) {
        long granny_text_track_get = grannyJNI.granny_text_track_get(this.swigCPtr, i);
        if (granny_text_track_get == 0) {
            return null;
        }
        return new C2982ma(granny_text_track_get, false);
    }
}
