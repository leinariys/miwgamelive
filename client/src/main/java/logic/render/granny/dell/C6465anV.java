package logic.render.granny.dell;

/* renamed from: a.anV  reason: case insensitive filesystem */
/* compiled from: a */
public class C6465anV {
    private long swigCPtr;

    public C6465anV(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6465anV() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m24198b(C6465anV anv) {
        if (anv == null) {
            return 0;
        }
        return anv.swigCPtr;
    }
}
