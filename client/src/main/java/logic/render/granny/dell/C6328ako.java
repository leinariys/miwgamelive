package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.ako  reason: case insensitive filesystem */
/* compiled from: a */
public class C6328ako {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6328ako(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6328ako() {
        this(grannyJNI.new_granny_curve_data_da_identity(), true);
    }

    /* renamed from: a */
    public static long m23610a(C6328ako ako) {
        if (ako == null) {
            return 0;
        }
        return ako.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve_data_da_identity(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo14523a(C6341alB alb) {
        grannyJNI.granny_curve_data_da_identity_CurveDataHeader_set(this.swigCPtr, C6341alB.m23687b(alb));
    }

    /* renamed from: Qa */
    public C6341alB mo14522Qa() {
        long granny_curve_data_da_identity_CurveDataHeader_get = grannyJNI.granny_curve_data_da_identity_CurveDataHeader_get(this.swigCPtr);
        if (granny_curve_data_da_identity_CurveDataHeader_get == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_da_identity_CurveDataHeader_get, false);
    }

    /* renamed from: t */
    public void mo14528t(short s) {
        grannyJNI.granny_curve_data_da_identity_Dimension_set(this.swigCPtr, s);
    }

    public short bVv() {
        return grannyJNI.granny_curve_data_da_identity_Dimension_get(this.swigCPtr);
    }

    /* renamed from: se */
    public C6328ako mo14527se(int i) {
        long granny_curve_data_da_identity_get = grannyJNI.granny_curve_data_da_identity_get(this.swigCPtr, i);
        if (granny_curve_data_da_identity_get == 0) {
            return null;
        }
        return new C6328ako(granny_curve_data_da_identity_get, false);
    }
}
