package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.VD */
/* compiled from: a */
public class C1441VD {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1441VD(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1441VD() {
        this(grannyJNI.new_granny_pwngb34333_vertex(), true);
    }

    /* renamed from: a */
    public static long m10454a(C1441VD vd) {
        if (vd == null) {
            return 0;
        }
        return vd.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pwngb34333_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo5961b(C3159oW oWVar) {
        grannyJNI.granny_pwngb34333_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo5971s() {
        long granny_pwngb34333_vertex_Position_get = grannyJNI.granny_pwngb34333_vertex_Position_get(this.swigCPtr);
        if (granny_pwngb34333_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngb34333_vertex_Position_get, false);
    }

    /* renamed from: a */
    public void mo5958a(C2382ef efVar) {
        grannyJNI.granny_pwngb34333_vertex_BoneWeights_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cP */
    public C2382ef mo5962cP() {
        long granny_pwngb34333_vertex_BoneWeights_get = grannyJNI.granny_pwngb34333_vertex_BoneWeights_get(this.swigCPtr);
        if (granny_pwngb34333_vertex_BoneWeights_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwngb34333_vertex_BoneWeights_get, false);
    }

    /* renamed from: b */
    public void mo5960b(C2382ef efVar) {
        grannyJNI.granny_pwngb34333_vertex_BoneIndices_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cQ */
    public C2382ef mo5963cQ() {
        long granny_pwngb34333_vertex_BoneIndices_get = grannyJNI.granny_pwngb34333_vertex_BoneIndices_get(this.swigCPtr);
        if (granny_pwngb34333_vertex_BoneIndices_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwngb34333_vertex_BoneIndices_get, false);
    }

    /* renamed from: h */
    public void mo5968h(C3159oW oWVar) {
        grannyJNI.granny_pwngb34333_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo5964cR() {
        long granny_pwngb34333_vertex_Normal_get = grannyJNI.granny_pwngb34333_vertex_Normal_get(this.swigCPtr);
        if (granny_pwngb34333_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngb34333_vertex_Normal_get, false);
    }

    /* renamed from: i */
    public void mo5969i(C3159oW oWVar) {
        grannyJNI.granny_pwngb34333_vertex_Tangent_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: gD */
    public C3159oW mo5967gD() {
        long granny_pwngb34333_vertex_Tangent_get = grannyJNI.granny_pwngb34333_vertex_Tangent_get(this.swigCPtr);
        if (granny_pwngb34333_vertex_Tangent_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngb34333_vertex_Tangent_get, false);
    }

    /* renamed from: w */
    public void mo5972w(C3159oW oWVar) {
        grannyJNI.granny_pwngb34333_vertex_Binormal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW amC() {
        long granny_pwngb34333_vertex_Binormal_get = grannyJNI.granny_pwngb34333_vertex_Binormal_get(this.swigCPtr);
        if (granny_pwngb34333_vertex_Binormal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngb34333_vertex_Binormal_get, false);
    }

    /* renamed from: oK */
    public C1441VD mo5970oK(int i) {
        long granny_pwngb34333_vertex_get = grannyJNI.granny_pwngb34333_vertex_get(this.swigCPtr, i);
        if (granny_pwngb34333_vertex_get == 0) {
            return null;
        }
        return new C1441VD(granny_pwngb34333_vertex_get, false);
    }
}
