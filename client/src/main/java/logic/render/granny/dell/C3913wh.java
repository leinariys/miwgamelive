package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.wh */
/* compiled from: a */
public class C3913wh {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3913wh(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3913wh() {
        this(grannyJNI.new_granny_stat_hud_footprint(), true);
    }

    /* renamed from: a */
    public static long m40714a(C3913wh whVar) {
        if (whVar == null) {
            return 0;
        }
        return whVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_stat_hud_footprint(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: fo */
    public void mo22796fo(int i) {
        grannyJNI.granny_stat_hud_footprint_TotalAllocationCount_set(this.swigCPtr, i);
    }

    public int amb() {
        return grannyJNI.granny_stat_hud_footprint_TotalAllocationCount_get(this.swigCPtr);
    }

    /* renamed from: fp */
    public void mo22797fp(int i) {
        grannyJNI.granny_stat_hud_footprint_TotalBytesRequested_set(this.swigCPtr, i);
    }

    public int amc() {
        return grannyJNI.granny_stat_hud_footprint_TotalBytesRequested_get(this.swigCPtr);
    }

    /* renamed from: fq */
    public void mo22798fq(int i) {
        grannyJNI.granny_stat_hud_footprint_TotalBytesAllocated_set(this.swigCPtr, i);
    }

    public int amd() {
        return grannyJNI.granny_stat_hud_footprint_TotalBytesAllocated_get(this.swigCPtr);
    }

    /* renamed from: fr */
    public C3913wh mo22799fr(int i) {
        long granny_stat_hud_footprint_get = grannyJNI.granny_stat_hud_footprint_get(this.swigCPtr, i);
        if (granny_stat_hud_footprint_get == 0) {
            return null;
        }
        return new C3913wh(granny_stat_hud_footprint_get, false);
    }
}
