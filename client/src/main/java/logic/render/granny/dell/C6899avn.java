package logic.render.granny.dell;

/* renamed from: a.avn  reason: case insensitive filesystem */
/* compiled from: a */
public class C6899avn {
    private long swigCPtr;

    public C6899avn(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6899avn() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m26730b(C6899avn avn) {
        if (avn == null) {
            return 0;
        }
        return avn.swigCPtr;
    }
}
