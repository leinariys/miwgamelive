package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.iY */
/* compiled from: a */
public class C2685iY {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2685iY(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C2685iY() {
        this(grannyJNI.new_granny_pwngt31332_vertex(), true);
    }

    /* renamed from: a */
    public static long m33412a(C2685iY iYVar) {
        if (iYVar == null) {
            return 0;
        }
        return iYVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pwngt31332_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo19628b(C3159oW oWVar) {
        grannyJNI.granny_pwngt31332_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo19640s() {
        long granny_pwngt31332_vertex_Position_get = grannyJNI.granny_pwngt31332_vertex_Position_get(this.swigCPtr);
        if (granny_pwngt31332_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngt31332_vertex_Position_get, false);
    }

    /* renamed from: n */
    public void mo19639n(long j) {
        grannyJNI.granny_pwngt31332_vertex_BoneIndex_set(this.swigCPtr, j);
    }

    /* renamed from: gC */
    public long mo19633gC() {
        return grannyJNI.granny_pwngt31332_vertex_BoneIndex_get(this.swigCPtr);
    }

    /* renamed from: h */
    public void mo19635h(C3159oW oWVar) {
        grannyJNI.granny_pwngt31332_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo19630cR() {
        long granny_pwngt31332_vertex_Normal_get = grannyJNI.granny_pwngt31332_vertex_Normal_get(this.swigCPtr);
        if (granny_pwngt31332_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngt31332_vertex_Normal_get, false);
    }

    /* renamed from: i */
    public void mo19636i(C3159oW oWVar) {
        grannyJNI.granny_pwngt31332_vertex_Tangent_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: gD */
    public C3159oW mo19634gD() {
        long granny_pwngt31332_vertex_Tangent_get = grannyJNI.granny_pwngt31332_vertex_Tangent_get(this.swigCPtr);
        if (granny_pwngt31332_vertex_Tangent_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngt31332_vertex_Tangent_get, false);
    }

    /* renamed from: j */
    public void mo19637j(C3159oW oWVar) {
        grannyJNI.granny_pwngt31332_vertex_UV_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: mv */
    public C3159oW mo19638mv() {
        long granny_pwngt31332_vertex_UV_get = grannyJNI.granny_pwngt31332_vertex_UV_get(this.swigCPtr);
        if (granny_pwngt31332_vertex_UV_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngt31332_vertex_UV_get, false);
    }

    /* renamed from: bZ */
    public C2685iY mo19629bZ(int i) {
        long granny_pwngt31332_vertex_get = grannyJNI.granny_pwngt31332_vertex_get(this.swigCPtr, i);
        if (granny_pwngt31332_vertex_get == 0) {
            return null;
        }
        return new C2685iY(granny_pwngt31332_vertex_get, false);
    }
}
