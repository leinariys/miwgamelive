package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.vH */
/* compiled from: a */
public class C3809vH {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3809vH(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3809vH() {
        this(grannyJNI.new_granny_png333_vertex(), true);
    }

    /* renamed from: a */
    public static long m40168a(C3809vH vHVar) {
        if (vHVar == null) {
            return 0;
        }
        return vHVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_png333_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo22555b(C3159oW oWVar) {
        grannyJNI.granny_png333_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo22563s() {
        long granny_png333_vertex_Position_get = grannyJNI.granny_png333_vertex_Position_get(this.swigCPtr);
        if (granny_png333_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_png333_vertex_Position_get, false);
    }

    /* renamed from: h */
    public void mo22561h(C3159oW oWVar) {
        grannyJNI.granny_png333_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo22556cR() {
        long granny_png333_vertex_Normal_get = grannyJNI.granny_png333_vertex_Normal_get(this.swigCPtr);
        if (granny_png333_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_png333_vertex_Normal_get, false);
    }

    /* renamed from: i */
    public void mo22562i(C3159oW oWVar) {
        grannyJNI.granny_png333_vertex_Tangent_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: gD */
    public C3159oW mo22560gD() {
        long granny_png333_vertex_Tangent_get = grannyJNI.granny_png333_vertex_Tangent_get(this.swigCPtr);
        if (granny_png333_vertex_Tangent_get == 0) {
            return null;
        }
        return new C3159oW(granny_png333_vertex_Tangent_get, false);
    }

    /* renamed from: eZ */
    public C3809vH mo22558eZ(int i) {
        long granny_png333_vertex_get = grannyJNI.granny_png333_vertex_get(this.swigCPtr, i);
        if (granny_png333_vertex_get == 0) {
            return null;
        }
        return new C3809vH(granny_png333_vertex_get, false);
    }
}
