package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aRL */
/* compiled from: a */
public class aRL {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aRL(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aRL() {
        this(grannyJNI.new_granny_pwngbt323332_vertex(), true);
    }

    /* renamed from: a */
    public static long m17735a(aRL arl) {
        if (arl == null) {
            return 0;
        }
        return arl.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pwngbt323332_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo11107b(C3159oW oWVar) {
        grannyJNI.granny_pwngbt323332_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo11118s() {
        long granny_pwngbt323332_vertex_Position_get = grannyJNI.granny_pwngbt323332_vertex_Position_get(this.swigCPtr);
        if (granny_pwngbt323332_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngbt323332_vertex_Position_get, false);
    }

    /* renamed from: a */
    public void mo11104a(C2382ef efVar) {
        grannyJNI.granny_pwngbt323332_vertex_BoneWeights_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cP */
    public C2382ef mo11108cP() {
        long granny_pwngbt323332_vertex_BoneWeights_get = grannyJNI.granny_pwngbt323332_vertex_BoneWeights_get(this.swigCPtr);
        if (granny_pwngbt323332_vertex_BoneWeights_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwngbt323332_vertex_BoneWeights_get, false);
    }

    /* renamed from: b */
    public void mo11106b(C2382ef efVar) {
        grannyJNI.granny_pwngbt323332_vertex_BoneIndices_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cQ */
    public C2382ef mo11109cQ() {
        long granny_pwngbt323332_vertex_BoneIndices_get = grannyJNI.granny_pwngbt323332_vertex_BoneIndices_get(this.swigCPtr);
        if (granny_pwngbt323332_vertex_BoneIndices_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwngbt323332_vertex_BoneIndices_get, false);
    }

    /* renamed from: h */
    public void mo11114h(C3159oW oWVar) {
        grannyJNI.granny_pwngbt323332_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo11110cR() {
        long granny_pwngbt323332_vertex_Normal_get = grannyJNI.granny_pwngbt323332_vertex_Normal_get(this.swigCPtr);
        if (granny_pwngbt323332_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngbt323332_vertex_Normal_get, false);
    }

    /* renamed from: i */
    public void mo11115i(C3159oW oWVar) {
        grannyJNI.granny_pwngbt323332_vertex_Tangent_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: gD */
    public C3159oW mo11113gD() {
        long granny_pwngbt323332_vertex_Tangent_get = grannyJNI.granny_pwngbt323332_vertex_Tangent_get(this.swigCPtr);
        if (granny_pwngbt323332_vertex_Tangent_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngbt323332_vertex_Tangent_get, false);
    }

    /* renamed from: w */
    public void mo11119w(C3159oW oWVar) {
        grannyJNI.granny_pwngbt323332_vertex_Binormal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW amC() {
        long granny_pwngbt323332_vertex_Binormal_get = grannyJNI.granny_pwngbt323332_vertex_Binormal_get(this.swigCPtr);
        if (granny_pwngbt323332_vertex_Binormal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngbt323332_vertex_Binormal_get, false);
    }

    /* renamed from: j */
    public void mo11116j(C3159oW oWVar) {
        grannyJNI.granny_pwngbt323332_vertex_UV_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: mv */
    public C3159oW mo11117mv() {
        long granny_pwngbt323332_vertex_UV_get = grannyJNI.granny_pwngbt323332_vertex_UV_get(this.swigCPtr);
        if (granny_pwngbt323332_vertex_UV_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngbt323332_vertex_UV_get, false);
    }

    /* renamed from: Az */
    public aRL mo11103Az(int i) {
        long granny_pwngbt323332_vertex_get = grannyJNI.granny_pwngbt323332_vertex_get(this.swigCPtr, i);
        if (granny_pwngbt323332_vertex_get == 0) {
            return null;
        }
        return new aRL(granny_pwngbt323332_vertex_get, false);
    }
}
