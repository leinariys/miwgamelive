package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.ri */
/* compiled from: a */
public class C3505ri {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3505ri(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3505ri() {
        this(grannyJNI.new_granny_file_reader(), true);
    }

    /* renamed from: a */
    public static long m38588a(C3505ri riVar) {
        if (riVar == null) {
            return 0;
        }
        return riVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_file_reader(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getSourceFileName() {
        return grannyJNI.granny_file_reader_SourceFileName_get(this.swigCPtr);
    }

    public void setSourceFileName(String str) {
        grannyJNI.granny_file_reader_SourceFileName_set(this.swigCPtr, str);
    }

    /* renamed from: O */
    public void mo21789O(int i) {
        grannyJNI.granny_file_reader_SourceLineNumber_set(this.swigCPtr, i);
    }

    /* renamed from: fV */
    public int mo21796fV() {
        return grannyJNI.granny_file_reader_SourceLineNumber_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo21793b(C2874lI lIVar) {
        grannyJNI.granny_file_reader_CloseFileReaderCallback_set(this.swigCPtr, C2874lI.m34779a(lIVar));
    }

    /* renamed from: Yo */
    public C2874lI mo21790Yo() {
        long granny_file_reader_CloseFileReaderCallback_get = grannyJNI.granny_file_reader_CloseFileReaderCallback_get(this.swigCPtr);
        if (granny_file_reader_CloseFileReaderCallback_get == 0) {
            return null;
        }
        return new C2874lI(granny_file_reader_CloseFileReaderCallback_get, false);
    }

    /* renamed from: a */
    public void mo21792a(C5795aab aab) {
        grannyJNI.granny_file_reader_ReadAtMostCallback_set(this.swigCPtr, C5795aab.m19538b(aab));
    }

    /* renamed from: Yp */
    public C5795aab mo21791Yp() {
        long granny_file_reader_ReadAtMostCallback_get = grannyJNI.granny_file_reader_ReadAtMostCallback_get(this.swigCPtr);
        if (granny_file_reader_ReadAtMostCallback_get == 0) {
            return null;
        }
        return new C5795aab(granny_file_reader_ReadAtMostCallback_get, false);
    }

    /* renamed from: ei */
    public C3505ri mo21795ei(int i) {
        long granny_file_reader_get = grannyJNI.granny_file_reader_get(this.swigCPtr, i);
        if (granny_file_reader_get == 0) {
            return null;
        }
        return new C3505ri(granny_file_reader_get, false);
    }
}
