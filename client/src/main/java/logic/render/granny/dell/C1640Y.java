package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.Y */
/* compiled from: a */
public class C1640Y {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1640Y(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1640Y() {
        this(grannyJNI.new_granny_tangent_frame(), true);
    }

    /* renamed from: a */
    public static long m11776a(C1640Y y) {
        if (y == null) {
            return 0;
        }
        return y.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_tangent_frame(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: e */
    public void mo6911e(C3159oW oWVar) {
        grannyJNI.granny_tangent_frame_U_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cM */
    public C3159oW mo6907cM() {
        long granny_tangent_frame_U_get = grannyJNI.granny_tangent_frame_U_get(this.swigCPtr);
        if (granny_tangent_frame_U_get == 0) {
            return null;
        }
        return new C3159oW(granny_tangent_frame_U_get, false);
    }

    /* renamed from: f */
    public void mo6912f(C3159oW oWVar) {
        grannyJNI.granny_tangent_frame_V_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cN */
    public C3159oW mo6908cN() {
        long granny_tangent_frame_V_get = grannyJNI.granny_tangent_frame_V_get(this.swigCPtr);
        if (granny_tangent_frame_V_get == 0) {
            return null;
        }
        return new C3159oW(granny_tangent_frame_V_get, false);
    }

    /* renamed from: g */
    public void mo6914g(C3159oW oWVar) {
        grannyJNI.granny_tangent_frame_N_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cO */
    public C3159oW mo6909cO() {
        long granny_tangent_frame_N_get = grannyJNI.granny_tangent_frame_N_get(this.swigCPtr);
        if (granny_tangent_frame_N_get == 0) {
            return null;
        }
        return new C3159oW(granny_tangent_frame_N_get, false);
    }

    /* renamed from: k */
    public C1640Y mo6915k(int i) {
        long granny_tangent_frame_get = grannyJNI.granny_tangent_frame_get(this.swigCPtr, i);
        if (granny_tangent_frame_get == 0) {
            return null;
        }
        return new C1640Y(granny_tangent_frame_get, false);
    }
}
