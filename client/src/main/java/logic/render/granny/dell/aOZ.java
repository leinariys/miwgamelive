package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aOZ */
/* compiled from: a */
public class aOZ {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aOZ(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aOZ() {
        this(grannyJNI.new_granny_stat_hud_animation_types(), true);
    }

    /* renamed from: b */
    public static long m16912b(aOZ aoz) {
        if (aoz == null) {
            return 0;
        }
        return aoz.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_stat_hud_animation_types(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: zT */
    public void mo10618zT(int i) {
        grannyJNI.granny_stat_hud_animation_types_TotalTrackCount_set(this.swigCPtr, i);
    }

    public int doa() {
        return grannyJNI.granny_stat_hud_animation_types_TotalTrackCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo10613b(C0353Eh eh) {
        grannyJNI.granny_stat_hud_animation_types_TrackTypes_set(this.swigCPtr, C0353Eh.m3020a(eh));
    }

    public C0353Eh dob() {
        long granny_stat_hud_animation_types_TrackTypes_get = grannyJNI.granny_stat_hud_animation_types_TrackTypes_get(this.swigCPtr);
        if (granny_stat_hud_animation_types_TrackTypes_get == 0) {
            return null;
        }
        return new C0353Eh(granny_stat_hud_animation_types_TrackTypes_get, false);
    }

    /* renamed from: zU */
    public aOZ mo10619zU(int i) {
        long granny_stat_hud_animation_types_get = grannyJNI.granny_stat_hud_animation_types_get(this.swigCPtr, i);
        if (granny_stat_hud_animation_types_get == 0) {
            return null;
        }
        return new aOZ(granny_stat_hud_animation_types_get, false);
    }
}
