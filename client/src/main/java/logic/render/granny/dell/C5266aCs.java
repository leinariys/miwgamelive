package logic.render.granny.dell;

/* renamed from: a.aCs  reason: case insensitive filesystem */
/* compiled from: a */
public final class C5266aCs {
    public static final C5266aCs huH = new C5266aCs("GrannyNoCompression");
    public static final C5266aCs huI = new C5266aCs("GrannyOodle0Compression");
    public static final C5266aCs huJ = new C5266aCs("GrannyOodle1Compression");
    public static final C5266aCs huK = new C5266aCs("GrannyOnePastLastCompressionType");
    private static C5266aCs[] huL = {huH, huI, huJ, huK};

    /* renamed from: pF */
    private static int f2490pF = 0;

    /* renamed from: pG */
    private final int f2491pG;

    /* renamed from: pH */
    private final String f2492pH;

    private C5266aCs(String str) {
        this.f2492pH = str;
        int i = f2490pF;
        f2490pF = i + 1;
        this.f2491pG = i;
    }

    private C5266aCs(String str, int i) {
        this.f2492pH = str;
        this.f2491pG = i;
        f2490pF = i + 1;
    }

    private C5266aCs(String str, C5266aCs acs) {
        this.f2492pH = str;
        this.f2491pG = acs.f2491pG;
        f2490pF = this.f2491pG + 1;
    }

    /* renamed from: xh */
    public static C5266aCs m13382xh(int i) {
        if (i < huL.length && i >= 0 && huL[i].f2491pG == i) {
            return huL[i];
        }
        for (int i2 = 0; i2 < huL.length; i2++) {
            if (huL[i2].f2491pG == i) {
                return huL[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C5266aCs.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f2491pG;
    }

    public String toString() {
        return this.f2492pH;
    }
}
