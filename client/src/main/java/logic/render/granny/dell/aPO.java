package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aPO */
/* compiled from: a */
public class aPO {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aPO(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aPO() {
        this(grannyJNI.new_granny_pwngt32332_vertex(), true);
    }

    /* renamed from: a */
    public static long m17165a(aPO apo) {
        if (apo == null) {
            return 0;
        }
        return apo.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pwngt32332_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo10749b(C3159oW oWVar) {
        grannyJNI.granny_pwngt32332_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo10760s() {
        long granny_pwngt32332_vertex_Position_get = grannyJNI.granny_pwngt32332_vertex_Position_get(this.swigCPtr);
        if (granny_pwngt32332_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngt32332_vertex_Position_get, false);
    }

    /* renamed from: a */
    public void mo10747a(C2382ef efVar) {
        grannyJNI.granny_pwngt32332_vertex_BoneWeights_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cP */
    public C2382ef mo10750cP() {
        long granny_pwngt32332_vertex_BoneWeights_get = grannyJNI.granny_pwngt32332_vertex_BoneWeights_get(this.swigCPtr);
        if (granny_pwngt32332_vertex_BoneWeights_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwngt32332_vertex_BoneWeights_get, false);
    }

    /* renamed from: b */
    public void mo10748b(C2382ef efVar) {
        grannyJNI.granny_pwngt32332_vertex_BoneIndices_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cQ */
    public C2382ef mo10751cQ() {
        long granny_pwngt32332_vertex_BoneIndices_get = grannyJNI.granny_pwngt32332_vertex_BoneIndices_get(this.swigCPtr);
        if (granny_pwngt32332_vertex_BoneIndices_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwngt32332_vertex_BoneIndices_get, false);
    }

    /* renamed from: h */
    public void mo10756h(C3159oW oWVar) {
        grannyJNI.granny_pwngt32332_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo10752cR() {
        long granny_pwngt32332_vertex_Normal_get = grannyJNI.granny_pwngt32332_vertex_Normal_get(this.swigCPtr);
        if (granny_pwngt32332_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngt32332_vertex_Normal_get, false);
    }

    /* renamed from: i */
    public void mo10757i(C3159oW oWVar) {
        grannyJNI.granny_pwngt32332_vertex_Tangent_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: gD */
    public C3159oW mo10755gD() {
        long granny_pwngt32332_vertex_Tangent_get = grannyJNI.granny_pwngt32332_vertex_Tangent_get(this.swigCPtr);
        if (granny_pwngt32332_vertex_Tangent_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngt32332_vertex_Tangent_get, false);
    }

    /* renamed from: j */
    public void mo10758j(C3159oW oWVar) {
        grannyJNI.granny_pwngt32332_vertex_UV_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: mv */
    public C3159oW mo10759mv() {
        long granny_pwngt32332_vertex_UV_get = grannyJNI.granny_pwngt32332_vertex_UV_get(this.swigCPtr);
        if (granny_pwngt32332_vertex_UV_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngt32332_vertex_UV_get, false);
    }

    /* renamed from: Ad */
    public aPO mo10746Ad(int i) {
        long granny_pwngt32332_vertex_get = grannyJNI.granny_pwngt32332_vertex_get(this.swigCPtr, i);
        if (granny_pwngt32332_vertex_get == 0) {
            return null;
        }
        return new aPO(granny_pwngt32332_vertex_get, false);
    }
}
