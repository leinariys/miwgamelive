package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import logic.render.granny.C5515aMh;
import logic.render.granny.aSM;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.OH */
/* compiled from: a */
public class C0970OH {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0970OH(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0970OH() {
        this(grannyJNI.new_granny_track_group(), true);
    }

    /* renamed from: e */
    public static long m7822e(C0970OH oh) {
        if (oh == null) {
            return 0;
        }
        return oh.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_track_group(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getName() {
        return grannyJNI.granny_track_group_Name_get(this.swigCPtr);
    }

    public void setName(String str) {
        grannyJNI.granny_track_group_Name_set(this.swigCPtr, str);
    }

    /* renamed from: mi */
    public void mo4359mi(int i) {
        grannyJNI.granny_track_group_VectorTrackCount_set(this.swigCPtr, i);
    }

    public int blZ() {
        return grannyJNI.granny_track_group_VectorTrackCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo4335a(C6766atK atk) {
        grannyJNI.granny_track_group_VectorTracks_set(this.swigCPtr, C6766atK.m25835b(atk));
    }

    public C6766atK bma() {
        long granny_track_group_VectorTracks_get = grannyJNI.granny_track_group_VectorTracks_get(this.swigCPtr);
        if (granny_track_group_VectorTracks_get == 0) {
            return null;
        }
        return new C6766atK(granny_track_group_VectorTracks_get, false);
    }

    /* renamed from: mj */
    public void mo4360mj(int i) {
        grannyJNI.granny_track_group_TransformTrackCount_set(this.swigCPtr, i);
    }

    public int bmb() {
        return grannyJNI.granny_track_group_TransformTrackCount_get(this.swigCPtr);
    }

    /* renamed from: g */
    public void mo4353g(C0777LF lf) {
        grannyJNI.granny_track_group_TransformTracks_set(this.swigCPtr, C0777LF.m6656f(lf));
    }

    public C0777LF bmc() {
        long granny_track_group_TransformTracks_get = grannyJNI.granny_track_group_TransformTracks_get(this.swigCPtr);
        if (granny_track_group_TransformTracks_get == 0) {
            return null;
        }
        return new C0777LF(granny_track_group_TransformTracks_get, false);
    }

    /* renamed from: mk */
    public void mo4361mk(int i) {
        grannyJNI.granny_track_group_TransformLODErrorCount_set(this.swigCPtr, i);
    }

    public int bmd() {
        return grannyJNI.granny_track_group_TransformLODErrorCount_get(this.swigCPtr);
    }

    /* renamed from: I */
    public void mo4332I(C3159oW oWVar) {
        grannyJNI.granny_track_group_TransformLODErrors_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW bme() {
        long granny_track_group_TransformLODErrors_get = grannyJNI.granny_track_group_TransformLODErrors_get(this.swigCPtr);
        if (granny_track_group_TransformLODErrors_get == 0) {
            return null;
        }
        return new C3159oW(granny_track_group_TransformLODErrors_get, false);
    }

    /* renamed from: ml */
    public void mo4362ml(int i) {
        grannyJNI.granny_track_group_TextTrackCount_set(this.swigCPtr, i);
    }

    public int bmf() {
        return grannyJNI.granny_track_group_TextTrackCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo4336b(C2982ma maVar) {
        grannyJNI.granny_track_group_TextTracks_set(this.swigCPtr, C2982ma.m35792a(maVar));
    }

    public C2982ma bmg() {
        long granny_track_group_TextTracks_get = grannyJNI.granny_track_group_TextTracks_get(this.swigCPtr);
        if (granny_track_group_TextTracks_get == 0) {
            return null;
        }
        return new C2982ma(granny_track_group_TextTracks_get, false);
    }

    /* renamed from: e */
    public void mo4351e(C5515aMh amh) {
        grannyJNI.granny_track_group_InitialPlacement_set(this.swigCPtr, C5515aMh.m16438f(amh));
    }

    public C5515aMh bmh() {
        long granny_track_group_InitialPlacement_get = grannyJNI.granny_track_group_InitialPlacement_get(this.swigCPtr);
        if (granny_track_group_InitialPlacement_get == 0) {
            return null;
        }
        return new C5515aMh(granny_track_group_InitialPlacement_get, false);
    }

    /* renamed from: lF */
    public void mo4358lF(int i) {
        grannyJNI.granny_track_group_Flags_set(this.swigCPtr, i);
    }

    public int getFlags() {
        return grannyJNI.granny_track_group_Flags_get(this.swigCPtr);
    }

    /* renamed from: J */
    public void mo4333J(C3159oW oWVar) {
        grannyJNI.granny_track_group_LoopTranslation_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW bmi() {
        long granny_track_group_LoopTranslation_get = grannyJNI.granny_track_group_LoopTranslation_get(this.swigCPtr);
        if (granny_track_group_LoopTranslation_get == 0) {
            return null;
        }
        return new C3159oW(granny_track_group_LoopTranslation_get, false);
    }

    /* renamed from: c */
    public void mo4349c(C2720jA jAVar) {
        grannyJNI.granny_track_group_PeriodicLoop_set(this.swigCPtr, C2720jA.m33633a(jAVar));
    }

    public C2720jA bmj() {
        long granny_track_group_PeriodicLoop_get = grannyJNI.granny_track_group_PeriodicLoop_get(this.swigCPtr);
        if (granny_track_group_PeriodicLoop_get == 0) {
            return null;
        }
        return new C2720jA(granny_track_group_PeriodicLoop_get, false);
    }

    /* renamed from: h */
    public void mo4356h(C0777LF lf) {
        grannyJNI.granny_track_group_RootMotion_set(this.swigCPtr, C0777LF.m6656f(lf));
    }

    public C0777LF bmk() {
        long granny_track_group_RootMotion_get = grannyJNI.granny_track_group_RootMotion_get(this.swigCPtr);
        if (granny_track_group_RootMotion_get == 0) {
            return null;
        }
        return new C0777LF(granny_track_group_RootMotion_get, false);
    }

    /* renamed from: a */
    public void mo4334a(aSM asm) {
        grannyJNI.granny_track_group_ExtendedData_set(this.swigCPtr, aSM.m18172c(asm));
    }

    /* renamed from: kM */
    public aSM mo4357kM() {
        long granny_track_group_ExtendedData_get = grannyJNI.granny_track_group_ExtendedData_get(this.swigCPtr);
        if (granny_track_group_ExtendedData_get == 0) {
            return null;
        }
        return new aSM(granny_track_group_ExtendedData_get, false);
    }

    /* renamed from: mm */
    public C0970OH mo4363mm(int i) {
        long granny_track_group_get = grannyJNI.granny_track_group_get(this.swigCPtr, i);
        if (granny_track_group_get == 0) {
            return null;
        }
        return new C0970OH(granny_track_group_get, false);
    }
}
