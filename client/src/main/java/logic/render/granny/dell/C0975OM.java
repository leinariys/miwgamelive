package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.OM */
/* compiled from: a */
public class C0975OM {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0975OM(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0975OM() {
        this(grannyJNI.new_granny_pwng3433_vertex(), true);
    }

    /* renamed from: a */
    public static long m7958a(C0975OM om) {
        if (om == null) {
            return 0;
        }
        return om.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pwng3433_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo4387b(C3159oW oWVar) {
        grannyJNI.granny_pwng3433_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo4397s() {
        long granny_pwng3433_vertex_Position_get = grannyJNI.granny_pwng3433_vertex_Position_get(this.swigCPtr);
        if (granny_pwng3433_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwng3433_vertex_Position_get, false);
    }

    /* renamed from: a */
    public void mo4385a(C2382ef efVar) {
        grannyJNI.granny_pwng3433_vertex_BoneWeights_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cP */
    public C2382ef mo4388cP() {
        long granny_pwng3433_vertex_BoneWeights_get = grannyJNI.granny_pwng3433_vertex_BoneWeights_get(this.swigCPtr);
        if (granny_pwng3433_vertex_BoneWeights_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwng3433_vertex_BoneWeights_get, false);
    }

    /* renamed from: b */
    public void mo4386b(C2382ef efVar) {
        grannyJNI.granny_pwng3433_vertex_BoneIndices_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cQ */
    public C2382ef mo4389cQ() {
        long granny_pwng3433_vertex_BoneIndices_get = grannyJNI.granny_pwng3433_vertex_BoneIndices_get(this.swigCPtr);
        if (granny_pwng3433_vertex_BoneIndices_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwng3433_vertex_BoneIndices_get, false);
    }

    /* renamed from: h */
    public void mo4394h(C3159oW oWVar) {
        grannyJNI.granny_pwng3433_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo4390cR() {
        long granny_pwng3433_vertex_Normal_get = grannyJNI.granny_pwng3433_vertex_Normal_get(this.swigCPtr);
        if (granny_pwng3433_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwng3433_vertex_Normal_get, false);
    }

    /* renamed from: i */
    public void mo4395i(C3159oW oWVar) {
        grannyJNI.granny_pwng3433_vertex_Tangent_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: gD */
    public C3159oW mo4393gD() {
        long granny_pwng3433_vertex_Tangent_get = grannyJNI.granny_pwng3433_vertex_Tangent_get(this.swigCPtr);
        if (granny_pwng3433_vertex_Tangent_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwng3433_vertex_Tangent_get, false);
    }

    /* renamed from: mn */
    public C0975OM mo4396mn(int i) {
        long granny_pwng3433_vertex_get = grannyJNI.granny_pwng3433_vertex_get(this.swigCPtr, i);
        if (granny_pwng3433_vertex_get == 0) {
            return null;
        }
        return new C0975OM(granny_pwng3433_vertex_get, false);
    }
}
