package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.KC */
/* compiled from: a */
public final class C0706KC {
    public static final C0706KC dqg = new C0706KC("GrannyIncludeComposites", grannyJNI.GrannyIncludeComposites_get());
    public static final C0706KC dqh = new C0706KC("GrannyExcludeComposites", grannyJNI.GrannyExcludeComposites_get());
    private static C0706KC[] dqi = {dqg, dqh};

    /* renamed from: pF */
    private static int f929pF = 0;

    /* renamed from: pG */
    private final int f930pG;

    /* renamed from: pH */
    private final String f931pH;

    private C0706KC(String str) {
        this.f931pH = str;
        int i = f929pF;
        f929pF = i + 1;
        this.f930pG = i;
    }

    private C0706KC(String str, int i) {
        this.f931pH = str;
        this.f930pG = i;
        f929pF = i + 1;
    }

    private C0706KC(String str, C0706KC kc) {
        this.f931pH = str;
        this.f930pG = kc.f930pG;
        f929pF = this.f930pG + 1;
    }

    /* renamed from: lv */
    public static C0706KC m6266lv(int i) {
        if (i < dqi.length && i >= 0 && dqi[i].f930pG == i) {
            return dqi[i];
        }
        for (int i2 = 0; i2 < dqi.length; i2++) {
            if (dqi[i2].f930pG == i) {
                return dqi[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C0706KC.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f930pG;
    }

    public String toString() {
        return this.f931pH;
    }
}
