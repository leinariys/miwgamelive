package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.zR */
/* compiled from: a */
public class C4098zR {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C4098zR(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C4098zR() {
        this(grannyJNI.new_granny_grn_section(), true);
    }

    /* renamed from: a */
    public static long m41559a(C4098zR zRVar) {
        if (zRVar == null) {
            return 0;
        }
        return zRVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_grn_section(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: dq */
    public void mo23323dq(long j) {
        grannyJNI.granny_grn_section_Format_set(this.swigCPtr, j);
    }

    public long aox() {
        return grannyJNI.granny_grn_section_Format_get(this.swigCPtr);
    }

    /* renamed from: dv */
    public void mo23324dv(long j) {
        grannyJNI.granny_grn_section_DataOffset_set(this.swigCPtr, j);
    }

    public long asE() {
        return grannyJNI.granny_grn_section_DataOffset_get(this.swigCPtr);
    }

    /* renamed from: dw */
    public void mo23325dw(long j) {
        grannyJNI.granny_grn_section_DataSize_set(this.swigCPtr, j);
    }

    public long asF() {
        return grannyJNI.granny_grn_section_DataSize_get(this.swigCPtr);
    }

    /* renamed from: dx */
    public void mo23326dx(long j) {
        grannyJNI.granny_grn_section_ExpandedDataSize_set(this.swigCPtr, j);
    }

    public long asG() {
        return grannyJNI.granny_grn_section_ExpandedDataSize_get(this.swigCPtr);
    }

    /* renamed from: dy */
    public void mo23327dy(long j) {
        grannyJNI.granny_grn_section_InternalAlignment_set(this.swigCPtr, j);
    }

    public long asH() {
        return grannyJNI.granny_grn_section_InternalAlignment_get(this.swigCPtr);
    }

    /* renamed from: dz */
    public void mo23328dz(long j) {
        grannyJNI.granny_grn_section_First16Bit_set(this.swigCPtr, j);
    }

    public long asI() {
        return grannyJNI.granny_grn_section_First16Bit_get(this.swigCPtr);
    }

    /* renamed from: dA */
    public void mo23317dA(long j) {
        grannyJNI.granny_grn_section_First8Bit_set(this.swigCPtr, j);
    }

    public long asJ() {
        return grannyJNI.granny_grn_section_First8Bit_get(this.swigCPtr);
    }

    /* renamed from: dB */
    public void mo23318dB(long j) {
        grannyJNI.granny_grn_section_PointerFixupArrayOffset_set(this.swigCPtr, j);
    }

    public long asK() {
        return grannyJNI.granny_grn_section_PointerFixupArrayOffset_get(this.swigCPtr);
    }

    /* renamed from: dC */
    public void mo23319dC(long j) {
        grannyJNI.granny_grn_section_PointerFixupArrayCount_set(this.swigCPtr, j);
    }

    public long asL() {
        return grannyJNI.granny_grn_section_PointerFixupArrayCount_get(this.swigCPtr);
    }

    /* renamed from: dD */
    public void mo23320dD(long j) {
        grannyJNI.granny_grn_section_MixedMarshallingFixupArrayOffset_set(this.swigCPtr, j);
    }

    public long asM() {
        return grannyJNI.granny_grn_section_MixedMarshallingFixupArrayOffset_get(this.swigCPtr);
    }

    /* renamed from: dE */
    public void mo23321dE(long j) {
        grannyJNI.granny_grn_section_MixedMarshallingFixupArrayCount_set(this.swigCPtr, j);
    }

    public long asN() {
        return grannyJNI.granny_grn_section_MixedMarshallingFixupArrayCount_get(this.swigCPtr);
    }

    /* renamed from: fP */
    public C4098zR mo23329fP(int i) {
        long granny_grn_section_get = grannyJNI.granny_grn_section_get(this.swigCPtr, i);
        if (granny_grn_section_get == 0) {
            return null;
        }
        return new C4098zR(granny_grn_section_get, false);
    }
}
