package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import logic.render.granny.C5448aJs;
import logic.render.granny.C6799atr;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.o */
/* compiled from: a */
public class C3120o {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3120o(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3120o() {
        this(grannyJNI.new_granny_camera(), true);
    }

    /* renamed from: a */
    public static long m36549a(C3120o oVar) {
        if (oVar == null) {
            return 0;
        }
        return oVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_camera(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo20888a(float f) {
        grannyJNI.granny_camera_WpOverHp_set(this.swigCPtr, f);
    }

    /* renamed from: h */
    public float mo20909h() {
        return grannyJNI.granny_camera_WpOverHp_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo20894b(float f) {
        grannyJNI.granny_camera_WrOverHr_set(this.swigCPtr, f);
    }

    /* renamed from: i */
    public float mo20910i() {
        return grannyJNI.granny_camera_WrOverHr_get(this.swigCPtr);
    }

    /* renamed from: c */
    public void mo20897c(float f) {
        grannyJNI.granny_camera_WwOverHw_set(this.swigCPtr, f);
    }

    /* renamed from: j */
    public float mo20911j() {
        return grannyJNI.granny_camera_WwOverHw_get(this.swigCPtr);
    }

    /* renamed from: d */
    public void mo20900d(float f) {
        grannyJNI.granny_camera_FOVY_set(this.swigCPtr, f);
    }

    /* renamed from: k */
    public float mo20912k() {
        return grannyJNI.granny_camera_FOVY_get(this.swigCPtr);
    }

    /* renamed from: e */
    public void mo20905e(float f) {
        grannyJNI.granny_camera_NearClipPlane_set(this.swigCPtr, f);
    }

    /* renamed from: l */
    public float mo20913l() {
        return grannyJNI.granny_camera_NearClipPlane_get(this.swigCPtr);
    }

    /* renamed from: f */
    public void mo20906f(float f) {
        grannyJNI.granny_camera_FarClipPlane_set(this.swigCPtr, f);
    }

    /* renamed from: m */
    public float mo20914m() {
        return grannyJNI.granny_camera_FarClipPlane_get(this.swigCPtr);
    }

    /* renamed from: g */
    public void mo20908g(float f) {
        grannyJNI.granny_camera_ZRangeEpsilon_set(this.swigCPtr, f);
    }

    /* renamed from: n */
    public float mo20915n() {
        return grannyJNI.granny_camera_ZRangeEpsilon_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo20891a(C2988mf mfVar) {
        grannyJNI.granny_camera_OutputZRange_set(this.swigCPtr, mfVar.swigValue());
    }

    /* renamed from: o */
    public C2988mf mo20916o() {
        return C2988mf.m35829cV(grannyJNI.granny_camera_OutputZRange_get(this.swigCPtr));
    }

    /* renamed from: a */
    public void mo20893a(boolean z) {
        grannyJNI.granny_camera_UseQuaternionOrientation_set(this.swigCPtr, z);
    }

    /* renamed from: p */
    public boolean mo20917p() {
        return grannyJNI.granny_camera_UseQuaternionOrientation_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo20892a(C3159oW oWVar) {
        grannyJNI.granny_camera_Orientation_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: q */
    public C3159oW mo20918q() {
        long granny_camera_Orientation_get = grannyJNI.granny_camera_Orientation_get(this.swigCPtr);
        if (granny_camera_Orientation_get == 0) {
            return null;
        }
        return new C3159oW(granny_camera_Orientation_get, false);
    }

    /* renamed from: a */
    public void mo20889a(C5448aJs ajs) {
        grannyJNI.granny_camera_OrientationMatrix_set(this.swigCPtr, C5448aJs.m15872b(ajs));
    }

    /* renamed from: r */
    public C5448aJs mo20919r() {
        long granny_camera_OrientationMatrix_get = grannyJNI.granny_camera_OrientationMatrix_get(this.swigCPtr);
        if (granny_camera_OrientationMatrix_get == 0) {
            return null;
        }
        return new C5448aJs(granny_camera_OrientationMatrix_get, false);
    }

    /* renamed from: b */
    public void mo20896b(C3159oW oWVar) {
        grannyJNI.granny_camera_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo20920s() {
        long granny_camera_Position_get = grannyJNI.granny_camera_Position_get(this.swigCPtr);
        if (granny_camera_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_camera_Position_get, false);
    }

    /* renamed from: c */
    public void mo20899c(C3159oW oWVar) {
        grannyJNI.granny_camera_ElevAzimRoll_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: t */
    public C3159oW mo20921t() {
        long granny_camera_ElevAzimRoll_get = grannyJNI.granny_camera_ElevAzimRoll_get(this.swigCPtr);
        if (granny_camera_ElevAzimRoll_get == 0) {
            return null;
        }
        return new C3159oW(granny_camera_ElevAzimRoll_get, false);
    }

    /* renamed from: d */
    public void mo20902d(C3159oW oWVar) {
        grannyJNI.granny_camera_Offset_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: u */
    public C3159oW mo20922u() {
        long granny_camera_Offset_get = grannyJNI.granny_camera_Offset_get(this.swigCPtr);
        if (granny_camera_Offset_get == 0) {
            return null;
        }
        return new C3159oW(granny_camera_Offset_get, false);
    }

    /* renamed from: a */
    public void mo20890a(C6799atr atr) {
        grannyJNI.granny_camera_View4x4_set(this.swigCPtr, C6799atr.m26063e(atr));
    }

    /* renamed from: v */
    public C6799atr mo20923v() {
        long granny_camera_View4x4_get = grannyJNI.granny_camera_View4x4_get(this.swigCPtr);
        if (granny_camera_View4x4_get == 0) {
            return null;
        }
        return new C6799atr(granny_camera_View4x4_get, false);
    }

    /* renamed from: b */
    public void mo20895b(C6799atr atr) {
        grannyJNI.granny_camera_InverseView4x4_set(this.swigCPtr, C6799atr.m26063e(atr));
    }

    /* renamed from: w */
    public C6799atr mo20924w() {
        long granny_camera_InverseView4x4_get = grannyJNI.granny_camera_InverseView4x4_get(this.swigCPtr);
        if (granny_camera_InverseView4x4_get == 0) {
            return null;
        }
        return new C6799atr(granny_camera_InverseView4x4_get, false);
    }

    /* renamed from: c */
    public void mo20898c(C6799atr atr) {
        grannyJNI.granny_camera_Projection4x4_set(this.swigCPtr, C6799atr.m26063e(atr));
    }

    /* renamed from: x */
    public C6799atr mo20925x() {
        long granny_camera_Projection4x4_get = grannyJNI.granny_camera_Projection4x4_get(this.swigCPtr);
        if (granny_camera_Projection4x4_get == 0) {
            return null;
        }
        return new C6799atr(granny_camera_Projection4x4_get, false);
    }

    /* renamed from: d */
    public void mo20901d(C6799atr atr) {
        grannyJNI.granny_camera_InverseProjection4x4_set(this.swigCPtr, C6799atr.m26063e(atr));
    }

    /* renamed from: y */
    public C6799atr mo20926y() {
        long granny_camera_InverseProjection4x4_get = grannyJNI.granny_camera_InverseProjection4x4_get(this.swigCPtr);
        if (granny_camera_InverseProjection4x4_get == 0) {
            return null;
        }
        return new C6799atr(granny_camera_InverseProjection4x4_get, false);
    }

    /* renamed from: e */
    public C3120o mo20904e(int i) {
        long granny_camera_get = grannyJNI.granny_camera_get(this.swigCPtr, i);
        if (granny_camera_get == 0) {
            return null;
        }
        return new C3120o(granny_camera_get, false);
    }
}
