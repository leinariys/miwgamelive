package logic.render.granny.dell;

/* renamed from: a.aNo  reason: case insensitive filesystem */
/* compiled from: a */
public class C5548aNo {
    private long swigCPtr;

    public C5548aNo(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5548aNo() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m16775b(C5548aNo ano) {
        if (ano == null) {
            return 0;
        }
        return ano.swigCPtr;
    }
}
