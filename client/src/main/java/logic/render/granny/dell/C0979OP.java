package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.OP */
/* compiled from: a */
public class C0979OP {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0979OP(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0979OP() {
        this(grannyJNI.new_granny_sample_context(), true);
    }

    /* renamed from: a */
    public static long m8025a(C0979OP op) {
        if (op == null) {
            return 0;
        }
        return op.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_sample_context(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: hd */
    public void mo4419hd(float f) {
        grannyJNI.granny_sample_context_LocalClock_set(this.swigCPtr, f);
    }

    public float bmE() {
        return grannyJNI.granny_sample_context_LocalClock_get(this.swigCPtr);
    }

    /* renamed from: he */
    public void mo4420he(float f) {
        grannyJNI.granny_sample_context_LocalDuration_set(this.swigCPtr, f);
    }

    public float bmF() {
        return grannyJNI.granny_sample_context_LocalDuration_get(this.swigCPtr);
    }

    /* renamed from: df */
    public void mo4416df(boolean z) {
        grannyJNI.granny_sample_context_UnderflowLoop_set(this.swigCPtr, z);
    }

    public boolean bmG() {
        return grannyJNI.granny_sample_context_UnderflowLoop_get(this.swigCPtr);
    }

    /* renamed from: dg */
    public void mo4417dg(boolean z) {
        grannyJNI.granny_sample_context_OverflowLoop_set(this.swigCPtr, z);
    }

    public boolean bmH() {
        return grannyJNI.granny_sample_context_OverflowLoop_get(this.swigCPtr);
    }

    /* renamed from: mo */
    public void mo4421mo(int i) {
        grannyJNI.granny_sample_context_FrameIndex_set(this.swigCPtr, i);
    }

    public int bmI() {
        return grannyJNI.granny_sample_context_FrameIndex_get(this.swigCPtr);
    }

    /* renamed from: mp */
    public C0979OP mo4422mp(int i) {
        long granny_sample_context_get = grannyJNI.granny_sample_context_get(this.swigCPtr, i);
        if (granny_sample_context_get == 0) {
            return null;
        }
        return new C0979OP(granny_sample_context_get, false);
    }
}
