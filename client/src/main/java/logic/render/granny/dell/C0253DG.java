package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.DG */
/* compiled from: a */
public class C0253DG {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0253DG(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0253DG() {
        this(grannyJNI.new_granny_pntg3323_vertex(), true);
    }

    /* renamed from: a */
    public static long m1952a(C0253DG dg) {
        if (dg == null) {
            return 0;
        }
        return dg.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pntg3323_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo1256b(C3159oW oWVar) {
        grannyJNI.granny_pntg3323_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo1266s() {
        long granny_pntg3323_vertex_Position_get = grannyJNI.granny_pntg3323_vertex_Position_get(this.swigCPtr);
        if (granny_pntg3323_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pntg3323_vertex_Position_get, false);
    }

    /* renamed from: h */
    public void mo1262h(C3159oW oWVar) {
        grannyJNI.granny_pntg3323_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo1257cR() {
        long granny_pntg3323_vertex_Normal_get = grannyJNI.granny_pntg3323_vertex_Normal_get(this.swigCPtr);
        if (granny_pntg3323_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pntg3323_vertex_Normal_get, false);
    }

    /* renamed from: j */
    public void mo1264j(C3159oW oWVar) {
        grannyJNI.granny_pntg3323_vertex_UV_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: mv */
    public C3159oW mo1265mv() {
        long granny_pntg3323_vertex_UV_get = grannyJNI.granny_pntg3323_vertex_UV_get(this.swigCPtr);
        if (granny_pntg3323_vertex_UV_get == 0) {
            return null;
        }
        return new C3159oW(granny_pntg3323_vertex_UV_get, false);
    }

    /* renamed from: i */
    public void mo1263i(C3159oW oWVar) {
        grannyJNI.granny_pntg3323_vertex_Tangent_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: gD */
    public C3159oW mo1260gD() {
        long granny_pntg3323_vertex_Tangent_get = grannyJNI.granny_pntg3323_vertex_Tangent_get(this.swigCPtr);
        if (granny_pntg3323_vertex_Tangent_get == 0) {
            return null;
        }
        return new C3159oW(granny_pntg3323_vertex_Tangent_get, false);
    }

    /* renamed from: gP */
    public C0253DG mo1261gP(int i) {
        long granny_pntg3323_vertex_get = grannyJNI.granny_pntg3323_vertex_get(this.swigCPtr, i);
        if (granny_pntg3323_vertex_get == 0) {
            return null;
        }
        return new C0253DG(granny_pntg3323_vertex_get, false);
    }
}
