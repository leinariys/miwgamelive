package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.apq  reason: case insensitive filesystem */
/* compiled from: a */
public class C6590apq {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6590apq(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6590apq() {
        this(grannyJNI.new_granny_unbound_track_mask(), true);
    }

    /* renamed from: a */
    public static long m25013a(C6590apq apq) {
        if (apq == null) {
            return 0;
        }
        return apq.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_unbound_track_mask(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: km */
    public void mo15499km(float f) {
        grannyJNI.granny_unbound_track_mask_DefaultWeight_set(this.swigCPtr, f);
    }

    public float cpz() {
        return grannyJNI.granny_unbound_track_mask_DefaultWeight_get(this.swigCPtr);
    }

    /* renamed from: tv */
    public void mo15500tv(int i) {
        grannyJNI.granny_unbound_track_mask_WeightCount_set(this.swigCPtr, i);
    }

    public int cpA() {
        return grannyJNI.granny_unbound_track_mask_WeightCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo15493b(C6059aff aff) {
        grannyJNI.granny_unbound_track_mask_Weights_set(this.swigCPtr, C6059aff.m21578a(aff));
    }

    public C6059aff cpB() {
        long granny_unbound_track_mask_Weights_get = grannyJNI.granny_unbound_track_mask_Weights_get(this.swigCPtr);
        if (granny_unbound_track_mask_Weights_get == 0) {
            return null;
        }
        return new C6059aff(granny_unbound_track_mask_Weights_get, false);
    }

    /* renamed from: tw */
    public C6590apq mo15501tw(int i) {
        long granny_unbound_track_mask_get = grannyJNI.granny_unbound_track_mask_get(this.swigCPtr, i);
        if (granny_unbound_track_mask_get == 0) {
            return null;
        }
        return new C6590apq(granny_unbound_track_mask_get, false);
    }
}
