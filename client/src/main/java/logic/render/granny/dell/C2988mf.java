package logic.render.granny.dell;

/* renamed from: a.mf */
/* compiled from: a */
public final class C2988mf {
    public static final C2988mf aBA = new C2988mf("GrannyCameraOutputZNegativeOneToOne");
    public static final C2988mf aBB = new C2988mf("GrannyCameraOutputZNegativeOneToZero");
    public static final C2988mf aBz = new C2988mf("GrannyCameraOutputZZeroToOne");
    private static C2988mf[] aBC = {aBz, aBA, aBB};
    /* renamed from: pF */
    private static int f8658pF = 0;

    /* renamed from: pG */
    private final int f8659pG;

    /* renamed from: pH */
    private final String f8660pH;

    private C2988mf(String str) {
        this.f8660pH = str;
        int i = f8658pF;
        f8658pF = i + 1;
        this.f8659pG = i;
    }

    private C2988mf(String str, int i) {
        this.f8660pH = str;
        this.f8659pG = i;
        f8658pF = i + 1;
    }

    private C2988mf(String str, C2988mf mfVar) {
        this.f8660pH = str;
        this.f8659pG = mfVar.f8659pG;
        f8658pF = this.f8659pG + 1;
    }

    /* renamed from: cV */
    public static C2988mf m35829cV(int i) {
        if (i < aBC.length && i >= 0 && aBC[i].f8659pG == i) {
            return aBC[i];
        }
        for (int i2 = 0; i2 < aBC.length; i2++) {
            if (aBC[i2].f8659pG == i) {
                return aBC[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C2988mf.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f8659pG;
    }

    public String toString() {
        return this.f8660pH;
    }
}
