package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.AQ */
/* compiled from: a */
public class C0031AQ {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0031AQ(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0031AQ() {
        this(grannyJNI.new_granny_grn_reference(), true);
    }

    /* renamed from: a */
    public static long m385a(C0031AQ aq) {
        if (aq == null) {
            return 0;
        }
        return aq.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_grn_reference(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: w */
    public void mo278w(long j) {
        grannyJNI.granny_grn_reference_SectionIndex_set(this.swigCPtr, j);
    }

    /* renamed from: pb */
    public long mo276pb() {
        return grannyJNI.granny_grn_reference_SectionIndex_get(this.swigCPtr);
    }

    public long getOffset() {
        return grannyJNI.granny_grn_reference_Offset_get(this.swigCPtr);
    }

    public void setOffset(long j) {
        grannyJNI.granny_grn_reference_Offset_set(this.swigCPtr, j);
    }

    /* renamed from: gu */
    public C0031AQ mo275gu(int i) {
        long granny_grn_reference_get = grannyJNI.granny_grn_reference_get(this.swigCPtr, i);
        if (granny_grn_reference_get == 0) {
            return null;
        }
        return new C0031AQ(granny_grn_reference_get, false);
    }
}
