package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.QZ */
/* compiled from: a */
public class C1126QZ {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1126QZ(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1126QZ() {
        this(grannyJNI.new_granny_mesh(), true);
    }

    /* renamed from: l */
    public static long m8909l(C1126QZ qz) {
        if (qz == null) {
            return 0;
        }
        return qz.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_mesh(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getName() {
        return grannyJNI.granny_mesh_Name_get(this.swigCPtr);
    }

    public void setName(String str) {
        grannyJNI.granny_mesh_Name_set(this.swigCPtr, str);
    }

    /* renamed from: b */
    public void mo5017b(C0251DE de) {
        grannyJNI.granny_mesh_PrimaryVertexData_set(this.swigCPtr, C0251DE.m1941a(de));
    }

    public C0251DE bqH() {
        long granny_mesh_PrimaryVertexData_get = grannyJNI.granny_mesh_PrimaryVertexData_get(this.swigCPtr);
        if (granny_mesh_PrimaryVertexData_get == 0) {
            return null;
        }
        return new C0251DE(granny_mesh_PrimaryVertexData_get, false);
    }

    /* renamed from: mZ */
    public void mo5032mZ(int i) {
        grannyJNI.granny_mesh_MorphTargetCount_set(this.swigCPtr, i);
    }

    public int bqI() {
        return grannyJNI.granny_mesh_MorphTargetCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo5014a(aMK amk) {
        grannyJNI.granny_mesh_MorphTargets_set(this.swigCPtr, aMK.m16345b(amk));
    }

    public aMK bqJ() {
        long granny_mesh_MorphTargets_get = grannyJNI.granny_mesh_MorphTargets_get(this.swigCPtr);
        if (granny_mesh_MorphTargets_get == 0) {
            return null;
        }
        return new aMK(granny_mesh_MorphTargets_get, false);
    }

    /* renamed from: c */
    public void mo5027c(C0580IB ib) {
        grannyJNI.granny_mesh_PrimaryTopology_set(this.swigCPtr, C0580IB.m5303b(ib));
    }

    public C0580IB bqK() {
        long granny_mesh_PrimaryTopology_get = grannyJNI.granny_mesh_PrimaryTopology_get(this.swigCPtr);
        if (granny_mesh_PrimaryTopology_get == 0) {
            return null;
        }
        return new C0580IB(granny_mesh_PrimaryTopology_get, false);
    }

    /* renamed from: na */
    public void mo5033na(int i) {
        grannyJNI.granny_mesh_MaterialBindingCount_set(this.swigCPtr, i);
    }

    public int bqL() {
        return grannyJNI.granny_mesh_MaterialBindingCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo5016a(C7006ayt ayt) {
        grannyJNI.granny_mesh_MaterialBindings_set(this.swigCPtr, C7006ayt.m27495b(ayt));
    }

    public C7006ayt bqM() {
        long granny_mesh_MaterialBindings_get = grannyJNI.granny_mesh_MaterialBindings_get(this.swigCPtr);
        if (granny_mesh_MaterialBindings_get == 0) {
            return null;
        }
        return new C7006ayt(granny_mesh_MaterialBindings_get, false);
    }

    /* renamed from: nb */
    public void mo5034nb(int i) {
        grannyJNI.granny_mesh_BoneBindingCount_set(this.swigCPtr, i);
    }

    public int bqN() {
        return grannyJNI.granny_mesh_BoneBindingCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo5018b(C0867MZ mz) {
        grannyJNI.granny_mesh_BoneBindings_set(this.swigCPtr, C0867MZ.m7062a(mz));
    }

    public C0867MZ bqO() {
        long granny_mesh_BoneBindings_get = grannyJNI.granny_mesh_BoneBindings_get(this.swigCPtr);
        if (granny_mesh_BoneBindings_get == 0) {
            return null;
        }
        return new C0867MZ(granny_mesh_BoneBindings_get, false);
    }

    /* renamed from: a */
    public void mo5015a(aSM asm) {
        grannyJNI.granny_mesh_ExtendedData_set(this.swigCPtr, aSM.m18172c(asm));
    }

    /* renamed from: kM */
    public aSM mo5031kM() {
        long granny_mesh_ExtendedData_get = grannyJNI.granny_mesh_ExtendedData_get(this.swigCPtr);
        if (granny_mesh_ExtendedData_get == 0) {
            return null;
        }
        return new aSM(granny_mesh_ExtendedData_get, false);
    }

    /* renamed from: nc */
    public C1126QZ mo5035nc(int i) {
        long granny_mesh_get = grannyJNI.granny_mesh_get(this.swigCPtr, i);
        if (granny_mesh_get == 0) {
            return null;
        }
        return new C1126QZ(granny_mesh_get, false);
    }
}
