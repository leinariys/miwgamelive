package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.adz  reason: case insensitive filesystem */
/* compiled from: a */
public class C5975adz {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5975adz(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5975adz() {
        this(grannyJNI.new_granny_model(), true);
    }

    /* renamed from: e */
    public static long m21108e(C5975adz adz) {
        if (adz == null) {
            return 0;
        }
        return adz.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_model(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getName() {
        return grannyJNI.granny_model_Name_get(this.swigCPtr);
    }

    public void setName(String str) {
        grannyJNI.granny_model_Name_set(this.swigCPtr, str);
    }

    /* renamed from: b */
    public void mo13001b(C0913NP np) {
        grannyJNI.granny_model_Skeleton_set(this.swigCPtr, C0913NP.m7619a(np));
    }

    public C0913NP getSkeleton() {
        long granny_model_Skeleton_get = grannyJNI.granny_model_Skeleton_get(this.swigCPtr);
        if (granny_model_Skeleton_get == 0) {
            return null;
        }
        return new C0913NP(granny_model_Skeleton_get, false);
    }

    /* renamed from: e */
    public void mo13006e(C5515aMh amh) {
        grannyJNI.granny_model_InitialPlacement_set(this.swigCPtr, C5515aMh.m16438f(amh));
    }

    public C5515aMh bmh() {
        long granny_model_InitialPlacement_get = grannyJNI.granny_model_InitialPlacement_get(this.swigCPtr);
        if (granny_model_InitialPlacement_get == 0) {
            return null;
        }
        return new C5515aMh(granny_model_InitialPlacement_get, false);
    }

    /* renamed from: pT */
    public void mo13010pT(int i) {
        grannyJNI.granny_model_MeshBindingCount_set(this.swigCPtr, i);
    }

    public int bRs() {
        return grannyJNI.granny_model_MeshBindingCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo13000a(aDP adp) {
        grannyJNI.granny_model_MeshBindings_set(this.swigCPtr, aDP.m13596b(adp));
    }

    public aDP bRt() {
        long granny_model_MeshBindings_get = grannyJNI.granny_model_MeshBindings_get(this.swigCPtr);
        if (granny_model_MeshBindings_get == 0) {
            return null;
        }
        return new aDP(granny_model_MeshBindings_get, false);
    }

    /* renamed from: pU */
    public C5975adz mo13011pU(int i) {
        long granny_model_get = grannyJNI.granny_model_get(this.swigCPtr, i);
        if (granny_model_get == 0) {
            return null;
        }
        return new C5975adz(granny_model_get, false);
    }
}
