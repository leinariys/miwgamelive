package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.Oe */
/* compiled from: a */
public class C0994Oe {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0994Oe(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0994Oe() {
        this(grannyJNI.new_granny_pixel_layout(), true);
    }

    /* renamed from: p */
    public static long m8106p(C0994Oe oe) {
        if (oe == null) {
            return 0;
        }
        return oe.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pixel_layout(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: ma */
    public void mo4470ma(int i) {
        grannyJNI.granny_pixel_layout_BytesPerPixel_set(this.swigCPtr, i);
    }

    public int bkZ() {
        return grannyJNI.granny_pixel_layout_BytesPerPixel_get(this.swigCPtr);
    }

    /* renamed from: i */
    public void mo4468i(C1845ab abVar) {
        grannyJNI.granny_pixel_layout_ShiftForComponent_set(this.swigCPtr, C1845ab.m19847a(abVar));
    }

    public C1845ab bla() {
        long granny_pixel_layout_ShiftForComponent_get = grannyJNI.granny_pixel_layout_ShiftForComponent_get(this.swigCPtr);
        if (granny_pixel_layout_ShiftForComponent_get == 0) {
            return null;
        }
        return new C1845ab(granny_pixel_layout_ShiftForComponent_get, false);
    }

    /* renamed from: j */
    public void mo4469j(C1845ab abVar) {
        grannyJNI.granny_pixel_layout_BitsForComponent_set(this.swigCPtr, C1845ab.m19847a(abVar));
    }

    public C1845ab blb() {
        long granny_pixel_layout_BitsForComponent_get = grannyJNI.granny_pixel_layout_BitsForComponent_get(this.swigCPtr);
        if (granny_pixel_layout_BitsForComponent_get == 0) {
            return null;
        }
        return new C1845ab(granny_pixel_layout_BitsForComponent_get, false);
    }

    /* renamed from: mb */
    public C0994Oe mo4471mb(int i) {
        long granny_pixel_layout_get = grannyJNI.granny_pixel_layout_get(this.swigCPtr, i);
        if (granny_pixel_layout_get == 0) {
            return null;
        }
        return new C0994Oe(granny_pixel_layout_get, false);
    }
}
