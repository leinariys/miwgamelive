package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.DE */
/* compiled from: a */
public class C0251DE {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0251DE(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0251DE() {
        this(grannyJNI.new_granny_vertex_data(), true);
    }

    /* renamed from: a */
    public static long m1941a(C0251DE de) {
        if (de == null) {
            return 0;
        }
        return de.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_vertex_data(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo1239a(C1731Za za) {
        grannyJNI.granny_vertex_data_VertexType_set(this.swigCPtr, C1731Za.m12101bi(za));
    }

    public C1731Za aGT() {
        long granny_vertex_data_VertexType_get = grannyJNI.granny_vertex_data_VertexType_get(this.swigCPtr);
        if (granny_vertex_data_VertexType_get == 0) {
            return null;
        }
        return new C1731Za(granny_vertex_data_VertexType_get, false);
    }

    /* renamed from: gK */
    public void mo1252gK(int i) {
        grannyJNI.granny_vertex_data_VertexCount_set(this.swigCPtr, i);
    }

    public int aGU() {
        return grannyJNI.granny_vertex_data_VertexCount_get(this.swigCPtr);
    }

    /* renamed from: e */
    public void mo1250e(C2382ef efVar) {
        grannyJNI.granny_vertex_data_Vertices_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    public C2382ef aGV() {
        long granny_vertex_data_Vertices_get = grannyJNI.granny_vertex_data_Vertices_get(this.swigCPtr);
        if (granny_vertex_data_Vertices_get == 0) {
            return null;
        }
        return new C2382ef(granny_vertex_data_Vertices_get, false);
    }

    /* renamed from: gL */
    public void mo1253gL(int i) {
        grannyJNI.granny_vertex_data_VertexComponentNameCount_set(this.swigCPtr, i);
    }

    public int aGW() {
        return grannyJNI.granny_vertex_data_VertexComponentNameCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo1241a(C6900avo avo) {
        grannyJNI.granny_vertex_data_VertexComponentNames_set(this.swigCPtr, C6900avo.m26731d(avo));
    }

    public C6900avo aGX() {
        long granny_vertex_data_VertexComponentNames_get = grannyJNI.granny_vertex_data_VertexComponentNames_get(this.swigCPtr);
        if (granny_vertex_data_VertexComponentNames_get == 0) {
            return null;
        }
        return new C6900avo(granny_vertex_data_VertexComponentNames_get, false);
    }

    /* renamed from: gM */
    public void mo1254gM(int i) {
        grannyJNI.granny_vertex_data_VertexAnnotationSetCount_set(this.swigCPtr, i);
    }

    public int aGY() {
        return grannyJNI.granny_vertex_data_VertexAnnotationSetCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo1240a(aSA asa) {
        grannyJNI.granny_vertex_data_VertexAnnotationSets_set(this.swigCPtr, aSA.m18148b(asa));
    }

    public aSA aGZ() {
        long granny_vertex_data_VertexAnnotationSets_get = grannyJNI.granny_vertex_data_VertexAnnotationSets_get(this.swigCPtr);
        if (granny_vertex_data_VertexAnnotationSets_get == 0) {
            return null;
        }
        return new aSA(granny_vertex_data_VertexAnnotationSets_get, false);
    }

    /* renamed from: gN */
    public C0251DE mo1255gN(int i) {
        long granny_vertex_data_get = grannyJNI.granny_vertex_data_get(this.swigCPtr, i);
        if (granny_vertex_data_get == 0) {
            return null;
        }
        return new C0251DE(granny_vertex_data_get, false);
    }
}
