package logic.render.granny;

import logic.render.granny.dell.*;
import utaikodom.render.granny.grannyJNI;

import java.nio.Buffer;

/* renamed from: a.HO */
/* compiled from: a */
public class AdapterGrannyJNI {

    /* renamed from: a */
    public static String m3941a(C6900avo avo, int i) {
        return grannyJNI.char_pointer_array_getitem(C6900avo.m26731d(avo), i);
    }

    /* renamed from: a */
    public static C6396amE m3853a(int i, Buffer buffer) {
        long GrannyReadEntireFileFromMemory = grannyJNI.GrannyReadEntireFileFromMemory(i, buffer);
        if (GrannyReadEntireFileFromMemory == 0) {
            return null;
        }
        return new C6396amE(GrannyReadEntireFileFromMemory, false);
    }

    /* renamed from: e */
    public static C2267dS m4834e(C6396amE ame) {
        long GrannyGetFileInfo = grannyJNI.GrannyGetFileInfo(C6396amE.m23854g(ame));
        if (GrannyGetFileInfo == 0) {
            return null;
        }
        return new C2267dS(GrannyGetFileInfo, false);
    }

    /* renamed from: d */
    public static C1731Za m4799d(C1126QZ qz) {
        long GrannyGetMeshVertexType = grannyJNI.GrannyGetMeshVertexType(C1126QZ.m8909l(qz));
        if (GrannyGetMeshVertexType == 0) {
            return null;
        }
        return new C1731Za(GrannyGetMeshVertexType, false);
    }

    /* renamed from: a */
    public static C6604aqE m3867a(C1731Za za, C1731Za za2, C6304akQ akq, C1622Xl xl) {
        long GrannyNewMeshDeformer = grannyJNI.GrannyNewMeshDeformer(C1731Za.m12101bi(za), C1731Za.m12101bi(za2), akq.swigValue(), xl.swigValue());
        if (GrannyNewMeshDeformer == 0) {
            return null;
        }
        return new C6604aqE(GrannyNewMeshDeformer, false);
    }

    /* renamed from: a */
    public static C6796ato m3879a(C1126QZ qz, C0913NP np, C0913NP np2) {
        long GrannyNewMeshBinding = grannyJNI.GrannyNewMeshBinding(C1126QZ.m8909l(qz), C0913NP.m7619a(np), C0913NP.m7619a(np2));
        if (GrannyNewMeshBinding == 0) {
            return null;
        }
        return new C6796ato(GrannyNewMeshBinding, false);
    }

    /* renamed from: a */
    public static void m4054a(C1126QZ qz, int i, Buffer buffer) {
        grannyJNI.GrannyCopyMeshIndices(C1126QZ.m8909l(qz), i, buffer);
    }

    /* renamed from: h */
    public static int m4884h(C1126QZ qz) {
        return grannyJNI.GrannyGetMeshIndexCount(C1126QZ.m8909l(qz));
    }

    /* renamed from: a */
    public static C5975adz m3836a(C3521ru ruVar, int i) {
        long granny_model_array_getitem = grannyJNI.granny_model_array_getitem(C3521ru.m38665b(ruVar), i);
        if (granny_model_array_getitem == 0) {
            return null;
        }
        return new C5975adz(granny_model_array_getitem, false);
    }

    public static C1731Za aVf() {
        long pNG333VertexType = grannyJNI.getPNG333VertexType();
        if (pNG333VertexType == 0) {
            return null;
        }
        return new C1731Za(pNG333VertexType, false);
    }

    /* renamed from: e */
    public static int m4827e(C1126QZ qz) {
        return grannyJNI.GrannyGetMeshVertexCount(C1126QZ.m8909l(qz));
    }

    /* renamed from: a */
    public static C3242pY m3910a(C6917awF awf, int i) {
        long granny_animation_array_getitem = grannyJNI.granny_animation_array_getitem(C6917awF.m26850c(awf), i);
        if (granny_animation_array_getitem == 0) {
            return null;
        }
        return new C3242pY(granny_animation_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4168a(C5515aMh amh, Buffer buffer) {
        grannyJNI.GrannyBuildCompositeTransform4x4(C5515aMh.m16438f(amh), buffer);
    }

    /* renamed from: ht */
    public static C1731Za m4919ht(int i) {
        long granny_data_type_definition_allocate = grannyJNI.granny_data_type_definition_allocate(i);
        if (granny_data_type_definition_allocate == 0) {
            return null;
        }
        return new C1731Za(granny_data_type_definition_allocate, false);
    }

    /* renamed from: a */
    public static void m4055a(C1126QZ qz, C1731Za za, Buffer buffer) {
        grannyJNI.GrannyCopyMeshVertices(C1126QZ.m8909l(qz), C1731Za.m12101bi(za), buffer);
    }

    /* renamed from: a */
    public static void m4036a(C0938Nm nm) {
        grannyJNI.GrannyGetSystemSeconds(C0938Nm.m7718b(nm));
    }

    /* renamed from: e */
    public static C0913NP m4829e(C6676arY ary) {
        long GrannyGetSourceSkeleton = grannyJNI.GrannyGetSourceSkeleton(C6676arY.m25508i(ary));
        if (GrannyGetSourceSkeleton == 0) {
            return null;
        }
        return new C0913NP(GrannyGetSourceSkeleton, false);
    }

    /* renamed from: d */
    public static C6676arY m4807d(C5975adz adz) {
        long GrannyInstantiateModel = grannyJNI.GrannyInstantiateModel(C5975adz.m21108e(adz));
        if (GrannyInstantiateModel == 0) {
            return null;
        }
        return new C6676arY(GrannyInstantiateModel, false);
    }

    /* renamed from: b */
    public static boolean m4715b(C0913NP np, String str, Buffer buffer) {
        return grannyJNI.GrannyFindBoneByNameLowercase(C0913NP.m7619a(np), str, buffer);
    }

    /* renamed from: a */
    public static void m4441a(C3894wP wPVar, int i, Buffer buffer) {
        grannyJNI.getWorldPoseCompositeArray(C3894wP.m40635a(wPVar), i, buffer);
    }

    /* renamed from: a */
    public static void m3961a(int i, Buffer buffer, Buffer buffer2) {
        grannyJNI.getBoneTransform__SWIG_1(i, buffer, buffer2);
    }

    /* renamed from: b */
    public static void m4708b(C3894wP wPVar, int i, Buffer buffer) {
        grannyJNI.getWorldPoseArray(C3894wP.m40635a(wPVar), i, buffer);
    }

    /* renamed from: a */
    public static void m4019a(C0913NP np, int i, int i2, aUB aub, Buffer buffer, C3894wP wPVar) {
        grannyJNI.GrannyBuildWorldPose(C0913NP.m7619a(np), i, i2, aUB.m18499d(aub), buffer, C3894wP.m40635a(wPVar));
    }

    /* renamed from: a */
    public static C5515aMh m3797a(aUB aub, int i) {
        long GrannyGetLocalPoseTransform = grannyJNI.GrannyGetLocalPoseTransform(aUB.m18499d(aub), i);
        if (GrannyGetLocalPoseTransform == 0) {
            return null;
        }
        return new C5515aMh(GrannyGetLocalPoseTransform, false);
    }

    /* renamed from: a */
    public static void m4160a(C5515aMh amh, float f, float f2, float f3, float f4) {
        grannyJNI.setTransformOrientation(C5515aMh.m16438f(amh), f, f2, f3, f4);
    }


    /* renamed from: b */
    public static void m4660b(C6676arY ary, int i, int i2, aUB aub) {
        grannyJNI.GrannySampleModelAnimations(C6676arY.m25508i(ary), i, i2, aUB.m18499d(aub));
    }

    /* renamed from: a */
    public static void m4319a(C6676arY ary, float f) {
        grannyJNI.GrannySetModelClock(C6676arY.m25508i(ary), f);
    }

    /* renamed from: f */
    public static C1845ab m4856f(C6796ato ato) {
        long GrannyGetMeshBindingToBoneIndices = grannyJNI.GrannyGetMeshBindingToBoneIndices(C6796ato.m26060i(ato));
        if (GrannyGetMeshBindingToBoneIndices == 0) {
            return null;
        }
        return new C1845ab(GrannyGetMeshBindingToBoneIndices, false);
    }

    /* renamed from: a */
    public static void m4245a(C1845ab abVar, Buffer buffer, Buffer buffer2) {
        grannyJNI.getBoneTransform__SWIG_0(C1845ab.m19847a(abVar), buffer, buffer2);
    }

    /* renamed from: a */
    public static void m4347a(C6796ato ato, C3894wP wPVar, int i, int i2, Buffer buffer) {
        grannyJNI.GrannyBuildMeshBinding4x4Array(C6796ato.m26060i(ato), C3894wP.m40635a(wPVar), i, i2, buffer);
    }

    /* renamed from: c */
    public static int m4740c(C6796ato ato) {
        return grannyJNI.GrannyGetMeshBindingBoneCount(C6796ato.m26060i(ato));
    }

    /* renamed from: b */
    public static boolean m4724b(C6796ato ato) {
        return grannyJNI.GrannyMeshBindingIsTransferred(C6796ato.m26060i(ato));
    }

    /* renamed from: f */
    public static C5501aLt m4852f(C1126QZ qz) {
        long GrannyGetMeshVertices = grannyJNI.GrannyGetMeshVertices(C1126QZ.m8909l(qz));
        if (GrannyGetMeshVertices == 0) {
            return null;
        }
        return new C5501aLt(GrannyGetMeshVertices, false);
    }

    /* renamed from: a */
    public static void m4210a(aUB aub) {
        grannyJNI.GrannyFreeLocalPose(aUB.m18499d(aub));
    }

    /* renamed from: b */
    public static void m4707b(C3894wP wPVar) {
        grannyJNI.GrannyFreeWorldPose(C3894wP.m40635a(wPVar));
    }

    /* renamed from: c */
    public static void m4772c(C6676arY ary) {
        grannyJNI.GrannyFreeModelInstance(C6676arY.m25508i(ary));
    }

    /* renamed from: a */
    public static void m4317a(C6604aqE aqe, C1845ab abVar, Buffer buffer, int i, C5501aLt alt, Buffer buffer2) {
        grannyJNI.GrannyDeformVertices(C6604aqE.m25068b(aqe), C1845ab.m19847a(abVar), buffer, i, C5501aLt.m16286h(alt), buffer2);
    }

    /* renamed from: hu */
    public static aUB m4920hu(int i) {
        long GrannyNewLocalPose = grannyJNI.GrannyNewLocalPose(i);
        if (GrannyNewLocalPose == 0) {
            return null;
        }
        return new aUB(GrannyNewLocalPose, false);
    }

    /* renamed from: hx */
    public static C3894wP m4923hx(int i) {
        long GrannyNewWorldPose = grannyJNI.GrannyNewWorldPose(i);
        if (GrannyNewWorldPose == 0) {
            return null;
        }
        return new C3894wP(GrannyNewWorldPose, false);
    }

    /* renamed from: h */
    public static float m4883h(C5801aah aah, float f) {
        return grannyJNI.GrannyEaseControlOut(C5801aah.m19565x(aah), f);
    }

    /* renamed from: b */
    public static void m4646b(C5801aah aah, float f) {
        grannyJNI.GrannyCompleteControlAt(C5801aah.m19565x(aah), f);
    }

    /* renamed from: t */
    public static float m5122t(C5801aah aah) {
        return grannyJNI.GrannyGetControlEffectiveWeight(C5801aah.m19565x(aah));
    }

    /* renamed from: a */
    public static void m4237a(C5801aah aah, int i) {
        grannyJNI.GrannySetControlLoopCount(C5801aah.m19565x(aah), i);
    }

    /* renamed from: b */
    public static float m4568b(C5801aah aah, float f, boolean z) {
        return grannyJNI.GrannyEaseControlIn(C5801aah.m19565x(aah), f, z);
    }

    /* renamed from: a */
    public static C5801aah m3829a(float f, C3242pY pYVar, C6676arY ary) {
        long GrannyPlayControlledAnimation = grannyJNI.GrannyPlayControlledAnimation(f, C3242pY.m37170b(pYVar), C6676arY.m25508i(ary));
        if (GrannyPlayControlledAnimation == 0) {
            return null;
        }
        return new C5801aah(GrannyPlayControlledAnimation, false);
    }

    /* renamed from: c */
    public static void m4766c(C5801aah aah) {
        grannyJNI.GrannyFreeControl(C5801aah.m19565x(aah));
    }

    /* renamed from: o */
    public static float m5098o(C5801aah aah) {
        return grannyJNI.GrannyGetControlDurationLeft(C5801aah.m19565x(aah));
    }

    /* renamed from: f */
    public static void m4861f(C5801aah aah, float f) {
        grannyJNI.GrannySetControlSpeed(C5801aah.m19565x(aah), f);
    }


    /* renamed from: g */
    public static boolean m4880g(C1126QZ qz) {
        return grannyJNI.GrannyMeshIsRigid(C1126QZ.m8909l(qz));
    }


//////////////////////////////


    /* renamed from: a */
    public static int m3691a(C1845ab abVar, int i) {
        return grannyJNI.int_pointer_get(C1845ab.m19847a(abVar), i);
    }

    /* renamed from: a */
    public static void m4242a(C1845ab abVar, int i, int i2) {
        grannyJNI.int_pointer_set(C1845ab.m19847a(abVar), i, i2);
    }

    /* renamed from: a */
    public static float m3675a(C3159oW oWVar, int i) {
        return grannyJNI.float_pointer_get(C3159oW.m36741q(oWVar), i);
    }

    /* renamed from: a */
    public static void m4412a(C3159oW oWVar, int i, float f) {
        grannyJNI.float_pointer_set(C3159oW.m36741q(oWVar), i, f);
    }

    /* renamed from: a */
    public static void m4167a(C5515aMh amh, C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3) {
        grannyJNI.GrannySetTransform(C5515aMh.m16438f(amh), C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3));
    }

    /* renamed from: b */
    public static void m4639b(C5515aMh amh, C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3) {
        grannyJNI.GrannySetTransformWithIdentityCheck(C5515aMh.m16438f(amh), C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3));
    }

    /* renamed from: b */
    public static void m4636b(C5515aMh amh) {
        grannyJNI.GrannyMakeIdentity(C5515aMh.m16438f(amh));
    }

    /* renamed from: c */
    public static void m4759c(C5515aMh amh) {
        grannyJNI.GrannyZeroTransform(C5515aMh.m16438f(amh));
    }

    /* renamed from: d */
    public static float m4794d(C5515aMh amh) {
        return grannyJNI.GrannyGetTransformDeterminant(C5515aMh.m16438f(amh));
    }

    /* renamed from: a */
    public static void m4413a(C3159oW oWVar, C5515aMh amh) {
        grannyJNI.GrannyTransformVectorInPlace(C3159oW.m36741q(oWVar), C5515aMh.m16438f(amh));
    }

    /* renamed from: b */
    public static void m4692b(C3159oW oWVar, C5515aMh amh) {
        grannyJNI.GrannyTransformVectorInPlaceTransposed(C3159oW.m36741q(oWVar), C5515aMh.m16438f(amh));
    }

    /* renamed from: a */
    public static void m4414a(C3159oW oWVar, C5515aMh amh, C3159oW oWVar2) {
        grannyJNI.GrannyTransformVector(C3159oW.m36741q(oWVar), C5515aMh.m16438f(amh), C3159oW.m36741q(oWVar2));
    }

    /* renamed from: c */
    public static void m4779c(C3159oW oWVar, C5515aMh amh) {
        grannyJNI.GrannyTransformPointInPlace(C3159oW.m36741q(oWVar), C5515aMh.m16438f(amh));
    }

    /* renamed from: b */
    public static void m4693b(C3159oW oWVar, C5515aMh amh, C3159oW oWVar2) {
        grannyJNI.GrannyTransformPoint(C3159oW.m36741q(oWVar), C5515aMh.m16438f(amh), C3159oW.m36741q(oWVar2));
    }

    /* renamed from: a */
    public static void m4163a(C5515aMh amh, C5515aMh amh2) {
        grannyJNI.GrannyPreMultiplyBy(C5515aMh.m16438f(amh), C5515aMh.m16438f(amh2));
    }

    /* renamed from: b */
    public static void m4638b(C5515aMh amh, C5515aMh amh2) {
        grannyJNI.GrannyPostMultiplyBy(C5515aMh.m16438f(amh), C5515aMh.m16438f(amh2));
    }

    /* renamed from: a */
    public static void m4165a(C5515aMh amh, C5515aMh amh2, C5515aMh amh3) {
        grannyJNI.GrannyMultiply(C5515aMh.m16438f(amh), C5515aMh.m16438f(amh2), C5515aMh.m16438f(amh3));
    }

    /* renamed from: a */
    public static void m4164a(C5515aMh amh, C5515aMh amh2, float f, C5515aMh amh3) {
        grannyJNI.GrannyLinearBlendTransform(C5515aMh.m16438f(amh), C5515aMh.m16438f(amh2), f, C5515aMh.m16438f(amh3));
    }

    /* renamed from: c */
    public static void m4761c(C5515aMh amh, C5515aMh amh2) {
        grannyJNI.GrannyBuildInverse(C5515aMh.m16438f(amh), C5515aMh.m16438f(amh2));
    }

    /* renamed from: c */
    public static void m4762c(C5515aMh amh, C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3) {
        grannyJNI.GrannySimilarityTransform(C5515aMh.m16438f(amh), C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3));
    }

    /* renamed from: a */
    public static void m4161a(C5515aMh amh, int i, C3159oW oWVar) {
        grannyJNI.GrannyBuildCompositeTransform(C5515aMh.m16438f(amh), i, C3159oW.m36741q(oWVar));
    }


    /* renamed from: a */
    public static void m4166a(C5515aMh amh, C3159oW oWVar) {
        grannyJNI.GrannyBuildCompositeTransform4x3(C5515aMh.m16438f(amh), C3159oW.m36741q(oWVar));
    }

    /* renamed from: a */
    public static void m4257a(C5975adz adz, C3159oW oWVar) {
        grannyJNI.GrannyGetModelInitialPlacement4x4(C5975adz.m21108e(adz), C3159oW.m36741q(oWVar));
    }

    /* renamed from: c */
    public static void m4757c(C1731Za za) {
        grannyJNI.GrannyModelMeshBindingType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSr() {
        long GrannyModelMeshBindingType_get = grannyJNI.GrannyModelMeshBindingType_get();
        if (GrannyModelMeshBindingType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyModelMeshBindingType_get, false);
    }

    /* renamed from: d */
    public static void m4810d(C1731Za za) {
        grannyJNI.GrannyModelType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSs() {
        long GrannyModelType_get = grannyJNI.GrannyModelType_get();
        if (GrannyModelType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyModelType_get, false);
    }

    /* renamed from: a */
    public static C6595apv m3866a(C6676arY ary) {
        long GrannyModelControlsBegin = grannyJNI.GrannyModelControlsBegin(C6676arY.m25508i(ary));
        if (GrannyModelControlsBegin == 0) {
            return null;
        }
        return new C6595apv(GrannyModelControlsBegin, false);
    }

    /* renamed from: a */
    public static C6595apv m3865a(C6595apv apv) {
        long GrannyModelControlsNext = grannyJNI.GrannyModelControlsNext(C6595apv.m25028g(apv));
        if (GrannyModelControlsNext == 0) {
            return null;
        }
        return new C6595apv(GrannyModelControlsNext, false);
    }

    /* renamed from: b */
    public static C6595apv m4592b(C6676arY ary) {
        long GrannyModelControlsEnd = grannyJNI.GrannyModelControlsEnd(C6676arY.m25508i(ary));
        if (GrannyModelControlsEnd == 0) {
            return null;
        }
        return new C6595apv(GrannyModelControlsEnd, false);
    }

    /* renamed from: a */
    public static C6595apv m3864a(C5801aah aah) {
        long GrannyControlModelsBegin = grannyJNI.GrannyControlModelsBegin(C5801aah.m19565x(aah));
        if (GrannyControlModelsBegin == 0) {
            return null;
        }
        return new C6595apv(GrannyControlModelsBegin, false);
    }

    /* renamed from: b */
    public static C6595apv m4591b(C6595apv apv) {
        long GrannyControlModelsNext = grannyJNI.GrannyControlModelsNext(C6595apv.m25028g(apv));
        if (GrannyControlModelsNext == 0) {
            return null;
        }
        return new C6595apv(GrannyControlModelsNext, false);
    }

    /* renamed from: b */
    public static C6595apv m4590b(C5801aah aah) {
        long GrannyControlModelsEnd = grannyJNI.GrannyControlModelsEnd(C5801aah.m19565x(aah));
        if (GrannyControlModelsEnd == 0) {
            return null;
        }
        return new C6595apv(GrannyControlModelsEnd, false);
    }

    /* renamed from: c */
    public static C6676arY m4749c(C6595apv apv) {
        long GrannyGetModelInstanceFromBinding = grannyJNI.GrannyGetModelInstanceFromBinding(C6595apv.m25028g(apv));
        if (GrannyGetModelInstanceFromBinding == 0) {
            return null;
        }
        return new C6676arY(GrannyGetModelInstanceFromBinding, false);
    }

    /* renamed from: d */
    public static C5801aah m4801d(C6595apv apv) {
        long GrannyGetControlFromBinding = grannyJNI.GrannyGetControlFromBinding(C6595apv.m25028g(apv));
        if (GrannyGetControlFromBinding == 0) {
            return null;
        }
        return new C5801aah(GrannyGetControlFromBinding, false);
    }


    /* renamed from: d */
    public static C5975adz m4803d(C6676arY ary) {
        long GrannyGetSourceModel = grannyJNI.GrannyGetSourceModel(C6676arY.m25508i(ary));
        if (GrannyGetSourceModel == 0) {
            return null;
        }
        return new C5975adz(GrannyGetSourceModel, false);
    }


    /* renamed from: f */
    public static void m4863f(C6676arY ary) {
        grannyJNI.GrannyFreeCompletedModelControls(C6676arY.m25508i(ary));
    }

    /* renamed from: a */
    public static void m4324a(C6676arY ary, int i, int i2, aUB aub, float f, C1845ab abVar) {
        grannyJNI.GrannyAccumulateModelAnimationsLODSparse(C6676arY.m25508i(ary), i, i2, aUB.m18499d(aub), f, C1845ab.m19847a(abVar));
    }

    /* renamed from: b */
    public static void m4662b(C6676arY ary, int i, int i2, aUB aub, float f, C1845ab abVar) {
        grannyJNI.GrannySampleModelAnimationsLODSparse(C6676arY.m25508i(ary), i, i2, aUB.m18499d(aub), f, C1845ab.m19847a(abVar));
    }

    /* renamed from: a */
    public static boolean m4502a(C6676arY ary, C5801aah aah, int i, int i2, aUB aub, float f, C1845ab abVar) {
        return grannyJNI.GrannySampleSingleModelAnimationLODSparse(C6676arY.m25508i(ary), C5801aah.m19565x(aah), i, i2, aUB.m18499d(aub), f, C1845ab.m19847a(abVar));
    }

    /* renamed from: a */
    public static void m4326a(C6676arY ary, int i, Buffer buffer, aUB aub, C3894wP wPVar, float f) {
        grannyJNI.GrannySampleModelAnimationsAcceleratedLOD(C6676arY.m25508i(ary), i, buffer, aUB.m18499d(aub), C3894wP.m40635a(wPVar), f);
    }

    /* renamed from: a */
    public static void m4322a(C6676arY ary, int i, int i2, aUB aub) {
        grannyJNI.GrannyAccumulateModelAnimations(C6676arY.m25508i(ary), i, i2, aUB.m18499d(aub));
    }


    /* renamed from: a */
    public static boolean m4500a(C6676arY ary, C5801aah aah, int i, int i2, aUB aub) {
        return grannyJNI.GrannySampleSingleModelAnimation(C6676arY.m25508i(ary), C5801aah.m19565x(aah), i, i2, aUB.m18499d(aub));
    }

    /* renamed from: a */
    public static void m4325a(C6676arY ary, int i, Buffer buffer, aUB aub, C3894wP wPVar) {
        grannyJNI.GrannySampleModelAnimationsAccelerated(C6676arY.m25508i(ary), i, buffer, aUB.m18499d(aub), C3894wP.m40635a(wPVar));
    }

    /* renamed from: a */
    public static void m4323a(C6676arY ary, int i, int i2, aUB aub, float f) {
        grannyJNI.GrannyAccumulateModelAnimationsLOD(C6676arY.m25508i(ary), i, i2, aUB.m18499d(aub), f);
    }

    /* renamed from: b */
    public static void m4661b(C6676arY ary, int i, int i2, aUB aub, float f) {
        grannyJNI.GrannySampleModelAnimationsLOD(C6676arY.m25508i(ary), i, i2, aUB.m18499d(aub), f);
    }

    /* renamed from: a */
    public static boolean m4501a(C6676arY ary, C5801aah aah, int i, int i2, aUB aub, float f) {
        return grannyJNI.GrannySampleSingleModelAnimationLOD(C6676arY.m25508i(ary), C5801aah.m19565x(aah), i, i2, aUB.m18499d(aub), f);
    }

    /* renamed from: a */
    public static void m4320a(C6676arY ary, float f, C3159oW oWVar, C3159oW oWVar2, boolean z) {
        grannyJNI.GrannyGetRootMotionVectors(C6676arY.m25508i(ary), f, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), z);
    }

    /* renamed from: a */
    public static void m4418a(C3159oW oWVar, C3159oW oWVar2, long j, C3159oW oWVar3, C3159oW oWVar4, C3159oW oWVar5, C3159oW oWVar6) {
        grannyJNI.GrannyClipRootMotionVectors(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), j, C3159oW.m36741q(oWVar3), C3159oW.m36741q(oWVar4), C3159oW.m36741q(oWVar5), C3159oW.m36741q(oWVar6));
    }

    /* renamed from: a */
    public static void m4462a(Buffer buffer, C3159oW oWVar, C3159oW oWVar2, Buffer buffer2) {
        grannyJNI.GrannyApplyRootMotionVectorsToMatrix(buffer, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), buffer2);
    }

    /* renamed from: a */
    public static void m4219a(aUB aub, C3159oW oWVar, C3159oW oWVar2) {
        grannyJNI.GrannyApplyRootMotionVectorsToLocalPose(aUB.m18499d(aub), C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2));
    }

    /* renamed from: a */
    public static void m4321a(C6676arY ary, float f, Buffer buffer, Buffer buffer2, boolean z) {
        grannyJNI.GrannyUpdateModelMatrix(C6676arY.m25508i(ary), f, buffer, buffer2, z);
    }

    /* renamed from: g */
    public static C5269aCv m4873g(C6676arY ary) {
        long GrannyGetModelUserDataArray = grannyJNI.GrannyGetModelUserDataArray(C6676arY.m25508i(ary));
        if (GrannyGetModelUserDataArray == 0) {
            return null;
        }
        return new C5269aCv(GrannyGetModelUserDataArray, false);
    }

    public static C6676arY aSt() {
        long GrannyGetGlobalModelInstancesBegin = grannyJNI.GrannyGetGlobalModelInstancesBegin();
        if (GrannyGetGlobalModelInstancesBegin == 0) {
            return null;
        }
        return new C6676arY(GrannyGetGlobalModelInstancesBegin, false);
    }

    public static C6676arY aSu() {
        long GrannyGetGlobalModelInstancesEnd = grannyJNI.GrannyGetGlobalModelInstancesEnd();
        if (GrannyGetGlobalModelInstancesEnd == 0) {
            return null;
        }
        return new C6676arY(GrannyGetGlobalModelInstancesEnd, false);
    }

    /* renamed from: h */
    public static C6676arY m4889h(C6676arY ary) {
        long GrannyGetGlobalNextModelInstance = grannyJNI.GrannyGetGlobalNextModelInstance(C6676arY.m25508i(ary));
        if (GrannyGetGlobalNextModelInstance == 0) {
            return null;
        }
        return new C6676arY(GrannyGetGlobalNextModelInstance, false);
    }

    /* renamed from: e */
    public static int m4828e(C1731Za za) {
        return grannyJNI.GrannyGetMemberUnitSize(C1731Za.m12101bi(za));
    }

    /* renamed from: f */
    public static int m4851f(C1731Za za) {
        return grannyJNI.GrannyGetMemberTypeSize(C1731Za.m12101bi(za));
    }

    /* renamed from: g */
    public static int m4871g(C1731Za za) {
        return grannyJNI.GrannyGetTotalObjectSize(C1731Za.m12101bi(za));
    }

    /* renamed from: h */
    public static int m4885h(C1731Za za) {
        return grannyJNI.GrannyGetTotalTypeSize(C1731Za.m12101bi(za));
    }

    /* renamed from: a */
    public static String m3942a(C3278pt ptVar) {
        return grannyJNI.GrannyGetMemberTypeName(ptVar.swigValue());
    }

    /* renamed from: b */
    public static String m4598b(C3278pt ptVar) {
        return grannyJNI.GrannyGetMemberCTypeName(ptVar.swigValue());
    }

    /* renamed from: i */
    public static boolean m4932i(C1731Za za) {
        return grannyJNI.GrannyMemberHasPointers(C1731Za.m12101bi(za));
    }

    /* renamed from: j */
    public static boolean m4992j(C1731Za za) {
        return grannyJNI.GrannyTypeHasPointers(C1731Za.m12101bi(za));
    }

    /* renamed from: k */
    public static long m5046k(C1731Za za) {
        return grannyJNI.GrannyGetMemberMarshalling(C1731Za.m12101bi(za));
    }

    /* renamed from: l */
    public static long m5080l(C1731Za za) {
        return grannyJNI.GrannyGetObjectMarshalling(C1731Za.m12101bi(za));
    }

    public static boolean GrannyIsMixedMarshalling(long j) {
        return grannyJNI.GrannyIsMixedMarshalling(j);
    }

    /* renamed from: a */
    public static int m3687a(C1731Za za, C5501aLt alt) {
        return grannyJNI.GrannyMakeEmptyDataTypeMember(C1731Za.m12101bi(za), C5501aLt.m16286h(alt));
    }

    /* renamed from: b */
    public static int m4573b(C1731Za za, C5501aLt alt) {
        return grannyJNI.GrannyMakeEmptyDataTypeObject(C1731Za.m12101bi(za), C5501aLt.m16286h(alt));
    }

    /* renamed from: m */
    public static int m5087m(C1731Za za) {
        return grannyJNI.GrannyGetMemberArrayWidth(C1731Za.m12101bi(za));
    }

    /* renamed from: b */
    public static int m4577b(C3339qT qTVar) {
        return grannyJNI.GrannyGetTypeTableCount(C3339qT.m37523a(qTVar));
    }

    /* renamed from: a */
    public static boolean m4484a(C1731Za za, C1731Za za2) {
        return grannyJNI.GrannyDataTypesAreEqual(C1731Za.m12101bi(za), C1731Za.m12101bi(za2));
    }

    /* renamed from: b */
    public static boolean m4719b(C1731Za za, C1731Za za2) {
        return grannyJNI.GrannyDataTypesAreEqualWithNames(C1731Za.m12101bi(za), C1731Za.m12101bi(za2));
    }

    /* renamed from: a */
    public static boolean m4485a(C1731Za za, C1731Za za2, C0256DJ dj) {
        return grannyJNI.GrannyDataTypesAreEqualWithNameCallback(C1731Za.m12101bi(za), C1731Za.m12101bi(za2), C0256DJ.m1973a(dj));
    }

    /* renamed from: c */
    public static C1731Za m4745c(C1731Za za, C1731Za za2) {
        long GrannyDataTypeBeginsWith = grannyJNI.GrannyDataTypeBeginsWith(C1731Za.m12101bi(za), C1731Za.m12101bi(za2));
        if (GrannyDataTypeBeginsWith == 0) {
            return null;
        }
        return new C1731Za(GrannyDataTypeBeginsWith, false);
    }

    /* renamed from: a */
    public static void m4112a(C1731Za za, int i, C5501aLt alt) {
        grannyJNI.GrannyReverseTypeArray(C1731Za.m12101bi(za), i, C5501aLt.m16286h(alt));
    }

    /* renamed from: n */
    public static void m5095n(C1731Za za) {
        grannyJNI.GrannyEmptyType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSv() {
        long GrannyEmptyType_get = grannyJNI.GrannyEmptyType_get();
        if (GrannyEmptyType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyEmptyType_get, false);
    }

    /* renamed from: o */
    public static void m5102o(C1731Za za) {
        grannyJNI.GrannyStringType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSw() {
        long GrannyStringType_get = grannyJNI.GrannyStringType_get();
        if (GrannyStringType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyStringType_get, false);
    }

    /* renamed from: p */
    public static void m5106p(C1731Za za) {
        grannyJNI.GrannyInt16Type_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSx() {
        long GrannyInt16Type_get = grannyJNI.GrannyInt16Type_get();
        if (GrannyInt16Type_get == 0) {
            return null;
        }
        return new C1731Za(GrannyInt16Type_get, false);
    }

    /* renamed from: q */
    public static void m5112q(C1731Za za) {
        grannyJNI.GrannyInt32Type_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSy() {
        long GrannyInt32Type_get = grannyJNI.GrannyInt32Type_get();
        if (GrannyInt32Type_get == 0) {
            return null;
        }
        return new C1731Za(GrannyInt32Type_get, false);
    }

    /* renamed from: r */
    public static void m5116r(C1731Za za) {
        grannyJNI.GrannyUInt8Type_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSz() {
        long GrannyUInt8Type_get = grannyJNI.GrannyUInt8Type_get();
        if (GrannyUInt8Type_get == 0) {
            return null;
        }
        return new C1731Za(GrannyUInt8Type_get, false);
    }

    /* renamed from: s */
    public static void m5120s(C1731Za za) {
        grannyJNI.GrannyUInt16Type_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSA() {
        long GrannyUInt16Type_get = grannyJNI.GrannyUInt16Type_get();
        if (GrannyUInt16Type_get == 0) {
            return null;
        }
        return new C1731Za(GrannyUInt16Type_get, false);
    }

    /* renamed from: t */
    public static void m5124t(C1731Za za) {
        grannyJNI.GrannyUInt32Type_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSB() {
        long GrannyUInt32Type_get = grannyJNI.GrannyUInt32Type_get();
        if (GrannyUInt32Type_get == 0) {
            return null;
        }
        return new C1731Za(GrannyUInt32Type_get, false);
    }

    /* renamed from: u */
    public static void m5127u(C1731Za za) {
        grannyJNI.GrannyReal32Type_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSC() {
        long GrannyReal32Type_get = grannyJNI.GrannyReal32Type_get();
        if (GrannyReal32Type_get == 0) {
            return null;
        }
        return new C1731Za(GrannyReal32Type_get, false);
    }

    /* renamed from: v */
    public static void m5130v(C1731Za za) {
        grannyJNI.GrannyTripleType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSD() {
        long GrannyTripleType_get = grannyJNI.GrannyTripleType_get();
        if (GrannyTripleType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyTripleType_get, false);
    }

    /* renamed from: w */
    public static void m5133w(C1731Za za) {
        grannyJNI.GrannyQuadType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSE() {
        long GrannyQuadType_get = grannyJNI.GrannyQuadType_get();
        if (GrannyQuadType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyQuadType_get, false);
    }

    /* renamed from: x */
    public static void m5135x(C1731Za za) {
        grannyJNI.GrannyTransformType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSF() {
        long GrannyTransformType_get = grannyJNI.GrannyTransformType_get();
        if (GrannyTransformType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyTransformType_get, false);
    }

    /* renamed from: b */
    public static void m4710b(C3914wi wiVar) {
        grannyJNI.GrannyCurveInitializeFormat(C3914wi.m40719a(wiVar));
    }

    /* renamed from: c */
    public static boolean m4792c(C3914wi wiVar) {
        return grannyJNI.GrannyCurveFormatIsInitializedCorrectly(C3914wi.m40719a(wiVar));
    }

    /* renamed from: d */
    public static boolean m4825d(C3914wi wiVar) {
        return grannyJNI.GrannyCurveIsKeyframed(C3914wi.m40719a(wiVar));
    }

    /* renamed from: e */
    public static boolean m4849e(C3914wi wiVar) {
        return grannyJNI.GrannyCurveIsIdentity(C3914wi.m40719a(wiVar));
    }

    /* renamed from: f */
    public static boolean m4869f(C3914wi wiVar) {
        return grannyJNI.GrannyCurveIsConstantOrIdentity(C3914wi.m40719a(wiVar));
    }

    /* renamed from: g */
    public static boolean m4882g(C3914wi wiVar) {
        return grannyJNI.GrannyCurveIsConstantNotIdentity(C3914wi.m40719a(wiVar));
    }

    /* renamed from: h */
    public static int m4886h(C3914wi wiVar) {
        return grannyJNI.GrannyCurveGetKnotCount(C3914wi.m40719a(wiVar));
    }

    /* renamed from: i */
    public static int m4927i(C3914wi wiVar) {
        return grannyJNI.GrannyCurveGetDimension(C3914wi.m40719a(wiVar));
    }

    /* renamed from: j */
    public static int m4988j(C3914wi wiVar) {
        return grannyJNI.GrannyCurveGetDegree(C3914wi.m40719a(wiVar));
    }

    /* renamed from: k */
    public static C1731Za m5048k(C3914wi wiVar) {
        long GrannyCurveGetDataTypeDefinition = grannyJNI.GrannyCurveGetDataTypeDefinition(C3914wi.m40719a(wiVar));
        if (GrannyCurveGetDataTypeDefinition == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveGetDataTypeDefinition, false);
    }

    /* renamed from: a */
    public static C6098agS m3839a(C1731Za za, int i, int i2, int i3) {
        long GrannyBeginCurve = grannyJNI.GrannyBeginCurve(C1731Za.m12101bi(za), i, i2, i3);
        if (GrannyBeginCurve == 0) {
            return null;
        }
        return new C6098agS(GrannyBeginCurve, false);
    }

    /* renamed from: l */
    public static C6098agS m5082l(C3914wi wiVar) {
        long GrannyBeginCurveCopy = grannyJNI.GrannyBeginCurveCopy(C3914wi.m40719a(wiVar));
        if (GrannyBeginCurveCopy == 0) {
            return null;
        }
        return new C6098agS(GrannyBeginCurveCopy, false);
    }

    /* renamed from: a */
    public static void m4264a(C6098agS ags, C3159oW oWVar) {
        grannyJNI.GrannyPushCurveKnotArray(C6098agS.m22037e(ags), C3159oW.m36741q(oWVar));
    }

    /* renamed from: b */
    public static void m4651b(C6098agS ags, C3159oW oWVar) {
        grannyJNI.GrannyPushCurveControlArray(C6098agS.m22037e(ags), C3159oW.m36741q(oWVar));
    }

    /* renamed from: a */
    public static void m4263a(C6098agS ags, int i, int i2, C3159oW oWVar, C3159oW oWVar2) {
        grannyJNI.GrannyPushCurveSampleArrays(C6098agS.m22037e(ags), i, i2, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2));
    }

    /* renamed from: a */
    public static int m3692a(C6098agS ags) {
        return grannyJNI.GrannyGetResultingCurveSize(C6098agS.m22037e(ags));
    }

    /* renamed from: a */
    public static C3914wi m3928a(C6098agS ags, Buffer buffer) {
        long GrannyEndCurveInPlace = grannyJNI.GrannyEndCurveInPlace(C6098agS.m22037e(ags), buffer);
        if (GrannyEndCurveInPlace == 0) {
            return null;
        }
        return new C3914wi(GrannyEndCurveInPlace, false);
    }

    /* renamed from: b */
    public static C3914wi m4597b(C6098agS ags) {
        long GrannyEndCurve = grannyJNI.GrannyEndCurve(C6098agS.m22037e(ags));
        if (GrannyEndCurve == 0) {
            return null;
        }
        return new C3914wi(GrannyEndCurve, false);
    }

    /* renamed from: c */
    public static void m4770c(C6098agS ags) {
        grannyJNI.GrannyAbortCurveBuilder(C6098agS.m22037e(ags));
    }

    /* renamed from: m */
    public static void m5091m(C3914wi wiVar) {
        grannyJNI.GrannyFreeCurve(C3914wi.m40719a(wiVar));
    }

    /* renamed from: d */
    public static int m4797d(C6098agS ags) {
        return grannyJNI.GrannyGetResultingCurveDataSize(C6098agS.m22037e(ags));
    }

    /* renamed from: a */
    public static C3914wi m3927a(C6098agS ags, C3914wi wiVar, C5501aLt alt) {
        long GrannyEndCurveDataInPlace = grannyJNI.GrannyEndCurveDataInPlace(C6098agS.m22037e(ags), C3914wi.m40719a(wiVar), C5501aLt.m16286h(alt));
        if (GrannyEndCurveDataInPlace == 0) {
            return null;
        }
        return new C3914wi(GrannyEndCurveDataInPlace, false);
    }

    /* renamed from: n */
    public static boolean m5097n(C3914wi wiVar) {
        return grannyJNI.GrannyCurveIsTypeDaK32fC32f(C3914wi.m40719a(wiVar));
    }

    /* renamed from: o */
    public static C6567apT m5100o(C3914wi wiVar) {
        long GrannyCurveGetContentsOfDaK32fC32f = grannyJNI.GrannyCurveGetContentsOfDaK32fC32f(C3914wi.m40719a(wiVar));
        if (GrannyCurveGetContentsOfDaK32fC32f == 0) {
            return null;
        }
        return new C6567apT(GrannyCurveGetContentsOfDaK32fC32f, false);
    }

    /* renamed from: a */
    public static C3914wi m3930a(C3914wi wiVar, C3159oW oWVar) {
        long GrannyCurveConvertToDaK32fC32f = grannyJNI.GrannyCurveConvertToDaK32fC32f(C3914wi.m40719a(wiVar), C3159oW.m36741q(oWVar));
        if (GrannyCurveConvertToDaK32fC32f == 0) {
            return null;
        }
        return new C3914wi(GrannyCurveConvertToDaK32fC32f, false);
    }

    /* renamed from: p */
    public static int m5104p(C3914wi wiVar) {
        return grannyJNI.GrannyGetResultingDaK32fC32fCurveSize(C3914wi.m40719a(wiVar));
    }

    /* renamed from: a */
    public static C3914wi m3931a(C3914wi wiVar, Buffer buffer, C3159oW oWVar) {
        long GrannyCurveConvertToDaK32fC32fInPlace = grannyJNI.GrannyCurveConvertToDaK32fC32fInPlace(C3914wi.m40719a(wiVar), buffer, C3159oW.m36741q(oWVar));
        if (GrannyCurveConvertToDaK32fC32fInPlace == 0) {
            return null;
        }
        return new C3914wi(GrannyCurveConvertToDaK32fC32fInPlace, false);
    }

    /* renamed from: a */
    public static void m4450a(C3914wi wiVar, C6567apT apt, int i, int i2, int i3, C3159oW oWVar, C3159oW oWVar2) {
        grannyJNI.GrannyCurveMakeStaticDaK32fC32f(C3914wi.m40719a(wiVar), C6567apT.m24891a(apt), i, i2, i3, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2));
    }

    /* renamed from: a */
    public static float m3676a(C3914wi wiVar, int i, C3159oW oWVar, C3159oW oWVar2) {
        return grannyJNI.GrannyCurveExtractKnotValue(C3914wi.m40719a(wiVar), i, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2));
    }

    /* renamed from: a */
    public static void m4448a(C3914wi wiVar, int i, int i2, C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3) {
        grannyJNI.GrannyCurveExtractKnotValues(C3914wi.m40719a(wiVar), i, i2, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3));
    }

    /* renamed from: a */
    public static void m4447a(C3914wi wiVar, int i, int i2, C3159oW oWVar, C3159oW oWVar2) {
        grannyJNI.GrannyCurveSetAllKnotValues(C3914wi.m40719a(wiVar), i, i2, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2));
    }

    /* renamed from: a */
    public static void m4449a(C3914wi wiVar, int i, C3159oW oWVar, C3159oW oWVar2, C1845ab abVar) {
        grannyJNI.GrannyCurveScaleOffsetSwizzle(C3914wi.m40719a(wiVar), i, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C1845ab.m19847a(abVar));
    }

    /* renamed from: a */
    public static int m3703a(C3914wi wiVar, float f) {
        return grannyJNI.GrannyCurveFindKnot(C3914wi.m40719a(wiVar), f);
    }

    /* renamed from: a */
    public static int m3704a(C3914wi wiVar, float f, int i) {
        return grannyJNI.GrannyCurveFindCloseKnot(C3914wi.m40719a(wiVar), f, i);
    }

    /* renamed from: q */
    public static int m5111q(C3914wi wiVar) {
        return grannyJNI.GrannyCurveGetSize(C3914wi.m40719a(wiVar));
    }

    /* renamed from: y */
    public static void m5137y(C1731Za za) {
        grannyJNI.GrannyOldCurveType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSG() {
        long GrannyOldCurveType_get = grannyJNI.GrannyOldCurveType_get();
        if (GrannyOldCurveType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyOldCurveType_get, false);
    }

    /* renamed from: z */
    public static void m5139z(C1731Za za) {
        grannyJNI.GrannyCurve2Type_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSH() {
        long GrannyCurve2Type_get = grannyJNI.GrannyCurve2Type_get();
        if (GrannyCurve2Type_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurve2Type_get, false);
    }

    /* renamed from: A */
    public static void m3636A(C1731Za za) {
        grannyJNI.GrannyCurveDataHeaderType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSI() {
        long GrannyCurveDataHeaderType_get = grannyJNI.GrannyCurveDataHeaderType_get();
        if (GrannyCurveDataHeaderType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataHeaderType_get, false);
    }

    /* renamed from: B */
    public static void m3638B(C1731Za za) {
        grannyJNI.GrannyCurveDataDaKeyframes32fType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSJ() {
        long GrannyCurveDataDaKeyframes32fType_get = grannyJNI.GrannyCurveDataDaKeyframes32fType_get();
        if (GrannyCurveDataDaKeyframes32fType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataDaKeyframes32fType_get, false);
    }

    /* renamed from: C */
    public static void m3640C(C1731Za za) {
        grannyJNI.GrannyCurveDataDaK32fC32fType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSK() {
        long GrannyCurveDataDaK32fC32fType_get = grannyJNI.GrannyCurveDataDaK32fC32fType_get();
        if (GrannyCurveDataDaK32fC32fType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataDaK32fC32fType_get, false);
    }

    /* renamed from: D */
    public static void m3643D(C1731Za za) {
        grannyJNI.GrannyCurveDataDaK16uC16uType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSL() {
        long GrannyCurveDataDaK16uC16uType_get = grannyJNI.GrannyCurveDataDaK16uC16uType_get();
        if (GrannyCurveDataDaK16uC16uType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataDaK16uC16uType_get, false);
    }

    /* renamed from: E */
    public static void m3646E(C1731Za za) {
        grannyJNI.GrannyCurveDataDaK8uC8uType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSM() {
        long GrannyCurveDataDaK8uC8uType_get = grannyJNI.GrannyCurveDataDaK8uC8uType_get();
        if (GrannyCurveDataDaK8uC8uType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataDaK8uC8uType_get, false);
    }

    /* renamed from: F */
    public static void m3649F(C1731Za za) {
        grannyJNI.GrannyCurveDataD3K16uC16uType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSN() {
        long GrannyCurveDataD3K16uC16uType_get = grannyJNI.GrannyCurveDataD3K16uC16uType_get();
        if (GrannyCurveDataD3K16uC16uType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataD3K16uC16uType_get, false);
    }

    /* renamed from: G */
    public static void m3652G(C1731Za za) {
        grannyJNI.GrannyCurveDataD3K8uC8uType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSO() {
        long GrannyCurveDataD3K8uC8uType_get = grannyJNI.GrannyCurveDataD3K8uC8uType_get();
        if (GrannyCurveDataD3K8uC8uType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataD3K8uC8uType_get, false);
    }

    /* renamed from: H */
    public static void m3653H(C1731Za za) {
        grannyJNI.GrannyCurveDataD4nK16uC15uType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSP() {
        long GrannyCurveDataD4nK16uC15uType_get = grannyJNI.GrannyCurveDataD4nK16uC15uType_get();
        if (GrannyCurveDataD4nK16uC15uType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataD4nK16uC15uType_get, false);
    }

    /* renamed from: I */
    public static void m3654I(C1731Za za) {
        grannyJNI.GrannyCurveDataD4nK8uC7uType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSQ() {
        long GrannyCurveDataD4nK8uC7uType_get = grannyJNI.GrannyCurveDataD4nK8uC7uType_get();
        if (GrannyCurveDataD4nK8uC7uType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataD4nK8uC7uType_get, false);
    }

    /* renamed from: J */
    public static void m3655J(C1731Za za) {
        grannyJNI.GrannyCurveDataDaIdentityType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSR() {
        long GrannyCurveDataDaIdentityType_get = grannyJNI.GrannyCurveDataDaIdentityType_get();
        if (GrannyCurveDataDaIdentityType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataDaIdentityType_get, false);
    }

    /* renamed from: K */
    public static void m3656K(C1731Za za) {
        grannyJNI.GrannyCurveDataDaConstant32fType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSS() {
        long GrannyCurveDataDaConstant32fType_get = grannyJNI.GrannyCurveDataDaConstant32fType_get();
        if (GrannyCurveDataDaConstant32fType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataDaConstant32fType_get, false);
    }

    /* renamed from: L */
    public static void m3657L(C1731Za za) {
        grannyJNI.GrannyCurveDataD3Constant32fType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aST() {
        long GrannyCurveDataD3Constant32fType_get = grannyJNI.GrannyCurveDataD3Constant32fType_get();
        if (GrannyCurveDataD3Constant32fType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataD3Constant32fType_get, false);
    }

    /* renamed from: M */
    public static void m3658M(C1731Za za) {
        grannyJNI.GrannyCurveDataD4Constant32fType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSU() {
        long GrannyCurveDataD4Constant32fType_get = grannyJNI.GrannyCurveDataD4Constant32fType_get();
        if (GrannyCurveDataD4Constant32fType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataD4Constant32fType_get, false);
    }

    /* renamed from: N */
    public static void m3659N(C1731Za za) {
        grannyJNI.GrannyCurveDataD9I1K16uC16uType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSV() {
        long GrannyCurveDataD9I1K16uC16uType_get = grannyJNI.GrannyCurveDataD9I1K16uC16uType_get();
        if (GrannyCurveDataD9I1K16uC16uType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataD9I1K16uC16uType_get, false);
    }

    /* renamed from: O */
    public static void m3660O(C1731Za za) {
        grannyJNI.GrannyCurveDataD9I3K16uC16uType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSW() {
        long GrannyCurveDataD9I3K16uC16uType_get = grannyJNI.GrannyCurveDataD9I3K16uC16uType_get();
        if (GrannyCurveDataD9I3K16uC16uType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataD9I3K16uC16uType_get, false);
    }

    /* renamed from: P */
    public static void m3661P(C1731Za za) {
        grannyJNI.GrannyCurveDataD9I1K8uC8uType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSX() {
        long GrannyCurveDataD9I1K8uC8uType_get = grannyJNI.GrannyCurveDataD9I1K8uC8uType_get();
        if (GrannyCurveDataD9I1K8uC8uType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataD9I1K8uC8uType_get, false);
    }

    /* renamed from: Q */
    public static void m3662Q(C1731Za za) {
        grannyJNI.GrannyCurveDataD9I3K8uC8uType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSY() {
        long GrannyCurveDataD9I3K8uC8uType_get = grannyJNI.GrannyCurveDataD9I3K8uC8uType_get();
        if (GrannyCurveDataD9I3K8uC8uType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataD9I3K8uC8uType_get, false);
    }

    /* renamed from: R */
    public static void m3663R(C1731Za za) {
        grannyJNI.GrannyCurveDataD3I1K32fC32fType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aSZ() {
        long GrannyCurveDataD3I1K32fC32fType_get = grannyJNI.GrannyCurveDataD3I1K32fC32fType_get();
        if (GrannyCurveDataD3I1K32fC32fType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataD3I1K32fC32fType_get, false);
    }

    /* renamed from: S */
    public static void m3664S(C1731Za za) {
        grannyJNI.GrannyCurveDataD3I1K16uC16uType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aTa() {
        long GrannyCurveDataD3I1K16uC16uType_get = grannyJNI.GrannyCurveDataD3I1K16uC16uType_get();
        if (GrannyCurveDataD3I1K16uC16uType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataD3I1K16uC16uType_get, false);
    }

    /* renamed from: T */
    public static void m3665T(C1731Za za) {
        grannyJNI.GrannyCurveDataD3I1K8uC8uType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aTb() {
        long GrannyCurveDataD3I1K8uC8uType_get = grannyJNI.GrannyCurveDataD3I1K8uC8uType_get();
        if (GrannyCurveDataD3I1K8uC8uType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyCurveDataD3I1K8uC8uType_get, false);
    }

    /* renamed from: A */
    public static void m3637A(C3159oW oWVar) {
        grannyJNI.GrannyCurveIdentityPosition_set(C3159oW.m36741q(oWVar));
    }

    public static C3159oW aTc() {
        long GrannyCurveIdentityPosition_get = grannyJNI.GrannyCurveIdentityPosition_get();
        if (GrannyCurveIdentityPosition_get == 0) {
            return null;
        }
        return new C3159oW(GrannyCurveIdentityPosition_get, false);
    }

    /* renamed from: B */
    public static void m3639B(C3159oW oWVar) {
        grannyJNI.GrannyCurveIdentityNormal_set(C3159oW.m36741q(oWVar));
    }

    public static C3159oW aTd() {
        long GrannyCurveIdentityNormal_get = grannyJNI.GrannyCurveIdentityNormal_get();
        if (GrannyCurveIdentityNormal_get == 0) {
            return null;
        }
        return new C3159oW(GrannyCurveIdentityNormal_get, false);
    }

    /* renamed from: C */
    public static void m3641C(C3159oW oWVar) {
        grannyJNI.GrannyCurveIdentityOrientation_set(C3159oW.m36741q(oWVar));
    }

    public static C3159oW aTe() {
        long GrannyCurveIdentityOrientation_get = grannyJNI.GrannyCurveIdentityOrientation_get();
        if (GrannyCurveIdentityOrientation_get == 0) {
            return null;
        }
        return new C3159oW(GrannyCurveIdentityOrientation_get, false);
    }

    /* renamed from: D */
    public static void m3644D(C3159oW oWVar) {
        grannyJNI.GrannyCurveIdentityScaleShear_set(C3159oW.m36741q(oWVar));
    }

    public static C3159oW aTf() {
        long GrannyCurveIdentityScaleShear_get = grannyJNI.GrannyCurveIdentityScaleShear_get();
        if (GrannyCurveIdentityScaleShear_get == 0) {
            return null;
        }
        return new C3159oW(GrannyCurveIdentityScaleShear_get, false);
    }

    /* renamed from: E */
    public static void m3647E(C3159oW oWVar) {
        grannyJNI.GrannyCurveIdentityScale_set(C3159oW.m36741q(oWVar));
    }

    public static C3159oW aTg() {
        long GrannyCurveIdentityScale_get = grannyJNI.GrannyCurveIdentityScale_get();
        if (GrannyCurveIdentityScale_get == 0) {
            return null;
        }
        return new C3159oW(GrannyCurveIdentityScale_get, false);
    }

    /* renamed from: F */
    public static void m3650F(C3159oW oWVar) {
        grannyJNI.GrannyCurveIdentityShear_set(C3159oW.m36741q(oWVar));
    }

    public static C3159oW aTh() {
        long GrannyCurveIdentityShear_get = grannyJNI.GrannyCurveIdentityShear_get();
        if (GrannyCurveIdentityShear_get == 0) {
            return null;
        }
        return new C3159oW(GrannyCurveIdentityShear_get, false);
    }

    /* renamed from: a */
    public static C5350aFy m3779a(C0201CS cs, C5400aHw ahw) {
        long GrannyGetMaterialTextureByType = grannyJNI.GrannyGetMaterialTextureByType(C0201CS.m1752a(cs), ahw.swigValue());
        if (GrannyGetMaterialTextureByType == 0) {
            return null;
        }
        return new C5350aFy(GrannyGetMaterialTextureByType, false);
    }

    /* renamed from: a */
    public static C0201CS m3712a(C0201CS cs, String str) {
        long GrannyGetTexturedMaterialByChannelName = grannyJNI.GrannyGetTexturedMaterialByChannelName(C0201CS.m1752a(cs), str);
        if (GrannyGetTexturedMaterialByChannelName == 0) {
            return null;
        }
        return new C0201CS(GrannyGetTexturedMaterialByChannelName, false);
    }

    /* renamed from: b */
    public static C5350aFy m4581b(C0201CS cs, String str) {
        long GrannyGetMaterialTextureByChannelName = grannyJNI.GrannyGetMaterialTextureByChannelName(C0201CS.m1752a(cs), str);
        if (GrannyGetMaterialTextureByChannelName == 0) {
            return null;
        }
        return new C5350aFy(GrannyGetMaterialTextureByChannelName, false);
    }

    /* renamed from: U */
    public static void m3666U(C1731Za za) {
        grannyJNI.GrannyMaterialMapType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aTi() {
        long GrannyMaterialMapType_get = grannyJNI.GrannyMaterialMapType_get();
        if (GrannyMaterialMapType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyMaterialMapType_get, false);
    }

    /* renamed from: V */
    public static void m3667V(C1731Za za) {
        grannyJNI.GrannyMaterialType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aTj() {
        long GrannyMaterialType_get = grannyJNI.GrannyMaterialType_get();
        if (GrannyMaterialType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyMaterialType_get, false);
    }

    /* renamed from: a */
    public static boolean m4480a(C0994Oe oe, C0994Oe oe2) {
        return grannyJNI.GrannyPixelLayoutsAreEqual(C0994Oe.m8106p(oe), C0994Oe.m8106p(oe2));
    }

    /* renamed from: a */
    public static boolean m4479a(C0994Oe oe) {
        return grannyJNI.GrannyPixelLayoutHasAlpha(C0994Oe.m8106p(oe));
    }

    /* renamed from: a */
    public static void m4048a(C0994Oe oe, C1845ab abVar, C1845ab abVar2) {
        grannyJNI.GrannySetStockSpecification(C0994Oe.m8106p(oe), C1845ab.m19847a(abVar), C1845ab.m19847a(abVar2));
    }

    /* renamed from: a */
    public static void m4047a(C0994Oe oe, int i, int i2, int i3, int i4) {
        grannyJNI.GrannySetStockRGBASpecification(C0994Oe.m8106p(oe), i, i2, i3, i4);
    }

    /* renamed from: b */
    public static void m4625b(C0994Oe oe, int i, int i2, int i3, int i4) {
        grannyJNI.GrannySetStockBGRASpecification(C0994Oe.m8106p(oe), i, i2, i3, i4);
    }

    /* renamed from: b */
    public static void m4624b(C0994Oe oe) {
        grannyJNI.GrannySwapRGBAToBGRA(C0994Oe.m8106p(oe));
    }

    /* renamed from: a */
    public static void m3948a(int i, int i2, C0994Oe oe, int i3, C5501aLt alt, C0994Oe oe2, int i4, C5501aLt alt2) {
        grannyJNI.GrannyConvertPixelFormat(i, i2, C0994Oe.m8106p(oe), i3, C5501aLt.m16286h(alt), C0994Oe.m8106p(oe2), i4, C5501aLt.m16286h(alt2));
    }

    /* renamed from: a */
    public static void m3964a(long j, long j2, long j3, C5501aLt alt, C5501aLt alt2) {
        grannyJNI.GrannyARGB8888SwizzleNGC(j, j2, j3, C5501aLt.m16286h(alt), C5501aLt.m16286h(alt2));
    }

    /* renamed from: b */
    public static void m4602b(long j, long j2, long j3, C5501aLt alt, C5501aLt alt2) {
        grannyJNI.GrannyAll16SwizzleNGC(j, j2, j3, C5501aLt.m16286h(alt), C5501aLt.m16286h(alt2));
    }

    /* renamed from: W */
    public static void m3668W(C1731Za za) {
        grannyJNI.GrannyPixelLayoutType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aTk() {
        long GrannyPixelLayoutType_get = grannyJNI.GrannyPixelLayoutType_get();
        if (GrannyPixelLayoutType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPixelLayoutType_get, false);
    }

    /* renamed from: c */
    public static void m4755c(C0994Oe oe) {
        grannyJNI.GrannyRGB555PixelFormat_set(C0994Oe.m8106p(oe));
    }

    public static C0994Oe aTl() {
        long GrannyRGB555PixelFormat_get = grannyJNI.GrannyRGB555PixelFormat_get();
        if (GrannyRGB555PixelFormat_get == 0) {
            return null;
        }
        return new C0994Oe(GrannyRGB555PixelFormat_get, false);
    }

    /* renamed from: d */
    public static void m4809d(C0994Oe oe) {
        grannyJNI.GrannyRGB565PixelFormat_set(C0994Oe.m8106p(oe));
    }

    public static C0994Oe aTm() {
        long GrannyRGB565PixelFormat_get = grannyJNI.GrannyRGB565PixelFormat_get();
        if (GrannyRGB565PixelFormat_get == 0) {
            return null;
        }
        return new C0994Oe(GrannyRGB565PixelFormat_get, false);
    }

    /* renamed from: e */
    public static void m4835e(C0994Oe oe) {
        grannyJNI.GrannyRGBA5551PixelFormat_set(C0994Oe.m8106p(oe));
    }

    public static C0994Oe aTn() {
        long GrannyRGBA5551PixelFormat_get = grannyJNI.GrannyRGBA5551PixelFormat_get();
        if (GrannyRGBA5551PixelFormat_get == 0) {
            return null;
        }
        return new C0994Oe(GrannyRGBA5551PixelFormat_get, false);
    }

    /* renamed from: f */
    public static void m4859f(C0994Oe oe) {
        grannyJNI.GrannyRGBA4444PixelFormat_set(C0994Oe.m8106p(oe));
    }

    public static C0994Oe aTo() {
        long GrannyRGBA4444PixelFormat_get = grannyJNI.GrannyRGBA4444PixelFormat_get();
        if (GrannyRGBA4444PixelFormat_get == 0) {
            return null;
        }
        return new C0994Oe(GrannyRGBA4444PixelFormat_get, false);
    }

    /* renamed from: g */
    public static void m4875g(C0994Oe oe) {
        grannyJNI.GrannyRGB888PixelFormat_set(C0994Oe.m8106p(oe));
    }

    public static C0994Oe aTp() {
        long GrannyRGB888PixelFormat_get = grannyJNI.GrannyRGB888PixelFormat_get();
        if (GrannyRGB888PixelFormat_get == 0) {
            return null;
        }
        return new C0994Oe(GrannyRGB888PixelFormat_get, false);
    }

    /* renamed from: h */
    public static void m4890h(C0994Oe oe) {
        grannyJNI.GrannyRGBA8888PixelFormat_set(C0994Oe.m8106p(oe));
    }

    public static C0994Oe aTq() {
        long GrannyRGBA8888PixelFormat_get = grannyJNI.GrannyRGBA8888PixelFormat_get();
        if (GrannyRGBA8888PixelFormat_get == 0) {
            return null;
        }
        return new C0994Oe(GrannyRGBA8888PixelFormat_get, false);
    }

    /* renamed from: i */
    public static void m4929i(C0994Oe oe) {
        grannyJNI.GrannyARGB8888PixelFormat_set(C0994Oe.m8106p(oe));
    }

    public static C0994Oe aTr() {
        long GrannyARGB8888PixelFormat_get = grannyJNI.GrannyARGB8888PixelFormat_get();
        if (GrannyARGB8888PixelFormat_get == 0) {
            return null;
        }
        return new C0994Oe(GrannyARGB8888PixelFormat_get, false);
    }

    /* renamed from: j */
    public static void m4990j(C0994Oe oe) {
        grannyJNI.GrannyBGR555PixelFormat_set(C0994Oe.m8106p(oe));
    }

    public static C0994Oe aTs() {
        long GrannyBGR555PixelFormat_get = grannyJNI.GrannyBGR555PixelFormat_get();
        if (GrannyBGR555PixelFormat_get == 0) {
            return null;
        }
        return new C0994Oe(GrannyBGR555PixelFormat_get, false);
    }

    /* renamed from: k */
    public static void m5050k(C0994Oe oe) {
        grannyJNI.GrannyBGR565PixelFormat_set(C0994Oe.m8106p(oe));
    }

    public static C0994Oe aTt() {
        long GrannyBGR565PixelFormat_get = grannyJNI.GrannyBGR565PixelFormat_get();
        if (GrannyBGR565PixelFormat_get == 0) {
            return null;
        }
        return new C0994Oe(GrannyBGR565PixelFormat_get, false);
    }

    /* renamed from: l */
    public static void m5083l(C0994Oe oe) {
        grannyJNI.GrannyBGRA5551PixelFormat_set(C0994Oe.m8106p(oe));
    }

    public static C0994Oe aTu() {
        long GrannyBGRA5551PixelFormat_get = grannyJNI.GrannyBGRA5551PixelFormat_get();
        if (GrannyBGRA5551PixelFormat_get == 0) {
            return null;
        }
        return new C0994Oe(GrannyBGRA5551PixelFormat_get, false);
    }

    /* renamed from: m */
    public static void m5088m(C0994Oe oe) {
        grannyJNI.GrannyBGRA4444PixelFormat_set(C0994Oe.m8106p(oe));
    }

    public static C0994Oe aTv() {
        long GrannyBGRA4444PixelFormat_get = grannyJNI.GrannyBGRA4444PixelFormat_get();
        if (GrannyBGRA4444PixelFormat_get == 0) {
            return null;
        }
        return new C0994Oe(GrannyBGRA4444PixelFormat_get, false);
    }

    /* renamed from: n */
    public static void m5094n(C0994Oe oe) {
        grannyJNI.GrannyBGR888PixelFormat_set(C0994Oe.m8106p(oe));
    }

    public static C0994Oe aTw() {
        long GrannyBGR888PixelFormat_get = grannyJNI.GrannyBGR888PixelFormat_get();
        if (GrannyBGR888PixelFormat_get == 0) {
            return null;
        }
        return new C0994Oe(GrannyBGR888PixelFormat_get, false);
    }

    /* renamed from: o */
    public static void m5101o(C0994Oe oe) {
        grannyJNI.GrannyBGRA8888PixelFormat_set(C0994Oe.m8106p(oe));
    }

    public static C0994Oe aTx() {
        long GrannyBGRA8888PixelFormat_get = grannyJNI.GrannyBGRA8888PixelFormat_get();
        if (GrannyBGRA8888PixelFormat_get == 0) {
            return null;
        }
        return new C0994Oe(GrannyBGRA8888PixelFormat_get, false);
    }

    /* renamed from: a */
    public static boolean m4510a(C3242pY pYVar, String str, C1845ab abVar) {
        return grannyJNI.GrannyFindTrackGroupForModel(C3242pY.m37170b(pYVar), str, C1845ab.m19847a(abVar));
    }

    /* renamed from: X */
    public static void m3669X(C1731Za za) {
        grannyJNI.GrannyAnimationType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aTy() {
        long GrannyAnimationType_get = grannyJNI.GrannyAnimationType_get();
        if (GrannyAnimationType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyAnimationType_get, false);
    }

    /* renamed from: a */
    public static void m4388a(C2563gq gqVar, C3242pY pYVar, int i) {
        grannyJNI.GrannyMakeDefaultAnimationBindingID(C2563gq.m32392a(gqVar), C3242pY.m37170b(pYVar), i);
    }

    /* renamed from: b */
    public static C6583apj m4589b(C2563gq gqVar) {
        long GrannyAcquireAnimationBindingFromID = grannyJNI.GrannyAcquireAnimationBindingFromID(C2563gq.m32392a(gqVar));
        if (GrannyAcquireAnimationBindingFromID == 0) {
            return null;
        }
        return new C6583apj(GrannyAcquireAnimationBindingFromID, false);
    }

    /* renamed from: a */
    public static C6583apj m3859a(C6583apj apj) {
        long GrannyAcquireAnimationBinding = grannyJNI.GrannyAcquireAnimationBinding(C6583apj.m24993f(apj));
        if (GrannyAcquireAnimationBinding == 0) {
            return null;
        }
        return new C6583apj(GrannyAcquireAnimationBinding, false);
    }

    /* renamed from: b */
    public static void m4656b(C6583apj apj) {
        grannyJNI.GrannyReleaseAnimationBinding(C6583apj.m24993f(apj));
    }

    public static int GrannyGetMaximumAnimationBindingCount() {
        return grannyJNI.GrannyGetMaximumAnimationBindingCount();
    }

    public static void GrannySetMaximumAnimationBindingCount(int i) {
        grannyJNI.GrannySetMaximumAnimationBindingCount(i);
    }

    public static void GrannyFlushAllUnusedAnimationBindings() {
        grannyJNI.GrannyFlushAllUnusedAnimationBindings();
    }

    /* renamed from: c */
    public static void m4783c(C3242pY pYVar) {
        grannyJNI.GrannyFlushAllBindingsForAnimation(C3242pY.m37170b(pYVar));
    }

    /* renamed from: c */
    public static void m4771c(C6583apj apj) {
        grannyJNI.GrannyFlushAnimationBinding(C6583apj.m24993f(apj));
    }

    /* renamed from: d */
    public static C6583apj m4805d(C3242pY pYVar) {
        long GrannyGetFirstBindingForAnimation = grannyJNI.GrannyGetFirstBindingForAnimation(C3242pY.m37170b(pYVar));
        if (GrannyGetFirstBindingForAnimation == 0) {
            return null;
        }
        return new C6583apj(GrannyGetFirstBindingForAnimation, false);
    }

    /* renamed from: a */
    public static C6583apj m3860a(C3242pY pYVar, C6583apj apj) {
        long GrannyGetNextBindingForAnimation = grannyJNI.GrannyGetNextBindingForAnimation(C3242pY.m37170b(pYVar), C6583apj.m24993f(apj));
        if (GrannyGetNextBindingForAnimation == 0) {
            return null;
        }
        return new C6583apj(GrannyGetNextBindingForAnimation, false);
    }

    public static C6583apj aTz() {
        long GrannyGetFirstUnusedAnimationBinding = grannyJNI.GrannyGetFirstUnusedAnimationBinding();
        if (GrannyGetFirstUnusedAnimationBinding == 0) {
            return null;
        }
        return new C6583apj(GrannyGetFirstUnusedAnimationBinding, false);
    }

    /* renamed from: d */
    public static C6583apj m4804d(C6583apj apj) {
        long GrannyGetNextUnusedAnimationBinding = grannyJNI.GrannyGetNextUnusedAnimationBinding(C6583apj.m24993f(apj));
        if (GrannyGetNextUnusedAnimationBinding == 0) {
            return null;
        }
        return new C6583apj(GrannyGetNextUnusedAnimationBinding, false);
    }

    /* renamed from: e */
    public static boolean m4848e(C3242pY pYVar) {
        return grannyJNI.GrannyIsAnimationUsed(C3242pY.m37170b(pYVar));
    }

    /* renamed from: a */
    public static void m4311a(C6583apj apj, C3242pY pYVar) {
        grannyJNI.GrannyRemapAnimationBindingPointers(C6583apj.m24993f(apj), C3242pY.m37170b(pYVar));
    }

    /* renamed from: a */
    public static void m4426a(C3242pY pYVar, C3242pY pYVar2) {
        grannyJNI.GrannyRemapAllAnimationBindingPointers(C3242pY.m37170b(pYVar), C3242pY.m37170b(pYVar2));
    }

    /* renamed from: a */
    public static void m4278a(C6208aiY aiy) {
        grannyJNI.GrannyGetAnimationBindingCacheStatus(C6208aiY.m22570c(aiy));
    }

    /* renamed from: a */
    public static void m4310a(C6583apj apj, C6796ato ato, boolean z, float f) {
        grannyJNI.GrannyCalculateLODErrorValues(C6583apj.m24993f(apj), C6796ato.m26060i(ato), z, f);
    }

    /* renamed from: a */
    public static void m4327a(C6676arY ary, C6796ato ato, boolean z, float f) {
        grannyJNI.GrannyCalculateLODErrorValuesAllBindings(C6676arY.m25508i(ary), C6796ato.m26060i(ato), z, f);
    }

    /* renamed from: a */
    public static float m3674a(C6583apj apj, int i) {
        return grannyJNI.GrannyGetLODErrorValue(C6583apj.m24993f(apj), i);
    }

    /* renamed from: a */
    public static void m4309a(C6583apj apj, int i, float f) {
        grannyJNI.GrannySetLODErrorValue(C6583apj.m24993f(apj), i, f);
    }

    /* renamed from: a */
    public static void m4308a(C6583apj apj, float f, boolean z) {
        grannyJNI.GrannyCopyLODErrorValuesToAnimation(C6583apj.m24993f(apj), f, z);
    }

    /* renamed from: a */
    public static void m4236a(C5801aah aah, float f, boolean z) {
        grannyJNI.GrannyCopyLODErrorValuesToAllAnimations(C5801aah.m19565x(aah), f, z);
    }

    /* renamed from: a */
    public static void m4307a(C6583apj apj, float f) {
        grannyJNI.GrannyCopyLODErrorValuesFromAnimation(C6583apj.m24993f(apj), f);
    }

    /* renamed from: a */
    public static void m4233a(C5801aah aah, float f) {
        grannyJNI.GrannyCopyLODErrorValuesFromAllAnimations(C5801aah.m19565x(aah), f);
    }

    /* renamed from: b */
    public static void m4657b(C6583apj apj, float f) {
        grannyJNI.GrannySetAllLODErrorValues(C6583apj.m24993f(apj), f);
    }

    /* renamed from: e */
    public static void m4842e(C6583apj apj) {
        grannyJNI.GrannyResetLODErrorValues(C6583apj.m24993f(apj));
    }

    /* renamed from: a */
    public static C2876lK m3897a(C3242pY pYVar, float f) {
        long GrannyCalculateAnimationLODBegin = grannyJNI.GrannyCalculateAnimationLODBegin(C3242pY.m37170b(pYVar), f);
        if (GrannyCalculateAnimationLODBegin == 0) {
            return null;
        }
        return new C2876lK(GrannyCalculateAnimationLODBegin, false);
    }

    /* renamed from: a */
    public static void m4398a(C2876lK lKVar, C5975adz adz, C6796ato ato, float f) {
        grannyJNI.GrannyCalculateAnimationLODAddMeshBinding(C2876lK.m34834a(lKVar), C5975adz.m21108e(adz), C6796ato.m26060i(ato), f);
    }

    /* renamed from: b */
    public static void m4681b(C2876lK lKVar) {
        grannyJNI.GrannyCalculateAnimationLODEnd(C2876lK.m34834a(lKVar));
    }

    /* renamed from: c */
    public static void m4776c(C2876lK lKVar) {
        grannyJNI.GrannyCalculateAnimationLODCleanup(C2876lK.m34834a(lKVar));
    }

    /* renamed from: Y */
    public static void m3670Y(C1731Za za) {
        grannyJNI.GrannyArtToolInfoType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aTA() {
        long GrannyArtToolInfoType_get = grannyJNI.GrannyArtToolInfoType_get();
        if (GrannyArtToolInfoType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyArtToolInfoType_get, false);
    }

    /* renamed from: a */
    public static boolean m4505a(C2267dS dSVar, float f, C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, C3159oW oWVar4, C3159oW oWVar5, C3159oW oWVar6, C3159oW oWVar7) {
        return grannyJNI.GrannyComputeBasisConversion(C2267dS.m28965a(dSVar), f, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), C3159oW.m36741q(oWVar4), C3159oW.m36741q(oWVar5), C3159oW.m36741q(oWVar6), C3159oW.m36741q(oWVar7));
    }

    /* renamed from: a */
    public static void m4056a(C1126QZ qz, C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, float f, float f2, long j) {
        grannyJNI.GrannyTransformMesh(C1126QZ.m8909l(qz), C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), f, f2, j);
    }

    /* renamed from: a */
    public static void m4029a(C0913NP np, C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, float f, float f2, long j) {
        grannyJNI.GrannyTransformSkeleton(C0913NP.m7619a(np), C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), f, f2, j);
    }

    /* renamed from: a */
    public static void m4258a(C5975adz adz, C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, float f, float f2, long j) {
        grannyJNI.GrannyTransformModel(C5975adz.m21108e(adz), C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), f, f2, j);
    }

    /* renamed from: a */
    public static void m4425a(C3242pY pYVar, C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, float f, float f2, long j) {
        grannyJNI.GrannyTransformAnimation(C3242pY.m37170b(pYVar), C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), f, f2, j);
    }

    /* renamed from: a */
    public static void m4385a(C2267dS dSVar, C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, float f, float f2, long j) {
        grannyJNI.GrannyTransformFile(C2267dS.m28965a(dSVar), C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), f, f2, j);
    }

    /* renamed from: f */
    public static void m4867f(C3242pY pYVar) {
        grannyJNI.GrannyResortAllAnimationTrackGroups(C3242pY.m37170b(pYVar));
    }

    /* renamed from: b */
    public static void m4670b(C2267dS dSVar) {
        grannyJNI.GrannyResortAllFileTrackGroups(C2267dS.m28965a(dSVar));
    }

    /* renamed from: cP */
    public static C0994Oe m4793cP(boolean z) {
        long GrannyGetBinkPixelLayout = grannyJNI.GrannyGetBinkPixelLayout(z);
        if (GrannyGetBinkPixelLayout == 0) {
            return null;
        }
        return new C0994Oe(GrannyGetBinkPixelLayout, false);
    }

    public static int GrannyGetMaximumBinkImageSize(int i, int i2, long j, int i3) {
        return grannyJNI.GrannyGetMaximumBinkImageSize(i, i2, j, i3);
    }

    /* renamed from: a */
    public static int m3677a(int i, int i2, int i3, C5501aLt alt, long j, int i4, C5501aLt alt2) {
        return grannyJNI.GrannyBinkCompressTexture(i, i2, i3, C5501aLt.m16286h(alt), j, i4, C5501aLt.m16286h(alt2));
    }

    /* renamed from: a */
    public static void m3947a(int i, int i2, long j, int i3, C5501aLt alt, C0994Oe oe, int i4, C5501aLt alt2) {
        grannyJNI.GrannyBinkDecompressTexture(i, i2, j, i3, C5501aLt.m16286h(alt), C0994Oe.m8106p(oe), i4, C5501aLt.m16286h(alt2));
    }

    /* renamed from: a */
    public static void m4417a(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline0x1(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: b */
    public static void m4695b(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline0x2(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: c */
    public static void m4780c(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline0x3(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: d */
    public static void m4821d(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline0x4(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: e */
    public static void m4844e(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline0x9(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: f */
    public static void m4865f(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline1x1(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: g */
    public static void m4879g(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline1x2(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: h */
    public static void m4891h(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline1x3(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: i */
    public static void m4931i(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline1x3n(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: j */
    public static void m4991j(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline1x4(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: k */
    public static void m5051k(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline1x4n(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: l */
    public static void m5085l(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline1x9(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: m */
    public static void m5090m(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline2x1(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: n */
    public static void m5096n(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline2x2(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: o */
    public static void m5103o(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline2x3(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: p */
    public static void m5107p(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline2x3n(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: q */
    public static void m5113q(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline2x4(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: r */
    public static void m5117r(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline2x4n(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: s */
    public static void m5121s(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline2x9(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: t */
    public static void m5125t(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline3x1(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: u */
    public static void m5128u(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline3x2(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: v */
    public static void m5131v(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline3x3(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: w */
    public static void m5134w(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline3x3n(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: x */
    public static void m5136x(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline3x4(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: y */
    public static void m5138y(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline3x4n(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: z */
    public static void m5140z(C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline3x9(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: a */
    public static void m3953a(int i, int i2, boolean z, C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannySampleBSpline(i, i2, z, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: a */
    public static void m3951a(int i, int i2, C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannyUncheckedSampleBSpline(i, i2, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: b */
    public static void m4599b(int i, int i2, C3159oW oWVar, C3159oW oWVar2, float f, C3159oW oWVar3) {
        grannyJNI.GrannyUncheckedSampleBSplineN(i, i2, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, C3159oW.m36741q(oWVar3));
    }

    /* renamed from: a */
    public static boolean m4467a(int i, C3914wi wiVar, C3914wi wiVar2, C3914wi wiVar3, float f, float f2, float f3, int i2, C3159oW oWVar, C3159oW oWVar2, C1615Xe xe, C1615Xe xe2, C3159oW oWVar3) {
        return grannyJNI.GrannyConstructBSplineBuffers(i, C3914wi.m40719a(wiVar), C3914wi.m40719a(wiVar2), C3914wi.m40719a(wiVar3), f, f2, f3, i2, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C1615Xe.m11542a(xe), C1615Xe.m11542a(xe2), C3159oW.m36741q(oWVar3));
    }

    /* renamed from: a */
    public static void m3962a(int i, boolean z, boolean z2, C3914wi wiVar, boolean z3, float f, float f2, C3159oW oWVar, C3159oW oWVar2) {
        grannyJNI.GrannyEvaluateCurveAtT(i, z, z2, C3914wi.m40719a(wiVar), z3, f, f2, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2));
    }

    /* renamed from: a */
    public static void m3963a(int i, boolean z, boolean z2, C3914wi wiVar, boolean z3, float f, int i2, float f2, C3159oW oWVar, C3159oW oWVar2) {
        grannyJNI.GrannyEvaluateCurveAtKnotIndex(i, z, z2, C3914wi.m40719a(wiVar), z3, f, i2, f2, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2));
    }

    public static int GrannyGetMaximumKnotCountForSampleCount(int i, int i2) {
        return grannyJNI.GrannyGetMaximumKnotCountForSampleCount(i, i2);
    }

    /* renamed from: f */
    public static aMA m4853f(int i, int i2, int i3) {
        long GrannyAllocateBSplineSolver = grannyJNI.GrannyAllocateBSplineSolver(i, i2, i3);
        if (GrannyAllocateBSplineSolver == 0) {
            return null;
        }
        return new aMA(GrannyAllocateBSplineSolver, false);
    }

    /* renamed from: a */
    public static void m4157a(aMA ama) {
        grannyJNI.GrannyDeallocateBSplineSolver(aMA.m16307b(ama));
    }

    /* renamed from: a */
    public static C3914wi m3925a(aMA ama, long j, int i, float f, float f2, float f3, C3159oW oWVar, int i2, int i3, float f4, C1731Za za, int i4, C1418Um um, C1845ab abVar) {
        long GrannyFitBSplineToSamples = grannyJNI.GrannyFitBSplineToSamples(aMA.m16307b(ama), j, i, f, f2, f3, C3159oW.m36741q(oWVar), i2, i3, f4, C1731Za.m12101bi(za), i4, C1418Um.m10339a(um), C1845ab.m19847a(abVar));
        if (GrannyFitBSplineToSamples == 0) {
            return null;
        }
        return new C3914wi(GrannyFitBSplineToSamples, false);
    }

    public static float GrannyOrientationToleranceFromDegrees(float f) {
        return grannyJNI.GrannyOrientationToleranceFromDegrees(f);
    }

    public static float GrannyDegreesFromOrientationTolerance(float f) {
        return grannyJNI.GrannyDegreesFromOrientationTolerance(f);
    }

    /* renamed from: a */
    public static void m3945a(int i, int i2, int i3, float f, C3914wi wiVar, int i4, C3159oW oWVar, C3159oW oWVar2, C6694arq arq) {
        grannyJNI.GrannyGetSquaredErrorOverCurve(i, i2, i3, f, C3914wi.m40719a(wiVar), i4, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C6694arq.m25594a(arq));
    }

    /* renamed from: b */
    public static void m4688b(C3120o oVar) {
        grannyJNI.GrannyInitializeDefaultCamera(C3120o.m36549a(oVar));
    }

    /* renamed from: a */
    public static void m4406a(C3120o oVar, float f, float f2, float f3, float f4, float f5) {
        grannyJNI.GrannySetCameraAspectRatios(C3120o.m36549a(oVar), f, f2, f3, f4, f5);
    }

    /* renamed from: a */
    public static void m4405a(C3120o oVar, float f, float f2, float f3) {
        grannyJNI.GrannyMoveCameraRelative(C3120o.m36549a(oVar), f, f2, f3);
    }

    /* renamed from: c */
    public static void m4777c(C3120o oVar) {
        grannyJNI.GrannyBuildCameraMatrices(C3120o.m36549a(oVar));
    }

    /* renamed from: a */
    public static void m4409a(C3120o oVar, C3159oW oWVar) {
        grannyJNI.GrannyGetCameraLocation(C3120o.m36549a(oVar), C3159oW.m36741q(oWVar));
    }

    /* renamed from: b */
    public static void m4690b(C3120o oVar, C3159oW oWVar) {
        grannyJNI.GrannyGetCameraLeft(C3120o.m36549a(oVar), C3159oW.m36741q(oWVar));
    }

    /* renamed from: c */
    public static void m4778c(C3120o oVar, C3159oW oWVar) {
        grannyJNI.GrannyGetCameraRight(C3120o.m36549a(oVar), C3159oW.m36741q(oWVar));
    }

    /* renamed from: d */
    public static void m4820d(C3120o oVar, C3159oW oWVar) {
        grannyJNI.GrannyGetCameraUp(C3120o.m36549a(oVar), C3159oW.m36741q(oWVar));
    }

    /* renamed from: e */
    public static void m4843e(C3120o oVar, C3159oW oWVar) {
        grannyJNI.GrannyGetCameraDown(C3120o.m36549a(oVar), C3159oW.m36741q(oWVar));
    }

    /* renamed from: f */
    public static void m4864f(C3120o oVar, C3159oW oWVar) {
        grannyJNI.GrannyGetCameraForward(C3120o.m36549a(oVar), C3159oW.m36741q(oWVar));
    }

    /* renamed from: g */
    public static void m4878g(C3120o oVar, C3159oW oWVar) {
        grannyJNI.GrannyGetCameraBack(C3120o.m36549a(oVar), C3159oW.m36741q(oWVar));
    }

    /* renamed from: a */
    public static void m4408a(C3120o oVar, float f, float f2, C3159oW oWVar, C3159oW oWVar2) {
        grannyJNI.GrannyWindowSpaceToWorldSpace(C3120o.m36549a(oVar), f, f2, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2));
    }

    /* renamed from: b */
    public static void m4689b(C3120o oVar, float f, float f2, C3159oW oWVar, C3159oW oWVar2) {
        grannyJNI.GrannyWorldSpaceToWindowSpace(C3120o.m36549a(oVar), f, f2, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2));
    }

    /* renamed from: a */
    public static void m4407a(C3120o oVar, float f, float f2, float f3, float f4, C3159oW oWVar, C3159oW oWVar2) {
        grannyJNI.GrannyGetPickingRay(C3120o.m36549a(oVar), f, f2, f3, f4, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2));
    }

    /* renamed from: a */
    public static void m4410a(C3120o oVar, boolean z, C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, C3159oW oWVar4) {
        grannyJNI.GrannyGetCameraRelativePlanarBases(C3120o.m36549a(oVar), z, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), C3159oW.m36741q(oWVar4));
    }

    public static float GrannyGetMostLikelyPhysicalAspectRatio(int i, int i2) {
        return grannyJNI.GrannyGetMostLikelyPhysicalAspectRatio(i, i2);
    }

    public static float GrannyFindAllowedLODError(float f, int i, float f2, float f3) {
        return grannyJNI.GrannyFindAllowedLODError(f, i, f2, f3);
    }

    /* renamed from: a */
    public static C3914wi m3926a(aMA ama, long j, C6640aqo aqo, C3159oW oWVar, int i, int i2, float f, C1418Um um) {
        long GrannyCompressCurve = grannyJNI.GrannyCompressCurve(aMA.m16307b(ama), j, C6640aqo.m25217a(aqo), C3159oW.m36741q(oWVar), i, i2, f, C1418Um.m10339a(um));
        if (GrannyCompressCurve == 0) {
            return null;
        }
        return new C3914wi(GrannyCompressCurve, false);
    }

    /* renamed from: l */
    public static C5801aah m5081l(float f, float f2) {
        long GrannyCreateControl = grannyJNI.GrannyCreateControl(f, f2);
        if (GrannyCreateControl == 0) {
            return null;
        }
        return new C5801aah(GrannyCreateControl, false);
    }

    /* renamed from: d */
    public static void m4816d(C5801aah aah) {
        grannyJNI.GrannyFreeControlOnceUnused(C5801aah.m19565x(aah));
    }

    /* renamed from: e */
    public static float m4826e(C5801aah aah) {
        return grannyJNI.GrannyGetControlCompletionClock(C5801aah.m19565x(aah));
    }

    /* renamed from: f */
    public static boolean m4868f(C5801aah aah) {
        return grannyJNI.GrannyGetControlCompletionCheckFlag(C5801aah.m19565x(aah));
    }

    /* renamed from: a */
    public static void m4239a(C5801aah aah, boolean z) {
        grannyJNI.GrannySetControlCompletionCheckFlag(C5801aah.m19565x(aah), z);
    }

    /* renamed from: g */
    public static float m4870g(C5801aah aah) {
        return grannyJNI.GrannyGetControlClock(C5801aah.m19565x(aah));
    }

    /* renamed from: c */
    public static void m4767c(C5801aah aah, float f) {
        grannyJNI.GrannySetControlClock(C5801aah.m19565x(aah), f);
    }

    /* renamed from: d */
    public static void m4817d(C5801aah aah, float f) {
        grannyJNI.GrannySetControlClockOnly(C5801aah.m19565x(aah), f);
    }

    /* renamed from: h */
    public static boolean m4892h(C5801aah aah) {
        return grannyJNI.GrannyControlIsComplete(C5801aah.m19565x(aah));
    }

    /* renamed from: i */
    public static boolean m4933i(C5801aah aah) {
        return grannyJNI.GrannyFreeControlIfComplete(C5801aah.m19565x(aah));
    }

    /* renamed from: j */
    public static float m4986j(C5801aah aah) {
        return grannyJNI.GrannyGetControlWeight(C5801aah.m19565x(aah));
    }

    /* renamed from: e */
    public static void m4839e(C5801aah aah, float f) {
        grannyJNI.GrannySetControlWeight(C5801aah.m19565x(aah), f);
    }

    /* renamed from: a */
    public static C5789aaV m3824a(C5801aah aah, C6676arY ary) {
        long GrannyGetControlTrackGroupModelMask = grannyJNI.GrannyGetControlTrackGroupModelMask(C5801aah.m19565x(aah), C6676arY.m25508i(ary));
        if (GrannyGetControlTrackGroupModelMask == 0) {
            return null;
        }
        return new C5789aaV(GrannyGetControlTrackGroupModelMask, false);
    }

    /* renamed from: a */
    public static C5789aaV m3825a(C5801aah aah, C3242pY pYVar, int i) {
        long GrannyGetControlTrackGroupTrackMask = grannyJNI.GrannyGetControlTrackGroupTrackMask(C5801aah.m19565x(aah), C3242pY.m37170b(pYVar), i);
        if (GrannyGetControlTrackGroupTrackMask == 0) {
            return null;
        }
        return new C5789aaV(GrannyGetControlTrackGroupTrackMask, false);
    }

    /* renamed from: k */
    public static int m5045k(C5801aah aah) {
        return grannyJNI.GrannyGetControlLoopCount(C5801aah.m19565x(aah));
    }

    /* renamed from: a */
    public static void m4238a(C5801aah aah, C1418Um um, C1418Um um2) {
        grannyJNI.GrannyGetControlLoopState(C5801aah.m19565x(aah), C1418Um.m10339a(um), C1418Um.m10339a(um2));
    }

    /* renamed from: l */
    public static int m5079l(C5801aah aah) {
        return grannyJNI.GrannyGetControlLoopIndex(C5801aah.m19565x(aah));
    }

    /* renamed from: b */
    public static void m4648b(C5801aah aah, int i) {
        grannyJNI.GrannySetControlLoopIndex(C5801aah.m19565x(aah), i);
    }

    /* renamed from: m */
    public static float m5086m(C5801aah aah) {
        return grannyJNI.GrannyGetControlSpeed(C5801aah.m19565x(aah));
    }


    /* renamed from: n */
    public static float m5092n(C5801aah aah) {
        return grannyJNI.GrannyGetControlDuration(C5801aah.m19565x(aah));
    }


    /* renamed from: p */
    public static boolean m5108p(C5801aah aah) {
        return grannyJNI.GrannyControlIsActive(C5801aah.m19565x(aah));
    }

    /* renamed from: b */
    public static void m4649b(C5801aah aah, boolean z) {
        grannyJNI.GrannySetControlActive(C5801aah.m19565x(aah), z);
    }

    /* renamed from: q */
    public static float m5110q(C5801aah aah) {
        return grannyJNI.GrannyGetControlClampedLocalClock(C5801aah.m19565x(aah));
    }

    /* renamed from: r */
    public static float m5114r(C5801aah aah) {
        return grannyJNI.GrannyGetControlLocalDuration(C5801aah.m19565x(aah));
    }

    /* renamed from: s */
    public static float m5118s(C5801aah aah) {
        return grannyJNI.GrannyGetControlEaseCurveMultiplier(C5801aah.m19565x(aah));
    }

    /* renamed from: c */
    public static void m4768c(C5801aah aah, boolean z) {
        grannyJNI.GrannySetControlEaseIn(C5801aah.m19565x(aah), z);
    }

    /* renamed from: a */
    public static void m4234a(C5801aah aah, float f, float f2, float f3, float f4, float f5, float f6) {
        grannyJNI.GrannySetControlEaseInCurve(C5801aah.m19565x(aah), f, f2, f3, f4, f5, f6);
    }

    /* renamed from: d */
    public static void m4818d(C5801aah aah, boolean z) {
        grannyJNI.GrannySetControlEaseOut(C5801aah.m19565x(aah), z);
    }

    /* renamed from: b */
    public static void m4647b(C5801aah aah, float f, float f2, float f3, float f4, float f5, float f6) {
        grannyJNI.GrannySetControlEaseOutCurve(C5801aah.m19565x(aah), f, f2, f3, f4, f5, f6);
    }

    /* renamed from: u */
    public static float m5126u(C5801aah aah) {
        return grannyJNI.GrannyGetControlRawLocalClock(C5801aah.m19565x(aah));
    }

    /* renamed from: g */
    public static void m4876g(C5801aah aah, float f) {
        grannyJNI.GrannySetControlRawLocalClock(C5801aah.m19565x(aah), f);
    }

    /* renamed from: v */
    public static C5269aCv m5129v(C5801aah aah) {
        long GrannyGetControlUserDataArray = grannyJNI.GrannyGetControlUserDataArray(C5801aah.m19565x(aah));
        if (GrannyGetControlUserDataArray == 0) {
            return null;
        }
        return new C5269aCv(GrannyGetControlUserDataArray, false);
    }

    public static C5801aah aTB() {
        long GrannyGetGlobalControlsBegin = grannyJNI.GrannyGetGlobalControlsBegin();
        if (GrannyGetGlobalControlsBegin == 0) {
            return null;
        }
        return new C5801aah(GrannyGetGlobalControlsBegin, false);
    }

    public static C5801aah aTC() {
        long GrannyGetGlobalControlsEnd = grannyJNI.GrannyGetGlobalControlsEnd();
        if (GrannyGetGlobalControlsEnd == 0) {
            return null;
        }
        return new C5801aah(GrannyGetGlobalControlsEnd, false);
    }

    /* renamed from: w */
    public static C5801aah m5132w(C5801aah aah) {
        long GrannyGetGlobalNextControl = grannyJNI.GrannyGetGlobalNextControl(C5801aah.m19565x(aah));
        if (GrannyGetGlobalNextControl == 0) {
            return null;
        }
        return new C5801aah(GrannyGetGlobalNextControl, false);
    }

    /* renamed from: i */
    public static void m4930i(C5801aah aah, float f) {
        grannyJNI.GrannyRecenterControlClocks(C5801aah.m19565x(aah), f);
    }

    public static void GrannyRecenterAllControlClocks(float f) {
        grannyJNI.GrannyRecenterAllControlClocks(f);
    }

    /* renamed from: b */
    public static void m4659b(C6676arY ary, float f) {
        grannyJNI.GrannyRecenterAllModelInstanceControlClocks(C6676arY.m25508i(ary), f);
    }

    /* renamed from: e */
    public static void m4840e(C5801aah aah, boolean z) {
        grannyJNI.GrannySetControlForceClampedLooping(C5801aah.m19565x(aah), z);
    }

    /* renamed from: a */
    public static void m4235a(C5801aah aah, float f, float f2, float f3, int i) {
        grannyJNI.GrannySetControlTargetState(C5801aah.m19565x(aah), f, f2, f3, i);
    }

    /* renamed from: a */
    public static C5801aah m3828a(float f, C3242pY pYVar, C6583apj apj, C6676arY ary) {
        long GrannyPlayControlledAnimationBinding = grannyJNI.GrannyPlayControlledAnimationBinding(f, C3242pY.m37170b(pYVar), C6583apj.m24993f(apj), C6676arY.m25508i(ary));
        if (GrannyPlayControlledAnimationBinding == 0) {
            return null;
        }
        return new C5801aah(GrannyPlayControlledAnimationBinding, false);
    }

    /* renamed from: a */
    public static C1407Ud m3764a(float f, C3242pY pYVar) {
        long GrannyBeginControlledAnimation = grannyJNI.GrannyBeginControlledAnimation(f, C3242pY.m37170b(pYVar));
        if (GrannyBeginControlledAnimation == 0) {
            return null;
        }
        return new C1407Ud(GrannyBeginControlledAnimation, false);
    }

    /* renamed from: a */
    public static C5801aah m3831a(C1407Ud ud) {
        long GrannyEndControlledAnimation = grannyJNI.GrannyEndControlledAnimation(C1407Ud.m10295b(ud));
        if (GrannyEndControlledAnimation == 0) {
            return null;
        }
        return new C5801aah(GrannyEndControlledAnimation, false);
    }

    /* renamed from: a */
    public static void m4098a(C1407Ud ud, C5801aah aah) {
        grannyJNI.GrannyUseExistingControlForAnimation(C1407Ud.m10295b(ud), C5801aah.m19565x(aah));
    }

    /* renamed from: a */
    public static void m4096a(C1407Ud ud, int i, String str, String str2) {
        grannyJNI.GrannySetTrackMatchRule(C1407Ud.m10295b(ud), i, str, str2);
    }

    /* renamed from: a */
    public static void m4095a(C1407Ud ud, int i, C6676arY ary) {
        grannyJNI.GrannySetTrackGroupTarget(C1407Ud.m10295b(ud), i, C6676arY.m25508i(ary));
    }

    /* renamed from: a */
    public static void m4094a(C1407Ud ud, int i, C6583apj apj) {
        grannyJNI.GrannySetTrackGroupBinding(C1407Ud.m10295b(ud), i, C6583apj.m24993f(apj));
    }

    /* renamed from: a */
    public static void m4092a(C1407Ud ud, int i, C5975adz adz, C5975adz adz2) {
        grannyJNI.GrannySetTrackGroupBasisTransform(C1407Ud.m10295b(ud), i, C5975adz.m21108e(adz), C5975adz.m21108e(adz2));
    }

    /* renamed from: a */
    public static void m4091a(C1407Ud ud, int i, C5789aaV aav) {
        grannyJNI.GrannySetTrackGroupTrackMask(C1407Ud.m10295b(ud), i, C5789aaV.m19495f(aav));
    }

    /* renamed from: b */
    public static void m4629b(C1407Ud ud, int i, C5789aaV aav) {
        grannyJNI.GrannySetTrackGroupModelMask(C1407Ud.m10295b(ud), i, C5789aaV.m19495f(aav));
    }

    /* renamed from: a */
    public static void m4093a(C1407Ud ud, int i, C6468anY any) {
        grannyJNI.GrannySetTrackGroupAccumulation(C1407Ud.m10295b(ud), i, any.swigValue());
    }

    /* renamed from: a */
    public static void m4097a(C1407Ud ud, int i, boolean z, float f) {
        grannyJNI.GrannySetTrackGroupLOD(C1407Ud.m10295b(ud), i, z, f);
    }

    /* renamed from: e */
    public static C6583apj m4833e(C6595apv apv) {
        long GrannyGetAnimationBindingFromControlBinding = grannyJNI.GrannyGetAnimationBindingFromControlBinding(C6595apv.m25028g(apv));
        if (GrannyGetAnimationBindingFromControlBinding == 0) {
            return null;
        }
        return new C6583apj(GrannyGetAnimationBindingFromControlBinding, false);
    }

    public static float GrannyGetGlobalLODFadingFactor() {
        return grannyJNI.GrannyGetGlobalLODFadingFactor();
    }

    public static void GrannySetGlobalLODFadingFactor(float f) {
        grannyJNI.GrannySetGlobalLODFadingFactor(f);
    }

    /* renamed from: a */
    public static C5801aah m3827a(float f, float f2, aUB aub, C6676arY ary, C5789aaV aav) {
        long GrannyPlayControlledPose = grannyJNI.GrannyPlayControlledPose(f, f2, aUB.m18499d(aub), C6676arY.m25508i(ary), C5789aaV.m19495f(aav));
        if (GrannyPlayControlledPose == 0) {
            return null;
        }
        return new C5801aah(GrannyPlayControlledPose, false);
    }

    /* renamed from: f */
    public static aUB m4855f(C6595apv apv) {
        long GrannyGetLocalPoseFromControlBinding = grannyJNI.GrannyGetLocalPoseFromControlBinding(C6595apv.m25028g(apv));
        if (GrannyGetLocalPoseFromControlBinding == 0) {
            return null;
        }
        return new aUB(GrannyGetLocalPoseFromControlBinding, false);
    }

    /* renamed from: a */
    public static void m4270a(C6135ahD ahd) {
        grannyJNI.GrannyBeginCRC32(C6135ahD.m22189i(ahd));
    }

    /* renamed from: a */
    public static void m4271a(C6135ahD ahd, long j, C5501aLt alt) {
        grannyJNI.GrannyAddToCRC32(C6135ahD.m22189i(ahd), j, C5501aLt.m16286h(alt));
    }

    /* renamed from: b */
    public static void m4652b(C6135ahD ahd) {
        grannyJNI.GrannyEndCRC32(C6135ahD.m22189i(ahd));
    }

    /* renamed from: a */
    public static boolean m4481a(C1325TP tp) {
        return grannyJNI.GrannyIsBlendDagLeafType(C1325TP.m9961u(tp));
    }

    /* renamed from: b */
    public static C3163oZ m4596b(C1325TP tp) {
        return C3163oZ.m36745dI(grannyJNI.GrannyGetBlendDagNodeType(C1325TP.m9961u(tp)));
    }

    /* renamed from: a */
    public static C1325TP m3757a(C6676arY ary, boolean z) {
        long GrannyCreateBlendDagNodeAnimationBlend = grannyJNI.GrannyCreateBlendDagNodeAnimationBlend(C6676arY.m25508i(ary), z);
        if (GrannyCreateBlendDagNodeAnimationBlend == 0) {
            return null;
        }
        return new C1325TP(GrannyCreateBlendDagNodeAnimationBlend, false);
    }

    /* renamed from: a */
    public static C1325TP m3755a(aUB aub, boolean z) {
        long GrannyCreateBlendDagNodeLocalPose = grannyJNI.GrannyCreateBlendDagNodeLocalPose(aUB.m18499d(aub), z);
        if (GrannyCreateBlendDagNodeLocalPose == 0) {
            return null;
        }
        return new C1325TP(GrannyCreateBlendDagNodeLocalPose, false);
    }

    /* renamed from: a */
    public static C1325TP m3758a(C6814auG aug, C6256ajU aju, C0067Ao ao, C5501aLt alt) {
        long GrannyCreateBlendDagNodeCallback = grannyJNI.GrannyCreateBlendDagNodeCallback(C6814auG.m26188a(aug), C6256ajU.m22930a(aju), C0067Ao.m543a(ao), C5501aLt.m16286h(alt));
        if (GrannyCreateBlendDagNodeCallback == 0) {
            return null;
        }
        return new C1325TP(GrannyCreateBlendDagNodeCallback, false);
    }

    /* renamed from: a */
    public static C1325TP m3753a(C1325TP tp, C1325TP tp2, float f, float f2, C5789aaV aav, boolean z, int i) {
        long GrannyCreateBlendDagNodeCrossfade = grannyJNI.GrannyCreateBlendDagNodeCrossfade(C1325TP.m9961u(tp), C1325TP.m9961u(tp2), f, f2, C5789aaV.m19495f(aav), z, i);
        if (GrannyCreateBlendDagNodeCrossfade == 0) {
            return null;
        }
        return new C1325TP(GrannyCreateBlendDagNodeCrossfade, false);
    }

    /* renamed from: a */
    public static C1325TP m3750a(C0913NP np, float f, C3807vF vFVar, int i) {
        long GrannyCreateBlendDagNodeWeightedBlend = grannyJNI.GrannyCreateBlendDagNodeWeightedBlend(C0913NP.m7619a(np), f, vFVar.swigValue(), i);
        if (GrannyCreateBlendDagNodeWeightedBlend == 0) {
            return null;
        }
        return new C1325TP(GrannyCreateBlendDagNodeWeightedBlend, false);
    }

    /* renamed from: c */
    public static void m4756c(C1325TP tp) {
        grannyJNI.GrannyFreeBlendDagNode(C1325TP.m9961u(tp));
    }

    /* renamed from: a */
    public static void m4081a(C1325TP tp, C6676arY ary, float f, boolean z) {
        grannyJNI.GrannySetBlendDagNodeAnimationBlend(C1325TP.m9961u(tp), C6676arY.m25508i(ary), f, z);
    }

    /* renamed from: a */
    public static void m4079a(C1325TP tp, aUB aub, boolean z) {
        grannyJNI.GrannySetBlendDagNodeLocalPose(C1325TP.m9961u(tp), aUB.m18499d(aub), z);
    }

    /* renamed from: a */
    public static void m4082a(C1325TP tp, C6814auG aug, C6256ajU aju, C0067Ao ao, C5501aLt alt) {
        grannyJNI.GrannySetBlendDagNodeCallbacks(C1325TP.m9961u(tp), C6814auG.m26188a(aug), C6256ajU.m22930a(aju), C0067Ao.m543a(ao), C5501aLt.m16286h(alt));
    }

    /* renamed from: a */
    public static void m4072a(C1325TP tp, float f, float f2, C5789aaV aav, boolean z) {
        grannyJNI.GrannySetBlendDagNodeCrossfade(C1325TP.m9961u(tp), f, f2, C5789aaV.m19495f(aav), z);
    }

    /* renamed from: a */
    public static void m4071a(C1325TP tp, float f, float f2) {
        grannyJNI.GrannySetBlendDagNodeCrossfadeWeights(C1325TP.m9961u(tp), f, f2);
    }

    /* renamed from: a */
    public static void m4077a(C1325TP tp, C0913NP np, float f, C3807vF vFVar) {
        grannyJNI.GrannySetBlendDagNodeWeightedBlend(C1325TP.m9961u(tp), C0913NP.m7619a(np), f, vFVar.swigValue());
    }

    /* renamed from: d */
    public static C6676arY m4806d(C1325TP tp) {
        long GrannyGetBlendDagNodeAnimationBlend = grannyJNI.GrannyGetBlendDagNodeAnimationBlend(C1325TP.m9961u(tp));
        if (GrannyGetBlendDagNodeAnimationBlend == 0) {
            return null;
        }
        return new C6676arY(GrannyGetBlendDagNodeAnimationBlend, false);
    }

    /* renamed from: e */
    public static aUB m4832e(C1325TP tp) {
        long GrannyGetBlendDagNodeLocalPose = grannyJNI.GrannyGetBlendDagNodeLocalPose(C1325TP.m9961u(tp));
        if (GrannyGetBlendDagNodeLocalPose == 0) {
            return null;
        }
        return new aUB(GrannyGetBlendDagNodeLocalPose, false);
    }

    /* renamed from: f */
    public static C6814auG m4857f(C1325TP tp) {
        long GrannyGetBlendDagNodeCallbackSampleCallback = grannyJNI.GrannyGetBlendDagNodeCallbackSampleCallback(C1325TP.m9961u(tp));
        if (GrannyGetBlendDagNodeCallbackSampleCallback == 0) {
            return null;
        }
        return new C6814auG(GrannyGetBlendDagNodeCallbackSampleCallback, false);
    }

    /* renamed from: g */
    public static C6256ajU m4874g(C1325TP tp) {
        long GrannyGetBlendDagNodeCallbackSetClockCallback = grannyJNI.GrannyGetBlendDagNodeCallbackSetClockCallback(C1325TP.m9961u(tp));
        if (GrannyGetBlendDagNodeCallbackSetClockCallback == 0) {
            return null;
        }
        return new C6256ajU(GrannyGetBlendDagNodeCallbackSetClockCallback, false);
    }

    /* renamed from: h */
    public static C0067Ao m4887h(C1325TP tp) {
        long GrannyGetBlendDagNodeCallbackMotionVectorsCallback = grannyJNI.GrannyGetBlendDagNodeCallbackMotionVectorsCallback(C1325TP.m9961u(tp));
        if (GrannyGetBlendDagNodeCallbackMotionVectorsCallback == 0) {
            return null;
        }
        return new C0067Ao(GrannyGetBlendDagNodeCallbackMotionVectorsCallback, false);
    }

    /* renamed from: i */
    public static C5501aLt m4928i(C1325TP tp) {
        long GrannyGetBlendDagNodeCallbackUserData = grannyJNI.GrannyGetBlendDagNodeCallbackUserData(C1325TP.m9961u(tp));
        if (GrannyGetBlendDagNodeCallbackUserData == 0) {
            return null;
        }
        return new C5501aLt(GrannyGetBlendDagNodeCallbackUserData, false);
    }

    /* renamed from: j */
    public static C5789aaV m4989j(C1325TP tp) {
        long GrannyGetBlendDagNodeCrossfadeTrackMask = grannyJNI.GrannyGetBlendDagNodeCrossfadeTrackMask(C1325TP.m9961u(tp));
        if (GrannyGetBlendDagNodeCrossfadeTrackMask == 0) {
            return null;
        }
        return new C5789aaV(GrannyGetBlendDagNodeCrossfadeTrackMask, false);
    }

    /* renamed from: a */
    public static boolean m4483a(C1325TP tp, C3159oW oWVar, C3159oW oWVar2) {
        return grannyJNI.GrannyGetBlendDagNodeCrossfadeWeights(C1325TP.m9961u(tp), C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2));
    }

    /* renamed from: k */
    public static C0913NP m5047k(C1325TP tp) {
        long GrannyGetBlendDagNodeWeightedBlendSkeleton = grannyJNI.GrannyGetBlendDagNodeWeightedBlendSkeleton(C1325TP.m9961u(tp));
        if (GrannyGetBlendDagNodeWeightedBlendSkeleton == 0) {
            return null;
        }
        return new C0913NP(GrannyGetBlendDagNodeWeightedBlendSkeleton, false);
    }

    /* renamed from: l */
    public static void m5084l(C1325TP tp) {
        grannyJNI.GrannyBlendDagNodeAnimationBlendFreeCompletedControls(C1325TP.m9961u(tp));
    }

    /* renamed from: m */
    public static void m5089m(C1325TP tp) {
        grannyJNI.GrannyClearBlendDagNodeChildren(C1325TP.m9961u(tp));
    }

    /* renamed from: n */
    public static int m5093n(C1325TP tp) {
        return grannyJNI.GrannyGetBlendDagNodeChildrenCount(C1325TP.m9961u(tp));
    }

    /* renamed from: a */
    public static void m4076a(C1325TP tp, int i, C6211aib aib) {
        grannyJNI.GrannySetBlendDagNodeChildren(C1325TP.m9961u(tp), i, C6211aib.m22616b(aib));
    }

    /* renamed from: a */
    public static void m4075a(C1325TP tp, int i, C1325TP tp2) {
        grannyJNI.GrannySetBlendDagNodeChild(C1325TP.m9961u(tp), i, C1325TP.m9961u(tp2));
    }

    /* renamed from: a */
    public static C1325TP m3752a(C1325TP tp, int i) {
        long GrannyGetBlendDagNodeChild = grannyJNI.GrannyGetBlendDagNodeChild(C1325TP.m9961u(tp), i);
        if (GrannyGetBlendDagNodeChild == 0) {
            return null;
        }
        return new C1325TP(GrannyGetBlendDagNodeChild, false);
    }

    /* renamed from: a */
    public static void m4078a(C1325TP tp, C1325TP tp2) {
        grannyJNI.GrannyAddBlendDagNodeChild(C1325TP.m9961u(tp), C1325TP.m9961u(tp2));
    }

    /* renamed from: b */
    public static void m4628b(C1325TP tp, C1325TP tp2) {
        grannyJNI.GrannyRemoveBlendDagNodeChild(C1325TP.m9961u(tp), C1325TP.m9961u(tp2));
    }

    /* renamed from: o */
    public static C1325TP m5099o(C1325TP tp) {
        long GrannyGetBlendDagNodeParent = grannyJNI.GrannyGetBlendDagNodeParent(C1325TP.m9961u(tp));
        if (GrannyGetBlendDagNodeParent == 0) {
            return null;
        }
        return new C1325TP(GrannyGetBlendDagNodeParent, false);
    }

    /* renamed from: a */
    public static void m4080a(C1325TP tp, C5789aaV aav, boolean z) {
        grannyJNI.GrannySetBlendDagNodeResultTrackMask(C1325TP.m9961u(tp), C5789aaV.m19495f(aav), z);
    }

    /* renamed from: p */
    public static C5789aaV m5105p(C1325TP tp) {
        long GrannyGetBlendDagNodeResultTrackMask = grannyJNI.GrannyGetBlendDagNodeResultTrackMask(C1325TP.m9961u(tp));
        if (GrannyGetBlendDagNodeResultTrackMask == 0) {
            return null;
        }
        return new C5789aaV(GrannyGetBlendDagNodeResultTrackMask, false);
    }

    /* renamed from: a */
    public static void m4070a(C1325TP tp, float f) {
        grannyJNI.GrannySetBlendDagNodeResultWeight(C1325TP.m9961u(tp), f);
    }

    /* renamed from: q */
    public static float m5109q(C1325TP tp) {
        return grannyJNI.GrannyGetBlendDagNodeResultWeight(C1325TP.m9961u(tp));
    }

    /* renamed from: a */
    public static void m4073a(C1325TP tp, float f, C3159oW oWVar, C3159oW oWVar2, boolean z) {
        grannyJNI.GrannyGetBlendDagTreeMotionVectors(C1325TP.m9961u(tp), f, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), z);
    }

    /* renamed from: a */
    public static void m4074a(C1325TP tp, float f, Buffer buffer, Buffer buffer2, boolean z) {
        grannyJNI.GrannyUpdateBlendDagTreeMatrix(C1325TP.m9961u(tp), f, buffer, buffer2, z);
    }

    public static void GrannyPrimeBlendDagLocalPoseCache(int i, int i2) {
        grannyJNI.GrannyPrimeBlendDagLocalPoseCache(i, i2);
    }

    public static void GrannyFreeBlendDagLocalPoseCache() {
        grannyJNI.GrannyFreeBlendDagLocalPoseCache();
    }

    /* renamed from: D */
    public static aOF m3642D(int i, int i2) {
        long GrannyCreateDagPoseCache = grannyJNI.GrannyCreateDagPoseCache(i, i2);
        if (GrannyCreateDagPoseCache == 0) {
            return null;
        }
        return new aOF(GrannyCreateDagPoseCache, false);
    }

    /* renamed from: a */
    public static void m4186a(aOF aof) {
        grannyJNI.GrannyFreeDagPoseCache(aOF.m16805b(aof));
    }

    /* renamed from: a */
    public static aUB m3816a(C1325TP tp, int i, float f, C1845ab abVar, aOF aof) {
        long GrannySampleBlendDagTreeLODSparseReentrant = grannyJNI.GrannySampleBlendDagTreeLODSparseReentrant(C1325TP.m9961u(tp), i, f, C1845ab.m19847a(abVar), aOF.m16805b(aof));
        if (GrannySampleBlendDagTreeLODSparseReentrant == 0) {
            return null;
        }
        return new aUB(GrannySampleBlendDagTreeLODSparseReentrant, false);
    }

    /* renamed from: a */
    public static aUB m3815a(C1325TP tp, int i, float f, C1845ab abVar) {
        long GrannySampleBlendDagTreeLODSparse = grannyJNI.GrannySampleBlendDagTreeLODSparse(C1325TP.m9961u(tp), i, f, C1845ab.m19847a(abVar));
        if (GrannySampleBlendDagTreeLODSparse == 0) {
            return null;
        }
        return new aUB(GrannySampleBlendDagTreeLODSparse, false);
    }

    /* renamed from: b */
    public static aUB m4584b(C1325TP tp, int i) {
        long GrannySampleBlendDagTree = grannyJNI.GrannySampleBlendDagTree(C1325TP.m9961u(tp), i);
        if (GrannySampleBlendDagTree == 0) {
            return null;
        }
        return new aUB(GrannySampleBlendDagTree, false);
    }

    /* renamed from: a */
    public static aUB m3813a(C1325TP tp, int i, float f) {
        long GrannySampleBlendDagTreeLOD = grannyJNI.GrannySampleBlendDagTreeLOD(C1325TP.m9961u(tp), i, f);
        if (GrannySampleBlendDagTreeLOD == 0) {
            return null;
        }
        return new aUB(GrannySampleBlendDagTreeLOD, false);
    }

    /* renamed from: a */
    public static aUB m3817a(C1325TP tp, int i, aOF aof) {
        long GrannySampleBlendDagTreeReentrant = grannyJNI.GrannySampleBlendDagTreeReentrant(C1325TP.m9961u(tp), i, aOF.m16805b(aof));
        if (GrannySampleBlendDagTreeReentrant == 0) {
            return null;
        }
        return new aUB(GrannySampleBlendDagTreeReentrant, false);
    }

    /* renamed from: a */
    public static aUB m3814a(C1325TP tp, int i, float f, aOF aof) {
        long GrannySampleBlendDagTreeLODReentrant = grannyJNI.GrannySampleBlendDagTreeLODReentrant(C1325TP.m9961u(tp), i, f, aOF.m16805b(aof));
        if (GrannySampleBlendDagTreeLODReentrant == 0) {
            return null;
        }
        return new aUB(GrannySampleBlendDagTreeLODReentrant, false);
    }

    /* renamed from: b */
    public static void m4626b(C1325TP tp, float f) {
        grannyJNI.GrannySetBlendDagTreeClock(C1325TP.m9961u(tp), f);
    }

    /* renamed from: b */
    public static void m4627b(C1325TP tp, int i, C6211aib aib) {
        grannyJNI.GrannyGetBlendDagNodeChildren(C1325TP.m9961u(tp), i, C6211aib.m22616b(aib));
    }

    /* renamed from: a */
    public static C1325TP m3751a(C0913NP np, float f, C3807vF vFVar, int i, int i2, C6211aib aib) {
        long GrannyCreateBlendDagNodeWeightedBlendChildren = grannyJNI.GrannyCreateBlendDagNodeWeightedBlendChildren(C0913NP.m7619a(np), f, vFVar.swigValue(), i, i2, C6211aib.m22616b(aib));
        if (GrannyCreateBlendDagNodeWeightedBlendChildren == 0) {
            return null;
        }
        return new C1325TP(GrannyCreateBlendDagNodeWeightedBlendChildren, false);
    }

    /* renamed from: r */
    public static void m5115r(C1325TP tp) {
        grannyJNI.GrannyFreeBlendDagEntireTree(C1325TP.m9961u(tp));
    }

    /* renamed from: s */
    public static void m5119s(C1325TP tp) {
        grannyJNI.GrannyBlendDagFreeCompletedControlsEntireTree(C1325TP.m9961u(tp));
    }

    /* renamed from: a */
    public static C1325TP m3754a(C1325TP tp, C6211aib aib, C6211aib aib2, int i, boolean z) {
        long GrannyDuplicateBlendDagTree = grannyJNI.GrannyDuplicateBlendDagTree(C1325TP.m9961u(tp), C6211aib.m22616b(aib), C6211aib.m22616b(aib2), i, z);
        if (GrannyDuplicateBlendDagTree == 0) {
            return null;
        }
        return new C1325TP(GrannyDuplicateBlendDagTree, false);
    }

    /* renamed from: a */
    public static boolean m4482a(C1325TP tp, C6900avo avo, C6211aib aib) {
        return grannyJNI.GrannyIsBlendDagNodeValid(C1325TP.m9961u(tp), C6900avo.m26731d(avo), C6211aib.m22616b(aib));
    }

    /* renamed from: b */
    public static boolean m4718b(C1325TP tp, C6900avo avo, C6211aib aib) {
        return grannyJNI.GrannyIsBlendDagTreeValid(C1325TP.m9961u(tp), C6900avo.m26731d(avo), C6211aib.m22616b(aib));
    }

    /* renamed from: t */
    public static int m5123t(C1325TP tp) {
        return grannyJNI.GrannyFindBlendDagTreeDepth(C1325TP.m9961u(tp));
    }

    /* renamed from: a */
    public static boolean m4488a(C1731Za za, C5501aLt alt, String str, aSM asm) {
        return grannyJNI.GrannyFindMatchingMember(C1731Za.m12101bi(za), C5501aLt.m16286h(alt), str, aSM.m18172c(asm));
    }

    /* renamed from: a */
    public static void m4114a(C1731Za za, C5501aLt alt, C1731Za za2, C5501aLt alt2) {
        grannyJNI.GrannyConvertSingleObject(C1731Za.m12101bi(za), C5501aLt.m16286h(alt), C1731Za.m12101bi(za2), C5501aLt.m16286h(alt2));
    }

    /* renamed from: b */
    public static void m4631b(C1731Za za, C5501aLt alt, C1731Za za2, C5501aLt alt2) {
        grannyJNI.GrannyMergeSingleObject(C1731Za.m12101bi(za), C5501aLt.m16286h(alt), C1731Za.m12101bi(za2), C5501aLt.m16286h(alt2));
    }

    /* renamed from: a */
    public static C5501aLt m3789a(C1731Za za, C5501aLt alt, C1731Za za2) {
        long GrannyConvertTree = grannyJNI.GrannyConvertTree(C1731Za.m12101bi(za), C5501aLt.m16286h(alt), C1731Za.m12101bi(za2));
        if (GrannyConvertTree == 0) {
            return null;
        }
        return new C5501aLt(GrannyConvertTree, false);
    }

    /* renamed from: b */
    public static int m4574b(C1731Za za, C5501aLt alt, C1731Za za2) {
        return grannyJNI.GrannyGetConvertedTreeSize(C1731Za.m12101bi(za), C5501aLt.m16286h(alt), C1731Za.m12101bi(za2));
    }

    /* renamed from: a */
    public static C5501aLt m3790a(C1731Za za, C5501aLt alt, C1731Za za2, Buffer buffer) {
        long GrannyConvertTreeInPlace = grannyJNI.GrannyConvertTreeInPlace(C1731Za.m12101bi(za), C5501aLt.m16286h(alt), C1731Za.m12101bi(za2), buffer);
        if (GrannyConvertTreeInPlace == 0) {
            return null;
        }
        return new C5501aLt(GrannyConvertTreeInPlace, false);
    }

    /* renamed from: a */
    public static boolean m4487a(C1731Za za, C5501aLt alt, int i, boolean z) {
        return grannyJNI.GrannyRebasePointers(C1731Za.m12101bi(za), C5501aLt.m16286h(alt), i, z);
    }

    /* renamed from: a */
    public static boolean m4486a(C1731Za za, C5501aLt alt, int i, C0083Ax ax, C5501aLt alt2) {
        return grannyJNI.GrannyRebasePointersStringCallback(C1731Za.m12101bi(za), C5501aLt.m16286h(alt), i, C0083Ax.m687a(ax), C5501aLt.m16286h(alt2));
    }

    /* renamed from: a */
    public static C3952xI m3933a(C1731Za za, C5501aLt alt, int i, int i2) {
        long GrannyBeginFileDataTreeWriting = grannyJNI.GrannyBeginFileDataTreeWriting(C1731Za.m12101bi(za), C5501aLt.m16286h(alt), i, i2);
        if (GrannyBeginFileDataTreeWriting == 0) {
            return null;
        }
        return new C3952xI(GrannyBeginFileDataTreeWriting, false);
    }

    /* renamed from: a */
    public static void m4456a(C3952xI xIVar, C6396amE ame) {
        grannyJNI.GrannyPreserveObjectFileSections(C3952xI.m40883a(xIVar), C6396amE.m23854g(ame));
    }

    /* renamed from: b */
    public static void m4712b(C3952xI xIVar) {
        grannyJNI.GrannyEndFileDataTreeWriting(C3952xI.m40883a(xIVar));
    }

    /* renamed from: a */
    public static void m4452a(C3952xI xIVar, long j) {
        grannyJNI.GrannySetFileDataTreeFlags(C3952xI.m40883a(xIVar), j);
    }

    /* renamed from: a */
    public static void m4454a(C3952xI xIVar, C1731Za za, int i) {
        grannyJNI.GrannySetFileSectionForObjectsOfType(C3952xI.m40883a(xIVar), C1731Za.m12101bi(za), i);
    }

    /* renamed from: a */
    public static void m4455a(C3952xI xIVar, C5501aLt alt, int i) {
        grannyJNI.GrannySetFileSectionForObject(C3952xI.m40883a(xIVar), C5501aLt.m16286h(alt), i);
    }

    /* renamed from: a */
    public static boolean m4514a(C3952xI xIVar, C6756atA ata) {
        return grannyJNI.GrannyWriteDataTreeToFileBuilder(C3952xI.m40883a(xIVar), C6756atA.m25817b(ata));
    }

    /* renamed from: a */
    public static boolean m4513a(C3952xI xIVar, long j, C5974ady ady, String str, int i) {
        return grannyJNI.GrannyWriteDataTreeToFile(C3952xI.m40883a(xIVar), j, C5974ady.m21102g(ady), str, i);
    }

    /* renamed from: a */
    public static void m4453a(C3952xI xIVar, C1159R r, C5501aLt alt) {
        grannyJNI.GrannySetFileWriterStringCallback(C3952xI.m40883a(xIVar), C1159R.m9068a(r), C5501aLt.m16286h(alt));
    }

    /* renamed from: a */
    public static boolean m4508a(C3159oW oWVar, long j) {
        return grannyJNI.GrannyClipPositionDOFs(C3159oW.m36741q(oWVar), j);
    }

    /* renamed from: b */
    public static boolean m4726b(C3159oW oWVar, long j) {
        return grannyJNI.GrannyClipAngularVelocityDOFs(C3159oW.m36741q(oWVar), j);
    }

    /* renamed from: c */
    public static boolean m4791c(C3159oW oWVar, long j) {
        return grannyJNI.GrannyClipOrientationDOFs(C3159oW.m36741q(oWVar), j);
    }

    /* renamed from: a */
    public static void m4162a(C5515aMh amh, long j) {
        grannyJNI.GrannyClipTransformDOFs(C5515aMh.m16438f(amh), j);
    }

    /* renamed from: Z */
    public static void m3671Z(C1731Za za) {
        grannyJNI.GrannyExporterInfoType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aTD() {
        long GrannyExporterInfoType_get = grannyJNI.GrannyExporterInfoType_get();
        if (GrannyExporterInfoType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyExporterInfoType_get, false);
    }

    public static boolean GrannyFileCRCIsValid(String str) {
        return grannyJNI.GrannyFileCRCIsValid(str);
    }

    /* renamed from: ex */
    public static C6396amE m4850ex(String str) {
        long GrannyReadEntireFile = grannyJNI.GrannyReadEntireFile(str);
        if (GrannyReadEntireFile == 0) {
            return null;
        }
        return new C6396amE(GrannyReadEntireFile, false);
    }


    /* renamed from: b */
    public static C6396amE m4588b(C3505ri riVar) {
        long GrannyReadEntireFileFromReader = grannyJNI.GrannyReadEntireFileFromReader(C3505ri.m38588a(riVar));
        if (GrannyReadEntireFileFromReader == 0) {
            return null;
        }
        return new C6396amE(GrannyReadEntireFileFromReader, false);
    }

    /* renamed from: c */
    public static C6396amE m4748c(C3505ri riVar) {
        long GrannyReadPartialFileFromReader = grannyJNI.GrannyReadPartialFileFromReader(C3505ri.m38588a(riVar));
        if (GrannyReadPartialFileFromReader == 0) {
            return null;
        }
        return new C6396amE(GrannyReadPartialFileFromReader, false);
    }

    /* renamed from: a */
    public static void m4290a(C6396amE ame, int i) {
        grannyJNI.GrannyFreeFileSection(C6396amE.m23854g(ame), i);
    }

    /* renamed from: a */
    public static void m4289a(C6396amE ame) {
        grannyJNI.GrannyFreeAllFileSections(C6396amE.m23854g(ame));
    }

    /* renamed from: a */
    public static void m4430a(C3505ri riVar, C6396amE ame, int i) {
        grannyJNI.GrannyReadFileSection(C3505ri.m38588a(riVar), C6396amE.m23854g(ame), i);
    }

    /* renamed from: a */
    public static void m4432a(C3505ri riVar, C6396amE ame, int i, Buffer buffer) {
        grannyJNI.GrannyReadFileSectionInPlace(C3505ri.m38588a(riVar), C6396amE.m23854g(ame), i, buffer);
    }

    /* renamed from: a */
    public static void m4291a(C6396amE ame, int i, C0577Hz hz) {
        grannyJNI.GrannyFixupFileSectionPhase1(C6396amE.m23854g(ame), i, C0577Hz.m5296a(hz));
    }

    /* renamed from: a */
    public static void m4431a(C3505ri riVar, C6396amE ame, int i, C0577Hz hz) {
        grannyJNI.GrannyFixupFileSectionPhase2(C3505ri.m38588a(riVar), C6396amE.m23854g(ame), i, C0577Hz.m5296a(hz));
    }

    /* renamed from: a */
    public static C0577Hz m3726a(C3505ri riVar, C4098zR zRVar, boolean z) {
        long GrannyLoadFixupArray = grannyJNI.GrannyLoadFixupArray(C3505ri.m38588a(riVar), C4098zR.m41559a(zRVar), z);
        if (GrannyLoadFixupArray == 0) {
            return null;
        }
        return new C0577Hz(GrannyLoadFixupArray, false);
    }

    /* renamed from: b */
    public static void m4653b(C6396amE ame) {
        grannyJNI.GrannyFreeFile(C6396amE.m23854g(ame));
    }

    /* renamed from: a */
    public static int m3693a(C6396amE ame, C5501aLt alt) {
        return grannyJNI.GrannyGetFileSectionOfLoadedObject(C6396amE.m23854g(ame), C5501aLt.m16286h(alt));
    }

    /* renamed from: a */
    public static void m4292a(C6396amE ame, aSM asm) {
        grannyJNI.GrannyGetDataTreeFromFile(C6396amE.m23854g(ame), aSM.m18172c(asm));
    }

    /* renamed from: c */
    public static long m4744c(C6396amE ame) {
        return grannyJNI.GrannyGetFileTypeTag(C6396amE.m23854g(ame));
    }

    /* renamed from: a */
    public static boolean m4515a(String str, String str2, int i, C1845ab abVar) {
        return grannyJNI.GrannyRecompressFile(str, str2, i, C1845ab.m19847a(abVar));
    }

    /* renamed from: a */
    public static boolean m4506a(C2267dS dSVar, String str) {
        return grannyJNI.GrannyConvertFileInfoToRaw(C2267dS.m28965a(dSVar), str);
    }

    public static boolean GrannyConvertFileToRaw(String str, String str2) {
        return grannyJNI.GrannyConvertFileToRaw(str, str2);
    }

    /* renamed from: a */
    public static boolean m4511a(C3505ri riVar, C5731aUp aup, C5974ady ady, boolean z) {
        return grannyJNI.GrannyPlatformConvertReaderToWriter(C3505ri.m38588a(riVar), C5731aUp.m18737b(aup), C5974ady.m21102g(ady), z);
    }

    /* renamed from: d */
    public static long m4798d(C6396amE ame) {
        return grannyJNI.GrannyGetInMemoryFileCRC(C6396amE.m23854g(ame));
    }

    /* renamed from: a */
    public static C6756atA m3874a(int i, long j, C5974ady ady, String str, String str2) {
        long GrannyBeginFile = grannyJNI.GrannyBeginFile(i, j, C5974ady.m21102g(ady), str, str2);
        if (GrannyBeginFile == 0) {
            return null;
        }
        return new C6756atA(GrannyBeginFile, false);
    }

    /* renamed from: a */
    public static C6756atA m3873a(int i, long j, C5974ady ady, int i2) {
        long GrannyBeginFileInMemory = grannyJNI.GrannyBeginFileInMemory(i, j, C5974ady.m21102g(ady), i2);
        if (GrannyBeginFileInMemory == 0) {
            return null;
        }
        return new C6756atA(GrannyBeginFileInMemory, false);
    }

    /* renamed from: a */
    public static boolean m4504a(C6756atA ata, String str) {
        return grannyJNI.GrannyEndFile(C6756atA.m25817b(ata), str);
    }

    /* renamed from: a */
    public static boolean m4503a(C6756atA ata, C5731aUp aup) {
        return grannyJNI.GrannyEndFileToWriter(C6756atA.m25817b(ata), C5731aUp.m18737b(aup));
    }

    /* renamed from: b */
    public static boolean m4723b(C6756atA ata, String str) {
        return grannyJNI.GrannyEndFileRaw(C6756atA.m25817b(ata), str);
    }

    /* renamed from: b */
    public static boolean m4722b(C6756atA ata, C5731aUp aup) {
        return grannyJNI.GrannyEndFileRawToWriter(C6756atA.m25817b(ata), C5731aUp.m18737b(aup));
    }

    /* renamed from: a */
    public static void m4333a(C6756atA ata) {
        grannyJNI.GrannyAbortFile(C6756atA.m25817b(ata));
    }

    /* renamed from: a */
    public static void m4336a(C6756atA ata, int i, C5266aCs acs, int i2) {
        grannyJNI.GrannySetFileSectionFormat(C6756atA.m25817b(ata), i, acs.swigValue(), i2);
    }

    /* renamed from: a */
    public static void m4338a(C6756atA ata, C6396amE ame) {
        grannyJNI.GrannyPreserveFileSectionFormats(C6756atA.m25817b(ata), C6396amE.m23854g(ame));
    }

    /* renamed from: a */
    public static void m4335a(C6756atA ata, int i, long j, long j2, C5501aLt alt, C2481ff ffVar) {
        grannyJNI.GrannyWriteFileChunk(C6756atA.m25817b(ata), i, j, j2, C5501aLt.m16286h(alt), C2481ff.m31245a(ffVar));
    }

    /* renamed from: a */
    public static void m4340a(C6756atA ata, C2481ff ffVar, long j, C2481ff ffVar2) {
        grannyJNI.GrannyOffsetFileLocation(C6756atA.m25817b(ata), C2481ff.m31245a(ffVar), j, C2481ff.m31245a(ffVar2));
    }

    /* renamed from: a */
    public static C6932awU m3883a(C6756atA ata, C2481ff ffVar, int i, C2481ff ffVar2) {
        long GrannyMarkFileFixup = grannyJNI.GrannyMarkFileFixup(C6756atA.m25817b(ata), C2481ff.m31245a(ffVar), i, C2481ff.m31245a(ffVar2));
        if (GrannyMarkFileFixup == 0) {
            return null;
        }
        return new C6932awU(GrannyMarkFileFixup, false);
    }

    /* renamed from: a */
    public static void m4339a(C6756atA ata, C6932awU awu, C2481ff ffVar) {
        grannyJNI.GrannyAdjustFileFixup(C6756atA.m25817b(ata), C6932awU.m26879a(awu), C2481ff.m31245a(ffVar));
    }

    /* renamed from: a */
    public static void m4342a(C6756atA ata, C2481ff ffVar, C2481ff ffVar2, int i) {
        grannyJNI.GrannyMarkMarshallingFixup(C6756atA.m25817b(ata), C2481ff.m31245a(ffVar), C2481ff.m31245a(ffVar2), i);
    }

    /* renamed from: a */
    public static void m4341a(C6756atA ata, C2481ff ffVar, C2481ff ffVar2) {
        grannyJNI.GrannyMarkFileRootObject(C6756atA.m25817b(ata), C2481ff.m31245a(ffVar), C2481ff.m31245a(ffVar2));
    }

    /* renamed from: a */
    public static void m4337a(C6756atA ata, long j) {
        grannyJNI.GrannySetFileStringDatabaseCRC(C6756atA.m25817b(ata), j);
    }

    /* renamed from: a */
    public static void m4334a(C6756atA ata, int i, long j) {
        grannyJNI.GrannySetFileExtraTag(C6756atA.m25817b(ata), i, j);
    }

    /* renamed from: a */
    public static int m3688a(C5266aCs acs) {
        return grannyJNI.GrannyGetCompressedBytesPaddingSize(acs.swigValue());
    }

    /* renamed from: a */
    public static boolean m4489a(C5266aCs acs, boolean z, int i, C5501aLt alt, int i2, int i3, int i4, C5501aLt alt2) {
        return grannyJNI.GrannyDecompressData(acs.swigValue(), z, i, C5501aLt.m16286h(alt), i2, i3, i4, C5501aLt.m16286h(alt2));
    }

    /* renamed from: a */
    public static boolean m4490a(C5266aCs acs, boolean z, int i, C5501aLt alt, int i2, C5501aLt alt2) {
        return grannyJNI.GrannyDecompressDataChunk(acs.swigValue(), z, i, C5501aLt.m16286h(alt), i2, C5501aLt.m16286h(alt2));
    }

    /* renamed from: a */
    public static C0207CY m3715a(long j, int i, C5266aCs acs, boolean z, C5731aUp aup) {
        long GrannyBeginFileCompression = grannyJNI.GrannyBeginFileCompression(j, i, acs.swigValue(), z, C5731aUp.m18737b(aup));
        if (GrannyBeginFileCompression == 0) {
            return null;
        }
        return new C0207CY(GrannyBeginFileCompression, false);
    }

    /* renamed from: a */
    public static boolean m4472a(C0207CY cy, int i, String str, int i2, C5501aLt alt) {
        return grannyJNI.GrannyCompressContentsOfFile(C0207CY.m1809a(cy), i, str, i2, C5501aLt.m16286h(alt));
    }

    /* renamed from: a */
    public static boolean m4471a(C0207CY cy, int i, C3505ri riVar, int i2, C5501aLt alt) {
        return grannyJNI.GrannyCompressContentsOfReader(C0207CY.m1809a(cy), i, C3505ri.m38588a(riVar), i2, C5501aLt.m16286h(alt));
    }

    /* renamed from: a */
    public static boolean m4470a(C0207CY cy, int i, C5501aLt alt) {
        return grannyJNI.GrannyCompressContentsOfMemory(C0207CY.m1809a(cy), i, C5501aLt.m16286h(alt));
    }

    /* renamed from: a */
    public static boolean m4473a(C0207CY cy, C6135ahD ahd) {
        return grannyJNI.GrannyEndFileCompression(C0207CY.m1809a(cy), C6135ahD.m22189i(ahd));
    }

    public static String GrannyGetStandardSectionName(int i) {
        return grannyJNI.GrannyGetStandardSectionName(i);
    }

    /* renamed from: a */
    public static boolean m4496a(C5974ady ady, C6135ahD ahd, C1418Um um, C1418Um um2) {
        return grannyJNI.GrannyIsGrannyFile(C5974ady.m21102g(ady), C6135ahD.m22189i(ahd), C1418Um.m10339a(um), C1418Um.m10339a(um2));
    }

    /* renamed from: a */
    public static C4098zR m3935a(C5827abH abh) {
        long GrannyGetGRNSectionArray = grannyJNI.GrannyGetGRNSectionArray(C5827abH.m19928b(abh));
        if (GrannyGetGRNSectionArray == 0) {
            return null;
        }
        return new C4098zR(GrannyGetGRNSectionArray, false);
    }

    /* renamed from: a */
    public static void m3946a(int i, int i2, int i3, C5501aLt alt) {
        grannyJNI.GrannyReverseSection(i, i2, i3, C5501aLt.m16286h(alt));
    }

    /* renamed from: a */
    public static void m3966a(long j, C0577Hz hz, C5269aCv acv, C5501aLt alt) {
        grannyJNI.GrannyGRNFixUp(j, C0577Hz.m5296a(hz), C5269aCv.m13385b(acv), C5501aLt.m16286h(alt));
    }

    /* renamed from: a */
    public static void m3965a(long j, C0347Ec ec, C5269aCv acv, C5501aLt alt) {
        grannyJNI.GrannyGRNMarshall(j, C0347Ec.m2989a(ec), C5269aCv.m13385b(acv), C5501aLt.m16286h(alt));
    }

    /* renamed from: a */
    public static C5501aLt m3791a(C5269aCv acv, C0031AQ aq) {
        long GrannyDecodeGRNReference = grannyJNI.GrannyDecodeGRNReference(C5269aCv.m13385b(acv), C0031AQ.m385a(aq));
        if (GrannyDecodeGRNReference == 0) {
            return null;
        }
        return new C5501aLt(GrannyDecodeGRNReference, false);
    }

    /* renamed from: a */
    public static boolean m4468a(int i, boolean z, C5974ady ady) {
        return grannyJNI.GrannyGetMagicValueForPlatform(i, z, C5974ady.m21102g(ady));
    }

    /* renamed from: a */
    public static void m4243a(C1845ab abVar, C1418Um um) {
        grannyJNI.GrannyGetThisPlatformProperties(C1845ab.m19847a(abVar), C1418Um.m10339a(um));
    }

    /* renamed from: a */
    public static boolean m4495a(C5974ady ady, C1845ab abVar, C1418Um um) {
        return grannyJNI.GrannyGetMagicValueProperties(C5974ady.m21102g(ady), C1845ab.m19847a(abVar), C1418Um.m10339a(um));
    }

    /* renamed from: a */
    public static void m4256a(C5974ady ady) {
        grannyJNI.GrannyGRNFileMV_Old_set(C5974ady.m21102g(ady));
    }

    public static C5974ady aTE() {
        long GrannyGRNFileMV_Old_get = grannyJNI.GrannyGRNFileMV_Old_get();
        if (GrannyGRNFileMV_Old_get == 0) {
            return null;
        }
        return new C5974ady(GrannyGRNFileMV_Old_get, false);
    }

    /* renamed from: b */
    public static void m4650b(C5974ady ady) {
        grannyJNI.GrannyGRNFileMV_32Bit_LittleEndian_set(C5974ady.m21102g(ady));
    }

    public static C5974ady aTF() {
        long GrannyGRNFileMV_32Bit_LittleEndian_get = grannyJNI.GrannyGRNFileMV_32Bit_LittleEndian_get();
        if (GrannyGRNFileMV_32Bit_LittleEndian_get == 0) {
            return null;
        }
        return new C5974ady(GrannyGRNFileMV_32Bit_LittleEndian_get, false);
    }

    /* renamed from: c */
    public static void m4769c(C5974ady ady) {
        grannyJNI.GrannyGRNFileMV_32Bit_BigEndian_set(C5974ady.m21102g(ady));
    }

    public static C5974ady aTG() {
        long GrannyGRNFileMV_32Bit_BigEndian_get = grannyJNI.GrannyGRNFileMV_32Bit_BigEndian_get();
        if (GrannyGRNFileMV_32Bit_BigEndian_get == 0) {
            return null;
        }
        return new C5974ady(GrannyGRNFileMV_32Bit_BigEndian_get, false);
    }

    /* renamed from: d */
    public static void m4819d(C5974ady ady) {
        grannyJNI.GrannyGRNFileMV_64Bit_LittleEndian_set(C5974ady.m21102g(ady));
    }

    public static C5974ady aTH() {
        long GrannyGRNFileMV_64Bit_LittleEndian_get = grannyJNI.GrannyGRNFileMV_64Bit_LittleEndian_get();
        if (GrannyGRNFileMV_64Bit_LittleEndian_get == 0) {
            return null;
        }
        return new C5974ady(GrannyGRNFileMV_64Bit_LittleEndian_get, false);
    }

    /* renamed from: e */
    public static void m4841e(C5974ady ady) {
        grannyJNI.GrannyGRNFileMV_64Bit_BigEndian_set(C5974ady.m21102g(ady));
    }

    public static C5974ady aTI() {
        long GrannyGRNFileMV_64Bit_BigEndian_get = grannyJNI.GrannyGRNFileMV_64Bit_BigEndian_get();
        if (GrannyGRNFileMV_64Bit_BigEndian_get == 0) {
            return null;
        }
        return new C5974ady(GrannyGRNFileMV_64Bit_BigEndian_get, false);
    }

    /* renamed from: f */
    public static void m4862f(C5974ady ady) {
        grannyJNI.GrannyGRNFileMV_ThisPlatform_set(C5974ady.m21102g(ady));
    }

    public static C5974ady aTJ() {
        long GrannyGRNFileMV_ThisPlatform_get = grannyJNI.GrannyGRNFileMV_ThisPlatform_get();
        if (GrannyGRNFileMV_ThisPlatform_get == 0) {
            return null;
        }
        return new C5974ady(GrannyGRNFileMV_ThisPlatform_get, false);
    }

    /* renamed from: aa */
    public static void m4542aa(C1731Za za) {
        grannyJNI.GrannyFileInfoType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aTK() {
        long GrannyFileInfoType_get = grannyJNI.GrannyFileInfoType_get();
        if (GrannyFileInfoType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyFileInfoType_get, false);
    }

    public static String GrannyGetTemporaryDirectory() {
        return grannyJNI.GrannyGetTemporaryDirectory();
    }

    /* renamed from: a */
    public static void m4460a(String str, int i, C2874lI lIVar, C5795aab aab, C3505ri riVar) {
        grannyJNI.GrannyInitializeFileReader(str, i, C2874lI.m34779a(lIVar), C5795aab.m19538b(aab), C3505ri.m38588a(riVar));
    }

    /* renamed from: c */
    public static C3505ri m4752c(String str, int i, String str2) {
        long GrannyCreatePlatformFileReader = grannyJNI.GrannyCreatePlatformFileReader(str, i, str2);
        if (GrannyCreatePlatformFileReader == 0) {
            return null;
        }
        return new C3505ri(GrannyCreatePlatformFileReader, false);
    }

    public static C5547aNn aTL() {
        long GrannyGetDefaultFileReaderOpenCallback = grannyJNI.GrannyGetDefaultFileReaderOpenCallback();
        if (GrannyGetDefaultFileReaderOpenCallback == 0) {
            return null;
        }
        return new C5547aNn(GrannyGetDefaultFileReaderOpenCallback, false);
    }

    /* renamed from: a */
    public static void m4179a(C5547aNn ann) {
        grannyJNI.GrannySetDefaultFileReaderOpenCallback(C5547aNn.m16774b(ann));
    }

    /* renamed from: a */
    public static int m3690a(C5731aUp aup) {
        return grannyJNI.GrannyAlignWriter(C5731aUp.m18737b(aup));
    }

    public static int GrannyPredictWriterAlignment(int i) {
        return grannyJNI.GrannyPredictWriterAlignment(i);
    }

    /* renamed from: a */
    public static void m4461a(String str, int i, C3386qx qxVar, C6358alS als, C6487anr anr, C6586apm apm, C2510gA gAVar, C5731aUp aup) {
        grannyJNI.GrannyInitializeFileWriter(str, i, C3386qx.m37893a(qxVar), C6358alS.m23715a(als), C6487anr.m24338a(anr), C6586apm.m25008a(apm), C2510gA.m31649a(gAVar), C5731aUp.m18737b(aup));
    }

    /* renamed from: a */
    public static C5731aUp m3820a(String str, int i, String str2, boolean z) {
        long GrannyCreatePlatformFileWriter = grannyJNI.GrannyCreatePlatformFileWriter(str, i, str2, z);
        if (GrannyCreatePlatformFileWriter == 0) {
            return null;
        }
        return new C5731aUp(GrannyCreatePlatformFileWriter, false);
    }

    public static aVX aTM() {
        long GrannyGetDefaultFileWriterOpenCallback = grannyJNI.GrannyGetDefaultFileWriterOpenCallback();
        if (GrannyGetDefaultFileWriterOpenCallback == 0) {
            return null;
        }
        return new aVX(GrannyGetDefaultFileWriterOpenCallback, false);
    }

    /* renamed from: a */
    public static void m4226a(aVX avx) {
        grannyJNI.GrannySetDefaultFileWriterOpenCallback(aVX.m18876b(avx));
    }

    /* renamed from: a */
    public static int m3705a(String str, int i, C5731aUp aup, int i2) {
        return grannyJNI.GrannySeekWriterFromStartStub(str, i, C5731aUp.m18737b(aup), i2);
    }

    /* renamed from: b */
    public static int m4578b(String str, int i, C5731aUp aup, int i2) {
        return grannyJNI.GrannySeekWriterFromEndStub(str, i, C5731aUp.m18737b(aup), i2);
    }

    /* renamed from: c */
    public static int m4743c(String str, int i, C5731aUp aup, int i2) {
        return grannyJNI.GrannySeekWriterFromCurrentPositionStub(str, i, C5731aUp.m18737b(aup), i2);
    }

    /* renamed from: a */
    public static int m3679a(int i, C3159oW oWVar, float f) {
        return grannyJNI.GrannyFindKnot(i, C3159oW.m36741q(oWVar), f);
    }

    /* renamed from: a */
    public static int m3680a(int i, C3159oW oWVar, float f, int i2) {
        return grannyJNI.GrannyFindCloseKnot(i, C3159oW.m36741q(oWVar), f, i2);
    }

    /* renamed from: a */
    public static C5501aLt m3792a(aIE aie) {
        long GrannyAllocateFixed = grannyJNI.GrannyAllocateFixed(aIE.m15360c(aie));
        if (GrannyAllocateFixed == 0) {
            return null;
        }
        return new C5501aLt(GrannyAllocateFixed, false);
    }

    /* renamed from: a */
    public static void m4144a(aIE aie, Buffer buffer) {
        grannyJNI.GrannyDeallocateFixed(aIE.m15360c(aie), buffer);
    }

    /* renamed from: b */
    public static void m4634b(aIE aie) {
        grannyJNI.GrannyDeallocateAllFixed(aIE.m15360c(aie));
    }

    /* renamed from: a */
    public static void m4143a(aIE aie, int i) {
        grannyJNI.GrannyInitializeFixedAllocator(aIE.m15360c(aie), i);
    }

    public static int GrannyReal32ToReal16(float f) {
        return grannyJNI.GrannyReal32ToReal16(f);
    }

    /* renamed from: a */
    public static void m3960a(int i, C3159oW oWVar) {
        grannyJNI.GrannyReal16ToReal32(i, C3159oW.m36741q(oWVar));
    }

    /* renamed from: a */
    public static void m3950a(int i, int i2, C3159oW oWVar, int i3, C0913NP np, C3159oW oWVar2, aUB aub, C3894wP wPVar) {
        grannyJNI.GrannyIKUpdate(i, i2, C3159oW.m36741q(oWVar), i3, C0913NP.m7619a(np), C3159oW.m36741q(oWVar2), aUB.m18499d(aub), C3894wP.m40635a(wPVar));
    }

    /* renamed from: a */
    public static boolean m4466a(int i, C3159oW oWVar, C3159oW oWVar2, C0913NP np, C3159oW oWVar3, aUB aub, C3894wP wPVar, float f, float f2) {
        return grannyJNI.GrannyIKUpdate2Bone(i, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C0913NP.m7619a(np), C3159oW.m36741q(oWVar3), aUB.m18499d(aub), C3894wP.m40635a(wPVar), f, f2);
    }

    /* renamed from: a */
    public static void m3986a(C0430Fy fy, int i, int i2, int i3, C2382ef efVar, int i4, int i5, int i6, C2382ef efVar2) {
        grannyJNI.GrannyScaleImage(fy.swigValue(), i, i2, i3, C2382ef.m29966c(efVar), i4, i5, i6, C2382ef.m29966c(efVar2));
    }

    /* renamed from: a */
    public static int m3697a(C3159oW oWVar, float f, C3159oW oWVar2, C3159oW oWVar3, C3159oW oWVar4) {
        return grannyJNI.GrannyRayIntersectsPlaneAt(C3159oW.m36741q(oWVar), f, C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), C3159oW.m36741q(oWVar4));
    }

    /* renamed from: a */
    public static boolean m4507a(C3159oW oWVar, float f, C3159oW oWVar2, C3159oW oWVar3) {
        return grannyJNI.GrannyRayIntersectsSphere(C3159oW.m36741q(oWVar), f, C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3));
    }

    /* renamed from: a */
    public static int m3698a(C3159oW oWVar, float f, C3159oW oWVar2, C3159oW oWVar3, C3159oW oWVar4, C3159oW oWVar5) {
        return grannyJNI.GrannyRayIntersectsSphereAt(C3159oW.m36741q(oWVar), f, C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), C3159oW.m36741q(oWVar4), C3159oW.m36741q(oWVar5));
    }

    /* renamed from: a */
    public static int m3699a(C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, C3159oW oWVar4, C3159oW oWVar5) {
        return grannyJNI.GrannyRayIntersectsBox(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), C3159oW.m36741q(oWVar4), C3159oW.m36741q(oWVar5));
    }

    /* renamed from: a */
    public static int m3701a(C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, C3159oW oWVar4, C3159oW oWVar5, C5458aKc akc) {
        return grannyJNI.GrannyRayIntersectsBoxAt(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), C3159oW.m36741q(oWVar4), C3159oW.m36741q(oWVar5), C5458aKc.m16023a(akc));
    }

    /* renamed from: a */
    public static int m3700a(C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, C3159oW oWVar4, C3159oW oWVar5, C0206CX cx) {
        return grannyJNI.GrannyRayIntersectsTriangleAt(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), C3159oW.m36741q(oWVar4), C3159oW.m36741q(oWVar5), C0206CX.m1802a(cx));
    }

    public static int GrannyGetResultingLocalPoseSize(int i) {
        return grannyJNI.GrannyGetResultingLocalPoseSize(i);
    }

    /* renamed from: b */
    public static aUB m4583b(int i, Buffer buffer) {
        long GrannyNewLocalPoseInPlace = grannyJNI.GrannyNewLocalPoseInPlace(i, buffer);
        if (GrannyNewLocalPoseInPlace == 0) {
            return null;
        }
        return new aUB(GrannyNewLocalPoseInPlace, false);
    }

    /* renamed from: b */
    public static int m4576b(aUB aub) {
        return grannyJNI.GrannyGetLocalPoseBoneCount(aUB.m18499d(aub));
    }

    /* renamed from: a */
    public static void m4018a(C0913NP np, int i, int i2, aUB aub) {
        grannyJNI.GrannyBuildRestLocalPose(C0913NP.m7619a(np), i, i2, aUB.m18499d(aub));
    }

    /* renamed from: a */
    public static void m4217a(aUB aub, int i, int i2, C1845ab abVar) {
        grannyJNI.GrannyBeginLocalPoseAccumulation(aUB.m18499d(aub), i, i2, C1845ab.m19847a(abVar));
    }

    /* renamed from: a */
    public static void m4214a(aUB aub, int i, int i2, float f, C0913NP np, C3807vF vFVar, C5515aMh amh) {
        grannyJNI.GrannyAccumulateLocalTransform(aUB.m18499d(aub), i, i2, f, C0913NP.m7619a(np), vFVar.swigValue(), C5515aMh.m16438f(amh));
    }

    /* renamed from: a */
    public static void m4216a(aUB aub, int i, int i2, C0913NP np, C1845ab abVar) {
        grannyJNI.GrannyEndLocalPoseAccumulation(aUB.m18499d(aub), i, i2, C0913NP.m7619a(np), C1845ab.m19847a(abVar));
    }

    /* renamed from: a */
    public static void m4215a(aUB aub, int i, int i2, C0913NP np, float f, C1845ab abVar) {
        grannyJNI.GrannyEndLocalPoseAccumulationLOD(aUB.m18499d(aub), i, i2, C0913NP.m7619a(np), f, C1845ab.m19847a(abVar));
    }

    /* renamed from: c */
    public static float m4735c(aUB aub) {
        return grannyJNI.GrannyGetLocalPoseFillThreshold(aUB.m18499d(aub));
    }

    /* renamed from: a */
    public static void m4211a(aUB aub, float f) {
        grannyJNI.GrannySetLocalPoseFillThreshold(aUB.m18499d(aub), f);
    }

    /* renamed from: a */
    public static void m4025a(C0913NP np, int i, aUB aub, Buffer buffer, C3159oW oWVar, C1845ab abVar, C1845ab abVar2) {
        grannyJNI.GrannyGetWorldMatrixFromLocalPose(C0913NP.m7619a(np), i, aUB.m18499d(aub), buffer, C3159oW.m36741q(oWVar), C1845ab.m19847a(abVar), C1845ab.m19847a(abVar2));
    }

    /* renamed from: b */
    public static void m4621b(C0913NP np, int i, aUB aub, Buffer buffer, C3159oW oWVar, C1845ab abVar, C1845ab abVar2) {
        grannyJNI.GrannyGetAttachmentOffset(C0913NP.m7619a(np), i, aUB.m18499d(aub), buffer, C3159oW.m36741q(oWVar), C1845ab.m19847a(abVar), C1845ab.m19847a(abVar2));
    }

    /* renamed from: a */
    public static void m4213a(aUB aub, float f, float f2, C5789aaV aav, aUB aub2, C1845ab abVar) {
        grannyJNI.GrannyModulationCompositeLocalPoseSparse(aUB.m18499d(aub), f, f2, C5789aaV.m19495f(aav), aUB.m18499d(aub2), C1845ab.m19847a(abVar));
    }

    /* renamed from: a */
    public static void m4028a(C0913NP np, aUB aub, C3894wP wPVar, Buffer buffer, int i, int i2) {
        grannyJNI.GrannyLocalPoseFromWorldPose(C0913NP.m7619a(np), aUB.m18499d(aub), C3894wP.m40635a(wPVar), buffer, i, i2);
    }

    /* renamed from: b */
    public static void m4622b(C0913NP np, aUB aub, C3894wP wPVar, Buffer buffer, int i, int i2) {
        grannyJNI.GrannyLocalPoseFromWorldPoseNoScale(C0913NP.m7619a(np), aUB.m18499d(aub), C3894wP.m40635a(wPVar), buffer, i, i2);
    }

    /* renamed from: a */
    public static void m4212a(aUB aub, float f, float f2, C5789aaV aav, aUB aub2) {
        grannyJNI.GrannyModulationCompositeLocalPose(aUB.m18499d(aub), f, f2, C5789aaV.m19495f(aav), aUB.m18499d(aub2));
    }

    /* renamed from: a */
    public static void m4218a(aUB aub, aUB aub2) {
        grannyJNI.GrannyCopyLocalPose(aUB.m18499d(aub), aUB.m18499d(aub2));
    }

    /* renamed from: a */
    public static int m3683a(C0913NP np, int i, C1845ab abVar, C1845ab abVar2) {
        return grannyJNI.GrannySparseBoneArrayCreateSingleBone(C0913NP.m7619a(np), i, C1845ab.m19847a(abVar), C1845ab.m19847a(abVar2));
    }

    /* renamed from: a */
    public static int m3682a(C0913NP np, int i, int i2, C1845ab abVar, C1845ab abVar2) {
        return grannyJNI.GrannySparseBoneArrayAddBone(C0913NP.m7619a(np), i, i2, C1845ab.m19847a(abVar), C1845ab.m19847a(abVar2));
    }

    /* renamed from: a */
    public static void m4027a(C0913NP np, aUB aub, int i, C1845ab abVar, C1845ab abVar2, int i2, aUB aub2) {
        grannyJNI.GrannySparseBoneArrayExpand(C0913NP.m7619a(np), aUB.m18499d(aub), i, C1845ab.m19847a(abVar), C1845ab.m19847a(abVar2), i2, aUB.m18499d(aub2));
    }

    /* renamed from: a */
    public static boolean m4465a(int i, int i2, C1845ab abVar, C1845ab abVar2) {
        return grannyJNI.GrannySparseBoneArrayIsValid(i, i2, C1845ab.m19847a(abVar), C1845ab.m19847a(abVar2));
    }

    /* renamed from: ab */
    public static void m4543ab(C1731Za za) {
        grannyJNI.GrannyLocalPoseTransformType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aTN() {
        long GrannyLocalPoseTransformType_get = grannyJNI.GrannyLocalPoseTransformType_get();
        if (GrannyLocalPoseTransformType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyLocalPoseTransformType_get, false);
    }

    /* renamed from: ac */
    public static void m4544ac(C1731Za za) {
        grannyJNI.GrannyLocalPoseType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aTO() {
        long GrannyLocalPoseType_get = grannyJNI.GrannyLocalPoseType_get();
        if (GrannyLocalPoseType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyLocalPoseType_get, false);
    }

    public static boolean GrannySetLogFileName(String str, boolean z) {
        return grannyJNI.GrannySetLogFileName(str, z);
    }

    /* renamed from: a */
    public static void m4354a(C6870avK avk) {
        grannyJNI.GrannyGetLogCallback(C6870avK.m26571c(avk));
    }

    /* renamed from: b */
    public static void m4664b(C6870avK avk) {
        grannyJNI.GrannySetLogCallback(C6870avK.m26571c(avk));
    }

    /* renamed from: a */
    public static String m3939a(C6427amj amj) {
        return grannyJNI.GrannyGetLogMessageTypeString(amj.swigValue());
    }

    /* renamed from: a */
    public static String m3943a(C3338qS qSVar) {
        return grannyJNI.GrannyGetLogMessageOriginString(qSVar.swigValue());
    }

    public static boolean GrannyLogging() {
        return grannyJNI.GrannyLogging();
    }

    /* renamed from: a */
    public static void m4427a(C3338qS qSVar, boolean z) {
        grannyJNI.GrannyFilterMessage(qSVar.swigValue(), z);
    }

    public static void GrannyFilterAllMessages(boolean z) {
        grannyJNI.GrannyFilterAllMessages(z);
    }

    public static C6427amj aTP() {
        return C6427amj.m23977sA(grannyJNI.GrannyGetMostSeriousMessageType());
    }

    public static String GrannyGetMostSeriousMessage() {
        return grannyJNI.GrannyGetMostSeriousMessage();
    }

    public static void GrannyClearMostSeriousMessage() {
        grannyJNI.GrannyClearMostSeriousMessage();
    }

    /* renamed from: b */
    public static void m4601b(int i, C3159oW oWVar) {
        grannyJNI.GrannyEnsureQuaternionContinuity(i, C3159oW.m36741q(oWVar));
    }

    /* renamed from: b */
    public static boolean m4725b(C3159oW oWVar, float f, C3159oW oWVar2, C3159oW oWVar3) {
        return grannyJNI.GrannyPolarDecompose(C3159oW.m36741q(oWVar), f, C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3));
    }

    /* renamed from: a */
    public static void m4419a(C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3) {
        grannyJNI.GrannyColumnMatrixMultiply4x3(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3));
    }

    /* renamed from: b */
    public static void m4696b(C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3) {
        grannyJNI.GrannyColumnMatrixMultiply4x3Transpose(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3));
    }

    /* renamed from: c */
    public static void m4781c(C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3) {
        grannyJNI.GrannyColumnMatrixMultiply4x4(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3));
    }

    /* renamed from: a */
    public static void m4415a(C3159oW oWVar, C3159oW oWVar2) {
        grannyJNI.GrannyMatrixEqualsQuaternion3x3(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2));
    }

    /* renamed from: b */
    public static void m4694b(C3159oW oWVar, C3159oW oWVar2) {
        grannyJNI.GrannyQuaternionEqualsMatrix3x3(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2));
    }

    /* renamed from: d */
    public static void m4822d(C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3) {
        grannyJNI.GrannyInPlaceSimilarityTransformPosition(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3));
    }

    /* renamed from: e */
    public static void m4845e(C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3) {
        grannyJNI.GrannyInPlaceSimilarityTransformOrientation(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3));
    }

    /* renamed from: f */
    public static void m4866f(C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3) {
        grannyJNI.GrannyInPlaceSimilarityTransformScaleShear(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3));
    }

    /* renamed from: a */
    public static void m4423a(C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, C3159oW oWVar4, C3159oW oWVar5, C3159oW oWVar6) {
        grannyJNI.GrannyInPlaceSimilarityTransform(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), C3159oW.m36741q(oWVar4), C3159oW.m36741q(oWVar5), C3159oW.m36741q(oWVar6));
    }

    /* renamed from: a */
    public static void m4421a(C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, C3159oW oWVar4) {
        grannyJNI.GrannyInPlaceSimilarityTransform4x3(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), C3159oW.m36741q(oWVar4));
    }

    public static C6114agi aTQ() {
        long GrannyAllocationsBegin = grannyJNI.GrannyAllocationsBegin();
        if (GrannyAllocationsBegin == 0) {
            return null;
        }
        return new C6114agi(GrannyAllocationsBegin, false);
    }

    /* renamed from: a */
    public static C6114agi m3842a(C6114agi agi) {
        long GrannyNextAllocation = grannyJNI.GrannyNextAllocation(C6114agi.m22105b(agi));
        if (GrannyNextAllocation == 0) {
            return null;
        }
        return new C6114agi(GrannyNextAllocation, false);
    }

    public static C6114agi aTR() {
        long GrannyAllocationsEnd = grannyJNI.GrannyAllocationsEnd();
        if (GrannyAllocationsEnd == 0) {
            return null;
        }
        return new C6114agi(GrannyAllocationsEnd, false);
    }

    /* renamed from: a */
    public static void m4267a(C6114agi agi, C0431Fz fz) {
        grannyJNI.GrannyGetAllocationInformation(C6114agi.m22105b(agi), C0431Fz.m3301a(fz));
    }

    public static C5501aLt aTS() {
        long GrannyBeginAllocationCheck = grannyJNI.GrannyBeginAllocationCheck();
        if (GrannyBeginAllocationCheck == 0) {
            return null;
        }
        return new C5501aLt(GrannyBeginAllocationCheck, false);
    }

    /* renamed from: b */
    public static C6114agi m4587b(C5501aLt alt) {
        long GrannyCheckedAllocationsEnd = grannyJNI.GrannyCheckedAllocationsEnd(C5501aLt.m16286h(alt));
        if (GrannyCheckedAllocationsEnd == 0) {
            return null;
        }
        return new C6114agi(GrannyCheckedAllocationsEnd, false);
    }

    /* renamed from: c */
    public static boolean m4790c(C5501aLt alt) {
        return grannyJNI.GrannyEndAllocationCheck(C5501aLt.m16286h(alt));
    }

    /* renamed from: a */
    public static void m4435a(C3546sR sRVar, C0212Cd cd) {
        grannyJNI.GrannyGetAllocator(C3546sR.m38735a(sRVar), C0212Cd.m1814a(cd));
    }

    /* renamed from: a */
    public static void m4126a(aEG aeg, C0420Fr fr) {
        grannyJNI.GrannySetAllocator(aEG.m14050a(aeg), C0420Fr.m3291a(fr));
    }

    /* renamed from: d */
    public static void m4811d(C5501aLt alt) {
        grannyJNI.GrannyFreeBuilderResult(C5501aLt.m16286h(alt));
    }

    public static void GrannyAcquireMemorySpinlock() {
        grannyJNI.GrannyAcquireMemorySpinlock();
    }

    public static void GrannyReleaseMemorySpinlock() {
        grannyJNI.GrannyReleaseMemorySpinlock();
    }

    public static C3854vp aTT() {
        long GrannyNewMemoryArena = grannyJNI.GrannyNewMemoryArena();
        if (GrannyNewMemoryArena == 0) {
            return null;
        }
        return new C3854vp(GrannyNewMemoryArena, false);
    }

    /* renamed from: a */
    public static void m4438a(C3854vp vpVar, int i) {
        grannyJNI.GrannySetArenaAlignment(C3854vp.m40424a(vpVar), i);
    }

    /* renamed from: b */
    public static void m4704b(C3854vp vpVar) {
        grannyJNI.GrannyClearArena(C3854vp.m40424a(vpVar));
    }

    /* renamed from: c */
    public static void m4786c(C3854vp vpVar) {
        grannyJNI.GrannyFreeMemoryArena(C3854vp.m40424a(vpVar));
    }

    /* renamed from: b */
    public static C5501aLt m4582b(C3854vp vpVar, int i) {
        long GrannyMemoryArenaPush = grannyJNI.GrannyMemoryArenaPush(C3854vp.m40424a(vpVar), i);
        if (GrannyMemoryArenaPush == 0) {
            return null;
        }
        return new C5501aLt(GrannyMemoryArenaPush, false);
    }

    /* renamed from: a */
    public static C3505ri m3913a(String str, int i, int i2, int i3, Buffer buffer) {
        long GrannyCreateMemoryFileReader = grannyJNI.GrannyCreateMemoryFileReader(str, i, i2, i3, buffer);
        if (GrannyCreateMemoryFileReader == 0) {
            return null;
        }
        return new C3505ri(GrannyCreateMemoryFileReader, false);
    }

    /* renamed from: c */
    public static C5731aUp m4747c(String str, int i, int i2) {
        long GrannyCreateMemoryFileWriter = grannyJNI.GrannyCreateMemoryFileWriter(str, i, i2);
        if (GrannyCreateMemoryFileWriter == 0) {
            return null;
        }
        return new C5731aUp(GrannyCreateMemoryFileWriter, false);
    }

    /* renamed from: a */
    public static boolean m4494a(C5731aUp aup, aFO afo, C1845ab abVar) {
        return grannyJNI.GrannyStealMemoryWriterBuffer(C5731aUp.m18737b(aup), aFO.m14554a(afo), C1845ab.m19847a(abVar));
    }

    /* renamed from: g */
    public static void m4877g(C2382ef efVar) {
        grannyJNI.GrannyFreeMemoryWriterBuffer(C2382ef.m29966c(efVar));
    }

    /* renamed from: a */
    public static int m3685a(C1126QZ qz) {
        return grannyJNI.GrannyGetMeshMorphTargetCount(C1126QZ.m8909l(qz));
    }

    /* renamed from: b */
    public static int m4569b(C1126QZ qz) {
        return grannyJNI.GrannyGetMeshTriangleGroupCount(C1126QZ.m8909l(qz));
    }

    /* renamed from: c */
    public static C3152oP m4750c(C1126QZ qz) {
        long GrannyGetMeshTriangleGroups = grannyJNI.GrannyGetMeshTriangleGroups(C1126QZ.m8909l(qz));
        if (GrannyGetMeshTriangleGroups == 0) {
            return null;
        }
        return new C3152oP(GrannyGetMeshTriangleGroups, false);
    }


    /* renamed from: a */
    public static C1731Za m3771a(C1126QZ qz, int i) {
        long GrannyGetMeshMorphVertexType = grannyJNI.GrannyGetMeshMorphVertexType(C1126QZ.m8909l(qz), i);
        if (GrannyGetMeshMorphVertexType == 0) {
            return null;
        }
        return new C1731Za(GrannyGetMeshMorphVertexType, false);
    }


    /* renamed from: b */
    public static int m4570b(C1126QZ qz, int i) {
        return grannyJNI.GrannyGetMeshMorphVertexCount(C1126QZ.m8909l(qz), i);
    }

    /* renamed from: a */
    public static void m4053a(C1126QZ qz, int i, C1731Za za, Buffer buffer) {
        grannyJNI.GrannyCopyMeshMorphVertices(C1126QZ.m8909l(qz), i, C1731Za.m12101bi(za), buffer);
    }


    /* renamed from: c */
    public static C5501aLt m4746c(C1126QZ qz, int i) {
        long GrannyGetMeshMorphVertices = grannyJNI.GrannyGetMeshMorphVertices(C1126QZ.m8909l(qz), i);
        if (GrannyGetMeshMorphVertices == 0) {
            return null;
        }
        return new C5501aLt(GrannyGetMeshMorphVertices, false);
    }


    /* renamed from: i */
    public static int m4926i(C1126QZ qz) {
        return grannyJNI.GrannyGetMeshTriangleCount(C1126QZ.m8909l(qz));
    }

    /* renamed from: j */
    public static int m4987j(C1126QZ qz) {
        return grannyJNI.GrannyGetMeshBytesPerIndex(C1126QZ.m8909l(qz));
    }

    /* renamed from: k */
    public static C5501aLt m5049k(C1126QZ qz) {
        long GrannyGetMeshIndices = grannyJNI.GrannyGetMeshIndices(C1126QZ.m8909l(qz));
        if (GrannyGetMeshIndices == 0) {
            return null;
        }
        return new C5501aLt(GrannyGetMeshIndices, false);
    }


    /* renamed from: b */
    public static void m4698b(C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, C3159oW oWVar4) {
        grannyJNI.GrannyTransformBoundingBox(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), C3159oW.m36741q(oWVar4));
    }

    /* renamed from: ad */
    public static void m4545ad(C1731Za za) {
        grannyJNI.GrannyBoneBindingType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aTU() {
        long GrannyBoneBindingType_get = grannyJNI.GrannyBoneBindingType_get();
        if (GrannyBoneBindingType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyBoneBindingType_get, false);
    }

    /* renamed from: ae */
    public static void m4546ae(C1731Za za) {
        grannyJNI.GrannyMaterialBindingType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aTV() {
        long GrannyMaterialBindingType_get = grannyJNI.GrannyMaterialBindingType_get();
        if (GrannyMaterialBindingType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyMaterialBindingType_get, false);
    }

    /* renamed from: af */
    public static void m4547af(C1731Za za) {
        grannyJNI.GrannyMorphTargetType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aTW() {
        long GrannyMorphTargetType_get = grannyJNI.GrannyMorphTargetType_get();
        if (GrannyMorphTargetType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyMorphTargetType_get, false);
    }

    /* renamed from: ag */
    public static void m4548ag(C1731Za za) {
        grannyJNI.GrannyMeshType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aTX() {
        long GrannyMeshType_get = grannyJNI.GrannyMeshType_get();
        if (GrannyMeshType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyMeshType_get, false);
    }

    /* renamed from: a */
    public static void m4346a(C6796ato ato) {
        grannyJNI.GrannyFreeMeshBinding(C6796ato.m26060i(ato));
    }

    /* renamed from: b */
    public static int m4571b(C1126QZ qz, C0913NP np, C0913NP np2) {
        return grannyJNI.GrannyGetResultingMeshBindingSize(C1126QZ.m8909l(qz), C0913NP.m7619a(np), C0913NP.m7619a(np2));
    }

    /* renamed from: a */
    public static C6796ato m3880a(C1126QZ qz, C0913NP np, C0913NP np2, Buffer buffer) {
        long GrannyNewMeshBindingInPlace = grannyJNI.GrannyNewMeshBindingInPlace(C1126QZ.m8909l(qz), C0913NP.m7619a(np), C0913NP.m7619a(np2), buffer);
        if (GrannyNewMeshBindingInPlace == 0) {
            return null;
        }
        return new C6796ato(GrannyNewMeshBindingInPlace, false);
    }

    /* renamed from: a */
    public static int m3696a(C6796ato ato, int i) {
        return grannyJNI.GrannyGetMeshBinding4x4ArraySize(C6796ato.m26060i(ato), i);
    }

    /* renamed from: d */
    public static C1845ab m4802d(C6796ato ato) {
        long GrannyGetMeshBindingFromBoneIndices = grannyJNI.GrannyGetMeshBindingFromBoneIndices(C6796ato.m26060i(ato));
        if (GrannyGetMeshBindingFromBoneIndices == 0) {
            return null;
        }
        return new C1845ab(GrannyGetMeshBindingFromBoneIndices, false);
    }

    /* renamed from: e */
    public static C0913NP m4830e(C6796ato ato) {
        long GrannyGetMeshBindingFromSkeleton = grannyJNI.GrannyGetMeshBindingFromSkeleton(C6796ato.m26060i(ato));
        if (GrannyGetMeshBindingFromSkeleton == 0) {
            return null;
        }
        return new C0913NP(GrannyGetMeshBindingFromSkeleton, false);
    }


    /* renamed from: g */
    public static C0913NP m4872g(C6796ato ato) {
        long GrannyGetMeshBindingToSkeleton = grannyJNI.GrannyGetMeshBindingToSkeleton(C6796ato.m26060i(ato));
        if (GrannyGetMeshBindingToSkeleton == 0) {
            return null;
        }
        return new C0913NP(GrannyGetMeshBindingToSkeleton, false);
    }

    /* renamed from: h */
    public static C1126QZ m4888h(C6796ato ato) {
        long GrannyGetMeshBindingSourceMesh = grannyJNI.GrannyGetMeshBindingSourceMesh(C6796ato.m26060i(ato));
        if (GrannyGetMeshBindingSourceMesh == 0) {
            return null;
        }
        return new C1126QZ(GrannyGetMeshBindingSourceMesh, false);
    }

    /* renamed from: a */
    public static C5610aPy m3806a(int i, int i2, int i3, int i4, C1731Za za) {
        long GrannyBeginMesh = grannyJNI.GrannyBeginMesh(i, i2, i3, i4, C1731Za.m12101bi(za));
        if (GrannyBeginMesh == 0) {
            return null;
        }
        return new C5610aPy(GrannyBeginMesh, false);
    }

    /* renamed from: a */
    public static void m4191a(C5610aPy apy) {
        grannyJNI.GrannyGenerateTangentSpaceFromUVs(C5610aPy.m17402h(apy));
    }

    /* renamed from: a */
    public static boolean m4493a(C5610aPy apy, C0438GF gf, C6658arG arg) {
        return grannyJNI.GrannyEndMesh(C5610aPy.m17402h(apy), C0438GF.m3342b(gf), C6658arG.m25380c(arg));
    }

    /* renamed from: b */
    public static int m4575b(C5610aPy apy) {
        return grannyJNI.GrannyGetResultingVertexCount(C5610aPy.m17402h(apy));
    }

    /* renamed from: a */
    public static void m4201a(C5610aPy apy, C1845ab abVar) {
        grannyJNI.GrannySerializeResultingCoincidentVertexMap(C5610aPy.m17402h(apy), C1845ab.m19847a(abVar));
    }

    /* renamed from: b */
    public static void m4644b(C5610aPy apy, C1845ab abVar) {
        grannyJNI.GrannySerializeResultingVertexToTriangleMap(C5610aPy.m17402h(apy), C1845ab.m19847a(abVar));
    }

    /* renamed from: a */
    public static void m4199a(C5610aPy apy, C5501aLt alt) {
        grannyJNI.GrannySerializeResultingVertices(C5610aPy.m17402h(apy), C5501aLt.m16286h(alt));
    }

    /* renamed from: c */
    public static int m4738c(C5610aPy apy) {
        return grannyJNI.GrannyGetResultingVertexDataSize(C5610aPy.m17402h(apy));
    }

    /* renamed from: d */
    public static int m4796d(C5610aPy apy) {
        return grannyJNI.GrannyGetResultingTopologySize(C5610aPy.m17402h(apy));
    }

    /* renamed from: a */
    public static void m4200a(C5610aPy apy, C5501aLt alt, C0438GF gf, C5501aLt alt2, C6658arG arg) {
        grannyJNI.GrannyEndMeshInPlace(C5610aPy.m17402h(apy), C5501aLt.m16286h(alt), C0438GF.m3342b(gf), C5501aLt.m16286h(alt2), C6658arG.m25380c(arg));
    }

    /* renamed from: a */
    public static void m4192a(C5610aPy apy, float f) {
        grannyJNI.GrannySetNormalTolerance(C5610aPy.m17402h(apy), f);
    }

    /* renamed from: b */
    public static void m4640b(C5610aPy apy, float f) {
        grannyJNI.GrannySetTangentTolerance(C5610aPy.m17402h(apy), f);
    }

    /* renamed from: c */
    public static void m4763c(C5610aPy apy, float f) {
        grannyJNI.GrannySetBinormalTolerance(C5610aPy.m17402h(apy), f);
    }

    /* renamed from: d */
    public static void m4813d(C5610aPy apy, float f) {
        grannyJNI.GrannySetTangentBinormalCrossTolerance(C5610aPy.m17402h(apy), f);
    }

    /* renamed from: e */
    public static void m4837e(C5610aPy apy, float f) {
        grannyJNI.GrannySetTangentMergingTolerance(C5610aPy.m17402h(apy), f);
    }

    /* renamed from: a */
    public static void m4194a(C5610aPy apy, int i, float f) {
        grannyJNI.GrannySetChannelTolerance(C5610aPy.m17402h(apy), i, f);
    }

    /* renamed from: a */
    public static void m4198a(C5610aPy apy, int i, C6900avo avo) {
        grannyJNI.GrannySetVertexChannelComponentNames(C5610aPy.m17402h(apy), i, C6900avo.m26731d(avo));
    }

    /* renamed from: a */
    public static void m4193a(C5610aPy apy, float f, float f2, float f3) {
        grannyJNI.GrannySetPosition(C5610aPy.m17402h(apy), f, f2, f3);
    }

    /* renamed from: b */
    public static void m4642b(C5610aPy apy, int i, float f) {
        grannyJNI.GrannyAddWeight(C5610aPy.m17402h(apy), i, f);
    }

    /* renamed from: e */
    public static void m4836e(C5610aPy apy) {
        grannyJNI.GrannyPushVertex(C5610aPy.m17402h(apy));
    }

    /* renamed from: a */
    public static void m4196a(C5610aPy apy, int i, int i2) {
        grannyJNI.GrannySetVertexIndex(C5610aPy.m17402h(apy), i, i2);
    }

    /* renamed from: a */
    public static void m4195a(C5610aPy apy, int i, float f, float f2, float f3) {
        grannyJNI.GrannySetNormal(C5610aPy.m17402h(apy), i, f, f2, f3);
    }

    /* renamed from: b */
    public static void m4643b(C5610aPy apy, int i, float f, float f2, float f3) {
        grannyJNI.GrannySetTangent(C5610aPy.m17402h(apy), i, f, f2, f3);
    }

    /* renamed from: c */
    public static void m4764c(C5610aPy apy, int i, float f, float f2, float f3) {
        grannyJNI.GrannySetBinormal(C5610aPy.m17402h(apy), i, f, f2, f3);
    }

    /* renamed from: d */
    public static void m4814d(C5610aPy apy, int i, float f, float f2, float f3) {
        grannyJNI.GrannySetTangentBinormalCross(C5610aPy.m17402h(apy), i, f, f2, f3);
    }

    /* renamed from: a */
    public static int m3689a(C5610aPy apy, int i) {
        return grannyJNI.GrannyGetChannelComponentCount(C5610aPy.m17402h(apy), i);
    }

    /* renamed from: a */
    public static void m4197a(C5610aPy apy, int i, int i2, C3159oW oWVar) {
        grannyJNI.GrannySetChannel(C5610aPy.m17402h(apy), i, i2, C3159oW.m36741q(oWVar));
    }

    /* renamed from: b */
    public static void m4641b(C5610aPy apy, int i) {
        grannyJNI.GrannySetMaterial(C5610aPy.m17402h(apy), i);
    }

    /* renamed from: f */
    public static void m4860f(C5610aPy apy) {
        grannyJNI.GrannyPushTriangle(C5610aPy.m17402h(apy));
    }

    /* renamed from: c */
    public static int m4739c(C5610aPy apy, int i) {
        return grannyJNI.GrannyGetOriginalVertex(C5610aPy.m17402h(apy), i);
    }


    /* renamed from: a */
    public static void m4316a(C6604aqE aqe) {
        grannyJNI.GrannyFreeMeshDeformer(C6604aqE.m25068b(aqe));
    }


    /* renamed from: f */
    public static C3610su m4858f(long j, int i) {
        long GrannyOodle1BeginSimple = grannyJNI.GrannyOodle1BeginSimple(j, i);
        if (GrannyOodle1BeginSimple == 0) {
            return null;
        }
        return new C3610su(GrannyOodle1BeginSimple, false);
    }

    public static int GrannyGetOodle1CompressBufferPaddingSize() {
        return grannyJNI.GrannyGetOodle1CompressBufferPaddingSize();
    }

    /* renamed from: a */
    public static void m4436a(C3610su suVar, int i, C5501aLt alt) {
        grannyJNI.GrannyOodle1Compress(C3610su.m39206a(suVar), i, C5501aLt.m16286h(alt));
    }

    /* renamed from: a */
    public static int m3702a(C3610su suVar, C5269aCv acv, boolean z) {
        return grannyJNI.GrannyOodle1End(C3610su.m39206a(suVar), C5269aCv.m13385b(acv), z);
    }

    /* renamed from: b */
    public static void m4702b(C3610su suVar) {
        grannyJNI.GrannyOodle1FreeSimple(C3610su.m39206a(suVar));
    }

    public static int GrannyGetOodle1DecompressBufferPaddingSize() {
        return grannyJNI.GrannyGetOodle1DecompressBufferPaddingSize();
    }

    /* renamed from: a */
    public static void m4463a(boolean z, int i, C5501aLt alt, int i2, C5501aLt alt2) {
        grannyJNI.GrannyOodle1Decompress(z, i, C5501aLt.m16286h(alt), i2, C5501aLt.m16286h(alt2));
    }

    /* renamed from: b */
    public static void m4678b(C2720jA jAVar) {
        grannyJNI.GrannyZeroPeriodicLoop(C2720jA.m33633a(jAVar));
    }

    /* renamed from: a */
    public static void m4422a(C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, C3159oW oWVar4, float f, C2720jA jAVar) {
        grannyJNI.GrannyFitPeriodicLoop(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), C3159oW.m36741q(oWVar4), f, C2720jA.m33633a(jAVar));
    }

    /* renamed from: a */
    public static void m4395a(C2720jA jAVar, float f, C3159oW oWVar) {
        grannyJNI.GrannyComputePeriodicLoopVector(C2720jA.m33633a(jAVar), f, C3159oW.m36741q(oWVar));
    }

    /* renamed from: b */
    public static void m4679b(C2720jA jAVar, float f, C3159oW oWVar) {
        grannyJNI.GrannyComputePeriodicLoopLog(C2720jA.m33633a(jAVar), f, C3159oW.m36741q(oWVar));
    }

    /* renamed from: a */
    public static void m4396a(C2720jA jAVar, float f, C3159oW oWVar, C3159oW oWVar2) {
        grannyJNI.GrannyStepPeriodicLoop(C2720jA.m33633a(jAVar), f, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2));
    }

    /* renamed from: ah */
    public static void m4549ah(C1731Za za) {
        grannyJNI.GrannyPeriodicLoopType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aTY() {
        long GrannyPeriodicLoopType_get = grannyJNI.GrannyPeriodicLoopType_get();
        if (GrannyPeriodicLoopType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPeriodicLoopType_get, false);
    }

    public static C6127agv aTZ() {
        long GrannyNewPointerHash = grannyJNI.GrannyNewPointerHash();
        if (GrannyNewPointerHash == 0) {
            return null;
        }
        return new C6127agv(GrannyNewPointerHash, false);
    }

    /* renamed from: a */
    public static void m4268a(C6127agv agv) {
        grannyJNI.GrannyDeletePointerHash(C6127agv.m22157b(agv));
    }

    /* renamed from: a */
    public static void m4269a(C6127agv agv, C5501aLt alt, C5501aLt alt2) {
        grannyJNI.GrannyAddPointerToHash(C6127agv.m22157b(agv), C5501aLt.m16286h(alt), C5501aLt.m16286h(alt2));
    }

    /* renamed from: b */
    public static boolean m4721b(C6127agv agv, C5501aLt alt, C5501aLt alt2) {
        return grannyJNI.GrannySetHashedPointerData(C6127agv.m22157b(agv), C5501aLt.m16286h(alt), C5501aLt.m16286h(alt2));
    }

    /* renamed from: a */
    public static boolean m4498a(C6127agv agv, C5501aLt alt, C5269aCv acv) {
        return grannyJNI.GrannyGetHashedPointerData(C6127agv.m22157b(agv), C5501aLt.m16286h(alt), C5269aCv.m13385b(acv));
    }

    /* renamed from: a */
    public static boolean m4497a(C6127agv agv, C5501aLt alt) {
        return grannyJNI.GrannyHashedPointerKeyExists(C6127agv.m22157b(agv), C5501aLt.m16286h(alt));
    }

    /* renamed from: a */
    public static C0994Oe m3742a(C6646aqu aqu) {
        long GrannyGetS3TCPixelLayout = grannyJNI.GrannyGetS3TCPixelLayout(aqu.swigValue());
        if (GrannyGetS3TCPixelLayout == 0) {
            return null;
        }
        return new C0994Oe(GrannyGetS3TCPixelLayout, false);
    }

    public static String GrannyGetS3TCTextureFormatName(int i) {
        return grannyJNI.GrannyGetS3TCTextureFormatName(i);
    }

    /* renamed from: a */
    public static void m3958a(int i, C5515aMh amh, int i2, C1845ab abVar, int i3, C5515aMh amh2) {
        grannyJNI.GrannyBuildSkeletonRelativeTransform(i, C5515aMh.m16438f(amh), i2, C1845ab.m19847a(abVar), i3, C5515aMh.m16438f(amh2));
    }

    /* renamed from: a */
    public static void m3957a(int i, C5515aMh amh, int i2, C1845ab abVar, int i3, int i4, C5515aMh amh2) {
        grannyJNI.GrannyBuildSkeletonRelativeTransforms(i, C5515aMh.m16438f(amh), i2, C1845ab.m19847a(abVar), i3, i4, C5515aMh.m16438f(amh2));
    }

    /* renamed from: a */
    public static boolean m4476a(C0913NP np, String str, Buffer buffer) {
        return grannyJNI.GrannyFindBoneByName(C0913NP.m7619a(np), str, buffer);
    }


    /* renamed from: a */
    public static int m3681a(C0913NP np, float f) {
        return grannyJNI.GrannyGetBoneCountForLOD(C0913NP.m7619a(np), f);
    }

    /* renamed from: ai */
    public static void m4550ai(C1731Za za) {
        grannyJNI.GrannyBoneType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUa() {
        long GrannyBoneType_get = grannyJNI.GrannyBoneType_get();
        if (GrannyBoneType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyBoneType_get, false);
    }

    /* renamed from: aj */
    public static void m4551aj(C1731Za za) {
        grannyJNI.GrannySkeletonType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUb() {
        long GrannySkeletonType_get = grannyJNI.GrannySkeletonType_get();
        if (GrannySkeletonType_get == 0) {
            return null;
        }
        return new C1731Za(GrannySkeletonType_get, false);
    }

    /* renamed from: hv */
    public static C1641YA m4921hv(int i) {
        long GrannyBeginSkeleton = grannyJNI.GrannyBeginSkeleton(i);
        if (GrannyBeginSkeleton == 0) {
            return null;
        }
        return new C1641YA(GrannyBeginSkeleton, false);
    }

    /* renamed from: a */
    public static C0913NP m3734a(C1641YA ya, C1845ab abVar) {
        long GrannyEndSkeleton = grannyJNI.GrannyEndSkeleton(C1641YA.m11784b(ya), C1845ab.m19847a(abVar));
        if (GrannyEndSkeleton == 0) {
            return null;
        }
        return new C0913NP(GrannyEndSkeleton, false);
    }

    /* renamed from: a */
    public static int m3686a(C1641YA ya) {
        return grannyJNI.GrannyGetResultingSkeletonSize(C1641YA.m11784b(ya));
    }

    /* renamed from: a */
    public static C0913NP m3733a(C1641YA ya, C5501aLt alt, C1845ab abVar) {
        long GrannyEndSkeletonInPlace = grannyJNI.GrannyEndSkeletonInPlace(C1641YA.m11784b(ya), C5501aLt.m16286h(alt), C1845ab.m19847a(abVar));
        if (GrannyEndSkeletonInPlace == 0) {
            return null;
        }
        return new C0913NP(GrannyEndSkeletonInPlace, false);
    }

    /* renamed from: a */
    public static void m4109a(C1641YA ya, C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, C3159oW oWVar4, C3159oW oWVar5, C3159oW oWVar6) {
        grannyJNI.GrannyAddBone(C1641YA.m11784b(ya), C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), C3159oW.m36741q(oWVar4), C3159oW.m36741q(oWVar5), C3159oW.m36741q(oWVar6));
    }

    /* renamed from: a */
    public static void m4108a(C1641YA ya, int i, int i2) {
        grannyJNI.GrannySetBoneParent(C1641YA.m11784b(ya), i, i2);
    }

    /* renamed from: a */
    public static void m4107a(C1641YA ya, int i, float f) {
        grannyJNI.GrannySetBoneLODError(C1641YA.m11784b(ya), i, f);
    }

    /* renamed from: a */
    public static void m3967a(C0028AN an, int i, int i2) {
        grannyJNI.GrannyStackInitialize(C0028AN.m373a(an), i, i2);
    }

    /* renamed from: a */
    public static void m3968a(C0028AN an, int i, int i2, int i3) {
        grannyJNI.GrannyStackInitializeWithDirectory(C0028AN.m373a(an), i, i2, i3);
    }

    /* renamed from: b */
    public static void m4603b(C0028AN an) {
        grannyJNI.GrannyStackCleanUp(C0028AN.m373a(an));
    }

    /* renamed from: c */
    public static int m4736c(C0028AN an) {
        return grannyJNI.GrannyGetStackUnitCount(C0028AN.m373a(an));
    }

    /* renamed from: a */
    public static C5501aLt m3788a(C0028AN an, C1845ab abVar) {
        long GrannyNewStackUnit = grannyJNI.GrannyNewStackUnit(C0028AN.m373a(an), C1845ab.m19847a(abVar));
        if (GrannyNewStackUnit == 0) {
            return null;
        }
        return new C5501aLt(GrannyNewStackUnit, false);
    }

    /* renamed from: a */
    public static C5501aLt m3787a(C0028AN an, int i) {
        long GrannyGetStackUnit = grannyJNI.GrannyGetStackUnit(C0028AN.m373a(an), i);
        if (GrannyGetStackUnit == 0) {
            return null;
        }
        return new C5501aLt(GrannyGetStackUnit, false);
    }

    /* renamed from: b */
    public static void m4604b(C0028AN an, int i) {
        grannyJNI.GrannyPopStackUnits(C0028AN.m373a(an), i);
    }

    /* renamed from: a */
    public static boolean m4469a(C0028AN an, int i, C1845ab abVar, C5501aLt alt) {
        return grannyJNI.GrannyMultipleNewStackUnits(C0028AN.m373a(an), i, C1845ab.m19847a(abVar), C5501aLt.m16286h(alt));
    }

    /* renamed from: a */
    public static void m3969a(C0028AN an, C5501aLt alt) {
        grannyJNI.GrannySerializeStack(C0028AN.m373a(an), C5501aLt.m16286h(alt));
    }

    public static int GrannyGetCounterCount() {
        return grannyJNI.GrannyGetCounterCount();
    }

    public static double GrannyGetCounterTicksPerSecond() {
        return grannyJNI.GrannyGetCounterTicksPerSecond();
    }

    public static void GrannyResetCounters() {
        grannyJNI.GrannyResetCounters();
    }

    public static void GrannyResetCounterPeaks() {
        grannyJNI.GrannyResetCounterPeaks();
    }

    /* renamed from: a */
    public static void m3959a(int i, C6506aoK aok) {
        grannyJNI.GrannyGetCounterResults(i, C6506aoK.m24404a(aok));
    }

    /* renamed from: hw */
    public static C1157Qy m4922hw(int i) {
        long GrannyCaptureCurrentStats = grannyJNI.GrannyCaptureCurrentStats(i);
        if (GrannyCaptureCurrentStats == 0) {
            return null;
        }
        return new C1157Qy(GrannyCaptureCurrentStats, false);
    }

    /* renamed from: a */
    public static void m4057a(C1157Qy qy) {
        grannyJNI.GrannyFreeStats(C1157Qy.m9055c(qy));
    }

    /* renamed from: b */
    public static C6900avo m4593b(C1157Qy qy) {
        long GrannyDumpStatHUD = grannyJNI.GrannyDumpStatHUD(C1157Qy.m9055c(qy));
        if (GrannyDumpStatHUD == 0) {
            return null;
        }
        return new C6900avo(GrannyDumpStatHUD, false);
    }

    /* renamed from: b */
    public static void m4665b(C6900avo avo) {
        grannyJNI.GrannyFreeStatHUDDump(C6900avo.m26731d(avo));
    }

    public static int GrannyStringDifference(String str, String str2) {
        return grannyJNI.GrannyStringDifference(str, str2);
    }

    /* renamed from: b */
    public static void m4609b(C0256DJ dj) {
        grannyJNI.GrannySetStringComparisonCallback(C0256DJ.m1973a(dj));
    }

    /* renamed from: f */
    public static aPJ m4854f(C6396amE ame) {
        long GrannyGetStringDatabase = grannyJNI.GrannyGetStringDatabase(C6396amE.m23854g(ame));
        if (GrannyGetStringDatabase == 0) {
            return null;
        }
        return new aPJ(GrannyGetStringDatabase, false);
    }

    /* renamed from: a */
    public static boolean m4499a(C6396amE ame, aPJ apj) {
        return grannyJNI.GrannyRemapFileStrings(C6396amE.m23854g(ame), aPJ.m17153a(apj));
    }

    /* renamed from: a */
    public static String m3938a(C5501aLt alt, long j) {
        return grannyJNI.GrannyRebaseToStringDatabase(C5501aLt.m16286h(alt), j);
    }

    /* renamed from: ak */
    public static void m4552ak(C1731Za za) {
        grannyJNI.GrannyStringDatabaseType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUc() {
        long GrannyStringDatabaseType_get = grannyJNI.GrannyStringDatabaseType_get();
        if (GrannyStringDatabaseType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyStringDatabaseType_get, false);
    }

    public static C6795atn aUd() {
        long GrannyNewStringTable = grannyJNI.GrannyNewStringTable();
        if (GrannyNewStringTable == 0) {
            return null;
        }
        return new C6795atn(GrannyNewStringTable, false);
    }

    /* renamed from: a */
    public static void m4345a(C6795atn atn) {
        grannyJNI.GrannyFreeStringTable(C6795atn.m26059c(atn));
    }

    /* renamed from: a */
    public static String m3940a(C6795atn atn, String str) {
        return grannyJNI.GrannyMapString(C6795atn.m26059c(atn), str);
    }


    /* renamed from: a */
    public static float m3672a(C0938Nm nm, C0938Nm nm2) {
        return grannyJNI.GrannyGetSecondsElapsed(C0938Nm.m7718b(nm), C0938Nm.m7718b(nm2));
    }

    public static void GrannySleepForSeconds(float f) {
        grannyJNI.GrannySleepForSeconds(f);
    }

    /* renamed from: g */
    public static boolean m4881g(C5610aPy apy) {
        return grannyJNI.GrannyBuildTangentSpace(C5610aPy.m17402h(apy));
    }

    /* renamed from: al */
    public static void m4553al(C1731Za za) {
        grannyJNI.GrannyGBX333VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUe() {
        long GrannyGBX333VertexType_get = grannyJNI.GrannyGBX333VertexType_get();
        if (GrannyGBX333VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyGBX333VertexType_get, false);
    }

    public static String GrannyGetTextureTypeName(int i) {
        return grannyJNI.GrannyGetTextureTypeName(i);
    }

    public static String GrannyGetTextureEncodingName(int i) {
        return grannyJNI.GrannyGetTextureEncodingName(i);
    }

    /* renamed from: a */
    public static int m3684a(C0994Oe oe, int i, int i2, int i3) {
        return grannyJNI.GrannyGetRawImageSize(C0994Oe.m8106p(oe), i, i2, i3);
    }

    /* renamed from: a */
    public static int m3695a(C6646aqu aqu, int i, int i2) {
        return grannyJNI.GrannyGetS3TCImageSize(aqu.swigValue(), i, i2);
    }

    /* renamed from: a */
    public static boolean m4492a(C5350aFy afy, C0994Oe oe) {
        return grannyJNI.GrannyGetRecommendedPixelLayout(C5350aFy.m14701c(afy), C0994Oe.m8106p(oe));
    }

    /* renamed from: a */
    public static boolean m4491a(C5350aFy afy, int i, int i2, C0994Oe oe, int i3, int i4, int i5, C5501aLt alt) {
        return grannyJNI.GrannyCopyTextureImage(C5350aFy.m14701c(afy), i, i2, C0994Oe.m8106p(oe), i3, i4, i5, C5501aLt.m16286h(alt));
    }

    /* renamed from: b */
    public static boolean m4720b(C5350aFy afy) {
        return grannyJNI.GrannyTextureHasAlpha(C5350aFy.m14701c(afy));
    }

    /* renamed from: am */
    public static void m4554am(C1731Za za) {
        grannyJNI.GrannyTextureMIPLevelType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUf() {
        long GrannyTextureMIPLevelType_get = grannyJNI.GrannyTextureMIPLevelType_get();
        if (GrannyTextureMIPLevelType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyTextureMIPLevelType_get, false);
    }

    /* renamed from: an */
    public static void m4555an(C1731Za za) {
        grannyJNI.GrannyTextureImageType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUg() {
        long GrannyTextureImageType_get = grannyJNI.GrannyTextureImageType_get();
        if (GrannyTextureImageType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyTextureImageType_get, false);
    }

    /* renamed from: ao */
    public static void m4556ao(C1731Za za) {
        grannyJNI.GrannyTextureType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUh() {
        long GrannyTextureType_get = grannyJNI.GrannyTextureType_get();
        if (GrannyTextureType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyTextureType_get, false);
    }

    /* renamed from: a */
    public static C1406Uc m3761a(int i, int i2, C0994Oe oe, int i3) {
        long GrannyBeginRawTexture = grannyJNI.GrannyBeginRawTexture(i, i2, C0994Oe.m8106p(oe), i3);
        if (GrannyBeginRawTexture == 0) {
            return null;
        }
        return new C1406Uc(GrannyBeginRawTexture, false);
    }

    /* renamed from: a */
    public static C1406Uc m3762a(int i, int i2, C6646aqu aqu) {
        long GrannyBeginS3TCTexture = grannyJNI.GrannyBeginS3TCTexture(i, i2, aqu.swigValue());
        if (GrannyBeginS3TCTexture == 0) {
            return null;
        }
        return new C1406Uc(GrannyBeginS3TCTexture, false);
    }

    /* renamed from: E */
    public static C1406Uc m3645E(int i, int i2) {
        long GrannyBeginBestMatchS3TCTexture = grannyJNI.GrannyBeginBestMatchS3TCTexture(i, i2);
        if (GrannyBeginBestMatchS3TCTexture == 0) {
            return null;
        }
        return new C1406Uc(GrannyBeginBestMatchS3TCTexture, false);
    }

    /* renamed from: a */
    public static C1406Uc m3760a(int i, int i2, int i3, long j) {
        long GrannyBeginBinkTexture = grannyJNI.GrannyBeginBinkTexture(i, i2, i3, j);
        if (GrannyBeginBinkTexture == 0) {
            return null;
        }
        return new C1406Uc(GrannyBeginBinkTexture, false);
    }

    /* renamed from: a */
    public static C5350aFy m3780a(C1406Uc uc) {
        long GrannyEndTexture = grannyJNI.GrannyEndTexture(C1406Uc.m10294c(uc));
        if (GrannyEndTexture == 0) {
            return null;
        }
        return new C5350aFy(GrannyEndTexture, false);
    }

    /* renamed from: b */
    public static int m4572b(C1406Uc uc) {
        return grannyJNI.GrannyGetResultingTextureSize(C1406Uc.m10294c(uc));
    }

    /* renamed from: a */
    public static C5350aFy m3781a(C1406Uc uc, Buffer buffer) {
        long GrannyEndTextureInPlace = grannyJNI.GrannyEndTextureInPlace(C1406Uc.m10294c(uc), buffer);
        if (GrannyEndTextureInPlace == 0) {
            return null;
        }
        return new C5350aFy(GrannyEndTextureInPlace, false);
    }

    /* renamed from: a */
    public static void m4090a(C1406Uc uc, C0430Fy fy, C0430Fy fy2) {
        grannyJNI.GrannySetImageScalingFilter(C1406Uc.m10294c(uc), fy.swigValue(), fy2.swigValue());
    }

    /* renamed from: a */
    public static void m4089a(C1406Uc uc, int i, int i2, int i3, int i4, C5501aLt alt) {
        grannyJNI.GrannyEncodeImage(C1406Uc.m10294c(uc), i, i2, i3, i4, C5501aLt.m16286h(alt));
    }

    public static void GrannySetAllowGlobalStateChanges(boolean z) {
        grannyJNI.GrannySetAllowGlobalStateChanges(z);
    }

    public static boolean GrannyGetAllowGlobalStateChanges() {
        return grannyJNI.GrannyGetAllowGlobalStateChanges();
    }

    /* renamed from: a */
    public static C1354Tj m3759a(C1354Tj tj) {
        long GrannySetThreadIDCallback = grannyJNI.GrannySetThreadIDCallback(C1354Tj.m10098b(tj));
        if (GrannySetThreadIDCallback == 0) {
            return null;
        }
        return new C1354Tj(GrannySetThreadIDCallback, false);
    }

    public static boolean GrannyThreadAllowedToCallGranny() {
        return grannyJNI.GrannyThreadAllowedToCallGranny();
    }

    /* renamed from: a */
    public static void m4041a(C0970OH oh, C3159oW oWVar) {
        grannyJNI.GrannyGetTrackGroupInitialPlacement4x4(C0970OH.m7822e(oh), C3159oW.m36741q(oWVar));
    }

    /* renamed from: b */
    public static void m4637b(C5515aMh amh, int i, C3159oW oWVar) {
        grannyJNI.GrannyTransformCurve3(C5515aMh.m16438f(amh), i, C3159oW.m36741q(oWVar));
    }

    /* renamed from: c */
    public static void m4760c(C5515aMh amh, int i, C3159oW oWVar) {
        grannyJNI.GrannyTransformCurve4(C5515aMh.m16438f(amh), i, C3159oW.m36741q(oWVar));
    }

    /* renamed from: d */
    public static void m4812d(C5515aMh amh, int i, C3159oW oWVar) {
        grannyJNI.GrannyTransformCurve3x3(C5515aMh.m16438f(amh), i, C3159oW.m36741q(oWVar));
    }

    /* renamed from: a */
    public static void m4006a(C0777LF lf, C5515aMh amh) {
        grannyJNI.GrannyGetTrackInitialTransform(C0777LF.m6656f(lf), C5515aMh.m16438f(amh));
    }

    /* renamed from: b */
    public static void m4617b(C0777LF lf) {
        grannyJNI.GrannyRemoveTrackInitialTransform(C0777LF.m6656f(lf));
    }

    /* renamed from: a */
    public static boolean m4509a(C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, float f, float f2, boolean z) {
        return grannyJNI.GrannyBasisConversionRequiresCurveDecompression(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), f, f2, z);
    }

    /* renamed from: a */
    public static void m4416a(C3159oW oWVar, C3159oW oWVar2, float f, float f2, C3914wi wiVar) {
        grannyJNI.GrannyTransformCurveVec3(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), f, f2, C3914wi.m40719a(wiVar));
    }

    /* renamed from: a */
    public static void m4420a(C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, float f, float f2, C3914wi wiVar) {
        grannyJNI.GrannySimilarityTransformCurvePosition(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), f, f2, C3914wi.m40719a(wiVar));
    }

    /* renamed from: b */
    public static void m4697b(C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, float f, float f2, C3914wi wiVar) {
        grannyJNI.GrannySimilarityTransformCurveQuaternion(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), f, f2, C3914wi.m40719a(wiVar));
    }

    /* renamed from: c */
    public static void m4782c(C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, float f, float f2, C3914wi wiVar) {
        grannyJNI.GrannySimilarityTransformCurveScaleShear(C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), f, f2, C3914wi.m40719a(wiVar));
    }

    /* renamed from: a */
    public static void m4042a(C0970OH oh, C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, float f, float f2) {
        grannyJNI.GrannySimilarityTransformTrackGroup(C0970OH.m7822e(oh), C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), f, f2);
    }

    /* renamed from: a */
    public static void m3952a(int i, int i2, C3159oW oWVar, C3159oW oWVar2, C6022aeu aeu, C3159oW oWVar3, C3159oW oWVar4) {
        grannyJNI.GrannyGetVectorDifferences(i, i2, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), aeu.swigValue(), C3159oW.m36741q(oWVar3), C3159oW.m36741q(oWVar4));
    }

    /* renamed from: a */
    public static boolean m4464a(int i, int i2, int i3, C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, float f, C6022aeu aeu, C1845ab abVar, C1845ab abVar2) {
        return grannyJNI.GrannyKnotsAreReducible(i, i2, i3, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), f, aeu.swigValue(), C1845ab.m19847a(abVar), C1845ab.m19847a(abVar2));
    }

    /* renamed from: a */
    public static boolean m4512a(C3914wi wiVar, C3159oW oWVar, float f, C6022aeu aeu, C1845ab abVar, C1845ab abVar2) {
        return grannyJNI.GrannyCurveIsReducible(C3914wi.m40719a(wiVar), C3159oW.m36741q(oWVar), f, aeu.swigValue(), C1845ab.m19847a(abVar), C1845ab.m19847a(abVar2));
    }

    /* renamed from: c */
    public static boolean m4789c(C0777LF lf) {
        return grannyJNI.GrannyTransformTrackHasKeyframedCurves(C0777LF.m6656f(lf));
    }

    /* renamed from: d */
    public static boolean m4824d(C0777LF lf) {
        return grannyJNI.GrannyTransformTrackIsAnimated(C0777LF.m6656f(lf));
    }

    /* renamed from: e */
    public static boolean m4847e(C0777LF lf) {
        return grannyJNI.GrannyTransformTrackIsIdentity(C0777LF.m6656f(lf));
    }

    /* renamed from: a */
    public static boolean m4477a(C0970OH oh, String str, C1845ab abVar) {
        return grannyJNI.GrannyFindTrackByName(C0970OH.m7822e(oh), str, C1845ab.m19847a(abVar));
    }

    /* renamed from: a */
    public static boolean m4478a(C0970OH oh, String str, String str2, C1845ab abVar) {
        return grannyJNI.GrannyFindTrackByRule(C0970OH.m7822e(oh), str, str2, C1845ab.m19847a(abVar));
    }

    /* renamed from: b */
    public static boolean m4716b(C0970OH oh, String str, C1845ab abVar) {
        return grannyJNI.GrannyFindVectorTrackByName(C0970OH.m7822e(oh), str, C1845ab.m19847a(abVar));
    }

    /* renamed from: b */
    public static boolean m4717b(C0970OH oh, String str, String str2, C1845ab abVar) {
        return grannyJNI.GrannyFindVectorTrackByRule(C0970OH.m7822e(oh), str, str2, C1845ab.m19847a(abVar));
    }

    /* renamed from: a */
    public static void m4040a(C0970OH oh, C6135ahD ahd, C6135ahD ahd2) {
        grannyJNI.GrannyGetTrackGroupFlags(C0970OH.m7822e(oh), C6135ahD.m22189i(ahd), C6135ahD.m22189i(ahd2));
    }

    /* renamed from: a */
    public static void m4039a(C0970OH oh, long j, long j2) {
        grannyJNI.GrannySetTrackGroupFlags(C0970OH.m7822e(oh), j, j2);
    }

    /* renamed from: a */
    public static long m3707a(C0913NP np, int i, String str) {
        return grannyJNI.GrannyVectorTrackKeyForBone(C0913NP.m7619a(np), i, str);
    }

    /* renamed from: ap */
    public static void m4557ap(C1731Za za) {
        grannyJNI.GrannyVectorTrackType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUi() {
        long GrannyVectorTrackType_get = grannyJNI.GrannyVectorTrackType_get();
        if (GrannyVectorTrackType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyVectorTrackType_get, false);
    }

    /* renamed from: aq */
    public static void m4558aq(C1731Za za) {
        grannyJNI.GrannyTransformTrackType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUj() {
        long GrannyTransformTrackType_get = grannyJNI.GrannyTransformTrackType_get();
        if (GrannyTransformTrackType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyTransformTrackType_get, false);
    }

    /* renamed from: ar */
    public static void m4559ar(C1731Za za) {
        grannyJNI.GrannyTextTrackEntryType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUk() {
        long GrannyTextTrackEntryType_get = grannyJNI.GrannyTextTrackEntryType_get();
        if (GrannyTextTrackEntryType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyTextTrackEntryType_get, false);
    }

    /* renamed from: as */
    public static void m4560as(C1731Za za) {
        grannyJNI.GrannyTextTrackType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUl() {
        long GrannyTextTrackType_get = grannyJNI.GrannyTextTrackType_get();
        if (GrannyTextTrackType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyTextTrackType_get, false);
    }

    /* renamed from: at */
    public static void m4561at(C1731Za za) {
        grannyJNI.GrannyTrackGroupType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUm() {
        long GrannyTrackGroupType_get = grannyJNI.GrannyTrackGroupType_get();
        if (GrannyTrackGroupType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyTrackGroupType_get, false);
    }

    /* renamed from: a */
    public static C3895wQ m3922a(String str, int i, int i2, int i3, boolean z) {
        long GrannyBeginTrackGroup = grannyJNI.GrannyBeginTrackGroup(str, i, i2, i3, z);
        if (GrannyBeginTrackGroup == 0) {
            return null;
        }
        return new C3895wQ(GrannyBeginTrackGroup, false);
    }

    /* renamed from: b */
    public static C0970OH m4580b(C3895wQ wQVar) {
        long GrannyEndTrackGroup = grannyJNI.GrannyEndTrackGroup(C3895wQ.m40636a(wQVar));
        if (GrannyEndTrackGroup == 0) {
            return null;
        }
        return new C0970OH(GrannyEndTrackGroup, false);
    }

    /* renamed from: c */
    public static int m4742c(C3895wQ wQVar) {
        return grannyJNI.GrannyGetResultingTrackGroupSize(C3895wQ.m40636a(wQVar));
    }

    /* renamed from: a */
    public static C0970OH m3738a(C3895wQ wQVar, Buffer buffer) {
        long GrannyEndTrackGroupInPlace = grannyJNI.GrannyEndTrackGroupInPlace(C3895wQ.m40636a(wQVar), buffer);
        if (GrannyEndTrackGroupInPlace == 0) {
            return null;
        }
        return new C0970OH(GrannyEndTrackGroupInPlace, false);
    }

    /* renamed from: a */
    public static void m4446a(C3895wQ wQVar, String str, long j, C3914wi wiVar) {
        grannyJNI.GrannyPushVectorTrackCurve(C3895wQ.m40636a(wQVar), str, j, C3914wi.m40719a(wiVar));
    }

    /* renamed from: a */
    public static void m4445a(C3895wQ wQVar, String str, int i) {
        grannyJNI.GrannyBeginTransformTrack(C3895wQ.m40636a(wQVar), str, i);
    }

    /* renamed from: a */
    public static void m4443a(C3895wQ wQVar, C3914wi wiVar) {
        grannyJNI.GrannySetTransformTrackPositionCurve(C3895wQ.m40636a(wQVar), C3914wi.m40719a(wiVar));
    }

    /* renamed from: b */
    public static void m4709b(C3895wQ wQVar, C3914wi wiVar) {
        grannyJNI.GrannySetTransformTrackOrientationCurve(C3895wQ.m40636a(wQVar), C3914wi.m40719a(wiVar));
    }

    /* renamed from: c */
    public static void m4787c(C3895wQ wQVar, C3914wi wiVar) {
        grannyJNI.GrannySetTransformTrackScaleShearCurve(C3895wQ.m40636a(wQVar), C3914wi.m40719a(wiVar));
    }

    /* renamed from: d */
    public static void m4823d(C3895wQ wQVar) {
        grannyJNI.GrannyEndTransformTrack(C3895wQ.m40636a(wQVar));
    }

    /* renamed from: a */
    public static void m4444a(C3895wQ wQVar, String str) {
        grannyJNI.GrannyBeginTextTrack(C3895wQ.m40636a(wQVar), str);
    }

    /* renamed from: a */
    public static void m4442a(C3895wQ wQVar, float f, String str) {
        grannyJNI.GrannyAddTextEntry(C3895wQ.m40636a(wQVar), f, str);
    }

    /* renamed from: e */
    public static void m4846e(C3895wQ wQVar) {
        grannyJNI.GrannyEndTextTrack(C3895wQ.m40636a(wQVar));
    }

    /* renamed from: a */
    public static void m4037a(C0970OH oh) {
        grannyJNI.GrannyResortTrackGroup(C0970OH.m7822e(oh));
    }

    /* renamed from: b */
    public static void m4623b(C0970OH oh) {
        grannyJNI.GrannyAllocateLODErrorSpace(C0970OH.m7822e(oh));
    }

    /* renamed from: c */
    public static void m4754c(C0970OH oh) {
        grannyJNI.GrannyFreeLODErrorSpace(C0970OH.m7822e(oh));
    }

    /* renamed from: a */
    public static void m4038a(C0970OH oh, float f) {
        grannyJNI.GrannySetAllLODErrorSpace(C0970OH.m7822e(oh), f);
    }

    /* renamed from: d */
    public static void m4808d(C0970OH oh) {
        grannyJNI.GrannyResetLODErrorSpace(C0970OH.m7822e(oh));
    }

    /* renamed from: F */
    public static C5469aKn m3648F(int i, int i2) {
        long GrannyBeginSampledAnimation = grannyJNI.GrannyBeginSampledAnimation(i, i2);
        if (GrannyBeginSampledAnimation == 0) {
            return null;
        }
        return new C5469aKn(GrannyBeginSampledAnimation, false);
    }

    /* renamed from: a */
    public static void m4153a(C5469aKn akn) {
        grannyJNI.GrannyEndSampledAnimation(C5469aKn.m16060c(akn));
    }

    /* renamed from: G */
    public static C5469aKn m3651G(int i, int i2) {
        long GrannyBeginSampledAnimationNonBlocked = grannyJNI.GrannyBeginSampledAnimationNonBlocked(i, i2);
        if (GrannyBeginSampledAnimationNonBlocked == 0) {
            return null;
        }
        return new C5469aKn(GrannyBeginSampledAnimationNonBlocked, false);
    }

    /* renamed from: a */
    public static C3159oW m3905a(C5469aKn akn, int i) {
        long GrannyGetPositionSamples = grannyJNI.GrannyGetPositionSamples(C5469aKn.m16060c(akn), i);
        if (GrannyGetPositionSamples == 0) {
            return null;
        }
        return new C3159oW(GrannyGetPositionSamples, false);
    }

    /* renamed from: b */
    public static C3159oW m4594b(C5469aKn akn, int i) {
        long GrannyGetOrientationSamples = grannyJNI.GrannyGetOrientationSamples(C5469aKn.m16060c(akn), i);
        if (GrannyGetOrientationSamples == 0) {
            return null;
        }
        return new C3159oW(GrannyGetOrientationSamples, false);
    }

    /* renamed from: c */
    public static C3159oW m4751c(C5469aKn akn, int i) {
        long GrannyGetScaleShearSamples = grannyJNI.GrannyGetScaleShearSamples(C5469aKn.m16060c(akn), i);
        if (GrannyGetScaleShearSamples == 0) {
            return null;
        }
        return new C3159oW(GrannyGetScaleShearSamples, false);
    }

    /* renamed from: a */
    public static void m4154a(C5469aKn akn, int i, C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3) {
        grannyJNI.GrannySetTransformSample(C5469aKn.m16060c(akn), i, C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3));
    }

    /* renamed from: b */
    public static void m4635b(C5469aKn akn) {
        grannyJNI.GrannyPushSampledFrame(C5469aKn.m16060c(akn));
    }

    /* renamed from: b */
    public static C5789aaV m4585b(float f, int i) {
        long GrannyNewTrackMask = grannyJNI.GrannyNewTrackMask(f, i);
        if (GrannyNewTrackMask == 0) {
            return null;
        }
        return new C5789aaV(GrannyNewTrackMask, false);
    }

    /* renamed from: a */
    public static C5288aDo m3777a(C5789aaV aav, int i, C0913NP np, String str, float f, boolean z) {
        return C5288aDo.m13844xz(grannyJNI.GrannyExtractTrackMask(C5789aaV.m19495f(aav), i, C0913NP.m7619a(np), str, f, z));
    }

    /* renamed from: a */
    public static float m3673a(C5789aaV aav, int i) {
        return grannyJNI.GrannyGetTrackMaskBoneWeight(C5789aaV.m19495f(aav), i);
    }

    /* renamed from: a */
    public static void m4230a(C5789aaV aav, int i, float f) {
        grannyJNI.GrannySetTrackMaskBoneWeight(C5789aaV.m19495f(aav), i, f);
    }

    /* renamed from: a */
    public static void m4229a(C5789aaV aav) {
        grannyJNI.GrannyFreeTrackMask(C5789aaV.m19495f(aav));
    }

    /* renamed from: b */
    public static C5789aaV m4586b(C5789aaV aav) {
        long GrannyCopyTrackMask = grannyJNI.GrannyCopyTrackMask(C5789aaV.m19495f(aav));
        if (GrannyCopyTrackMask == 0) {
            return null;
        }
        return new C5789aaV(GrannyCopyTrackMask, false);
    }

    /* renamed from: c */
    public static void m4765c(C5789aaV aav) {
        grannyJNI.GrannyInvertTrackMask(C5789aaV.m19495f(aav));
    }

    /* renamed from: a */
    public static void m4232a(C5789aaV aav, C0913NP np, C0970OH oh, float f, float f2, float f3) {
        grannyJNI.GrannySetSkeletonTrackMaskFromTrackGroup(C5789aaV.m19495f(aav), C0913NP.m7619a(np), C0970OH.m7822e(oh), f, f2, f3);
    }

    /* renamed from: a */
    public static void m4231a(C5789aaV aav, C0913NP np, int i, float f) {
        grannyJNI.GrannySetSkeletonTrackMaskChainUpwards(C5789aaV.m19495f(aav), C0913NP.m7619a(np), i, f);
    }

    /* renamed from: b */
    public static void m4645b(C5789aaV aav, C0913NP np, int i, float f) {
        grannyJNI.GrannySetSkeletonTrackMaskChainDownwards(C5789aaV.m19495f(aav), C0913NP.m7619a(np), i, f);
    }

    /* renamed from: a */
    public static int m3694a(C6590apq apq, String str) {
        return grannyJNI.GrannyFindMaskIndexForName(C6590apq.m25013a(apq), str);
    }

    /* renamed from: a */
    public static void m4313a(C6590apq apq, C5975adz adz, C5789aaV aav) {
        grannyJNI.GrannyBindTrackmaskToModel(C6590apq.m25013a(apq), C5975adz.m21108e(adz), C5789aaV.m19495f(aav));
    }

    /* renamed from: a */
    public static void m4312a(C6590apq apq, C0970OH oh, C5789aaV aav) {
        grannyJNI.GrannyBindTrackmaskToTrackGroup(C6590apq.m25013a(apq), C0970OH.m7822e(oh), C5789aaV.m19495f(aav));
    }

    /* renamed from: au */
    public static void m4562au(C1731Za za) {
        grannyJNI.GrannyTrackMaskType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUn() {
        long GrannyTrackMaskType_get = grannyJNI.GrannyTrackMaskType_get();
        if (GrannyTrackMaskType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyTrackMaskType_get, false);
    }

    /* renamed from: av */
    public static void m4563av(C1731Za za) {
        grannyJNI.GrannyUnboundTrackMaskType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUo() {
        long GrannyUnboundTrackMaskType_get = grannyJNI.GrannyUnboundTrackMaskType_get();
        if (GrannyUnboundTrackMaskType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyUnboundTrackMaskType_get, false);
    }

    /* renamed from: d */
    public static void m4815d(C5789aaV aav) {
        grannyJNI.GrannyIdentityTrackMask_set(C5789aaV.m19495f(aav));
    }

    public static C5789aaV aUp() {
        long GrannyIdentityTrackMask_get = grannyJNI.GrannyIdentityTrackMask_get();
        if (GrannyIdentityTrackMask_get == 0) {
            return null;
        }
        return new C5789aaV(GrannyIdentityTrackMask_get, false);
    }

    /* renamed from: e */
    public static void m4838e(C5789aaV aav) {
        grannyJNI.GrannyNullTrackMask_set(C5789aaV.m19495f(aav));
    }

    public static C5789aaV aUq() {
        long GrannyNullTrackMask_get = grannyJNI.GrannyNullTrackMask_get();
        if (GrannyNullTrackMask_get == 0) {
            return null;
        }
        return new C5789aaV(GrannyNullTrackMask_get, false);
    }

    /* renamed from: a */
    public static void m4043a(C0979OP op, C0777LF lf, C0476Gb gb, C5515aMh amh) {
        grannyJNI.GrannySampleTrackUUULocal(C0979OP.m8025a(op), C0777LF.m6656f(lf), C0476Gb.m3443a(gb), C5515aMh.m16438f(amh));
    }

    /* renamed from: a */
    public static void m4044a(C0979OP op, C0777LF lf, C0476Gb gb, C3159oW oWVar, C3159oW oWVar2) {
        grannyJNI.GrannySampleTrackPOLocal(C0979OP.m8025a(op), C0777LF.m6656f(lf), C0476Gb.m3443a(gb), C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2));
    }

    public static C5316aEq aUr() {
        long GrannyGetTrackSamplerUUU = grannyJNI.GrannyGetTrackSamplerUUU();
        if (GrannyGetTrackSamplerUUU == 0) {
            return null;
        }
        return new C5316aEq(GrannyGetTrackSamplerUUU, false);
    }

    public static C5316aEq aUs() {
        long GrannyGetTrackSamplerSSS = grannyJNI.GrannyGetTrackSamplerSSS();
        if (GrannyGetTrackSamplerSSS == 0) {
            return null;
        }
        return new C5316aEq(GrannyGetTrackSamplerSSS, false);
    }

    public static C5316aEq aUt() {
        long GrannyGetTrackSamplerIII = grannyJNI.GrannyGetTrackSamplerIII();
        if (GrannyGetTrackSamplerIII == 0) {
            return null;
        }
        return new C5316aEq(GrannyGetTrackSamplerIII, false);
    }

    public static C5316aEq aUu() {
        long GrannyGetTrackSamplerIIU = grannyJNI.GrannyGetTrackSamplerIIU();
        if (GrannyGetTrackSamplerIIU == 0) {
            return null;
        }
        return new C5316aEq(GrannyGetTrackSamplerIIU, false);
    }

    /* renamed from: a */
    public static void m3992a(C0580IB ib) {
        grannyJNI.GrannyInvertTriTopologyWinding(C0580IB.m5303b(ib));
    }

    /* renamed from: a */
    public static void m3993a(C0580IB ib, int i, C1845ab abVar) {
        grannyJNI.GrannyRemapTopologyMaterials(C0580IB.m5303b(ib), i, C1845ab.m19847a(abVar));
    }

    /* renamed from: a */
    public static void m3949a(int i, int i2, C5501aLt alt, int i3, C5501aLt alt2) {
        grannyJNI.GrannyConvertIndices(i, i2, C5501aLt.m16286h(alt), i3, C5501aLt.m16286h(alt2));
    }

    /* renamed from: aw */
    public static void m4564aw(C1731Za za) {
        grannyJNI.GrannyTriMaterialGroupType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUv() {
        long GrannyTriMaterialGroupType_get = grannyJNI.GrannyTriMaterialGroupType_get();
        if (GrannyTriMaterialGroupType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyTriMaterialGroupType_get, false);
    }

    /* renamed from: ax */
    public static void m4565ax(C1731Za za) {
        grannyJNI.GrannyTriAnnotationSetType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUw() {
        long GrannyTriAnnotationSetType_get = grannyJNI.GrannyTriAnnotationSetType_get();
        if (GrannyTriAnnotationSetType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyTriAnnotationSetType_get, false);
    }

    /* renamed from: ay */
    public static void m4566ay(C1731Za za) {
        grannyJNI.GrannyTriTopologyType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUx() {
        long GrannyTriTopologyType_get = grannyJNI.GrannyTriTopologyType_get();
        if (GrannyTriTopologyType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyTriTopologyType_get, false);
    }

    public static int GrannyGetDefinedTypeCount() {
        return grannyJNI.GrannyGetDefinedTypeCount();
    }

    /* renamed from: a */
    public static void m4120a(C5204aAi aai) {
        grannyJNI.GrannyDefinedTypes_set(C5204aAi.m12619b(aai));
    }

    public static C5204aAi aUy() {
        long GrannyDefinedTypes_get = grannyJNI.GrannyDefinedTypes_get();
        if (GrannyDefinedTypes_get == 0) {
            return null;
        }
        return new C5204aAi(GrannyDefinedTypes_get, false);
    }

    /* renamed from: b */
    public static C0429Fx m4579b(C6795atn atn) {
        long GrannyBeginVariant = grannyJNI.GrannyBeginVariant(C6795atn.m26059c(atn));
        if (GrannyBeginVariant == 0) {
            return null;
        }
        return new C0429Fx(GrannyBeginVariant, false);
    }

    /* renamed from: a */
    public static boolean m4475a(C0429Fx fx, C3339qT qTVar, C5269aCv acv) {
        return grannyJNI.GrannyEndVariant(C0429Fx.m3299a(fx), C3339qT.m37523a(qTVar), C5269aCv.m13385b(acv));
    }

    /* renamed from: b */
    public static void m4613b(C0429Fx fx) {
        grannyJNI.GrannyAbortVariant(C0429Fx.m3299a(fx));
    }

    /* renamed from: c */
    public static int m4737c(C0429Fx fx) {
        return grannyJNI.GrannyGetResultingVariantTypeSize(C0429Fx.m3299a(fx));
    }

    /* renamed from: d */
    public static int m4795d(C0429Fx fx) {
        return grannyJNI.GrannyGetResultingVariantObjectSize(C0429Fx.m3299a(fx));
    }

    /* renamed from: a */
    public static boolean m4474a(C0429Fx fx, C5501aLt alt, C3339qT qTVar, C5501aLt alt2, C5269aCv acv) {
        return grannyJNI.GrannyEndVariantInPlace(C0429Fx.m3299a(fx), C5501aLt.m16286h(alt), C3339qT.m37523a(qTVar), C5501aLt.m16286h(alt2), C5269aCv.m13385b(acv));
    }

    /* renamed from: a */
    public static void m3979a(C0429Fx fx, String str, int i) {
        grannyJNI.GrannyAddBoolMember(C0429Fx.m3299a(fx), str, i);
    }

    /* renamed from: b */
    public static void m4614b(C0429Fx fx, String str, int i) {
        grannyJNI.GrannyAddIntegerMember(C0429Fx.m3299a(fx), str, i);
    }

    /* renamed from: a */
    public static void m3981a(C0429Fx fx, String str, int i, C1845ab abVar) {
        grannyJNI.GrannyAddIntegerArrayMember(C0429Fx.m3299a(fx), str, i, C1845ab.m19847a(abVar));
    }

    /* renamed from: a */
    public static void m3983a(C0429Fx fx, String str, long j) {
        grannyJNI.GrannyAddUnsignedIntegerMember(C0429Fx.m3299a(fx), str, j);
    }

    /* renamed from: a */
    public static void m3978a(C0429Fx fx, String str, float f) {
        grannyJNI.GrannyAddScalarMember(C0429Fx.m3299a(fx), str, f);
    }

    /* renamed from: a */
    public static void m3982a(C0429Fx fx, String str, int i, C3159oW oWVar) {
        grannyJNI.GrannyAddScalarArrayMember(C0429Fx.m3299a(fx), str, i, C3159oW.m36741q(oWVar));
    }

    /* renamed from: a */
    public static void m3985a(C0429Fx fx, String str, String str2) {
        grannyJNI.GrannyAddStringMember(C0429Fx.m3299a(fx), str, str2);
    }

    /* renamed from: a */
    public static void m3984a(C0429Fx fx, String str, C1731Za za, C5501aLt alt) {
        grannyJNI.GrannyAddReferenceMember(C0429Fx.m3299a(fx), str, C1731Za.m12101bi(za), C5501aLt.m16286h(alt));
    }

    /* renamed from: a */
    public static void m3980a(C0429Fx fx, String str, int i, C1731Za za, C5501aLt alt) {
        grannyJNI.GrannyAddDynamicArrayMember(C0429Fx.m3299a(fx), str, i, C1731Za.m12101bi(za), C5501aLt.m16286h(alt));
    }

    public static String GrannyGetVersionString() {
        return grannyJNI.GrannyGetVersionString();
    }

    /* renamed from: a */
    public static void m4244a(C1845ab abVar, C1845ab abVar2, C1845ab abVar3, C1845ab abVar4) {
        grannyJNI.GrannyGetVersion(C1845ab.m19847a(abVar), C1845ab.m19847a(abVar2), C1845ab.m19847a(abVar3), C1845ab.m19847a(abVar4));
    }

    public static boolean GrannyVersionsMatch_(int i, int i2, int i3, int i4) {
        return grannyJNI.GrannyVersionsMatch_(i, i2, i3, i4);
    }

    /* renamed from: a */
    public static void m3955a(int i, C1731Za za, C5501aLt alt, C1731Za za2, Buffer buffer) {
        grannyJNI.GrannyConvertVertexLayouts(i, C1731Za.m12101bi(za), C5501aLt.m16286h(alt), C1731Za.m12101bi(za2), buffer);
    }

    /* renamed from: c */
    public static void m4758c(C1731Za za, C5501aLt alt) {
        grannyJNI.GrannyEnsureExactOneNorm(C1731Za.m12101bi(za), C5501aLt.m16286h(alt));
    }

    /* renamed from: a */
    public static void m3954a(int i, C1731Za za, C5501aLt alt) {
        grannyJNI.GrannyOneNormalizeWeights(i, C1731Za.m12101bi(za), C5501aLt.m16286h(alt));
    }

    /* renamed from: a */
    public static void m3956a(int i, C1731Za za, C5501aLt alt, C3159oW oWVar, C3159oW oWVar2, C3159oW oWVar3, boolean z, boolean z2) {
        grannyJNI.GrannyTransformVertices(i, C1731Za.m12101bi(za), C5501aLt.m16286h(alt), C3159oW.m36741q(oWVar), C3159oW.m36741q(oWVar2), C3159oW.m36741q(oWVar3), z, z2);
    }

    /* renamed from: b */
    public static void m4600b(int i, C1731Za za, C5501aLt alt) {
        grannyJNI.GrannyNormalizeVertices(i, C1731Za.m12101bi(za), C5501aLt.m16286h(alt));
    }

    public static int GrannyGetVertexTextureCoordinatesName(int i, String str) {
        return grannyJNI.GrannyGetVertexTextureCoordinatesName(i, str);
    }

    public static int GrannyGetVertexDiffuseColorName(int i, String str) {
        return grannyJNI.GrannyGetVertexDiffuseColorName(i, str);
    }

    public static int GrannyGetVertexSpecularColorName(int i, String str) {
        return grannyJNI.GrannyGetVertexSpecularColorName(i, str);
    }

    public static boolean GrannyIsSpatialVertexMember(String str) {
        return grannyJNI.GrannyIsSpatialVertexMember(str);
    }

    /* renamed from: az */
    public static int m4567az(C1731Za za) {
        return grannyJNI.GrannyGetVertexBoneCount(C1731Za.m12101bi(za));
    }

    /* renamed from: aA */
    public static int m4516aA(C1731Za za) {
        return grannyJNI.GrannyGetVertexChannelCount(C1731Za.m12101bi(za));
    }

    /* renamed from: a */
    public static void m3974a(C0251DE de, int i, C1731Za za, C5501aLt alt) {
        grannyJNI.GrannyGetSingleVertex(C0251DE.m1941a(de), i, C1731Za.m12101bi(za), C5501aLt.m16286h(alt));
    }

    /* renamed from: a */
    public static void m4115a(C1731Za za, C5501aLt alt, C3159oW oWVar) {
        grannyJNI.GrannySetVertexPosition(C1731Za.m12101bi(za), C5501aLt.m16286h(alt), C3159oW.m36741q(oWVar));
    }

    /* renamed from: b */
    public static void m4632b(C1731Za za, C5501aLt alt, C3159oW oWVar) {
        grannyJNI.GrannySetVertexNormal(C1731Za.m12101bi(za), C5501aLt.m16286h(alt), C3159oW.m36741q(oWVar));
    }

    /* renamed from: a */
    public static void m4113a(C1731Za za, C5501aLt alt, int i, C3159oW oWVar) {
        grannyJNI.GrannySetVertexColor(C1731Za.m12101bi(za), C5501aLt.m16286h(alt), i, C3159oW.m36741q(oWVar));
    }

    /* renamed from: b */
    public static void m4630b(C1731Za za, C5501aLt alt, int i, C3159oW oWVar) {
        grannyJNI.GrannySetVertexUVW(C1731Za.m12101bi(za), C5501aLt.m16286h(alt), i, C3159oW.m36741q(oWVar));
    }

    /* renamed from: aB */
    public static int m4517aB(C1731Za za) {
        return grannyJNI.GrannyGetVertexComponentCount(C1731Za.m12101bi(za));
    }

    /* renamed from: a */
    public static int m3706a(String str, C1731Za za) {
        return grannyJNI.GrannyGetVertexComponentIndex(str, C1731Za.m12101bi(za));
    }

    /* renamed from: a */
    public static String m3944a(String str, C0251DE de) {
        return grannyJNI.GrannyGetVertexComponentToolName(str, C0251DE.m1941a(de));
    }

    /* renamed from: aC */
    public static void m4518aC(C1731Za za) {
        grannyJNI.GrannyVertexAnnotationSetType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUz() {
        long GrannyVertexAnnotationSetType_get = grannyJNI.GrannyVertexAnnotationSetType_get();
        if (GrannyVertexAnnotationSetType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyVertexAnnotationSetType_get, false);
    }

    /* renamed from: aD */
    public static void m4519aD(C1731Za za) {
        grannyJNI.GrannyVertexDataType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUA() {
        long GrannyVertexDataType_get = grannyJNI.GrannyVertexDataType_get();
        if (GrannyVertexDataType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyVertexDataType_get, false);
    }

    /* renamed from: aE */
    public static void m4520aE(C1731Za za) {
        grannyJNI.GrannyP3VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUB() {
        long GrannyP3VertexType_get = grannyJNI.GrannyP3VertexType_get();
        if (GrannyP3VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyP3VertexType_get, false);
    }

    /* renamed from: aF */
    public static void m4521aF(C1731Za za) {
        grannyJNI.GrannyPT32VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUC() {
        long GrannyPT32VertexType_get = grannyJNI.GrannyPT32VertexType_get();
        if (GrannyPT32VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPT32VertexType_get, false);
    }

    /* renamed from: aG */
    public static void m4522aG(C1731Za za) {
        grannyJNI.GrannyPN33VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUD() {
        long GrannyPN33VertexType_get = grannyJNI.GrannyPN33VertexType_get();
        if (GrannyPN33VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPN33VertexType_get, false);
    }

    /* renamed from: aH */
    public static void m4523aH(C1731Za za) {
        grannyJNI.GrannyPNG333VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUE() {
        long GrannyPNG333VertexType_get = grannyJNI.GrannyPNG333VertexType_get();
        if (GrannyPNG333VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPNG333VertexType_get, false);
    }

    /* renamed from: aI */
    public static void m4524aI(C1731Za za) {
        grannyJNI.GrannyPNGT3332VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUF() {
        long GrannyPNGT3332VertexType_get = grannyJNI.GrannyPNGT3332VertexType_get();
        if (GrannyPNGT3332VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPNGT3332VertexType_get, false);
    }

    /* renamed from: aJ */
    public static void m4525aJ(C1731Za za) {
        grannyJNI.GrannyPNTG3323VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUG() {
        long GrannyPNTG3323VertexType_get = grannyJNI.GrannyPNTG3323VertexType_get();
        if (GrannyPNTG3323VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPNTG3323VertexType_get, false);
    }

    /* renamed from: aK */
    public static void m4526aK(C1731Za za) {
        grannyJNI.GrannyPNGB3333VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUH() {
        long GrannyPNGB3333VertexType_get = grannyJNI.GrannyPNGB3333VertexType_get();
        if (GrannyPNGB3333VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPNGB3333VertexType_get, false);
    }

    /* renamed from: aL */
    public static void m4527aL(C1731Za za) {
        grannyJNI.GrannyPNT332VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUI() {
        long GrannyPNT332VertexType_get = grannyJNI.GrannyPNT332VertexType_get();
        if (GrannyPNT332VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPNT332VertexType_get, false);
    }

    /* renamed from: aM */
    public static void m4528aM(C1731Za za) {
        grannyJNI.GrannyPNGBT33332VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUJ() {
        long GrannyPNGBT33332VertexType_get = grannyJNI.GrannyPNGBT33332VertexType_get();
        if (GrannyPNGBT33332VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPNGBT33332VertexType_get, false);
    }

    /* renamed from: aN */
    public static void m4529aN(C1731Za za) {
        grannyJNI.GrannyPNT333VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUK() {
        long GrannyPNT333VertexType_get = grannyJNI.GrannyPNT333VertexType_get();
        if (GrannyPNT333VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPNT333VertexType_get, false);
    }

    /* renamed from: aO */
    public static void m4530aO(C1731Za za) {
        grannyJNI.GrannyPNGBT33333VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUL() {
        long GrannyPNGBT33333VertexType_get = grannyJNI.GrannyPNGBT33333VertexType_get();
        if (GrannyPNGBT33333VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPNGBT33333VertexType_get, false);
    }

    /* renamed from: aP */
    public static void m4531aP(C1731Za za) {
        grannyJNI.GrannyPWN313VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUM() {
        long GrannyPWN313VertexType_get = grannyJNI.GrannyPWN313VertexType_get();
        if (GrannyPWN313VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWN313VertexType_get, false);
    }

    /* renamed from: aQ */
    public static void m4532aQ(C1731Za za) {
        grannyJNI.GrannyPWNG3133VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUN() {
        long GrannyPWNG3133VertexType_get = grannyJNI.GrannyPWNG3133VertexType_get();
        if (GrannyPWNG3133VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWNG3133VertexType_get, false);
    }

    /* renamed from: aR */
    public static void m4533aR(C1731Za za) {
        grannyJNI.GrannyPWNGT31332VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUO() {
        long GrannyPWNGT31332VertexType_get = grannyJNI.GrannyPWNGT31332VertexType_get();
        if (GrannyPWNGT31332VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWNGT31332VertexType_get, false);
    }

    /* renamed from: aS */
    public static void m4534aS(C1731Za za) {
        grannyJNI.GrannyPWNGB31333VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUP() {
        long GrannyPWNGB31333VertexType_get = grannyJNI.GrannyPWNGB31333VertexType_get();
        if (GrannyPWNGB31333VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWNGB31333VertexType_get, false);
    }

    /* renamed from: aT */
    public static void m4535aT(C1731Za za) {
        grannyJNI.GrannyPWNT3132VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUQ() {
        long GrannyPWNT3132VertexType_get = grannyJNI.GrannyPWNT3132VertexType_get();
        if (GrannyPWNT3132VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWNT3132VertexType_get, false);
    }

    /* renamed from: aU */
    public static void m4536aU(C1731Za za) {
        grannyJNI.GrannyPWNGBT313332VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUR() {
        long GrannyPWNGBT313332VertexType_get = grannyJNI.GrannyPWNGBT313332VertexType_get();
        if (GrannyPWNGBT313332VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWNGBT313332VertexType_get, false);
    }

    /* renamed from: aV */
    public static void m4537aV(C1731Za za) {
        grannyJNI.GrannyPWN323VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUS() {
        long GrannyPWN323VertexType_get = grannyJNI.GrannyPWN323VertexType_get();
        if (GrannyPWN323VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWN323VertexType_get, false);
    }

    /* renamed from: aW */
    public static void m4538aW(C1731Za za) {
        grannyJNI.GrannyPWNG3233VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUT() {
        long GrannyPWNG3233VertexType_get = grannyJNI.GrannyPWNG3233VertexType_get();
        if (GrannyPWNG3233VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWNG3233VertexType_get, false);
    }

    /* renamed from: aX */
    public static void m4539aX(C1731Za za) {
        grannyJNI.GrannyPWNGT32332VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUU() {
        long GrannyPWNGT32332VertexType_get = grannyJNI.GrannyPWNGT32332VertexType_get();
        if (GrannyPWNGT32332VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWNGT32332VertexType_get, false);
    }

    /* renamed from: aY */
    public static void m4540aY(C1731Za za) {
        grannyJNI.GrannyPWNGB32333VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUV() {
        long GrannyPWNGB32333VertexType_get = grannyJNI.GrannyPWNGB32333VertexType_get();
        if (GrannyPWNGB32333VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWNGB32333VertexType_get, false);
    }

    /* renamed from: aZ */
    public static void m4541aZ(C1731Za za) {
        grannyJNI.GrannyPWNT3232VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUW() {
        long GrannyPWNT3232VertexType_get = grannyJNI.GrannyPWNT3232VertexType_get();
        if (GrannyPWNT3232VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWNT3232VertexType_get, false);
    }

    /* renamed from: ba */
    public static void m4727ba(C1731Za za) {
        grannyJNI.GrannyPWNGBT323332VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUX() {
        long GrannyPWNGBT323332VertexType_get = grannyJNI.GrannyPWNGBT323332VertexType_get();
        if (GrannyPWNGBT323332VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWNGBT323332VertexType_get, false);
    }

    /* renamed from: bb */
    public static void m4728bb(C1731Za za) {
        grannyJNI.GrannyPWN343VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUY() {
        long GrannyPWN343VertexType_get = grannyJNI.GrannyPWN343VertexType_get();
        if (GrannyPWN343VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWN343VertexType_get, false);
    }

    /* renamed from: bc */
    public static void m4729bc(C1731Za za) {
        grannyJNI.GrannyPWNG3433VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aUZ() {
        long GrannyPWNG3433VertexType_get = grannyJNI.GrannyPWNG3433VertexType_get();
        if (GrannyPWNG3433VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWNG3433VertexType_get, false);
    }

    /* renamed from: bd */
    public static void m4730bd(C1731Za za) {
        grannyJNI.GrannyPWNGT34332VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aVa() {
        long GrannyPWNGT34332VertexType_get = grannyJNI.GrannyPWNGT34332VertexType_get();
        if (GrannyPWNGT34332VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWNGT34332VertexType_get, false);
    }

    /* renamed from: be */
    public static void m4731be(C1731Za za) {
        grannyJNI.GrannyPWNGB34333VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aVb() {
        long GrannyPWNGB34333VertexType_get = grannyJNI.GrannyPWNGB34333VertexType_get();
        if (GrannyPWNGB34333VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWNGB34333VertexType_get, false);
    }

    /* renamed from: bf */
    public static void m4732bf(C1731Za za) {
        grannyJNI.GrannyPWNT3432VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aVc() {
        long GrannyPWNT3432VertexType_get = grannyJNI.GrannyPWNT3432VertexType_get();
        if (GrannyPWNT3432VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWNT3432VertexType_get, false);
    }

    /* renamed from: bg */
    public static void m4733bg(C1731Za za) {
        grannyJNI.GrannyPWNGBT343332VertexType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aVd() {
        long GrannyPWNGBT343332VertexType_get = grannyJNI.GrannyPWNGBT343332VertexType_get();
        if (GrannyPWNGBT343332VertexType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyPWNGBT343332VertexType_get, false);
    }

    /* renamed from: bh */
    public static void m4734bh(C1731Za za) {
        grannyJNI.GrannyVertexWeightArraysType_set(C1731Za.m12101bi(za));
    }

    public static C1731Za aVe() {
        long GrannyVertexWeightArraysType_get = grannyJNI.GrannyVertexWeightArraysType_get();
        if (GrannyVertexWeightArraysType_get == 0) {
            return null;
        }
        return new C1731Za(GrannyVertexWeightArraysType_get, false);
    }


    /* renamed from: hy */
    public static C3894wP m4924hy(int i) {
        long GrannyNewWorldPoseNoComposite = grannyJNI.GrannyNewWorldPoseNoComposite(i);
        if (GrannyNewWorldPoseNoComposite == 0) {
            return null;
        }
        return new C3894wP(GrannyNewWorldPoseNoComposite, false);
    }

    /* renamed from: c */
    public static int m4741c(C3894wP wPVar) {
        return grannyJNI.GrannyGetWorldPoseBoneCount(C3894wP.m40635a(wPVar));
    }

    /* renamed from: a */
    public static int m3678a(int i, C0706KC kc) {
        return grannyJNI.GrannyGetResultingWorldPoseSize(i, kc.swigValue());
    }

    /* renamed from: a */
    public static C3894wP m3919a(int i, C0706KC kc, Buffer buffer) {
        long GrannyNewWorldPoseInPlace = grannyJNI.GrannyNewWorldPoseInPlace(i, kc.swigValue(), buffer);
        if (GrannyNewWorldPoseInPlace == 0) {
            return null;
        }
        return new C3894wP(GrannyNewWorldPoseInPlace, false);
    }

    /* renamed from: a */
    public static C3159oW m3906a(C3894wP wPVar, int i) {
        long GrannyGetWorldPose4x4 = grannyJNI.GrannyGetWorldPose4x4(C3894wP.m40635a(wPVar), i);
        if (GrannyGetWorldPose4x4 == 0) {
            return null;
        }
        return new C3159oW(GrannyGetWorldPose4x4, false);
    }

    /* renamed from: b */
    public static C3159oW m4595b(C3894wP wPVar, int i) {
        long GrannyGetWorldPoseComposite4x4 = grannyJNI.GrannyGetWorldPoseComposite4x4(C3894wP.m40635a(wPVar), i);
        if (GrannyGetWorldPoseComposite4x4 == 0) {
            return null;
        }
        return new C3159oW(GrannyGetWorldPoseComposite4x4, false);
    }

    /* renamed from: d */
    public static aNK m4800d(C3894wP wPVar) {
        long GrannyGetWorldPose4x4Array = grannyJNI.GrannyGetWorldPose4x4Array(C3894wP.m40635a(wPVar));
        if (GrannyGetWorldPose4x4Array == 0) {
            return null;
        }
        return new aNK(GrannyGetWorldPose4x4Array, false);
    }

    /* renamed from: e */
    public static aNK m4831e(C3894wP wPVar) {
        long GrannyGetWorldPoseComposite4x4Array = grannyJNI.GrannyGetWorldPoseComposite4x4Array(C3894wP.m40635a(wPVar));
        if (GrannyGetWorldPoseComposite4x4Array == 0) {
            return null;
        }
        return new aNK(GrannyGetWorldPoseComposite4x4Array, false);
    }

    /* renamed from: a */
    public static void m4017a(C0913NP np, int i, int i2, int i3, int i4, aUB aub, Buffer buffer, C3894wP wPVar) {
        grannyJNI.GrannyBuildWorldPoseLOD(C0913NP.m7619a(np), i, i2, i3, i4, aUB.m18499d(aub), buffer, C3894wP.m40635a(wPVar));
    }

    /* renamed from: a */
    public static void m4020a(C0913NP np, int i, int i2, C1845ab abVar, C1845ab abVar2, aUB aub, Buffer buffer, C3894wP wPVar) {
        grannyJNI.GrannyBuildWorldPoseSparse(C0913NP.m7619a(np), i, i2, C1845ab.m19847a(abVar), C1845ab.m19847a(abVar2), aUB.m18499d(aub), buffer, C3894wP.m40635a(wPVar));
    }

    /* renamed from: a */
    public static void m4024a(C0913NP np, int i, int i2, Buffer buffer, C3894wP wPVar) {
        grannyJNI.GrannyBuildRestWorldPose(C0913NP.m7619a(np), i, i2, buffer, C3894wP.m40635a(wPVar));
    }

    /* renamed from: b */
    public static void m4619b(C0913NP np, int i, int i2, aUB aub, Buffer buffer, C3894wP wPVar) {
        grannyJNI.GrannyBuildWorldPoseNoComposite(C0913NP.m7619a(np), i, i2, aUB.m18499d(aub), buffer, C3894wP.m40635a(wPVar));
    }

    /* renamed from: b */
    public static void m4618b(C0913NP np, int i, int i2, int i3, int i4, aUB aub, Buffer buffer, C3894wP wPVar) {
        grannyJNI.GrannyBuildWorldPoseNoCompositeLOD(C0913NP.m7619a(np), i, i2, i3, i4, aUB.m18499d(aub), buffer, C3894wP.m40635a(wPVar));
    }

    /* renamed from: b */
    public static void m4620b(C0913NP np, int i, int i2, C1845ab abVar, C1845ab abVar2, aUB aub, Buffer buffer, C3894wP wPVar) {
        grannyJNI.GrannyBuildWorldPoseNoCompositeSparse(C0913NP.m7619a(np), i, i2, C1845ab.m19847a(abVar), C1845ab.m19847a(abVar2), aUB.m18499d(aub), buffer, C3894wP.m40635a(wPVar));
    }

    /* renamed from: a */
    public static void m4021a(C0913NP np, int i, int i2, C3894wP wPVar) {
        grannyJNI.GrannyBuildWorldPoseComposites(C0913NP.m7619a(np), i, i2, C3894wP.m40635a(wPVar));
    }

    /* renamed from: a */
    public static void m4023a(C0913NP np, int i, int i2, C3894wP wPVar, aNK ank) {
        grannyJNI.GrannyBuildCompositeBuffer(C0913NP.m7619a(np), i, i2, C3894wP.m40635a(wPVar), aNK.m16548a(ank));
    }

    /* renamed from: a */
    public static void m4022a(C0913NP np, int i, int i2, C3894wP wPVar, aMV amv) {
        grannyJNI.GrannyBuildCompositeBufferTransposed(C0913NP.m7619a(np), i, i2, C3894wP.m40635a(wPVar), aMV.m16389a(amv));
    }

    /* renamed from: a */
    public static void m4031a(C0913NP np, C3894wP wPVar, C1845ab abVar, int i, aNK ank) {
        grannyJNI.GrannyBuildIndexedCompositeBuffer(C0913NP.m7619a(np), C3894wP.m40635a(wPVar), C1845ab.m19847a(abVar), i, aNK.m16548a(ank));
    }

    /* renamed from: a */
    public static void m4030a(C0913NP np, C3894wP wPVar, C1845ab abVar, int i, aMV amv) {
        grannyJNI.GrannyBuildIndexedCompositeBufferTransposed(C0913NP.m7619a(np), C3894wP.m40635a(wPVar), C1845ab.m19847a(abVar), i, aMV.m16389a(amv));
    }

    /* renamed from: a */
    public static void m4026a(C0913NP np, int i, aUB aub, Buffer buffer, C3894wP wPVar) {
        grannyJNI.GrannyUpdateWorldPoseChildren(C0913NP.m7619a(np), i, aUB.m18499d(aub), buffer, C3894wP.m40635a(wPVar));
    }

    /* renamed from: hz */
    public static C6900avo m4925hz(int i) {
        long new_char_pointer_array = grannyJNI.new_char_pointer_array(i);
        if (new_char_pointer_array == 0) {
            return null;
        }
        return new C6900avo(new_char_pointer_array, false);
    }

    /* renamed from: c */
    public static void m4773c(C6900avo avo) {
        grannyJNI.delete_char_pointer_array(C6900avo.m26731d(avo));
    }


    /* renamed from: a */
    public static void m4359a(C6900avo avo, int i, String str) {
        grannyJNI.char_pointer_array_setitem(C6900avo.m26731d(avo), i, str);
    }

    /* renamed from: hA */
    public static C3521ru m4893hA(int i) {
        long new_granny_model_array = grannyJNI.new_granny_model_array(i);
        if (new_granny_model_array == 0) {
            return null;
        }
        return new C3521ru(new_granny_model_array, false);
    }

    /* renamed from: c */
    public static void m4785c(C3521ru ruVar) {
        grannyJNI.delete_granny_model_array(C3521ru.m38665b(ruVar));
    }


    /* renamed from: a */
    public static void m4434a(C3521ru ruVar, int i, C5975adz adz) {
        grannyJNI.granny_model_array_setitem(C3521ru.m38665b(ruVar), i, C5975adz.m21108e(adz));
    }

    /* renamed from: hB */
    public static C6508aoM m4894hB(int i) {
        long new_granny_allocated_block_array = grannyJNI.new_granny_allocated_block_array(i);
        if (new_granny_allocated_block_array == 0) {
            return null;
        }
        return new C6508aoM(new_granny_allocated_block_array, false);
    }

    /* renamed from: b */
    public static void m4655b(C6508aoM aom) {
        grannyJNI.delete_granny_allocated_block_array(C6508aoM.m24414c(aom));
    }

    /* renamed from: a */
    public static C0007AA m3708a(C6508aoM aom, int i) {
        long granny_allocated_block_array_getitem = grannyJNI.granny_allocated_block_array_getitem(C6508aoM.m24414c(aom), i);
        if (granny_allocated_block_array_getitem == 0) {
            return null;
        }
        return new C0007AA(granny_allocated_block_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4302a(C6508aoM aom, int i, C0007AA aa) {
        grannyJNI.granny_allocated_block_array_setitem(C6508aoM.m24414c(aom), i, C0007AA.m7a(aa));
    }

    /* renamed from: hC */
    public static C6428amk m4895hC(int i) {
        long new_granny_allocation_header_array = grannyJNI.new_granny_allocation_header_array(i);
        if (new_granny_allocation_header_array == 0) {
            return null;
        }
        return new C6428amk(new_granny_allocation_header_array, false);
    }

    /* renamed from: a */
    public static void m4293a(C6428amk amk) {
        grannyJNI.delete_granny_allocation_header_array(C6428amk.m23978b(amk));
    }

    /* renamed from: a */
    public static C6114agi m3843a(C6428amk amk, int i) {
        long granny_allocation_header_array_getitem = grannyJNI.granny_allocation_header_array_getitem(C6428amk.m23978b(amk), i);
        if (granny_allocation_header_array_getitem == 0) {
            return null;
        }
        return new C6114agi(granny_allocation_header_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4294a(C6428amk amk, int i, C6114agi agi) {
        grannyJNI.granny_allocation_header_array_setitem(C6428amk.m23978b(amk), i, C6114agi.m22105b(agi));
    }

    /* renamed from: hD */
    public static aPV m4896hD(int i) {
        long new_granny_allocation_information_array = grannyJNI.new_granny_allocation_information_array(i);
        if (new_granny_allocation_information_array == 0) {
            return null;
        }
        return new aPV(new_granny_allocation_information_array, false);
    }

    /* renamed from: a */
    public static void m4189a(aPV apv) {
        grannyJNI.delete_granny_allocation_information_array(aPV.m17259b(apv));
    }

    /* renamed from: a */
    public static C0431Fz m3723a(aPV apv, int i) {
        long granny_allocation_information_array_getitem = grannyJNI.granny_allocation_information_array_getitem(aPV.m17259b(apv), i);
        if (granny_allocation_information_array_getitem == 0) {
            return null;
        }
        return new C0431Fz(granny_allocation_information_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4190a(aPV apv, int i, C0431Fz fz) {
        grannyJNI.granny_allocation_information_array_setitem(aPV.m17259b(apv), i, C0431Fz.m3301a(fz));
    }

    /* renamed from: hE */
    public static C6917awF m4897hE(int i) {
        long new_granny_animation_array = grannyJNI.new_granny_animation_array(i);
        if (new_granny_animation_array == 0) {
            return null;
        }
        return new C6917awF(new_granny_animation_array, false);
    }

    /* renamed from: b */
    public static void m4666b(C6917awF awf) {
        grannyJNI.delete_granny_animation_array(C6917awF.m26850c(awf));
    }


    /* renamed from: a */
    public static void m4364a(C6917awF awf, int i, C3242pY pYVar) {
        grannyJNI.granny_animation_array_setitem(C6917awF.m26850c(awf), i, C3242pY.m37170b(pYVar));
    }

    /* renamed from: hF */
    public static C3917wl m4898hF(int i) {
        long new_granny_animation_binding_array = grannyJNI.new_granny_animation_binding_array(i);
        if (new_granny_animation_binding_array == 0) {
            return null;
        }
        return new C3917wl(new_granny_animation_binding_array, false);
    }

    /* renamed from: b */
    public static void m4711b(C3917wl wlVar) {
        grannyJNI.delete_granny_animation_binding_array(C3917wl.m40724a(wlVar));
    }

    /* renamed from: a */
    public static C6583apj m3861a(C3917wl wlVar, int i) {
        long granny_animation_binding_array_getitem = grannyJNI.granny_animation_binding_array_getitem(C3917wl.m40724a(wlVar), i);
        if (granny_animation_binding_array_getitem == 0) {
            return null;
        }
        return new C6583apj(granny_animation_binding_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4451a(C3917wl wlVar, int i, C6583apj apj) {
        grannyJNI.granny_animation_binding_array_setitem(C3917wl.m40724a(wlVar), i, C6583apj.m24993f(apj));
    }

    /* renamed from: hG */
    public static C5865abt m4899hG(int i) {
        long new_granny_animation_binding_cache_status_array = grannyJNI.new_granny_animation_binding_cache_status_array(i);
        if (new_granny_animation_binding_cache_status_array == 0) {
            return null;
        }
        return new C5865abt(new_granny_animation_binding_cache_status_array, false);
    }

    /* renamed from: a */
    public static void m4250a(C5865abt abt) {
        grannyJNI.delete_granny_animation_binding_cache_status_array(C5865abt.m20101b(abt));
    }

    /* renamed from: a */
    public static C6208aiY m3846a(C5865abt abt, int i) {
        long granny_animation_binding_cache_status_array_getitem = grannyJNI.granny_animation_binding_cache_status_array_getitem(C5865abt.m20101b(abt), i);
        if (granny_animation_binding_cache_status_array_getitem == 0) {
            return null;
        }
        return new C6208aiY(granny_animation_binding_cache_status_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4251a(C5865abt abt, int i, C6208aiY aiy) {
        grannyJNI.granny_animation_binding_cache_status_array_setitem(C5865abt.m20101b(abt), i, C6208aiY.m22570c(aiy));
    }

    /* renamed from: hH */
    public static C3096nk m4900hH(int i) {
        long new_granny_animation_binding_identifier_array = grannyJNI.new_granny_animation_binding_identifier_array(i);
        if (new_granny_animation_binding_identifier_array == 0) {
            return null;
        }
        return new C3096nk(new_granny_animation_binding_identifier_array, false);
    }

    /* renamed from: b */
    public static void m4687b(C3096nk nkVar) {
        grannyJNI.delete_granny_animation_binding_identifier_array(C3096nk.m36488a(nkVar));
    }

    /* renamed from: a */
    public static C2563gq m3892a(C3096nk nkVar, int i) {
        long granny_animation_binding_identifier_array_getitem = grannyJNI.granny_animation_binding_identifier_array_getitem(C3096nk.m36488a(nkVar), i);
        if (granny_animation_binding_identifier_array_getitem == 0) {
            return null;
        }
        return new C2563gq(granny_animation_binding_identifier_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4404a(C3096nk nkVar, int i, C2563gq gqVar) {
        grannyJNI.granny_animation_binding_identifier_array_setitem(C3096nk.m36488a(nkVar), i, C2563gq.m32392a(gqVar));
    }

    /* renamed from: hI */
    public static C0590IL m4901hI(int i) {
        long new_granny_animation_lod_builder_array = grannyJNI.new_granny_animation_lod_builder_array(i);
        if (new_granny_animation_lod_builder_array == 0) {
            return null;
        }
        return new C0590IL(new_granny_animation_lod_builder_array, false);
    }

    /* renamed from: a */
    public static void m3994a(C0590IL il) {
        grannyJNI.delete_granny_animation_lod_builder_array(C0590IL.m5346b(il));
    }

    /* renamed from: a */
    public static C2876lK m3896a(C0590IL il, int i) {
        long granny_animation_lod_builder_array_getitem = grannyJNI.granny_animation_lod_builder_array_getitem(C0590IL.m5346b(il), i);
        if (granny_animation_lod_builder_array_getitem == 0) {
            return null;
        }
        return new C2876lK(granny_animation_lod_builder_array_getitem, false);
    }

    /* renamed from: a */
    public static void m3995a(C0590IL il, int i, C2876lK lKVar) {
        grannyJNI.granny_animation_lod_builder_array_setitem(C0590IL.m5346b(il), i, C2876lK.m34834a(lKVar));
    }

    /* renamed from: hJ */
    public static C6002aea m4902hJ(int i) {
        long new_granny_art_tool_info_array = grannyJNI.new_granny_art_tool_info_array(i);
        if (new_granny_art_tool_info_array == 0) {
            return null;
        }
        return new C6002aea(new_granny_art_tool_info_array, false);
    }

    /* renamed from: a */
    public static void m4259a(C6002aea aea) {
        grannyJNI.delete_granny_art_tool_info_array(C6002aea.m21235b(aea));
    }

    /* renamed from: a */
    public static C3222pQ m3909a(C6002aea aea, int i) {
        long granny_art_tool_info_array_getitem = grannyJNI.granny_art_tool_info_array_getitem(C6002aea.m21235b(aea), i);
        if (granny_art_tool_info_array_getitem == 0) {
            return null;
        }
        return new C3222pQ(granny_art_tool_info_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4260a(C6002aea aea, int i, C3222pQ pQVar) {
        grannyJNI.granny_art_tool_info_array_setitem(C6002aea.m21235b(aea), i, C3222pQ.m37096b(pQVar));
    }

    /* renamed from: hK */
    public static C6211aib m4903hK(int i) {
        long new_granny_blend_dag_node_array = grannyJNI.new_granny_blend_dag_node_array(i);
        if (new_granny_blend_dag_node_array == 0) {
            return null;
        }
        return new C6211aib(new_granny_blend_dag_node_array, false);
    }

    /* renamed from: a */
    public static void m4279a(C6211aib aib) {
        grannyJNI.delete_granny_blend_dag_node_array(C6211aib.m22616b(aib));
    }

    /* renamed from: a */
    public static C1325TP m3756a(C6211aib aib, int i) {
        long granny_blend_dag_node_array_getitem = grannyJNI.granny_blend_dag_node_array_getitem(C6211aib.m22616b(aib), i);
        if (granny_blend_dag_node_array_getitem == 0) {
            return null;
        }
        return new C1325TP(granny_blend_dag_node_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4280a(C6211aib aib, int i, C1325TP tp) {
        grannyJNI.granny_blend_dag_node_array_setitem(C6211aib.m22616b(aib), i, C1325TP.m9961u(tp));
    }

    /* renamed from: hL */
    public static C1391UO m4904hL(int i) {
        long new_granny_bone_array = grannyJNI.new_granny_bone_array(i);
        if (new_granny_bone_array == 0) {
            return null;
        }
        return new C1391UO(new_granny_bone_array, false);
    }

    /* renamed from: a */
    public static void m4087a(C1391UO uo) {
        grannyJNI.delete_granny_bone_array(C1391UO.m10256b(uo));
    }

    /* renamed from: a */
    public static C5767aVz m3821a(C1391UO uo, int i) {
        long granny_bone_array_getitem = grannyJNI.granny_bone_array_getitem(C1391UO.m10256b(uo), i);
        if (granny_bone_array_getitem == 0) {
            return null;
        }
        return new C5767aVz(granny_bone_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4088a(C1391UO uo, int i, C5767aVz avz) {
        grannyJNI.granny_bone_array_setitem(C1391UO.m10256b(uo), i, C5767aVz.m19225b(avz));
    }

    /* renamed from: hM */
    public static aTW m4905hM(int i) {
        long new_granny_bone_binding_array = grannyJNI.new_granny_bone_binding_array(i);
        if (new_granny_bone_binding_array == 0) {
            return null;
        }
        return new aTW(new_granny_bone_binding_array, false);
    }

    /* renamed from: a */
    public static void m4208a(aTW atw) {
        grannyJNI.delete_granny_bone_binding_array(aTW.m18357b(atw));
    }

    /* renamed from: a */
    public static C0867MZ m3731a(aTW atw, int i) {
        long granny_bone_binding_array_getitem = grannyJNI.granny_bone_binding_array_getitem(aTW.m18357b(atw), i);
        if (granny_bone_binding_array_getitem == 0) {
            return null;
        }
        return new C0867MZ(granny_bone_binding_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4209a(aTW atw, int i, C0867MZ mz) {
        grannyJNI.granny_bone_binding_array_setitem(aTW.m18357b(atw), i, C0867MZ.m7062a(mz));
    }

    /* renamed from: hN */
    public static aMC m4906hN(int i) {
        long new_granny_bound_transform_track_array = grannyJNI.new_granny_bound_transform_track_array(i);
        if (new_granny_bound_transform_track_array == 0) {
            return null;
        }
        return new aMC(new_granny_bound_transform_track_array, false);
    }

    /* renamed from: a */
    public static void m4158a(aMC amc) {
        grannyJNI.delete_granny_bound_transform_track_array(aMC.m16314b(amc));
    }

    /* renamed from: a */
    public static C0476Gb m3724a(aMC amc, int i) {
        long granny_bound_transform_track_array_getitem = grannyJNI.granny_bound_transform_track_array_getitem(aMC.m16314b(amc), i);
        if (granny_bound_transform_track_array_getitem == 0) {
            return null;
        }
        return new C0476Gb(granny_bound_transform_track_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4159a(aMC amc, int i, C0476Gb gb) {
        grannyJNI.granny_bound_transform_track_array_setitem(aMC.m16314b(amc), i, C0476Gb.m3443a(gb));
    }

    /* renamed from: hO */
    public static C3866vy m4907hO(int i) {
        long new_granny_box_intersection_array = grannyJNI.new_granny_box_intersection_array(i);
        if (new_granny_box_intersection_array == 0) {
            return null;
        }
        return new C3866vy(new_granny_box_intersection_array, false);
    }

    /* renamed from: b */
    public static void m4706b(C3866vy vyVar) {
        grannyJNI.delete_granny_box_intersection_array(C3866vy.m40441a(vyVar));
    }

    /* renamed from: a */
    public static C5458aKc m3784a(C3866vy vyVar, int i) {
        long granny_box_intersection_array_getitem = grannyJNI.granny_box_intersection_array_getitem(C3866vy.m40441a(vyVar), i);
        if (granny_box_intersection_array_getitem == 0) {
            return null;
        }
        return new C5458aKc(granny_box_intersection_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4440a(C3866vy vyVar, int i, C5458aKc akc) {
        grannyJNI.granny_box_intersection_array_setitem(C3866vy.m40441a(vyVar), i, C5458aKc.m16023a(akc));
    }

    /* renamed from: hP */
    public static C1293Sz m4908hP(int i) {
        long new_granny_bspline_error_array = grannyJNI.new_granny_bspline_error_array(i);
        if (new_granny_bspline_error_array == 0) {
            return null;
        }
        return new C1293Sz(new_granny_bspline_error_array, false);
    }

    /* renamed from: a */
    public static void m4066a(C1293Sz sz) {
        grannyJNI.delete_granny_bspline_error_array(C1293Sz.m9641b(sz));
    }

    /* renamed from: a */
    public static C6694arq m3871a(C1293Sz sz, int i) {
        long granny_bspline_error_array_getitem = grannyJNI.granny_bspline_error_array_getitem(C1293Sz.m9641b(sz), i);
        if (granny_bspline_error_array_getitem == 0) {
            return null;
        }
        return new C6694arq(granny_bspline_error_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4067a(C1293Sz sz, int i, C6694arq arq) {
        grannyJNI.granny_bspline_error_array_setitem(C1293Sz.m9641b(sz), i, C6694arq.m25594a(arq));
    }

    /* renamed from: hQ */
    public static C1969as m4909hQ(int i) {
        long new_granny_bspline_solver_array = grannyJNI.new_granny_bspline_solver_array(i);
        if (new_granny_bspline_solver_array == 0) {
            return null;
        }
        return new C1969as(new_granny_bspline_solver_array, false);
    }

    /* renamed from: b */
    public static void m4663b(C1969as asVar) {
        grannyJNI.delete_granny_bspline_solver_array(C1969as.m25628a(asVar));
    }

    /* renamed from: a */
    public static aMA m3793a(C1969as asVar, int i) {
        long granny_bspline_solver_array_getitem = grannyJNI.granny_bspline_solver_array_getitem(C1969as.m25628a(asVar), i);
        if (granny_bspline_solver_array_getitem == 0) {
            return null;
        }
        return new aMA(granny_bspline_solver_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4328a(C1969as asVar, int i, aMA ama) {
        grannyJNI.granny_bspline_solver_array_setitem(C1969as.m25628a(asVar), i, aMA.m16307b(ama));
    }

    /* renamed from: hR */
    public static C2985mc m4910hR(int i) {
        long new_granny_camera_array = grannyJNI.new_granny_camera_array(i);
        if (new_granny_camera_array == 0) {
            return null;
        }
        return new C2985mc(new_granny_camera_array, false);
    }

    /* renamed from: b */
    public static void m4685b(C2985mc mcVar) {
        grannyJNI.delete_granny_camera_array(C2985mc.m35800a(mcVar));
    }

    /* renamed from: a */
    public static C3120o m3902a(C2985mc mcVar, int i) {
        long granny_camera_array_getitem = grannyJNI.granny_camera_array_getitem(C2985mc.m35800a(mcVar), i);
        if (granny_camera_array_getitem == 0) {
            return null;
        }
        return new C3120o(granny_camera_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4402a(C2985mc mcVar, int i, C3120o oVar) {
        grannyJNI.granny_camera_array_setitem(C2985mc.m35800a(mcVar), i, C3120o.m36549a(oVar));
    }

    /* renamed from: hS */
    public static C6772atQ m4911hS(int i) {
        long new_granny_compress_curve_parameters_array = grannyJNI.new_granny_compress_curve_parameters_array(i);
        if (new_granny_compress_curve_parameters_array == 0) {
            return null;
        }
        return new C6772atQ(new_granny_compress_curve_parameters_array, false);
    }

    /* renamed from: a */
    public static void m4343a(C6772atQ atq) {
        grannyJNI.delete_granny_compress_curve_parameters_array(C6772atQ.m25862b(atq));
    }

    /* renamed from: a */
    public static C6640aqo m3869a(C6772atQ atq, int i) {
        long granny_compress_curve_parameters_array_getitem = grannyJNI.granny_compress_curve_parameters_array_getitem(C6772atQ.m25862b(atq), i);
        if (granny_compress_curve_parameters_array_getitem == 0) {
            return null;
        }
        return new C6640aqo(granny_compress_curve_parameters_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4344a(C6772atQ atq, int i, C6640aqo aqo) {
        grannyJNI.granny_compress_curve_parameters_array_setitem(C6772atQ.m25862b(atq), i, C6640aqo.m25217a(aqo));
    }

    /* renamed from: hT */
    public static C0127Ba m4912hT(int i) {
        long new_granny_control_array = grannyJNI.new_granny_control_array(i);
        if (new_granny_control_array == 0) {
            return null;
        }
        return new C0127Ba(new_granny_control_array, false);
    }

    /* renamed from: b */
    public static void m4607b(C0127Ba ba) {
        grannyJNI.delete_granny_control_array(C0127Ba.m1252a(ba));
    }

    /* renamed from: a */
    public static C5801aah m3830a(C0127Ba ba, int i) {
        long granny_control_array_getitem = grannyJNI.granny_control_array_getitem(C0127Ba.m1252a(ba), i);
        if (granny_control_array_getitem == 0) {
            return null;
        }
        return new C5801aah(granny_control_array_getitem, false);
    }

    /* renamed from: a */
    public static void m3972a(C0127Ba ba, int i, C5801aah aah) {
        grannyJNI.granny_control_array_setitem(C0127Ba.m1252a(ba), i, C5801aah.m19565x(aah));
    }

    /* renamed from: hU */
    public static C3511rn m4913hU(int i) {
        long new_granny_controlled_animation_array = grannyJNI.new_granny_controlled_animation_array(i);
        if (new_granny_controlled_animation_array == 0) {
            return null;
        }
        return new C3511rn(new_granny_controlled_animation_array, false);
    }

    /* renamed from: b */
    public static void m4701b(C3511rn rnVar) {
        grannyJNI.delete_granny_controlled_animation_array(C3511rn.m38638a(rnVar));
    }

    /* renamed from: a */
    public static C6700arw m3872a(C3511rn rnVar, int i) {
        long granny_controlled_animation_array_getitem = grannyJNI.granny_controlled_animation_array_getitem(C3511rn.m38638a(rnVar), i);
        if (granny_controlled_animation_array_getitem == 0) {
            return null;
        }
        return new C6700arw(granny_controlled_animation_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4433a(C3511rn rnVar, int i, C6700arw arw) {
        grannyJNI.granny_controlled_animation_array_setitem(C3511rn.m38638a(rnVar), i, C6700arw.m25605a(arw));
    }

    /* renamed from: hV */
    public static C0848MN m4914hV(int i) {
        long new_granny_controlled_animation_builder_array = grannyJNI.new_granny_controlled_animation_builder_array(i);
        if (new_granny_controlled_animation_builder_array == 0) {
            return null;
        }
        return new C0848MN(new_granny_controlled_animation_builder_array, false);
    }

    /* renamed from: a */
    public static void m4013a(C0848MN mn) {
        grannyJNI.delete_granny_controlled_animation_builder_array(C0848MN.m6971b(mn));
    }

    /* renamed from: a */
    public static C1407Ud m3765a(C0848MN mn, int i) {
        long granny_controlled_animation_builder_array_getitem = grannyJNI.granny_controlled_animation_builder_array_getitem(C0848MN.m6971b(mn), i);
        if (granny_controlled_animation_builder_array_getitem == 0) {
            return null;
        }
        return new C1407Ud(granny_controlled_animation_builder_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4014a(C0848MN mn, int i, C1407Ud ud) {
        grannyJNI.granny_controlled_animation_builder_array_setitem(C0848MN.m6971b(mn), i, C1407Ud.m10295b(ud));
    }

    /* renamed from: hW */
    public static C5383aHf m4915hW(int i) {
        long new_granny_controlled_pose_array = grannyJNI.new_granny_controlled_pose_array(i);
        if (new_granny_controlled_pose_array == 0) {
            return null;
        }
        return new C5383aHf(new_granny_controlled_pose_array, false);
    }

    /* renamed from: a */
    public static void m4141a(C5383aHf ahf) {
        grannyJNI.delete_granny_controlled_pose_array(C5383aHf.m15277b(ahf));
    }

    /* renamed from: a */
    public static C2751jV m3895a(C5383aHf ahf, int i) {
        long granny_controlled_pose_array_getitem = grannyJNI.granny_controlled_pose_array_getitem(C5383aHf.m15277b(ahf), i);
        if (granny_controlled_pose_array_getitem == 0) {
            return null;
        }
        return new C2751jV(granny_controlled_pose_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4142a(C5383aHf ahf, int i, C2751jV jVVar) {
        grannyJNI.granny_controlled_pose_array_setitem(C5383aHf.m15277b(ahf), i, C2751jV.m34032a(jVVar));
    }

    /* renamed from: hX */
    public static C3770uo m4916hX(int i) {
        long new_granny_counter_results_array = grannyJNI.new_granny_counter_results_array(i);
        if (new_granny_counter_results_array == 0) {
            return null;
        }
        return new C3770uo(new_granny_counter_results_array, false);
    }

    /* renamed from: b */
    public static void m4703b(C3770uo uoVar) {
        grannyJNI.delete_granny_counter_results_array(C3770uo.m40112a(uoVar));
    }

    /* renamed from: a */
    public static C6506aoK m3856a(C3770uo uoVar, int i) {
        long granny_counter_results_array_getitem = grannyJNI.granny_counter_results_array_getitem(C3770uo.m40112a(uoVar), i);
        if (granny_counter_results_array_getitem == 0) {
            return null;
        }
        return new C6506aoK(granny_counter_results_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4437a(C3770uo uoVar, int i, C6506aoK aok) {
        grannyJNI.granny_counter_results_array_setitem(C3770uo.m40112a(uoVar), i, C6506aoK.m24404a(aok));
    }

    /* renamed from: hY */
    public static C2363eW m4917hY(int i) {
        long new_granny_curve2_array = grannyJNI.new_granny_curve2_array(i);
        if (new_granny_curve2_array == 0) {
            return null;
        }
        return new C2363eW(new_granny_curve2_array, false);
    }

    /* renamed from: b */
    public static void m4672b(C2363eW eWVar) {
        grannyJNI.delete_granny_curve2_array(C2363eW.m29554a(eWVar));
    }

    /* renamed from: a */
    public static C3914wi m3929a(C2363eW eWVar, int i) {
        long granny_curve2_array_getitem = grannyJNI.granny_curve2_array_getitem(C2363eW.m29554a(eWVar), i);
        if (granny_curve2_array_getitem == 0) {
            return null;
        }
        return new C3914wi(granny_curve2_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4387a(C2363eW eWVar, int i, C3914wi wiVar) {
        grannyJNI.granny_curve2_array_setitem(C2363eW.m29554a(eWVar), i, C3914wi.m40719a(wiVar));
    }

    /* renamed from: hZ */
    public static aFT m4918hZ(int i) {
        long new_granny_curve_builder_array = grannyJNI.new_granny_curve_builder_array(i);
        if (new_granny_curve_builder_array == 0) {
            return null;
        }
        return new aFT(new_granny_curve_builder_array, false);
    }

    /* renamed from: a */
    public static void m4129a(aFT aft) {
        grannyJNI.delete_granny_curve_builder_array(aFT.m14560b(aft));
    }

    /* renamed from: a */
    public static C6098agS m3840a(aFT aft, int i) {
        long granny_curve_builder_array_getitem = grannyJNI.granny_curve_builder_array_getitem(aFT.m14560b(aft), i);
        if (granny_curve_builder_array_getitem == 0) {
            return null;
        }
        return new C6098agS(granny_curve_builder_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4130a(aFT aft, int i, C6098agS ags) {
        grannyJNI.granny_curve_builder_array_setitem(aFT.m14560b(aft), i, C6098agS.m22037e(ags));
    }

    /* renamed from: ia */
    public static C5532aMy m4960ia(int i) {
        long new_granny_curve_data_d3_constant32f_array = grannyJNI.new_granny_curve_data_d3_constant32f_array(i);
        if (new_granny_curve_data_d3_constant32f_array == 0) {
            return null;
        }
        return new C5532aMy(new_granny_curve_data_d3_constant32f_array, false);
    }

    /* renamed from: a */
    public static void m4169a(C5532aMy amy) {
        grannyJNI.delete_granny_curve_data_d3_constant32f_array(C5532aMy.m16528b(amy));
    }

    /* renamed from: a */
    public static C5862abq m3834a(C5532aMy amy, int i) {
        long granny_curve_data_d3_constant32f_array_getitem = grannyJNI.granny_curve_data_d3_constant32f_array_getitem(C5532aMy.m16528b(amy), i);
        if (granny_curve_data_d3_constant32f_array_getitem == 0) {
            return null;
        }
        return new C5862abq(granny_curve_data_d3_constant32f_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4170a(C5532aMy amy, int i, C5862abq abq) {
        grannyJNI.granny_curve_data_d3_constant32f_array_setitem(C5532aMy.m16528b(amy), i, C5862abq.m20088a(abq));
    }

    /* renamed from: ib */
    public static C5455aJz m4961ib(int i) {
        long new_granny_curve_data_d3_k16u_c16u_array = grannyJNI.new_granny_curve_data_d3_k16u_c16u_array(i);
        if (new_granny_curve_data_d3_k16u_c16u_array == 0) {
            return null;
        }
        return new C5455aJz(new_granny_curve_data_d3_k16u_c16u_array, false);
    }

    /* renamed from: a */
    public static void m4149a(C5455aJz ajz) {
        grannyJNI.delete_granny_curve_data_d3_k16u_c16u_array(C5455aJz.m15892b(ajz));
    }

    /* renamed from: a */
    public static C1105QI m3745a(C5455aJz ajz, int i) {
        long granny_curve_data_d3_k16u_c16u_array_getitem = grannyJNI.granny_curve_data_d3_k16u_c16u_array_getitem(C5455aJz.m15892b(ajz), i);
        if (granny_curve_data_d3_k16u_c16u_array_getitem == 0) {
            return null;
        }
        return new C1105QI(granny_curve_data_d3_k16u_c16u_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4150a(C5455aJz ajz, int i, C1105QI qi) {
        grannyJNI.granny_curve_data_d3_k16u_c16u_array_setitem(C5455aJz.m15892b(ajz), i, C1105QI.m8741a(qi));
    }

    /* renamed from: ic */
    public static C5555aNv m4962ic(int i) {
        long new_granny_curve_data_d3_k8u_c8u_array = grannyJNI.new_granny_curve_data_d3_k8u_c8u_array(i);
        if (new_granny_curve_data_d3_k8u_c8u_array == 0) {
            return null;
        }
        return new C5555aNv(new_granny_curve_data_d3_k8u_c8u_array, false);
    }

    /* renamed from: a */
    public static void m4182a(C5555aNv anv) {
        grannyJNI.delete_granny_curve_data_d3_k8u_c8u_array(C5555aNv.m16789b(anv));
    }

    /* renamed from: a */
    public static C0878Mj m3732a(C5555aNv anv, int i) {
        long granny_curve_data_d3_k8u_c8u_array_getitem = grannyJNI.granny_curve_data_d3_k8u_c8u_array_getitem(C5555aNv.m16789b(anv), i);
        if (granny_curve_data_d3_k8u_c8u_array_getitem == 0) {
            return null;
        }
        return new C0878Mj(granny_curve_data_d3_k8u_c8u_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4183a(C5555aNv anv, int i, C0878Mj mj) {
        grannyJNI.granny_curve_data_d3_k8u_c8u_array_setitem(C5555aNv.m16789b(anv), i, C0878Mj.m7148a(mj));
    }

    /* renamed from: id */
    public static C2879lN m4963id(int i) {
        long new_granny_curve_data_d3i1_k16u_c16u_array = grannyJNI.new_granny_curve_data_d3i1_k16u_c16u_array(i);
        if (new_granny_curve_data_d3i1_k16u_c16u_array == 0) {
            return null;
        }
        return new C2879lN(new_granny_curve_data_d3i1_k16u_c16u_array, false);
    }

    /* renamed from: b */
    public static void m4682b(C2879lN lNVar) {
        grannyJNI.delete_granny_curve_data_d3i1_k16u_c16u_array(C2879lN.m34843a(lNVar));
    }

    /* renamed from: a */
    public static C6259ajX m3847a(C2879lN lNVar, int i) {
        long granny_curve_data_d3i1_k16u_c16u_array_getitem = grannyJNI.granny_curve_data_d3i1_k16u_c16u_array_getitem(C2879lN.m34843a(lNVar), i);
        if (granny_curve_data_d3i1_k16u_c16u_array_getitem == 0) {
            return null;
        }
        return new C6259ajX(granny_curve_data_d3i1_k16u_c16u_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4399a(C2879lN lNVar, int i, C6259ajX ajx) {
        grannyJNI.granny_curve_data_d3i1_k16u_c16u_array_setitem(C2879lN.m34843a(lNVar), i, C6259ajX.m22978a(ajx));
    }

    /* renamed from: ie */
    public static C0088BB m4964ie(int i) {
        long new_granny_curve_data_d3i1_k32f_c32f_array = grannyJNI.new_granny_curve_data_d3i1_k32f_c32f_array(i);
        if (new_granny_curve_data_d3i1_k32f_c32f_array == 0) {
            return null;
        }
        return new C0088BB(new_granny_curve_data_d3i1_k32f_c32f_array, false);
    }

    /* renamed from: b */
    public static void m4606b(C0088BB bb) {
        grannyJNI.delete_granny_curve_data_d3i1_k32f_c32f_array(C0088BB.m735a(bb));
    }

    /* renamed from: a */
    public static C3098nm m3900a(C0088BB bb, int i) {
        long granny_curve_data_d3i1_k32f_c32f_array_getitem = grannyJNI.granny_curve_data_d3i1_k32f_c32f_array_getitem(C0088BB.m735a(bb), i);
        if (granny_curve_data_d3i1_k32f_c32f_array_getitem == 0) {
            return null;
        }
        return new C3098nm(granny_curve_data_d3i1_k32f_c32f_array_getitem, false);
    }

    /* renamed from: a */
    public static void m3971a(C0088BB bb, int i, C3098nm nmVar) {
        grannyJNI.granny_curve_data_d3i1_k32f_c32f_array_setitem(C0088BB.m735a(bb), i, C3098nm.m36489a(nmVar));
    }

    /* renamed from: if */
    public static aUY m4965if(int i) {
        long new_granny_curve_data_d3i1_k8u_c8u_array = grannyJNI.new_granny_curve_data_d3i1_k8u_c8u_array(i);
        if (new_granny_curve_data_d3i1_k8u_c8u_array == 0) {
            return null;
        }
        return new aUY(new_granny_curve_data_d3i1_k8u_c8u_array, false);
    }

    /* renamed from: a */
    public static void m4224a(aUY auy) {
        grannyJNI.delete_granny_curve_data_d3i1_k8u_c8u_array(aUY.m18624b(auy));
    }

    /* renamed from: a */
    public static C0629Iv m3728a(aUY auy, int i) {
        long granny_curve_data_d3i1_k8u_c8u_array_getitem = grannyJNI.granny_curve_data_d3i1_k8u_c8u_array_getitem(aUY.m18624b(auy), i);
        if (granny_curve_data_d3i1_k8u_c8u_array_getitem == 0) {
            return null;
        }
        return new C0629Iv(granny_curve_data_d3i1_k8u_c8u_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4225a(aUY auy, int i, C0629Iv iv) {
        grannyJNI.granny_curve_data_d3i1_k8u_c8u_array_setitem(aUY.m18624b(auy), i, C0629Iv.m5494a(iv));
    }

    /* renamed from: ig */
    public static C6340alA m4966ig(int i) {
        long new_granny_curve_data_d4_constant32f_array = grannyJNI.new_granny_curve_data_d4_constant32f_array(i);
        if (new_granny_curve_data_d4_constant32f_array == 0) {
            return null;
        }
        return new C6340alA(new_granny_curve_data_d4_constant32f_array, false);
    }

    /* renamed from: a */
    public static void m4285a(C6340alA ala) {
        grannyJNI.delete_granny_curve_data_d4_constant32f_array(C6340alA.m23686b(ala));
    }

    /* renamed from: a */
    public static C1522WO m3767a(C6340alA ala, int i) {
        long granny_curve_data_d4_constant32f_array_getitem = grannyJNI.granny_curve_data_d4_constant32f_array_getitem(C6340alA.m23686b(ala), i);
        if (granny_curve_data_d4_constant32f_array_getitem == 0) {
            return null;
        }
        return new C1522WO(granny_curve_data_d4_constant32f_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4286a(C6340alA ala, int i, C1522WO wo) {
        grannyJNI.granny_curve_data_d4_constant32f_array_setitem(C6340alA.m23686b(ala), i, C1522WO.m11150a(wo));
    }

    /* renamed from: ih */
    public static C6888avc m4967ih(int i) {
        long new_granny_curve_data_d4n_k16u_c15u_array = grannyJNI.new_granny_curve_data_d4n_k16u_c15u_array(i);
        if (new_granny_curve_data_d4n_k16u_c15u_array == 0) {
            return null;
        }
        return new C6888avc(new_granny_curve_data_d4n_k16u_c15u_array, false);
    }

    /* renamed from: a */
    public static void m4355a(C6888avc avc) {
        grannyJNI.delete_granny_curve_data_d4n_k16u_c15u_array(C6888avc.m26617b(avc));
    }

    /* renamed from: a */
    public static C3190ov m3908a(C6888avc avc, int i) {
        long granny_curve_data_d4n_k16u_c15u_array_getitem = grannyJNI.granny_curve_data_d4n_k16u_c15u_array_getitem(C6888avc.m26617b(avc), i);
        if (granny_curve_data_d4n_k16u_c15u_array_getitem == 0) {
            return null;
        }
        return new C3190ov(granny_curve_data_d4n_k16u_c15u_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4356a(C6888avc avc, int i, C3190ov ovVar) {
        grannyJNI.granny_curve_data_d4n_k16u_c15u_array_setitem(C6888avc.m26617b(avc), i, C3190ov.m36910a(ovVar));
    }

    /* renamed from: ii */
    public static aNV m4968ii(int i) {
        long new_granny_curve_data_d4n_k8u_c7u_array = grannyJNI.new_granny_curve_data_d4n_k8u_c7u_array(i);
        if (new_granny_curve_data_d4n_k8u_c7u_array == 0) {
            return null;
        }
        return new aNV(new_granny_curve_data_d4n_k8u_c7u_array, false);
    }

    /* renamed from: a */
    public static void m4177a(aNV anv) {
        grannyJNI.delete_granny_curve_data_d4n_k8u_c7u_array(aNV.m16602b(anv));
    }

    /* renamed from: a */
    public static C3898wT m3923a(aNV anv, int i) {
        long granny_curve_data_d4n_k8u_c7u_array_getitem = grannyJNI.granny_curve_data_d4n_k8u_c7u_array_getitem(aNV.m16602b(anv), i);
        if (granny_curve_data_d4n_k8u_c7u_array_getitem == 0) {
            return null;
        }
        return new C3898wT(granny_curve_data_d4n_k8u_c7u_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4178a(aNV anv, int i, C3898wT wTVar) {
        grannyJNI.granny_curve_data_d4n_k8u_c7u_array_setitem(aNV.m16602b(anv), i, C3898wT.m40637a(wTVar));
    }

    /* renamed from: ij */
    public static aOS m4969ij(int i) {
        long new_granny_curve_data_d9i1_k16u_c16u_array = grannyJNI.new_granny_curve_data_d9i1_k16u_c16u_array(i);
        if (new_granny_curve_data_d9i1_k16u_c16u_array == 0) {
            return null;
        }
        return new aOS(new_granny_curve_data_d9i1_k16u_c16u_array, false);
    }

    /* renamed from: a */
    public static void m4187a(aOS aos) {
        grannyJNI.delete_granny_curve_data_d9i1_k16u_c16u_array(aOS.m16904b(aos));
    }

    /* renamed from: a */
    public static C3359qf m3911a(aOS aos, int i) {
        long granny_curve_data_d9i1_k16u_c16u_array_getitem = grannyJNI.granny_curve_data_d9i1_k16u_c16u_array_getitem(aOS.m16904b(aos), i);
        if (granny_curve_data_d9i1_k16u_c16u_array_getitem == 0) {
            return null;
        }
        return new C3359qf(granny_curve_data_d9i1_k16u_c16u_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4188a(aOS aos, int i, C3359qf qfVar) {
        grannyJNI.granny_curve_data_d9i1_k16u_c16u_array_setitem(aOS.m16904b(aos), i, C3359qf.m37754a(qfVar));
    }

    /* renamed from: ik */
    public static aNE m4970ik(int i) {
        long new_granny_curve_data_d9i1_k8u_c8u_array = grannyJNI.new_granny_curve_data_d9i1_k8u_c8u_array(i);
        if (new_granny_curve_data_d9i1_k8u_c8u_array == 0) {
            return null;
        }
        return new aNE(new_granny_curve_data_d9i1_k8u_c8u_array, false);
    }

    /* renamed from: a */
    public static void m4173a(aNE ane) {
        grannyJNI.delete_granny_curve_data_d9i1_k8u_c8u_array(aNE.m16543b(ane));
    }

    /* renamed from: a */
    public static axY m3885a(aNE ane, int i) {
        long granny_curve_data_d9i1_k8u_c8u_array_getitem = grannyJNI.granny_curve_data_d9i1_k8u_c8u_array_getitem(aNE.m16543b(ane), i);
        if (granny_curve_data_d9i1_k8u_c8u_array_getitem == 0) {
            return null;
        }
        return new axY(granny_curve_data_d9i1_k8u_c8u_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4174a(aNE ane, int i, axY axy) {
        grannyJNI.granny_curve_data_d9i1_k8u_c8u_array_setitem(aNE.m16543b(ane), i, axY.m27086a(axy));
    }

    /* renamed from: il */
    public static C6899avn m4971il(int i) {
        long new_granny_curve_data_d9i3_k16u_c16u_array = grannyJNI.new_granny_curve_data_d9i3_k16u_c16u_array(i);
        if (new_granny_curve_data_d9i3_k16u_c16u_array == 0) {
            return null;
        }
        return new C6899avn(new_granny_curve_data_d9i3_k16u_c16u_array, false);
    }

    /* renamed from: a */
    public static void m4357a(C6899avn avn) {
        grannyJNI.delete_granny_curve_data_d9i3_k16u_c16u_array(C6899avn.m26730b(avn));
    }

    /* renamed from: a */
    public static aNX m3799a(C6899avn avn, int i) {
        long granny_curve_data_d9i3_k16u_c16u_array_getitem = grannyJNI.granny_curve_data_d9i3_k16u_c16u_array_getitem(C6899avn.m26730b(avn), i);
        if (granny_curve_data_d9i3_k16u_c16u_array_getitem == 0) {
            return null;
        }
        return new aNX(granny_curve_data_d9i3_k16u_c16u_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4358a(C6899avn avn, int i, aNX anx) {
        grannyJNI.granny_curve_data_d9i3_k16u_c16u_array_setitem(C6899avn.m26730b(avn), i, aNX.m16607a(anx));
    }

    /* renamed from: im */
    public static aUQ m4972im(int i) {
        long new_granny_curve_data_d9i3_k8u_c8u_array = grannyJNI.new_granny_curve_data_d9i3_k8u_c8u_array(i);
        if (new_granny_curve_data_d9i3_k8u_c8u_array == 0) {
            return null;
        }
        return new aUQ(new_granny_curve_data_d9i3_k8u_c8u_array, false);
    }

    /* renamed from: a */
    public static void m4220a(aUQ auq) {
        grannyJNI.delete_granny_curve_data_d9i3_k8u_c8u_array(aUQ.m18567b(auq));
    }

    /* renamed from: a */
    public static C6553apF m3857a(aUQ auq, int i) {
        long granny_curve_data_d9i3_k8u_c8u_array_getitem = grannyJNI.granny_curve_data_d9i3_k8u_c8u_array_getitem(aUQ.m18567b(auq), i);
        if (granny_curve_data_d9i3_k8u_c8u_array_getitem == 0) {
            return null;
        }
        return new C6553apF(granny_curve_data_d9i3_k8u_c8u_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4221a(aUQ auq, int i, C6553apF apf) {
        grannyJNI.granny_curve_data_d9i3_k8u_c8u_array_setitem(aUQ.m18567b(auq), i, C6553apF.m24792a(apf));
    }

    /* renamed from: in */
    public static C1065Pa m4973in(int i) {
        long new_granny_curve_data_da_constant32f_array = grannyJNI.new_granny_curve_data_da_constant32f_array(i);
        if (new_granny_curve_data_da_constant32f_array == 0) {
            return null;
        }
        return new C1065Pa(new_granny_curve_data_da_constant32f_array, false);
    }

    /* renamed from: a */
    public static void m4049a(C1065Pa pa) {
        grannyJNI.delete_granny_curve_data_da_constant32f_array(C1065Pa.m8516b(pa));
    }

    /* renamed from: a */
    public static C5650aRm m3809a(C1065Pa pa, int i) {
        long granny_curve_data_da_constant32f_array_getitem = grannyJNI.granny_curve_data_da_constant32f_array_getitem(C1065Pa.m8516b(pa), i);
        if (granny_curve_data_da_constant32f_array_getitem == 0) {
            return null;
        }
        return new C5650aRm(granny_curve_data_da_constant32f_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4050a(C1065Pa pa, int i, C5650aRm arm) {
        grannyJNI.granny_curve_data_da_constant32f_array_setitem(C1065Pa.m8516b(pa), i, C5650aRm.m17928a(arm));
    }

    /* renamed from: io */
    public static C0689Jp m4974io(int i) {
        long new_granny_curve_data_da_identity_array = grannyJNI.new_granny_curve_data_da_identity_array(i);
        if (new_granny_curve_data_da_identity_array == 0) {
            return null;
        }
        return new C0689Jp(new_granny_curve_data_da_identity_array, false);
    }

    /* renamed from: a */
    public static void m4000a(C0689Jp jp) {
        grannyJNI.delete_granny_curve_data_da_identity_array(C0689Jp.m6015b(jp));
    }

    /* renamed from: a */
    public static C6328ako m3850a(C0689Jp jp, int i) {
        long granny_curve_data_da_identity_array_getitem = grannyJNI.granny_curve_data_da_identity_array_getitem(C0689Jp.m6015b(jp), i);
        if (granny_curve_data_da_identity_array_getitem == 0) {
            return null;
        }
        return new C6328ako(granny_curve_data_da_identity_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4001a(C0689Jp jp, int i, C6328ako ako) {
        grannyJNI.granny_curve_data_da_identity_array_setitem(C0689Jp.m6015b(jp), i, C6328ako.m23610a(ako));
    }

    /* renamed from: ip */
    public static C6940awf m4975ip(int i) {
        long new_granny_curve_data_da_k16u_c16u_array = grannyJNI.new_granny_curve_data_da_k16u_c16u_array(i);
        if (new_granny_curve_data_da_k16u_c16u_array == 0) {
            return null;
        }
        return new C6940awf(new_granny_curve_data_da_k16u_c16u_array, false);
    }

    /* renamed from: a */
    public static void m4365a(C6940awf awf) {
        grannyJNI.delete_granny_curve_data_da_k16u_c16u_array(C6940awf.m26902b(awf));
    }

    /* renamed from: a */
    public static C0047AZ m3711a(C6940awf awf, int i) {
        long granny_curve_data_da_k16u_c16u_array_getitem = grannyJNI.granny_curve_data_da_k16u_c16u_array_getitem(C6940awf.m26902b(awf), i);
        if (granny_curve_data_da_k16u_c16u_array_getitem == 0) {
            return null;
        }
        return new C0047AZ(granny_curve_data_da_k16u_c16u_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4366a(C6940awf awf, int i, C0047AZ az) {
        grannyJNI.granny_curve_data_da_k16u_c16u_array_setitem(C6940awf.m26902b(awf), i, C0047AZ.m424a(az));
    }

    /* renamed from: iq */
    public static aGA m4976iq(int i) {
        long new_granny_curve_data_da_k32f_c32f_array = grannyJNI.new_granny_curve_data_da_k32f_c32f_array(i);
        if (new_granny_curve_data_da_k32f_c32f_array == 0) {
            return null;
        }
        return new aGA(new_granny_curve_data_da_k32f_c32f_array, false);
    }

    /* renamed from: a */
    public static void m4133a(aGA aga) {
        grannyJNI.delete_granny_curve_data_da_k32f_c32f_array(aGA.m14721b(aga));
    }

    /* renamed from: a */
    public static C6567apT m3858a(aGA aga, int i) {
        long granny_curve_data_da_k32f_c32f_array_getitem = grannyJNI.granny_curve_data_da_k32f_c32f_array_getitem(aGA.m14721b(aga), i);
        if (granny_curve_data_da_k32f_c32f_array_getitem == 0) {
            return null;
        }
        return new C6567apT(granny_curve_data_da_k32f_c32f_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4134a(aGA aga, int i, C6567apT apt) {
        grannyJNI.granny_curve_data_da_k32f_c32f_array_setitem(aGA.m14721b(aga), i, C6567apT.m24891a(apt));
    }

    /* renamed from: ir */
    public static azQ m4977ir(int i) {
        long new_granny_curve_data_da_k8u_c8u_array = grannyJNI.new_granny_curve_data_da_k8u_c8u_array(i);
        if (new_granny_curve_data_da_k8u_c8u_array == 0) {
            return null;
        }
        return new azQ(new_granny_curve_data_da_k8u_c8u_array, false);
    }

    /* renamed from: a */
    public static void m4374a(azQ azq) {
        grannyJNI.delete_granny_curve_data_da_k8u_c8u_array(azQ.m27664b(azq));
    }

    /* renamed from: a */
    public static C0417Fo m3721a(azQ azq, int i) {
        long granny_curve_data_da_k8u_c8u_array_getitem = grannyJNI.granny_curve_data_da_k8u_c8u_array_getitem(azQ.m27664b(azq), i);
        if (granny_curve_data_da_k8u_c8u_array_getitem == 0) {
            return null;
        }
        return new C0417Fo(granny_curve_data_da_k8u_c8u_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4375a(azQ azq, int i, C0417Fo fo) {
        grannyJNI.granny_curve_data_da_k8u_c8u_array_setitem(azQ.m27664b(azq), i, C0417Fo.m3279a(fo));
    }

    /* renamed from: is */
    public static C1444VG m4978is(int i) {
        long new_granny_curve_data_da_keyframes32f_array = grannyJNI.new_granny_curve_data_da_keyframes32f_array(i);
        if (new_granny_curve_data_da_keyframes32f_array == 0) {
            return null;
        }
        return new C1444VG(new_granny_curve_data_da_keyframes32f_array, false);
    }

    /* renamed from: a */
    public static void m4099a(C1444VG vg) {
        grannyJNI.delete_granny_curve_data_da_keyframes32f_array(C1444VG.m10481b(vg));
    }

    /* renamed from: a */
    public static C6073aft m3838a(C1444VG vg, int i) {
        long granny_curve_data_da_keyframes32f_array_getitem = grannyJNI.granny_curve_data_da_keyframes32f_array_getitem(C1444VG.m10481b(vg), i);
        if (granny_curve_data_da_keyframes32f_array_getitem == 0) {
            return null;
        }
        return new C6073aft(granny_curve_data_da_keyframes32f_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4100a(C1444VG vg, int i, C6073aft aft) {
        grannyJNI.granny_curve_data_da_keyframes32f_array_setitem(C1444VG.m10481b(vg), i, C6073aft.m21649a(aft));
    }

    /* renamed from: it */
    public static C1236SM m4979it(int i) {
        long new_granny_curve_data_header_array = grannyJNI.new_granny_curve_data_header_array(i);
        if (new_granny_curve_data_header_array == 0) {
            return null;
        }
        return new C1236SM(new_granny_curve_data_header_array, false);
    }

    /* renamed from: a */
    public static void m4062a(C1236SM sm) {
        grannyJNI.delete_granny_curve_data_header_array(C1236SM.m9385b(sm));
    }

    /* renamed from: a */
    public static C6341alB m3851a(C1236SM sm, int i) {
        long granny_curve_data_header_array_getitem = grannyJNI.granny_curve_data_header_array_getitem(C1236SM.m9385b(sm), i);
        if (granny_curve_data_header_array_getitem == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_header_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4063a(C1236SM sm, int i, C6341alB alb) {
        grannyJNI.granny_curve_data_header_array_setitem(C1236SM.m9385b(sm), i, C6341alB.m23687b(alb));
    }

    /* renamed from: iu */
    public static C6910avy m4980iu(int i) {
        long new_granny_dag_pose_cache_array = grannyJNI.new_granny_dag_pose_cache_array(i);
        if (new_granny_dag_pose_cache_array == 0) {
            return null;
        }
        return new C6910avy(new_granny_dag_pose_cache_array, false);
    }

    /* renamed from: a */
    public static void m4362a(C6910avy avy) {
        grannyJNI.delete_granny_dag_pose_cache_array(C6910avy.m26831b(avy));
    }

    /* renamed from: a */
    public static aOF m3800a(C6910avy avy, int i) {
        long granny_dag_pose_cache_array_getitem = grannyJNI.granny_dag_pose_cache_array_getitem(C6910avy.m26831b(avy), i);
        if (granny_dag_pose_cache_array_getitem == 0) {
            return null;
        }
        return new aOF(granny_dag_pose_cache_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4363a(C6910avy avy, int i, aOF aof) {
        grannyJNI.granny_dag_pose_cache_array_setitem(C6910avy.m26831b(avy), i, aOF.m16805b(aof));
    }

    /* renamed from: iv */
    public static C3339qT m4981iv(int i) {
        long new_granny_data_type_definition_array = grannyJNI.new_granny_data_type_definition_array(i);
        if (new_granny_data_type_definition_array == 0) {
            return null;
        }
        return new C3339qT(new_granny_data_type_definition_array, false);
    }

    /* renamed from: c */
    public static void m4784c(C3339qT qTVar) {
        grannyJNI.delete_granny_data_type_definition_array(C3339qT.m37523a(qTVar));
    }

    /* renamed from: a */
    public static C1731Za m3772a(C3339qT qTVar, int i) {
        long granny_data_type_definition_array_getitem = grannyJNI.granny_data_type_definition_array_getitem(C3339qT.m37523a(qTVar), i);
        if (granny_data_type_definition_array_getitem == 0) {
            return null;
        }
        return new C1731Za(granny_data_type_definition_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4428a(C3339qT qTVar, int i, C1731Za za) {
        grannyJNI.granny_data_type_definition_array_setitem(C3339qT.m37523a(qTVar), i, C1731Za.m12101bi(za));
    }

    /* renamed from: iw */
    public static C1386UJ m4982iw(int i) {
        long new_granny_defined_type_array = grannyJNI.new_granny_defined_type_array(i);
        if (new_granny_defined_type_array == 0) {
            return null;
        }
        return new C1386UJ(new_granny_defined_type_array, false);
    }

    /* renamed from: a */
    public static void m4085a(C1386UJ uj) {
        grannyJNI.delete_granny_defined_type_array(C1386UJ.m10212b(uj));
    }

    /* renamed from: a */
    public static C5204aAi m3774a(C1386UJ uj, int i) {
        long granny_defined_type_array_getitem = grannyJNI.granny_defined_type_array_getitem(C1386UJ.m10212b(uj), i);
        if (granny_defined_type_array_getitem == 0) {
            return null;
        }
        return new C5204aAi(granny_defined_type_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4086a(C1386UJ uj, int i, C5204aAi aai) {
        grannyJNI.granny_defined_type_array_setitem(C1386UJ.m10212b(uj), i, C5204aAi.m12619b(aai));
    }

    /* renamed from: ix */
    public static C5489aLh m4983ix(int i) {
        long new_granny_exporter_info_array = grannyJNI.new_granny_exporter_info_array(i);
        if (new_granny_exporter_info_array == 0) {
            return null;
        }
        return new C5489aLh(new_granny_exporter_info_array, false);
    }

    /* renamed from: a */
    public static void m4155a(C5489aLh alh) {
        grannyJNI.delete_granny_exporter_info_array(C5489aLh.m16263b(alh));
    }

    /* renamed from: a */
    public static C3167od m3907a(C5489aLh alh, int i) {
        long granny_exporter_info_array_getitem = grannyJNI.granny_exporter_info_array_getitem(C5489aLh.m16263b(alh), i);
        if (granny_exporter_info_array_getitem == 0) {
            return null;
        }
        return new C3167od(granny_exporter_info_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4156a(C5489aLh alh, int i, C3167od odVar) {
        grannyJNI.granny_exporter_info_array_setitem(C5489aLh.m16263b(alh), i, C3167od.m36747b(odVar));
    }

    /* renamed from: iy */
    public static aNC m4984iy(int i) {
        long new_granny_file_array = grannyJNI.new_granny_file_array(i);
        if (new_granny_file_array == 0) {
            return null;
        }
        return new aNC(new_granny_file_array, false);
    }

    /* renamed from: a */
    public static void m4171a(aNC anc) {
        grannyJNI.delete_granny_file_array(aNC.m16542b(anc));
    }

    /* renamed from: a */
    public static C6396amE m3854a(aNC anc, int i) {
        long granny_file_array_getitem = grannyJNI.granny_file_array_getitem(aNC.m16542b(anc), i);
        if (granny_file_array_getitem == 0) {
            return null;
        }
        return new C6396amE(granny_file_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4172a(aNC anc, int i, C6396amE ame) {
        grannyJNI.granny_file_array_setitem(aNC.m16542b(anc), i, C6396amE.m23854g(ame));
    }

    /* renamed from: iz */
    public static C2207cl m4985iz(int i) {
        long new_granny_file_builder_array = grannyJNI.new_granny_file_builder_array(i);
        if (new_granny_file_builder_array == 0) {
            return null;
        }
        return new C2207cl(new_granny_file_builder_array, false);
    }

    /* renamed from: b */
    public static void m4669b(C2207cl clVar) {
        grannyJNI.delete_granny_file_builder_array(C2207cl.m28598a(clVar));
    }

    /* renamed from: a */
    public static C6756atA m3875a(C2207cl clVar, int i) {
        long granny_file_builder_array_getitem = grannyJNI.granny_file_builder_array_getitem(C2207cl.m28598a(clVar), i);
        if (granny_file_builder_array_getitem == 0) {
            return null;
        }
        return new C6756atA(granny_file_builder_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4384a(C2207cl clVar, int i, C6756atA ata) {
        grannyJNI.granny_file_builder_array_setitem(C2207cl.m28598a(clVar), i, C6756atA.m25817b(ata));
    }

    /* renamed from: iA */
    public static C3129oG m4934iA(int i) {
        long new_granny_file_compressor_array = grannyJNI.new_granny_file_compressor_array(i);
        if (new_granny_file_compressor_array == 0) {
            return null;
        }
        return new C3129oG(new_granny_file_compressor_array, false);
    }

    /* renamed from: b */
    public static void m4691b(C3129oG oGVar) {
        grannyJNI.delete_granny_file_compressor_array(C3129oG.m36603a(oGVar));
    }

    /* renamed from: a */
    public static C0207CY m3716a(C3129oG oGVar, int i) {
        long granny_file_compressor_array_getitem = grannyJNI.granny_file_compressor_array_getitem(C3129oG.m36603a(oGVar), i);
        if (granny_file_compressor_array_getitem == 0) {
            return null;
        }
        return new C0207CY(granny_file_compressor_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4411a(C3129oG oGVar, int i, C0207CY cy) {
        grannyJNI.granny_file_compressor_array_setitem(C3129oG.m36603a(oGVar), i, C0207CY.m1809a(cy));
    }

    /* renamed from: iB */
    public static aRC m4935iB(int i) {
        long new_granny_file_data_tree_writer_array = grannyJNI.new_granny_file_data_tree_writer_array(i);
        if (new_granny_file_data_tree_writer_array == 0) {
            return null;
        }
        return new aRC(new_granny_file_data_tree_writer_array, false);
    }

    /* renamed from: a */
    public static void m4202a(aRC arc) {
        grannyJNI.delete_granny_file_data_tree_writer_array(aRC.m17724b(arc));
    }

    /* renamed from: a */
    public static C3952xI m3934a(aRC arc, int i) {
        long granny_file_data_tree_writer_array_getitem = grannyJNI.granny_file_data_tree_writer_array_getitem(aRC.m17724b(arc), i);
        if (granny_file_data_tree_writer_array_getitem == 0) {
            return null;
        }
        return new C3952xI(granny_file_data_tree_writer_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4203a(aRC arc, int i, C3952xI xIVar) {
        grannyJNI.granny_file_data_tree_writer_array_setitem(aRC.m17724b(arc), i, C3952xI.m40883a(xIVar));
    }

    /* renamed from: iC */
    public static C2594hJ m4936iC(int i) {
        long new_granny_file_fixup_array = grannyJNI.new_granny_file_fixup_array(i);
        if (new_granny_file_fixup_array == 0) {
            return null;
        }
        return new C2594hJ(new_granny_file_fixup_array, false);
    }

    /* renamed from: b */
    public static void m4673b(C2594hJ hJVar) {
        grannyJNI.delete_granny_file_fixup_array(C2594hJ.m32547a(hJVar));
    }

    /* renamed from: a */
    public static C6932awU m3884a(C2594hJ hJVar, int i) {
        long granny_file_fixup_array_getitem = grannyJNI.granny_file_fixup_array_getitem(C2594hJ.m32547a(hJVar), i);
        if (granny_file_fixup_array_getitem == 0) {
            return null;
        }
        return new C6932awU(granny_file_fixup_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4390a(C2594hJ hJVar, int i, C6932awU awu) {
        grannyJNI.granny_file_fixup_array_setitem(C2594hJ.m32547a(hJVar), i, C6932awU.m26879a(awu));
    }

    /* renamed from: iD */
    public static aDW m4937iD(int i) {
        long new_granny_file_info_array = grannyJNI.new_granny_file_info_array(i);
        if (new_granny_file_info_array == 0) {
            return null;
        }
        return new aDW(new_granny_file_info_array, false);
    }

    /* renamed from: a */
    public static void m4124a(aDW adw) {
        grannyJNI.delete_granny_file_info_array(aDW.m13608b(adw));
    }

    /* renamed from: a */
    public static C2267dS m3888a(aDW adw, int i) {
        long granny_file_info_array_getitem = grannyJNI.granny_file_info_array_getitem(aDW.m13608b(adw), i);
        if (granny_file_info_array_getitem == 0) {
            return null;
        }
        return new C2267dS(granny_file_info_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4125a(aDW adw, int i, C2267dS dSVar) {
        grannyJNI.granny_file_info_array_setitem(aDW.m13608b(adw), i, C2267dS.m28965a(dSVar));
    }

    /* renamed from: iE */
    public static C6714asK m4938iE(int i) {
        long new_granny_file_location_array = grannyJNI.new_granny_file_location_array(i);
        if (new_granny_file_location_array == 0) {
            return null;
        }
        return new C6714asK(new_granny_file_location_array, false);
    }

    /* renamed from: a */
    public static void m4331a(C6714asK ask) {
        grannyJNI.delete_granny_file_location_array(C6714asK.m25652b(ask));
    }

    /* renamed from: a */
    public static C2481ff m3891a(C6714asK ask, int i) {
        long granny_file_location_array_getitem = grannyJNI.granny_file_location_array_getitem(C6714asK.m25652b(ask), i);
        if (granny_file_location_array_getitem == 0) {
            return null;
        }
        return new C2481ff(granny_file_location_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4332a(C6714asK ask, int i, C2481ff ffVar) {
        grannyJNI.granny_file_location_array_setitem(C6714asK.m25652b(ask), i, C2481ff.m31245a(ffVar));
    }

    /* renamed from: iF */
    public static C1250SU m4939iF(int i) {
        long new_granny_file_reader_array = grannyJNI.new_granny_file_reader_array(i);
        if (new_granny_file_reader_array == 0) {
            return null;
        }
        return new C1250SU(new_granny_file_reader_array, false);
    }

    /* renamed from: a */
    public static void m4064a(C1250SU su) {
        grannyJNI.delete_granny_file_reader_array(C1250SU.m9465b(su));
    }

    /* renamed from: a */
    public static C3505ri m3912a(C1250SU su, int i) {
        long granny_file_reader_array_getitem = grannyJNI.granny_file_reader_array_getitem(C1250SU.m9465b(su), i);
        if (granny_file_reader_array_getitem == 0) {
            return null;
        }
        return new C3505ri(granny_file_reader_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4065a(C1250SU su, int i, C3505ri riVar) {
        grannyJNI.granny_file_reader_array_setitem(C1250SU.m9465b(su), i, C3505ri.m38588a(riVar));
    }

    /* renamed from: iG */
    public static C2295dj m4940iG(int i) {
        long new_granny_file_writer_array = grannyJNI.new_granny_file_writer_array(i);
        if (new_granny_file_writer_array == 0) {
            return null;
        }
        return new C2295dj(new_granny_file_writer_array, false);
    }

    /* renamed from: b */
    public static void m4671b(C2295dj djVar) {
        grannyJNI.delete_granny_file_writer_array(C2295dj.m29185a(djVar));
    }

    /* renamed from: a */
    public static C5731aUp m3819a(C2295dj djVar, int i) {
        long granny_file_writer_array_getitem = grannyJNI.granny_file_writer_array_getitem(C2295dj.m29185a(djVar), i);
        if (granny_file_writer_array_getitem == 0) {
            return null;
        }
        return new C5731aUp(granny_file_writer_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4386a(C2295dj djVar, int i, C5731aUp aup) {
        grannyJNI.granny_file_writer_array_setitem(C2295dj.m29185a(djVar), i, C5731aUp.m18737b(aup));
    }

    /* renamed from: iH */
    public static C1197Rf m4941iH(int i) {
        long new_granny_fixed_allocator_array = grannyJNI.new_granny_fixed_allocator_array(i);
        if (new_granny_fixed_allocator_array == 0) {
            return null;
        }
        return new C1197Rf(new_granny_fixed_allocator_array, false);
    }

    /* renamed from: a */
    public static void m4058a(C1197Rf rf) {
        grannyJNI.delete_granny_fixed_allocator_array(C1197Rf.m9251b(rf));
    }

    /* renamed from: a */
    public static aIE m3783a(C1197Rf rf, int i) {
        long granny_fixed_allocator_array_getitem = grannyJNI.granny_fixed_allocator_array_getitem(C1197Rf.m9251b(rf), i);
        if (granny_fixed_allocator_array_getitem == 0) {
            return null;
        }
        return new aIE(granny_fixed_allocator_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4059a(C1197Rf rf, int i, aIE aie) {
        grannyJNI.granny_fixed_allocator_array_setitem(C1197Rf.m9251b(rf), i, aIE.m15360c(aie));
    }

    /* renamed from: iI */
    public static aVY m4942iI(int i) {
        long new_granny_fixed_allocator_block_array = grannyJNI.new_granny_fixed_allocator_block_array(i);
        if (new_granny_fixed_allocator_block_array == 0) {
            return null;
        }
        return new aVY(new_granny_fixed_allocator_block_array, false);
    }

    /* renamed from: a */
    public static void m4227a(aVY avy) {
        grannyJNI.delete_granny_fixed_allocator_block_array(aVY.m18877b(avy));
    }

    /* renamed from: a */
    public static C6277ajp m3848a(aVY avy, int i) {
        long granny_fixed_allocator_block_array_getitem = grannyJNI.granny_fixed_allocator_block_array_getitem(aVY.m18877b(avy), i);
        if (granny_fixed_allocator_block_array_getitem == 0) {
            return null;
        }
        return new C6277ajp(granny_fixed_allocator_block_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4228a(aVY avy, int i, C6277ajp ajp) {
        grannyJNI.granny_fixed_allocator_block_array_setitem(aVY.m18877b(avy), i, C6277ajp.m23053a(ajp));
    }

    /* renamed from: iJ */
    public static C0937Nl m4943iJ(int i) {
        long new_granny_fixed_allocator_unit_array = grannyJNI.new_granny_fixed_allocator_unit_array(i);
        if (new_granny_fixed_allocator_unit_array == 0) {
            return null;
        }
        return new C0937Nl(new_granny_fixed_allocator_unit_array, false);
    }

    /* renamed from: a */
    public static void m4034a(C0937Nl nl) {
        grannyJNI.delete_granny_fixed_allocator_unit_array(C0937Nl.m7717b(nl));
    }

    /* renamed from: a */
    public static C6372alg m3852a(C0937Nl nl, int i) {
        long granny_fixed_allocator_unit_array_getitem = grannyJNI.granny_fixed_allocator_unit_array_getitem(C0937Nl.m7717b(nl), i);
        if (granny_fixed_allocator_unit_array_getitem == 0) {
            return null;
        }
        return new C6372alg(granny_fixed_allocator_unit_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4035a(C0937Nl nl, int i, C6372alg alg) {
        grannyJNI.granny_fixed_allocator_unit_array_setitem(C0937Nl.m7717b(nl), i, C6372alg.m23797b(alg));
    }

    /* renamed from: iK */
    public static aUT m4944iK(int i) {
        long new_granny_grn_file_header_array = grannyJNI.new_granny_grn_file_header_array(i);
        if (new_granny_grn_file_header_array == 0) {
            return null;
        }
        return new aUT(new_granny_grn_file_header_array, false);
    }

    /* renamed from: a */
    public static void m4222a(aUT aut) {
        grannyJNI.delete_granny_grn_file_header_array(aUT.m18600b(aut));
    }

    /* renamed from: a */
    public static C5827abH m3832a(aUT aut, int i) {
        long granny_grn_file_header_array_getitem = grannyJNI.granny_grn_file_header_array_getitem(aUT.m18600b(aut), i);
        if (granny_grn_file_header_array_getitem == 0) {
            return null;
        }
        return new C5827abH(granny_grn_file_header_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4223a(aUT aut, int i, C5827abH abh) {
        grannyJNI.granny_grn_file_header_array_setitem(aUT.m18600b(aut), i, C5827abH.m19928b(abh));
    }

    /* renamed from: iL */
    public static C6474ane m4945iL(int i) {
        long new_granny_grn_file_magic_value_array = grannyJNI.new_granny_grn_file_magic_value_array(i);
        if (new_granny_grn_file_magic_value_array == 0) {
            return null;
        }
        return new C6474ane(new_granny_grn_file_magic_value_array, false);
    }

    /* renamed from: a */
    public static void m4298a(C6474ane ane) {
        grannyJNI.delete_granny_grn_file_magic_value_array(C6474ane.m24223b(ane));
    }

    /* renamed from: a */
    public static C5974ady m3835a(C6474ane ane, int i) {
        long granny_grn_file_magic_value_array_getitem = grannyJNI.granny_grn_file_magic_value_array_getitem(C6474ane.m24223b(ane), i);
        if (granny_grn_file_magic_value_array_getitem == 0) {
            return null;
        }
        return new C5974ady(granny_grn_file_magic_value_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4299a(C6474ane ane, int i, C5974ady ady) {
        grannyJNI.granny_grn_file_magic_value_array_setitem(C6474ane.m24223b(ane), i, C5974ady.m21102g(ady));
    }

    /* renamed from: iM */
    public static aAF m4946iM(int i) {
        long new_granny_grn_mixed_marshalling_fixup_array = grannyJNI.new_granny_grn_mixed_marshalling_fixup_array(i);
        if (new_granny_grn_mixed_marshalling_fixup_array == 0) {
            return null;
        }
        return new aAF(new_granny_grn_mixed_marshalling_fixup_array, false);
    }

    /* renamed from: a */
    public static void m4118a(aAF aaf) {
        grannyJNI.delete_granny_grn_mixed_marshalling_fixup_array(aAF.m12453b(aaf));
    }

    /* renamed from: a */
    public static C0347Ec m3719a(aAF aaf, int i) {
        long granny_grn_mixed_marshalling_fixup_array_getitem = grannyJNI.granny_grn_mixed_marshalling_fixup_array_getitem(aAF.m12453b(aaf), i);
        if (granny_grn_mixed_marshalling_fixup_array_getitem == 0) {
            return null;
        }
        return new C0347Ec(granny_grn_mixed_marshalling_fixup_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4119a(aAF aaf, int i, C0347Ec ec) {
        grannyJNI.granny_grn_mixed_marshalling_fixup_array_setitem(aAF.m12453b(aaf), i, C0347Ec.m2989a(ec));
    }

    /* renamed from: iN */
    public static C0310EB m4947iN(int i) {
        long new_granny_grn_pointer_fixup_array = grannyJNI.new_granny_grn_pointer_fixup_array(i);
        if (new_granny_grn_pointer_fixup_array == 0) {
            return null;
        }
        return new C0310EB(new_granny_grn_pointer_fixup_array, false);
    }

    /* renamed from: b */
    public static void m4610b(C0310EB eb) {
        grannyJNI.delete_granny_grn_pointer_fixup_array(C0310EB.m2778a(eb));
    }

    /* renamed from: a */
    public static C0577Hz m3725a(C0310EB eb, int i) {
        long granny_grn_pointer_fixup_array_getitem = grannyJNI.granny_grn_pointer_fixup_array_getitem(C0310EB.m2778a(eb), i);
        if (granny_grn_pointer_fixup_array_getitem == 0) {
            return null;
        }
        return new C0577Hz(granny_grn_pointer_fixup_array_getitem, false);
    }

    /* renamed from: a */
    public static void m3975a(C0310EB eb, int i, C0577Hz hz) {
        grannyJNI.granny_grn_pointer_fixup_array_setitem(C0310EB.m2778a(eb), i, C0577Hz.m5296a(hz));
    }

    /* renamed from: iO */
    public static C0491Go m4948iO(int i) {
        long new_granny_grn_reference_array = grannyJNI.new_granny_grn_reference_array(i);
        if (new_granny_grn_reference_array == 0) {
            return null;
        }
        return new C0491Go(new_granny_grn_reference_array, false);
    }

    /* renamed from: b */
    public static void m4616b(C0491Go go) {
        grannyJNI.delete_granny_grn_reference_array(C0491Go.m3487a(go));
    }

    /* renamed from: a */
    public static C0031AQ m3710a(C0491Go go, int i) {
        long granny_grn_reference_array_getitem = grannyJNI.granny_grn_reference_array_getitem(C0491Go.m3487a(go), i);
        if (granny_grn_reference_array_getitem == 0) {
            return null;
        }
        return new C0031AQ(granny_grn_reference_array_getitem, false);
    }

    /* renamed from: a */
    public static void m3989a(C0491Go go, int i, C0031AQ aq) {
        grannyJNI.granny_grn_reference_array_setitem(C0491Go.m3487a(go), i, C0031AQ.m385a(aq));
    }

    /* renamed from: iP */
    public static C2897lZ m4949iP(int i) {
        long new_granny_grn_section_array = grannyJNI.new_granny_grn_section_array(i);
        if (new_granny_grn_section_array == 0) {
            return null;
        }
        return new C2897lZ(new_granny_grn_section_array, false);
    }

    /* renamed from: b */
    public static void m4683b(C2897lZ lZVar) {
        grannyJNI.delete_granny_grn_section_array(C2897lZ.m35116a(lZVar));
    }

    /* renamed from: a */
    public static C4098zR m3936a(C2897lZ lZVar, int i) {
        long granny_grn_section_array_getitem = grannyJNI.granny_grn_section_array_getitem(C2897lZ.m35116a(lZVar), i);
        if (granny_grn_section_array_getitem == 0) {
            return null;
        }
        return new C4098zR(granny_grn_section_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4400a(C2897lZ lZVar, int i, C4098zR zRVar) {
        grannyJNI.granny_grn_section_array_setitem(C2897lZ.m35116a(lZVar), i, C4098zR.m41559a(zRVar));
    }

    /* renamed from: iQ */
    public static azS m4950iQ(int i) {
        long new_granny_local_pose_array = grannyJNI.new_granny_local_pose_array(i);
        if (new_granny_local_pose_array == 0) {
            return null;
        }
        return new azS(new_granny_local_pose_array, false);
    }

    /* renamed from: a */
    public static void m4376a(azS azs) {
        grannyJNI.delete_granny_local_pose_array(azS.m27666b(azs));
    }

    /* renamed from: a */
    public static aUB m3818a(azS azs, int i) {
        long granny_local_pose_array_getitem = grannyJNI.granny_local_pose_array_getitem(azS.m27666b(azs), i);
        if (granny_local_pose_array_getitem == 0) {
            return null;
        }
        return new aUB(granny_local_pose_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4377a(azS azs, int i, aUB aub) {
        grannyJNI.granny_local_pose_array_setitem(azS.m27666b(azs), i, aUB.m18499d(aub));
    }

    /* renamed from: iR */
    public static C3216pK m4951iR(int i) {
        long new_granny_log_callback_array = grannyJNI.new_granny_log_callback_array(i);
        if (new_granny_log_callback_array == 0) {
            return null;
        }
        return new C3216pK(new_granny_log_callback_array, false);
    }

    /* renamed from: b */
    public static void m4699b(C3216pK pKVar) {
        grannyJNI.delete_granny_log_callback_array(C3216pK.m37091a(pKVar));
    }

    /* renamed from: a */
    public static C6870avK m3882a(C3216pK pKVar, int i) {
        long granny_log_callback_array_getitem = grannyJNI.granny_log_callback_array_getitem(C3216pK.m37091a(pKVar), i);
        if (granny_log_callback_array_getitem == 0) {
            return null;
        }
        return new C6870avK(granny_log_callback_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4424a(C3216pK pKVar, int i, C6870avK avk) {
        grannyJNI.granny_log_callback_array_setitem(C3216pK.m37091a(pKVar), i, C6870avK.m26571c(avk));
    }

    /* renamed from: iS */
    public static aBS m4952iS(int i) {
        long new_granny_material_array = grannyJNI.new_granny_material_array(i);
        if (new_granny_material_array == 0) {
            return null;
        }
        return new aBS(new_granny_material_array, false);
    }

    /* renamed from: b */
    public static void m4633b(aBS abs) {
        grannyJNI.delete_granny_material_array(aBS.m12939c(abs));
    }

    /* renamed from: a */
    public static C0201CS m3713a(aBS abs, int i) {
        long granny_material_array_getitem = grannyJNI.granny_material_array_getitem(aBS.m12939c(abs), i);
        if (granny_material_array_getitem == 0) {
            return null;
        }
        return new C0201CS(granny_material_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4121a(aBS abs, int i, C0201CS cs) {
        grannyJNI.granny_material_array_setitem(aBS.m12939c(abs), i, C0201CS.m1752a(cs));
    }

    /* renamed from: iT */
    public static aOC m4953iT(int i) {
        long new_granny_material_binding_array = grannyJNI.new_granny_material_binding_array(i);
        if (new_granny_material_binding_array == 0) {
            return null;
        }
        return new aOC(new_granny_material_binding_array, false);
    }

    /* renamed from: a */
    public static void m4184a(aOC aoc) {
        grannyJNI.delete_granny_material_binding_array(aOC.m16800b(aoc));
    }

    /* renamed from: a */
    public static C7006ayt m3886a(aOC aoc, int i) {
        long granny_material_binding_array_getitem = grannyJNI.granny_material_binding_array_getitem(aOC.m16800b(aoc), i);
        if (granny_material_binding_array_getitem == 0) {
            return null;
        }
        return new C7006ayt(granny_material_binding_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4185a(aOC aoc, int i, C7006ayt ayt) {
        grannyJNI.granny_material_binding_array_setitem(aOC.m16800b(aoc), i, C7006ayt.m27495b(ayt));
    }

    /* renamed from: iU */
    public static C4028yJ m4954iU(int i) {
        long new_granny_material_map_array = grannyJNI.new_granny_material_map_array(i);
        if (new_granny_material_map_array == 0) {
            return null;
        }
        return new C4028yJ(new_granny_material_map_array, false);
    }

    /* renamed from: b */
    public static void m4714b(C4028yJ yJVar) {
        grannyJNI.delete_granny_material_map_array(C4028yJ.m41266a(yJVar));
    }

    /* renamed from: a */
    public static aBV m3775a(C4028yJ yJVar, int i) {
        long granny_material_map_array_getitem = grannyJNI.granny_material_map_array_getitem(C4028yJ.m41266a(yJVar), i);
        if (granny_material_map_array_getitem == 0) {
            return null;
        }
        return new aBV(granny_material_map_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4458a(C4028yJ yJVar, int i, aBV abv) {
        grannyJNI.granny_material_map_array_setitem(C4028yJ.m41266a(yJVar), i, aBV.m12973b(abv));
    }

    /* renamed from: iV */
    public static C5909acl m4955iV(int i) {
        long new_granny_memory_arena_array = grannyJNI.new_granny_memory_arena_array(i);
        if (new_granny_memory_arena_array == 0) {
            return null;
        }
        return new C5909acl(new_granny_memory_arena_array, false);
    }

    /* renamed from: a */
    public static void m4254a(C5909acl acl) {
        grannyJNI.delete_granny_memory_arena_array(C5909acl.m20580b(acl));
    }

    /* renamed from: a */
    public static C3854vp m3918a(C5909acl acl, int i) {
        long granny_memory_arena_array_getitem = grannyJNI.granny_memory_arena_array_getitem(C5909acl.m20580b(acl), i);
        if (granny_memory_arena_array_getitem == 0) {
            return null;
        }
        return new C3854vp(granny_memory_arena_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4255a(C5909acl acl, int i, C3854vp vpVar) {
        grannyJNI.granny_memory_arena_array_setitem(C5909acl.m20580b(acl), i, C3854vp.m40424a(vpVar));
    }

    /* renamed from: iW */
    public static C2591hG m4956iW(int i) {
        long new_granny_mesh_array = grannyJNI.new_granny_mesh_array(i);
        if (new_granny_mesh_array == 0) {
            return null;
        }
        return new C2591hG(new_granny_mesh_array, false);
    }

    /* renamed from: c */
    public static void m4775c(C2591hG hGVar) {
        grannyJNI.delete_granny_mesh_array(C2591hG.m32536b(hGVar));
    }

    /* renamed from: a */
    public static C1126QZ m3746a(C2591hG hGVar, int i) {
        long granny_mesh_array_getitem = grannyJNI.granny_mesh_array_getitem(C2591hG.m32536b(hGVar), i);
        if (granny_mesh_array_getitem == 0) {
            return null;
        }
        return new C1126QZ(granny_mesh_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4389a(C2591hG hGVar, int i, C1126QZ qz) {
        grannyJNI.granny_mesh_array_setitem(C2591hG.m32536b(hGVar), i, C1126QZ.m8909l(qz));
    }

    /* renamed from: iX */
    public static C6908avw m4957iX(int i) {
        long new_granny_mesh_binding_array = grannyJNI.new_granny_mesh_binding_array(i);
        if (new_granny_mesh_binding_array == 0) {
            return null;
        }
        return new C6908avw(new_granny_mesh_binding_array, false);
    }

    /* renamed from: a */
    public static void m4360a(C6908avw avw) {
        grannyJNI.delete_granny_mesh_binding_array(C6908avw.m26793b(avw));
    }

    /* renamed from: a */
    public static C6796ato m3881a(C6908avw avw, int i) {
        long granny_mesh_binding_array_getitem = grannyJNI.granny_mesh_binding_array_getitem(C6908avw.m26793b(avw), i);
        if (granny_mesh_binding_array_getitem == 0) {
            return null;
        }
        return new C6796ato(granny_mesh_binding_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4361a(C6908avw avw, int i, C6796ato ato) {
        grannyJNI.granny_mesh_binding_array_setitem(C6908avw.m26793b(avw), i, C6796ato.m26060i(ato));
    }

    /* renamed from: iY */
    public static C1508WC m4958iY(int i) {
        long new_granny_mesh_builder_array = grannyJNI.new_granny_mesh_builder_array(i);
        if (new_granny_mesh_builder_array == 0) {
            return null;
        }
        return new C1508WC(new_granny_mesh_builder_array, false);
    }

    /* renamed from: a */
    public static void m4103a(C1508WC wc) {
        grannyJNI.delete_granny_mesh_builder_array(C1508WC.m11015b(wc));
    }

    /* renamed from: a */
    public static C5610aPy m3807a(C1508WC wc, int i) {
        long granny_mesh_builder_array_getitem = grannyJNI.granny_mesh_builder_array_getitem(C1508WC.m11015b(wc), i);
        if (granny_mesh_builder_array_getitem == 0) {
            return null;
        }
        return new C5610aPy(granny_mesh_builder_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4104a(C1508WC wc, int i, C5610aPy apy) {
        grannyJNI.granny_mesh_builder_array_setitem(C1508WC.m11015b(wc), i, C5610aPy.m17402h(apy));
    }

    /* renamed from: iZ */
    public static aIV m4959iZ(int i) {
        long new_granny_mesh_deformer_array = grannyJNI.new_granny_mesh_deformer_array(i);
        if (new_granny_mesh_deformer_array == 0) {
            return null;
        }
        return new aIV(new_granny_mesh_deformer_array, false);
    }

    /* renamed from: a */
    public static void m4147a(aIV aiv) {
        grannyJNI.delete_granny_mesh_deformer_array(aIV.m15408b(aiv));
    }

    /* renamed from: a */
    public static C6604aqE m3868a(aIV aiv, int i) {
        long granny_mesh_deformer_array_getitem = grannyJNI.granny_mesh_deformer_array_getitem(aIV.m15408b(aiv), i);
        if (granny_mesh_deformer_array_getitem == 0) {
            return null;
        }
        return new C6604aqE(granny_mesh_deformer_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4148a(aIV aiv, int i, C6604aqE aqe) {
        grannyJNI.granny_mesh_deformer_array_setitem(aIV.m15408b(aiv), i, C6604aqE.m25068b(aqe));
    }

    /* renamed from: ja */
    public static aFX m5019ja(int i) {
        long new_granny_model_control_binding_array = grannyJNI.new_granny_model_control_binding_array(i);
        if (new_granny_model_control_binding_array == 0) {
            return null;
        }
        return new aFX(new_granny_model_control_binding_array, false);
    }

    /* renamed from: a */
    public static void m4131a(aFX afx) {
        grannyJNI.delete_granny_model_control_binding_array(aFX.m14561b(afx));
    }

    /* renamed from: a */
    public static C6595apv m3863a(aFX afx, int i) {
        long granny_model_control_binding_array_getitem = grannyJNI.granny_model_control_binding_array_getitem(aFX.m14561b(afx), i);
        if (granny_model_control_binding_array_getitem == 0) {
            return null;
        }
        return new C6595apv(granny_model_control_binding_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4132a(aFX afx, int i, C6595apv apv) {
        grannyJNI.granny_model_control_binding_array_setitem(aFX.m14561b(afx), i, C6595apv.m25028g(apv));
    }

    /* renamed from: jb */
    public static axQ m5020jb(int i) {
        long new_granny_model_instance_array = grannyJNI.new_granny_model_instance_array(i);
        if (new_granny_model_instance_array == 0) {
            return null;
        }
        return new axQ(new_granny_model_instance_array, false);
    }

    /* renamed from: a */
    public static void m4367a(axQ axq) {
        grannyJNI.delete_granny_model_instance_array(axQ.m27043b(axq));
    }

    /* renamed from: a */
    public static C6676arY m3870a(axQ axq, int i) {
        long granny_model_instance_array_getitem = grannyJNI.granny_model_instance_array_getitem(axQ.m27043b(axq), i);
        if (granny_model_instance_array_getitem == 0) {
            return null;
        }
        return new C6676arY(granny_model_instance_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4368a(axQ axq, int i, C6676arY ary) {
        grannyJNI.granny_model_instance_array_setitem(axQ.m27043b(axq), i, C6676arY.m25508i(ary));
    }

    /* renamed from: jc */
    public static C3953xJ m5021jc(int i) {
        long new_granny_model_mesh_binding_array = grannyJNI.new_granny_model_mesh_binding_array(i);
        if (new_granny_model_mesh_binding_array == 0) {
            return null;
        }
        return new C3953xJ(new_granny_model_mesh_binding_array, false);
    }

    /* renamed from: b */
    public static void m4713b(C3953xJ xJVar) {
        grannyJNI.delete_granny_model_mesh_binding_array(C3953xJ.m40884a(xJVar));
    }

    /* renamed from: a */
    public static aDP m3776a(C3953xJ xJVar, int i) {
        long granny_model_mesh_binding_array_getitem = grannyJNI.granny_model_mesh_binding_array_getitem(C3953xJ.m40884a(xJVar), i);
        if (granny_model_mesh_binding_array_getitem == 0) {
            return null;
        }
        return new aDP(granny_model_mesh_binding_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4457a(C3953xJ xJVar, int i, aDP adp) {
        grannyJNI.granny_model_mesh_binding_array_setitem(C3953xJ.m40884a(xJVar), i, aDP.m13596b(adp));
    }

    /* renamed from: jd */
    public static C1751Zs m5022jd(int i) {
        long new_granny_morph_target_array = grannyJNI.new_granny_morph_target_array(i);
        if (new_granny_morph_target_array == 0) {
            return null;
        }
        return new C1751Zs(new_granny_morph_target_array, false);
    }

    /* renamed from: a */
    public static void m4116a(C1751Zs zs) {
        grannyJNI.delete_granny_morph_target_array(C1751Zs.m12233b(zs));
    }

    /* renamed from: a */
    public static aMK m3795a(C1751Zs zs, int i) {
        long granny_morph_target_array_getitem = grannyJNI.granny_morph_target_array_getitem(C1751Zs.m12233b(zs), i);
        if (granny_morph_target_array_getitem == 0) {
            return null;
        }
        return new aMK(granny_morph_target_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4117a(C1751Zs zs, int i, aMK amk) {
        grannyJNI.granny_morph_target_array_setitem(C1751Zs.m12233b(zs), i, aMK.m16345b(amk));
    }

    /* renamed from: je */
    public static C5876acE m5023je(int i) {
        long new_granny_old_curve_array = grannyJNI.new_granny_old_curve_array(i);
        if (new_granny_old_curve_array == 0) {
            return null;
        }
        return new C5876acE(new_granny_old_curve_array, false);
    }

    /* renamed from: a */
    public static void m4252a(C5876acE ace) {
        grannyJNI.delete_granny_old_curve_array(C5876acE.m20234b(ace));
    }

    /* renamed from: a */
    public static C5562aOc m3803a(C5876acE ace, int i) {
        long granny_old_curve_array_getitem = grannyJNI.granny_old_curve_array_getitem(C5876acE.m20234b(ace), i);
        if (granny_old_curve_array_getitem == 0) {
            return null;
        }
        return new C5562aOc(granny_old_curve_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4253a(C5876acE ace, int i, C5562aOc aoc) {
        grannyJNI.granny_old_curve_array_setitem(C5876acE.m20234b(ace), i, C5562aOc.m16918a(aoc));
    }

    /* renamed from: jf */
    public static C6547aoz m5024jf(int i) {
        long new_granny_oodle1_state_array = grannyJNI.new_granny_oodle1_state_array(i);
        if (new_granny_oodle1_state_array == 0) {
            return null;
        }
        return new C6547aoz(new_granny_oodle1_state_array, false);
    }

    /* renamed from: a */
    public static void m4305a(C6547aoz aoz) {
        grannyJNI.delete_granny_oodle1_state_array(C6547aoz.m24739b(aoz));
    }

    /* renamed from: a */
    public static C3610su m3915a(C6547aoz aoz, int i) {
        long granny_oodle1_state_array_getitem = grannyJNI.granny_oodle1_state_array_getitem(C6547aoz.m24739b(aoz), i);
        if (granny_oodle1_state_array_getitem == 0) {
            return null;
        }
        return new C3610su(granny_oodle1_state_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4306a(C6547aoz aoz, int i, C3610su suVar) {
        grannyJNI.granny_oodle1_state_array_setitem(C6547aoz.m24739b(aoz), i, C3610su.m39206a(suVar));
    }

    /* renamed from: jg */
    public static C6854auu m5025jg(int i) {
        long new_granny_p3_vertex_array = grannyJNI.new_granny_p3_vertex_array(i);
        if (new_granny_p3_vertex_array == 0) {
            return null;
        }
        return new C6854auu(new_granny_p3_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4350a(C6854auu auu) {
        grannyJNI.delete_granny_p3_vertex_array(C6854auu.m26467b(auu));
    }

    /* renamed from: a */
    public static C3106nt m3901a(C6854auu auu, int i) {
        long granny_p3_vertex_array_getitem = grannyJNI.granny_p3_vertex_array_getitem(C6854auu.m26467b(auu), i);
        if (granny_p3_vertex_array_getitem == 0) {
            return null;
        }
        return new C3106nt(granny_p3_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4351a(C6854auu auu, int i, C3106nt ntVar) {
        grannyJNI.granny_p3_vertex_array_setitem(C6854auu.m26467b(auu), i, C3106nt.m36512a(ntVar));
    }

    /* renamed from: jh */
    public static C1321TL m5026jh(int i) {
        long new_granny_periodic_loop_array = grannyJNI.new_granny_periodic_loop_array(i);
        if (new_granny_periodic_loop_array == 0) {
            return null;
        }
        return new C1321TL(new_granny_periodic_loop_array, false);
    }

    /* renamed from: a */
    public static void m4068a(C1321TL tl) {
        grannyJNI.delete_granny_periodic_loop_array(C1321TL.m9938b(tl));
    }

    /* renamed from: a */
    public static C2720jA m3894a(C1321TL tl, int i) {
        long granny_periodic_loop_array_getitem = grannyJNI.granny_periodic_loop_array_getitem(C1321TL.m9938b(tl), i);
        if (granny_periodic_loop_array_getitem == 0) {
            return null;
        }
        return new C2720jA(granny_periodic_loop_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4069a(C1321TL tl, int i, C2720jA jAVar) {
        grannyJNI.granny_periodic_loop_array_setitem(C1321TL.m9938b(tl), i, C2720jA.m33633a(jAVar));
    }

    /* renamed from: ji */
    public static C6345alF m5027ji(int i) {
        long new_granny_pixel_layout_array = grannyJNI.new_granny_pixel_layout_array(i);
        if (new_granny_pixel_layout_array == 0) {
            return null;
        }
        return new C6345alF(new_granny_pixel_layout_array, false);
    }

    /* renamed from: a */
    public static void m4287a(C6345alF alf) {
        grannyJNI.delete_granny_pixel_layout_array(C6345alF.m23693b(alf));
    }

    /* renamed from: a */
    public static C0994Oe m3741a(C6345alF alf, int i) {
        long granny_pixel_layout_array_getitem = grannyJNI.granny_pixel_layout_array_getitem(C6345alF.m23693b(alf), i);
        if (granny_pixel_layout_array_getitem == 0) {
            return null;
        }
        return new C0994Oe(granny_pixel_layout_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4288a(C6345alF alf, int i, C0994Oe oe) {
        grannyJNI.granny_pixel_layout_array_setitem(C6345alF.m23693b(alf), i, C0994Oe.m8106p(oe));
    }

    /* renamed from: jj */
    public static C0792LT m5028jj(int i) {
        long new_granny_pn33_vertex_array = grannyJNI.new_granny_pn33_vertex_array(i);
        if (new_granny_pn33_vertex_array == 0) {
            return null;
        }
        return new C0792LT(new_granny_pn33_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4007a(C0792LT lt) {
        grannyJNI.delete_granny_pn33_vertex_array(C0792LT.m6782b(lt));
    }

    /* renamed from: a */
    public static aMB m3794a(C0792LT lt, int i) {
        long granny_pn33_vertex_array_getitem = grannyJNI.granny_pn33_vertex_array_getitem(C0792LT.m6782b(lt), i);
        if (granny_pn33_vertex_array_getitem == 0) {
            return null;
        }
        return new aMB(granny_pn33_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4008a(C0792LT lt, int i, aMB amb) {
        grannyJNI.granny_pn33_vertex_array_setitem(C0792LT.m6782b(lt), i, aMB.m16308a(amb));
    }

    /* renamed from: jk */
    public static C1222SB m5029jk(int i) {
        long new_granny_png333_vertex_array = grannyJNI.new_granny_png333_vertex_array(i);
        if (new_granny_png333_vertex_array == 0) {
            return null;
        }
        return new C1222SB(new_granny_png333_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4060a(C1222SB sb) {
        grannyJNI.delete_granny_png333_vertex_array(C1222SB.m9307b(sb));
    }

    /* renamed from: a */
    public static C3809vH m3916a(C1222SB sb, int i) {
        long granny_png333_vertex_array_getitem = grannyJNI.granny_png333_vertex_array_getitem(C1222SB.m9307b(sb), i);
        if (granny_png333_vertex_array_getitem == 0) {
            return null;
        }
        return new C3809vH(granny_png333_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4061a(C1222SB sb, int i, C3809vH vHVar) {
        grannyJNI.granny_png333_vertex_array_setitem(C1222SB.m9307b(sb), i, C3809vH.m40168a(vHVar));
    }

    /* renamed from: jl */
    public static C2654iC m5030jl(int i) {
        long new_granny_pngb3333_vertex_array = grannyJNI.new_granny_pngb3333_vertex_array(i);
        if (new_granny_pngb3333_vertex_array == 0) {
            return null;
        }
        return new C2654iC(new_granny_pngb3333_vertex_array, false);
    }

    /* renamed from: b */
    public static void m4675b(C2654iC iCVar) {
        grannyJNI.delete_granny_pngb3333_vertex_array(C2654iC.m33047a(iCVar));
    }

    /* renamed from: a */
    public static C5846aba m3833a(C2654iC iCVar, int i) {
        long granny_pngb3333_vertex_array_getitem = grannyJNI.granny_pngb3333_vertex_array_getitem(C2654iC.m33047a(iCVar), i);
        if (granny_pngb3333_vertex_array_getitem == 0) {
            return null;
        }
        return new C5846aba(granny_pngb3333_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4392a(C2654iC iCVar, int i, C5846aba aba) {
        grannyJNI.granny_pngb3333_vertex_array_setitem(C2654iC.m33047a(iCVar), i, C5846aba.m20017a(aba));
    }

    /* renamed from: jm */
    public static C0841MG m5031jm(int i) {
        long new_granny_pngbt33332_vertex_array = grannyJNI.new_granny_pngbt33332_vertex_array(i);
        if (new_granny_pngbt33332_vertex_array == 0) {
            return null;
        }
        return new C0841MG(new_granny_pngbt33332_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4011a(C0841MG mg) {
        grannyJNI.delete_granny_pngbt33332_vertex_array(C0841MG.m6957b(mg));
    }

    /* renamed from: a */
    public static C3939wy m3932a(C0841MG mg, int i) {
        long granny_pngbt33332_vertex_array_getitem = grannyJNI.granny_pngbt33332_vertex_array_getitem(C0841MG.m6957b(mg), i);
        if (granny_pngbt33332_vertex_array_getitem == 0) {
            return null;
        }
        return new C3939wy(granny_pngbt33332_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4012a(C0841MG mg, int i, C3939wy wyVar) {
        grannyJNI.granny_pngbt33332_vertex_array_setitem(C0841MG.m6957b(mg), i, C3939wy.m40764a(wyVar));
    }

    /* renamed from: jn */
    public static C0469GV m5032jn(int i) {
        long new_granny_pngbt33333_vertex_array = grannyJNI.new_granny_pngbt33333_vertex_array(i);
        if (new_granny_pngbt33333_vertex_array == 0) {
            return null;
        }
        return new C0469GV(new_granny_pngbt33333_vertex_array, false);
    }

    /* renamed from: b */
    public static void m4615b(C0469GV gv) {
        grannyJNI.delete_granny_pngbt33333_vertex_array(C0469GV.m3411a(gv));
    }

    /* renamed from: a */
    public static aOK m3801a(C0469GV gv, int i) {
        long granny_pngbt33333_vertex_array_getitem = grannyJNI.granny_pngbt33333_vertex_array_getitem(C0469GV.m3411a(gv), i);
        if (granny_pngbt33333_vertex_array_getitem == 0) {
            return null;
        }
        return new aOK(granny_pngbt33333_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m3988a(C0469GV gv, int i, aOK aok) {
        grannyJNI.granny_pngbt33333_vertex_array_setitem(C0469GV.m3411a(gv), i, aOK.m16808a(aok));
    }

    /* renamed from: jo */
    public static C6529aoh m5033jo(int i) {
        long new_granny_pngt3332_vertex_array = grannyJNI.new_granny_pngt3332_vertex_array(i);
        if (new_granny_pngt3332_vertex_array == 0) {
            return null;
        }
        return new C6529aoh(new_granny_pngt3332_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4303a(C6529aoh aoh) {
        grannyJNI.delete_granny_pngt3332_vertex_array(C6529aoh.m24676b(aoh));
    }

    /* renamed from: a */
    public static C5468aKm m3785a(C6529aoh aoh, int i) {
        long granny_pngt3332_vertex_array_getitem = grannyJNI.granny_pngt3332_vertex_array_getitem(C6529aoh.m24676b(aoh), i);
        if (granny_pngt3332_vertex_array_getitem == 0) {
            return null;
        }
        return new C5468aKm(granny_pngt3332_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4304a(C6529aoh aoh, int i, C5468aKm akm) {
        grannyJNI.granny_pngt3332_vertex_array_setitem(C6529aoh.m24676b(aoh), i, C5468aKm.m16050a(akm));
    }

    /* renamed from: jp */
    public static C1114QN m5034jp(int i) {
        long new_granny_pnt332_vertex_array = grannyJNI.new_granny_pnt332_vertex_array(i);
        if (new_granny_pnt332_vertex_array == 0) {
            return null;
        }
        return new C1114QN(new_granny_pnt332_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4051a(C1114QN qn) {
        grannyJNI.delete_granny_pnt332_vertex_array(C1114QN.m8839b(qn));
    }

    /* renamed from: a */
    public static C3153oQ m3904a(C1114QN qn, int i) {
        long granny_pnt332_vertex_array_getitem = grannyJNI.granny_pnt332_vertex_array_getitem(C1114QN.m8839b(qn), i);
        if (granny_pnt332_vertex_array_getitem == 0) {
            return null;
        }
        return new C3153oQ(granny_pnt332_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4052a(C1114QN qn, int i, C3153oQ oQVar) {
        grannyJNI.granny_pnt332_vertex_array_setitem(C1114QN.m8839b(qn), i, C3153oQ.m36716a(oQVar));
    }

    /* renamed from: jq */
    public static C6713asJ m5035jq(int i) {
        long new_granny_pnt333_vertex_array = grannyJNI.new_granny_pnt333_vertex_array(i);
        if (new_granny_pnt333_vertex_array == 0) {
            return null;
        }
        return new C6713asJ(new_granny_pnt333_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4329a(C6713asJ asj) {
        grannyJNI.delete_granny_pnt333_vertex_array(C6713asJ.m25651b(asj));
    }

    /* renamed from: a */
    public static C3832vX m3917a(C6713asJ asj, int i) {
        long granny_pnt333_vertex_array_getitem = grannyJNI.granny_pnt333_vertex_array_getitem(C6713asJ.m25651b(asj), i);
        if (granny_pnt333_vertex_array_getitem == 0) {
            return null;
        }
        return new C3832vX(granny_pnt333_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4330a(C6713asJ asj, int i, C3832vX vXVar) {
        grannyJNI.granny_pnt333_vertex_array_setitem(C6713asJ.m25651b(asj), i, C3832vX.m40277a(vXVar));
    }

    /* renamed from: jr */
    public static C6203aiT m5036jr(int i) {
        long new_granny_pntg3323_vertex_array = grannyJNI.new_granny_pntg3323_vertex_array(i);
        if (new_granny_pntg3323_vertex_array == 0) {
            return null;
        }
        return new C6203aiT(new_granny_pntg3323_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4276a(C6203aiT ait) {
        grannyJNI.delete_granny_pntg3323_vertex_array(C6203aiT.m22543b(ait));
    }

    /* renamed from: a */
    public static C0253DG m3718a(C6203aiT ait, int i) {
        long granny_pntg3323_vertex_array_getitem = grannyJNI.granny_pntg3323_vertex_array_getitem(C6203aiT.m22543b(ait), i);
        if (granny_pntg3323_vertex_array_getitem == 0) {
            return null;
        }
        return new C0253DG(granny_pntg3323_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4277a(C6203aiT ait, int i, C0253DG dg) {
        grannyJNI.granny_pntg3323_vertex_array_setitem(C6203aiT.m22543b(ait), i, C0253DG.m1952a(dg));
    }

    /* renamed from: js */
    public static C2643hw m5037js(int i) {
        long new_granny_pointer_hash_array = grannyJNI.new_granny_pointer_hash_array(i);
        if (new_granny_pointer_hash_array == 0) {
            return null;
        }
        return new C2643hw(new_granny_pointer_hash_array, false);
    }

    /* renamed from: b */
    public static void m4674b(C2643hw hwVar) {
        grannyJNI.delete_granny_pointer_hash_array(C2643hw.m33024a(hwVar));
    }

    /* renamed from: a */
    public static C6127agv m3844a(C2643hw hwVar, int i) {
        long granny_pointer_hash_array_getitem = grannyJNI.granny_pointer_hash_array_getitem(C2643hw.m33024a(hwVar), i);
        if (granny_pointer_hash_array_getitem == 0) {
            return null;
        }
        return new C6127agv(granny_pointer_hash_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4391a(C2643hw hwVar, int i, C6127agv agv) {
        grannyJNI.granny_pointer_hash_array_setitem(C2643hw.m33024a(hwVar), i, C6127agv.m22157b(agv));
    }

    /* renamed from: jt */
    public static azA m5038jt(int i) {
        long new_granny_pt32_vertex_array = grannyJNI.new_granny_pt32_vertex_array(i);
        if (new_granny_pt32_vertex_array == 0) {
            return null;
        }
        return new azA(new_granny_pt32_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4372a(azA aza) {
        grannyJNI.delete_granny_pt32_vertex_array(azA.m27560b(aza));
    }

    /* renamed from: a */
    public static C2362eV m3889a(azA aza, int i) {
        long granny_pt32_vertex_array_getitem = grannyJNI.granny_pt32_vertex_array_getitem(azA.m27560b(aza), i);
        if (granny_pt32_vertex_array_getitem == 0) {
            return null;
        }
        return new C2362eV(granny_pt32_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4373a(azA aza, int i, C2362eV eVVar) {
        grannyJNI.granny_pt32_vertex_array_setitem(azA.m27560b(aza), i, C2362eV.m29548a(eVVar));
    }

    /* renamed from: ju */
    public static C0991Ob m5039ju(int i) {
        long new_granny_pwn313_vertex_array = grannyJNI.new_granny_pwn313_vertex_array(i);
        if (new_granny_pwn313_vertex_array == 0) {
            return null;
        }
        return new C0991Ob(new_granny_pwn313_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4045a(C0991Ob ob) {
        grannyJNI.delete_granny_pwn313_vertex_array(C0991Ob.m8093b(ob));
    }

    /* renamed from: a */
    public static C3518rr m3914a(C0991Ob ob, int i) {
        long granny_pwn313_vertex_array_getitem = grannyJNI.granny_pwn313_vertex_array_getitem(C0991Ob.m8093b(ob), i);
        if (granny_pwn313_vertex_array_getitem == 0) {
            return null;
        }
        return new C3518rr(granny_pwn313_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4046a(C0991Ob ob, int i, C3518rr rrVar) {
        grannyJNI.granny_pwn313_vertex_array_setitem(C0991Ob.m8093b(ob), i, C3518rr.m38647a(rrVar));
    }

    /* renamed from: jv */
    public static C5834abO m5040jv(int i) {
        long new_granny_pwn323_vertex_array = grannyJNI.new_granny_pwn323_vertex_array(i);
        if (new_granny_pwn323_vertex_array == 0) {
            return null;
        }
        return new C5834abO(new_granny_pwn323_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4246a(C5834abO abo) {
        grannyJNI.delete_granny_pwn323_vertex_array(C5834abO.m19965b(abo));
    }

    /* renamed from: a */
    public static C1834aa m3823a(C5834abO abo, int i) {
        long granny_pwn323_vertex_array_getitem = grannyJNI.granny_pwn323_vertex_array_getitem(C5834abO.m19965b(abo), i);
        if (granny_pwn323_vertex_array_getitem == 0) {
            return null;
        }
        return new C1834aa(granny_pwn323_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4247a(C5834abO abo, int i, C1834aa aaVar) {
        grannyJNI.granny_pwn323_vertex_array_setitem(C5834abO.m19965b(abo), i, C1834aa.m19362a(aaVar));
    }

    /* renamed from: jw */
    public static C7026azn m5041jw(int i) {
        long new_granny_pwn343_vertex_array = grannyJNI.new_granny_pwn343_vertex_array(i);
        if (new_granny_pwn343_vertex_array == 0) {
            return null;
        }
        return new C7026azn(new_granny_pwn343_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4380a(C7026azn azn) {
        grannyJNI.delete_granny_pwn343_vertex_array(C7026azn.m27739b(azn));
    }

    /* renamed from: a */
    public static C1048PM m3743a(C7026azn azn, int i) {
        long granny_pwn343_vertex_array_getitem = grannyJNI.granny_pwn343_vertex_array_getitem(C7026azn.m27739b(azn), i);
        if (granny_pwn343_vertex_array_getitem == 0) {
            return null;
        }
        return new C1048PM(granny_pwn343_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4381a(C7026azn azn, int i, C1048PM pm) {
        grannyJNI.granny_pwn343_vertex_array_setitem(C7026azn.m27739b(azn), i, C1048PM.m8401a(pm));
    }

    /* renamed from: jx */
    public static C5548aNo m5042jx(int i) {
        long new_granny_pwng3133_vertex_array = grannyJNI.new_granny_pwng3133_vertex_array(i);
        if (new_granny_pwng3133_vertex_array == 0) {
            return null;
        }
        return new C5548aNo(new_granny_pwng3133_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4180a(C5548aNo ano) {
        grannyJNI.delete_granny_pwng3133_vertex_array(C5548aNo.m16775b(ano));
    }

    /* renamed from: a */
    public static C2032bK m3887a(C5548aNo ano, int i) {
        long granny_pwng3133_vertex_array_getitem = grannyJNI.granny_pwng3133_vertex_array_getitem(C5548aNo.m16775b(ano), i);
        if (granny_pwng3133_vertex_array_getitem == 0) {
            return null;
        }
        return new C2032bK(granny_pwng3133_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4181a(C5548aNo ano, int i, C2032bK bKVar) {
        grannyJNI.granny_pwng3133_vertex_array_setitem(C5548aNo.m16775b(ano), i, C2032bK.m27772a(bKVar));
    }

    /* renamed from: jy */
    public static aFC m5043jy(int i) {
        long new_granny_pwng3233_vertex_array = grannyJNI.new_granny_pwng3233_vertex_array(i);
        if (new_granny_pwng3233_vertex_array == 0) {
            return null;
        }
        return new aFC(new_granny_pwng3233_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4127a(aFC afc) {
        grannyJNI.delete_granny_pwng3233_vertex_array(aFC.m14492b(afc));
    }

    /* renamed from: a */
    public static C1178RR m3749a(aFC afc, int i) {
        long granny_pwng3233_vertex_array_getitem = grannyJNI.granny_pwng3233_vertex_array_getitem(aFC.m14492b(afc), i);
        if (granny_pwng3233_vertex_array_getitem == 0) {
            return null;
        }
        return new C1178RR(granny_pwng3233_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4128a(aFC afc, int i, C1178RR rr) {
        grannyJNI.granny_pwng3233_vertex_array_setitem(aFC.m14492b(afc), i, C1178RR.m9166a(rr));
    }

    /* renamed from: jz */
    public static aCT m5044jz(int i) {
        long new_granny_pwng3433_vertex_array = grannyJNI.new_granny_pwng3433_vertex_array(i);
        if (new_granny_pwng3433_vertex_array == 0) {
            return null;
        }
        return new aCT(new_granny_pwng3433_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4122a(aCT act) {
        grannyJNI.delete_granny_pwng3433_vertex_array(aCT.m13276b(act));
    }

    /* renamed from: a */
    public static C0975OM m3739a(aCT act, int i) {
        long granny_pwng3433_vertex_array_getitem = grannyJNI.granny_pwng3433_vertex_array_getitem(aCT.m13276b(act), i);
        if (granny_pwng3433_vertex_array_getitem == 0) {
            return null;
        }
        return new C0975OM(granny_pwng3433_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4123a(aCT act, int i, C0975OM om) {
        grannyJNI.granny_pwng3433_vertex_array_setitem(aCT.m13276b(act), i, C0975OM.m7958a(om));
    }

    /* renamed from: jA */
    public static C6465anV m4993jA(int i) {
        long new_granny_pwngb31333_vertex_array = grannyJNI.new_granny_pwngb31333_vertex_array(i);
        if (new_granny_pwngb31333_vertex_array == 0) {
            return null;
        }
        return new C6465anV(new_granny_pwngb31333_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4296a(C6465anV anv) {
        grannyJNI.delete_granny_pwngb31333_vertex_array(C6465anV.m24198b(anv));
    }

    /* renamed from: a */
    public static C6109agd m3841a(C6465anV anv, int i) {
        long granny_pwngb31333_vertex_array_getitem = grannyJNI.granny_pwngb31333_vertex_array_getitem(C6465anV.m24198b(anv), i);
        if (granny_pwngb31333_vertex_array_getitem == 0) {
            return null;
        }
        return new C6109agd(granny_pwngb31333_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4297a(C6465anV anv, int i, C6109agd agd) {
        grannyJNI.granny_pwngb31333_vertex_array_setitem(C6465anV.m24198b(anv), i, C6109agd.m22091a(agd));
    }

    /* renamed from: jB */
    public static C5668aSe m4994jB(int i) {
        long new_granny_pwngb32333_vertex_array = grannyJNI.new_granny_pwngb32333_vertex_array(i);
        if (new_granny_pwngb32333_vertex_array == 0) {
            return null;
        }
        return new C5668aSe(new_granny_pwngb32333_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4206a(C5668aSe ase) {
        grannyJNI.delete_granny_pwngb32333_vertex_array(C5668aSe.m18221b(ase));
    }

    /* renamed from: a */
    public static C4107za m3937a(C5668aSe ase, int i) {
        long granny_pwngb32333_vertex_array_getitem = grannyJNI.granny_pwngb32333_vertex_array_getitem(C5668aSe.m18221b(ase), i);
        if (granny_pwngb32333_vertex_array_getitem == 0) {
            return null;
        }
        return new C4107za(granny_pwngb32333_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4207a(C5668aSe ase, int i, C4107za zaVar) {
        grannyJNI.granny_pwngb32333_vertex_array_setitem(C5668aSe.m18221b(ase), i, C4107za.m41632a(zaVar));
    }

    /* renamed from: jC */
    public static C2819kZ m4995jC(int i) {
        long new_granny_pwngb34333_vertex_array = grannyJNI.new_granny_pwngb34333_vertex_array(i);
        if (new_granny_pwngb34333_vertex_array == 0) {
            return null;
        }
        return new C2819kZ(new_granny_pwngb34333_vertex_array, false);
    }

    /* renamed from: b */
    public static void m4680b(C2819kZ kZVar) {
        grannyJNI.delete_granny_pwngb34333_vertex_array(C2819kZ.m34453a(kZVar));
    }

    /* renamed from: a */
    public static C1441VD m3766a(C2819kZ kZVar, int i) {
        long granny_pwngb34333_vertex_array_getitem = grannyJNI.granny_pwngb34333_vertex_array_getitem(C2819kZ.m34453a(kZVar), i);
        if (granny_pwngb34333_vertex_array_getitem == 0) {
            return null;
        }
        return new C1441VD(granny_pwngb34333_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4397a(C2819kZ kZVar, int i, C1441VD vd) {
        grannyJNI.granny_pwngb34333_vertex_array_setitem(C2819kZ.m34453a(kZVar), i, C1441VD.m10454a(vd));
    }

    /* renamed from: jD */
    public static azV m4996jD(int i) {
        long new_granny_pwngbt313332_vertex_array = grannyJNI.new_granny_pwngbt313332_vertex_array(i);
        if (new_granny_pwngbt313332_vertex_array == 0) {
            return null;
        }
        return new azV(new_granny_pwngbt313332_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4378a(azV azv) {
        grannyJNI.delete_granny_pwngbt313332_vertex_array(azV.m27675b(azv));
    }

    /* renamed from: a */
    public static C6775atT m3877a(azV azv, int i) {
        long granny_pwngbt313332_vertex_array_getitem = grannyJNI.granny_pwngbt313332_vertex_array_getitem(azV.m27675b(azv), i);
        if (granny_pwngbt313332_vertex_array_getitem == 0) {
            return null;
        }
        return new C6775atT(granny_pwngbt313332_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4379a(azV azv, int i, C6775atT att) {
        grannyJNI.granny_pwngbt313332_vertex_array_setitem(azV.m27675b(azv), i, C6775atT.m25883a(att));
    }

    /* renamed from: jE */
    public static C6806aty m4997jE(int i) {
        long new_granny_pwngbt323332_vertex_array = grannyJNI.new_granny_pwngbt323332_vertex_array(i);
        if (new_granny_pwngbt323332_vertex_array == 0) {
            return null;
        }
        return new C6806aty(new_granny_pwngbt323332_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4348a(C6806aty aty) {
        grannyJNI.delete_granny_pwngbt323332_vertex_array(C6806aty.m26128b(aty));
    }

    /* renamed from: a */
    public static aRL m3808a(C6806aty aty, int i) {
        long granny_pwngbt323332_vertex_array_getitem = grannyJNI.granny_pwngbt323332_vertex_array_getitem(C6806aty.m26128b(aty), i);
        if (granny_pwngbt323332_vertex_array_getitem == 0) {
            return null;
        }
        return new aRL(granny_pwngbt323332_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4349a(C6806aty aty, int i, aRL arl) {
        grannyJNI.granny_pwngbt323332_vertex_array_setitem(C6806aty.m26128b(aty), i, aRL.m17735a(arl));
    }

    /* renamed from: jF */
    public static C0639JD m4998jF(int i) {
        long new_granny_pwngbt343332_vertex_array = grannyJNI.new_granny_pwngbt343332_vertex_array(i);
        if (new_granny_pwngbt343332_vertex_array == 0) {
            return null;
        }
        return new C0639JD(new_granny_pwngbt343332_vertex_array, false);
    }

    /* renamed from: a */
    public static void m3996a(C0639JD jd) {
        grannyJNI.delete_granny_pwngbt343332_vertex_array(C0639JD.m5534b(jd));
    }

    /* renamed from: a */
    public static C1747Zo m3773a(C0639JD jd, int i) {
        long granny_pwngbt343332_vertex_array_getitem = grannyJNI.granny_pwngbt343332_vertex_array_getitem(C0639JD.m5534b(jd), i);
        if (granny_pwngbt343332_vertex_array_getitem == 0) {
            return null;
        }
        return new C1747Zo(granny_pwngbt343332_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m3997a(C0639JD jd, int i, C1747Zo zo) {
        grannyJNI.granny_pwngbt343332_vertex_array_setitem(C0639JD.m5534b(jd), i, C1747Zo.m12214a(zo));
    }

    /* renamed from: jG */
    public static aIN m4999jG(int i) {
        long new_granny_pwngt31332_vertex_array = grannyJNI.new_granny_pwngt31332_vertex_array(i);
        if (new_granny_pwngt31332_vertex_array == 0) {
            return null;
        }
        return new aIN(new_granny_pwngt31332_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4145a(aIN ain) {
        grannyJNI.delete_granny_pwngt31332_vertex_array(aIN.m15402b(ain));
    }

    /* renamed from: a */
    public static C2685iY m3893a(aIN ain, int i) {
        long granny_pwngt31332_vertex_array_getitem = grannyJNI.granny_pwngt31332_vertex_array_getitem(aIN.m15402b(ain), i);
        if (granny_pwngt31332_vertex_array_getitem == 0) {
            return null;
        }
        return new C2685iY(granny_pwngt31332_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4146a(aIN ain, int i, C2685iY iYVar) {
        grannyJNI.granny_pwngt31332_vertex_array_setitem(aIN.m15402b(ain), i, C2685iY.m33412a(iYVar));
    }

    /* renamed from: jH */
    public static C5848abc m5000jH(int i) {
        long new_granny_pwngt32332_vertex_array = grannyJNI.new_granny_pwngt32332_vertex_array(i);
        if (new_granny_pwngt32332_vertex_array == 0) {
            return null;
        }
        return new C5848abc(new_granny_pwngt32332_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4248a(C5848abc abc) {
        grannyJNI.delete_granny_pwngt32332_vertex_array(C5848abc.m20046b(abc));
    }

    /* renamed from: a */
    public static aPO m3805a(C5848abc abc, int i) {
        long granny_pwngt32332_vertex_array_getitem = grannyJNI.granny_pwngt32332_vertex_array_getitem(C5848abc.m20046b(abc), i);
        if (granny_pwngt32332_vertex_array_getitem == 0) {
            return null;
        }
        return new aPO(granny_pwngt32332_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4249a(C5848abc abc, int i, aPO apo) {
        grannyJNI.granny_pwngt32332_vertex_array_setitem(C5848abc.m20046b(abc), i, aPO.m17165a(apo));
    }

    /* renamed from: jI */
    public static C6241ajF m5001jI(int i) {
        long new_granny_pwngt34332_vertex_array = grannyJNI.new_granny_pwngt34332_vertex_array(i);
        if (new_granny_pwngt34332_vertex_array == 0) {
            return null;
        }
        return new C6241ajF(new_granny_pwngt34332_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4281a(C6241ajF ajf) {
        grannyJNI.delete_granny_pwngt34332_vertex_array(C6241ajF.m22793b(ajf));
    }

    /* renamed from: a */
    public static C6471anb m3855a(C6241ajF ajf, int i) {
        long granny_pwngt34332_vertex_array_getitem = grannyJNI.granny_pwngt34332_vertex_array_getitem(C6241ajF.m22793b(ajf), i);
        if (granny_pwngt34332_vertex_array_getitem == 0) {
            return null;
        }
        return new C6471anb(granny_pwngt34332_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4282a(C6241ajF ajf, int i, C6471anb anb) {
        grannyJNI.granny_pwngt34332_vertex_array_setitem(C6241ajF.m22793b(ajf), i, C6471anb.m24206a(anb));
    }

    /* renamed from: jJ */
    public static C6599apz m5002jJ(int i) {
        long new_granny_pwnt3132_vertex_array = grannyJNI.new_granny_pwnt3132_vertex_array(i);
        if (new_granny_pwnt3132_vertex_array == 0) {
            return null;
        }
        return new C6599apz(new_granny_pwnt3132_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4314a(C6599apz apz) {
        grannyJNI.delete_granny_pwnt3132_vertex_array(C6599apz.m25040b(apz));
    }

    /* renamed from: a */
    public static C1155Qw m3747a(C6599apz apz, int i) {
        long granny_pwnt3132_vertex_array_getitem = grannyJNI.granny_pwnt3132_vertex_array_getitem(C6599apz.m25040b(apz), i);
        if (granny_pwnt3132_vertex_array_getitem == 0) {
            return null;
        }
        return new C1155Qw(granny_pwnt3132_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4315a(C6599apz apz, int i, C1155Qw qw) {
        grannyJNI.granny_pwnt3132_vertex_array_setitem(C6599apz.m25040b(apz), i, C1155Qw.m9043a(qw));
    }

    /* renamed from: jK */
    public static C1614Xd m5003jK(int i) {
        long new_granny_pwnt3232_vertex_array = grannyJNI.new_granny_pwnt3232_vertex_array(i);
        if (new_granny_pwnt3232_vertex_array == 0) {
            return null;
        }
        return new C1614Xd(new_granny_pwnt3232_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4105a(C1614Xd xd) {
        grannyJNI.delete_granny_pwnt3232_vertex_array(C1614Xd.m11541b(xd));
    }

    /* renamed from: a */
    public static C1683Yn m3770a(C1614Xd xd, int i) {
        long granny_pwnt3232_vertex_array_getitem = grannyJNI.granny_pwnt3232_vertex_array_getitem(C1614Xd.m11541b(xd), i);
        if (granny_pwnt3232_vertex_array_getitem == 0) {
            return null;
        }
        return new C1683Yn(granny_pwnt3232_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4106a(C1614Xd xd, int i, C1683Yn yn) {
        grannyJNI.granny_pwnt3232_vertex_array_setitem(C1614Xd.m11541b(xd), i, C1683Yn.m11938a(yn));
    }

    /* renamed from: jL */
    public static C6860avA m5004jL(int i) {
        long new_granny_pwnt3432_vertex_array = grannyJNI.new_granny_pwnt3432_vertex_array(i);
        if (new_granny_pwnt3432_vertex_array == 0) {
            return null;
        }
        return new C6860avA(new_granny_pwnt3432_vertex_array, false);
    }

    /* renamed from: a */
    public static void m4352a(C6860avA ava) {
        grannyJNI.delete_granny_pwnt3432_vertex_array(C6860avA.m26477b(ava));
    }

    /* renamed from: a */
    public static C5710aTu m3812a(C6860avA ava, int i) {
        long granny_pwnt3432_vertex_array_getitem = grannyJNI.granny_pwnt3432_vertex_array_getitem(C6860avA.m26477b(ava), i);
        if (granny_pwnt3432_vertex_array_getitem == 0) {
            return null;
        }
        return new C5710aTu(granny_pwnt3432_vertex_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4353a(C6860avA ava, int i, C5710aTu atu) {
        grannyJNI.granny_pwnt3432_vertex_array_setitem(C6860avA.m26477b(ava), i, C5710aTu.m18431a(atu));
    }

    /* renamed from: jM */
    public static aHP m5005jM(int i) {
        long new_granny_sample_context_array = grannyJNI.new_granny_sample_context_array(i);
        if (new_granny_sample_context_array == 0) {
            return null;
        }
        return new aHP(new_granny_sample_context_array, false);
    }

    /* renamed from: a */
    public static void m4137a(aHP ahp) {
        grannyJNI.delete_granny_sample_context_array(aHP.m15162b(ahp));
    }

    /* renamed from: a */
    public static C0979OP m3740a(aHP ahp, int i) {
        long granny_sample_context_array_getitem = grannyJNI.granny_sample_context_array_getitem(aHP.m15162b(ahp), i);
        if (granny_sample_context_array_getitem == 0) {
            return null;
        }
        return new C0979OP(granny_sample_context_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4138a(aHP ahp, int i, C0979OP op) {
        grannyJNI.granny_sample_context_array_setitem(aHP.m15162b(ahp), i, C0979OP.m8025a(op));
    }

    /* renamed from: jN */
    public static C4082zD m5006jN(int i) {
        long new_granny_skeleton_array = grannyJNI.new_granny_skeleton_array(i);
        if (new_granny_skeleton_array == 0) {
            return null;
        }
        return new C4082zD(new_granny_skeleton_array, false);
    }

    /* renamed from: c */
    public static void m4788c(C4082zD zDVar) {
        grannyJNI.delete_granny_skeleton_array(C4082zD.m41509b(zDVar));
    }

    /* renamed from: a */
    public static C0913NP m3735a(C4082zD zDVar, int i) {
        long granny_skeleton_array_getitem = grannyJNI.granny_skeleton_array_getitem(C4082zD.m41509b(zDVar), i);
        if (granny_skeleton_array_getitem == 0) {
            return null;
        }
        return new C0913NP(granny_skeleton_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4459a(C4082zD zDVar, int i, C0913NP np) {
        grannyJNI.granny_skeleton_array_setitem(C4082zD.m41509b(zDVar), i, C0913NP.m7619a(np));
    }

    /* renamed from: jO */
    public static C6053afZ m5007jO(int i) {
        long new_granny_skeleton_builder_array = grannyJNI.new_granny_skeleton_builder_array(i);
        if (new_granny_skeleton_builder_array == 0) {
            return null;
        }
        return new C6053afZ(new_granny_skeleton_builder_array, false);
    }

    /* renamed from: a */
    public static void m4261a(C6053afZ afz) {
        grannyJNI.delete_granny_skeleton_builder_array(C6053afZ.m21557b(afz));
    }

    /* renamed from: a */
    public static C1641YA m3769a(C6053afZ afz, int i) {
        long granny_skeleton_builder_array_getitem = grannyJNI.granny_skeleton_builder_array_getitem(C6053afZ.m21557b(afz), i);
        if (granny_skeleton_builder_array_getitem == 0) {
            return null;
        }
        return new C1641YA(granny_skeleton_builder_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4262a(C6053afZ afz, int i, C1641YA ya) {
        grannyJNI.granny_skeleton_builder_array_setitem(C6053afZ.m21557b(afz), i, C1641YA.m11784b(ya));
    }

    /* renamed from: jP */
    public static C2994mk m5008jP(int i) {
        long new_granny_stack_allocator_array = grannyJNI.new_granny_stack_allocator_array(i);
        if (new_granny_stack_allocator_array == 0) {
            return null;
        }
        return new C2994mk(new_granny_stack_allocator_array, false);
    }

    /* renamed from: b */
    public static void m4686b(C2994mk mkVar) {
        grannyJNI.delete_granny_stack_allocator_array(C2994mk.m35836a(mkVar));
    }

    /* renamed from: a */
    public static C0028AN m3709a(C2994mk mkVar, int i) {
        long granny_stack_allocator_array_getitem = grannyJNI.granny_stack_allocator_array_getitem(C2994mk.m35836a(mkVar), i);
        if (granny_stack_allocator_array_getitem == 0) {
            return null;
        }
        return new C0028AN(granny_stack_allocator_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4403a(C2994mk mkVar, int i, C0028AN an) {
        grannyJNI.granny_stack_allocator_array_setitem(C2994mk.m35836a(mkVar), i, C0028AN.m373a(an));
    }

    /* renamed from: jQ */
    public static C6113agh m5009jQ(int i) {
        long new_granny_stat_hud_array = grannyJNI.new_granny_stat_hud_array(i);
        if (new_granny_stat_hud_array == 0) {
            return null;
        }
        return new C6113agh(new_granny_stat_hud_array, false);
    }

    /* renamed from: a */
    public static void m4265a(C6113agh agh) {
        grannyJNI.delete_granny_stat_hud_array(C6113agh.m22104b(agh));
    }

    /* renamed from: a */
    public static C1157Qy m3748a(C6113agh agh, int i) {
        long granny_stat_hud_array_getitem = grannyJNI.granny_stat_hud_array_getitem(C6113agh.m22104b(agh), i);
        if (granny_stat_hud_array_getitem == 0) {
            return null;
        }
        return new C1157Qy(granny_stat_hud_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4266a(C6113agh agh, int i, C1157Qy qy) {
        grannyJNI.granny_stat_hud_array_setitem(C6113agh.m22104b(agh), i, C1157Qy.m9055c(qy));
    }

    /* renamed from: jR */
    public static C0226Cq m5010jR(int i) {
        long new_granny_stat_hud_alloc_point_array = grannyJNI.new_granny_stat_hud_alloc_point_array(i);
        if (new_granny_stat_hud_alloc_point_array == 0) {
            return null;
        }
        return new C0226Cq(new_granny_stat_hud_alloc_point_array, false);
    }

    /* renamed from: b */
    public static void m4608b(C0226Cq cq) {
        grannyJNI.delete_granny_stat_hud_alloc_point_array(C0226Cq.m1834a(cq));
    }

    /* renamed from: a */
    public static C1833aZ m3822a(C0226Cq cq, int i) {
        long granny_stat_hud_alloc_point_array_getitem = grannyJNI.granny_stat_hud_alloc_point_array_getitem(C0226Cq.m1834a(cq), i);
        if (granny_stat_hud_alloc_point_array_getitem == 0) {
            return null;
        }
        return new C1833aZ(granny_stat_hud_alloc_point_array_getitem, false);
    }

    /* renamed from: a */
    public static void m3973a(C0226Cq cq, int i, C1833aZ aZVar) {
        grannyJNI.granny_stat_hud_alloc_point_array_setitem(C0226Cq.m1834a(cq), i, C1833aZ.m19350a(aZVar));
    }

    /* renamed from: jS */
    public static C0684Jk m5011jS(int i) {
        long new_granny_stat_hud_animation_types_array = grannyJNI.new_granny_stat_hud_animation_types_array(i);
        if (new_granny_stat_hud_animation_types_array == 0) {
            return null;
        }
        return new C0684Jk(new_granny_stat_hud_animation_types_array, false);
    }

    /* renamed from: a */
    public static void m3998a(C0684Jk jk) {
        grannyJNI.delete_granny_stat_hud_animation_types_array(C0684Jk.m6006b(jk));
    }

    /* renamed from: a */
    public static aOZ m3802a(C0684Jk jk, int i) {
        long granny_stat_hud_animation_types_array_getitem = grannyJNI.granny_stat_hud_animation_types_array_getitem(C0684Jk.m6006b(jk), i);
        if (granny_stat_hud_animation_types_array_getitem == 0) {
            return null;
        }
        return new aOZ(granny_stat_hud_animation_types_array_getitem, false);
    }

    /* renamed from: a */
    public static void m3999a(C0684Jk jk, int i, aOZ aoz) {
        grannyJNI.granny_stat_hud_animation_types_array_setitem(C0684Jk.m6006b(jk), i, aOZ.m16912b(aoz));
    }

    /* renamed from: jT */
    public static C0935Nj m5012jT(int i) {
        long new_granny_stat_hud_footprint_array = grannyJNI.new_granny_stat_hud_footprint_array(i);
        if (new_granny_stat_hud_footprint_array == 0) {
            return null;
        }
        return new C0935Nj(new_granny_stat_hud_footprint_array, false);
    }

    /* renamed from: a */
    public static void m4032a(C0935Nj nj) {
        grannyJNI.delete_granny_stat_hud_footprint_array(C0935Nj.m7716b(nj));
    }

    /* renamed from: a */
    public static C3913wh m3924a(C0935Nj nj, int i) {
        long granny_stat_hud_footprint_array_getitem = grannyJNI.granny_stat_hud_footprint_array_getitem(C0935Nj.m7716b(nj), i);
        if (granny_stat_hud_footprint_array_getitem == 0) {
            return null;
        }
        return new C3913wh(granny_stat_hud_footprint_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4033a(C0935Nj nj, int i, C3913wh whVar) {
        grannyJNI.granny_stat_hud_footprint_array_setitem(C0935Nj.m7716b(nj), i, C3913wh.m40714a(whVar));
    }

    /* renamed from: jU */
    public static C6171ahn m5013jU(int i) {
        long new_granny_stat_hud_model_controls_array = grannyJNI.new_granny_stat_hud_model_controls_array(i);
        if (new_granny_stat_hud_model_controls_array == 0) {
            return null;
        }
        return new C6171ahn(new_granny_stat_hud_model_controls_array, false);
    }

    /* renamed from: a */
    public static void m4272a(C6171ahn ahn) {
        grannyJNI.delete_granny_stat_hud_model_controls_array(C6171ahn.m22303b(ahn));
    }

    /* renamed from: a */
    public static aNT m3798a(C6171ahn ahn, int i) {
        long granny_stat_hud_model_controls_array_getitem = grannyJNI.granny_stat_hud_model_controls_array_getitem(C6171ahn.m22303b(ahn), i);
        if (granny_stat_hud_model_controls_array_getitem == 0) {
            return null;
        }
        return new aNT(granny_stat_hud_model_controls_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4273a(C6171ahn ahn, int i, aNT ant) {
        grannyJNI.granny_stat_hud_model_controls_array_setitem(C6171ahn.m22303b(ahn), i, aNT.m16576b(ant));
    }

    /* renamed from: jV */
    public static C0528HR m5014jV(int i) {
        long new_granny_stat_hud_model_instances_array = grannyJNI.new_granny_stat_hud_model_instances_array(i);
        if (new_granny_stat_hud_model_instances_array == 0) {
            return null;
        }
        return new C0528HR(new_granny_stat_hud_model_instances_array, false);
    }

    /* renamed from: a */
    public static void m3990a(C0528HR hr) {
        grannyJNI.delete_granny_stat_hud_model_instances_array(C0528HR.m5149b(hr));
    }

    /* renamed from: a */
    public static C1075Pj m3744a(C0528HR hr, int i) {
        long granny_stat_hud_model_instances_array_getitem = grannyJNI.granny_stat_hud_model_instances_array_getitem(C0528HR.m5149b(hr), i);
        if (granny_stat_hud_model_instances_array_getitem == 0) {
            return null;
        }
        return new C1075Pj(granny_stat_hud_model_instances_array_getitem, false);
    }

    /* renamed from: a */
    public static void m3991a(C0528HR hr, int i, C1075Pj pj) {
        grannyJNI.granny_stat_hud_model_instances_array_setitem(C0528HR.m5149b(hr), i, C1075Pj.m8601a(pj));
    }

    /* renamed from: jW */
    public static C0059Ah m5015jW(int i) {
        long new_granny_stat_hud_perf_array = grannyJNI.new_granny_stat_hud_perf_array(i);
        if (new_granny_stat_hud_perf_array == 0) {
            return null;
        }
        return new C0059Ah(new_granny_stat_hud_perf_array, false);
    }

    /* renamed from: b */
    public static void m4605b(C0059Ah ah) {
        grannyJNI.delete_granny_stat_hud_perf_array(C0059Ah.m455a(ah));
    }

    /* renamed from: a */
    public static C6181ahx m3845a(C0059Ah ah, int i) {
        long granny_stat_hud_perf_array_getitem = grannyJNI.granny_stat_hud_perf_array_getitem(C0059Ah.m455a(ah), i);
        if (granny_stat_hud_perf_array_getitem == 0) {
            return null;
        }
        return new C6181ahx(granny_stat_hud_perf_array_getitem, false);
    }

    /* renamed from: a */
    public static void m3970a(C0059Ah ah, int i, C6181ahx ahx) {
        grannyJNI.granny_stat_hud_perf_array_setitem(C0059Ah.m455a(ah), i, C6181ahx.m22394b(ahx));
    }

    /* renamed from: jX */
    public static C5652aRo m5016jX(int i) {
        long new_granny_stat_hud_perf_point_array = grannyJNI.new_granny_stat_hud_perf_point_array(i);
        if (new_granny_stat_hud_perf_point_array == 0) {
            return null;
        }
        return new C5652aRo(new_granny_stat_hud_perf_point_array, false);
    }

    /* renamed from: a */
    public static void m4204a(C5652aRo aro) {
        grannyJNI.delete_granny_stat_hud_perf_point_array(C5652aRo.m17936b(aro));
    }

    /* renamed from: a */
    public static C0705KB m3729a(C5652aRo aro, int i) {
        long granny_stat_hud_perf_point_array_getitem = grannyJNI.granny_stat_hud_perf_point_array_getitem(C5652aRo.m17936b(aro), i);
        if (granny_stat_hud_perf_point_array_getitem == 0) {
            return null;
        }
        return new C0705KB(granny_stat_hud_perf_point_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4205a(C5652aRo aro, int i, C0705KB kb) {
        grannyJNI.granny_stat_hud_perf_point_array_setitem(C5652aRo.m17936b(aro), i, C0705KB.m6261a(kb));
    }

    /* renamed from: jY */
    public static C0741Ka m5017jY(int i) {
        long new_granny_string_database_array = grannyJNI.new_granny_string_database_array(i);
        if (new_granny_string_database_array == 0) {
            return null;
        }
        return new C0741Ka(new_granny_string_database_array, false);
    }

    /* renamed from: a */
    public static void m4004a(C0741Ka ka) {
        grannyJNI.delete_granny_string_database_array(C0741Ka.m6484b(ka));
    }

    /* renamed from: a */
    public static aPJ m3804a(C0741Ka ka, int i) {
        long granny_string_database_array_getitem = grannyJNI.granny_string_database_array_getitem(C0741Ka.m6484b(ka), i);
        if (granny_string_database_array_getitem == 0) {
            return null;
        }
        return new aPJ(granny_string_database_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4005a(C0741Ka ka, int i, aPJ apj) {
        grannyJNI.granny_string_database_array_setitem(C0741Ka.m6484b(ka), i, aPJ.m17153a(apj));
    }

    /* renamed from: jZ */
    public static C2719j m5018jZ(int i) {
        long new_granny_string_table_array = grannyJNI.new_granny_string_table_array(i);
        if (new_granny_string_table_array == 0) {
            return null;
        }
        return new C2719j(new_granny_string_table_array, false);
    }

    /* renamed from: b */
    public static void m4677b(C2719j jVar) {
        grannyJNI.delete_granny_string_table_array(C2719j.m33632a(jVar));
    }

    /* renamed from: a */
    public static C6795atn m3878a(C2719j jVar, int i) {
        long granny_string_table_array_getitem = grannyJNI.granny_string_table_array_getitem(C2719j.m33632a(jVar), i);
        if (granny_string_table_array_getitem == 0) {
            return null;
        }
        return new C6795atn(granny_string_table_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4394a(C2719j jVar, int i, C6795atn atn) {
        grannyJNI.granny_string_table_array_setitem(C2719j.m33632a(jVar), i, C6795atn.m26059c(atn));
    }

    /* renamed from: ka */
    public static C0708KE m5053ka(int i) {
        long new_granny_system_clock_array = grannyJNI.new_granny_system_clock_array(i);
        if (new_granny_system_clock_array == 0) {
            return null;
        }
        return new C0708KE(new_granny_system_clock_array, false);
    }

    /* renamed from: a */
    public static void m4002a(C0708KE ke) {
        grannyJNI.delete_granny_system_clock_array(C0708KE.m6269b(ke));
    }

    /* renamed from: a */
    public static C0938Nm m3736a(C0708KE ke, int i) {
        long granny_system_clock_array_getitem = grannyJNI.granny_system_clock_array_getitem(C0708KE.m6269b(ke), i);
        if (granny_system_clock_array_getitem == 0) {
            return null;
        }
        return new C0938Nm(granny_system_clock_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4003a(C0708KE ke, int i, C0938Nm nm) {
        grannyJNI.granny_system_clock_array_setitem(C0708KE.m6269b(ke), i, C0938Nm.m7718b(nm));
    }

    /* renamed from: kb */
    public static C1717ZP m5054kb(int i) {
        long new_granny_tangent_frame_array = grannyJNI.new_granny_tangent_frame_array(i);
        if (new_granny_tangent_frame_array == 0) {
            return null;
        }
        return new C1717ZP(new_granny_tangent_frame_array, false);
    }

    /* renamed from: a */
    public static void m4110a(C1717ZP zp) {
        grannyJNI.delete_granny_tangent_frame_array(C1717ZP.m12037b(zp));
    }

    /* renamed from: a */
    public static C1640Y m3768a(C1717ZP zp, int i) {
        long granny_tangent_frame_array_getitem = grannyJNI.granny_tangent_frame_array_getitem(C1717ZP.m12037b(zp), i);
        if (granny_tangent_frame_array_getitem == 0) {
            return null;
        }
        return new C1640Y(granny_tangent_frame_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4111a(C1717ZP zp, int i, C1640Y y) {
        grannyJNI.granny_tangent_frame_array_setitem(C1717ZP.m12037b(zp), i, C1640Y.m11776a(y));
    }

    /* renamed from: kc */
    public static C1445VH m5055kc(int i) {
        long new_granny_text_track_array = grannyJNI.new_granny_text_track_array(i);
        if (new_granny_text_track_array == 0) {
            return null;
        }
        return new C1445VH(new_granny_text_track_array, false);
    }

    /* renamed from: a */
    public static void m4101a(C1445VH vh) {
        grannyJNI.delete_granny_text_track_array(C1445VH.m10482b(vh));
    }

    /* renamed from: a */
    public static C2982ma m3898a(C1445VH vh, int i) {
        long granny_text_track_array_getitem = grannyJNI.granny_text_track_array_getitem(C1445VH.m10482b(vh), i);
        if (granny_text_track_array_getitem == 0) {
            return null;
        }
        return new C2982ma(granny_text_track_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4102a(C1445VH vh, int i, C2982ma maVar) {
        grannyJNI.granny_text_track_array_setitem(C1445VH.m10482b(vh), i, C2982ma.m35792a(maVar));
    }

    /* renamed from: kd */
    public static C0428Fw m5056kd(int i) {
        long new_granny_text_track_entry_array = grannyJNI.new_granny_text_track_entry_array(i);
        if (new_granny_text_track_entry_array == 0) {
            return null;
        }
        return new C0428Fw(new_granny_text_track_entry_array, false);
    }

    /* renamed from: b */
    public static void m4612b(C0428Fw fw) {
        grannyJNI.delete_granny_text_track_entry_array(C0428Fw.m3298a(fw));
    }

    /* renamed from: a */
    public static C5324aEy m3778a(C0428Fw fw, int i) {
        long granny_text_track_entry_array_getitem = grannyJNI.granny_text_track_entry_array_getitem(C0428Fw.m3298a(fw), i);
        if (granny_text_track_entry_array_getitem == 0) {
            return null;
        }
        return new C5324aEy(granny_text_track_entry_array_getitem, false);
    }

    /* renamed from: a */
    public static void m3977a(C0428Fw fw, int i, C5324aEy aey) {
        grannyJNI.granny_text_track_entry_array_setitem(C0428Fw.m3298a(fw), i, C5324aEy.m14469b(aey));
    }

    /* renamed from: ke */
    public static C6456anM m5057ke(int i) {
        long new_granny_texture_array = grannyJNI.new_granny_texture_array(i);
        if (new_granny_texture_array == 0) {
            return null;
        }
        return new C6456anM(new_granny_texture_array, false);
    }

    /* renamed from: b */
    public static void m4654b(C6456anM anm) {
        grannyJNI.delete_granny_texture_array(C6456anM.m24139c(anm));
    }

    /* renamed from: a */
    public static C5350aFy m3782a(C6456anM anm, int i) {
        long granny_texture_array_getitem = grannyJNI.granny_texture_array_getitem(C6456anM.m24139c(anm), i);
        if (granny_texture_array_getitem == 0) {
            return null;
        }
        return new C5350aFy(granny_texture_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4295a(C6456anM anm, int i, C5350aFy afy) {
        grannyJNI.granny_texture_array_setitem(C6456anM.m24139c(anm), i, C5350aFy.m14701c(afy));
    }

    /* renamed from: kf */
    public static C5369aGr m5058kf(int i) {
        long new_granny_texture_builder_array = grannyJNI.new_granny_texture_builder_array(i);
        if (new_granny_texture_builder_array == 0) {
            return null;
        }
        return new C5369aGr(new_granny_texture_builder_array, false);
    }

    /* renamed from: a */
    public static void m4135a(C5369aGr agr) {
        grannyJNI.delete_granny_texture_builder_array(C5369aGr.m15020b(agr));
    }

    /* renamed from: a */
    public static C1406Uc m3763a(C5369aGr agr, int i) {
        long granny_texture_builder_array_getitem = grannyJNI.granny_texture_builder_array_getitem(C5369aGr.m15020b(agr), i);
        if (granny_texture_builder_array_getitem == 0) {
            return null;
        }
        return new C1406Uc(granny_texture_builder_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4136a(C5369aGr agr, int i, C1406Uc uc) {
        grannyJNI.granny_texture_builder_array_setitem(C5369aGr.m15020b(agr), i, C1406Uc.m10294c(uc));
    }

    /* renamed from: kg */
    public static C6321akh m5059kg(int i) {
        long new_granny_texture_image_array = grannyJNI.new_granny_texture_image_array(i);
        if (new_granny_texture_image_array == 0) {
            return null;
        }
        return new C6321akh(new_granny_texture_image_array, false);
    }

    /* renamed from: a */
    public static void m4283a(C6321akh akh) {
        grannyJNI.delete_granny_texture_image_array(C6321akh.m23531b(akh));
    }

    /* renamed from: a */
    public static C2406ex m3890a(C6321akh akh, int i) {
        long granny_texture_image_array_getitem = grannyJNI.granny_texture_image_array_getitem(C6321akh.m23531b(akh), i);
        if (granny_texture_image_array_getitem == 0) {
            return null;
        }
        return new C2406ex(granny_texture_image_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4284a(C6321akh akh, int i, C2406ex exVar) {
        grannyJNI.granny_texture_image_array_setitem(C6321akh.m23531b(akh), i, C2406ex.m30145a(exVar));
    }

    /* renamed from: kh */
    public static aKG m5060kh(int i) {
        long new_granny_texture_mip_level_array = grannyJNI.new_granny_texture_mip_level_array(i);
        if (new_granny_texture_mip_level_array == 0) {
            return null;
        }
        return new aKG(new_granny_texture_mip_level_array, false);
    }

    /* renamed from: a */
    public static void m4151a(aKG akg) {
        grannyJNI.delete_granny_texture_mip_level_array(aKG.m15939b(akg));
    }

    /* renamed from: a */
    public static C3025n m3899a(aKG akg, int i) {
        long granny_texture_mip_level_array_getitem = grannyJNI.granny_texture_mip_level_array_getitem(aKG.m15939b(akg), i);
        if (granny_texture_mip_level_array_getitem == 0) {
            return null;
        }
        return new C3025n(granny_texture_mip_level_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4152a(aKG akg, int i, C3025n nVar) {
        grannyJNI.granny_texture_mip_level_array_setitem(aKG.m15939b(akg), i, C3025n.m35921a(nVar));
    }

    /* renamed from: ki */
    public static C2016az m5061ki(int i) {
        long new_granny_track_group_array = grannyJNI.new_granny_track_group_array(i);
        if (new_granny_track_group_array == 0) {
            return null;
        }
        return new C2016az(new_granny_track_group_array, false);
    }

    /* renamed from: c */
    public static void m4774c(C2016az azVar) {
        grannyJNI.delete_granny_track_group_array(C2016az.m27559a(azVar));
    }

    /* renamed from: a */
    public static C0970OH m3737a(C2016az azVar, int i) {
        long granny_track_group_array_getitem = grannyJNI.granny_track_group_array_getitem(C2016az.m27559a(azVar), i);
        if (granny_track_group_array_getitem == 0) {
            return null;
        }
        return new C0970OH(granny_track_group_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4371a(C2016az azVar, int i, C0970OH oh) {
        grannyJNI.granny_track_group_array_setitem(C2016az.m27559a(azVar), i, C0970OH.m7822e(oh));
    }

    /* renamed from: kj */
    public static C0893My m5062kj(int i) {
        long new_granny_track_group_builder_array = grannyJNI.new_granny_track_group_builder_array(i);
        if (new_granny_track_group_builder_array == 0) {
            return null;
        }
        return new C0893My(new_granny_track_group_builder_array, false);
    }

    /* renamed from: a */
    public static void m4015a(C0893My my) {
        grannyJNI.delete_granny_track_group_builder_array(C0893My.m7229b(my));
    }

    /* renamed from: a */
    public static C3895wQ m3921a(C0893My my, int i) {
        long granny_track_group_builder_array_getitem = grannyJNI.granny_track_group_builder_array_getitem(C0893My.m7229b(my), i);
        if (granny_track_group_builder_array_getitem == 0) {
            return null;
        }
        return new C3895wQ(granny_track_group_builder_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4016a(C0893My my, int i, C3895wQ wQVar) {
        grannyJNI.granny_track_group_builder_array_setitem(C0893My.m7229b(my), i, C3895wQ.m40636a(wQVar));
    }

    /* renamed from: kk */
    public static aHX m5063kk(int i) {
        long new_granny_track_group_sampler_array = grannyJNI.new_granny_track_group_sampler_array(i);
        if (new_granny_track_group_sampler_array == 0) {
            return null;
        }
        return new aHX(new_granny_track_group_sampler_array, false);
    }

    /* renamed from: a */
    public static void m4139a(aHX ahx) {
        grannyJNI.delete_granny_track_group_sampler_array(aHX.m15224b(ahx));
    }

    /* renamed from: a */
    public static C5469aKn m3786a(aHX ahx, int i) {
        long granny_track_group_sampler_array_getitem = grannyJNI.granny_track_group_sampler_array_getitem(aHX.m15224b(ahx), i);
        if (granny_track_group_sampler_array_getitem == 0) {
            return null;
        }
        return new C5469aKn(granny_track_group_sampler_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4140a(aHX ahx, int i, C5469aKn akn) {
        grannyJNI.granny_track_group_sampler_array_setitem(aHX.m15224b(ahx), i, C5469aKn.m16060c(akn));
    }

    /* renamed from: kl */
    public static C2977mW m5064kl(int i) {
        long new_granny_track_mask_array = grannyJNI.new_granny_track_mask_array(i);
        if (new_granny_track_mask_array == 0) {
            return null;
        }
        return new C2977mW(new_granny_track_mask_array, false);
    }

    /* renamed from: b */
    public static void m4684b(C2977mW mWVar) {
        grannyJNI.delete_granny_track_mask_array(C2977mW.m35783a(mWVar));
    }

    /* renamed from: a */
    public static C5789aaV m3826a(C2977mW mWVar, int i) {
        long granny_track_mask_array_getitem = grannyJNI.granny_track_mask_array_getitem(C2977mW.m35783a(mWVar), i);
        if (granny_track_mask_array_getitem == 0) {
            return null;
        }
        return new C5789aaV(granny_track_mask_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4401a(C2977mW mWVar, int i, C5789aaV aav) {
        grannyJNI.granny_track_mask_array_setitem(C2977mW.m35783a(mWVar), i, C5789aaV.m19495f(aav));
    }

    /* renamed from: km */
    public static aNF m5065km(int i) {
        long new_granny_transform_array = grannyJNI.new_granny_transform_array(i);
        if (new_granny_transform_array == 0) {
            return null;
        }
        return new aNF(new_granny_transform_array, false);
    }

    /* renamed from: a */
    public static void m4175a(aNF anf) {
        grannyJNI.delete_granny_transform_array(aNF.m16544b(anf));
    }

    /* renamed from: a */
    public static C5515aMh m3796a(aNF anf, int i) {
        long granny_transform_array_getitem = grannyJNI.granny_transform_array_getitem(aNF.m16544b(anf), i);
        if (granny_transform_array_getitem == 0) {
            return null;
        }
        return new C5515aMh(granny_transform_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4176a(aNF anf, int i, C5515aMh amh) {
        grannyJNI.granny_transform_array_setitem(aNF.m16544b(anf), i, C5515aMh.m16438f(amh));
    }

    /* renamed from: kn */
    public static C5803aaj m5066kn(int i) {
        long new_granny_transform_track_array = grannyJNI.new_granny_transform_track_array(i);
        if (new_granny_transform_track_array == 0) {
            return null;
        }
        return new C5803aaj(new_granny_transform_track_array, false);
    }

    /* renamed from: a */
    public static void m4240a(C5803aaj aaj) {
        grannyJNI.delete_granny_transform_track_array(C5803aaj.m19656b(aaj));
    }

    /* renamed from: a */
    public static C0777LF m3730a(C5803aaj aaj, int i) {
        long granny_transform_track_array_getitem = grannyJNI.granny_transform_track_array_getitem(C5803aaj.m19656b(aaj), i);
        if (granny_transform_track_array_getitem == 0) {
            return null;
        }
        return new C0777LF(granny_transform_track_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4241a(C5803aaj aaj, int i, C0777LF lf) {
        grannyJNI.granny_transform_track_array_setitem(C5803aaj.m19656b(aaj), i, C0777LF.m6656f(lf));
    }

    /* renamed from: ko */
    public static C2126cI m5067ko(int i) {
        long new_granny_tri_annotation_set_array = grannyJNI.new_granny_tri_annotation_set_array(i);
        if (new_granny_tri_annotation_set_array == 0) {
            return null;
        }
        return new C2126cI(new_granny_tri_annotation_set_array, false);
    }

    /* renamed from: b */
    public static void m4667b(C2126cI cIVar) {
        grannyJNI.delete_granny_tri_annotation_set_array(C2126cI.m28277a(cIVar));
    }

    /* renamed from: a */
    public static C0370Ew m3720a(C2126cI cIVar, int i) {
        long granny_tri_annotation_set_array_getitem = grannyJNI.granny_tri_annotation_set_array_getitem(C2126cI.m28277a(cIVar), i);
        if (granny_tri_annotation_set_array_getitem == 0) {
            return null;
        }
        return new C0370Ew(granny_tri_annotation_set_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4382a(C2126cI cIVar, int i, C0370Ew ew) {
        grannyJNI.granny_tri_annotation_set_array_setitem(C2126cI.m28277a(cIVar), i, C0370Ew.m3111a(ew));
    }

    /* renamed from: kp */
    public static C2670iM m5068kp(int i) {
        long new_granny_tri_material_group_array = grannyJNI.new_granny_tri_material_group_array(i);
        if (new_granny_tri_material_group_array == 0) {
            return null;
        }
        return new C2670iM(new_granny_tri_material_group_array, false);
    }

    /* renamed from: b */
    public static void m4676b(C2670iM iMVar) {
        grannyJNI.delete_granny_tri_material_group_array(C2670iM.m33106a(iMVar));
    }

    /* renamed from: a */
    public static C3152oP m3903a(C2670iM iMVar, int i) {
        long granny_tri_material_group_array_getitem = grannyJNI.granny_tri_material_group_array_getitem(C2670iM.m33106a(iMVar), i);
        if (granny_tri_material_group_array_getitem == 0) {
            return null;
        }
        return new C3152oP(granny_tri_material_group_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4393a(C2670iM iMVar, int i, C3152oP oPVar) {
        grannyJNI.granny_tri_material_group_array_setitem(C2670iM.m33106a(iMVar), i, C3152oP.m36708a(oPVar));
    }

    /* renamed from: kq */
    public static C6658arG m5069kq(int i) {
        long new_granny_tri_topology_array = grannyJNI.new_granny_tri_topology_array(i);
        if (new_granny_tri_topology_array == 0) {
            return null;
        }
        return new C6658arG(new_granny_tri_topology_array, false);
    }

    /* renamed from: b */
    public static void m4658b(C6658arG arg) {
        grannyJNI.delete_granny_tri_topology_array(C6658arG.m25380c(arg));
    }

    /* renamed from: a */
    public static C0580IB m3727a(C6658arG arg, int i) {
        long granny_tri_topology_array_getitem = grannyJNI.granny_tri_topology_array_getitem(C6658arG.m25380c(arg), i);
        if (granny_tri_topology_array_getitem == 0) {
            return null;
        }
        return new C0580IB(granny_tri_topology_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4318a(C6658arG arg, int i, C0580IB ib) {
        grannyJNI.granny_tri_topology_array_setitem(C6658arG.m25380c(arg), i, C0580IB.m5303b(ib));
    }

    /* renamed from: kr */
    public static C6994ayh m5070kr(int i) {
        long new_granny_triangle_intersection_array = grannyJNI.new_granny_triangle_intersection_array(i);
        if (new_granny_triangle_intersection_array == 0) {
            return null;
        }
        return new C6994ayh(new_granny_triangle_intersection_array, false);
    }

    /* renamed from: a */
    public static void m4369a(C6994ayh ayh) {
        grannyJNI.delete_granny_triangle_intersection_array(C6994ayh.m27479b(ayh));
    }

    /* renamed from: a */
    public static C0206CX m3714a(C6994ayh ayh, int i) {
        long granny_triangle_intersection_array_getitem = grannyJNI.granny_triangle_intersection_array_getitem(C6994ayh.m27479b(ayh), i);
        if (granny_triangle_intersection_array_getitem == 0) {
            return null;
        }
        return new C0206CX(granny_triangle_intersection_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4370a(C6994ayh ayh, int i, C0206CX cx) {
        grannyJNI.granny_triangle_intersection_array_setitem(C6994ayh.m27479b(ayh), i, C0206CX.m1802a(cx));
    }

    /* renamed from: ks */
    public static C3421rO m5071ks(int i) {
        long new_granny_unbound_track_mask_array = grannyJNI.new_granny_unbound_track_mask_array(i);
        if (new_granny_unbound_track_mask_array == 0) {
            return null;
        }
        return new C3421rO(new_granny_unbound_track_mask_array, false);
    }

    /* renamed from: b */
    public static void m4700b(C3421rO rOVar) {
        grannyJNI.delete_granny_unbound_track_mask_array(C3421rO.m38089a(rOVar));
    }

    /* renamed from: a */
    public static C6590apq m3862a(C3421rO rOVar, int i) {
        long granny_unbound_track_mask_array_getitem = grannyJNI.granny_unbound_track_mask_array_getitem(C3421rO.m38089a(rOVar), i);
        if (granny_unbound_track_mask_array_getitem == 0) {
            return null;
        }
        return new C6590apq(granny_unbound_track_mask_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4429a(C3421rO rOVar, int i, C6590apq apq) {
        grannyJNI.granny_unbound_track_mask_array_setitem(C3421rO.m38089a(rOVar), i, C6590apq.m25013a(apq));
    }

    /* renamed from: kt */
    public static C1382UG m5072kt(int i) {
        long new_granny_unbound_weight_array = grannyJNI.new_granny_unbound_weight_array(i);
        if (new_granny_unbound_weight_array == 0) {
            return null;
        }
        return new C1382UG(new_granny_unbound_weight_array, false);
    }

    /* renamed from: a */
    public static void m4083a(C1382UG ug) {
        grannyJNI.delete_granny_unbound_weight_array(C1382UG.m10150b(ug));
    }

    /* renamed from: a */
    public static C6059aff m3837a(C1382UG ug, int i) {
        long granny_unbound_weight_array_getitem = grannyJNI.granny_unbound_weight_array_getitem(C1382UG.m10150b(ug), i);
        if (granny_unbound_weight_array_getitem == 0) {
            return null;
        }
        return new C6059aff(granny_unbound_weight_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4084a(C1382UG ug, int i, C6059aff aff) {
        grannyJNI.granny_unbound_weight_array_setitem(C1382UG.m10150b(ug), i, C6059aff.m21578a(aff));
    }

    /* renamed from: ku */
    public static C6176ahs m5073ku(int i) {
        long new_granny_variant_array = grannyJNI.new_granny_variant_array(i);
        if (new_granny_variant_array == 0) {
            return null;
        }
        return new C6176ahs(new_granny_variant_array, false);
    }

    /* renamed from: a */
    public static void m4274a(C6176ahs ahs) {
        grannyJNI.delete_granny_variant_array(C6176ahs.m22391b(ahs));
    }

    /* renamed from: a */
    public static aSM m3811a(C6176ahs ahs, int i) {
        long granny_variant_array_getitem = grannyJNI.granny_variant_array_getitem(C6176ahs.m22391b(ahs), i);
        if (granny_variant_array_getitem == 0) {
            return null;
        }
        return new aSM(granny_variant_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4275a(C6176ahs ahs, int i, aSM asm) {
        grannyJNI.granny_variant_array_setitem(C6176ahs.m22391b(ahs), i, aSM.m18172c(asm));
    }

    /* renamed from: kv */
    public static C6502aoG m5074kv(int i) {
        long new_granny_variant_builder_array = grannyJNI.new_granny_variant_builder_array(i);
        if (new_granny_variant_builder_array == 0) {
            return null;
        }
        return new C6502aoG(new_granny_variant_builder_array, false);
    }

    /* renamed from: a */
    public static void m4300a(C6502aoG aog) {
        grannyJNI.delete_granny_variant_builder_array(C6502aoG.m24370b(aog));
    }

    /* renamed from: a */
    public static C0429Fx m3722a(C6502aoG aog, int i) {
        long granny_variant_builder_array_getitem = grannyJNI.granny_variant_builder_array_getitem(C6502aoG.m24370b(aog), i);
        if (granny_variant_builder_array_getitem == 0) {
            return null;
        }
        return new C0429Fx(granny_variant_builder_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4301a(C6502aoG aog, int i, C0429Fx fx) {
        grannyJNI.granny_variant_builder_array_setitem(C6502aoG.m24370b(aog), i, C0429Fx.m3299a(fx));
    }

    /* renamed from: kw */
    public static C0818Lp m5075kw(int i) {
        long new_granny_vector_track_array = grannyJNI.new_granny_vector_track_array(i);
        if (new_granny_vector_track_array == 0) {
            return null;
        }
        return new C0818Lp(new_granny_vector_track_array, false);
    }

    /* renamed from: a */
    public static void m4009a(C0818Lp lp) {
        grannyJNI.delete_granny_vector_track_array(C0818Lp.m6865b(lp));
    }

    /* renamed from: a */
    public static C6766atK m3876a(C0818Lp lp, int i) {
        long granny_vector_track_array_getitem = grannyJNI.granny_vector_track_array_getitem(C0818Lp.m6865b(lp), i);
        if (granny_vector_track_array_getitem == 0) {
            return null;
        }
        return new C6766atK(granny_vector_track_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4010a(C0818Lp lp, int i, C6766atK atk) {
        grannyJNI.granny_vector_track_array_setitem(C0818Lp.m6865b(lp), i, C6766atK.m25835b(atk));
    }

    /* renamed from: kx */
    public static C2178cW m5076kx(int i) {
        long new_granny_vertex_annotation_set_array = grannyJNI.new_granny_vertex_annotation_set_array(i);
        if (new_granny_vertex_annotation_set_array == 0) {
            return null;
        }
        return new C2178cW(new_granny_vertex_annotation_set_array, false);
    }

    /* renamed from: b */
    public static void m4668b(C2178cW cWVar) {
        grannyJNI.delete_granny_vertex_annotation_set_array(C2178cW.m28361a(cWVar));
    }

    /* renamed from: a */
    public static aSA m3810a(C2178cW cWVar, int i) {
        long granny_vertex_annotation_set_array_getitem = grannyJNI.granny_vertex_annotation_set_array_getitem(C2178cW.m28361a(cWVar), i);
        if (granny_vertex_annotation_set_array_getitem == 0) {
            return null;
        }
        return new aSA(granny_vertex_annotation_set_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4383a(C2178cW cWVar, int i, aSA asa) {
        grannyJNI.granny_vertex_annotation_set_array_setitem(C2178cW.m28361a(cWVar), i, aSA.m18148b(asa));
    }

    /* renamed from: ky */
    public static C0438GF m5077ky(int i) {
        long new_granny_vertex_data_array = grannyJNI.new_granny_vertex_data_array(i);
        if (new_granny_vertex_data_array == 0) {
            return null;
        }
        return new C0438GF(new_granny_vertex_data_array, false);
    }

    /* renamed from: c */
    public static void m4753c(C0438GF gf) {
        grannyJNI.delete_granny_vertex_data_array(C0438GF.m3342b(gf));
    }

    /* renamed from: a */
    public static C0251DE m3717a(C0438GF gf, int i) {
        long granny_vertex_data_array_getitem = grannyJNI.granny_vertex_data_array_getitem(C0438GF.m3342b(gf), i);
        if (granny_vertex_data_array_getitem == 0) {
            return null;
        }
        return new C0251DE(granny_vertex_data_array_getitem, false);
    }

    /* renamed from: a */
    public static void m3987a(C0438GF gf, int i, C0251DE de) {
        grannyJNI.granny_vertex_data_array_setitem(C0438GF.m3342b(gf), i, C0251DE.m1941a(de));
    }

    /* renamed from: kz */
    public static C3855vq m5078kz(int i) {
        long new_granny_vertex_weight_arrays_array = grannyJNI.new_granny_vertex_weight_arrays_array(i);
        if (new_granny_vertex_weight_arrays_array == 0) {
            return null;
        }
        return new C3855vq(new_granny_vertex_weight_arrays_array, false);
    }

    /* renamed from: b */
    public static void m4705b(C3855vq vqVar) {
        grannyJNI.delete_granny_vertex_weight_arrays_array(C3855vq.m40425a(vqVar));
    }

    /* renamed from: a */
    public static C6289akB m3849a(C3855vq vqVar, int i) {
        long granny_vertex_weight_arrays_array_getitem = grannyJNI.granny_vertex_weight_arrays_array_getitem(C3855vq.m40425a(vqVar), i);
        if (granny_vertex_weight_arrays_array_getitem == 0) {
            return null;
        }
        return new C6289akB(granny_vertex_weight_arrays_array_getitem, false);
    }

    /* renamed from: a */
    public static void m4439a(C3855vq vqVar, int i, C6289akB akb) {
        grannyJNI.granny_vertex_weight_arrays_array_setitem(C3855vq.m40425a(vqVar), i, C6289akB.m23112a(akb));
    }

    /* renamed from: kA */
    public static C0317EI m5052kA(int i) {
        long new_granny_world_pose_array = grannyJNI.new_granny_world_pose_array(i);
        if (new_granny_world_pose_array == 0) {
            return null;
        }
        return new C0317EI(new_granny_world_pose_array, false);
    }

    /* renamed from: b */
    public static void m4611b(C0317EI ei) {
        grannyJNI.delete_granny_world_pose_array(C0317EI.m2800a(ei));
    }

    /* renamed from: a */
    public static C3894wP m3920a(C0317EI ei, int i) {
        long granny_world_pose_array_getitem = grannyJNI.granny_world_pose_array_getitem(C0317EI.m2800a(ei), i);
        if (granny_world_pose_array_getitem == 0) {
            return null;
        }
        return new C3894wP(granny_world_pose_array_getitem, false);
    }

    /* renamed from: a */
    public static void m3976a(C0317EI ei, int i, C3894wP wPVar) {
        grannyJNI.granny_world_pose_array_setitem(C0317EI.m2800a(ei), i, C3894wP.m40635a(wPVar));
    }


}
