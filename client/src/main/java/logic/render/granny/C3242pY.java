package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.pY */
/* compiled from: a */
public class C3242pY {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3242pY(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3242pY() {
        this(grannyJNI.new_granny_animation(), true);
    }

    /* renamed from: b */
    public static long m37170b(C3242pY pYVar) {
        if (pYVar == null) {
            return 0;
        }
        return pYVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_animation(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getName() {
        return grannyJNI.granny_animation_Name_get(this.swigCPtr);
    }

    public void setName(String str) {
        grannyJNI.granny_animation_Name_set(this.swigCPtr, str);
    }

    /* renamed from: bc */
    public void mo21179bc(float f) {
        grannyJNI.granny_animation_Duration_set(this.swigCPtr, f);
    }

    /* renamed from: jt */
    public float mo21186jt() {
        return grannyJNI.granny_animation_Duration_get(this.swigCPtr);
    }

    /* renamed from: cS */
    public void mo21180cS(float f) {
        grannyJNI.granny_animation_TimeStep_set(this.swigCPtr, f);
    }

    /* renamed from: WE */
    public float mo21175WE() {
        return grannyJNI.granny_animation_TimeStep_get(this.swigCPtr);
    }

    /* renamed from: cT */
    public void mo21181cT(float f) {
        grannyJNI.granny_animation_Oversampling_set(this.swigCPtr, f);
    }

    /* renamed from: WF */
    public float mo21176WF() {
        return grannyJNI.granny_animation_Oversampling_get(this.swigCPtr);
    }

    /* renamed from: an */
    public void mo21177an(int i) {
        grannyJNI.granny_animation_TrackGroupCount_set(this.swigCPtr, i);
    }

    /* renamed from: kI */
    public int mo21187kI() {
        return grannyJNI.granny_animation_TrackGroupCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo21178b(C2016az azVar) {
        grannyJNI.granny_animation_TrackGroups_set(this.swigCPtr, C2016az.m27559a(azVar));
    }

    /* renamed from: kJ */
    public C2016az mo21188kJ() {
        long granny_animation_TrackGroups_get = grannyJNI.granny_animation_TrackGroups_get(this.swigCPtr);
        if (granny_animation_TrackGroups_get == 0) {
            return null;
        }
        return new C2016az(granny_animation_TrackGroups_get, false);
    }

    /* renamed from: dP */
    public C3242pY mo21182dP(int i) {
        long granny_animation_get = grannyJNI.granny_animation_get(this.swigCPtr, i);
        if (granny_animation_get == 0) {
            return null;
        }
        return new C3242pY(granny_animation_get, false);
    }
}
