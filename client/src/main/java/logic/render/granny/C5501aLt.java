package logic.render.granny;

/* renamed from: a.aLt  reason: case insensitive filesystem */
/* compiled from: a */
public class C5501aLt {
    private long swigCPtr;

    public C5501aLt(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5501aLt() {
        this.swigCPtr = 0;
    }

    /* renamed from: h */
    public static long m16286h(C5501aLt alt) {
        if (alt == null) {
            return 0;
        }
        return alt.swigCPtr;
    }
}
