package logic.render.granny;

/* renamed from: a.atr  reason: case insensitive filesystem */
/* compiled from: a */
public class C6799atr {
    private long swigCPtr;

    public C6799atr(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6799atr() {
        this.swigCPtr = 0;
    }

    /* renamed from: e */
    public static long m26063e(C6799atr atr) {
        if (atr == null) {
            return 0;
        }
        return atr.swigCPtr;
    }
}
