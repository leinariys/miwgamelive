package logic.render.granny;

import logic.render.granny.dell.C1418Um;
import logic.render.granny.dell.C5269aCv;
import logic.render.granny.dell.C5827abH;
import logic.render.granny.dell.C5974ady;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.amE  reason: case insensitive filesystem */
/* compiled from: a */
public class C6396amE {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6396amE(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6396amE() {
        this(grannyJNI.new_granny_file(), true);
    }

    /* renamed from: g */
    public static long m23854g(C6396amE ame) {
        if (ame == null) {
            return 0;
        }
        return ame.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_file(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: fi */
    public void mo14802fi(boolean z) {
        grannyJNI.granny_file_IsByteReversed_set(this.swigCPtr, z);
    }

    public boolean cjK() {
        return grannyJNI.granny_file_IsByteReversed_get(this.swigCPtr);
    }

    /* renamed from: c */
    public void mo14791c(C5827abH abh) {
        grannyJNI.granny_file_Header_set(this.swigCPtr, C5827abH.m19928b(abh));
    }

    public C5827abH cjL() {
        long granny_file_Header_get = grannyJNI.granny_file_Header_get(this.swigCPtr);
        if (granny_file_Header_get == 0) {
            return null;
        }
        return new C5827abH(granny_file_Header_get, false);
    }

    /* renamed from: h */
    public void mo14804h(C5974ady ady) {
        grannyJNI.granny_file_SourceMagicValue_set(this.swigCPtr, C5974ady.m21102g(ady));
    }

    public C5974ady cjM() {
        long granny_file_SourceMagicValue_get = grannyJNI.granny_file_SourceMagicValue_get(this.swigCPtr);
        if (granny_file_SourceMagicValue_get == 0) {
            return null;
        }
        return new C5974ady(granny_file_SourceMagicValue_get, false);
    }

    /* renamed from: sB */
    public void mo14805sB(int i) {
        grannyJNI.granny_file_SectionCount_set(this.swigCPtr, i);
    }

    public int cjN() {
        return grannyJNI.granny_file_SectionCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo14788a(C5269aCv acv) {
        grannyJNI.granny_file_Sections_set(this.swigCPtr, C5269aCv.m13385b(acv));
    }

    public C5269aCv cjO() {
        long granny_file_Sections_get = grannyJNI.granny_file_Sections_get(this.swigCPtr);
        if (granny_file_Sections_get == 0) {
            return null;
        }
        return new C5269aCv(granny_file_Sections_get, false);
    }

    /* renamed from: b */
    public void mo14789b(C1418Um um) {
        grannyJNI.granny_file_Marshalled_set(this.swigCPtr, C1418Um.m10339a(um));
    }

    public C1418Um cjP() {
        long granny_file_Marshalled_get = grannyJNI.granny_file_Marshalled_get(this.swigCPtr);
        if (granny_file_Marshalled_get == 0) {
            return null;
        }
        return new C1418Um(granny_file_Marshalled_get, false);
    }

    /* renamed from: c */
    public void mo14790c(C1418Um um) {
        grannyJNI.granny_file_IsUserMemory_set(this.swigCPtr, C1418Um.m10339a(um));
    }

    public C1418Um cjQ() {
        long granny_file_IsUserMemory_get = grannyJNI.granny_file_IsUserMemory_get(this.swigCPtr);
        if (granny_file_IsUserMemory_get == 0) {
            return null;
        }
        return new C1418Um(granny_file_IsUserMemory_get, false);
    }

    /* renamed from: e */
    public void mo14801e(C5501aLt alt) {
        grannyJNI.granny_file_ConversionBuffer_set(this.swigCPtr, C5501aLt.m16286h(alt));
    }

    public C5501aLt cjR() {
        long granny_file_ConversionBuffer_get = grannyJNI.granny_file_ConversionBuffer_get(this.swigCPtr);
        if (granny_file_ConversionBuffer_get == 0) {
            return null;
        }
        return new C5501aLt(granny_file_ConversionBuffer_get, false);
    }

    /* renamed from: sC */
    public C6396amE mo14806sC(int i) {
        long granny_file_get = grannyJNI.granny_file_get(this.swigCPtr, i);
        if (granny_file_get == 0) {
            return null;
        }
        return new C6396amE(granny_file_get, false);
    }
}
