package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.CS */
/* compiled from: a */
public class C0201CS {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0201CS(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0201CS() {
        this(grannyJNI.new_granny_material(), true);
    }

    /* renamed from: a */
    public static long m1752a(C0201CS cs) {
        if (cs == null) {
            return 0;
        }
        return cs.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_material(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getName() {
        return grannyJNI.granny_material_Name_get(this.swigCPtr);
    }

    public void setName(String str) {
        grannyJNI.granny_material_Name_set(this.swigCPtr, str);
    }

    /* renamed from: gA */
    public void mo1113gA(int i) {
        grannyJNI.granny_material_MapCount_set(this.swigCPtr, i);
    }

    public int aDy() {
        return grannyJNI.granny_material_MapCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo1105a(aBV abv) {
        grannyJNI.granny_material_Maps_set(this.swigCPtr, aBV.m12973b(abv));
    }

    public aBV aDz() {
        long granny_material_Maps_get = grannyJNI.granny_material_Maps_get(this.swigCPtr);
        if (granny_material_Maps_get == 0) {
            return null;
        }
        return new aBV(granny_material_Maps_get, false);
    }

    /* renamed from: a */
    public void mo1106a(C5350aFy afy) {
        grannyJNI.granny_material_Texture_set(this.swigCPtr, C5350aFy.m14701c(afy));
    }

    public C5350aFy aDA() {
        long granny_material_Texture_get = grannyJNI.granny_material_Texture_get(this.swigCPtr);
        if (granny_material_Texture_get == 0) {
            return null;
        }
        return new C5350aFy(granny_material_Texture_get, false);
    }

    /* renamed from: a */
    public void mo1107a(aSM asm) {
        grannyJNI.granny_material_ExtendedData_set(this.swigCPtr, aSM.m18172c(asm));
    }

    /* renamed from: kM */
    public aSM mo1116kM() {
        long granny_material_ExtendedData_get = grannyJNI.granny_material_ExtendedData_get(this.swigCPtr);
        if (granny_material_ExtendedData_get == 0) {
            return null;
        }
        return new aSM(granny_material_ExtendedData_get, false);
    }

    /* renamed from: gB */
    public C0201CS mo1114gB(int i) {
        long granny_material_get = grannyJNI.granny_material_get(this.swigCPtr, i);
        if (granny_material_get == 0) {
            return null;
        }
        return new C0201CS(granny_material_get, false);
    }
}
