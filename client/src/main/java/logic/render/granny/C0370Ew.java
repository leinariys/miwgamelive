package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.Ew */
/* compiled from: a */
public class C0370Ew {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0370Ew(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0370Ew() {
        this(grannyJNI.new_granny_tri_annotation_set(), true);
    }

    /* renamed from: a */
    public static long m3111a(C0370Ew ew) {
        if (ew == null) {
            return 0;
        }
        return ew.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_tri_annotation_set(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getName() {
        return grannyJNI.granny_tri_annotation_set_Name_get(this.swigCPtr);
    }

    public void setName(String str) {
        grannyJNI.granny_tri_annotation_set_Name_set(this.swigCPtr, str);
    }

    /* renamed from: b */
    public void mo2046b(C1731Za za) {
        grannyJNI.granny_tri_annotation_set_TriAnnotationType_set(this.swigCPtr, C1731Za.m12101bi(za));
    }

    public C1731Za aOE() {
        long granny_tri_annotation_set_TriAnnotationType_get = grannyJNI.granny_tri_annotation_set_TriAnnotationType_get(this.swigCPtr);
        if (granny_tri_annotation_set_TriAnnotationType_get == 0) {
            return null;
        }
        return new C1731Za(granny_tri_annotation_set_TriAnnotationType_get, false);
    }

    /* renamed from: gX */
    public void mo2051gX(int i) {
        grannyJNI.granny_tri_annotation_set_TriAnnotationCount_set(this.swigCPtr, i);
    }

    public int aOF() {
        return grannyJNI.granny_tri_annotation_set_TriAnnotationCount_get(this.swigCPtr);
    }

    /* renamed from: f */
    public void mo2049f(C2382ef efVar) {
        grannyJNI.granny_tri_annotation_set_TriAnnotations_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    public C2382ef aOG() {
        long granny_tri_annotation_set_TriAnnotations_get = grannyJNI.granny_tri_annotation_set_TriAnnotations_get(this.swigCPtr);
        if (granny_tri_annotation_set_TriAnnotations_get == 0) {
            return null;
        }
        return new C2382ef(granny_tri_annotation_set_TriAnnotations_get, false);
    }

    /* renamed from: gY */
    public void mo2052gY(int i) {
        grannyJNI.granny_tri_annotation_set_IndicesMapFromTriToAnnotation_set(this.swigCPtr, i);
    }

    public int aOH() {
        return grannyJNI.granny_tri_annotation_set_IndicesMapFromTriToAnnotation_get(this.swigCPtr);
    }

    /* renamed from: gZ */
    public void mo2053gZ(int i) {
        grannyJNI.granny_tri_annotation_set_TriAnnotationIndexCount_set(this.swigCPtr, i);
    }

    public int aOI() {
        return grannyJNI.granny_tri_annotation_set_TriAnnotationIndexCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo2047b(C1845ab abVar) {
        grannyJNI.granny_tri_annotation_set_TriAnnotationIndices_set(this.swigCPtr, C1845ab.m19847a(abVar));
    }

    public C1845ab aOJ() {
        long granny_tri_annotation_set_TriAnnotationIndices_get = grannyJNI.granny_tri_annotation_set_TriAnnotationIndices_get(this.swigCPtr);
        if (granny_tri_annotation_set_TriAnnotationIndices_get == 0) {
            return null;
        }
        return new C1845ab(granny_tri_annotation_set_TriAnnotationIndices_get, false);
    }

    /* renamed from: ha */
    public int mo2055ha(int i) {
        return grannyJNI.granny_tri_annotation_set_getTriAnnotation(this.swigCPtr, i);
    }

    /* renamed from: hb */
    public int mo2056hb(int i) {
        return grannyJNI.granny_tri_annotation_set_getTriAnnotationIndex(this.swigCPtr, i);
    }

    /* renamed from: hc */
    public C0370Ew mo2057hc(int i) {
        long granny_tri_annotation_set_get = grannyJNI.granny_tri_annotation_set_get(this.swigCPtr, i);
        if (granny_tri_annotation_set_get == 0) {
            return null;
        }
        return new C0370Ew(granny_tri_annotation_set_get, false);
    }
}
