package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.pt */
/* compiled from: a */
public final class C3278pt {
    public static final C3278pt aRH = new C3278pt("GrannyEndMember");
    public static final C3278pt aRI = new C3278pt("GrannyInlineMember");
    public static final C3278pt aRJ = new C3278pt("GrannyReferenceMember");
    public static final C3278pt aRK = new C3278pt("GrannyReferenceToArrayMember");
    public static final C3278pt aRL = new C3278pt("GrannyArrayOfReferencesMember");
    public static final C3278pt aRM = new C3278pt("GrannyVariantReferenceMember");
    public static final C3278pt aRN = new C3278pt("GrannyUnsupportedMemberType_Remove");
    public static final C3278pt aRO = new C3278pt("GrannyReferenceToVariantArrayMember");
    public static final C3278pt aRP = new C3278pt("GrannyStringMember");
    public static final C3278pt aRQ = new C3278pt("GrannyTransformMember");
    public static final C3278pt aRR = new C3278pt("GrannyReal32Member");
    public static final C3278pt aRS = new C3278pt("GrannyInt8Member");
    public static final C3278pt aRT = new C3278pt("GrannyUInt8Member");
    public static final C3278pt aRU = new C3278pt("GrannyBinormalInt8Member");
    public static final C3278pt aRV = new C3278pt("GrannyNormalUInt8Member");
    public static final C3278pt aRW = new C3278pt("GrannyInt16Member");
    public static final C3278pt aRX = new C3278pt("GrannyUInt16Member");
    public static final C3278pt aRY = new C3278pt("GrannyBinormalInt16Member");
    public static final C3278pt aRZ = new C3278pt("GrannyNormalUInt16Member");
    public static final C3278pt aSa = new C3278pt("GrannyInt32Member");
    public static final C3278pt aSb = new C3278pt("GrannyUInt32Member");
    public static final C3278pt aSc = new C3278pt("GrannyReal16Member");
    public static final C3278pt aSd = new C3278pt("GrannyEmptyReferenceMember");
    public static final C3278pt aSe = new C3278pt("GrannyOnePastLastMemberType");
    public static final C3278pt aSf = new C3278pt("GrannyBool32Member", grannyJNI.GrannyBool32Member_get());
    private static C3278pt[] aSg = {aRH, aRI, aRJ, aRK, aRL, aRM, aRN, aRO, aRP, aRQ, aRR, aRS, aRT, aRU, aRV, aRW, aRX, aRY, aRZ, aSa, aSb, aSc, aSd, aSe, aSf};

    /* renamed from: pF */
    private static int f8852pF = 0;

    /* renamed from: pG */
    private final int f8853pG;

    /* renamed from: pH */
    private final String f8854pH;

    private C3278pt(String str) {
        this.f8854pH = str;
        int i = f8852pF;
        f8852pF = i + 1;
        this.f8853pG = i;
    }

    private C3278pt(String str, int i) {
        this.f8854pH = str;
        this.f8853pG = i;
        f8852pF = i + 1;
    }

    private C3278pt(String str, C3278pt ptVar) {
        this.f8854pH = str;
        this.f8853pG = ptVar.f8853pG;
        f8852pF = this.f8853pG + 1;
    }

    /* renamed from: dK */
    public static C3278pt m37325dK(int i) {
        if (i < aSg.length && i >= 0 && aSg[i].f8853pG == i) {
            return aSg[i];
        }
        for (int i2 = 0; i2 < aSg.length; i2++) {
            if (aSg[i2].f8853pG == i) {
                return aSg[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C3278pt.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f8853pG;
    }

    public String toString() {
        return this.f8854pH;
    }
}
