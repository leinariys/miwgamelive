package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aFy  reason: case insensitive filesystem */
/* compiled from: a */
public class C5350aFy {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5350aFy(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5350aFy() {
        this(grannyJNI.new_granny_texture(), true);
    }

    /* renamed from: c */
    public static long m14701c(C5350aFy afy) {
        if (afy == null) {
            return 0;
        }
        return afy.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_texture(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: T */
    public void mo8893T(String str) {
        grannyJNI.granny_texture_FromFileName_set(this.swigCPtr, str);
    }

    /* renamed from: kt */
    public String mo8907kt() {
        return grannyJNI.granny_texture_FromFileName_get(this.swigCPtr);
    }

    /* renamed from: xO */
    public void mo8911xO(int i) {
        grannyJNI.granny_texture_TextureType_set(this.swigCPtr, i);
    }

    public int cYM() {
        return grannyJNI.granny_texture_TextureType_get(this.swigCPtr);
    }

    public int getWidth() {
        return grannyJNI.granny_texture_Width_get(this.swigCPtr);
    }

    public void setWidth(int i) {
        grannyJNI.granny_texture_Width_set(this.swigCPtr, i);
    }

    public int getHeight() {
        return grannyJNI.granny_texture_Height_get(this.swigCPtr);
    }

    public void setHeight(int i) {
        grannyJNI.granny_texture_Height_set(this.swigCPtr, i);
    }

    /* renamed from: xP */
    public void mo8912xP(int i) {
        grannyJNI.granny_texture_Encoding_set(this.swigCPtr, i);
    }

    public int cYN() {
        return grannyJNI.granny_texture_Encoding_get(this.swigCPtr);
    }

    /* renamed from: xQ */
    public void mo8913xQ(int i) {
        grannyJNI.granny_texture_SubFormat_set(this.swigCPtr, i);
    }

    public int cYO() {
        return grannyJNI.granny_texture_SubFormat_get(this.swigCPtr);
    }

    /* renamed from: q */
    public void mo8908q(C0994Oe oe) {
        grannyJNI.granny_texture_Layout_set(this.swigCPtr, C0994Oe.m8106p(oe));
    }

    public C0994Oe cYP() {
        long granny_texture_Layout_get = grannyJNI.granny_texture_Layout_get(this.swigCPtr);
        if (granny_texture_Layout_get == 0) {
            return null;
        }
        return new C0994Oe(granny_texture_Layout_get, false);
    }

    /* renamed from: xR */
    public void mo8914xR(int i) {
        grannyJNI.granny_texture_ImageCount_set(this.swigCPtr, i);
    }

    public int cYQ() {
        return grannyJNI.granny_texture_ImageCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo8895b(C2406ex exVar) {
        grannyJNI.granny_texture_Images_set(this.swigCPtr, C2406ex.m30145a(exVar));
    }

    public C2406ex cYR() {
        long granny_texture_Images_get = grannyJNI.granny_texture_Images_get(this.swigCPtr);
        if (granny_texture_Images_get == 0) {
            return null;
        }
        return new C2406ex(granny_texture_Images_get, false);
    }

    /* renamed from: a */
    public void mo8894a(aSM asm) {
        grannyJNI.granny_texture_ExtendedData_set(this.swigCPtr, aSM.m18172c(asm));
    }

    /* renamed from: kM */
    public aSM mo8906kM() {
        long granny_texture_ExtendedData_get = grannyJNI.granny_texture_ExtendedData_get(this.swigCPtr);
        if (granny_texture_ExtendedData_get == 0) {
            return null;
        }
        return new aSM(granny_texture_ExtendedData_get, false);
    }

    /* renamed from: xS */
    public C5350aFy mo8915xS(int i) {
        long granny_texture_get = grannyJNI.granny_texture_get(this.swigCPtr, i);
        if (granny_texture_get == 0) {
            return null;
        }
        return new C5350aFy(granny_texture_get, false);
    }
}
