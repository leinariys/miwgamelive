package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aDP */
/* compiled from: a */
public class aDP {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aDP(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aDP() {
        this(grannyJNI.new_granny_model_mesh_binding(), true);
    }

    /* renamed from: b */
    public static long m13596b(aDP adp) {
        if (adp == null) {
            return 0;
        }
        return adp.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_model_mesh_binding(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: m */
    public void mo8388m(C1126QZ qz) {
        grannyJNI.granny_model_mesh_binding_Mesh_set(this.swigCPtr, C1126QZ.m8909l(qz));
    }

    public C1126QZ cWp() {
        long granny_model_mesh_binding_Mesh_get = grannyJNI.granny_model_mesh_binding_Mesh_get(this.swigCPtr);
        if (granny_model_mesh_binding_Mesh_get == 0) {
            return null;
        }
        return new C1126QZ(granny_model_mesh_binding_Mesh_get, false);
    }

    /* renamed from: xD */
    public aDP mo8389xD(int i) {
        long granny_model_mesh_binding_get = grannyJNI.granny_model_mesh_binding_get(this.swigCPtr, i);
        if (granny_model_mesh_binding_get == 0) {
            return null;
        }
        return new aDP(granny_model_mesh_binding_get, false);
    }
}
