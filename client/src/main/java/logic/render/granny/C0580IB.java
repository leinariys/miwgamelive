package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.IB */
/* compiled from: a */
public class C0580IB {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0580IB(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0580IB() {
        this(grannyJNI.new_granny_tri_topology(), true);
    }

    /* renamed from: b */
    public static long m5303b(C0580IB ib) {
        if (ib == null) {
            return 0;
        }
        return ib.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_tri_topology(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: kJ */
    public void mo2748kJ(int i) {
        grannyJNI.granny_tri_topology_GroupCount_set(this.swigCPtr, i);
    }

    public int aWr() {
        return grannyJNI.granny_tri_topology_GroupCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo2738b(C3152oP oPVar) {
        grannyJNI.granny_tri_topology_Groups_set(this.swigCPtr, C3152oP.m36708a(oPVar));
    }

    public C3152oP aWs() {
        long granny_tri_topology_Groups_get = grannyJNI.granny_tri_topology_Groups_get(this.swigCPtr);
        if (granny_tri_topology_Groups_get == 0) {
            return null;
        }
        return new C3152oP(granny_tri_topology_Groups_get, false);
    }

    /* renamed from: kK */
    public void mo2749kK(int i) {
        grannyJNI.granny_tri_topology_IndexCount_set(this.swigCPtr, i);
    }

    public int aWt() {
        return grannyJNI.granny_tri_topology_IndexCount_get(this.swigCPtr);
    }

    /* renamed from: c */
    public void mo2739c(C1845ab abVar) {
        grannyJNI.granny_tri_topology_Indices_set(this.swigCPtr, C1845ab.m19847a(abVar));
    }

    public C1845ab aWu() {
        long granny_tri_topology_Indices_get = grannyJNI.granny_tri_topology_Indices_get(this.swigCPtr);
        if (granny_tri_topology_Indices_get == 0) {
            return null;
        }
        return new C1845ab(granny_tri_topology_Indices_get, false);
    }

    /* renamed from: kL */
    public void mo2750kL(int i) {
        grannyJNI.granny_tri_topology_Index16Count_set(this.swigCPtr, i);
    }

    public int aWv() {
        return grannyJNI.granny_tri_topology_Index16Count_get(this.swigCPtr);
    }

    /* renamed from: c */
    public void mo2741c(C2062bl blVar) {
        grannyJNI.granny_tri_topology_Indices16_set(this.swigCPtr, C2062bl.m27965a(blVar));
    }

    public C2062bl aWw() {
        long granny_tri_topology_Indices16_get = grannyJNI.granny_tri_topology_Indices16_get(this.swigCPtr);
        if (granny_tri_topology_Indices16_get == 0) {
            return null;
        }
        return new C2062bl(granny_tri_topology_Indices16_get, false);
    }

    /* renamed from: kM */
    public void mo2751kM(int i) {
        grannyJNI.granny_tri_topology_VertexToVertexCount_set(this.swigCPtr, i);
    }

    public int aWx() {
        return grannyJNI.granny_tri_topology_VertexToVertexCount_get(this.swigCPtr);
    }

    /* renamed from: d */
    public void mo2742d(C1845ab abVar) {
        grannyJNI.granny_tri_topology_VertexToVertexMap_set(this.swigCPtr, C1845ab.m19847a(abVar));
    }

    public C1845ab aWy() {
        long granny_tri_topology_VertexToVertexMap_get = grannyJNI.granny_tri_topology_VertexToVertexMap_get(this.swigCPtr);
        if (granny_tri_topology_VertexToVertexMap_get == 0) {
            return null;
        }
        return new C1845ab(granny_tri_topology_VertexToVertexMap_get, false);
    }

    /* renamed from: kN */
    public void mo2752kN(int i) {
        grannyJNI.granny_tri_topology_VertexToTriangleCount_set(this.swigCPtr, i);
    }

    public int aWz() {
        return grannyJNI.granny_tri_topology_VertexToTriangleCount_get(this.swigCPtr);
    }

    /* renamed from: e */
    public void mo2744e(C1845ab abVar) {
        grannyJNI.granny_tri_topology_VertexToTriangleMap_set(this.swigCPtr, C1845ab.m19847a(abVar));
    }

    public C1845ab aWA() {
        long granny_tri_topology_VertexToTriangleMap_get = grannyJNI.granny_tri_topology_VertexToTriangleMap_get(this.swigCPtr);
        if (granny_tri_topology_VertexToTriangleMap_get == 0) {
            return null;
        }
        return new C1845ab(granny_tri_topology_VertexToTriangleMap_get, false);
    }

    /* renamed from: kO */
    public void mo2753kO(int i) {
        grannyJNI.granny_tri_topology_SideToNeighborCount_set(this.swigCPtr, i);
    }

    public int aWB() {
        return grannyJNI.granny_tri_topology_SideToNeighborCount_get(this.swigCPtr);
    }

    /* renamed from: c */
    public void mo2740c(C6135ahD ahd) {
        grannyJNI.granny_tri_topology_SideToNeighborMap_set(this.swigCPtr, C6135ahD.m22189i(ahd));
    }

    public C6135ahD aWC() {
        long granny_tri_topology_SideToNeighborMap_get = grannyJNI.granny_tri_topology_SideToNeighborMap_get(this.swigCPtr);
        if (granny_tri_topology_SideToNeighborMap_get == 0) {
            return null;
        }
        return new C6135ahD(granny_tri_topology_SideToNeighborMap_get, false);
    }

    /* renamed from: kP */
    public void mo2754kP(int i) {
        grannyJNI.granny_tri_topology_BonesForTriangleCount_set(this.swigCPtr, i);
    }

    public int aWD() {
        return grannyJNI.granny_tri_topology_BonesForTriangleCount_get(this.swigCPtr);
    }

    /* renamed from: f */
    public void mo2745f(C1845ab abVar) {
        grannyJNI.granny_tri_topology_BonesForTriangle_set(this.swigCPtr, C1845ab.m19847a(abVar));
    }

    public C1845ab aWE() {
        long granny_tri_topology_BonesForTriangle_get = grannyJNI.granny_tri_topology_BonesForTriangle_get(this.swigCPtr);
        if (granny_tri_topology_BonesForTriangle_get == 0) {
            return null;
        }
        return new C1845ab(granny_tri_topology_BonesForTriangle_get, false);
    }

    /* renamed from: kQ */
    public void mo2755kQ(int i) {
        grannyJNI.granny_tri_topology_TriangleToBoneCount_set(this.swigCPtr, i);
    }

    public int aWF() {
        return grannyJNI.granny_tri_topology_TriangleToBoneCount_get(this.swigCPtr);
    }

    /* renamed from: g */
    public void mo2747g(C1845ab abVar) {
        grannyJNI.granny_tri_topology_TriangleToBoneIndices_set(this.swigCPtr, C1845ab.m19847a(abVar));
    }

    public C1845ab aWG() {
        long granny_tri_topology_TriangleToBoneIndices_get = grannyJNI.granny_tri_topology_TriangleToBoneIndices_get(this.swigCPtr);
        if (granny_tri_topology_TriangleToBoneIndices_get == 0) {
            return null;
        }
        return new C1845ab(granny_tri_topology_TriangleToBoneIndices_get, false);
    }

    /* renamed from: kR */
    public void mo2756kR(int i) {
        grannyJNI.granny_tri_topology_TriAnnotationSetCount_set(this.swigCPtr, i);
    }

    public int aWH() {
        return grannyJNI.granny_tri_topology_TriAnnotationSetCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo2737b(C0370Ew ew) {
        grannyJNI.granny_tri_topology_TriAnnotationSets_set(this.swigCPtr, C0370Ew.m3111a(ew));
    }

    public C0370Ew aWI() {
        long granny_tri_topology_TriAnnotationSets_get = grannyJNI.granny_tri_topology_TriAnnotationSets_get(this.swigCPtr);
        if (granny_tri_topology_TriAnnotationSets_get == 0) {
            return null;
        }
        return new C0370Ew(granny_tri_topology_TriAnnotationSets_get, false);
    }

    /* renamed from: kS */
    public int mo2757kS(int i) {
        return grannyJNI.granny_tri_topology_getVertexToTriangleMap(this.swigCPtr, i);
    }

    /* renamed from: kT */
    public C0580IB mo2758kT(int i) {
        long granny_tri_topology_get = grannyJNI.granny_tri_topology_get(this.swigCPtr, i);
        if (granny_tri_topology_get == 0) {
            return null;
        }
        return new C0580IB(granny_tri_topology_get, false);
    }
}
