package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aSA */
/* compiled from: a */
public class aSA {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aSA(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aSA() {
        this(grannyJNI.new_granny_vertex_annotation_set(), true);
    }

    /* renamed from: b */
    public static long m18148b(aSA asa) {
        if (asa == null) {
            return 0;
        }
        return asa.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_vertex_annotation_set(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getName() {
        return grannyJNI.granny_vertex_annotation_set_Name_get(this.swigCPtr);
    }

    public void setName(String str) {
        grannyJNI.granny_vertex_annotation_set_Name_set(this.swigCPtr, str);
    }

    /* renamed from: bn */
    public void mo11315bn(C1731Za za) {
        grannyJNI.granny_vertex_annotation_set_VertexAnnotationType_set(this.swigCPtr, C1731Za.m12101bi(za));
    }

    public C1731Za dux() {
        long granny_vertex_annotation_set_VertexAnnotationType_get = grannyJNI.granny_vertex_annotation_set_VertexAnnotationType_get(this.swigCPtr);
        if (granny_vertex_annotation_set_VertexAnnotationType_get == 0) {
            return null;
        }
        return new C1731Za(granny_vertex_annotation_set_VertexAnnotationType_get, false);
    }

    /* renamed from: AC */
    public void mo11311AC(int i) {
        grannyJNI.granny_vertex_annotation_set_VertexAnnotationCount_set(this.swigCPtr, i);
    }

    public int duy() {
        return grannyJNI.granny_vertex_annotation_set_VertexAnnotationCount_get(this.swigCPtr);
    }

    /* renamed from: i */
    public void mo11325i(C2382ef efVar) {
        grannyJNI.granny_vertex_annotation_set_VertexAnnotations_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    public C2382ef duz() {
        long granny_vertex_annotation_set_VertexAnnotations_get = grannyJNI.granny_vertex_annotation_set_VertexAnnotations_get(this.swigCPtr);
        if (granny_vertex_annotation_set_VertexAnnotations_get == 0) {
            return null;
        }
        return new C2382ef(granny_vertex_annotation_set_VertexAnnotations_get, false);
    }

    /* renamed from: AD */
    public void mo11312AD(int i) {
        grannyJNI.m45858xa8aab23e(this.swigCPtr, i);
    }

    public int duA() {
        return grannyJNI.m45857xa8aa8532(this.swigCPtr);
    }

    /* renamed from: AE */
    public void mo11313AE(int i) {
        grannyJNI.granny_vertex_annotation_set_VertexAnnotationIndexCount_set(this.swigCPtr, i);
    }

    public int duB() {
        return grannyJNI.granny_vertex_annotation_set_VertexAnnotationIndexCount_get(this.swigCPtr);
    }

    /* renamed from: l */
    public void mo11326l(C1845ab abVar) {
        grannyJNI.granny_vertex_annotation_set_VertexAnnotationIndices_set(this.swigCPtr, C1845ab.m19847a(abVar));
    }

    public C1845ab duC() {
        long granny_vertex_annotation_set_VertexAnnotationIndices_get = grannyJNI.granny_vertex_annotation_set_VertexAnnotationIndices_get(this.swigCPtr);
        if (granny_vertex_annotation_set_VertexAnnotationIndices_get == 0) {
            return null;
        }
        return new C1845ab(granny_vertex_annotation_set_VertexAnnotationIndices_get, false);
    }

    /* renamed from: AF */
    public aSA mo11314AF(int i) {
        long granny_vertex_annotation_set_get = grannyJNI.granny_vertex_annotation_set_get(this.swigCPtr, i);
        if (granny_vertex_annotation_set_get == 0) {
            return null;
        }
        return new aSA(granny_vertex_annotation_set_get, false);
    }
}
