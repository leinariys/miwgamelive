package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.NP */
/* compiled from: a */
public class C0913NP {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0913NP(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0913NP() {
        this(grannyJNI.new_granny_skeleton(), true);
    }

    /* renamed from: a */
    public static long m7619a(C0913NP np) {
        if (np == null) {
            return 0;
        }
        return np.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_skeleton(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getName() {
        return grannyJNI.granny_skeleton_Name_get(this.swigCPtr);
    }

    public void setName(String str) {
        grannyJNI.granny_skeleton_Name_set(this.swigCPtr, str);
    }

    /* renamed from: lX */
    public void mo4231lX(int i) {
        grannyJNI.granny_skeleton_BoneCount_set(this.swigCPtr, i);
    }

    public int bkG() {
        return grannyJNI.granny_skeleton_BoneCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo4222a(C5767aVz avz) {
        grannyJNI.granny_skeleton_Bones_set(this.swigCPtr, C5767aVz.m19225b(avz));
    }

    public C5767aVz bkH() {
        long granny_skeleton_Bones_get = grannyJNI.granny_skeleton_Bones_get(this.swigCPtr);
        if (granny_skeleton_Bones_get == 0) {
            return null;
        }
        return new C5767aVz(granny_skeleton_Bones_get, false);
    }

    /* renamed from: lY */
    public void mo4232lY(int i) {
        grannyJNI.granny_skeleton_LODType_set(this.swigCPtr, i);
    }

    public int bkI() {
        return grannyJNI.granny_skeleton_LODType_get(this.swigCPtr);
    }

    /* renamed from: ga */
    public int mo4228ga(String str) {
        return grannyJNI.granny_skeleton_findBoneByNameLowercase(this.swigCPtr, str);
    }

    /* renamed from: gb */
    public int mo4229gb(String str) {
        return grannyJNI.granny_skeleton_findBoneByName(this.swigCPtr, str);
    }

    /* renamed from: lZ */
    public C0913NP mo4233lZ(int i) {
        long granny_skeleton_get = grannyJNI.granny_skeleton_get(this.swigCPtr, i);
        if (granny_skeleton_get == 0) {
            return null;
        }
        return new C0913NP(granny_skeleton_get, false);
    }
}
