package logic.render.granny;

/* renamed from: a.ato  reason: case insensitive filesystem */
/* compiled from: a */
public class C6796ato {
    private long swigCPtr;

    public C6796ato(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6796ato() {
        this.swigCPtr = 0;
    }

    /* renamed from: i */
    public static long m26060i(C6796ato ato) {
        if (ato == null) {
            return 0;
        }
        return ato.swigCPtr;
    }
}
