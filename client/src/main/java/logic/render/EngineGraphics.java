package logic.render;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import com.sun.opengl.util.Screenshot;
import game.geometry.Vector2fWrap;
import logic.C6245ajJ;
import logic.res.ConfigIniKeyValue;
import logic.res.ConfigManagerSection;
import logic.res.FileControl;
import logic.res.KeyCode;
import logic.thred.LogPrinter;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Controllers;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.addon.neo.preferences.GUIPrefAddon;
import taikodom.infra.script.I18NString;
import taikodom.render.*;
import taikodom.render.camera.Camera;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.gl.GLWrapper;
import taikodom.render.gui.GBillboard;
import taikodom.render.gui.GuiScene;
import taikodom.render.helpers.RenderTask;
import taikodom.render.impostor.ImpostorManager;
import taikodom.render.loader.FileSceneLoader;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.provider.BitmapFontFactory;
import taikodom.render.loader.provider.FilePath;
import taikodom.render.loader.provider.ImageLoader;
import taikodom.render.postProcessing.GlowFX;
import taikodom.render.postProcessing.PostProcessingFX;
import taikodom.render.scene.*;
import taikodom.render.textures.DDSLoader;
import taikodom.render.textures.Material;
import taikodom.render.textures.RenderTarget;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.*;
import java.util.concurrent.Semaphore;


/**
 * Графический движок JR.java
 */
/* renamed from: a.JR */
/* compiled from: a */
public class EngineGraphics implements IEngineGraphics {
    private static final LogPrinter logger = LogPrinter.m10275K(EngineGraphics.class);
    private static Semaphore idM = new Semaphore(1);

    static {
        System.loadLibrary("mss32");// нужен для воспроизведения звука
        System.loadLibrary("granny2");// библиотека, обеспечивающая корректную работу игрового приложения
        System.loadLibrary("binkw32");//библиотеку видео кодека для игр, разработанного RAD Game Tools
    }

    private final GuiScene iel = new GuiScene();
    /**
     * Эксклюзивный видеопроигрыватель
     */
    private final ExclusiveVideoPlayer ieq = new ExclusiveVideoPlayer();
    private final StepContext stepContext = new StepContext();
    /* access modifiers changed from: private */
    public int bpp;
    /* access modifiers changed from: private */
    public boolean fullscreen;
    /* access modifiers changed from: private */
    public RenderConfig renderConfig = new RenderConfig();
    /**
     * Возвращает экран GraphicsDevice по умолчанию.
     */
    public GraphicsDevice idW;
    /**
     * Возвращает текущий режим отображения этого GraphicsDevice.
     * Настройки экрана, разрешение, глубина цвета, частота
     */
    public DisplayMode idX;
    /**
     * Окно клиента
     */
    public Frame idZ;
    /* access modifiers changed from: private */
    public int frWidth;
    /* access modifiers changed from: private */
    public int frHeight;
    /**
     * совпадает ли размер окна и разрешение экрана
     */
    public boolean iec;
    /**
     * Закрыто ли окно
     */
    public boolean iee;
    /**
     * Размер окна полноэкранного режима
     */
    DisplayMode ier = null;
    /**
     * GLCanvas и GLJPanel или JGLDesktop(своя реализация) - это два основных класса графического интерфейс openGL
     * Всякий раз, когда вы сталкиваетесь с проблемами с GLJCanvas , вы должны использовать класс GLJPanel .
     */
    private JGLDesktop ddf;
    /**
     * EventManager Блокировщик чего-то или выполнитель задачь в главном потоке для графического движка
     */
    private C6245ajJ eventManager;
    private List<RenderTask> ghr = new ArrayList();
    private GLContext glContext;
    /**
     * Локализация
     */
    private String localization = Locale.getDefault().getLanguage().toLowerCase();
    /**
     * список настроек раздела [render]
     */
    private ConfigManagerSection configManagerSection;
    private C0656b idQ = new C0656b(this, (C0656b) null);
    private List<C6296akI.C1925a> idR = new ArrayList();
    private List<C6296akI.C1925a> idS = new ArrayList();
    private List<C6296akI.C1925a> idT = new ArrayList();
    /**
     * Список иконок окна
     */
    private List<Image> idU;
    private List<SceneView> idV = Collections.synchronizedList(new LinkedList());
    /**
     * Списка допустимые параметры экрана для игры. Разрешение экрана и частота
     */
    private ArrayList<Vec3f> idY;
    private Robot ied;
    private boolean ief;
    private boolean ieg;
    private SceneView ieh;
    private SceneView iei;
    private List<SceneEvent> iej;
    private boolean iek = false;
    private SoundObject iem;
    private SoundObject ien;
    private GBillboard ieo;
    private boolean iep;
    private ImpostorManager impostorManager;
    /**
     * Загрузчик ресурсов файлов с расширением .pro
     */
    private FileSceneLoader loader;
    private int refreshRate;
    private RenderGui renderGui;
    private RenderView renderView;
    /**
     * Корневой путь. Каталог с кодом отрисовки графики
     */
    private FilePath rootpath;

    public EngineGraphics(C6245ajJ ajj) {
        this.eventManager = ajj;
    }

    public SceneView aea() {
        return this.ieh;
    }

    public Scene adZ() {
        return this.ieh.getScene();
    }

    public Camera adY() {
        return this.ieh.getCamera();
    }

    /**
     * Применяем настройки раздела [render]
     *
     * @param yr  список настроек раздела [render]
     * @param ain Каталог с кодом отрисовки графики
     */
    /* renamed from: a */
    public void mo3003a(ConfigManagerSection yr, FilePath ain) {
        boolean z;
        this.configManagerSection = yr;
        this.renderConfig.setScreenWidth(this.configManagerSection.mo7195a(ConfigIniKeyValue.XRES, (Integer) 1024).intValue());
        this.renderConfig.setScreenHeight(this.configManagerSection.mo7195a(ConfigIniKeyValue.YRES, Integer.valueOf(KeyCode.cuB)).intValue());
        this.renderConfig.setBpp(this.configManagerSection.mo7195a(ConfigIniKeyValue.BPP, (Integer) 32).shortValue());
        this.renderConfig.setTextureQuality(this.configManagerSection.mo7195a(ConfigIniKeyValue.TEXTURE_QUALITY, (Integer) 512).intValue());
        this.renderConfig.setTextureFilter(this.configManagerSection.mo7195a(ConfigIniKeyValue.TEXTURE_FILTER, (Integer) 0).intValue());
        this.renderConfig.setAntiAliasing(this.configManagerSection.mo7195a(ConfigIniKeyValue.ANTI_ALIASING, (Integer) 0).intValue());
        this.renderConfig.setLodQuality(this.configManagerSection.mo7193a(ConfigIniKeyValue.LOD_QUALITY, Float.valueOf(0.0f)).floatValue());
        this.renderConfig.setEnvFxQuality(this.configManagerSection.mo7195a(ConfigIniKeyValue.ENVIRONMENT_FX_QUALITY, (Integer) 0).intValue());
        this.renderConfig.setShaderQuality(this.configManagerSection.mo7195a(ConfigIniKeyValue.SHADER_QUALITY, (Integer) 0).intValue());
        this.renderConfig.setFullscreen(this.configManagerSection.mo7191a(ConfigIniKeyValue.ENABLE_FULLSCREEN, Boolean.FALSE).booleanValue());
        this.renderConfig.setUseVbo(this.configManagerSection.mo7191a(ConfigIniKeyValue.ENABLE_VBO, Boolean.TRUE).booleanValue());
        this.renderConfig.setAnisotropicFilter(this.configManagerSection.mo7193a(ConfigIniKeyValue.ANISOTROPIC_FILTER, Float.valueOf(0.0f)));
        this.renderConfig.setPostProcessingFX(this.configManagerSection.mo7191a(ConfigIniKeyValue.ENABLE_POST_PROCESSING_FX, (Boolean) true).booleanValue());
        this.renderConfig.setGuiContrastLevel(this.configManagerSection.mo7193a(ConfigIniKeyValue.GUI_CONTRAST_BLOCKER, Float.valueOf(0.0f)).floatValue());
        this.idQ.egF = this.configManagerSection.mo7193a(ConfigIniKeyValue.VOL_MUSIC, Float.valueOf(1.0f)).floatValue();
        this.idQ.egG = this.configManagerSection.mo7193a(ConfigIniKeyValue.VOL_AMBIENCE, Float.valueOf(1.0f)).floatValue();
        this.idQ.egH = this.configManagerSection.mo7193a(ConfigIniKeyValue.VOL_GUI, Float.valueOf(1.0f)).floatValue();
        this.idQ.egI = this.configManagerSection.mo7193a(ConfigIniKeyValue.VOL_ENGINE, Float.valueOf(1.0f)).floatValue();
        this.idQ.egJ = this.configManagerSection.mo7193a(ConfigIniKeyValue.VOL_FX, Float.valueOf(1.0f)).floatValue();

        this.rootpath = ain;//Каталог с кодом отрисовки графики
        logger.info("Render root path: " + this.rootpath);
        BitmapFontFactory.setRootPath(this.rootpath.mo2253BF().getPath());
        this.loader = new FileSceneLoader(this.rootpath);
        ImageLoader.setMaxMipMapSize(this.renderConfig.getTextureQuality());

        setupJOGL();
        setupSound();
        setupControllers();
        setupGLEventListener();//Сохранение настроек

        this.renderView = new RenderView((GLAutoDrawable) this.ddf);
        GLWrapper gLWrapper = new GLWrapper(this.ddf.getGL());
        if (!GLSupportedCaps.isPbuffer() || !GLSupportedCaps.isDds()) {
            if (aei().equals(I18NString.DEFAULT_LOCATION)) {
                JOptionPane.showMessageDialog(this.idZ, "A required resource was not found. Update your video driver to the lastest version and try again.", "Error", 0);
            } else {
                JOptionPane.showMessageDialog(this.idZ, "Um recurso requerido não foi encontrado. Atualize seu driver de video para uma versão mais recente e tente novamente.", "Erro", 0);
            }
            System.exit(1);
        }
        logger.info("Available accelerated memory: " + this.idW.getAvailableAcceleratedMemory());
        RenderConfig renderConfig = this.renderConfig;
        if (!this.renderConfig.isUseVbo() || !GLSupportedCaps.isVbo()) {
            z = false;
        } else {
            z = true;
        }
        renderConfig.setUseVbo(z);
        this.ddf.setGL(gLWrapper);
        this.renderView.setGL(this.ddf.getGL());
        this.renderView.getDrawContext().setCurrentFrame(1000);
        this.renderView.getDrawContext().setRenderView(this.renderView);
        this.renderView.getDrawContext().setUseVbo(this.renderConfig.isUseVbo());
        this.renderGui = new RenderGui();
        this.renderGui.getGuiContext().setDc(this.renderView.getDrawContext());
        this.renderGui.resize(0, 0, this.frWidth, this.frHeight);
        this.ieh = new SceneView();
        this.ieh.setPriority(100);
        this.ieh.getScene().setName("Default Scene");
        this.ieh.getScene().createPartitioningStructure();
        this.ieh.setViewport(0, 0, this.frWidth, this.frHeight);
        this.ieh.setAllowImpostors(true);
        this.impostorManager = new ImpostorManager(this.ddf.getContext());
        this.renderView.getRenderContext().setImpostorManager(this.impostorManager);
        this.ieh.addPostProcessingFX(new GlowFX());
        this.ieh.getScene().addChild(new MilesSoundListener());
        mo3005a(this.ieh);
        this.iei = new SceneView();
        this.iei.setClearColorBuffer(false);
        this.iei.setClearDepthBuffer(false);
        this.iei.setClearStencilBuffer(false);
        this.iei.setEnabled(true);
        mo3005a(this.iei);
        this.ieo = new GBillboard();
        Material material = new Material();
        material.setShader(this.loader.getDefaultVertexColorShader());
        this.ieo.setMaterial(material);
        this.ieo.setPrimitiveColor(new Color(0.0f, 0.0f, 0.0f, this.renderConfig.getGuiContrastLevel()));
        this.ieo.setSize(new Vector2fWrap((float) this.renderConfig.getScreenWidth(), (float) this.renderConfig.getScreenHeight()));
        this.ieo.setPosition(new Vector2fWrap((float) (this.renderConfig.getScreenWidth() / 2), (float) (this.renderConfig.getScreenHeight() / 2)));
        this.ieo.setName("Screen Contrast Billboard");
        this.ieo.setRender(true);
        this.iel.addChild(this.ieo);
        logger.info("Render module initialized");
    }

    private void setupSound() {
        SoundMiles.getInstance().init("");
    }

    private void setupJOGL() {
        GLCapabilities gLCapabilities = new GLCapabilities();//Определяет набор возможностей OpenGL.
        gLCapabilities.setDoubleBuffered(true);//Включает или отключает двойную буферизацию.
        gLCapabilities.setHardwareAccelerated(true);//Включает или отключает аппаратное ускорение.
        if (this.renderConfig.getAntiAliasing() > 0) {//Сглаживание
            gLCapabilities.setSampleBuffers(true);//Указывает, должны ли быть выделены буферы выборки для полного сглаживания сцены (FSAA) для этой возможности.
            gLCapabilities.setNumSamples(this.renderConfig.getAntiAliasing());//Если буферы выборки включены, указывается количество выделенных буферов.
        }
        if (this.renderConfig.getBpp() == 32) {
            gLCapabilities.setDepthBits(24);//Устанавливает количество бит, запрошенных для буфера глубины.
            gLCapabilities.setStencilBits(8);//Устанавливает количество бит, запрошенных для буфера трафарета.
            gLCapabilities.setRedBits(8);//Устанавливает количество бит, запрошенных для красного компонента цветового буфера.
            gLCapabilities.setBlueBits(8);//Устанавливает количество бит, запрошенных для синего компонента цветового буфера.
            gLCapabilities.setGreenBits(8);//Устанавливает количество бит, запрошенных для зеленого компонента цветового буфера.
            gLCapabilities.setAlphaBits(8);//Устанавливает количество бит, запрошенных для альфа-компонента цветового буфера.
            this.idW = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();//Возвращает экран GraphicsDevice по умолчанию.
            this.idX = this.idW.getDisplayMode();//Возвращает настройки экрана, разрешение, глубина цвета, частота
            DisplayMode[] displayModes = this.idW.getDisplayModes();//Получить список возможных настроек экрана разрешение/частота /цвет
            setListSettingsDisplay(displayModes);
            // isFullScreenSupported Возвращает true, если GraphicsDevice поддерживает полноэкранный эксклюзивный режим.
            if (this.idW.isFullScreenSupported() && this.renderConfig.isFullscreen()) {//Создать размер полноэканного режима
                this.ier = setFullScreenDisplay(displayModes, this.renderConfig.getScreenWidth(), this.renderConfig.getScreenHeight(), this.renderConfig.getRefreshRate());
            }
            if (this.renderConfig.isFullscreen()) {
                if (this.ier == null) {//Полноэкранный режим не поддерживается, вместо этого выполняется в оконном режиме
                    logger.warn("Full-screen mode not supported, running in windowed mode instead.");
                } else {//Разрешение полноэкранного режима
                    logger.info("Client resolution: " + this.ier.getWidth() + GUIPrefAddon.C4817c.X + this.ier.getHeight() + " bpp:" + this.ier.getBitDepth() + " refresh:" + this.ier.getRefreshRate());
                }
            }
            this.frWidth = this.renderConfig.getScreenWidth();
            this.frHeight = this.renderConfig.getScreenHeight();
            this.refreshRate = this.renderConfig.getRefreshRate();//частота экрана
            //совпадает ли размер окна и разрешение экрана
            if (this.frWidth == this.idX.getWidth() && this.frHeight == this.idX.getHeight()) {
                this.iec = true;
            }
            this.idZ = new Frame("Taikodom");//Заголовок окна
            if (this.idU != null) {
                this.idZ.setIconImage(this.idU.get(1));
            }
            this.fullscreen = false;
            this.idZ.setResizable(false);//Устанавливает, изменяется ли этот кадр пользователем.
            this.idZ.setSize(this.frWidth, this.frHeight);//Задать размер окна
            if (this.ier != null) {//Если есть настройки Размер окна полноэкранного режима
                this.idZ.setUndecorated(true);//разрешает декорации для этого фрейма. // вызвать только в том случае, если кадр не отображается.
                this.fullscreen = true;
            } else if (this.iec) {
                this.idZ.setUndecorated(true);
            }
            if (!this.fullscreen) {
                Insets insets = this.idZ.getInsets();
                int width = (this.idX.getWidth() - insets.left) - insets.right;
                int height = (this.idX.getHeight() - insets.top) - insets.bottom;
                if (this.frWidth <= width) {
                    width = this.frWidth;
                }
                this.frWidth = width;
                this.frHeight = this.frHeight > height ? height : this.frHeight;
            }
            this.bpp = this.renderConfig.getBpp();//Глубина цвета 32-bit
            this.idZ.setVisible(true);
            this.idZ.setFocusable(false);
            this.idZ.enableInputMethods(false);//Отключает поддержку метода ввода для этого компонента.
            try {
                this.ied = new Robot();
            } catch (AWTException e) {
                e.printStackTrace();
            }
            this.ied.mouseMove(0, 0);//переместить курсор
            this.ied.setAutoDelay(0);
            this.ied.setAutoWaitForIdle(false);
            this.ddf = new C0658d(gLCapabilities);//создаём объект openGL
            this.ddf.setSize(this.frWidth, this.frHeight);//Задаём размер JGLDesktop
            if (this.iec) {
                this.ddf.getRootPane().setSize(this.frWidth, this.frHeight);
            }
            this.ddf.setAutoSwapBufferMode(false);//отключает автоматическую свопинг буфера для этой возможности.
            this.ddf.getContext().setSynchronized(true);
            this.ddf.enableInputMethods(true);//Включает поддержку метода ввода для этого компонента.
            this.glContext = this.ddf.getContext();
            if (this.fullscreen) {
                this.idZ.add(this.ddf);//Добавляем  openGL в окно
            } else {
                this.idZ.add(this.ddf);
            }
            this.idZ.addWindowListener(new C0657c());
            if (!this.idW.isFullScreenSupported() || this.ier == null) {
                m5716aF(this.frWidth, this.frHeight);
            } else {
                this.idW.setFullScreenWindow(this.idZ);
                if (this.idW.isDisplayChangeSupported()) {
                    this.idW.setDisplayMode(this.ier);
                    this.fullscreen = true;
                } else {
                    m5716aF(this.frWidth, this.frHeight);
                    logger.warn("Unable to change display mode, full-screen disabled.");
                }
            }
            if (this.fullscreen) {
                this.idZ.addWindowListener(new C0654a());
            }
            if (this.ddf.getContext().makeCurrent() == 0) {
                throw new RuntimeException("Unable to make GL context active");
            }
            this.ddf.requestFocus();
            this.ddf.getGL().glClear(DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ);
            return;
        }
        throw new RuntimeException("Invalid bpp: " + this.renderConfig.getBpp() + ". Taikodom needs 32 bpp.");
    }

    /**
     * Задать полноэкранный режим
     *
     * @param displayModeArr Списка допустимые параметры экрана для игры. Разрешение экрана и частота
     * @param i              Ширина
     * @param i2             Высота
     * @param i3             Частота
     * @return
     */
    /* renamed from: a */
    private DisplayMode setFullScreenDisplay(DisplayMode[] displayModeArr, int i, int i2, int i3) {
        DisplayMode displayMode = null;
        for (DisplayMode displayMode2 : displayModeArr) {
            if (displayMode2.getBitDepth() == 32 && displayMode2.getWidth() == i && displayMode2.getHeight() == i2 && displayMode2.getRefreshRate() >= i3 && (displayMode == null || displayMode.getRefreshRate() < i3)) {
                displayMode = displayMode2;
            }
        }
        if (displayMode == null) {
            for (DisplayMode displayMode3 : displayModeArr) {
                if (displayMode3.getBitDepth() == 32 && displayMode3.getWidth() >= i && displayMode3.getHeight() >= i2 && displayMode3.getRefreshRate() >= 60 && (displayMode == null || displayMode.getRefreshRate() < i3)) {
                    displayMode = displayMode3;
                }
            }
        }
        return displayMode;
    }

    /**
     * Заполнения списка допустимые параметры экрана для игры
     *
     * @param displayModeArr Cписок возможных настроек экрана разрешение/частота /цвет
     */
    /* renamed from: a */
    private void setListSettingsDisplay(DisplayMode[] displayModeArr) {
        boolean z;
        this.idY = new ArrayList<>();
        for (DisplayMode displayMode : displayModeArr) {
            if (displayMode.getWidth() >= 1024 && displayMode.getBitDepth() == 32 && displayMode.getRefreshRate() >= 60) {
                Vec3f vec3f = new Vec3f((float) displayMode.getWidth(), (float) displayMode.getHeight(), (float) displayMode.getRefreshRate());
                Iterator<Vec3f> it = this.idY.iterator();
                while (true) {
                    if (it.hasNext()) {//ПРоверка, что такого параметра нет ещё в списке
                        if (it.next().equals(vec3f)) {
                            z = true;
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
                if (!z) {
                    this.idY.add(vec3f);//Добавить в список
                }
            }
        }
    }


    /**
     * Установить иконки окна
     *
     * @param list Массив иконок
     */
    /* renamed from: j */
    public void mo3075j(List<Image> list) {
        this.idU = new ArrayList(list);
        if (this.idZ != null) {
            this.idZ.setIconImage(this.idU.get(1));//Задаём окну иконку
        }
    }

    /* renamed from: aF */
    private void m5716aF(int i, int i2) {
        try {
            EventQueue.invokeAndWait(new C0659e(this.idZ, i, i2, this.ddf));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getAntiAliasing() {
        return this.renderConfig.getAntiAliasing();
    }

    public void setAntiAliasing(int i) {
        this.renderConfig.setAntiAliasing(i);
    }

    public int aes() {
        return this.ddf.getWidth();
    }

    public int aet() {
        return this.ddf.getHeight();
    }

    public float aeg() {
        return this.idQ.egH;
    }

    /* renamed from: dp */
    public void mo3060dp(float f) {
        this.idQ.egH = f;
    }

    public float aed() {
        return this.idQ.egJ;
    }

    /* renamed from: do */
    public void mo3059do(float f) {
        this.idQ.egJ = f;
    }

    public void setAnisotropicFilter(float f) {
        this.renderConfig.setAnisotropicFilter(f);
    }

    /* renamed from: bb */
    public float mo3053bb() {
        return this.renderConfig.getAnisotropicFilter().floatValue();
    }

    public boolean aeu() {
        return this.iee;
    }

    public void close() {
        if (this.fullscreen) {
            this.idW.setDisplayMode(this.idX);
            this.idW.setFullScreenWindow((Window) null);
        }
        this.ddf.getContext().release();
        this.idZ.setVisible(false);
        this.idZ.dispose();
        this.ddf = null;
        this.idZ = null;
        SoundMiles.getInstance().shutdown();
    }

    public String aei() {
        return this.localization;
    }

    public void aez() {
        this.ief = true;
    }

    public void aeA() {
        this.ieg = true;
    }

    public void swapBuffers() {
        try {
            this.ddf.getDrawable().swapBuffers();
            if (this.ief) {
                this.ief = false;
                new FileControl("data/screenshots").getFile().mkdirs();
                Calendar instance = Calendar.getInstance();
                FileOutputStream fileOutputStream = new FileOutputStream(new FileControl("data/screenshots/screenshot_" + (String.valueOf(String.valueOf(String.valueOf(new StringBuilder().append(instance.get(1)).toString()) + "_" + instance.get(2)) + "_" + instance.get(5)) + "_" + instance.get(14)) + "." + "jpg").getFile());
                ImageIO.write(Screenshot.readToBufferedImage(this.frWidth, this.frHeight), "jpg", fileOutputStream);
                fileOutputStream.close();
            }
            if (this.ieg) {
                try {
                    this.ieg = false;
                    RenderTarget renderTarget = new RenderTarget();//Создание поверхности рисования для снимков HighRes
                    logger.info("Creating draw surface for HighRes screenshot");
                    renderTarget.createAsPBO(2048, 2048, this.renderView.getDrawContext());
                    renderTarget.bind(this.renderView.getDrawContext());
                    RenderTarget renderTarget2 = this.ieh.getRenderTarget();
                    this.ieh.setRenderTarget((RenderTarget) null);
                    ArrayList arrayList = new ArrayList(this.ieh.getPostProcessingFXs());
                    this.ieh.getPostProcessingFXs().clear();
                    Viewport viewport = new Viewport(this.ieh.getViewport());
                    this.ieh.setViewport(0, 0, 2048, 2048);
                    this.ieh.getRenderInfo().setClearColor(1.0f, 0.0f, 0.0f, 1.0f);
                    float aspect = this.ieh.getCamera().getAspect();
                    this.ieh.getCamera().setAspect(1.0f);
                    getGL().glDisable(3089);
                    logger.info("Rendering to screenshot surface");
                    this.renderView.render(this.ieh, ScriptRuntime.NaN, 0.0f);
                    this.ieh.setRenderTarget(renderTarget2);
                    this.ieh.setViewport(viewport);
                    this.ieh.getCamera().setAspect(aspect);
                    this.ieh.getPostProcessingFXs().addAll(arrayList);
                    new FileControl("data/screenshots").getFile().mkdirs();
                    Calendar instance2 = Calendar.getInstance();
                    String str = String.valueOf(String.valueOf(String.valueOf(new StringBuilder().append(instance2.get(1)).toString()) + "_" + instance2.get(2)) + "_" + instance2.get(5)) + "_" + instance2.get(14);
                    logger.info("Creating file screenshot_high_" + str + "." + "png");
                    FileOutputStream fileOutputStream2 = new FileOutputStream(new FileControl("data/screenshots/screenshot_high_" + str + "." + "png").getFile());
                    BufferedImage readToBufferedImage = Screenshot.readToBufferedImage(2048, 2048);
                    logger.info("Saving screenshot");
                    ImageIO.write(readToBufferedImage, "png", fileOutputStream2);
                    fileOutputStream2.close();
                    renderTarget.unbind(this.renderView.getDrawContext());
                    renderTarget.releaseReferences(getGL());
                    logger.info("Screenshot saved");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e2) {
        }
    }

    /* renamed from: kw */
    private boolean m5724kw(String str) {
        return this.rootpath.concat(str).exists();
    }

    /**
     * Загрузка адресов ресурсов из файлов .pro
     *
     * @param str Пример data/audio/interface/interface_sfx.pro
     */
    /* JADX INFO: finally extract failed */
    /* renamed from: bV */
    public void mo3049bV(String str) {
        if (str == null) {
            logger.debug("trying to loadStockDefinitions from null file!");
            return;
        }
        if (aei() != null) {
            String replaceAll = str.replaceAll("([.][^.]+)$", "_" + aei() + "$1");
            if (m5724kw(replaceAll)) {
                str = replaceAll;
            }
        }
        try {
            idM.acquire();
            if (this.loader.getStockFile(str) != null) {
                idM.release();
                return;
            }
            idM.release();
            logger.debug("loadStockDefinitions " + str);
            idM.acquire();
            try {
                this.loader.loadFile(str);
                dfm();
                idM.release();
            } catch (FileNotFoundException e) {
                System.err.println("StockDefinitions: Failed to load file: '" + str + "'");
                e.printStackTrace();
                idM.release();
            } catch (Throwable th) {
                idM.release();
                throw th;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        } catch (Throwable th2) {
            idM.release();
            throw th2;
        }
    }

    /**
     * Попытка загрузка локализованных текстур из includes
     *
     * @throws IOException
     */
    private void dfm() throws IOException {
        while (this.loader.getMissingIncludes().size() > 0) {
            String next = this.loader.getMissingIncludes().iterator().next();
            if (aei() != null) {
                String replaceAll = next.replaceAll("([.][^.]+)$", "_" + aei() + "$1");
                if (m5724kw(replaceAll)) {
                    next = replaceAll;
                }
            }
            this.loader.loadFile(next);
        }
    }

    /* renamed from: bT */
    public <T extends RenderAsset> T mo3047bT(String str) {
        T instantiateAsset;
        if (str == null) {
            logger.debug("null object name");
            return null;
        } else if (aei() == null || (instantiateAsset = this.loader.instantiateAsset(String.valueOf(str) + "_" + aei())) == null) {
            return this.loader.instantiateAsset(str);
        } else {
            return instantiateAsset;
        }
    }

    /**
     * Переместить указатель мыши в указанные координаты
     *
     * @param i
     * @param i2
     * @param z
     */
    /* renamed from: a */
    public void mo3002a(int i, int i2, boolean z) {
        Insets insets = this.idZ.getInsets();
        Rectangle bounds = this.idZ.getBounds();
        int i3 = insets.top + i2 + bounds.y;
        this.ied.mouseMove(insets.left + i + bounds.x, i3);
    }

    /* renamed from: a */
    public void mo3004a(C6296akI.C1925a aVar) {
        if (aVar != null) {
            this.idS.add(aVar);
        } else if (logger.isDebugEnabled()) {
            throw new IllegalArgumentException("Null steppable added to RenderModule.");
        }
    }

    /* renamed from: b */
    public void mo3044b(C6296akI.C1925a aVar) {
        this.idT.add(aVar);
    }

    public boolean step(float f) {
        ArrayList arrayList;
        long currentTimeMillis = SingletonCurrentTimeMilliImpl.currentTimeMillis();
        this.stepContext.reset();
        this.stepContext.setDeltaTime(f);
        synchronized (this.idV) {
            arrayList = new ArrayList(this.idV);
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            SceneView sceneView = (SceneView) it.next();
            if (sceneView.isEnabled()) {
                this.stepContext.setCamera(sceneView.getCamera());
                sceneView.getScene().step(this.stepContext);
                sceneView.getCamera().step(this.stepContext);
            }
        }
        this.iel.step(this.stepContext);
        this.idR.removeAll(this.idT);
        this.idR.addAll(this.idS);
        this.idS.clear();
        this.idT.clear();
        for (C6296akI.C1925a next : this.idR) {
            try {
                next.mo1968a(f, currentTimeMillis);
            } catch (Exception e) {
                this.idR.remove(next);
                System.err.println(e);
                e.printStackTrace();
                return true;
            }
        }
        return true;
    }

    /* renamed from: i */
    public void mo3074i(float f, float f2) {
        ArrayList arrayList;
        this.stepContext.setDeltaTime(f2);
        aey();
        synchronized (this.idV) {
            arrayList = new ArrayList(this.idV);
        }
        this.renderView.getDrawContext().reset();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            SceneView sceneView = (SceneView) it.next();
            if (sceneView.isEnabled()) {
                this.stepContext.setCamera(sceneView.getCamera());
                sceneView.getCamera().step(this.stepContext);
                this.renderView.render(sceneView, (double) f, f2);
            }
        }
        try {
            this.renderGui.render(this.iel);
        } catch (Exception e) {
            logger.warn("Exception while rendering HUD scene");
            e.printStackTrace();
        }
    }

    public List<SceneView> aep() {
        return this.idV;
    }

    /* renamed from: a */
    public void mo3005a(SceneView sceneView) {
        synchronized (this.idV) {
            int i = 0;
            for (SceneView priority : this.idV) {
                if (priority.getPriority() < sceneView.getPriority()) {
                    break;
                }
                i++;
            }
            this.idV.add(i, sceneView);
        }
        dfn();
    }

    /* renamed from: b */
    public void mo3046b(SceneView sceneView) {
        this.idV.remove(sceneView);
    }

    private void dfn() {
        ArrayList arrayList;
        synchronized (this.idV) {
            arrayList = new ArrayList(this.idV);
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            SceneView sceneView = (SceneView) it.next();
            sceneView.setSceneViewQuality(this.renderConfig.getSceneViewQuality());
            for (PostProcessingFX enabled : sceneView.getPostProcessingFXs()) {
                enabled.setEnabled(this.renderConfig.isPostProcessingFX());
            }
        }
    }

    public FilePath getRootPath() {
        return this.rootpath;
    }

    public int getShaderQuality() {
        return this.renderConfig.getShaderQuality();
    }

    public void setShaderQuality(int i) {
        this.renderConfig.setShaderQuality(i);
        dfn();
    }

    public FileSceneLoader aej() {
        return this.loader;
    }

    public JGLDesktop aee() {
        return this.ddf;
    }

    /* renamed from: bU */
    public RenderAsset mo3048bU(String str) {
        return mo3047bT(str);
    }

    public boolean getUseVBO() {
        return this.renderConfig.isUseVbo();
    }

    public float getLodQuality() {
        return this.renderConfig.getLodQuality();
    }

    public void setLodQuality(float f) {
        this.renderConfig.setLodQuality(f);
        dfn();
    }

    /* renamed from: eJ */
    public void mo3066eJ(int i) {
        this.renderConfig.setEnvFxQuality(i);
        C1255SZ.m9468Al(i);
        dfn();
    }

    public int aec() {
        return this.renderConfig.getEnvFxQuality();
    }

    public int ael() {
        return this.idY.size();
    }

    /* renamed from: eH */
    public Vec3f mo3064eH(int i) {
        return this.idY.get(i);
    }

    public float adX() {
        return this.idQ.egG;
    }

    /* renamed from: dm */
    public void mo3057dm(float f) {
        this.idQ.egG = f;
        if (this.iem != null) {
            this.iem.setGain(f);
        }
    }

    public float aeb() {
        return this.idQ.egI;
    }

    /* renamed from: dn */
    public void mo3058dn(float f) {
        this.idQ.egI = f;
        dfo();
    }

    private void dfo() {
        Scene adZ = adZ();
        synchronized (adZ.getChildren()) {
            for (SceneObject next : adZ.getChildren()) {
                if (next instanceof SPitchedSet) {
                    ((SPitchedSet) next).setGain(this.idQ.egI);
                }
            }
        }
    }

    /* renamed from: dq */
    public void mo3061dq(float f) {
        this.idQ.egF = f;
        if (this.ien != null) {
            this.ien.setGain(f);
        }
    }

    public float aek() {
        return this.idQ.egF;
    }

    public void adV() {
        this.ieh.getScene().removeAllChildren();
        aeh().removeAllChildren();
        aeh().addChild(this.ieo);
        this.impostorManager.clear();
        adZ().addChild(new MilesSoundListener());
    }

    /* renamed from: dr */
    public void mo3062dr(float f) {
        if (this.iem != null) {
            this.iem.fadeOut(f);
            this.iem = null;
        }
    }

    /* renamed from: ds */
    public void mo3063ds(float f) {
        if (this.ien != null) {
            this.ien.fadeOut(f);
            this.ien = null;
        }
    }

    /* renamed from: eI */
    public void mo3065eI(int i) {
        //Звуковые мины получают экземпляр для голодания
        SoundMiles.getInstance().prepareForStarving(i);
    }

    public void aex() {
        SoundMiles.getInstance().restoreFromStarving();
    }

    public GLContext aef() {
        return this.glContext;
    }

    public GL getGL() {
        return this.glContext.getGL();
    }

    public void setPostProcessingFX(boolean z) {
        this.renderConfig.setPostProcessingFX(z);
        dfn();
    }

    public boolean aem() {
        return this.renderConfig.isPostProcessingFX();
    }

    public void aev() {
        throw new RuntimeException("Must implement this");
    }

    /* renamed from: bW */
    public void mo3050bW(String str) {
        mo3045b(str, 0.0f);
    }

    /* renamed from: b */
    public void mo3045b(String str, float f) {
        logger.debug("playAmbienceMusic " + str);
        SoundObject soundObject = (SoundObject) mo3047bT(str);
        if (soundObject != null) {
            if (soundObject != this.iem) {
                this.iei.getScene().addChild(soundObject);
            } else {
                return;
            }
        }
        if (this.iem != null) {
            SoundObject soundObject2 = this.iem;
            mo3063ds(2.0f);
            this.iei.getScene().addChild(soundObject2);
        }
        this.iem = soundObject;
        if (this.iem != null) {
            this.iem.setGain(adX());
            this.iem.play();
            if (f > 0.0f) {
                this.iem.fadeIn(f);
            }
        }
    }

    /* renamed from: bX */
    public void mo3051bX(String str) {
        mo3054c(str, 0.0f);
    }

    /* renamed from: c */
    public void mo3054c(String str, float f) {
        logger.debug("playMultiLayerMusic " + str);
        SoundObject soundObject = (SoundObject) mo3047bT(str);
        if (soundObject != null) {
            if (soundObject != this.ien) {
                this.iei.getScene().addChild(soundObject);
            } else {
                return;
            }
        }
        if (this.ien != null) {
            SoundObject soundObject2 = this.ien;
            mo3063ds(2.0f);
            this.iei.getScene().addChild(soundObject2);
        }
        this.ien = soundObject;
        if (this.ien != null) {
            this.ien.setGain(aek());
            this.ien.play();
            if (f > 0.0f) {
                this.ien.fadeIn(f);
            }
        }
    }

    public List<SceneEvent> aeo() {
        if (this.iej == null) {
            this.iej = new ArrayList();
        }
        if (!(getRenderView() == null || getRenderView().getDrawContext().eventList == null)) {
            this.iej.addAll(getRenderView().getDrawContext().eventList);
            getRenderView().getDrawContext().eventList.clear();
        }
        return this.iej;
    }

    public void adW() {
        this.glContext.getGL().glFlush();
    }

    public void aeE() {
        this.glContext.getGL().glFinish();
    }

    private void setupGLEventListener() {
        if (this.eventManager == null) {//Предупреждение: нет EventManager. Слушатели не будут созданы
            System.out.println("Warning: no eventManager present. Listeners wont be created");
            return;
        }
        this.ddf.addGLEventListener(new C0660f());
        this.ddf.setFocusTraversalKeysEnabled(false);
    }

    private void setupControllers() {
        try {
            Controllers.create();
        } catch (LWJGLException e) {//Невозможно инициализировать контроллеры
            logger.debug("Unable to initialize controllers!");
        } finally {
            this.iek = true;
        }
    }

    public void aew() {
        if (this.iek) {
            Controllers.clearEvents();
            Controllers.poll();
            dfr();
        }
    }

    /**
     * Обработчик нажатий кнопок джостика
     */
    private void dfr() {
        int i;
        while (Controllers.next()) {
            if (Controllers.isEventButton()) {//Джостик кнопка
                m5723h(new C0963OA(Controllers.getEventControlIndex(), Controllers.getEventSource().isButtonPressed(Controllers.getEventControlIndex())));
            } else if (Controllers.isEventAxis()) {//Джостик оси
                int eventControlIndex = Controllers.getEventControlIndex();
                if (!Controllers.isEventXAxis()) {
                    if (!Controllers.isEventYAxis()) {
                        switch (eventControlIndex) {
                            case 0:
                            case 3:
                                i = 3;
                                break;
                            default:
                                i = 2;
                                break;
                        }
                    } else {
                        i = 1;
                    }
                } else {
                    i = 0;
                }
                m5723h(new C6009aeh(i, Controllers.getEventSource().getAxisValue(Controllers.getEventControlIndex())));
            } else if (Controllers.isEventPovX() || Controllers.isEventPovY()) {
                m5723h(new C0366Et(C1108QK.m8807n(Controllers.getEventSource().getPovX(), Controllers.getEventSource().getPovY()), true));
            }
        }
    }

    /* renamed from: h */
    private void m5723h(Object obj) {
        this.eventManager.mo13975h(obj);
    }

    public GuiScene aeh() {
        return this.iel;
    }

    public RenderView getRenderView() {
        return this.renderView;
    }

    public StepContext aer() {
        return this.stepContext;
    }

    public float aeq() {
        return ((float) this.frWidth) / ((float) this.frHeight);
    }

    /* renamed from: a */
    public void mo3007a(RenderTask renderTask) {
        synchronized (this.ghr) {
            if (!this.ghr.contains(renderTask)) {
                this.ghr.add(renderTask);
            }
        }
    }

    public void aey() {
        ArrayList<RenderTask> arrayList;
        synchronized (this.ghr) {
            arrayList = new ArrayList<>(this.ghr);
            this.ghr.clear();
        }
        for (RenderTask renderTask : arrayList) {
            try {
                renderTask.run(this.renderView);
            } catch (Exception e) {
                logger.error("Ignoring erroneous render task: " + renderTask.getClass(), e);
            }
        }
    }

    public void setTextureQuality(int i) {
        this.renderConfig.setTextureQuality(i);
        ImageLoader.setMaxMipMapSize(this.renderConfig.getTextureQuality());
    }

    /* renamed from: bY */
    public SceneView mo3052bY(String str) {
        for (SceneView next : this.idV) {
            if (next.getHandle() != null && next.getHandle().equals(str)) {
                return next;
            }
        }
        return null;
    }

    /**
     * Установить видео для проигрывания в Эксклюзивный видеопроигрыватель
     *
     * @param video
     */
    /* renamed from: a */
    public void mo3006a(Video video) {
        this.ieq.playVideo(video);
    }

    public void aeD() {
        this.ieq.stopVideo();
    }

    /**
     * Проигрывается ли видео
     *
     * @return
     */
    public boolean aeB() {
        return this.ieq.isPlaying();
    }

    /**
     * Начать проигрование видео заставки
     */
    public void aeC() {
        this.ieq.renderFrame(this.renderView.getDrawContext());
    }

    public int getRefreshRate() {
        return this.refreshRate;
    }

    /**
     * Получить окно клиента
     *
     * @return
     */
    public Frame dfs() {
        return this.idZ;
    }

    /* renamed from: a.JR$b */
    /* compiled from: a */
    private class C0656b {
        public float egF;
        public float egG;
        public float egH;
        public float egI;
        public float egJ;

        private C0656b() {
        }

        /* synthetic */ C0656b(EngineGraphics jr, C0656b bVar) {
            this();
        }
    }

    /* renamed from: a.JR$d */
    /* compiled from: a */
    class C0658d extends JGLDesktop {
        private static final long serialVersionUID = 3731193311504296919L;

        C0658d(GLCapabilities gLCapabilities) {
            super(gLCapabilities);
        }

        public boolean isFocusCycleRoot() {
            return true;
        }

        public void paint(Graphics graphics) {
        }
    }

    /* renamed from: a.JR$c */
    /* compiled from: a */
    class C0657c extends WindowAdapter {
        C0657c() {
        }

        public void windowClosing(WindowEvent windowEvent) {
            EngineGraphics.this.iee = true;
        }

        public void windowClosed(WindowEvent windowEvent) {
            EngineGraphics.this.iee = true;
        }

        public void windowOpened(WindowEvent windowEvent) {
            super.windowOpened(windowEvent);
            EngineGraphics.this.idZ.invalidate();//флаг - требует обновления макета
        }
    }

    /* renamed from: a.JR$a */
    class C0654a extends WindowAdapter {
        C0654a() {
        }

        public void windowActivated(WindowEvent windowEvent) {
            new C0655a("FullScreen").start();
        }

        public void windowDeactivated(WindowEvent windowEvent) {
            if (EngineGraphics.this.idW.isDisplayChangeSupported()) {
                EngineGraphics.this.idW.setDisplayMode(EngineGraphics.this.idX);
            }
        }

        /* renamed from: a.JR$a$a */
        class C0655a extends Thread {
            C0655a(String str) {
                super(str);
            }

            public void run() {
                try {
                    Thread.sleep(2000);
                    EngineGraphics.this.idW.setDisplayMode(EngineGraphics.this.idX);
                    Thread.sleep(2000);
                    EngineGraphics.this.idW.setDisplayMode(EngineGraphics.this.ier);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /* renamed from: a.JR$e */
    /* compiled from: a */
    class C0659e implements Runnable {
        private final /* synthetic */ Frame iFg;
        private final /* synthetic */ int iFh;
        private final /* synthetic */ int iFi;
        private final /* synthetic */ JGLDesktop iFj;

        C0659e(Frame frame, int i, int i2, JGLDesktop jGLDesktop) {
            this.iFg = frame;
            this.iFh = i;
            this.iFi = i2;
            this.iFj = jGLDesktop;
        }

        public void run() {
            Insets insets = this.iFg.getInsets();
            this.iFg.setSize(this.iFh + insets.left + insets.right, this.iFi + insets.top + insets.bottom);
            this.iFj.setLocation(insets.left, insets.top);
            this.iFj.setSize(this.iFh, this.iFi);
            if (EngineGraphics.this.iec) {
                this.iFj.getRootPane().setSize(this.iFh, this.iFi);
            }
        }
    }

    /* renamed from: a.JR$f */
    /* compiled from: a */
    class C0660f implements GLEventListener {
        C0660f() {
        }

        /**
         * Он вызывается объектом интерфейса GLAutoDrawable во время первой перерисовки после изменения размера компонента.
         * Он также вызывается всякий раз, когда положение компонента в окне изменяется.
         *
         * @param glAutoDrawable Поверхность для рисования для команд OpenGL.
         * @param x
         * @param y
         * @param width
         * @param height
         */
        public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
            m5755c(width, height, 32, false);
        }

        /* renamed from: c */
        private void m5755c(int i, int i2, int i3, boolean z) {
            EngineGraphics.this.renderConfig.setScreenWidth(i);
            EngineGraphics.this.renderConfig.setScreenHeight(i);
            EngineGraphics.this.renderConfig.setBpp(i3);
            EngineGraphics.this.renderConfig.setFullscreen(z);
            EngineGraphics.this.frWidth = i;
            EngineGraphics.this.frHeight = i2;
            EngineGraphics.this.bpp = i3;
            EngineGraphics.this.fullscreen = z;
        }

        /**
         * этот метод содержит логику, используемую для рисования графических элементов с использованием OpenGL API.
         *
         * @param gLAutoDrawable Поверхность для рисования OpenGL и примитивы рисования(линии треугольники).
         */
        public void display(GLAutoDrawable gLAutoDrawable) {
        }

        /**
         * @param gLAutoDrawable Поверхность для рисования для команд OpenGL.
         * @param var2
         * @param var3
         */
        public void displayChanged(GLAutoDrawable gLAutoDrawable, boolean var2, boolean var3) {
        }

        /**
         * Он вызывается объектом интерфейса GLAutoDrawable сразу после инициализации GLContext OpenGL.
         *
         * @param gLAutoDrawable Поверхность для рисования OpenGL и примитивы рисования(линии треугольники).
         */
        public void init(GLAutoDrawable gLAutoDrawable) {
        }
    }
}
