package logic.render;

import game.engine.C6082agC;
import logic.res.ILoaderImageInterface;
import logic.res.LoadFonts;
import logic.res.code.SoftTimer;
import logic.swing.C5457aKb;
import logic.thred.LogPrinter;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.C6758atC;
import p001a.SingletonCurrentTimeMilliImpl;
import taikodom.render.DrawContext;
import taikodom.render.JGLDesktop;
import taikodom.render.RenderView;
import taikodom.render.SceneEvent;
import taikodom.render.graphics2d.RGraphics2;
import taikodom.render.graphics2d.RGraphicsContext;
import taikodom.render.loader.provider.FilePath;
import taikodom.render.textures.DDSLoader;
import taikodom.render.textures.RenderTarget;
import taikodom.render.textures.SimpleTexture;

import javax.media.opengl.GL;
import javax.swing.*;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;

/* renamed from: a.HL */
/* compiled from: a */
public class GUIModule {
    public static final String aGx = "GUIModule";
    static LogPrinter logger = LogPrinter.m10275K(GUIModule.class);
    /* access modifiers changed from: private */
    public RGraphics2 hYP;
    /* access modifiers changed from: private */
    public RGraphics2 hYQ;
    Rectangle ddg = new Rectangle();
    FilePath rootPathRender;
    boolean hYN = false;
    /* renamed from: Th */
    private ILoaderImageInterface f658Th;
    private long fKP;
    private long fKQ;
    private LoadFonts hYO;
    private boolean hYR = true;

    /* renamed from: lV */
    private IEngineGraphics engineGraphics;
    private RenderTarget renderTarget;

    /* renamed from: rv */
    private RenderView f660rv;
    private SimpleTexture texture;

    public GUIModule() {
        C6082agC.m21936a(new C6082agC(this));
    }

    /* renamed from: a */
    public void setProperties(FilePath rootPathRender, String str, String str2, IEngineGraphics engineGraphics, SoftTimer abg) {
        this.rootPathRender = rootPathRender;
        this.engineGraphics = engineGraphics;
        C5457aKb.getSharedInstance();
        JGLDesktop aee = this.engineGraphics.aee();
        RepaintManager.currentManager(aee).setDoubleBufferingEnabled(false);
        aee.getRootPane().setDoubleBuffered(false);
        aee.getRootPane().setOpaque(false);
        aee.getRootPane().setBackground(new Color(0, 0, 0, 0));
        aee.getContentPane().setBackground(new Color(0, 0, 0, 0));
        aee.getLayeredPane().setBackground(new Color(0, 0, 0, 0));
        aee.getLayeredPane().setOpaque(false);
        aee.setBackground(new Color(0, 0, 0, 0));
        this.f660rv = new RenderView(this.engineGraphics.aef());
        this.f660rv.setViewport(0, 0, aee.getWidth(), aee.getHeight());
        this.f660rv.setGL(aee.getGL());
        this.f660rv.getDrawContext().setMaxShaderQuality(this.engineGraphics.getShaderQuality());
        RGraphicsContext rGraphicsContext = new RGraphicsContext(this.f660rv.getDrawContext());
        this.hYP = new RGraphics2(rGraphicsContext);
        this.hYQ = new RGraphics2(rGraphicsContext);
        this.hYO = new LoadFonts(this);
        C6758atC atc = new C6758atC(this.engineGraphics);
        atc.mo16003j(this.rootPathRender);
        this.f658Th = atc;
        ToolTipManager.sharedInstance().setInitialDelay(700);
        ToolTipManager.sharedInstance().setDismissDelay(Integer.MAX_VALUE);
        ToolTipManager.sharedInstance().setReshowDelay(500);
    }

    public void dispose() {
    }

    public int getScreenWidth() {
        return ddH().getWidth();
    }

    public int getScreenHeight() {
        return ddH().getHeight();
    }

    public boolean step(float f) {
        if (!this.hYR) {
            return true;
        }
        DrawContext drawContext = this.f660rv.getDrawContext();
        drawContext.setMaxShaderQuality(this.engineGraphics.getShaderQuality());
        GL gl = drawContext.getGl();
        gl.glMatrixMode(5888);
        gl.glLoadIdentity();
        gl.glMatrixMode(5889);
        gl.glLoadIdentity();
        JGLDesktop aee = this.engineGraphics.aee();
        gl.glViewport(0, 0, aee.getWidth(), aee.getHeight());
        gl.glDisable(2929);
        gl.glOrtho(ScriptRuntime.NaN, (double) aee.getWidth(), (double) aee.getHeight(), ScriptRuntime.NaN, -1.0d, 1.0d);
        gl.glMatrixMode(5888);
        gl.glDisable(2884);
        drawContext.setCurrentFrame(10);
        if (!this.hYN) {
            if (this.renderTarget == null) {
                this.renderTarget = new RenderTarget();
                this.renderTarget.setWidth(getScreenWidth());
                this.renderTarget.setHeight(getScreenHeight());
                this.renderTarget.create(getScreenWidth(), getScreenHeight(), false, drawContext, true);
                this.texture = new SimpleTexture();
                this.texture.setName("Simple texture for GUI");
                this.texture.createEmptyRGBA(getScreenWidth(), getScreenHeight(), 32);
                this.renderTarget.bind(drawContext);
                try {
                    gl.glDisable(3089);
                    gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
                    gl.glClear(DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ);
                    this.renderTarget.copyBufferToTexture(drawContext);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    this.renderTarget.unbind(drawContext);
                }
            }
            RepaintManagerWraper currentManager = (RepaintManagerWraper) RepaintManager.currentManager((JComponent) null);
            if (!(this.ddg.width == 0 || this.ddg.height == 0)) {
                if (drawContext.getMaxShaderQuality() <= 1) {
                    this.renderTarget.bind(drawContext);
                }
                try {
                    int height = (aee.getHeight() - this.ddg.y) - this.ddg.height;
                    gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
                    gl.glClear(DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ);
                    gl.glMatrixMode(5888);
                    gl.glLoadIdentity();
                    gl.glMatrixMode(5889);
                    gl.glLoadIdentity();
                    gl.glViewport(0, 0, aee.getWidth(), aee.getHeight());
                    gl.glDisable(2929);
                    gl.glDisable(3042);
                    gl.glOrtho(ScriptRuntime.NaN, (double) aee.getWidth(), (double) aee.getHeight(), ScriptRuntime.NaN, -1.0d, 1.0d);
                    gl.glMatrixMode(5888);
                    this.hYP.getGuiScene().draw(this.renderTarget);
                    this.texture.copyFromCurrentBuffer(gl, this.ddg.x, height, this.ddg.x, height, this.ddg.width, this.ddg.height);
                    if (drawContext.getMaxShaderQuality() <= 1) {
                        this.renderTarget.unbind(drawContext);
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    currentManager.markCompletelyDirty(aee.getRootPane());
                    if (drawContext.getMaxShaderQuality() <= 1) {
                        this.renderTarget.unbind(drawContext);
                    }
                } catch (Throwable th) {
                    if (drawContext.getMaxShaderQuality() <= 1) {
                        this.renderTarget.unbind(drawContext);
                    }
                    throw th;
                }
            }
            long cjJ = currentManager.cjJ();
            if (cjJ == this.fKP) {
                if (this.fKQ + 3000 < SingletonCurrentTimeMilliImpl.currentTimeMillis()) {
                    this.fKQ = SingletonCurrentTimeMilliImpl.currentTimeMillis();
                    currentManager.markCompletelyDirty(aee.getRootPane());
                }
            } else if (cjJ != this.fKP) {
                this.fKP = cjJ;
                this.hYN = true;
                try {
                    SwingUtilities.invokeAndWait(new C0518a(this, aee, this.ddg, currentManager, (C0518a) null));
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                } catch (InvocationTargetException e4) {
                    e4.printStackTrace();
                }
            }
        }
        return false;
    }

    public void render() {
        if (this.hYR) {
            DrawContext drawContext = this.f660rv.getDrawContext();
            drawContext.setMaxShaderQuality(this.engineGraphics.getShaderQuality());
            GL gl = drawContext.getGl();
            gl.glMatrixMode(5888);
            gl.glLoadIdentity();
            gl.glMatrixMode(5889);
            gl.glLoadIdentity();
            JGLDesktop aee = this.engineGraphics.aee();
            gl.glViewport(0, 0, aee.getWidth(), aee.getHeight());
            gl.glDisable(2929);
            gl.glOrtho(ScriptRuntime.NaN, (double) aee.getWidth(), (double) aee.getHeight(), ScriptRuntime.NaN, -1.0d, 1.0d);
            gl.glMatrixMode(5888);
            if (this.renderTarget != null) {
                this.texture.bind(drawContext);
                gl.glEnable(3042);
                gl.glBlendFunc(1, 771);
                gl.glDisableClientState(32884);
                gl.glDisableClientState(32888);
                gl.glEnable(3553);
                gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
                gl.glBegin(7);
                float height = ((float) this.renderTarget.getHeight()) / ((float) this.renderTarget.getTexture().getSizeY());
                gl.glTexCoord2f(0.0f, height);
                gl.glVertex2f(0.0f, 0.0f);
                gl.glTexCoord2f(0.0f, 0.0f);
                gl.glVertex2f(0.0f, (float) this.renderTarget.getHeight());
                float width = ((float) this.renderTarget.getWidth()) / ((float) this.renderTarget.getTexture().getSizeX());
                gl.glTexCoord2f(width, 0.0f);
                gl.glVertex2f((float) this.renderTarget.getWidth(), (float) this.renderTarget.getHeight());
                gl.glTexCoord2f(width, height);
                gl.glVertex2f((float) this.renderTarget.getWidth(), 0.0f);
                gl.glEnd();
                gl.glBindTexture(3553, 0);
            }
        }
    }

    public List<SceneEvent> aeo() {
        if (this.f660rv.getDrawContext().eventList == null) {
            this.f660rv.getDrawContext().eventList = new LinkedList();
        }
        return this.f660rv.getDrawContext().eventList;
    }

    public ILoaderImageInterface adz() {
        return this.f658Th;
    }

    public LoadFonts ddG() {
        return this.hYO;
    }

    public JGLDesktop ddH() {
        return this.engineGraphics.aee();
    }

    public Cursor ddI() {
        return ddH().getCursor();
    }

    /* renamed from: a */
    public void mo2524a(Cursor cursor) {
        ddH().setCursor(cursor);
    }

    public boolean ddJ() {
        return this.hYR;
    }

    /* renamed from: jA */
    public void mo2534jA(boolean z) {
        this.hYR = z;
    }

    /* renamed from: a.HL$a */
    private final class C0518a implements Runnable {
        private JGLDesktop ddf;
        private Rectangle ddg;
        private RepaintManagerWraper ddh;

        /* synthetic */ C0518a(GUIModule hl, JGLDesktop jGLDesktop, Rectangle rectangle, RepaintManagerWraper amc, C0518a aVar) {
            this(jGLDesktop, rectangle, amc);
        }

        private C0518a(JGLDesktop jGLDesktop, Rectangle rectangle, RepaintManagerWraper amc) {
            this.ddf = jGLDesktop;
            this.ddg = rectangle;
            this.ddh = amc;
        }

        /* JADX INFO: finally extract failed */
        public void run() {
            GUIModule.this.hYQ.reset();
            GUIModule.this.hYQ.getContext().startRendering();
            try {
                Rectangle a = this.ddh.mo14766a(this.ddf.getRootPane(), GUIModule.this.hYQ.create());
                this.ddg.x = a.x;
                this.ddg.y = a.y;
                this.ddg.width = a.width;
                this.ddg.height = a.height;
                GUIModule.this.hYQ.getContext().stopRendering();
                GUIModule.this.hYP = GUIModule.this.hYQ;
                GUIModule.this.hYN = false;
            } catch (Throwable th) {
                GUIModule.this.hYQ.getContext().stopRendering();
                throw th;
            }
        }
    }
}
