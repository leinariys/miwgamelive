package logic.res;

import logic.render.GUIModule;
import p001a.C0933Nh;
import taikodom.render.graphics2d.C5885acN;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Загрузчик шрифтов
 */
/* renamed from: a.pq */
/* compiled from: a */
public final class LoadFonts implements ILoadFonts {
    private final GUIModule iZu;
    ThreadLocal<C3273a> iZt = new C0933Nh(this);
    private Map<String, Font> iZv = new HashMap();
    private Map<C3273a, Font> map = new HashMap();

    public LoadFonts(GUIModule hl) {
        this.iZu = hl;
    }

    /* renamed from: m */
    public Font mo21225m(String str, int i, int i2) {
        return mo13453b(str, i, i2, false);
    }

    /* renamed from: b */
    public Font mo13453b(String str, int i, int i2, boolean z) {
        C3273a aVar = this.iZt.get();
        aVar.mo21227a(str, i, i2, z);
        Font font = this.map.get(aVar);
        if (font != null) {
            return font;
        }
        String str2 = "";
        if ((i2 & 1) > 0) {
            str2 = String.valueOf(str2) + "-Bold";
        }
        if ((i2 & 2) > 0) {
            str2 = String.valueOf(str2) + "-Italic";
        }
        String str3 = this.iZu.rootPathRender + "/data/fonts/" + str + str2 + ".ttf";
        Font font2 = this.iZv.get(str3);
        if (font2 == null) {
            File file = new File(str3);
            try {
                if (file.exists()) {
                    font2 = Font.createFont(0, file);
                } else {
                    File file2 = new File(this.iZu.rootPathRender + "/data/fonts/" + str + ".ttf");
                    if (file2.exists()) {
                        font2 = Font.createFont(0, file2);
                    } else {
                        File file3 = new File(this.iZu.rootPathRender + "/data/fonts/" + str.replaceAll("-.*$", "") + ".ttf");
                        font2 = file3.exists() ? Font.createFont(0, file3) : new Font(str, i2, i);
                    }
                }
            } catch (FontFormatException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        Font deriveFont = font2.deriveFont(i2, (float) i);
        C5885acN.put(deriveFont, "antialiased", Boolean.valueOf(z));
        this.map.put(aVar.mo21226UZ(), deriveFont);
        return deriveFont;
    }

    /* renamed from: a.pq$a */
    static class C3273a {
        boolean aRD;
        int height;
        String name;
        int style;

        C3273a() {
        }

        public boolean equals(Object obj) {
            C3273a aVar = (C3273a) obj;
            return this.name.equals(aVar.name) && this.height == aVar.height && this.style == aVar.style && this.aRD == aVar.aRD;
        }

        /* renamed from: a */
        public void mo21227a(String str, int i, int i2, boolean z) {
            this.name = str;
            this.height = i;
            this.style = i2;
            this.aRD = z;
        }

        /* renamed from: UZ */
        public C3273a mo21226UZ() {
            C3273a aVar = new C3273a();
            aVar.mo21227a(this.name, this.height, this.style, this.aRD);
            return aVar;
        }

        public int hashCode() {
            return ((this.aRD ? 1 : 0) + 255) ^ (this.style + ((this.name.hashCode() * 31) + (this.height * 31)));
        }
    }
}
