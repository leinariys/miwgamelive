package logic.res.scene;

import com.hoplon.geometry.Color;
import game.geometry.Matrix4fWrap;
import game.geometry.Vector2fWrap;

/* renamed from: a.rm */
/* compiled from: a */
public interface C3510rm extends C6980axt {
    /* renamed from: a */
    void mo21803a(float f, float f2, float f3, float f4, float f5, float f6);

    /* renamed from: a */
    void mo21804a(float f, float f2, float f3, float f4, boolean z);

    /* renamed from: a */
    void mo21805a(aRZ arz);

    /* renamed from: a */
    void mo21806a(C3038nJ nJVar, float f, float f2);

    /* renamed from: a */
    void mo21807a(C3038nJ nJVar, float f, float f2, float f3, float f4);

    /* renamed from: a */
    void mo21808a(C3038nJ nJVar, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8);

    /* renamed from: a */
    void mo21809a(C3510rm rmVar);

    /* renamed from: a */
    void mo21810a(C3510rm rmVar, float f, float f2, float f3, float f4);

    /* renamed from: a */
    void mo21811a(Color color);

    /* renamed from: a */
    void mo21812a(String str, float f, float f2, float f3);

    /* renamed from: a */
    void mo21813a(String str, float f, float f2, boolean z);

    /* renamed from: a */
    void mo21814a(short s);

    /* renamed from: a */
    void mo21815a(float[] fArr, int i);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    Vector2fWrap mo21816b(String str, int i);

    /* renamed from: b */
    void mo21817b(float f, float f2);

    /* renamed from: b */
    void mo21818b(float f, float f2, float f3, float f4);

    /* renamed from: b */
    void mo21819b(float f, float f2, float f3, float f4, float f5, float f6);

    /* renamed from: b */
    void mo21820b(Color color);

    /* renamed from: b */
    void mo21821b(String str, float f, float f2);

    /* renamed from: b */
    void mo21822b(String str, float f, float f2, boolean z);

    /* renamed from: c */
    void mo21823c(float f, float f2, float f3, float f4);

    /* renamed from: c */
    void mo21824c(float f, float f2, float f3, float f4, float f5, float f6);

    /* renamed from: d */
    void mo21825d(float f, float f2, float f3, float f4);

    /* renamed from: d */
    void mo21826d(float f, float f2, float f3, float f4, float f5, float f6);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    void drawString(String str, float f, float f2);

    /* renamed from: e */
    void mo21828e(float f, float f2, float f3, float f4);

    /* renamed from: f */
    void mo21829f(float f, float f2, float f3, float f4);

    /* renamed from: fA */
    void mo21830fA();

    /* renamed from: fw */
    Color mo21831fw();

    /* renamed from: fx */
    Color mo21832fx();

    /* renamed from: fy */
    aRZ mo21833fy();

    /* renamed from: fz */
    short mo21834fz();

    /* renamed from: g */
    void mo21835g(float f, float f2, float f3, float f4);

    float getAlpha();

    void setAlpha(float f);

    float getLineWidth();

    void setLineWidth(float f);

    Matrix4fWrap getTransform();

    void setTransform(Matrix4fWrap ajk);

    /* renamed from: h */
    void mo21838h(float f, float f2, float f3, float f4);

    /* renamed from: i */
    void mo21839i(float f, float f2, float f3, float f4);

    boolean isClipped();

    /* renamed from: j */
    void mo21841j(float f, float f2, float f3, float f4);

    void releaseReferences();

    void reset();

    /* renamed from: y */
    Vector2fWrap mo21844y(String str);

    /* renamed from: z */
    Vector2fWrap mo21845z(String str);
}
