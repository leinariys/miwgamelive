package logic.res.scene;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.abf  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5851abf extends C6980axt {
    /* renamed from: a */
    void mo12497a(C5415aIl ail, float f);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: d */
    void mo12498d(Vec3f vec3f, Vec3f vec3f2, float f);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    boolean isNeedToStep();

    void releaseReferences();
}
