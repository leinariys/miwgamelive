package logic.res.scene;

import com.hoplon.geometry.Color;

/* renamed from: a.aVp  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5757aVp extends C5759aVr {
    /* renamed from: a */
    void mo11945a(int i, Color color);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    int dcx();

    /* renamed from: dy */
    C3087nb mo764dy();

    float[] getData();

    int getDepth();

    int getSizeX();

    int getSizeY();

    /* renamed from: yc */
    Color mo11951yc(int i);
}
