package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;

/* renamed from: a.aBN */
/* compiled from: a */
public interface aBN extends C6980axt {
    /* renamed from: a */
    boolean mo7879a(Vec3f vec3f, Vec3f vec3f2, float f);

    /* renamed from: a */
    boolean mo7880a(Vec3f vec3f, Vec3f vec3f2, float f, boolean z);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: d */
    void mo7881d(C1891ah ahVar);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo765dz();

    /* renamed from: gO */
    BoundingBox mo766gO();

    /* renamed from: gT */
    void mo2298gT();

    float getMinimumScreenSize();

    void setMinimumScreenSize(float f);

    /* renamed from: lc */
    C1891ah mo7883lc();

    void releaseReferences();
}
