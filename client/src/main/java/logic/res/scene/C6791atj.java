package logic.res.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vector2fWrap;

/* renamed from: a.atj  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6791atj extends C6000aeY {
    /* renamed from: V */
    void mo16135V(Color color);

    /* renamed from: W */
    void mo16136W(Color color);

    /* renamed from: a */
    C2036bO mo16137a(Vector2fWrap aka, long[] jArr, int i);

    /* renamed from: a */
    C2036bO mo16138a(Vec3f vec3f, Vec3f vec3f2, long[] jArr, int i);

    /* renamed from: a */
    void mo16139a(C0676Jc jc);

    /* renamed from: a */
    void mo16140a(aMF amf);

    /* renamed from: a */
    void mo16141a(C5739aUx aux);

    /* renamed from: a */
    void mo16142a(C6100agU agu);

    /* renamed from: a */
    void mo13079a(C2036bO bOVar);

    /* renamed from: a */
    void mo16143a(C2609hX hXVar);

    /* renamed from: a */
    void mo16144a(Color color, Vec3f vec3f, float f, int i, float f2, int i2);

    /* renamed from: aW */
    long mo759aW();

    void addColorIntensity(float f);

    /* renamed from: ax */
    C5739aUx mo16146ax(Vec3f vec3f);

    /* renamed from: b */
    C2036bO mo16147b(Vector2fWrap aka, long[] jArr, int i);

    /* renamed from: b */
    void mo16148b(C5739aUx aux);

    /* renamed from: b */
    void mo16149b(C6100agU agu);

    /* renamed from: b */
    void mo16150b(boolean z, boolean z2);

    long bZL();

    void bZM();

    long bZN();

    boolean bZO();

    boolean bZP();

    boolean bZQ();

    boolean bZR();

    boolean bZS();

    boolean bZT();

    boolean bZU();

    boolean bZV();

    boolean bZW();

    boolean bZX();

    boolean bZY();

    boolean bZZ();

    /* renamed from: c */
    void mo13081c(C2036bO bOVar);

    Vec3f caA();

    float caB();

    boolean caC();

    boolean caD();

    void caE();

    void caF();

    aMF caa();

    Color cab();

    int cac();

    boolean cad();

    void cae();

    C1710ZI caf();

    C0676Jc cag();

    void cah();

    void cai();

    boolean caj();

    boolean cak();

    long cal();

    long cam();

    long can();

    long cao();

    long cap();

    long caq();

    long car();

    long cas();

    long cat();

    long cau();

    long cav();

    long caw();

    long cax();

    long cay();

    float caz();

    void createPartitioningStructure();

    /* renamed from: d */
    C2036bO mo16199d(Vector2fWrap aka);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: e */
    C2036bO mo16200e(Vector2fWrap aka);

    /* renamed from: e */
    void mo16201e(float f, int i);

    /* renamed from: eD */
    void mo16202eD(boolean z);

    /* renamed from: eE */
    void mo16203eE(boolean z);

    /* renamed from: eF */
    void mo16204eF(boolean z);

    /* renamed from: eG */
    void mo16205eG(boolean z);

    /* renamed from: eH */
    void mo16206eH(boolean z);

    /* renamed from: eI */
    void mo16207eI(boolean z);

    /* renamed from: eJ */
    void mo16208eJ(boolean z);

    /* renamed from: eK */
    void mo16209eK(boolean z);

    /* renamed from: eL */
    void mo16210eL(boolean z);

    /* renamed from: eM */
    void mo16211eM(boolean z);

    /* renamed from: eN */
    void mo16212eN(boolean z);

    /* renamed from: eO */
    void mo16213eO(boolean z);

    /* renamed from: eP */
    void mo16214eP(boolean z);

    /* renamed from: eQ */
    void mo16215eQ(boolean z);

    /* renamed from: eR */
    void mo16216eR(boolean z);

    /* renamed from: eS */
    void mo16217eS(boolean z);

    Color getClearColor();

    float getColorIntensity();

    boolean getEditMode();

    long getNumObjectsStepped();

    /* renamed from: h */
    void mo16222h(C6980axt axt);

    /* renamed from: i */
    void mo16223i(C6980axt axt);

    boolean isCullOutSmallObjects();

    void setCullOutSmallObjects(boolean z);

    boolean isEnabled();

    /* renamed from: jr */
    void mo16226jr(float f);

    /* renamed from: js */
    void mo16227js(float f);

    /* renamed from: jt */
    void mo16228jt(float f);

    /* renamed from: l */
    void mo16229l(C2036bO bOVar);

    /* renamed from: qN */
    C6100agU mo16230qN(int i);

    /* renamed from: qO */
    void mo16231qO(int i);

    /* renamed from: qP */
    C2609hX mo16232qP(int i);

    /* renamed from: qQ */
    C6980axt mo16233qQ(int i);

    void releaseReferences();

    void removeAllChildren();

    void setAllowImpostors(boolean z);

    void setClearBuffers(boolean z, boolean z2, boolean z3);

    void setShowGrid(boolean z);

    void setUseTkdWorld(boolean z);

    void setViewport(int i, int i2, int i3, int i4);

    void step(float f);
}
