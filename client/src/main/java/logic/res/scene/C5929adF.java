package logic.res.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vector2fWrap;

/* renamed from: a.adF  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5929adF extends C6980axt {
    /* renamed from: a */
    void mo12750a(C2036bO bOVar, C6791atj atj);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: aw */
    void mo12751aw(Vec3f vec3f);

    C3038nJ bQT();

    C2036bO bQV();

    C6791atj bRx();

    Vec3f bRy();

    float bRz();

    /* renamed from: d */
    void mo12757d(C3038nJ nJVar);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    Vector2fWrap getCenterBillboardSize();

    void setCenterBillboardSize(Vector2fWrap aka);

    Color getCenterColor();

    void setCenterColor(Color color);

    float getMarkSize();

    /* renamed from: je */
    void mo12761je(float f);

    /* renamed from: jf */
    void mo9079jf(float f);

    /* renamed from: q */
    void mo12762q(float f, float f2);

    void releaseReferences();
}
