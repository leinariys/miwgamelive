package logic.res.scene;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.aCY */
/* compiled from: a */
public interface aCY extends C6980axt {
    /* renamed from: a */
    void mo8195a(float f, float f2, Vec3f vec3f, Vec3f vec3f2, float f3);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: bg */
    void mo8196bg(Vec3f vec3f);

    /* renamed from: bh */
    void mo8197bh(Vec3f vec3f);

    float cJh();

    float cJi();

    Vec3f cJj();

    Vec3f cJk();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    float getLifeTime();

    void setLifeTime(float f);

    void releaseReferences();

    void setFinalSize(float f);

    void setInitialSize(float f);

    void trigger();
}
