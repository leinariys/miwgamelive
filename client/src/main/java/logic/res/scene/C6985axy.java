package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vector2fWrap;
import game.geometry.Vector4fWrap;

/* renamed from: a.axy  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6985axy extends C1891ah {
    /* renamed from: H */
    void mo16866H(Vec3f vec3f, Vec3f vec3f2);

    /* renamed from: a */
    void mo16867a(Vec3f vec3f, Vector4fWrap ajf);

    /* renamed from: a */
    void mo16868a(Vec3f vec3f, Vector2fWrap aka);

    /* renamed from: a */
    void mo16869a(Vec3f vec3f, Vector2fWrap aka, Vector4fWrap ajf);

    /* renamed from: a */
    void mo16870a(Vec3f vec3f, Vec3f vec3f2, Vector4fWrap ajf);

    /* renamed from: a */
    void mo16871a(Vec3f vec3f, Vec3f vec3f2, Vector4fWrap ajf, Vector4fWrap ajf2);

    /* renamed from: a */
    void mo16872a(Vec3f vec3f, Vec3f vec3f2, Vector4fWrap ajf, Vector4fWrap ajf2, Vector2fWrap aka);

    /* renamed from: a */
    boolean mo4481a(Vec3f vec3f, Vec3f vec3f2, float f);

    /* renamed from: aW */
    long mo759aW();

    void addPoint(Vec3f vec3f);

    void addPoint(Vec3f vec3f, Vector2fWrap aka, Vector4fWrap ajf);

    int apZ();

    int cCJ();

    void cCK();

    int cCL();

    /* renamed from: d */
    Vec3f mo13484d(long j);

    /* renamed from: dC */
    long mo13487dC();

    /* renamed from: dD */
    long mo13488dD();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo13489dz();

    /* renamed from: fA */
    void mo16879fA(int i);

    /* renamed from: g */
    int mo13492g(long j);

    void reset();

    void setCurrentPoints(int i);

    void setIndexSize(int i);

    /* renamed from: vU */
    void mo16883vU(int i);
}
