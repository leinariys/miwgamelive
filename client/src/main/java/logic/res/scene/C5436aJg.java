package logic.res.scene;

/* renamed from: a.aJg  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5436aJg extends C2036bO {
    /* renamed from: a */
    void mo9603a(aKQ akq);

    /* renamed from: aW */
    long mo759aW();

    aKQ chX();

    boolean chY();

    float chZ();

    float cia();

    float cib();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    float getAnimationSpeed();

    void setAnimationSpeed(float f);

    float getFadeTime();

    void setFadeTime(float f);

    boolean isNeedToStep();

    boolean isPlaying();

    void play();

    void play(int i);

    void releaseReferences();

    void reset();

    void setRemoveOnEnd(boolean z);

    void stop();
}
