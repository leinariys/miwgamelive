package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;

/* renamed from: a.aFv  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5347aFv extends C6980axt {
    /* renamed from: a */
    boolean mo7879a(Vec3f vec3f, Vec3f vec3f2, float f);

    /* renamed from: a */
    boolean mo7880a(Vec3f vec3f, Vec3f vec3f2, float f, boolean z);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: bG */
    void mo8878bG(boolean z);

    C6980axt biv();

    C6980axt biw();

    C6980axt bix();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo765dz();

    /* renamed from: e */
    void mo8882e(C6980axt axt);

    /* renamed from: f */
    void mo8883f(C6980axt axt);

    /* renamed from: g */
    void mo8884g(C6980axt axt);

    /* renamed from: gO */
    BoundingBox mo766gO();

    /* renamed from: gP */
    BoundingBox mo8885gP();

    /* renamed from: gT */
    void mo2298gT();

    float getDistanceLod0();

    void setDistanceLod0(float f);

    float getDistanceLod1();

    void setDistanceLod1(float f);

    float getDistanceLod2();

    void setDistanceLod2(float f);

    boolean isDisposed();

    void releaseReferences();
}
