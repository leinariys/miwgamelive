package logic.res.scene;

/* renamed from: a.ec */
/* compiled from: a */
public interface C2378ec extends C5682aSs {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    int getBitDepth();

    int getBufferId();

    int getChannels();

    long getSampleCount();

    int getSampleRate();

    float getTimeLength();

    boolean isValid();
}
