package logic.res.scene;

import com.hoplon.geometry.Color;
import game.geometry.Matrix4fWrap;
import game.geometry.Vector2fWrap;

/* renamed from: a.nJ */
/* compiled from: a */
public interface C3038nJ extends C5759aVr {
    /* renamed from: A */
    C1710ZI mo20684A(int i);

    /* renamed from: B */
    int mo20685B(int i);

    /* renamed from: a */
    void mo20686a(int i, C1710ZI zi);

    /* renamed from: a */
    void mo20687a(int i, C2724jE jEVar);

    /* renamed from: a */
    void mo20688a(C1710ZI zi);

    /* renamed from: a */
    void mo20689a(C2724jE jEVar);

    /* renamed from: a */
    void mo20690a(C3674tb tbVar);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    void mo20691b(int i, C1710ZI zi);

    /* renamed from: b */
    void mo20692b(C1710ZI zi);

    /* renamed from: c */
    void mo20693c(int i, C1710ZI zi);

    /* renamed from: c */
    void mo20694c(C1710ZI zi);

    /* renamed from: d */
    void mo20695d(int i, C1710ZI zi);

    /* renamed from: d */
    void mo20696d(C1710ZI zi);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: e */
    void mo20697e(int i, C1710ZI zi);

    /* renamed from: e */
    void mo20698e(C1710ZI zi);

    /* renamed from: eo */
    C3674tb mo20699eo();

    /* renamed from: ep */
    Vector2fWrap mo20700ep();

    /* renamed from: eq */
    C1710ZI mo20701eq();

    /* renamed from: er */
    C1710ZI mo20702er();

    /* renamed from: es */
    C1710ZI mo20703es();

    /* renamed from: et */
    C1710ZI mo20704et();

    Vector2fWrap getScaling();

    float getShininess();

    void setShininess(float f);

    Color getSpecular();

    void setSpecular(Color color);

    Matrix4fWrap getTransform();

    void setTransform(Matrix4fWrap ajk);

    void importShaderParametersFromShader();

    int shaderParameterCount();

    int textureCount();

    /* renamed from: z */
    C2724jE mo20715z(int i);
}
