package logic.res.scene;

import com.hoplon.geometry.BoundingBox;

/* renamed from: a.abD  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5823abD extends C6980axt {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo765dz();

    /* renamed from: gP */
    BoundingBox mo8885gP();

    /* renamed from: gT */
    void mo2298gT();

    void releaseReferences();
}
