package logic.res.scene;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.KG */
/* compiled from: a */
public interface C0710KG extends aMF {
    /* renamed from: a */
    void mo3479a(C0710KG kg);

    /* renamed from: aW */
    long mo759aW();

    C2036bO bcU();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: g */
    void mo3481g(C2036bO bOVar);

    Vec3f getCurrentCameraOffset();

    void setCurrentCameraOffset(Vec3f vec3f);

    float getCurrentRotZ();

    void setCurrentRotZ(float f);

    float getFilterTimeConstant();

    void setFilterTimeConstant(float f);
}
