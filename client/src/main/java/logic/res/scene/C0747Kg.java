package logic.res.scene;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.Kg */
/* compiled from: a */
public interface C0747Kg extends C6980axt {
    /* renamed from: a */
    void mo3599a(float f, float f2, float f3);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: ao */
    void mo3600ao(boolean z);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    void releaseReferences();

    void reset();

    void setFollowEmitter(boolean z);

    void setLifeTime(float f);

    void setScaling(float f);

    void setScaling(Vec3f vec3f);
}
