package logic.res.scene;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.apD  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6551apD extends C1710ZI {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: f */
    void mo15346f(C1710ZI zi);

    Vec3f getPanSpeed();

    void setPanSpeed(Vec3f vec3f);

    /* renamed from: wb */
    C1710ZI mo15349wb();
}
