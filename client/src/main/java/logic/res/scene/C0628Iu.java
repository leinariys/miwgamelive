package logic.res.scene;

import game.geometry.Vector2fWrap;

/* renamed from: a.Iu */
/* compiled from: a */
public interface C0628Iu extends C6980axt {
    /* renamed from: Q */
    void mo2884Q(float f);

    /* renamed from: a */
    void mo2885a(C6980axt axt);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    Vector2fWrap getBillboardSize();

    void setBillboardSize(Vector2fWrap aka);

    float getFadeIn();

    void setFadeIn(float f);

    float getFadeOut();

    void setFadeOut(float f);

    float getRegionSize();

    void setRegionSize(float f);

    /* renamed from: me */
    float mo2890me();

    /* renamed from: mf */
    C6980axt mo2891mf();

    void releaseReferences();
}
