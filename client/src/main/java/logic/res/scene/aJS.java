package logic.res.scene;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.aJS */
/* compiled from: a */
public interface aJS extends C0710KG {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: an */
    void mo9559an(Vec3f vec3f);

    /* renamed from: ao */
    void mo9560ao(Vec3f vec3f);

    void bCI();

    Vec3f bCJ();

    float bCK();

    float bCL();

    void bCM();

    void bCN();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    float getPitch();

    void setPitch(float f);

    float getRoll();

    void setRoll(float f);

    float getYaw();

    void setYaw(float f);

    /* renamed from: iA */
    void mo9570iA(float f);

    /* renamed from: iB */
    void mo9571iB(float f);

    /* renamed from: k */
    void mo9572k(float f, float f2, float f3);
}
