package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Vector2fWrap;

/* renamed from: a.aIl  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5415aIl extends C6980axt {
    /* renamed from: a */
    void mo9375a(aBZ abz);

    /* renamed from: a */
    void mo9376a(C3038nJ nJVar, boolean z);

    /* renamed from: a */
    boolean mo7879a(Vec3f vec3f, Vec3f vec3f2, float f);

    /* renamed from: a */
    boolean mo7880a(Vec3f vec3f, Vec3f vec3f2, float f, boolean z);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    void mo9377b(C3038nJ nJVar);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    boolean dei();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo765dz();

    /* renamed from: g */
    void mo9379g(Matrix4fWrap ajk, Matrix4fWrap ajk2, Matrix4fWrap ajk3);

    /* renamed from: gO */
    BoundingBox mo766gO();

    /* renamed from: gT */
    void mo2298gT();

    int getAlignment();

    void setAlignment(int i);

    Vec3f getAlignmentDirection();

    void setAlignmentDirection(Vec3f vec3f);

    boolean getDisableDepthWrite();

    void setDisableDepthWrite(boolean z);

    Vector2fWrap getSize();

    void setSize(Vector2fWrap aka);

    /* renamed from: jB */
    void mo9384jB(boolean z);

    void releaseReferences();

    void setSize(float f, float f2);
}
