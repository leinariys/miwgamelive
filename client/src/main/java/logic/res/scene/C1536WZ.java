package logic.res.scene;

import gametoolkit.client.scene.jni.Factory;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;

import java.util.concurrent.ConcurrentLinkedQueue;

/* renamed from: a.WZ */
/* compiled from: a */
public abstract class C1536WZ implements C6561apN {
    private static ConcurrentLinkedQueue<C1536WZ> eAo = new ConcurrentLinkedQueue<>();

    static {
        Factory.init();
    }

    public long eAm;
    private boolean eAn;
    private transient Log logger;

    public C1536WZ(long j) {
        this.eAm = j;
        if (this.eAm != 0) {
            try {
                mo6596PO();
            } catch (Exception e) {
                m11265hy(e.getMessage());
            }
        }
    }

    public static void bFc() {
        for (C1536WZ poll = eAo.poll(); poll != null; poll = eAo.poll()) {
            poll.bFb();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: PO */
    public abstract void mo6596PO();

    /* access modifiers changed from: protected */
    /* renamed from: PP */
    public abstract void mo6597PP();

    public boolean equals(Object obj) {
        if (obj != null && getClass().equals(obj.getClass()) && ((C1536WZ) obj).eAm == this.eAm) {
            return true;
        }
        return false;
    }

    public final void release() {
        if (!this.eAn && this.eAm != 0) {
            this.eAn = true;
            eAo.add(this);
        }
    }

    /* renamed from: hy */
    private void m11265hy(String str) {
        if (this.logger == null) {
            this.logger = LogPrinter.setClass(getClass());
        }
        this.logger.warn(str);
    }

    private void bFb() {
        if (this.eAm != 0) {
            try {
                mo6597PP();
            } catch (Exception e) {
                m11265hy(e.getMessage());
            }
            this.eAm = 0;
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        release();
    }

    /* renamed from: dE */
    public long mo762dE() {
        return this.eAm;
    }

    /* renamed from: dF */
    public long mo763dF() {
        if (this.eAn) {
            return 0;
        }
        return this.eAm;
    }

    public String toString() {
        return String.valueOf(getClass().getSimpleName()) + ":" + this.eAm;
    }

    public String getTypeName() {
        return getClass().getSimpleName();
    }

    /* renamed from: a */
    public boolean mo6598a(C6561apN apn) {
        return apn != null && mo762dE() == apn.mo762dE();
    }

    public int hashCode() {
        return (int) (this.eAm % 2147483647L);
    }
}
