package logic.res.scene;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.amR  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6409amR extends C6980axt {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    void mo14827b(C2036bO bOVar, C6791atj atj);

    /* renamed from: b */
    void mo9377b(C3038nJ nJVar);

    C2036bO bcU();

    C2036bO ckb();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: g */
    void mo14830g(C2036bO bOVar);

    Vec3f getScreenPosition();

    /* renamed from: m */
    void mo14832m(C2036bO bOVar);

    void setProjectionDistance(float f);
}
