package logic.res.scene;

/* renamed from: a.aAw  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5218aAw extends C5682aSs {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    void mo7786b(C2378ec ecVar);

    C2378ec cIb();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    float getFinalControllerValue();

    void setFinalControllerValue(float f);

    float getFinalPitchValue();

    void setFinalPitchValue(float f);

    boolean getFirst();

    void setFirst(boolean z);

    float getGainMultiplier();

    void setGainMultiplier(float f);

    float getInitialControllerValue();

    void setInitialControllerValue(float f);

    float getInitialPitchValue();

    void setInitialPitchValue(float f);

    boolean getLast();

    void setLast(boolean z);

    /* renamed from: lg */
    boolean mo7795lg(float f);

    void setParentGain(float f);
}
