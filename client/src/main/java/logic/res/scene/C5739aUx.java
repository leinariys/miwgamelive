package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;

/* renamed from: a.aUx  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5739aUx extends C2036bO {
    /* renamed from: aR */
    void mo11727aR(Vec3f vec3f);

    /* renamed from: aW */
    long mo759aW();

    Vec3f cid();

    C1710ZI cie();

    C1710ZI cif();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo765dz();

    /* renamed from: g */
    void mo11731g(C1710ZI zi);

    /* renamed from: gT */
    void mo2298gT();

    Color getAmbientColor();

    void setAmbientColor(Color color);

    Color getFogColor();

    void setFogColor(Color color);

    float getFogEnd();

    void setFogEnd(float f);

    float getFogExp();

    void setFogExp(float f);

    float getFogStart();

    void setFogStart(float f);

    int getFogType();

    void setFogType(int i);

    /* renamed from: h */
    void mo11738h(C1710ZI zi);

    boolean isNeedToStep();

    void releaseReferences();
}
