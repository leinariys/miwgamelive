package logic.res.scene;

/* renamed from: a.agU  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6100agU extends C5759aVr {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    boolean isEnabled();

    void setEnabled(boolean z);

    void setSceneIntensity(float f);
}
