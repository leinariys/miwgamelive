package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vector4fWrap;

/* renamed from: a.ah */
/* compiled from: a */
public interface C1891ah extends C0999Oj {
    /* renamed from: a */
    void mo13483a(C1891ah ahVar);

    /* renamed from: a */
    boolean mo4481a(Vec3f vec3f, Vec3f vec3f2, float f);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: d */
    Vec3f mo13484d(long j);

    /* renamed from: dA */
    void mo13485dA();

    /* renamed from: dB */
    void mo13486dB();

    /* renamed from: dC */
    long mo13487dC();

    /* renamed from: dD */
    long mo13488dD();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo13489dz();

    /* renamed from: e */
    Vec3f mo13490e(long j);

    /* renamed from: f */
    Vector4fWrap mo13491f(long j);

    /* renamed from: g */
    int mo13492g(long j);
}
