package logic.res.scene;

import com.hoplon.geometry.Color;

/* renamed from: a.aGd  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5355aGd extends C5929adF {
    /* renamed from: aW */
    long mo759aW();

    boolean cZk();

    C3038nJ cZl();

    C3038nJ cZm();

    C3038nJ cZn();

    float cZo();

    float cZp();

    boolean cZq();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: g */
    void mo9070g(C3038nJ nJVar);

    float getAreaRadius();

    void setAreaRadius(float f);

    float getMaxVisibleDistance();

    void setMaxVisibleDistance(float f);

    Color getOutColor();

    void setOutColor(Color color);

    Color getSatellitesColor();

    void setSatellitesColor(Color color);

    Color getSelectedColor();

    void setSelectedColor(Color color);

    /* renamed from: h */
    void mo9076h(C3038nJ nJVar);

    /* renamed from: i */
    void mo9077i(C3038nJ nJVar);

    boolean isShowSatellites();

    void setShowSatellites(boolean z);

    /* renamed from: jf */
    void mo9079jf(float f);

    /* renamed from: mC */
    void mo9080mC(float f);

    /* renamed from: mD */
    void mo9081mD(float f);

    void releaseReferences();

    void setShowWhenOffscreen(boolean z);
}
