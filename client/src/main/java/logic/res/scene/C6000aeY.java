package logic.res.scene;

/* renamed from: a.aeY  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6000aeY extends C6552apE {
    /* renamed from: Yq */
    boolean mo13077Yq();

    /* renamed from: a */
    void mo13078a(int i, C2036bO bOVar);

    /* renamed from: a */
    void mo13079a(C2036bO bOVar);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: bg */
    C2036bO mo13080bg(String str);

    /* renamed from: c */
    void mo13081c(C2036bO bOVar);

    int childCount();

    /* renamed from: cx */
    C2036bO mo13083cx(int i);

    /* renamed from: d */
    boolean mo13084d(C2036bO bOVar);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    void releaseReferences();

    void removeAllChildren();

    void setCanRemoveChildren(boolean z);
}
