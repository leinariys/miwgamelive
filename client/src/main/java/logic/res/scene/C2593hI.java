package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;

/* renamed from: a.hI */
/* compiled from: a */
public interface C2593hI extends C6980axt {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    void mo17267b(C2036bO bOVar);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo765dz();

    float getCutoff();

    void setCutoff(float f);

    Color getDiffuseColor();

    void setDiffuseColor(Color color);

    Vec3f getDirection();

    void setDirection(Vec3f vec3f);

    float getExponent();

    void setExponent(float f);

    Vec3f getGlobalDirection();

    float getRadius();

    void setRadius(float f);

    Color getSpecularColor();

    void setSpecularColor(Color color);

    int getType();

    void setType(int i);

    void setMainLight(boolean z);

    /* renamed from: yA */
    C3602sp mo19233yA();

    /* renamed from: yB */
    boolean mo19234yB();
}
