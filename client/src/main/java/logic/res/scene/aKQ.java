package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;

/* renamed from: a.aKQ */
/* compiled from: a */
public interface aKQ extends C2036bO {
    /* renamed from: a */
    void mo9716a(int i, aBN abn);

    /* renamed from: a */
    boolean mo7879a(Vec3f vec3f, Vec3f vec3f2, float f);

    /* renamed from: a */
    boolean mo7880a(Vec3f vec3f, Vec3f vec3f2, float f, boolean z);

    /* renamed from: aW */
    long mo759aW();

    void addBoneRotation(String str, Quat4fWrap aoy);

    /* renamed from: b */
    void mo9718b(aBN abn);

    void bxV();

    int bxW();

    float[] bxX();

    boolean bxY();

    void clearBoneRotations();

    void clearSubMeshes();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo765dz();

    float getGameClock();

    int getNumBones();

    void setNumBones(int i);

    boolean isNeedToStep();

    /* renamed from: nE */
    aBN mo9727nE(int i);

    void setHasActiveAnimation(boolean z);

    void setMaxMutableVertexCount(int i);
}
