package logic.res.scene;

import gametoolkit.client.scene.editor.SceneEditorImpl;
import p001a.axM;
import taikodom.render.textures.DDSLoader;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/* renamed from: a.Db */
/* compiled from: a */
public class C0280Db extends SceneEditorImpl implements C4035yQ {
    /* renamed from: c */
    public static byte[] m2460c(InputStream inputStream) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(DDSLoader.DDSCAPS2_CUBEMAP_NEGATIVEZ);
            byte[] bArr = new byte[DDSLoader.DDSCAPS2_CUBEMAP_NEGATIVEZ];
            while (true) {
                int read = inputStream.read(bArr, 0, bArr.length);
                if (read == -1) {
                    return byteArrayOutputStream.toByteArray();
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        } finally {
            inputStream.close();
        }
    }

    /* renamed from: a */
    public void mo1636a(axM axm, String str) {
        byte[] c = m2460c(axm.mo16749kD(str));
        bqG().mo12369a(str, c, c.length);
        String[] bNK = bqG().bNK();
        for (int i = 0; i < 300 && bNK.length > 0; i++) {
            for (String str2 : bNK) {
                byte[] c2 = m2460c(axm.mo16749kD(str2));
                bqG().mo12369a(str2, c2, c2.length);
            }
            bNK = bqG().bNK();
        }
    }

    /* renamed from: b */
    public void mo1638b(axM axm, String str) {
        System.out.println("Saving " + str + "... (not really)");
    }

    /* renamed from: fK */
    public boolean mo1640fK(int i) {
        return super.mo4991fw((long) i);
    }

    /* renamed from: dU */
    public C3087nb mo1639dU(String str) {
        return bqG().mo12392ig(str);
    }

    public String[] aDK() {
        C5821abB bqG = bqG();
        String[] strArr = new String[bqG.entryCount()];
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = bqG.mo12371aF(i);
        }
        return strArr;
    }
}
