package logic.res.scene;

import com.hoplon.geometry.Color;

/* renamed from: a.Tf */
/* compiled from: a */
public interface C1347Tf extends C2724jE {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    void mo1883b(C2724jE jEVar);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    Color getValue();

    void setValue(Color color);
}
