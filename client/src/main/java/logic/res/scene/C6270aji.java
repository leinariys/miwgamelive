package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;

/* renamed from: a.aji  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6270aji extends C6980axt {
    /* renamed from: XJ */
    C6980axt mo14162XJ();

    /* renamed from: XK */
    C5415aIl mo14163XK();

    /* renamed from: XL */
    C5415aIl mo14164XL();

    /* renamed from: XM */
    int mo14165XM();

    /* renamed from: a */
    void mo14166a(int i, C5415aIl ail);

    /* renamed from: a */
    void mo14167a(C5415aIl ail);

    /* renamed from: a */
    boolean mo7879a(Vec3f vec3f, Vec3f vec3f2, float f);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    void mo14168b(C5415aIl ail);

    /* renamed from: c */
    void mo14169c(C5415aIl ail);

    /* renamed from: d */
    void mo14170d(C6980axt axt);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo765dz();

    /* renamed from: eh */
    C5415aIl mo14171eh(int i);

    /* renamed from: gT */
    void mo2298gT();

    float getOcclusionRatio();

    void setOcclusionRatio(float f);

    boolean isRayTraceable();

    void releaseReferences();
}
