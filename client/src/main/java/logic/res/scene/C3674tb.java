package logic.res.scene;

/* renamed from: a.tb */
/* compiled from: a */
public interface C3674tb extends C5759aVr {
    /* renamed from: T */
    C4076yy mo22257T(int i);

    /* renamed from: a */
    void mo22258a(int i, C4076yy yyVar);

    /* renamed from: a */
    void mo22259a(C4076yy yyVar);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    void mo22260b(C3674tb tbVar);

    void clearPasses();

    void compile();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    int getQuality();

    void setQuality(int i);

    /* renamed from: gs */
    C3674tb mo22264gs();

    /* renamed from: gt */
    C3674tb mo22265gt();

    int passCount();
}
