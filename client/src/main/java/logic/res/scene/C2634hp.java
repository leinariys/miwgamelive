package logic.res.scene;

/* renamed from: a.hp */
/* compiled from: a */
public interface C2634hp extends C5682aSs {
    /* renamed from: a */
    void mo19384a(int i, C6498aoC aoc);

    /* renamed from: a */
    void mo19385a(C6498aoC aoc);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    boolean mo19386b(C4076yy yyVar);

    /* renamed from: bp */
    C6498aoC mo19387bp(int i);

    /* renamed from: bq */
    C2724jE mo19388bq(int i);

    /* renamed from: c */
    void mo19389c(C4076yy yyVar);

    void clear();

    /* renamed from: d */
    boolean mo19391d(C4076yy yyVar);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    int getManualParamsCount();

    int getTangentChannel();

    boolean getUseDiffuseCubemap();

    boolean getUseReflectCubemap();

    int shaderProgramObjectCount();

    void unbind();

    /* renamed from: yp */
    boolean mo19398yp();

    /* renamed from: yq */
    boolean mo19399yq();

    /* renamed from: yr */
    boolean mo19400yr();
}
