package logic.res.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;

/* renamed from: a.sp */
/* compiled from: a */
public interface C3602sp extends C0999Oj {
    /* renamed from: Mr */
    int mo22028Mr();

    /* renamed from: Ms */
    void mo22029Ms();

    /* renamed from: Mt */
    float mo22030Mt();

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: aX */
    void mo22031aX(float f);

    /* renamed from: aY */
    void mo22032aY(float f);

    /* renamed from: aZ */
    void mo22033aZ(float f);

    /* renamed from: cM */
    void mo22034cM(int i);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: gL */
    Vec3f mo22035gL();

    float getCosCutoff();

    Color getDiffuseColor();

    void setDiffuseColor(Color color);

    Vec3f getDirection();

    void setDirection(Vec3f vec3f);

    float getRadius();

    void setRadius(float f);

    Color getSpecularColor();

    void setSpecularColor(Color color);

    float getSpotExponent();

    void setPosition(Vec3f vec3f);
}
