package logic.res.scene;

/* renamed from: a.aO */
/* compiled from: a */
public interface C1813aO extends C4027yI {
    /* renamed from: L */
    C4039yU mo10535L(int i);

    /* renamed from: a */
    void mo10536a(int i, C4039yU yUVar);

    /* renamed from: a */
    void mo10537a(C4039yU yUVar);

    /* renamed from: aW */
    long mo759aW();

    void changeLayer(int i, float f);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    boolean fadeOut(float f);

    float getGain();

    void setGain(float f);

    int musicCount();

    boolean pause();

    boolean play();

    boolean stop();
}
