package logic.res.scene;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.gB */
/* compiled from: a */
public interface C2511gB extends C1710ZI {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: f */
    void mo18883f(C1710ZI zi);

    Vec3f getTile();

    void setTile(Vec3f vec3f);

    /* renamed from: wb */
    C1710ZI mo18886wb();
}
