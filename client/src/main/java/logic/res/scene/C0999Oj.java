package logic.res.scene;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.Oj */
/* compiled from: a */
public interface C0999Oj extends C5759aVr {
    /* renamed from: a */
    boolean mo4481a(Vec3f vec3f, Vec3f vec3f2, float f);

    /* renamed from: aW */
    long mo759aW();

    C3674tb blf();

    C3038nJ blg();

    /* renamed from: c */
    void mo4484c(C3038nJ nJVar);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: e */
    void mo4485e(C3674tb tbVar);

    /* renamed from: g */
    void mo4486g(boolean z);

    boolean getUseVBO();
}
