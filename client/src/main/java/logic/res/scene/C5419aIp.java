package logic.res.scene;

/* renamed from: a.aIp  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5419aIp extends C5682aSs {
    long aDk();

    /* renamed from: aW */
    long mo759aW();

    boolean bSv();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    String getClonesFrom();

    void setClonesFrom(String str);

    boolean isComplete();

    void setUnloaded(boolean z);

    /* renamed from: zZ */
    C3087nb mo9396zZ();
}
