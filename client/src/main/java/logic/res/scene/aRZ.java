package logic.res.scene;

import com.hoplon.geometry.Color;

/* renamed from: a.aRZ */
/* compiled from: a */
public interface aRZ extends C5682aSs {
    /* renamed from: YU */
    C3038nJ mo11156YU();

    /* renamed from: a */
    void mo11157a(aIF aif);

    /* renamed from: a */
    void mo11158a(C6985axy axy, String str, float f, float f2, Color color);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    void mo11159b(C3038nJ nJVar);

    aIF cjY();

    int cjZ();

    float cka();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    float getSize();

    void setSize(float f);

    /* renamed from: js */
    float mo11164js(String str);

    /* renamed from: jt */
    float mo11165jt(String str);

    /* renamed from: ju */
    void mo11166ju(String str);

    void render(String str);

    /* renamed from: sD */
    void mo11168sD(int i);
}
