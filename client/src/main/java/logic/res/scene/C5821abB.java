package logic.res.scene;

/* renamed from: a.abB  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5821abB extends aGO {
    /* renamed from: UF */
    int mo12364UF();

    /* renamed from: a */
    C5419aIp mo12365a(C6385alt alt, C5682aSs ass);

    /* renamed from: a */
    void mo12366a(C6385alt alt);

    /* renamed from: a */
    void mo12367a(C6385alt alt, String str);

    /* renamed from: a */
    boolean mo12368a(C5419aIp aip);

    /* renamed from: a */
    boolean mo12369a(String str, byte[] bArr, int i);

    /* renamed from: aE */
    C5419aIp mo12370aE(int i);

    /* renamed from: aF */
    String mo12371aF(int i);

    /* renamed from: aW */
    long mo12372aW();

    /* renamed from: b */
    C5419aIp mo12373b(C3087nb nbVar);

    /* renamed from: b */
    String mo12374b(C6385alt alt);

    /* renamed from: b */
    void mo12375b(C5419aIp aip);

    /* renamed from: b */
    void mo12376b(C6385alt alt, String str);

    boolean bNG();

    int bNH();

    void bNI();

    void bNJ();

    String[] bNK();

    void bNL();

    void bNM();

    int bNN();

    /* renamed from: c */
    boolean mo12385c(C5419aIp aip);

    /* renamed from: dE */
    long mo12386dE();

    /* renamed from: dF */
    long mo12387dF();

    /* renamed from: dH */
    C6385alt mo12388dH(int i);

    /* renamed from: dY */
    void mo12389dY(boolean z);

    int entryCount();

    /* renamed from: if */
    boolean mo12391if(String str);

    /* renamed from: ig */
    C3087nb mo12392ig(String str);

    /* renamed from: ih */
    C3087nb mo12393ih(String str);

    /* renamed from: ii */
    C5419aIp mo12394ii(String str);

    /* renamed from: ij */
    C6385alt mo12395ij(String str);

    /* renamed from: ik */
    C1710ZI mo12396ik(String str);

    /* renamed from: il */
    C3038nJ mo12397il(String str);

    /* renamed from: pM */
    String mo12398pM(int i);

    void setRootPath(String str);
}
