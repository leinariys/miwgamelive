package logic.res.scene;

import game.geometry.Vector2fWrap;

/* renamed from: a.HE */
/* compiled from: a */
public interface C0509HE extends C4027yI {
    /* renamed from: a */
    void mo2493a(C2378ec ecVar);

    /* renamed from: aW */
    long mo759aW();

    C2378ec aqG();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: e */
    void mo2495e(C2036bO bOVar);

    boolean fadeOut(float f);

    float getAudibleRadius();

    void setAudibleRadius(float f);

    Vector2fWrap getDistances();

    void setDistances(Vector2fWrap aka);

    float getGain();

    void setGain(float f);

    boolean getIs3d();

    void setIs3d(boolean z);

    int getLoopCount();

    void setLoopCount(int i);

    boolean isAlive();

    boolean isPlaying();

    boolean isValid();

    void onRemove();

    boolean play();

    void reset();

    void setPitch(float f);

    boolean stop();
}
