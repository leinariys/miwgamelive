package logic.res.scene;

import game.geometry.Matrix4fWrap;

/* renamed from: a.aps  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6592aps extends C3087nb {
    /* renamed from: a */
    boolean mo15502a(long j, C6592aps aps);

    /* renamed from: a */
    boolean mo15503a(Matrix4fWrap ajk, C6791atj atj, String str, String str2, int i, boolean z);

    /* renamed from: a */
    boolean mo15504a(C6791atj atj, float f);

    /* renamed from: a */
    boolean mo15505a(C6791atj atj, String str, int i, int i2);

    /* renamed from: aW */
    long mo759aW();

    void cpC();

    boolean cpD();

    boolean cpE();

    long cpF();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: e */
    boolean mo15510e(String str, int i, int i2);

    void releaseDevices();

    void setShaderQuality(int i);

    void swapBuffers();
}
