package logic.res.scene;

/* renamed from: a.uK */
/* compiled from: a */
public interface C3733uK extends C1710ZI {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: f */
    void mo22344f(C1710ZI zi);

    float getFps();

    void setFps(float f);

    int getHorizontalFrames();

    void setHorizontalFrames(int i);

    boolean getIsLooped();

    void setIsLooped(boolean z);

    int getVerticalFrames();

    void setVerticalFrames(int i);

    void rewind();

    /* renamed from: wb */
    C1710ZI mo22354wb();
}
