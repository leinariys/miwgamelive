package logic.res.scene;

/* renamed from: a.yy */
/* compiled from: a */
public interface C4076yy extends C5682aSs {
    /* renamed from: a */
    void mo23184a(C2634hp hpVar);

    /* renamed from: aW */
    long mo759aW();

    int alA();

    boolean alB();

    boolean alC();

    boolean alD();

    C2634hp alk();

    int all();

    void alm();

    int aln();

    boolean alo();

    int alp();

    int alq();

    boolean alr();

    boolean als();

    int alt();

    boolean alu();

    boolean alv();

    boolean alw();

    int alx();

    boolean aly();

    boolean alz();

    void clearTexChannels();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: fb */
    void mo23206fb(int i);

    /* renamed from: fc */
    void mo23207fc(int i);

    /* renamed from: fd */
    void mo23208fd(int i);

    /* renamed from: fe */
    void mo23209fe(int i);

    /* renamed from: ff */
    void mo23210ff(int i);

    /* renamed from: fg */
    void mo23211fg(int i);

    /* renamed from: fh */
    int mo23212fh(int i);

    /* renamed from: fi */
    int mo23213fi(int i);

    /* renamed from: fj */
    int mo23214fj(int i);

    /* renamed from: fk */
    void mo23215fk(int i);

    float getAlphaRef();

    void setAlphaRef(float f);

    float getDepthRangeMax();

    void setDepthRangeMax(float f);

    float getDepthRangeMin();

    void setDepthRangeMin(float f);

    float getLineWidth();

    void setLineWidth(float f);

    boolean getUseDiffuseCubemap();

    void setUseDiffuseCubemap(boolean z);

    boolean getUseFixedFunctionLighting();

    void setUseFixedFunctionLighting(boolean z);

    boolean getUseReflectCubemap();

    void setUseReflectCubemap(boolean z);

    boolean getUseSceneAmbientColor();

    void setUseSceneAmbientColor(boolean z);

    void setAlphaTestEnabled(boolean z);

    void setBlendEnabled(boolean z);

    void setColorArrayEnabled(boolean z);

    void setColorMask(boolean z);

    void setCullFaceEnabled(boolean z);

    void setDepthMask(boolean z);

    void setDepthTestEnabled(boolean z);

    void setFogEnabled(boolean z);

    void setLineMode(boolean z);

    void setNormalArrayEnabled(boolean z);

    /* renamed from: v */
    void mo23242v(int i, int i2);

    /* renamed from: w */
    void mo23243w(int i, int i2);

    /* renamed from: x */
    void mo23244x(int i, int i2);
}
