package logic.res.scene;

import gametoolkit.client.scene.common.NamedObjectImpl;

import java.util.HashSet;
import java.util.Set;

/* renamed from: a.jP */
/* compiled from: a */
public abstract class C2744jP extends NamedObjectImpl implements C6552apE {
    public C2744jP(long j) {
        super(j);
    }

    /* access modifiers changed from: protected */
    public abstract int childCount();

    /* access modifiers changed from: protected */
    /* renamed from: cx */
    public abstract C2036bO mo19920cx(int i);

    /* renamed from: Ix */
    public C2036bO[] mo15350Ix() {
        C2036bO[] bOVarArr = new C2036bO[childCount()];
        for (int i = 0; i < bOVarArr.length; i++) {
            bOVarArr[i] = mo19920cx(i);
        }
        return bOVarArr;
    }

    /* renamed from: aI */
    public C2036bO mo15352aI(String str) {
        C2036bO aI;
        for (int i = 0; i < childCount(); i++) {
            C2036bO cx = mo19920cx(i);
            if (str.equals(cx.getName())) {
                return cx;
            }
            if (cx.childCount() > 0 && (aI = cx.mo15352aI(str)) != null) {
                return aI;
            }
        }
        return null;
    }

    /* renamed from: a */
    public boolean mo15351a(C6552apE ape) {
        return m33887a(new HashSet(), ape);
    }

    /* renamed from: a */
    private boolean m33887a(Set<Long> set, C6552apE ape) {
        if (mo6598a(ape)) {
            return true;
        }
        if (set.contains(this)) {
            return false;
        }
        set.add(Long.valueOf(mo762dE()));
        long childCount = (long) childCount();
        for (int i = 0; ((long) i) < childCount; i++) {
            if (((C2744jP) mo19920cx(i)).m33887a(set, ape)) {
                return true;
            }
        }
        return false;
    }
}
