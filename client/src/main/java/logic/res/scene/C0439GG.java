package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Color;
import game.geometry.Vector2fWrap;

/* renamed from: a.GG */
/* compiled from: a */
public interface C0439GG extends C6980axt {
    /* renamed from: R */
    void mo2285R(Color color);

    /* renamed from: S */
    void mo2286S(Color color);

    float aRm();

    float aRn();

    Color aRo();

    Color aRp();

    Vector2fWrap aRq();

    Vector2fWrap aRr();

    int aRs();

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    void mo2294b(Vector2fWrap aka);

    /* renamed from: c */
    void mo2295c(Vector2fWrap aka);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo765dz();

    /* renamed from: fq */
    void mo2296fq(float f);

    /* renamed from: fr */
    void mo2297fr(float f);

    /* renamed from: gT */
    void mo2298gT();

    float getDensity();

    void setDensity(float f);

    float getRadius();

    void setRadius(float f);

    boolean isNeedToStep();
}
