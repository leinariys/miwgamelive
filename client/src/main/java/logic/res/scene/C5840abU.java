package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;

/* renamed from: a.abU  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5840abU extends C6980axt {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    void mo12467b(C1891ah ahVar);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo765dz();

    Vec3f getShotSize();

    void setShotSize(Vec3f vec3f);

    void releaseReferences();

    /* renamed from: yk */
    C1891ah mo12470yk();
}
