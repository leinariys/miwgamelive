package logic.res.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Quat4fWrap;

/* renamed from: a.akq  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6330akq extends C6980axt {
    /* renamed from: IV */
    Vec3f mo14529IV();

    /* renamed from: IW */
    Vec3f mo14530IW();

    /* renamed from: IX */
    Vec3f mo14531IX();

    /* renamed from: IY */
    boolean mo14532IY();

    /* renamed from: IZ */
    float mo14533IZ();

    /* renamed from: Ja */
    boolean mo14534Ja();

    /* renamed from: Jb */
    boolean mo14535Jb();

    /* renamed from: Jc */
    boolean mo14536Jc();

    /* renamed from: Jd */
    boolean mo14537Jd();

    /* renamed from: Je */
    boolean mo14538Je();

    /* renamed from: Jf */
    boolean mo14539Jf();

    /* renamed from: Jg */
    boolean mo14540Jg();

    /* renamed from: aS */
    void mo14541aS(float f);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: ao */
    void mo14542ao(boolean z);

    /* renamed from: ap */
    void mo14543ap(boolean z);

    /* renamed from: aq */
    void mo14544aq(boolean z);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    int getAlignment();

    void setAlignment(int i);

    Vec3f getAlignmentDirection();

    void setAlignmentDirection(Vec3f vec3f);

    int getInitialBurst();

    void setInitialBurst(int i);

    float getInitialDelay();

    void setInitialDelay(float f);

    float getInitialMaxAngle();

    void setInitialMaxAngle(float f);

    float getInitialMinAngle();

    void setInitialMinAngle(float f);

    int getMaxParticles();

    void setMaxParticles(int i);

    float getMaxRotationSpeed();

    void setMaxRotationSpeed(float f);

    float getMinRotationSpeed();

    void setMinRotationSpeed(float f);

    float getNewPartsSecond();

    void setNewPartsSecond(float f);

    Color getParticlesColorVariation();

    void setParticlesColorVariation(Color color);

    Color getParticlesFinalColor();

    void setParticlesFinalColor(Color color);

    float getParticlesFinalSize();

    void setParticlesFinalSize(float f);

    float getParticlesFinalSizeVariation();

    void setParticlesFinalSizeVariation(float f);

    Color getParticlesInitialColor();

    void setParticlesInitialColor(Color color);

    float getParticlesInitialSize();

    void setParticlesInitialSize(float f);

    float getParticlesInitialSizeVariation();

    void setParticlesInitialSizeVariation(float f);

    float getParticlesLifeTimeVariation();

    void setParticlesLifeTimeVariation(float f);

    float getParticlesMaxLifeTime();

    void setParticlesMaxLifeTime(float f);

    float getSystemLifeTime();

    void setSystemLifeTime(float f);

    boolean isNeedToStep();

    /* renamed from: m */
    void mo14565m(Vec3f vec3f);

    /* renamed from: n */
    void mo14566n(Vec3f vec3f);

    /* renamed from: o */
    void mo14567o(Vec3f vec3f);

    void releaseReferences();

    void reset();

    void setAlignToHorizon(boolean z);

    void setAlignToMovement(boolean z);

    void setFollowEmitter(boolean z);

    void setPosition(float f, float f2, float f3);

    void setPosition(Vec3f vec3f);

    void setTransform(Matrix4fWrap ajk);

    void setTransform(Vec3f vec3f, Quat4fWrap aoy);

    void setTransform(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4);
}
