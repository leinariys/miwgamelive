package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import game.geometry.Matrix4fWrap;

/* renamed from: a.acT  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5891acT extends C6980axt {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    void mo12612b(C6980axt axt);

    /* renamed from: c */
    void mo12613c(Matrix4fWrap ajk);

    /* renamed from: c */
    void mo12614c(C6980axt axt);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo765dz();

    void releaseReferences();

    /* renamed from: vG */
    Matrix4fWrap mo12615vG();

    /* renamed from: vH */
    C6980axt mo12616vH();

    /* renamed from: vI */
    C6980axt mo12617vI();
}
