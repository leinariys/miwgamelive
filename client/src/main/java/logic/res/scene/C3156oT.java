package logic.res.scene;

import gametoolkit.client.scene.common.ManagedObjectImpl;

/* renamed from: a.oT */
/* compiled from: a */
public abstract class C3156oT extends ManagedObjectImpl implements aGO {
    public C3156oT(long j) {
        super(j);
    }

    /* access modifiers changed from: protected */
    /* renamed from: UF */
    public abstract int mo20998UF();

    /* access modifiers changed from: protected */
    /* renamed from: aE */
    public abstract C5419aIp mo20999aE(int i);

    /* access modifiers changed from: protected */
    /* renamed from: aF */
    public abstract String mo21000aF(int i);

    /* access modifiers changed from: protected */
    /* renamed from: dH */
    public abstract C6385alt mo21001dH(int i);

    /* access modifiers changed from: protected */
    public abstract int entryCount();

    /* renamed from: mo */
    public C5419aIp[] mo8935mo() {
        C5419aIp[] aipArr = new C5419aIp[entryCount()];
        for (int i = 0; i < aipArr.length; i++) {
            aipArr[i] = mo20999aE(i);
        }
        return aipArr;
    }

    /* renamed from: mp */
    public String[] mo8936mp() {
        String[] strArr = new String[entryCount()];
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = mo21000aF(i);
        }
        return strArr;
    }

    /* renamed from: UE */
    public C6385alt[] mo8934UE() {
        C6385alt[] altArr = new C6385alt[mo20998UF()];
        for (int i = 0; i < altArr.length; i++) {
            altArr[i] = mo21001dH(i);
        }
        return altArr;
    }
}
