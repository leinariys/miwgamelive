package logic.res.scene;

/* renamed from: a.aoC  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6498aoC extends C5682aSs {
    /* renamed from: aW */
    long mo759aW();

    boolean alB();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    int getObjectId();

    int getShaderType();

    void setShaderType(int i);

    String getSourceCode();

    void setSourceCode(String str);
}
