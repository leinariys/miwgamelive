package logic.res.scene;

/* renamed from: a.yI */
/* compiled from: a */
public interface C4027yI extends C2036bO {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    boolean fadeOut(float f);

    float getAudibleRadius();

    void setAudibleRadius(float f);

    float getGain();

    void setGain(float f);

    boolean isAlive();

    boolean isNeedToStep();

    boolean isPlaying();

    boolean isValid();

    boolean play();

    void setControllerValue(float f);

    void setLoopCount(int i);

    void setPitch(float f);

    boolean stop();
}
