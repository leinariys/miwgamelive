package logic.res.scene;

import game.geometry.Vector2fWrap;

/* renamed from: a.EY */
/* compiled from: a */
public interface C0336EY extends C2724jE {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    void mo1883b(C2724jE jEVar);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    Vector2fWrap getValue();

    void setValue(Vector2fWrap aka);
}
