package logic.res.scene;

import gametoolkit.client.scene.common.NamedObjectImpl;

/* renamed from: a.eM */
/* compiled from: a */
public abstract class C2351eM extends NamedObjectImpl implements C2608hW {
    public C2351eM(long j) {
        super(j);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aE */
    public abstract C5419aIp mo17998aE(int i);

    /* access modifiers changed from: protected */
    /* renamed from: aF */
    public abstract String mo17999aF(int i);

    /* access modifiers changed from: protected */
    public abstract int entryCount();

    /* renamed from: mo */
    public C5419aIp[] mo18001mo() {
        C5419aIp[] aipArr = new C5419aIp[entryCount()];
        for (int i = 0; i < aipArr.length; i++) {
            aipArr[i] = mo17998aE(i);
        }
        return aipArr;
    }

    /* renamed from: mp */
    public String[] mo18002mp() {
        String[] strArr = new String[entryCount()];
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = mo17999aF(i);
        }
        return strArr;
    }
}
