package logic.res.scene;

import game.geometry.Vector2fWrap;

/* renamed from: a.fD */
/* compiled from: a */
public interface C2416fD extends C4027yI {
    /* renamed from: a */
    void mo18362a(int i, C5218aAw aaw);

    /* renamed from: a */
    void mo18363a(C5218aAw aaw);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: aZ */
    C5218aAw mo18364aZ(int i);

    /* renamed from: b */
    void mo18365b(C5218aAw aaw);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    Vector2fWrap getDistances();

    void setDistances(Vector2fWrap aka);

    float getGain();

    void setGain(float f);

    boolean isAlive();

    boolean isPlaying();

    boolean isValid();

    void onRemove();

    int pitchEntryCount();

    boolean play();

    void removePitchEntry(int i);

    void setControllerValue(float f);

    boolean stop();
}
