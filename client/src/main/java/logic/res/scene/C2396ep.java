package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;

/* renamed from: a.ep */
/* compiled from: a */
public interface C2396ep extends C6980axt {
    /* renamed from: a */
    boolean mo7879a(Vec3f vec3f, Vec3f vec3f2, float f);

    /* renamed from: a */
    boolean mo7880a(Vec3f vec3f, Vec3f vec3f2, float f, boolean z);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: aq */
    void mo18190aq(int i);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo765dz();

    /* renamed from: gO */
    BoundingBox mo766gO();

    /* renamed from: gT */
    void mo2298gT();

    float getFinalAngle();

    void setFinalAngle(float f);

    float getInitialAngle();

    void setInitialAngle(float f);

    float getInnerRadius();

    void setInnerRadius(float f);

    float getNumSpiralTurns();

    void setNumSpiralTurns(float f);

    float getSpiralHeight();

    void setSpiralHeight(float f);

    float getSpiralWidth();

    void setSpiralWidth(float f);

    int getTesselation();

    void setTesselation(int i);

    /* renamed from: lc */
    C1891ah mo18198lc();

    /* renamed from: ld */
    int mo18199ld();

    void releaseReferences();
}
