package logic.res.scene;

import game.geometry.Matrix4fWrap;

/* renamed from: a.ZI */
/* compiled from: a */
public interface C1710ZI extends C5759aVr {
    /* renamed from: Xk */
    int mo7341Xk();

    /* renamed from: Xl */
    int mo7342Xl();

    /* renamed from: Xm */
    int mo7343Xm();

    /* renamed from: Xn */
    int mo7344Xn();

    /* renamed from: Xo */
    int mo7345Xo();

    /* renamed from: Xp */
    C5757aVp mo7346Xp();

    /* renamed from: Xq */
    void mo7347Xq();

    /* renamed from: a */
    void mo7348a(int i, int i2, int i3, int i4, int i5);

    /* renamed from: a */
    void mo7349a(int i, int i2, int i3, int i4, int i5, C5757aVp avp);

    /* renamed from: a */
    void mo7350a(C5757aVp avp);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: c */
    boolean mo7351c(int i, int i2, int i3);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dX */
    void mo7352dX(int i);

    /* renamed from: dY */
    void mo7353dY(int i);

    /* renamed from: dZ */
    void mo7354dZ(int i);

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: ea */
    void mo7355ea(int i);

    /* renamed from: eb */
    void mo7356eb(int i);

    int getBpp();

    void setBpp(int i);

    int getDataSize();

    int getInternalFormat();

    int getSizeX();

    void setSizeX(int i);

    int getSizeY();

    void setSizeY(int i);

    int getTarget();

    int getTexId();

    Matrix4fWrap getTransform();

    void setTransform(Matrix4fWrap ajk);

    float getTransformedHeight();

    float getTransformedWidth();

    int getType();

    boolean isTransformed();

    boolean isValid();

    /* renamed from: r */
    boolean mo7370r(int i, int i2);
}
