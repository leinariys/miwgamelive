package logic.res.scene;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import gametoolkit.client.scene.ContainerImpl;

/* renamed from: a.arF  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C6657arF extends ContainerImpl implements C6016aeo {
    static final Vec3d gtf = new Vec3d();

    public C6657arF(long j) {
        super(j);
    }

    public static Vec3d css() {
        return gtf;
    }

    /* renamed from: Y */
    public static void m25379Y(Vec3d ajr) {
        gtf.mo9484aA(ajr);
    }

    public abstract void dispose();

    public abstract void setPosition(Vec3f vec3f);

    public void destroy() {
        dispose();
        release();
    }

    public void setPosition(Vec3d ajr) {
        setPosition(ajr.mo9485aB(gtf));
    }
}
