package logic.res.scene;

/* renamed from: a.aHU */
/* compiled from: a */
public interface aHU extends C3087nb {
    /* renamed from: a */
    void mo9217a(String str, String str2);

    /* renamed from: a */
    boolean mo9218a(int i, int i2, int i3, boolean z);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: aX */
    boolean mo9219aX();

    /* renamed from: aY */
    boolean mo9220aY();

    /* renamed from: aZ */
    boolean mo9221aZ();

    /* renamed from: ba */
    int mo9222ba();

    /* renamed from: bb */
    float mo9223bb();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: e */
    void mo9224e(String str);

    /* renamed from: e */
    void mo9225e(boolean z);

    /* renamed from: f */
    void mo9226f(boolean z);

    /* renamed from: g */
    void mo9227g(boolean z);

    float getLodQuality();

    void setLodQuality(float f);

    int getShaderQuality();

    void setShaderQuality(int i);

    int getTextureFilter();

    void setTextureFilter(int i);

    int getTextureQuality();

    void setTextureQuality(int i);

    boolean getUseVBO();

    /* renamed from: i */
    void mo9233i(int i);

    void setAnisotropicFilter(float f);

    void setWireframeOnly(boolean z);

    void swapBuffers();
}
