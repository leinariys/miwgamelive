package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;

/* renamed from: a.yF */
/* compiled from: a */
public interface C4024yF extends C6980axt {
    /* renamed from: a */
    void mo23094a(Vec3f vec3f, Vec3f vec3f2, float f, float f2);

    /* renamed from: a */
    void mo23095a(Vec3f vec3f, Vec3f vec3f2, float f, float f2, float f3);

    /* renamed from: aW */
    long mo759aW();

    void addPoint(Vec3f vec3f);

    int apZ();

    void createCircle(Vec3f vec3f, Vec3f vec3f2, float f);

    void createCircle(Vec3f vec3f, Vec3f vec3f2, float f, float f2, float f3, float f4);

    /* renamed from: d */
    void mo23100d(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3);

    /* renamed from: d */
    void mo23101d(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo765dz();

    /* renamed from: e */
    void mo23102e(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4);

    /* renamed from: fA */
    void mo23103fA(int i);

    /* renamed from: g */
    void mo23104g(Vec3f vec3f, Vec3f vec3f2);

    /* renamed from: gO */
    BoundingBox mo766gO();

    short getLineStipple();

    void setLineStipple(short s);

    float getLineWidth();

    void setLineWidth(float f);

    void releaseReferences();

    void resetLine();

    void setDrawBigSegments();

    void setDrawLine();

    void setDrawMediumSegments();

    void setDrawSmallSegments();
}
