package logic.res.scene;

import com.hoplon.geometry.Color;

/* renamed from: a.auw  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6856auw extends C6100agU {
    /* renamed from: YU */
    C3038nJ mo16409YU();

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    void mo16410b(C3038nJ nJVar);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    Color getColor();

    void setColor(Color color);
}
