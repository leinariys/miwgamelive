package logic.res.scene;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.kl */
/* compiled from: a */
public interface C2832kl extends C0710KG {
    /* renamed from: JH */
    Vec3f mo20119JH();

    /* renamed from: JI */
    Vec3f mo20120JI();

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    float getFovY();

    float getMaxCameraRelativeOffset();

    void setMaxCameraRelativeOffset(float f);

    float getMaxCameraRotation();

    void setMaxCameraRotation(float f);

    float getMaxFovY();

    void setMaxFovY(float f);

    float getMaxShake();

    void setMaxShake(float f);

    float getMaxSolidAngularVelocity();

    void setMaxSolidAngularVelocity(float f);

    float getMaxSolidLinearVelocity();

    void setMaxSolidLinearVelocity(float f);

    float getMinFovY();

    void setMinFovY(float f);

    boolean isEnableFov();

    void setEnableFov(boolean z);

    /* renamed from: p */
    void mo20129p(Vec3f vec3f);

    /* renamed from: q */
    void mo20130q(Vec3f vec3f);

    void setShakeForever(boolean z);

    void setShakeTime(float f);
}
