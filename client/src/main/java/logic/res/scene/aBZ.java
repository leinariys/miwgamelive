package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vector4fWrap;

/* renamed from: a.aBZ */
/* compiled from: a */
public interface aBZ extends C5759aVr {
    /* renamed from: NC */
    void mo7935NC();

    /* renamed from: a */
    void mo7936a(Vector4fWrap ajf);

    /* renamed from: a */
    boolean mo7937a(Vec3f vec3f, Vec3f vec3f2, float f);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo7938dz();

    /* renamed from: v */
    void mo7939v(Vec3f vec3f);
}
