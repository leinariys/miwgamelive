package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vector2fWrap;

/* renamed from: a.azU */
/* compiled from: a */
public interface azU extends C6980axt {
    /* renamed from: aS */
    void mo17195aS(Vec3f vec3f);

    /* renamed from: aW */
    long mo759aW();

    C3038nJ ckE();

    C3038nJ ckF();

    void clearEntries();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo765dz();

    /* renamed from: e */
    void mo17199e(C3038nJ nJVar);

    /* renamed from: f */
    void mo17200f(C3038nJ nJVar);

    Vector2fWrap getBillboardSize();

    void setBillboardSize(Vector2fWrap aka);

    float getLifeTime();

    void setLifeTime(float f);

    float getMinSpeed();

    void setMinSpeed(float f);

    float getSpawnDistance();

    void setSpawnDistance(float f);

    float getTrailWidth();

    void setTrailWidth(float f);

    boolean isNeedToStep();

    void releaseReferences();

    void setLifeTimeModifier(float f);
}
