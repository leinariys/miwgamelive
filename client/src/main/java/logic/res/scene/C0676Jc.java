package logic.res.scene;

/* renamed from: a.Jc */
/* compiled from: a */
public interface C0676Jc extends C5759aVr {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: f */
    void mo3140f(C1710ZI zi);

    int getHeight();

    void setHeight(int i);

    int getWidth();

    void setWidth(int i);

    /* renamed from: wb */
    C1710ZI mo3145wb();
}
