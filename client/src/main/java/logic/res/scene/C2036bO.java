package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Quat4fWrap;

/* renamed from: a.bO */
/* compiled from: a */
public interface C2036bO extends C6016aeo {
    /* renamed from: a */
    void mo3599a(float f, float f2, float f3);

    /* renamed from: a */
    void mo13078a(int i, C2036bO bOVar);

    /* renamed from: a */
    void mo17265a(C6000aeY aey);

    /* renamed from: a */
    void mo13079a(C2036bO bOVar);

    /* renamed from: a */
    boolean mo7879a(Vec3f vec3f, Vec3f vec3f2, float f);

    /* renamed from: a */
    boolean mo7880a(Vec3f vec3f, Vec3f vec3f2, float f, boolean z);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    void mo17266b(Matrix4fWrap ajk);

    /* renamed from: b */
    void mo17267b(C2036bO bOVar);

    /* renamed from: c */
    void mo13081c(C2036bO bOVar);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    void dispose();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: dz */
    BoundingBox mo765dz();

    /* renamed from: e */
    void mo17268e(Vec3f vec3f);

    /* renamed from: gL */
    Vec3f mo9992gL();

    /* renamed from: gM */
    Vec3f mo17269gM();

    /* renamed from: gN */
    Matrix4fWrap mo17270gN();

    /* renamed from: gO */
    BoundingBox mo766gO();

    /* renamed from: gP */
    BoundingBox mo8885gP();

    /* renamed from: gQ */
    BoundingBox mo17271gQ();

    /* renamed from: gR */
    boolean mo17272gR();

    /* renamed from: gS */
    boolean mo17273gS();

    /* renamed from: gT */
    void mo2298gT();

    /* renamed from: gU */
    boolean mo17274gU();

    Vec3f getAngularVelocity();

    void setAngularVelocity(Vec3f vec3f);

    Vec3f getDesiredSize();

    void setDesiredSize(Vec3f vec3f);

    float getMaxVisibleDistance();

    void setMaxVisibleDistance(float f);

    int getRenderPriority();

    void setRenderPriority(int i);

    Vec3f getScaling();

    void setScaling(float f);

    void setScaling(Vec3f vec3f);

    Matrix4fWrap getTransform();

    void setTransform(Matrix4fWrap ajk);

    Vec3f getVelocity();

    void setVelocity(Vec3f vec3f);

    void hide();

    boolean isDisposed();

    boolean isHidden();

    boolean isNeedToStep();

    boolean isRayTraceable();

    void onRemove();

    void releaseReferences();

    void removeAllChildren();

    void reset();

    void setAllowSelection(boolean z);

    void setDisposeWhenEmpty(boolean z);

    void setOrientation(Quat4fWrap aoy);

    void setPosition(float f, float f2, float f3);

    void setPosition(Vec3f vec3f);

    void setRender(boolean z);

    void setTransform(Vec3f vec3f, Quat4fWrap aoy);

    void setTransform(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4);

    void show();
}
