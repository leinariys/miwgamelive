package logic.res.scene;

import game.geometry.Vector2fWrap;

/* renamed from: a.Oq */
/* compiled from: a */
public interface C1007Oq extends C6980axt {
    /* renamed from: aW */
    long mo759aW();

    boolean blu();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: de */
    void mo4529de(boolean z);

    /* renamed from: dy */
    C3087nb mo764dy();

    Vector2fWrap getSize();

    void setSize(Vector2fWrap aka);

    void releaseReferences();
}
