package logic.res.scene;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;

/* renamed from: a.aMF */
/* compiled from: a */
public interface aMF extends C6980axt {
    /* renamed from: J */
    void mo9976J(Vec3f vec3f, Vec3f vec3f2);

    /* renamed from: K */
    void mo9977K(Vec3f vec3f, Vec3f vec3f2);

    /* renamed from: a */
    float mo9978a(int i, Vec3f vec3f, float f);

    /* renamed from: a */
    void mo9979a(aMF amf, Vec3f vec3f, Vec3f vec3f2);

    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    void mo9980b(double d, double d2, double d3, double d4);

    /* renamed from: be */
    Vec3f mo9981be(Vec3f vec3f);

    /* renamed from: bf */
    Vec3f mo9982bf(Vec3f vec3f);

    boolean cId();

    double cIe();

    double cIf();

    double cIg();

    double cIh();

    void cIi();

    float cIj();

    float cIk();

    void calculateProjectionMatrix();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: gL */
    Vec3f mo9992gL();

    float getAspect();

    void setAspect(float f);

    float getFarPlane();

    void setFarPlane(float f);

    float getFovY();

    void setFovY(float f);

    float getNearPlane();

    void setNearPlane(float f);

    Matrix4fWrap getProjection();

    float getTanHalfFovY();

    /* renamed from: hF */
    void mo9999hF(boolean z);

    /* renamed from: lh */
    void mo10000lh(float f);

    /* renamed from: li */
    void mo10001li(float f);

    void releaseReferences();
}
