package logic.res.scene;

/* renamed from: a.yU */
/* compiled from: a */
public interface C4039yU extends C4027yI {
    /* renamed from: a */
    void mo23133a(C2378ec ecVar);

    /* renamed from: aW */
    long mo759aW();

    C2378ec aqG();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    boolean fadeOut(float f);

    float getGain();

    void setGain(float f);

    boolean isAlive();

    boolean isPlaying();

    boolean isValid();

    boolean pause();

    boolean play();

    void setLoopCount(int i);

    void setPitch(float f);

    boolean stop();
}
