package logic.res.scene;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.Pc */
/* compiled from: a */
public interface C1068Pc extends C2724jE {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: b */
    void mo1883b(C2724jE jEVar);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    Vec3f getValue();

    void setValue(Vec3f vec3f);
}
