package logic.res.scene;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.WR */
/* compiled from: a */
public interface C1525WR extends C3417rK {
    /* renamed from: aW */
    long mo759aW();

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dy */
    C3087nb mo764dy();

    Vec3f getTile();

    void setTile(Vec3f vec3f);
}
