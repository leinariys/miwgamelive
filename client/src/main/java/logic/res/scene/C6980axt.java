package logic.res.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;

/* renamed from: a.axt  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6980axt extends C2036bO {
    /* renamed from: YU */
    C3038nJ mo16837YU();

    /* renamed from: a */
    void mo758a(C3674tb tbVar);

    /* renamed from: aW */
    long mo759aW();

    boolean anY();

    boolean anZ();

    float aoa();

    C0676Jc aob();

    void aoc();

    int aod();

    int aoe();

    boolean aof();

    Vec3f aog();

    BoundingBox aoh();

    boolean aoi();

    int aoj();

    /* renamed from: b */
    void mo9377b(C3038nJ nJVar);

    /* renamed from: bG */
    void mo8878bG(boolean z);

    /* renamed from: d */
    void mo16850d(C3674tb tbVar);

    /* renamed from: dE */
    long mo762dE();

    /* renamed from: dF */
    long mo763dF();

    /* renamed from: dZ */
    void mo16851dZ(float f);

    /* renamed from: dy */
    C3087nb mo764dy();

    /* renamed from: eo */
    C3674tb mo16852eo();

    /* renamed from: fy */
    void mo16853fy(int i);

    float getImpostorBlendingDistance();

    void setImpostorBlendingDistance(float f);

    Color getPrimitiveColor();

    void setPrimitiveColor(Color color);

    boolean getVisible();

    void setVisible(boolean z);

    boolean isRayTraceable();

    void setImpostorEnabled(boolean z);

    void setOcclusionQueryEnabled(boolean z);

    void setUseLighting(boolean z);
}
