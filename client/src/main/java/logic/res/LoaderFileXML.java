package logic.res;

import org.xmlpull.mxp1.MXParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.*;
import java.util.Stack;

/* renamed from: a.oM */
/* compiled from: a */
public class LoaderFileXML {
    /* renamed from: aQl */
    int[] positionText = new int[2];

    /* renamed from: aD */
    public XmlNode loadFileXml(String str, String str2) throws IOException {
        return loadFileXml((InputStream) new FileInputStream(str), str2);
    }

    /* renamed from: a */
    public XmlNode loadFileXml(StringReader stringReader) {
        try {
            MXParser mXParser = new MXParser();
            mXParser.setInput(stringReader);
            return new LoaderFileXML().loadFileXml((XmlPullParser) mXParser);
        } catch (XmlPullParserException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: a */
    public XmlNode loadFileXml(InputStream inputStream, String str) throws IOException {
        try {
            MXParser mXParser = new MXParser();
            mXParser.setInput(new InputStreamReader(inputStream, str));
            return loadFileXml(mXParser);
        } catch (XmlPullParserException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Пробегаемся по документу
     *
     * @param xmlPullParser
     * @return
     */
    /* renamed from: a */
    private XmlNode loadFileXml(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        XmlNode agy = new XmlNode();//Создаём Пустой Тег
        Stack stack = new Stack();//Флагбток, последовательность иерархии
        stack.push(agy);
        boolean z = false;
        while (!z) {
            switch (xmlPullParser.next()) {
                case XmlPullParser.START_DOCUMENT:// начало документа
                default:
                    break;
                case XmlPullParser.END_DOCUMENT:// конец документа
                    z = true;
                    break;
                case XmlPullParser.START_TAG:// начало тэга <company>
                    stack.add(loadFileXml((XmlNode) stack.lastElement(), xmlPullParser));
                    break;
                case XmlPullParser.END_TAG:// конец тэга </company>
                    stack.pop();//Вытащить объект из стека
                    break;
                case XmlPullParser.TEXT:// содержимое элемента <company>TEXT</company>
                    setText((XmlNode) stack.lastElement(), xmlPullParser);
                    break;
            }
        }
        return agy.getListChildrenTag().get(0);//Достаём из пустого тега
    }

    /**
     * Создать потомка
     *
     * @param agy           Родительский элемент
     * @param xmlPullParser Тег с атрибутами
     * @return
     */
    /* renamed from: a */
    private XmlNode loadFileXml(XmlNode agy, XmlPullParser xmlPullParser) {
        XmlNode mz = agy.addChildrenTag(xmlPullParser.getName());//Добавить потомка с заданым тегом
        for (int i = 0; i < xmlPullParser.getAttributeCount(); i++) {
            //Заполняем атрибуты
            mz.addAttribute(xmlPullParser.getAttributeName(i), xmlPullParser.getAttributeValue(i));
        }
        return mz;
    }

    /**
     * Задать текстовое содержимое тега
     *
     * @param agy           Родительский элемент
     * @param xmlPullParser Содержимое
     */
    /* renamed from: b */
    private void setText(XmlNode agy, XmlPullParser xmlPullParser) {
        //Позиция текста
        agy.setText(new String(xmlPullParser.getTextCharacters(this.positionText), this.positionText[0], this.positionText[1]));
    }
}
