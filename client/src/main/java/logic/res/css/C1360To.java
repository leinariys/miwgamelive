package logic.res.css;

import org.w3c.css.sac.Condition;

import java.io.Serializable;

/* renamed from: a.To */
/* compiled from: a */
public class C1360To implements C3994xr, Serializable {
    private Condition eiV;
    private Condition eiW;

    public C1360To(Condition aib, Condition aib2) {
        this.eiV = aib;
        this.eiW = aib2;
    }

    /* renamed from: rg */
    public short getConditionType() {
        return 0;
    }

    public Condition anF() {
        return this.eiV;
    }

    public Condition anG() {
        return this.eiW;
    }

    public String toString() {
        return String.valueOf(anF().toString()) + anG().toString();
    }
}
