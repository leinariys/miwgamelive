package logic.res.css;

import com.steadystate.css.parser.CSSOMParser;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;

/* renamed from: a.abn  reason: case insensitive filesystem */
/* compiled from: a */
public class C5859abn extends ThreadLocal<Reference<CSSOMParser>> {
    public C5859abn() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: bNi */
    public Reference<CSSOMParser> initialValue() {
        return new SoftReference(new CSSOMParser());
    }
}
