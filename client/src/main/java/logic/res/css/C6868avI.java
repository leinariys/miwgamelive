package logic.res.css;

import com.steadystate.css.parser.CSSOMParser;
import logic.swing.C1366Tu;
import logic.swing.C2741jM;
import logic.ui.C1162RC;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.*;

import java.awt.*;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;

/* renamed from: a.avI  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C6868avI {
    /* renamed from: c */
    public static C1366Tu m26540c(CSSValue cSSValue) {
        if (cSSValue == null) {
            return C1366Tu.fEC;
        }
        if (cSSValue.getCssValueType() != 1) {
            return C1366Tu.fEE;
        }
        CSSPrimitiveValue cSSPrimitiveValue = (CSSPrimitiveValue) cSSValue;
        if (cSSPrimitiveValue.getPrimitiveType() == 2) {
            return new C1366Tu(cSSPrimitiveValue.getFloatValue((short) 2), C1366Tu.C1367a.PERCENT);
        }
        if (cSSPrimitiveValue.getPrimitiveType() == 3) {
            return new C1366Tu(cSSPrimitiveValue.getFloatValue((short) 3), C1366Tu.C1367a.EM);
        }
        return new C1366Tu(cSSPrimitiveValue.getFloatValue((short) 5), C1366Tu.C1367a.PX);
    }

    /* renamed from: a */
    public abstract CSSValue mo3817a(C1162RC rc, String str, boolean z);

    /* renamed from: a */
    public abstract boolean mo3818a(C1162RC rc, aKV akv, boolean z);

    /* renamed from: a */
    public float mo16532a(C1162RC rc, String str, float f, boolean z) {
        CSSPrimitiveValue a = (CSSPrimitiveValue) mo3817a(rc, str, z);
        if (a == null) {
            return -1.0f;
        }
        if (a.getCssValueType() != 1) {
            C1043PI.m8372p(a + " isn't a primitive value");
            return -1.0f;
        }
        CSSPrimitiveValue cSSPrimitiveValue = a;
        if (cSSPrimitiveValue.getPrimitiveType() == 2) {
            return (cSSPrimitiveValue.getFloatValue((short) 2) / 100.0f) * f;
        }
        if (cSSPrimitiveValue.getPrimitiveType() == 3) {
            return cSSPrimitiveValue.getFloatValue((short) 2) * f;
        }
        return cSSPrimitiveValue.getFloatValue((short) 5);
    }

    /* renamed from: a */
    public float mo16531a(C1162RC rc, String str) {
        return mo16537b(rc, str, true);
    }

    /* renamed from: b */
    public float mo16537b(C1162RC rc, String str, boolean z) {
        return mo16533a(rc, str, z, -1.0f);
    }

    /* renamed from: a */
    public float mo16533a(C1162RC rc, String str, boolean z, float f) {
        CSSPrimitiveValue a = (CSSPrimitiveValue) mo3817a(rc, str, z);
        if (a == null) {
            return f;
        }
        if (a.getCssValueType() == 1) {
            return a.getFloatValue((short) 5);
        }
        C1043PI.m8372p(a + " isn't a primitive value");
        return f;
    }

    /* renamed from: c */
    public C1366Tu mo16540c(C1162RC rc, String str, boolean z) {
        return m26540c(mo3817a(rc, str, z));
    }

    /* renamed from: a */
    public C1366Tu mo16534a(C1162RC rc, String str, boolean z, C1366Tu tu) {
        C1366Tu c = m26540c(mo3817a(rc, str, z));
        return C1366Tu.fEC.equals(c) ? tu : c;
    }

    /* renamed from: d */
    public boolean mo16546d(C1162RC rc, String str, boolean z) {
        if (mo3817a(rc, str, z) == null) {
            return false;
        }
        return true;
    }

    /* renamed from: b */
    public boolean mo16539b(C1162RC rc, String str) {
        return mo16546d(rc, str, true);
    }

    /* renamed from: c */
    public String mo16542c(C1162RC rc, String str) {
        return mo16549e(rc, str, true);
    }

    /* renamed from: e */
    public String mo16549e(C1162RC rc, String str, boolean z) {
        CSSPrimitiveValue a = (CSSPrimitiveValue) mo3817a(rc, str, z);
        if (a == null) {
            return null;
        }
        if (a.getCssValueType() == 1) {
            return a.getStringValue();
        }
        C1043PI.m8372p(a + " isn't a primitive value");
        return null;
    }

    /* renamed from: f */
    public Point mo16552f(C1162RC rc, String str, boolean z) {
        CSSValueList a = (CSSValueList) mo3817a(rc, str, z);
        if (a == null) {
            return null;
        }
        if (a.getCssValueType() != 2) {
            return null;
        }
        CSSValueList cSSValueList = a;
        Point point = new Point();
        point.setLocation((double) ((CSSPrimitiveValue) cSSValueList.item(0)).getFloatValue((short) 2), (double) ((CSSPrimitiveValue) cSSValueList.item(1)).getFloatValue((short) 2));
        return point;
    }

    /* renamed from: d */
    public Image mo16545d(C1162RC rc, String str) {
        String e = mo16549e(rc, str, false);
        if (e == null || "none".equals(e)) {
            return null;
        }
        if (e.startsWith("res://")) {
            return getImage(e.substring("res://".length()));
        }
        System.err.println("imagename must start with 'res://'. Wrong image name " + e);
        return null;
    }

    public Image getImage(String str) {
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: v */
    public <T> T mo16559v(T... tArr) {
        for (T t : tArr) {
            if (t != null) {
                return t;
            }
        }
        return null;
    }

    /* renamed from: b */
    public CSSStyleDeclaration mo16538b(C1162RC rc) {
        String attribute = rc.getAttribute("style");
        if (attribute == null) {
            return null;
        }
        try {
            return mo16536aY(attribute);
        } catch (CSSException e2) {
            new CSSException((short) -1, "Error at Element: " + getClass().getName(), e2).printStackTrace();
            return new CSSStyleDeclarationImpl((CSSRule) null);
        }
    }

    /* renamed from: c */
    public axS mo16541c(C1162RC rc) {
        return (axS) mo16559v(axS.m27082kE(mo16549e(rc, "position", false)), axS.STATIC);
    }

    /* renamed from: d */
    public C1206Ro mo16543d(C1162RC rc) {
        int i = 1;
        String c = mo16542c(rc, "font-family");
        int a = (int) mo16531a(rc, "font-size");
        if (c == null || "".equals(c) || a < 1) {
            return null;
        }
        String c2 = mo16542c(rc, "font-weight");
        String c3 = mo16542c(rc, "font-style");
        if (!"bold".equals(c2)) {
            i = 0;
        }
        if ("italic".equals(c3) || "oblique".equals(c3)) {
            i |= 2;
        }
        return new C1206Ro(c, i, a);
    }

    /* renamed from: aY */
    public CSSStyleDeclaration mo16536aY(String str) {
        CSSStyleDeclaration d = new CSSOMParser().parseStyleDeclaration(new InputSource((Reader) new StringReader("{" + str + "}")));
        if (d != null) {
            C0325EQ.m2829a(d);
        }
        return d;
    }

    /* renamed from: e */
    public String[] mo16550e(C1162RC rc, String str) {
        CSSValueList a = (CSSValueList) mo3817a(rc, str, true);
        if (a == null) {
            C1043PI.m8372p("not array found");
            return null;
        }
        ArrayList arrayList = new ArrayList();
        if (a.getCssValueType() == 2) {
            CSSValueList cSSValueList = a;
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= cSSValueList.getLength()) {
                    break;
                }
                arrayList.add(((CSSPrimitiveValue) cSSValueList.item(i2)).getStringValue());
                i = i2 + 1;
            }
        } else {
            arrayList.add(((CSSPrimitiveValue) a).getStringValue());
        }
        return C1043PI.m8370a(arrayList);
    }

    /* renamed from: e */
    public Color mo16548e(C1162RC rc) {
        CSSPrimitiveValue a = (CSSPrimitiveValue) mo3817a(rc, "background-color", false);
        if (a == null) {
            return null;
        }
        return m26539a(a.getRGBColorValue());
    }

    /* renamed from: f */
    public Color mo16551f(C1162RC rc) {
        CSSPrimitiveValue a = (CSSPrimitiveValue) mo3817a(rc, "border-color", true);
        if (a == null) {
            return null;
        }
        return m26539a(a.getRGBColorValue());
    }

    /* renamed from: g */
    public Color mo16553g(C1162RC rc) {
        return mo16535a(rc, true);
    }

    /* renamed from: a */
    public Color mo16535a(C1162RC rc, boolean z) {
        return mo16554g(rc, "color", z);
    }

    /* renamed from: g */
    public Color mo16554g(C1162RC rc, String str, boolean z) {
        CSSPrimitiveValue a = (CSSPrimitiveValue) mo3817a(rc, str, z);
        if (a == null) {
            return null;
        }
        float b = mo16537b(rc, String.valueOf(str) + "-alpha", z);
        if (a.getCssValueType() == 1) {
            CSSPrimitiveValue cSSPrimitiveValue = a;
            if (cSSPrimitiveValue.getPrimitiveType() == 25) {
                ColorCss rGBColorValue = (ColorCss) cSSPrimitiveValue.getRGBColorValue();
                if (b == -1.0f) {
                    return new Color((int) rGBColorValue.getRed().getFloatValue((short) 1), (int) rGBColorValue.getGreen().getFloatValue((short) 1), (int) rGBColorValue.getBlue().getFloatValue((short) 1), (int) rGBColorValue.dwv().getFloatValue((short) 1));
                }
                return new Color((int) rGBColorValue.getRed().getFloatValue((short) 1), (int) rGBColorValue.getGreen().getFloatValue((short) 1), (int) rGBColorValue.getBlue().getFloatValue((short) 1), (int) (b * 255.0f));
            }
        }
        return null;
    }

    /* renamed from: d */
    public Color mo16544d(CSSValue cSSValue) {
        if (cSSValue.getCssValueType() == 1) {
            CSSPrimitiveValue cSSPrimitiveValue = (CSSPrimitiveValue) cSSValue;
            if (cSSPrimitiveValue.getPrimitiveType() == 25) {
                ColorCss rGBColorValue = (ColorCss) cSSPrimitiveValue.getRGBColorValue();
                return new Color((int) rGBColorValue.getRed().getFloatValue((short) 1), (int) rGBColorValue.getGreen().getFloatValue((short) 1), (int) rGBColorValue.getBlue().getFloatValue((short) 1), (int) rGBColorValue.dwv().getFloatValue((short) 1));
            }
        }
        return null;
    }

    /* renamed from: a */
    private Color m26539a(RGBColor rGBColor) {
        return new Color(rGBColor.getRed().getFloatValue((short) 1) / 255.0f, rGBColor.getGreen().getFloatValue((short) 1) / 255.0f, rGBColor.getBlue().getFloatValue((short) 1) / 255.0f);
    }

    /* renamed from: h */
    public C6959awy mo16556h(C1162RC rc) {
        float a = mo16531a(rc, "border-top-width");
        float a2 = mo16531a(rc, "border-bottom-width");
        float a3 = mo16531a(rc, "border-left-width");
        float a4 = mo16531a(rc, "border-right-width");
        C6959awy awy = new C6959awy();
        awy.top = (int) a;
        awy.bottom = (int) a2;
        awy.left = (int) a3;
        awy.right = (int) a4;
        return awy;
    }

    @Deprecated
    /* renamed from: i */
    public C6959awy mo16557i(C1162RC rc) {
        float a = mo16533a(rc, "padding-top", false, 0.0f);
        float a2 = mo16533a(rc, "padding-bottom", false, 0.0f);
        float a3 = mo16533a(rc, "padding-left", false, 0.0f);
        float a4 = mo16533a(rc, "padding-right", false, 0.0f);
        C6959awy awy = new C6959awy();
        awy.top = (int) a;
        awy.bottom = (int) a2;
        awy.left = (int) a3;
        awy.right = (int) a4;
        return awy;
    }

    @Deprecated
    /* renamed from: j */
    public C6959awy mo16558j(C1162RC rc) {
        float a = mo16533a(rc, "margin-top", false, 0.0f);
        float a2 = mo16533a(rc, "margin-bottom", false, 0.0f);
        float a3 = mo16533a(rc, "margin-left", false, 0.0f);
        float a4 = mo16533a(rc, "margin-right", false, 0.0f);
        C6959awy awy = new C6959awy();
        awy.top = (int) a;
        awy.bottom = (int) a2;
        awy.left = (int) a3;
        awy.right = (int) a4;
        return awy;
    }

    /* renamed from: e */
    public C2741jM mo16547e(CSSValue cSSValue) {
        if (cSSValue == null) {
            return C2741jM.apd;
        }
        if (cSSValue.getCssValueType() != 2) {
            return C2741jM.apd;
        }
        CSSValueList cSSValueList = (CSSValueList) cSSValue;
        return new C2741jM(m26540c(cSSValueList.item(0)), m26540c(cSSValueList.item(1)));
    }
}
