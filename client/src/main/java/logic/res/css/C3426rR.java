package logic.res.css;

import org.w3c.css.sac.Condition;
import org.w3c.css.sac.ConditionalSelector;
import org.w3c.css.sac.SimpleSelector;

import java.io.Serializable;

/* renamed from: a.rR */
/* compiled from: a */
public class C3426rR implements ConditionalSelector, Serializable {
    private SimpleSelector bek;
    private Condition bel;

    public C3426rR(SimpleSelector kw, Condition aib) {
        this.bek = kw;
        this.bel = aib;
    }

    public short getSelectorType() {
        return 0;
    }

    /* renamed from: Bv */
    public SimpleSelector getSimpleSelector() {
        return this.bek;
    }

    /* renamed from: SW */
    public Condition getCondition() {
        return this.bel;
    }

    public String toString() {
        return String.valueOf(this.bek.toString()) + this.bel.toString();
    }
}
