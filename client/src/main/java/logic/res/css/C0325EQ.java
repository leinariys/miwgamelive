package logic.res.css;

import com.steadystate.css.dom.CSSValueImpl;
import com.steadystate.css.parser.CSSOMParser;
import logic.ui.C1162RC;
import logic.ui.C6179ahv;
import logic.ui.C6738asi;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.*;

import java.io.Reader;
import java.io.StringReader;
import java.util.HashMap;

/* renamed from: a.EQ */
/* compiled from: a */
public class C0325EQ {
    private static HashMap<String, CSSValueImpl> cUg = new HashMap<>();

    static {
        cUg.put("black", new CSSValueImpl((RGBColor) new ColorCss(0, 0, 0)));
        cUg.put("white", new CSSValueImpl((RGBColor) new ColorCss(255, 255, 255)));
        cUg.put("red", new CSSValueImpl((RGBColor) new ColorCss(255, 0, 0)));
        cUg.put("yellow", new CSSValueImpl((RGBColor) new ColorCss(255, 255, 0)));
        cUg.put("lime", new CSSValueImpl((RGBColor) new ColorCss(0, 255, 0)));
        cUg.put("aqua", new CSSValueImpl((RGBColor) new ColorCss(0, 255, 255)));
        cUg.put("blue", new CSSValueImpl((RGBColor) new ColorCss(0, 0, 255)));
        cUg.put("fuchsia", new CSSValueImpl((RGBColor) new ColorCss(255, 0, 255)));
        cUg.put("gray", new CSSValueImpl((RGBColor) new ColorCss(128, 128, 128)));
        cUg.put("silver", new CSSValueImpl((RGBColor) new ColorCss(192, 192, 192)));
        cUg.put("maroon", new CSSValueImpl((RGBColor) new ColorCss(128, 0, 0)));
        cUg.put("olive", new CSSValueImpl((RGBColor) new ColorCss(128, 128, 0)));
        cUg.put("green", new CSSValueImpl((RGBColor) new ColorCss(0, 128, 0)));
        cUg.put("teal", new CSSValueImpl((RGBColor) new ColorCss(0, 128, 128)));
        cUg.put("navy", new CSSValueImpl((RGBColor) new ColorCss(0, 0, 128)));
        cUg.put("purple", new CSSValueImpl((RGBColor) new ColorCss(128, 0, 128)));
    }

    CssNode cUf;

    public C0325EQ(CssNode akf) {
        this.cUf = akf;
    }

    /* renamed from: a */
    public static CSSStyleRule m2830a(CSSStyleRule cSSStyleRule) {
        m2829a(cSSStyleRule.getStyle());
        return cSSStyleRule;
    }

    /* renamed from: a */
    public static CSSStyleDeclaration m2829a(CSSStyleDeclaration cSSStyleDeclaration) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= cSSStyleDeclaration.getLength()) {
                return cSSStyleDeclaration;
            }
            String item = cSSStyleDeclaration.item(i2);
            if (item.equals("color") || item.equals("background-color") || item.equals("border-color")) {
                CSSPrimitiveValue propertyCSSValue = (CSSPrimitiveValue) cSSStyleDeclaration.getPropertyCSSValue(item);
                if (!(propertyCSSValue.getCssValueType() == 1 && propertyCSSValue.getPrimitiveType() == 25)) {
                    CSSValueImpl en = m2834en(cSSStyleDeclaration.getPropertyValue(item));
                    if (cSSStyleDeclaration instanceof CSSStyleDeclarationImpl) {
                        ((CSSStyleDeclarationImpl) cSSStyleDeclaration).mo9286a(item, en);
                    } else {
                        cSSStyleDeclaration.setProperty(item, en.toString(), (String) null);
                    }
                }
            }
            if (item.equals("padding")) {
                m2831a(item, cSSStyleDeclaration);
            }
            if (item.equals("margin")) {
                m2831a(item, cSSStyleDeclaration);
            }
            if (item.equals("border-width")) {
                m2832a("border-width", cSSStyleDeclaration, "border", "-width");
            }
            if (item.equals("background-position")) {
                m2833b(cSSStyleDeclaration);
            }
            i = i2 + 1;
        }
    }

    /* renamed from: a */
    public static void m2831a(String str, CSSStyleDeclaration cSSStyleDeclaration) {
        m2832a(str, cSSStyleDeclaration, str, "");
    }

    /* renamed from: b */
    public static void m2833b(CSSStyleDeclaration cSSStyleDeclaration) {
        CSSValue propertyCSSValue = cSSStyleDeclaration.getPropertyCSSValue("background-position");
        if (propertyCSSValue.getCssValueType() != 2 && propertyCSSValue.getCssValueType() == 1) {
            String cssText = propertyCSSValue.getCssText();
            if (cssText.startsWith("top")) {
                cSSStyleDeclaration.setProperty("background-position", "50% 0%", (String) null);
            }
            if (cssText.startsWith("bottom")) {
                cSSStyleDeclaration.setProperty("background-position", "50% 100%", (String) null);
            }
            if (cssText.startsWith("left")) {
                cSSStyleDeclaration.setProperty("background-position", "0% 50%", (String) null);
            }
            if (cssText.startsWith("right")) {
                cSSStyleDeclaration.setProperty("background-position", "100% 50%", (String) null);
            }
        }
    }

    /* renamed from: a */
    private static void m2832a(String str, CSSStyleDeclaration cSSStyleDeclaration, String str2, String str3) {
        CSSValueImpl bj;
        CSSValueImpl bj2;
        CSSValueImpl bj3;
        CSSValueImpl bj4;
        CSSValueList cSSValueList;
        CSSValueList cSSValueList2;
        CSSValueList cSSValueList3;
        CSSValueList cSSValueList4;
        CSSValueImpl propertyCSSValue = (CSSValueImpl) cSSStyleDeclaration.getPropertyCSSValue(str);
        CSSValueImpl bj5 = (CSSValueImpl) propertyCSSValue;
        if (propertyCSSValue.getCssValueType() == 2) {
            if (bj5.getLength() == 2) {
                cSSValueList3 = (CSSValueList) bj5.item(0);
                cSSValueList2 = (CSSValueList) bj5.item(0);
                CSSValueList item = (CSSValueList) bj5.item(1);
                cSSValueList = (CSSValueList) bj5.item(1);
                bj2 = (CSSValueImpl) item;
            } else {
                cSSValueList = null;
                bj2 = null;
                cSSValueList2 = null;
                cSSValueList3 = null;
            }
            if (bj5.getLength() == 3) {
                cSSValueList3 = (CSSValueList) bj5.item(0);
                bj2 = (CSSValueImpl) bj5.item(1);
                cSSValueList4 = (CSSValueList) bj5.item(1);
                bj3 = (CSSValueImpl) bj5.item(2);
            } else {
                cSSValueList4 = cSSValueList;
                bj3 = (CSSValueImpl) cSSValueList2;
            }
            if (bj5.getLength() == 4) {
                bj4 = (CSSValueImpl) bj5.item(0);
                bj = (CSSValueImpl) bj5.item(1);
                bj3 = (CSSValueImpl) bj5.item(2);
                bj2 = (CSSValueImpl) bj5.item(3);
            } else {
                bj = (CSSValueImpl) cSSValueList4;
                bj4 = (CSSValueImpl) cSSValueList3;
            }
        } else {
            bj = bj5;
            bj2 = bj5;
            bj3 = bj5;
            bj4 = bj5;
        }
        if (cSSStyleDeclaration instanceof CSSStyleDeclarationImpl) {
            CSSStyleDeclarationImpl ahx = (CSSStyleDeclarationImpl) cSSStyleDeclaration;
            ahx.mo9286a(String.valueOf(str2) + "-top" + str3, bj4);
            ahx.mo9286a(String.valueOf(str2) + "-bottom" + str3, bj3);
            ahx.mo9286a(String.valueOf(str2) + "-left" + str3, bj2);
            ahx.mo9286a(String.valueOf(str2) + "-right" + str3, bj);
            return;
        }
        cSSStyleDeclaration.setProperty(String.valueOf(str2) + "-top" + str3, bj4.getCssText(), (String) null);
        cSSStyleDeclaration.setProperty(String.valueOf(str2) + "-bottom" + str3, bj3.getCssText(), (String) null);
        cSSStyleDeclaration.setProperty(String.valueOf(str2) + "-left" + str3, bj2.getCssText(), (String) null);
        cSSStyleDeclaration.setProperty(String.valueOf(str2) + "-right" + str3, bj.getCssText(), (String) null);
    }

    /* renamed from: en */
    public static CSSValueImpl m2834en(String str) {
        if (str.indexOf("rgb") >= 0) {
            return null;
        }
        return cUg.get(str.toLowerCase());
    }

    /* renamed from: a */
    public void mo1841a(CssNode akf) {
        this.cUf = akf;
    }

    public void parse(Reader reader) {
        CSSStyleSheet c = new CSSOMParser().parseStyleSheet(new InputSource(reader));
        this.cUf.add(c);
        mo1842a(c);
    }

    /* renamed from: a */
    public void mo1840a(C1162RC rc) {
        String attribute = rc.getAttribute("style");
        if (attribute != null && !this.cUf.mo9786b((C6738asi) rc)) {
            CSSStyleSheet c = new CSSOMParser().parseStyleSheet(new InputSource((Reader) new StringReader(attribute)));
            this.cUf.add(c);
            this.cUf.mo9783a((C6738asi) rc);
            mo1842a(c);
        }
        C6179ahv Vl = rc.mo5150Vl();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < Vl.getLength()) {
                C6738asi qR = Vl.mo9893qR(i2);
                if (qR.getNodeType() == 0) {
                    mo1840a((C1162RC) qR);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    public void mo1842a(CSSStyleSheet cSSStyleSheet) {
        CSSOMParser vl;
        CSSRuleList cssRules = cSSStyleSheet.getCssRules();
        CSSOMParser vl2 = null;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < cssRules.getLength()) {
                CSSStyleRule item = (CSSStyleRule) cssRules.item(i2);
                if (item.getType() == 1) {
                    aQJ a = (aQJ) m2830a(item);
                    CssSelector lDVar = new CssSelector();
                    lDVar.cssRule = a;
                    lDVar.cssStyleSheet = cSSStyleSheet;
                    if (a instanceof aQJ) {
                        lDVar.selectorList = a.dqd();
                    } else {
                        String selectorText = a.getSelectorText();
                        if (vl2 == null) {
                            vl = CSSOMParser.getInstance();
                        } else {
                            vl = vl2;
                        }
                        lDVar.selectorList = vl.parseSelectors(new InputSource((Reader) new StringReader(selectorText)));
                        vl2 = vl;
                    }
                    lDVar.cssStyleDeclaration = a.getStyle();
                    this.cUf.add(lDVar);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }
}
