package logic.res.css;

import org.w3c.css.sac.Parser;

/* renamed from: a.BI */
/* compiled from: a */
public class ParserFactory {
    public Parser makeParser() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        String property = System.getProperty("org.w3c.css.sac.parser");
        if (property != null) {
            return (Parser) Class.forName(property).newInstance();
        }
        throw new NullPointerException("No value for sac.parser property");
    }
}
