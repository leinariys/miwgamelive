package logic.res.css;

import com.steadystate.css.dom.CSSStyleSheetImpl;
import com.steadystate.css.parser.CSSOMParser;
import org.w3c.css.sac.InputSource;
import org.w3c.css.sac.SelectorList;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSStyleRule;
import org.w3c.dom.css.CSSStyleSheet;

import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;

/* renamed from: a.aQJ */
/* compiled from: a */
public class aQJ implements Serializable, CSSStyleRule {
    private CSSStyleDeclaration fMQ = null;
    private SelectorList iFf = null;

    /* renamed from: qq */
    private CSSStyleSheetImpl f3593qq = null;

    /* renamed from: qr */
    private CSSRule f3594qr = null;

    public aQJ(CSSStyleSheetImpl ss, CSSRule cSSRule, SelectorList ahq) {
        this.f3593qq = ss;
        this.f3594qr = cSSRule;
        this.iFf = ahq;
    }

    public short getType() {
        return 1;
    }

    public String getCssText() {
        return String.valueOf(getSelectorText()) + " " + getStyle().toString();
    }

    public void setCssText(String str) {
        if (this.f3593qq == null || !this.f3593qq.isReadOnly()) {
            try {
                aQJ f = (aQJ) new CSSOMParser().parseRule(new InputSource((Reader) new StringReader(str)));
                if (f.getType() == 1) {
                    this.iFf = f.iFf;
                    this.fMQ = f.fMQ;
                    return;
                }
                throw new C3331qN(13, 4);
            } catch (CSSException e) {
                throw new C3331qN((short) 12, 0, e.getMessage());
            }
        } else {
            throw new C3331qN(7, 2);
        }
    }

    public CSSStyleSheet getParentStyleSheet() {
        return this.f3593qq;
    }

    public CSSRule getParentRule() {
        return this.f3594qr;
    }

    public String getSelectorText() {
        return this.iFf.toString();
    }

    public void setSelectorText(String str) {
        if (this.f3593qq == null || !this.f3593qq.isReadOnly()) {
            try {
                this.iFf = new CSSOMParser().parseSelectors(new InputSource((Reader) new StringReader(str)));
            } catch (CSSException e) {
                throw new C3331qN((short) 12, 0, e.getMessage());
            }
        } else {
            throw new C3331qN(7, 2);
        }
    }

    public SelectorList dqd() {
        return this.iFf;
    }

    public CSSStyleDeclaration getStyle() {
        return this.fMQ;
    }

    /* renamed from: a */
    public void mo10899a(CSSStyleDeclarationImpl ahx) {
        this.fMQ = ahx;
    }

    public String toString() {
        return getCssText();
    }
}
