package logic.res.css;

import com.steadystate.css.parser.CSSOMParser;
import logic.ui.C1162RC;
import logic.ui.C6738asi;
import org.w3c.css.sac.*;
import org.w3c.dom.css.CSSStyleDeclaration;

import java.io.Reader;
import java.io.StringReader;
import java.util.List;
import java.util.Locale;

/* renamed from: a.tX */
/* compiled from: a */
public class C3669tX {
    List<CssSelector> buT;

    public C3669tX(List<CssSelector> list) {
        this.buT = list;
    }

    public static boolean matches(String str, String str2) {
        int length;
        int length2;
        if (str == null || str2 == null || (length = str.length()) < (length2 = str2.length())) {
            return false;
        }
        boolean z = true;
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            if (length - i2 < length2 - i) {
                return false;
            }
            char charAt = str.charAt(i2);
            if (Character.isSpaceChar(charAt)) {
                if (i == length2) {
                    return true;
                }
                z = true;
                i = 0;
            } else if (z) {
                if (i >= length2 || str2.charAt(i) != charAt) {
                    z = false;
                    i = 0;
                } else {
                    i++;
                }
            }
        }
        if (i == length2) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public CSSStyleDeclaration mo22245a(C6738asi asi, String str, boolean z) {
        CSSStyleDeclaration cSSStyleDeclaration;
        for (int size = this.buT.size() - 1; size >= 0; size--) {
            CssSelector lDVar = this.buT.get(size);
            for (int i = 0; i < lDVar.selectorList.getLength(); i++) {
                if (m39653a(lDVar.selectorList.item(i), asi) && (cSSStyleDeclaration = lDVar.cssStyleDeclaration) != null && cSSStyleDeclaration.getPropertyValue(str) != null && !cSSStyleDeclaration.getPropertyValue(str).equals("")) {
                    return cSSStyleDeclaration;
                }
            }
        }
        if (!z || asi.mo15974Vm() == null) {
            return null;
        }
        return mo22245a(asi.mo15974Vm(), str, z);
    }

    /* renamed from: a */
    public boolean mo22247a(String str, C6738asi asi) {
        try {
            SelectorList g = new CSSOMParser().parseSelectors(new InputSource((Reader) new StringReader(str)));
            for (int i = 0; i < g.getLength(); i++) {
                if (m39653a(g.item(i), asi)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            System.err.println(e);
            return false;
        }
    }

    /* renamed from: a */
    private boolean m39653a(Selector awz, C6738asi asi) {
        switch (awz.getSelectorType()) {
            case 0:
                return m39649a((ConditionalSelector) awz, asi);
            case 4:
                return m39657b((ElementSelector) awz, asi);
            case 9:
                return m39654a((ElementSelector) awz, asi);
            case 10:
                return m39655b((C0546Hb) awz, asi);
            case 11:
                return m39648a((C0546Hb) awz, asi);
            default:
                System.err.println("unrecognized selector type: " + awz + " Element = " + asi.getClass());
                return false;
        }
    }

    /* renamed from: a */
    private boolean m39654a(ElementSelector nyVar, C6738asi asi) {
        if (nyVar.getLocalName() != null && !asi.mo13058ba(nyVar.getLocalName())) {
            return false;
        }
        return true;
    }

    /* renamed from: b */
    private boolean m39657b(ElementSelector nyVar, C6738asi asi) {
        if (nyVar.getLocalName() != null && !m39652a(asi, nyVar.getLocalName())) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    private boolean m39652a(C6738asi asi, String str) {
        return asi.getNodeName().equalsIgnoreCase(str);
    }

    /* renamed from: a */
    private boolean m39649a(ConditionalSelector vu, C6738asi asi) {
        return m39653a((Selector) vu.getSimpleSelector(), asi) && m39650a(vu.getCondition(), vu, asi);
    }

    /* renamed from: a */
    private boolean m39650a(Condition aib, ConditionalSelector vu, C6738asi asi) {
        switch (aib.getConditionType()) {
            case 0:
                C3994xr xrVar = (C3994xr) aib;
                if (!m39650a(xrVar.anF(), vu, asi) || !m39650a(xrVar.anG(), vu, asi)) {
                    return false;
                }
                return true;
            case 2:
                if (!m39650a(((C3179on) aib).mo21055SW(), vu, asi)) {
                    return true;
                }
                return false;
            case 4:
                return m39651a((AttributeCondition) aib, vu.getSimpleSelector(), asi);
            case 5:
                return m39659d((AttributeCondition) aib, vu.getSimpleSelector(), asi);
            case 6:
                return ((LangCondition) aib).getLang().equalsIgnoreCase(Locale.getDefault().getLanguage());
            case 9:
                return m39658c((AttributeCondition) aib, vu.getSimpleSelector(), asi);
            case 10:
                return m39656b((AttributeCondition) aib, vu.getSimpleSelector(), asi);
            default:
                System.err.println("unrecognized condition type:  " + aib + " " + aib.getConditionType() + " --> " + aib.getClass());
                return false;
        }
    }

    /* renamed from: a */
    private boolean m39651a(AttributeCondition akt, SimpleSelector kw, C6738asi asi) {
        return akt.getValue().equals(((C1162RC) asi).getAttribute(akt.getLocalName()));
    }

    /* renamed from: b */
    private boolean m39656b(AttributeCondition akt, SimpleSelector kw, C6738asi asi) {
        if (asi == null) {
            return false;
        }
        return asi.mo13058ba(akt.getValue());
    }

    /* renamed from: a */
    private boolean m39648a(C0546Hb hb, C6738asi asi) {
        C6738asi Vm;
        SimpleSelector Bv = hb.mo833Bv();
        Selector ayH = hb.ayH();
        if (!m39653a((Selector) Bv, asi) || (Vm = asi.mo15974Vm()) == null || !m39653a(ayH, Vm)) {
            return false;
        }
        return true;
    }

    /* renamed from: b */
    private boolean m39655b(C0546Hb hb, C6738asi asi) {
        SimpleSelector Bv = hb.mo833Bv();
        Selector ayH = hb.ayH();
        if (!m39653a((Selector) Bv, asi)) {
            return false;
        }
        C1162RC rc = (C1162RC) asi;
        while (true) {
            C1162RC rc2 = (C1162RC) rc.mo15974Vm();
            if (rc2 == null) {
                return false;
            }
            if (m39653a(ayH, (C6738asi) rc2)) {
                return true;
            }
            rc = rc2;
        }
    }

    /* renamed from: c */
    private boolean m39658c(AttributeCondition akt, SimpleSelector kw, C6738asi asi) {
        C1162RC rc = (C1162RC) asi;
        if (!rc.hasAttribute("class")) {
            return false;
        }
        if (!matches(rc.getAttribute("class"), akt.getValue())) {
            return false;
        }
        if (((ElementSelector) kw).getLocalName() == null) {
            return true;
        }
        if (!m39652a(asi, ((ElementSelector) kw).getLocalName())) {
            return false;
        }
        return true;
    }

    /* renamed from: d */
    private boolean m39659d(AttributeCondition akt, SimpleSelector kw, C6738asi asi) {
        C1162RC rc = (C1162RC) asi;
        if (!rc.hasAttribute("id")) {
            return false;
        }
        if (!rc.getAttribute("id").equals(akt.getValue())) {
            return false;
        }
        if (((ElementSelector) kw).getLocalName() == null) {
            return true;
        }
        if (!m39652a(asi, ((ElementSelector) kw).getLocalName())) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public boolean mo22246a(C6738asi asi, aKV akv, boolean z) {
        C6738asi Vm;
        for (int size = this.buT.size() - 1; size >= 0; size--) {
            CssSelector lDVar = this.buT.get(size);
            for (int i = 0; i < lDVar.selectorList.getLength(); i++) {
                if (m39653a(lDVar.selectorList.item(i), asi) && !akv.mo9738c(lDVar.cssStyleDeclaration)) {
                    return false;
                }
            }
        }
        if (!z || (Vm = asi.mo15974Vm()) == null) {
            return true;
        }
        return mo22246a(Vm, akv, z);
    }
}
