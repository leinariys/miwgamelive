package logic.res.css;

import org.w3c.css.sac.LexicalUnit;
import org.w3c.dom.css.Counter;

import java.io.Serializable;

/* renamed from: a.akw  reason: case insensitive filesystem */
/* compiled from: a */
public class C6336akw implements Serializable, Counter {
    private String fTD;
    private String fTE;
    private String fTF;

    public C6336akw(boolean z, LexicalUnit ald) {
        this.fTD = ald.getStringValue();
        LexicalUnit lG = ald.getNextLexicalUnit();
        if (z && lG != null) {
            LexicalUnit lG2 = lG.getNextLexicalUnit();
            this.fTF = lG2.getStringValue();
            lG = lG2.getNextLexicalUnit();
        }
        if (lG != null) {
            this.fTE = lG.getStringValue();
            lG.getNextLexicalUnit();
        }
    }

    public String getIdentifier() {
        return this.fTD;
    }

    public String getListStyle() {
        return this.fTE;
    }

    public String getSeparator() {
        return this.fTF;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        if (this.fTF == null) {
            stringBuffer.append("counter(");
        } else {
            stringBuffer.append("counters(");
        }
        stringBuffer.append(this.fTD);
        if (this.fTF != null) {
            stringBuffer.append(", \"").append(this.fTF).append("\"");
        }
        if (this.fTE != null) {
            stringBuffer.append(", ").append(this.fTE);
        }
        stringBuffer.append(")");
        return stringBuffer.toString();
    }
}
