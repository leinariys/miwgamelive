package logic.res.css;

import org.w3c.dom.DOMException;

import java.util.Locale;
import java.util.ResourceBundle;

/* renamed from: a.qN */
/* compiled from: a */
public class C3331qN extends DOMException {
    public static final int aWA = 7;
    public static final int aWB = 8;
    public static final int aWC = 9;
    public static final int aWD = 10;
    public static final int aWE = 11;
    public static final int aWF = 12;
    public static final int aWG = 13;
    public static final int aWH = 14;
    public static final int aWI = 15;
    public static final int aWJ = 16;
    public static final int aWK = 17;
    public static final int aWL = 18;
    public static final int aWM = 19;
    public static final int aWt = 0;
    public static final int aWu = 1;
    public static final int aWv = 2;
    public static final int aWw = 3;
    public static final int aWx = 4;
    public static final int aWy = 5;
    public static final int aWz = 6;
    private static ResourceBundle aWN = ResourceBundle.getBundle(C4101zU.class.getName(), Locale.getDefault());

    public C3331qN(short s, int i) {
        super(s, aWN.getString(m37501ef(i)));
    }

    public C3331qN(int i, int i2) {
        super((short) i, aWN.getString(m37501ef(i2)));
    }

    public C3331qN(short s, int i, String str) {
        super(s, aWN.getString(m37501ef(i)));
    }

    /* renamed from: ef */
    private static String m37501ef(int i) {
        return "s" + String.valueOf(i);
    }
}
