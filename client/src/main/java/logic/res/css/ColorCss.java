package logic.res.css;

import com.steadystate.css.dom.CSSValueImpl;
import com.steadystate.css.parser.LexicalUnitImpl;
import org.w3c.css.sac.LexicalUnit;
import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.RGBColor;

import java.io.Serializable;

/* renamed from: a.aTN */
/* compiled from: a */
public class ColorCss implements Serializable, RGBColor {
    private CSSPrimitiveValue red = null;
    private CSSPrimitiveValue green = null;
    private CSSPrimitiveValue blue = null;
    private CSSPrimitiveValue alpha = null;

    public ColorCss(LexicalUnit ald) {
        this.red = new CSSValueImpl(ald, true);
        LexicalUnit lG = ald.getNextLexicalUnit().getNextLexicalUnit();
        this.green = new CSSValueImpl(lG, true);
        LexicalUnit lG2 = lG.getNextLexicalUnit().getNextLexicalUnit();
        this.blue = new CSSValueImpl(lG2, true);
        LexicalUnit lG3 = lG2.getNextLexicalUnit();
        if (lG3 != null) {
            LexicalUnit lG4 = lG3.getNextLexicalUnit();
            if (lG4 != null) {
                this.alpha = new CSSValueImpl(lG4, true);
            } else {
                this.alpha = new CSSValueImpl(LexicalUnitImpl.perseValueColor((LexicalUnit) null, 255.0f), true);
            }
        } else {
            this.alpha = new CSSValueImpl(LexicalUnitImpl.perseValueColor((LexicalUnit) null, 255.0f), true);
        }
    }

    public ColorCss() {
    }

    public ColorCss(int i, int i2, int i3) {
        this.red = new CSSValueImpl(LexicalUnitImpl.perseValueColor((LexicalUnit) null, (float) i));
        this.green = new CSSValueImpl(LexicalUnitImpl.perseValueColor((LexicalUnit) null, (float) i2));
        this.blue = new CSSValueImpl(LexicalUnitImpl.perseValueColor((LexicalUnit) null, (float) i3));
        this.alpha = new CSSValueImpl(LexicalUnitImpl.perseValueColor((LexicalUnit) null, 255.0f));
    }

    public CSSPrimitiveValue getRed() {
        return this.red;
    }

    /* renamed from: a */
    public void mo11489a(CSSPrimitiveValue cSSPrimitiveValue) {
        this.red = cSSPrimitiveValue;
    }

    public CSSPrimitiveValue getGreen() {
        return this.green;
    }

    /* renamed from: b */
    public void mo11490b(CSSPrimitiveValue cSSPrimitiveValue) {
        this.green = cSSPrimitiveValue;
    }

    public CSSPrimitiveValue getBlue() {
        return this.blue;
    }

    public CSSPrimitiveValue dwv() {
        return this.alpha;
    }

    /* renamed from: c */
    public void mo11491c(CSSPrimitiveValue cSSPrimitiveValue) {
        this.alpha = cSSPrimitiveValue;
    }

    /* renamed from: d */
    public void mo11492d(CSSPrimitiveValue cSSPrimitiveValue) {
        this.blue = cSSPrimitiveValue;
    }

    public String toString() {
        return "rgb(" + this.red.toString() + ", " + this.green.toString() + ", " + this.blue.toString() + "," + this.alpha.toString() + ")";
    }
}
