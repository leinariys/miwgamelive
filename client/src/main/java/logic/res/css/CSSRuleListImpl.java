package logic.res.css;

import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSRuleList;

import java.io.Serializable;
import java.util.ArrayList;

/* renamed from: a.RK */
/* compiled from: a */
public class CSSRuleListImpl extends ArrayList<CSSRule> implements CSSRuleList, Serializable {

    public CSSRuleListImpl() {
        super(1);
    }

    public int getLength() {
        return size();
    }

    public CSSRule item(int i) {
        return (CSSRule) get(i);
    }

    /* renamed from: a */
    public void mo5162a(CSSRule cSSRule, int i) {
        super.add(i, cSSRule);
    }

    public void delete(int i) {
        remove(i);
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < getLength(); i++) {
            stringBuffer.append(item(i).toString()).append("\r\n");
        }
        return stringBuffer.toString();
    }
}
