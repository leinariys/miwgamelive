package logic.res.css;

import org.w3c.css.sac.ElementSelector;

import java.io.Serializable;

/* renamed from: a.Do */
/* compiled from: a */
public class C0294Do implements ElementSelector, Serializable {
    private String _localName;

    public C0294Do(String str) {
        this._localName = str;
    }

    public short getSelectorType() {
        return 4;
    }

    public String getNamespaceURI() {
        return null;
    }

    public String getLocalName() {
        return this._localName;
    }

    public String toString() {
        return getLocalName() != null ? getLocalName() : "*";
    }
}
