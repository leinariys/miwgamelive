package logic.res.css;

/* renamed from: a.BW */
/* compiled from: a */
public class CSSParseException extends CSSException {
    private int columnNumber;
    private int lineNumber;
    private String uri;

    public CSSParseException(String str, aJE aje) {
        super(str);
        this.code = SAC_SYNTAX_ERR;
        this.uri = aje.getURI();
        this.lineNumber = aje.getLineNumber();
        this.columnNumber = aje.getColumnNumber();
    }

    public CSSParseException(String str, aJE aje, Exception exc) {
        super(SAC_SYNTAX_ERR, str, exc);
        this.uri = aje.getURI();
        this.lineNumber = aje.getLineNumber();
        this.columnNumber = aje.getColumnNumber();
    }

    public CSSParseException(String str, String str2, int i, int i2) {
        super(str);
        this.code = SAC_SYNTAX_ERR;
        this.uri = str2;
        this.lineNumber = i;
        this.columnNumber = i2;
    }

    public CSSParseException(String str, String str2, int i, int i2, Exception exc) {
        super(SAC_SYNTAX_ERR, str, exc);
        this.uri = str2;
        this.lineNumber = i;
        this.columnNumber = i2;
    }

    public String getURI() {
        return this.uri;
    }

    public int getLineNumber() {
        return this.lineNumber;
    }

    public int getColumnNumber() {
        return this.columnNumber;
    }
}
