package logic.res.css;


import org.w3c.css.sac.AttributeCondition;
import org.w3c.css.sac.Condition;
import org.w3c.css.sac.LangCondition;
import org.w3c.css.sac.PositionalCondition;

/* renamed from: a.aJW */
/* compiled from: a */
public class aJW implements C0864MW {
    /* renamed from: a */
    public C3994xr mo3964a(Condition aib, Condition aib2) {
        return new C1360To(aib, aib2);
    }

    /* renamed from: b */
    public C3994xr mo3966b(Condition aib, Condition aib2) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    /* renamed from: a */
    public C3179on mo3963a(Condition aib) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    /* renamed from: a */
    public PositionalCondition mo3961a(int i, boolean z, boolean z2) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    /* renamed from: a */
    public AttributeCondition mo3962a(String str, String str2, boolean z, String str3) {
        return new aWn(str, str3);
    }

    /* renamed from: eL */
    public AttributeCondition mo3970eL(String str) {
        return new C2417fE(str);
    }

    /* renamed from: eM */
    public LangCondition mo3971eM(String str) {
        return new C0984OU(str);
    }

    /* renamed from: b */
    public AttributeCondition mo3965b(String str, String str2, boolean z, String str3) {
        return new C6914awC(str, str3);
    }

    /* renamed from: c */
    public AttributeCondition mo3969c(String str, String str2, boolean z, String str3) {
        return new C6955awu(str, str3);
    }

    /* renamed from: D */
    public AttributeCondition mo3959D(String str, String str2) {
        return new C3400rF(str2);
    }

    /* renamed from: E */
    public AttributeCondition mo3960E(String str, String str2) {
        return new C1378UC(str2);
    }

    public Condition bif() {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    public Condition big() {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    /* renamed from: eN */
    public C1269Sl mo3972eN(String str) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }
}
