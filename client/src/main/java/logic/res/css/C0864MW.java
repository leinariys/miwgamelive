package logic.res.css;

import org.w3c.css.sac.AttributeCondition;
import org.w3c.css.sac.Condition;
import org.w3c.css.sac.LangCondition;
import org.w3c.css.sac.PositionalCondition;

/* renamed from: a.MW */
/* compiled from: a */
public interface C0864MW {
    /* renamed from: D */
    AttributeCondition mo3959D(String str, String str2);

    /* renamed from: E */
    AttributeCondition mo3960E(String str, String str2);

    /* renamed from: a */
    PositionalCondition mo3961a(int i, boolean z, boolean z2);

    /* renamed from: a */
    AttributeCondition mo3962a(String str, String str2, boolean z, String str3);

    /* renamed from: a */
    C3179on mo3963a(Condition aib);

    /* renamed from: a */
    C3994xr mo3964a(Condition aib, Condition aib2);

    /* renamed from: b */
    AttributeCondition mo3965b(String str, String str2, boolean z, String str3);

    /* renamed from: b */
    C3994xr mo3966b(Condition aib, Condition aib2);

    Condition bif();

    Condition big();

    /* renamed from: c */
    AttributeCondition mo3969c(String str, String str2, boolean z, String str3);

    /* renamed from: eL */
    AttributeCondition mo3970eL(String str);

    /* renamed from: eM */
    LangCondition mo3971eM(String str);

    /* renamed from: eN */
    C1269Sl mo3972eN(String str);
}
