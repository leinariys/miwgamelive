package logic.res.css;

import com.steadystate.css.dom.CSSStyleSheetImpl;
import com.steadystate.css.parser.CSSOMParser;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.CSSMediaRule;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSRuleList;
import org.w3c.dom.css.CSSStyleSheet;
import org.w3c.dom.stylesheets.MediaList;

import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;

/* renamed from: a.aNI */
/* compiled from: a */
public class CssRuleMedia implements Serializable, CSSMediaRule {
    private CSSRuleList iwD = null;

    /* renamed from: qq */
    private CSSStyleSheetImpl f3426qq = null;

    /* renamed from: qr */
    private CSSRule f3427qr = null;

    /* renamed from: qt */
    private MediaList f3428qt = null;

    public CssRuleMedia(CSSStyleSheetImpl ss, CSSRule cSSRule, MediaList mediaList) {
        this.f3426qq = ss;
        this.f3427qr = cSSRule;
        this.f3428qt = mediaList;
    }

    public short getType() {
        return 4;
    }

    public String getCssText() {
        StringBuffer stringBuffer = new StringBuffer("@media ");
        stringBuffer.append(getMedia().toString()).append(" {");
        for (int i = 0; i < getCssRules().getLength(); i++) {
            stringBuffer.append(getCssRules().item(i).getCssText()).append(" ");
        }
        stringBuffer.append("}");
        return stringBuffer.toString();
    }

    public void setCssText(String str) {
        if (this.f3426qq == null || !this.f3426qq.isReadOnly()) {
            try {
                CssRuleMedia f = (CssRuleMedia) new CSSOMParser().parseRule(new InputSource((Reader) new StringReader(str)));
                if (f.getType() == 4) {
                    this.f3428qt = f.f3428qt;
                    this.iwD = f.iwD;
                    return;
                }
                throw new C3331qN(13, 7);
            } catch (CSSException e) {
                throw new C3331qN((short) 12, 0, e.getMessage());
            }
        } else {
            throw new C3331qN(7, 2);
        }
    }

    public CSSStyleSheet getParentStyleSheet() {
        return this.f3426qq;
    }

    public CSSRule getParentRule() {
        return this.f3427qr;
    }

    public MediaList getMedia() {
        return this.f3428qt;
    }

    public CSSRuleList getCssRules() {
        return this.iwD;
    }

    public int insertRule(String str, int i) {
        if (this.f3426qq == null || !this.f3426qq.isReadOnly()) {
            try {
                InputSource fh = new InputSource((Reader) new StringReader(str));
                CSSOMParser bAk = CSSOMParser.getInstance();
                bAk.setParentStyleSheet(this.f3426qq);
                bAk.getParentStyleSheet(this.f3427qr);
                ((CSSRuleListImpl) getCssRules()).mo5162a(bAk.parseRule(fh), i);
                return i;
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new C3331qN((short) 1, 1, e.getMessage());
            } catch (CSSException e2) {
                throw new C3331qN((short) 12, 0, e2.getMessage());
            }
        } else {
            throw new C3331qN(7, 2);
        }
    }

    public void deleteRule(int i) {
        if (this.f3426qq == null || !this.f3426qq.isReadOnly()) {
            try {
                ((CSSRuleListImpl) getCssRules()).delete(i);
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new C3331qN((short) 1, 1, e.getMessage());
            }
        } else {
            throw new C3331qN(7, 2);
        }
    }

    /* renamed from: a */
    public void mo10212a(CSSRuleListImpl rk) {
        this.iwD = rk;
    }

    public String toString() {
        return getCssText();
    }
}
