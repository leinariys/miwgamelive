package logic.res.css;

import org.w3c.css.sac.Selector;
import org.w3c.css.sac.SimpleSelector;

import java.io.Serializable;

/* renamed from: a.Bq */
/* compiled from: a */
public class C0159Bq implements C0546Hb, Serializable {
    private SimpleSelector bek;
    private Selector clG;

    public C0159Bq(Selector awz, SimpleSelector kw) {
        this.clG = awz;
        this.bek = kw;
    }

    public short getSelectorType() {
        return 11;
    }

    public Selector ayH() {
        return this.clG;
    }

    /* renamed from: Bv */
    public SimpleSelector mo833Bv() {
        return this.bek;
    }

    public String toString() {
        return String.valueOf(this.clG.toString()) + " > " + this.bek.toString();
    }
}
