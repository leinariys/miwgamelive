package logic.res.css;

import java.util.ListResourceBundle;

/* renamed from: a.zU */
/* compiled from: a */
public class C4101zU extends ListResourceBundle {
    static final Object[][] contents = {
            new Object[]{"s0", "Syntax error"},
            new Object[]{"s1", "Array out of bounds error"},
            new Object[]{"s2", "This style sheet is read only"},
            new Object[]{"s3", "The text does not represent an unknown rule"},
            new Object[]{"s4", "The text does not represent a style rule"},
            new Object[]{"s5", "The text does not represent a charset rule"},
            new Object[]{"s6", "The text does not represent an import rule"},
            new Object[]{"s7", "The text does not represent a media rule"},
            new Object[]{"s8", "The text does not represent a font face rule"},
            new Object[]{"s9", "The text does not represent a page rule"},
            new Object[]{"s10", "This isn't a Float type"},
            new Object[]{"s11", "This isn't a String type"},
            new Object[]{"s12", "This isn't a Counter type"},
            new Object[]{"s13", "This isn't a Rect type"},
            new Object[]{"s14", "This isn't an RGBColor type"},
            new Object[]{"s15", "A charset rule must be the first rule"},
            new Object[]{"s16", "A charset rule already exists"},
            new Object[]{"s17", "An import rule must preceed all other rules"},
            new Object[]{"s18", "The specified type was not found"}
    };

    public Object[][] getContents() {
        return contents;
    }
}
