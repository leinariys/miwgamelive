package logic.res.css;

import com.steadystate.css.dom.CSSValueImpl;
import com.steadystate.css.parser.CSSOMParser;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;

import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;

/* renamed from: a.aHx  reason: case insensitive filesystem */
/* compiled from: a */
public class CSSStyleDeclarationImpl implements Serializable, CSSStyleDeclaration {
    private ArrayList hXD = new ArrayList();

    /* renamed from: qr */
    private CSSRule f3015qr;

    public CSSStyleDeclarationImpl(CSSRule cSSRule) {
        this.f3015qr = cSSRule;
    }

    public String getCssText() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("{");
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.hXD.size()) {
                stringBuffer.append("}");
                return stringBuffer.toString();
            }
            C5576aOq aoq = (C5576aOq) this.hXD.get(i2);
            if (aoq != null) {
                stringBuffer.append(aoq.toString());
            }
            if (i2 < this.hXD.size() - 1) {
                stringBuffer.append("; ");
            }
            i = i2 + 1;
        }
    }

    public void setCssText(String str) {
        try {
            InputSource fh = new InputSource((Reader) new StringReader(str));
            CSSOMParser bAk = CSSOMParser.getInstance();
            this.hXD.clear();
            bAk.parseStyleDeclaration((CSSStyleDeclaration) this, fh);
        } catch (Exception e) {
            throw new C3331qN((short) 12, 0, e.getMessage());
        }
    }

    public String getPropertyValue(String str) {
        C5576aOq mK = m15325mK(str);
        return mK != null ? mK.dnu().toString() : "";
    }

    public CSSValue getPropertyCSSValue(String str) {
        C5576aOq mK = m15325mK(str);
        if (mK != null) {
            return mK.dnu();
        }
        return null;
    }

    public String removeProperty(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.hXD.size()) {
                return "";
            }
            C5576aOq aoq = (C5576aOq) this.hXD.get(i2);
            if (aoq.getName().equalsIgnoreCase(str)) {
                this.hXD.remove(i2);
                return aoq.dnu().toString();
            }
            i = i2 + 1;
        }
    }

    public String getPropertyPriority(String str) {
        C5576aOq mK = m15325mK(str);
        return (mK == null || !mK.dnv()) ? "" : "important";
    }

    public void setProperty(String str, String str2, String str3) {
        boolean z;
        try {
            CSSValue e = new CSSOMParser().parsePropertyValue(new InputSource((Reader) new StringReader(str2)));
            C5576aOq mK = m15325mK(str);
            if (str3 != null) {
                z = str3.equalsIgnoreCase("important");
            } else {
                z = false;
            }
            if (mK == null) {
                mo9285a(new C5576aOq(str, e, z));
                return;
            }
            mK.mo10680f(e);
            mK.mo10682jU(z);
        } catch (Exception e2) {
            throw new C3331qN((short) 12, 0, e2.getMessage());
        }
    }

    /* renamed from: a */
    public void mo9286a(String str, CSSValueImpl bj) {
        C5576aOq mK = m15325mK(str);
        if (mK == null) {
            mo9285a(new C5576aOq(str, bj, false));
            return;
        }
        mK.mo10680f(bj);
        mK.mo10682jU(false);
    }

    public int getLength() {
        return this.hXD.size();
    }

    public String item(int i) {
        C5576aOq aoq = (C5576aOq) this.hXD.get(i);
        return aoq != null ? aoq.getName() : "";
    }

    /* renamed from: yh */
    public C5576aOq mo9298yh(int i) {
        return (C5576aOq) this.hXD.get(i);
    }

    public CSSRule getParentRule() {
        return this.f3015qr;
    }

    /* renamed from: a */
    public void mo9285a(C5576aOq aoq) {
        this.hXD.add(aoq);
    }

    /* renamed from: mK */
    private C5576aOq m15325mK(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.hXD.size()) {
                return null;
            }
            C5576aOq aoq = (C5576aOq) this.hXD.get(i2);
            if (aoq.getName().equalsIgnoreCase(str)) {
                return aoq;
            }
            i = i2 + 1;
        }
    }

    public String toString() {
        return getCssText();
    }
}
