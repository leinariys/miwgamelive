package logic.res;

import game.engine.C5452aJw;
import gnu.trove.THashMap;
import p001a.C3048nR;
import p001a.ayQ;
import taikodom.addon.neo.preferences.GUIPrefAddon;

import java.io.*;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

/* renamed from: a.aAo  reason: case insensitive filesystem */
/* compiled from: a */
public class ConfigManager {
    public static final String NAME = "CONFIG_MANAGER";
    private static ConfigManager hdg;
    public Map<String, ConfigManagerSection> sectionOptions = new THashMap();

    public static ConfigManager initConfigManager() {
        if (hdg == null) {
            hdg = new ConfigManager();
        }
        return hdg;
    }

    public static ConfigManager cHv() {
        return new ConfigManager();
    }

    public void clear() {
        this.sectionOptions.clear();
    }

    /* renamed from: kT */
    public void loadConfig(String str) {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(str));
        int indexLine = 0;
        ConfigManagerSection section = null;
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                int i2 = indexLine + 1;
                String trim = readLine.trim();
                if (!trim.startsWith("#")) {//Если это пустая страка или начало # читаем следующую строку
                    if (trim.length() == 0) {
                        indexLine = i2;
                    } else if (trim.startsWith("[")) {//Если это раздел настроек
                        if (!trim.endsWith("]")) {//Если нет символа окончания раздела
                            System.err.println("WARNING: Badly formatted config section on " + str + ";" + Integer.toString(i2));
                            trim.concat("]");
                        }
                        String substring = trim.substring(1, trim.length() - 1);
                        ConfigManagerSection sectionTemp = this.sectionOptions.get(substring);//Проверяем добавлен ли такой раздел
                        if (sectionTemp == null) {//Нет такого раздела
                            section = new ConfigManagerSection();//Создаём хранилище параметров раздела настроек
                            this.sectionOptions.put(substring, section);//render, input, network
                        } else {
                            //Текой раздел есть
                            section = sectionTemp;
                        }
                        indexLine = i2;
                    } else {//Если это не раздел настроек
                        int indexOf = trim.indexOf("=");
                        if (indexOf == -1) {
                            System.err.print("Error at config file, invalid string: " + trim);
                            indexLine = i2;
                        } else {
                            String substring2 = trim.substring(0, indexOf);//Название настройки
                            String substring3 = trim.substring(indexOf + 1);//Значение настройки
                            if (substring2.length() <= 0) {
                                System.err.println("WARNING: Zero-length key on " + str + ":" + Integer.toString(i2));
                            } else if (section == null) {//Если раздел настроек не задан
                                System.err.println("WARNING: Found an entry before a section on " + str + ":" + Integer.toString(i2));
                                indexLine = i2;
                            } else if (substring3.startsWith("\"")) {
                                if (!substring3.endsWith("\"")) {
                                    System.err.println("WARNING: Incorrectly terminated string - missing closing quotes on " + str + ":" + Integer.toString(i2));
                                    substring3.concat("\"");
                                }
                                section.mo7190X(substring2, substring3.substring(1, substring3.length() - 1));
                                indexLine = i2;
                            } else if (substring3.matches("-?[0-9]*\\.[0-9]+[f]?")) {
                                section.mo7205b(substring2, Float.valueOf(substring3));
                                indexLine = i2;
                            } else if (substring3.matches("-?[0-9]*")) {
                                if (substring3 != null && substring3.length() > 0) {
                                    section.mo7206b(substring2, Integer.valueOf(substring3));
                                    indexLine = i2;
                                }
                            } else if (substring3.equalsIgnoreCase("true") || substring3.equalsIgnoreCase("yes") || substring3.equalsIgnoreCase(GUIPrefAddon.C4817c.Y) || substring3.equalsIgnoreCase("on")) {
                                section.mo7204b(substring2, Boolean.TRUE);
                                indexLine = i2;
                            } else if (substring3.equalsIgnoreCase("false") || substring3.equalsIgnoreCase("no") || substring3.equalsIgnoreCase("n") || substring3.equalsIgnoreCase("off")) {
                                section.mo7204b(substring2, Boolean.FALSE);
                                indexLine = i2;
                            } else {
                                System.err.println("WARNING: Incorrectly formatted value on " + str + ":" + Integer.toString(i2));
                                indexLine = i2;
                            }
                        }
                    }
                }
                indexLine = i2;
            } else {
                return;
            }
        }
    }

    /* renamed from: s */
    private void saveConfig(String str, boolean z) {
        BufferedWriter bufferedWriter;
        BufferedWriter bufferedWriter2 = null;
        Iterator<String> it = this.sectionOptions.keySet().iterator();
        while (true) {
            bufferedWriter = bufferedWriter2;
            if (!it.hasNext()) {
                break;
            }
            String next = it.next();
            boolean z2 = false;
            bufferedWriter2 = bufferedWriter;
            for (Map.Entry next2 : this.sectionOptions.get(next).getOptions().entrySet()) {
                ayQ ayq = (ayQ) next2.getValue();
                if (!z || ayq.isDirty()) {
                    if (bufferedWriter2 == null) {
                        bufferedWriter2 = new BufferedWriter(new FileWriter(str));
                        bufferedWriter2.write("# Saved automatically by ConfigManager");
                        bufferedWriter2.newLine();
                        bufferedWriter2.write("# " + new Date().toString());
                        bufferedWriter2.newLine();
                        bufferedWriter2.newLine();
                    }
                    if (!z2) {
                        z2 = true;
                        bufferedWriter2.write("[" + next + "]");
                        bufferedWriter2.newLine();
                    }
                    bufferedWriter2.write(String.valueOf((String) next2.getKey()) + "=");
                    Class<?> type = ayq.getType();
                    if (type == String.class) {
                        bufferedWriter2.write("\"");
                    }
                    bufferedWriter2.write(ayq.getValue().toString());
                    if (type == String.class) {
                        bufferedWriter2.write("\"");
                    }
                    bufferedWriter2.newLine();
                }
            }
            if (z2) {
                bufferedWriter2.newLine();
            }
        }
        if (bufferedWriter != null) {
            bufferedWriter.close();
        }
    }

    /* renamed from: kU */
    public void saveConfig(String str) {
        saveConfig(str, false);
    }

    /* renamed from: kV */
    public void loadConfigIni(String str) {
        saveConfig(str, true);
    }

    /**
     * Получаем настройки указанного раздела
     *
     * @param sectionName имя рездела
     * @return
     */
    /* renamed from: kW */
    public ConfigManagerSection getSection(String sectionName) {
        ConfigManagerSection section = this.sectionOptions.get(sectionName);
        if (section != null) {
            return section;
        }
        throw new C5452aJw("[" + sectionName + "]");
    }

    public void readExternal(ObjectInput objectInput) {
    }

    /* renamed from: cHw */
    public ConfigManager clone() {
        ConfigManager configManager = new ConfigManager();
        for (Map.Entry next : this.sectionOptions.entrySet()) {
            configManager.sectionOptions.put((String) next.getKey(), ((ConfigManagerSection) next.getValue()).clone());
        }
        return configManager;
    }

    public void setMarkAsDefaultConfig() {
        for (Map.Entry<String, ConfigManagerSection> value : this.sectionOptions.entrySet()) {
            ((ConfigManagerSection) value.getValue()).bHU();
        }
    }

    /* renamed from: kX */
    public String mo7761kX(String str) {
        return C3048nR.m36077b(new File(str));
    }
}
