package logic.res;

/**
 * Список возможных Параметров раздела настроек
 */
/* renamed from: a.ahY  reason: case insensitive filesystem */
/* compiled from: a */
public enum ConfigIniKeyValue {
    LANGUAGE("language", "user"),
    LAST_RUN_VERSION("lastRunVersion", "user"),
    USER_NAME("username", "user"),
    LAST_PLAYED_CHARACTER("lastPlayedCharacter", "user"),
    XRES("xres", "render"),
    YRES("yres", "render"),
    REFRESH_RATE("refresh", "render"),
    BPP("bpp", "render"),
    ENABLE_FULLSCREEN("fullscreen", "render"),
    TEXTURE_QUALITY("textureQuality", "render"),
    TEXTURE_FILTER("textureFilter", "render"),
    ANTI_ALIASING("antiAliasing", "render"),
    ENABLE_POST_PROCESSING_FX("postProcessingFx", "render"),
    SHADER_QUALITY("shaderQuality", "render"),
    ANISOTROPIC_FILTER("anisotropicFilter", "render"),
    LOD_QUALITY("lodQuality", "render"),
    ENVIRONMENT_FX_QUALITY("environmentFxQuality", "render"),
    VOL_MUSIC("musicVolume", "render"),
    VOL_AMBIENCE("ambienceVolume", "render"),
    VOL_FX("fxVolume", "render"),
    VOL_GUI("guiVolume", "render"),
    VOL_ENGINE("engineVolume", "render"),
    GUI_CONTRAST_BLOCKER("guiContrastBlocker", "render"),
    ENABLE_VBO("useVbo", "render"),
    ENABLE_PIXEL_LIGHTING("usePixelLight", "render"),
    ENABLE_WIREFRAME_ONLY("wireframeOnly", "render"),
    DYNAMIC_TEXTURE_RATE("dynamicTextureRate", "render"),
    GLOW_FEEDBACK("glowFeedback", "render"),
    SHOW_SELECTION_OUTLINE("showSelectionOutline"),
    WEB_SITE("site", "url"),
    SERVER("server", "network"),
    SERVER_PORT("tcp_port", "network"),
    ENABLE_VERBOSE_MODE("verbose", "console");

    private final String value;
    private final String key;

    private ConfigIniKeyValue(String key) {
        this.key = key;
        this.value = "";
    }

    private ConfigIniKeyValue(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return this.key;
    }

    public String toString() {
        return this.key;
    }

    public String getValue() {
        return this.value;
    }
}
