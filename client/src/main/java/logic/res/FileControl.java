package logic.res;

import logic.thred.LogPrinter;
import taikodom.render.graphics2d.C0559Hm;
import taikodom.render.loader.provider.C0399FY;
import taikodom.render.loader.provider.FilePath;

import java.io.*;
import java.nio.ByteBuffer;

/* renamed from: a.avr  reason: case insensitive filesystem */
/* compiled from: a */
public class FileControl implements FilePath {
    static LogPrinter log = LogPrinter.m10275K(FileControl.class);
    private File file;

    public FileControl(File file2) {
        this.file = file2;
    }

    public FileControl(String str) {
        this(new File(str));
    }

    public FileControl(File file2, String str) {
        this(new File(file2, str));
    }

    public boolean exists() {
        return this.file.exists();
    }

    /* renamed from: aC */
    public FilePath concat(String str) {
        return new FileControl(this.file, str);
    }

    public String getName() {
        return this.file.getName();
    }

    /* renamed from: BB */
    public FilePath mo2249BB() {
        return new FileControl(this.file.getParentFile());
    }

    public boolean isDir() {
        return this.file.isDirectory();
    }

    /* renamed from: BC */
    public FilePath[] mo2250BC() {
        File[] listFiles = this.file.listFiles();
        FilePath[] ainArr = new FilePath[listFiles.length];
        for (int i = 0; i < listFiles.length; i++) {
            ainArr[i] = new FileControl(listFiles[i]);
        }
        return ainArr;
    }

    /* renamed from: a */
    public FilePath[] mo2258a(C0399FY fy) {
        File[] listFiles = this.file.listFiles(new C2002a(fy));
        FilePath[] ainArr = new FilePath[listFiles.length];
        for (int i = 0; i < listFiles.length; i++) {
            ainArr[i] = new FileControl(listFiles[i]);
        }
        return ainArr;
    }

    /* renamed from: BD */
    public void mo2251BD() {
        this.file.mkdirs();
    }

    public InputStream openInputStream() {
        log.debug("openning " + this.file);
        return new FileInputStream(this.file);
    }

    /* renamed from: BE */
    public PrintWriter mo2252BE() {
        return new PrintWriter(new BufferedOutputStream(new FileOutputStream(this.file)));
    }

    public OutputStream openOutputStream() {
        mo2249BB().mo2251BD();
        return new FileOutputStream(this.file);
    }

    public boolean delete() {
        return this.file.delete();
    }

    public String getPath() {
        return this.file.getPath();
    }

    /* renamed from: d */
    public boolean mo2261d(FilePath ain) {
        return this.file.renameTo(((FileControl) ain).file);
    }

    /* renamed from: BF */
    public FilePath mo2253BF() {
        return new FileControl(this.file.getAbsoluteFile());
    }

    public long lastModified() {
        return this.file.lastModified();
    }

    /* renamed from: BG */
    public File getFile() {
        return this.file;
    }

    public String toString() {
        return this.file == null ? "null" : this.file.toString();
    }

    public int hashCode() {
        return this.file.hashCode();
    }

    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == FileControl.class) {
            return this.file.equals(((FileControl) obj).file);
        }
        return false;
    }

    /* renamed from: BI */
    public ByteBuffer mo2256BI() {
        return ByteBuffer.wrap(mo2255BH());
    }

    /* renamed from: BH */
    public byte[] mo2255BH() {
        return C0559Hm.m5264q(this.file);
    }

    public long length() {
        return this.file.length();
    }

    /* renamed from: d */
    public void mo2260d(byte[] bArr) {
        C0559Hm.m5248a(this.file, bArr);
    }

    /* renamed from: a.avr$a */
    class C2002a implements FilenameFilter {
        private final /* synthetic */ C0399FY irC;

        C2002a(C0399FY fy) {
            this.irC = fy;
        }

        public boolean accept(File file, String str) {
            return this.irC.mo2163a(FileControl.this, str);
        }
    }
}
