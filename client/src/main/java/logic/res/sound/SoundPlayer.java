package logic.res.sound;

import game.script.resource.Asset;
import logic.render.IEngineGraphics;
import logic.thred.LogPrinter;
import p001a.C5916acs;
import taikodom.render.RenderView;
import taikodom.render.SceneView;
import taikodom.render.helpers.RenderTask;
import taikodom.render.scene.SSoundSource;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SoundObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Озвучка интерфейса
 */
/* renamed from: a.Ix */
/* compiled from: a */
public class SoundPlayer implements ISoundPlayer {
    private static final float dfE = 100.0f;
    private static final String dfF = "data/audio/interface/interface_sfx.pro";
    private static final LogPrinter logger = LogPrinter.setClass(SoundPlayer.class);
    /* access modifiers changed from: private */
    public static Map<String, SoundObject> dfH = new HashMap();
    private static SoundPlayer thisClass;
    /* renamed from: lV */
    public final IEngineGraphics engineGraphics = C5916acs.getSingolton().getEngineGraphics();
    /* access modifiers changed from: private */
    /* access modifiers changed from: private */
    public SceneView dfI;

    private SoundPlayer() {
        if (this.engineGraphics != null) {
            this.engineGraphics.mo3049bV("data/audio/interface/interface_sfx.pro");
            this.dfI = new SceneView();
            this.dfI.setClearColorBuffer(false);
            this.dfI.setClearDepthBuffer(false);
            this.dfI.setClearStencilBuffer(false);
            this.engineGraphics.mo3005a(this.dfI);
        }
    }

    public static SoundPlayer aWp() {
        if (thisClass == null) {
            thisClass = new SoundPlayer();
        }
        return thisClass;
    }

    /* renamed from: A */
    public void mo2911A(String str, String str2) {
        C5916acs.getSingolton().getLoaderTrail().mo4995a(str, str2, (Scene) null, new C6446anC(), "Sound player impl");
    }

    /* renamed from: as */
    public SoundObject mo2914as(Asset tCVar) {
        if (this.engineGraphics == null || tCVar == null) {
            return null;
        }
        return mo2912B(tCVar.getFile(), tCVar.getHandle());
    }

    /* renamed from: B */
    public SoundObject mo2912B(String str, String str2) {
        if (this.engineGraphics == null) {
            return null;
        }
        if (str != null) {
            C5916acs.getSingolton().getLoaderTrail().mo4995a(str, (String) null, (Scene) null, (C0907NJ) null, "SoundPlayer");
        }
        return mo2917eC(str2);
    }

    /* renamed from: j */
    private SoundObject m5510j(String str, boolean z) {
        if (this.engineGraphics == null) {
            return null;
        }
        if (this.engineGraphics.aeg() <= 0.0f) {
            return null;
        }
        SoundObject soundObject = dfH.get(str);
        if (soundObject == null) {
            soundObject = (SoundObject) this.engineGraphics.mo3047bT(str);
            if (soundObject == null) {
                logger.warn("Failed to create " + str);
                return null;
            }
            if (soundObject instanceof SSoundSource) {
                ((SSoundSource) soundObject).setDisposeAfterPlaying(false);
            }
            dfH.put(str, soundObject);
        }
        soundObject.setGain(this.engineGraphics.aeg());
        this.engineGraphics.mo3007a((RenderTask) new C0632a(z, soundObject));
        return soundObject;
    }

    /* renamed from: eC */
    public SoundObject mo2917eC(String str) {
        return m5510j(str, true);
    }

    /* renamed from: cx */
    public SoundObject mo2915cx(String str) {
        return mo2916e(str, true);
    }

    /* renamed from: C */
    public SoundObject mo2913C(String str, String str2) {
        return mo2912B(str2, str);
    }

    /* renamed from: e */
    public SoundObject mo2916e(String str, boolean z) {
        if (str == null || "".equals(str)) {
            return null;
        }
        return m5510j(str, z);
    }

    /* renamed from: a.Ix$a */
    class C0632a implements RenderTask {
        private final /* synthetic */ boolean dIj;
        private final /* synthetic */ SoundObject dIk;

        C0632a(boolean z, SoundObject soundObject) {
            this.dIj = z;
            this.dIk = soundObject;
        }

        public void run(RenderView renderView) {
            Scene scene = SoundPlayer.this.dfI.getScene();
            if (this.dIj) {
                if (scene.hasChild(this.dIk)) {
                    this.dIk.play();
                    return;
                }
                this.dIk.play();
                scene.addChild(this.dIk);
            } else if (!scene.hasChild(this.dIk)) {
                this.dIk.play();
                scene.addChild(this.dIk);
            } else if (!this.dIk.isPlaying() || this.dIk.getSamplePosition() > SoundPlayer.dfE) {
                SSoundSource sSoundSource = (SSoundSource) this.dIk.cloneAsset();
                sSoundSource.setGain(SoundPlayer.this.engineGraphics.aeg());
                sSoundSource.play();
                scene.addChild(sSoundSource);
                SoundPlayer.dfH.put(sSoundSource.getName(), sSoundSource);
                this.dIk.fadeOut(0.05f);
            }
        }
    }
}
