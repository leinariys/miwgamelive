package logic.res.sound;

import taikodom.render.SoundBuffer;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.SSoundGroup;
import taikodom.render.scene.SSoundSource;

/* renamed from: a.anC  reason: case insensitive filesystem */
/* compiled from: a */
public class C6446anC implements C0907NJ {
    /* renamed from: a */
    public void mo968a(RenderAsset renderAsset) {
        if (renderAsset instanceof SSoundSource) {
            ((SSoundSource) renderAsset).getBuffer().getData();
        } else if (renderAsset instanceof SoundBuffer) {
            ((SoundBuffer) renderAsset).getData();
        } else if (renderAsset instanceof SSoundGroup) {
            SSoundGroup sSoundGroup = (SSoundGroup) renderAsset;
            for (int i = 0; i < sSoundGroup.bufferCount(); i++) {
                sSoundGroup.getBuffer(i).getData();
            }
        }
    }
}
