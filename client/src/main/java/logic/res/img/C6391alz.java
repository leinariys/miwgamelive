package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.alz  reason: case insensitive filesystem */
/* compiled from: a */
public class C6391alz extends C2404ev {
    public static String fXE = "map_hack_05_pt";

    /* renamed from: ov */
    public static String f4888ov = "imageset_map_05_pt";

    /* renamed from: a */
    public static String m23833a(String str, String str2, String str3) {
        if (m23831B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m23831B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f4888ov);
    }

    /* renamed from: b */
    public static String m23835b(String str, String str2) {
        if (m23831B(str)) {
            return str;
        }
        if (m23831B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f4888ov);
    }

    /* renamed from: B */
    public static boolean m23831B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C6391alz.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m23836b(String str, String str2, String str3) {
        return String.valueOf(f4888ov) + C0147Bi.SEPARATOR + m23833a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m23837c(String str, String str2) {
        return String.valueOf(f4888ov) + C0147Bi.SEPARATOR + m23835b(str, str2);
    }

    /* renamed from: a */
    public static String m23832a(String str, int i, String str2) {
        if (!m23831B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f4888ov);
        }
        try {
            Field declaredField = C6391alz.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f4888ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f4888ov) + C0147Bi.SEPARATOR + m23835b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f4888ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f4888ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f4888ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m23834a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m23831B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f4888ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f4888ov) + C0147Bi.SEPARATOR + m23835b(strArr[i], str);
        }
        return strArr2;
    }
}
