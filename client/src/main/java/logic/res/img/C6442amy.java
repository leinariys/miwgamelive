package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.amy  reason: case insensitive filesystem */
/* compiled from: a */
public class C6442amy extends C2404ev {
    public static String gbb = "map_hack_05_en";

    /* renamed from: ov */
    public static String f4934ov = "imageset_map_05_en";

    /* renamed from: a */
    public static String m24025a(String str, String str2, String str3) {
        if (m24023B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m24023B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f4934ov);
    }

    /* renamed from: b */
    public static String m24027b(String str, String str2) {
        if (m24023B(str)) {
            return str;
        }
        if (m24023B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f4934ov);
    }

    /* renamed from: B */
    public static boolean m24023B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C6442amy.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m24028b(String str, String str2, String str3) {
        return String.valueOf(f4934ov) + C0147Bi.SEPARATOR + m24025a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m24029c(String str, String str2) {
        return String.valueOf(f4934ov) + C0147Bi.SEPARATOR + m24027b(str, str2);
    }

    /* renamed from: a */
    public static String m24024a(String str, int i, String str2) {
        if (!m24023B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f4934ov);
        }
        try {
            Field declaredField = C6442amy.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f4934ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f4934ov) + C0147Bi.SEPARATOR + m24027b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f4934ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f4934ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f4934ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m24026a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m24023B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f4934ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f4934ov) + C0147Bi.SEPARATOR + m24027b(strArr[i], str);
        }
        return strArr2;
    }
}
