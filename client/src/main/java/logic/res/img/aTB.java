package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.aTB */
/* compiled from: a */
public class aTB extends C2404ev {
    public static String bQu = "button_left_disabled";
    public static String bRl = "button_left_enabled";
    public static String bSA = "button_down_press";
    public static String bSB = "button_minus_disabled";
    public static String bSC = "button_minus_enabled";
    public static String bSE = "button_minus_over";
    public static String bSF = "button_minus_press";
    public static String bSG = "button_plus_disabled";
    public static String bSH = "button_plus_enabled";
    public static String bSJ = "button_plus_over";
    public static String bSK = "button_plus_press";
    public static String bSP = "button_up_disabled";
    public static String bSQ = "button_up_enabled";
    public static String bSS = "button_up_over";
    public static String bST = "button_up_press";
    public static String bSw = "button_down_disabled";
    public static String bSx = "button_down_enabled";
    public static String bSz = "button_down_over";
    public static String bTF = "button_right_over";
    public static String bTG = "button_right_press";
    public static String bTd = "button_left_over";
    public static String bTe = "button_left_press";
    public static String bTj = "button_right_disabled";
    public static String bTk = "button_right_enabled";
    public static String[] bXK = {bSC, bSE, bSF, bSB};
    public static String[] bXQ = {bSx, bSz, bSA, bSw};
    public static String[] bXR = {bTk, bTF, bTG, bTj};
    public static String[] bXs = {bSH, bSJ, bSK, bSG};
    public static String[] bYi = {bSQ, bSS, bST, bSP};
    public static String[] bYw = {bRl, bTd, bTe, bQu};
    public static String iQA = "botao_erro_over_left";
    public static String iQB = "botao_erro_over_right";
    public static String iQC = "botao_criacao_normal_center";
    public static String iQD = "button_handle_horizontal_enabled";
    public static String iQE = "button_handle_vertical_over";
    public static String iQF = "button_handle_vertical_press";
    public static String iQG = "fundo_bottom";
    public static String iQH = "fundo_caixa_center";
    public static String iQI = "dropdown_frame_lower_right";
    public static String iQJ = "botao_erro_pressed_left";
    public static String iQK = "botao_erro_pressed_right";
    public static String iQL = "barra_titulo_left";
    public static String iQM = "barra_titulo_right";
    public static String iQN = "barra_titulo_glow";
    public static String iQO = "caixa_nome_top";
    public static String iQP = "fundo_caixa";
    public static String iQQ = "dropdown_frame_bottom";
    public static String iQR = "brilho_azul_botao";
    public static String iQS = "botao_criacao_normal_left";
    public static String iQT = "botao_criacao_normal_right";
    public static String iQU = "botao_criacao_over_center";
    public static String iQV = "botao_criacao_over_left";
    public static String iQW = "botao_criacao_over_right";
    public static String iQX = "botao_criacao_pressed_center";
    public static String iQY = "botao_criacao_pressed_left";
    public static String iQZ = "botao_criacao_pressed_right";
    public static String iQp = "sinal_alerta";
    public static String iQq = "button_handle_horizontal_disabled";
    public static String iQr = "dropdown_frame_center";
    public static String iQs = "botao_erro_normal_center";
    public static String iQt = "botao_erro_over_center";
    public static String iQu = "glow_azul";
    public static String iQv = "button_handle_vertical_disabled";
    public static String iQw = "button_handle_vertical_enabled";
    public static String iQx = "botao_erro_pressed_center";
    public static String iQy = "botao_erro_normal_left";
    public static String iQz = "botao_erro_normal_right";
    public static String iRA = "dropdown_frame_top";
    public static String iRB = "dropdown_frame_upper_left";
    public static String iRC = "textbox_frame_bottom_enabled";
    public static String iRD = "textbox_frame_bottom_selected";
    public static String iRE = "textbox_frame_center_enabled";
    public static String iRF = "textbox_frame_center_selected";
    public static String iRG = "textbox_frame_left_enabled";
    public static String iRH = "textbox_frame_left_selected";
    public static String iRI = "textbox_frame_lower_left_enabled";
    public static String iRJ = "textbox_frame_lower_left_selected";
    public static String iRK = "textbox_frame_lower_right_enabled";
    public static String iRL = "textbox_frame_lower_right_selected";
    public static String iRM = "textbox_frame_right_enabled";
    public static String iRN = "textbox_frame_right_selected";
    public static String iRO = "textbox_frame_top_enabled";
    public static String iRP = "textbox_frame_top_selected";
    public static String iRQ = "textbox_frame_upper_left_enabled";
    public static String iRR = "textbox_frame_upper_left_selected";
    public static String iRS = "textbox_frame_upper_right_enabled";
    public static String iRT = "textbox_frame_upper_right_selected";
    public static String iRU = "new_station_02";
    public static String[] iRV;
    public static String[] iRW;
    public static String[] iRX;
    public static String[] iRZ;
    public static String iRa = "barra_titulo_center";
    public static String iRb = "caixa_nome_upper_left";
    public static String iRc = "button_handle_horizontal_over";
    public static String iRd = "button_handle_horizontal_press";
    public static String[] iRY = {iQD, iRc, iRd, iQq};
    public static String iRe = "caixa_nome_upper_right";
    public static String iRf = "fundo_center";
    public static String iRg = "fundo_escuro_caixa";
    public static String iRh = "fundo_left";
    public static String iRi = "fundo_lower_left";
    public static String iRj = "caixa_nome_bottom";
    public static String iRk = "new_station_01";
    public static String iRl = "fundo_lower_right";
    public static String iRm = "fundo_right";
    public static String iRn = "fundo_top";
    public static String iRo = "fundo_upper_left";
    public static String iRp = "fundo_upper_right";
    public static String iRq = "caixa_nome_center";
    public static String iRr = "caixa_nome_left";
    public static String iRs = "caixa_nome_lower_left";
    public static String iRt = "caixa_nome_lower_right";
    public static String iRu = "caixa_nome_right";
    public static String iRv = "fundo_caixa_nomes";
    public static String iRw = "dropdown_frame_lower_left";
    public static String iRx = "dropdown_frame_right";
    public static String iRy = "dropdown_frame_upper_right";
    public static String iRz = "dropdown_frame_left";
    public static String[] iSa = {iQw, iQE, iQF, iQv};
    public static String[] iSb;
    public static String[] iSc;
    public static String[] iSd;
    public static String[] iSe;
    public static String[] iSf;
    public static String ieA = "window_center";
    public static String ieW = "window_bottom";
    public static String ieX = "window_left";
    public static String ieY = "window_lower_left";
    public static String ieZ = "window_lower_right";
    public static String ifa = "window_right";
    public static String ifb = "window_top";
    public static String ifc = "window_upper_left";
    public static String ifd = "window_upper_right";

    /* renamed from: ov */
    public static String f3790ov = "imageset_neutral";

    static {
        String[] strArr = new String[5];
        strArr[0] = iRM;
        strArr[4] = iRN;
        iRV = strArr;
        String[] strArr2 = new String[5];
        strArr2[0] = iRC;
        strArr2[4] = iRD;
        iRW = strArr2;
        String[] strArr3 = new String[5];
        strArr3[0] = iRI;
        strArr3[4] = iRJ;
        iRX = strArr3;
        String[] strArr4 = new String[5];
        strArr4[0] = iRQ;
        strArr4[4] = iRR;
        iRZ = strArr4;
        String[] strArr5 = new String[5];
        strArr5[0] = iRO;
        strArr5[4] = iRP;
        iSb = strArr5;
        String[] strArr6 = new String[5];
        strArr6[0] = iRK;
        strArr6[4] = iRL;
        iSc = strArr6;
        String[] strArr7 = new String[5];
        strArr7[0] = iRE;
        strArr7[4] = iRF;
        iSd = strArr7;
        String[] strArr8 = new String[5];
        strArr8[0] = iRG;
        strArr8[4] = iRH;
        iSe = strArr8;
        String[] strArr9 = new String[5];
        strArr9[0] = iRS;
        strArr9[4] = iRT;
        iSf = strArr9;
    }

    /* renamed from: a */
    public static String m18326a(String str, String str2, String str3) {
        if (m18324B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m18324B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f3790ov);
    }

    /* renamed from: b */
    public static String m18328b(String str, String str2) {
        if (m18324B(str)) {
            return str;
        }
        if (m18324B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f3790ov);
    }

    /* renamed from: B */
    public static boolean m18324B(String str) {
        if (str == null) {
            return false;
        }
        try {
            aTB.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m18329b(String str, String str2, String str3) {
        return String.valueOf(f3790ov) + C0147Bi.SEPARATOR + m18326a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m18330c(String str, String str2) {
        return String.valueOf(f3790ov) + C0147Bi.SEPARATOR + m18328b(str, str2);
    }

    /* renamed from: a */
    public static String m18325a(String str, int i, String str2) {
        if (!m18324B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f3790ov);
        }
        try {
            Field declaredField = aTB.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f3790ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f3790ov) + C0147Bi.SEPARATOR + m18328b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f3790ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f3790ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f3790ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m18327a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m18324B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f3790ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f3790ov) + C0147Bi.SEPARATOR + m18328b(strArr[i], str);
        }
        return strArr2;
    }
}
