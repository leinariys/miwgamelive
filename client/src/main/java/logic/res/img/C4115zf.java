package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.zf */
/* compiled from: a */
public class C4115zf extends C2404ev {
    public static String bNE = "botao_fechar_disabled";
    public static String bNF = "botao_fechar_over";
    public static String bNG = "botao_fechar_pressed";
    public static String bNH = "button_text_disabled";
    public static String bNI = "button_text_enabled";
    public static String bNJ = "button_text_over";
    public static String bNK = "button_text_press";
    public static String bNL = "botao_entrar_disabled";
    public static String bNM = "caixa_botao_normal";
    public static String bNN = "caixa_botao_over";
    public static String bNO = "caixa_botao_pressed";
    public static String bNP = "carregando_center";
    public static String bNQ = "carregando_left";
    public static String bNR = "carregando_right";
    public static String bNS = "caixa_termos_bottom";
    public static String bNT = "caixa_termos_left";
    public static String bNU = "caixa_termos_lower_left";
    public static String bNV = "carregando_barra";
    public static String bNW = "loadingbar_normal_center";
    public static String bNX = "loadingbar_shine_center";
    public static String bNY = "senha_top";
    public static String bNZ = "senha_upper_left";
    public static String[] bOA = {bNI, bNJ, bNK, bNH};
    public static String bOa = "senha_upper_right";
    public static String bOb = "senha_bottom";
    public static String bOc = "botao_entrar_over";
    public static String bOd = "loadingbar_normal_left";
    public static String bOe = "loadingbar_normal_right";
    public static String bOf = "loadingbar_shine_left";
    public static String bOg = "loadingbar_shine_right";
    public static String bOh = "caixa_termos_lower_right";
    public static String bOi = "caixa_termos_right";
    public static String bOj = "caixa_termos_top";
    public static String bOk = "caixa_termos_upper_left";
    public static String bOl = "caixa_termos_upper_right";
    public static String bOm = "caixa_selecao_menu";
    public static String bOn = "senha_lower_left";
    public static String bOo = "senha_lower_right";
    public static String bOp = "caixa_termos_center";
    public static String bOq = "senha_left";
    public static String bOr = "senha_right";
    public static String bOs = "table_background_main";
    public static String bOt = "transparentBackground";
    public static String bOu = "senha_center";
    public static String bOv = "botao_entrar_pressed";
    public static String bOw = "logo_taikodom";
    public static String bOx = "logo_lettering";
    public static String[] bOy;
    public static String[] bOz;

    /* renamed from: ov */
    public static String f9705ov = "imageset_login";

    static {
        String[] strArr = new String[4];
        strArr[1] = bOc;
        strArr[3] = bNL;
        bOy = strArr;
        String[] strArr2 = new String[4];
        strArr2[1] = bNF;
        strArr2[3] = bNE;
        bOz = strArr2;
    }

    /* renamed from: a */
    public static String m41696a(String str, String str2, String str3) {
        if (m41694B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m41694B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f9705ov);
    }

    /* renamed from: b */
    public static String m41698b(String str, String str2) {
        if (m41694B(str)) {
            return str;
        }
        if (m41694B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f9705ov);
    }

    /* renamed from: B */
    public static boolean m41694B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C4115zf.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m41699b(String str, String str2, String str3) {
        return String.valueOf(f9705ov) + C0147Bi.SEPARATOR + m41696a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m41700c(String str, String str2) {
        return String.valueOf(f9705ov) + C0147Bi.SEPARATOR + m41698b(str, str2);
    }

    /* renamed from: a */
    public static String m41695a(String str, int i, String str2) {
        if (!m41694B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f9705ov);
        }
        try {
            Field declaredField = C4115zf.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f9705ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f9705ov) + C0147Bi.SEPARATOR + m41698b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f9705ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f9705ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f9705ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m41697a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m41694B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f9705ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f9705ov) + C0147Bi.SEPARATOR + m41698b(strArr[i], str);
        }
        return strArr2;
    }
}
