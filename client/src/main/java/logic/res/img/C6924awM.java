package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.awM  reason: case insensitive filesystem */
/* compiled from: a */
public class C6924awM extends C2404ev {
    public static String gNR = "Untitled_1";
    public static String gNS = "Untitled_2b";

    /* renamed from: ov */
    public static String f5510ov = "imageset_hwt_test";

    /* renamed from: a */
    public static String m26859a(String str, String str2, String str3) {
        if (m26857B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m26857B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f5510ov);
    }

    /* renamed from: b */
    public static String m26861b(String str, String str2) {
        if (m26857B(str)) {
            return str;
        }
        if (m26857B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f5510ov);
    }

    /* renamed from: B */
    public static boolean m26857B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C6924awM.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m26862b(String str, String str2, String str3) {
        return String.valueOf(f5510ov) + C0147Bi.SEPARATOR + m26859a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m26863c(String str, String str2) {
        return String.valueOf(f5510ov) + C0147Bi.SEPARATOR + m26861b(str, str2);
    }

    /* renamed from: a */
    public static String m26858a(String str, int i, String str2) {
        if (!m26857B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f5510ov);
        }
        try {
            Field declaredField = C6089agJ.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f5510ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f5510ov) + C0147Bi.SEPARATOR + m26861b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f5510ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f5510ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f5510ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m26860a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m26857B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f5510ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f5510ov) + C0147Bi.SEPARATOR + m26861b(strArr[i], str);
        }
        return strArr2;
    }
}
