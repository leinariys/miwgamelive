package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.Ya */
/* compiled from: a */
public class C1670Ya extends C2404ev {
    public static String eKV = "tela06";

    /* renamed from: ov */
    public static String f2175ov = "imageset_loading_bg6";

    /* renamed from: a */
    public static String m11860a(String str, String str2, String str3) {
        if (m11858B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m11858B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f2175ov);
    }

    /* renamed from: b */
    public static String m11862b(String str, String str2) {
        if (m11858B(str)) {
            return str;
        }
        if (m11858B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f2175ov);
    }

    /* renamed from: B */
    public static boolean m11858B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C1670Ya.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m11863b(String str, String str2, String str3) {
        return String.valueOf(f2175ov) + C0147Bi.SEPARATOR + m11860a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m11864c(String str, String str2) {
        return String.valueOf(f2175ov) + C0147Bi.SEPARATOR + m11862b(str, str2);
    }

    /* renamed from: a */
    public static String m11859a(String str, int i, String str2) {
        if (!m11858B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f2175ov);
        }
        try {
            Field declaredField = C6089agJ.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f2175ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f2175ov) + C0147Bi.SEPARATOR + m11862b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f2175ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f2175ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f2175ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m11861a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m11858B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f2175ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f2175ov) + C0147Bi.SEPARATOR + m11862b(strArr[i], str);
        }
        return strArr2;
    }
}
