package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.lT */
/* compiled from: a */
public class C2885lT extends C2404ev {
    public static String aAD = "scanline";

    /* renamed from: ov */
    public static String f8534ov = "imageset_interactionmode";

    /* renamed from: a */
    public static String m34873a(String str, String str2, String str3) {
        if (m34871B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m34871B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f8534ov);
    }

    /* renamed from: b */
    public static String m34875b(String str, String str2) {
        if (m34871B(str)) {
            return str;
        }
        if (m34871B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f8534ov);
    }

    /* renamed from: B */
    public static boolean m34871B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C2885lT.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m34876b(String str, String str2, String str3) {
        return String.valueOf(f8534ov) + C0147Bi.SEPARATOR + m34873a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m34877c(String str, String str2) {
        return String.valueOf(f8534ov) + C0147Bi.SEPARATOR + m34875b(str, str2);
    }

    /* renamed from: a */
    public static String m34872a(String str, int i, String str2) {
        if (!m34871B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f8534ov);
        }
        try {
            Field declaredField = C2885lT.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f8534ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f8534ov) + C0147Bi.SEPARATOR + m34875b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f8534ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f8534ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f8534ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m34874a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m34871B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f8534ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f8534ov) + C0147Bi.SEPARATOR + m34875b(strArr[i], str);
        }
        return strArr2;
    }
}
