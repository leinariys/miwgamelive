package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.Yf */
/* compiled from: a */
public class C1675Yf extends C2404ev {

    /* renamed from: Ax */
    public static String f2183Ax = "tela03";

    /* renamed from: ov */
    public static String f2184ov = "imageset_loading_bg3";

    /* renamed from: a */
    public static String m11888a(String str, String str2, String str3) {
        if (m11886B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m11886B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f2184ov);
    }

    /* renamed from: b */
    public static String m11890b(String str, String str2) {
        if (m11886B(str)) {
            return str;
        }
        if (m11886B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f2184ov);
    }

    /* renamed from: B */
    public static boolean m11886B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C1675Yf.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m11891b(String str, String str2, String str3) {
        return String.valueOf(f2184ov) + C0147Bi.SEPARATOR + m11888a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m11892c(String str, String str2) {
        return String.valueOf(f2184ov) + C0147Bi.SEPARATOR + m11890b(str, str2);
    }

    /* renamed from: a */
    public static String m11887a(String str, int i, String str2) {
        if (!m11886B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f2184ov);
        }
        try {
            Field declaredField = C1675Yf.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f2184ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f2184ov) + C0147Bi.SEPARATOR + m11890b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f2184ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f2184ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f2184ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m11889a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m11886B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f2184ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f2184ov) + C0147Bi.SEPARATOR + m11890b(strArr[i], str);
        }
        return strArr2;
    }
}
