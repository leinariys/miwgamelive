package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.vS */
/* compiled from: a */
public class C3823vS extends C2404ev {
    public static String bAA = "npc_car_nzeanzo";
    public static String bAB = "npc_car_oz";
    public static String bAC = "npc_car_ptah";
    public static String bAD = "npc_car_shenzou";
    public static String bAE = "npc_dev_mestredosmagos";
    public static String bAF = "npc_empty";
    public static String bAG = "npc_pir_rage";
    public static String bAH = "npc_pir_stiletto";
    public static String bAI = "npc_pir_suzaneindigo";
    public static String bAJ = "npc_resl_thecollector";
    public static String bAK = "npc_res_bihaiko";
    public static String bAL = "npc_res_cobra";
    public static String bAM = "npc_res_fernandomachado";
    public static String bAN = "npc_res_gregbarber";
    public static String bAO = "npc_res_kirstenhare";
    public static String bAP = "npc_res_lorimccall";
    public static String bAQ = "npc_res_michaeldoyle";
    public static String bAR = "npc_res_shears";
    public static String bAS = "npc_res_wesbelmont";
    public static String bAT = "npc_spa_alfredmcguffin";
    public static String bAU = "npc_spa_bartgoodwin";
    public static String bAV = "npc_spa_bugsylansky";
    public static String bAW = "npc_spa_caioavidio";
    public static String bAX = "npc_spa_chissanosao";
    public static String bAY = "npc_spa_heinzgrafsteiner";
    public static String bAZ = "npc_spa_iliabaader";
    public static String bAc = "npc_arc_baykohan";
    public static String bAd = "npc_arc_brickdurden";
    public static String bAe = "npc_arc_jaimesalinas";
    public static String bAf = "npc_bel_anthonyrichards";
    public static String bAg = "npc_bel_baykohan";
    public static String bAh = "npc_bel_elliotfrost";
    public static String bAi = "npc_bel_leelakaram";
    public static String bAj = "npc_bel_milajolie";
    public static String bAk = "npc_bel_rochellespitfire";
    public static String bAl = "npc_bel_samkirchner";
    public static String bAm = "npc_bel_sanchez";
    public static String bAn = "npc_bel_sarakell";
    public static String bAo = "npc_bel_tashairynova";
    public static String bAp = "npc_bel_tyrone";
    public static String bAq = "npc_bel_wilsonpatch";
    public static String bAr = "npc_car_abbey";
    public static String bAs = "npc_car_aita";
    public static String bAt = "npc_car_aurora";
    public static String bAu = "npc_car_cautha";
    public static String bAv = "npc_car_hathor";
    public static String bAw = "npc_car_heremita";
    public static String bAx = "npc_car_hermes";
    public static String bAy = "npc_car_isis";
    public static String bAz = "npc_car_jamesalfred";
    public static String bBa = "npc_spa_irenethompson";
    public static String bBb = "npc_spa_isadorakaterina";
    public static String bBc = "npc_spa_jacquesbeauvoir";
    public static String bBd = "npc_spa_juliahonore";
    public static String bBe = "npc_spa_kevinness";
    public static String bBf = "npc_spa_larasambaquy";
    public static String bBg = "npc_spa_lorensung";
    public static String bBh = "npc_spa_marcellebauldelaire";
    public static String bBi = "npc_spa_mortmerresler";
    public static String bBj = "npc_spa_nathanhale";
    public static String bBk = "npc_spa_neilpaice";
    public static String bBl = "npc_spa_richardboone";
    public static String bBm = "npc_spa_sofiavanvogt";
    public static String bBn = "npc_spa_sparrow";
    public static String bBo = "npc_spa_stanleyclarcke";
    public static String bBp = "npc_spa_tarantula";
    public static String bBq = "npc_spa_turgutbey";
    public static String bBr = "npc_spa_waynemesquitajr";
    public static String bBs = "npc_wor_henrydutch";
    public static String bBt = "npc_wrm_charlesforsythe";
    public static String bBu = "avatar_max_";
    public static String bBv = "avatar_portrait_";

    /* renamed from: ov */
    public static String f9415ov = "imageset_avatar";

    /* renamed from: a */
    public static String m40257a(String str, String str2, String str3) {
        if (m40255B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m40255B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f9415ov);
    }

    /* renamed from: b */
    public static String m40259b(String str, String str2) {
        if (m40255B(str)) {
            return str;
        }
        if (m40255B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f9415ov);
    }

    /* renamed from: B */
    public static boolean m40255B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C3823vS.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m40260b(String str, String str2, String str3) {
        return String.valueOf(f9415ov) + C0147Bi.SEPARATOR + m40257a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m40261c(String str, String str2) {
        return String.valueOf(f9415ov) + C0147Bi.SEPARATOR + m40259b(str, str2);
    }

    /* renamed from: a */
    public static String m40256a(String str, int i, String str2) {
        if (!m40255B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f9415ov);
        }
        try {
            Field declaredField = C3823vS.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f9415ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f9415ov) + C0147Bi.SEPARATOR + m40259b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f9415ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f9415ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f9415ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m40258a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m40255B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f9415ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f9415ov) + C0147Bi.SEPARATOR + m40259b(strArr[i], str);
        }
        return strArr2;
    }
}
