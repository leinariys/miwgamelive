package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.mY */
/* compiled from: a */
public class C2979mY extends C2404ev {
    public static String aGy = "tela_login_fundo";

    /* renamed from: ov */
    public static String f8655ov = "imageset_login_background";

    /* renamed from: a */
    public static String m35787a(String str, String str2, String str3) {
        if (m35785B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m35785B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f8655ov);
    }

    /* renamed from: b */
    public static String m35789b(String str, String str2) {
        if (m35785B(str)) {
            return str;
        }
        if (m35785B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f8655ov);
    }

    /* renamed from: B */
    public static boolean m35785B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C2979mY.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m35790b(String str, String str2, String str3) {
        return String.valueOf(f8655ov) + C0147Bi.SEPARATOR + m35787a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m35791c(String str, String str2) {
        return String.valueOf(f8655ov) + C0147Bi.SEPARATOR + m35789b(str, str2);
    }

    /* renamed from: a */
    public static String m35786a(String str, int i, String str2) {
        if (!m35785B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f8655ov);
        }
        try {
            Field declaredField = C2979mY.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f8655ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f8655ov) + C0147Bi.SEPARATOR + m35789b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f8655ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f8655ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f8655ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m35788a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m35785B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f8655ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f8655ov) + C0147Bi.SEPARATOR + m35789b(strArr[i], str);
        }
        return strArr2;
    }
}
