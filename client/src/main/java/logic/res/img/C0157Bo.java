package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.Bo */
/* compiled from: a */
public class C0157Bo extends C2404ev {

    /* renamed from: Zq */
    public static String f254Zq = "damage_top";

    /* renamed from: Zr */
    public static String f255Zr = "damage_left";

    /* renamed from: Zs */
    public static String f256Zs = "damage_right";
    public static String cln = "damage_botton";

    /* renamed from: ov */
    public static String f257ov = "imageset_damage";

    /* renamed from: a */
    public static String m1310a(String str, String str2, String str3) {
        if (m1308B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m1308B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f257ov);
    }

    /* renamed from: b */
    public static String m1312b(String str, String str2) {
        if (m1308B(str)) {
            return str;
        }
        if (m1308B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f257ov);
    }

    /* renamed from: B */
    public static boolean m1308B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C0157Bo.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m1313b(String str, String str2, String str3) {
        return String.valueOf(f257ov) + C0147Bi.SEPARATOR + m1310a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m1314c(String str, String str2) {
        return String.valueOf(f257ov) + C0147Bi.SEPARATOR + m1312b(str, str2);
    }

    /* renamed from: a */
    public static String m1309a(String str, int i, String str2) {
        if (!m1308B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f257ov);
        }
        try {
            Field declaredField = C0157Bo.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f257ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f257ov) + C0147Bi.SEPARATOR + m1312b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f257ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f257ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f257ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m1311a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m1308B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f257ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f257ov) + C0147Bi.SEPARATOR + m1312b(strArr[i], str);
        }
        return strArr2;
    }
}
