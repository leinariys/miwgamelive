package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.Bx */
/* compiled from: a */
public class C0170Bx extends C2404ev {
    public static String cmA = "scrollbar_hor_handle_right_over";
    public static String cmB = "scrollbar_hor_handle_right_press";
    public static String cmC = "scrollbar_ver_frame_center_bottom";
    public static String cmD = "scrollbar_ver_frame_center_top";
    public static String cmE = "scrollbar_ver_handle_bottom_disabled";
    public static String cmF = "scrollbar_ver_handle_bottom_enabled";
    public static String cmG = "scrollbar_ver_handle_bottom_over";
    public static String cmH = "scrollbar_ver_handle_bottom_press";
    public static String cmI = "scrollbar_ver_handle_center_enabled";
    public static String cmJ = "scrollbar_ver_handle_top_disabled";
    public static String cmK = "scrollbar_ver_handle_top_enabled";
    public static String cmL = "scrollbar_ver_handle_top_over";
    public static String cmM = "scrollbar_ver_handle_top_press";
    public static String cmN = "scrollbar_hor_frame_center_expansor";
    public static String cmO = "scrollbar_hor_handle_center_disabled";
    public static String cmP = "scrollbar_hor_handle_center_enabled";
    public static String cmQ = "scrollbar_hor_handle_center_over";
    public static String cmR = "scrollbar_hor_handle_center_press";
    public static String cmS = "scrollbar_ver_frame_center_expansor";
    public static String cmT = "scrollbar_ver_handle_center_disabled";
    public static String cmU = "scrollbar_ver_handle_center_over";
    public static String cmV = "scrollbar_ver_handle_center_press";
    public static String[] cmW = {cmK, cmL, cmM, cmJ};
    public static String cmc = "button_arrow_down_disabled";
    public static String cmd = "button_arrow_down_enabled";
    public static String cme = "button_arrow_down_over";
    public static String cmf = "button_arrow_down_press";
    public static String cmg = "button_arrow_left_disabled";
    public static String cmh = "button_arrow_left_enabled";
    public static String cmi = "button_arrow_left_over";
    public static String cmj = "button_arrow_left_press";
    public static String cmk = "button_arrow_right_disabled";
    public static String cml = "button_arrow_right_enabled";
    public static String cmm = "button_arrow_right_over";
    public static String cmn = "button_arrow_right_press";
    public static String[] cmY = {cml, cmm, cmn, cmk};
    public static String cmo = "button_arrow_up_disabled";
    public static String cmp = "button_arrow_up_enabled";
    public static String cmq = "button_arrow_up_over";
    public static String cmr = "button_arrow_up_press";
    public static String[] cmZ = {cmp, cmq, cmr, cmo};
    public static String cms = "scrollbar_hor_frame_center_left";
    public static String cmt = "scrollbar_hor_frame_center_right";
    public static String cmu = "scrollbar_hor_handle_left_disabled";
    public static String cmv = "scrollbar_hor_handle_left_enabled";
    public static String cmw = "scrollbar_hor_handle_left_over";
    public static String cmx = "scrollbar_hor_handle_left_press";
    public static String cmy = "scrollbar_hor_handle_right_disabled";
    public static String cmz = "scrollbar_hor_handle_right_enabled";
    public static String[] cmX = {cmz, cmA, cmB, cmy};
    public static String[] cna = {cmv, cmw, cmx, cmu};
    public static String[] cnb = {cmd, cme, cmf, cmc};
    public static String[] cnc = {cmP, cmQ, cmR, cmO};
    public static String[] cnd = {cmI, cmU, cmV, cmT};
    public static String[] cne = {cmF, cmG, cmH, cmE};
    public static String[] cnf = {cmh, cmi, cmj, cmg};

    /* renamed from: ov */
    public static String f262ov = "imageset_new_scrollbar";

    /* renamed from: a */
    public static String m1371a(String str, String str2, String str3) {
        if (m1369B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m1369B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f262ov);
    }

    /* renamed from: b */
    public static String m1373b(String str, String str2) {
        if (m1369B(str)) {
            return str;
        }
        if (m1369B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f262ov);
    }

    /* renamed from: B */
    public static boolean m1369B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C0170Bx.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m1374b(String str, String str2, String str3) {
        return String.valueOf(f262ov) + C0147Bi.SEPARATOR + m1371a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m1375c(String str, String str2) {
        return String.valueOf(f262ov) + C0147Bi.SEPARATOR + m1373b(str, str2);
    }

    /* renamed from: a */
    public static String m1370a(String str, int i, String str2) {
        if (!m1369B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f262ov);
        }
        try {
            Field declaredField = C0170Bx.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f262ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f262ov) + C0147Bi.SEPARATOR + m1373b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f262ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f262ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f262ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m1372a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m1369B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f262ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f262ov) + C0147Bi.SEPARATOR + m1373b(strArr[i], str);
        }
        return strArr2;
    }
}
