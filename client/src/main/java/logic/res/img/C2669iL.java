package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.iL */
/* compiled from: a */
public class C2669iL extends C2404ev {

    /* renamed from: ZA */
    public static String f8160ZA = "hud_movement_feedback_right";

    /* renamed from: ZB */
    public static String f8161ZB = "quickbar_frame_bottom_disabled";

    /* renamed from: ZC */
    public static String f8162ZC = "hud_infinite_aim";

    /* renamed from: ZD */
    public static String f8163ZD = "canhoes_frame_left";

    /* renamed from: ZE */
    public static String f8164ZE = "target_waypoint";

    /* renamed from: ZF */
    public static String f8165ZF = "party_frame_lower_left";

    /* renamed from: ZG */
    public static String f8166ZG = "canhoes_frame_lower_left";

    /* renamed from: ZH */
    public static String f8167ZH = "icon_patyleader";

    /* renamed from: ZI */
    public static String f8168ZI = "target_asteroid";

    /* renamed from: ZJ */
    public static String f8169ZJ = "party_formation_cell_left_enabled";

    /* renamed from: ZK */
    public static String f8170ZK = "party_formation_cell_left_over";

    /* renamed from: ZL */
    public static String f8171ZL = "party_formation_cell_left_press";

    /* renamed from: ZM */
    public static String f8172ZM = "party_formation_cell_left_selected";

    /* renamed from: ZN */
    public static String f8173ZN = "party_info_right_disabled";

    /* renamed from: ZO */
    public static String f8174ZO = "party_info_right_enabled";

    /* renamed from: ZP */
    public static String f8175ZP = "party_info_right_over";

    /* renamed from: ZQ */
    public static String f8176ZQ = "party_info_right_press";

    /* renamed from: ZR */
    public static String f8177ZR = "party_info_right_selected";

    /* renamed from: ZS */
    public static String f8178ZS = "target_loot";

    /* renamed from: ZT */
    public static String f8179ZT = "hud_target_integrity_frame_borders";

    /* renamed from: ZU */
    public static String f8180ZU = "window_objectlist_background";

    /* renamed from: ZV */
    public static String f8181ZV = "hud_missile_lock_02";

    /* renamed from: ZW */
    public static String f8182ZW = "hud_target_aim_fire";

    /* renamed from: ZX */
    public static String f8183ZX = "medidores_casco_right_disabled";

    /* renamed from: ZY */
    public static String f8184ZY = "medidores_casco_right_enabled";

    /* renamed from: ZZ */
    public static String f8185ZZ = "medidores_escudo_left_disabled";

    /* renamed from: Zo */
    public static String f8186Zo = "hud_missile_lock_01";

    /* renamed from: Zp */
    public static String f8187Zp = "damage_bottom";

    /* renamed from: Zq */
    public static String f8188Zq = "damage_top";

    /* renamed from: Zr */
    public static String f8189Zr = "damage_left";

    /* renamed from: Zs */
    public static String f8190Zs = "damage_right";

    /* renamed from: Zt */
    public static String f8191Zt = "hud_integrity_left";

    /* renamed from: Zu */
    public static String f8192Zu = "quickbar_frame_center";

    /* renamed from: Zv */
    public static String f8193Zv = "medidores_setores_lower_left_disabled";

    /* renamed from: Zw */
    public static String f8194Zw = "medidores_setores_lower_left_enabled";

    /* renamed from: Zx */
    public static String f8195Zx = "medidores_setores_lower_right_disabled";

    /* renamed from: Zy */
    public static String f8196Zy = "medidores_indicador_casco_right";

    /* renamed from: Zz */
    public static String f8197Zz = "hud_target_selection_cannon_left";
    public static String aaA = "button_party_selected";
    public static String aaB = "lancadores_frame_right";
    public static String aaC = "party_info_double_disabled";
    public static String aaD = "party_info_double_enabled";
    public static String aaE = "party_info_double_over";
    public static String aaF = "party_info_double_press";
    public static String aaG = "party_info_double_selected";
    public static String aaH = "canhoes_frame_bottom";
    public static String aaI = "lancadores_frame_bottom";
    public static String aaJ = "party_info_left_disabled";
    public static String aaK = "party_info_left_enabled";
    public static String aaL = "party_info_left_over";
    public static String aaM = "party_info_left_press";
    public static String aaN = "party_info_left_selected";
    public static String aaO = "target_frame_object_dock_left";
    public static String aaP = "target_frame_object_dock_right";
    public static String aaQ = "quickbar_frame_top";
    public static String aaR = "button_formation_cell_disabled";
    public static String aaS = "button_formation_cell_enabled";
    public static String aaT = "party_frame_lower_right";
    public static String aaU = "party_frame_upper_left";
    public static String aaV = "party_frame_upper_right";
    public static String aaW = "lancadores_frame_lower_right";
    public static String aaX = "target_portal";
    public static String aaY = "target_station";
    public static String aaZ = "button_party_buff_bottom_closed_enabled";
    public static String aaa = "medidores_escudo_left_enabled";
    public static String aab = "medidores_escudo_right_disabled";
    public static String aac = "medidores_escudo_right_enabled";
    public static String aad = "hud_speed";
    public static String aae = "hud_speed_desired";
    public static String aaf = "hud_speed_frame";
    public static String aag = "hud_super_speed_desired";
    public static String aah = "target_frame_missile_lock_1";
    public static String aai = "hud_target_aim_normal";
    public static String aaj = "quickbar_frame_upper_left";
    public static String aak = "medidores_setores_lower_right_enabled";
    public static String aal = "medidores_indicador_escudo_left";
    public static String aam = "hud_target_selection_dock_left";
    public static String aan = "hud_target_selection_dock_right";
    public static String aao = "hud_target_selection_launcher_right";
    public static String aap = "quickbar_frame_right";
    public static String aaq = "quickbar_frame_left";
    public static String aar = "medidores_setores_upper_left_disabled";
    public static String aas = "spa_aim_target";
    public static String aat = "hud_movement_feedback_top";
    public static String aau = "progressbar_party_background";
    public static String aav = "quickbar_frame_bottom_enabled";
    public static String aaw = "button_party_disabled";
    public static String aax = "button_party_enabled";
    public static String aay = "button_party_over";
    public static String aaz = "button_party_press";
    public static String abA = "party_info_middle_enabled";
    public static String abB = "party_info_middle_over";
    public static String abC = "party_info_middle_press";
    public static String abD = "party_info_middle_selected";
    public static String abE = "icon_formation_disabled";
    public static String abF = "icon_formation_enabled";
    public static String abG = "icon_formation_press";
    public static String abH = "icon_formation_selected";
    public static String abI = "party_frame_left";
    public static String abJ = "party_frame_right";
    public static String abK = "spa_aim_infinity";
    public static String abL = "target_corner";
    public static String abM = "button_party_buff_expander_closed_enabled";
    public static String abN = "button_party_buff_expander_closed_over";
    public static String abO = "progressbar_party_hull_center";
    public static String abP = "progressbar_party_hull_left";
    public static String abQ = "progressbar_party_hull_right";
    public static String abR = "progressbar_party_shield_center";
    public static String abS = "progressbar_party_shield_left";
    public static String abT = "progressbar_party_shield_right";
    public static String abU = "party_frame_bottom";
    public static String abV = "party_frame_top";
    public static String abW = "loadingbar_title_center";
    public static String abX = "hud_target_integrity_hull_left";
    public static String abY = "hud_target_integrity_hull_right";
    public static String abZ = "hud_target_integrity_shield_left";
    public static String aba = "button_party_buff_bottom_closed_over";
    public static String abb = "button_party_buff_bottom_closed_press";
    public static String abc = "button_party_buff_bottom_open_enabled";
    public static String abd = "button_party_buff_bottom_open_over";
    public static String abe = "button_party_buff_bottom_open_press";
    public static String abf = "button_party_buff_top_closed_enabled";
    public static String abg = "button_party_buff_top_closed_over";
    public static String abh = "button_party_buff_top_closed_press";
    public static String abi = "button_party_buff_top_open_enabled";
    public static String abj = "button_party_buff_top_open_over";
    public static String abk = "button_party_buff_top_open_press";
    public static String abl = "target_dist_center";
    public static String abm = "target_dist_left";
    public static String abn = "target_name_center";
    public static String abo = "target_name_right";
    public static String abp = "button_party_buff_center_closed_enabled";
    public static String abq = "button_party_buff_center_closed_over";
    public static String abr = "button_party_buff_center_closed_press";
    public static String abs = "button_party_buff_center_open_enabled";
    public static String abt = "button_party_buff_center_open_over";
    public static String abu = "button_party_buff_center_open_press";
    public static String abv = "party_formation_cell_center_enabled";
    public static String abw = "party_formation_cell_center_over";
    public static String abx = "party_formation_cell_center_press";
    public static String aby = "party_formation_cell_center_selected";
    public static String abz = "party_info_middle_disabled";
    public static String acA = "button_formation_cell_normal";
    public static String acB = "button_formation_cell_over";
    public static String acC = "button_formation_cell_over_selected";
    public static String acD = "button_formation_cell_press";
    public static String acE = "button_formation_cell_selected";
    public static String acF = "icon_bomber";
    public static String acG = "icon_docked";
    public static String acH = "icon_explorer";
    public static String acI = "icon_fighter";
    public static String acJ = "icon_freighter";
    public static String acK = "icon_inrange";
    public static String acL = "icon_noship";
    public static String acM = "icon_outofrange";
    public static String acN = "target_boss";
    public static String acO = "target_npc";
    public static String acP = "target_player";
    public static String acQ = "button_party_buff_expander_closed_press";
    public static String acR = "button_party_buff_expander_open_enabled";
    public static String acS = "button_party_buff_expander_open_over";
    public static String acT = "button_party_buff_expander_open_press";
    public static String acU = "target_frame_range_lock_3";
    public static String acV = "target_frame_sector";
    public static String acW = "target_hull_00";
    public static String acX = "target_hull_01";
    public static String acY = "target_hull_02";
    public static String acZ = "target_hull_03";
    public static String aca = "hud_target_integrity_shield_right";
    public static String acb = "hud_target_integrity_frame_center";
    public static String acc = "hud_target_integrity_hull_center";
    public static String acd = "hud_target_integrity_shield_center";
    public static String ace = "hud_integrity_right";
    public static String acf = "hud_integrity_left_fill";
    public static String acg = "hud_integrity_right_fill";
    public static String ach = "hud_super_speed_frame";
    public static String aci = "target_frame_missile_lock_2";
    public static String acj = "target_frame_object";
    public static String ack = "target_frame_range_lock_1";
    public static String acl = "target_frame_range_lock_2";
    public static String acm = "hud_target_selection_normal";
    public static String acn = "quickbar_frame_upper_right";
    public static String aco = "medidores_setores_upper_left_enabled";
    public static String acp = "medidores_setores_upper_right_disabled";
    public static String acq = "button_party_formationframe_enabled";
    public static String acr = "button_party_formationframe_over";
    public static String acs = "button_party_formationframe_press";
    public static String act = "button_party_formationframe_selected";
    public static String acu = "ico_turret_disabled";
    public static String acv = "ico_turret_enabled";
    public static String acw = "party_formation_cell_right_enabled";
    public static String acx = "party_formation_cell_right_over";
    public static String acy = "party_formation_cell_right_press";
    public static String acz = "party_formation_cell_right_selected";
    public static String[] adA;
    public static String[] adB;
    public static String[] adC;
    public static String[] adD;
    public static String[] adE;
    public static String[] adF = {aax, aay, aaz, aaw, aaA};
    public static String[] adG;
    public static String[] adH = {aaK, aaL, aaM, aaJ, aaN};
    public static String[] adI;
    public static String[] adJ = {aaS, acB, acD, aaR, acE};
    public static String[] adK;
    public static String[] adL;
    public static String[] adM;
    public static String[] adN;
    public static String[] adO = {f8174ZO, f8175ZP, f8176ZQ, f8173ZN, f8177ZR};
    public static String[] adP;
    public static String[] adQ;
    public static String[] adR;
    public static String[] adS;
    public static String[] adT = {aaD, aaE, aaF, aaC, aaG};
    public static String[] adU;
    public static String[] adV;
    public static String[] adW;
    public static String[] adX;
    public static String[] adY;
    public static String[] adZ;
    public static String ada = "target_hull_04";
    public static String adb = "target_hull_05";
    public static String adc = "target_hull_06";
    public static String add = "target_hull_07";
    public static String ade = "target_hull_08";
    public static String adf = "medidores_setores_upper_right_enabled";
    public static String adg = "target_hull_09";
    public static String adh = "target_hull_10";
    public static String adi = "target_hull_block";
    public static String adj = "target_hull_hit";
    public static String adk = "target_sector_hull";
    public static String adl = "target_shield_00";
    public static String adm = "target_shield_01";
    public static String adn = "target_shield_02";
    public static String ado = "target_shield_03";
    public static String adp = "target_shield_04";
    public static String adq = "target_shield_05";
    public static String adr = "target_shield_06";
    public static String ads = "target_shield_07";
    public static String adt = "target_shield_08";
    public static String adu = "target_shield_09";
    public static String adv = "target_shield_10";
    public static String adw = "target_shield_block";
    public static String adx = "target_shield_hit";
    public static String ady = "hud_target_selection_interact";
    public static String adz = "hud_target_selection_lock";
    public static String[] aea;
    public static String[] aeb = {abA, abB, abC, abz, abD};

    /* renamed from: ov */
    public static String f8198ov = "imageset_hud";

    static {
        String[] strArr = new String[4];
        strArr[0] = aco;
        strArr[3] = aar;
        adA = strArr;
        String[] strArr2 = new String[4];
        strArr2[0] = abp;
        strArr2[1] = abq;
        strArr2[2] = abr;
        adB = strArr2;
        String[] strArr3 = new String[5];
        strArr3[0] = acw;
        strArr3[1] = acx;
        strArr3[2] = acy;
        strArr3[4] = acz;
        adC = strArr3;
        String[] strArr4 = new String[4];
        strArr4[0] = acv;
        strArr4[3] = acu;
        adD = strArr4;
        String[] strArr5 = new String[4];
        strArr5[0] = aak;
        strArr5[3] = f8195Zx;
        adE = strArr5;
        String[] strArr6 = new String[4];
        strArr6[0] = abi;
        strArr6[1] = abj;
        strArr6[2] = abk;
        adG = strArr6;
        String[] strArr7 = new String[4];
        strArr7[0] = f8184ZY;
        strArr7[3] = f8183ZX;
        adI = strArr7;
        String[] strArr8 = new String[5];
        strArr8[0] = abF;
        strArr8[2] = abG;
        strArr8[3] = abE;
        strArr8[4] = abH;
        adK = strArr8;
        String[] strArr9 = new String[4];
        strArr9[0] = abs;
        strArr9[1] = abt;
        strArr9[2] = abu;
        adL = strArr9;
        String[] strArr10 = new String[4];
        strArr10[0] = acR;
        strArr10[1] = acS;
        strArr10[2] = acT;
        adM = strArr10;
        String[] strArr11 = new String[4];
        strArr11[0] = aac;
        strArr11[3] = aab;
        adN = strArr11;
        String[] strArr12 = new String[4];
        strArr12[0] = abc;
        strArr12[1] = abd;
        strArr12[2] = abe;
        adP = strArr12;
        String[] strArr13 = new String[4];
        strArr13[0] = adf;
        strArr13[3] = acp;
        adQ = strArr13;
        String[] strArr14 = new String[5];
        strArr14[0] = f8169ZJ;
        strArr14[1] = f8170ZK;
        strArr14[2] = f8171ZL;
        strArr14[4] = f8172ZM;
        adR = strArr14;
        String[] strArr15 = new String[4];
        strArr15[0] = aav;
        strArr15[3] = f8161ZB;
        adS = strArr15;
        String[] strArr16 = new String[5];
        strArr16[0] = abv;
        strArr16[1] = abw;
        strArr16[2] = abx;
        strArr16[4] = aby;
        adU = strArr16;
        String[] strArr17 = new String[4];
        strArr17[0] = abf;
        strArr17[1] = abg;
        strArr17[2] = abh;
        adV = strArr17;
        String[] strArr18 = new String[4];
        strArr18[0] = aaa;
        strArr18[3] = f8185ZZ;
        adW = strArr18;
        String[] strArr19 = new String[4];
        strArr19[0] = aaZ;
        strArr19[1] = aba;
        strArr19[2] = abb;
        adX = strArr19;
        String[] strArr20 = new String[4];
        strArr20[0] = abM;
        strArr20[1] = abN;
        strArr20[2] = acQ;
        adY = strArr20;
        String[] strArr21 = new String[5];
        strArr21[0] = acq;
        strArr21[1] = acr;
        strArr21[2] = acs;
        strArr21[4] = act;
        adZ = strArr21;
        String[] strArr22 = new String[4];
        strArr22[0] = f8194Zw;
        strArr22[3] = f8193Zv;
        aea = strArr22;
    }

    /* renamed from: a */
    public static String m33101a(String str, String str2, String str3) {
        if (m33099B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m33099B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f8198ov);
    }

    /* renamed from: b */
    public static String m33103b(String str, String str2) {
        if (m33099B(str)) {
            return str;
        }
        if (m33099B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f8198ov);
    }

    /* renamed from: B */
    public static boolean m33099B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C2669iL.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m33104b(String str, String str2, String str3) {
        return String.valueOf(f8198ov) + C0147Bi.SEPARATOR + m33101a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m33105c(String str, String str2) {
        return String.valueOf(f8198ov) + C0147Bi.SEPARATOR + m33103b(str, str2);
    }

    /* renamed from: a */
    public static String m33100a(String str, int i, String str2) {
        if (!m33099B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f8198ov);
        }
        try {
            Field declaredField = C2669iL.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f8198ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f8198ov) + C0147Bi.SEPARATOR + m33103b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f8198ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f8198ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f8198ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m33102a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m33099B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f8198ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f8198ov) + C0147Bi.SEPARATOR + m33103b(strArr[i], str);
        }
        return strArr2;
    }
}
