package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.aza  reason: case insensitive filesystem */
/* compiled from: a */
public class C7013aza extends C2404ev {
    public static String gXX = "progressbar_blue_small_left";
    public static String gXY = "progressbar_blue_small_right";
    public static String gXZ = "progressbar_frame_small_left";
    public static String gYa = "progressbar_frame_small_right";
    public static String gYb = "progressbar_red_small_left";
    public static String gYc = "progressbar_red_small_right";
    public static String gYd = "progressbar_blue_small_center";
    public static String gYe = "progressbar_frame_small_center";
    public static String gYf = "progressbar_red_small_center";

    /* renamed from: ov */
    public static String f5698ov = "imageset_new_progressbar";

    /* renamed from: a */
    public static String m27720a(String str, String str2, String str3) {
        if (m27718B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m27718B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f5698ov);
    }

    /* renamed from: b */
    public static String m27722b(String str, String str2) {
        if (m27718B(str)) {
            return str;
        }
        if (m27718B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f5698ov);
    }

    /* renamed from: B */
    public static boolean m27718B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C7013aza.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m27723b(String str, String str2, String str3) {
        return String.valueOf(f5698ov) + C0147Bi.SEPARATOR + m27720a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m27724c(String str, String str2) {
        return String.valueOf(f5698ov) + C0147Bi.SEPARATOR + m27722b(str, str2);
    }

    /* renamed from: a */
    public static String m27719a(String str, int i, String str2) {
        if (!m27718B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f5698ov);
        }
        try {
            Field declaredField = C7013aza.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f5698ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f5698ov) + C0147Bi.SEPARATOR + m27722b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f5698ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f5698ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f5698ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m27721a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m27718B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f5698ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f5698ov) + C0147Bi.SEPARATOR + m27722b(strArr[i], str);
        }
        return strArr2;
    }
}
