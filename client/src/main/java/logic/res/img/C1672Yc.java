package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.Yc */
/* compiled from: a */
public class C1672Yc extends C2404ev {

    /* renamed from: Az */
    public static String f2178Az = "tela05";

    /* renamed from: ov */
    public static String f2179ov = "imageset_loading_bg5";

    /* renamed from: a */
    public static String m11874a(String str, String str2, String str3) {
        if (m11872B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m11872B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f2179ov);
    }

    /* renamed from: b */
    public static String m11876b(String str, String str2) {
        if (m11872B(str)) {
            return str;
        }
        if (m11872B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f2179ov);
    }

    /* renamed from: B */
    public static boolean m11872B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C1672Yc.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m11877b(String str, String str2, String str3) {
        return String.valueOf(f2179ov) + C0147Bi.SEPARATOR + m11874a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m11878c(String str, String str2) {
        return String.valueOf(f2179ov) + C0147Bi.SEPARATOR + m11876b(str, str2);
    }

    /* renamed from: a */
    public static String m11873a(String str, int i, String str2) {
        if (!m11872B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f2179ov);
        }
        try {
            Field declaredField = C1672Yc.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f2179ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f2179ov) + C0147Bi.SEPARATOR + m11876b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f2179ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f2179ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f2179ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m11875a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m11872B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f2179ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f2179ov) + C0147Bi.SEPARATOR + m11876b(strArr[i], str);
        }
        return strArr2;
    }
}
