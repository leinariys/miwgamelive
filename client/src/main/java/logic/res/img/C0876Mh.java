package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.Mh */
/* compiled from: a */
public class C0876Mh extends C2404ev {
    public static String dzx = "map_hack_01_pt";

    /* renamed from: ov */
    public static String f1111ov = "imageset_map_01_pt";

    /* renamed from: a */
    public static String m7141a(String str, String str2, String str3) {
        if (m7139B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m7139B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f1111ov);
    }

    /* renamed from: b */
    public static String m7143b(String str, String str2) {
        if (m7139B(str)) {
            return str;
        }
        if (m7139B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f1111ov);
    }

    /* renamed from: B */
    public static boolean m7139B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C0876Mh.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m7144b(String str, String str2, String str3) {
        return String.valueOf(f1111ov) + C0147Bi.SEPARATOR + m7141a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m7145c(String str, String str2) {
        return String.valueOf(f1111ov) + C0147Bi.SEPARATOR + m7143b(str, str2);
    }

    /* renamed from: a */
    public static String m7140a(String str, int i, String str2) {
        if (!m7139B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f1111ov);
        }
        try {
            Field declaredField = C0876Mh.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f1111ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f1111ov) + C0147Bi.SEPARATOR + m7143b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f1111ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f1111ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f1111ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m7142a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m7139B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f1111ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f1111ov) + C0147Bi.SEPARATOR + m7143b(strArr[i], str);
        }
        return strArr2;
    }
}
