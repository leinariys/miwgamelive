package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.aFN */
/* compiled from: a */
public class aFN extends C2404ev {
    public static String hLA = "cor_aguia01_small";
    public static String hLB = "cor_aguia02_small";
    public static String hLC = "cor_aguia03_small";
    public static String hLD = "cor_arvore01_small";
    public static String hLE = "cor_arvore02_small";
    public static String hLF = "cor_arvore03_small";
    public static String hLG = "cor_caveira01_small";
    public static String hLH = "cor_caveira02_small";
    public static String hLI = "cor_caveira03_small";
    public static String hLJ = "cor_cobra01_small";
    public static String hLK = "cor_cobra02_small";
    public static String hLL = "cor_cobra03_small";
    public static String hLM = "cor_cometa01_small";
    public static String hLN = "cor_cometa02_small";
    public static String hLO = "cor_cometa03_small";
    public static String hLP = "cor_escudo01_small";
    public static String hLQ = "cor_escudo02_small";
    public static String hLR = "cor_escudo03_small";
    public static String hLS = "cor_lobo01_small";
    public static String hLT = "cor_lobo02_small";
    public static String hLU = "cor_lobo03_small";
    public static String hLV = "cor_machado01_small";
    public static String hLW = "cor_machado02_small";
    public static String hLX = "cor_machado03_small";
    public static String hLY = "cor_nave01_small";
    public static String hLZ = "cor_nave02_small";
    public static String hLb = "cor_aguia01_big";
    public static String hLc = "cor_aguia02_big";
    public static String hLd = "cor_aguia03_big";
    public static String hLe = "cor_arvore01_big";
    public static String hLf = "cor_arvore02_big";
    public static String hLg = "cor_arvore03_big";
    public static String hLh = "cor_caveira01_big";
    public static String hLi = "cor_caveira02_big";
    public static String hLj = "cor_caveira03_big";
    public static String hLk = "cor_cobra01_big";
    public static String hLl = "cor_cobra02_big";
    public static String hLm = "cor_cobra03_big";
    public static String hLn = "cor_cometa01_big";
    public static String hLo = "cor_cometa02_big";
    public static String hLp = "cor_cometa03_big";
    public static String hLq = "cor_escudo01_big";
    public static String hLr = "cor_escudo02_big";
    public static String hLs = "cor_escudo03_big";
    public static String hLt = "cor_lobo01_big";
    public static String hLu = "cor_lobo02_big";
    public static String hLv = "cor_lobo03_big";
    public static String hLw = "cor_machado01_big";
    public static String hLx = "cor_machado02_big";
    public static String hLy = "cor_machado03_big";
    public static String hLz = "cor_nave01_big";
    public static String hMA = "cor_cometa01_small_border";
    public static String hMB = "cor_cometa02_small_border";
    public static String hMC = "cor_cometa03_small_border";
    public static String hMD = "cor_arvore03_medium";
    public static String hME = "cor_caveira01_medium";
    public static String hMF = "cor_caveira02_medium";
    public static String hMG = "cor_caveira03_medium";
    public static String hMH = "cor_cobra01_medium";
    public static String hMI = "cor_cobra02_medium";
    public static String hMJ = "cor_cobra03_medium";
    public static String hMK = "cor_cometa01_medium";
    public static String hML = "cor_cometa02_medium";
    public static String hMM = "cor_cometa03_medium";
    public static String hMN = "cor_escudo01_medium";
    public static String hMO = "cor_escudo02_medium";
    public static String hMP = "cor_escudo03_medium";
    public static String hMQ = "cor_lobo01_medium";
    public static String hMR = "cor_lobo02_medium";
    public static String hMS = "cor_lobo03_medium";
    public static String hMT = "cor_consortium_small_border";
    public static String hMU = "cor_escudo01_small_border";
    public static String hMV = "cor_escudo02_small_border";
    public static String hMW = "cor_escudo03_small_border";
    public static String hMX = "cor_lobo01_small_border";
    public static String hMY = "cor_lobo02_small_border";
    public static String hMZ = "cor_lobo03_small_border";
    public static String hMa = "cor_nave03_small";
    public static String hMb = "cor_sol01_small";
    public static String hMc = "cor_sol02_small";
    public static String hMd = "cor_sol03_small";
    public static String hMe = "cor_nave02_big";
    public static String hMf = "cor_nave03_big";
    public static String hMg = "cor_sol01_big";
    public static String hMh = "cor_sol02_big";
    public static String hMi = "cor_aguia01_medium";
    public static String hMj = "cor_aguia02_medium";
    public static String hMk = "cor_aguia03_medium";
    public static String hMl = "cor_arvore01_medium";
    public static String hMm = "cor_arvore02_medium";
    public static String hMn = "cor_aguia01_small_border";
    public static String hMo = "cor_aguia02_small_border";
    public static String hMp = "cor_aguia03_small_border";
    public static String hMq = "cor_arvore01_small_border";
    public static String hMr = "cor_arvore02_small_border";
    public static String hMs = "cor_arvore03_small_border";
    public static String hMt = "cor_caveira01_small_border";
    public static String hMu = "cor_caveira02_small_border";
    public static String hMv = "cor_sol03_big";
    public static String hMw = "cor_caveira03_small_border";
    public static String hMx = "cor_cobra01_small_border";
    public static String hMy = "cor_cobra02_small_border";
    public static String hMz = "cor_cobra03_small_border";
    public static String hNa = "cor_machado01_small_border";
    public static String hNb = "cor_machado02_small_border";
    public static String hNc = "cor_machado03_small_border";
    public static String hNd = "cor_nave01_small_border";
    public static String hNe = "cor_nave02_small_border";
    public static String hNf = "cor_nave03_small_border";
    public static String hNg = "cor_neutral_small_border";
    public static String hNh = "cor_renegades_small_border";
    public static String hNi = "cor_sol01_small_border";
    public static String hNj = "cor_sol02_small_border";
    public static String hNk = "cor_machado01_medium";
    public static String hNl = "cor_machado02_medium";
    public static String hNm = "cor_machado03_medium";
    public static String hNn = "cor_nave01_medium";
    public static String hNo = "cor_nave02_medium";
    public static String hNp = "cor_nave03_medium";
    public static String hNq = "cor_sol01_medium";
    public static String hNr = "cor_sol02_medium";
    public static String hNs = "cor_sol03_medium";
    public static String hNt = "cor_sol03_small_border";

    /* renamed from: ov */
    public static String f2806ov = "imageset_corpimages";

    /* renamed from: a */
    public static String m14549a(String str, String str2, String str3) {
        if (m14547B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m14547B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f2806ov);
    }

    /* renamed from: b */
    public static String m14551b(String str, String str2) {
        if (m14547B(str)) {
            return str;
        }
        if (m14547B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f2806ov);
    }

    /* renamed from: B */
    public static boolean m14547B(String str) {
        if (str == null) {
            return false;
        }
        try {
            aFN.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m14552b(String str, String str2, String str3) {
        return String.valueOf(f2806ov) + C0147Bi.SEPARATOR + m14549a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m14553c(String str, String str2) {
        return String.valueOf(f2806ov) + C0147Bi.SEPARATOR + m14551b(str, str2);
    }

    /* renamed from: a */
    public static String m14548a(String str, int i, String str2) {
        if (!m14547B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f2806ov);
        }
        try {
            Field declaredField = aFN.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f2806ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f2806ov) + C0147Bi.SEPARATOR + m14551b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f2806ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f2806ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f2806ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m14550a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m14547B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f2806ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f2806ov) + C0147Bi.SEPARATOR + m14551b(strArr[i], str);
        }
        return strArr2;
    }
}
