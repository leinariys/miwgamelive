package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.VB */
/* compiled from: a */
public class C1439VB extends C2404ev {
    public static String etV = "help_01_en";
    public static String etW = "help_01_pt";
    public static String etX = "help_02_en";
    public static String etY = "help_02_pt";
    public static String etZ = "help_03_en";
    public static String euA = "help_16_pt";
    public static String eua = "help_03_pt";
    public static String eub = "help_04_en";
    public static String euc = "help_04_pt";
    public static String eud = "help_05_en";
    public static String eue = "help_05_pt";
    public static String euf = "help_06_en";
    public static String eug = "help_06_pt";
    public static String euh = "help_07_en";
    public static String eui = "help_07_pt";
    public static String euj = "help_08_en";
    public static String euk = "help_08_pt";
    public static String eul = "help_09_en";
    public static String eum = "help_09_pt";
    public static String eun = "help_10_en";
    public static String euo = "help_10_pt";
    public static String eup = "help_11_en";
    public static String euq = "help_11_pt";
    public static String eur = "help_12_en";
    public static String eus = "help_12_pt";
    public static String eut = "help_13_en";
    public static String euu = "help_13_pt";
    public static String euv = "help_14_en";
    public static String euw = "help_14_pt";
    public static String eux = "help_15_en";
    public static String euy = "help_15_pt";
    public static String euz = "help_16_en";

    /* renamed from: ov */
    public static String f1848ov = "imageset_help";

    /* renamed from: a */
    public static String m10448a(String str, String str2, String str3) {
        if (m10446B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m10446B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f1848ov);
    }

    /* renamed from: b */
    public static String m10450b(String str, String str2) {
        if (m10446B(str)) {
            return str;
        }
        if (m10446B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f1848ov);
    }

    /* renamed from: B */
    public static boolean m10446B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C1439VB.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m10451b(String str, String str2, String str3) {
        return String.valueOf(f1848ov) + C0147Bi.SEPARATOR + m10448a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m10452c(String str, String str2) {
        return String.valueOf(f1848ov) + C0147Bi.SEPARATOR + m10450b(str, str2);
    }

    /* renamed from: a */
    public static String m10447a(String str, int i, String str2) {
        if (!m10446B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f1848ov);
        }
        try {
            Field declaredField = C1439VB.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f1848ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f1848ov) + C0147Bi.SEPARATOR + m10450b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f1848ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f1848ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f1848ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m10449a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m10446B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f1848ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f1848ov) + C0147Bi.SEPARATOR + m10450b(strArr[i], str);
        }
        return strArr2;
    }
}
