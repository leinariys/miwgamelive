package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.Mt */
/* compiled from: a */
public class C0888Mt extends C2404ev {
    public static String bQL = "chat_window_bottom";
    public static String bQM = "chat_window_center";
    public static String bQN = "chat_window_left";
    public static String bQO = "chat_window_lower_left";
    public static String bRB = "chat_window_lower_right";
    public static String bRC = "chat_window_right";
    public static String bRD = "chat_window_top";
    public static String bRE = "chat_window_upper_left";
    public static String bRS = "chat_window_upper_right";
    public static String bVR = "table_cell_frame_bottom_selected";
    public static String bVU = "table_cell_frame_center_selected";
    public static String bVX = "table_cell_frame_left_selected";
    public static String bWa = "table_cell_frame_lower_left_selected";
    public static String bWd = "table_cell_frame_lower_right_selected";
    public static String bWg = "table_cell_frame_right_selected";
    public static String bWj = "table_cell_frame_top_selected";
    public static String bWm = "table_cell_frame_upper_left_selected";
    public static String bWp = "table_cell_frame_upper_right_selected";
    public static String dAJ = "launcher_frame";
    public static String dAK = "weapon_frame";
    public static String dAL = "monitor_frame_upper_left";
    public static String dAM = "monitor_frame_upper_right";
    public static String dAN = "monitor_frame_lower_left";
    public static String dAO = "button_monitor_center_disabled";
    public static String dAP = "button_monitor_center_enabled";
    public static String dAQ = "button_monitor_center_over";
    public static String dAR = "button_monitor_center_press";
    public static String dAS = "monitor_frame_left";
    public static String dAT = "monitor_frame_right";
    public static String dAU = "monitor_frame_top";
    public static String dAV = "ico_ammo_number";
    public static String dAW = "monitor_frame_lower_right";
    public static String dAX = "button_monitor_left_disabled";
    public static String dAY = "button_monitor_left_enabled";
    public static String dAZ = "button_monitor_left_over";
    public static String dBa = "button_monitor_left_press";
    public static String dBb = "button_monitor_right_disabled";
    public static String dBc = "button_monitor_right_enabled";
    public static String dBd = "button_monitor_right_over";
    public static String dBe = "button_monitor_right_press";
    public static String[] dBf = {dBc, dBd, dBe, dBb};
    public static String[] dBg = {dAY, dAZ, dBa, dAX};
    public static String[] dBh = {dAP, dAQ, dAR, dAO};

    /* renamed from: ov */
    public static String f1164ov = "imageset_monitor_hack";

    /* renamed from: a */
    public static String m7182a(String str, String str2, String str3) {
        if (m7180B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m7180B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f1164ov);
    }

    /* renamed from: b */
    public static String m7184b(String str, String str2) {
        if (m7180B(str)) {
            return str;
        }
        if (m7180B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f1164ov);
    }

    /* renamed from: B */
    public static boolean m7180B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C0888Mt.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m7185b(String str, String str2, String str3) {
        return String.valueOf(f1164ov) + C0147Bi.SEPARATOR + m7182a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m7186c(String str, String str2) {
        return String.valueOf(f1164ov) + C0147Bi.SEPARATOR + m7184b(str, str2);
    }

    /* renamed from: a */
    public static String m7181a(String str, int i, String str2) {
        if (!m7180B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f1164ov);
        }
        try {
            Field declaredField = C6089agJ.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f1164ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f1164ov) + C0147Bi.SEPARATOR + m7184b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f1164ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f1164ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f1164ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m7183a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m7180B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f1164ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f1164ov) + C0147Bi.SEPARATOR + m7184b(strArr[i], str);
        }
        return strArr2;
    }
}
