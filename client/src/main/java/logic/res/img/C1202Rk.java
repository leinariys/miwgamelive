package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.Rk */
/* compiled from: a */
public class C1202Rk extends C2404ev {
    public static String bRl = "button_left_enabled";
    public static String bTF = "button_right_over";
    public static String bTG = "button_right_press";
    public static String bTd = "button_left_over";
    public static String bTe = "button_left_press";
    public static String bTk = "button_right_enabled";
    public static String bUm = "button_center_enabled";
    public static String bUo = "button_center_over";
    public static String bUp = "button_center_press";
    public static String[] bXR;
    public static String[] bXy;
    public static String[] bYw;
    public static String dYA = "button_upper_left_enabled";
    public static String dYB = "button_upper_left_over";
    public static String dYC = "button_upper_left_press";
    public static String dYD = "button_upper_right_enabled";
    public static String dYE = "button_upper_right_over";
    public static String dYF = "button_upper_right_press";
    public static String dYG = "spa_bom_omega_over";
    public static String dYH = "spa_bom_omega_press";
    public static String dYI = "spa_bom_omega_selected";
    public static String dYJ = "spa_fig_duration_enabled";
    public static String dYK = "spa_fig_duration_over";
    public static String dYL = "spa_fig_duration_press";
    public static String dYM = "spa_fig_duration_selected";
    public static String dYN = "spa_fig_speed_enabled";
    public static String dYO = "spa_fig_speed_over";
    public static String dYP = "spa_fig_speed_press";
    public static String dYQ = "spa_fig_speed_selected";
    public static String dYR = "spa_fre_increasevaultspace_enabled";
    public static String dYS = "spa_fre_increasevaultspace_over";
    public static String dYT = "spa_fre_increasevaultspace_press";
    public static String dYU = "spa_fre_increasevaultspace_selected";
    public static String dYV = "spa_fre_unlocksotheritems_enabled";
    public static String dYW = "spa_fre_unlocksotheritems_over";
    public static String dYX = "spa_fre_unlocksotheritems_press";
    public static String dYY = "spa_fre_unlocksotheritems_selected";
    public static String dYZ = "spa_exp_duration_enabled";
    public static String dYj = "spa_bom_duration_enabled";
    public static String dYk = "spa_bom_duration_over";
    public static String dYl = "spa_bom_duration_press";
    public static String dYm = "spa_bom_duration_selected";
    public static String dYn = "spa_bom_omega_enabled";
    public static String dYo = "button_bottom_enabled";
    public static String dYp = "button_bottom_over";
    public static String dYq = "button_bottom_press";
    public static String dYr = "button_lower_left_enabled";
    public static String dYs = "button_lower_left_over";
    public static String dYt = "button_lower_left_press";
    public static String dYu = "button_lower_right_enabled";
    public static String dYv = "button_lower_right_over";
    public static String dYw = "button_lower_right_press";
    public static String dYx = "button_top_enabled";
    public static String dYy = "button_top_over";
    public static String dYz = "button_top_press";
    public static String dZa = "spa_exp_duration_over";
    public static String dZb = "spa_exp_duration_press";
    public static String dZc = "spa_exp_duration_selected";
    public static String dZd = "spa_exp_invulnerability_enabled";
    public static String dZe = "spa_exp_invulnerability_over";
    public static String dZf = "spa_exp_invulnerability_press";
    public static String dZg = "spa_exp_invulnerability_selected";
    public static String[] dZh;
    public static String[] dZi;
    public static String[] dZj;
    public static String[] dZk;
    public static String[] dZl;
    public static String[] dZm;
    public static String[] dZn;
    public static String[] dZo;
    public static String[] dZp;
    public static String[] dZq;
    public static String[] dZr;
    public static String[] dZs;
    public static String[] dZt;
    public static String[] dZu;

    /* renamed from: ov */
    public static String f1503ov = "imageset_new_button";

    static {
        String[] strArr = new String[5];
        strArr[0] = dYV;
        strArr[1] = dYW;
        strArr[2] = dYX;
        strArr[4] = dYY;
        dZh = strArr;
        String[] strArr2 = new String[5];
        strArr2[0] = dYn;
        strArr2[1] = dYG;
        strArr2[2] = dYH;
        strArr2[4] = dYI;
        dZi = strArr2;
        String[] strArr3 = new String[4];
        strArr3[0] = dYo;
        strArr3[1] = dYp;
        strArr3[2] = dYq;
        dZj = strArr3;
        String[] strArr4 = new String[5];
        strArr4[0] = dZd;
        strArr4[1] = dZe;
        strArr4[2] = dZf;
        strArr4[4] = dZg;
        dZk = strArr4;
        String[] strArr5 = new String[4];
        strArr5[0] = dYD;
        strArr5[1] = dYE;
        strArr5[2] = dYF;
        dZl = strArr5;
        String[] strArr6 = new String[4];
        strArr6[0] = dYu;
        strArr6[1] = dYv;
        strArr6[2] = dYw;
        dZm = strArr6;
        String[] strArr7 = new String[4];
        strArr7[0] = dYr;
        strArr7[1] = dYs;
        strArr7[2] = dYt;
        dZn = strArr7;
        String[] strArr8 = new String[4];
        strArr8[0] = bRl;
        strArr8[1] = bTd;
        strArr8[2] = bTe;
        bYw = strArr8;
        String[] strArr9 = new String[5];
        strArr9[0] = dYN;
        strArr9[1] = dYO;
        strArr9[2] = dYP;
        strArr9[4] = dYQ;
        dZo = strArr9;
        String[] strArr10 = new String[5];
        strArr10[0] = dYJ;
        strArr10[1] = dYK;
        strArr10[2] = dYL;
        strArr10[4] = dYM;
        dZp = strArr10;
        String[] strArr11 = new String[4];
        strArr11[0] = bUm;
        strArr11[1] = bUo;
        strArr11[2] = bUp;
        bXy = strArr11;
        String[] strArr12 = new String[4];
        strArr12[0] = dYx;
        strArr12[1] = dYy;
        strArr12[2] = dYz;
        dZq = strArr12;
        String[] strArr13 = new String[5];
        strArr13[0] = dYj;
        strArr13[1] = dYk;
        strArr13[2] = dYl;
        strArr13[4] = dYm;
        dZr = strArr13;
        String[] strArr14 = new String[4];
        strArr14[0] = bTk;
        strArr14[1] = bTF;
        strArr14[2] = bTG;
        bXR = strArr14;
        String[] strArr15 = new String[5];
        strArr15[0] = dYZ;
        strArr15[1] = dZa;
        strArr15[2] = dZb;
        strArr15[4] = dZc;
        dZs = strArr15;
        String[] strArr16 = new String[4];
        strArr16[0] = dYA;
        strArr16[1] = dYB;
        strArr16[2] = dYC;
        dZt = strArr16;
        String[] strArr17 = new String[5];
        strArr17[0] = dYR;
        strArr17[1] = dYS;
        strArr17[2] = dYT;
        strArr17[4] = dYU;
        dZu = strArr17;
    }

    /* renamed from: a */
    public static String m9254a(String str, String str2, String str3) {
        if (m9252B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m9252B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f1503ov);
    }

    /* renamed from: b */
    public static String m9256b(String str, String str2) {
        if (m9252B(str)) {
            return str;
        }
        if (m9252B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f1503ov);
    }

    /* renamed from: B */
    public static boolean m9252B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C1202Rk.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m9257b(String str, String str2, String str3) {
        return String.valueOf(f1503ov) + C0147Bi.SEPARATOR + m9254a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m9258c(String str, String str2) {
        return String.valueOf(f1503ov) + C0147Bi.SEPARATOR + m9256b(str, str2);
    }

    /* renamed from: a */
    public static String m9253a(String str, int i, String str2) {
        if (!m9252B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f1503ov);
        }
        try {
            Field declaredField = C1202Rk.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f1503ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f1503ov) + C0147Bi.SEPARATOR + m9256b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f1503ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f1503ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f1503ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m9255a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m9252B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f1503ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f1503ov) + C0147Bi.SEPARATOR + m9256b(strArr[i], str);
        }
        return strArr2;
    }
}
