package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.Yb */
/* compiled from: a */
public class C1671Yb extends C2404ev {

    /* renamed from: Ay */
    public static String f2176Ay = "tela04";

    /* renamed from: ov */
    public static String f2177ov = "imageset_loading_bg4";

    /* renamed from: a */
    public static String m11867a(String str, String str2, String str3) {
        if (m11865B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m11865B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f2177ov);
    }

    /* renamed from: b */
    public static String m11869b(String str, String str2) {
        if (m11865B(str)) {
            return str;
        }
        if (m11865B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f2177ov);
    }

    /* renamed from: B */
    public static boolean m11865B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C1671Yb.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m11870b(String str, String str2, String str3) {
        return String.valueOf(f2177ov) + C0147Bi.SEPARATOR + m11867a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m11871c(String str, String str2) {
        return String.valueOf(f2177ov) + C0147Bi.SEPARATOR + m11869b(str, str2);
    }

    /* renamed from: a */
    public static String m11866a(String str, int i, String str2) {
        if (!m11865B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f2177ov);
        }
        try {
            Field declaredField = C1671Yb.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f2177ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f2177ov) + C0147Bi.SEPARATOR + m11869b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f2177ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f2177ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f2177ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m11868a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m11865B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f2177ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f2177ov) + C0147Bi.SEPARATOR + m11869b(strArr[i], str);
        }
        return strArr2;
    }
}
