package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.dN */
/* compiled from: a */
public class C2257dN extends C2404ev {

    /* renamed from: AA */
    public static String f6499AA = "map_hack_en";

    /* renamed from: ov */
    public static String f6500ov = "imageset_map_en";

    /* renamed from: a */
    public static String m28823a(String str, String str2, String str3) {
        if (m28821B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m28821B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f6500ov);
    }

    /* renamed from: b */
    public static String m28825b(String str, String str2) {
        if (m28821B(str)) {
            return str;
        }
        if (m28821B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f6500ov);
    }

    /* renamed from: B */
    public static boolean m28821B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C2257dN.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m28826b(String str, String str2, String str3) {
        return String.valueOf(f6500ov) + C0147Bi.SEPARATOR + m28823a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m28827c(String str, String str2) {
        return String.valueOf(f6500ov) + C0147Bi.SEPARATOR + m28825b(str, str2);
    }

    /* renamed from: a */
    public static String m28822a(String str, int i, String str2) {
        if (!m28821B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f6500ov);
        }
        try {
            Field declaredField = C6089agJ.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f6500ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f6500ov) + C0147Bi.SEPARATOR + m28825b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f6500ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f6500ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f6500ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m28824a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m28821B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f6500ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f6500ov) + C0147Bi.SEPARATOR + m28825b(strArr[i], str);
        }
        return strArr2;
    }
}
