package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.aFz  reason: case insensitive filesystem */
/* compiled from: a */
public class C5351aFz extends C2404ev {
    public static String hJy = "splash_screen";

    /* renamed from: ov */
    public static String f2845ov = "imageset_splash_screen";

    /* renamed from: a */
    public static String m14715a(String str, String str2, String str3) {
        if (m14713B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m14713B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f2845ov);
    }

    /* renamed from: b */
    public static String m14717b(String str, String str2) {
        if (m14713B(str)) {
            return str;
        }
        if (m14713B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f2845ov);
    }

    /* renamed from: B */
    public static boolean m14713B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C5351aFz.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m14718b(String str, String str2, String str3) {
        return String.valueOf(f2845ov) + C0147Bi.SEPARATOR + m14715a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m14719c(String str, String str2) {
        return String.valueOf(f2845ov) + C0147Bi.SEPARATOR + m14717b(str, str2);
    }

    /* renamed from: a */
    public static String m14714a(String str, int i, String str2) {
        if (!m14713B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f2845ov);
        }
        try {
            Field declaredField = C6089agJ.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f2845ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f2845ov) + C0147Bi.SEPARATOR + m14717b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f2845ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f2845ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f2845ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m14716a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m14713B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f2845ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f2845ov) + C0147Bi.SEPARATOR + m14717b(strArr[i], str);
        }
        return strArr2;
    }
}
