package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.aqT  reason: case insensitive filesystem */
/* compiled from: a */
public class C6619aqT extends C2404ev {
    public static String gqY = "map_hack_02_en";

    /* renamed from: ov */
    public static String f5174ov = "imageset_map_02_en";

    /* renamed from: a */
    public static String m25122a(String str, String str2, String str3) {
        if (m25120B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m25120B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f5174ov);
    }

    /* renamed from: b */
    public static String m25124b(String str, String str2) {
        if (m25120B(str)) {
            return str;
        }
        if (m25120B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f5174ov);
    }

    /* renamed from: B */
    public static boolean m25120B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C6619aqT.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m25125b(String str, String str2, String str3) {
        return String.valueOf(f5174ov) + C0147Bi.SEPARATOR + m25122a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m25126c(String str, String str2) {
        return String.valueOf(f5174ov) + C0147Bi.SEPARATOR + m25124b(str, str2);
    }

    /* renamed from: a */
    public static String m25121a(String str, int i, String str2) {
        if (!m25120B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f5174ov);
        }
        try {
            Field declaredField = C6619aqT.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f5174ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f5174ov) + C0147Bi.SEPARATOR + m25124b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f5174ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f5174ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f5174ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m25123a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m25120B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f5174ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f5174ov) + C0147Bi.SEPARATOR + m25124b(strArr[i], str);
        }
        return strArr2;
    }
}
