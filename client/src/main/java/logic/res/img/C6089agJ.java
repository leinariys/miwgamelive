package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.agJ  reason: case insensitive filesystem */
/* compiled from: a */
public class C6089agJ extends C2404ev {
    public static String eAS = "icon_boost_enabled";
    public static String eAT = "icon_combatmode_enabled";
    public static String eAU = "icon_cruise_enabled";
    public static String eAX = "icon_radar_enabled";
    public static String eGW = "ico_buf_attack";
    public static String eHN = "ico_buf_defense";
    public static String eHc = "ico_buf_cruisespeed";
    public static String eIn = "icon_max_";
    public static String eIo = "icon_normal_";
    public static String eIp = "icon_mini_";
    public static String fAA = "button_container_press";
    public static String fAB = "button_container_selected";
    public static String fAC = "button_hangar_enabled";
    public static String fAD = "button_hangar_over";
    public static String fAE = "button_hangar_press";
    public static String fAF = "button_hangar_selected";
    public static String fAG = "button_mining_enabled";
    public static String fAH = "button_mining_over";
    public static String fAI = "button_mining_press";
    public static String fAJ = "button_mining_selected";
    public static String fAK = "button_mission_disabled";
    public static String fAL = "button_mission_highlighted";
    public static String fAM = "button_reassign_enabled";
    public static String fAN = "button_reassign_over";
    public static String fAO = "button_reassign_press";
    public static String fAP = "button_reassign_selected";
    public static String fAQ = "button_transport_enabled";
    public static String fAR = "button_transport_over";
    public static String fAS = "button_transport_press";
    public static String fAT = "button_transport_selected";
    public static String fAU = "button_certificate_enabled";
    public static String fAV = "button_certificate_over";
    public static String fAW = "button_certificate_press";
    public static String fAX = "button_certificate_selected";
    public static String fAY = "button_mini_tutorial_enabled";
    public static String fAZ = "button_mini_tutorial_over";
    public static String fAa = "button_outpost_enabled";
    public static String fAb = "button_outpost_over";
    public static String fAc = "button_outpost_press";
    public static String fAd = "button_outpost_selected";
    public static String fAe = "button_ship_enabled";
    public static String fAf = "button_ship_over";
    public static String fAg = "button_ship_press";
    public static String fAh = "button_ship_selected";
    public static String fAi = "button_station_enabled";
    public static String fAj = "button_station_over";
    public static String fAk = "button_station_press";
    public static String fAl = "button_station_selected";
    public static String fAm = "button_storage_enabled";
    public static String fAn = "button_storage_over";
    public static String fAo = "button_storage_press";
    public static String fAp = "button_storage_selected";
    public static String fAq = "button_vault_enabled";
    public static String fAr = "button_vault_over";
    public static String fAs = "button_vault_press";
    public static String fAt = "button_vault_selected";
    public static String fAu = "button_bounty_enabled";
    public static String fAv = "button_bounty_over";
    public static String fAw = "button_bounty_press";
    public static String fAx = "button_bounty_selected";
    public static String fAy = "button_container_enabled";
    public static String fAz = "button_container_over";
    public static String fBA = "button_cannon_selected";
    public static String fBB = "button_launcher_enabled";
    public static String fBC = "badge_corp_1";
    public static String fBD = "button_trash_press";
    public static String fBE = "button_trash_selected";
    public static String fBF = "button_storage_disabled";
    public static String fBG = "button_launcher_over";
    public static String fBH = "button_launcher_press";
    public static String fBI = "button_launcher_selected";
    public static String fBJ = "button_special_enabled";
    public static String fBK = "badge_corp_2";
    public static String fBL = "button_storage_highlighted";
    public static String fBM = "button_special_over";
    public static String fBN = "button_special_press";
    public static String fBO = "button_special_selected";
    public static String fBP = "button_system_enabled";
    public static String fBQ = "button_system_over";
    public static String fBR = "button_system_press";
    public static String fBS = "button_system_selected";
    public static String fBT = "button_dock_container_enabled";
    public static String fBU = "button_dock_container_over";
    public static String fBV = "button_dock_container_press";
    public static String fBW = "button_dock_container_selected";
    public static String fBX = "button_dock_ship_enabled";
    public static String fBY = "button_dock_ship_over";
    public static String fBZ = "button_dock_ship_press";
    public static String fBa = "button_mini_tutorial_press";
    public static String fBb = "button_oni_disabled";
    public static String fBc = "button_oni_highlighted";
    public static String fBd = "button_outpost_disabled";
    public static String fBe = "button_outpost_highlighted";
    public static String fBf = "button_ship_disabled";
    public static String fBg = "button_ship_highlighted";
    public static String fBh = "button_station_disabled";
    public static String fBi = "button_station_highlighted";
    public static String fBj = "button_transfer_disabled";
    public static String fBk = "button_transfer_enabled";
    public static String fBl = "button_transfer_highlighted";
    public static String fBm = "button_transfer_over";
    public static String fBn = "button_transfer_press";
    public static String fBo = "button_transfer_selected";
    public static String fBp = "button_trash_disabled";
    public static String fBq = "icon_hull_fighter";
    public static String fBr = "button_cannon_enabled";
    public static String fBs = "amm_grp_balistic";
    public static String fBt = "icon_moremission";
    public static String fBu = "icon_nomission";
    public static String fBv = "button_trash_enabled";
    public static String fBw = "button_trash_highlighted";
    public static String fBx = "button_trash_over";
    public static String fBy = "button_cannon_over";
    public static String fBz = "button_cannon_press";
    public static String fCA = "can_grp_railgun";
    public static String fCB = "can_grp_scatter";
    public static String fCC = "can_non_class";
    public static String fCD = "cmb_grp_battleship";
    public static String fCE = "cmb_grp_bomber";
    public static String fCF = "cmb_grp_fighter";
    public static String fCG = "cmb_non_class";
    public static String fCH = "cmm_grp_basic";
    public static String fCI = "cmm_grp_broken";
    public static String fCJ = "cmm_grp_component";
    public static String fCK = "cmm_grp_exotic";
    public static String fCL = "cmm_grp_luxury";
    public static String fCM = "cmm_non_class";
    public static String fCN = "cop_grp_alloy";
    public static String fCO = "cop_grp_conductor";
    public static String fCP = "cop_grp_hardware";
    public static String fCQ = "cop_grp_ship";
    public static String fCR = "cop_grp_software";
    public static String fCS = "cop_grp_special";
    public static String fCT = "cop_grp_storage";
    public static String fCU = "cop_grp_structure";
    public static String fCV = "cop_grp_weapon";
    public static String fCW = "cop_non_class";
    public static String fCX = "icon_ammo_small";
    public static String fCY = "icon_cannon_small";
    public static String fCZ = "icon_utillity_metal";
    public static String fCa = "button_dock_ship_selected";
    public static String fCb = "amm_grp_countermeasure";
    public static String fCc = "amm_grp_gauss";
    public static String fCd = "amm_grp_mine";
    public static String fCe = "amm_grp_missile";
    public static String fCf = "amm_grp_rail";
    public static String fCg = "amm_grp_rocket";
    public static String fCh = "amm_grp_scatter";
    public static String fCi = "amm_grp_smallmissile";
    public static String fCj = "amm_grp_torpedo";
    public static String fCk = "amm_non_class";
    public static String fCl = "amp_non_class";
    public static String fCm = "blu_grp_ammo";
    public static String fCn = "blu_grp_commodity";
    public static String fCo = "blu_grp_component";
    public static String fCp = "blu_grp_equipment";
    public static String fCq = "blu_grp_ship";
    public static String fCr = "blu_grp_system";
    public static String fCs = "blu_non_class";
    public static String fCt = "can_grp_balistic";
    public static String fCu = "can_grp_blaster";
    public static String fCv = "can_grp_gauss";
    public static String fCw = "can_grp_magnetic";
    public static String fCx = "can_grp_neutron";
    public static String fCy = "can_grp_particle";
    public static String fCz = "can_grp_plasma";
    public static String fDA = "badge_corp_3";
    public static String fDB = "badge_corp_4";
    public static String fDC = "badge_corp_5";
    public static String[] fDD;
    public static String[] fDE;
    public static String[] fDF;
    public static String[] fDG;
    public static String[] fDH;
    public static String[] fDI;
    public static String[] fDJ;
    public static String[] fDK = {fBk, fBm, fBn, fBj, fBo};
    public static String[] fDL;
    public static String[] fDM;
    public static String[] fDN = {fAe, fAf, fAg, fBf, fAh};
    public static String[] fDO;
    public static String[] fDP = {fAm, fAn, fAo, fBF, fAp};
    public static String[] fDQ;
    public static String[] fDR;
    public static String[] fDS;
    public static String[] fDT;
    public static String[] fDU = {fAa, fAb, fAc, fBd, fAd};
    public static String[] fDW;
    public static String[] fDY;
    public static String[] fDZ;
    public static String fDa = "lau_grp_countermeasure";
    public static String fDb = "lau_grp_medium";
    public static String fDc = "lau_grp_mine";
    public static String fDd = "lau_grp_rocket";
    public static String fDe = "lau_grp_small";
    public static String fDf = "lau_grp_torpedo";
    public static String fDg = "lau_non_class";
    public static String fDh = "med_non_class";
    public static String fDi = "mit_non_class";
    public static String fDj = "npi_grp_battleship";
    public static String fDk = "npi_grp_drone";
    public static String fDl = "npi_grp_staticobject";
    public static String fDm = "npi_grp_station";
    public static String fDn = "raw_grp_exotic";
    public static String fDo = "raw_grp_gas";
    public static String fDp = "raw_grp_metal";
    public static String fDq = "raw_grp_nonmetal";
    public static String fDr = "raw_non_class";
    public static String fDs = "rec_mod_solarradiation";
    public static String fDt = "rec_mod_spacetimedistortion";
    public static String fDu = "spe_non_class";
    public static String fDv = "sys_grp_amplifier";
    public static String fDw = "sys_grp_module";
    public static String fDx = "sys_non_class";
    public static String fDy = "trd_grp_explorer";
    public static String fDz = "trd_grp_freighter";
    public static String[] fEa = {fBv, fBx, fBD, fBp, fBE};
    public static String[] fEb;
    public static String[] fEc;
    public static String[] fEd = {fAi, fAj, fAk, fBh, fAl};
    public static String[] fEe;
    public static String[] fEf;
    public static String[] fEg;
    public static String[] fEh;
    public static String[] fEi;
    public static String[] fEj;
    public static String[] fEk;
    public static String fEl = "icon_";
    public static String fyV = "Untitled3";
    public static String fyW = "itemslot_size_big";
    public static String fyX = "itemslot_size_medium";
    public static String fyY = "itemslot_size_small";
    public static String fyZ = "button_mini_tutorial_highlighted";
    public static String fzA = "button_corporation_press";
    public static String fzB = "button_corporation_selected";
    public static String fzC = "button_finances_enabled";
    public static String fzD = "button_finances_over";
    public static String fzE = "button_finances_press";
    public static String fzF = "button_finances_selected";
    public static String fzG = "button_help_enabled";
    public static String fzH = "button_help_over";
    public static String fzI = "button_help_press";
    public static String fzJ = "button_help_selected";
    public static String fzK = "button_map_enabled";
    public static String fzL = "button_map_over";
    public static String fzM = "button_map_press";
    public static String fzN = "button_map_selected";
    public static String fzO = "button_market_enabled";
    public static String fzP = "button_market_over";
    public static String fzQ = "button_market_press";
    public static String fzR = "button_market_selected";
    public static String fzS = "button_mission_enabled";
    public static String fzT = "button_mission_over";
    public static String fzU = "button_mission_press";
    public static String fzV = "button_mission_selected";
    public static String[] fDX = {fzS, fzT, fzU, fAK, fzV};
    public static String fzW = "button_oni_enabled";
    public static String fzX = "button_oni_over";
    public static String fzY = "button_oni_press";
    public static String fzZ = "button_oni_selected";
    public static String[] fDV = {fzW, fzX, fzY, fBb, fzZ};
    public static String fza = "button_agents_enabled";
    public static String fzb = "button_agents_over";
    public static String fzc = "button_agents_press";
    public static String fzd = "button_agents_selected";
    public static String fze = "button_associates_enabled";
    public static String fzf = "button_associates_over";
    public static String fzg = "button_associates_press";
    public static String fzh = "button_associates_selected";
    public static String fzi = "button_cloning_enabled";
    public static String fzj = "button_cloning_over";
    public static String fzk = "button_cloning_press";
    public static String fzl = "button_cloning_selected";
    public static String fzm = "button_consignment_enabled";
    public static String fzn = "button_consignment_over";
    public static String fzo = "button_consignment_press";
    public static String fzp = "button_consignment_selected";
    public static String fzq = "button_contracts_enabled";
    public static String fzr = "button_contracts_over";
    public static String fzs = "button_contracts_press";
    public static String fzt = "button_contracts_selected";
    public static String fzu = "button_corpcargo_enabled";
    public static String fzv = "button_corpcargo_over";
    public static String fzw = "button_corpcargo_press";
    public static String fzx = "button_corpcargo_selected";
    public static String fzy = "button_corporation_enabled";
    public static String fzz = "button_corporation_over";

    /* renamed from: je */
    public static String f4509je = "symbol_container";

    /* renamed from: jf */
    public static String f4510jf = "symbol_hangar";

    /* renamed from: jg */
    public static String f4511jg = "symbol_ship";

    /* renamed from: jh */
    public static String f4512jh = "symbo_storage";

    /* renamed from: ov */
    public static String f4513ov = "imageset_iconography";

    static {
        String[] strArr = new String[5];
        strArr[0] = fAU;
        strArr[1] = fAV;
        strArr[2] = fAW;
        strArr[4] = fAX;
        fDD = strArr;
        String[] strArr2 = new String[5];
        strArr2[0] = fAy;
        strArr2[1] = fAz;
        strArr2[2] = fAA;
        strArr2[4] = fAB;
        fDE = strArr2;
        String[] strArr3 = new String[5];
        strArr3[0] = fzm;
        strArr3[1] = fzn;
        strArr3[2] = fzo;
        strArr3[4] = fzp;
        fDF = strArr3;
        String[] strArr4 = new String[5];
        strArr4[0] = fAQ;
        strArr4[1] = fAR;
        strArr4[2] = fAS;
        strArr4[4] = fAT;
        fDG = strArr4;
        String[] strArr5 = new String[5];
        strArr5[0] = fzC;
        strArr5[1] = fzD;
        strArr5[2] = fzE;
        strArr5[4] = fzF;
        fDH = strArr5;
        String[] strArr6 = new String[5];
        strArr6[0] = fzG;
        strArr6[1] = fzH;
        strArr6[2] = fzI;
        strArr6[4] = fzJ;
        fDI = strArr6;
        String[] strArr7 = new String[5];
        strArr7[0] = fAG;
        strArr7[1] = fAH;
        strArr7[2] = fAI;
        strArr7[4] = fAJ;
        fDJ = strArr7;
        String[] strArr8 = new String[5];
        strArr8[0] = fzy;
        strArr8[1] = fzz;
        strArr8[2] = fzA;
        strArr8[4] = fzB;
        fDL = strArr8;
        String[] strArr9 = new String[5];
        strArr9[0] = fzK;
        strArr9[1] = fzL;
        strArr9[2] = fzM;
        strArr9[4] = fzN;
        fDM = strArr9;
        String[] strArr10 = new String[5];
        strArr10[0] = fAC;
        strArr10[1] = fAD;
        strArr10[2] = fAE;
        strArr10[4] = fAF;
        fDO = strArr10;
        String[] strArr11 = new String[5];
        strArr11[0] = fAq;
        strArr11[1] = fAr;
        strArr11[2] = fAs;
        strArr11[4] = fAt;
        fDQ = strArr11;
        String[] strArr12 = new String[5];
        strArr12[0] = fBr;
        strArr12[1] = fBy;
        strArr12[2] = fBz;
        strArr12[4] = fBA;
        fDR = strArr12;
        String[] strArr13 = new String[5];
        strArr13[0] = fza;
        strArr13[1] = fzb;
        strArr13[2] = fzc;
        strArr13[4] = fzd;
        fDS = strArr13;
        String[] strArr14 = new String[5];
        strArr14[0] = fAM;
        strArr14[1] = fAN;
        strArr14[2] = fAO;
        strArr14[4] = fAP;
        fDT = strArr14;
        String[] strArr15 = new String[5];
        strArr15[0] = fzq;
        strArr15[1] = fzr;
        strArr15[2] = fzs;
        strArr15[4] = fzt;
        fDW = strArr15;
        String[] strArr16 = new String[5];
        strArr16[0] = fzu;
        strArr16[1] = fzv;
        strArr16[2] = fzw;
        strArr16[4] = fzx;
        fDY = strArr16;
        String[] strArr17 = new String[5];
        strArr17[0] = fBP;
        strArr17[1] = fBQ;
        strArr17[2] = fBR;
        strArr17[4] = fBS;
        fDZ = strArr17;
        String[] strArr18 = new String[5];
        strArr18[0] = fzi;
        strArr18[1] = fzj;
        strArr18[2] = fzk;
        strArr18[4] = fzl;
        fEb = strArr18;
        String[] strArr19 = new String[5];
        strArr19[0] = fzO;
        strArr19[1] = fzP;
        strArr19[2] = fzQ;
        strArr19[4] = fzR;
        fEc = strArr19;
        String[] strArr20 = new String[5];
        strArr20[0] = fAu;
        strArr20[1] = fAv;
        strArr20[2] = fAw;
        strArr20[4] = fAx;
        fEe = strArr20;
        String[] strArr21 = new String[5];
        strArr21[0] = fBJ;
        strArr21[1] = fBM;
        strArr21[2] = fBN;
        strArr21[4] = fBO;
        fEf = strArr21;
        String[] strArr22 = new String[5];
        strArr22[0] = fBB;
        strArr22[1] = fBG;
        strArr22[2] = fBH;
        strArr22[4] = fBI;
        fEg = strArr22;
        String[] strArr23 = new String[5];
        strArr23[0] = fze;
        strArr23[1] = fzf;
        strArr23[2] = fzg;
        strArr23[4] = fzh;
        fEh = strArr23;
        String[] strArr24 = new String[5];
        strArr24[0] = fBT;
        strArr24[1] = fBU;
        strArr24[2] = fBV;
        strArr24[4] = fBW;
        fEi = strArr24;
        String[] strArr25 = new String[4];
        strArr25[0] = fAY;
        strArr25[1] = fAZ;
        strArr25[2] = fBa;
        fEj = strArr25;
        String[] strArr26 = new String[5];
        strArr26[0] = fBX;
        strArr26[1] = fBY;
        strArr26[2] = fBZ;
        strArr26[4] = fCa;
        fEk = strArr26;
    }

    /* renamed from: a */
    public static String m21973a(String str, String str2, String str3) {
        if (m21971B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m21971B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f4513ov);
    }

    /* renamed from: b */
    public static String m21975b(String str, String str2) {
        if (m21971B(str)) {
            return str;
        }
        if (m21971B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f4513ov);
    }

    /* renamed from: B */
    public static boolean m21971B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C6089agJ.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m21976b(String str, String str2, String str3) {
        return String.valueOf(f4513ov) + C0147Bi.SEPARATOR + m21973a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m21977c(String str, String str2) {
        return String.valueOf(f4513ov) + C0147Bi.SEPARATOR + m21975b(str, str2);
    }

    /* renamed from: a */
    public static String m21972a(String str, int i, String str2) {
        if (!m21971B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f4513ov);
        }
        try {
            Field declaredField = C6089agJ.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f4513ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f4513ov) + C0147Bi.SEPARATOR + m21975b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f4513ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f4513ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f4513ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m21974a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m21971B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f4513ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f4513ov) + C0147Bi.SEPARATOR + m21975b(strArr[i], str);
        }
        return strArr2;
    }
}
