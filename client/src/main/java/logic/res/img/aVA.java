package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.aVA */
/* compiled from: a */
public class aVA extends C2404ev {
    public static String gHO = "frame_dim_borders";
    public static String gHo = "frame_dim_center";
    public static String jaA = "list_cell_unselected_borders";
    public static String jaB = "cannon_display_center";
    public static String jaC = "cannon_display_type_ammo";
    public static String jaD = "ico_placeholder_medium";
    public static String jaE = "ico_frame_ammo_left";
    public static String jaF = "ico_number_left";
    public static String jaG = "ico_frame_ammo_center";
    public static String jaH = "ico_frame_ammo_right";
    public static String jaI = "ico_number_center";
    public static String jaJ = "ico_number_right";
    public static String jaK = "ico_placeholder_big";
    public static String jaL = "cannon_display_expander_left";
    public static String jaM = "cannon_display_expander_right";
    public static String jaN = "cannon_display_left";
    public static String jaO = "ico_placeholder_small";
    public static String jaP = "ico_placeholder_type";
    public static String jal = "cannon_display";
    public static String jam = "launcher_display";
    public static String jan = "cannon_display_right";
    public static String jao = "list_handle_left";
    public static String jap = "list_handle_right";
    public static String jaq = "list_cell_lower_left_selected";
    public static String jar = "list_cell_lower_right_selected";
    public static String jas = "list_cell_upper_left_selected";
    public static String jat = "list_cell_upper_right_selected";
    public static String jau = "list_cell_bottom_selected";
    public static String jav = "list_cell_left_selected";
    public static String jaw = "list_cell_right_selected";
    public static String jax = "list_cell_top_selected";
    public static String jay = "list_cell_center_selected";
    public static String jaz = "list_cell_unselected_center";

    /* renamed from: ov */
    public static String f3890ov = "imageset_weaponselector";

    /* renamed from: a */
    public static String m18780a(String str, String str2, String str3) {
        if (m18778B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m18778B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f3890ov);
    }

    /* renamed from: b */
    public static String m18782b(String str, String str2) {
        if (m18778B(str)) {
            return str;
        }
        if (m18778B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f3890ov);
    }

    /* renamed from: B */
    public static boolean m18778B(String str) {
        if (str == null) {
            return false;
        }
        try {
            aVA.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m18783b(String str, String str2, String str3) {
        return String.valueOf(f3890ov) + C0147Bi.SEPARATOR + m18780a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m18784c(String str, String str2) {
        return String.valueOf(f3890ov) + C0147Bi.SEPARATOR + m18782b(str, str2);
    }

    /* renamed from: a */
    public static String m18779a(String str, int i, String str2) {
        if (!m18778B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f3890ov);
        }
        try {
            Field declaredField = C6089agJ.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f3890ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f3890ov) + C0147Bi.SEPARATOR + m18782b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f3890ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f3890ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f3890ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m18781a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m18778B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f3890ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f3890ov) + C0147Bi.SEPARATOR + m18782b(strArr[i], str);
        }
        return strArr2;
    }
}
