package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.adr  reason: case insensitive filesystem */
/* compiled from: a */
public class C5967adr extends C2404ev {
    public static String fhA = "button_confirm_enabled";
    public static String fhB = "certificate_free_right_enabled";
    public static String fhC = "certificate_free_right_selected";
    public static String fhD = "certificate_installed_right_enabled";
    public static String fhE = "certificate_installed_right_over";
    public static String fhF = "certificate_installed_right_selected";
    public static String fhG = "certificate_owned_right_selected";
    public static String fhH = "certificate_free_left_enabled";
    public static String fhI = "certificate_free_left_selected";
    public static String fhJ = "certificate_memoryvalue";
    public static String fhK = "oni_left_arrow";
    public static String fhL = "oni_right_arrow";
    public static String fhM = "certificate_bomber";
    public static String fhN = "certificate_explorer";
    public static String fhO = "memory_cell_overclock";
    public static String fhP = "memory_fill_normal";
    public static String fhQ = "certificate_free_center_enabled";
    public static String fhR = "certificate_free_center_selected";
    public static String fhS = "certificate_installed_center_enabled";
    public static String fhT = "certificate_fighter";
    public static String fhU = "certificate_freighter";
    public static String fhV = "certificate_systems";
    public static String fhW = "memory_fill_overclock";
    public static String fhX = "certificate_installed_center_over";
    public static String fhY = "certificate_installed_center_selected";
    public static String fhZ = "certificate_owned_center_enabled";
    public static String fhw = "oni_header_top";
    public static String fhx = "memory_cell_frame";
    public static String fhy = "oni_frame";
    public static String fhz = "button_confirm_disabled";
    public static String[] fiA;
    public static String[] fiB;
    public static String[] fiC;
    public static String[] fiD;
    public static String[] fiE;
    public static String[] fiG;
    public static String[] fiH;
    public static String[] fiI;
    public static String[] fiJ;
    public static String fia = "certificate_owned_center_over";
    public static String fib = "certificate_owned_center_selected";
    public static String fic = "button_confirm_over";
    public static String fid = "button_confirm_press";
    public static String[] fiF = {fhA, fic, fid, fhz};
    public static String fie = "button_install_enabled";
    public static String fif = "button_install_over";
    public static String fig = "button_install_press";
    public static String fih = "button_remove_disabled";
    public static String fii = "button_remove_enabled";
    public static String fij = "button_remove_over";
    public static String fik = "button_remove_press";
    public static String fil = "certificate_installed_left_enabled";
    public static String fim = "certificate_installed_left_over";
    public static String fin = "certificate_installed_left_selected";
    public static String fio = "certificate_owned_left_enabled";
    public static String fip = "certificate_owned_left_over";
    public static String fiq = "certificate_owned_left_selected";
    public static String fir = "certificate_weapons";
    public static String fis = "button_return_disabled";
    public static String fit = "button_return_enabled";
    public static String fiu = "button_return_over";
    public static String fiv = "oni_header_bottom_left";
    public static String fiw = "oni_header_bottom_right";
    public static String fix = "oni_table_certificate";
    public static String fiy = "button_return_press";
    public static String[] fiK = {fit, fiu, fiy, fis};
    public static String[] fiz = {fii, fij, fik, fih};

    /* renamed from: ov */
    public static String f4340ov = "imageset_progression";

    static {
        String[] strArr = new String[5];
        strArr[0] = fhB;
        strArr[4] = fhC;
        fiA = strArr;
        String[] strArr2 = new String[5];
        strArr2[0] = fhD;
        strArr2[1] = fhE;
        strArr2[4] = fhF;
        fiB = strArr2;
        String[] strArr3 = new String[5];
        strArr3[0] = fhH;
        strArr3[4] = fhI;
        fiC = strArr3;
        String[] strArr4 = new String[5];
        strArr4[0] = fhZ;
        strArr4[1] = fia;
        strArr4[4] = fib;
        fiD = strArr4;
        String[] strArr5 = new String[4];
        strArr5[0] = fie;
        strArr5[1] = fif;
        strArr5[2] = fig;
        fiE = strArr5;
        String[] strArr6 = new String[5];
        strArr6[0] = fhQ;
        strArr6[4] = fhR;
        fiG = strArr6;
        String[] strArr7 = new String[5];
        strArr7[0] = fio;
        strArr7[1] = fip;
        strArr7[4] = fiq;
        fiH = strArr7;
        String[] strArr8 = new String[5];
        strArr8[0] = fhS;
        strArr8[1] = fhX;
        strArr8[4] = fhY;
        fiI = strArr8;
        String[] strArr9 = new String[5];
        strArr9[0] = fil;
        strArr9[1] = fim;
        strArr9[4] = fin;
        fiJ = strArr9;
    }

    /* renamed from: a */
    public static String m20932a(String str, String str2, String str3) {
        if (m20930B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m20930B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f4340ov);
    }

    /* renamed from: b */
    public static String m20934b(String str, String str2) {
        if (m20930B(str)) {
            return str;
        }
        if (m20930B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f4340ov);
    }

    /* renamed from: B */
    public static boolean m20930B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C5967adr.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m20935b(String str, String str2, String str3) {
        return String.valueOf(f4340ov) + C0147Bi.SEPARATOR + m20932a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m20936c(String str, String str2) {
        return String.valueOf(f4340ov) + C0147Bi.SEPARATOR + m20934b(str, str2);
    }

    /* renamed from: a */
    public static String m20931a(String str, int i, String str2) {
        if (!m20930B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f4340ov);
        }
        try {
            Field declaredField = C5967adr.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f4340ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f4340ov) + C0147Bi.SEPARATOR + m20934b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f4340ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f4340ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f4340ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m20933a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m20930B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f4340ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f4340ov) + C0147Bi.SEPARATOR + m20934b(strArr[i], str);
        }
        return strArr2;
    }
}
