package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.zm */
/* compiled from: a */
public class C4122zm extends C2404ev {
    public static String bYX = "button2_down_enable";
    public static String bYY = "button2_down_over";
    public static String bYZ = "button2_down_press";
    public static String bZa = "button2_up_enabled";
    public static String bZb = "button2_up_over";
    public static String bZc = "button2_up_press";
    public static String bZd = "icon_corp_invite_acepted";
    public static String bZe = "icon_corp_invite_declined";
    public static String bZf = "icon_corp_invite_waiting";
    public static String[] bZg;
    public static String[] bZh;

    /* renamed from: ov */
    public static String f9739ov = "imageset_corp";

    static {
        String[] strArr = new String[4];
        strArr[0] = bZa;
        strArr[1] = bZb;
        strArr[2] = bZc;
        bZg = strArr;
        String[] strArr2 = new String[4];
        strArr2[1] = bYY;
        strArr2[2] = bYZ;
        bZh = strArr2;
    }

    /* renamed from: a */
    public static String m41843a(String str, String str2, String str3) {
        if (m41841B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m41841B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f9739ov);
    }

    /* renamed from: b */
    public static String m41845b(String str, String str2) {
        if (m41841B(str)) {
            return str;
        }
        if (m41841B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f9739ov);
    }

    /* renamed from: B */
    public static boolean m41841B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C4122zm.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m41846b(String str, String str2, String str3) {
        return String.valueOf(f9739ov) + C0147Bi.SEPARATOR + m41843a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m41847c(String str, String str2) {
        return String.valueOf(f9739ov) + C0147Bi.SEPARATOR + m41845b(str, str2);
    }

    /* renamed from: a */
    public static String m41842a(String str, int i, String str2) {
        if (!m41841B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f9739ov);
        }
        try {
            Field declaredField = C4122zm.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f9739ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f9739ov) + C0147Bi.SEPARATOR + m41845b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f9739ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f9739ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f9739ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m41844a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m41841B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f9739ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f9739ov) + C0147Bi.SEPARATOR + m41845b(strArr[i], str);
        }
        return strArr2;
    }
}
