package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.tg */
/* compiled from: a */
public class C3681tg extends C2404ev {
    public static String bkn = "infobar_frame_center";
    public static String bko = "lowerbar_frame_center";
    public static String bkp = "infobar_frame_lower_left";
    public static String bkq = "infobar_frame_lower_right";
    public static String bkr = "lowerbar_frame_upper_left";
    public static String bks = "lowerbar_frame_upper_right";
    public static String bkt = "infobar_frame_bottom";
    public static String bku = "lowerbar_frame_top";
    public static String bkv = "box_frame_borders";
    public static String bkw = "box_frame_center";
    public static String bkx = "infobar_frame_borders";
    public static String bky = "lowerbar_frame_borders";

    /* renamed from: ov */
    public static String f9263ov = "imageset_new_infobar";

    /* renamed from: a */
    public static String m39703a(String str, String str2, String str3) {
        if (m39701B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m39701B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f9263ov);
    }

    /* renamed from: b */
    public static String m39705b(String str, String str2) {
        if (m39701B(str)) {
            return str;
        }
        if (m39701B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f9263ov);
    }

    /* renamed from: B */
    public static boolean m39701B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C3681tg.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m39706b(String str, String str2, String str3) {
        return String.valueOf(f9263ov) + C0147Bi.SEPARATOR + m39703a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m39707c(String str, String str2) {
        return String.valueOf(f9263ov) + C0147Bi.SEPARATOR + m39705b(str, str2);
    }

    /* renamed from: a */
    public static String m39702a(String str, int i, String str2) {
        if (!m39701B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f9263ov);
        }
        try {
            Field declaredField = C3681tg.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f9263ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f9263ov) + C0147Bi.SEPARATOR + m39705b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f9263ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f9263ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f9263ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m39704a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m39701B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f9263ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f9263ov) + C0147Bi.SEPARATOR + m39705b(strArr[i], str);
        }
        return strArr2;
    }
}
