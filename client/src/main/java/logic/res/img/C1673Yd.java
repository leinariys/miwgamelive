package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.Yd */
/* compiled from: a */
public class C1673Yd extends C2404ev {

    /* renamed from: Aw */
    public static String f2180Aw = "tela02";

    /* renamed from: ov */
    public static String f2181ov = "imageset_loading_bg2";

    /* renamed from: a */
    public static String m11881a(String str, String str2, String str3) {
        if (m11879B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m11879B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f2181ov);
    }

    /* renamed from: b */
    public static String m11883b(String str, String str2) {
        if (m11879B(str)) {
            return str;
        }
        if (m11879B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f2181ov);
    }

    /* renamed from: B */
    public static boolean m11879B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C1673Yd.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m11884b(String str, String str2, String str3) {
        return String.valueOf(f2181ov) + C0147Bi.SEPARATOR + m11881a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m11885c(String str, String str2) {
        return String.valueOf(f2181ov) + C0147Bi.SEPARATOR + m11883b(str, str2);
    }

    /* renamed from: a */
    public static String m11880a(String str, int i, String str2) {
        if (!m11879B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f2181ov);
        }
        try {
            Field declaredField = C1673Yd.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f2181ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f2181ov) + C0147Bi.SEPARATOR + m11883b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f2181ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f2181ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f2181ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m11882a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m11879B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f2181ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f2181ov) + C0147Bi.SEPARATOR + m11883b(strArr[i], str);
        }
        return strArr2;
    }
}
