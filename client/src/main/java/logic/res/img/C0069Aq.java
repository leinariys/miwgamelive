package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.Aq */
/* compiled from: a */
public class C0069Aq extends C2404ev {
    public static String cfu = "editbox_frame_borders_enabled";
    public static String cfv = "editbox_frame_borders_selected";
    public static String cfw = "editbox_frame_center_enabled";
    public static String cfx = "editbox_frame_center_selected";
    public static String[] cfy;
    public static String[] cfz;

    /* renamed from: ov */
    public static String f97ov = "imageset_new_editbox";

    static {
        String[] strArr = new String[5];
        strArr[0] = cfw;
        strArr[4] = cfx;
        cfy = strArr;
        String[] strArr2 = new String[5];
        strArr2[0] = cfu;
        strArr2[4] = cfv;
        cfz = strArr2;
    }

    /* renamed from: a */
    public static String m546a(String str, String str2, String str3) {
        if (m544B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m544B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f97ov);
    }

    /* renamed from: b */
    public static String m548b(String str, String str2) {
        if (m544B(str)) {
            return str;
        }
        if (m544B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f97ov);
    }

    /* renamed from: B */
    public static boolean m544B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C0069Aq.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m549b(String str, String str2, String str3) {
        return String.valueOf(f97ov) + C0147Bi.SEPARATOR + m546a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m550c(String str, String str2) {
        return String.valueOf(f97ov) + C0147Bi.SEPARATOR + m548b(str, str2);
    }

    /* renamed from: a */
    public static String m545a(String str, int i, String str2) {
        if (!m544B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f97ov);
        }
        try {
            Field declaredField = C0069Aq.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f97ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f97ov) + C0147Bi.SEPARATOR + m548b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f97ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f97ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f97ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m547a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m544B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f97ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f97ov) + C0147Bi.SEPARATOR + m548b(strArr[i], str);
        }
        return strArr2;
    }
}
