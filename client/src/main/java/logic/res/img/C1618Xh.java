package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.Xh */
/* compiled from: a */
public class C1618Xh extends C2404ev {
    public static String eAS = "icon_boost_enabled";
    public static String eAT = "icon_combatmode_enabled";
    public static String eAU = "icon_cruise_enabled";
    public static String eAV = "icon_normal_empty";
    public static String eAW = "icon_normal_neutral";
    public static String eAX = "icon_radar_enabled";
    public static String eAY = "icon_vault_enabled";
    public static String eAZ = "ico_amm_ballistic";
    public static String eBA = "ico_amm_scatter";
    public static String eBB = "ico_amp_afterburnerbig";
    public static String eBC = "ico_amp_afterburnermedium";
    public static String eBD = "ico_amp_afterburnersmall";
    public static String eBE = "ico_amp_armorbig";
    public static String eBF = "ico_amp_armormedium";
    public static String eBG = "ico_amp_armorsmall";
    public static String eBH = "ico_amp_aspisbig";
    public static String eBI = "ico_amp_aspismall";
    public static String eBJ = "ico_amp_aspismedium";
    public static String eBK = "ico_amp_blockadebig";
    public static String eBL = "ico_amp_blockademedium";
    public static String eBM = "ico_amp_blockadesmall";
    public static String eBN = "ico_amp_boosterbig";
    public static String eBO = "ico_amp_boostermedium";
    public static String eBP = "ico_amp_boostersmall";
    public static String eBQ = "ico_amp_dolphinbig";
    public static String eBR = "ico_amp_dolphinmedium";
    public static String eBS = "ico_amp_dolphinsmall";
    public static String eBT = "ico_amp_expanderbig";
    public static String eBU = "ico_amp_expandermedium";
    public static String eBV = "ico_amp_expandersmall";
    public static String eBW = "ico_amp_shieldbig";
    public static String eBX = "ico_amp_shieldmedium";
    public static String eBY = "ico_amp_shieldsmall";
    public static String eBZ = "ico_amp_structurebig";
    public static String eBa = "ico_amm_countermeasure";
    public static String eBb = "ico_amm_gauss";
    public static String eBc = "ico_amm_mine001";
    public static String eBd = "ico_amm_mine002";
    public static String eBe = "ico_amm_minebig01";
    public static String eBf = "ico_amm_minebig02";
    public static String eBg = "ico_amm_minemedium01";
    public static String eBh = "ico_amm_minemedium02";
    public static String eBi = "ico_amm_minesmall01";
    public static String eBj = "ico_amm_minesmall02";
    public static String eBk = "ico_amm_missile001";
    public static String eBl = "ico_amm_missile002";
    public static String eBm = "ico_amm_missile003";
    public static String eBn = "ico_amm_missile004";
    public static String eBo = "ico_amm_missile005";
    public static String eBp = "ico_amm_missile006";
    public static String eBq = "ico_amm_missile007";
    public static String eBr = "ico_amm_missile008";
    public static String eBs = "ico_amm_missilebig";
    public static String eBt = "ico_amm_missilemedium";
    public static String eBu = "ico_amm_missilesmall";
    public static String eBv = "ico_amm_rail";
    public static String eBw = "ico_amm_rocket001";
    public static String eBx = "ico_amm_rocket002";
    public static String eBy = "ico_amm_rocket003";
    public static String eBz = "ico_amm_rocket004";
    public static String eCA = "ico_brk_armor";
    public static String eCB = "ico_brk_balistic";
    public static String eCC = "ico_brk_blaster";
    public static String eCD = "ico_brk_booster";
    public static String eCE = "ico_brk_cpu";
    public static String eCF = "ico_brk_doubleblaster";
    public static String eCG = "ico_brk_expander";
    public static String eCH = "ico_brk_gauss";
    public static String eCI = "ico_brk_impulsorg";
    public static String eCJ = "ico_brk_magnetic";
    public static String eCK = "ico_brk_neutron";
    public static String eCL = "ico_brk_particle";
    public static String eCM = "ico_brk_rail";
    public static String eCN = "ico_brk_rastreador";
    public static String eCO = "ico_brk_scatter";
    public static String eCP = "ico_brk_shield";
    public static String eCQ = "ico_brk_structure";
    public static String eCR = "ico_brk_tokamak";
    public static String eCS = "ico_bts_mobydick";
    public static String eCT = "ico_bus_grouper";
    public static String eCU = "ico_cen_basic";
    public static String eCV = "ico_cen_blaster";
    public static String eCW = "ico_cen_blasterbig";
    public static String eCX = "ico_cen_doubleblaster";
    public static String eCY = "ico_cen_doublemagnetic";
    public static String eCZ = "ico_cen_doubleneutron";
    public static String eCa = "ico_amp_structuremedium";
    public static String eCb = "ico_amp_structuresmall";
    public static String eCc = "ico_amp_titanbig";
    public static String eCd = "ico_amp_titanmedium";
    public static String eCe = "ico_amp_titansmall";
    public static String eCf = "ico_amt_arsenio";
    public static String eCg = "ico_amt_boro";
    public static String eCh = "ico_amt_germanio";
    public static String eCi = "ico_bas_bebidasalcoolicas";
    public static String eCj = "ico_bas_cereais";
    public static String eCk = "ico_bas_comidadesidratada";
    public static String eCl = "ico_bom_darkwhale";
    public static String eCm = "ico_bom_dervish";
    public static String eCn = "ico_bom_ifrit";
    public static String eCo = "ico_bom_kingcrab";
    public static String eCp = "ico_bom_lobster";
    public static String eCq = "ico_bom_narwhal";
    public static String eCr = "ico_bom_scorpio";
    public static String eCs = "ico_bom_stingray";
    public static String eCt = "ico_bom_sudan";
    public static String eCu = "ico_bom_zilant";
    public static String eCv = "ico_box_ballistic";
    public static String eCw = "ico_box_gauss";
    public static String eCx = "ico_box_rail";
    public static String eCy = "ico_box_scatter";
    public static String eCz = "ico_brk_afterburner";
    public static String eDA = "ico_cmm_15";
    public static String eDB = "ico_cop_001";
    public static String eDC = "ico_cop_002";
    public static String eDD = "ico_cop_003";
    public static String eDE = "ico_cop_004";
    public static String eDF = "ico_cop_005";
    public static String eDG = "ico_cop_006";
    public static String eDH = "ico_cop_007";
    public static String eDI = "ico_cop_008";
    public static String eDJ = "ico_cop_009";
    public static String eDK = "ico_cop_010";
    public static String eDL = "ico_cop_011";
    public static String eDM = "ico_cop_012";
    public static String eDN = "ico_cop_013";
    public static String eDO = "ico_cop_014";
    public static String eDP = "ico_cop_015";
    public static String eDQ = "ico_cop_016";
    public static String eDR = "ico_cop_017";
    public static String eDS = "ico_cop_018";
    public static String eDT = "ico_cop_019";
    public static String eDU = "ico_cop_020";
    public static String eDV = "ico_cop_021";
    public static String eDW = "ico_cop_022";
    public static String eDX = "ico_cop_023";
    public static String eDY = "ico_cop_024";
    public static String eDZ = "ico_cop_025";
    public static String eDa = "ico_cen_doubleparticle";
    public static String eDb = "ico_cen_doubleprotoblaster";
    public static String eDc = "ico_cen_magnetic";
    public static String eDd = "ico_cen_magneticprototype";
    public static String eDe = "ico_cen_neutron";
    public static String eDf = "ico_cen_neutronprototype";
    public static String eDg = "ico_cen_particle";
    public static String eDh = "ico_cen_particleprototype";
    public static String eDi = "ico_cen_plasmabig";
    public static String eDj = "ico_cen_plasmamedium";
    public static String eDk = "ico_cen_plasmaprototype";
    public static String eDl = "ico_cen_protoblaster";
    public static String eDm = "ico_cmm_001";
    public static String eDn = "ico_cmm_002";
    public static String eDo = "ico_cmm_003";
    public static String eDp = "ico_cmm_004";
    public static String eDq = "ico_cmm_005";
    public static String eDr = "ico_cmm_006";
    public static String eDs = "ico_cmm_007";
    public static String eDt = "ico_cmm_008";
    public static String eDu = "ico_cmm_009";
    public static String eDv = "ico_cmm_010";
    public static String eDw = "ico_cmm_011";
    public static String eDx = "ico_cmm_012";
    public static String eDy = "ico_cmm_013";
    public static String eDz = "ico_cmm_014";
    public static String eEA = "ico_cop_052";
    public static String eEB = "ico_cop_053";
    public static String eEC = "ico_cop_054";
    public static String eED = "ico_cop_055";
    public static String eEE = "ico_cop_056";
    public static String eEF = "ico_cop_057";
    public static String eEG = "ico_cop_058";
    public static String eEH = "ico_cop_059";
    public static String eEI = "ico_cop_060";
    public static String eEJ = "ico_cop_061";
    public static String eEK = "ico_cop_062";
    public static String eEL = "ico_cop_063";
    public static String eEM = "ico_cop_064";
    public static String eEN = "ico_cop_65";
    public static String eEO = "ico_cop_66";
    public static String eEP = "ico_fra_medium_big";
    public static String eEQ = "ico_fra_medium_enabled";
    public static String eER = "ico_fra_medium_medium";
    public static String eES = "ico_fra_medium_over";
    public static String eET = "ico_fra_medium_red";
    public static String eEU = "ico_cop_67";
    public static String eEV = "ico_cop_68";
    public static String eEW = "ico_cpr_ballisticbig";
    public static String eEX = "ico_cpr_ballisticmedium";
    public static String eEY = "ico_cpr_ballisticprototype";
    public static String eEZ = "ico_cpr_doublegauss";
    public static String eEa = "ico_cop_026";
    public static String eEb = "ico_cop_027";
    public static String eEc = "ico_cop_028";
    public static String eEd = "ico_cop_029";
    public static String eEe = "ico_cop_030";
    public static String eEf = "ico_cop_031";
    public static String eEg = "ico_cop_032";
    public static String eEh = "ico_cop_033";
    public static String eEi = "ico_cop_034";
    public static String eEj = "ico_cop_035";
    public static String eEk = "ico_cop_036";
    public static String eEl = "ico_cop_037";
    public static String eEm = "ico_cop_038";
    public static String eEn = "ico_cop_039";
    public static String eEo = "ico_cop_040";
    public static String eEp = "ico_cop_041";
    public static String eEq = "ico_cop_042";
    public static String eEr = "ico_cop_043";
    public static String eEs = "ico_cop_044";
    public static String eEt = "ico_cop_045";
    public static String eEu = "ico_cop_046";
    public static String eEv = "ico_cop_047";
    public static String eEw = "ico_cop_048";
    public static String eEx = "ico_cop_049";
    public static String eEy = "ico_cop_050";
    public static String eEz = "ico_cop_051";
    public static String eFA = "ico_fig_kalifa";
    public static String eFB = "ico_fig_lionfish";
    public static String eFC = "ico_fig_marid";
    public static String eFD = "ico_fig_naja";
    public static String eFE = "ico_fig_piranha";
    public static String eFF = "ico_fre_alborak";
    public static String eFG = "ico_fre_chelonia";
    public static String eFH = "ico_fre_kalifa";
    public static String eFI = "ico_fre_khalifa";
    public static String eFJ = "ico_fre_morey";
    public static String eFK = "ico_fre_narwhal";
    public static String eFL = "ico_fre_typhon";
    public static String eFM = "ico_fre_typhoon";
    public static String eFN = "ico_fre_vizir";
    public static String eFO = "ico_gas_argonio";
    public static String eFP = "ico_gas_criptonio";
    public static String eFQ = "ico_gas_helio";
    public static String eFR = "ico_gas_hidrogenio";
    public static String eFS = "ico_lau_big";
    public static String eFT = "ico_lau_bigmine";
    public static String eFU = "ico_lau_countermeasure";
    public static String eFV = "ico_lau_launcherbigproto";
    public static String eFW = "ico_lau_launchermediumproto";
    public static String eFX = "ico_lau_launchersmallproto";
    public static String eFY = "ico_lau_medium";
    public static String eFZ = "ico_lau_mediummine";
    public static String eFa = "ico_cpr_doublerail";
    public static String eFb = "ico_cpr_doublescatter";
    public static String eFc = "ico_cpr_gauss";
    public static String eFd = "ico_cpr_gaussprototype";
    public static String eFe = "ico_cpr_rail";
    public static String eFf = "ico_cpr_railrprototype";
    public static String eFg = "ico_cpr_scatter";
    public static String eFh = "ico_cpr_scatterprototype";
    public static String eFi = "ico_dro_bee";
    public static String eFj = "ico_exo_paleocolecionaveis";
    public static String eFk = "ico_exp_longfin";
    public static String eFl = "ico_exp_qaswa";
    public static String eFm = "ico_exp_sherah";
    public static String eFn = "ico_exp_simbad";
    public static String eFo = "ico_exp_sunfish";
    public static String eFp = "ico_exp_zahhak";
    public static String eFq = "ico_exp_zaratan";
    public static String eFr = "ico_exp_zaruk";
    public static String eFs = "ico_ext_neutronio";
    public static String eFt = "ico_fig_barracuda";
    public static String eFu = "ico_fig_bullfrog";
    public static String eFv = "ico_fig_bullfrogii";
    public static String eFw = "ico_fig_bullfrogx";
    public static String eFx = "ico_fig_dagger";
    public static String eFy = "ico_fig_djinn";
    public static String eFz = "ico_fig_ghoul";
    public static String eGA = "ico_pro_ammo";
    public static String eGB = "ico_pro_component";
    public static String eGC = "ico_pro_equipment";
    public static String eGD = "ico_pro_ship";
    public static String eGE = "ico_raw_001";
    public static String eGF = "ico_raw_002";
    public static String eGG = "ico_raw_003";
    public static String eGH = "ico_raw_004";
    public static String eGI = "ico_raw_005";
    public static String eGJ = "ico_raw_006";
    public static String eGK = "ico_raw_007";
    public static String eGL = "ico_raw_008";
    public static String eGM = "ico_raw_009";
    public static String eGN = "ico_raw_010";
    public static String eGO = "ico_raw_011";
    public static String eGP = "ico_raw_012";
    public static String eGQ = "ico_non_npc";
    public static String eGR = "ico_fra_medium_small";
    public static String eGS = "ico_buf_achilles";
    public static String eGT = "ico_buf_anchor";
    public static String eGU = "ico_buf_armor";
    public static String eGV = "ico_buf_aspis";
    public static String eGW = "ico_buf_attack";
    public static String eGX = "ico_buf_blank";
    public static String eGY = "ico_buf_blockaderunner";
    public static String eGZ = "ico_buf_booster";
    public static String eGa = "ico_lau_rocketbig";
    public static String eGb = "ico_lau_rocketmedium";
    public static String eGc = "ico_lau_rocketproto";
    public static String eGd = "ico_lau_rocketsmall";
    public static String eGe = "ico_lau_small";
    public static String eGf = "ico_lau_smallmine";
    public static String eGg = "ico_lau_torpedo";
    public static String eGh = "ico_lau_torpedoproto";
    public static String eGi = "ico_lux_artesanatobelter";
    public static String eGj = "ico_lux_holodramas";
    public static String eGk = "ico_lux_veiculospessoais";
    public static String eGl = "ico_med_001";
    public static String eGm = "ico_med_002";
    public static String eGn = "ico_med_003";
    public static String eGo = "ico_med_004";
    public static String eGp = "ico_med_005";
    public static String eGq = "ico_med_006";
    public static String eGr = "ico_mis_pizza";
    public static String eGs = "ico_mis_timemachine";
    public static String eGt = "ico_mod_achilles";
    public static String eGu = "ico_mod_anchor";
    public static String eGv = "ico_mod_chargerbig";
    public static String eGw = "ico_mod_chargermedium";
    public static String eGx = "ico_mod_chargersmall";
    public static String eGy = "ico_mod_disruptor";
    public static String eGz = "ico_non_empty";
    public static String eHA = "ico_spe_mission004";
    public static String eHB = "ico_spe_mission005";
    public static String eHC = "ico_spe_mission006";
    public static String eHD = "ico_spe_mission007";
    public static String eHE = "ico_spe_mission008";
    public static String eHF = "ico_spe_mission009";
    public static String eHG = "ico_spe_mission010";
    public static String eHH = "ico_spe_mission011";
    public static String eHI = "ico_spe_mission012";
    public static String eHJ = "ico_spe_mission013";
    public static String eHK = "ico_spe_mission014";
    public static String eHL = "ico_spe_mission015";
    public static String eHM = "ico_spe_mission016";
    public static String eHN = "ico_buf_defense";
    public static String eHO = "ico_buf_desruptorfield";
    public static String eHP = "ico_buf_disruptor";
    public static String eHQ = "ico_buf_disruptorescudo";
    public static String eHR = "ico_buf_disruptorvelocidade";
    public static String eHS = "ico_buf_dolphin";
    public static String eHT = "ico_buf_energeticweb";
    public static String eHU = "ico_buf_energyshield";
    public static String eHV = "ico_buf_expansor";
    public static String eHW = "ico_buf_gravityfield";
    public static String eHX = "ico_spe_mission017";
    public static String eHY = "ico_spe_mission018";
    public static String eHZ = "ico_buf_highdensity";
    public static String eHa = "ico_buf_charger";
    public static String eHb = "ico_buf_comet";
    public static String eHc = "ico_buf_cruisespeed";
    public static String eHd = "ico_buf_cruisespeedinhibitor";
    public static String eHe = "ico_raw_013";
    public static String eHf = "ico_raw_014";
    public static String eHg = "ico_raw_015";
    public static String eHh = "ico_raw_016";
    public static String eHi = "ico_raw_017";
    public static String eHj = "ico_raw_018";
    public static String eHk = "ico_raw_019";
    public static String eHl = "ico_raw_020";
    public static String eHm = "ico_raw_021";
    public static String eHn = "ico_raw_022";
    public static String eHo = "ico_raw_023";
    public static String eHp = "ico_raw_024";
    public static String eHq = "ico_raw_025";
    public static String eHr = "ico_raw_026";
    public static String eHs = "ico_raw_027";
    public static String eHt = "ico_res_hail";
    public static String eHu = "ico_res_hurricane";
    public static String eHv = "ico_res_mistral";
    public static String eHw = "ico_res_sleet";
    public static String eHx = "ico_spe_mission001";
    public static String eHy = "ico_spe_mission002";
    public static String eHz = "ico_spe_mission003";
    public static String eIa = "ico_buf_interventionarea";
    public static String eIb = "ico_buf_nuvempoeira";
    public static String eIc = "ico_spe_redbutton";
    public static String eId = "ico_buf_placasarmadura";
    public static String eIe = "ico_buf_radiacaosolar";
    public static String eIf = "ico_buf_redbutton";
    public static String eIg = "ico_buf_shield";
    public static String eIh = "ico_buf_shieldrecharge";
    public static String eIi = "ico_buf_shieldrepair";
    public static String eIj = "ico_buf_solarradiation";
    public static String eIk = "ico_buf_structure";
    public static String eIl = "ico_buf_titan";
    public static String[] eIm;
    public static String eIn = "icon_max_";
    public static String eIo = "icon_normal_";
    public static String eIp = "icon_mini_";
    public static String eIq = "tooltip_detail_";

    /* renamed from: ov */
    public static String f2121ov = "imageset_items";

    static {
        String[] strArr = new String[4];
        strArr[0] = eEQ;
        strArr[1] = eES;
        eIm = strArr;
    }

    /* renamed from: a */
    public static String m11558a(String str, String str2, String str3) {
        if (m11556B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m11556B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f2121ov);
    }

    /* renamed from: b */
    public static String m11560b(String str, String str2) {
        if (m11556B(str)) {
            return str;
        }
        if (m11556B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f2121ov);
    }

    /* renamed from: B */
    public static boolean m11556B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C1618Xh.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m11561b(String str, String str2, String str3) {
        return String.valueOf(f2121ov) + C0147Bi.SEPARATOR + m11558a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m11562c(String str, String str2) {
        return String.valueOf(f2121ov) + C0147Bi.SEPARATOR + m11560b(str, str2);
    }

    /* renamed from: a */
    public static String m11557a(String str, int i, String str2) {
        if (!m11556B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f2121ov);
        }
        try {
            Field declaredField = C1618Xh.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f2121ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f2121ov) + C0147Bi.SEPARATOR + m11560b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f2121ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f2121ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f2121ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m11559a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m11556B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f2121ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f2121ov) + C0147Bi.SEPARATOR + m11560b(strArr[i], str);
        }
        return strArr2;
    }
}
