package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.aJa  reason: case insensitive filesystem */
/* compiled from: a */
public class C5430aJa extends C2404ev {
    public static String bPL = "button_tab_left_enabled";
    public static String bTL = "button_tab_left_over";
    public static String bTM = "button_tab_left_press";
    public static String bTN = "button_tab_left_selected";
    public static String bTP = "button_tab_right_enabled";
    public static String bTR = "button_tab_right_over";
    public static String bTS = "button_tab_right_press";
    public static String bTT = "button_tab_right_selected";
    public static String bUe = "button_tab_center_enabled";
    public static String bUg = "button_tab_center_over";
    public static String bUh = "button_tab_center_press";
    public static String bUk = "button_tab_center_selected";
    public static String[] bYh;
    public static String[] bYj;
    public static String[] bYq;
    public static String ieA = "window_center";
    public static String ieB = "border_center";
    public static String ieC = "content_center";
    public static String ieD = "title_center";
    public static String ieE = "info_bottom";
    public static String ieF = "info_center";
    public static String ieG = "info_left";
    public static String ieH = "info_lower_left";
    public static String ieI = "info_lower_right";
    public static String ieJ = "info_right";
    public static String ieK = "content_bottom";
    public static String ieL = "content_left";
    public static String ieM = "content_lower_left";
    public static String ieN = "content_lower_right";
    public static String ieO = "content_right";
    public static String ieP = "content_top";
    public static String ieQ = "content_upper_left";
    public static String ieR = "content_upper_right";
    public static String ieS = "background_borders";
    public static String ieT = "info_top";
    public static String ieU = "info_upper_left";
    public static String ieV = "info_upper_right";
    public static String ieW = "window_bottom";
    public static String ieX = "window_left";
    public static String ieY = "window_lower_left";
    public static String ieZ = "window_lower_right";
    public static String ifa = "window_right";
    public static String ifb = "window_top";
    public static String ifc = "window_upper_left";
    public static String ifd = "window_upper_right";
    public static String ife = "background_center";
    public static String iff = "border_borders";
    public static String ifg = "title_borders";

    /* renamed from: ov */
    public static String f3214ov = "imageset_new_window";

    static {
        String[] strArr = new String[5];
        strArr[0] = bTP;
        strArr[1] = bTR;
        strArr[2] = bTS;
        strArr[4] = bTT;
        bYq = strArr;
        String[] strArr2 = new String[5];
        strArr2[0] = bPL;
        strArr2[1] = bTL;
        strArr2[2] = bTM;
        strArr2[4] = bTN;
        bYh = strArr2;
        String[] strArr3 = new String[5];
        strArr3[0] = bUe;
        strArr3[1] = bUg;
        strArr3[2] = bUh;
        strArr3[4] = bUk;
        bYj = strArr3;
    }

    /* renamed from: a */
    public static String m15792a(String str, String str2, String str3) {
        if (m15790B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m15790B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f3214ov);
    }

    /* renamed from: b */
    public static String m15794b(String str, String str2) {
        if (m15790B(str)) {
            return str;
        }
        if (m15790B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f3214ov);
    }

    /* renamed from: B */
    public static boolean m15790B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C5430aJa.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m15795b(String str, String str2, String str3) {
        return String.valueOf(f3214ov) + C0147Bi.SEPARATOR + m15792a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m15796c(String str, String str2) {
        return String.valueOf(f3214ov) + C0147Bi.SEPARATOR + m15794b(str, str2);
    }

    /* renamed from: a */
    public static String m15791a(String str, int i, String str2) {
        if (!m15790B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f3214ov);
        }
        try {
            Field declaredField = C5430aJa.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f3214ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f3214ov) + C0147Bi.SEPARATOR + m15794b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f3214ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f3214ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f3214ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m15793a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m15790B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f3214ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f3214ov) + C0147Bi.SEPARATOR + m15794b(strArr[i], str);
        }
        return strArr2;
    }
}
