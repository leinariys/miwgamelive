package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.aTk  reason: case insensitive filesystem */
/* compiled from: a */
public class C5700aTk extends C2404ev {
    public static String bPL = "button_tab_left_enabled";
    public static String bTL = "button_tab_left_over";
    public static String bTM = "button_tab_left_press";
    public static String bTN = "button_tab_left_selected";
    public static String bTP = "button_tab_right_enabled";
    public static String bTR = "button_tab_right_over";
    public static String bTS = "button_tab_right_press";
    public static String bTT = "button_tab_right_selected";
    public static String bUe = "button_tab_center_enabled";
    public static String bUg = "button_tab_center_over";
    public static String bUh = "button_tab_center_press";
    public static String bUk = "button_tab_center_selected";
    public static String[] bYh;
    public static String[] bYj;
    public static String[] bYq;
    public static String gHK = "button_squadron_left_enabled";
    public static String gHO = "frame_dim_borders";
    public static String gHS = "frame_header_center";
    public static String gHY = "button_squadron_left_over";
    public static String gHZ = "button_squadron_left_press";
    public static String gHo = "frame_dim_center";
    public static String gHt = "frame_header_left";
    public static String gHu = "frame_header_right";
    public static String gHv = "frame_footer_left";
    public static String gHy = "frame_footer_right";
    public static String[] gIH;
    public static String[] gIK;
    public static String[] gIN;
    public static String gIa = "button_squadron_left_selected";
    public static String gIb = "button_squadron_right_enabled";
    public static String gIc = "button_squadron_right_over";
    public static String gId = "button_squadron_right_press";
    public static String gIe = "button_squadron_right_selected";
    public static String gIm = "button_squadron_center_enabled";
    public static String gIn = "button_squadron_center_over";
    public static String gIo = "button_squadron_center_press";
    public static String gIp = "button_squadron_center_selected";

    /* renamed from: ov */
    public static String f3816ov = "imageset_new_hudframe";

    static {
        String[] strArr = new String[5];
        strArr[0] = gIb;
        strArr[1] = gIc;
        strArr[2] = gId;
        strArr[4] = gIe;
        gIH = strArr;
        String[] strArr2 = new String[5];
        strArr2[0] = gHK;
        strArr2[1] = gHY;
        strArr2[2] = gHZ;
        strArr2[4] = gIa;
        gIN = strArr2;
        String[] strArr3 = new String[5];
        strArr3[0] = gIm;
        strArr3[1] = gIn;
        strArr3[2] = gIo;
        strArr3[4] = gIp;
        gIK = strArr3;
        String[] strArr4 = new String[5];
        strArr4[0] = bTP;
        strArr4[1] = bTR;
        strArr4[2] = bTS;
        strArr4[4] = bTT;
        bYq = strArr4;
        String[] strArr5 = new String[5];
        strArr5[0] = bPL;
        strArr5[1] = bTL;
        strArr5[2] = bTM;
        strArr5[4] = bTN;
        bYh = strArr5;
        String[] strArr6 = new String[5];
        strArr6[0] = bUe;
        strArr6[1] = bUg;
        strArr6[2] = bUh;
        strArr6[4] = bUk;
        bYj = strArr6;
    }

    /* renamed from: a */
    public static String m18384a(String str, String str2, String str3) {
        if (m18382B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m18382B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f3816ov);
    }

    /* renamed from: b */
    public static String m18386b(String str, String str2) {
        if (m18382B(str)) {
            return str;
        }
        if (m18382B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f3816ov);
    }

    /* renamed from: B */
    public static boolean m18382B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C5700aTk.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m18387b(String str, String str2, String str3) {
        return String.valueOf(f3816ov) + C0147Bi.SEPARATOR + m18384a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m18388c(String str, String str2) {
        return String.valueOf(f3816ov) + C0147Bi.SEPARATOR + m18386b(str, str2);
    }

    /* renamed from: a */
    public static String m18383a(String str, int i, String str2) {
        if (!m18382B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f3816ov);
        }
        try {
            Field declaredField = C5700aTk.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f3816ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f3816ov) + C0147Bi.SEPARATOR + m18386b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f3816ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f3816ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f3816ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m18385a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m18382B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f3816ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f3816ov) + C0147Bi.SEPARATOR + m18386b(strArr[i], str);
        }
        return strArr2;
    }
}
