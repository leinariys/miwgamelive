package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.aVQ */
/* compiled from: a */
public class aVQ extends C2404ev {
    public static String gHO = "frame_dim_borders";
    public static String gHo = "frame_dim_center";

    /* renamed from: ov */
    public static String f3916ov = "imageset_new_content";

    /* renamed from: a */
    public static String m18807a(String str, String str2, String str3) {
        if (m18805B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m18805B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f3916ov);
    }

    /* renamed from: b */
    public static String m18809b(String str, String str2) {
        if (m18805B(str)) {
            return str;
        }
        if (m18805B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f3916ov);
    }

    /* renamed from: B */
    public static boolean m18805B(String str) {
        if (str == null) {
            return false;
        }
        try {
            aVQ.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m18810b(String str, String str2, String str3) {
        return String.valueOf(f3916ov) + C0147Bi.SEPARATOR + m18807a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m18811c(String str, String str2) {
        return String.valueOf(f3916ov) + C0147Bi.SEPARATOR + m18809b(str, str2);
    }

    /* renamed from: a */
    public static String m18806a(String str, int i, String str2) {
        if (!m18805B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f3916ov);
        }
        try {
            Field declaredField = C6089agJ.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f3916ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f3916ov) + C0147Bi.SEPARATOR + m18809b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f3916ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f3916ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f3916ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m18808a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m18805B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f3916ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f3916ov) + C0147Bi.SEPARATOR + m18809b(strArr[i], str);
        }
        return strArr2;
    }
}
