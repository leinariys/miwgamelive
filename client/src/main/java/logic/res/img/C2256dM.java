package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.dM */
/* compiled from: a */
public class C2256dM extends C2404ev {

    /* renamed from: Av */
    public static String f6493Av = "tela01";

    /* renamed from: Aw */
    public static String f6494Aw = "tela02";

    /* renamed from: Ax */
    public static String f6495Ax = "tela03";

    /* renamed from: Ay */
    public static String f6496Ay = "tela04";

    /* renamed from: Az */
    public static String f6497Az = "tela05";

    /* renamed from: ov */
    public static String f6498ov = "imageset_loading";

    /* renamed from: a */
    public static String m28816a(String str, String str2, String str3) {
        if (m28814B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m28814B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f6498ov);
    }

    /* renamed from: b */
    public static String m28818b(String str, String str2) {
        if (m28814B(str)) {
            return str;
        }
        if (m28814B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f6498ov);
    }

    /* renamed from: B */
    public static boolean m28814B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C2256dM.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m28819b(String str, String str2, String str3) {
        return String.valueOf(f6498ov) + C0147Bi.SEPARATOR + m28816a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m28820c(String str, String str2) {
        return String.valueOf(f6498ov) + C0147Bi.SEPARATOR + m28818b(str, str2);
    }

    /* renamed from: a */
    public static String m28815a(String str, int i, String str2) {
        if (!m28814B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f6498ov);
        }
        try {
            Field declaredField = C2256dM.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f6498ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f6498ov) + C0147Bi.SEPARATOR + m28818b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f6498ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f6498ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f6498ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m28817a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m28814B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f6498ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f6498ov) + C0147Bi.SEPARATOR + m28818b(strArr[i], str);
        }
        return strArr2;
    }
}
