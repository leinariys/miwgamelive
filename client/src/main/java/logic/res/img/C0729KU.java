package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.KU */
/* compiled from: a */
public class C0729KU extends C2404ev {
    public static String drz = "map_hack_01_en";

    /* renamed from: ov */
    public static String f972ov = "imageset_map_01_en";

    /* renamed from: a */
    public static String m6412a(String str, String str2, String str3) {
        if (m6410B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m6410B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f972ov);
    }

    /* renamed from: b */
    public static String m6414b(String str, String str2) {
        if (m6410B(str)) {
            return str;
        }
        if (m6410B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f972ov);
    }

    /* renamed from: B */
    public static boolean m6410B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C0729KU.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m6415b(String str, String str2, String str3) {
        return String.valueOf(f972ov) + C0147Bi.SEPARATOR + m6412a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m6416c(String str, String str2) {
        return String.valueOf(f972ov) + C0147Bi.SEPARATOR + m6414b(str, str2);
    }

    /* renamed from: a */
    public static String m6411a(String str, int i, String str2) {
        if (!m6410B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f972ov);
        }
        try {
            Field declaredField = C0729KU.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f972ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f972ov) + C0147Bi.SEPARATOR + m6414b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f972ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f972ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f972ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m6413a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m6410B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f972ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f972ov) + C0147Bi.SEPARATOR + m6414b(strArr[i], str);
        }
        return strArr2;
    }
}
