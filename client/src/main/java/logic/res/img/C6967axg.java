package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.axg  reason: case insensitive filesystem */
/* compiled from: a */
public class C6967axg extends C2404ev {
    public static String gPs = "map_hack_03_en";

    /* renamed from: ov */
    public static String f5572ov = "imageset_map_03_en";

    /* renamed from: a */
    public static String m27167a(String str, String str2, String str3) {
        if (m27165B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m27165B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f5572ov);
    }

    /* renamed from: b */
    public static String m27169b(String str, String str2) {
        if (m27165B(str)) {
            return str;
        }
        if (m27165B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f5572ov);
    }

    /* renamed from: B */
    public static boolean m27165B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C6967axg.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m27170b(String str, String str2, String str3) {
        return String.valueOf(f5572ov) + C0147Bi.SEPARATOR + m27167a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m27171c(String str, String str2) {
        return String.valueOf(f5572ov) + C0147Bi.SEPARATOR + m27169b(str, str2);
    }

    /* renamed from: a */
    public static String m27166a(String str, int i, String str2) {
        if (!m27165B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f5572ov);
        }
        try {
            Field declaredField = C6967axg.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f5572ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f5572ov) + C0147Bi.SEPARATOR + m27169b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f5572ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f5572ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f5572ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m27168a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m27165B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f5572ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f5572ov) + C0147Bi.SEPARATOR + m27169b(strArr[i], str);
        }
        return strArr2;
    }
}
