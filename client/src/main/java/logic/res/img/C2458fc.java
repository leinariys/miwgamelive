package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.fc */
/* compiled from: a */
public class C2458fc extends C2404ev {

    /* renamed from: Jg */
    public static String f7412Jg = "map_hack_pt";

    /* renamed from: ov */
    public static String f7413ov = "imageset_map_pt";

    /* renamed from: a */
    public static String m31198a(String str, String str2, String str3) {
        if (m31196B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m31196B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f7413ov);
    }

    /* renamed from: b */
    public static String m31200b(String str, String str2) {
        if (m31196B(str)) {
            return str;
        }
        if (m31196B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f7413ov);
    }

    /* renamed from: B */
    public static boolean m31196B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C2458fc.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m31201b(String str, String str2, String str3) {
        return String.valueOf(f7413ov) + C0147Bi.SEPARATOR + m31198a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m31202c(String str, String str2) {
        return String.valueOf(f7413ov) + C0147Bi.SEPARATOR + m31200b(str, str2);
    }

    /* renamed from: a */
    public static String m31197a(String str, int i, String str2) {
        if (!m31196B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f7413ov);
        }
        try {
            Field declaredField = C6089agJ.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f7413ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f7413ov) + C0147Bi.SEPARATOR + m31200b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f7413ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f7413ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f7413ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m31199a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m31196B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f7413ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f7413ov) + C0147Bi.SEPARATOR + m31200b(strArr[i], str);
        }
        return strArr2;
    }
}
