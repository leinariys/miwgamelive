package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.avu  reason: case insensitive filesystem */
/* compiled from: a */
public class C6906avu extends C2404ev {
    public static String bPL = "button_tab_left_enabled";
    public static String bTL = "button_tab_left_over";
    public static String bTM = "button_tab_left_press";
    public static String bTN = "button_tab_left_selected";
    public static String bTP = "button_tab_right_enabled";
    public static String bTR = "button_tab_right_over";
    public static String bTS = "button_tab_right_press";
    public static String bTT = "button_tab_right_selected";
    public static String bUe = "button_tab_center_enabled";
    public static String bUg = "button_tab_center_over";
    public static String bUh = "button_tab_center_press";
    public static String bUk = "button_tab_center_selected";
    public static String[] bYh;
    public static String[] bYj;
    public static String[] bYq;
    public static String gHA = "button_sidebar_left_top_over";
    public static String gHB = "button_sidebar_left_top_over_selected";
    public static String gHC = "button_sidebar_left_top_press";
    public static String gHD = "button_sidebar_left_top_selected";
    public static String gHE = "button_sidebar_right_bottom_enabled";
    public static String gHF = "button_sidebar_right_bottom_over";
    public static String gHG = "button_sidebar_right_bottom_over_selected";
    public static String gHH = "button_sidebar_right_bottom_selected";
    public static String gHI = "button_sidebar_right_top_enabled";
    public static String gHJ = "button_sidebar_right_top_over";
    public static String gHK = "button_squadron_left_enabled";
    public static String gHL = "sidebar_right_frame_lower_left";
    public static String gHM = "button_sidebar_left_center_enabled";
    public static String gHN = "sidebar_arrow_black_left";
    public static String gHO = "frame_dim_borders";
    public static String gHP = "button_sidebar_right_top_over_selected";
    public static String gHQ = "button_sidebar_right_top_press";
    public static String gHR = "button_sidebar_right_top_selected";
    public static String gHS = "frame_header_center";
    public static String gHT = "sidebar_left_frame_right";
    public static String gHU = "button_sidebar_left_bottom_press";
    public static String gHV = "button_sidebar_right_bottom_press";
    public static String gHW = "button_sidebar_left_center_over";
    public static String gHX = "sidebar_arrow_black_right";
    public static String gHY = "button_squadron_left_over";
    public static String gHZ = "button_squadron_left_press";
    public static String gHo = "frame_dim_center";
    public static String gHp = "button_sidebar_left_bottom_enabled";
    public static String gHq = "button_sidebar_left_bottom_over";
    public static String gHr = "sidebar_left_frame_lower_left";
    public static String gHs = "sidebar_left_frame_upper_left";
    public static String gHt = "frame_header_left";
    public static String gHu = "frame_header_right";
    public static String gHv = "frame_footer_left";
    public static String gHw = "button_sidebar_left_bottom_over_selected";
    public static String gHx = "button_sidebar_left_bottom_selected";
    public static String gHy = "frame_footer_right";
    public static String gHz = "button_sidebar_left_top_enabled";
    public static String gIA = "sidebar_left_frame_upper_right";
    public static String gIB = "sidebar_right_frame_bottom";
    public static String gIC = "sidebar_right_frame_lower_right";
    public static String gID = "sidebar_right_frame_top";
    public static String gIE = "sidebar_right_frame_upper_right";
    public static String[] gIF;
    public static String[] gIG;
    public static String[] gIH;
    public static String[] gII;
    public static String[] gIJ;
    public static String[] gIK;
    public static String[] gIL;
    public static String[] gIM;
    public static String[] gIN;
    public static String gIa = "button_squadron_left_selected";
    public static String gIb = "button_squadron_right_enabled";
    public static String gIc = "button_squadron_right_over";
    public static String gId = "button_squadron_right_press";
    public static String gIe = "button_squadron_right_selected";
    public static String gIf = "sidebar_right_frame_left";
    public static String gIg = "sidebar_left_frame_center";
    public static String gIh = "sidebar_right_frame_center";
    public static String gIi = "sidebar_right_frame_upper_left";
    public static String gIj = "button_sidebar_left_center_over_selected";
    public static String gIk = "sidebar_arrow_white_left";
    public static String gIl = "sidebar_arrow_white_right";
    public static String gIm = "button_squadron_center_enabled";
    public static String gIn = "button_squadron_center_over";
    public static String gIo = "button_squadron_center_press";
    public static String gIp = "button_squadron_center_selected";
    public static String gIq = "button_sidebar_left_center_press";
    public static String gIr = "button_sidebar_left_center_selected";
    public static String gIs = "button_sidebar_right_center_enabled";
    public static String gIt = "sidebar_left_frame_bottom";
    public static String gIu = "button_sidebar_right_center_over";
    public static String gIv = "button_sidebar_right_center_over_selected";
    public static String gIw = "button_sidebar_right_center_press";
    public static String gIx = "button_sidebar_right_center_selected";
    public static String gIy = "sidebar_left_frame_lower_right";
    public static String gIz = "sidebar_left_frame_top";

    /* renamed from: ov */
    public static String f5471ov = "imageset_new_sidebar";

    static {
        String[] strArr = new String[5];
        strArr[0] = gHE;
        strArr[1] = gHF;
        strArr[2] = gHV;
        strArr[4] = gHH;
        gIF = strArr;
        String[] strArr2 = new String[5];
        strArr2[0] = gHp;
        strArr2[1] = gHq;
        strArr2[2] = gHU;
        strArr2[4] = gHx;
        gIG = strArr2;
        String[] strArr3 = new String[5];
        strArr3[0] = gIb;
        strArr3[1] = gIc;
        strArr3[2] = gId;
        strArr3[4] = gIe;
        gIH = strArr3;
        String[] strArr4 = new String[5];
        strArr4[0] = gHI;
        strArr4[1] = gHJ;
        strArr4[2] = gHQ;
        strArr4[4] = gHR;
        gII = strArr4;
        String[] strArr5 = new String[5];
        strArr5[0] = gHz;
        strArr5[1] = gHA;
        strArr5[2] = gHC;
        strArr5[4] = gHD;
        gIJ = strArr5;
        String[] strArr6 = new String[5];
        strArr6[0] = gIm;
        strArr6[1] = gIn;
        strArr6[2] = gIo;
        strArr6[4] = gIp;
        gIK = strArr6;
        String[] strArr7 = new String[5];
        strArr7[0] = bPL;
        strArr7[1] = bTL;
        strArr7[2] = bTM;
        strArr7[4] = bTN;
        bYh = strArr7;
        String[] strArr8 = new String[5];
        strArr8[0] = gIs;
        strArr8[1] = gIu;
        strArr8[2] = gIw;
        strArr8[4] = gIx;
        gIL = strArr8;
        String[] strArr9 = new String[5];
        strArr9[0] = bUe;
        strArr9[1] = bUg;
        strArr9[2] = bUh;
        strArr9[4] = bUk;
        bYj = strArr9;
        String[] strArr10 = new String[5];
        strArr10[0] = gHM;
        strArr10[1] = gHW;
        strArr10[2] = gIq;
        strArr10[4] = gIr;
        gIM = strArr10;
        String[] strArr11 = new String[5];
        strArr11[0] = gHK;
        strArr11[1] = gHY;
        strArr11[2] = gHZ;
        strArr11[4] = gIa;
        gIN = strArr11;
        String[] strArr12 = new String[5];
        strArr12[0] = bTP;
        strArr12[1] = bTR;
        strArr12[2] = bTS;
        strArr12[4] = bTT;
        bYq = strArr12;
    }

    /* renamed from: a */
    public static String m26758a(String str, String str2, String str3) {
        if (m26756B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m26756B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f5471ov);
    }

    /* renamed from: b */
    public static String m26760b(String str, String str2) {
        if (m26756B(str)) {
            return str;
        }
        if (m26756B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f5471ov);
    }

    /* renamed from: B */
    public static boolean m26756B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C6906avu.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m26761b(String str, String str2, String str3) {
        return String.valueOf(f5471ov) + C0147Bi.SEPARATOR + m26758a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m26762c(String str, String str2) {
        return String.valueOf(f5471ov) + C0147Bi.SEPARATOR + m26760b(str, str2);
    }

    /* renamed from: a */
    public static String m26757a(String str, int i, String str2) {
        if (!m26756B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f5471ov);
        }
        try {
            Field declaredField = C6906avu.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f5471ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f5471ov) + C0147Bi.SEPARATOR + m26760b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f5471ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f5471ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f5471ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m26759a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m26756B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f5471ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f5471ov) + C0147Bi.SEPARATOR + m26760b(strArr[i], str);
        }
        return strArr2;
    }
}
