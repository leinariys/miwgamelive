package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.agF  reason: case insensitive filesystem */
/* compiled from: a */
public class C6085agF extends C2404ev {
    public static String fyJ = "map_hack_04_en";

    /* renamed from: ov */
    public static String f4507ov = "imageset_map_04_en";

    /* renamed from: a */
    public static String m21961a(String str, String str2, String str3) {
        if (m21959B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m21959B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f4507ov);
    }

    /* renamed from: b */
    public static String m21963b(String str, String str2) {
        if (m21959B(str)) {
            return str;
        }
        if (m21959B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f4507ov);
    }

    /* renamed from: B */
    public static boolean m21959B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C6085agF.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m21964b(String str, String str2, String str3) {
        return String.valueOf(f4507ov) + C0147Bi.SEPARATOR + m21961a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m21965c(String str, String str2) {
        return String.valueOf(f4507ov) + C0147Bi.SEPARATOR + m21963b(str, str2);
    }

    /* renamed from: a */
    public static String m21960a(String str, int i, String str2) {
        if (!m21959B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f4507ov);
        }
        try {
            Field declaredField = C6085agF.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f4507ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f4507ov) + C0147Bi.SEPARATOR + m21963b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f4507ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f4507ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f4507ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m21962a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m21959B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f4507ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f4507ov) + C0147Bi.SEPARATOR + m21963b(strArr[i], str);
        }
        return strArr2;
    }
}
