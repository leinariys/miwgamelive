package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.aCp  reason: case insensitive filesystem */
/* compiled from: a */
public class C5263aCp extends C2404ev {
    public static String fhO = "memory_cell_overclock";
    public static String fhP = "memory_fill_normal";
    public static String fhW = "memory_fill_overclock";
    public static String fhx = "memory_cell_frame";
    public static String[] fiE;
    public static String fie = "button_install_enabled";
    public static String fif = "button_install_over";
    public static String fig = "button_install_press";
    public static String hrA = "button_explorer_over";
    public static String hrB = "button_explorer_over_selected";
    public static String hrC = "button_explorer_press";
    public static String hrD = "button_explorer_selected";
    public static String hrE = "button_fighter_enabled";
    public static String hrF = "button_fighter_over";
    public static String hrG = "memory_cell_normal";
    public static String hrH = "frame_bottom";
    public static String hrI = "frame_left";
    public static String hrJ = "frame_lower_left";
    public static String hrK = "frame_lower_right";
    public static String hrL = "frame_right";
    public static String hrM = "frame_top";
    public static String hrN = "frame_upper_left";
    public static String hrO = "frame_upper_right";
    public static String hrP = "button_delete_enabled";
    public static String hrQ = "button_delete_over";
    public static String hrR = "button_delete_press";
    public static String hrS = "certificate_installed_enabled";
    public static String hrT = "certificate_installed_over";
    public static String hrU = "certificate_over";
    public static String hrV = "button_fighter_over_selected";
    public static String hrW = "button_fighter_press";
    public static String hrX = "button_fighter_selected";
    public static String hrY = "button_freighter_enabled";
    public static String hrZ = "button_freighter_over";
    public static String hrq = "header_top";
    public static String hrr = "header_right";
    public static String hrs = "header_left";
    public static String hrt = "certificate_enabled";
    public static String hru = "button_bomber_enabled";
    public static String hrv = "button_bomber_over";
    public static String hrw = "button_bomber_over_selected";
    public static String hrx = "button_bomber_press";
    public static String hry = "button_bomber_selected";
    public static String hrz = "button_explorer_enabled";
    public static String hsA = "button_big_press";
    public static String hsB = "certificate_press";
    public static String hsC = "certificate_selected";
    public static String hsD = "special_selected";
    public static String hsE = "special_unavailable_enabled";
    public static String hsF = "certificate_unavailable_enabled";
    public static String hsG = "certificate_unavailable_over";
    public static String[] hsH;
    public static String[] hsI;
    public static String[] hsJ;
    public static String[] hsK;
    public static String[] hsL;
    public static String[] hsM;
    public static String[] hsN;
    public static String[] hsO;
    public static String[] hsP;
    public static String[] hsQ;
    public static String[] hsR;
    public static String[] hsS;
    public static String hsa = "button_freighter_over_selected";
    public static String hsb = "button_freighter_press";
    public static String hsc = "button_freighter_selected";
    public static String hsd = "button_systems_enabled";
    public static String hse = "button_systems_over";
    public static String hsf = "button_systems_over_selected";
    public static String hsg = "button_systems_press";
    public static String hsh = "button_systems_selected";
    public static String hsi = "button_weapons_enabled";
    public static String hsj = "button_weapons_over";
    public static String hsk = "button_weapons_over_selected";
    public static String hsl = "button_weapons_press";
    public static String hsm = "button_weapons_selected";
    public static String hsn = "frame_center";
    public static String hso = "icon_certificate_bomber";
    public static String hsp = "icon_certificate_explorer";
    public static String hsq = "icon_certificate_fighter";
    public static String hsr = "icon_certificate_freighter";
    public static String hss = "icon_certificate_systems";
    public static String hst = "icon_certificate_weapons";
    public static String hsu = "button_big_enabled";
    public static String hsv = "button_big_over";
    public static String hsw = "certificate_over_selected";
    public static String hsx = "special_enabled";
    public static String hsy = "special_over";
    public static String hsz = "special_over_selected";

    /* renamed from: ov */
    public static String f2488ov = "imageset_new_progression";

    static {
        String[] strArr = new String[4];
        strArr[0] = fie;
        strArr[1] = fif;
        strArr[2] = fig;
        fiE = strArr;
        String[] strArr2 = new String[4];
        strArr2[0] = hrS;
        strArr2[1] = hrT;
        hsH = strArr2;
        String[] strArr3 = new String[4];
        strArr3[0] = hsu;
        strArr3[1] = hsv;
        strArr3[2] = hsA;
        hsI = strArr3;
        String[] strArr4 = new String[5];
        strArr4[0] = hsi;
        strArr4[1] = hsj;
        strArr4[2] = hsl;
        strArr4[4] = hsm;
        hsJ = strArr4;
        String[] strArr5 = new String[5];
        strArr5[0] = hrt;
        strArr5[1] = hrU;
        strArr5[2] = hsB;
        strArr5[4] = hsC;
        hsK = strArr5;
        String[] strArr6 = new String[5];
        strArr6[0] = hru;
        strArr6[1] = hrv;
        strArr6[2] = hrx;
        strArr6[4] = hry;
        hsL = strArr6;
        String[] strArr7 = new String[4];
        strArr7[0] = hsF;
        strArr7[1] = hsG;
        hsM = strArr7;
        String[] strArr8 = new String[4];
        strArr8[0] = hrP;
        strArr8[1] = hrQ;
        strArr8[2] = hrR;
        hsN = strArr8;
        String[] strArr9 = new String[5];
        strArr9[0] = hrY;
        strArr9[1] = hrZ;
        strArr9[2] = hsb;
        strArr9[4] = hsc;
        hsO = strArr9;
        String[] strArr10 = new String[5];
        strArr10[0] = hrz;
        strArr10[1] = hrA;
        strArr10[2] = hrC;
        strArr10[4] = hrD;
        hsP = strArr10;
        String[] strArr11 = new String[5];
        strArr11[0] = hsx;
        strArr11[1] = hsy;
        strArr11[4] = hsD;
        hsQ = strArr11;
        String[] strArr12 = new String[5];
        strArr12[0] = hrE;
        strArr12[1] = hrF;
        strArr12[2] = hrW;
        strArr12[4] = hrX;
        hsR = strArr12;
        String[] strArr13 = new String[5];
        strArr13[0] = hsd;
        strArr13[1] = hse;
        strArr13[2] = hsg;
        strArr13[4] = hsh;
        hsS = strArr13;
    }

    /* renamed from: a */
    public static String m13377a(String str, String str2, String str3) {
        if (m13375B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m13375B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f2488ov);
    }

    /* renamed from: b */
    public static String m13379b(String str, String str2) {
        if (m13375B(str)) {
            return str;
        }
        if (m13375B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f2488ov);
    }

    /* renamed from: B */
    public static boolean m13375B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C5263aCp.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m13380b(String str, String str2, String str3) {
        return String.valueOf(f2488ov) + C0147Bi.SEPARATOR + m13377a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m13381c(String str, String str2) {
        return String.valueOf(f2488ov) + C0147Bi.SEPARATOR + m13379b(str, str2);
    }

    /* renamed from: a */
    public static String m13376a(String str, int i, String str2) {
        if (!m13375B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f2488ov);
        }
        try {
            Field declaredField = C5263aCp.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f2488ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f2488ov) + C0147Bi.SEPARATOR + m13379b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f2488ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f2488ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f2488ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m13378a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m13375B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f2488ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f2488ov) + C0147Bi.SEPARATOR + m13379b(strArr[i], str);
        }
        return strArr2;
    }
}
