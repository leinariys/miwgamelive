package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.bx */
/* compiled from: a */
public class C2104bx extends C2404ev {

    /* renamed from: ov */
    public static String f5942ov = "imageset_character_background";

    /* renamed from: ow */
    public static String f5943ow = "tela_personagem_fundo";

    /* renamed from: a */
    public static String m28132a(String str, String str2, String str3) {
        if (m28130B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m28130B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f5942ov);
    }

    /* renamed from: b */
    public static String m28134b(String str, String str2) {
        if (m28130B(str)) {
            return str;
        }
        if (m28130B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f5942ov);
    }

    /* renamed from: B */
    public static boolean m28130B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C2104bx.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m28135b(String str, String str2, String str3) {
        return String.valueOf(f5942ov) + C0147Bi.SEPARATOR + m28132a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m28136c(String str, String str2) {
        return String.valueOf(f5942ov) + C0147Bi.SEPARATOR + m28134b(str, str2);
    }

    /* renamed from: a */
    public static String m28131a(String str, int i, String str2) {
        if (!m28130B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f5942ov);
        }
        try {
            Field declaredField = C2104bx.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f5942ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f5942ov) + C0147Bi.SEPARATOR + m28134b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f5942ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f5942ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f5942ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m28133a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m28130B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f5942ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f5942ov) + C0147Bi.SEPARATOR + m28134b(strArr[i], str);
        }
        return strArr2;
    }
}
