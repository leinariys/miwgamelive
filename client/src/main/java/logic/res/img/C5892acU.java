package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.acU  reason: case insensitive filesystem */
/* compiled from: a */
public class C5892acU extends C2404ev {
    public static String ffG = "map_hack_04_pt";

    /* renamed from: ov */
    public static String f4226ov = "imageset_map_04_pt";

    /* renamed from: a */
    public static String m20387a(String str, String str2, String str3) {
        if (m20385B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m20385B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f4226ov);
    }

    /* renamed from: b */
    public static String m20389b(String str, String str2) {
        if (m20385B(str)) {
            return str;
        }
        if (m20385B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f4226ov);
    }

    /* renamed from: B */
    public static boolean m20385B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C5892acU.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m20390b(String str, String str2, String str3) {
        return String.valueOf(f4226ov) + C0147Bi.SEPARATOR + m20387a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m20391c(String str, String str2) {
        return String.valueOf(f4226ov) + C0147Bi.SEPARATOR + m20389b(str, str2);
    }

    /* renamed from: a */
    public static String m20386a(String str, int i, String str2) {
        if (!m20385B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f4226ov);
        }
        try {
            Field declaredField = C5892acU.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f4226ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f4226ov) + C0147Bi.SEPARATOR + m20389b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f4226ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f4226ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f4226ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m20388a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m20385B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f4226ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f4226ov) + C0147Bi.SEPARATOR + m20389b(strArr[i], str);
        }
        return strArr2;
    }
}
