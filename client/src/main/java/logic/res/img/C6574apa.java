package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.apa  reason: case insensitive filesystem */
/* compiled from: a */
public class C6574apa extends C2404ev {
    public static String gkR = "map_hack_02_pt";

    /* renamed from: ov */
    public static String f5109ov = "imageset_map_02_pt";

    /* renamed from: a */
    public static String m24931a(String str, String str2, String str3) {
        if (m24929B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m24929B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f5109ov);
    }

    /* renamed from: b */
    public static String m24933b(String str, String str2) {
        if (m24929B(str)) {
            return str;
        }
        if (m24929B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f5109ov);
    }

    /* renamed from: B */
    public static boolean m24929B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C6574apa.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m24934b(String str, String str2, String str3) {
        return String.valueOf(f5109ov) + C0147Bi.SEPARATOR + m24931a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m24935c(String str, String str2) {
        return String.valueOf(f5109ov) + C0147Bi.SEPARATOR + m24933b(str, str2);
    }

    /* renamed from: a */
    public static String m24930a(String str, int i, String str2) {
        if (!m24929B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f5109ov);
        }
        try {
            Field declaredField = C6574apa.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f5109ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f5109ov) + C0147Bi.SEPARATOR + m24933b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f5109ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f5109ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f5109ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m24932a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m24929B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f5109ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f5109ov) + C0147Bi.SEPARATOR + m24933b(strArr[i], str);
        }
        return strArr2;
    }
}
