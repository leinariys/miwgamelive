package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.Yh */
/* compiled from: a */
public class C1677Yh extends C2404ev {

    /* renamed from: Av */
    public static String f2186Av = "tela01";

    /* renamed from: ov */
    public static String f2187ov = "imageset_loading_bg1";

    /* renamed from: a */
    public static String m11916a(String str, String str2, String str3) {
        if (m11914B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m11914B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f2187ov);
    }

    /* renamed from: b */
    public static String m11918b(String str, String str2) {
        if (m11914B(str)) {
            return str;
        }
        if (m11914B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f2187ov);
    }

    /* renamed from: B */
    public static boolean m11914B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C1677Yh.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m11919b(String str, String str2, String str3) {
        return String.valueOf(f2187ov) + C0147Bi.SEPARATOR + m11916a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m11920c(String str, String str2) {
        return String.valueOf(f2187ov) + C0147Bi.SEPARATOR + m11918b(str, str2);
    }

    /* renamed from: a */
    public static String m11915a(String str, int i, String str2) {
        if (!m11914B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f2187ov);
        }
        try {
            Field declaredField = C1677Yh.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f2187ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f2187ov) + C0147Bi.SEPARATOR + m11918b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f2187ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f2187ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f2187ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m11917a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m11914B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f2187ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f2187ov) + C0147Bi.SEPARATOR + m11918b(strArr[i], str);
        }
        return strArr2;
    }
}
