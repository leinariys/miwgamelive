package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.aLj  reason: case insensitive filesystem */
/* compiled from: a */
public class C5491aLj extends C2404ev {
    public static String ikI = "ping_green";
    public static String ikJ = "ping_orange";
    public static String ikK = "ping_red";

    /* renamed from: ov */
    public static String f3323ov = "imageset_bandwidth";

    /* renamed from: a */
    public static String m16268a(String str, String str2, String str3) {
        if (m16266B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m16266B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f3323ov);
    }

    /* renamed from: b */
    public static String m16270b(String str, String str2) {
        if (m16266B(str)) {
            return str;
        }
        if (m16266B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f3323ov);
    }

    /* renamed from: B */
    public static boolean m16266B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C5491aLj.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m16271b(String str, String str2, String str3) {
        return String.valueOf(f3323ov) + C0147Bi.SEPARATOR + m16268a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m16272c(String str, String str2) {
        return String.valueOf(f3323ov) + C0147Bi.SEPARATOR + m16270b(str, str2);
    }

    /* renamed from: a */
    public static String m16267a(String str, int i, String str2) {
        if (!m16266B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f3323ov);
        }
        try {
            Field declaredField = C5491aLj.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f3323ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f3323ov) + C0147Bi.SEPARATOR + m16270b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f3323ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f3323ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f3323ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m16269a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m16266B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f3323ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f3323ov) + C0147Bi.SEPARATOR + m16270b(strArr[i], str);
        }
        return strArr2;
    }
}
