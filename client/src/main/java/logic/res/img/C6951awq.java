package logic.res.img;

import org.mozilla1.classfile.C0147Bi;

import java.lang.reflect.Field;

@C0386FL
/* renamed from: a.awq  reason: case insensitive filesystem */
/* compiled from: a */
public class C6951awq extends C2404ev {
    public static String gMe = "map_hack_03_pt";

    /* renamed from: ov */
    public static String f5529ov = "imageset_map_03_pt";

    /* renamed from: a */
    public static String m26963a(String str, String str2, String str3) {
        if (m26961B(String.valueOf(str) + str2)) {
            return String.valueOf(str) + str2;
        }
        if (m26961B(str3)) {
            return str3;
        }
        throw new RuntimeException("default image " + str + str3 + " in imageset " + f5529ov);
    }

    /* renamed from: b */
    public static String m26965b(String str, String str2) {
        if (m26961B(str)) {
            return str;
        }
        if (m26961B(str2)) {
            return str2;
        }
        throw new RuntimeException("default image " + str2 + " in imageset " + f5529ov);
    }

    /* renamed from: B */
    public static boolean m26961B(String str) {
        if (str == null) {
            return false;
        }
        try {
            C6951awq.class.getDeclaredField(str);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m26966b(String str, String str2, String str3) {
        return String.valueOf(f5529ov) + C0147Bi.SEPARATOR + m26963a(str, str2, str3);
    }

    /* renamed from: c */
    public static String m26967c(String str, String str2) {
        return String.valueOf(f5529ov) + C0147Bi.SEPARATOR + m26965b(str, str2);
    }

    /* renamed from: a */
    public static String m26962a(String str, int i, String str2) {
        if (!m26961B(str2)) {
            throw new RuntimeException("default image " + str2 + " in imageset " + f5529ov);
        }
        try {
            Field declaredField = C6951awq.class.getDeclaredField(str);
            if (!declaredField.getType().equals(String[].class)) {
                return String.valueOf(f5529ov) + C0147Bi.SEPARATOR + str2;
            }
            return String.valueOf(f5529ov) + C0147Bi.SEPARATOR + m26965b(((String[]) declaredField.get((Object) null))[i], str2);
        } catch (NoSuchFieldException e) {
            return String.valueOf(f5529ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalArgumentException e2) {
            return String.valueOf(f5529ov) + C0147Bi.SEPARATOR + str2;
        } catch (IllegalAccessException e3) {
            return String.valueOf(f5529ov) + C0147Bi.SEPARATOR + str2;
        }
    }

    /* renamed from: a */
    public static String[] m26964a(String[] strArr, String str) {
        String[] strArr2 = new String[strArr.length];
        if (!m26961B(str)) {
            throw new RuntimeException("default image " + str + " in imageset " + f5529ov);
        }
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = String.valueOf(f5529ov) + C0147Bi.SEPARATOR + m26965b(strArr[i], str);
        }
        return strArr2;
    }
}
