package logic.res;

import java.util.HashMap;
import java.util.Map;

/* renamed from: a.Kx */
/* compiled from: a */
public class KeyCodeName {

    /* renamed from: OC */
    public static final String f1014OC = "UNDEFINED";
    private static Map<Integer, String> dqb = new HashMap();
    private static Map<String, Integer> dqc = new HashMap();

    static {
        m6643a(0, "UNDEFINED");
        m6643a(8, "BACKSPACE");
        m6643a(9, "TAB");
        m6643a(12, "CLEAR");
        m6643a(13, "RETURN");
        m6643a(19, "PAUSE");
        m6643a(27, "ESCAPE");
        m6643a(32, "SPACE");
        m6643a(33, "EXCLAIM");
        m6643a(34, "QUOTEDBL");
        m6643a(35, "HASH");
        m6643a(36, "DOLLAR");
        m6643a(38, "AMPERSAND");
        m6643a(39, "QUOTE");
        m6643a(40, "LEFTPAREN");
        m6643a(41, "RIGHTPAREN");
        m6643a(42, "ASTERISK");
        m6643a(43, "PLUS");
        m6643a(44, "COMMA");
        m6643a(45, "MINUS");
        m6643a(46, "PERIOD");
        m6643a(47, "SLASH");
        m6643a(48, "0");
        m6643a(49, "1");
        m6643a(50, "2");
        m6643a(51, "3");
        m6643a(52, "4");
        m6643a(53, "5");
        m6643a(54, "6");
        m6643a(55, "7");
        m6643a(56, "8");
        m6643a(57, "9");
        m6643a(58, "COLON");
        m6643a(59, "SEMICOLON");
        m6643a(60, "LESS");
        m6643a(61, "EQUALS");
        m6643a(62, "GREATER");
        m6643a(63, "QUESTION");
        m6643a(64, "AT");
        m6643a(91, "LEFTBRACKET");
        m6643a(92, "BACKSLASH");
        m6643a(93, "RIGHTBRACKET");
        m6643a(94, "CARET");
        m6643a(95, "UNDERSCORE");
        m6643a(96, "BACKQUOTE");
        m6643a(97, "A");
        m6643a(98, "B");
        m6643a(99, "C");
        m6643a(100, "D");
        m6643a(101, "E");
        m6643a(102, "F");
        m6643a(103, "G");
        m6643a(104, "H");
        m6643a(105, "I");
        m6643a(106, "J");
        m6643a(107, "K");
        m6643a(108, "L");
        m6643a(109, "M");
        m6643a(110, "N");
        m6643a(111, "O");
        m6643a(112, "P");
        m6643a(113, "Q");
        m6643a(114, "R");
        m6643a(115, "S");
        m6643a(116, "T");
        m6643a(117, "U");
        m6643a(118, "V");
        m6643a(119, "W");
        m6643a(120, "X");
        m6643a(121, "Y");
        m6643a(122, "Z");
        m6643a(127, "DELETE");
        m6643a(160, "WORLD_0");
        m6643a(161, "WORLD_1");
        m6643a(162, "WORLD_2");
        m6643a(163, "WORLD_3");
        m6643a(164, "WORLD_4");
        m6643a(165, "WORLD_5");
        m6643a(166, "WORLD_6");
        m6643a(167, "WORLD_7");
        m6643a(168, "WORLD_8");
        m6643a(169, "WORLD_9");
        m6643a(170, "WORLD_10");
        m6643a(171, "WORLD_11");
        m6643a(172, "WORLD_12");
        m6643a(173, "WORLD_13");
        m6643a(174, "WORLD_14");
        m6643a(175, "WORLD_15");
        m6643a(176, "WORLD_16");
        m6643a(177, "WORLD_17");
        m6643a(178, "WORLD_18");
        m6643a(179, "WORLD_19");
        m6643a(180, "WORLD_20");
        m6643a(181, "WORLD_21");
        m6643a(182, "WORLD_22");
        m6643a(183, "WORLD_23");
        m6643a(184, "WORLD_24");
        m6643a(185, "WORLD_25");
        m6643a(KeyCode.cso, "WORLD_26");
        m6643a(187, "WORLD_27");
        m6643a(188, "WORLD_28");
        m6643a(189, "WORLD_29");
        m6643a(190, "WORLD_30");
        m6643a(191, "WORLD_31");
        m6643a(192, "WORLD_32");
        m6643a(193, "WORLD_33");
        m6643a(194, "WORLD_34");
        m6643a(195, "WORLD_35");
        m6643a(196, "WORLD_36");
        m6643a(197, "WORLD_37");
        m6643a(198, "WORLD_38");
        m6643a(199, "WORLD_39");
        m6643a(200, "WORLD_40");
        m6643a(201, "WORLD_41");
        m6643a(202, "WORLD_42");
        m6643a(KeyCode.csG, "WORLD_43");
        m6643a(KeyCode.csH, "WORLD_44");
        m6643a(KeyCode.csI, "WORLD_45");
        m6643a(KeyCode.csJ, "WORLD_46");
        m6643a(KeyCode.csK, "WORLD_47");
        m6643a(KeyCode.csL, "WORLD_48");
        m6643a(KeyCode.csM, "WORLD_49");
        m6643a(KeyCode.csN, "WORLD_50");
        m6643a(KeyCode.csO, "WORLD_51");
        m6643a(KeyCode.csP, "WORLD_52");
        m6643a(KeyCode.csQ, "WORLD_53");
        m6643a(KeyCode.csR, "WORLD_54");
        m6643a(KeyCode.csS, "WORLD_55");
        m6643a(KeyCode.csT, "WORLD_56");
        m6643a(KeyCode.csU, "WORLD_57");
        m6643a(KeyCode.csV, "WORLD_58");
        m6643a(KeyCode.csW, "WORLD_59");
        m6643a(KeyCode.csX, "WORLD_60");
        m6643a(KeyCode.csY, "WORLD_61");
        m6643a(KeyCode.csZ, "WORLD_62");
        m6643a(KeyCode.cta, "WORLD_63");
        m6643a(KeyCode.ctb, "WORLD_64");
        m6643a(KeyCode.ctc, "WORLD_65");
        m6643a(KeyCode.ctd, "WORLD_66");
        m6643a(KeyCode.cte, "WORLD_67");
        m6643a(KeyCode.ctf, "WORLD_68");
        m6643a(KeyCode.ctg, "WORLD_69");
        m6643a(KeyCode.cth, "WORLD_70");
        m6643a(KeyCode.cti, "WORLD_71");
        m6643a(KeyCode.ctj, "WORLD_72");
        m6643a(KeyCode.ctk, "WORLD_73");
        m6643a(KeyCode.ctl, "WORLD_74");
        m6643a(KeyCode.ctm, "WORLD_75");
        m6643a(KeyCode.ctn, "WORLD_76");
        m6643a(KeyCode.cto, "WORLD_77");
        m6643a(KeyCode.ctp, "WORLD_78");
        m6643a(KeyCode.ctq, "WORLD_79");
        m6643a(240, "WORLD_80");
        m6643a(KeyCode.cts, "WORLD_81");
        m6643a(242, "WORLD_82");
        m6643a(243, "WORLD_83");
        m6643a(244, "WORLD_84");
        m6643a(KeyCode.ctw, "WORLD_85");
        m6643a(KeyCode.ctx, "WORLD_86");
        m6643a(KeyCode.cty, "WORLD_87");
        m6643a(KeyCode.ctz, "WORLD_88");
        m6643a(KeyCode.ctA, "WORLD_89");
        m6643a(250, "WORLD_90");
        m6643a(KeyCode.ctC, "WORLD_91");
        m6643a(KeyCode.ctD, "WORLD_92");
        m6643a(KeyCode.ctE, "WORLD_93");
        m6643a(254, "WORLD_94");
        m6643a(255, "WORLD_95");
        m6643a(256, "KP_0");
        m6643a(KeyCode.ctI, "KP_1");
        m6643a(KeyCode.ctJ, "KP_2");
        m6643a(KeyCode.ctK, "KP_3");
        m6643a(KeyCode.ctL, "KP_4");
        m6643a(KeyCode.ctM, "KP_5");
        m6643a(KeyCode.ctN, "KP_6");
        m6643a(KeyCode.ctO, "KP_7");
        m6643a(KeyCode.ctP, "KP_8");
        m6643a(KeyCode.ctQ, "KP_9");
        m6643a(KeyCode.ctR, "KP_PERIOD");
        m6643a(KeyCode.ctS, "KP_DIVIDE");
        m6643a(KeyCode.ctT, "KP_MULTIPLY");
        m6643a(KeyCode.ctU, "KP_MINUS");
        m6643a(KeyCode.ctV, "KP_PLUS");
        m6643a(KeyCode.ctW, "KP_ENTER");
        m6643a(KeyCode.ctX, "KP_EQUALS");
        m6643a(KeyCode.f209UP, "UP");
        m6643a(KeyCode.DOWN, "DOWN");
        m6643a(KeyCode.RIGHT, "RIGHT");
        m6643a(KeyCode.LEFT, "LEFT");
        m6643a(KeyCode.INSERT, "INSERT");
        m6643a(KeyCode.HOME, "HOME");
        m6643a(KeyCode.END, "END");
        m6643a(KeyCode.ctY, "PAGEUP");
        m6643a(KeyCode.ctZ, "PAGEDOWN");
        m6643a(KeyCode.f200F1, "F1");
        m6643a(KeyCode.f201F2, "F2");
        m6643a(KeyCode.f202F3, "F3");
        m6643a(KeyCode.f203F4, "F4");
        m6643a(KeyCode.f204F5, "F5");
        m6643a(KeyCode.f205F6, "F6");
        m6643a(KeyCode.f206F7, "F7");
        m6643a(KeyCode.f207F8, "F8");
        m6643a(KeyCode.f208F9, "F9");
        m6643a(KeyCode.F10, "F10");
        m6643a(KeyCode.F11, "F11");
        m6643a(KeyCode.F12, "F12");
        m6643a(KeyCode.F13, "F13");
        m6643a(KeyCode.F14, "F14");
        m6643a(KeyCode.F15, "F15");
        m6643a(KeyCode.cua, "NUMLOCK");
        m6643a(KeyCode.cub, "CAPSLOCK");
        m6643a(KeyCode.cuc, "SCROLLOCK");
        m6643a(KeyCode.RSHIFT, "RSHIFT");
        m6643a(KeyCode.LSHIFT, "LSHIFT");
        m6643a(KeyCode.cud, "RCTRL");
        m6643a(KeyCode.cue, "LCTRL");
        m6643a(KeyCode.cuf, "RALT");
        m6643a(KeyCode.cug, "LALT");
        m6643a(KeyCode.cuh, "RMETA");
        m6643a(KeyCode.cui, "LMETA");
        m6643a(KeyCode.cuj, "LSUPER");
        m6643a(KeyCode.cuk, "RSUPER");
        m6643a(KeyCode.cul, "MODE");
        m6643a(KeyCode.cum, "COMPOSE");
        m6643a(KeyCode.HELP, "HELP");
        m6643a(KeyCode.cun, "PRINT");
        m6643a(KeyCode.cuo, "SYSREQ");
        m6643a(KeyCode.BREAK, "BREAK");
        m6643a(KeyCode.MENU, "MENU");
        m6643a(KeyCode.cup, "POWER");
        m6643a(KeyCode.cuq, "EURO");
        m6643a(KeyCode.cur, "UNDO");
        m6643a(64, "LCTRL");
        m6643a(128, "RCTRL");
        m6643a(1, "LSHIFT");
        m6643a(2, "RSHIFT");
        m6643a(256, "LALT");
        m6643a(512, "RALT");
        m6643a(KeyCode.cuB, "ALT");
        m6643a(3, "SHIFT");
    }

    /* renamed from: a */
    private static void m6643a(int i, String str) {
        dqb.put(Integer.valueOf(i), str);
        dqc.put(str, Integer.valueOf(i));
    }

    /* renamed from: eF */
    public static int m6644eF(String str) {
        Integer num = dqc.get(str);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    /* renamed from: ls */
    public static String m6645ls(int i) {
        String str = dqb.get(Integer.valueOf(i));
        return str != null ? str : "UNDEFINED";
    }
}
