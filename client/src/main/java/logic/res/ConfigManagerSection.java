package logic.res;

import game.engine.C5452aJw;
import gnu.trove.THashMap;
import p001a.C6130agy;
import p001a.ayQ;

import java.util.Map;
import java.util.Set;

/* renamed from: a.YR */
/* compiled from: a */
public class ConfigManagerSection {
    private Map<String, ayQ> eMT;

    public ConfigManagerSection() {
        this.eMT = new THashMap();
    }

    public ConfigManagerSection(Map<String, ayQ> map) {
        this.eMT = map;
    }

    /* renamed from: bHT */
    public ConfigManagerSection clone() {
        ConfigManagerSection yr = new ConfigManagerSection();
        for (Map.Entry next : this.eMT.entrySet()) {
            yr.eMT.put((String) next.getKey(), (ayQ) ((ayQ) next.getValue()).clone());
        }
        return yr;
    }

    public Map<String, ayQ> getOptions() {
        return this.eMT;
    }

    /* renamed from: e */
    private Object m11820e(String str, Class<?> cls) {
        ayQ ayq = this.eMT.get(str);
        if (ayq == null) {
            throw new C5452aJw(str);
        } else if (ayq.getType() == cls) {
            return ayq.getValue();
        } else {
            throw new C6130agy(ayq.getType(), cls, str);
        }
    }

    /* renamed from: a */
    private Object m11818a(String str, Class<?> cls, Object obj) {
        ayQ ayq = this.eMT.get(str);
        return (ayq == null || ayq.getType() != cls) ? obj : ayq.getValue();
    }

    /* renamed from: b */
    private void m11819b(String str, Class<?> cls, Object obj) {
        ayQ ayq = this.eMT.get(str);
        ayQ ayq2 = new ayQ(cls, obj);
        if (ayq == null || !ayq.equals(ayq2)) {
            this.eMT.put(str, ayq2);
        }
    }

    /* renamed from: hH */
    public String mo7214hH(String str) {
        return (String) m11820e(str, String.class);
    }

    /* renamed from: a */
    public String mo7197a(ConfigIniKeyValue ahy) {
        return (String) m11820e(ahy.getKey(), String.class);
    }

    /* renamed from: W */
    public String mo7189W(String str, String str2) {
        return (String) m11818a(str, String.class, str2);
    }

    /* renamed from: a */
    public String getValuePropertyString(ConfigIniKeyValue ahy, String str) {
        return (String) m11818a(ahy.getKey(), String.class, str);
    }

    /* renamed from: X */
    public void mo7190X(String str, String str2) {
        m11819b(str, String.class, str2);
    }

    /* renamed from: b */
    public void mo7203b(ConfigIniKeyValue ahy, String str) {
        m11819b(ahy.getKey(), String.class, str);
    }

    /* renamed from: hI */
    public Integer mo7215hI(String str) {
        return (Integer) m11820e(str, Integer.class);
    }

    /* renamed from: b */
    public int mo7199b(ConfigIniKeyValue ahy) {
        return ((Integer) m11820e(ahy.getKey(), Integer.class)).intValue();
    }

    /* renamed from: a */
    public Integer mo7196a(String str, Integer num) {
        return (Integer) m11818a(str, Integer.class, num);
    }

    /* renamed from: a */
    public Integer mo7195a(ConfigIniKeyValue ahy, Integer num) {
        return (Integer) m11818a(ahy.getKey(), Integer.class, num);
    }

    /* renamed from: b */
    public void mo7206b(String str, Integer num) {
        m11819b(str, Integer.class, num);
    }

    /* renamed from: b */
    public void mo7202b(ConfigIniKeyValue ahy, Integer num) {
        m11819b(ahy.getKey(), Integer.class, num);
    }

    /* renamed from: hJ */
    public Float mo7216hJ(String str) {
        return (Float) m11820e(str, Float.class);
    }

    /* renamed from: c */
    public Float mo7209c(ConfigIniKeyValue ahy) {
        return (Float) m11820e(ahy.getKey(), Float.class);
    }

    /* renamed from: a */
    public Float mo7194a(String str, Float f) {
        return (Float) m11818a(str, Float.class, f);
    }

    /* renamed from: a */
    public Float mo7193a(ConfigIniKeyValue ahy, Float f) {
        return (Float) m11818a(ahy.getKey(), Float.class, f);
    }

    /* renamed from: b */
    public void mo7205b(String str, Float f) {
        m11819b(str, Float.class, f);
    }

    /* renamed from: b */
    public void mo7201b(ConfigIniKeyValue ahy, Float f) {
        m11819b(ahy.getKey(), Float.class, f);
    }

    /* renamed from: hK */
    public Boolean mo7217hK(String str) {
        return (Boolean) m11820e(str, Boolean.class);
    }

    /* renamed from: d */
    public Boolean mo7211d(ConfigIniKeyValue ahy) {
        return (Boolean) m11820e(ahy.getKey(), Boolean.class);
    }

    /* renamed from: a */
    public Boolean mo7192a(String str, Boolean bool) {
        return (Boolean) m11818a(str, Boolean.class, bool);
    }

    /* renamed from: a */
    public Boolean mo7191a(ConfigIniKeyValue ahy, Boolean bool) {
        return (Boolean) m11818a(ahy.getKey(), Boolean.class, bool);
    }

    /* renamed from: b */
    public void mo7204b(String str, Boolean bool) {
        m11819b(str, Boolean.class, bool);
    }

    /* renamed from: b */
    public void mo7200b(ConfigIniKeyValue ahy, Boolean bool) {
        m11819b(ahy.getKey(), Boolean.class, bool);
    }

    public Set<String> getKeys() {
        return this.eMT.keySet();
    }

    public void bHU() {
        for (Map.Entry<String, ayQ> value : this.eMT.entrySet()) {
            ((ayQ) value.getValue()).setDirty(false);
        }
    }
}
