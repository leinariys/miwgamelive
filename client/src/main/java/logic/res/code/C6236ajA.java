package logic.res.code;

import com.hoplon.taikodom.common.TaikodomVersion;
import game.network.build.BuilderSocketStatic;
import game.network.build.IServerConnect;
import game.network.build.ManagerMessageTransportImpl;
import game.network.channel.NetworkChannel;
import game.network.exception.C1728ZX;
import game.network.exception.ClientAlreadyConnectedException;
import game.network.message.C5581aOv;
import game.network.message.externalizable.C2592hH;
import game.script.Taikodom;
import game.script.login.UserConnection;
import game.script.player.Player;
import game.script.ship.Ship;
import logic.C0886Mr;
import logic.res.LoaderTrail;
import logic.res.html.C6663arL;
import logic.thred.LogPrinter;
import org.mozilla1.classfile.C0147Bi;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.render.loader.provider.FilePath;
import util.C1646YF;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.concurrent.Executor;

/* renamed from: a.ajA  reason: case insensitive filesystem */
/* compiled from: a */
public class C6236ajA implements Executor {
    static LogPrinter logger = LogPrinter.m10275K(C6236ajA.class);
    /* access modifiers changed from: private */

    /* renamed from: Aa */
    public org.mozilla1.javascript.Scriptable f4677Aa;
    /* renamed from: zZ */
    public org.mozilla1.javascript.Context f4682zZ;
    /* renamed from: Ab */
    private long f4678Ab = 0;
    /* renamed from: Ac */
    private Reader f4679Ac;
    /* renamed from: Ae */
    private Runnable f4680Ae;
    private Taikodom bFZ;
    private UserConnection dzV;
    private NetworkChannel fEJ;
    private ManagerMessageTransportImpl gLj;
    private DataGameEventImpl gLk;
    private C0886Mr gLl;
    private Ship gLm;
    private Runnable gLn;
    private C6224aio gLo;
    /* access modifiers changed from: private */
    /* renamed from: zY */
    private boolean f4681zY = false;

    public C6236ajA(String str, String str2, LoaderTrail glVar, SoftTimer abg, FilePath ain, FilePath ain2) throws ClientAlreadyConnectedException {
        this.gLl = new C0886Mr(glVar, abg, ain2, ain);
        login(str, str2);
    }

    public C6236ajA(DataGameEventImpl arVar, String str, String str2, LoaderTrail glVar, SoftTimer abg, FilePath ain, FilePath ain2) throws ClientAlreadyConnectedException {
        this.gLl = new C0886Mr(glVar, abg, ain2, ain);
        login(str, str2);
        this.gLj = new ManagerMessageTransportImpl(arVar, this.fEJ);
        this.gLk = arVar;
        cAG();
        m22747b(arVar);
    }

    private void login(String str, String str2) throws ClientAlreadyConnectedException {
        try {
            String property = System.getProperty("main.server", IServerConnect.DEFAULT_HOST);
            logger.info("bot login: username/password: " + str + C0147Bi.SEPARATOR + str2);
            this.fEJ = BuilderSocketStatic.m21641a(property, IServerConnect.PORT_LOGIN, str, str2, false, TaikodomVersion.VERSION);
        } catch (ClientAlreadyConnectedException e) {
            throw e;
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }

    public ManagerMessageTransportImpl cAF() {
        return this.gLj;
    }

    public UserConnection aMl() {
        return this.dzV;
    }

    public NetworkChannel aPT() {
        return this.fEJ;
    }

    /* renamed from: a */
    public void mo13882a(DataGameEventImpl arVar) {
        this.gLj = arVar.bGU();
        arVar.mo3362ac(this.gLl);
        m22747b(arVar);
    }

    /* renamed from: b */
    private void m22747b(DataGameEventImpl arVar) {
        this.gLk = arVar;
        this.bFZ = arVar.ala();
        try {
            this.dzV = (UserConnection) ((C0755Kn) ((C3582se) this.bFZ.bFf()).mo21980c((C5581aOv) new C2592hH(this.bFZ))).bcB();
            this.gLl.mo4087c(this.dzV);
            try {
                this.bFZ.aMa().mo11175F(this.dzV.getUser());
                this.dzV.mo15383bL((Player) this.dzV.getUser().cSQ().get(0));
            } catch (C0849MO e) {
                throw new RuntimeException(e);
            }
        } catch (C1728ZX e2) {
            throw new RuntimeException(e2);
        }
    }

    public boolean step(float f) {
        Runnable dwp;
        cAG();
        this.gLk.mo3366b(f, (Executor) this);
        if (this.f4681zY) {
            try {
                if (!(this.gLo == null || (dwp = this.gLo.dwp()) == null)) {
                    this.gLn = dwp;
                    cAH();
                }
                if (this.f4679Ac != null) {
                    m22749kg();
                    try {
                        m22748b(this.f4679Ac);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                    this.f4679Ac = null;
                }
                if (this.f4678Ab != 0 && this.f4678Ab <= System.currentTimeMillis()) {
                    cAH();
                }
                if (!(this.gLm == null || this.gLm.cLJ() == null)) {
                    logger.info("waking bot up coz actor '" + this.gLm + "' was spawned");
                    this.gLm = null;
                    cAH();
                }
            } catch (org.mozilla1.javascript.RhinoException e3) {
                e3.printStackTrace();
                mo13889kf();
            }
        }
        this.bFZ.aLW().mo11542gZ();
        if (this.gLo != null && this.gLo.dwq()) {
            this.f4681zY = true;
        }
        return this.f4681zY;
    }

    public void cAG() {
        C6663arL bFf;
        this.gLk.mo3362ac(this.gLl);
        this.gLk.mo3350a(this.gLj);
        DataGameEventImpl arVar = this.gLk;
        if (this.dzV == null) {
            bFf = null;
        } else {
            bFf = this.dzV.bFf();
        }
        arVar.mo3424c(bFf);
        C5916acs.setRootAndRenderPath(this.gLl);
    }

    public void execute(Runnable runnable) {
        runnable.run();
    }

    /* renamed from: a */
    public void mo13883a(Reader reader) {
        this.f4681zY = true;
        this.f4679Ac = reader;
    }

    /* renamed from: b */
    private void m22748b(Reader reader) {
        this.f4682zZ = new org.mozilla1.javascript.Context();
        org.mozilla1.javascript.Context.call(this.f4682zZ);
        this.f4682zZ.setOptimizationLevel(-2);
        this.f4677Aa = this.f4682zZ.initStandardObjects((org.mozilla1.javascript.ScriptableObject) null);
        this.gLo = new C6224aio(this.gLk, this.dzV.mo15388dL());
        this.f4677Aa.put("player", this.f4677Aa, (Object) this.gLo);
        this.f4677Aa.put("util", this.f4677Aa, (Object) new C1646YF());
        InputStreamReader inputStreamReader = new InputStreamReader(getClass().getResourceAsStream("bootstrap.js"));
        this.f4682zZ.evaluateReader(this.f4677Aa, (Reader) inputStreamReader, "bootstrap.js", 0, (Object) null);
        inputStreamReader.close();
        try {
            this.f4682zZ.evaluateReader(this.f4677Aa, reader, "<game.script>", 0, (Object) null);
        } catch (org.mozilla1.javascript.JavaScriptException e) {
            m22745a(e);
        }
    }

    private void cAH() {
        m22749kg();
        try {
            this.gLn.run();
        } catch (org.mozilla1.javascript.JavaScriptException e) {
            m22745a(e);
        }
    }

    /* renamed from: kg */
    private void m22749kg() {
        this.f4678Ab = 0;
        this.f4681zY = false;
        this.gLm = null;
        this.f4680Ae = null;
        if (this.f4677Aa != null) {
            this.f4677Aa.put("_x_sleep", this.f4677Aa, (Object) 0);
            this.f4677Aa.put("_x_waitForSpawn", this.f4677Aa, (Object) false);
        }
    }

    /* renamed from: a */
    private void m22745a(org.mozilla1.javascript.JavaScriptException dxVar) {
        String obj = dxVar.getValue().toString();
        if ("_x_cont".equals(obj)) {
            this.f4681zY = true;
            double parseDouble = Double.parseDouble(new StringBuilder().append(this.f4677Aa.get("_x_sleep", this.f4677Aa)).toString());
            if (parseDouble != ScriptRuntime.NaN) {
                this.f4678Ab = (long) (parseDouble + ((double) System.currentTimeMillis()));
            } else {
                this.f4678Ab = 0;
            }
            Boolean bool = (Boolean) this.f4677Aa.get("_x_waitForSpawn", this.f4677Aa);
            if (bool != null && bool.booleanValue()) {
                this.gLm = this.dzV.mo15388dL().bQx();
                logger.info("game.script stopped waiting for player spawn: " + this.gLm);
            }
            m22750kn("_x_cont()");
        } else if (obj.startsWith("_x_cont: ")) {
            String[] split = obj.split(":");
            if ("nextState".equals(split[1].trim())) {
                this.f4681zY = true;
                this.f4680Ae = this.gLo.mo13805gO(split[2].trim());
            }
        } else {
            dxVar.printStackTrace();
        }
    }

    /* renamed from: kn */
    private void m22750kn(String str) {
        this.gLn = new C1917a(str);
    }

    /* renamed from: kf */
    public void mo13889kf() {
        m22749kg();
        if (this.f4679Ac != null) {
            try {
                this.f4679Ac.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a.ajA$a */
    class C1917a implements Runnable {
        private final /* synthetic */ String fRs;

        C1917a(String str) {
            this.fRs = str;
        }

        public void run() {
            C6236ajA.this.f4682zZ.evaluateString(C6236ajA.this.f4677Aa, this.fRs, "<game.script>", 0, (Object) null);
        }
    }
}
