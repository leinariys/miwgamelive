package logic.res.code;

import gametoolkit.timer.module.InternalTimer;
import gametoolkit.timer.module.SoftTimerConsole;
import logic.WrapRunnable;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.util.*;
import java.util.concurrent.Executor;

/* renamed from: a.aBg  reason: case insensitive filesystem */
/* compiled from: a */
public class SoftTimer {
    /* access modifiers changed from: private */
    public Map<WrapRunnable, InternalTimer> listTask;
    private List<InternalTimer> hga;

    public SoftTimer() {
        this(false);
    }

    public SoftTimer(boolean z) {
        this.listTask = new HashMap();
        this.hga = new ArrayList();
        if (z) {
            mBeanServerRegistered();
        }
    }

    private void mBeanServerRegistered() {
        MBeanServer platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
        if (platformMBeanServer != null) {
            SoftTimerConsole softTimerConsole = new SoftTimerConsole(this);
            try {
                ObjectName objectName = new ObjectName("Bitverse:name=SoftTimer");
                if (!platformMBeanServer.isRegistered(objectName)) {
                    platformMBeanServer.registerMBean(softTimerConsole, objectName);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: jB */
    public void mo7965jB(long j) {
        synchronized (this.listTask) {
            Iterator<InternalTimer> it = this.listTask.values().iterator();
            while (it.hasNext()) {
                InternalTimer next = it.next();
                if (next.isCanceled()) {
                    it.remove();
                } else if (next.mo23540ih(j)) {
                    this.hga.add(next);
                } else if (next.isCanceled()) {
                    it.remove();
                }
            }
        }
        if (!this.hga.isEmpty()) {
            Iterator<InternalTimer> it2 = this.hga.iterator();
            while (it2.hasNext()) {
                InternalTimer next2 = it2.next();
                next2.run();
                if (next2.isCanceled()) {
                    it2.remove();
                }
            }
            this.hga.clear();
        }
    }

    /* renamed from: a */
    public void step(long j, Executor executor) {
        synchronized (this.listTask) {
            Iterator<InternalTimer> it = this.listTask.values().iterator();
            while (it.hasNext()) {
                InternalTimer next = it.next();
                if (next.isCanceled()) {
                    it.remove();
                } else if (next.mo23540ih(j)) {
                    this.hga.add(next);
                } else if (next.isCanceled()) {
                    it.remove();
                }
            }
        }
        if (!this.hga.isEmpty()) {
            Iterator<InternalTimer> it2 = this.hga.iterator();
            while (it2.hasNext()) {
                InternalTimer next2 = it2.next();
                if (next2.isCanceled()) {
                    it2.remove();
                } else {
                    executor.execute(next2);
                }
            }
            this.hga.clear();
        }
    }

    /* renamed from: a */
    public void mo7963a(WrapRunnable aen) {
        synchronized (this.listTask) {
            this.listTask.remove(aen);
        }
    }

    /* renamed from: b */
    public WrapRunnable addTask(String str, WrapRunnable aen, long j) {
        return mo7961a(str, aen, j, false);
    }

    /* renamed from: a */
    public WrapRunnable mo7961a(String str, WrapRunnable aen, long j, boolean z) {
        aen.setReset(false);
        aen.setCanceled(false);
        synchronized (this.listTask) {
            this.listTask.put(aen, new InternalTimer(this, str, aen, j, z, true));
        }
        return aen;
    }

    /* renamed from: a */
    public WrapRunnable mo7960a(String str, WrapRunnable aen, long j) {
        aen.setReset(false);
        aen.setCanceled(false);
        synchronized (this.listTask) {
            this.listTask.put(aen, new InternalTimer(this, str, aen, j, false, false));
        }
        return aen;
    }
}
