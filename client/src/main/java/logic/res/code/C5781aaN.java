package logic.res.code;

import logic.baa.C1616Xf;
import taikodom.infra.script.I18NString;
import util.Syst;

import java.lang.reflect.Field;

/* renamed from: a.aaN  reason: case insensitive filesystem */
/* compiled from: a */
public class C5781aaN extends C2807kO {


    public C5781aaN(Class<?> cls, Field field, String str, Class<?>[] clsArr) {
        super(cls, field, str, clsArr);
    }

    /* renamed from: b */
    public Object mo2188b(C1616Xf xf) {
        return new I18NString(String.valueOf(Syst.getClassName(getClass())) + "." + name());
    }

    /* renamed from: d */
    public Object mo11307d(Object obj, Object obj2) {
        return (obj == null || !obj.equals(obj2)) ? obj2 : obj;
    }
}
