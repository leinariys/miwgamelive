package logic.res.code;

import game.io.IReadExternal;
import game.io.IWriteExternal;
import game.network.message.externalizable.C5572aOm;
import game.network.message.serializable.C1556Wo;
import logic.baa.C1616Xf;
import logic.data.mbean.C0677Jd;
import logic.res.html.C0029AO;
import logic.res.html.DataClassSerializer;
import lombok.extern.slf4j.Slf4j;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import sun.misc.Unsafe;

import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.lang.reflect.Field;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: a.jd */
/* compiled from: a */
@Slf4j
public class C2759jd extends C1720ZS implements C5663aRz {

    private static final Unsafe unsafe = getUnsafe();
    private final Class<?> agV;
    private final boolean agW;
    private final boolean agX;
    private final boolean agY;
    private final Class<?>[] aha;
    private final boolean ahc;
    private final boolean ahf;
    private final Class<?> ahl;
    private C6346alG agZ;
    private C3248pc ahb;
    private DataClassSerializer ahd;
    private DataClassSerializer[] ahe;
    private boolean ahg;
    private C3122oB.C3123a ahh;
    private long ahi;
    private C1020Ot.C1021a ahj;
    private long ahk;
    private long ahm;
    private long ahn;
    private char aho;
    private boolean ahp;
    private long ahq;
    private boolean ahr;
    private AtomicLong ahs = new AtomicLong();
    private AtomicLong aht = new AtomicLong();
    private Object defaultValue = null;

    public C2759jd(Class<?> cls, Field field, String str, Class<?>[] clsArr) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7 = true;
        this.ahl = cls;
        this.aha = clsArr;
        this.eRF = ((C0064Am) field.getAnnotation(C0064Am.class)).aum();
        this.cUW = str;
        this.agV = field.getType();
        this.ahp = this.agV.isPrimitive();
        if (this.ahp) {
            if (this.agV == Integer.TYPE) {
                this.aho = 'I';
            } else if (this.agV == Long.TYPE) {
                this.aho = 'J';
            } else if (this.agV == Byte.TYPE) {
                this.aho = 'B';
            } else if (this.agV == Short.TYPE) {
                this.aho = 'S';
            } else if (this.agV == Character.TYPE) {
                this.aho = 'C';
            } else if (this.agV == Boolean.TYPE) {
                this.aho = 'Z';
            } else if (this.agV == Float.TYPE) {
                this.aho = 'F';
            } else if (this.agV == Double.TYPE) {
                this.aho = 'D';
            }
            field.getType().getCanonicalName();
        } else if (this.agV.isArray()) {
            this.aho = '[';
        } else {
            this.aho = 'L';
        }
        this.ahc = C5572aOm.class.isAssignableFrom(this.agV);
        if (!C1616Xf.class.isAssignableFrom(this.agV) || (this instanceof C6035afH)) {
            this.ahb = (C3248pc) field.getAnnotation(C3248pc.class);
            this.name = field.getName();
            this.eRL = field.getAnnotation(ClientOnly.class) != null;
            if (field.getAnnotation(C5256aCi.class) != null) {
                z = true;
            } else {
                z = false;
            }
            this.ahg = z;
            C2499fr frVar = (C2499fr) field.getAnnotation(C2499fr.class);
            this.eRM = frVar;
            if (frVar != null) {
                z2 = true;
            } else {
                z2 = false;
            }
            this.eRJ = z2;
            if (field.getAnnotation(C5566aOg.class) != null) {
                z3 = true;
            } else {
                z3 = false;
            }
            this.eRH = z3;
            if (field.getAnnotation(C1253SX.class) != null) {
                z4 = true;
            } else {
                z4 = false;
            }
            this.eRI = z4;
            if (field.getAnnotation(C4034yP.class) != null) {
                z5 = true;
            } else {
                z5 = false;
            }
            this.eRK = z5;
            this.agW = C0029AO.class.isAssignableFrom(field.getType());
            this.agX = C1556Wo.class.isAssignableFrom(field.getType());
            if (field.getAnnotation(C3990xn.class) != null) {
                z6 = true;
            } else {
                z6 = false;
            }
            this.agY = z6;
            this.agZ = (C6346alG) field.getAnnotation(C6346alG.class);
            C1020Ot ot = (C1020Ot) field.getAnnotation(C1020Ot.class);
            if (ot != null) {
                this.ahj = ot.drz();
                this.ahi = ot.mo4553Ur();
            } else {
                this.ahj = C1020Ot.C1021a.IMMEDIATE;
                this.ahk = Long.MAX_VALUE;
            }
            C3122oB oBVar = (C3122oB) field.getAnnotation(C3122oB.class);
            if (oBVar != null) {
                this.ahh = oBVar.mo20937Uq();
                this.ahi = oBVar.mo20938Ur();
            } else {
                this.ahh = C3122oB.C3123a.IMMEDIATE;
                this.ahi = Long.MAX_VALUE;
            }
            if (!this.eRL && !this.ahg && !this.eRI) {
                z7 = false;
            }
            this.ahf = z7;
            synchronized (eRE) {
                eRE.put(str, this);
            }
            if (!this.agV.isPrimitive()) {
                return;
            }
            if (this.agV == Integer.TYPE) {
                this.defaultValue = 0;
            } else if (this.agV == Float.TYPE) {
                this.defaultValue = Float.valueOf(0.0f);
            } else if (this.agV == Double.TYPE) {
                this.defaultValue = Double.valueOf(ScriptRuntime.NaN);
            } else if (this.agV == Long.TYPE) {
                this.defaultValue = 0L;
            } else if (this.agV == Byte.TYPE) {
                this.defaultValue = (byte) 0;
            } else if (this.agV == Character.TYPE) {
                this.defaultValue = 0;
            } else if (this.agV == Short.TYPE) {
                this.defaultValue = (short) 0;
            } else if (this.agV == Boolean.TYPE) {
                this.defaultValue = Boolean.FALSE;
            }
        } else {
            throw new IllegalArgumentException("Should use ScriptField to describe this field");
        }
    }

    private static Unsafe getUnsafe() {
        try {
            Field declaredField = Unsafe.class.getDeclaredField("theUnsafe");
            declaredField.setAccessible(true);
            return (Unsafe) declaredField.get((Object) null);
        } catch (Exception e) {
            log.error("Could not find the sun.misc.Unsafe! The application will not work!", e);
            return null;
        }
    }

    /* renamed from: Ek */
    public boolean mo2179Ek() {
        return true;
    }

    /* renamed from: El */
    public Class<?> mo11291El() {
        return this.agV;
    }

    /* renamed from: Em */
    public boolean mo19967Em() {
        return this.agY;
    }

    public boolean isCollection() {
        return this.agW;
    }

    /* renamed from: En */
    public boolean mo11292En() {
        return this.agX;
    }

    public Object getDefaultValue() {
        return this.defaultValue;
    }

    /* renamed from: A */
    public void mo11289A(Object obj) {
        this.defaultValue = obj;
    }

    public String toString() {
        return String.valueOf(name()) + "[" + mo7395hq() + "]";
    }

    /* renamed from: Eo */
    public C6346alG mo11293Eo() {
        return this.agZ;
    }

    /* renamed from: Ep */
    public boolean mo11294Ep() {
        return this.agZ != null;
    }

    /* renamed from: Eq */
    public Class<?>[] mo11295Eq() {
        return this.aha;
    }

    /* renamed from: Er */
    public C3248pc mo11296Er() {
        return this.ahb;
    }

    /* renamed from: Es */
    public boolean mo11297Es() {
        return this.ahb != null;
    }

    public Object writeReplace() {
        return super.writeReplace();
    }

    /* renamed from: a */
    public void mo2187a(C1616Xf xf, IWriteExternal att, Object obj) {
        mo19962EE().serializeData(att, obj);
    }

    /* renamed from: a */
    public Object mo2186a(C1616Xf xf, IReadExternal vm) {
        try {
            return mo19962EE().mo2357a(vm);
        } catch (Exception e) {
            throw new CountFailedReadOrWriteException("Error deserializing field " + this, e);
        }
    }

    /* renamed from: a */
    public void mo11305a(C1616Xf xf, ObjectOutput objectOutput, Object obj) {
        try {
            mo19962EE().serializeData(objectOutput, obj);
        } catch (Exception e) {
            throw new CountFailedReadOrWriteException("Error serializing field " + this, e);
        }
    }

    /* renamed from: a */
    public Object mo11304a(C1616Xf xf, ObjectInput objectInput) {
        return mo19962EE().inputReadObject(objectInput);
    }

    /* renamed from: z */
    public Object mo2181z(Object obj) {
        if (obj == null) {
            return null;
        }
        if (this.ahc) {
            return ((C5572aOm) obj).mo8390Jq();
        }
        if (obj instanceof C1616Xf) {
            return obj;
        }
        if (obj instanceof C5572aOm) {
            return ((C5572aOm) obj).mo8390Jq();
        }
        throw new RuntimeException(new CloneNotSupportedException("At field " + this.ahl.getName() + "." + name() + " value is " + obj.getClass().getName()));
    }

    /* renamed from: a */
    public Object mo2185a(C0677Jd jd, Object obj) {
        return mo2181z(obj);
    }

    /* renamed from: yl */
    public boolean mo2180yl() {
        return this.ahc;
    }

    /* renamed from: b */
    public Object mo2188b(C1616Xf xf) {
        return this.defaultValue;
    }

    /* renamed from: Et */
    public C5546aNm mo19968Et() {
        return null;
    }

    /* renamed from: c */
    public C5546aNm mo11306c(Object obj, Object obj2) {
        return null;
    }

    /* renamed from: Eu */
    public DataClassSerializer[] mo11298Eu() {
        if (!this.ahr) {
            if (this.aha != null) {
                DataClassSerializer[] kdArr = new DataClassSerializer[this.aha.length];
                for (int i = 0; i < this.aha.length; i++) {
                    kdArr[i] = C5726aUk.m18699b(this.aha[i], (Class<?>[]) null);
                }
                this.ahe = kdArr;
            }
            this.ahr = true;
        }
        return this.ahe;
    }

    /* renamed from: d */
    public Object mo11307d(Object obj, Object obj2) {
        return obj2;
    }

    /* renamed from: Ev */
    public boolean mo11299Ev() {
        return this.ahf;
    }

    /* renamed from: Ew */
    public C1020Ot.C1021a mo11300Ew() {
        return this.ahj;
    }

    /* renamed from: Ex */
    public long mo11301Ex() {
        return this.ahk;
    }

    /* renamed from: Ey */
    public C3122oB.C3123a mo11302Ey() {
        return this.ahh;
    }

    /* renamed from: Ez */
    public long mo11303Ez() {
        return this.ahi;
    }

    /* renamed from: EA */
    public long mo19959EA() {
        return this.ahm;
    }

    /* renamed from: EB */
    public long mo19960EB() {
        return this.ahq;
    }

    /* renamed from: EC */
    public long mo19961EC() {
        return this.ahn;
    }

    /* renamed from: ED */
    public char mo11290ED() {
        return this.aho;
    }

    public boolean isPrimitive() {
        return this.ahp;
    }

    /* renamed from: a */
    public void mo19969a(Field field) {
        this.ahm = unsafe.objectFieldOffset(field);
    }

    /* renamed from: b */
    public void mo19970b(Field field) {
        this.ahn = unsafe.objectFieldOffset(field);
    }

    /* renamed from: c */
    public void mo19971c(Field field) {
        this.ahq = unsafe.objectFieldOffset(field);
    }

    /* renamed from: EE */
    public DataClassSerializer mo19962EE() {
        if (this.ahd == null) {
            this.ahd = C5726aUk.m18699b(this.agV, this.aha);
        }
        return this.ahd;
    }

    /* renamed from: EF */
    public long mo19963EF() {
        return this.ahs.get();
    }

    /* renamed from: EG */
    public void mo19964EG() {
        this.ahs.incrementAndGet();
    }

    /* renamed from: EH */
    public long mo19965EH() {
        return this.aht.get();
    }

    /* renamed from: EI */
    public void mo19966EI() {
        this.aht.incrementAndGet();
    }
}
