package logic.res.code;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import gnu.trove.THashMap;
import logic.baa.C1616Xf;
import logic.res.html.C0029AO;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.lang.reflect.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: a.aRc  reason: case insensitive filesystem */
/* compiled from: a */
public class C5640aRc {
    private static THashMap<Class<?>, Constructor<? extends C5663aRz>> iGv = new THashMap<>();
    private static ConcurrentHashMap<String, Field> iGw = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, C5663aRz> iGx = new ConcurrentHashMap<>();

    static {
        try {
            m17845d(UUID.class, C1537Wa.class);
            m17845d(String.class, C1537Wa.class);
            m17845d(String.class, C1537Wa.class);
            m17845d(Quat4fWrap.class, C1537Wa.class);
            m17845d(Class.class, C1537Wa.class);
            m17845d(Vec3f.class, C6846aum.class);
            m17845d(Vec3d.class, C6175ahr.class);
            m17845d(C3438ra.class, C0412Fl.class);
            m17845d(C0029AO.class, C3607sr.class);
            m17845d(C2686iZ.class, C0538HZ.class);
            m17845d(C1556Wo.class, C6671arT.class);
            m17845d(I18NString.class, C5781aaN.class);
            m17845d(Boolean.TYPE, C1537Wa.class);
            m17845d(Integer.TYPE, C1537Wa.class);
            m17845d(Integer.TYPE, C1537Wa.class);
            m17845d(Double.TYPE, C1537Wa.class);
            m17845d(Long.TYPE, C1537Wa.class);
            m17845d(Float.TYPE, C1537Wa.class);
            m17845d(Byte.TYPE, C1537Wa.class);
            m17845d(Short.TYPE, C1537Wa.class);
            m17845d(Character.TYPE, C1537Wa.class);
            m17845d(Boolean.class, C1537Wa.class);
            m17845d(Integer.class, C1537Wa.class);
            m17845d(Integer.class, C1537Wa.class);
            m17845d(Double.class, C1537Wa.class);
            m17845d(Long.class, C1537Wa.class);
            m17845d(Float.class, C1537Wa.class);
            m17845d(Byte.class, C1537Wa.class);
            m17845d(Short.class, C1537Wa.class);
            m17845d(Character.class, C1537Wa.class);
        } catch (SecurityException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e2) {
            e2.printStackTrace();
            throw new RuntimeException(e2);
        }
    }

    public static Set<C5663aRz> dre() {
        return new HashSet(iGx.values());
    }

    /* renamed from: ns */
    public static C5663aRz m17847ns(String str) {
        return iGx.get(str);
    }

    /* renamed from: b */
    public static C5663aRz m17844b(Class<?> cls, String str, int i) {
        C5663aRz arz = iGx.get(str);
        if (arz != null) {
            return arz;
        }
        if (i >= 0) {
            C2759jd jdVar = (C2759jd) m17846m(cls, str);
            jdVar.mo7398pq(i);
            iGx.put(str, jdVar);
            return jdVar;
        }
        for (Field field : cls.getDeclaredFields()) {
            if (C5663aRz.class.isAssignableFrom(field.getType())) {
                field.setAccessible(true);
                try {
                    C5663aRz arz2 = (C5663aRz) field.get((Object) null);
                    if (str.equals(arz2.mo7396hr())) {
                        return arz2;
                    }
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e2) {
                    e2.printStackTrace();
                }
            }
        }
        throw new IllegalArgumentException("Method not found: " + cls + " " + str);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v4, resolved type: java.lang.Object[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static C5663aRz m17846m(java.lang.Class<?> paramClass, java.lang.String paramString) {
        Field localField = (Field) iGw.remove(paramString);
        C0064Am localAm;
        if (localField == null) {
            Field[] arrayOfField;
            int j = (arrayOfField = paramClass.getDeclaredFields()).length;
            for (int i = 0; i < j; i++) {
                Field localObject1 = arrayOfField[i];
                localAm = (C0064Am) ((Field) localObject1).getAnnotation(C0064Am.class);
                if (localAm != null) {
                    iGw.put(localAm.aul(), localObject1);
                }
            }
            localField = (Field) iGw.remove(paramString);
        }
        if (localField == null) {
            throw new RuntimeException("field not found: " + paramClass +
                    " - " + paramString);
        }
        Object localObject1 = (Class[]) null;
        if ((localField.getGenericType() instanceof ParameterizedType)) {
            ParameterizedType localObject2 = (ParameterizedType) localField.getGenericType();
            localObject3 = ((ParameterizedType) localObject2).getActualTypeArguments();
            localObject1 = new Class[localObject3.length];
            for (int k = 0; k < localObject1.length; k++) {
                localAm = localObject3[k];
                if ((localAm instanceof Class)) {
                    localObject1[k] = ((Class) localAm);
                } else {
                    Object localObject4;
                    if ((localAm instanceof TypeVariable)) {
                        localObject4 = ((TypeVariable) localAm).getBounds();
                        if ((localObject4 != null) && (localObject4.length > 0) &&
                                ((localObject4[0] instanceof Class))) {
                            localObject1[k] = ((Class) localObject4[0]);
                        }
                    } else if ((localAm instanceof ParameterizedType)) {
                        localObject4 = (ParameterizedType) localAm;
                        localObject1[k] = ((Class) ((ParameterizedType) localObject4).getRawType());
                    } else {
                        localObject1 = (Class[]) null;
                        break;
                    }
                }
                if (localObject1[k] == null) {
                    localObject1[k] = Object.class;
                }
            }
        }
        Object localObject2 = localField.getType();
        Object localObject3 = (Constructor) iGv.get(localObject2);
        if (localObject3 != null) {
            try {
                return (C5663aRz) ((Constructor) localObject3).newInstance(new Object[]{paramClass,
                        localField, paramString, localObject1});
            } catch (InstantiationException localInstantiationException) {
                throw new RuntimeException(localInstantiationException);
            } catch (IllegalAccessException localIllegalAccessException) {
                throw new RuntimeException(localIllegalAccessException);
            } catch (IllegalArgumentException localIllegalArgumentException) {
                throw new RuntimeException(localIllegalArgumentException);
            } catch (InvocationTargetException localInvocationTargetException) {
                throw new RuntimeException(localInvocationTargetException);
            }
        }
        if (String.class == localObject2) {
            return new C1537Wa(paramClass, localField, paramString,
                    (Class[]) localObject1);
        }
        if (C1616Xf.class.isAssignableFrom((Class) localObject2)) {
            return new C6035afH(paramClass,
                    localField, paramString, (Class[]) localObject1);
        }
        if (((Class) localObject2).isEnum()) {
            return new C0409Fi(paramClass,
                    localField, paramString, (Class[]) localObject1);
        }
        if (Cloneable.class.isAssignableFrom((Class) localObject2)) {
            return new C2807kO(paramClass, localField, paramString, (Class[]) localObject1);
        }
        return new C2759jd(paramClass, localField, paramString, (Class[]) localObject1);
    }


    /* renamed from: d */
    public static void m17845d(Class<?> cls, Class<? extends C5663aRz> cls2) throws NoSuchMethodException {
        iGv.put(cls, cls2.getConstructor(new Class[]{Class.class, Field.class, String.class, Class[].class}));
    }
}
