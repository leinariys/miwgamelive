package logic.res.code;

import logic.data.mbean.C0677Jd;
import logic.res.html.C2491fm;
import logic.res.html.MessageContainer;
import p001a.*;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: a.asE  reason: case insensitive filesystem */
/* compiled from: a */
public class C6708asE implements C6494any {
    private static HashMap<Class<?>, C6708asE> gvF = new HashMap<>();
    private final Class<?> agS;
    private final boolean eRH;
    private final boolean eRI;
    private final boolean eRL;
    private final C6147ahP[] gvA;
    private final boolean gvB;
    private final AtomicLong gvC = new AtomicLong();
    private final AtomicLong gvD = new AtomicLong();
    private final C5663aRz[] gvy;
    private final C2491fm[] gvz;
    private C1020Ot.C1021a ahj;
    private C5829abJ gvE;

    public C6708asE(Class<?> cls, Class<?> cls2, C5663aRz[] arzArr, C2491fm[] fmVarArr) {
        boolean z;
        boolean z2;
        C2491fm fmVar;
        boolean z3 = true;
        gvF.put(cls, this);
        this.agS = cls;
        this.eRH = cls.getAnnotation(C5566aOg.class) != null;
        if (cls.getAnnotation(C1253SX.class) != null) {
            z = true;
        } else {
            z = false;
        }
        this.eRI = z;
        if (cls.getAnnotation(ClientOnly.class) != null) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.eRL = z2;
        this.gvB = cls.getAnnotation(C0909NL.class) == null ? false : z3;
        this.gvE = (C5829abJ) cls.getAnnotation(C5829abJ.class);
        C1020Ot ot = (C1020Ot) cls.getAnnotation(C1020Ot.class);
        if (ot != null) {
            this.ahj = ot.drz();
        } else {
            this.ahj = C1020Ot.C1021a.IMMEDIATE;
        }
        this.gvy = arzArr;
        this.gvz = fmVarArr;
        Class<? super Object> cls3 = cls2;
        while (cls3 != null && cls3 != Object.class && cls3 != C0677Jd.class) {
            for (Field field : cls3.getDeclaredFields()) {
                C0064Am am = (C0064Am) field.getAnnotation(C0064Am.class);
                if (am != null) {
                    C2759jd jdVar = (C2759jd) C5640aRc.m17847ns(am.aul());
                    if (am.aum() == -2) {
                        jdVar.mo19971c(field);
                    } else if (am.aum() == -1) {
                        jdVar.mo19970b(field);
                    } else if (am.aum() >= 0) {
                        jdVar.mo19969a(field);
                    }
                }
            }
            cls3 = cls3.getSuperclass();
        }
        List dgK = MessageContainer.init();
        C2499fr frVar = (C2499fr) cls.getAnnotation(C2499fr.class);
        if (frVar != null) {
            for (String str : frVar.mo18855qf()) {
                synchronized (C1720ZS.eRE) {
                    fmVar = (C2491fm) C1720ZS.eRE.get(str);
                }
                if (fmVar != null) {
                    dgK.add(new C3384qv(fmVar));
                } else {
                    System.err.println("Warning: replication rule " + str + " not found for " + fmVar);
                }
            }
        }
        this.gvA = (C6147ahP[]) dgK.toArray(new C6147ahP[dgK.size()]);
    }

    /* renamed from: av */
    public static C6708asE m25631av(Class<?> cls) {
        return gvF.get(cls);
    }

    /* renamed from: c */
    public C5663aRz[] mo15889c() {
        return this.gvy;
    }

    public C2491fm[] clx() {
        return this.gvz;
    }

    public C6147ahP[] cuC() {
        return this.gvA;
    }

    /* renamed from: hk */
    public boolean mo15897hk() {
        return this.eRL | this.eRI;
    }

    /* renamed from: ho */
    public boolean mo15898ho() {
        return this.eRH;
    }

    /* renamed from: hp */
    public boolean mo15899hp() {
        return this.eRI;
    }

    /* renamed from: hD */
    public Class<?> mo15896hD() {
        return this.agS;
    }

    public String getName() {
        return this.agS.getName();
    }

    public boolean cly() {
        return this.gvB;
    }

    public C6147ahP[] clz() {
        return this.gvA;
    }

    /* renamed from: Ew */
    public C1020Ot.C1021a mo15115Ew() {
        return this.ahj;
    }

    public String getVersion() {
        return this.gvE != null ? this.gvE.value() : "";
    }

    public void cuD() {
        this.gvC.incrementAndGet();
    }

    public void cuE() {
        this.gvC.incrementAndGet();
    }

    public long getPullCount() {
        return this.gvC.get();
    }

    public long cuF() {
        return this.gvD.get();
    }
}
