package logic.res.code;

import logic.thred.LogPrinter;
import util.Syst;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/* renamed from: a.kO */
/* compiled from: a */
public class C2807kO extends C2759jd {
    private static final Class<?>[] avv = new Class[0];
    private static final Object[] avw = new Object[0];

    static Method avx = null;
    static LogPrinter logger = LogPrinter.m10275K(C2807kO.class);

    static {
        try {
            avx = Object.class.getDeclaredMethod("clone", avv);
            avx.setAccessible(true);
        } catch (Exception e) {
            logger.info("Should never happen!!!");
            e.printStackTrace();
        }
    }

    public C2807kO(Class<?> cls, Field field, String str, Class<?>[] clsArr) {
        super(cls, field, str, clsArr);
    }

    /* renamed from: z */
    public Object mo2181z(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            return avx.invoke(obj, avw);
        } catch (Exception e) {
            throw new RuntimeException("Error cloning field " + mo11291El() + "." + toString() + " value class " + obj.getClass() + " " + Syst.m15907f(obj));
        }
    }

    /* renamed from: yl */
    public boolean mo2180yl() {
        return true;
    }
}
