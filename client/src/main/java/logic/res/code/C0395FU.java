package logic.res.code;

/* renamed from: a.FU */
/* compiled from: a */
public class C0395FU {
    /* renamed from: eL */
    public static String m3226eL(long j) {
        return m3227er(new StringBuilder().append(j).toString());
    }

    /* renamed from: e */
    public static String m3225e(long j, int i) {
        return m3227er(String.format("%0" + i + "d", new Object[]{Long.valueOf(j)}));
    }

    /* renamed from: er */
    public static String m3227er(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt < '0' || charAt > '9') {
                sb.append(charAt);
            } else {
                sb.append((char) ((charAt - '0') + 65));
            }
        }
        return sb.toString();
    }
}
