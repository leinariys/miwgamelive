package logic.res.code;

import game.io.IReadExternal;
import game.io.IWriteExternal;
import logic.baa.C1616Xf;
import logic.data.mbean.C0677Jd;
import logic.res.html.C2194cc;
import logic.res.html.DataClassSerializer;
import p001a.*;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aRz  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5663aRz extends C2194cc {
    /* renamed from: A */
    void mo11289A(Object obj);

    /* renamed from: ED */
    char mo11290ED();

    /* renamed from: Ek */
    boolean mo2179Ek();

    /* renamed from: El */
    Class<?> mo11291El();

    /* renamed from: En */
    boolean mo11292En();

    /* renamed from: Eo */
    C6346alG mo11293Eo();

    /* renamed from: Ep */
    boolean mo11294Ep();

    /* renamed from: Eq */
    Class<?>[] mo11295Eq();

    /* renamed from: Er */
    C3248pc mo11296Er();

    /* renamed from: Es */
    boolean mo11297Es();

    /* renamed from: Eu */
    DataClassSerializer[] mo11298Eu();

    /* renamed from: Ev */
    boolean mo11299Ev();

    /* renamed from: Ew */
    C1020Ot.C1021a mo11300Ew();

    /* renamed from: Ex */
    long mo11301Ex();

    /* renamed from: Ey */
    C3122oB.C3123a mo11302Ey();

    /* renamed from: Ez */
    long mo11303Ez();

    /* renamed from: a */
    Object mo2185a(C0677Jd jd, Object obj);

    /* renamed from: a */
    Object mo2186a(C1616Xf xf, IReadExternal vm);

    /* renamed from: a */
    Object mo11304a(C1616Xf xf, ObjectInput objectInput);

    /* renamed from: a */
    void mo2187a(C1616Xf xf, IWriteExternal att, Object obj);

    /* renamed from: a */
    void mo11305a(C1616Xf xf, ObjectOutput objectOutput, Object obj);

    /* renamed from: b */
    Object mo2188b(C1616Xf xf);

    /* renamed from: c */
    C5546aNm mo11306c(Object obj, Object obj2);

    /* renamed from: d */
    Object mo11307d(Object obj, Object obj2);

    Object getDefaultValue();

    boolean isCollection();

    boolean isPrimitive();

    /* renamed from: yl */
    boolean mo2180yl();

    /* renamed from: z */
    Object mo2181z(Object obj);
}
