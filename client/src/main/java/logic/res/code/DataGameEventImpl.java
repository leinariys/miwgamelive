package logic.res.code;

import com.hoplon.geometry.Color;
import game.engine.DataGameEvent;
import game.engine.IEngineGame;
import game.network.WhoAmI;
import game.network.channel.NetworkChannel;
import game.network.manager.C1810aL;
import game.network.message.externalizable.*;
import game.script.PlayerSocialController;
import game.script.Taikodom;
import game.script.bank.Bank;
import game.script.mission.Mission;
import game.script.nls.NLSManager;
import game.script.player.Player;
import logic.aaa.*;
import logic.baa.C1616Xf;
import logic.baa.aDJ;
import logic.bbb.C2820ka;
import logic.bbb.C4029yK;
import logic.bbb.aDR;
import logic.data.link.C0060Ai;
import logic.data.link.C0471GX;
import logic.data.link.C2530gR;
import lombok.extern.slf4j.Slf4j;
import p001a.*;
import taikodom.geom.Orientation;
import taikodom.infra.script.ai.behaviours.AimQuality;

import java.io.IOException;
import java.net.InetAddress;

/* renamed from: a.ar */
/* compiled from: a */
@Slf4j
public class DataGameEventImpl extends DataGameEvent {
    private final C1966a enc;
    private C1810aL end;

    public DataGameEventImpl(C0390FP fp) {
        this(fp, (C1966a) null);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public DataGameEventImpl(p001a.C0390FP paramFP, DataGameEventImpl.C1966a parama) {
        super(paramFP, parama == null);
        this.enc = parama;
        if ((parama == null) && (bGU() != null)) {
            bGU().mo15544a(this.end);
        }
    }

    /* renamed from: a */
    public static final void m25331a(C6656arE are) {
        bxC();
        are.mo12009at(aDJ.class);
        are.mo12009at(C6483ann.class);
        are.mo12009at(C1506WA.class);
        are.mo12009at(C6128agw.C1890a.class);
        are.mo12009at(aME.class);
        are.mo12009at(C5260aCm.class);
        are.mo12009at(C1374U.C1375a.class);
        are.mo12009at(Mission.C0015a.class);
        are.mo12009at(NLSManager.C1472a.class);
        are.mo12009at(PlayerSocialController.C3362a.class);
        are.mo12009at(AimQuality.class);
        are.mo12009at(C0352Eg.class);
        are.mo12009at(C1506WA.class);
        are.mo12009at(C1374U.class);
        are.mo12009at(C1274Sq.class);
        are.mo12009at(C3829vV.class);
        are.mo12009at(C0352Eg.class);
        are.mo12009at(aME.class);
        are.mo12009at(C6788atg.class);
        are.mo12009at(aNJ.class);
        are.mo12009at(C6128agw.class);
        are.mo12009at(C2783jw.class);
        are.mo12009at(C0060Ai.class);
        are.mo12009at(C0471GX.C0472a.class);
        are.mo12009at(C2530gR.C2531a.class);
        are.mo12009at(Color.class);
        are.mo12009at(C5820abA.class);
        are.mo12009at(C2783jw.C2784a.class);
        are.mo12009at(C4129zt.class);
        are.mo12009at(Bank.C1861a.class);
        are.mo12009at(Orientation.class);
        are.mo12009at(C6753asx.class);
        are.mo12009at(C6384als.class);
        are.mo12009at(C0465GR.class);
        are.mo12009at(C6037afJ.class);
        are.mo12009at(C4031yM.class);
        are.mo12009at(C6729asZ.class);
        are.mo12009at(C5874acC.class);
        are.mo12009at(C3856vr.class);
    }

    public static void bxC() {
        C5726aUk.m18700e(Orientation.class, C1668YY.class);
        C5726aUk.m18700e(C2820ka.class, C3046nP.class);
        C5726aUk.m18700e(C4029yK.class, C5342aFq.class);
        C5726aUk.m18700e(C2820ka.class, C3046nP.class);
        try {
            C5640aRc.m17845d(C6853aut.class, C1537Wa.class);
            C5640aRc.m17845d(C3892wO.class, C6523aob.class);
            C5640aRc.m17845d(Orientation.class, C1537Wa.class);
            C5640aRc.m17845d(C4029yK.class, C1537Wa.class);
        } catch (SecurityException e) {
            throw new Error(e);
        } catch (NoSuchMethodException e2) {
            throw new Error(e2);
        }
    }

    public static void bxD() {
        try {
            aWd awd = new aWd();
            awd.mo12009at(Taikodom.class);
            Class.forName("taikodom.infra.script.ScriptDatabaseImpl").newInstance();
            m25331a((C6656arE) awd);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        } catch (ClassNotFoundException e3) {
            e3.printStackTrace();
        }
    }

    public void init() {
        super.init();
        if (!(this.enc == null || bGU() == null)) {
            bGU().mo15544a(this.end);
        }
        if (getWhoAmI() == WhoAmI.SERVER) {
            log.info("===============================");
            log.info("");
            log.info("Server ready.");
            log.info("");
            log.info("===============================");
            log.info("gc");
            System.gc();
        }
        log.info("Environment initied.");
        mo15692s("Environment setup done");
    }

    /* access modifiers changed from: protected */
    public void finalizingEnvironmentSetupAndRebuildingClassCache() {
        mo15692s("Finalizing environment setup");
        super.finalizingEnvironmentSetupAndRebuildingClassCache();
        mo15692s("Rebuilding class cache");
        m25331a(this.eKt);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void setupNetworkTransportLayer(NetworkChannel ahg) {
        mo15692s("Setup network transport layer");
        super.setupNetworkTransportLayer(ahg);
    }

    /* access modifiers changed from: protected */
    public <T extends C1616Xf> T resolvingTaikodomRootObject() {
        mo15692s("Resolving taikodom root object");
        return super.resolvingTaikodomRootObject();
    }

    /* access modifiers changed from: protected */
    public void initializingClient() {
        if (!this.eKw.aQb()) {
            mo15692s("Initializing client");
            log.info("Init client.");
            ((Taikodom) bGz().mo6901yn()).aMn();
            log.info("Client initied.");
            super.initializingClient();
        }
        InetAddress jh = this.eKw.aPT().getIpRemoteHost();
        if (jh != null) {
            RttMesThread rttThread = new RttMesThread(jh, 15230, (int) (15300.0d + (Math.random() * 100.0d)));
            try {
                rttThread.start();
            } catch (IOException e) {
            }
            setSynchronizerClientTime((CurrentTimeMilli) rttThread.getSynchronizerTime());
            log.info("Clock sync");
        }
    }

    public IEngineGame ald() {
        return (IEngineGame) aPW();
    }

    /* renamed from: a */
    public void mo15689a(String str, float f, float f2) {
        if (this.enc != null) {
            this.enc.mo15693a(str, f, f2);
        }
    }

    /* renamed from: s */
    public void mo15692s(String str) {
        if (this.enc != null) {
            this.enc.mo15694s(str);
        }
    }

    /* access modifiers changed from: protected */
    public void aMr() {
        ala().aMr();
    }

    public Taikodom ala() {
        return (Taikodom) bGz().mo6901yn();
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo3446h(aDR adr) {
        super.mo3446h(adr);
        ala().aLY().mo11044A(adr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: i */
    public void mo3448i(aDR adr) {
        ala().aLY().mo11045C(adr);
        super.mo3448i(adr);
    }

    /* renamed from: a.ar$a */
    public interface C1966a {
        /* renamed from: a */
        void mo15693a(String str, float f, float f2);

        /* renamed from: s */
        void mo15694s(String str);
    }

    /* renamed from: a.ar$b */
    /* compiled from: a */
    class RttMesThread extends RoundTripTime {
        RttMesThread(InetAddress inetAddresServer, int portServer, int portListener) {
            super(inetAddresServer, portServer, portListener);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void relayingMessages(MessageContainerRtt cVar) {
            Player player;
            super.relayingMessages(cVar);
            Taikodom taikodom = (Taikodom) DataGameEventImpl.this.getTaikodom();
            if (taikodom != null && (player = taikodom.getPlayer()) != null) {
                player.mo14338A("rtt", Long.valueOf(cVar.dnB()));
            }
        }
    }
}
