package logic.res.code;

import logic.thred.PoolThread;

/* renamed from: a.ahN  reason: case insensitive filesystem */
/* compiled from: a */
public class C6145ahN {
    private long fLE;
    private long fLF;
    private long last;

    public C6145ahN(float f) {
        setFps(f);
    }

    /* access modifiers changed from: package-private */
    public void setFps(float f) {
        this.fLF = (long) (1000.0f / f);
    }

    /* access modifiers changed from: package-private */
    public long caT() {
        this.fLE = System.currentTimeMillis();
        long j = (this.fLE - this.last) - this.fLF;
        this.last = this.fLE;
        long j2 = this.fLF - j;
        if (j2 > 0) {
            PoolThread.sleep(j2);
        }
        if (j > (-this.fLF)) {
            return this.fLF + j;
        }
        return 0;
    }
}
