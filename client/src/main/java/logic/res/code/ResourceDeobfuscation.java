package logic.res.code;

import p001a.*;

import java.io.IOException;

/* renamed from: a.SH */
/* compiled from: a */
public class ResourceDeobfuscation extends C6094agO implements C0390FP {
    private final C6781atZ djq;

    public ResourceDeobfuscation() {
        this((C6781atZ) null);
    }

    public ResourceDeobfuscation(C6781atZ atz) {
        this.djq = atz;
        mo13371eu(!C5877acF.fdO);
    }

    public C6280ajs ate() {
        C6280ajs ate = super.ate();
        C0433GA ga = (C0433GA) ate;
        if (ate == null) {
            C0433GA ga2 = new C0433GA(this.djq);
            try {
                ga2.mo2248d(getClass().getResource("/taikodom/infra/script/InfraScripts"));
                ga2.mo2248d(getClass().getResource("/taikodom/game/script/GameScripts"));
                super.mo13364b((C6280ajs) ga2);
                ga = ga2;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return ga;
    }
}
