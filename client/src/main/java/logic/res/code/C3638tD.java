package logic.res.code;

import com.hoplon.geometry.Vec3f;
import logic.render.IEngineGraphics;
import logic.res.ConfigManagerSection;
import p001a.C6296akI;
import taikodom.render.*;
import taikodom.render.camera.Camera;
import taikodom.render.gui.GuiScene;
import taikodom.render.helpers.RenderTask;
import taikodom.render.loader.FileSceneLoader;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.provider.FilePath;
import taikodom.render.scene.Scene;

import javax.media.opengl.GL;
import javax.media.opengl.GLContext;
import java.awt.*;
import java.util.List;

/* renamed from: a.tD */
/* compiled from: a */
public class C3638tD implements IEngineGraphics {
    /* renamed from: a */
    public void mo3007a(RenderTask renderTask) {
    }

    /* renamed from: a */
    public void mo3005a(SceneView sceneView) {
    }

    /* renamed from: a */
    public void mo3004a(C6296akI.C1925a aVar) {
    }

    public void adV() {
    }

    public void close() {
    }

    /* renamed from: bT */
    public <T extends RenderAsset> T mo3047bT(String str) {
        return null;
    }

    /* renamed from: bU */
    public RenderAsset mo3048bU(String str) {
        return null;
    }

    public void adW() {
    }

    public float adX() {
        return 0.0f;
    }

    /* renamed from: bb */
    public float mo3053bb() {
        return 0.0f;
    }

    public int getAntiAliasing() {
        return 0;
    }

    public void setAntiAliasing(int i) {
    }

    public Camera adY() {
        return null;
    }

    public Scene adZ() {
        return null;
    }

    public SceneView aea() {
        return null;
    }

    public float aeb() {
        return 0.0f;
    }

    public int aec() {
        return 0;
    }

    public float aed() {
        return 0.0f;
    }

    public GL getGL() {
        return null;
    }

    public JGLDesktop aee() {
        return null;
    }

    public GLContext aef() {
        return null;
    }

    public float aeg() {
        return 0.0f;
    }

    public GuiScene aeh() {
        return null;
    }

    public String aei() {
        return null;
    }

    public FileSceneLoader aej() {
        return null;
    }

    public float getLodQuality() {
        return 0.0f;
    }

    public void setLodQuality(float f) {
    }

    public float aek() {
        return 0.0f;
    }

    public int ael() {
        return 0;
    }

    public boolean aem() {
        return false;
    }

    public RenderView getRenderView() {
        return null;
    }

    public FilePath getRootPath() {
        return null;
    }

    public List<SceneEvent> aeo() {
        return null;
    }

    public List<SceneView> aep() {
        return null;
    }

    public float aeq() {
        return 0.0f;
    }

    /* renamed from: eH */
    public Vec3f mo3064eH(int i) {
        return null;
    }

    public int getShaderQuality() {
        return 0;
    }

    public void setShaderQuality(int i) {
    }

    public StepContext aer() {
        return null;
    }

    public boolean getUseVBO() {
        return false;
    }

    public int aes() {
        return 0;
    }

    public int aet() {
        return 0;
    }

    /* renamed from: eI */
    public void mo3065eI(int i) {
    }

    /* renamed from: a */
    public void mo3003a(ConfigManagerSection yr, FilePath ain) {
    }

    public boolean aeu() {
        return false;
    }

    /* renamed from: bV */
    public void mo3049bV(String str) {
    }

    public void aev() {
    }

    /* renamed from: bW */
    public void mo3050bW(String str) {
    }

    /* renamed from: b */
    public void mo3045b(String str, float f) {
    }

    /* renamed from: bX */
    public void mo3051bX(String str) {
    }

    /* renamed from: c */
    public void mo3054c(String str, float f) {
    }

    public void aew() {
    }

    /* renamed from: b */
    public void mo3046b(SceneView sceneView) {
    }

    /* renamed from: b */
    public void mo3044b(C6296akI.C1925a aVar) {
    }

    /* renamed from: i */
    public void mo3074i(float f, float f2) {
    }

    public void aex() {
    }

    public void aey() {
    }

    /* renamed from: dm */
    public void mo3057dm(float f) {
    }

    public void setAnisotropicFilter(float f) {
    }

    /* renamed from: dn */
    public void mo3058dn(float f) {
    }

    /* renamed from: eJ */
    public void mo3066eJ(int i) {
    }

    /* renamed from: do */
    public void mo3059do(float f) {
    }

    /* renamed from: dp */
    public void mo3060dp(float f) {
    }

    /* renamed from: a */
    public void mo3002a(int i, int i2, boolean z) {
    }

    /* renamed from: dq */
    public void mo3061dq(float f) {
    }

    public void setPostProcessingFX(boolean z) {
    }

    public void setTextureQuality(int i) {
    }

    /* renamed from: j */
    public void mo3075j(List<Image> list) {
    }

    public boolean step(float f) {
        return false;
    }

    /* renamed from: dr */
    public void mo3062dr(float f) {
    }

    /* renamed from: ds */
    public void mo3063ds(float f) {
    }

    public void swapBuffers() {
    }

    public void aez() {
    }

    public void aeA() {
    }

    /* renamed from: bY */
    public SceneView mo3052bY(String str) {
        return null;
    }

    public boolean aeB() {
        return false;
    }

    /* renamed from: a */
    public void mo3006a(Video video) {
    }

    public void aeC() {
    }

    public void aeD() {
    }

    public int getRefreshRate() {
        return 0;
    }

    public void aeE() {
    }
}
