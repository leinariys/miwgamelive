package logic.res.html;

import logic.thred.C2316dw;

import java.util.LinkedList;

/* renamed from: a.aoI  reason: case insensitive filesystem */
/* compiled from: a */
public class C6504aoI<T> implements C7025azm<T> {
    private final LinkedList<T> gja = new LinkedList<>();
    private boolean disposed;

    public boolean isEmpty() {
        return this.gja.isEmpty();
    }

    public int size() {
        return this.gja.size();
    }

    public int capacity() {
        return Integer.MAX_VALUE;
    }

    public boolean isFull() {
        return Integer.MAX_VALUE - this.gja.size() == 0;
    }

    public void push(T t) {
        synchronized (this.gja) {
            this.gja.add(t);
            this.gja.notify();
        }
    }

    public T pop() {
        T removeFirst;
        synchronized (this.gja) {
            while (this.gja.size() == 0) {
                if (this.disposed) {
                    throw new axI("ThreadID: " + Thread.currentThread().getId());
                }
                try {
                    this.gja.wait();
                } catch (InterruptedException e) {
                    throw new C2316dw("wait interrupted", e);
                }
            }
            removeFirst = this.gja.removeFirst();
        }
        return removeFirst;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return true;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: t */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean mo12044t(java.util.Collection<T> r5) {
        /*
            r4 = this;
            r0 = 1
            java.util.LinkedList<T> r1 = r4.gja
            monitor-enter(r1)
        L_0x0004:
            java.util.LinkedList<T> r2 = r4.gja     // Catch:{ all -> 0x0036 }
            int r2 = r2.size()     // Catch:{ all -> 0x0036 }
            if (r2 == 0) goto L_0x0020
            java.util.LinkedList<T> r2 = r4.gja     // Catch:{ all -> 0x0036 }
            int r2 = r2.size()     // Catch:{ all -> 0x0036 }
            if (r2 <= r0) goto L_0x0039
            java.util.LinkedList<T> r2 = r4.gja     // Catch:{ all -> 0x0036 }
            r5.addAll(r2)     // Catch:{ all -> 0x0036 }
            java.util.LinkedList<T> r2 = r4.gja     // Catch:{ all -> 0x0036 }
            r2.clear()     // Catch:{ all -> 0x0036 }
        L_0x001e:
            monitor-exit(r1)     // Catch:{ all -> 0x0036 }
        L_0x001f:
            return r0
        L_0x0020:
            boolean r2 = r4.disposed     // Catch:{ all -> 0x0036 }
            if (r2 == 0) goto L_0x0027
            monitor-exit(r1)     // Catch:{ all -> 0x0036 }
            r0 = 0
            goto L_0x001f
        L_0x0027:
            java.util.LinkedList<T> r2 = r4.gja     // Catch:{ InterruptedException -> 0x002d }
            r2.wait()     // Catch:{ InterruptedException -> 0x002d }
            goto L_0x0004
        L_0x002d:
            r0 = move-exception
            a.dw r2 = new a.dw     // Catch:{ all -> 0x0036 }
            java.lang.String r3 = "wait interrupted"
            r2.<init>(r3, r0)     // Catch:{ all -> 0x0036 }
            throw r2     // Catch:{ all -> 0x0036 }
        L_0x0036:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0036 }
            throw r0
        L_0x0039:
            java.util.LinkedList<T> r2 = r4.gja     // Catch:{ all -> 0x0036 }
            java.lang.Object r2 = r2.removeFirst()     // Catch:{ all -> 0x0036 }
            r5.add(r2)     // Catch:{ all -> 0x0036 }
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: logic.res.html.C6504aoI.mo12044t(java.util.Collection):boolean");
    }

    public boolean isDisposed() {
        return this.disposed;
    }

    public void dispose() {
        synchronized (this.gja) {
            if (!this.disposed) {
                this.disposed = true;
                this.gja.notifyAll();
            }
        }
    }
}
