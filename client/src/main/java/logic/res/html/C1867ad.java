package logic.res.html;

import logic.res.code.C5663aRz;

import java.util.Iterator;

/* renamed from: a.ad */
/* compiled from: a */
public interface C1867ad<T> {
    /* renamed from: a */
    <E> C1867ad<E> mo7224a(C5663aRz arz);

    /* renamed from: c */
    C0911NN mo7225c(T t);

    /* renamed from: dg */
    Iterator<T> mo7226dg();
}
