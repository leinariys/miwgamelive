package logic.res.html;

import game.network.GameScriptClasses;
import p001a.C3582se;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;

/* renamed from: a.Fv */
/* compiled from: a */
public class GameScriptClassesImpl implements GameScriptClasses, Externalizable, Serializable {

    private long agR;
    private int magicNumbers;
    private transient C3582se cVz;

    public GameScriptClassesImpl() {
    }

    public GameScriptClassesImpl(C3582se seVar) {
        this.cVz = seVar;
        Class<?> cls = seVar.mo6901yn().getClass();
        this.magicNumbers = seVar.mo6866PM().bxy().getMagicNumber(cls);
        if (this.magicNumbers <= 0) {
            throw new IllegalStateException("All game.script classes must have magic numbers, " + cls.getName());
        }
    }

    /* renamed from: Ej */
    public long mo2229Ej() {
        if (this.cVz != null) {
            return this.cVz.getObjectId().getId();
        }
        return this.agR;
    }

    public int aPv() {
        return this.magicNumbers;
    }

    public void readExternal(ObjectInput objectInput) {
        this.magicNumbers = objectInput.readUnsignedShort();
        if (this.magicNumbers == 65535) {
            throw new IllegalStateException("All game.script classes must have magic numbers");
        }
        this.agR = objectInput.readLong();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeShort(aPv());
        objectOutput.writeLong(mo2229Ej());
    }
}
