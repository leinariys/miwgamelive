package logic.res.html;

import java.util.Collection;

/* renamed from: a.azm  reason: case insensitive filesystem */
/* compiled from: a */
public interface C7025azm<T> {
    int capacity();

    void dispose();

    boolean isDisposed();

    boolean isEmpty();

    boolean isFull();

    T pop();

    void push(T t);

    int size();

    /* renamed from: t */
    boolean mo12044t(Collection<T> collection);
}
