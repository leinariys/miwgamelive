package logic.res.html;

import p001a.C3538sJ;
import p001a.C6147ahP;

import java.io.Serializable;

/* renamed from: a.cc */
/* compiled from: a */
public interface C2194cc extends Serializable {
    /* renamed from: hi */
    C6147ahP[] mo7387hi();

    /* renamed from: hj */
    C3538sJ mo7388hj();

    /* renamed from: hk */
    boolean mo7389hk();

    /* renamed from: hl */
    boolean mo7390hl();

    /* renamed from: hm */
    boolean mo7391hm();

    /* renamed from: hn */
    boolean mo7392hn();

    /* renamed from: ho */
    boolean mo7393ho();

    /* renamed from: hp */
    boolean mo7394hp();

    /* renamed from: hq */
    int mo7395hq();

    /* renamed from: hr */
    String mo7396hr();

    String name();
}
