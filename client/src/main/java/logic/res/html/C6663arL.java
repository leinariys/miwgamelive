package logic.res.html;

import game.engine.DataGameEvent;
import game.network.channel.client.ClientConnect;
import game.network.message.externalizable.ObjectId;
import logic.baa.C1616Xf;
import logic.baa.C4062yl;
import logic.data.mbean.C6064afk;
import logic.res.code.C5663aRz;
import p001a.C0495Gr;

/* renamed from: a.arL  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6663arL {
    public static final int gtr = 0;
    public static final int gts = 1;
    public static final int gtt = 2;
    public static final int gtu = 3;

    /* renamed from: M */
    C1616Xf mo6865M(Class<?> cls);

    /* renamed from: PM */
    DataGameEvent mo6866PM();

    /* renamed from: a */
    void mo5599a(C5663aRz arz, Object obj);

    /* renamed from: a */
    void mo5600a(Class<? extends C4062yl> cls, C0495Gr gr);

    void aMr();

    /* renamed from: b */
    Object mo5602b(C5663aRz arz);

    void bFR();

    boolean bFS();

    boolean bFT();

    Class<? extends C1616Xf> bFU();

    Object bFV();

    String bFY();

    void bFZ();

    void bFj();

    /* renamed from: d */
    Object mo5606d(C0495Gr gr);

    /* renamed from: dq */
    C6064afk mo5608dq();

    /* renamed from: du */
    boolean mo6887du();

    /* renamed from: dv */
    GameScriptClassesImpl mo6888dv();

    /* renamed from: g */
    void mo6889g(C5663aRz arz, Object obj);

    /* renamed from: hC */
    ObjectId getObjectId();

    /* renamed from: hE */
    ClientConnect mo6892hE();

    /* renamed from: i */
    int mo6893i(C2491fm fmVar);

    /* renamed from: i */
    Object mo6894i(C1616Xf xf);

    boolean isDisposed();

    void push();

    /* renamed from: w */
    Object mo6898w(C5663aRz arz);

    /* renamed from: yn */
    C1616Xf mo6901yn();
}
