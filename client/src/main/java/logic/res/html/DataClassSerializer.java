package logic.res.html;

import game.io.IReadExternal;
import game.io.IWriteExternal;
import game.network.message.externalizable.C5572aOm;
import gnu.trove.THashSet;
import logic.baa.C1616Xf;
import logic.thred.LogPrinter;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Kd */
/* compiled from: a */
public class DataClassSerializer {
    private static final THashSet<Class<?>> makingSpecialSerializerToClass = new THashSet<>();

    public static DataClassSerializer dnL = new DataClassSerializer();
    static LogPrinter logger = LogPrinter.m10275K(DataClassSerializer.class);
    public Class<?> clazz;
    private boolean collection;
    private Class<?>[] aha;
    private boolean ahc;
    private Object defaultValue = null;
    private boolean dnM = true;

    /* renamed from: o */
    public static final void addClassMakingSpecialSerializer(String str, Object obj) {
        Class<?> cls = obj.getClass();
        if (!makingSpecialSerializerToClass.contains(cls)) {
            makingSpecialSerializerToClass.add(cls);
            logger.info(String.valueOf(str) + cls);
        }
    }

    /* renamed from: d */
    public static final void addClassTMakingSpecialSerializer(String str, Class<?> cls) {
        if (!makingSpecialSerializerToClass.contains(cls)) {
            makingSpecialSerializerToClass.add(cls);
            logger.info(String.valueOf(str) + cls);
        }
    }

    /* renamed from: b */
    public static int m6487b(ObjectInput objectInput, int i) {
        if (i <= 255) {
            return objectInput.readUnsignedByte();
        }
        if (i <= 65535) {
            return objectInput.readUnsignedShort();
        }
        if (i <= 16777215) {
            return (objectInput.readUnsignedShort() << 8) | objectInput.readUnsignedByte();
        }
        return objectInput.readInt();
    }

    /* renamed from: a */
    public static void m6486a(ObjectOutput objectOutput, int i, int i2) {
        if (i <= 255) {
            if (i2 < 0 || i2 > 255) {
                throw new IllegalArgumentException();
            }
            objectOutput.writeByte(i2);
        } else if (i <= 65535) {
            if (i2 < 0 || i2 > 65535) {
                throw new IllegalArgumentException();
            }
            objectOutput.writeShort(i2);
        } else if (i > 16777215) {
            objectOutput.writeInt(255);
        } else if (i2 < 0 || i2 > 16777215) {
            throw new IllegalArgumentException();
        } else {
            objectOutput.writeShort(i2 >> 8);
            objectOutput.writeByte(i2 & 255);
        }
    }

    public boolean isImmutable() {
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo3590a(Class<?> cls, Class<?>[] clsArr) {
        this.aha = clsArr;
        this.clazz = cls;
        this.ahc = C5572aOm.class.isAssignableFrom(cls);
        this.collection = C0029AO.class.isAssignableFrom(cls);
    }

    public Class<?> getType() {
        return this.clazz;
    }

    public boolean isCollection() {
        return this.collection;
    }

    public Object getDefaultValue() {
        return this.defaultValue;
    }

    /* renamed from: A */
    public void setDefaultValue(Object obj) {
        this.defaultValue = obj;
    }

    public String toString() {
        return this.clazz.getName();
    }

    /* renamed from: Eq */
    public Class<?>[] mo3589Eq() {
        return this.aha;
    }

    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) throws IOException {
        if (this.dnM && obj != null) {
            addClassMakingSpecialSerializer("Consider making a special serializer for ", obj);
            this.dnM = false;
        }
        objectOutput.writeObject(obj);
    }

    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) throws IOException {
        return objectInput.readObject();
    }

    /* renamed from: z */
    public Object mo3598z(Object obj) {
        if (obj == null) {
            return null;
        }
        if (this.ahc) {
            return ((C5572aOm) obj).mo8390Jq();
        }
        throw new RuntimeException(new CloneNotSupportedException());
    }

    /* renamed from: yl */
    public boolean mo3597yl() {
        return this.ahc;
    }

    /* renamed from: b */
    public Object mo3591b(C1616Xf xf) {
        return null;
    }

    /* renamed from: a */
    public void serializeData(IWriteExternal att, Object obj) {
        if (this.dnM && obj != null) {
            addClassTMakingSpecialSerializer("Consider making a special asym serializer for ", this.clazz);
            this.dnM = false;
        }
        att.mo16273g("unknown", obj);
    }

    /* renamed from: a */
    public Object mo2358a(String str, IReadExternal vm) throws IOException {
        vm.mo6369hg(str);
        Object a = mo2357a(vm);
        vm.mo6370pe();
        return a;
    }

    /* renamed from: a */
    public Object mo2357a(IReadExternal vm) {
        return vm.mo6366hd("unknown");
    }
}
