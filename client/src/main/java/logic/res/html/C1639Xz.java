package logic.res.html;

/* renamed from: a.Xz */
/* compiled from: a */
public class C1639Xz extends ThreadTimestamp {

    public final long agR;
    public final Class<?> agS;
    public final C2491fm eJi;
    public final boolean eJj;
    public int eJk;

    public C1639Xz(C6663arL arl, C2491fm fmVar) {
        this(arl, fmVar, false);
    }

    public C1639Xz(C6663arL arl, C2491fm fmVar, boolean z) {
        this.eJi = fmVar;
        this.eJk = arl.mo6866PM().bGU().mo15542HX();
        this.agS = arl.mo6901yn().getClass();
        this.agR = arl.getObjectId().getId();
        this.identifierThread = Thread.currentThread().getId();
        this.eJj = z;
    }

    public String toString() {
        return String.valueOf(this.timestamp) + " T:" + this.identifierThread + " TT:" + this.eJk + " " + this.eJi.name() + " on " + this.agS + ":" + this.agR;
    }
}
