package logic.res.html;

import gnu.trove.THashMap;
import gnu.trove.THashSet;

import java.util.*;

/* renamed from: a.aKU */
/* compiled from: a */
public abstract class MessageContainer {
    public static <VALUE> Set<VALUE> dgI() {
        return new THashSet();
    }

    /* renamed from: v */
    public static <VALUE> Set<VALUE> m16003v(Collection<VALUE> collection) {
        return new THashSet(collection);
    }

    public static <VALUE> Set<VALUE> dgJ() {
        return Collections.synchronizedSet(dgI());
    }

    public static <VALUE> List<VALUE> init() {
        return new ArrayList();
    }

    /* renamed from: yv */
    public static <VALUE> List<VALUE> m16005yv(int i) {
        return new ArrayList(i);
    }

    /* renamed from: w */
    public static <VALUE> List<VALUE> m16004w(Collection<VALUE> collection) {
        return new ArrayList(collection);
    }

    public static <VALUE> List<VALUE> dgL() {
        return Collections.synchronizedList(init());
    }

    public static <KEY, VALUE> Map<KEY, VALUE> newMap() {
        return new THashMap();
    }

    public static <KEY, VALUE> Map<KEY, VALUE> dgM() {
        return Collections.synchronizedMap(new HashMap());
    }

    public static <VALUE> C7025azm<VALUE> dgN() {
        return m16006yw(-1);
    }

    /* renamed from: yw */
    public static <VALUE> C7025azm<VALUE> m16006yw(int i) {
        return new C6504aoI();
    }
}
