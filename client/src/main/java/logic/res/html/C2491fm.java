package logic.res.html;

import p001a.C3248pc;
import p001a.C6580apg;

import java.lang.reflect.Type;

/* renamed from: a.fm */
/* compiled from: a */
public interface C2491fm extends C2194cc {

    /* renamed from: JR */
    public static final int f7457JR = 1;

    /* renamed from: JS */
    public static final int f7458JS = 2;

    /* renamed from: JT */
    public static final int f7459JT = 3;
    public static final int SKIP = 0;

    /* renamed from: A */
    void mo4715A(long j);

    /* renamed from: B */
    void mo4716B(long j);

    /* renamed from: aK */
    void mo4717aK(int i);

    /* renamed from: aL */
    void mo4718aL(int i);

    /* renamed from: aM */
    void mo4719aM(int i);

    /* renamed from: aN */
    void mo4720aN(int i);

    /* renamed from: aO */
    void mo4721aO(int i);

    /* renamed from: aP */
    void mo4722aP(int i);

    /* renamed from: aQ */
    void mo4723aQ(int i);

    /* renamed from: aR */
    void mo4724aR(int i);

    /* renamed from: aS */
    void mo4725aS(int i);

    /* renamed from: aT */
    void mo4726aT(int i);

    /* renamed from: aU */
    void mo4727aU(int i);

    /* renamed from: aV */
    void mo4728aV(int i);

    /* renamed from: aW */
    void mo4729aW(int i);

    /* renamed from: aX */
    void mo4730aX(int i);

    /* renamed from: aY */
    void mo4731aY(int i);

    String getDisplayName();

    int getExecuteCount();

    long getExecuteTime();

    Type getGenericReturnType();

    int getInfraMethodsCalled();

    int getObjectsChangedInTransaction();

    int getObjectsCreatedInTransaction();

    int getObjectsTouchedInPostponed();

    int getObjectsTouchedInTransaction();

    Class<?>[] getParameterTypes();

    int getReadLocksInTransaction();

    Class<?> getReturnType();

    int getTransactionCount();

    long getTransactionTime();

    int getTransactionalGets();

    int getTransactionalGetsInPostponed();

    int getTransactionalMethodsCalled();

    int getTransactionalSets();

    int getTransactionalSetsInPostponed();

    int getWriteLocksInTransaction();

    /* renamed from: hD */
    Class<?> mo4752hD();

    /* renamed from: hm */
    boolean mo7391hm();

    /* renamed from: hq */
    int mo7395hq();

    boolean isBlocking();

    boolean isEditable();

    /* renamed from: pA */
    int mo4755pA();

    /* renamed from: pB */
    boolean mo4756pB();

    /* renamed from: pC */
    boolean mo4757pC();

    /* renamed from: pD */
    boolean mo4758pD();

    /* renamed from: pE */
    boolean mo4759pE();

    /* renamed from: pF */
    boolean mo4760pF();

    /* renamed from: pG */
    boolean mo4761pG();

    /* renamed from: pH */
    boolean mo4762pH();

    /* renamed from: pI */
    boolean mo4763pI();

    /* renamed from: pJ */
    C3248pc.C3249a mo4764pJ();

    /* renamed from: pK */
    String mo4765pK();

    /* renamed from: pL */
    boolean mo4766pL();

    /* renamed from: pM */
    DataClassSerializer[] mo4767pM();

    /* renamed from: pN */
    Object mo4768pN();

    /* renamed from: pO */
    C6580apg mo4769pO();

    /* renamed from: pP */
    void mo4770pP();

    /* renamed from: pQ */
    int mo4771pQ();

    /* renamed from: pR */
    int mo4772pR();

    /* renamed from: pS */
    int mo4773pS();

    /* renamed from: pT */
    int mo4774pT();

    /* renamed from: pU */
    void mo4775pU();

    /* renamed from: pV */
    void mo4776pV();

    /* renamed from: pW */
    long mo4777pW();

    /* renamed from: pX */
    long mo4778pX();

    /* renamed from: pY */
    int mo4779pY();

    /* renamed from: pZ */
    boolean mo4780pZ();

    /* renamed from: pm */
    boolean mo4781pm();

    /* renamed from: pn */
    boolean mo4782pn();

    /* renamed from: po */
    boolean mo4783po();

    /* renamed from: pp */
    boolean mo4784pp();

    /* renamed from: pq */
    boolean mo4785pq();

    /* renamed from: pr */
    boolean mo4786pr();

    /* renamed from: ps */
    boolean mo4787ps();

    /* renamed from: pt */
    boolean mo4788pt();

    /* renamed from: pu */
    boolean mo4789pu();

    /* renamed from: pv */
    boolean mo4790pv();

    /* renamed from: pw */
    boolean mo4791pw();

    /* renamed from: px */
    boolean mo4792px();

    /* renamed from: py */
    boolean mo4793py();

    /* renamed from: pz */
    int mo4794pz();

    /* renamed from: qa */
    long mo4795qa();
}
