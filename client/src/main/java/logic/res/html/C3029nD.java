package logic.res.html;

/* renamed from: a.nD */
/* compiled from: a */
public class C3029nD extends ThreadTimestamp {

    public final C1639Xz aJg;
    public final Exception exception;

    public C3029nD(C1639Xz xz, Exception exc) {
        this.aJg = xz;
        this.exception = exc;
    }

    public String toString() {
        return " exception at " + this.aJg + ": " + this.exception;
    }
}
