package logic.res.html;

import java.io.Serializable;
import java.util.Collection;

/* renamed from: a.AO */
/* compiled from: a */
public interface C0029AO<T> extends Cloneable, Collection<T>, Serializable {
    /* renamed from: XC */
    C1867ad<T> mo271XC();
}
