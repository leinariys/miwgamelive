package logic.res.html;


import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/* renamed from: a.id */
/* compiled from: a */
public class C2690id {

    /* renamed from: WS */
    public static final String f8212WS = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-------|-------<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

    /* renamed from: WT */
    public static final String f8213WT = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|-------<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

    /* renamed from: WU */
    public static final String f8214WU = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>------>|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

    /* renamed from: WV */
    public static final String f8215WV = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-------|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

    /* renamed from: WW */
    public static final String f8216WW = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<------<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

    /* renamed from: WX */
    public static final String f8217WX = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

    /* renamed from: WY */
    public static final String f8218WY = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|----+&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<---+&nbsp;&nbsp;";

    /* renamed from: WZ */
    public List<ThreadTimestamp> f8219WZ = null;

    /* renamed from: Xa */
    public Map<Long, Class<?>> f8220Xa;

    /* renamed from: Xb */
    public List<Long> f8221Xb;

    /* renamed from: Xc */
    public List<Class<?>> f8222Xc;

    /* renamed from: Xd */
    public Map<Long, Stack<Long>> f8223Xd = new HashMap();

    /* renamed from: a */
    public void mo19758a(List<ThreadTimestamp> list, String str) {
        if (this.f8219WZ == null) {
            this.f8219WZ = MessageContainer.init();
        }
        Iterator<ThreadTimestamp> it = list.iterator();
        int i = 0;
        while (it.hasNext()) {
            ThreadTimestamp next = it.next();
            int i2 = i;
            while (i2 < this.f8219WZ.size() && next.timestamp > this.f8219WZ.get(i2).timestamp) {
                i2++;
            }
            next.eNl = str;
            this.f8219WZ.add(i2, next);
            i = i2;
        }
        mo19757Bf();
    }

    /* access modifiers changed from: protected */
    /* renamed from: Bf */
    public void mo19757Bf() {
        this.f8220Xa = new HashMap();
        for (ThreadTimestamp next : this.f8219WZ) {
            if (next instanceof C1639Xz) {
                C1639Xz xz = (C1639Xz) next;
                if (!this.f8220Xa.containsKey(Long.valueOf(xz.agR))) {
                    this.f8220Xa.put(Long.valueOf(xz.agR), xz.agS);
                }
            }
        }
        this.f8221Xb = MessageContainer.init();
        this.f8222Xc = MessageContainer.init();
        for (Long longValue : this.f8220Xa.keySet()) {
            this.f8221Xb.add(Long.valueOf(longValue.longValue()));
        }
        for (Long longValue2 : this.f8221Xb) {
            this.f8222Xc.add(this.f8220Xa.get(Long.valueOf(longValue2.longValue())));
        }
        this.f8221Xb.add(0, -1L);
        this.f8222Xc.add(0, Object.class);
    }

    /* access modifiers changed from: protected */
    /* renamed from: bx */
    public int mo19761bx(long j) {
        if (this.f8221Xb != null) {
            return this.f8221Xb.indexOf(Long.valueOf(j));
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo19760b(long j, long j2) {
        Stack stack = this.f8223Xd.get(Long.valueOf(j));
        if (stack == null) {
            stack = new Stack();
            this.f8223Xd.put(Long.valueOf(j), stack);
        }
        stack.push(Long.valueOf(j2));
    }

    /* access modifiers changed from: protected */
    /* renamed from: by */
    public Long mo19762by(long j) {
        Stack stack = this.f8223Xd.get(Long.valueOf(j));
        if (stack == null || stack.isEmpty()) {
            return -1L;
        }
        return (Long) stack.pop();
    }

    /* renamed from: aB */
    public void mo19759aB(String str) throws IOException {
        long j;
        if (this.f8219WZ == null) {
            throw new RuntimeException("LogCallEntry list is null!");
        }
        PrintWriter printWriter = new PrintWriter(new FileWriter(str));
        printWriter.print("<html>\n<head>\n<title>Call log</title>\n</head>\n<body>\n<table border=0 cellspacing=0 cellpadding=0>\n");
        printWriter.print("<td>Timestamp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Source&nbsp;&nbsp;&nbsp;&nbsp;</td><td>thread id&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>\n<td>transp. thread id&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>\n<td>method&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>\n");
        for (int i = 0; i < this.f8221Xb.size(); i++) {
            printWriter.print("<td align=center>");
            printWriter.print(this.f8222Xc.get(i).getSimpleName());
            printWriter.print("(");
            printWriter.print(this.f8221Xb.get(i));
            printWriter.print(")</td>\n");
        }
        printWriter.print("</tr>\n");
        long j2 = -1;
        for (ThreadTimestamp next : this.f8219WZ) {
            if (!(next instanceof aDF)) {
                printWriter.print("<tr>\n");
                printWriter.print("<td>");
                printWriter.print(next.timestamp);
                printWriter.print("&nbsp;&nbsp;&nbsp;</td>\n");
                printWriter.print("<td>");
                printWriter.print(next.eNl);
                printWriter.print("</td>\n");
                printWriter.print("<td>");
                printWriter.print(next.identifierThread);
                printWriter.print("</td>\n");
                if (next instanceof C1639Xz) {
                    C1639Xz xz = (C1639Xz) next;
                    printWriter.print("<td>");
                    printWriter.print(xz.eJk);
                    printWriter.print("</td>\n");
                    printWriter.print("<td>");
                    if (xz.eJi != null) {
                        printWriter.print(xz.eJi.name());
                    }
                    printWriter.print("()");
                    printWriter.print("</td>\n");
                    int bx = mo19761bx(j2);
                    int bx2 = mo19761bx(xz.agR);
                    int min = Math.min(bx, bx2);
                    for (int i2 = 0; i2 < min; i2++) {
                        printWriter.print("<td><tt>");
                        printWriter.print(f8217WX);
                        printWriter.print("</tt></td>\n");
                    }
                    if (bx == bx2) {
                        printWriter.print("<td><tt>");
                        printWriter.print(f8218WY);
                        printWriter.print("</tt></td>\n");
                    } else if (bx < bx2) {
                        printWriter.print("<td><tt>");
                        printWriter.print(f8213WT);
                        printWriter.print("</tt></td>\n");
                        for (int i3 = bx + 1; i3 < bx2; i3++) {
                            printWriter.print("<td><tt>");
                            printWriter.print(f8212WS);
                            printWriter.print("</tt></td>\n");
                        }
                        printWriter.print("<td><tt>");
                        printWriter.print(f8214WU);
                        printWriter.print("</tt></td>\n");
                    } else {
                        printWriter.print("<td><tt>");
                        printWriter.print(f8216WW);
                        printWriter.print("</tt></td>\n");
                        for (int i4 = bx2 + 1; i4 < bx; i4++) {
                            printWriter.print("<td><tt>");
                            printWriter.print(f8212WS);
                            printWriter.print("</tt></td>\n");
                        }
                        printWriter.print("<td><tt>");
                        printWriter.print(f8215WV);
                        printWriter.print("</tt></td>\n");
                    }
                    int max = Math.max(bx, bx2);
                    while (true) {
                        max++;
                        if (max >= this.f8221Xb.size()) {
                            break;
                        }
                        printWriter.print("<td><tt>");
                        printWriter.print(f8217WX);
                        printWriter.print("</tt></td>\n");
                    }
                    mo19760b(xz.identifierThread, j2);
                    j = xz.agR;
                } else {
                    if ((next instanceof C6739asj) || (next instanceof C3029nD)) {
                        printWriter.print("<td>");
                        printWriter.print("</td>\n");
                        printWriter.print("<td>");
                        if (next instanceof C6739asj) {
                            printWriter.print("return");
                        } else {
                            C3029nD nDVar = (C3029nD) next;
                            printWriter.print("exception<br>");
                            printWriter.print(nDVar.exception.getClass().getSimpleName());
                            printWriter.print("<br>");
                            printWriter.print(nDVar.exception.getMessage());
                        }
                        printWriter.print("</td>\n");
                        int bx3 = mo19761bx(j2);
                        j2 = mo19762by(next.identifierThread).longValue();
                        int bx4 = mo19761bx(j2);
                        int min2 = Math.min(bx3, bx4);
                        for (int i5 = 0; i5 < min2; i5++) {
                            printWriter.print("<td><tt>");
                            printWriter.print(f8217WX);
                            printWriter.print("</tt></td>\n");
                        }
                        if (bx3 == bx4) {
                            printWriter.print("<td><tt>");
                            printWriter.print(f8218WY);
                            printWriter.print("</tt></td>\n");
                        } else if (bx3 < bx4) {
                            printWriter.print("<td><tt>");
                            printWriter.print(f8213WT);
                            printWriter.print("</tt></td>\n");
                            for (int i6 = bx3 + 1; i6 < bx4; i6++) {
                                printWriter.print("<td><tt>");
                                printWriter.print(f8212WS);
                                printWriter.print("</tt></td>\n");
                            }
                            printWriter.print("<td><tt>");
                            printWriter.print(f8214WU);
                            printWriter.print("</tt></td>\n");
                        } else {
                            printWriter.print("<td><tt>");
                            printWriter.print(f8216WW);
                            printWriter.print("</tt></td>\n");
                            for (int i7 = bx4 + 1; i7 < bx3; i7++) {
                                printWriter.print("<td><tt>");
                                printWriter.print(f8212WS);
                                printWriter.print("</tt></td>\n");
                            }
                            printWriter.print("<td><tt>");
                            printWriter.print(f8215WV);
                            printWriter.print("</tt></td>\n");
                        }
                        int max2 = Math.max(bx3, bx4);
                        while (true) {
                            max2++;
                            if (max2 >= this.f8221Xb.size()) {
                                break;
                            }
                            printWriter.print("<td><tt>");
                            printWriter.print(f8217WX);
                            printWriter.print("</tt></td>\n");
                        }
                    }
                    j = j2;
                }
                printWriter.print("</tr>\n");
                j2 = j;
            }
        }
        printWriter.print("</table>\n</body>\n</html>");
        printWriter.close();
    }
}
