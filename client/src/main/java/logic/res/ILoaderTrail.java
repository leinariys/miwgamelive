package logic.res;

import logic.res.sound.C0907NJ;
import p001a.C1212Ru;
import p001a.C1249ST;
import p001a.C5964ado;
import p001a.aJP;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.Scene;

/* renamed from: a.QU */
/* compiled from: a */
public interface ILoaderTrail {
    /* renamed from: a */
    void mo4992a(aJP ajp);

    /* renamed from: a */
    void mo4993a(C5964ado ado);

    /* renamed from: a */
    void mo4994a(String str, C1249ST st, String str2);

    /* renamed from: a */
    void mo4995a(String str, String str2, Scene scene, C0907NJ nj, String str3);

    /* renamed from: a */
    void mo4996a(String str, String str2, Scene scene, C0907NJ nj, String str3, int i);

    /* renamed from: a */
    void mo4997a(String[] strArr);

    /* renamed from: ar */
    C1212Ru.C1213a mo4998ar(String str);

    /* renamed from: as */
    void mo4999as(String str);

    /* renamed from: b */
    void mo5000b(aJP ajp);

    /* renamed from: b */
    void mo5001b(C5964ado ado);

    void dispose();

    RenderAsset getAsset(String str);

    /* renamed from: l */
    RenderAsset mo5004l(String str, String str2);

    void start();

    void stop();

    /* renamed from: vr */
    void mo5007vr();

    /* renamed from: vu */
    void mo5008vu();
}
