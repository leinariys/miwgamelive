package logic.res;

import logic.render.IEngineGraphics;
import logic.res.code.C1625Xn;
import logic.res.sound.C0907NJ;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.*;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.provider.FilePath;
import taikodom.render.scene.RenderObject;
import taikodom.render.scene.Scene;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: a.gl */
/* compiled from: a */
public class LoaderTrail implements ILoaderTrail {
    /* access modifiers changed from: private */
    public static final Log log = LogPrinter.m10275K(LoaderTrail.class);
    /* access modifiers changed from: private */
    /* renamed from: PP */
    private final C1078Pm f7758PP;
    /* renamed from: PS */
    private final Comparator f7761PS = new C0558Hl(this);
    /* renamed from: PH */
    public List<C5964ado> f7751PH = new ArrayList();
    /* renamed from: PM */
    public int f7755PM = 0;
    /* access modifiers changed from: private */
    /* access modifiers changed from: private */
    public Thread thread;
    private Executor executor;
    /* renamed from: PJ */
    private List<aJP> f7752PJ = new ArrayList();
    /* renamed from: PK */
    private List<aGU> f7753PK = new LinkedList();
    /* renamed from: PL */
    private ReentrantLock f7754PL = new ReentrantLock();
    /* renamed from: PN */
    private C1212Ru f7756PN = new C1212Ru();
    /* renamed from: PO */
    private C1625Xn f7757PO;
    /* renamed from: PQ */
    private C5723aUh f7759PQ;
    /* renamed from: PR */
    private C6317akd f7760PR;
    /* renamed from: PT */
    private volatile aGU f7762PT;
    /* renamed from: PU */
    private LinkedBlockingQueue<aGU> f7763PU = new LinkedBlockingQueue<>(500);
    /* renamed from: lV */
    private IEngineGraphics f7764lV;

    public LoaderTrail(IEngineGraphics abd) {
        this.f7764lV = abd;
        this.f7758PP = new C1078Pm(this.f7764lV);
        this.f7759PQ = new C5723aUh();
        this.f7760PR = new C6317akd(new C1212Ru());
    }

    /* access modifiers changed from: protected */
    /* renamed from: vq */
    public C1625Xn mo19103vq() {
        return this.f7757PO;
    }

    /* renamed from: a */
    public void mo19096a(C1625Xn xn) {
        this.f7757PO = xn;
    }

    /* renamed from: ar */
    public C1212Ru.C1213a mo4998ar(String str) {
        try {
            return this.f7756PN.mo5283k(this.f7764lV.getRootPath().concat("data/models/trails/" + str + ".trail"));
        } catch (IOException e) {
            log.warn("Trying to load a null trail file, ignoring");
            return null;
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: a */
    public void mo19098a(String str, C2601hQ hQVar, String str2, boolean z) {
        if (str == null || hQVar == null) {
            log.warn(String.valueOf(str2) + " is trying to load a null file / null resource, ignoring");
            return;
        }
        synchronized (this.f7753PK) {
            this.f7753PK.add(new C1466Va(str, hQVar, this.f7759PQ, z));
            Collections.sort(this.f7753PK, this.f7761PS);
        }
        this.f7755PM++;
    }

    /* renamed from: a */
    public void mo4996a(String str, String str2, Scene scene, C0907NJ nj, String str3, int i) {
        if (str == null || (str2 == null && nj != null)) {
            log.warn(String.valueOf(str3) + " is trying to load a null file / null resource, ignoring");
            return;
        }
        synchronized (this.f7753PK) {
            this.f7753PK.add(new C5907acj(str, str2, nj, scene, this.f7758PP, i));
            Collections.sort(this.f7753PK, this.f7761PS);
        }
        this.f7755PM++;
    }

    /* renamed from: a */
    public void mo4995a(String str, String str2, Scene scene, C0907NJ nj, String str3) {
        mo4996a(str, str2, scene, nj, str3, 0);
    }

    /* renamed from: a */
    public void mo4994a(String str, C1249ST st, String str2) {
        if (str == null || st == null) {
            log.warn(String.valueOf(str2) + " is trying to load a null file / null trail resource, ignoring");
            return;
        }
        FilePath aC = this.f7764lV.getRootPath().concat("data/models/trails/" + str + ".trail");
        synchronized (this.f7753PK) {
            this.f7753PK.add(new C1058PU(str, aC, st, this.f7760PR));
            Collections.sort(this.f7753PK, this.f7761PS);
        }
        this.f7755PM++;
    }

    /* renamed from: vr */
    public void mo5007vr() {
        synchronized (this.f7753PK) {
            for (aGU next : this.f7753PK) {
                next.priority--;
            }
        }
    }

    /* renamed from: a */
    public void mo4997a(String[] strArr) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < strArr.length) {
                mo4995a(strArr[i2], (String) null, (Scene) null, (C0907NJ) null, "Scync loader for cache ");
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* renamed from: as */
    public void mo4999as(String str) {
        this.f7764lV.mo3049bV(str);
    }

    /* renamed from: j */
    public RenderObject mo19101j(String str, String str2) {
        return (RenderObject) m32349k(str, str2);
    }

    /* renamed from: k */
    private RenderAsset m32349k(String str, String str2) {
        this.f7764lV.mo3049bV(str);
        return this.f7764lV.mo3047bT(str2);
    }

    public boolean isReady() {
        return this.f7753PK.isEmpty();
    }

    public void start() {
        this.thread = new C2558d("Resource loader thread");
        this.thread.setDaemon(true);
        this.thread.start();
    }

    public void stop() {
        if (this.thread != null && this.thread.isAlive()) {
            this.thread.interrupt();
            try {
                this.thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: gZ */
    public void mo19099gZ() {
        aGU remove;
        if (this.f7753PK.size() > 0) {
            synchronized (this.f7753PK) {
                remove = this.f7753PK.remove(0);
            }
            C1625Xn vq = mo19103vq();
            boolean brL = vq.brL();
            try {
                if (remove.fileName == null || remove.fileName.length() <= 0) {
                    logger.error("Trying to load a resource with no filename!");
                    logger.error("current items to process " + this.f7753PK.size());
                    if (brL) {
                        try {
                            vq.brK();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    this.f7762PT = remove;
                    remove.hTO.mo4836b(remove);
                    if (brL) {
                        try {
                            vq.brK();
                        } catch (InterruptedException e2) {
                            e2.printStackTrace();
                        }
                    }
                    if (remove.hTN == null) {
                        return;
                    }
                    if (remove.hTN.length() <= 0) {
                        logger.error("Trying to load a resource with no name! " + remove.fileName);
                        logger.error("current items to process " + this.f7753PK.size());
                        return;
                    }
                    remove.hTO.mo4838c(remove);
                    mo19097a(remove);
                }
            } catch (aOX e3) {
                logger.error(e3.getMessage());
                if (brL) {
                    try {
                        vq.brK();
                    } catch (InterruptedException e4) {
                        e4.printStackTrace();
                    }
                }
            } catch (Exception e5) {
                logger.error(e5.getMessage(), e5);
                if (brL) {
                    try {
                        vq.brK();
                    } catch (InterruptedException e6) {
                        e6.printStackTrace();
                    }
                }
            } catch (Throwable th) {
                if (brL) {
                    try {
                        vq.brK();
                    } catch (InterruptedException e7) {
                        e7.printStackTrace();
                    }
                }
                throw th;
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: vs */
    public void m32350vs() {
        this.executor.execute(new C2557c());
    }

    /* access modifiers changed from: private */
    /* renamed from: vt */
    public void m32351vt() {
        this.f7754PL.lock();
        ArrayList<aJP> arrayList = new ArrayList<>();
        arrayList.addAll(this.f7752PJ);
        this.f7754PL.unlock();
        if (!arrayList.isEmpty()) {
            if (logger.isDebugEnabled()) {
                logger.debug("file loaded. total: " + this.f7755PM + " | to go: " + this.f7753PK.size());
            }
            for (aJP ad : arrayList) {
                ad.mo9474ad(this.f7755PM, this.f7753PK.size());
            }
        }
    }

    /* renamed from: a */
    public void mo4993a(C5964ado ado) {
        this.f7754PL.lock();
        this.f7751PH.add(ado);
        this.f7754PL.unlock();
    }

    /* renamed from: b */
    public void mo5001b(C5964ado ado) {
        this.f7754PL.lock();
        this.f7751PH.remove(ado);
        this.f7754PL.unlock();
    }

    /* renamed from: a */
    public void mo4992a(aJP ajp) {
        this.f7754PL.lock();
        this.f7752PJ.add(ajp);
        this.f7754PL.unlock();
    }

    /* renamed from: b */
    public void mo5000b(aJP ajp) {
        this.f7754PL.lock();
        this.f7752PJ.remove(ajp);
        this.f7754PL.unlock();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo19097a(aGU agu) {
        try {
            this.f7763PU.put(agu);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: vu */
    public void mo5008vu() {
        while (true) {
            aGU poll = this.f7763PU.poll();
            if (poll != null) {
                poll.boi();
                this.executor.execute(new C2556b(poll));
                if (this.f7753PK.isEmpty() && this.f7763PU.isEmpty()) {
                    this.executor.execute(new C2555a());
                }
            } else {
                return;
            }
        }
    }

    public RenderAsset getAsset(String str) {
        return this.f7764lV.mo3047bT(str);
    }

    /* renamed from: l */
    public RenderAsset mo5004l(String str, String str2) {
        aGU agu = this.f7762PT;
        if (agu == null || !str.equalsIgnoreCase(agu.fileName)) {
            return this.f7764lV.mo3048bU(str2);
        }
        return null;
    }

    public void dispose() {
        Thread thread2 = this.thread;
        this.thread = null;
        thread2.interrupt();
        try {
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setExecutor(Executor executor2) {
        this.executor = executor2;
    }

    /* renamed from: a.gl$d */
    /* compiled from: a */
    class C2558d extends Thread {
        C2558d(String str) {
            super(str);
        }

        public void run() {
            while (LoaderTrail.this.thread == Thread.currentThread()) {
                LoaderTrail.this.mo19099gZ();
                try {
                    sleep(20);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    /* renamed from: a.gl$c */
    /* compiled from: a */
    class C2557c implements Runnable {
        C2557c() {
        }

        public void run() {
            LoaderTrail.this.f7755PM = 0;
            if (LoaderTrail.logger.isDebugEnabled()) {
                LoaderTrail.logger.debug("loading completed");
            }
            if (!LoaderTrail.this.f7751PH.isEmpty()) {
                ArrayList<C5964ado> arrayList = new ArrayList<>();
                arrayList.addAll(LoaderTrail.this.f7751PH);
                for (C5964ado Xy : arrayList) {
                    Xy.mo12895Xy();
                }
            }
        }
    }

    /* renamed from: a.gl$b */
    /* compiled from: a */
    class C2556b implements Runnable {
        private final /* synthetic */ aGU daO;

        C2556b(aGU agu) {
            this.daO = agu;
        }

        public void run() {
            this.daO.boh();
            LoaderTrail.this.m32351vt();
        }
    }

    /* renamed from: a.gl$a */
    class C2555a implements Runnable {
        C2555a() {
        }

        public void run() {
            LoaderTrail.this.m32350vs();
        }
    }
}
