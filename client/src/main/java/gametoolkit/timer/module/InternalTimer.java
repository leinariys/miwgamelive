package gametoolkit.timer.module;

import logic.res.code.SoftTimer;
import logic.WrapRunnable;

import java.security.InvalidParameterException;

/* compiled from: a */
public class InternalTimer implements InternalTimerMBean, Runnable {

    final /* synthetic */ SoftTimer dXj;
    /* renamed from: Df */
    private final boolean f9750Df;
    private final long fLF;
    private final WrapRunnable ghx;
    private final boolean ghz;
    /* access modifiers changed from: private */
    public String name;
    private long ghy = 0;
    private boolean running = false;

    public InternalTimer(SoftTimer abg, String str, WrapRunnable aen, long j, boolean z, boolean z2) {
        this.dXj = abg;
        if (j <= 0) {
            throw new InvalidParameterException("interval must be > 0");
        }
        this.name = str;
        this.ghx = aen;
        this.fLF = j;
        this.ghz = z;
        this.f9750Df = z2;
    }

    public boolean isRunning() {
        return this.running;
    }

    public long getAccumulator() {
        return this.ghy;
    }

    public long getInterval() {
        return this.fLF;
    }

    /* renamed from: ih */
    public boolean mo23540ih(long j) {
        if (this.ghx.isCanceled()) {
            return false;
        }
        if (this.ghx.isReset()) {
            this.ghy = 0;
            this.ghx.setReset(false);
            return false;
        }
        this.ghy += j;
        if (this.ghy >= this.fLF) {
            return true;
        }
        return false;
    }

    public void run() {
        while (this.ghy >= this.fLF) {
            this.ghy -= this.fLF;
            if (!this.ghz && this.ghy >= this.fLF) {
                this.ghy %= this.fLF;
            }
            try {
                this.running = true;
                this.ghx.run();
                if (!this.f9750Df) {
                    return;
                }
            } finally {
                this.running = false;
                if (!this.f9750Df) {
                    this.ghx.cancel();
                }
            }
        }
    }

    public boolean isCanceled() {
        return this.ghx.isCanceled();
    }
}
