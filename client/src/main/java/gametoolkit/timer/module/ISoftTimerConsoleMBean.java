package gametoolkit.timer.module;

import java.util.List;

/* compiled from: a */
public interface ISoftTimerConsoleMBean {
    List<String> getTimers();

    int getTimersCount();
}
