package gametoolkit.client.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import logic.res.scene.C2036bO;
import logic.res.scene.C2593hI;
import logic.res.scene.C3087nb;
import logic.res.scene.C3602sp;

/* compiled from: a */
public class RLightImpl extends RenderObjectImpl implements C2593hI {
    public RLightImpl(long j) {
        super(j);
    }

    public RLightImpl() {
        super(nativeConstructor_());
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RLightImpl(C2593hI hIVar) {
        super(nativeConstructor_RLight(hIVar != null ? hIVar.mo762dE() : 0));
    }

    private static native long nativeConstructor_();

    private static native long nativeConstructor_RLight(long j);

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42057aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native float[] nativeGetAABB(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetCutoff(long j);

    private native float[] nativeGetDiffuseColor(long j);

    private native float[] nativeGetDirection(long j);

    private native float nativeGetExponent(long j);

    private native float[] nativeGetGlobalDirection(long j);

    private native C3602sp nativeGetLight(long j);

    private native boolean nativeGetMainLight(long j);

    private native float nativeGetRadius(long j);

    private native float[] nativeGetSpecularColor(long j);

    private native int nativeGetType(long j);

    private native void nativeSetCutoff(long j, float f);

    private native void nativeSetDiffuseColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetDirection(long j, float f, float f2, float f3);

    private native void nativeSetExponent(long j, float f);

    private native void nativeSetMainLight(long j, boolean z);

    private native void nativeSetRadius(long j, float f);

    private native void nativeSetSpecularColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetType(long j, int i);

    private native void nativeUpdateInternalGeometry(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public int getType() {
        return nativeGetType(this.eAm);
    }

    public void setType(int i) {
        nativeSetType(this.eAm, i);
    }

    public Color getDiffuseColor() {
        return new Color(nativeGetDiffuseColor(this.eAm));
    }

    public void setDiffuseColor(Color color) {
        nativeSetDiffuseColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public Color getSpecularColor() {
        return new Color(nativeGetSpecularColor(this.eAm));
    }

    public void setSpecularColor(Color color) {
        nativeSetSpecularColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public Vec3f getDirection() {
        return new Vec3f(nativeGetDirection(this.eAm));
    }

    public void setDirection(Vec3f vec3f) {
        nativeSetDirection(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    public float getExponent() {
        return nativeGetExponent(this.eAm);
    }

    public void setExponent(float f) {
        nativeSetExponent(this.eAm, f);
    }

    public float getCutoff() {
        return nativeGetCutoff(this.eAm);
    }

    public void setCutoff(float f) {
        nativeSetCutoff(this.eAm, f);
    }

    /* renamed from: yA */
    public C3602sp mo19233yA() {
        return nativeGetLight(this.eAm);
    }

    public float getRadius() {
        return nativeGetRadius(this.eAm);
    }

    public void setRadius(float f) {
        nativeSetRadius(this.eAm, f);
    }

    /* renamed from: dz */
    public BoundingBox mo765dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    public void setMainLight(boolean z) {
        nativeSetMainLight(this.eAm, z);
    }

    /* renamed from: yB */
    public boolean mo19234yB() {
        return nativeGetMainLight(this.eAm);
    }

    public Vec3f getGlobalDirection() {
        return new Vec3f(nativeGetGlobalDirection(this.eAm));
    }

    /* renamed from: b */
    public void mo17267b(C2036bO bOVar) {
        nativeUpdateInternalGeometry(this.eAm, bOVar != null ? bOVar.mo762dE() : 0);
    }
}
