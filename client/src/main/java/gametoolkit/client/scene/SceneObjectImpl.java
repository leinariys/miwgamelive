package gametoolkit.client.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Quat4fWrap;
import logic.res.scene.C2036bO;
import logic.res.scene.C3087nb;
import logic.res.scene.C6000aeY;
import logic.res.scene.C6657arF;

/* compiled from: a */
public abstract class SceneObjectImpl extends C6657arF implements C2036bO {
    public SceneObjectImpl(long j) {
        super(j);
    }

    public SceneObjectImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42323aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeAddChild(long j, long j2);

    private native C3087nb nativeClone(long j);

    private native float[] nativeComputeLocalSpaceAABB(long j);

    private native float[] nativeComputeScaledLocalSpaceAABB(long j);

    private native void nativeComputeWorldSpaceAABB(long j);

    private native void nativeDispose(long j);

    private native float[] nativeGetAABB(long j);

    private native float[] nativeGetAcceleration(long j);

    private native boolean nativeGetAllowSelection(long j);

    private native float[] nativeGetAngularVelocity(long j);

    private native long nativeGetClassSize(long j);

    private native float[] nativeGetDesiredSize(long j);

    private native boolean nativeGetDisposeWhenEmpty(long j);

    private native float[] nativeGetGlobalTransform(long j);

    private native float nativeGetMaxVisibleDistance(long j);

    private native float[] nativeGetPosition(long j);

    private native boolean nativeGetRender(long j);

    private native int nativeGetRenderPriority(long j);

    private native float[] nativeGetScaling(long j);

    private native float[] nativeGetTransform(long j);

    private native void nativeGetTransformWithNoScale(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16);

    private native float[] nativeGetTransformedAABB(long j);

    private native float[] nativeGetVelocity(long j);

    private native void nativeHide(long j);

    private native boolean nativeIsDisposed(long j);

    private native boolean nativeIsHidden(long j);

    private native boolean nativeIsNeedToStep(long j);

    private native boolean nativeIsRayTraceable(long j);

    private native void nativeOnRemove(long j);

    private native boolean nativeRayIntersects_Vec3fVec3fF(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7);

    private native boolean nativeRayIntersects_Vec3fVec3fFZ(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, boolean z);

    private native void nativeReleaseReferences(long j);

    private native void nativeRemoveAllChildren(long j);

    private native void nativeRemoveChild_SceneObject(long j, long j2);

    private native void nativeReset(long j);

    private native void nativeSetAcceleration(long j, float f, float f2, float f3);

    private native void nativeSetAllowSelection(long j, boolean z);

    private native void nativeSetAngularVelocity(long j, float f, float f2, float f3);

    private native void nativeSetChild(long j, int i, long j2);

    private native void nativeSetDesiredSize(long j, float f, float f2, float f3);

    private native void nativeSetDisposeWhenEmpty(long j, boolean z);

    private native void nativeSetMaxVisibleDistance(long j, float f);

    private native void nativeSetOrientation(long j, float f, float f2, float f3, float f4);

    private native void nativeSetPosition_FFF(long j, float f, float f2, float f3);

    private native void nativeSetPosition_Vec3f(long j, float f, float f2, float f3);

    private native void nativeSetRender(long j, boolean z);

    private native void nativeSetRenderPriority(long j, int i);

    private native void nativeSetScaling_F(long j, float f);

    private native void nativeSetScaling_FFF(long j, float f, float f2, float f3);

    private native void nativeSetScaling_Vec3f(long j, float f, float f2, float f3);

    private native void nativeSetTransform_Matrix4f(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16);

    private native void nativeSetTransform_Vec3fQuaternionf(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7);

    private native void nativeSetTransform_Vec3fVec3fVec3fVec3f(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12);

    private native void nativeSetVelocity(long j, float f, float f2, float f3);

    private native void nativeShow(long j);

    private native void nativeUpdateInternalGeometry(long j, long j2);

    private native void nativeWasAddedTo(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: gL */
    public Vec3f mo9992gL() {
        return new Vec3f(nativeGetPosition(this.eAm));
    }

    public Vec3f getVelocity() {
        return new Vec3f(nativeGetVelocity(this.eAm));
    }

    public void setVelocity(Vec3f vec3f) {
        nativeSetVelocity(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: gM */
    public Vec3f mo17269gM() {
        return new Vec3f(nativeGetAcceleration(this.eAm));
    }

    public Matrix4fWrap getTransform() {
        return new Matrix4fWrap(nativeGetTransform(this.eAm));
    }

    public void setTransform(Matrix4fWrap ajk) {
        nativeSetTransform_Matrix4f(this.eAm, ajk.m00, ajk.m10, ajk.m20, ajk.m30, ajk.m01, ajk.m11, ajk.m21, ajk.m31, ajk.m02, ajk.m12, ajk.m22, ajk.m32, ajk.m03, ajk.m13, ajk.m23, ajk.m33);
    }

    /* renamed from: b */
    public void mo17266b(Matrix4fWrap ajk) {
        nativeGetTransformWithNoScale(this.eAm, ajk.m00, ajk.m10, ajk.m20, ajk.m30, ajk.m01, ajk.m11, ajk.m21, ajk.m31, ajk.m02, ajk.m12, ajk.m22, ajk.m32, ajk.m03, ajk.m13, ajk.m23, ajk.m33);
    }

    /* renamed from: gN */
    public Matrix4fWrap mo17270gN() {
        return new Matrix4fWrap(nativeGetGlobalTransform(this.eAm));
    }

    public Vec3f getAngularVelocity() {
        return new Vec3f(nativeGetAngularVelocity(this.eAm));
    }

    public void setAngularVelocity(Vec3f vec3f) {
        nativeSetAngularVelocity(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    public void setPosition(Vec3f vec3f) {
        nativeSetPosition_Vec3f(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    public void setPosition(float f, float f2, float f3) {
        nativeSetPosition_FFF(this.eAm, f, f2, f3);
    }

    public void setTransform(Vec3f vec3f, Quat4fWrap aoy) {
        nativeSetTransform_Vec3fQuaternionf(this.eAm, vec3f.x, vec3f.y, vec3f.z, aoy.w, aoy.x, aoy.y, aoy.z);
    }

    public void setTransform(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
        nativeSetTransform_Vec3fVec3fVec3fVec3f(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, vec3f3.x, vec3f3.y, vec3f3.z, vec3f4.x, vec3f4.y, vec3f4.z);
    }

    public void setOrientation(Quat4fWrap aoy) {
        nativeSetOrientation(this.eAm, aoy.w, aoy.x, aoy.y, aoy.z);
    }

    /* renamed from: e */
    public void mo17268e(Vec3f vec3f) {
        nativeSetAcceleration(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    public void reset() {
        nativeReset(this.eAm);
    }

    public void onRemove() {
        nativeOnRemove(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public void show() {
        nativeShow(this.eAm);
    }

    public void hide() {
        nativeHide(this.eAm);
    }

    public boolean isHidden() {
        return nativeIsHidden(this.eAm);
    }

    public boolean isDisposed() {
        return nativeIsDisposed(this.eAm);
    }

    public void dispose() {
        nativeDispose(this.eAm);
    }

    /* renamed from: a */
    public void mo3599a(float f, float f2, float f3) {
        nativeSetScaling_FFF(this.eAm, f, f2, f3);
    }

    public Vec3f getScaling() {
        return new Vec3f(nativeGetScaling(this.eAm));
    }

    public void setScaling(float f) {
        nativeSetScaling_F(this.eAm, f);
    }

    public void setScaling(Vec3f vec3f) {
        nativeSetScaling_Vec3f(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: gO */
    public BoundingBox mo766gO() {
        return new BoundingBox(nativeGetTransformedAABB(this.eAm));
    }

    /* renamed from: dz */
    public BoundingBox mo765dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    /* renamed from: gP */
    public BoundingBox mo8885gP() {
        return new BoundingBox(nativeComputeLocalSpaceAABB(this.eAm));
    }

    /* renamed from: gQ */
    public BoundingBox mo17271gQ() {
        return new BoundingBox(nativeComputeScaledLocalSpaceAABB(this.eAm));
    }

    public void setRender(boolean z) {
        nativeSetRender(this.eAm, z);
    }

    /* renamed from: gR */
    public boolean mo17272gR() {
        return nativeGetRender(this.eAm);
    }

    public Vec3f getDesiredSize() {
        return new Vec3f(nativeGetDesiredSize(this.eAm));
    }

    public void setDesiredSize(Vec3f vec3f) {
        nativeSetDesiredSize(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: a */
    public void mo13079a(C2036bO bOVar) {
        nativeAddChild(this.eAm, bOVar != null ? bOVar.mo762dE() : 0);
    }

    /* renamed from: a */
    public void mo13078a(int i, C2036bO bOVar) {
        nativeSetChild(this.eAm, i, bOVar != null ? bOVar.mo762dE() : 0);
    }

    /* renamed from: a */
    public void mo17265a(C6000aeY aey) {
        nativeWasAddedTo(this.eAm, aey != null ? aey.mo762dE() : 0);
    }

    public void setAllowSelection(boolean z) {
        nativeSetAllowSelection(this.eAm, z);
    }

    /* renamed from: gS */
    public boolean mo17273gS() {
        return nativeGetAllowSelection(this.eAm);
    }

    public int getRenderPriority() {
        return nativeGetRenderPriority(this.eAm);
    }

    public void setRenderPriority(int i) {
        nativeSetRenderPriority(this.eAm, i);
    }

    public boolean isRayTraceable() {
        return nativeIsRayTraceable(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: a */
    public boolean mo7880a(Vec3f vec3f, Vec3f vec3f2, float f, boolean z) {
        return nativeRayIntersects_Vec3fVec3fFZ(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f, z);
    }

    /* renamed from: a */
    public boolean mo7879a(Vec3f vec3f, Vec3f vec3f2, float f) {
        return nativeRayIntersects_Vec3fVec3fF(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f);
    }

    public boolean isNeedToStep() {
        return nativeIsNeedToStep(this.eAm);
    }

    /* renamed from: gT */
    public void mo2298gT() {
        nativeComputeWorldSpaceAABB(this.eAm);
    }

    /* renamed from: b */
    public void mo17267b(C2036bO bOVar) {
        nativeUpdateInternalGeometry(this.eAm, bOVar != null ? bOVar.mo762dE() : 0);
    }

    /* renamed from: c */
    public void mo13081c(C2036bO bOVar) {
        nativeRemoveChild_SceneObject(this.eAm, bOVar != null ? bOVar.mo762dE() : 0);
    }

    public void removeAllChildren() {
        nativeRemoveAllChildren(this.eAm);
    }

    public void setDisposeWhenEmpty(boolean z) {
        nativeSetDisposeWhenEmpty(this.eAm, z);
    }

    /* renamed from: gU */
    public boolean mo17274gU() {
        return nativeGetDisposeWhenEmpty(this.eAm);
    }

    public float getMaxVisibleDistance() {
        return nativeGetMaxVisibleDistance(this.eAm);
    }

    public void setMaxVisibleDistance(float f) {
        nativeSetMaxVisibleDistance(this.eAm, f);
    }
}
