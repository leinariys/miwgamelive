package gametoolkit.client.scene;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import logic.res.scene.C3087nb;
import logic.res.scene.aMF;

/* compiled from: a */
public class RCameraImpl extends RenderObjectImpl implements aMF {
    public RCameraImpl(long j) {
        super(j);
    }

    public RCameraImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42005aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeBuildFrustum(long j);

    private native void nativeCalculateProjectionMatrix(long j);

    private native C3087nb nativeClone(long j);

    private native float nativeGetAspect(long j);

    private native double nativeGetBottomOrtho(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetEyeSeparation(long j);

    private native float nativeGetFarPlane(long j);

    private native float nativeGetFocalDistance(long j);

    private native float nativeGetFovY(long j);

    private native void nativeGetHudCoords(long j, long j2, float f, float f2, float f3, float f4, float f5, float f6);

    private native boolean nativeGetIsOrtho(long j);

    private native double nativeGetLeftOrtho(long j);

    private native float nativeGetNearPlane(long j);

    private native float[] nativeGetNormViewPortToWorldCoords_Vec3f(long j, float f, float f2, float f3);

    private native void nativeGetNormViewPortToWorldCoords_Vec3fVec3f(long j, float f, float f2, float f3, float f4, float f5, float f6);

    private native float[] nativeGetPosition(long j);

    private native float[] nativeGetProjection(long j);

    private native double nativeGetRightOrtho(long j);

    private native float nativeGetSizeOnScreen(long j, int i, float f, float f2, float f3, float f4);

    private native float nativeGetTanHalfFovY(long j);

    private native double nativeGetTopOrtho(long j);

    private native float[] nativeProjectPoint_Vec3f(long j, float f, float f2, float f3);

    private native void nativeProjectPoint_Vec3fVec3f(long j, float f, float f2, float f3, float f4, float f5, float f6);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetAspect(long j, float f);

    private native void nativeSetEyeSeparation(long j, float f);

    private native void nativeSetFarPlane(long j, float f);

    private native void nativeSetFocalDistance(long j, float f);

    private native void nativeSetFovY(long j, float f);

    private native void nativeSetIsOrtho(long j, boolean z);

    private native void nativeSetNearPlane(long j, float f);

    private native void nativeSetOrthoWindow(long j, double d, double d2, double d3, double d4);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    public float getFovY() {
        return nativeGetFovY(this.eAm);
    }

    public void setFovY(float f) {
        nativeSetFovY(this.eAm, f);
    }

    public float getNearPlane() {
        return nativeGetNearPlane(this.eAm);
    }

    public void setNearPlane(float f) {
        nativeSetNearPlane(this.eAm, f);
    }

    public float getFarPlane() {
        return nativeGetFarPlane(this.eAm);
    }

    public void setFarPlane(float f) {
        nativeSetFarPlane(this.eAm, f);
    }

    public float getAspect() {
        return nativeGetAspect(this.eAm);
    }

    public void setAspect(float f) {
        nativeSetAspect(this.eAm, f);
    }

    /* renamed from: a */
    public float mo9978a(int i, Vec3f vec3f, float f) {
        return nativeGetSizeOnScreen(this.eAm, i, vec3f.x, vec3f.y, vec3f.z, f);
    }

    /* renamed from: b */
    public void mo9980b(double d, double d2, double d3, double d4) {
        nativeSetOrthoWindow(this.eAm, d, d2, d3, d4);
    }

    /* renamed from: hF */
    public void mo9999hF(boolean z) {
        nativeSetIsOrtho(this.eAm, z);
    }

    public boolean cId() {
        return nativeGetIsOrtho(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public double cIe() {
        return nativeGetLeftOrtho(this.eAm);
    }

    public double cIf() {
        return nativeGetRightOrtho(this.eAm);
    }

    public double cIg() {
        return nativeGetBottomOrtho(this.eAm);
    }

    public double cIh() {
        return nativeGetTopOrtho(this.eAm);
    }

    /* renamed from: gL */
    public Vec3f mo9992gL() {
        return new Vec3f(nativeGetPosition(this.eAm));
    }

    public void calculateProjectionMatrix() {
        nativeCalculateProjectionMatrix(this.eAm);
    }

    public Matrix4fWrap getProjection() {
        return new Matrix4fWrap(nativeGetProjection(this.eAm));
    }

    /* renamed from: J */
    public void mo9976J(Vec3f vec3f, Vec3f vec3f2) {
        nativeProjectPoint_Vec3fVec3f(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z);
    }

    /* renamed from: be */
    public Vec3f mo9981be(Vec3f vec3f) {
        return new Vec3f(nativeProjectPoint_Vec3f(this.eAm, vec3f.x, vec3f.y, vec3f.z));
    }

    /* renamed from: a */
    public void mo9979a(aMF amf, Vec3f vec3f, Vec3f vec3f2) {
        nativeGetHudCoords(this.eAm, amf != null ? amf.mo762dE() : 0, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z);
    }

    /* renamed from: K */
    public void mo9977K(Vec3f vec3f, Vec3f vec3f2) {
        nativeGetNormViewPortToWorldCoords_Vec3fVec3f(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z);
    }

    /* renamed from: bf */
    public Vec3f mo9982bf(Vec3f vec3f) {
        return new Vec3f(nativeGetNormViewPortToWorldCoords_Vec3f(this.eAm, vec3f.x, vec3f.y, vec3f.z));
    }

    public void cIi() {
        nativeBuildFrustum(this.eAm);
    }

    public float getTanHalfFovY() {
        return nativeGetTanHalfFovY(this.eAm);
    }

    /* renamed from: lh */
    public void mo10000lh(float f) {
        nativeSetFocalDistance(this.eAm, f);
    }

    public float cIj() {
        return nativeGetFocalDistance(this.eAm);
    }

    /* renamed from: li */
    public void mo10001li(float f) {
        nativeSetEyeSeparation(this.eAm, f);
    }

    public float cIk() {
        return nativeGetEyeSeparation(this.eAm);
    }
}
