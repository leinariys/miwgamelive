package gametoolkit.client.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import logic.res.scene.C3087nb;
import logic.res.scene.C4024yF;

/* compiled from: a */
public class RLineImpl extends RenderObjectImpl implements C4024yF {
    public RLineImpl(long j) {
        super(j);
    }

    public RLineImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42064aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeAddPoint(long j, float f, float f2, float f3);

    private native C3087nb nativeClone(long j);

    private native void nativeCreateCircle_Vec3fVec3f(long j, float f, float f2, float f3, float f4, float f5, float f6);

    private native void nativeCreateCircle_Vec3fVec3fF(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7);

    private native void nativeCreateCircle_Vec3fVec3fFF(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8);

    private native void nativeCreateCircle_Vec3fVec3fFFF(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9);

    private native void nativeCreateCircle_Vec3fVec3fFFFF(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10);

    private native void nativeCreateElipse(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12);

    private native void nativeCreateRectangle(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12);

    private native void nativeCreateSquare(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9);

    private native float[] nativeGetAABB(long j);

    private native long nativeGetClassSize(long j);

    private native int nativeGetGeometryType(long j);

    private native short nativeGetLineStipple(long j);

    private native float nativeGetLineWidth(long j);

    private native float[] nativeGetTransformedAABB(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeResetLine(long j);

    private native void nativeSetDrawBigSegments(long j);

    private native void nativeSetDrawLine(long j);

    private native void nativeSetDrawMediumSegments(long j);

    private native void nativeSetDrawSmallSegments(long j);

    private native void nativeSetGeometryType(long j, int i);

    private native void nativeSetLineStipple(long j, short s);

    private native void nativeSetLineWidth(long j, float f);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: gO */
    public BoundingBox mo766gO() {
        return new BoundingBox(nativeGetTransformedAABB(this.eAm));
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: dz */
    public BoundingBox mo765dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    public void resetLine() {
        nativeResetLine(this.eAm);
    }

    public void addPoint(Vec3f vec3f) {
        nativeAddPoint(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    public float getLineWidth() {
        return nativeGetLineWidth(this.eAm);
    }

    public void setLineWidth(float f) {
        nativeSetLineWidth(this.eAm, f);
    }

    public short getLineStipple() {
        return nativeGetLineStipple(this.eAm);
    }

    public void setLineStipple(short s) {
        nativeSetLineStipple(this.eAm, s);
    }

    /* renamed from: fA */
    public void mo23103fA(int i) {
        nativeSetGeometryType(this.eAm, i);
    }

    public int apZ() {
        return nativeGetGeometryType(this.eAm);
    }

    public void createCircle(Vec3f vec3f, Vec3f vec3f2, float f, float f2, float f3, float f4) {
        nativeCreateCircle_Vec3fVec3fFFFF(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f, f2, f3, f4);
    }

    /* renamed from: a */
    public void mo23095a(Vec3f vec3f, Vec3f vec3f2, float f, float f2, float f3) {
        nativeCreateCircle_Vec3fVec3fFFF(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f, f2, f3);
    }

    /* renamed from: a */
    public void mo23094a(Vec3f vec3f, Vec3f vec3f2, float f, float f2) {
        nativeCreateCircle_Vec3fVec3fFF(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f, f2);
    }

    public void createCircle(Vec3f vec3f, Vec3f vec3f2, float f) {
        nativeCreateCircle_Vec3fVec3fF(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f);
    }

    /* renamed from: g */
    public void mo23104g(Vec3f vec3f, Vec3f vec3f2) {
        nativeCreateCircle_Vec3fVec3f(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z);
    }

    /* renamed from: d */
    public void mo23100d(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        nativeCreateSquare(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, vec3f3.x, vec3f3.y, vec3f3.z);
    }

    /* renamed from: d */
    public void mo23101d(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
        nativeCreateRectangle(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, vec3f3.x, vec3f3.y, vec3f3.z, vec3f4.x, vec3f4.y, vec3f4.z);
    }

    /* renamed from: e */
    public void mo23102e(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
        nativeCreateElipse(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, vec3f3.x, vec3f3.y, vec3f3.z, vec3f4.x, vec3f4.y, vec3f4.z);
    }

    public void setDrawLine() {
        nativeSetDrawLine(this.eAm);
    }

    public void setDrawBigSegments() {
        nativeSetDrawBigSegments(this.eAm);
    }

    public void setDrawMediumSegments() {
        nativeSetDrawMediumSegments(this.eAm);
    }

    public void setDrawSmallSegments() {
        nativeSetDrawSmallSegments(this.eAm);
    }
}
