package gametoolkit.client.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import logic.res.scene.*;

/* compiled from: a */
public class RenderObjectImpl extends SceneObjectImpl implements C6980axt {
    public RenderObjectImpl(long j) {
        super(j);
    }

    public RenderObjectImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42253aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetImpostorBlendingDistance(long j);

    private native float nativeGetImpostorDistance(long j);

    private native boolean nativeGetImpostorEnabled(long j);

    private native C0676Jc nativeGetImpostorRenderTarget(long j);

    private native int nativeGetImpostorResolutionHeight(long j);

    private native int nativeGetImpostorResolutionWidth(long j);

    private native C3038nJ nativeGetMaterial_(long j);

    private native boolean nativeGetOcclusionQueryEnabled(long j);

    private native float[] nativeGetPrimitiveColor(long j);

    private native C3674tb nativeGetShader_(long j);

    private native boolean nativeGetUseLighting(long j);

    private native boolean nativeGetVisible(long j);

    private native float[] nativeGetWorldSpaceAABB(long j);

    private native float[] nativeGetWorldSpaceAABBCenter(long j);

    private native void nativeInvalidateImpostor(long j);

    private native boolean nativeIsImpostorInTransition(long j);

    private native boolean nativeIsRayTraceable(long j);

    private native void nativeSetImpostorBlendingDistance(long j, float f);

    private native void nativeSetImpostorDistance(long j, float f);

    private native void nativeSetImpostorEnabled(long j, boolean z);

    private native void nativeSetLastFrameRendered(long j, int i);

    private native void nativeSetMaterial(long j, long j2);

    private native void nativeSetOcclusionQueryEnabled(long j, boolean z);

    private native void nativeSetPrimitiveColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetShader(long j, long j2);

    private native void nativeSetShaderRecursive(long j, long j2);

    private native void nativeSetUseLighting(long j, boolean z);

    private native void nativeSetUseLightingRecursive(long j, boolean z);

    private native void nativeSetVisible(long j, boolean z);

    private native int native_getLastFrameRendered(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: YU */
    public C3038nJ mo16837YU() {
        return nativeGetMaterial_(this.eAm);
    }

    /* renamed from: eo */
    public C3674tb mo16852eo() {
        return nativeGetShader_(this.eAm);
    }

    /* renamed from: b */
    public void mo9377b(C3038nJ nJVar) {
        nativeSetMaterial(this.eAm, nJVar != null ? nJVar.mo762dE() : 0);
    }

    /* renamed from: a */
    public void mo758a(C3674tb tbVar) {
        nativeSetShader(this.eAm, tbVar != null ? tbVar.mo762dE() : 0);
    }

    /* renamed from: d */
    public void mo16850d(C3674tb tbVar) {
        nativeSetShaderRecursive(this.eAm, tbVar != null ? tbVar.mo762dE() : 0);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public Color getPrimitiveColor() {
        return new Color(nativeGetPrimitiveColor(this.eAm));
    }

    public void setPrimitiveColor(Color color) {
        nativeSetPrimitiveColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public void setUseLighting(boolean z) {
        nativeSetUseLighting(this.eAm, z);
    }

    /* renamed from: bG */
    public void mo8878bG(boolean z) {
        nativeSetUseLightingRecursive(this.eAm, z);
    }

    public boolean anY() {
        return nativeGetUseLighting(this.eAm);
    }

    public boolean getVisible() {
        return nativeGetVisible(this.eAm);
    }

    public void setVisible(boolean z) {
        nativeSetVisible(this.eAm, z);
    }

    public void setImpostorEnabled(boolean z) {
        nativeSetImpostorEnabled(this.eAm, z);
    }

    public boolean anZ() {
        return nativeGetImpostorEnabled(this.eAm);
    }

    /* renamed from: dZ */
    public void mo16851dZ(float f) {
        nativeSetImpostorDistance(this.eAm, f);
    }

    public float aoa() {
        return nativeGetImpostorDistance(this.eAm);
    }

    public float getImpostorBlendingDistance() {
        return nativeGetImpostorBlendingDistance(this.eAm);
    }

    public void setImpostorBlendingDistance(float f) {
        nativeSetImpostorBlendingDistance(this.eAm, f);
    }

    public C0676Jc aob() {
        return nativeGetImpostorRenderTarget(this.eAm);
    }

    public void aoc() {
        nativeInvalidateImpostor(this.eAm);
    }

    public int aod() {
        return nativeGetImpostorResolutionWidth(this.eAm);
    }

    public int aoe() {
        return nativeGetImpostorResolutionHeight(this.eAm);
    }

    public boolean aof() {
        return nativeIsImpostorInTransition(this.eAm);
    }

    public Vec3f aog() {
        return new Vec3f(nativeGetWorldSpaceAABBCenter(this.eAm));
    }

    public BoundingBox aoh() {
        return new BoundingBox(nativeGetWorldSpaceAABB(this.eAm));
    }

    public void setOcclusionQueryEnabled(boolean z) {
        nativeSetOcclusionQueryEnabled(this.eAm, z);
    }

    public boolean aoi() {
        return nativeGetOcclusionQueryEnabled(this.eAm);
    }

    /* renamed from: fy */
    public void mo16853fy(int i) {
        nativeSetLastFrameRendered(this.eAm, i);
    }

    public int aoj() {
        return native_getLastFrameRendered(this.eAm);
    }

    public boolean isRayTraceable() {
        return nativeIsRayTraceable(this.eAm);
    }
}
