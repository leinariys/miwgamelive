package gametoolkit.client.scene;

import logic.res.scene.C2036bO;
import logic.res.scene.C2744jP;
import logic.res.scene.C3087nb;
import logic.res.scene.C6000aeY;

/* compiled from: a */
public abstract class ContainerImpl extends C2744jP implements C6000aeY {
    public ContainerImpl(long j) {
        super(j);
    }

    public ContainerImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m41961aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeAddChild(long j, long j2);

    private native int nativeChildCount(long j);

    private native C3087nb nativeClone(long j);

    private native boolean nativeGetCanRemoveChildren(long j);

    private native C2036bO nativeGetChild(long j, int i);

    private native C2036bO nativeGetChildByName(long j, String str);

    private native long nativeGetClassSize(long j);

    private native boolean nativeHasChild(long j, long j2);

    private native void nativeReleaseReferences(long j);

    private native void nativeRemoveAllChildren(long j);

    private native void nativeRemoveChild_SceneObject(long j, long j2);

    private native void nativeSetCanRemoveChildren(long j, boolean z);

    private native void nativeSetChild(long j, int i, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: d */
    public boolean mo13084d(C2036bO bOVar) {
        return nativeHasChild(this.eAm, bOVar != null ? bOVar.mo762dE() : 0);
    }

    /* renamed from: a */
    public void mo13079a(C2036bO bOVar) {
        nativeAddChild(this.eAm, bOVar != null ? bOVar.mo762dE() : 0);
    }

    /* renamed from: cx */
    public C2036bO mo19920cx(int i) {
        return nativeGetChild(this.eAm, i);
    }

    /* renamed from: a */
    public void mo13078a(int i, C2036bO bOVar) {
        nativeSetChild(this.eAm, i, bOVar != null ? bOVar.mo762dE() : 0);
    }

    public int childCount() {
        return nativeChildCount(this.eAm);
    }

    /* renamed from: c */
    public void mo13081c(C2036bO bOVar) {
        nativeRemoveChild_SceneObject(this.eAm, bOVar != null ? bOVar.mo762dE() : 0);
    }

    public void removeAllChildren() {
        nativeRemoveAllChildren(this.eAm);
    }

    /* renamed from: bg */
    public C2036bO mo13080bg(String str) {
        return nativeGetChildByName(this.eAm, str);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public void setCanRemoveChildren(boolean z) {
        nativeSetCanRemoveChildren(this.eAm, z);
    }

    /* renamed from: Yq */
    public boolean mo13077Yq() {
        return nativeGetCanRemoveChildren(this.eAm);
    }
}
