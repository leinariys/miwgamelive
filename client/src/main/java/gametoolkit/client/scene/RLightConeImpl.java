package gametoolkit.client.scene;

import com.hoplon.geometry.Color;
import logic.res.scene.C1136Qg;
import logic.res.scene.C3087nb;
import logic.res.scene.C5415aIl;

/* compiled from: a */
public class RLightConeImpl extends RenderObjectImpl implements C1136Qg {
    public RLightConeImpl(long j) {
        super(j);
    }

    public RLightConeImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42050aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native C5415aIl nativeGetConeBillboard(long j);

    private native float[] nativeGetConeColor(long j);

    private native C5415aIl nativeGetDotBillboard(long j);

    private native float[] nativeGetDotColor(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetConeBillboard(long j, long j2);

    private native void nativeSetConeColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetDotBillboard(long j, long j2);

    private native void nativeSetDotColor(long j, float f, float f2, float f3, float f4);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: d */
    public void mo5076d(C5415aIl ail) {
        nativeSetConeBillboard(this.eAm, ail != null ? ail.mo762dE() : 0);
    }

    public C5415aIl bpt() {
        return nativeGetConeBillboard(this.eAm);
    }

    /* renamed from: e */
    public void mo5077e(C5415aIl ail) {
        nativeSetDotBillboard(this.eAm, ail != null ? ail.mo762dE() : 0);
    }

    public C5415aIl bpu() {
        return nativeGetDotBillboard(this.eAm);
    }

    /* renamed from: T */
    public void mo5070T(Color color) {
        nativeSetConeColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public Color bpv() {
        return new Color(nativeGetConeColor(this.eAm));
    }

    /* renamed from: U */
    public void mo5071U(Color color) {
        nativeSetDotColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public Color bpw() {
        return new Color(nativeGetDotColor(this.eAm));
    }
}
