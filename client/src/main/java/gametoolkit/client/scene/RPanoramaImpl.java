package gametoolkit.client.scene;

import com.hoplon.geometry.BoundingBox;
import logic.res.scene.C0133Be;
import logic.res.scene.C1891ah;
import logic.res.scene.C3087nb;
import logic.res.scene.C3674tb;

/* compiled from: a */
public class RPanoramaImpl extends RenderObjectImpl implements C0133Be {
    public RPanoramaImpl(long j) {
        super(j);
    }

    public RPanoramaImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42146aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native float[] nativeGetAABB(long j);

    private native long nativeGetClassSize(long j);

    private native C1891ah nativeGetPanorama(long j);

    private native float[] nativeGetTransformedAABB(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetPanorama(long j, long j2);

    private native void nativeSetShader(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: a */
    public void mo758a(C3674tb tbVar) {
        nativeSetShader(this.eAm, tbVar != null ? tbVar.mo762dE() : 0);
    }

    /* renamed from: gO */
    public BoundingBox mo766gO() {
        return new BoundingBox(nativeGetTransformedAABB(this.eAm));
    }

    /* renamed from: dz */
    public BoundingBox mo765dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    /* renamed from: c */
    public void mo761c(C1891ah ahVar) {
        nativeSetPanorama(this.eAm, ahVar != null ? ahVar.mo762dE() : 0);
    }

    public C1891ah aym() {
        return nativeGetPanorama(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }
}
