package gametoolkit.client.scene;

import com.hoplon.geometry.Vec3f;
import logic.res.scene.C2036bO;
import logic.res.scene.C3038nJ;
import logic.res.scene.C6409amR;
import logic.res.scene.C6791atj;

/* compiled from: a */
public class RAimImpl extends RenderObjectImpl implements C6409amR {
    public RAimImpl(long j) {
        super(j);
    }

    public RAimImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m41971aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native long nativeGetClassSize(long j);

    private native float[] nativeGetScreenPosition(long j);

    private native C2036bO nativeGetShip(long j);

    private native C2036bO nativeGetTarget(long j);

    private native void nativeSetMaterial(long j, long j2);

    private native void nativeSetProjectionDistance(long j, float f);

    private native void nativeSetShip_SceneObject(long j, long j2);

    private native void nativeSetShip_SceneObjectScene(long j, long j2, long j3);

    private native void nativeSetTarget(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: m */
    public void mo14832m(C2036bO bOVar) {
        nativeSetShip_SceneObject(this.eAm, bOVar != null ? bOVar.mo762dE() : 0);
    }

    /* renamed from: b */
    public void mo14827b(C2036bO bOVar, C6791atj atj) {
        long j;
        long j2 = 0;
        long j3 = this.eAm;
        if (bOVar != null) {
            j = bOVar.mo762dE();
        } else {
            j = 0;
        }
        if (atj != null) {
            j2 = atj.mo762dE();
        }
        nativeSetShip_SceneObjectScene(j3, j, j2);
    }

    /* renamed from: g */
    public void mo14830g(C2036bO bOVar) {
        nativeSetTarget(this.eAm, bOVar != null ? bOVar.mo762dE() : 0);
    }

    public C2036bO ckb() {
        return nativeGetShip(this.eAm);
    }

    public C2036bO bcU() {
        return nativeGetTarget(this.eAm);
    }

    public Vec3f getScreenPosition() {
        return new Vec3f(nativeGetScreenPosition(this.eAm));
    }

    public void setProjectionDistance(float f) {
        nativeSetProjectionDistance(this.eAm, f);
    }

    /* renamed from: b */
    public void mo9377b(C3038nJ nJVar) {
        nativeSetMaterial(this.eAm, nJVar != null ? nJVar.mo762dE() : 0);
    }
}
