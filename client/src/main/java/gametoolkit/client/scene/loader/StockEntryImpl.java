package gametoolkit.client.scene.loader;

import gametoolkit.client.scene.common.NamedObjectImpl;
import logic.res.scene.C3087nb;
import logic.res.scene.C5419aIp;
import p001a.C1424Ur;
import p001a.C6377all;

@C6377all
@C1424Ur
/* compiled from: a */
public class StockEntryImpl extends NamedObjectImpl implements C5419aIp {
    public StockEntryImpl(long j) {
        super(j);
    }

    public StockEntryImpl() {
        super(nativeConstructor_());
    }

    public StockEntryImpl(long j, String str) {
        super(nativeConstructor_JS(j, str));
    }

    private static native long nativeConstructor_();

    private static native long nativeConstructor_JS(long j, String str);

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42450aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native String nativeGetClonesFrom(long j);

    private native C3087nb nativeGetObject(long j);

    private native long nativeGetStockFileId(long j);

    private native boolean nativeIsComplete(long j);

    private native void nativeSetClonesFrom(long j, String str);

    private native void nativeSetUnloaded(long j, boolean z);

    private native boolean nativeWasUnloaded(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: zZ */
    public C3087nb mo9396zZ() {
        return nativeGetObject(this.eAm);
    }

    public long aDk() {
        return nativeGetStockFileId(this.eAm);
    }

    public boolean isComplete() {
        return nativeIsComplete(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public String getClonesFrom() {
        return nativeGetClonesFrom(this.eAm);
    }

    public void setClonesFrom(String str) {
        nativeSetClonesFrom(this.eAm, str);
    }

    public boolean bSv() {
        return nativeWasUnloaded(this.eAm);
    }

    public void setUnloaded(boolean z) {
        nativeSetUnloaded(this.eAm, z);
    }
}
