package gametoolkit.client.scene.loader;

import logic.res.scene.*;
import p001a.C1424Ur;
import p001a.C6377all;

@C6377all
@C1424Ur
/* compiled from: a */
public class SceneLoaderImpl extends C3156oT implements C5821abB {
    public SceneLoaderImpl(long j) {
        super(j);
    }

    public SceneLoaderImpl() {
        super(nativeConstructor());
    }

    private static native long nativeConstructor();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42425aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C5419aIp nativeAddEntry(long j, long j2, long j3);

    private native void nativeAddFile(long j, long j2);

    private native void nativeAddInclude(long j, long j2, String str);

    private native void nativeDisableExceptions(long j);

    private native void nativeDiscardFiles(long j);

    private native void nativeDiscardPreviousFiles(long j);

    private native int nativeDoTextureCleanup(long j);

    private native void nativeEnableExceptions(long j);

    private native int nativeEntryCount(long j);

    private native int nativeFileCount(long j);

    private native long nativeGetClassSize(long j);

    private native String nativeGetEntryName(long j, int i);

    private native C5419aIp nativeGetEntry_I(long j, int i);

    private native C5419aIp nativeGetEntry_ManagedObject(long j, long j2);

    private native C5419aIp nativeGetEntry_S(long j, String str);

    private native String nativeGetFileName(long j, int i);

    private native C6385alt nativeGetFile_I(long j, int i);

    private native C6385alt nativeGetFile_S(long j, String str);

    private native C3038nJ nativeGetMaterialByName(long j, String str);

    private native String[] nativeGetMissingIncludes(long j);

    private native C3087nb nativeGetStockObject(long j, String str);

    private native C1710ZI nativeGetTextureByName(long j, String str);

    private native int nativeGetTotalImageMemory(long j);

    private native String nativeGetXML(long j, long j2);

    private native boolean nativeLoadFile_S(long j, String str);

    private native boolean nativeLoadFile_S_3BI(long j, String str, byte[] bArr, int i);

    private native C3087nb nativeNewInstance(long j, String str);

    private native boolean nativeReloadEntry(long j, long j2);

    private native void nativeRemoveEntry(long j, long j2);

    private native void nativeRemoveInclude(long j, long j2, String str);

    private native void nativeSetCompleteStockObjects(long j, boolean z);

    private native void nativeSetRootPath(long j, String str);

    private native boolean nativeValidateAllStockEntries(long j);

    private native boolean nativeValidateStockEntry(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public boolean bNG() {
        return nativeValidateAllStockEntries(this.eAm);
    }

    /* renamed from: a */
    public boolean mo12368a(C5419aIp aip) {
        return nativeValidateStockEntry(this.eAm, aip != null ? aip.mo762dE() : 0);
    }

    /* renamed from: if */
    public boolean mo12391if(String str) {
        return nativeLoadFile_S(this.eAm, str);
    }

    /* renamed from: a */
    public boolean mo12369a(String str, byte[] bArr, int i) {
        return nativeLoadFile_S_3BI(this.eAm, str, bArr, i);
    }

    /* renamed from: ig */
    public C3087nb mo12392ig(String str) {
        return nativeGetStockObject(this.eAm, str);
    }

    /* renamed from: ih */
    public C3087nb mo12393ih(String str) {
        return nativeNewInstance(this.eAm, str);
    }

    /* renamed from: ii */
    public C5419aIp mo12394ii(String str) {
        return nativeGetEntry_S(this.eAm, str);
    }

    /* renamed from: b */
    public C5419aIp mo12373b(C3087nb nbVar) {
        return nativeGetEntry_ManagedObject(this.eAm, nbVar != null ? nbVar.mo762dE() : 0);
    }

    /* renamed from: aE */
    public C5419aIp mo20999aE(int i) {
        return nativeGetEntry_I(this.eAm, i);
    }

    /* renamed from: aF */
    public String mo21000aF(int i) {
        return nativeGetEntryName(this.eAm, i);
    }

    public int entryCount() {
        return nativeEntryCount(this.eAm);
    }

    /* renamed from: a */
    public C5419aIp mo12365a(C6385alt alt, C5682aSs ass) {
        long j;
        long j2 = 0;
        long j3 = this.eAm;
        if (alt != null) {
            j = alt.mo762dE();
        } else {
            j = 0;
        }
        if (ass != null) {
            j2 = ass.mo762dE();
        }
        return nativeAddEntry(j3, j, j2);
    }

    /* renamed from: b */
    public void mo12375b(C5419aIp aip) {
        nativeRemoveEntry(this.eAm, aip != null ? aip.mo762dE() : 0);
    }

    public int bNH() {
        return nativeDoTextureCleanup(this.eAm);
    }

    /* renamed from: c */
    public boolean mo12385c(C5419aIp aip) {
        return nativeReloadEntry(this.eAm, aip != null ? aip.mo762dE() : 0);
    }

    public void bNI() {
        nativeDiscardFiles(this.eAm);
    }

    public void bNJ() {
        nativeDiscardPreviousFiles(this.eAm);
    }

    /* renamed from: ij */
    public C6385alt mo12395ij(String str) {
        return nativeGetFile_S(this.eAm, str);
    }

    /* renamed from: dH */
    public C6385alt mo21001dH(int i) {
        return nativeGetFile_I(this.eAm, i);
    }

    /* renamed from: pM */
    public String mo12398pM(int i) {
        return nativeGetFileName(this.eAm, i);
    }

    /* renamed from: UF */
    public int mo20998UF() {
        return nativeFileCount(this.eAm);
    }

    /* renamed from: a */
    public void mo12366a(C6385alt alt) {
        nativeAddFile(this.eAm, alt != null ? alt.mo762dE() : 0);
    }

    /* renamed from: b */
    public String mo12374b(C6385alt alt) {
        return nativeGetXML(this.eAm, alt != null ? alt.mo762dE() : 0);
    }

    /* renamed from: ik */
    public C1710ZI mo12396ik(String str) {
        return nativeGetTextureByName(this.eAm, str);
    }

    /* renamed from: il */
    public C3038nJ mo12397il(String str) {
        return nativeGetMaterialByName(this.eAm, str);
    }

    /* renamed from: a */
    public void mo12367a(C6385alt alt, String str) {
        nativeAddInclude(this.eAm, alt != null ? alt.mo762dE() : 0, str);
    }

    /* renamed from: b */
    public void mo12376b(C6385alt alt, String str) {
        nativeRemoveInclude(this.eAm, alt != null ? alt.mo762dE() : 0, str);
    }

    public String[] bNK() {
        return nativeGetMissingIncludes(this.eAm);
    }

    public void bNL() {
        nativeEnableExceptions(this.eAm);
    }

    public void bNM() {
        nativeDisableExceptions(this.eAm);
    }

    /* renamed from: dY */
    public void mo12389dY(boolean z) {
        nativeSetCompleteStockObjects(this.eAm, z);
    }

    public void setRootPath(String str) {
        nativeSetRootPath(this.eAm, str);
    }

    public int bNN() {
        return nativeGetTotalImageMemory(this.eAm);
    }
}
