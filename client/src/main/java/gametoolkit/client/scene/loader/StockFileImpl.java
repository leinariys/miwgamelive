package gametoolkit.client.scene.loader;

import logic.res.scene.C2351eM;
import logic.res.scene.C5419aIp;
import logic.res.scene.C6385alt;
import p001a.C1424Ur;
import p001a.C6377all;

@C6377all
@C1424Ur
/* compiled from: a */
public class StockFileImpl extends C2351eM implements C6385alt {
    public StockFileImpl(long j) {
        super(j);
    }

    public StockFileImpl() {
        super(nativeConstructor());
    }

    private static native long nativeConstructor();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42454aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeAddInclude(long j, String str);

    private native int nativeEntryCount(long j);

    private native long nativeGetClassSize(long j);

    private native C5419aIp nativeGetEntry(long j, int i);

    private native String nativeGetEntryName(long j, int i);

    private native String[] nativeGetIncludes(long j);

    private native long nativeGetStockFileId(long j);

    private native void nativeRemoveInclude(long j, String str);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public long aDk() {
        return nativeGetStockFileId(this.eAm);
    }

    public String[] aDl() {
        return nativeGetIncludes(this.eAm);
    }

    /* renamed from: aE */
    public C5419aIp mo17998aE(int i) {
        return nativeGetEntry(this.eAm, i);
    }

    /* renamed from: aF */
    public String mo17999aF(int i) {
        return nativeGetEntryName(this.eAm, i);
    }

    public int entryCount() {
        return nativeEntryCount(this.eAm);
    }

    /* renamed from: dS */
    public void mo14752dS(String str) {
        nativeAddInclude(this.eAm, str);
    }

    public void removeInclude(String str) {
        nativeRemoveInclude(this.eAm, str);
    }
}
