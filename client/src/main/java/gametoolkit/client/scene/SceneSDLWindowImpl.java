package gametoolkit.client.scene;

import gametoolkit.client.scene.common.ManagedObjectImpl;
import logic.res.scene.aHU;

/* compiled from: a */
public class SceneSDLWindowImpl extends ManagedObjectImpl implements aHU {
    public SceneSDLWindowImpl(long j) {
        super(j);
    }

    public SceneSDLWindowImpl() {
        super(nativeConstructor());
    }

    private static native long nativeConstructor();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42347aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native float nativeGetAnisotropicFilter(long j);

    private native long nativeGetClassSize(long j);

    private native int nativeGetDynamicTextureRate(long j);

    private native float nativeGetLodQuality(long j);

    private native boolean nativeGetPostProcessingFx(long j);

    private native int nativeGetShaderQuality(long j);

    private native int nativeGetTextureFilter(long j);

    private native int nativeGetTextureQuality(long j);

    private native boolean nativeGetUsePixelLight(long j);

    private native boolean nativeGetUseVBO(long j);

    private native boolean nativeGetWireframeOnly(long j);

    private native boolean nativeInit(long j, int i, int i2, int i3, boolean z);

    private native void nativeSetAnisotropicFilter(long j, float f);

    private native void nativeSetDynamicTextureRate(long j, int i);

    private native void nativeSetLodQuality(long j, float f);

    private native void nativeSetPostProcessingFx(long j, boolean z);

    private native void nativeSetShaderQuality(long j, int i);

    private native void nativeSetTextureFilter(long j, int i);

    private native void nativeSetTextureQuality(long j, int i);

    private native void nativeSetUsePixelLight(long j, boolean z);

    private native void nativeSetUseVBO(long j, boolean z);

    private native void nativeSetWindowIcon(long j, String str, String str2);

    private native void nativeSetWindowTitle(long j, String str);

    private native void nativeSetWireframeOnly(long j, boolean z);

    private native void nativeSwapBuffers(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: a */
    public boolean mo9218a(int i, int i2, int i3, boolean z) {
        return nativeInit(this.eAm, i, i2, i3, z);
    }

    public void swapBuffers() {
        nativeSwapBuffers(this.eAm);
    }

    /* renamed from: e */
    public void mo9224e(String str) {
        nativeSetWindowTitle(this.eAm, str);
    }

    /* renamed from: a */
    public void mo9217a(String str, String str2) {
        nativeSetWindowIcon(this.eAm, str, str2);
    }

    /* renamed from: e */
    public void mo9225e(boolean z) {
        nativeSetPostProcessingFx(this.eAm, z);
    }

    /* renamed from: aX */
    public boolean mo9219aX() {
        return nativeGetPostProcessingFx(this.eAm);
    }

    /* renamed from: f */
    public void mo9226f(boolean z) {
        nativeSetUsePixelLight(this.eAm, z);
    }

    /* renamed from: aY */
    public boolean mo9220aY() {
        return nativeGetUsePixelLight(this.eAm);
    }

    public float getLodQuality() {
        return nativeGetLodQuality(this.eAm);
    }

    public void setLodQuality(float f) {
        nativeSetLodQuality(this.eAm, f);
    }

    public void setWireframeOnly(boolean z) {
        nativeSetWireframeOnly(this.eAm, z);
    }

    /* renamed from: aZ */
    public boolean mo9221aZ() {
        return nativeGetWireframeOnly(this.eAm);
    }

    public int getTextureFilter() {
        return nativeGetTextureFilter(this.eAm);
    }

    public void setTextureFilter(int i) {
        nativeSetTextureFilter(this.eAm, i);
    }

    /* renamed from: i */
    public void mo9233i(int i) {
        nativeSetDynamicTextureRate(this.eAm, i);
    }

    /* renamed from: ba */
    public int mo9222ba() {
        return nativeGetDynamicTextureRate(this.eAm);
    }

    public int getTextureQuality() {
        return nativeGetTextureQuality(this.eAm);
    }

    public void setTextureQuality(int i) {
        nativeSetTextureQuality(this.eAm, i);
    }

    /* renamed from: g */
    public void mo9227g(boolean z) {
        nativeSetUseVBO(this.eAm, z);
    }

    public boolean getUseVBO() {
        return nativeGetUseVBO(this.eAm);
    }

    public void setAnisotropicFilter(float f) {
        nativeSetAnisotropicFilter(this.eAm, f);
    }

    /* renamed from: bb */
    public float mo9223bb() {
        return nativeGetAnisotropicFilter(this.eAm);
    }

    public int getShaderQuality() {
        return nativeGetShaderQuality(this.eAm);
    }

    public void setShaderQuality(int i) {
        nativeSetShaderQuality(this.eAm, i);
    }
}
