package gametoolkit.client.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vector2fWrap;
import logic.res.scene.*;

/* compiled from: a */
public class RMarkImpl extends RenderObjectImpl implements C5929adF {
    public RMarkImpl(long j) {
        super(j);
    }

    public RMarkImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42094aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native float nativeGetBillboardSize(long j);

    private native float[] nativeGetCenterBillboardSize(long j);

    private native float[] nativeGetCenterColor(long j);

    private native long nativeGetClassSize(long j);

    private native C3038nJ nativeGetMark(long j);

    private native float nativeGetMarkSize(long j);

    private native float[] nativeGetScreenPositon(long j);

    private native C2036bO nativeGetTargetObject(long j);

    private native C6791atj nativeGetTargetScene(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetCenterBillboardSize_FF(long j, float f, float f2);

    private native void nativeSetCenterBillboardSize_Vec2f(long j, float f, float f2);

    private native void nativeSetCenterColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetMark(long j, long j2);

    private native void nativeSetMarkSize(long j, float f);

    private native void nativeSetMaxScreenHeigth(long j, float f);

    private native void nativeSetScreenPositon(long j, float f, float f2, float f3);

    private native void nativeSetTarget(long j, long j2, long j3);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    public C3038nJ bQT() {
        return nativeGetMark(this.eAm);
    }

    public C2036bO bQV() {
        return nativeGetTargetObject(this.eAm);
    }

    public C6791atj bRx() {
        return nativeGetTargetScene(this.eAm);
    }

    public Vec3f bRy() {
        return new Vec3f(nativeGetScreenPositon(this.eAm));
    }

    public float getMarkSize() {
        return nativeGetMarkSize(this.eAm);
    }

    public float bRz() {
        return nativeGetBillboardSize(this.eAm);
    }

    /* renamed from: d */
    public void mo12757d(C3038nJ nJVar) {
        nativeSetMark(this.eAm, nJVar != null ? nJVar.mo762dE() : 0);
    }

    /* renamed from: a */
    public void mo12750a(C2036bO bOVar, C6791atj atj) {
        long j;
        long j2 = 0;
        long j3 = this.eAm;
        if (bOVar != null) {
            j = bOVar.mo762dE();
        } else {
            j = 0;
        }
        if (atj != null) {
            j2 = atj.mo762dE();
        }
        nativeSetTarget(j3, j, j2);
    }

    /* renamed from: aw */
    public void mo12751aw(Vec3f vec3f) {
        nativeSetScreenPositon(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: je */
    public void mo12761je(float f) {
        nativeSetMarkSize(this.eAm, f);
    }

    /* renamed from: jf */
    public void mo9079jf(float f) {
        nativeSetMaxScreenHeigth(this.eAm, f);
    }

    public Color getCenterColor() {
        return new Color(nativeGetCenterColor(this.eAm));
    }

    public void setCenterColor(Color color) {
        nativeSetCenterColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: q */
    public void mo12762q(float f, float f2) {
        nativeSetCenterBillboardSize_FF(this.eAm, f, f2);
    }

    public Vector2fWrap getCenterBillboardSize() {
        return new Vector2fWrap(nativeGetCenterBillboardSize(this.eAm));
    }

    public void setCenterBillboardSize(Vector2fWrap aka) {
        nativeSetCenterBillboardSize_Vec2f(this.eAm, aka.x, aka.y);
    }
}
