package gametoolkit.client.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import logic.res.scene.C1891ah;
import logic.res.scene.C3087nb;
import logic.res.scene.aBN;

/* compiled from: a */
public class RMeshImpl extends RenderObjectImpl implements aBN {
    public RMeshImpl(long j) {
        super(j);
    }

    public RMeshImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42103aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native void nativeComputeWorldSpaceAABB(long j);

    private native float[] nativeGetAABB(long j);

    private native long nativeGetClassSize(long j);

    private native C1891ah nativeGetMesh(long j);

    private native float nativeGetMinimumScreenSize(long j);

    private native float[] nativeGetTransformedAABB(long j);

    private native boolean nativeRayIntersects_Vec3fVec3fF(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7);

    private native boolean nativeRayIntersects_Vec3fVec3fFZ(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, boolean z);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetMesh(long j, long j2);

    private native void nativeSetMinimumScreenSize(long j, float f);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: lc */
    public C1891ah mo7883lc() {
        return nativeGetMesh(this.eAm);
    }

    /* renamed from: d */
    public void mo7881d(C1891ah ahVar) {
        nativeSetMesh(this.eAm, ahVar != null ? ahVar.mo762dE() : 0);
    }

    /* renamed from: gT */
    public void mo2298gT() {
        nativeComputeWorldSpaceAABB(this.eAm);
    }

    /* renamed from: gO */
    public BoundingBox mo766gO() {
        return new BoundingBox(nativeGetTransformedAABB(this.eAm));
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: dz */
    public BoundingBox mo765dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    public float getMinimumScreenSize() {
        return nativeGetMinimumScreenSize(this.eAm);
    }

    public void setMinimumScreenSize(float f) {
        nativeSetMinimumScreenSize(this.eAm, f);
    }

    /* renamed from: a */
    public boolean mo7880a(Vec3f vec3f, Vec3f vec3f2, float f, boolean z) {
        return nativeRayIntersects_Vec3fVec3fFZ(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f, z);
    }

    /* renamed from: a */
    public boolean mo7879a(Vec3f vec3f, Vec3f vec3f2, float f) {
        return nativeRayIntersects_Vec3fVec3fF(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f);
    }
}
