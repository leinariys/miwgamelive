package gametoolkit.client.scene;

import com.hoplon.geometry.Vec3f;
import logic.res.scene.C2832kl;
import logic.res.scene.C3087nb;

/* compiled from: a */
public class RChaseCameraImpl extends RSimulatedCameraImpl implements C2832kl {
    public RChaseCameraImpl(long j) {
        super(j);
    }

    public RChaseCameraImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42019aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native float[] nativeGetCameraOffset(long j);

    private native float[] nativeGetCameraTargetOffset(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetFovY(long j);

    private native float nativeGetMaxCameraRelativeOffset(long j);

    private native float nativeGetMaxCameraRotation(long j);

    private native float nativeGetMaxFovY(long j);

    private native float nativeGetMaxShake(long j);

    private native float nativeGetMaxSolidAngularVelocity(long j);

    private native float nativeGetMaxSolidLinearVelocity(long j);

    private native float nativeGetMinFovY(long j);

    private native boolean nativeIsEnableFov(long j);

    private native void nativeSetCameraOffset(long j, float f, float f2, float f3);

    private native void nativeSetCameraTargetOffset(long j, float f, float f2, float f3);

    private native void nativeSetEnableFov(long j, boolean z);

    private native void nativeSetMaxCameraRelativeOffset(long j, float f);

    private native void nativeSetMaxCameraRotation(long j, float f);

    private native void nativeSetMaxFovY(long j, float f);

    private native void nativeSetMaxShake(long j, float f);

    private native void nativeSetMaxSolidAngularVelocity(long j, float f);

    private native void nativeSetMaxSolidLinearVelocity(long j, float f);

    private native void nativeSetMinFovY(long j, float f);

    private native void nativeSetShakeForever(long j, boolean z);

    private native void nativeSetShakeTime(long j, float f);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: p */
    public void mo20129p(Vec3f vec3f) {
        nativeSetCameraOffset(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: JH */
    public Vec3f mo20119JH() {
        return new Vec3f(nativeGetCameraOffset(this.eAm));
    }

    public boolean isEnableFov() {
        return nativeIsEnableFov(this.eAm);
    }

    public void setEnableFov(boolean z) {
        nativeSetEnableFov(this.eAm, z);
    }

    public float getFovY() {
        return nativeGetFovY(this.eAm);
    }

    public float getMaxSolidLinearVelocity() {
        return nativeGetMaxSolidLinearVelocity(this.eAm);
    }

    public void setMaxSolidLinearVelocity(float f) {
        nativeSetMaxSolidLinearVelocity(this.eAm, f);
    }

    public float getMaxSolidAngularVelocity() {
        return nativeGetMaxSolidAngularVelocity(this.eAm);
    }

    public void setMaxSolidAngularVelocity(float f) {
        nativeSetMaxSolidAngularVelocity(this.eAm, f);
    }

    public float getMaxCameraRelativeOffset() {
        return nativeGetMaxCameraRelativeOffset(this.eAm);
    }

    public void setMaxCameraRelativeOffset(float f) {
        nativeSetMaxCameraRelativeOffset(this.eAm, f);
    }

    public float getMaxCameraRotation() {
        return nativeGetMaxCameraRotation(this.eAm);
    }

    public void setMaxCameraRotation(float f) {
        nativeSetMaxCameraRotation(this.eAm, f);
    }

    public float getMaxFovY() {
        return nativeGetMaxFovY(this.eAm);
    }

    public void setMaxFovY(float f) {
        nativeSetMaxFovY(this.eAm, f);
    }

    public float getMinFovY() {
        return nativeGetMinFovY(this.eAm);
    }

    public void setMinFovY(float f) {
        nativeSetMinFovY(this.eAm, f);
    }

    /* renamed from: JI */
    public Vec3f mo20120JI() {
        return new Vec3f(nativeGetCameraTargetOffset(this.eAm));
    }

    /* renamed from: q */
    public void mo20130q(Vec3f vec3f) {
        nativeSetCameraTargetOffset(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    public void setShakeTime(float f) {
        nativeSetShakeTime(this.eAm, f);
    }

    public void setShakeForever(boolean z) {
        nativeSetShakeForever(this.eAm, z);
    }

    public float getMaxShake() {
        return nativeGetMaxShake(this.eAm);
    }

    public void setMaxShake(float f) {
        nativeSetMaxShake(this.eAm, f);
    }
}
