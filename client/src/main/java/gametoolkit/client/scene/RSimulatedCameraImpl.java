package gametoolkit.client.scene;

import com.hoplon.geometry.Vec3f;
import logic.res.scene.C0710KG;
import logic.res.scene.C2036bO;

/* compiled from: a */
public class RSimulatedCameraImpl extends RCameraImpl implements C0710KG {
    public RSimulatedCameraImpl(long j) {
        super(j);
    }

    public RSimulatedCameraImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42217aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeCopyState(long j, long j2);

    private native long nativeGetClassSize(long j);

    private native float[] nativeGetCurrentCameraOffset(long j);

    private native float nativeGetCurrentRotZ(long j);

    private native float nativeGetFilterTimeConstant(long j);

    private native C2036bO nativeGetTarget(long j);

    private native void nativeSetCurrentCameraOffset(long j, float f, float f2, float f3);

    private native void nativeSetCurrentRotZ(long j, float f);

    private native void nativeSetFilterTimeConstant(long j, float f);

    private native void nativeSetTarget(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: g */
    public void mo3481g(C2036bO bOVar) {
        nativeSetTarget(this.eAm, bOVar != null ? bOVar.mo762dE() : 0);
    }

    public C2036bO bcU() {
        return nativeGetTarget(this.eAm);
    }

    public float getCurrentRotZ() {
        return nativeGetCurrentRotZ(this.eAm);
    }

    public void setCurrentRotZ(float f) {
        nativeSetCurrentRotZ(this.eAm, f);
    }

    public Vec3f getCurrentCameraOffset() {
        return new Vec3f(nativeGetCurrentCameraOffset(this.eAm));
    }

    public void setCurrentCameraOffset(Vec3f vec3f) {
        nativeSetCurrentCameraOffset(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    public float getFilterTimeConstant() {
        return nativeGetFilterTimeConstant(this.eAm);
    }

    public void setFilterTimeConstant(float f) {
        nativeSetFilterTimeConstant(this.eAm, f);
    }

    /* renamed from: a */
    public void mo3479a(C0710KG kg) {
        nativeCopyState(this.eAm, kg != null ? kg.mo762dE() : 0);
    }
}
