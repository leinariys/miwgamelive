package gametoolkit.client.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import logic.res.scene.C1710ZI;
import logic.res.scene.C3087nb;
import logic.res.scene.C5739aUx;

/* compiled from: a */
public class RSceneAreaInfoImpl extends SceneObjectImpl implements C5739aUx {
    public RSceneAreaInfoImpl(long j) {
        super(j);
    }

    public RSceneAreaInfoImpl() {
        super(nativeConstructor_());
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RSceneAreaInfoImpl(C5739aUx aux) {
        super(nativeConstructor_RSceneAreaInfo(aux != null ? aux.mo762dE() : 0));
    }

    private static native long nativeConstructor_();

    private static native long nativeConstructor_RSceneAreaInfo(long j);

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42198aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native void nativeComputeWorldSpaceAABB(long j);

    private native float[] nativeGetAABB(long j);

    private native float[] nativeGetAmbientColor(long j);

    private native long nativeGetClassSize(long j);

    private native C1710ZI nativeGetDiffuseCubemap(long j);

    private native float[] nativeGetFogColor(long j);

    private native float nativeGetFogEnd(long j);

    private native float nativeGetFogExp(long j);

    private native float nativeGetFogStart(long j);

    private native int nativeGetFogType(long j);

    private native C1710ZI nativeGetReflectiveCubemap(long j);

    private native float[] nativeGetSize(long j);

    private native boolean nativeIsNeedToStep(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetAmbientColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetDiffuseCubemap(long j, long j2);

    private native void nativeSetFogColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetFogEnd(long j, float f);

    private native void nativeSetFogExp(long j, float f);

    private native void nativeSetFogStart(long j, float f);

    private native void nativeSetFogType(long j, int i);

    private native void nativeSetReflectiveCubemap(long j, long j2);

    private native void nativeSetSize(long j, float f, float f2, float f3);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: dz */
    public BoundingBox mo765dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    public int getFogType() {
        return nativeGetFogType(this.eAm);
    }

    public void setFogType(int i) {
        nativeSetFogType(this.eAm, i);
    }

    public Color getFogColor() {
        return new Color(nativeGetFogColor(this.eAm));
    }

    public void setFogColor(Color color) {
        nativeSetFogColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public float getFogStart() {
        return nativeGetFogStart(this.eAm);
    }

    public void setFogStart(float f) {
        nativeSetFogStart(this.eAm, f);
    }

    public float getFogEnd() {
        return nativeGetFogEnd(this.eAm);
    }

    public void setFogEnd(float f) {
        nativeSetFogEnd(this.eAm, f);
    }

    public float getFogExp() {
        return nativeGetFogExp(this.eAm);
    }

    public void setFogExp(float f) {
        nativeSetFogExp(this.eAm, f);
    }

    public Color getAmbientColor() {
        return new Color(nativeGetAmbientColor(this.eAm));
    }

    public void setAmbientColor(Color color) {
        nativeSetAmbientColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    /* renamed from: aR */
    public void mo11727aR(Vec3f vec3f) {
        nativeSetSize(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    public Vec3f cid() {
        return new Vec3f(nativeGetSize(this.eAm));
    }

    /* renamed from: g */
    public void mo11731g(C1710ZI zi) {
        nativeSetDiffuseCubemap(this.eAm, zi != null ? zi.mo762dE() : 0);
    }

    public C1710ZI cie() {
        return nativeGetDiffuseCubemap(this.eAm);
    }

    /* renamed from: h */
    public void mo11738h(C1710ZI zi) {
        nativeSetReflectiveCubemap(this.eAm, zi != null ? zi.mo762dE() : 0);
    }

    public C1710ZI cif() {
        return nativeGetReflectiveCubemap(this.eAm);
    }

    public boolean isNeedToStep() {
        return nativeIsNeedToStep(this.eAm);
    }

    /* renamed from: gT */
    public void mo2298gT() {
        nativeComputeWorldSpaceAABB(this.eAm);
    }
}
