package gametoolkit.client.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import logic.res.scene.C1891ah;
import logic.res.scene.C2396ep;
import logic.res.scene.C3087nb;

/* compiled from: a */
public class RShapeImpl extends RenderObjectImpl implements C2396ep {
    public RShapeImpl(long j) {
        super(j);
    }

    public RShapeImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42206aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native void nativeComputeWorldSpaceAABB(long j);

    private native float[] nativeGetAABB(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetFinalAngle(long j);

    private native float nativeGetInitialAngle(long j);

    private native float nativeGetInnerRadius(long j);

    private native C1891ah nativeGetMesh(long j);

    private native float nativeGetNumSpiralTurns(long j);

    private native int nativeGetShapeType(long j);

    private native float nativeGetSpiralHeight(long j);

    private native float nativeGetSpiralWidth(long j);

    private native int nativeGetTesselation(long j);

    private native float[] nativeGetTransformedAABB(long j);

    private native boolean nativeRayIntersects_Vec3fVec3fF(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7);

    private native boolean nativeRayIntersects_Vec3fVec3fFZ(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, boolean z);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetFinalAngle(long j, float f);

    private native void nativeSetInitialAngle(long j, float f);

    private native void nativeSetInnerRadius(long j, float f);

    private native void nativeSetNumSpiralTurns(long j, float f);

    private native void nativeSetShapeType(long j, int i);

    private native void nativeSetSpiralHeight(long j, float f);

    private native void nativeSetSpiralWidth(long j, float f);

    private native void nativeSetTesselation(long j, int i);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: lc */
    public C1891ah mo18198lc() {
        return nativeGetMesh(this.eAm);
    }

    /* renamed from: aq */
    public void mo18190aq(int i) {
        nativeSetShapeType(this.eAm, i);
    }

    /* renamed from: ld */
    public int mo18199ld() {
        return nativeGetShapeType(this.eAm);
    }

    public int getTesselation() {
        return nativeGetTesselation(this.eAm);
    }

    public void setTesselation(int i) {
        nativeSetTesselation(this.eAm, i);
    }

    /* renamed from: gO */
    public BoundingBox mo766gO() {
        return new BoundingBox(nativeGetTransformedAABB(this.eAm));
    }

    /* renamed from: dz */
    public BoundingBox mo765dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    public float getInnerRadius() {
        return nativeGetInnerRadius(this.eAm);
    }

    public void setInnerRadius(float f) {
        nativeSetInnerRadius(this.eAm, f);
    }

    public float getInitialAngle() {
        return nativeGetInitialAngle(this.eAm);
    }

    public void setInitialAngle(float f) {
        nativeSetInitialAngle(this.eAm, f);
    }

    public float getFinalAngle() {
        return nativeGetFinalAngle(this.eAm);
    }

    public void setFinalAngle(float f) {
        nativeSetFinalAngle(this.eAm, f);
    }

    /* renamed from: a */
    public boolean mo7880a(Vec3f vec3f, Vec3f vec3f2, float f, boolean z) {
        return nativeRayIntersects_Vec3fVec3fFZ(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f, z);
    }

    /* renamed from: a */
    public boolean mo7879a(Vec3f vec3f, Vec3f vec3f2, float f) {
        return nativeRayIntersects_Vec3fVec3fF(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f);
    }

    public float getNumSpiralTurns() {
        return nativeGetNumSpiralTurns(this.eAm);
    }

    public void setNumSpiralTurns(float f) {
        nativeSetNumSpiralTurns(this.eAm, f);
    }

    public float getSpiralHeight() {
        return nativeGetSpiralHeight(this.eAm);
    }

    public void setSpiralHeight(float f) {
        nativeSetSpiralHeight(this.eAm, f);
    }

    public float getSpiralWidth() {
        return nativeGetSpiralWidth(this.eAm);
    }

    public void setSpiralWidth(float f) {
        nativeSetSpiralWidth(this.eAm, f);
    }

    /* renamed from: gT */
    public void mo2298gT() {
        nativeComputeWorldSpaceAABB(this.eAm);
    }
}
