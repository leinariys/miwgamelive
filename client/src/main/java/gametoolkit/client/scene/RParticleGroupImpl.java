package gametoolkit.client.scene;

import com.hoplon.geometry.Vec3f;
import logic.res.scene.C0747Kg;
import logic.res.scene.C3087nb;

/* compiled from: a */
public class RParticleGroupImpl extends RenderObjectImpl implements C0747Kg {
    public RParticleGroupImpl(long j) {
        super(j);
    }

    public RParticleGroupImpl() {
        super(nativeConstructor_());
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RParticleGroupImpl(C0747Kg kg) {
        super(nativeConstructor_RParticleGroup(kg != null ? kg.mo762dE() : 0));
    }

    private static native long nativeConstructor_();

    private static native long nativeConstructor_RParticleGroup(long j);

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42153aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeReset(long j);

    private native void nativeSetFollowEmitter(long j, boolean z);

    private native void nativeSetGroupScaling(long j, boolean z);

    private native void nativeSetLifeTime(long j, float f);

    private native void nativeSetScaling_F(long j, float f);

    private native void nativeSetScaling_FFF(long j, float f, float f2, float f3);

    private native void nativeSetScaling_Vec3f(long j, float f, float f2, float f3);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    public void reset() {
        nativeReset(this.eAm);
    }

    /* renamed from: a */
    public void mo3599a(float f, float f2, float f3) {
        nativeSetScaling_FFF(this.eAm, f, f2, f3);
    }

    public void setScaling(Vec3f vec3f) {
        nativeSetScaling_Vec3f(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    public void setScaling(float f) {
        nativeSetScaling_F(this.eAm, f);
    }

    public void setLifeTime(float f) {
        nativeSetLifeTime(this.eAm, f);
    }

    /* renamed from: ao */
    public void mo3600ao(boolean z) {
        nativeSetGroupScaling(this.eAm, z);
    }

    public void setFollowEmitter(boolean z) {
        nativeSetFollowEmitter(this.eAm, z);
    }
}
