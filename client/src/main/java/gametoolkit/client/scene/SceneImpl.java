package gametoolkit.client.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vector2fWrap;
import logic.res.scene.*;

/* compiled from: a */
public class SceneImpl extends ContainerImpl implements C6791atj {
    public SceneImpl(long j) {
        super(j);
    }

    public SceneImpl() {
        super(nativeConstructor());
    }

    private static native long nativeConstructor();

    private static native long nativeGetStaticClassInfoPointer();

    private static native String nativeGetVersion();

    /* renamed from: aV */
    public static long m42268aV() {
        return nativeGetStaticClassInfoPointer();
    }

    public static String getVersion() {
        return nativeGetVersion();
    }

    private native void nativeAddChild(long j, long j2);

    private native void nativeAddColorIntensity(long j, float f);

    private native void nativeAddFustrumObject(long j, long j2);

    private native void nativeAddImpostor(long j, long j2);

    private native void nativeAddPostProcessingFX(long j, long j2);

    private native void nativeAddSceneAreaInfo(long j, long j2);

    private native void nativeAddSceneEvent(long j, long j2);

    private native void nativeAddStaticChild(long j, long j2);

    private native void nativeClearSceneEvents(long j);

    private native void nativeCreatePartitioningStructure_(long j);

    private native void nativeCreatePartitioningStructure_FI(long j, float f, int i);

    private native void nativeCreateRenderTarget(long j);

    private native void nativeDisableScene(long j);

    private native void nativeEnableScene(long j);

    private native void nativeEndFrame(long j);

    private native int nativeFustrumObjectCount(long j);

    private native boolean nativeGetAllowImpostors(long j);

    private native float[] nativeGetAmbientLight(long j);

    private native aMF nativeGetCamera(long j);

    private native long nativeGetClassSize(long j);

    private native float[] nativeGetClearColor(long j);

    private native float nativeGetColorIntensity(long j);

    private native long nativeGetCurrentNumDrawCalls(long j);

    private native long nativeGetCurrentNumImpostorsRendered(long j);

    private native long nativeGetCurrentNumImpostorsUpdated(long j);

    private native long nativeGetCurrentNumMaterialBinds(long j);

    private native long nativeGetCurrentNumMeshBinds(long j);

    private native long nativeGetCurrentNumShaderBinds(long j);

    private native long nativeGetCurrentNumTexBinds(long j);

    private native long nativeGetCurrentNumTris(long j);

    private native long nativeGetCurrentTexMemoryBinds(long j);

    private native boolean nativeGetEditMode(long j);

    private native float nativeGetElapsedTimeMilliseconds(long j);

    private native boolean nativeGetForceSimpleShader(long j);

    private native C6980axt nativeGetFustrumObject(long j, int i);

    private native float nativeGetLastRayTraceDistance(long j);

    private native float[] nativeGetLastWorldSpaceRayTraceIntersection(long j);

    private native long nativeGetNumBillboardsRendered(long j);

    private native long nativeGetNumLights(long j);

    private native long nativeGetNumObjectsStepped(long j);

    private native long nativeGetNumTrailsGenerated(long j);

    private native long nativeGetNumTrailsRendered(long j);

    private native long nativeGetNumTrailsStep(long j);

    private native C6100agU nativeGetPostProcessingFX(long j, int i);

    private native boolean nativeGetRender3DAnaglyph(long j);

    private native C0676Jc nativeGetRenderTarget(long j);

    private native C5739aUx nativeGetSceneAreaInfo(long j, float f, float f2, float f3);

    private native C2609hX nativeGetSceneEvent(long j, int i);

    private native C1710ZI nativeGetSceneTexture(long j);

    private native boolean nativeGetShowAABB(long j);

    private native boolean nativeGetShowAxis(long j);

    private native boolean nativeGetShowGrid(long j);

    private native boolean nativeGetShowInfo(long j);

    private native boolean nativeGetShowLightDirection(long j);

    private native boolean nativeGetShowLightScissors(long j);

    private native boolean nativeGetShowLightVolums(long j);

    private native boolean nativeGetShowNormals(long j);

    private native boolean nativeGetShowProfilerInfo(long j);

    private native boolean nativeGetShowWireFrame(long j);

    private native boolean nativeGetUsePostProcessingFx(long j);

    private native boolean nativeGetUseTkdWorld(long j);

    private native boolean nativeIsCullOutSmallObjects(long j);

    private native boolean nativeIsEnabled(long j);

    private native long nativePostProcessingFXCount(long j);

    private native C2036bO nativeRayTraceMouse(long j, float f, float f2, long[] jArr, int i);

    private native C2036bO nativeRayTraceMouseOverScene(long j, float f, float f2);

    private native C2036bO nativeRayTraceMouseOverSceneNoSubChilds_Vec2f(long j, float f, float f2);

    private native C2036bO nativeRayTraceMouseOverSceneNoSubChilds_Vec2f_3JI(long j, float f, float f2, long[] jArr, int i);

    private native C2036bO nativeRayTraceMouseOverSceneNoSubChilds_Vec3fVec3f_3JI(long j, float f, float f2, float f3, float f4, float f5, float f6, long[] jArr, int i);

    private native void nativeReleaseReferences(long j);

    private native void nativeRemoveAllChildren(long j);

    private native void nativeRemoveChild_SceneObject(long j, long j2);

    private native void nativeRemovePostProcessingFX(long j, long j2);

    private native void nativeRemovePostProcessingFXByIndex(long j, int i);

    private native void nativeRemoveSceneAreaInfo(long j, long j2);

    private native void nativeRenderWorld(long j, float f);

    private native long nativeSceneEventCount(long j);

    private native void nativeSetAllowImpostors(long j, boolean z);

    private native void nativeSetAmbientLight(long j, float f, float f2, float f3, float f4);

    private native void nativeSetCamera(long j, long j2);

    private native void nativeSetClearBuffers_ZZ(long j, boolean z, boolean z2);

    private native void nativeSetClearBuffers_ZZZ(long j, boolean z, boolean z2, boolean z3);

    private native void nativeSetClearColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetCullOutSmallObjects(long j, boolean z);

    private native void nativeSetEditMode(long j, boolean z);

    private native void nativeSetForceSimpleShader(long j, boolean z);

    private native void nativeSetGridInfo(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, int i, float f9, int i2);

    private native void nativeSetNeedSorting(long j, boolean z);

    private native void nativeSetRender3DAnaglyph(long j, boolean z);

    private native void nativeSetRenderTarget(long j, long j2);

    private native void nativeSetShowAABB(long j, boolean z);

    private native void nativeSetShowAxis(long j, boolean z);

    private native void nativeSetShowGrid(long j, boolean z);

    private native void nativeSetShowInfo(long j, boolean z);

    private native void nativeSetShowLightDirection(long j, boolean z);

    private native void nativeSetShowLightScissors(long j, boolean z);

    private native void nativeSetShowLightVolums(long j, boolean z);

    private native void nativeSetShowNormals(long j, boolean z);

    private native void nativeSetShowProfilerInfo(long j, boolean z);

    private native void nativeSetShowWireFrame(long j, boolean z);

    private native void nativeSetUsePostProcessingFx(long j, boolean z);

    private native void nativeSetUseTkdWorld(long j, boolean z);

    private native void nativeSetViewport(long j, int i, int i2, int i3, int i4);

    private native void nativeStartFrame(long j);

    private native void nativeStep(long j, float f);

    private native void nativeStepButDontRender(long j, float f);

    private native void nativeStepWorld(long j, float f);

    private native void nativeUseGlowShader(long j, boolean z);

    private native void nativeUseLightShader(long j, boolean z);

    private native boolean nativeUsingGlowShader(long j);

    private native boolean nativeUsingLightShader(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void setViewport(int i, int i2, int i3, int i4) {
        nativeSetViewport(this.eAm, i, i2, i3, i4);
    }

    public void createPartitioningStructure() {
        nativeCreatePartitioningStructure_(this.eAm);
    }

    /* renamed from: e */
    public void mo16201e(float f, int i) {
        nativeCreatePartitioningStructure_FI(this.eAm, f, i);
    }

    /* renamed from: a */
    public void mo13079a(C2036bO bOVar) {
        nativeAddChild(this.eAm, bOVar != null ? bOVar.mo762dE() : 0);
    }

    /* renamed from: l */
    public void mo16229l(C2036bO bOVar) {
        nativeAddStaticChild(this.eAm, bOVar != null ? bOVar.mo762dE() : 0);
    }

    /* renamed from: c */
    public void mo13081c(C2036bO bOVar) {
        nativeRemoveChild_SceneObject(this.eAm, bOVar != null ? bOVar.mo762dE() : 0);
    }

    public void removeAllChildren() {
        nativeRemoveAllChildren(this.eAm);
    }

    public void step(float f) {
        nativeStep(this.eAm, f);
    }

    /* renamed from: jr */
    public void mo16226jr(float f) {
        nativeStepButDontRender(this.eAm, f);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    public void setClearBuffers(boolean z, boolean z2, boolean z3) {
        nativeSetClearBuffers_ZZZ(this.eAm, z, z2, z3);
    }

    /* renamed from: b */
    public void mo16150b(boolean z, boolean z2) {
        nativeSetClearBuffers_ZZ(this.eAm, z, z2);
    }

    /* renamed from: V */
    public void mo16135V(Color color) {
        nativeSetClearColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public Color getClearColor() {
        return new Color(nativeGetClearColor(this.eAm));
    }

    /* renamed from: a */
    public void mo16142a(C6100agU agu) {
        nativeAddPostProcessingFX(this.eAm, agu != null ? agu.mo762dE() : 0);
    }

    /* renamed from: qN */
    public C6100agU mo16230qN(int i) {
        return nativeGetPostProcessingFX(this.eAm, i);
    }

    /* renamed from: qO */
    public void mo16231qO(int i) {
        nativeRemovePostProcessingFXByIndex(this.eAm, i);
    }

    /* renamed from: b */
    public void mo16149b(C6100agU agu) {
        nativeRemovePostProcessingFX(this.eAm, agu != null ? agu.mo762dE() : 0);
    }

    public long bZL() {
        return nativePostProcessingFXCount(this.eAm);
    }

    /* renamed from: a */
    public void mo16143a(C2609hX hXVar) {
        nativeAddSceneEvent(this.eAm, hXVar != null ? hXVar.mo762dE() : 0);
    }

    public void bZM() {
        nativeClearSceneEvents(this.eAm);
    }

    /* renamed from: qP */
    public C2609hX mo16232qP(int i) {
        return nativeGetSceneEvent(this.eAm, i);
    }

    public long bZN() {
        return nativeSceneEventCount(this.eAm);
    }

    /* renamed from: eD */
    public void mo16202eD(boolean z) {
        nativeSetShowWireFrame(this.eAm, z);
    }

    /* renamed from: eE */
    public void mo16203eE(boolean z) {
        nativeSetShowAABB(this.eAm, z);
    }

    /* renamed from: eF */
    public void mo16204eF(boolean z) {
        nativeSetShowNormals(this.eAm, z);
    }

    /* renamed from: eG */
    public void mo16205eG(boolean z) {
        nativeSetShowLightVolums(this.eAm, z);
    }

    /* renamed from: eH */
    public void mo16206eH(boolean z) {
        nativeSetShowInfo(this.eAm, z);
    }

    /* renamed from: eI */
    public void mo16207eI(boolean z) {
        nativeSetShowLightScissors(this.eAm, z);
    }

    /* renamed from: eJ */
    public void mo16208eJ(boolean z) {
        nativeSetShowLightDirection(this.eAm, z);
    }

    /* renamed from: eK */
    public void mo16209eK(boolean z) {
        nativeSetShowProfilerInfo(this.eAm, z);
    }

    public void setShowGrid(boolean z) {
        nativeSetShowGrid(this.eAm, z);
    }

    /* renamed from: eL */
    public void mo16210eL(boolean z) {
        nativeSetShowAxis(this.eAm, z);
    }

    public boolean bZO() {
        return nativeGetShowAxis(this.eAm);
    }

    public boolean bZP() {
        return nativeGetShowGrid(this.eAm);
    }

    public boolean bZQ() {
        return nativeGetShowAABB(this.eAm);
    }

    public boolean bZR() {
        return nativeGetShowInfo(this.eAm);
    }

    public boolean bZS() {
        return nativeGetShowLightDirection(this.eAm);
    }

    public boolean bZT() {
        return nativeGetShowLightScissors(this.eAm);
    }

    public boolean bZU() {
        return nativeGetShowLightVolums(this.eAm);
    }

    public boolean bZV() {
        return nativeGetShowNormals(this.eAm);
    }

    public boolean bZW() {
        return nativeGetShowWireFrame(this.eAm);
    }

    public boolean bZX() {
        return nativeGetShowProfilerInfo(this.eAm);
    }

    /* renamed from: eM */
    public void mo16211eM(boolean z) {
        nativeUseLightShader(this.eAm, z);
    }

    /* renamed from: eN */
    public void mo16212eN(boolean z) {
        nativeUseGlowShader(this.eAm, z);
    }

    public boolean bZY() {
        return nativeUsingLightShader(this.eAm);
    }

    public boolean bZZ() {
        return nativeUsingGlowShader(this.eAm);
    }

    public aMF caa() {
        return nativeGetCamera(this.eAm);
    }

    /* renamed from: a */
    public void mo16140a(aMF amf) {
        nativeSetCamera(this.eAm, amf != null ? amf.mo762dE() : 0);
    }

    /* renamed from: W */
    public void mo16136W(Color color) {
        nativeSetAmbientLight(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public Color cab() {
        return new Color(nativeGetAmbientLight(this.eAm));
    }

    /* renamed from: h */
    public void mo16222h(C6980axt axt) {
        nativeAddImpostor(this.eAm, axt != null ? axt.mo762dE() : 0);
    }

    /* renamed from: i */
    public void mo16223i(C6980axt axt) {
        nativeAddFustrumObject(this.eAm, axt != null ? axt.mo762dE() : 0);
    }

    public int cac() {
        return nativeFustrumObjectCount(this.eAm);
    }

    /* renamed from: eO */
    public void mo16213eO(boolean z) {
        nativeSetForceSimpleShader(this.eAm, z);
    }

    public boolean cad() {
        return nativeGetForceSimpleShader(this.eAm);
    }

    public void cae() {
        nativeCreateRenderTarget(this.eAm);
    }

    public C1710ZI caf() {
        return nativeGetSceneTexture(this.eAm);
    }

    /* renamed from: eP */
    public void mo16214eP(boolean z) {
        nativeSetEditMode(this.eAm, z);
    }

    public boolean getEditMode() {
        return nativeGetEditMode(this.eAm);
    }

    /* renamed from: a */
    public void mo16139a(C0676Jc jc) {
        nativeSetRenderTarget(this.eAm, jc != null ? jc.mo762dE() : 0);
    }

    public C0676Jc cag() {
        return nativeGetRenderTarget(this.eAm);
    }

    /* renamed from: a */
    public C2036bO mo16137a(Vector2fWrap aka, long[] jArr, int i) {
        return nativeRayTraceMouse(this.eAm, aka.x, aka.y, jArr, i);
    }

    /* renamed from: d */
    public C2036bO mo16199d(Vector2fWrap aka) {
        return nativeRayTraceMouseOverScene(this.eAm, aka.x, aka.y);
    }

    /* renamed from: e */
    public C2036bO mo16200e(Vector2fWrap aka) {
        return nativeRayTraceMouseOverSceneNoSubChilds_Vec2f(this.eAm, aka.x, aka.y);
    }

    /* renamed from: b */
    public C2036bO mo16147b(Vector2fWrap aka, long[] jArr, int i) {
        return nativeRayTraceMouseOverSceneNoSubChilds_Vec2f_3JI(this.eAm, aka.x, aka.y, jArr, i);
    }

    /* renamed from: a */
    public C2036bO mo16138a(Vec3f vec3f, Vec3f vec3f2, long[] jArr, int i) {
        return nativeRayTraceMouseOverSceneNoSubChilds_Vec3fVec3f_3JI(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, jArr, i);
    }

    public void cah() {
        nativeDisableScene(this.eAm);
    }

    public void cai() {
        nativeEnableScene(this.eAm);
    }

    public boolean isEnabled() {
        return nativeIsEnabled(this.eAm);
    }

    /* renamed from: qQ */
    public C6980axt mo16233qQ(int i) {
        return nativeGetFustrumObject(this.eAm, i);
    }

    /* renamed from: a */
    public void mo16144a(Color color, Vec3f vec3f, float f, int i, float f2, int i2) {
        nativeSetGridInfo(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), vec3f.x, vec3f.y, vec3f.z, f, i, f2, i2);
    }

    /* renamed from: a */
    public void mo16141a(C5739aUx aux) {
        nativeAddSceneAreaInfo(this.eAm, aux != null ? aux.mo762dE() : 0);
    }

    /* renamed from: b */
    public void mo16148b(C5739aUx aux) {
        nativeRemoveSceneAreaInfo(this.eAm, aux != null ? aux.mo762dE() : 0);
    }

    /* renamed from: ax */
    public C5739aUx mo16146ax(Vec3f vec3f) {
        return nativeGetSceneAreaInfo(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: eQ */
    public void mo16215eQ(boolean z) {
        nativeSetUsePostProcessingFx(this.eAm, z);
    }

    public boolean caj() {
        return nativeGetUsePostProcessingFx(this.eAm);
    }

    public void setAllowImpostors(boolean z) {
        nativeSetAllowImpostors(this.eAm, z);
    }

    public boolean cak() {
        return nativeGetAllowImpostors(this.eAm);
    }

    public long cal() {
        return nativeGetCurrentNumTris(this.eAm);
    }

    public long cam() {
        return nativeGetCurrentNumDrawCalls(this.eAm);
    }

    public long can() {
        return nativeGetCurrentNumMeshBinds(this.eAm);
    }

    public long cao() {
        return nativeGetCurrentNumTexBinds(this.eAm);
    }

    public long cap() {
        return nativeGetCurrentNumMaterialBinds(this.eAm);
    }

    public long caq() {
        return nativeGetCurrentTexMemoryBinds(this.eAm);
    }

    public long car() {
        return nativeGetCurrentNumShaderBinds(this.eAm);
    }

    public long cas() {
        return nativeGetNumLights(this.eAm);
    }

    public long cat() {
        return nativeGetCurrentNumImpostorsRendered(this.eAm);
    }

    public long cau() {
        return nativeGetCurrentNumImpostorsUpdated(this.eAm);
    }

    public long getNumObjectsStepped() {
        return nativeGetNumObjectsStepped(this.eAm);
    }

    public long cav() {
        return nativeGetNumBillboardsRendered(this.eAm);
    }

    public long caw() {
        return nativeGetNumTrailsStep(this.eAm);
    }

    public long cax() {
        return nativeGetNumTrailsGenerated(this.eAm);
    }

    public long cay() {
        return nativeGetNumTrailsRendered(this.eAm);
    }

    public float caz() {
        return nativeGetLastRayTraceDistance(this.eAm);
    }

    public Vec3f caA() {
        return new Vec3f(nativeGetLastWorldSpaceRayTraceIntersection(this.eAm));
    }

    public float caB() {
        return nativeGetElapsedTimeMilliseconds(this.eAm);
    }

    public void addColorIntensity(float f) {
        nativeAddColorIntensity(this.eAm, f);
    }

    public float getColorIntensity() {
        return nativeGetColorIntensity(this.eAm);
    }

    /* renamed from: eR */
    public void mo16216eR(boolean z) {
        nativeSetRender3DAnaglyph(this.eAm, z);
    }

    public boolean caC() {
        return nativeGetRender3DAnaglyph(this.eAm);
    }

    /* renamed from: eS */
    public void mo16217eS(boolean z) {
        nativeSetNeedSorting(this.eAm, z);
    }

    public void setUseTkdWorld(boolean z) {
        nativeSetUseTkdWorld(this.eAm, z);
    }

    public boolean caD() {
        return nativeGetUseTkdWorld(this.eAm);
    }

    public void caE() {
        nativeStartFrame(this.eAm);
    }

    /* renamed from: js */
    public void mo16227js(float f) {
        nativeStepWorld(this.eAm, f);
    }

    /* renamed from: jt */
    public void mo16228jt(float f) {
        nativeRenderWorld(this.eAm, f);
    }

    public void caF() {
        nativeEndFrame(this.eAm);
    }

    public boolean isCullOutSmallObjects() {
        return nativeIsCullOutSmallObjects(this.eAm);
    }

    public void setCullOutSmallObjects(boolean z) {
        nativeSetCullOutSmallObjects(this.eAm, z);
    }
}
