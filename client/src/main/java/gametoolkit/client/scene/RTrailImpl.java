package gametoolkit.client.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vector2fWrap;
import logic.res.scene.C3038nJ;
import logic.res.scene.C3087nb;
import logic.res.scene.azU;

/* compiled from: a */
public class RTrailImpl extends RenderObjectImpl implements azU {
    public RTrailImpl(long j) {
        super(j);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RTrailImpl(azU azu) {
        super(nativeConstructor_RTrail(azu != null ? azu.mo762dE() : 0));
    }

    public RTrailImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeConstructor_RTrail(long j);

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42246aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeAddEntry(long j, float f, float f2, float f3);

    private native void nativeClearEntries(long j);

    private native C3087nb nativeClone(long j);

    private native float[] nativeGetAABB(long j);

    private native C3038nJ nativeGetBillboardMaterial(long j);

    private native float[] nativeGetBillboardSize(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetLifeTime(long j);

    private native float nativeGetMinSpeed(long j);

    private native float nativeGetSpawnDistance(long j);

    private native C3038nJ nativeGetTrailMaterial(long j);

    private native float nativeGetTrailWidth(long j);

    private native boolean nativeIsNeedToStep(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetBillboardMaterial(long j, long j2);

    private native void nativeSetBillboardSize(long j, float f, float f2);

    private native void nativeSetLifeTime(long j, float f);

    private native void nativeSetLifeTimeModifier(long j, float f);

    private native void nativeSetMinSpeed(long j, float f);

    private native void nativeSetSpawnDistance(long j, float f);

    private native void nativeSetTrailMaterial(long j, long j2);

    private native void nativeSetTrailWidth(long j, float f);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public boolean isNeedToStep() {
        return nativeIsNeedToStep(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    public float getMinSpeed() {
        return nativeGetMinSpeed(this.eAm);
    }

    public void setMinSpeed(float f) {
        nativeSetMinSpeed(this.eAm, f);
    }

    public float getSpawnDistance() {
        return nativeGetSpawnDistance(this.eAm);
    }

    public void setSpawnDistance(float f) {
        nativeSetSpawnDistance(this.eAm, f);
    }

    public float getLifeTime() {
        return nativeGetLifeTime(this.eAm);
    }

    public void setLifeTime(float f) {
        nativeSetLifeTime(this.eAm, f);
    }

    public float getTrailWidth() {
        return nativeGetTrailWidth(this.eAm);
    }

    public void setTrailWidth(float f) {
        nativeSetTrailWidth(this.eAm, f);
    }

    public Vector2fWrap getBillboardSize() {
        return new Vector2fWrap(nativeGetBillboardSize(this.eAm));
    }

    public void setBillboardSize(Vector2fWrap aka) {
        nativeSetBillboardSize(this.eAm, aka.x, aka.y);
    }

    /* renamed from: e */
    public void mo17199e(C3038nJ nJVar) {
        nativeSetTrailMaterial(this.eAm, nJVar != null ? nJVar.mo762dE() : 0);
    }

    public C3038nJ ckE() {
        return nativeGetTrailMaterial(this.eAm);
    }

    /* renamed from: f */
    public void mo17200f(C3038nJ nJVar) {
        nativeSetBillboardMaterial(this.eAm, nJVar != null ? nJVar.mo762dE() : 0);
    }

    public C3038nJ ckF() {
        return nativeGetBillboardMaterial(this.eAm);
    }

    public void setLifeTimeModifier(float f) {
        nativeSetLifeTimeModifier(this.eAm, f);
    }

    /* renamed from: dz */
    public BoundingBox mo765dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    /* renamed from: aS */
    public void mo17195aS(Vec3f vec3f) {
        nativeAddEntry(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    public void clearEntries() {
        nativeClearEntries(this.eAm);
    }
}
