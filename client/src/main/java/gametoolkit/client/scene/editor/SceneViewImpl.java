package gametoolkit.client.scene.editor;

import game.geometry.Matrix4fWrap;
import gametoolkit.client.scene.common.ManagedObjectImpl;
import logic.res.scene.C6592aps;
import logic.res.scene.C6791atj;
import p001a.C1424Ur;
import p001a.C6377all;

@C6377all
@C1424Ur
/* compiled from: a */
public class SceneViewImpl extends ManagedObjectImpl implements C6592aps {
    public SceneViewImpl(long j) {
        super(j);
    }

    public SceneViewImpl() {
        super(nativeConstructor());
    }

    private static native long nativeConstructor();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42377aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native boolean nativeBindContext(long j);

    private native boolean nativeGenerateCubemap(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16, long j2, String str, String str2, int i, boolean z);

    private native long nativeGetClassSize(long j);

    private native long nativeGetHrc(long j);

    private native boolean nativeInit(long j, long j2, long j3);

    private native void nativeRecycleWasteBasket(long j);

    private native void nativeReleaseDevices(long j);

    private native boolean nativeRunSceneFrame(long j, long j2, float f);

    private native boolean nativeSaveHighResScreenShot(long j, long j2, String str, int i, int i2);

    private native boolean nativeSaveScreenShot(long j, String str, int i, int i2);

    private native void nativeSetShaderQuality(long j, int i);

    private native void nativeSwapBuffers(long j);

    private native boolean nativeUnbindContext(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void cpC() {
        nativeRecycleWasteBasket(this.eAm);
    }

    public boolean cpD() {
        return nativeBindContext(this.eAm);
    }

    public boolean cpE() {
        return nativeUnbindContext(this.eAm);
    }

    /* renamed from: a */
    public boolean mo15502a(long j, C6592aps aps) {
        return nativeInit(this.eAm, j, aps != null ? aps.mo762dE() : 0);
    }

    /* renamed from: a */
    public boolean mo15504a(C6791atj atj, float f) {
        return nativeRunSceneFrame(this.eAm, atj != null ? atj.mo762dE() : 0, f);
    }

    public void swapBuffers() {
        nativeSwapBuffers(this.eAm);
    }

    public long cpF() {
        return nativeGetHrc(this.eAm);
    }

    /* renamed from: e */
    public boolean mo15510e(String str, int i, int i2) {
        return nativeSaveScreenShot(this.eAm, str, i, i2);
    }

    /* renamed from: a */
    public boolean mo15505a(C6791atj atj, String str, int i, int i2) {
        return nativeSaveHighResScreenShot(this.eAm, atj != null ? atj.mo762dE() : 0, str, i, i2);
    }

    /* renamed from: a */
    public boolean mo15503a(Matrix4fWrap ajk, C6791atj atj, String str, String str2, int i, boolean z) {
        return nativeGenerateCubemap(this.eAm, ajk.m00, ajk.m10, ajk.m20, ajk.m30, ajk.m01, ajk.m11, ajk.m21, ajk.m31, ajk.m02, ajk.m12, ajk.m22, ajk.m32, ajk.m03, ajk.m13, ajk.m23, ajk.m33, atj != null ? atj.mo762dE() : 0, str, str2, i, z);
    }

    public void releaseDevices() {
        nativeReleaseDevices(this.eAm);
    }

    public void setShaderQuality(int i) {
        nativeSetShaderQuality(this.eAm, i);
    }
}
