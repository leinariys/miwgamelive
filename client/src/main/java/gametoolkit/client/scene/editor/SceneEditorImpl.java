package gametoolkit.client.scene.editor;

import gametoolkit.client.scene.common.ManagedObjectImpl;
import logic.res.scene.C1117QQ;
import logic.res.scene.C5821abB;
import logic.res.scene.C6791atj;
import p001a.C1424Ur;
import p001a.C6377all;

@C6377all
@C1424Ur
/* compiled from: a */
public class SceneEditorImpl extends ManagedObjectImpl implements C1117QQ {
    public SceneEditorImpl(long j) {
        super(j);
    }

    public SceneEditorImpl() {
        super(nativeConstructor());
    }

    private static native long nativeConstructor();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42371aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeAddScene(long j, long j2);

    private native long nativeGetClassSize(long j);

    private native C5821abB nativeGetLoader(long j);

    private native boolean nativeInit(long j, long j2);

    private native boolean nativeRunFrame(long j, float f);

    private native boolean nativeRunSceneFrame(long j, long j2, float f);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: fw */
    public boolean mo4991fw(long j) {
        return nativeInit(this.eAm, j);
    }

    /* renamed from: eb */
    public boolean mo4990eb(float f) {
        return nativeRunFrame(this.eAm, f);
    }

    /* renamed from: a */
    public boolean mo4988a(C6791atj atj, float f) {
        return nativeRunSceneFrame(this.eAm, atj != null ? atj.mo762dE() : 0, f);
    }

    /* renamed from: a */
    public void mo4987a(C6791atj atj) {
        nativeAddScene(this.eAm, atj != null ? atj.mo762dE() : 0);
    }

    public C5821abB bqG() {
        return nativeGetLoader(this.eAm);
    }
}
