package gametoolkit.client.scene;

import logic.res.scene.*;

/* compiled from: a */
public class RMark3DImpl extends RenderObjectImpl implements C5957adh {
    public RMark3DImpl(long j) {
        super(j);
    }

    public RMark3DImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42089aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native C3038nJ nativeGetMark(long j);

    private native C5415aIl nativeGetMarkBB(long j);

    private native C2036bO nativeGetTargetObject(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetMark(long j, long j2);

    private native void nativeSetTargetObject(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    public C3038nJ bQT() {
        return nativeGetMark(this.eAm);
    }

    public C5415aIl bQU() {
        return nativeGetMarkBB(this.eAm);
    }

    public C2036bO bQV() {
        return nativeGetTargetObject(this.eAm);
    }

    /* renamed from: d */
    public void mo12889d(C3038nJ nJVar) {
        nativeSetMark(this.eAm, nJVar != null ? nJVar.mo762dE() : 0);
    }

    /* renamed from: k */
    public void mo12890k(C2036bO bOVar) {
        nativeSetTargetObject(this.eAm, bOVar != null ? bOVar.mo762dE() : 0);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }
}
