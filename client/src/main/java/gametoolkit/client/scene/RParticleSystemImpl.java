package gametoolkit.client.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Quat4fWrap;
import logic.res.scene.C3087nb;
import logic.res.scene.C6330akq;

/* compiled from: a */
public class RParticleSystemImpl extends RenderObjectImpl implements C6330akq {
    public RParticleSystemImpl(long j) {
        super(j);
    }

    public RParticleSystemImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42158aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native boolean nativeGetAlignToHorizon(long j);

    private native boolean nativeGetAlignToMovement(long j);

    private native int nativeGetAlignment(long j);

    private native float[] nativeGetAlignmentDirection(long j);

    private native long nativeGetClassSize(long j);

    private native boolean nativeGetFollowEmitter(long j);

    private native boolean nativeGetGroupScaling(long j);

    private native int nativeGetInitialBurst(long j);

    private native float nativeGetInitialDelay(long j);

    private native float nativeGetInitialMaxAngle(long j);

    private native float nativeGetInitialMinAngle(long j);

    private native boolean nativeGetLoop(long j);

    private native int nativeGetMaxParticles(long j);

    private native float nativeGetMaxRotationSpeed(long j);

    private native float nativeGetMinRotationSpeed(long j);

    private native float nativeGetNewPartsSecond(long j);

    private native float[] nativeGetParticlesAcceleration(long j);

    private native float[] nativeGetParticlesColorVariation(long j);

    private native float nativeGetParticlesDump(long j);

    private native float[] nativeGetParticlesFinalColor(long j);

    private native float nativeGetParticlesFinalSize(long j);

    private native float nativeGetParticlesFinalSizeVariation(long j);

    private native float[] nativeGetParticlesInitialColor(long j);

    private native float nativeGetParticlesInitialSize(long j);

    private native float nativeGetParticlesInitialSizeVariation(long j);

    private native float nativeGetParticlesLifeTimeVariation(long j);

    private native float nativeGetParticlesMaxLifeTime(long j);

    private native float[] nativeGetParticlesVelocity(long j);

    private native float[] nativeGetParticlesVelocityVariation(long j);

    private native boolean nativeGetScaleable(long j);

    private native float nativeGetSystemLifeTime(long j);

    private native boolean nativeIsLoop(long j);

    private native boolean nativeIsNeedToStep(long j);

    private native boolean nativeIsScaleable(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeReset(long j);

    private native void nativeSetAlignToHorizon(long j, boolean z);

    private native void nativeSetAlignToMovement(long j, boolean z);

    private native void nativeSetAlignment(long j, int i);

    private native void nativeSetAlignmentDirection(long j, float f, float f2, float f3);

    private native void nativeSetFollowEmitter(long j, boolean z);

    private native void nativeSetGroupScaling(long j, boolean z);

    private native void nativeSetInitialBurst(long j, int i);

    private native void nativeSetInitialDelay(long j, float f);

    private native void nativeSetInitialMaxAngle(long j, float f);

    private native void nativeSetInitialMinAngle(long j, float f);

    private native void nativeSetLoop(long j, boolean z);

    private native void nativeSetMaxParticles(long j, int i);

    private native void nativeSetMaxRotationSpeed(long j, float f);

    private native void nativeSetMinRotationSpeed(long j, float f);

    private native void nativeSetNewPartsSecond(long j, float f);

    private native void nativeSetParticlesAcceleration(long j, float f, float f2, float f3);

    private native void nativeSetParticlesColorVariation(long j, float f, float f2, float f3, float f4);

    private native void nativeSetParticlesDump(long j, float f);

    private native void nativeSetParticlesFinalColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetParticlesFinalSize(long j, float f);

    private native void nativeSetParticlesFinalSizeVariation(long j, float f);

    private native void nativeSetParticlesInitialColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetParticlesInitialSize(long j, float f);

    private native void nativeSetParticlesInitialSizeVariation(long j, float f);

    private native void nativeSetParticlesLifeTimeVariation(long j, float f);

    private native void nativeSetParticlesMaxLifeTime(long j, float f);

    private native void nativeSetParticlesVelocity(long j, float f, float f2, float f3);

    private native void nativeSetParticlesVelocityVariation(long j, float f, float f2, float f3);

    private native void nativeSetPosition_FFF(long j, float f, float f2, float f3);

    private native void nativeSetPosition_Vec3f(long j, float f, float f2, float f3);

    private native void nativeSetScaleable(long j, boolean z);

    private native void nativeSetSystemLifeTime(long j, float f);

    private native void nativeSetTransform_Matrix4f(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16);

    private native void nativeSetTransform_Vec3fQuaternionf(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7);

    private native void nativeSetTransform_Vec3fVec3fVec3fVec3f(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public void reset() {
        nativeReset(this.eAm);
    }

    public int getMaxParticles() {
        return nativeGetMaxParticles(this.eAm);
    }

    public void setMaxParticles(int i) {
        nativeSetMaxParticles(this.eAm, i);
    }

    public float getNewPartsSecond() {
        return nativeGetNewPartsSecond(this.eAm);
    }

    public void setNewPartsSecond(float f) {
        nativeSetNewPartsSecond(this.eAm, f);
    }

    public float getParticlesInitialSize() {
        return nativeGetParticlesInitialSize(this.eAm);
    }

    public void setParticlesInitialSize(float f) {
        nativeSetParticlesInitialSize(this.eAm, f);
    }

    public float getParticlesFinalSize() {
        return nativeGetParticlesFinalSize(this.eAm);
    }

    public void setParticlesFinalSize(float f) {
        nativeSetParticlesFinalSize(this.eAm, f);
    }

    /* renamed from: m */
    public void mo14565m(Vec3f vec3f) {
        nativeSetParticlesVelocity(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: IV */
    public Vec3f mo14529IV() {
        return new Vec3f(nativeGetParticlesVelocity(this.eAm));
    }

    /* renamed from: n */
    public void mo14566n(Vec3f vec3f) {
        nativeSetParticlesAcceleration(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: IW */
    public Vec3f mo14530IW() {
        return new Vec3f(nativeGetParticlesAcceleration(this.eAm));
    }

    public Color getParticlesInitialColor() {
        return new Color(nativeGetParticlesInitialColor(this.eAm));
    }

    public void setParticlesInitialColor(Color color) {
        nativeSetParticlesInitialColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public Color getParticlesFinalColor() {
        return new Color(nativeGetParticlesFinalColor(this.eAm));
    }

    public void setParticlesFinalColor(Color color) {
        nativeSetParticlesFinalColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    /* renamed from: o */
    public void mo14567o(Vec3f vec3f) {
        nativeSetParticlesVelocityVariation(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: IX */
    public Vec3f mo14531IX() {
        return new Vec3f(nativeGetParticlesVelocityVariation(this.eAm));
    }

    public Color getParticlesColorVariation() {
        return new Color(nativeGetParticlesColorVariation(this.eAm));
    }

    public void setParticlesColorVariation(Color color) {
        nativeSetParticlesColorVariation(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public float getParticlesMaxLifeTime() {
        return nativeGetParticlesMaxLifeTime(this.eAm);
    }

    public void setParticlesMaxLifeTime(float f) {
        nativeSetParticlesMaxLifeTime(this.eAm, f);
    }

    public float getParticlesLifeTimeVariation() {
        return nativeGetParticlesLifeTimeVariation(this.eAm);
    }

    public void setParticlesLifeTimeVariation(float f) {
        nativeSetParticlesLifeTimeVariation(this.eAm, f);
    }

    public float getParticlesInitialSizeVariation() {
        return nativeGetParticlesInitialSizeVariation(this.eAm);
    }

    public void setParticlesInitialSizeVariation(float f) {
        nativeSetParticlesInitialSizeVariation(this.eAm, f);
    }

    public float getParticlesFinalSizeVariation() {
        return nativeGetParticlesFinalSizeVariation(this.eAm);
    }

    public void setParticlesFinalSizeVariation(float f) {
        nativeSetParticlesFinalSizeVariation(this.eAm, f);
    }

    public int getInitialBurst() {
        return nativeGetInitialBurst(this.eAm);
    }

    public void setInitialBurst(int i) {
        nativeSetInitialBurst(this.eAm, i);
    }

    public float getSystemLifeTime() {
        return nativeGetSystemLifeTime(this.eAm);
    }

    public void setSystemLifeTime(float f) {
        nativeSetSystemLifeTime(this.eAm, f);
    }

    public void setAlignToMovement(boolean z) {
        nativeSetAlignToMovement(this.eAm, z);
    }

    /* renamed from: IY */
    public boolean mo14532IY() {
        return nativeGetAlignToMovement(this.eAm);
    }

    /* renamed from: IZ */
    public float mo14533IZ() {
        return nativeGetParticlesDump(this.eAm);
    }

    /* renamed from: aS */
    public void mo14541aS(float f) {
        nativeSetParticlesDump(this.eAm, f);
    }

    public void setAlignToHorizon(boolean z) {
        nativeSetAlignToHorizon(this.eAm, z);
    }

    /* renamed from: Ja */
    public boolean mo14534Ja() {
        return nativeGetAlignToHorizon(this.eAm);
    }

    public float getInitialMinAngle() {
        return nativeGetInitialMinAngle(this.eAm);
    }

    public void setInitialMinAngle(float f) {
        nativeSetInitialMinAngle(this.eAm, f);
    }

    public float getInitialMaxAngle() {
        return nativeGetInitialMaxAngle(this.eAm);
    }

    public void setInitialMaxAngle(float f) {
        nativeSetInitialMaxAngle(this.eAm, f);
    }

    public float getInitialDelay() {
        return nativeGetInitialDelay(this.eAm);
    }

    public void setInitialDelay(float f) {
        nativeSetInitialDelay(this.eAm, f);
    }

    public float getMinRotationSpeed() {
        return nativeGetMinRotationSpeed(this.eAm);
    }

    public void setMinRotationSpeed(float f) {
        nativeSetMinRotationSpeed(this.eAm, f);
    }

    public float getMaxRotationSpeed() {
        return nativeGetMaxRotationSpeed(this.eAm);
    }

    public void setMaxRotationSpeed(float f) {
        nativeSetMaxRotationSpeed(this.eAm, f);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    public void setPosition(Vec3f vec3f) {
        nativeSetPosition_Vec3f(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    public void setPosition(float f, float f2, float f3) {
        nativeSetPosition_FFF(this.eAm, f, f2, f3);
    }

    public void setTransform(Matrix4fWrap ajk) {
        nativeSetTransform_Matrix4f(this.eAm, ajk.m00, ajk.m10, ajk.m20, ajk.m30, ajk.m01, ajk.m11, ajk.m21, ajk.m31, ajk.m02, ajk.m12, ajk.m22, ajk.m32, ajk.m03, ajk.m13, ajk.m23, ajk.m33);
    }

    public void setTransform(Vec3f vec3f, Quat4fWrap aoy) {
        nativeSetTransform_Vec3fQuaternionf(this.eAm, vec3f.x, vec3f.y, vec3f.z, aoy.w, aoy.x, aoy.y, aoy.z);
    }

    public void setTransform(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
        nativeSetTransform_Vec3fVec3fVec3fVec3f(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, vec3f3.x, vec3f3.y, vec3f3.z, vec3f4.x, vec3f4.y, vec3f4.z);
    }

    public int getAlignment() {
        return nativeGetAlignment(this.eAm);
    }

    public void setAlignment(int i) {
        nativeSetAlignment(this.eAm, i);
    }

    public Vec3f getAlignmentDirection() {
        return new Vec3f(nativeGetAlignmentDirection(this.eAm));
    }

    public void setAlignmentDirection(Vec3f vec3f) {
        nativeSetAlignmentDirection(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    public void setFollowEmitter(boolean z) {
        nativeSetFollowEmitter(this.eAm, z);
    }

    /* renamed from: Jb */
    public boolean mo14535Jb() {
        return nativeGetFollowEmitter(this.eAm);
    }

    /* renamed from: ao */
    public void mo14542ao(boolean z) {
        nativeSetGroupScaling(this.eAm, z);
    }

    /* renamed from: Jc */
    public boolean mo14536Jc() {
        return nativeGetGroupScaling(this.eAm);
    }

    /* renamed from: ap */
    public void mo14543ap(boolean z) {
        nativeSetLoop(this.eAm, z);
    }

    /* renamed from: Jd */
    public boolean mo14537Jd() {
        return nativeGetLoop(this.eAm);
    }

    /* renamed from: Je */
    public boolean mo14538Je() {
        return nativeIsLoop(this.eAm);
    }

    /* renamed from: aq */
    public void mo14544aq(boolean z) {
        nativeSetScaleable(this.eAm, z);
    }

    /* renamed from: Jf */
    public boolean mo14539Jf() {
        return nativeGetScaleable(this.eAm);
    }

    /* renamed from: Jg */
    public boolean mo14540Jg() {
        return nativeIsScaleable(this.eAm);
    }

    public boolean isNeedToStep() {
        return nativeIsNeedToStep(this.eAm);
    }
}
