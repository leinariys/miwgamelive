package gametoolkit.client.scene;

import logic.res.scene.C3087nb;
import logic.res.scene.C5436aJg;
import logic.res.scene.aKQ;

/* compiled from: a */
public class RAnimationImpl extends SceneObjectImpl implements C5436aJg {
    public RAnimationImpl(long j) {
        super(j);
    }

    public RAnimationImpl() {
        super(nativeConstructor_());
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RAnimationImpl(C5436aJg ajg) {
        super(nativeConstructor_RAnimation(ajg != null ? ajg.mo762dE() : 0));
    }

    private static native long nativeConstructor_();

    private static native long nativeConstructor_RAnimation(long j);

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m41977aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native float nativeGetAnimationSpeed(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetFadeTime(long j);

    private native float nativeGetLocalElapsedTimeRatio(long j);

    private native float nativeGetLocalElapsedTimeSeconds(long j);

    private native float nativeGetLocalTotalTimeSeconds(long j);

    private native aKQ nativeGetModel(long j);

    private native boolean nativeGetRemoveOnEnd(long j);

    private native boolean nativeIsNeedToStep(long j);

    private native boolean nativeIsPlaying(long j);

    private native void nativePlay_(long j);

    private native void nativePlay_I(long j, int i);

    private native void nativeReleaseReferences(long j);

    private native void nativeReset(long j);

    private native void nativeSetAnimationSpeed(long j, float f);

    private native void nativeSetFadeTime(long j, float f);

    private native void nativeSetModel(long j, long j2);

    private native void nativeSetRemoveOnEnd(long j, boolean z);

    private native void nativeStop(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public boolean isNeedToStep() {
        return nativeIsNeedToStep(this.eAm);
    }

    /* renamed from: a */
    public void mo9603a(aKQ akq) {
        nativeSetModel(this.eAm, akq != null ? akq.mo762dE() : 0);
    }

    public aKQ chX() {
        return nativeGetModel(this.eAm);
    }

    public void play(int i) {
        nativePlay_I(this.eAm, i);
    }

    public void play() {
        nativePlay_(this.eAm);
    }

    public void stop() {
        nativeStop(this.eAm);
    }

    public boolean isPlaying() {
        return nativeIsPlaying(this.eAm);
    }

    public void reset() {
        nativeReset(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public float getFadeTime() {
        return nativeGetFadeTime(this.eAm);
    }

    public void setFadeTime(float f) {
        nativeSetFadeTime(this.eAm, f);
    }

    public float getAnimationSpeed() {
        return nativeGetAnimationSpeed(this.eAm);
    }

    public void setAnimationSpeed(float f) {
        nativeSetAnimationSpeed(this.eAm, f);
    }

    public void setRemoveOnEnd(boolean z) {
        nativeSetRemoveOnEnd(this.eAm, z);
    }

    public boolean chY() {
        return nativeGetRemoveOnEnd(this.eAm);
    }

    public float chZ() {
        return nativeGetLocalTotalTimeSeconds(this.eAm);
    }

    public float cia() {
        return nativeGetLocalElapsedTimeSeconds(this.eAm);
    }

    public float cib() {
        return nativeGetLocalElapsedTimeRatio(this.eAm);
    }
}
