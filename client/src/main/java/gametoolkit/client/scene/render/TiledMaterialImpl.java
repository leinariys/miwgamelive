package gametoolkit.client.scene.render;

import com.hoplon.geometry.Vec3f;
import logic.res.scene.C1525WR;
import logic.res.scene.C3087nb;

/* compiled from: a */
public class TiledMaterialImpl extends ExtendedMaterialImpl implements C1525WR {
    public TiledMaterialImpl(long j) {
        super(j);
    }

    public TiledMaterialImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42693aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native float[] nativeGetTile(long j);

    private native void nativeSetTile(long j, float f, float f2, float f3);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public Vec3f getTile() {
        return new Vec3f(nativeGetTile(this.eAm));
    }

    public void setTile(Vec3f vec3f) {
        nativeSetTile(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }
}
