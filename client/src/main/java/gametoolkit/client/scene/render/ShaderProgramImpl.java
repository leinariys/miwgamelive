package gametoolkit.client.scene.render;

import gametoolkit.client.scene.common.NamedObjectImpl;
import logic.res.scene.*;

/* compiled from: a */
public class ShaderProgramImpl extends NamedObjectImpl implements C2634hp {
    public ShaderProgramImpl(long j) {
        super(j);
    }

    public ShaderProgramImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42649aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeAddShaderProgramObject(long j, long j2);

    private native void nativeBind(long j, long j2);

    private native void nativeClear(long j);

    private native C3087nb nativeClone(long j);

    private native boolean nativeCompile(long j, long j2);

    private native long nativeGetClassSize(long j);

    private native C2724jE nativeGetManualParameter(long j, int i);

    private native int nativeGetManualParamsCount(long j);

    private native C6498aoC nativeGetShaderProgramObject(long j, int i);

    private native int nativeGetTangentChannel(long j);

    private native boolean nativeGetUseDiffuseCubemap(long j);

    private native boolean nativeGetUseNormalArray(long j);

    private native boolean nativeGetUseReflectCubemap(long j);

    private native boolean nativeGetUseTangentArray(long j);

    private native boolean nativeIsProgramCompiled(long j);

    private native boolean nativeLinkObjects(long j, long j2);

    private native void nativeSetShaderProgramObject(long j, int i, long j2);

    private native int nativeShaderProgramObjectCount(long j);

    private native void nativeUnbind(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: a */
    public void mo19385a(C6498aoC aoc) {
        nativeAddShaderProgramObject(this.eAm, aoc != null ? aoc.mo762dE() : 0);
    }

    public int shaderProgramObjectCount() {
        return nativeShaderProgramObjectCount(this.eAm);
    }

    /* renamed from: bp */
    public C6498aoC mo19387bp(int i) {
        return nativeGetShaderProgramObject(this.eAm, i);
    }

    /* renamed from: a */
    public void mo19384a(int i, C6498aoC aoc) {
        nativeSetShaderProgramObject(this.eAm, i, aoc != null ? aoc.mo762dE() : 0);
    }

    /* renamed from: b */
    public boolean mo19386b(C4076yy yyVar) {
        return nativeLinkObjects(this.eAm, yyVar != null ? yyVar.mo762dE() : 0);
    }

    /* renamed from: c */
    public void mo19389c(C4076yy yyVar) {
        nativeBind(this.eAm, yyVar != null ? yyVar.mo762dE() : 0);
    }

    public void unbind() {
        nativeUnbind(this.eAm);
    }

    /* renamed from: d */
    public boolean mo19391d(C4076yy yyVar) {
        return nativeCompile(this.eAm, yyVar != null ? yyVar.mo762dE() : 0);
    }

    /* renamed from: yp */
    public boolean mo19398yp() {
        return nativeIsProgramCompiled(this.eAm);
    }

    /* renamed from: yq */
    public boolean mo19399yq() {
        return nativeGetUseTangentArray(this.eAm);
    }

    public int getTangentChannel() {
        return nativeGetTangentChannel(this.eAm);
    }

    public int getManualParamsCount() {
        return nativeGetManualParamsCount(this.eAm);
    }

    /* renamed from: bq */
    public C2724jE mo19388bq(int i) {
        return nativeGetManualParameter(this.eAm, i);
    }

    public void clear() {
        nativeClear(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public boolean getUseReflectCubemap() {
        return nativeGetUseReflectCubemap(this.eAm);
    }

    public boolean getUseDiffuseCubemap() {
        return nativeGetUseDiffuseCubemap(this.eAm);
    }

    /* renamed from: yr */
    public boolean mo19400yr() {
        return nativeGetUseNormalArray(this.eAm);
    }
}
