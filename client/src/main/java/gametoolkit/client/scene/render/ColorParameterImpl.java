package gametoolkit.client.scene.render;

import com.hoplon.geometry.Color;
import logic.res.scene.C1347Tf;
import logic.res.scene.C2724jE;
import logic.res.scene.C3087nb;

/* compiled from: a */
public class ColorParameterImpl extends ShaderParameterImpl implements C1347Tf {
    public ColorParameterImpl(long j) {
        super(j);
    }

    public ColorParameterImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42475aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native float[] nativeGetValue(long j);

    private native void nativeSetValue_Color(long j, float f, float f2, float f3, float f4);

    private native void nativeSetValue_ShaderParameter(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: b */
    public void mo1883b(C2724jE jEVar) {
        nativeSetValue_ShaderParameter(this.eAm, jEVar != null ? jEVar.mo762dE() : 0);
    }

    public Color getValue() {
        return new Color(nativeGetValue(this.eAm));
    }

    public void setValue(Color color) {
        nativeSetValue_Color(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }
}
