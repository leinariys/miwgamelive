package gametoolkit.client.scene.render;

import com.hoplon.geometry.Color;
import game.geometry.Matrix4fWrap;
import game.geometry.Vector2fWrap;
import logic.res.scene.*;

/* compiled from: a */
public class MaterialImpl extends ResourceImpl implements C3038nJ {
    public MaterialImpl(long j) {
        super(j);
    }

    public MaterialImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42550aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeAddShaderParameter(long j, long j2);

    private native void nativeAddTexture(long j, long j2);

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native C1710ZI nativeGetDiffuseDetailTexture(long j);

    private native C1710ZI nativeGetDiffuseTexture(long j);

    private native float[] nativeGetDiffuseTextureTexelSize(long j);

    private native C1710ZI nativeGetNormalTexture(long j);

    private native float[] nativeGetScaling(long j);

    private native C1710ZI nativeGetSelfIluminationTexture(long j);

    private native C3674tb nativeGetShader(long j);

    private native C2724jE nativeGetShaderParameter(long j, int i);

    private native float nativeGetShininess(long j);

    private native float[] nativeGetSpecular(long j);

    private native C1710ZI nativeGetTexture(long j, int i);

    private native float[] nativeGetTransform(long j);

    private native void nativeImportShaderParametersFromShader(long j);

    private native int nativeMaxTextureCount(long j, int i);

    private native void nativeSetDiffuseDetailTexture_ITexture(long j, int i, long j2);

    private native void nativeSetDiffuseDetailTexture_Texture(long j, long j2);

    private native void nativeSetDiffuseTexture_ITexture(long j, int i, long j2);

    private native void nativeSetDiffuseTexture_Texture(long j, long j2);

    private native void nativeSetNormalTexture_ITexture(long j, int i, long j2);

    private native void nativeSetNormalTexture_Texture(long j, long j2);

    private native void nativeSetSelfIluminationTexture_ITexture(long j, int i, long j2);

    private native void nativeSetSelfIluminationTexture_Texture(long j, long j2);

    private native void nativeSetShader(long j, long j2);

    private native void nativeSetShaderParameter(long j, int i, long j2);

    private native void nativeSetShininess(long j, float f);

    private native void nativeSetSpecular(long j, float f, float f2, float f3, float f4);

    private native void nativeSetTexture(long j, int i, long j2);

    private native void nativeSetTransform(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16);

    private native int nativeShaderParameterCount(long j);

    private native int nativeTextureCount(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void importShaderParametersFromShader() {
        nativeImportShaderParametersFromShader(this.eAm);
    }

    /* renamed from: z */
    public C2724jE mo20715z(int i) {
        return nativeGetShaderParameter(this.eAm, i);
    }

    /* renamed from: a */
    public void mo20687a(int i, C2724jE jEVar) {
        nativeSetShaderParameter(this.eAm, i, jEVar != null ? jEVar.mo762dE() : 0);
    }

    public int shaderParameterCount() {
        return nativeShaderParameterCount(this.eAm);
    }

    /* renamed from: a */
    public void mo20689a(C2724jE jEVar) {
        nativeAddShaderParameter(this.eAm, jEVar != null ? jEVar.mo762dE() : 0);
    }

    /* renamed from: a */
    public void mo20690a(C3674tb tbVar) {
        nativeSetShader(this.eAm, tbVar != null ? tbVar.mo762dE() : 0);
    }

    /* renamed from: eo */
    public C3674tb mo20699eo() {
        return nativeGetShader(this.eAm);
    }

    public Matrix4fWrap getTransform() {
        return new Matrix4fWrap(nativeGetTransform(this.eAm));
    }

    public void setTransform(Matrix4fWrap ajk) {
        nativeSetTransform(this.eAm, ajk.m00, ajk.m10, ajk.m20, ajk.m30, ajk.m01, ajk.m11, ajk.m21, ajk.m31, ajk.m02, ajk.m12, ajk.m22, ajk.m32, ajk.m03, ajk.m13, ajk.m23, ajk.m33);
    }

    public Vector2fWrap getScaling() {
        return new Vector2fWrap(nativeGetScaling(this.eAm));
    }

    /* renamed from: ep */
    public Vector2fWrap mo20700ep() {
        return new Vector2fWrap(nativeGetDiffuseTextureTexelSize(this.eAm));
    }

    /* renamed from: A */
    public C1710ZI mo20684A(int i) {
        return nativeGetTexture(this.eAm, i);
    }

    /* renamed from: a */
    public void mo20686a(int i, C1710ZI zi) {
        nativeSetTexture(this.eAm, i, zi != null ? zi.mo762dE() : 0);
    }

    public int textureCount() {
        return nativeTextureCount(this.eAm);
    }

    /* renamed from: a */
    public void mo20688a(C1710ZI zi) {
        nativeAddTexture(this.eAm, zi != null ? zi.mo762dE() : 0);
    }

    public float getShininess() {
        return nativeGetShininess(this.eAm);
    }

    public void setShininess(float f) {
        nativeSetShininess(this.eAm, f);
    }

    /* renamed from: B */
    public int mo20685B(int i) {
        return nativeMaxTextureCount(this.eAm, i);
    }

    public Color getSpecular() {
        return new Color(nativeGetSpecular(this.eAm));
    }

    public void setSpecular(Color color) {
        nativeSetSpecular(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    /* renamed from: eq */
    public C1710ZI mo20701eq() {
        return nativeGetDiffuseTexture(this.eAm);
    }

    /* renamed from: er */
    public C1710ZI mo20702er() {
        return nativeGetNormalTexture(this.eAm);
    }

    /* renamed from: es */
    public C1710ZI mo20703es() {
        return nativeGetSelfIluminationTexture(this.eAm);
    }

    /* renamed from: et */
    public C1710ZI mo20704et() {
        return nativeGetDiffuseDetailTexture(this.eAm);
    }

    /* renamed from: b */
    public void mo20692b(C1710ZI zi) {
        nativeSetDiffuseTexture_Texture(this.eAm, zi != null ? zi.mo762dE() : 0);
    }

    /* renamed from: c */
    public void mo20694c(C1710ZI zi) {
        nativeSetNormalTexture_Texture(this.eAm, zi != null ? zi.mo762dE() : 0);
    }

    /* renamed from: d */
    public void mo20696d(C1710ZI zi) {
        nativeSetSelfIluminationTexture_Texture(this.eAm, zi != null ? zi.mo762dE() : 0);
    }

    /* renamed from: e */
    public void mo20698e(C1710ZI zi) {
        nativeSetDiffuseDetailTexture_Texture(this.eAm, zi != null ? zi.mo762dE() : 0);
    }

    /* renamed from: b */
    public void mo20691b(int i, C1710ZI zi) {
        nativeSetDiffuseTexture_ITexture(this.eAm, i, zi != null ? zi.mo762dE() : 0);
    }

    /* renamed from: c */
    public void mo20693c(int i, C1710ZI zi) {
        nativeSetNormalTexture_ITexture(this.eAm, i, zi != null ? zi.mo762dE() : 0);
    }

    /* renamed from: d */
    public void mo20695d(int i, C1710ZI zi) {
        nativeSetSelfIluminationTexture_ITexture(this.eAm, i, zi != null ? zi.mo762dE() : 0);
    }

    /* renamed from: e */
    public void mo20697e(int i, C1710ZI zi) {
        nativeSetDiffuseDetailTexture_ITexture(this.eAm, i, zi != null ? zi.mo762dE() : 0);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }
}
