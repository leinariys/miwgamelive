package gametoolkit.client.scene.render;

import logic.res.scene.C1710ZI;
import logic.res.scene.C3038nJ;
import logic.res.scene.C3087nb;
import logic.res.scene.C3417rK;

/* compiled from: a */
public class ExtendedMaterialImpl extends MaterialImpl implements C3417rK {
    public ExtendedMaterialImpl(long j) {
        super(j);
    }

    public ExtendedMaterialImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42501aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native int nativeGetChannel(long j);

    private native long nativeGetClassSize(long j);

    private native C3038nJ nativeGetMaterial(long j);

    private native C1710ZI nativeGetTexture(long j, int i);

    private native void nativeSetChannel(long j, int i);

    private native void nativeSetMaterial(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: b */
    public void mo21596b(C3038nJ nJVar) {
        nativeSetMaterial(this.eAm, nJVar != null ? nJVar.mo762dE() : 0);
    }

    /* renamed from: YU */
    public C3038nJ mo21595YU() {
        return nativeGetMaterial(this.eAm);
    }

    public int getChannel() {
        return nativeGetChannel(this.eAm);
    }

    public void setChannel(int i) {
        nativeSetChannel(this.eAm, i);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: A */
    public C1710ZI mo20684A(int i) {
        return nativeGetTexture(this.eAm, i);
    }
}
