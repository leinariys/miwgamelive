package gametoolkit.client.scene.render;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import logic.res.scene.C3087nb;
import logic.res.scene.C3602sp;
import p001a.C1424Ur;

@C1424Ur
/* compiled from: a */
public class LightImpl extends PrimitiveImpl implements C3602sp {
    public LightImpl(long j) {
        super(j);
    }

    public LightImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42539aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeClearScissorValues(long j);

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetCosCutoff(long j);

    private native float nativeGetCurrentDistanceToCamera(long j);

    private native float[] nativeGetDiffuseColor(long j);

    private native float[] nativeGetDirection(long j);

    private native int nativeGetLightType(long j);

    private native float[] nativeGetPosition(long j);

    private native float nativeGetRadius(long j);

    private native float[] nativeGetSpecularColor(long j);

    private native float nativeGetSpotExponent(long j);

    private native void nativeSetCosCutoff(long j, float f);

    private native void nativeSetCurrentDistanceToCamera(long j, float f);

    private native void nativeSetDiffuseColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetDirection(long j, float f, float f2, float f3);

    private native void nativeSetLightType(long j, int i);

    private native void nativeSetPosition(long j, float f, float f2, float f3);

    private native void nativeSetRadius(long j, float f);

    private native void nativeSetSpecularColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetSpotExponent(long j, float f);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: Mr */
    public int mo22028Mr() {
        return nativeGetLightType(this.eAm);
    }

    /* renamed from: cM */
    public void mo22034cM(int i) {
        nativeSetLightType(this.eAm, i);
    }

    /* renamed from: gL */
    public Vec3f mo22035gL() {
        return new Vec3f(nativeGetPosition(this.eAm));
    }

    public void setPosition(Vec3f vec3f) {
        nativeSetPosition(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    public Vec3f getDirection() {
        return new Vec3f(nativeGetDirection(this.eAm));
    }

    public void setDirection(Vec3f vec3f) {
        nativeSetDirection(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    public float getRadius() {
        return nativeGetRadius(this.eAm);
    }

    public void setRadius(float f) {
        nativeSetRadius(this.eAm, f);
    }

    public Color getSpecularColor() {
        return new Color(nativeGetSpecularColor(this.eAm));
    }

    public void setSpecularColor(Color color) {
        nativeSetSpecularColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public Color getDiffuseColor() {
        return new Color(nativeGetDiffuseColor(this.eAm));
    }

    public void setDiffuseColor(Color color) {
        nativeSetDiffuseColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    /* renamed from: aX */
    public void mo22031aX(float f) {
        nativeSetCosCutoff(this.eAm, f);
    }

    public float getCosCutoff() {
        return nativeGetCosCutoff(this.eAm);
    }

    /* renamed from: aY */
    public void mo22032aY(float f) {
        nativeSetSpotExponent(this.eAm, f);
    }

    public float getSpotExponent() {
        return nativeGetSpotExponent(this.eAm);
    }

    /* renamed from: Ms */
    public void mo22029Ms() {
        nativeClearScissorValues(this.eAm);
    }

    /* renamed from: Mt */
    public float mo22030Mt() {
        return nativeGetCurrentDistanceToCamera(this.eAm);
    }

    /* renamed from: aZ */
    public void mo22033aZ(float f) {
        nativeSetCurrentDistanceToCamera(this.eAm, f);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }
}
