package gametoolkit.client.scene.render;

import logic.res.scene.C2634hp;
import logic.res.scene.C5738aUw;

/* compiled from: a */
public class GlowFXImpl extends PostProcessingFXImpl implements C5738aUw {
    public GlowFXImpl(long j) {
        super(j);
    }

    public GlowFXImpl() {
        super(nativeConstructor());
    }

    private static native long nativeConstructor();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42525aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native long nativeGetClassSize(long j);

    private native float nativeGetFeedback(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetFeedback(long j, float f);

    private native void nativeSetHorizontalBlurShader(long j, long j2);

    private native void nativeSetSceneIntensity(long j, float f);

    private native void nativeSetVerticalBlurShader(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: fa */
    public void mo11724fa(float f) {
        nativeSetFeedback(this.eAm, f);
    }

    public float aHh() {
        return nativeGetFeedback(this.eAm);
    }

    /* renamed from: b */
    public void mo11722b(C2634hp hpVar) {
        nativeSetVerticalBlurShader(this.eAm, hpVar != null ? hpVar.mo762dE() : 0);
    }

    /* renamed from: c */
    public void mo11723c(C2634hp hpVar) {
        nativeSetHorizontalBlurShader(this.eAm, hpVar != null ? hpVar.mo762dE() : 0);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    public void setSceneIntensity(float f) {
        nativeSetSceneIntensity(this.eAm, f);
    }
}
