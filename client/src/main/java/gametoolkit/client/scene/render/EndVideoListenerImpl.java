package gametoolkit.client.scene.render;

import gametoolkit.client.scene.common.ManagedObjectImpl;
import logic.res.scene.aDI;
import p001a.C1424Ur;

@C1424Ur
/* compiled from: a */
public class EndVideoListenerImpl extends ManagedObjectImpl implements aDI {
    public EndVideoListenerImpl(long j) {
        super(j);
    }

    public EndVideoListenerImpl() {
        super(nativeConstructor());
    }

    private static native long nativeConstructor();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42479aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeBindJavaDispatcher(long j);

    private native long nativeGetClassSize(long j);

    private native void nativeOnVideoEnd(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: iD */
    public void mo8315iD() {
        nativeBindJavaDispatcher(this.eAm);
    }

    /* renamed from: iE */
    public void mo8316iE() {
        nativeOnVideoEnd(this.eAm);
    }
}
