package gametoolkit.client.scene.render;

import com.hoplon.geometry.Vec3f;
import logic.res.scene.C1710ZI;
import logic.res.scene.C2511gB;
import logic.res.scene.C3087nb;

/* compiled from: a */
public class TiledTextureImpl extends TextureImpl implements C2511gB {
    public TiledTextureImpl(long j) {
        super(j);
    }

    public TiledTextureImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42696aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native C1710ZI nativeGetTexture(long j);

    private native float[] nativeGetTile(long j);

    private native void nativeSetTexture(long j, long j2);

    private native void nativeSetTile(long j, float f, float f2, float f3);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: f */
    public void mo18883f(C1710ZI zi) {
        nativeSetTexture(this.eAm, zi != null ? zi.mo762dE() : 0);
    }

    /* renamed from: wb */
    public C1710ZI mo18886wb() {
        return nativeGetTexture(this.eAm);
    }

    public Vec3f getTile() {
        return new Vec3f(nativeGetTile(this.eAm));
    }

    public void setTile(Vec3f vec3f) {
        nativeSetTile(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }
}
