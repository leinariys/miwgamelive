package gametoolkit.client.scene.render;

import logic.res.scene.C0676Jc;
import logic.res.scene.C1710ZI;
import logic.res.scene.C3087nb;
import p001a.C1424Ur;

@C1424Ur
/* compiled from: a */
public class RenderTargetImpl extends ResourceImpl implements C0676Jc {
    public RenderTargetImpl(long j) {
        super(j);
    }

    public RenderTargetImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42611aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native int nativeGetHeight(long j);

    private native C1710ZI nativeGetTexture(long j);

    private native int nativeGetWidth(long j);

    private native void nativeSetHeight(long j, int i);

    private native void nativeSetTexture(long j, long j2);

    private native void nativeSetWidth(long j, int i);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: f */
    public void mo3140f(C1710ZI zi) {
        nativeSetTexture(this.eAm, zi != null ? zi.mo762dE() : 0);
    }

    /* renamed from: wb */
    public C1710ZI mo3145wb() {
        return nativeGetTexture(this.eAm);
    }

    public int getWidth() {
        return nativeGetWidth(this.eAm);
    }

    public void setWidth(int i) {
        nativeSetWidth(this.eAm, i);
    }

    public int getHeight() {
        return nativeGetHeight(this.eAm);
    }

    public void setHeight(int i) {
        nativeSetHeight(this.eAm, i);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }
}
