package gametoolkit.client.scene.render;

import logic.res.scene.C3087nb;
import logic.res.scene.C3674tb;
import logic.res.scene.C4076yy;

/* compiled from: a */
public class ShaderImpl extends ResourceImpl implements C3674tb {
    public ShaderImpl(long j) {
        super(j);
    }

    public ShaderImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42620aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeAddPass(long j, long j2);

    private native void nativeClearPasses(long j);

    private native C3087nb nativeClone(long j);

    private native void nativeCompile(long j);

    private native C3674tb nativeGetBestShader(long j);

    private native long nativeGetClassSize(long j);

    private native C3674tb nativeGetFallback(long j);

    private native C4076yy nativeGetPass(long j, int i);

    private native int nativeGetQuality(long j);

    private native int nativePassCount(long j);

    private native void nativeSetFallback(long j, long j2);

    private native void nativeSetPass(long j, int i, long j2);

    private native void nativeSetQuality(long j, int i);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: a */
    public void mo22259a(C4076yy yyVar) {
        nativeAddPass(this.eAm, yyVar != null ? yyVar.mo762dE() : 0);
    }

    public int passCount() {
        return nativePassCount(this.eAm);
    }

    /* renamed from: T */
    public C4076yy mo22257T(int i) {
        return nativeGetPass(this.eAm, i);
    }

    /* renamed from: a */
    public void mo22258a(int i, C4076yy yyVar) {
        nativeSetPass(this.eAm, i, yyVar != null ? yyVar.mo762dE() : 0);
    }

    public void clearPasses() {
        nativeClearPasses(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: b */
    public void mo22260b(C3674tb tbVar) {
        nativeSetFallback(this.eAm, tbVar != null ? tbVar.mo762dE() : 0);
    }

    /* renamed from: gs */
    public C3674tb mo22264gs() {
        return nativeGetFallback(this.eAm);
    }

    public int getQuality() {
        return nativeGetQuality(this.eAm);
    }

    public void setQuality(int i) {
        nativeSetQuality(this.eAm, i);
    }

    /* renamed from: gt */
    public C3674tb mo22265gt() {
        return nativeGetBestShader(this.eAm);
    }

    public void compile() {
        nativeCompile(this.eAm);
    }
}
