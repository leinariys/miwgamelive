package gametoolkit.client.scene.render;

import gametoolkit.client.scene.common.NamedObjectImpl;
import logic.res.scene.C3087nb;
import logic.res.scene.aIF;

/* compiled from: a */
public class FontBufferImpl extends NamedObjectImpl implements aIF {
    public FontBufferImpl(long j) {
        super(j);
    }

    public FontBufferImpl() {
        super(nativeConstructor());
    }

    private static native long nativeConstructor();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42511aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native int nativeGetLength(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public int getLength() {
        return nativeGetLength(this.eAm);
    }
}
