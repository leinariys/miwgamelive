package gametoolkit.client.scene.render;

import logic.res.scene.C6276ajo;

/* compiled from: a */
public class MotionBlurFXImpl extends PostProcessingFXImpl implements C6276ajo {
    public MotionBlurFXImpl(long j) {
        super(j);
    }

    public MotionBlurFXImpl() {
        super(nativeConstructor());
    }

    private static native long nativeConstructor();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42589aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native float nativeGetBlurAmount(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetRadialAmount(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetBlurAmount(long j, float f);

    private native void nativeSetBlurType(long j, int i);

    private native void nativeSetRadialAmount(long j, float f);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: dT */
    public void mo14181dT(float f) {
        nativeSetBlurAmount(this.eAm, f);
    }

    public float akU() {
        return nativeGetBlurAmount(this.eAm);
    }

    /* renamed from: dU */
    public void mo14182dU(float f) {
        nativeSetRadialAmount(this.eAm, f);
    }

    public float akV() {
        return nativeGetRadialAmount(this.eAm);
    }

    /* renamed from: eX */
    public void mo14183eX(int i) {
        nativeSetBlurType(this.eAm, i);
    }
}
