package gametoolkit.client.scene.render;

import com.hoplon.geometry.Color;
import gametoolkit.client.scene.common.NamedObjectImpl;
import logic.res.scene.*;

/* compiled from: a */
public class FontImpl extends NamedObjectImpl implements aRZ {
    public FontImpl(long j) {
        super(j);
    }

    public FontImpl() {
        super(nativeConstructor_());
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FontImpl(aRZ arz, float f) {
        super(nativeConstructor_FontF(arz != null ? arz.mo762dE() : 0, f));
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FontImpl(aRZ arz, float f, int i) {
        super(nativeConstructor_FontFI(arz != null ? arz.mo762dE() : 0, f, i));
    }

    private static native long nativeConstructor_();

    private static native long nativeConstructor_FontF(long j, float f);

    private static native long nativeConstructor_FontFI(long j, float f, int i);

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42514aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native void nativeFillMesh(long j, long j2, String str, float f, float f2, float f3, float f4, float f5, float f6);

    private native long nativeGetClassSize(long j);

    private native aIF nativeGetFontFile(long j);

    private native int nativeGetInternalSize(long j);

    private native float nativeGetLineHeight(long j);

    private native C3038nJ nativeGetMaterial(long j);

    private native float nativeGetSize(long j);

    private native void nativeRender(long j, String str);

    private native void nativeRenderUnicode(long j, String str);

    private native void nativeSetFontFile(long j, long j2);

    private native void nativeSetInternalSize(long j, int i);

    private native void nativeSetMaterial(long j, long j2);

    private native void nativeSetSize(long j, float f);

    private native float nativeStringWidth(long j, String str);

    private native float nativeUnicodeStringWidth(long j, String str);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: a */
    public void mo11157a(aIF aif) {
        nativeSetFontFile(this.eAm, aif != null ? aif.mo762dE() : 0);
    }

    public aIF cjY() {
        return nativeGetFontFile(this.eAm);
    }

    public float getSize() {
        return nativeGetSize(this.eAm);
    }

    public void setSize(float f) {
        nativeSetSize(this.eAm, f);
    }

    /* renamed from: sD */
    public void mo11168sD(int i) {
        nativeSetInternalSize(this.eAm, i);
    }

    public int cjZ() {
        return nativeGetInternalSize(this.eAm);
    }

    /* renamed from: js */
    public float mo11164js(String str) {
        return nativeStringWidth(this.eAm, str);
    }

    /* renamed from: jt */
    public float mo11165jt(String str) {
        return nativeUnicodeStringWidth(this.eAm, str);
    }

    public float cka() {
        return nativeGetLineHeight(this.eAm);
    }

    public void render(String str) {
        nativeRender(this.eAm, str);
    }

    /* renamed from: ju */
    public void mo11166ju(String str) {
        nativeRenderUnicode(this.eAm, str);
    }

    /* renamed from: b */
    public void mo11159b(C3038nJ nJVar) {
        nativeSetMaterial(this.eAm, nJVar != null ? nJVar.mo762dE() : 0);
    }

    /* renamed from: YU */
    public C3038nJ mo11156YU() {
        return nativeGetMaterial(this.eAm);
    }

    /* renamed from: a */
    public void mo11158a(C6985axy axy, String str, float f, float f2, Color color) {
        nativeFillMesh(this.eAm, axy != null ? axy.mo762dE() : 0, str, f, f2, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }
}
