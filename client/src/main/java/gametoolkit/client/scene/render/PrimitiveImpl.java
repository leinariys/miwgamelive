package gametoolkit.client.scene.render;

import com.hoplon.geometry.Vec3f;
import logic.res.scene.C0999Oj;
import logic.res.scene.C3038nJ;
import logic.res.scene.C3087nb;
import logic.res.scene.C3674tb;
import p001a.C1424Ur;

@C1424Ur
/* compiled from: a */
public class PrimitiveImpl extends ResourceImpl implements C0999Oj {
    public PrimitiveImpl(long j) {
        super(j);
    }

    public PrimitiveImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    private static native void nativeUnbind();

    /* renamed from: aV */
    public static long m42604aV() {
        return nativeGetStaticClassInfoPointer();
    }

    public static void unbind() {
        nativeUnbind();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native C3038nJ nativeGetDefaultMaterial(long j);

    private native C3674tb nativeGetDefaultShader(long j);

    private native boolean nativeGetUseVBO(long j);

    private native boolean nativeRayIntersects(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7);

    private native void nativeSetDefaultMaterial(long j, long j2);

    private native void nativeSetDefaultShader(long j, long j2);

    private native void nativeSetUseVBO(long j, boolean z);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public C3674tb blf() {
        return nativeGetDefaultShader(this.eAm);
    }

    public C3038nJ blg() {
        return nativeGetDefaultMaterial(this.eAm);
    }

    /* renamed from: a */
    public boolean mo4481a(Vec3f vec3f, Vec3f vec3f2, float f) {
        return nativeRayIntersects(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f);
    }

    /* renamed from: e */
    public void mo4485e(C3674tb tbVar) {
        nativeSetDefaultShader(this.eAm, tbVar != null ? tbVar.mo762dE() : 0);
    }

    /* renamed from: c */
    public void mo4484c(C3038nJ nJVar) {
        nativeSetDefaultMaterial(this.eAm, nJVar != null ? nJVar.mo762dE() : 0);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public boolean getUseVBO() {
        return nativeGetUseVBO(this.eAm);
    }

    /* renamed from: g */
    public void mo4486g(boolean z) {
        nativeSetUseVBO(this.eAm, z);
    }
}
