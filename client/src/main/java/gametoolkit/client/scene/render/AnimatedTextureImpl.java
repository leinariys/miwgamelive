package gametoolkit.client.scene.render;

import logic.res.scene.C1710ZI;
import logic.res.scene.C3087nb;
import logic.res.scene.C3733uK;

/* compiled from: a */
public class AnimatedTextureImpl extends TextureImpl implements C3733uK {
    public AnimatedTextureImpl(long j) {
        super(j);
    }

    public AnimatedTextureImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42459aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetFps(long j);

    private native int nativeGetHorizontalFrames(long j);

    private native boolean nativeGetIsLooped(long j);

    private native C1710ZI nativeGetTexture(long j);

    private native int nativeGetVerticalFrames(long j);

    private native void nativeRewind(long j);

    private native void nativeSetFps(long j, float f);

    private native void nativeSetHorizontalFrames(long j, int i);

    private native void nativeSetIsLooped(long j, boolean z);

    private native void nativeSetTexture(long j, long j2);

    private native void nativeSetVerticalFrames(long j, int i);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: f */
    public void mo22344f(C1710ZI zi) {
        nativeSetTexture(this.eAm, zi != null ? zi.mo762dE() : 0);
    }

    /* renamed from: wb */
    public C1710ZI mo22354wb() {
        return nativeGetTexture(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public int getHorizontalFrames() {
        return nativeGetHorizontalFrames(this.eAm);
    }

    public void setHorizontalFrames(int i) {
        nativeSetHorizontalFrames(this.eAm, i);
    }

    public int getVerticalFrames() {
        return nativeGetVerticalFrames(this.eAm);
    }

    public void setVerticalFrames(int i) {
        nativeSetVerticalFrames(this.eAm, i);
    }

    public boolean getIsLooped() {
        return nativeGetIsLooped(this.eAm);
    }

    public void setIsLooped(boolean z) {
        nativeSetIsLooped(this.eAm, z);
    }

    public float getFps() {
        return nativeGetFps(this.eAm);
    }

    public void setFps(float f) {
        nativeSetFps(this.eAm, f);
    }

    public void rewind() {
        nativeRewind(this.eAm);
    }
}
