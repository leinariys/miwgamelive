package gametoolkit.client.scene.render;

import logic.res.scene.C6100agU;
import p001a.C1424Ur;

@C1424Ur
/* compiled from: a */
public abstract class PostProcessingFXImpl extends ResourceImpl implements C6100agU {
    public PostProcessingFXImpl(long j) {
        super(j);
    }

    public PostProcessingFXImpl() {
        super(nativeConstructor());
    }

    private static native long nativeConstructor();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42602aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native long nativeGetClassSize(long j);

    private native boolean nativeIsEnabled(long j);

    private native void nativeSetEnabled(long j, boolean z);

    private native void nativeSetSceneIntensity(long j, float f);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void setSceneIntensity(float f) {
        nativeSetSceneIntensity(this.eAm, f);
    }

    public boolean isEnabled() {
        return nativeIsEnabled(this.eAm);
    }

    public void setEnabled(boolean z) {
        nativeSetEnabled(this.eAm, z);
    }
}
