package gametoolkit.client.scene.render;

import gametoolkit.client.scene.common.NamedObjectImpl;
import logic.res.scene.C3087nb;
import logic.res.scene.C5759aVr;

/* compiled from: a */
public class ResourceImpl extends NamedObjectImpl implements C5759aVr {
    public ResourceImpl(long j) {
        super(j);
    }

    public ResourceImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42616aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native boolean nativeGetSealed(long j);

    private native void nativeRender(long j);

    private native void nativeSetSealed(long j, boolean z);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public boolean cvB() {
        return nativeGetSealed(this.eAm);
    }

    /* renamed from: gA */
    public void mo11953gA(boolean z) {
        nativeSetSealed(this.eAm, z);
    }

    public void render() {
        nativeRender(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }
}
