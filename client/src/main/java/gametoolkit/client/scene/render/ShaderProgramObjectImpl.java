package gametoolkit.client.scene.render;

import gametoolkit.client.scene.common.NamedObjectImpl;
import logic.res.scene.C3087nb;
import logic.res.scene.C6498aoC;
import p001a.C1424Ur;

@C1424Ur
/* compiled from: a */
public class ShaderProgramObjectImpl extends NamedObjectImpl implements C6498aoC {
    public ShaderProgramObjectImpl(long j) {
        super(j);
    }

    public ShaderProgramObjectImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42662aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native boolean nativeCompile(long j);

    private native long nativeGetClassSize(long j);

    private native int nativeGetObjectId(long j);

    private native int nativeGetShaderType(long j);

    private native String nativeGetSourceCode(long j);

    private native void nativeSetShaderType(long j, int i);

    private native void nativeSetSourceCode(long j, String str);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public String getSourceCode() {
        return nativeGetSourceCode(this.eAm);
    }

    public void setSourceCode(String str) {
        nativeSetSourceCode(this.eAm, str);
    }

    public int getShaderType() {
        return nativeGetShaderType(this.eAm);
    }

    public void setShaderType(int i) {
        nativeSetShaderType(this.eAm, i);
    }

    public int getObjectId() {
        return nativeGetObjectId(this.eAm);
    }

    public boolean alB() {
        return nativeCompile(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }
}
