package gametoolkit.client.scene.render;

import com.hoplon.geometry.Vec3f;
import logic.res.scene.C1068Pc;
import logic.res.scene.C2724jE;
import logic.res.scene.C3087nb;

/* compiled from: a */
public class Vec3ParameterImpl extends ShaderParameterImpl implements C1068Pc {
    public Vec3ParameterImpl(long j) {
        super(j);
    }

    public Vec3ParameterImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42705aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native float[] nativeGetValue(long j);

    private native void nativeSetValue_ShaderParameter(long j, long j2);

    private native void nativeSetValue_Vec3f(long j, float f, float f2, float f3);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: b */
    public void mo1883b(C2724jE jEVar) {
        nativeSetValue_ShaderParameter(this.eAm, jEVar != null ? jEVar.mo762dE() : 0);
    }

    public Vec3f getValue() {
        return new Vec3f(nativeGetValue(this.eAm));
    }

    public void setValue(Vec3f vec3f) {
        nativeSetValue_Vec3f(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }
}
