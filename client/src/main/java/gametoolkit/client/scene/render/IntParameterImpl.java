package gametoolkit.client.scene.render;

import logic.res.scene.C2517gF;
import logic.res.scene.C2724jE;
import logic.res.scene.C3087nb;

/* compiled from: a */
public class IntParameterImpl extends ShaderParameterImpl implements C2517gF {
    public IntParameterImpl(long j) {
        super(j);
    }

    public IntParameterImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42535aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native int nativeGetValue(long j);

    private native void nativeSetValue_I(long j, int i);

    private native void nativeSetValue_ShaderParameter(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: b */
    public void mo1883b(C2724jE jEVar) {
        nativeSetValue_ShaderParameter(this.eAm, jEVar != null ? jEVar.mo762dE() : 0);
    }

    public int getValue() {
        return nativeGetValue(this.eAm);
    }

    public void setValue(int i) {
        nativeSetValue_I(this.eAm, i);
    }
}
