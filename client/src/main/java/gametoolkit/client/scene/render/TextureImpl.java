package gametoolkit.client.scene.render;

import game.geometry.Matrix4fWrap;
import logic.res.scene.C1710ZI;
import logic.res.scene.C3087nb;
import logic.res.scene.C5757aVp;
import p001a.C1424Ur;

@C1424Ur
/* compiled from: a */
public class TextureImpl extends ResourceImpl implements C1710ZI {
    public TextureImpl(long j) {
        super(j);
    }

    public TextureImpl() {
        super(nativeConstructor_());
    }

    private static native int nativeCalculateImageSize_III(int i, int i2, int i3);

    private static native int nativeCalculateImageSize_IIIZ(int i, int i2, int i3, boolean z);

    private static native int nativeCalculateImageSize_IIIZI(int i, int i2, int i3, boolean z, int i4);

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    private static native int nativeGetTotalMemory();

    /* renamed from: aV */
    public static long m42671aV() {
        return nativeGetStaticClassInfoPointer();
    }

    /* renamed from: a */
    public static int m42670a(int i, int i2, int i3, boolean z, int i4) {
        return nativeCalculateImageSize_IIIZI(i, i2, i3, z, i4);
    }

    /* renamed from: b */
    public static int m42672b(int i, int i2, int i3, boolean z) {
        return nativeCalculateImageSize_IIIZ(i, i2, i3, z);
    }

    /* renamed from: d */
    public static int m42673d(int i, int i2, int i3) {
        return nativeCalculateImageSize_III(i, i2, i3);
    }

    /* renamed from: Xr */
    public static int m42669Xr() {
        return nativeGetTotalMemory();
    }

    private native void nativeClearInternalData(long j);

    private native C3087nb nativeClone(long j);

    private native boolean nativeCreateEmpty_II(long j, int i, int i2);

    private native boolean nativeCreateEmpty_III(long j, int i, int i2, int i3);

    private native int nativeGetBpp(long j);

    private native long nativeGetClassSize(long j);

    private native int nativeGetDataSize(long j);

    private native int nativeGetInternalFormat(long j);

    private native int nativeGetMagFilter(long j);

    private native int nativeGetMinFilter(long j);

    private native int nativeGetSizeX(long j);

    private native int nativeGetSizeY(long j);

    private native int nativeGetTarget(long j);

    private native int nativeGetTexId(long j);

    private native C5757aVp nativeGetTextureData(long j);

    private native float[] nativeGetTransform(long j);

    private native float nativeGetTransformedHeight(long j);

    private native float nativeGetTransformedWidth(long j);

    private native int nativeGetType(long j);

    private native int nativeGetWrap(long j);

    private native int nativeGetWrapS(long j);

    private native int nativeGetWrapT(long j);

    private native boolean nativeIsTransformed(long j);

    private native boolean nativeIsValid(long j);

    private native void nativeSetBpp(long j, int i);

    private native void nativeSetData_IIIII(long j, int i, int i2, int i3, int i4, int i5);

    private native void nativeSetData_IIIIIImageFloat(long j, int i, int i2, int i3, int i4, int i5, long j2);

    private native void nativeSetMagFilter(long j, int i);

    private native void nativeSetMinFilter(long j, int i);

    private native void nativeSetSizeX(long j, int i);

    private native void nativeSetSizeY(long j, int i);

    private native void nativeSetTransform(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16);

    private native void nativeSetWrap(long j, int i);

    private native void nativeSetWrapS(long j, int i);

    private native void nativeSetWrapT(long j, int i);

    private native void nativeUploadData_ImageFloat(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public boolean isValid() {
        return nativeIsValid(this.eAm);
    }

    /* renamed from: a */
    public void mo7348a(int i, int i2, int i3, int i4, int i5) {
        nativeSetData_IIIII(this.eAm, i, i2, i3, i4, i5);
    }

    /* renamed from: a */
    public void mo7349a(int i, int i2, int i3, int i4, int i5, C5757aVp avp) {
        nativeSetData_IIIIIImageFloat(this.eAm, i, i2, i3, i4, i5, avp != null ? avp.mo762dE() : 0);
    }

    /* renamed from: c */
    public boolean mo7351c(int i, int i2, int i3) {
        return nativeCreateEmpty_III(this.eAm, i, i2, i3);
    }

    /* renamed from: r */
    public boolean mo7370r(int i, int i2) {
        return nativeCreateEmpty_II(this.eAm, i, i2);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: Xk */
    public int mo7341Xk() {
        return nativeGetMinFilter(this.eAm);
    }

    /* renamed from: dX */
    public void mo7352dX(int i) {
        nativeSetMinFilter(this.eAm, i);
    }

    /* renamed from: Xl */
    public int mo7342Xl() {
        return nativeGetMagFilter(this.eAm);
    }

    /* renamed from: dY */
    public void mo7353dY(int i) {
        nativeSetMagFilter(this.eAm, i);
    }

    public int getDataSize() {
        return nativeGetDataSize(this.eAm);
    }

    public float getTransformedWidth() {
        return nativeGetTransformedWidth(this.eAm);
    }

    public float getTransformedHeight() {
        return nativeGetTransformedHeight(this.eAm);
    }

    public int getSizeX() {
        return nativeGetSizeX(this.eAm);
    }

    public void setSizeX(int i) {
        nativeSetSizeX(this.eAm, i);
    }

    public int getSizeY() {
        return nativeGetSizeY(this.eAm);
    }

    public void setSizeY(int i) {
        nativeSetSizeY(this.eAm, i);
    }

    public int getBpp() {
        return nativeGetBpp(this.eAm);
    }

    public void setBpp(int i) {
        nativeSetBpp(this.eAm, i);
    }

    public Matrix4fWrap getTransform() {
        return new Matrix4fWrap(nativeGetTransform(this.eAm));
    }

    public void setTransform(Matrix4fWrap ajk) {
        nativeSetTransform(this.eAm, ajk.m00, ajk.m10, ajk.m20, ajk.m30, ajk.m01, ajk.m11, ajk.m21, ajk.m31, ajk.m02, ajk.m12, ajk.m22, ajk.m32, ajk.m03, ajk.m13, ajk.m23, ajk.m33);
    }

    public boolean isTransformed() {
        return nativeIsTransformed(this.eAm);
    }

    public int getTexId() {
        return nativeGetTexId(this.eAm);
    }

    public int getTarget() {
        return nativeGetTarget(this.eAm);
    }

    public int getType() {
        return nativeGetType(this.eAm);
    }

    public int getInternalFormat() {
        return nativeGetInternalFormat(this.eAm);
    }

    /* renamed from: dZ */
    public void mo7354dZ(int i) {
        nativeSetWrap(this.eAm, i);
    }

    /* renamed from: Xm */
    public int mo7343Xm() {
        return nativeGetWrap(this.eAm);
    }

    /* renamed from: ea */
    public void mo7355ea(int i) {
        nativeSetWrapS(this.eAm, i);
    }

    /* renamed from: Xn */
    public int mo7344Xn() {
        return nativeGetWrapS(this.eAm);
    }

    /* renamed from: eb */
    public void mo7356eb(int i) {
        nativeSetWrapT(this.eAm, i);
    }

    /* renamed from: Xo */
    public int mo7345Xo() {
        return nativeGetWrapT(this.eAm);
    }

    /* renamed from: a */
    public void mo7350a(C5757aVp avp) {
        nativeUploadData_ImageFloat(this.eAm, avp != null ? avp.mo762dE() : 0);
    }

    /* renamed from: Xp */
    public C5757aVp mo7346Xp() {
        return nativeGetTextureData(this.eAm);
    }

    /* renamed from: Xq */
    public void mo7347Xq() {
        nativeClearInternalData(this.eAm);
    }
}
