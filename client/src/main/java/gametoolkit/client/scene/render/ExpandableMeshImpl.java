package gametoolkit.client.scene.render;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vector2fWrap;
import game.geometry.Vector4fWrap;
import logic.res.scene.C3087nb;
import logic.res.scene.C6985axy;
import p001a.C1424Ur;

@C1424Ur
/* compiled from: a */
public class ExpandableMeshImpl extends MeshImpl implements C6985axy {
    public ExpandableMeshImpl(long j) {
        super(j);
    }

    public ExpandableMeshImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42483aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeAddPointIgnoreAABB(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9);

    private native void nativeAddPoint_Vec3f(long j, float f, float f2, float f3);

    private native void nativeAddPoint_Vec3fVec2f(long j, float f, float f2, float f3, float f4, float f5);

    private native void nativeAddPoint_Vec3fVec2fVec4f(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9);

    private native void nativeAddPoint_Vec3fVec3f(long j, float f, float f2, float f3, float f4, float f5, float f6);

    private native void nativeAddPoint_Vec3fVec3fVec4f(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10);

    private native void nativeAddPoint_Vec3fVec3fVec4fVec4f(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14);

    private native void nativeAddPoint_Vec3fVec3fVec4fVec4fVec2f(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16);

    private native void nativeAddPoint_Vec3fVec4f(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7);

    private native C3087nb nativeClone(long j);

    private native float[] nativeGetAABB(long j);

    private native long nativeGetClassSize(long j);

    private native int nativeGetGeometryType(long j);

    private native int nativeGetIndex(long j, long j2);

    private native long nativeGetNumIndexes(long j);

    private native long nativeGetNumPrimitives(long j);

    private native int nativeGetNumTris(long j);

    private native float[] nativeGetVertice(long j, long j2);

    private native int nativePointCount(long j);

    private native boolean nativeRayIntersects(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7);

    private native void nativeRebuildAabb(long j);

    private native void nativeReservePoints(long j, int i);

    private native void nativeReset(long j);

    private native void nativeSetCurrentPoints(long j, int i);

    private native void nativeSetGeometryType(long j, int i);

    private native void nativeSetIndexSize(long j, int i);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public void reset() {
        nativeReset(this.eAm);
    }

    public void addPoint(Vec3f vec3f) {
        nativeAddPoint_Vec3f(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: a */
    public void mo16867a(Vec3f vec3f, Vector4fWrap ajf) {
        nativeAddPoint_Vec3fVec4f(this.eAm, vec3f.x, vec3f.y, vec3f.z, ajf.x, ajf.y, ajf.z, ajf.w);
    }

    /* renamed from: a */
    public void mo16868a(Vec3f vec3f, Vector2fWrap aka) {
        nativeAddPoint_Vec3fVec2f(this.eAm, vec3f.x, vec3f.y, vec3f.z, aka.x, aka.y);
    }

    public void addPoint(Vec3f vec3f, Vector2fWrap aka, Vector4fWrap ajf) {
        nativeAddPoint_Vec3fVec2fVec4f(this.eAm, vec3f.x, vec3f.y, vec3f.z, aka.x, aka.y, ajf.x, ajf.y, ajf.z, ajf.w);
    }

    /* renamed from: a */
    public void mo16869a(Vec3f vec3f, Vector2fWrap aka, Vector4fWrap ajf) {
        nativeAddPointIgnoreAABB(this.eAm, vec3f.x, vec3f.y, vec3f.z, aka.x, aka.y, ajf.x, ajf.y, ajf.z, ajf.w);
    }

    /* renamed from: H */
    public void mo16866H(Vec3f vec3f, Vec3f vec3f2) {
        nativeAddPoint_Vec3fVec3f(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z);
    }

    /* renamed from: a */
    public void mo16870a(Vec3f vec3f, Vec3f vec3f2, Vector4fWrap ajf) {
        nativeAddPoint_Vec3fVec3fVec4f(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, ajf.x, ajf.y, ajf.z, ajf.w);
    }

    /* renamed from: a */
    public void mo16871a(Vec3f vec3f, Vec3f vec3f2, Vector4fWrap ajf, Vector4fWrap ajf2) {
        nativeAddPoint_Vec3fVec3fVec4fVec4f(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, ajf.x, ajf.y, ajf.z, ajf.w, ajf2.x, ajf2.y, ajf2.z, ajf2.w);
    }

    /* renamed from: a */
    public void mo16872a(Vec3f vec3f, Vec3f vec3f2, Vector4fWrap ajf, Vector4fWrap ajf2, Vector2fWrap aka) {
        nativeAddPoint_Vec3fVec3fVec4fVec4fVec2f(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, ajf.x, ajf.y, ajf.z, ajf.w, ajf2.x, ajf2.y, ajf2.z, ajf2.w, aka.x, aka.y);
    }

    /* renamed from: vU */
    public void mo16883vU(int i) {
        nativeReservePoints(this.eAm, i);
    }

    /* renamed from: fA */
    public void mo16879fA(int i) {
        nativeSetGeometryType(this.eAm, i);
    }

    public int apZ() {
        return nativeGetGeometryType(this.eAm);
    }

    /* renamed from: dz */
    public BoundingBox mo13489dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    public int cCJ() {
        return nativeGetNumTris(this.eAm);
    }

    public void cCK() {
        nativeRebuildAabb(this.eAm);
    }

    public int cCL() {
        return nativePointCount(this.eAm);
    }

    /* renamed from: dC */
    public long mo13487dC() {
        return nativeGetNumPrimitives(this.eAm);
    }

    /* renamed from: d */
    public Vec3f mo13484d(long j) {
        return new Vec3f(nativeGetVertice(this.eAm, j));
    }

    /* renamed from: dD */
    public long mo13488dD() {
        return nativeGetNumIndexes(this.eAm);
    }

    /* renamed from: g */
    public int mo13492g(long j) {
        return nativeGetIndex(this.eAm, j);
    }

    public void setIndexSize(int i) {
        nativeSetIndexSize(this.eAm, i);
    }

    public void setCurrentPoints(int i) {
        nativeSetCurrentPoints(this.eAm, i);
    }

    /* renamed from: a */
    public boolean mo4481a(Vec3f vec3f, Vec3f vec3f2, float f) {
        return nativeRayIntersects(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f);
    }
}
