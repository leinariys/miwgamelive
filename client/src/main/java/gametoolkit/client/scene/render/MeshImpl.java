package gametoolkit.client.scene.render;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vector4fWrap;
import logic.res.scene.C1891ah;
import logic.res.scene.C3087nb;
import p001a.C1424Ur;

@C1424Ur
/* compiled from: a */
public class MeshImpl extends PrimitiveImpl implements C1891ah {
    public MeshImpl(long j) {
        super(j);
    }

    public MeshImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42575aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native void nativeCreateIndexesVBO(long j);

    private native void nativeCreateVerticesVBO(long j);

    private native float[] nativeGetAABB(long j);

    private native long nativeGetClassSize(long j);

    private native int nativeGetIndex(long j, long j2);

    private native float[] nativeGetNormal(long j, long j2);

    private native long nativeGetNumIndexes(long j);

    private native long nativeGetNumPrimitives(long j);

    private native float[] nativeGetTangent(long j, long j2);

    private native float[] nativeGetVertice(long j, long j2);

    private native boolean nativeRayIntersects(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7);

    private native void nativeSetVBOOwner(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: dz */
    public BoundingBox mo13489dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    /* renamed from: a */
    public void mo13483a(C1891ah ahVar) {
        nativeSetVBOOwner(this.eAm, ahVar != null ? ahVar.mo762dE() : 0);
    }

    /* renamed from: dA */
    public void mo13485dA() {
        nativeCreateVerticesVBO(this.eAm);
    }

    /* renamed from: dB */
    public void mo13486dB() {
        nativeCreateIndexesVBO(this.eAm);
    }

    /* renamed from: dC */
    public long mo13487dC() {
        return nativeGetNumPrimitives(this.eAm);
    }

    /* renamed from: d */
    public Vec3f mo13484d(long j) {
        return new Vec3f(nativeGetVertice(this.eAm, j));
    }

    /* renamed from: e */
    public Vec3f mo13490e(long j) {
        return new Vec3f(nativeGetNormal(this.eAm, j));
    }

    /* renamed from: f */
    public Vector4fWrap mo13491f(long j) {
        return new Vector4fWrap(nativeGetTangent(this.eAm, j));
    }

    /* renamed from: dD */
    public long mo13488dD() {
        return nativeGetNumIndexes(this.eAm);
    }

    /* renamed from: g */
    public int mo13492g(long j) {
        return nativeGetIndex(this.eAm, j);
    }

    /* renamed from: a */
    public boolean mo4481a(Vec3f vec3f, Vec3f vec3f2, float f) {
        return nativeRayIntersects(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f);
    }
}
