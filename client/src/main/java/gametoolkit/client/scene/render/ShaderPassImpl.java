package gametoolkit.client.scene.render;

import gametoolkit.client.scene.common.NamedObjectImpl;
import logic.res.scene.C2634hp;
import logic.res.scene.C3087nb;
import logic.res.scene.C4076yy;

/* compiled from: a */
public class ShaderPassImpl extends NamedObjectImpl implements C4076yy {
    public ShaderPassImpl(long j) {
        super(j);
    }

    public ShaderPassImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42632aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native int nativeChannelTextureSetCount(long j);

    private native void nativeClearTexChannels(long j);

    private native C3087nb nativeClone(long j);

    private native boolean nativeCompile(long j);

    private native int nativeGetAlphaFunc(long j);

    private native float nativeGetAlphaRef(long j);

    private native boolean nativeGetAlphaTestEnabled(long j);

    private native boolean nativeGetBlendEnabled(long j);

    private native long nativeGetClassSize(long j);

    private native boolean nativeGetColorArrayEnabled(long j);

    private native boolean nativeGetColorMask(long j);

    private native int nativeGetCullFace(long j);

    private native boolean nativeGetCullFaceEnabled(long j);

    private native int nativeGetDepthFunc(long j);

    private native boolean nativeGetDepthMask(long j);

    private native float nativeGetDepthRangeMax(long j);

    private native float nativeGetDepthRangeMin(long j);

    private native boolean nativeGetDepthTestEnabled(long j);

    private native int nativeGetDstBlend(long j);

    private native boolean nativeGetFogEnabled(long j);

    private native boolean nativeGetLineMode(long j);

    private native float nativeGetLineWidth(long j);

    private native boolean nativeGetNormalArrayEnabled(long j);

    private native C2634hp nativeGetProgram(long j);

    private native int nativeGetSrcBlend(long j);

    private native int nativeGetTexChannel(long j, int i);

    private native int nativeGetTexCoordSet(long j, int i);

    private native int nativeGetTexEnvMode(long j, int i);

    private native boolean nativeGetUseDiffuseCubemap(long j);

    private native boolean nativeGetUseFixedFunctionLighting(long j);

    private native int nativeGetUseLights(long j);

    private native boolean nativeGetUseReflectCubemap(long j);

    private native boolean nativeGetUseSceneAmbientColor(long j);

    private native void nativeSetAlphaFunc(long j, int i);

    private native void nativeSetAlphaRef(long j, float f);

    private native void nativeSetAlphaTestEnabled(long j, boolean z);

    private native void nativeSetBlendEnabled(long j, boolean z);

    private native void nativeSetColorArrayEnabled(long j, boolean z);

    private native void nativeSetColorMask(long j, boolean z);

    private native void nativeSetCullFace(long j, int i);

    private native void nativeSetCullFaceEnabled(long j, boolean z);

    private native void nativeSetDepthFunc(long j, int i);

    private native void nativeSetDepthMask(long j, boolean z);

    private native void nativeSetDepthRangeMax(long j, float f);

    private native void nativeSetDepthRangeMin(long j, float f);

    private native void nativeSetDepthTestEnabled(long j, boolean z);

    private native void nativeSetDstBlend(long j, int i);

    private native void nativeSetFogEnabled(long j, boolean z);

    private native void nativeSetLineMode(long j, boolean z);

    private native void nativeSetLineWidth(long j, float f);

    private native void nativeSetNormalArrayEnabled(long j, boolean z);

    private native void nativeSetProgram(long j, long j2);

    private native void nativeSetSrcBlend(long j, int i);

    private native void nativeSetTexChannel(long j, int i, int i2);

    private native void nativeSetTexCoordSet(long j, int i, int i2);

    private native void nativeSetTexEnvMode(long j, int i, int i2);

    private native void nativeSetUseDiffuseCubemap(long j, boolean z);

    private native void nativeSetUseFixedFunctionLighting(long j, boolean z);

    private native void nativeSetUseLights(long j, int i);

    private native void nativeSetUseReflectCubemap(long j, boolean z);

    private native void nativeSetUseSceneAmbientColor(long j, boolean z);

    private native void nativeSetUsedTextureChannels(long j, int i);

    private native void nativeUnbindShader(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public C2634hp alk() {
        return nativeGetProgram(this.eAm);
    }

    /* renamed from: a */
    public void mo23184a(C2634hp hpVar) {
        nativeSetProgram(this.eAm, hpVar != null ? hpVar.mo762dE() : 0);
    }

    public int all() {
        return nativeGetUseLights(this.eAm);
    }

    /* renamed from: fb */
    public void mo23206fb(int i) {
        nativeSetUseLights(this.eAm, i);
    }

    public void alm() {
        nativeUnbindShader(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public boolean getUseFixedFunctionLighting() {
        return nativeGetUseFixedFunctionLighting(this.eAm);
    }

    public void setUseFixedFunctionLighting(boolean z) {
        nativeSetUseFixedFunctionLighting(this.eAm, z);
    }

    public float getAlphaRef() {
        return nativeGetAlphaRef(this.eAm);
    }

    public void setAlphaRef(float f) {
        nativeSetAlphaRef(this.eAm, f);
    }

    /* renamed from: fc */
    public void mo23207fc(int i) {
        nativeSetAlphaFunc(this.eAm, i);
    }

    public int aln() {
        return nativeGetAlphaFunc(this.eAm);
    }

    public void setAlphaTestEnabled(boolean z) {
        nativeSetAlphaTestEnabled(this.eAm, z);
    }

    public boolean alo() {
        return nativeGetAlphaTestEnabled(this.eAm);
    }

    /* renamed from: fd */
    public void mo23208fd(int i) {
        nativeSetDstBlend(this.eAm, i);
    }

    public int alp() {
        return nativeGetDstBlend(this.eAm);
    }

    /* renamed from: fe */
    public void mo23209fe(int i) {
        nativeSetSrcBlend(this.eAm, i);
    }

    public int alq() {
        return nativeGetSrcBlend(this.eAm);
    }

    public void setBlendEnabled(boolean z) {
        nativeSetBlendEnabled(this.eAm, z);
    }

    public boolean alr() {
        return nativeGetBlendEnabled(this.eAm);
    }

    public void setColorMask(boolean z) {
        nativeSetColorMask(this.eAm, z);
    }

    public boolean als() {
        return nativeGetColorMask(this.eAm);
    }

    /* renamed from: ff */
    public void mo23210ff(int i) {
        nativeSetCullFace(this.eAm, i);
    }

    public int alt() {
        return nativeGetCullFace(this.eAm);
    }

    public void setCullFaceEnabled(boolean z) {
        nativeSetCullFaceEnabled(this.eAm, z);
    }

    public boolean alu() {
        return nativeGetCullFaceEnabled(this.eAm);
    }

    public void setDepthMask(boolean z) {
        nativeSetDepthMask(this.eAm, z);
    }

    public boolean alv() {
        return nativeGetDepthMask(this.eAm);
    }

    public void setDepthTestEnabled(boolean z) {
        nativeSetDepthTestEnabled(this.eAm, z);
    }

    public boolean alw() {
        return nativeGetDepthTestEnabled(this.eAm);
    }

    /* renamed from: fg */
    public void mo23211fg(int i) {
        nativeSetDepthFunc(this.eAm, i);
    }

    public int alx() {
        return nativeGetDepthFunc(this.eAm);
    }

    public void setNormalArrayEnabled(boolean z) {
        nativeSetNormalArrayEnabled(this.eAm, z);
    }

    public boolean aly() {
        return nativeGetNormalArrayEnabled(this.eAm);
    }

    public void setColorArrayEnabled(boolean z) {
        nativeSetColorArrayEnabled(this.eAm, z);
    }

    public boolean alz() {
        return nativeGetColorArrayEnabled(this.eAm);
    }

    public float getDepthRangeMin() {
        return nativeGetDepthRangeMin(this.eAm);
    }

    public void setDepthRangeMin(float f) {
        nativeSetDepthRangeMin(this.eAm, f);
    }

    public float getDepthRangeMax() {
        return nativeGetDepthRangeMax(this.eAm);
    }

    public void setDepthRangeMax(float f) {
        nativeSetDepthRangeMax(this.eAm, f);
    }

    public int alA() {
        return nativeChannelTextureSetCount(this.eAm);
    }

    /* renamed from: v */
    public void mo23242v(int i, int i2) {
        nativeSetTexCoordSet(this.eAm, i, i2);
    }

    /* renamed from: fh */
    public int mo23212fh(int i) {
        return nativeGetTexCoordSet(this.eAm, i);
    }

    /* renamed from: w */
    public void mo23243w(int i, int i2) {
        nativeSetTexEnvMode(this.eAm, i, i2);
    }

    /* renamed from: fi */
    public int mo23213fi(int i) {
        return nativeGetTexEnvMode(this.eAm, i);
    }

    /* renamed from: x */
    public void mo23244x(int i, int i2) {
        nativeSetTexChannel(this.eAm, i, i2);
    }

    /* renamed from: fj */
    public int mo23214fj(int i) {
        return nativeGetTexChannel(this.eAm, i);
    }

    public boolean getUseSceneAmbientColor() {
        return nativeGetUseSceneAmbientColor(this.eAm);
    }

    public void setUseSceneAmbientColor(boolean z) {
        nativeSetUseSceneAmbientColor(this.eAm, z);
    }

    /* renamed from: fk */
    public void mo23215fk(int i) {
        nativeSetUsedTextureChannels(this.eAm, i);
    }

    public void clearTexChannels() {
        nativeClearTexChannels(this.eAm);
    }

    public boolean alB() {
        return nativeCompile(this.eAm);
    }

    public void setFogEnabled(boolean z) {
        nativeSetFogEnabled(this.eAm, z);
    }

    public boolean alC() {
        return nativeGetFogEnabled(this.eAm);
    }

    public boolean getUseReflectCubemap() {
        return nativeGetUseReflectCubemap(this.eAm);
    }

    public void setUseReflectCubemap(boolean z) {
        nativeSetUseReflectCubemap(this.eAm, z);
    }

    public boolean getUseDiffuseCubemap() {
        return nativeGetUseDiffuseCubemap(this.eAm);
    }

    public void setUseDiffuseCubemap(boolean z) {
        nativeSetUseDiffuseCubemap(this.eAm, z);
    }

    public void setLineMode(boolean z) {
        nativeSetLineMode(this.eAm, z);
    }

    public boolean alD() {
        return nativeGetLineMode(this.eAm);
    }

    public float getLineWidth() {
        return nativeGetLineWidth(this.eAm);
    }

    public void setLineWidth(float f) {
        nativeSetLineWidth(this.eAm, f);
    }
}
