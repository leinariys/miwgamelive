package gametoolkit.client.scene.render;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vector4fWrap;
import logic.res.scene.C3087nb;
import logic.res.scene.aBZ;
import p001a.C1424Ur;
import p001a.C6377all;

@C6377all
@C1424Ur
/* compiled from: a */
public class BillboardImpl extends ResourceImpl implements aBZ {
    public BillboardImpl(long j) {
        super(j);
    }

    public BillboardImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42464aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native void nativeCreateData(long j);

    private native float[] nativeGetAABB(long j);

    private native long nativeGetClassSize(long j);

    private native boolean nativeRayIntersects(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7);

    private native void nativeSetColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetDir(long j, float f, float f2, float f3);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: NC */
    public void mo7935NC() {
        nativeCreateData(this.eAm);
    }

    /* renamed from: dz */
    public BoundingBox mo7938dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    /* renamed from: a */
    public void mo7936a(Vector4fWrap ajf) {
        nativeSetColor(this.eAm, ajf.x, ajf.y, ajf.z, ajf.w);
    }

    /* renamed from: v */
    public void mo7939v(Vec3f vec3f) {
        nativeSetDir(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: a */
    public boolean mo7937a(Vec3f vec3f, Vec3f vec3f2, float f) {
        return nativeRayIntersects(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }
}
