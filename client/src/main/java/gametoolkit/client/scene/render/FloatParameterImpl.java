package gametoolkit.client.scene.render;

import logic.res.scene.C2724jE;
import logic.res.scene.C3087nb;
import logic.res.scene.aTM;

/* compiled from: a */
public class FloatParameterImpl extends ShaderParameterImpl implements aTM {
    public FloatParameterImpl(long j) {
        super(j);
    }

    public FloatParameterImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42507aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetValue(long j);

    private native void nativeSetValue_F(long j, float f);

    private native void nativeSetValue_ShaderParameter(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: b */
    public void mo1883b(C2724jE jEVar) {
        nativeSetValue_ShaderParameter(this.eAm, jEVar != null ? jEVar.mo762dE() : 0);
    }

    public float getValue() {
        return nativeGetValue(this.eAm);
    }

    public void setValue(float f) {
        nativeSetValue_F(this.eAm, f);
    }
}
