package gametoolkit.client.scene.render;

import com.hoplon.geometry.Vec3f;
import logic.res.scene.C1710ZI;
import logic.res.scene.C3087nb;
import logic.res.scene.C6551apD;

/* compiled from: a */
public class PannedTextureImpl extends TextureImpl implements C6551apD {
    public PannedTextureImpl(long j) {
        super(j);
    }

    public PannedTextureImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42597aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native float[] nativeGetPanSpeed(long j);

    private native C1710ZI nativeGetTexture(long j);

    private native void nativeSetPanSpeed(long j, float f, float f2, float f3);

    private native void nativeSetTexture(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: f */
    public void mo15346f(C1710ZI zi) {
        nativeSetTexture(this.eAm, zi != null ? zi.mo762dE() : 0);
    }

    /* renamed from: wb */
    public C1710ZI mo15349wb() {
        return nativeGetTexture(this.eAm);
    }

    public Vec3f getPanSpeed() {
        return new Vec3f(nativeGetPanSpeed(this.eAm));
    }

    public void setPanSpeed(Vec3f vec3f) {
        nativeSetPanSpeed(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }
}
