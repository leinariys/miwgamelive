package gametoolkit.client.scene.render;

import com.hoplon.geometry.Color;
import logic.res.scene.C3087nb;
import logic.res.scene.C5757aVp;
import p001a.C1424Ur;

@C1424Ur
/* compiled from: a */
public class ImageFloatImpl extends ResourceImpl implements C5757aVp {
    public ImageFloatImpl(long j) {
        super(j);
    }

    public ImageFloatImpl() {
        super(nativeConstructor_());
    }

    public ImageFloatImpl(int i, int i2, int i3) {
        super(nativeConstructor_III(i, i2, i3));
    }

    public ImageFloatImpl(int i, int i2, int i3, float[] fArr) {
        super(nativeConstructor_III_3F(i, i2, i3, fArr));
    }

    private static native long nativeConstructor_();

    private static native long nativeConstructor_III(int i, int i2, int i3);

    private static native long nativeConstructor_III_3F(int i, int i2, int i3, float[] fArr);

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42530aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native float[] nativeGetData(long j);

    private native int nativeGetDepth(long j);

    private native float[] nativeGetPixel(long j, int i);

    private native int nativeGetPixelCount(long j);

    private native int nativeGetSizeX(long j);

    private native int nativeGetSizeY(long j);

    private native void nativeSetPixel(long j, int i, float f, float f2, float f3, float f4);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public int getSizeX() {
        return nativeGetSizeX(this.eAm);
    }

    public int getSizeY() {
        return nativeGetSizeY(this.eAm);
    }

    public int getDepth() {
        return nativeGetDepth(this.eAm);
    }

    public float[] getData() {
        return nativeGetData(this.eAm);
    }

    /* renamed from: yc */
    public Color mo11951yc(int i) {
        return new Color(nativeGetPixel(this.eAm, i));
    }

    /* renamed from: a */
    public void mo11945a(int i, Color color) {
        nativeSetPixel(this.eAm, i, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public int dcx() {
        return nativeGetPixelCount(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }
}
