package gametoolkit.client.scene.render;

import com.hoplon.geometry.Color;
import logic.res.scene.C3038nJ;
import logic.res.scene.C6856auw;

/* compiled from: a */
public class SingleQuadFXImpl extends PostProcessingFXImpl implements C6856auw {
    public SingleQuadFXImpl(long j) {
        super(j);
    }

    public SingleQuadFXImpl() {
        super(nativeConstructor());
    }

    private static native long nativeConstructor();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42665aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native long nativeGetClassSize(long j);

    private native float[] nativeGetColor(long j);

    private native C3038nJ nativeGetMaterial(long j);

    private native void nativeSetColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetMaterial(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: b */
    public void mo16410b(C3038nJ nJVar) {
        nativeSetMaterial(this.eAm, nJVar != null ? nJVar.mo762dE() : 0);
    }

    /* renamed from: YU */
    public C3038nJ mo16409YU() {
        return nativeGetMaterial(this.eAm);
    }

    public Color getColor() {
        return new Color(nativeGetColor(this.eAm));
    }

    public void setColor(Color color) {
        nativeSetColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }
}
