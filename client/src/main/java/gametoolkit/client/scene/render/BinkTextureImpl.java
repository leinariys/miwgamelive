package gametoolkit.client.scene.render;

import logic.res.scene.C3746uT;
import logic.res.scene.aDI;
import p001a.C1424Ur;

@C1424Ur
/* compiled from: a */
public class BinkTextureImpl extends TextureImpl implements C3746uT {
    public BinkTextureImpl(long j) {
        super(j);
    }

    public BinkTextureImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42472aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeAddEndVideoListener(long j, long j2);

    private native void nativeDisableLoop(long j);

    private native void nativeEnableLoop(long j);

    private native void nativeForceDestroy(long j);

    private native long nativeGetClassSize(long j);

    private native int nativeGetVideoX(long j);

    private native int nativeGetVideoY(long j);

    private native boolean nativeHasFinished(long j);

    private native void nativePlayLoop(long j);

    private native void nativePlayOnce(long j);

    private native void nativeRemoveAllEndVideoListeners(long j);

    private native void nativeRewind(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void forceDestroy() {
        nativeForceDestroy(this.eAm);
    }

    public int ajY() {
        return nativeGetVideoX(this.eAm);
    }

    public int ajZ() {
        return nativeGetVideoY(this.eAm);
    }

    /* renamed from: a */
    public void mo22405a(aDI adi) {
        nativeAddEndVideoListener(this.eAm, adi != null ? adi.mo762dE() : 0);
    }

    public void aka() {
        nativeRemoveAllEndVideoListeners(this.eAm);
    }

    public void akb() {
        nativeDisableLoop(this.eAm);
    }

    public void akc() {
        nativeEnableLoop(this.eAm);
    }

    public boolean akd() {
        return nativeHasFinished(this.eAm);
    }

    public void rewind() {
        nativeRewind(this.eAm);
    }

    public void ake() {
        nativePlayOnce(this.eAm);
    }

    public void akf() {
        nativePlayLoop(this.eAm);
    }
}
