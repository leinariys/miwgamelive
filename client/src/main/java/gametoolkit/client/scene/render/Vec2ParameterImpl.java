package gametoolkit.client.scene.render;

import game.geometry.Vector2fWrap;
import logic.res.scene.C0336EY;
import logic.res.scene.C2724jE;
import logic.res.scene.C3087nb;

/* compiled from: a */
public class Vec2ParameterImpl extends ShaderParameterImpl implements C0336EY {
    public Vec2ParameterImpl(long j) {
        super(j);
    }

    public Vec2ParameterImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42701aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native float[] nativeGetValue(long j);

    private native void nativeSetValue_ShaderParameter(long j, long j2);

    private native void nativeSetValue_Vec2f(long j, float f, float f2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: b */
    public void mo1883b(C2724jE jEVar) {
        nativeSetValue_ShaderParameter(this.eAm, jEVar != null ? jEVar.mo762dE() : 0);
    }

    public Vector2fWrap getValue() {
        return new Vector2fWrap(nativeGetValue(this.eAm));
    }

    public void setValue(Vector2fWrap aka) {
        nativeSetValue_Vec2f(this.eAm, aka.x, aka.y);
    }
}
