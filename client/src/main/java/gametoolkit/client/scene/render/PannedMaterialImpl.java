package gametoolkit.client.scene.render;

import com.hoplon.geometry.Vec3f;
import logic.res.scene.C1074Pi;
import logic.res.scene.C3087nb;

/* compiled from: a */
public class PannedMaterialImpl extends ExtendedMaterialImpl implements C1074Pi {
    public PannedMaterialImpl(long j) {
        super(j);
    }

    public PannedMaterialImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42594aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native float[] nativeGetPanSpeed(long j);

    private native void nativeSetPanSpeed(long j, float f, float f2, float f3);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public Vec3f getPanSpeed() {
        return new Vec3f(nativeGetPanSpeed(this.eAm));
    }

    public void setPanSpeed(Vec3f vec3f) {
        nativeSetPanSpeed(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }
}
