package gametoolkit.client.scene.render;

import gametoolkit.client.scene.common.NamedObjectImpl;
import logic.res.scene.C2724jE;

/* compiled from: a */
public class ShaderParameterImpl extends NamedObjectImpl implements C2724jE {
    public ShaderParameterImpl(long j) {
        super(j);
    }

    public ShaderParameterImpl() {
        super(nativeConstructor_());
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ShaderParameterImpl(C2724jE jEVar) {
        super(nativeConstructor_ShaderParameter(jEVar != null ? jEVar.mo762dE() : 0));
    }

    private static native long nativeConstructor_();

    private static native long nativeConstructor_ShaderParameter(long j);

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42629aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native long nativeGetClassSize(long j);

    private native int nativeGetLocationId(long j);

    private native String nativeGetParameterName(long j);

    private native void nativeSetLocationId(long j, int i);

    private native void nativeSetParameterName(long j, String str);

    private native void nativeSetValue(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: b */
    public void mo1883b(C2724jE jEVar) {
        nativeSetValue(this.eAm, jEVar != null ? jEVar.mo762dE() : 0);
    }

    public int getLocationId() {
        return nativeGetLocationId(this.eAm);
    }

    public void setLocationId(int i) {
        nativeSetLocationId(this.eAm, i);
    }

    public String getParameterName() {
        return nativeGetParameterName(this.eAm);
    }

    public void setParameterName(String str) {
        nativeSetParameterName(this.eAm, str);
    }
}
