package gametoolkit.client.scene;

import com.hoplon.geometry.BoundingBox;
import game.geometry.Matrix4fWrap;
import logic.res.scene.C3087nb;
import logic.res.scene.C5891acT;
import logic.res.scene.C6980axt;

/* compiled from: a */
public class RStargateImpl extends RenderObjectImpl implements C5891acT {
    public RStargateImpl(long j) {
        super(j);
    }

    public RStargateImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42228aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native float[] nativeGetAABB(long j);

    private native long nativeGetClassSize(long j);

    private native C6980axt nativeGetGateObject(long j);

    private native C6980axt nativeGetTargetScene(long j);

    private native float[] nativeGetTargetSceneTransform(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetGateObject(long j, long j2);

    private native void nativeSetTargetScene(long j, long j2);

    private native void nativeSetTargetSceneTransform(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: c */
    public void mo12613c(Matrix4fWrap ajk) {
        nativeSetTargetSceneTransform(this.eAm, ajk.m00, ajk.m10, ajk.m20, ajk.m30, ajk.m01, ajk.m11, ajk.m21, ajk.m31, ajk.m02, ajk.m12, ajk.m22, ajk.m32, ajk.m03, ajk.m13, ajk.m23, ajk.m33);
    }

    /* renamed from: vG */
    public Matrix4fWrap mo12615vG() {
        return new Matrix4fWrap(nativeGetTargetSceneTransform(this.eAm));
    }

    /* renamed from: b */
    public void mo12612b(C6980axt axt) {
        nativeSetGateObject(this.eAm, axt != null ? axt.mo762dE() : 0);
    }

    /* renamed from: vH */
    public C6980axt mo12616vH() {
        return nativeGetGateObject(this.eAm);
    }

    /* renamed from: c */
    public void mo12614c(C6980axt axt) {
        nativeSetTargetScene(this.eAm, axt != null ? axt.mo762dE() : 0);
    }

    /* renamed from: vI */
    public C6980axt mo12617vI() {
        return nativeGetTargetScene(this.eAm);
    }

    /* renamed from: dz */
    public BoundingBox mo765dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }
}
