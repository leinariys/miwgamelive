package gametoolkit.client.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Color;
import game.geometry.Vector2fWrap;
import logic.res.scene.C0439GG;
import logic.res.scene.C3087nb;

/* compiled from: a */
public class RBillboardsCloudImpl extends RenderObjectImpl implements C0439GG {
    public RBillboardsCloudImpl(long j) {
        super(j);
    }

    public RBillboardsCloudImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m41994aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native void nativeComputeWorldSpaceAABB(long j);

    private native float[] nativeGetAABB(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetDensity(long j);

    private native float[] nativeGetMaxBillboardSize(long j);

    private native float[] nativeGetMaxColor(long j);

    private native float nativeGetMaxRotation(long j);

    private native float[] nativeGetMinBillboardSize(long j);

    private native float[] nativeGetMinColor(long j);

    private native float nativeGetMinRotation(long j);

    private native int nativeGetNumBillboards(long j);

    private native float nativeGetRadius(long j);

    private native boolean nativeIsNeedToStep(long j);

    private native void nativeSetDensity(long j, float f);

    private native void nativeSetMaxBillboardSize(long j, float f, float f2);

    private native void nativeSetMaxColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetMaxRotation(long j, float f);

    private native void nativeSetMinBillboardSize(long j, float f, float f2);

    private native void nativeSetMinColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetMinRotation(long j, float f);

    private native void nativeSetRadius(long j, float f);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public boolean isNeedToStep() {
        return nativeIsNeedToStep(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public float getRadius() {
        return nativeGetRadius(this.eAm);
    }

    public void setRadius(float f) {
        nativeSetRadius(this.eAm, f);
    }

    public float getDensity() {
        return nativeGetDensity(this.eAm);
    }

    public void setDensity(float f) {
        nativeSetDensity(this.eAm, f);
    }

    /* renamed from: fq */
    public void mo2296fq(float f) {
        nativeSetMinRotation(this.eAm, f);
    }

    public float aRm() {
        return nativeGetMinRotation(this.eAm);
    }

    /* renamed from: fr */
    public void mo2297fr(float f) {
        nativeSetMaxRotation(this.eAm, f);
    }

    public float aRn() {
        return nativeGetMaxRotation(this.eAm);
    }

    /* renamed from: R */
    public void mo2285R(Color color) {
        nativeSetMinColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public Color aRo() {
        return new Color(nativeGetMinColor(this.eAm));
    }

    /* renamed from: S */
    public void mo2286S(Color color) {
        nativeSetMaxColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public Color aRp() {
        return new Color(nativeGetMaxColor(this.eAm));
    }

    /* renamed from: b */
    public void mo2294b(Vector2fWrap aka) {
        nativeSetMinBillboardSize(this.eAm, aka.x, aka.y);
    }

    public Vector2fWrap aRq() {
        return new Vector2fWrap(nativeGetMinBillboardSize(this.eAm));
    }

    /* renamed from: c */
    public void mo2295c(Vector2fWrap aka) {
        nativeSetMaxBillboardSize(this.eAm, aka.x, aka.y);
    }

    public Vector2fWrap aRr() {
        return new Vector2fWrap(nativeGetMaxBillboardSize(this.eAm));
    }

    /* renamed from: dz */
    public BoundingBox mo765dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    public int aRs() {
        return nativeGetNumBillboards(this.eAm);
    }

    /* renamed from: gT */
    public void mo2298gT() {
        nativeComputeWorldSpaceAABB(this.eAm);
    }
}
