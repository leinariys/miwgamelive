package gametoolkit.client.scene;

import game.geometry.Vector2fWrap;
import logic.res.scene.C0628Iu;
import logic.res.scene.C3087nb;
import logic.res.scene.C6980axt;

/* compiled from: a */
public class RSpacedustImpl extends RenderObjectImpl implements C0628Iu {
    public RSpacedustImpl(long j) {
        super(j);
    }

    public RSpacedustImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42221aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native float[] nativeGetBillboardSize(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetFadeIn(long j);

    private native float nativeGetFadeOut(long j);

    private native float nativeGetNumBillboards(long j);

    private native C6980axt nativeGetReferenceObject(long j);

    private native float nativeGetRegionSize(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetBillboardSize(long j, float f, float f2);

    private native void nativeSetFadeIn(long j, float f);

    private native void nativeSetFadeOut(long j, float f);

    private native void nativeSetNumBillboards(long j, float f);

    private native void nativeSetReferenceObject(long j, long j2);

    private native void nativeSetRegionSize(long j, float f);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: Q */
    public void mo2884Q(float f) {
        nativeSetNumBillboards(this.eAm, f);
    }

    public float getRegionSize() {
        return nativeGetRegionSize(this.eAm);
    }

    public void setRegionSize(float f) {
        nativeSetRegionSize(this.eAm, f);
    }

    public Vector2fWrap getBillboardSize() {
        return new Vector2fWrap(nativeGetBillboardSize(this.eAm));
    }

    public void setBillboardSize(Vector2fWrap aka) {
        nativeSetBillboardSize(this.eAm, aka.x, aka.y);
    }

    /* renamed from: me */
    public float mo2890me() {
        return nativeGetNumBillboards(this.eAm);
    }

    public float getFadeIn() {
        return nativeGetFadeIn(this.eAm);
    }

    public void setFadeIn(float f) {
        nativeSetFadeIn(this.eAm, f);
    }

    public float getFadeOut() {
        return nativeGetFadeOut(this.eAm);
    }

    public void setFadeOut(float f) {
        nativeSetFadeOut(this.eAm, f);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: a */
    public void mo2885a(C6980axt axt) {
        nativeSetReferenceObject(this.eAm, axt != null ? axt.mo762dE() : 0);
    }

    /* renamed from: mf */
    public C6980axt mo2891mf() {
        return nativeGetReferenceObject(this.eAm);
    }
}
