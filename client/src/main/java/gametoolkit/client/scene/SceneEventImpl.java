package gametoolkit.client.scene;

import gametoolkit.client.scene.common.NamedObjectImpl;
import logic.res.scene.C2609hX;
import logic.res.scene.C3087nb;

/* compiled from: a */
public class SceneEventImpl extends NamedObjectImpl implements C2609hX {
    public SceneEventImpl(long j) {
        super(j);
    }

    public SceneEventImpl() {
        super(nativeConstructor());
    }

    private static native long nativeConstructor();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42264aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native long nativeGetClassSize(long j);

    private native String nativeGetMessage(long j);

    private native C3087nb nativeGetObject(long j);

    private native void nativeSetMessage(long j, String str);

    private native void nativeSetObject(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public String getMessage() {
        return nativeGetMessage(this.eAm);
    }

    public void setMessage(String str) {
        nativeSetMessage(this.eAm, str);
    }

    /* renamed from: a */
    public void mo19276a(C3087nb nbVar) {
        nativeSetObject(this.eAm, nbVar != null ? nbVar.mo762dE() : 0);
    }

    /* renamed from: zZ */
    public C3087nb mo19279zZ() {
        return nativeGetObject(this.eAm);
    }
}
