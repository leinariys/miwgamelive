package gametoolkit.client.scene;

import com.hoplon.geometry.Color;
import logic.res.scene.C3038nJ;
import logic.res.scene.C3087nb;
import logic.res.scene.C5355aGd;

/* compiled from: a */
public class RHudMarkImpl extends RMarkImpl implements C5355aGd {
    public RHudMarkImpl(long j) {
        super(j);
    }

    public RHudMarkImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42041aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native float nativeGetAreaRadius(long j);

    private native long nativeGetClassSize(long j);

    private native boolean nativeGetIsOffScreen(long j);

    private native C3038nJ nativeGetLock(long j);

    private native C3038nJ nativeGetMarkOut(long j);

    private native C3038nJ nativeGetMarkSelected(long j);

    private native float nativeGetMaxLockSize(long j);

    private native float nativeGetMaxVisibleDistance(long j);

    private native float nativeGetMinLockSize(long j);

    private native float[] nativeGetOutColor(long j);

    private native float[] nativeGetSatellitesColor(long j);

    private native float[] nativeGetSelectedColor(long j);

    private native boolean nativeGetShowWhenOffscreen(long j);

    private native boolean nativeIsShowSatellites(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetAreaRadius(long j, float f);

    private native void nativeSetLock(long j, long j2);

    private native void nativeSetMarkOut(long j, long j2);

    private native void nativeSetMarkSelected(long j, long j2);

    private native void nativeSetMaxLockSize(long j, float f);

    private native void nativeSetMaxScreenHeigth(long j, float f);

    private native void nativeSetMaxVisibleDistance(long j, float f);

    private native void nativeSetMinLockSize(long j, float f);

    private native void nativeSetOutColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetSatellitesColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetSelectedColor(long j, float f, float f2, float f3, float f4);

    private native void nativeSetShowSatellites(long j, boolean z);

    private native void nativeSetShowWhenOffscreen(long j, boolean z);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    public boolean cZk() {
        return nativeGetShowWhenOffscreen(this.eAm);
    }

    public void setShowWhenOffscreen(boolean z) {
        nativeSetShowWhenOffscreen(this.eAm, z);
    }

    public float getMaxVisibleDistance() {
        return nativeGetMaxVisibleDistance(this.eAm);
    }

    public void setMaxVisibleDistance(float f) {
        nativeSetMaxVisibleDistance(this.eAm, f);
    }

    public C3038nJ cZl() {
        return nativeGetMarkOut(this.eAm);
    }

    public C3038nJ cZm() {
        return nativeGetLock(this.eAm);
    }

    public C3038nJ cZn() {
        return nativeGetMarkSelected(this.eAm);
    }

    public float cZo() {
        return nativeGetMaxLockSize(this.eAm);
    }

    public float cZp() {
        return nativeGetMinLockSize(this.eAm);
    }

    public float getAreaRadius() {
        return nativeGetAreaRadius(this.eAm);
    }

    public void setAreaRadius(float f) {
        nativeSetAreaRadius(this.eAm, f);
    }

    /* renamed from: g */
    public void mo9070g(C3038nJ nJVar) {
        nativeSetMarkOut(this.eAm, nJVar != null ? nJVar.mo762dE() : 0);
    }

    /* renamed from: h */
    public void mo9076h(C3038nJ nJVar) {
        nativeSetMarkSelected(this.eAm, nJVar != null ? nJVar.mo762dE() : 0);
    }

    /* renamed from: i */
    public void mo9077i(C3038nJ nJVar) {
        nativeSetLock(this.eAm, nJVar != null ? nJVar.mo762dE() : 0);
    }

    /* renamed from: mC */
    public void mo9080mC(float f) {
        nativeSetMaxLockSize(this.eAm, f);
    }

    /* renamed from: mD */
    public void mo9081mD(float f) {
        nativeSetMinLockSize(this.eAm, f);
    }

    /* renamed from: jf */
    public void mo9079jf(float f) {
        nativeSetMaxScreenHeigth(this.eAm, f);
    }

    public Color getOutColor() {
        return new Color(nativeGetOutColor(this.eAm));
    }

    public void setOutColor(Color color) {
        nativeSetOutColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public Color getSatellitesColor() {
        return new Color(nativeGetSatellitesColor(this.eAm));
    }

    public void setSatellitesColor(Color color) {
        nativeSetSatellitesColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public Color getSelectedColor() {
        return new Color(nativeGetSelectedColor(this.eAm));
    }

    public void setSelectedColor(Color color) {
        nativeSetSelectedColor(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public boolean isShowSatellites() {
        return nativeIsShowSatellites(this.eAm);
    }

    public void setShowSatellites(boolean z) {
        nativeSetShowSatellites(this.eAm, z);
    }

    public boolean cZq() {
        return nativeGetIsOffScreen(this.eAm);
    }
}
