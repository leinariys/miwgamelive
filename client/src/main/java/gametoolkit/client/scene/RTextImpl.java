package gametoolkit.client.scene;

import logic.res.scene.C3087nb;
import logic.res.scene.C6952awr;
import logic.res.scene.aRZ;

/* compiled from: a */
public class RTextImpl extends RenderObjectImpl implements C6952awr {
    public RTextImpl(long j) {
        super(j);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RTextImpl(C6952awr awr) {
        super(nativeConstructor_RText(awr != null ? awr.mo762dE() : 0));
    }

    public RTextImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeConstructor_RText(long j);

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42238aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native aRZ nativeGetFont(long j);

    private native String nativeGetText(long j);

    private native float nativeGetTextWidthNoScale(long j);

    private native float nativeGetTextWidthWithScale(long j);

    private native String nativeGetUnicodeText(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetFont(long j, long j2);

    private native void nativeSetText(long j, String str);

    private native void nativeSetTextWidthNoScale(long j, float f);

    private native void nativeSetTextWidthWithScale(long j, float f);

    private native void nativeSetUnicodeText(long j, String str);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: a */
    public void mo16724a(aRZ arz) {
        nativeSetFont(this.eAm, arz != null ? arz.mo762dE() : 0);
    }

    /* renamed from: fy */
    public aRZ mo16728fy() {
        return nativeGetFont(this.eAm);
    }

    public String getText() {
        return nativeGetText(this.eAm);
    }

    public void setText(String str) {
        nativeSetText(this.eAm, str);
    }

    /* renamed from: jW */
    public void mo16730jW(String str) {
        nativeSetUnicodeText(this.eAm, str);
    }

    public String cvW() {
        return nativeGetUnicodeText(this.eAm);
    }

    /* renamed from: kJ */
    public void mo16731kJ(float f) {
        nativeSetTextWidthNoScale(this.eAm, f);
    }

    public float cvX() {
        return nativeGetTextWidthNoScale(this.eAm);
    }

    /* renamed from: kK */
    public void mo16732kK(float f) {
        nativeSetTextWidthWithScale(this.eAm, f);
    }

    public float cvY() {
        return nativeGetTextWidthWithScale(this.eAm);
    }
}
