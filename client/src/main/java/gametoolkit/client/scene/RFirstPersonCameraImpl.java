package gametoolkit.client.scene;

import logic.res.scene.C1144Qn;
import logic.res.scene.C2036bO;

/* compiled from: a */
public class RFirstPersonCameraImpl extends RSimulatedCameraImpl implements C1144Qn {
    public RFirstPersonCameraImpl(long j) {
        super(j);
    }

    public RFirstPersonCameraImpl() {
        super(nativeConstructor());
    }

    private static native long nativeConstructor();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42026aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native long nativeGetClassSize(long j);

    private native void nativeSetTarget(long j, long j2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: g */
    public void mo3481g(C2036bO bOVar) {
        nativeSetTarget(this.eAm, bOVar != null ? bOVar.mo762dE() : 0);
    }
}
