package gametoolkit.client.scene;

import com.hoplon.geometry.Vec3f;
import logic.res.scene.C3087nb;
import logic.res.scene.C5415aIl;
import logic.res.scene.C5851abf;

/* compiled from: a */
public class RRayImpl extends RenderObjectImpl implements C5851abf {
    public RRayImpl(long j) {
        super(j);
    }

    public RRayImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42190aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native boolean nativeIsNeedToStep(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetRay(long j, long j2, float f);

    private native void nativeTrigger(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public boolean isNeedToStep() {
        return nativeIsNeedToStep(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: a */
    public void mo12497a(C5415aIl ail, float f) {
        nativeSetRay(this.eAm, ail != null ? ail.mo762dE() : 0, f);
    }

    /* renamed from: d */
    public void mo12498d(Vec3f vec3f, Vec3f vec3f2, float f) {
        nativeTrigger(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f);
    }
}
