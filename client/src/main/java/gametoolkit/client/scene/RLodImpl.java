package gametoolkit.client.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import logic.res.scene.C3087nb;
import logic.res.scene.C5347aFv;
import logic.res.scene.C6980axt;

/* compiled from: a */
public class RLodImpl extends RenderObjectImpl implements C5347aFv {
    public RLodImpl(long j) {
        super(j);
    }

    public RLodImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42076aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native float[] nativeComputeLocalSpaceAABB(long j);

    private native void nativeComputeWorldSpaceAABB(long j);

    private native float[] nativeGetAABB(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetDistanceLod0(long j);

    private native float nativeGetDistanceLod1(long j);

    private native float nativeGetDistanceLod2(long j);

    private native C6980axt nativeGetObjectLod0(long j);

    private native C6980axt nativeGetObjectLod1(long j);

    private native C6980axt nativeGetObjectLod2(long j);

    private native float[] nativeGetTransformedAABB(long j);

    private native boolean nativeIsDisposed(long j);

    private native boolean nativeRayIntersects_Vec3fVec3fF(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7);

    private native boolean nativeRayIntersects_Vec3fVec3fFZ(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, boolean z);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetDistanceLod0(long j, float f);

    private native void nativeSetDistanceLod1(long j, float f);

    private native void nativeSetDistanceLod2(long j, float f);

    private native void nativeSetObjectLod0(long j, long j2);

    private native void nativeSetObjectLod1(long j, long j2);

    private native void nativeSetObjectLod2(long j, long j2);

    private native void nativeSetUseLightingRecursive(long j, boolean z);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: gO */
    public BoundingBox mo766gO() {
        return new BoundingBox(nativeGetTransformedAABB(this.eAm));
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: dz */
    public BoundingBox mo765dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    /* renamed from: e */
    public void mo8882e(C6980axt axt) {
        nativeSetObjectLod0(this.eAm, axt != null ? axt.mo762dE() : 0);
    }

    public C6980axt biv() {
        return nativeGetObjectLod0(this.eAm);
    }

    public float getDistanceLod0() {
        return nativeGetDistanceLod0(this.eAm);
    }

    public void setDistanceLod0(float f) {
        nativeSetDistanceLod0(this.eAm, f);
    }

    /* renamed from: f */
    public void mo8883f(C6980axt axt) {
        nativeSetObjectLod1(this.eAm, axt != null ? axt.mo762dE() : 0);
    }

    public C6980axt biw() {
        return nativeGetObjectLod1(this.eAm);
    }

    public float getDistanceLod1() {
        return nativeGetDistanceLod1(this.eAm);
    }

    public void setDistanceLod1(float f) {
        nativeSetDistanceLod1(this.eAm, f);
    }

    /* renamed from: g */
    public void mo8884g(C6980axt axt) {
        nativeSetObjectLod2(this.eAm, axt != null ? axt.mo762dE() : 0);
    }

    public C6980axt bix() {
        return nativeGetObjectLod2(this.eAm);
    }

    public float getDistanceLod2() {
        return nativeGetDistanceLod2(this.eAm);
    }

    public void setDistanceLod2(float f) {
        nativeSetDistanceLod2(this.eAm, f);
    }

    /* renamed from: gT */
    public void mo2298gT() {
        nativeComputeWorldSpaceAABB(this.eAm);
    }

    /* renamed from: gP */
    public BoundingBox mo8885gP() {
        return new BoundingBox(nativeComputeLocalSpaceAABB(this.eAm));
    }

    /* renamed from: bG */
    public void mo8878bG(boolean z) {
        nativeSetUseLightingRecursive(this.eAm, z);
    }

    /* renamed from: a */
    public boolean mo7880a(Vec3f vec3f, Vec3f vec3f2, float f, boolean z) {
        return nativeRayIntersects_Vec3fVec3fFZ(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f, z);
    }

    /* renamed from: a */
    public boolean mo7879a(Vec3f vec3f, Vec3f vec3f2, float f) {
        return nativeRayIntersects_Vec3fVec3fF(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f);
    }

    public boolean isDisposed() {
        return nativeIsDisposed(this.eAm);
    }
}
