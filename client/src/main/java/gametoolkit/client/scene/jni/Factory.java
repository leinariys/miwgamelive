package gametoolkit.client.scene.jni;

import gametoolkit.client.scene.*;
import gametoolkit.client.scene.common.ManagedObjectImpl;
import gametoolkit.client.scene.common.NamedObjectImpl;
import gametoolkit.client.scene.editor.SceneEditorImpl;
import gametoolkit.client.scene.editor.SceneViewImpl;
import gametoolkit.client.scene.gui.RGuiGraphicsImpl;
import gametoolkit.client.scene.loader.SceneLoaderImpl;
import gametoolkit.client.scene.loader.StockEntryImpl;
import gametoolkit.client.scene.loader.StockFileImpl;
import gametoolkit.client.scene.render.*;
import gametoolkit.client.scene.sound.*;
import logic.res.scene.*;

import java.util.HashMap;
import java.util.Map;

/* compiled from: a */
public class Factory {
    public static final Class[] eSs = {C1347Tf.class, C4024yF.class, C6330akq.class, C3087nb.class, C2396ep.class, C1074Pi.class, C0710KG.class, C5419aIp.class, C3038nJ.class, C0509HE.class, C2378ec.class, aQK.class, C1525WR.class, aDI.class, C5436aJg.class, C1144Qn.class, C5218aAw.class, C6856auw.class, aBN.class, C3417rK.class, C1068Pc.class, aRZ.class, C6409amR.class, C0999Oj.class, azU.class, C5739aUx.class, C6980axt.class, C6791atj.class, C3602sp.class, C3746uT.class, C5759aVr.class, C5347aFv.class, C6276ajo.class, aCY.class, C5821abB.class, C5823abD.class, C5355aGd.class, C0439GG.class, C0747Kg.class, C6592aps.class, C0676Jc.class, aTM.class, C1136Qg.class, C3733uK.class, C5682aSs.class, aBZ.class, C2036bO.class, C6495anz.class, aKQ.class, C5415aIl.class, C0336EY.class, C3674tb.class, aIF.class, C6551apD.class, aHU.class, C1813aO.class, C2593hI.class, C6100agU.class, C1117QQ.class, C6952awr.class, C1891ah.class, C5757aVp.class, aFS.class, C2517gF.class, C2634hp.class, C5738aUw.class, C0563Hn.class, C6270aji.class, C6385alt.class, C2511gB.class, C6985axy.class, C5851abf.class, C0628Iu.class, C6000aeY.class, C2832kl.class, C5891acT.class, C2416fD.class, C4076yy.class, C2609hX.class, C4027yI.class, C5957adh.class, C5929adF.class, aMF.class, C0133Be.class, aLW.class, C4039yU.class, C1007Oq.class, C2724jE.class, C3510rm.class, C1710ZI.class, C5840abU.class, aJS.class, C6498aoC.class};
    private static Map<Class, Class> map = new HashMap();

    static {
        map.put(C1347Tf.class, ColorParameterImpl.class);
        map.put(C4024yF.class, RLineImpl.class);
        map.put(C6330akq.class, RParticleSystemImpl.class);
        map.put(C3087nb.class, ManagedObjectImpl.class);
        map.put(C2396ep.class, RShapeImpl.class);
        map.put(C1074Pi.class, PannedMaterialImpl.class);
        map.put(C0710KG.class, RSimulatedCameraImpl.class);
        map.put(C5419aIp.class, StockEntryImpl.class);
        map.put(C3038nJ.class, MaterialImpl.class);
        map.put(C0509HE.class, SSoundSourceImpl.class);
        map.put(C2378ec.class, SoundBufferImpl.class);
        map.put(aQK.class, SceneManagerImpl.class);
        map.put(C1525WR.class, TiledMaterialImpl.class);
        map.put(aDI.class, EndVideoListenerImpl.class);
        map.put(C5436aJg.class, RAnimationImpl.class);
        map.put(C1144Qn.class, RFirstPersonCameraImpl.class);
        map.put(C5218aAw.class, PitchEntryImpl.class);
        map.put(C6856auw.class, SingleQuadFXImpl.class);
        map.put(aBN.class, RMeshImpl.class);
        map.put(C3417rK.class, ExtendedMaterialImpl.class);
        map.put(C1068Pc.class, Vec3ParameterImpl.class);
        map.put(aRZ.class, FontImpl.class);
        map.put(C6409amR.class, RAimImpl.class);
        map.put(C0999Oj.class, PrimitiveImpl.class);
        map.put(azU.class, RTrailImpl.class);
        map.put(C5739aUx.class, RSceneAreaInfoImpl.class);
        map.put(C6980axt.class, RenderObjectImpl.class);
        map.put(C6791atj.class, SceneImpl.class);
        map.put(C3602sp.class, LightImpl.class);
        map.put(C3746uT.class, BinkTextureImpl.class);
        map.put(C5759aVr.class, ResourceImpl.class);
        map.put(C5347aFv.class, RLodImpl.class);
        map.put(C6276ajo.class, MotionBlurFXImpl.class);
        map.put(aCY.class, RPulsingLightImpl.class);
        map.put(C5821abB.class, SceneLoaderImpl.class);
        map.put(C5823abD.class, RGroupImpl.class);
        map.put(C5355aGd.class, RHudMarkImpl.class);
        map.put(C0439GG.class, RBillboardsCloudImpl.class);
        map.put(C0747Kg.class, RParticleGroupImpl.class);
        map.put(C6592aps.class, SceneViewImpl.class);
        map.put(C0676Jc.class, RenderTargetImpl.class);
        map.put(aTM.class, FloatParameterImpl.class);
        map.put(C1136Qg.class, RLightConeImpl.class);
        map.put(C3733uK.class, AnimatedTextureImpl.class);
        map.put(C5682aSs.class, NamedObjectImpl.class);
        map.put(aBZ.class, BillboardImpl.class);
        map.put(C2036bO.class, SceneObjectImpl.class);
        map.put(C6495anz.class, SoundListenerImpl.class);
        map.put(aKQ.class, RModelImpl.class);
        map.put(C5415aIl.class, RBillboardImpl.class);
        map.put(C0336EY.class, Vec2ParameterImpl.class);
        map.put(C3674tb.class, ShaderImpl.class);
        map.put(aIF.class, FontBufferImpl.class);
        map.put(C6551apD.class, PannedTextureImpl.class);
        map.put(aHU.class, SceneSDLWindowImpl.class);
        map.put(C1813aO.class, SMultiLayerMusicImpl.class);
        map.put(C2593hI.class, RLightImpl.class);
        map.put(C6100agU.class, PostProcessingFXImpl.class);
        map.put(C1117QQ.class, SceneEditorImpl.class);
        map.put(C6952awr.class, RTextImpl.class);
        map.put(C1891ah.class, MeshImpl.class);
        map.put(C5757aVp.class, ImageFloatImpl.class);
        map.put(aFS.class, MilesSoundListenerImpl.class);
        map.put(C2517gF.class, IntParameterImpl.class);
        map.put(C2634hp.class, ShaderProgramImpl.class);
        map.put(C5738aUw.class, GlowFXImpl.class);
        map.put(C0563Hn.class, MilesSoundBufferImpl.class);
        map.put(C6270aji.class, RNuclearLightImpl.class);
        map.put(C6385alt.class, StockFileImpl.class);
        map.put(C2511gB.class, TiledTextureImpl.class);
        map.put(C6985axy.class, ExpandableMeshImpl.class);
        map.put(C5851abf.class, RRayImpl.class);
        map.put(C0628Iu.class, RSpacedustImpl.class);
        map.put(C6000aeY.class, ContainerImpl.class);
        map.put(C2832kl.class, RChaseCameraImpl.class);
        map.put(C5891acT.class, RStargateImpl.class);
        map.put(C2416fD.class, SPitchedSetImpl.class);
        map.put(C4076yy.class, ShaderPassImpl.class);
        map.put(C2609hX.class, SceneEventImpl.class);
        map.put(C4027yI.class, SoundObjectImpl.class);
        map.put(C5957adh.class, RMark3DImpl.class);
        map.put(C5929adF.class, RMarkImpl.class);
        map.put(aMF.class, RCameraImpl.class);
        map.put(C0133Be.class, RPanoramaImpl.class);
        map.put(aLW.class, RRotorImpl.class);
        map.put(C4039yU.class, SMusicImpl.class);
        map.put(C1007Oq.class, RPlanetImpl.class);
        map.put(C2724jE.class, ShaderParameterImpl.class);
        map.put(C3510rm.class, RGuiGraphicsImpl.class);
        map.put(C1710ZI.class, TextureImpl.class);
        map.put(C5840abU.class, RFlowerShotImpl.class);
        map.put(aJS.class, ROrbitalCameraImpl.class);
        map.put(C6498aoC.class, ShaderProgramObjectImpl.class);
    }

    private static native void nativeInit(long j, Class cls);

    public static void init() {
        nativeInit(ColorParameterImpl.m42475aV(), ColorParameterImpl.class);
        nativeInit(RLineImpl.m42064aV(), RLineImpl.class);
        nativeInit(RParticleSystemImpl.m42158aV(), RParticleSystemImpl.class);
        nativeInit(RShapeImpl.m42206aV(), RShapeImpl.class);
        nativeInit(PannedMaterialImpl.m42594aV(), PannedMaterialImpl.class);
        nativeInit(RSimulatedCameraImpl.m42217aV(), RSimulatedCameraImpl.class);
        nativeInit(StockEntryImpl.m42450aV(), StockEntryImpl.class);
        nativeInit(MaterialImpl.m42550aV(), MaterialImpl.class);
        nativeInit(SSoundSourceImpl.m42736aV(), SSoundSourceImpl.class);
        nativeInit(SoundBufferImpl.m42741aV(), SoundBufferImpl.class);
        nativeInit(SceneManagerImpl.m42316aV(), SceneManagerImpl.class);
        nativeInit(TiledMaterialImpl.m42693aV(), TiledMaterialImpl.class);
        nativeInit(EndVideoListenerImpl.m42479aV(), EndVideoListenerImpl.class);
        nativeInit(RAnimationImpl.m41977aV(), RAnimationImpl.class);
        nativeInit(RFirstPersonCameraImpl.m42026aV(), RFirstPersonCameraImpl.class);
        nativeInit(PitchEntryImpl.m42714aV(), PitchEntryImpl.class);
        nativeInit(SingleQuadFXImpl.m42665aV(), SingleQuadFXImpl.class);
        nativeInit(RMeshImpl.m42103aV(), RMeshImpl.class);
        nativeInit(ExtendedMaterialImpl.m42501aV(), ExtendedMaterialImpl.class);
        nativeInit(Vec3ParameterImpl.m42705aV(), Vec3ParameterImpl.class);
        nativeInit(FontImpl.m42514aV(), FontImpl.class);
        nativeInit(RAimImpl.m41971aV(), RAimImpl.class);
        nativeInit(PrimitiveImpl.m42604aV(), PrimitiveImpl.class);
        nativeInit(RTrailImpl.m42246aV(), RTrailImpl.class);
        nativeInit(RSceneAreaInfoImpl.m42198aV(), RSceneAreaInfoImpl.class);
        nativeInit(RenderObjectImpl.m42253aV(), RenderObjectImpl.class);
        nativeInit(SceneImpl.m42268aV(), SceneImpl.class);
        nativeInit(LightImpl.m42539aV(), LightImpl.class);
        nativeInit(BinkTextureImpl.m42472aV(), BinkTextureImpl.class);
        nativeInit(ResourceImpl.m42616aV(), ResourceImpl.class);
        nativeInit(RLodImpl.m42076aV(), RLodImpl.class);
        nativeInit(MotionBlurFXImpl.m42589aV(), MotionBlurFXImpl.class);
        nativeInit(RPulsingLightImpl.m42184aV(), RPulsingLightImpl.class);
        nativeInit(SceneLoaderImpl.m42425aV(), SceneLoaderImpl.class);
        nativeInit(RGroupImpl.m42035aV(), RGroupImpl.class);
        nativeInit(RHudMarkImpl.m42041aV(), RHudMarkImpl.class);
        nativeInit(RBillboardsCloudImpl.m41994aV(), RBillboardsCloudImpl.class);
        nativeInit(RParticleGroupImpl.m42153aV(), RParticleGroupImpl.class);
        nativeInit(SceneViewImpl.m42377aV(), SceneViewImpl.class);
        nativeInit(RenderTargetImpl.m42611aV(), RenderTargetImpl.class);
        nativeInit(FloatParameterImpl.m42507aV(), FloatParameterImpl.class);
        nativeInit(RLightConeImpl.m42050aV(), RLightConeImpl.class);
        nativeInit(AnimatedTextureImpl.m42459aV(), AnimatedTextureImpl.class);
        nativeInit(BillboardImpl.m42464aV(), BillboardImpl.class);
        nativeInit(RModelImpl.m42113aV(), RModelImpl.class);
        nativeInit(RBillboardImpl.m41981aV(), RBillboardImpl.class);
        nativeInit(Vec2ParameterImpl.m42701aV(), Vec2ParameterImpl.class);
        nativeInit(ShaderImpl.m42620aV(), ShaderImpl.class);
        nativeInit(FontBufferImpl.m42511aV(), FontBufferImpl.class);
        nativeInit(PannedTextureImpl.m42597aV(), PannedTextureImpl.class);
        nativeInit(SceneSDLWindowImpl.m42347aV(), SceneSDLWindowImpl.class);
        nativeInit(SMultiLayerMusicImpl.m42719aV(), SMultiLayerMusicImpl.class);
        nativeInit(RLightImpl.m42057aV(), RLightImpl.class);
        nativeInit(SceneEditorImpl.m42371aV(), SceneEditorImpl.class);
        nativeInit(RTextImpl.m42238aV(), RTextImpl.class);
        nativeInit(MeshImpl.m42575aV(), MeshImpl.class);
        nativeInit(ImageFloatImpl.m42530aV(), ImageFloatImpl.class);
        nativeInit(MilesSoundListenerImpl.m42712aV(), MilesSoundListenerImpl.class);
        nativeInit(IntParameterImpl.m42535aV(), IntParameterImpl.class);
        nativeInit(ShaderProgramImpl.m42649aV(), ShaderProgramImpl.class);
        nativeInit(GlowFXImpl.m42525aV(), GlowFXImpl.class);
        nativeInit(MilesSoundBufferImpl.m42709aV(), MilesSoundBufferImpl.class);
        nativeInit(RNuclearLightImpl.m42122aV(), RNuclearLightImpl.class);
        nativeInit(StockFileImpl.m42454aV(), StockFileImpl.class);
        nativeInit(TiledTextureImpl.m42696aV(), TiledTextureImpl.class);
        nativeInit(ExpandableMeshImpl.m42483aV(), ExpandableMeshImpl.class);
        nativeInit(RRayImpl.m42190aV(), RRayImpl.class);
        nativeInit(RSpacedustImpl.m42221aV(), RSpacedustImpl.class);
        nativeInit(RChaseCameraImpl.m42019aV(), RChaseCameraImpl.class);
        nativeInit(RStargateImpl.m42228aV(), RStargateImpl.class);
        nativeInit(SPitchedSetImpl.m42729aV(), SPitchedSetImpl.class);
        nativeInit(ShaderPassImpl.m42632aV(), ShaderPassImpl.class);
        nativeInit(SceneEventImpl.m42264aV(), SceneEventImpl.class);
        nativeInit(RMark3DImpl.m42089aV(), RMark3DImpl.class);
        nativeInit(RMarkImpl.m42094aV(), RMarkImpl.class);
        nativeInit(RCameraImpl.m42005aV(), RCameraImpl.class);
        nativeInit(RPanoramaImpl.m42146aV(), RPanoramaImpl.class);
        nativeInit(RRotorImpl.m42195aV(), RRotorImpl.class);
        nativeInit(SMusicImpl.m42725aV(), SMusicImpl.class);
        nativeInit(RPlanetImpl.m42180aV(), RPlanetImpl.class);
        nativeInit(ShaderParameterImpl.m42629aV(), ShaderParameterImpl.class);
        nativeInit(RGuiGraphicsImpl.m42384aV(), RGuiGraphicsImpl.class);
        nativeInit(TextureImpl.m42671aV(), TextureImpl.class);
        nativeInit(RFlowerShotImpl.m42029aV(), RFlowerShotImpl.class);
        nativeInit(ROrbitalCameraImpl.m42138aV(), ROrbitalCameraImpl.class);
        nativeInit(ShaderProgramObjectImpl.m42662aV(), ShaderProgramObjectImpl.class);
        m42424pr(0);
    }

    /* renamed from: pr */
    public static Object[] m42424pr(int i) {
        if (i * 10 <= 1) {
            return null;
        }
        return new Object[]{new ColorParameterImpl(0), new RLineImpl(0), new RParticleSystemImpl(0), new RShapeImpl(0), new PannedMaterialImpl(0), new RSimulatedCameraImpl(0), new StockEntryImpl(0), new MaterialImpl(0), new SSoundSourceImpl(0), new SoundBufferImpl(0), new SceneManagerImpl(0), new TiledMaterialImpl(0), new EndVideoListenerImpl(0), new RAnimationImpl(0), new RFirstPersonCameraImpl(0), new PitchEntryImpl(0), new SingleQuadFXImpl(0), new RMeshImpl(0), new ExtendedMaterialImpl(0), new Vec3ParameterImpl(0), new FontImpl(0), new RAimImpl(0), new PrimitiveImpl(0), new RTrailImpl(0), new RSceneAreaInfoImpl(0), new RenderObjectImpl(0), new SceneImpl(0), new LightImpl(0), new BinkTextureImpl(0), new ResourceImpl(0), new RLodImpl(0), new MotionBlurFXImpl(0), new RPulsingLightImpl(0), new SceneLoaderImpl(0), new RGroupImpl(0), new RHudMarkImpl(0), new RBillboardsCloudImpl(0), new RParticleGroupImpl(0), new SceneViewImpl(0), new RenderTargetImpl(0), new FloatParameterImpl(0), new RLightConeImpl(0), new AnimatedTextureImpl(0), new BillboardImpl(0), new RModelImpl(0), new RBillboardImpl(0), new Vec2ParameterImpl(0), new ShaderImpl(0), new FontBufferImpl(0), new PannedTextureImpl(0), new SceneSDLWindowImpl(0), new SMultiLayerMusicImpl(0), new RLightImpl(0), new SceneEditorImpl(0), new RTextImpl(0), new MeshImpl(0), new ImageFloatImpl(0), new MilesSoundListenerImpl(0), new IntParameterImpl(0), new ShaderProgramImpl(0), new GlowFXImpl(0), new MilesSoundBufferImpl(0), new RNuclearLightImpl(0), new StockFileImpl(0), new TiledTextureImpl(0), new ExpandableMeshImpl(0), new RRayImpl(0), new RSpacedustImpl(0), new RChaseCameraImpl(0), new RStargateImpl(0), new SPitchedSetImpl(0), new ShaderPassImpl(0), new SceneEventImpl(0), new RMark3DImpl(0), new RMarkImpl(0), new RCameraImpl(0), new RPanoramaImpl(0), new RRotorImpl(0), new SMusicImpl(0), new RPlanetImpl(0), new ShaderParameterImpl(0), new RGuiGraphicsImpl(0), new TextureImpl(0), new RFlowerShotImpl(0), new ROrbitalCameraImpl(0), new ShaderProgramObjectImpl(0)};
    }

    /* renamed from: V */
    public static <T> Class<T> m42423V(Class<T> cls) {
        Class<T> cls2 = map.get(cls);
        if (cls2 != null) {
            return cls2;
        }
        throw new IllegalArgumentException("Can't find a suitable implementation for: " + cls.getName());
    }

    public static <T> T create(Class<T> cls) {
        Class<T> V = m42423V(cls);
        try {
            return V.newInstance();
        } catch (Exception e) {
            throw new IllegalStateException("Error creating class: " + V.getName(), e);
        }
    }
}
