package gametoolkit.client.scene.common;

import logic.res.scene.C3087nb;
import logic.res.scene.C5682aSs;

/* compiled from: a */
public abstract class NamedObjectImpl extends ManagedObjectImpl implements C5682aSs {
    public NamedObjectImpl(long j) {
        super(j);
    }

    public NamedObjectImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42368aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native String nativeGetName(long j);

    private native void nativeSetName(long j, String str);

    private native String nativeToString(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public String getName() {
        return nativeGetName(this.eAm);
    }

    public void setName(String str) {
        nativeSetName(this.eAm, str);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public String toString() {
        return nativeToString(this.eAm);
    }
}
