package gametoolkit.client.scene.common;

import logic.res.scene.C1536WZ;
import logic.res.scene.C3087nb;

/* compiled from: a */
public abstract class ManagedObjectImpl extends C1536WZ implements C3087nb {
    public ManagedObjectImpl(long j) {
        super(j);
    }

    public ManagedObjectImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native int nativeGetNumAllocations();

    private static native long nativeGetStaticClassInfoPointer();

    private static native long nativeGetTotalUsedMemory();

    private static native void nativePrintAllocatedObjects();

    /* renamed from: aV */
    public static long m42361aV() {
        return nativeGetStaticClassInfoPointer();
    }

    public static int aVg() {
        return nativeGetNumAllocations();
    }

    public static long aVh() {
        return nativeGetTotalUsedMemory();
    }

    public static void aVi() {
        nativePrintAllocatedObjects();
    }

    private native C3087nb nativeClone(long j);

    private native void nativeDecReferenceCount(long j);

    private native long nativeGetClassSize(long j);

    private native int nativeGetHandle(long j);

    private native void nativeGetNextHandle(long j);

    private native int nativeGetReferenceCount(long j);

    private native void nativeIncReferenceCount(long j);

    private native String nativeToString(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: PO */
    public void mo6596PO() {
        nativeIncReferenceCount(this.eAm);
    }

    /* renamed from: PP */
    public void mo6597PP() {
        nativeDecReferenceCount(this.eAm);
    }

    /* renamed from: PQ */
    public int mo20808PQ() {
        return nativeGetReferenceCount(this.eAm);
    }

    public String toString() {
        return nativeToString(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb clone() {
        return nativeClone(this.eAm);
    }

    public int getHandle() {
        return nativeGetHandle(this.eAm);
    }

    /* renamed from: PR */
    public void mo20809PR() {
        nativeGetNextHandle(this.eAm);
    }
}
