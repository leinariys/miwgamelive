package gametoolkit.client.scene.sound;

import logic.res.scene.aFS;

/* compiled from: a */
public class MilesSoundListenerImpl extends SoundListenerImpl implements aFS {
    public MilesSoundListenerImpl(long j) {
        super(j);
    }

    public MilesSoundListenerImpl() {
        super(nativeConstructor());
    }

    private static native long nativeConstructor();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42712aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native long nativeGetClassSize(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }
}
