package gametoolkit.client.scene.sound;

import gametoolkit.client.scene.SceneObjectImpl;
import logic.res.scene.C6495anz;

/* compiled from: a */
public abstract class SoundListenerImpl extends SceneObjectImpl implements C6495anz {
    public SoundListenerImpl(long j) {
        super(j);
    }

    public SoundListenerImpl() {
        super(nativeConstructor());
    }

    private static native long nativeConstructor();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42744aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native long nativeGetClassSize(long j);

    private native float nativeGetGain(long j);

    private native boolean nativeIsNeedToStep(long j);

    private native void nativeSetGain_(long j);

    private native void nativeSetGain_F(long j, float f);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public float getGain() {
        return nativeGetGain(this.eAm);
    }

    public void setGain(float f) {
        nativeSetGain_F(this.eAm, f);
    }

    public boolean isNeedToStep() {
        return nativeIsNeedToStep(this.eAm);
    }

    public void clA() {
        nativeSetGain_(this.eAm);
    }
}
