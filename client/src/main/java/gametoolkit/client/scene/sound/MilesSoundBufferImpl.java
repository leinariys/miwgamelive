package gametoolkit.client.scene.sound;

import logic.res.scene.C0563Hn;

/* compiled from: a */
public class MilesSoundBufferImpl extends SoundBufferImpl implements C0563Hn {
    public MilesSoundBufferImpl(long j) {
        super(j);
    }

    public MilesSoundBufferImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42709aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C0563Hn nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native String nativeGetExtension(long j);

    private native int nativeGetLength(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public int getLength() {
        return nativeGetLength(this.eAm);
    }

    public String getExtension() {
        return nativeGetExtension(this.eAm);
    }

    /* renamed from: aRy */
    public C0563Hn mo764dy() {
        return nativeClone(this.eAm);
    }
}
