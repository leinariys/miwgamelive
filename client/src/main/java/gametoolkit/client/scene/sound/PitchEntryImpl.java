package gametoolkit.client.scene.sound;

import gametoolkit.client.scene.common.NamedObjectImpl;
import logic.res.scene.C2378ec;
import logic.res.scene.C3087nb;
import logic.res.scene.C5218aAw;

/* compiled from: a */
public class PitchEntryImpl extends NamedObjectImpl implements C5218aAw {
    public PitchEntryImpl(long j) {
        super(j);
    }

    public PitchEntryImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42714aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetFinalControllerValue(long j);

    private native float nativeGetFinalPitchValue(long j);

    private native boolean nativeGetFirst(long j);

    private native float nativeGetGainMultiplier(long j);

    private native float nativeGetInitialControllerValue(long j);

    private native float nativeGetInitialPitchValue(long j);

    private native boolean nativeGetLast(long j);

    private native C2378ec nativeGetSoundBuffer(long j);

    private native void nativeSetFinalControllerValue(long j, float f);

    private native void nativeSetFinalPitchValue(long j, float f);

    private native void nativeSetFirst(long j, boolean z);

    private native void nativeSetGainMultiplier(long j, float f);

    private native void nativeSetInitialControllerValue(long j, float f);

    private native void nativeSetInitialPitchValue(long j, float f);

    private native void nativeSetLast(long j, boolean z);

    private native void nativeSetParentGain(long j, float f);

    private native void nativeSetSoundBuffer(long j, long j2);

    private native boolean nativeUpdate(long j, float f);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public float getInitialControllerValue() {
        return nativeGetInitialControllerValue(this.eAm);
    }

    public void setInitialControllerValue(float f) {
        nativeSetInitialControllerValue(this.eAm, f);
    }

    public float getFinalControllerValue() {
        return nativeGetFinalControllerValue(this.eAm);
    }

    public void setFinalControllerValue(float f) {
        nativeSetFinalControllerValue(this.eAm, f);
    }

    public float getInitialPitchValue() {
        return nativeGetInitialPitchValue(this.eAm);
    }

    public void setInitialPitchValue(float f) {
        nativeSetInitialPitchValue(this.eAm, f);
    }

    public float getFinalPitchValue() {
        return nativeGetFinalPitchValue(this.eAm);
    }

    public void setFinalPitchValue(float f) {
        nativeSetFinalPitchValue(this.eAm, f);
    }

    public boolean getFirst() {
        return nativeGetFirst(this.eAm);
    }

    public void setFirst(boolean z) {
        nativeSetFirst(this.eAm, z);
    }

    public boolean getLast() {
        return nativeGetLast(this.eAm);
    }

    public void setLast(boolean z) {
        nativeSetLast(this.eAm, z);
    }

    public void setParentGain(float f) {
        nativeSetParentGain(this.eAm, f);
    }

    /* renamed from: b */
    public void mo7786b(C2378ec ecVar) {
        nativeSetSoundBuffer(this.eAm, ecVar != null ? ecVar.mo762dE() : 0);
    }

    public C2378ec cIb() {
        return nativeGetSoundBuffer(this.eAm);
    }

    /* renamed from: lg */
    public boolean mo7795lg(float f) {
        return nativeUpdate(this.eAm, f);
    }

    public float getGainMultiplier() {
        return nativeGetGainMultiplier(this.eAm);
    }

    public void setGainMultiplier(float f) {
        nativeSetGainMultiplier(this.eAm, f);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }
}
