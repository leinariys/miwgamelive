package gametoolkit.client.scene.sound;

import game.geometry.Vector2fWrap;
import logic.res.scene.C2416fD;
import logic.res.scene.C3087nb;
import logic.res.scene.C5218aAw;

/* compiled from: a */
public class SPitchedSetImpl extends SoundObjectImpl implements C2416fD {
    public SPitchedSetImpl(long j) {
        super(j);
    }

    public SPitchedSetImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42729aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeAddPitchEntry(long j, long j2);

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native float[] nativeGetDistances(long j);

    private native float nativeGetGain(long j);

    private native C5218aAw nativeGetPitchEntry(long j, int i);

    private native boolean nativeIsAlive(long j);

    private native boolean nativeIsPlaying(long j);

    private native boolean nativeIsValid(long j);

    private native void nativeOnRemove(long j);

    private native int nativePitchEntryCount(long j);

    private native boolean nativePlay(long j);

    private native void nativeRemovePitchEntry_I(long j, int i);

    private native void nativeRemovePitchEntry_PitchEntry(long j, long j2);

    private native void nativeSetControllerValue(long j, float f);

    private native void nativeSetDistances(long j, float f, float f2);

    private native void nativeSetGain(long j, float f);

    private native void nativeSetPitchEntry(long j, int i, long j2);

    private native boolean nativeStop(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public float getGain() {
        return nativeGetGain(this.eAm);
    }

    public void setGain(float f) {
        nativeSetGain(this.eAm, f);
    }

    public Vector2fWrap getDistances() {
        return new Vector2fWrap(nativeGetDistances(this.eAm));
    }

    public void setDistances(Vector2fWrap aka) {
        nativeSetDistances(this.eAm, aka.x, aka.y);
    }

    public boolean play() {
        return nativePlay(this.eAm);
    }

    public boolean stop() {
        return nativeStop(this.eAm);
    }

    public boolean isPlaying() {
        return nativeIsPlaying(this.eAm);
    }

    public boolean isValid() {
        return nativeIsValid(this.eAm);
    }

    public boolean isAlive() {
        return nativeIsAlive(this.eAm);
    }

    /* renamed from: a */
    public void mo18363a(C5218aAw aaw) {
        nativeAddPitchEntry(this.eAm, aaw != null ? aaw.mo762dE() : 0);
    }

    public int pitchEntryCount() {
        return nativePitchEntryCount(this.eAm);
    }

    /* renamed from: aZ */
    public C5218aAw mo18364aZ(int i) {
        return nativeGetPitchEntry(this.eAm, i);
    }

    /* renamed from: a */
    public void mo18362a(int i, C5218aAw aaw) {
        nativeSetPitchEntry(this.eAm, i, aaw != null ? aaw.mo762dE() : 0);
    }

    public void removePitchEntry(int i) {
        nativeRemovePitchEntry_I(this.eAm, i);
    }

    /* renamed from: b */
    public void mo18365b(C5218aAw aaw) {
        nativeRemovePitchEntry_PitchEntry(this.eAm, aaw != null ? aaw.mo762dE() : 0);
    }

    public void setControllerValue(float f) {
        nativeSetControllerValue(this.eAm, f);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public void onRemove() {
        nativeOnRemove(this.eAm);
    }
}
