package gametoolkit.client.scene.sound;

import game.geometry.Vector2fWrap;
import logic.res.scene.C0509HE;
import logic.res.scene.C2036bO;
import logic.res.scene.C2378ec;
import logic.res.scene.C3087nb;

/* compiled from: a */
public class SSoundSourceImpl extends SoundObjectImpl implements C0509HE {
    public SSoundSourceImpl(long j) {
        super(j);
    }

    public SSoundSourceImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    private static native int nativeGetTotalMilesSounds();

    /* renamed from: aV */
    public static long m42736aV() {
        return nativeGetStaticClassInfoPointer();
    }

    public static int getTotalMilesSounds() {
        return nativeGetTotalMilesSounds();
    }

    private native C3087nb nativeClone(long j);

    private native boolean nativeFadeOut(long j, float f);

    private native float nativeGetAudibleRadius(long j);

    private native C2378ec nativeGetBuffer(long j);

    private native long nativeGetClassSize(long j);

    private native float[] nativeGetDistances(long j);

    private native float nativeGetGain(long j);

    private native boolean nativeGetIs3d(long j);

    private native int nativeGetLoopCount(long j);

    private native boolean nativeIsAlive(long j);

    private native boolean nativeIsPlaying(long j);

    private native boolean nativeIsValid(long j);

    private native void nativeOnRemove(long j);

    private native boolean nativePlay(long j);

    private native void nativeReset(long j);

    private native void nativeSetAudibleRadius(long j, float f);

    private native void nativeSetBuffer(long j, long j2);

    private native void nativeSetDistances(long j, float f, float f2);

    private native void nativeSetGain(long j, float f);

    private native void nativeSetIs3d(long j, boolean z);

    private native void nativeSetLoopCount(long j, int i);

    private native void nativeSetPitch(long j, float f);

    private native void nativeSetSquaredSoundSource(long j, long j2);

    private native boolean nativeStop(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public int getLoopCount() {
        return nativeGetLoopCount(this.eAm);
    }

    public void setLoopCount(int i) {
        nativeSetLoopCount(this.eAm, i);
    }

    public float getGain() {
        return nativeGetGain(this.eAm);
    }

    public void setGain(float f) {
        nativeSetGain(this.eAm, f);
    }

    public void setPitch(float f) {
        nativeSetPitch(this.eAm, f);
    }

    public boolean play() {
        return nativePlay(this.eAm);
    }

    public boolean fadeOut(float f) {
        return nativeFadeOut(this.eAm, f);
    }

    public boolean stop() {
        return nativeStop(this.eAm);
    }

    public boolean isPlaying() {
        return nativeIsPlaying(this.eAm);
    }

    public boolean isValid() {
        return nativeIsValid(this.eAm);
    }

    /* renamed from: a */
    public void mo2493a(C2378ec ecVar) {
        nativeSetBuffer(this.eAm, ecVar != null ? ecVar.mo762dE() : 0);
    }

    public C2378ec aqG() {
        return nativeGetBuffer(this.eAm);
    }

    public boolean isAlive() {
        return nativeIsAlive(this.eAm);
    }

    public float getAudibleRadius() {
        return nativeGetAudibleRadius(this.eAm);
    }

    public void setAudibleRadius(float f) {
        nativeSetAudibleRadius(this.eAm, f);
    }

    public void reset() {
        nativeReset(this.eAm);
    }

    public Vector2fWrap getDistances() {
        return new Vector2fWrap(nativeGetDistances(this.eAm));
    }

    public void setDistances(Vector2fWrap aka) {
        nativeSetDistances(this.eAm, aka.x, aka.y);
    }

    public boolean getIs3d() {
        return nativeGetIs3d(this.eAm);
    }

    public void setIs3d(boolean z) {
        nativeSetIs3d(this.eAm, z);
    }

    /* renamed from: e */
    public void mo2495e(C2036bO bOVar) {
        nativeSetSquaredSoundSource(this.eAm, bOVar != null ? bOVar.mo762dE() : 0);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public void onRemove() {
        nativeOnRemove(this.eAm);
    }
}
