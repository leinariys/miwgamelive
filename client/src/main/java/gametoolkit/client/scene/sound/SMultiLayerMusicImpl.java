package gametoolkit.client.scene.sound;

import logic.res.scene.C1813aO;
import logic.res.scene.C3087nb;
import logic.res.scene.C4039yU;

/* compiled from: a */
public class SMultiLayerMusicImpl extends SoundObjectImpl implements C1813aO {
    public SMultiLayerMusicImpl(long j) {
        super(j);
    }

    public SMultiLayerMusicImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42719aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeAddMusic(long j, long j2);

    private native void nativeChangeLayer(long j, int i, float f);

    private native C3087nb nativeClone(long j);

    private native boolean nativeFadeOut(long j, float f);

    private native long nativeGetClassSize(long j);

    private native float nativeGetGain(long j);

    private native C4039yU nativeGetMusic(long j, int i);

    private native int nativeMusicCount(long j);

    private native boolean nativePause(long j);

    private native boolean nativePlay(long j);

    private native void nativeSetGain(long j, float f);

    private native void nativeSetMusic(long j, int i, long j2);

    private native boolean nativeStop(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public boolean play() {
        return nativePlay(this.eAm);
    }

    public boolean stop() {
        return nativeStop(this.eAm);
    }

    public boolean pause() {
        return nativePause(this.eAm);
    }

    public boolean fadeOut(float f) {
        return nativeFadeOut(this.eAm, f);
    }

    public void changeLayer(int i, float f) {
        nativeChangeLayer(this.eAm, i, f);
    }

    /* renamed from: a */
    public void mo10537a(C4039yU yUVar) {
        nativeAddMusic(this.eAm, yUVar != null ? yUVar.mo762dE() : 0);
    }

    public int musicCount() {
        return nativeMusicCount(this.eAm);
    }

    /* renamed from: L */
    public C4039yU mo10535L(int i) {
        return nativeGetMusic(this.eAm, i);
    }

    /* renamed from: a */
    public void mo10536a(int i, C4039yU yUVar) {
        nativeSetMusic(this.eAm, i, yUVar != null ? yUVar.mo762dE() : 0);
    }

    public float getGain() {
        return nativeGetGain(this.eAm);
    }

    public void setGain(float f) {
        nativeSetGain(this.eAm, f);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }
}
