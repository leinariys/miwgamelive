package gametoolkit.client.scene.sound;

import gametoolkit.client.scene.common.NamedObjectImpl;
import logic.res.scene.C2378ec;
import logic.res.scene.C3087nb;

/* compiled from: a */
public class SoundBufferImpl extends NamedObjectImpl implements C2378ec {
    public SoundBufferImpl(long j) {
        super(j);
    }

    public SoundBufferImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42741aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native int nativeGetBitDepth(long j);

    private native int nativeGetBufferId(long j);

    private native int nativeGetChannels(long j);

    private native long nativeGetClassSize(long j);

    private native long nativeGetSampleCount(long j);

    private native int nativeGetSampleRate(long j);

    private native float nativeGetTimeLength(long j);

    private native boolean nativeIsValid(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public boolean isValid() {
        return nativeIsValid(this.eAm);
    }

    public float getTimeLength() {
        return nativeGetTimeLength(this.eAm);
    }

    public long getSampleCount() {
        return nativeGetSampleCount(this.eAm);
    }

    public int getSampleRate() {
        return nativeGetSampleRate(this.eAm);
    }

    public int getChannels() {
        return nativeGetChannels(this.eAm);
    }

    public int getBitDepth() {
        return nativeGetBitDepth(this.eAm);
    }

    public int getBufferId() {
        return nativeGetBufferId(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }
}
