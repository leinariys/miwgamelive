package gametoolkit.client.scene.sound;

import gametoolkit.client.scene.SceneObjectImpl;
import logic.res.scene.C3087nb;
import logic.res.scene.C4027yI;

/* compiled from: a */
public abstract class SoundObjectImpl extends SceneObjectImpl implements C4027yI {
    public SoundObjectImpl(long j) {
        super(j);
    }

    public SoundObjectImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42746aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native boolean nativeFadeOut(long j, float f);

    private native float nativeGetAudibleRadius(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetGain(long j);

    private native boolean nativeIsAlive(long j);

    private native boolean nativeIsNeedToStep(long j);

    private native boolean nativeIsPlaying(long j);

    private native boolean nativeIsValid(long j);

    private native boolean nativePlay(long j);

    private native void nativeSetAudibleRadius(long j, float f);

    private native void nativeSetControllerValue(long j, float f);

    private native void nativeSetGain(long j, float f);

    private native void nativeSetLoopCount(long j, int i);

    private native void nativeSetPitch(long j, float f);

    private native boolean nativeStop(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public boolean isNeedToStep() {
        return nativeIsNeedToStep(this.eAm);
    }

    public void setLoopCount(int i) {
        nativeSetLoopCount(this.eAm, i);
    }

    public float getGain() {
        return nativeGetGain(this.eAm);
    }

    public void setGain(float f) {
        nativeSetGain(this.eAm, f);
    }

    public void setPitch(float f) {
        nativeSetPitch(this.eAm, f);
    }

    public boolean play() {
        return nativePlay(this.eAm);
    }

    public boolean stop() {
        return nativeStop(this.eAm);
    }

    public boolean isPlaying() {
        return nativeIsPlaying(this.eAm);
    }

    public boolean fadeOut(float f) {
        return nativeFadeOut(this.eAm, f);
    }

    public boolean isValid() {
        return nativeIsValid(this.eAm);
    }

    public void setControllerValue(float f) {
        nativeSetControllerValue(this.eAm, f);
    }

    public boolean isAlive() {
        return nativeIsAlive(this.eAm);
    }

    public float getAudibleRadius() {
        return nativeGetAudibleRadius(this.eAm);
    }

    public void setAudibleRadius(float f) {
        nativeSetAudibleRadius(this.eAm, f);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }
}
