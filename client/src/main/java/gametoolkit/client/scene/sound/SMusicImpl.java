package gametoolkit.client.scene.sound;

import logic.res.scene.C2378ec;
import logic.res.scene.C3087nb;
import logic.res.scene.C4039yU;

/* compiled from: a */
public class SMusicImpl extends SoundObjectImpl implements C4039yU {
    public SMusicImpl(long j) {
        super(j);
    }

    public SMusicImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42725aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native boolean nativeFadeOut(long j, float f);

    private native C2378ec nativeGetBuffer(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetGain(long j);

    private native boolean nativeIsAlive(long j);

    private native boolean nativeIsPlaying(long j);

    private native boolean nativeIsValid(long j);

    private native boolean nativePause(long j);

    private native boolean nativePlay(long j);

    private native void nativeSetBuffer(long j, long j2);

    private native void nativeSetGain(long j, float f);

    private native void nativeSetLoopCount(long j, int i);

    private native void nativeSetPitch(long j, float f);

    private native boolean nativeStop(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void setLoopCount(int i) {
        nativeSetLoopCount(this.eAm, i);
    }

    public float getGain() {
        return nativeGetGain(this.eAm);
    }

    public void setGain(float f) {
        nativeSetGain(this.eAm, f);
    }

    public void setPitch(float f) {
        nativeSetPitch(this.eAm, f);
    }

    public boolean play() {
        return nativePlay(this.eAm);
    }

    public boolean stop() {
        return nativeStop(this.eAm);
    }

    public boolean pause() {
        return nativePause(this.eAm);
    }

    public boolean isPlaying() {
        return nativeIsPlaying(this.eAm);
    }

    public boolean isValid() {
        return nativeIsValid(this.eAm);
    }

    public boolean fadeOut(float f) {
        return nativeFadeOut(this.eAm, f);
    }

    /* renamed from: a */
    public void mo23133a(C2378ec ecVar) {
        nativeSetBuffer(this.eAm, ecVar != null ? ecVar.mo762dE() : 0);
    }

    public C2378ec aqG() {
        return nativeGetBuffer(this.eAm);
    }

    public boolean isAlive() {
        return nativeIsAlive(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }
}
