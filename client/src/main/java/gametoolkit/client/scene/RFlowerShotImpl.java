package gametoolkit.client.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import logic.res.scene.C1891ah;
import logic.res.scene.C3087nb;
import logic.res.scene.C5840abU;

/* compiled from: a */
public class RFlowerShotImpl extends RenderObjectImpl implements C5840abU {
    public RFlowerShotImpl(long j) {
        super(j);
    }

    public RFlowerShotImpl() {
        super(nativeConstructor_());
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RFlowerShotImpl(C5840abU abu) {
        super(nativeConstructor_RFlowerShot(abu != null ? abu.mo762dE() : 0));
    }

    private static native long nativeConstructor_();

    private static native long nativeConstructor_RFlowerShot(long j);

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42029aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native float[] nativeGetAABB(long j);

    private native long nativeGetClassSize(long j);

    private native C1891ah nativeGetShotMesh(long j);

    private native float[] nativeGetShotSize(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetShotMesh(long j, long j2);

    private native void nativeSetShotSize(long j, float f, float f2, float f3);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: dz */
    public BoundingBox mo765dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    public Vec3f getShotSize() {
        return new Vec3f(nativeGetShotSize(this.eAm));
    }

    public void setShotSize(Vec3f vec3f) {
        nativeSetShotSize(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: b */
    public void mo12467b(C1891ah ahVar) {
        nativeSetShotMesh(this.eAm, ahVar != null ? ahVar.mo762dE() : 0);
    }

    /* renamed from: yk */
    public C1891ah mo12470yk() {
        return nativeGetShotMesh(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }
}
