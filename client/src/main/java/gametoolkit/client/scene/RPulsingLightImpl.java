package gametoolkit.client.scene;

import com.hoplon.geometry.Vec3f;
import logic.res.scene.C3087nb;
import logic.res.scene.aCY;

/* compiled from: a */
public class RPulsingLightImpl extends RenderObjectImpl implements aCY {
    public RPulsingLightImpl(long j) {
        super(j);
    }

    public RPulsingLightImpl() {
        super(nativeConstructor_());
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RPulsingLightImpl(aCY acy) {
        super(nativeConstructor_RPulsingLight(acy != null ? acy.mo762dE() : 0));
    }

    private static native long nativeConstructor_();

    private static native long nativeConstructor_RPulsingLight(long j);

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42184aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native float[] nativeGetFinalColor(long j);

    private native float nativeGetFinalSize(long j);

    private native float[] nativeGetInitialColor(long j);

    private native float nativeGetInitialSize(long j);

    private native float nativeGetLifeTime(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetFinalColor(long j, float f, float f2, float f3);

    private native void nativeSetFinalSize(long j, float f);

    private native void nativeSetInitialColor(long j, float f, float f2, float f3);

    private native void nativeSetInitialSize(long j, float f);

    private native void nativeSetLifeTime(long j, float f);

    private native void nativeTrigger_(long j);

    private native void nativeTrigger_FFVec3fVec3fF(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: a */
    public void mo8195a(float f, float f2, Vec3f vec3f, Vec3f vec3f2, float f3) {
        nativeTrigger_FFVec3fVec3fF(this.eAm, f, f2, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f3);
    }

    public void trigger() {
        nativeTrigger_(this.eAm);
    }

    public void setInitialSize(float f) {
        nativeSetInitialSize(this.eAm, f);
    }

    public void setFinalSize(float f) {
        nativeSetFinalSize(this.eAm, f);
    }

    /* renamed from: bg */
    public void mo8196bg(Vec3f vec3f) {
        nativeSetInitialColor(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: bh */
    public void mo8197bh(Vec3f vec3f) {
        nativeSetFinalColor(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    public float cJh() {
        return nativeGetInitialSize(this.eAm);
    }

    public float cJi() {
        return nativeGetFinalSize(this.eAm);
    }

    public float getLifeTime() {
        return nativeGetLifeTime(this.eAm);
    }

    public void setLifeTime(float f) {
        nativeSetLifeTime(this.eAm, f);
    }

    public Vec3f cJj() {
        return new Vec3f(nativeGetInitialColor(this.eAm));
    }

    public Vec3f cJk() {
        return new Vec3f(nativeGetFinalColor(this.eAm));
    }
}
