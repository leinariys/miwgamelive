package gametoolkit.client.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Vector2fWrap;
import logic.res.scene.C3038nJ;
import logic.res.scene.C3087nb;
import logic.res.scene.C5415aIl;
import logic.res.scene.aBZ;

/* compiled from: a */
public class RBillboardImpl extends RenderObjectImpl implements C5415aIl {
    public RBillboardImpl(long j) {
        super(j);
    }

    public RBillboardImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m41981aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native void nativeComputeWorldSpaceAABB(long j);

    private native float[] nativeGetAABB(long j);

    private native int nativeGetAlignment(long j);

    private native float[] nativeGetAlignmentDirection(long j);

    private native long nativeGetClassSize(long j);

    private native boolean nativeGetDisableDepthWrite(long j);

    private native boolean nativeGetIsTwoSideBillboard(long j);

    private native float[] nativeGetSize(long j);

    private native float[] nativeGetTransformedAABB(long j);

    private native void nativeGetTransformedMatrix(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16, float f17, float f18, float f19, float f20, float f21, float f22, float f23, float f24, float f25, float f26, float f27, float f28, float f29, float f30, float f31, float f32, float f33, float f34, float f35, float f36, float f37, float f38, float f39, float f40, float f41, float f42, float f43, float f44, float f45, float f46, float f47, float f48);

    private native boolean nativeRayIntersects_Vec3fVec3fF(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7);

    private native boolean nativeRayIntersects_Vec3fVec3fFZ(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, boolean z);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetAlignment(long j, int i);

    private native void nativeSetAlignmentDirection(long j, float f, float f2, float f3);

    private native void nativeSetBillboard(long j, long j2);

    private native void nativeSetDisableDepthWrite(long j, boolean z);

    private native void nativeSetIsTwoSideBillboard(long j, boolean z);

    private native void nativeSetMaterial_Material(long j, long j2);

    private native void nativeSetMaterial_MaterialZ(long j, long j2, boolean z);

    private native void nativeSetSize_FF(long j, float f, float f2);

    private native void nativeSetSize_Vec2f(long j, float f, float f2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public void setSize(float f, float f2) {
        nativeSetSize_FF(this.eAm, f, f2);
    }

    public Vector2fWrap getSize() {
        return new Vector2fWrap(nativeGetSize(this.eAm));
    }

    public void setSize(Vector2fWrap aka) {
        nativeSetSize_Vec2f(this.eAm, aka.x, aka.y);
    }

    public int getAlignment() {
        return nativeGetAlignment(this.eAm);
    }

    public void setAlignment(int i) {
        nativeSetAlignment(this.eAm, i);
    }

    /* renamed from: a */
    public void mo9375a(aBZ abz) {
        nativeSetBillboard(this.eAm, abz != null ? abz.mo762dE() : 0);
    }

    /* renamed from: g */
    public void mo9379g(Matrix4fWrap ajk, Matrix4fWrap ajk2, Matrix4fWrap ajk3) {
        nativeGetTransformedMatrix(this.eAm, ajk.m00, ajk.m10, ajk.m20, ajk.m30, ajk.m01, ajk.m11, ajk.m21, ajk.m31, ajk.m02, ajk.m12, ajk.m22, ajk.m32, ajk.m03, ajk.m13, ajk.m23, ajk.m33, ajk2.m00, ajk2.m10, ajk2.m20, ajk2.m30, ajk2.m01, ajk2.m11, ajk2.m21, ajk2.m31, ajk2.m02, ajk2.m12, ajk2.m22, ajk2.m32, ajk2.m03, ajk2.m13, ajk2.m23, ajk2.m33, ajk3.m00, ajk3.m10, ajk3.m20, ajk3.m30, ajk3.m01, ajk3.m11, ajk3.m21, ajk3.m31, ajk3.m02, ajk3.m12, ajk3.m22, ajk3.m32, ajk3.m03, ajk3.m13, ajk3.m23, ajk3.m33);
    }

    public Vec3f getAlignmentDirection() {
        return new Vec3f(nativeGetAlignmentDirection(this.eAm));
    }

    public void setAlignmentDirection(Vec3f vec3f) {
        nativeSetAlignmentDirection(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: b */
    public void mo9377b(C3038nJ nJVar) {
        nativeSetMaterial_Material(this.eAm, nJVar != null ? nJVar.mo762dE() : 0);
    }

    /* renamed from: a */
    public void mo9376a(C3038nJ nJVar, boolean z) {
        nativeSetMaterial_MaterialZ(this.eAm, nJVar != null ? nJVar.mo762dE() : 0, z);
    }

    public boolean getDisableDepthWrite() {
        return nativeGetDisableDepthWrite(this.eAm);
    }

    public void setDisableDepthWrite(boolean z) {
        nativeSetDisableDepthWrite(this.eAm, z);
    }

    public boolean dei() {
        return nativeGetIsTwoSideBillboard(this.eAm);
    }

    /* renamed from: jB */
    public void mo9384jB(boolean z) {
        nativeSetIsTwoSideBillboard(this.eAm, z);
    }

    /* renamed from: dz */
    public BoundingBox mo765dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    /* renamed from: gO */
    public BoundingBox mo766gO() {
        return new BoundingBox(nativeGetTransformedAABB(this.eAm));
    }

    /* renamed from: gT */
    public void mo2298gT() {
        nativeComputeWorldSpaceAABB(this.eAm);
    }

    /* renamed from: a */
    public boolean mo7880a(Vec3f vec3f, Vec3f vec3f2, float f, boolean z) {
        return nativeRayIntersects_Vec3fVec3fFZ(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f, z);
    }

    /* renamed from: a */
    public boolean mo7879a(Vec3f vec3f, Vec3f vec3f2, float f) {
        return nativeRayIntersects_Vec3fVec3fF(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f);
    }
}
