package gametoolkit.client.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import logic.res.scene.C3087nb;
import logic.res.scene.C5415aIl;
import logic.res.scene.C6270aji;
import logic.res.scene.C6980axt;

/* compiled from: a */
public class RNuclearLightImpl extends RenderObjectImpl implements C6270aji {
    public RNuclearLightImpl(long j) {
        super(j);
    }

    public RNuclearLightImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42122aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeAddFlare(long j, long j2);

    private native C3087nb nativeClone(long j);

    private native void nativeComputeWorldSpaceAABB(long j);

    private native int nativeFlareCount(long j);

    private native float[] nativeGetAABB(long j);

    private native long nativeGetClassSize(long j);

    private native C6980axt nativeGetDefaultObject(long j);

    private native C5415aIl nativeGetFlare(long j, int i);

    private native C5415aIl nativeGetNuclearBillboard(long j);

    private native C5415aIl nativeGetObjectBillboard(long j);

    private native float nativeGetOcclusionRatio(long j);

    private native boolean nativeIsRayTraceable(long j);

    private native boolean nativeRayIntersects(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetDefaultObject(long j, long j2);

    private native void nativeSetFlare(long j, int i, long j2);

    private native void nativeSetNuclearBillboard(long j, long j2);

    private native void nativeSetObjectBillboard(long j, long j2);

    private native void nativeSetOcclusionRatio(long j, float f);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: gT */
    public void mo2298gT() {
        nativeComputeWorldSpaceAABB(this.eAm);
    }

    /* renamed from: d */
    public void mo14170d(C6980axt axt) {
        nativeSetDefaultObject(this.eAm, axt != null ? axt.mo762dE() : 0);
    }

    /* renamed from: XJ */
    public C6980axt mo14162XJ() {
        return nativeGetDefaultObject(this.eAm);
    }

    /* renamed from: a */
    public void mo14167a(C5415aIl ail) {
        nativeSetNuclearBillboard(this.eAm, ail != null ? ail.mo762dE() : 0);
    }

    /* renamed from: XK */
    public C5415aIl mo14163XK() {
        return nativeGetNuclearBillboard(this.eAm);
    }

    /* renamed from: b */
    public void mo14168b(C5415aIl ail) {
        nativeSetObjectBillboard(this.eAm, ail != null ? ail.mo762dE() : 0);
    }

    /* renamed from: XL */
    public C5415aIl mo14164XL() {
        return nativeGetObjectBillboard(this.eAm);
    }

    public float getOcclusionRatio() {
        return nativeGetOcclusionRatio(this.eAm);
    }

    public void setOcclusionRatio(float f) {
        nativeSetOcclusionRatio(this.eAm, f);
    }

    /* renamed from: dz */
    public BoundingBox mo765dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    /* renamed from: c */
    public void mo14169c(C5415aIl ail) {
        nativeAddFlare(this.eAm, ail != null ? ail.mo762dE() : 0);
    }

    /* renamed from: XM */
    public int mo14165XM() {
        return nativeFlareCount(this.eAm);
    }

    /* renamed from: eh */
    public C5415aIl mo14171eh(int i) {
        return nativeGetFlare(this.eAm, i);
    }

    /* renamed from: a */
    public void mo14166a(int i, C5415aIl ail) {
        nativeSetFlare(this.eAm, i, ail != null ? ail.mo762dE() : 0);
    }

    public boolean isRayTraceable() {
        return nativeIsRayTraceable(this.eAm);
    }

    /* renamed from: a */
    public boolean mo7879a(Vec3f vec3f, Vec3f vec3f2, float f) {
        return nativeRayIntersects(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f);
    }
}
