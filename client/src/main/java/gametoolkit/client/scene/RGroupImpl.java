package gametoolkit.client.scene;

import com.hoplon.geometry.BoundingBox;
import logic.res.scene.C3087nb;
import logic.res.scene.C5823abD;

/* compiled from: a */
public class RGroupImpl extends RenderObjectImpl implements C5823abD {
    public RGroupImpl(long j) {
        super(j);
    }

    public RGroupImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42035aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native float[] nativeComputeLocalSpaceAABB(long j);

    private native void nativeComputeWorldSpaceAABB(long j);

    private native float[] nativeGetAABB(long j);

    private native long nativeGetClassSize(long j);

    private native void nativeReleaseReferences(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    /* renamed from: dz */
    public BoundingBox mo765dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    /* renamed from: gT */
    public void mo2298gT() {
        nativeComputeWorldSpaceAABB(this.eAm);
    }

    /* renamed from: gP */
    public BoundingBox mo8885gP() {
        return new BoundingBox(nativeComputeLocalSpaceAABB(this.eAm));
    }
}
