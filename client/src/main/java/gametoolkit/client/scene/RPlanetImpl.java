package gametoolkit.client.scene;

import game.geometry.Vector2fWrap;
import logic.res.scene.C1007Oq;
import logic.res.scene.C3087nb;

/* compiled from: a */
public class RPlanetImpl extends RenderObjectImpl implements C1007Oq {
    public RPlanetImpl(long j) {
        super(j);
    }

    public RPlanetImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42180aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native boolean nativeGetAlignToCamera(long j);

    private native long nativeGetClassSize(long j);

    private native float[] nativeGetSize(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeSetAlignToCamera(long j, boolean z);

    private native void nativeSetSize(long j, float f, float f2);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    public Vector2fWrap getSize() {
        return new Vector2fWrap(nativeGetSize(this.eAm));
    }

    public void setSize(Vector2fWrap aka) {
        nativeSetSize(this.eAm, aka.x, aka.y);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    /* renamed from: de */
    public void mo4529de(boolean z) {
        nativeSetAlignToCamera(this.eAm, z);
    }

    public boolean blu() {
        return nativeGetAlignToCamera(this.eAm);
    }
}
