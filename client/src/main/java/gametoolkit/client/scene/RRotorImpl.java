package gametoolkit.client.scene;

import logic.res.scene.C3087nb;
import logic.res.scene.aLW;

/* compiled from: a */
public class RRotorImpl extends RenderObjectImpl implements aLW {
    public RRotorImpl(long j) {
        super(j);
    }

    public RRotorImpl() {
        super(nativeConstructor_());
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RRotorImpl(aLW alw) {
        super(nativeConstructor_RRotor(alw != null ? alw.mo762dE() : 0));
    }

    private static native long nativeConstructor_();

    private static native long nativeConstructor_RRotor(long j);

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42195aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native long nativeGetClassSize(long j);

    private native boolean nativeIsNeedToStep(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeReset(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public boolean isNeedToStep() {
        return nativeIsNeedToStep(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    public void reset() {
        nativeReset(this.eAm);
    }
}
