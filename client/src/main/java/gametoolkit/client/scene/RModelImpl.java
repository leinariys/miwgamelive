package gametoolkit.client.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import logic.res.scene.C3087nb;
import logic.res.scene.aBN;
import logic.res.scene.aKQ;

/* compiled from: a */
public class RModelImpl extends SceneObjectImpl implements aKQ {
    public RModelImpl(long j) {
        super(j);
    }

    public RModelImpl() {
        super(nativeConstructor_());
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RModelImpl(aKQ akq) {
        super(nativeConstructor_RModel(akq != null ? akq.mo762dE() : 0));
    }

    private static native long nativeConstructor_();

    private static native long nativeConstructor_RModel(long j);

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42113aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeAddBoneRotation(long j, String str, float f, float f2, float f3, float f4);

    private native void nativeAddSubMesh(long j, long j2);

    private native void nativeAnimate(long j);

    private native void nativeClearBoneRotations(long j);

    private native void nativeClearSubMeshes(long j);

    private native C3087nb nativeClone(long j);

    private native float[] nativeGetAABB(long j);

    private native long nativeGetClassSize(long j);

    private native float nativeGetGameClock(long j);

    private native boolean nativeGetHasActiveAnimation(long j);

    private native int nativeGetNumBones(long j);

    private native float[] nativeGetPlacementMatrix(long j);

    private native aBN nativeGetSubMesh(long j, int i);

    private native boolean nativeIsNeedToStep(long j);

    private native boolean nativeRayIntersects_Vec3fVec3fF(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7);

    private native boolean nativeRayIntersects_Vec3fVec3fFZ(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, boolean z);

    private native void nativeSetHasActiveAnimation(long j, boolean z);

    private native void nativeSetMaxMutableVertexCount(long j, int i);

    private native void nativeSetNumBones(long j, int i);

    private native void nativeSetSubMesh(long j, int i, long j2);

    private native int nativeSubMeshCount(long j);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public boolean isNeedToStep() {
        return nativeIsNeedToStep(this.eAm);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public void bxV() {
        nativeAnimate(this.eAm);
    }

    /* renamed from: dz */
    public BoundingBox mo765dz() {
        return new BoundingBox(nativeGetAABB(this.eAm));
    }

    /* renamed from: b */
    public void mo9718b(aBN abn) {
        nativeAddSubMesh(this.eAm, abn != null ? abn.mo762dE() : 0);
    }

    public int bxW() {
        return nativeSubMeshCount(this.eAm);
    }

    /* renamed from: nE */
    public aBN mo9727nE(int i) {
        return nativeGetSubMesh(this.eAm, i);
    }

    /* renamed from: a */
    public void mo9716a(int i, aBN abn) {
        nativeSetSubMesh(this.eAm, i, abn != null ? abn.mo762dE() : 0);
    }

    public void clearSubMeshes() {
        nativeClearSubMeshes(this.eAm);
    }

    public int getNumBones() {
        return nativeGetNumBones(this.eAm);
    }

    public void setNumBones(int i) {
        nativeSetNumBones(this.eAm, i);
    }

    public void setMaxMutableVertexCount(int i) {
        nativeSetMaxMutableVertexCount(this.eAm, i);
    }

    public float[] bxX() {
        return nativeGetPlacementMatrix(this.eAm);
    }

    public boolean bxY() {
        return nativeGetHasActiveAnimation(this.eAm);
    }

    public void setHasActiveAnimation(boolean z) {
        nativeSetHasActiveAnimation(this.eAm, z);
    }

    public float getGameClock() {
        return nativeGetGameClock(this.eAm);
    }

    /* renamed from: a */
    public boolean mo7880a(Vec3f vec3f, Vec3f vec3f2, float f, boolean z) {
        return nativeRayIntersects_Vec3fVec3fFZ(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f, z);
    }

    /* renamed from: a */
    public boolean mo7879a(Vec3f vec3f, Vec3f vec3f2, float f) {
        return nativeRayIntersects_Vec3fVec3fF(this.eAm, vec3f.x, vec3f.y, vec3f.z, vec3f2.x, vec3f2.y, vec3f2.z, f);
    }

    public void addBoneRotation(String str, Quat4fWrap aoy) {
        nativeAddBoneRotation(this.eAm, str, aoy.w, aoy.x, aoy.y, aoy.z);
    }

    public void clearBoneRotations() {
        nativeClearBoneRotations(this.eAm);
    }
}
