package gametoolkit.client.scene.gui;

import com.hoplon.geometry.Color;
import game.geometry.Matrix4fWrap;
import game.geometry.Vector2fWrap;
import gametoolkit.client.scene.RenderObjectImpl;
import logic.res.scene.C3038nJ;
import logic.res.scene.C3510rm;
import logic.res.scene.aRZ;

/* compiled from: a */
public class RGuiGraphicsImpl extends RenderObjectImpl implements C3510rm {
    public RGuiGraphicsImpl(long j) {
        super(j);
    }

    public RGuiGraphicsImpl() {
        super(nativeConstructor());
    }

    private static native long nativeConstructor();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42384aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeAddSubGraphics_RGuiGraphics(long j, long j2);

    private native void nativeAddSubGraphics_RGuiGraphicsFFFF(long j, long j2, float f, float f2, float f3, float f4);

    private native void nativeDrawArc(long j, float f, float f2, float f3, float f4, float f5, float f6);

    private native void nativeDrawFocus(long j, float f, float f2, float f3, float f4);

    private native void nativeDrawImage_MaterialFF(long j, long j2, float f, float f2);

    private native void nativeDrawImage_MaterialFFFF(long j, long j2, float f, float f2, float f3, float f4);

    private native void nativeDrawImage_MaterialFFFFFFFF(long j, long j2, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8);

    private native void nativeDrawLine(long j, float f, float f2, float f3, float f4);

    private native void nativeDrawOval(long j, float f, float f2, float f3, float f4);

    private native void nativeDrawPoint(long j, float f, float f2);

    private native void nativeDrawRectangle_FFFF(long j, float f, float f2, float f3, float f4);

    private native void nativeDrawRoundRectangle(long j, float f, float f2, float f3, float f4, float f5, float f6);

    private native void nativeDrawString_SFF(long j, String str, float f, float f2);

    private native void nativeDrawString_SFFZ(long j, String str, float f, float f2, boolean z);

    private native void nativeDrawText_SFF(long j, String str, float f, float f2);

    private native void nativeDrawText_SFFF(long j, String str, float f, float f2, float f3);

    private native void nativeDrawText_SFFZ(long j, String str, float f, float f2, boolean z);

    private native void nativeFillArc(long j, float f, float f2, float f3, float f4, float f5, float f6);

    private native void nativeFillGradientRectangle(long j, float f, float f2, float f3, float f4, boolean z);

    private native void nativeFillOval(long j, float f, float f2, float f3, float f4);

    private native void nativeFillPolygon(long j, float[] fArr, int i);

    private native void nativeFillRectangle_FFFF(long j, float f, float f2, float f3, float f4);

    private native void nativeFillRoundRectangle(long j, float f, float f2, float f3, float f4, float f5, float f6);

    private native float nativeGetAlpha(long j);

    private native float[] nativeGetBackground(long j);

    private native long nativeGetClassSize(long j);

    private native aRZ nativeGetFont(long j);

    private native float[] nativeGetForeground(long j);

    private native short nativeGetLineDash(long j);

    private native float nativeGetLineWidth(long j);

    private native float[] nativeGetTransform(long j);

    private native boolean nativeIsClipped(long j);

    private native void nativeReleaseReferences(long j);

    private native void nativeReset(long j);

    private native void nativeResetClipping(long j);

    private native void nativeSetAlpha(long j, float f);

    private native void nativeSetBackground_Color(long j, float f, float f2, float f3, float f4);

    private native void nativeSetBackground_FFFF(long j, float f, float f2, float f3, float f4);

    private native void nativeSetClipping_FFFF(long j, float f, float f2, float f3, float f4);

    private native void nativeSetFont(long j, long j2);

    private native void nativeSetForeground_Color(long j, float f, float f2, float f3, float f4);

    private native void nativeSetForeground_FFFF(long j, float f, float f2, float f3, float f4);

    private native void nativeSetLineDash(long j, short s);

    private native void nativeSetLineWidth(long j, float f);

    private native void nativeSetTransform(long j, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16);

    private native float[] nativeStringExtent(long j, String str);

    private native float[] nativeTextExtent_S(long j, String str);

    private native float[] nativeTextExtent_SI(long j, String str, int i);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void releaseReferences() {
        nativeReleaseReferences(this.eAm);
    }

    public void reset() {
        nativeReset(this.eAm);
    }

    /* renamed from: a */
    public void mo21809a(C3510rm rmVar) {
        nativeAddSubGraphics_RGuiGraphics(this.eAm, rmVar != null ? rmVar.mo762dE() : 0);
    }

    public boolean isClipped() {
        return nativeIsClipped(this.eAm);
    }

    /* renamed from: fw */
    public Color mo21831fw() {
        return new Color(nativeGetBackground(this.eAm));
    }

    /* renamed from: fx */
    public Color mo21832fx() {
        return new Color(nativeGetForeground(this.eAm));
    }

    public Matrix4fWrap getTransform() {
        return new Matrix4fWrap(nativeGetTransform(this.eAm));
    }

    public void setTransform(Matrix4fWrap ajk) {
        nativeSetTransform(this.eAm, ajk.m00, ajk.m10, ajk.m20, ajk.m30, ajk.m01, ajk.m11, ajk.m21, ajk.m31, ajk.m02, ajk.m12, ajk.m22, ajk.m32, ajk.m03, ajk.m13, ajk.m23, ajk.m33);
    }

    public float getAlpha() {
        return nativeGetAlpha(this.eAm);
    }

    public void setAlpha(float f) {
        nativeSetAlpha(this.eAm, f);
    }

    public float getLineWidth() {
        return nativeGetLineWidth(this.eAm);
    }

    public void setLineWidth(float f) {
        nativeSetLineWidth(this.eAm, f);
    }

    /* renamed from: y */
    public Vector2fWrap mo21844y(String str) {
        return new Vector2fWrap(nativeStringExtent(this.eAm, str));
    }

    /* renamed from: z */
    public Vector2fWrap mo21845z(String str) {
        return new Vector2fWrap(nativeTextExtent_S(this.eAm, str));
    }

    /* renamed from: b */
    public Vector2fWrap mo21816b(String str, int i) {
        return new Vector2fWrap(nativeTextExtent_SI(this.eAm, str, i));
    }

    /* renamed from: fy */
    public aRZ mo21833fy() {
        return nativeGetFont(this.eAm);
    }

    /* renamed from: fz */
    public short mo21834fz() {
        return nativeGetLineDash(this.eAm);
    }

    /* renamed from: a */
    public void mo21803a(float f, float f2, float f3, float f4, float f5, float f6) {
        nativeDrawArc(this.eAm, f, f2, f3, f4, f5, f6);
    }

    /* renamed from: b */
    public void mo21818b(float f, float f2, float f3, float f4) {
        nativeDrawFocus(this.eAm, f, f2, f3, f4);
    }

    /* renamed from: a */
    public void mo21808a(C3038nJ nJVar, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8) {
        nativeDrawImage_MaterialFFFFFFFF(this.eAm, nJVar != null ? nJVar.mo762dE() : 0, f, f2, f3, f4, f5, f6, f7, f8);
    }

    /* renamed from: a */
    public void mo21806a(C3038nJ nJVar, float f, float f2) {
        nativeDrawImage_MaterialFF(this.eAm, nJVar != null ? nJVar.mo762dE() : 0, f, f2);
    }

    /* renamed from: a */
    public void mo21807a(C3038nJ nJVar, float f, float f2, float f3, float f4) {
        nativeDrawImage_MaterialFFFF(this.eAm, nJVar != null ? nJVar.mo762dE() : 0, f, f2, f3, f4);
    }

    /* renamed from: c */
    public void mo21823c(float f, float f2, float f3, float f4) {
        nativeDrawLine(this.eAm, f, f2, f3, f4);
    }

    /* renamed from: d */
    public void mo21825d(float f, float f2, float f3, float f4) {
        nativeDrawOval(this.eAm, f, f2, f3, f4);
    }

    /* renamed from: b */
    public void mo21817b(float f, float f2) {
        nativeDrawPoint(this.eAm, f, f2);
    }

    /* renamed from: e */
    public void mo21828e(float f, float f2, float f3, float f4) {
        nativeDrawRectangle_FFFF(this.eAm, f, f2, f3, f4);
    }

    /* renamed from: b */
    public void mo21819b(float f, float f2, float f3, float f4, float f5, float f6) {
        nativeDrawRoundRectangle(this.eAm, f, f2, f3, f4, f5, f6);
    }

    public void drawString(String str, float f, float f2) {
        nativeDrawString_SFF(this.eAm, str, f, f2);
    }

    /* renamed from: a */
    public void mo21813a(String str, float f, float f2, boolean z) {
        nativeDrawString_SFFZ(this.eAm, str, f, f2, z);
    }

    /* renamed from: b */
    public void mo21821b(String str, float f, float f2) {
        nativeDrawText_SFF(this.eAm, str, f, f2);
    }

    /* renamed from: b */
    public void mo21822b(String str, float f, float f2, boolean z) {
        nativeDrawText_SFFZ(this.eAm, str, f, f2, z);
    }

    /* renamed from: a */
    public void mo21812a(String str, float f, float f2, float f3) {
        nativeDrawText_SFFF(this.eAm, str, f, f2, f3);
    }

    /* renamed from: c */
    public void mo21824c(float f, float f2, float f3, float f4, float f5, float f6) {
        nativeFillArc(this.eAm, f, f2, f3, f4, f5, f6);
    }

    /* renamed from: a */
    public void mo21804a(float f, float f2, float f3, float f4, boolean z) {
        nativeFillGradientRectangle(this.eAm, f, f2, f3, f4, z);
    }

    /* renamed from: f */
    public void mo21829f(float f, float f2, float f3, float f4) {
        nativeFillOval(this.eAm, f, f2, f3, f4);
    }

    /* renamed from: a */
    public void mo21815a(float[] fArr, int i) {
        nativeFillPolygon(this.eAm, fArr, i);
    }

    /* renamed from: g */
    public void mo21835g(float f, float f2, float f3, float f4) {
        nativeFillRectangle_FFFF(this.eAm, f, f2, f3, f4);
    }

    /* renamed from: d */
    public void mo21826d(float f, float f2, float f3, float f4, float f5, float f6) {
        nativeFillRoundRectangle(this.eAm, f, f2, f3, f4, f5, f6);
    }

    /* renamed from: fA */
    public void mo21830fA() {
        nativeResetClipping(this.eAm);
    }

    /* renamed from: a */
    public void mo21811a(Color color) {
        nativeSetBackground_Color(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    /* renamed from: h */
    public void mo21838h(float f, float f2, float f3, float f4) {
        nativeSetBackground_FFFF(this.eAm, f, f2, f3, f4);
    }

    /* renamed from: i */
    public void mo21839i(float f, float f2, float f3, float f4) {
        nativeSetClipping_FFFF(this.eAm, f, f2, f3, f4);
    }

    /* renamed from: a */
    public void mo21805a(aRZ arz) {
        nativeSetFont(this.eAm, arz != null ? arz.mo762dE() : 0);
    }

    /* renamed from: b */
    public void mo21820b(Color color) {
        nativeSetForeground_Color(this.eAm, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    /* renamed from: j */
    public void mo21841j(float f, float f2, float f3, float f4) {
        nativeSetForeground_FFFF(this.eAm, f, f2, f3, f4);
    }

    /* renamed from: a */
    public void mo21814a(short s) {
        nativeSetLineDash(this.eAm, s);
    }

    /* renamed from: a */
    public void mo21810a(C3510rm rmVar, float f, float f2, float f3, float f4) {
        nativeAddSubGraphics_RGuiGraphicsFFFF(this.eAm, rmVar != null ? rmVar.mo762dE() : 0, f, f2, f3, f4);
    }
}
