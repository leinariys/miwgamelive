package gametoolkit.client.scene;

import gametoolkit.client.scene.common.ManagedObjectImpl;
import logic.res.scene.C6791atj;
import logic.res.scene.aQK;

/* compiled from: a */
public class SceneManagerImpl extends ManagedObjectImpl implements aQK {
    public SceneManagerImpl(long j) {
        super(j);
    }

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42316aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native void nativeAddScene(long j, long j2);

    private native long nativeGetClassSize(long j);

    private native C6791atj nativeGetScene(long j, int i);

    private native long nativeGetSceneCount(long j);

    private native void nativeRemoveScene_I(long j, int i);

    private native void nativeRemoveScene_Scene(long j, long j2);

    private native void nativeRunFrame(long j, float f);

    private native void nativeStep(long j, float f);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    /* renamed from: ib */
    public void mo10913ib(float f) {
        nativeRunFrame(this.eAm, f);
    }

    public void step(float f) {
        nativeStep(this.eAm, f);
    }

    public long bxw() {
        return nativeGetSceneCount(this.eAm);
    }

    /* renamed from: a */
    public void mo10910a(C6791atj atj) {
        nativeAddScene(this.eAm, atj != null ? atj.mo762dE() : 0);
    }

    /* renamed from: nB */
    public void mo10914nB(int i) {
        nativeRemoveScene_I(this.eAm, i);
    }

    /* renamed from: b */
    public void mo10911b(C6791atj atj) {
        nativeRemoveScene_Scene(this.eAm, atj != null ? atj.mo762dE() : 0);
    }

    /* renamed from: nC */
    public C6791atj mo10915nC(int i) {
        return nativeGetScene(this.eAm, i);
    }
}
