package gametoolkit.client.scene;

import com.hoplon.geometry.Vec3f;
import logic.res.scene.C3087nb;
import logic.res.scene.aJS;

/* compiled from: a */
public class ROrbitalCameraImpl extends RSimulatedCameraImpl implements aJS {
    public ROrbitalCameraImpl(long j) {
        super(j);
    }

    public ROrbitalCameraImpl() {
        super(nativeConstructor_());
    }

    private static native long nativeConstructor_();

    private static native long nativeGetStaticClassInfoPointer();

    /* renamed from: aV */
    public static long m42138aV() {
        return nativeGetStaticClassInfoPointer();
    }

    private native C3087nb nativeClone(long j);

    private native void nativeDecrementDistance(long j);

    private native void nativeGenerateException(long j);

    private native long nativeGetClassSize(long j);

    private native float[] nativeGetDistance(long j);

    private native float nativeGetMaxZoomOut(long j);

    private native float nativeGetPitch(long j);

    private native float nativeGetRoll(long j);

    private native float nativeGetYaw(long j);

    private native float nativeGetZoomFactor(long j);

    private native void nativeIncrementDistance(long j);

    private native void nativeSetDistance(long j, float f, float f2, float f3);

    private native void nativeSetMaxZoomOut(long j, float f);

    private native void nativeSetOrbitalOrientation_FFF(long j, float f, float f2, float f3);

    private native void nativeSetOrbitalOrientation_Vec3f(long j, float f, float f2, float f3);

    private native void nativeSetPitch(long j, float f);

    private native void nativeSetRoll(long j, float f);

    private native void nativeSetYaw(long j, float f);

    private native void nativeSetZoomFactor(long j, float f);

    /* renamed from: aW */
    public long mo759aW() {
        return nativeGetClassSize(this.eAm);
    }

    public void bCI() {
        nativeGenerateException(this.eAm);
    }

    /* renamed from: an */
    public void mo9559an(Vec3f vec3f) {
        nativeSetDistance(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    public Vec3f bCJ() {
        return new Vec3f(nativeGetDistance(this.eAm));
    }

    /* renamed from: k */
    public void mo9572k(float f, float f2, float f3) {
        nativeSetOrbitalOrientation_FFF(this.eAm, f, f2, f3);
    }

    /* renamed from: ao */
    public void mo9560ao(Vec3f vec3f) {
        nativeSetOrbitalOrientation_Vec3f(this.eAm, vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: dy */
    public C3087nb mo764dy() {
        return nativeClone(this.eAm);
    }

    public float getYaw() {
        return nativeGetYaw(this.eAm);
    }

    public void setYaw(float f) {
        nativeSetYaw(this.eAm, f);
    }

    public float getPitch() {
        return nativeGetPitch(this.eAm);
    }

    public void setPitch(float f) {
        nativeSetPitch(this.eAm, f);
    }

    public float getRoll() {
        return nativeGetRoll(this.eAm);
    }

    public void setRoll(float f) {
        nativeSetRoll(this.eAm, f);
    }

    /* renamed from: iA */
    public void mo9570iA(float f) {
        nativeSetMaxZoomOut(this.eAm, f);
    }

    public float bCK() {
        return nativeGetMaxZoomOut(this.eAm);
    }

    /* renamed from: iB */
    public void mo9571iB(float f) {
        nativeSetZoomFactor(this.eAm, f);
    }

    public float bCL() {
        return nativeGetZoomFactor(this.eAm);
    }

    public void bCM() {
        nativeIncrementDistance(this.eAm);
    }

    public void bCN() {
        nativeDecrementDistance(this.eAm);
    }
}
