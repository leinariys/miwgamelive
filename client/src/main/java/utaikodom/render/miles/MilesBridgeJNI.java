package utaikodom.render.miles;

import java.nio.Buffer;

/* compiled from: a */
public class MilesBridgeJNI {
    MilesBridgeJNI() {
    }

    public static final native long ADPCMDATA_blockleft_get(long j);

    public static final native void ADPCMDATA_blockleft_set(long j, long j2);

    public static final native long ADPCMDATA_blocksize_get(long j);

    public static final native void ADPCMDATA_blocksize_set(long j, long j2);

    public static final native long ADPCMDATA_destend_get(long j);

    public static final native void ADPCMDATA_destend_set(long j, long j2);

    public static final native long ADPCMDATA_extrasamples_get(long j);

    public static final native void ADPCMDATA_extrasamples_set(long j, long j2);

    public static final native long ADPCMDATA_moresamples_get(long j);

    public static final native void ADPCMDATA_moresamples_set(long j, long j2);

    public static final native long ADPCMDATA_sample_get(long j);

    public static final native void ADPCMDATA_sample_set(long j, long j2);

    public static final native long ADPCMDATA_samplesL_get(long j);

    public static final native void ADPCMDATA_samplesL_set(long j, long j2);

    public static final native long ADPCMDATA_samplesR_get(long j);

    public static final native void ADPCMDATA_samplesR_set(long j, long j2);

    public static final native long ADPCMDATA_savesrc_get(long j);

    public static final native void ADPCMDATA_savesrc_set(long j, long j2);

    public static final native long ADPCMDATA_srcend_get(long j);

    public static final native void ADPCMDATA_srcend_set(long j, long j2);

    public static final native long ADPCMDATA_step_get(long j);

    public static final native void ADPCMDATA_step_set(long j, long j2);

    public static final native int AILDLSINFO_CurrentDLSMemory_get(long j);

    public static final native void AILDLSINFO_CurrentDLSMemory_set(long j, int i);

    public static final native String AILDLSINFO_Description_get(long j);

    public static final native void AILDLSINFO_Description_set(long j, String str);

    public static final native int AILDLSINFO_GMAvailable_get(long j);

    public static final native void AILDLSINFO_GMAvailable_set(long j, int i);

    public static final native int AILDLSINFO_GMBankSize_get(long j);

    public static final native void AILDLSINFO_GMBankSize_set(long j, int i);

    public static final native int AILDLSINFO_LargestSize_get(long j);

    public static final native void AILDLSINFO_LargestSize_set(long j, int i);

    public static final native int AILDLSINFO_MaxDLSMemory_get(long j);

    public static final native void AILDLSINFO_MaxDLSMemory_set(long j, int i);

    public static final native long AILMIXINFO_Info_get(long j);

    public static final native void AILMIXINFO_Info_set(long j, long j2);

    public static final native int AILMIXINFO_left_val_get(long j);

    public static final native void AILMIXINFO_left_val_set(long j, int i);

    public static final native long AILMIXINFO_mss_adpcm_get(long j);

    public static final native void AILMIXINFO_mss_adpcm_set(long j, long j2);

    public static final native int AILMIXINFO_right_val_get(long j);

    public static final native void AILMIXINFO_right_val_set(long j, int i);

    public static final native long AILMIXINFO_src_fract_get(long j);

    public static final native void AILMIXINFO_src_fract_set(long j, long j2);

    public static final native int AILSOUNDINFO_bits_get(long j);

    public static final native void AILSOUNDINFO_bits_set(long j, int i);

    public static final native long AILSOUNDINFO_block_size_get(long j);

    public static final native void AILSOUNDINFO_block_size_set(long j, long j2);

    public static final native long AILSOUNDINFO_channel_mask_get(long j);

    public static final native void AILSOUNDINFO_channel_mask_set(long j, long j2);

    public static final native int AILSOUNDINFO_channels_get(long j);

    public static final native void AILSOUNDINFO_channels_set(long j, int i);

    public static final native long AILSOUNDINFO_data_len_get(long j);

    public static final native void AILSOUNDINFO_data_len_set(long j, long j2);

    public static final native long AILSOUNDINFO_data_ptr_get(long j);

    public static final native void AILSOUNDINFO_data_ptr_set(long j, long j2);

    public static final native int AILSOUNDINFO_format_get(long j);

    public static final native void AILSOUNDINFO_format_set(long j, int i);

    public static final native long AILSOUNDINFO_initial_ptr_get(long j);

    public static final native void AILSOUNDINFO_initial_ptr_set(long j, long j2);

    public static final native long AILSOUNDINFO_rate_get(long j);

    public static final native void AILSOUNDINFO_rate_set(long j, long j2);

    public static final native long AILSOUNDINFO_samples_get(long j);

    public static final native void AILSOUNDINFO_samples_set(long j, long j2);

    public static final native long AILTIMERSTR_callback_get(long j);

    public static final native void AILTIMERSTR_callback_set(long j, long j2);

    public static final native int AILTIMERSTR_callingCT_get(long j);

    public static final native void AILTIMERSTR_callingCT_set(long j, int i);

    public static final native int AILTIMERSTR_callingDS_get(long j);

    public static final native void AILTIMERSTR_callingDS_set(long j, int i);

    public static final native int AILTIMERSTR_elapsed_get(long j);

    public static final native void AILTIMERSTR_elapsed_set(long j, int i);

    public static final native long AILTIMERSTR_status_get(long j);

    public static final native void AILTIMERSTR_status_set(long j, long j2);

    public static final native long AILTIMERSTR_user_get(long j);

    public static final native void AILTIMERSTR_user_set(long j, long j2);

    public static final native int AILTIMERSTR_value_get(long j);

    public static final native void AILTIMERSTR_value_set(long j, int i);

    public static final native float AIL_3D_distance_factor(long j);

    public static final native float AIL_3D_doppler_factor(long j);

    public static final native float AIL_3D_rolloff_factor(long j);

    public static final native void AIL_DLS_close(long j, long j2);

    public static final native void AIL_DLS_compact(long j);

    public static final native void AIL_DLS_get_info(long j, long j2, long j3);

    public static final native long AIL_DLS_load_file(long j, String str, long j2);

    public static final native long AIL_DLS_load_memory(long j, long j2, long j3);

    public static final native long AIL_DLS_open(long j, long j2, String str, long j3, long j4, int i, int i2);

    public static final native long AIL_DLS_sample_handle(long j);

    public static final native void AIL_DLS_unload(long j, long j2);

    public static final native long AIL_HWND();

    public static final native int AIL_INPUT_INFO_buffer_size_get(long j);

    public static final native void AIL_INPUT_INFO_buffer_size_set(long j, int i);

    public static final native long AIL_INPUT_INFO_callback_get(long j);

    public static final native void AIL_INPUT_INFO_callback_set(long j, long j2);

    public static final native long AIL_INPUT_INFO_device_ID_get(long j);

    public static final native void AIL_INPUT_INFO_device_ID_set(long j, long j2);

    public static final native long AIL_INPUT_INFO_hardware_format_get(long j);

    public static final native void AIL_INPUT_INFO_hardware_format_set(long j, long j2);

    public static final native long AIL_INPUT_INFO_hardware_rate_get(long j);

    public static final native void AIL_INPUT_INFO_hardware_rate_set(long j, long j2);

    public static final native long AIL_INPUT_INFO_user_data_get(long j);

    public static final native void AIL_INPUT_INFO_user_data_set(long j, long j2);

    public static final native int AIL_MIDI_handle_reacquire(long j);

    public static final native int AIL_MIDI_handle_release(long j);

    public static final native int AIL_MIDI_to_XMI(long j, long j2, long j3, long j4, int i);

    public static final native long AIL_MMX_available();

    public static final native int AIL_WAV_file_write(String str, long j, long j2, int i, int i2);

    public static final native int AIL_WAV_info(Buffer buffer, long j);

    public static final native int AIL_XMIDI_master_volume(long j);

    public static final native int AIL_active_sample_count(long j);

    public static final native int AIL_active_sequence_count(long j);

    public static final native long AIL_allocate_sample_handle(long j);

    public static final native long AIL_allocate_sequence_handle(long j);

    public static final native void AIL_auto_service_stream(long j, int i);

    public static final native int AIL_background();

    public static final native int AIL_background_CPU_percent();

    public static final native void AIL_branch_index(long j, long j2);

    public static final native int AIL_channel_notes(long j, int i);

    public static final native void AIL_close_XMIDI_driver(long j);

    public static final native void AIL_close_digital_driver(long j);

    public static final native void AIL_close_filter(int i);

    public static final native void AIL_close_input(long j);

    public static final native void AIL_close_library(long j);

    public static final native void AIL_close_stream(long j);

    public static final native int AIL_compress_ADPCM(long j, long j2, long j3);

    public static final native int AIL_compress_ASI(long j, String str, long j2, long j3, long j4);

    public static final native int AIL_compress_DLS(long j, String str, long j2, long j3, long j4);

    public static final native int AIL_controller_value(long j, int i, int i2);

    public static final native long AIL_create_wave_synthesizer(long j, long j2, long j3, int i);

    public static final native void AIL_debug_printf(String str);

    public static final native int AIL_decompress_ADPCM(long j, long j2, long j3);

    public static final native int AIL_decompress_ASI(long j, long j2, String str, long j3, long j4, long j5);

    public static final native void AIL_delay(int i);

    public static final native void AIL_destroy_wave_synthesizer(long j);

    public static final native int AIL_digital_CPU_percent(long j);

    public static final native void AIL_digital_configuration(long j, long j2, long j3, String str);

    public static final native long AIL_digital_driver_processor(long j, int i);

    public static final native int AIL_digital_handle_reacquire(long j);

    public static final native int AIL_digital_handle_release(long j);

    public static final native int AIL_digital_latency(long j);

    public static final native void AIL_digital_master_reverb(long j, long j2, long j3, long j4);

    public static final native void AIL_digital_master_reverb_levels(long j, long j2, long j3);

    public static final native float AIL_digital_master_volume_level(long j);

    public static final native long AIL_digital_output_filter(long j);

    public static final native void AIL_end_sample(long j);

    public static final native void AIL_end_sequence(long j);

    public static final native int AIL_enumerate_MP3_frames(long j);

    public static final native int AIL_enumerate_filter_properties(long j, long j2, long j3);

    public static final native int AIL_enumerate_filter_sample_properties(long j, long j2, long j3);

    public static final native int AIL_enumerate_filters(long j, long j2, long j3);

    public static final native int AIL_enumerate_output_filter_driver_properties(long j, long j2, long j3);

    public static final native int AIL_enumerate_output_filter_sample_properties(long j, long j2, long j3);

    public static final native int AIL_enumerate_sample_stage_properties(long j, int i, long j2, long j3);

    public static final native int AIL_extract_DLS(long j, long j2, long j3, long j4, long j5, long j6, long j7);

    public static final native int AIL_file_error();

    public static final native Buffer AIL_file_read(String str, Buffer buffer);

    public static final native int AIL_file_size(String str);

    public static final native int AIL_file_type(Buffer buffer, long j);

    public static final native int AIL_file_type_named(Buffer buffer, String str, long j);

    public static final native int AIL_file_write(String str, long j, long j2);

    public static final native int AIL_filter_DLS_with_XMI(long j, long j2, long j3, long j4, int i, long j5);

    public static final native int AIL_filter_property(long j, String str, Buffer buffer, Buffer buffer2, Buffer buffer3);

    public static final native int AIL_find_DLS(Buffer buffer, long j, long j2, long j3, long j4, long j5);

    public static final native int AIL_find_filter(String str, Buffer buffer);

    public static final native String AIL_ftoa(float f);

    public static final native void AIL_get_DirectSound_info(long j, long j2, long j3);

    public static final native long AIL_get_input_info(long j);

    public static final native int AIL_get_preference(long j);

    public static final native long AIL_get_timer_highest_delay();

    public static final native int AIL_init_sample(long j, int i, long j2);

    public static final native int AIL_init_sequence(long j, Buffer buffer, int i);

    public static final native void AIL_inspect_MP3(long j, long j2, int i);

    public static final native String AIL_last_error();

    public static final native String AIL_library_resource_filename(long j, String str, String str2, int i);

    public static final native int AIL_list_DLS(long j, long j2, long j3, int i, String str);

    public static final native int AIL_list_MIDI(long j, long j2, long j3, long j4, int i);

    public static final native void AIL_listener_3D_orientation(long j, long j2, long j3, long j4, long j5, long j6, long j7);

    public static final native void AIL_listener_3D_position(long j, long j2, long j3, long j4);

    public static final native void AIL_listener_3D_velocity(long j, long j2, long j3, long j4);

    public static final native long AIL_listener_relative_receiver_array(long j, long j2);

    public static final native long AIL_load_sample_attributes(long j, long j2);

    public static final native int AIL_load_sample_buffer(long j, int i, long j2, long j3);

    public static final native void AIL_lock();

    public static final native int AIL_lock_channel(long j);

    public static final native void AIL_lock_mutex();

    public static final native void AIL_map_sequence_channel(long j, int i, int i2);

    public static final native long AIL_mem_alloc_lock(long j);

    public static final native void AIL_mem_free_lock(long j);

    public static final native long AIL_mem_use_free(long j);

    public static final native long AIL_mem_use_malloc(long j);

    public static final native int AIL_merge_DLS_with_XMI(long j, long j2, long j3, long j4);

    public static final native void AIL_midiOutClose(long j);

    public static final native int AIL_midiOutOpen(long j, long j2, int i);

    public static final native int AIL_minimum_sample_buffer_size(long j, int i, int i2);

    public static final native long AIL_ms_count();

    public static final native long AIL_open_XMIDI_driver(long j);

    public static final native long AIL_open_digital_driver(long j, int i, int i2, long j2);

    public static final native int AIL_open_filter(long j, long j2);

    public static final native long AIL_open_input(long j);

    public static final native long AIL_open_library(String str, int i);

    public static final native long AIL_open_stream(long j, String str, int i);

    public static final native int AIL_output_filter_driver_property(long j, String str, Buffer buffer, Buffer buffer2, Buffer buffer3);

    public static final native void AIL_pause_stream(long j, int i);

    public static final native int AIL_platform_property(long j, int i, Buffer buffer, Buffer buffer2, Buffer buffer3);

    public static final native long AIL_primary_digital_driver(long j);

    public static final native int AIL_process_digital_audio(long j, int i, long j2, long j3, int i2, long j4);

    public static final native long AIL_quick_copy(long j);

    public static final native void AIL_quick_halt(long j);

    public static final native void AIL_quick_handles(long j, long j2, long j3);

    public static final native long AIL_quick_load(String str);

    public static final native long AIL_quick_load_and_play(String str, long j, int i);

    public static final native long AIL_quick_load_mem(long j, long j2);

    public static final native long AIL_quick_load_named_mem(long j, String str, long j2);

    public static final native int AIL_quick_ms_length(long j);

    public static final native int AIL_quick_ms_position(long j);

    public static final native int AIL_quick_play(long j, long j2);

    public static final native void AIL_quick_set_low_pass_cut_off(long j, float f);

    public static final native void AIL_quick_set_ms_position(long j, int i);

    public static final native void AIL_quick_set_reverb_levels(long j, float f, float f2);

    public static final native void AIL_quick_set_speed(long j, int i);

    public static final native void AIL_quick_set_volume(long j, float f, float f2);

    public static final native void AIL_quick_shutdown();

    public static final native int AIL_quick_startup(int i, int i2, long j, int i3, int i4);

    public static final native int AIL_quick_status(long j);

    public static final native int AIL_quick_type(long j);

    public static final native void AIL_quick_unload(long j);

    public static final native void AIL_redbook_close(long j);

    public static final native void AIL_redbook_eject(long j);

    public static final native long AIL_redbook_id(long j);

    public static final native long AIL_redbook_open(long j);

    public static final native long AIL_redbook_open_drive(int i);

    public static final native long AIL_redbook_pause(long j);

    public static final native long AIL_redbook_play(long j, long j2, long j3);

    public static final native long AIL_redbook_position(long j);

    public static final native long AIL_redbook_resume(long j);

    public static final native void AIL_redbook_retract(long j);

    public static final native float AIL_redbook_set_volume_level(long j, float f);

    public static final native long AIL_redbook_status(long j);

    public static final native long AIL_redbook_stop(long j);

    public static final native long AIL_redbook_track(long j);

    public static final native void AIL_redbook_track_info(long j, long j2, long j3, long j4);

    public static final native long AIL_redbook_tracks(long j);

    public static final native float AIL_redbook_volume_level(long j);

    public static final native long AIL_register_EOB_callback(long j, long j2);

    public static final native long AIL_register_EOS_callback(long j, long j2);

    public static final native void AIL_register_ICA_array(long j, long j2);

    public static final native long AIL_register_SOB_callback(long j, long j2);

    public static final native long AIL_register_beat_callback(long j, long j2);

    public static final native long AIL_register_event_callback(long j, long j2);

    public static final native long AIL_register_falloff_function_callback(long j, long j2);

    public static final native long AIL_register_prefix_callback(long j, long j2);

    public static final native long AIL_register_sequence_callback(long j, long j2);

    public static final native long AIL_register_stream_callback(long j, long j2);

    public static final native long AIL_register_timbre_callback(long j, long j2);

    public static final native int AIL_register_timer(long j);

    public static final native long AIL_register_trace_callback(long j, int i);

    public static final native long AIL_register_trigger_callback(long j, long j2);

    public static final native void AIL_release_all_timers();

    public static final native void AIL_release_channel(long j, int i);

    public static final native void AIL_release_sample_handle(long j);

    public static final native void AIL_release_sequence_handle(long j);

    public static final native void AIL_release_timer_handle(int i);

    public static final native void AIL_request_EOB_ASI_reset(long j, long j2, int i);

    public static final native void AIL_resume_sample(long j);

    public static final native void AIL_resume_sequence(long j);

    public static final native int AIL_room_type(long j);

    public static final native void AIL_sample_3D_cone(long j, long j2, long j3, long j4);

    public static final native void AIL_sample_3D_distances(long j, long j2, long j3, long j4);

    public static final native void AIL_sample_3D_orientation(long j, long j2, long j3, long j4, long j5, long j6, long j7);

    public static final native int AIL_sample_3D_position(long j, long j2, long j3, long j4);

    public static final native void AIL_sample_3D_velocity(long j, long j2, long j3, long j4);

    public static final native void AIL_sample_51_volume_levels(long j, long j2, long j3, long j4, long j5, long j6, long j7);

    public static final native void AIL_sample_51_volume_pan(long j, long j2, long j3, long j4, long j5, long j6);

    public static final native int AIL_sample_buffer_available(long j);

    public static final native int AIL_sample_buffer_count(long j);

    public static final native int AIL_sample_buffer_info(long j, int i, long j2, long j3, long j4, long j5);

    public static final native int AIL_sample_channel_count(long j, long j2);

    public static final native float AIL_sample_exclusion(long j);

    public static final native long AIL_sample_granularity(long j);

    public static final native int AIL_sample_loop_block(long j, long j2, long j3);

    public static final native int AIL_sample_loop_count(long j);

    public static final native float AIL_sample_low_pass_cut_off(long j);

    public static final native void AIL_sample_ms_position(long j, Buffer buffer, Buffer buffer2);

    public static final native float AIL_sample_obstruction(long j);

    public static final native float AIL_sample_occlusion(long j);

    public static final native int AIL_sample_playback_rate(long j);

    public static final native long AIL_sample_position(long j);

    public static final native long AIL_sample_processor(long j, int i);

    public static final native void AIL_sample_reverb_levels(long j, long j2, long j3);

    public static final native int AIL_sample_stage_property(long j, int i, String str, Buffer buffer, Buffer buffer2, Buffer buffer3);

    public static final native int AIL_sample_stage_property_RAMP_TIME(long j, int i, float f);

    public static final native int AIL_sample_stage_property_RAMP_TO(long j, int i, float f);

    public static final native long AIL_sample_status(long j);

    public static final native int AIL_sample_user_data(long j, long j2);

    public static final native void AIL_sample_volume_levels(long j, Buffer buffer, Buffer buffer2);

    public static final native void AIL_sample_volume_pan(long j, long j2, long j3);

    public static final native void AIL_save_sample_attributes(long j, long j2);

    public static final native void AIL_send_channel_voice_message(long j, long j2, int i, int i2, int i3);

    public static final native void AIL_send_sysex_message(long j, long j2);

    public static final native int AIL_sequence_loop_count(long j);

    public static final native void AIL_sequence_ms_position(long j, Buffer buffer, Buffer buffer2);

    public static final native void AIL_sequence_position(long j, long j2, long j3);

    public static final native long AIL_sequence_status(long j);

    public static final native int AIL_sequence_tempo(long j);

    public static final native int AIL_sequence_user_data(long j, long j2);

    public static final native int AIL_sequence_volume(long j);

    public static final native void AIL_serve();

    public static final native int AIL_service_stream(long j, int i);

    public static final native void AIL_set_3D_distance_factor(long j, float f);

    public static final native void AIL_set_3D_doppler_factor(long j, float f);

    public static final native void AIL_set_3D_rolloff_factor(long j, float f);

    public static final native int AIL_set_DirectSound_HWND(long j, long j2);

    public static final native void AIL_set_XMIDI_master_volume(long j, int i);

    public static final native long AIL_set_digital_driver_processor(long j, int i, long j2);

    public static final native void AIL_set_digital_master_reverb(long j, float f, float f2, float f3);

    public static final native void AIL_set_digital_master_reverb_levels(long j, float f, float f2);

    public static final native void AIL_set_digital_master_volume_level(long j, float f);

    public static final native void AIL_set_error(String str);

    public static final native void AIL_set_file_async_callbacks(long j, long j2, long j3, long j4, long j5);

    public static final native void AIL_set_file_callbacks(long j, long j2, long j3, long j4);

    public static final native int AIL_set_input_state(long j, int i);

    public static final native void AIL_set_listener_3D_orientation(long j, float f, float f2, float f3, float f4, float f5, float f6);

    public static final native void AIL_set_listener_3D_position(long j, float f, float f2, float f3);

    public static final native void AIL_set_listener_3D_velocity(long j, float f, float f2, float f3, float f4);

    public static final native void AIL_set_listener_3D_velocity_vector(long j, float f, float f2, float f3);

    public static final native void AIL_set_listener_relative_receiver_array(long j, long j2, int i);

    public static final native int AIL_set_named_sample_file(long j, String str, Buffer buffer, long j2, int i);

    public static final native int AIL_set_preference(long j, int i);

    public static final native String AIL_set_redist_directory(String str);

    public static final native void AIL_set_room_type(long j, int i);

    public static final native void AIL_set_sample_3D_cone(long j, float f, float f2, float f3);

    public static final native void AIL_set_sample_3D_distances(long j, float f, float f2, int i);

    public static final native void AIL_set_sample_3D_orientation(long j, float f, float f2, float f3, float f4, float f5, float f6);

    public static final native void AIL_set_sample_3D_position(long j, float f, float f2, float f3);

    public static final native void AIL_set_sample_3D_velocity(long j, float f, float f2, float f3, float f4);

    public static final native void AIL_set_sample_3D_velocity_vector(long j, float f, float f2, float f3);

    public static final native void AIL_set_sample_51_volume_levels(long j, float f, float f2, float f3, float f4, float f5, float f6);

    public static final native void AIL_set_sample_51_volume_pan(long j, float f, float f2, float f3, float f4, float f5);

    public static final native void AIL_set_sample_address(long j, Buffer buffer, long j2);

    public static final native void AIL_set_sample_adpcm_block_size(long j, long j2);

    public static final native int AIL_set_sample_buffer_count(long j, int i);

    public static final native void AIL_set_sample_exclusion(long j, float f);

    public static final native int AIL_set_sample_file(long j, Buffer buffer, int i);

    public static final native int AIL_set_sample_info(long j, long j2);

    public static final native void AIL_set_sample_loop_block(long j, int i, int i2);

    public static final native void AIL_set_sample_loop_count(long j, int i);

    public static final native void AIL_set_sample_low_pass_cut_off(long j, float f);

    public static final native void AIL_set_sample_ms_position(long j, int i);

    public static final native void AIL_set_sample_obstruction(long j, float f);

    public static final native void AIL_set_sample_occlusion(long j, float f);

    public static final native void AIL_set_sample_playback_rate(long j, int i);

    public static final native void AIL_set_sample_position(long j, long j2);

    public static final native long AIL_set_sample_processor(long j, int i, long j2);

    public static final native void AIL_set_sample_reverb_levels(long j, float f, float f2);

    public static final native void AIL_set_sample_user_data(long j, long j2, int i);

    public static final native void AIL_set_sample_volume_levels(long j, float f, float f2);

    public static final native void AIL_set_sample_volume_pan(long j, float f, float f2);

    public static final native void AIL_set_sequence_loop_count(long j, int i);

    public static final native void AIL_set_sequence_ms_position(long j, int i);

    public static final native void AIL_set_sequence_tempo(long j, int i, int i2);

    public static final native void AIL_set_sequence_user_data(long j, long j2, int i);

    public static final native void AIL_set_sequence_volume(long j, int i, int i2);

    public static final native void AIL_set_speaker_configuration(long j, long j2, int i, float f);

    public static final native void AIL_set_speaker_reverb_levels(long j, long j2, long j3, long j4, int i);

    public static final native void AIL_set_stream_loop_block(long j, int i, int i2);

    public static final native void AIL_set_stream_loop_count(long j, int i);

    public static final native void AIL_set_stream_ms_position(long j, int i);

    public static final native void AIL_set_stream_position(long j, int i);

    public static final native void AIL_set_stream_user_data(long j, long j2, int i);

    public static final native void AIL_set_timer_divisor(int i, long j);

    public static final native void AIL_set_timer_frequency(int i, long j);

    public static final native void AIL_set_timer_period(int i, long j);

    public static final native long AIL_set_timer_user(int i, long j);

    public static final native void AIL_shutdown();

    public static final native int AIL_size_processed_digital_audio(long j, long j2, int i, long j3);

    public static final native long AIL_speaker_configuration(long j, long j2, long j3, long j4, long j5);

    public static final native void AIL_start_all_timers();

    public static final native void AIL_start_sample(long j);

    public static final native void AIL_start_sequence(long j);

    public static final native void AIL_start_stream(long j);

    public static final native void AIL_start_timer(int i);

    public static final native int AIL_startup();

    public static final native void AIL_stop_all_timers();

    public static final native void AIL_stop_sample(long j);

    public static final native void AIL_stop_sequence(long j);

    public static final native void AIL_stop_timer(int i);

    public static final native void AIL_stream_info(long j, long j2, long j3, long j4, long j5);

    public static final native int AIL_stream_loop_count(long j);

    public static final native void AIL_stream_ms_position(long j, Buffer buffer, Buffer buffer2);

    public static final native int AIL_stream_position(long j);

    public static final native long AIL_stream_sample_handle(long j);

    public static final native int AIL_stream_status(long j);

    public static final native int AIL_stream_user_data(long j, long j2);

    public static final native int AIL_true_sequence_channel(long j, int i);

    public static final native void AIL_unlock();

    public static final native void AIL_unlock_mutex();

    public static final native void AIL_update_listener_3D_position(long j, float f);

    public static final native void AIL_update_sample_3D_position(long j, float f);

    public static final native long AIL_us_count();

    public static final native long ASISTAGE_ASI_stream_close_get(long j);

    public static final native void ASISTAGE_ASI_stream_close_set(long j, long j2);

    public static final native long ASISTAGE_ASI_stream_open_get(long j);

    public static final native void ASISTAGE_ASI_stream_open_set(long j, long j2);

    public static final native long ASISTAGE_ASI_stream_process_get(long j);

    public static final native void ASISTAGE_ASI_stream_process_set(long j, long j2);

    public static final native long ASISTAGE_ASI_stream_property_get(long j);

    public static final native void ASISTAGE_ASI_stream_property_set(long j, long j2);

    public static final native long ASISTAGE_ASI_stream_seek_get(long j);

    public static final native void ASISTAGE_ASI_stream_seek_set(long j, long j2);

    public static final native long ASISTAGE_DATA_LEN_get(long j);

    public static final native void ASISTAGE_DATA_LEN_set(long j, long j2);

    public static final native long ASISTAGE_DATA_START_OFFSET_get(long j);

    public static final native void ASISTAGE_DATA_START_OFFSET_set(long j, long j2);

    public static final native long ASISTAGE_INPUT_BITS_get(long j);

    public static final native void ASISTAGE_INPUT_BITS_set(long j, long j2);

    public static final native long ASISTAGE_INPUT_BIT_RATE_get(long j);

    public static final native void ASISTAGE_INPUT_BIT_RATE_set(long j, long j2);

    public static final native long ASISTAGE_INPUT_CHANNELS_get(long j);

    public static final native void ASISTAGE_INPUT_CHANNELS_set(long j, long j2);

    public static final native long ASISTAGE_INPUT_SAMPLE_RATE_get(long j);

    public static final native void ASISTAGE_INPUT_SAMPLE_RATE_set(long j, long j2);

    public static final native long ASISTAGE_MIN_INPUT_BLOCK_SIZE_get(long j);

    public static final native void ASISTAGE_MIN_INPUT_BLOCK_SIZE_set(long j, long j2);

    public static final native long ASISTAGE_OUTPUT_BITS_get(long j);

    public static final native void ASISTAGE_OUTPUT_BITS_set(long j, long j2);

    public static final native long ASISTAGE_OUTPUT_BIT_RATE_get(long j);

    public static final native void ASISTAGE_OUTPUT_BIT_RATE_set(long j, long j2);

    public static final native long ASISTAGE_OUTPUT_CHANNELS_get(long j);

    public static final native void ASISTAGE_OUTPUT_CHANNELS_set(long j, long j2);

    public static final native long ASISTAGE_OUTPUT_CHANNEL_MASK_get(long j);

    public static final native void ASISTAGE_OUTPUT_CHANNEL_MASK_set(long j, long j2);

    public static final native long ASISTAGE_OUTPUT_RESERVOIR_get(long j);

    public static final native void ASISTAGE_OUTPUT_RESERVOIR_set(long j, long j2);

    public static final native long ASISTAGE_OUTPUT_SAMPLE_RATE_get(long j);

    public static final native void ASISTAGE_OUTPUT_SAMPLE_RATE_set(long j, long j2);

    public static final native long ASISTAGE_PERCENT_DONE_get(long j);

    public static final native void ASISTAGE_PERCENT_DONE_set(long j, long j2);

    public static final native long ASISTAGE_POSITION_get(long j);

    public static final native void ASISTAGE_POSITION_set(long j, long j2);

    public static final native long ASISTAGE_RAW_BITS_get(long j);

    public static final native void ASISTAGE_RAW_BITS_set(long j, long j2);

    public static final native long ASISTAGE_RAW_CHANNELS_get(long j);

    public static final native void ASISTAGE_RAW_CHANNELS_set(long j, long j2);

    public static final native long ASISTAGE_RAW_RATE_get(long j);

    public static final native void ASISTAGE_RAW_RATE_set(long j, long j2);

    public static final native long ASISTAGE_REQUESTED_BITS_get(long j);

    public static final native void ASISTAGE_REQUESTED_BITS_set(long j, long j2);

    public static final native long ASISTAGE_REQUESTED_CHANS_get(long j);

    public static final native void ASISTAGE_REQUESTED_CHANS_set(long j, long j2);

    public static final native long ASISTAGE_REQUESTED_RATE_get(long j);

    public static final native void ASISTAGE_REQUESTED_RATE_set(long j, long j2);

    public static final native long ASISTAGE_STREAM_SEEK_POS_get(long j);

    public static final native void ASISTAGE_STREAM_SEEK_POS_set(long j, long j2);

    public static final native int ASISTAGE_stream_get(long j);

    public static final native void ASISTAGE_stream_set(long j, int i);

    public static final native float AUDIO_TYPE_cutoff_get(long j);

    public static final native void AUDIO_TYPE_cutoff_set(long j, float f);

    public static final native long AUDIO_TYPE_data_get(long j);

    public static final native void AUDIO_TYPE_data_set(long j, long j2);

    public static final native long AUDIO_TYPE_dlsid_get(long j);

    public static final native void AUDIO_TYPE_dlsid_set(long j, long j2);

    public static final native long AUDIO_TYPE_dlsmem_get(long j);

    public static final native void AUDIO_TYPE_dlsmem_set(long j, long j2);

    public static final native long AUDIO_TYPE_dlsmemunc_get(long j);

    public static final native void AUDIO_TYPE_dlsmemunc_set(long j, long j2);

    public static final native float AUDIO_TYPE_dry_get(long j);

    public static final native void AUDIO_TYPE_dry_set(long j, float f);

    public static final native float AUDIO_TYPE_extravol_get(long j);

    public static final native void AUDIO_TYPE_extravol_set(long j, float f);

    public static final native long AUDIO_TYPE_handle_get(long j);

    public static final native void AUDIO_TYPE_handle_set(long j, long j2);

    public static final native int AUDIO_TYPE_length_get(long j);

    public static final native void AUDIO_TYPE_length_set(long j, int i);

    public static final native int AUDIO_TYPE_milliseconds_get(long j);

    public static final native void AUDIO_TYPE_milliseconds_set(long j, int i);

    public static final native long AUDIO_TYPE_next_get(long j);

    public static final native void AUDIO_TYPE_next_set(long j, long j2);

    public static final native int AUDIO_TYPE_size_get(long j);

    public static final native void AUDIO_TYPE_size_set(long j, int i);

    public static final native int AUDIO_TYPE_speed_get(long j);

    public static final native void AUDIO_TYPE_speed_set(long j, int i);

    public static final native int AUDIO_TYPE_status_get(long j);

    public static final native void AUDIO_TYPE_status_set(long j, int i);

    public static final native int AUDIO_TYPE_type_get(long j);

    public static final native void AUDIO_TYPE_type_set(long j, int i);

    public static final native int AUDIO_TYPE_userdata_get(long j);

    public static final native void AUDIO_TYPE_userdata_set(long j, int i);

    public static final native float AUDIO_TYPE_volume_get(long j);

    public static final native void AUDIO_TYPE_volume_set(long j, float f);

    public static final native float AUDIO_TYPE_wet_get(long j);

    public static final native void AUDIO_TYPE_wet_set(long j, float f);

    public static final native long CTRL_LOG_RPN_L_get(long j);

    public static final native void CTRL_LOG_RPN_L_set(long j, long j2);

    public static final native long CTRL_LOG_RPN_M_get(long j);

    public static final native void CTRL_LOG_RPN_M_set(long j, long j2);

    public static final native long CTRL_LOG_bank_get(long j);

    public static final native void CTRL_LOG_bank_set(long j, long j2);

    public static final native long CTRL_LOG_bend_range_get(long j);

    public static final native void CTRL_LOG_bend_range_set(long j, long j2);

    public static final native long CTRL_LOG_c_lock_get(long j);

    public static final native void CTRL_LOG_c_lock_set(long j, long j2);

    public static final native long CTRL_LOG_c_mute_get(long j);

    public static final native void CTRL_LOG_c_mute_set(long j, long j2);

    public static final native long CTRL_LOG_c_prot_get(long j);

    public static final native void CTRL_LOG_c_prot_set(long j, long j2);

    public static final native long CTRL_LOG_c_v_prot_get(long j);

    public static final native void CTRL_LOG_c_v_prot_set(long j, long j2);

    public static final native long CTRL_LOG_callback_get(long j);

    public static final native void CTRL_LOG_callback_set(long j, long j2);

    public static final native long CTRL_LOG_chorus_get(long j);

    public static final native void CTRL_LOG_chorus_set(long j, long j2);

    public static final native long CTRL_LOG_exp_get(long j);

    public static final native void CTRL_LOG_exp_set(long j, long j2);

    public static final native long CTRL_LOG_gm_bank_l_get(long j);

    public static final native void CTRL_LOG_gm_bank_l_set(long j, long j2);

    public static final native long CTRL_LOG_gm_bank_m_get(long j);

    public static final native void CTRL_LOG_gm_bank_m_set(long j, long j2);

    public static final native long CTRL_LOG_indirect_get(long j);

    public static final native void CTRL_LOG_indirect_set(long j, long j2);

    public static final native long CTRL_LOG_mod_get(long j);

    public static final native void CTRL_LOG_mod_set(long j, long j2);

    public static final native long CTRL_LOG_pan_get(long j);

    public static final native void CTRL_LOG_pan_set(long j, long j2);

    public static final native long CTRL_LOG_pitch_h_get(long j);

    public static final native void CTRL_LOG_pitch_h_set(long j, long j2);

    public static final native long CTRL_LOG_pitch_l_get(long j);

    public static final native void CTRL_LOG_pitch_l_set(long j, long j2);

    public static final native long CTRL_LOG_program_get(long j);

    public static final native void CTRL_LOG_program_set(long j, long j2);

    public static final native long CTRL_LOG_reverb_get(long j);

    public static final native void CTRL_LOG_reverb_set(long j, long j2);

    public static final native long CTRL_LOG_sus_get(long j);

    public static final native void CTRL_LOG_sus_set(long j, long j2);

    public static final native long CTRL_LOG_vol_get(long j);

    public static final native void CTRL_LOG_vol_set(long j, long j2);

    public static final native long D3DSTATE_ambient_channels_get(long j);

    public static final native void D3DSTATE_ambient_channels_set(long j, long j2);

    public static final native long D3DSTATE_directional_channels_get(long j);

    public static final native void D3DSTATE_directional_channels_set(long j, long j2);

    public static final native float D3DSTATE_distance_factor_get(long j);

    public static final native void D3DSTATE_distance_factor_set(long j, float f);

    public static final native float D3DSTATE_doppler_factor_get(long j);

    public static final native void D3DSTATE_doppler_factor_set(long j, float f);

    public static final native float D3DSTATE_falloff_power_get(long j);

    public static final native void D3DSTATE_falloff_power_set(long j, float f);

    public static final native long D3DSTATE_listen_cross_get(long j);

    public static final native void D3DSTATE_listen_cross_set(long j, long j2);

    public static final native long D3DSTATE_listen_face_get(long j);

    public static final native void D3DSTATE_listen_face_set(long j, long j2);

    public static final native long D3DSTATE_listen_position_get(long j);

    public static final native void D3DSTATE_listen_position_set(long j, long j2);

    public static final native long D3DSTATE_listen_up_get(long j);

    public static final native void D3DSTATE_listen_up_set(long j, long j2);

    public static final native long D3DSTATE_listen_velocity_get(long j);

    public static final native void D3DSTATE_listen_velocity_set(long j, long j2);

    public static final native long D3DSTATE_listener_to_speaker_get(long j);

    public static final native void D3DSTATE_listener_to_speaker_set(long j, long j2);

    public static final native int D3DSTATE_mute_at_max_get(long j);

    public static final native void D3DSTATE_mute_at_max_set(long j, int i);

    public static final native int D3DSTATE_n_ambient_channels_get(long j);

    public static final native void D3DSTATE_n_ambient_channels_set(long j, int i);

    public static final native int D3DSTATE_n_directional_channels_get(long j);

    public static final native void D3DSTATE_n_directional_channels_set(long j, int i);

    public static final native int D3DSTATE_n_receiver_specs_get(long j);

    public static final native void D3DSTATE_n_receiver_specs_set(long j, int i);

    public static final native long D3DSTATE_receiver_specifications_get(long j);

    public static final native void D3DSTATE_receiver_specifications_set(long j, long j2);

    public static final native float D3DSTATE_rolloff_factor_get(long j);

    public static final native void D3DSTATE_rolloff_factor_set(long j, float f);

    public static final native long D3DSTATE_speaker_dry_reverb_response_get(long j);

    public static final native void D3DSTATE_speaker_dry_reverb_response_set(long j, long j2);

    public static final native long D3DSTATE_speaker_positions_get(long j);

    public static final native void D3DSTATE_speaker_positions_set(long j, long j2);

    public static final native long D3DSTATE_speaker_wet_reverb_response_get(long j);

    public static final native void D3DSTATE_speaker_wet_reverb_response_set(long j, long j2);

    public static final native long DIG_DRIVER_D3D_get(long j);

    public static final native void DIG_DRIVER_D3D_set(long j, long j2);

    public static final native int DIG_DRIVER_DMA_rate_get(long j);

    public static final native void DIG_DRIVER_DMA_rate_set(long j, int i);

    public static final native int DIG_DRIVER_DS_buffer_size_get(long j);

    public static final native void DIG_DRIVER_DS_buffer_size_set(long j, int i);

    public static final native int DIG_DRIVER_DS_frag_cnt_get(long j);

    public static final native void DIG_DRIVER_DS_frag_cnt_set(long j, int i);

    public static final native int DIG_DRIVER_DS_frag_size_get(long j);

    public static final native void DIG_DRIVER_DS_frag_size_set(long j, int i);

    public static final native int DIG_DRIVER_DS_initialized_get(long j);

    public static final native void DIG_DRIVER_DS_initialized_set(long j, int i);

    public static final native int DIG_DRIVER_DS_last_frag_get(long j);

    public static final native void DIG_DRIVER_DS_last_frag_set(long j, int i);

    public static final native int DIG_DRIVER_DS_last_timer_get(long j);

    public static final native void DIG_DRIVER_DS_last_timer_set(long j, int i);

    public static final native int DIG_DRIVER_DS_last_write_get(long j);

    public static final native void DIG_DRIVER_DS_last_write_set(long j, int i);

    public static final native long DIG_DRIVER_DS_out_buff_get(long j);

    public static final native void DIG_DRIVER_DS_out_buff_set(long j, long j2);

    public static final native long DIG_DRIVER_DS_sec_buff_get(long j);

    public static final native void DIG_DRIVER_DS_sec_buff_set(long j, long j2);

    public static final native int DIG_DRIVER_DS_skip_time_get(long j);

    public static final native void DIG_DRIVER_DS_skip_time_set(long j, int i);

    public static final native int DIG_DRIVER_DS_use_default_format_get(long j);

    public static final native void DIG_DRIVER_DS_use_default_format_set(long j, int i);

    public static final native int DIG_DRIVER_backgroundtimer_get(long j);

    public static final native void DIG_DRIVER_backgroundtimer_set(long j, int i);

    public static final native long DIG_DRIVER_build_get(long j);

    public static final native void DIG_DRIVER_build_set(long j, long j2);

    public static final native int DIG_DRIVER_build_size_get(long j);

    public static final native void DIG_DRIVER_build_size_set(long j, int i);

    public static final native int DIG_DRIVER_bytes_per_channel_get(long j);

    public static final native void DIG_DRIVER_bytes_per_channel_set(long j, int i);

    public static final native int DIG_DRIVER_callingCT_get(long j);

    public static final native void DIG_DRIVER_callingCT_set(long j, int i);

    public static final native int DIG_DRIVER_callingDS_get(long j);

    public static final native void DIG_DRIVER_callingDS_set(long j, int i);

    public static final native int DIG_DRIVER_channel_spec_get(long j);

    public static final native void DIG_DRIVER_channel_spec_set(long j, int i);

    public static final native long DIG_DRIVER_dsHwnd_get(long j);

    public static final native void DIG_DRIVER_dsHwnd_set(long j, long j2);

    public static final native long DIG_DRIVER_ds_priority_get(long j);

    public static final native void DIG_DRIVER_ds_priority_set(long j, long j2);

    public static final native int DIG_DRIVER_emulated_ds_get(long j);

    public static final native void DIG_DRIVER_emulated_ds_set(long j, int i);

    public static final native long DIG_DRIVER_first_get(long j);

    public static final native void DIG_DRIVER_first_set(long j, long j2);

    public static final native long DIG_DRIVER_foreground_timer_get(long j);

    public static final native void DIG_DRIVER_foreground_timer_set(long j, long j2);

    public static final native long DIG_DRIVER_guid_get(long j);

    public static final native void DIG_DRIVER_guid_set(long j, long j2);

    public static final native long DIG_DRIVER_hWaveOut_get(long j);

    public static final native void DIG_DRIVER_hWaveOut_set(long j, long j2);

    public static final native int DIG_DRIVER_hardware_buffer_size_get(long j);

    public static final native void DIG_DRIVER_hardware_buffer_size_set(long j, int i);

    public static final native int DIG_DRIVER_hw_format_get(long j);

    public static final native void DIG_DRIVER_hw_format_set(long j, int i);

    public static final native long DIG_DRIVER_hw_mode_flags_get(long j);

    public static final native void DIG_DRIVER_hw_mode_flags_set(long j, long j2);

    public static final native long DIG_DRIVER_last_ds_move_get(long j);

    public static final native void DIG_DRIVER_last_ds_move_set(long j, long j2);

    public static final native long DIG_DRIVER_last_ds_play_get(long j);

    public static final native void DIG_DRIVER_last_ds_play_set(long j, long j2);

    public static final native long DIG_DRIVER_last_ds_write_get(long j);

    public static final native void DIG_DRIVER_last_ds_write_set(long j, long j2);

    public static final native long DIG_DRIVER_last_ms_polled_get(long j);

    public static final native void DIG_DRIVER_last_ms_polled_set(long j, long j2);

    public static final native long DIG_DRIVER_last_percent_get(long j);

    public static final native void DIG_DRIVER_last_percent_set(long j, long j2);

    public static final native int DIG_DRIVER_logical_channels_per_sample_get(long j);

    public static final native void DIG_DRIVER_logical_channels_per_sample_set(long j, int i);

    public static final native long DIG_DRIVER_lpbufflist_get(long j);

    public static final native void DIG_DRIVER_lpbufflist_set(long j, long j2);

    public static final native long DIG_DRIVER_lppdsb_get(long j);

    public static final native void DIG_DRIVER_lppdsb_set(long j, long j2);

    public static final native float DIG_DRIVER_master_dry_get(long j);

    public static final native void DIG_DRIVER_master_dry_set(long j, float f);

    public static final native float DIG_DRIVER_master_volume_get(long j);

    public static final native void DIG_DRIVER_master_volume_set(long j, float f);

    public static final native float DIG_DRIVER_master_wet_get(long j);

    public static final native void DIG_DRIVER_master_wet_set(long j, float f);

    public static final native long DIG_DRIVER_matrix_filter_get(long j);

    public static final native void DIG_DRIVER_matrix_filter_set(long j, long j2);

    public static final native int DIG_DRIVER_max_buffs_get(long j);

    public static final native void DIG_DRIVER_max_buffs_set(long j, int i);

    public static final native long DIG_DRIVER_ms_count_get(long j);

    public static final native void DIG_DRIVER_ms_count_set(long j, long j2);

    public static final native int DIG_DRIVER_n_active_samples_get(long j);

    public static final native void DIG_DRIVER_n_active_samples_set(long j, int i);

    public static final native int DIG_DRIVER_n_buffers_get(long j);

    public static final native void DIG_DRIVER_n_buffers_set(long j, int i);

    public static final native int DIG_DRIVER_n_build_buffers_get(long j);

    public static final native void DIG_DRIVER_n_build_buffers_set(long j, int i);

    public static final native int DIG_DRIVER_n_samples_get(long j);

    public static final native void DIG_DRIVER_n_samples_set(long j, int i);

    public static final native long DIG_DRIVER_next_get(long j);

    public static final native void DIG_DRIVER_next_set(long j, long j2);

    public static final native int DIG_DRIVER_no_wom_done_get(long j);

    public static final native void DIG_DRIVER_no_wom_done_set(long j, int i);

    public static final native long DIG_DRIVER_pDS_get(long j);

    public static final native void DIG_DRIVER_pDS_set(long j, long j2);

    public static final native int DIG_DRIVER_physical_channels_per_sample_get(long j);

    public static final native void DIG_DRIVER_physical_channels_per_sample_set(long j, int i);

    public static final native long DIG_DRIVER_pipeline_get(long j);

    public static final native void DIG_DRIVER_pipeline_set(long j, long j2);

    public static final native int DIG_DRIVER_playing_get(long j);

    public static final native void DIG_DRIVER_playing_set(long j, int i);

    public static final native long DIG_DRIVER_position_error_get(long j);

    public static final native void DIG_DRIVER_position_error_set(long j, long j2);

    public static final native int DIG_DRIVER_quiet_get(long j);

    public static final native void DIG_DRIVER_quiet_set(long j, int i);

    public static final native int DIG_DRIVER_released_get(long j);

    public static final native void DIG_DRIVER_released_set(long j, int i);

    public static final native long DIG_DRIVER_request_reset_get(long j);

    public static final native void DIG_DRIVER_request_reset_set(long j, long j2);

    public static final native long DIG_DRIVER_reset_works_get(long j);

    public static final native void DIG_DRIVER_reset_works_set(long j, long j2);

    public static final native int DIG_DRIVER_return_head_get(long j);

    public static final native void DIG_DRIVER_return_head_set(long j, int i);

    public static final native long DIG_DRIVER_return_list_get(long j);

    public static final native void DIG_DRIVER_return_list_set(long j, long j2);

    public static final native int DIG_DRIVER_return_tail_get(long j);

    public static final native void DIG_DRIVER_return_tail_set(long j, int i);

    public static final native int DIG_DRIVER_reverb_buffer_size_get(long j);

    public static final native void DIG_DRIVER_reverb_buffer_size_set(long j, int i);

    public static final native long DIG_DRIVER_reverb_build_buffer_get(long j);

    public static final native void DIG_DRIVER_reverb_build_buffer_set(long j, long j2);

    public static final native float DIG_DRIVER_reverb_damping_get(long j);

    public static final native void DIG_DRIVER_reverb_damping_set(long j, float f);

    public static final native float DIG_DRIVER_reverb_decay_time_s_get(long j);

    public static final native void DIG_DRIVER_reverb_decay_time_s_set(long j, float f);

    public static final native long DIG_DRIVER_reverb_duration_ms_get(long j);

    public static final native void DIG_DRIVER_reverb_duration_ms_set(long j, long j2);

    public static final native int DIG_DRIVER_reverb_fragment_size_get(long j);

    public static final native void DIG_DRIVER_reverb_fragment_size_set(long j, int i);

    public static final native int DIG_DRIVER_reverb_head_get(long j);

    public static final native void DIG_DRIVER_reverb_head_set(long j, int i);

    public static final native long DIG_DRIVER_reverb_off_time_ms_get(long j);

    public static final native void DIG_DRIVER_reverb_off_time_ms_set(long j, long j2);

    public static final native int DIG_DRIVER_reverb_on_get(long j);

    public static final native void DIG_DRIVER_reverb_on_set(long j, int i);

    public static final native float DIG_DRIVER_reverb_predelay_s_get(long j);

    public static final native void DIG_DRIVER_reverb_predelay_s_set(long j, float f);

    public static final native int DIG_DRIVER_reverb_tail_get(long j);

    public static final native void DIG_DRIVER_reverb_tail_set(long j, int i);

    public static final native int DIG_DRIVER_reverb_total_size_get(long j);

    public static final native void DIG_DRIVER_reverb_total_size_set(long j, int i);

    public static final native long DIG_DRIVER_ri_get(long j);

    public static final native void DIG_DRIVER_ri_set(long j, long j2);

    public static final native int DIG_DRIVER_room_type_get(long j);

    public static final native void DIG_DRIVER_room_type_set(long j, int i);

    public static final native long DIG_DRIVER_samp_list_get(long j);

    public static final native void DIG_DRIVER_samp_list_set(long j, long j2);

    public static final native long DIG_DRIVER_sample_status_get(long j);

    public static final native void DIG_DRIVER_sample_status_set(long j, long j2);

    public static final native long DIG_DRIVER_samples_get(long j);

    public static final native int DIG_DRIVER_samples_per_buffer_get(long j);

    public static final native void DIG_DRIVER_samples_per_buffer_set(long j, int i);

    public static final native void DIG_DRIVER_samples_set(long j, long j2);

    public static final native long DIG_DRIVER_sec_format_get(long j);

    public static final native void DIG_DRIVER_sec_format_set(long j, long j2);

    public static final native long DIG_DRIVER_stream_callback_get(long j);

    public static final native void DIG_DRIVER_stream_callback_set(long j, long j2);

    public static final native long DIG_DRIVER_system_data_get(long j);

    public static final native void DIG_DRIVER_system_data_set(long j, long j2);

    public static final native String DIG_DRIVER_tag_get(long j);

    public static final native void DIG_DRIVER_tag_set(long j, String str);

    public static final native long DIG_DRIVER_us_count_get(long j);

    public static final native void DIG_DRIVER_us_count_set(long j, long j2);

    public static final native int DIG_DRIVER_use_MMX_get(long j);

    public static final native void DIG_DRIVER_use_MMX_set(long j, int i);

    public static final native long DIG_DRIVER_voice_filter_get(long j);

    public static final native void DIG_DRIVER_voice_filter_set(long j, long j2);

    public static final native String DIG_DRIVER_wfextra_get(long j);

    public static final native void DIG_DRIVER_wfextra_set(long j, String str);

    public static final native long DIG_DRIVER_wformat_get(long j);

    public static final native void DIG_DRIVER_wformat_set(long j, long j2);

    public static final native long DIG_DRIVER_wom_done_buffers_get(long j);

    public static final native void DIG_DRIVER_wom_done_buffers_set(long j, long j2);

    public static final native long DIG_INPUT_DRIVER_DMA_get(long j);

    public static final native void DIG_INPUT_DRIVER_DMA_set(long j, long j2);

    public static final native long DIG_INPUT_DRIVER_DMA_size_get(long j);

    public static final native void DIG_INPUT_DRIVER_DMA_size_set(long j, long j2);

    public static final native int DIG_INPUT_DRIVER_background_timer_get(long j);

    public static final native void DIG_INPUT_DRIVER_background_timer_set(long j, int i);

    public static final native long DIG_INPUT_DRIVER_callback_user_get(long j);

    public static final native void DIG_INPUT_DRIVER_callback_user_set(long j, long j2);

    public static final native int DIG_INPUT_DRIVER_device_active_get(long j);

    public static final native void DIG_INPUT_DRIVER_device_active_set(long j, int i);

    public static final native long DIG_INPUT_DRIVER_hWaveIn_get(long j);

    public static final native void DIG_INPUT_DRIVER_hWaveIn_set(long j, long j2);

    public static final native long DIG_INPUT_DRIVER_info_get(long j);

    public static final native void DIG_INPUT_DRIVER_info_set(long j, long j2);

    public static final native int DIG_INPUT_DRIVER_input_enabled_get(long j);

    public static final native void DIG_INPUT_DRIVER_input_enabled_set(long j, int i);

    public static final native short DIG_INPUT_DRIVER_silence_get(long j);

    public static final native void DIG_INPUT_DRIVER_silence_set(long j, short s);

    public static final native String DIG_INPUT_DRIVER_tag_get(long j);

    public static final native void DIG_INPUT_DRIVER_tag_set(long j, String str);

    public static final native long DIG_INPUT_DRIVER_wavehdr_get(long j);

    public static final native void DIG_INPUT_DRIVER_wavehdr_set(long j, long j2);

    public static final native int DLSDEVICE_DLSHandle_get(long j);

    public static final native void DLSDEVICE_DLSHandle_set(long j, int i);

    public static final native long DLSDEVICE_buffer_get(long j);

    public static final native void DLSDEVICE_buffer_set(long j, long j2);

    public static final native long DLSDEVICE_buffer_size_get(long j);

    public static final native void DLSDEVICE_buffer_size_set(long j, long j2);

    public static final native long DLSDEVICE_dig_get(long j);

    public static final native void DLSDEVICE_dig_set(long j, long j2);

    public static final native long DLSDEVICE_first_get(long j);

    public static final native void DLSDEVICE_first_set(long j, long j2);

    public static final native long DLSDEVICE_format_get(long j);

    public static final native void DLSDEVICE_format_set(long j, long j2);

    public static final native long DLSDEVICE_lib_get(long j);

    public static final native void DLSDEVICE_lib_set(long j, long j2);

    public static final native long DLSDEVICE_mdi_get(long j);

    public static final native void DLSDEVICE_mdi_set(long j, long j2);

    public static final native long DLSDEVICE_pClose_get(long j);

    public static final native void DLSDEVICE_pClose_set(long j, long j2);

    public static final native long DLSDEVICE_pCompact_get(long j);

    public static final native void DLSDEVICE_pCompact_set(long j, long j2);

    public static final native long DLSDEVICE_pGetInfo_get(long j);

    public static final native void DLSDEVICE_pGetInfo_set(long j, long j2);

    public static final native long DLSDEVICE_pGetPref_get(long j);

    public static final native void DLSDEVICE_pGetPref_set(long j, long j2);

    public static final native long DLSDEVICE_pLoadFile_get(long j);

    public static final native void DLSDEVICE_pLoadFile_set(long j, long j2);

    public static final native long DLSDEVICE_pLoadMem_get(long j);

    public static final native void DLSDEVICE_pLoadMem_set(long j, long j2);

    public static final native long DLSDEVICE_pMSSOpen_get(long j);

    public static final native void DLSDEVICE_pMSSOpen_set(long j, long j2);

    public static final native long DLSDEVICE_pOpen_get(long j);

    public static final native void DLSDEVICE_pOpen_set(long j, long j2);

    public static final native long DLSDEVICE_pSetAttr_get(long j);

    public static final native void DLSDEVICE_pSetAttr_set(long j, long j2);

    public static final native long DLSDEVICE_pSetPref_get(long j);

    public static final native void DLSDEVICE_pSetPref_set(long j, long j2);

    public static final native long DLSDEVICE_pUnloadAll_get(long j);

    public static final native void DLSDEVICE_pUnloadAll_set(long j, long j2);

    public static final native long DLSDEVICE_pUnloadFile_get(long j);

    public static final native void DLSDEVICE_pUnloadFile_set(long j, long j2);

    public static final native long DLSDEVICE_sample_get(long j);

    public static final native void DLSDEVICE_sample_set(long j, long j2);

    public static final native int DLSFILEID_id_get(long j);

    public static final native void DLSFILEID_id_set(long j, int i);

    public static final native long DLSFILEID_next_get(long j);

    public static final native void DLSFILEID_next_set(long j, long j2);

    public static final native long DPINFO_TYPE_MSS_mixer_adpcm_decode_get(long j);

    public static final native void DPINFO_TYPE_MSS_mixer_adpcm_decode_set(long j, long j2);

    public static final native long DPINFO_TYPE_MSS_mixer_copy_get(long j);

    public static final native void DPINFO_TYPE_MSS_mixer_copy_set(long j, long j2);

    public static final native long DPINFO_TYPE_MSS_mixer_flush_get(long j);

    public static final native void DPINFO_TYPE_MSS_mixer_flush_set(long j, long j2);

    public static final native long DPINFO_TYPE_MSS_mixer_mc_copy_get(long j);

    public static final native void DPINFO_TYPE_MSS_mixer_mc_copy_set(long j, long j2);

    public static final native long DPINFO_TYPE_get(long j);

    public static final native int DPINFO_active_get(long j);

    public static final native void DPINFO_active_set(long j, int i);

    public static final native long DPINFO_provider_get(long j);

    public static final native void DPINFO_provider_set(long j, long j2);

    public static final native int DP_FLUSH_get();

    public static final native int EAX_AUTOGAIN_Effect_get(long j);

    public static final native void EAX_AUTOGAIN_Effect_set(long j, int i);

    public static final native long EAX_AUTOGAIN_OnOff_get(long j);

    public static final native void EAX_AUTOGAIN_OnOff_set(long j, long j2);

    public static final native int EAX_AUTOGAIN_Volume_get(long j);

    public static final native void EAX_AUTOGAIN_Volume_set(long j, int i);

    public static final native float EAX_AUTOWAH_AttackTime_get(long j);

    public static final native void EAX_AUTOWAH_AttackTime_set(long j, float f);

    public static final native int EAX_AUTOWAH_Effect_get(long j);

    public static final native void EAX_AUTOWAH_Effect_set(long j, int i);

    public static final native int EAX_AUTOWAH_PeakLevel_get(long j);

    public static final native void EAX_AUTOWAH_PeakLevel_set(long j, int i);

    public static final native float EAX_AUTOWAH_ReleaseTime_get(long j);

    public static final native void EAX_AUTOWAH_ReleaseTime_set(long j, float f);

    public static final native int EAX_AUTOWAH_Resonance_get(long j);

    public static final native void EAX_AUTOWAH_Resonance_set(long j, int i);

    public static final native int EAX_AUTOWAH_Volume_get(long j);

    public static final native void EAX_AUTOWAH_Volume_set(long j, int i);

    public static final native float EAX_CHORUS_Delay_get(long j);

    public static final native void EAX_CHORUS_Delay_set(long j, float f);

    public static final native float EAX_CHORUS_Depth_get(long j);

    public static final native void EAX_CHORUS_Depth_set(long j, float f);

    public static final native int EAX_CHORUS_Effect_get(long j);

    public static final native void EAX_CHORUS_Effect_set(long j, int i);

    public static final native float EAX_CHORUS_Feedback_get(long j);

    public static final native void EAX_CHORUS_Feedback_set(long j, float f);

    public static final native int EAX_CHORUS_Phase_get(long j);

    public static final native void EAX_CHORUS_Phase_set(long j, int i);

    public static final native float EAX_CHORUS_Rate_get(long j);

    public static final native void EAX_CHORUS_Rate_set(long j, float f);

    public static final native int EAX_CHORUS_Volume_get(long j);

    public static final native void EAX_CHORUS_Volume_set(long j, int i);

    public static final native long EAX_CHORUS_Waveform_get(long j);

    public static final native void EAX_CHORUS_Waveform_set(long j, long j2);

    public static final native float EAX_DISTORTION_EQBandwidth_get(long j);

    public static final native void EAX_DISTORTION_EQBandwidth_set(long j, float f);

    public static final native float EAX_DISTORTION_EQCenter_get(long j);

    public static final native void EAX_DISTORTION_EQCenter_set(long j, float f);

    public static final native float EAX_DISTORTION_Edge_get(long j);

    public static final native void EAX_DISTORTION_Edge_set(long j, float f);

    public static final native int EAX_DISTORTION_Effect_get(long j);

    public static final native void EAX_DISTORTION_Effect_set(long j, int i);

    public static final native int EAX_DISTORTION_Gain_get(long j);

    public static final native void EAX_DISTORTION_Gain_set(long j, int i);

    public static final native float EAX_DISTORTION_LowPassCutOff_get(long j);

    public static final native void EAX_DISTORTION_LowPassCutOff_set(long j, float f);

    public static final native int EAX_DISTORTION_Volume_get(long j);

    public static final native void EAX_DISTORTION_Volume_set(long j, int i);

    public static final native float EAX_ECHO_Damping_get(long j);

    public static final native void EAX_ECHO_Damping_set(long j, float f);

    public static final native float EAX_ECHO_Delay_get(long j);

    public static final native void EAX_ECHO_Delay_set(long j, float f);

    public static final native int EAX_ECHO_Effect_get(long j);

    public static final native void EAX_ECHO_Effect_set(long j, int i);

    public static final native float EAX_ECHO_Feedback_get(long j);

    public static final native void EAX_ECHO_Feedback_set(long j, float f);

    public static final native float EAX_ECHO_LRDelay_get(long j);

    public static final native void EAX_ECHO_LRDelay_set(long j, float f);

    public static final native float EAX_ECHO_Spread_get(long j);

    public static final native void EAX_ECHO_Spread_set(long j, float f);

    public static final native int EAX_ECHO_Volume_get(long j);

    public static final native void EAX_ECHO_Volume_set(long j, int i);

    public static final native int EAX_ENVIRONMENT_ALLEY_get();

    public static final native int EAX_ENVIRONMENT_ARENA_get();

    public static final native int EAX_ENVIRONMENT_AUDITORIUM_get();

    public static final native int EAX_ENVIRONMENT_BATHROOM_get();

    public static final native int EAX_ENVIRONMENT_CARPETEDHALLWAY_get();

    public static final native int EAX_ENVIRONMENT_CAVE_get();

    public static final native int EAX_ENVIRONMENT_CITY_get();

    public static final native int EAX_ENVIRONMENT_CONCERTHALL_get();

    public static final native int EAX_ENVIRONMENT_COUNT_get();

    public static final native int EAX_ENVIRONMENT_DIZZY_get();

    public static final native int EAX_ENVIRONMENT_DRUGGED_get();

    public static final native int EAX_ENVIRONMENT_FOREST_get();

    public static final native int EAX_ENVIRONMENT_GENERIC_get();

    public static final native int EAX_ENVIRONMENT_HALLWAY_get();

    public static final native int EAX_ENVIRONMENT_HANGAR_get();

    public static final native int EAX_ENVIRONMENT_LIVINGROOM_get();

    public static final native int EAX_ENVIRONMENT_MOUNTAINS_get();

    public static final native int EAX_ENVIRONMENT_PADDEDCELL_get();

    public static final native int EAX_ENVIRONMENT_PARKINGLOT_get();

    public static final native int EAX_ENVIRONMENT_PLAIN_get();

    public static final native int EAX_ENVIRONMENT_PSYCHOTIC_get();

    public static final native int EAX_ENVIRONMENT_QUARRY_get();

    public static final native int EAX_ENVIRONMENT_ROOM_get();

    public static final native int EAX_ENVIRONMENT_SEWERPIPE_get();

    public static final native int EAX_ENVIRONMENT_STONECORRIDOR_get();

    public static final native int EAX_ENVIRONMENT_STONEROOM_get();

    public static final native int EAX_ENVIRONMENT_UNDERWATER_get();

    public static final native int EAX_EQUALIZER_Effect_get(long j);

    public static final native void EAX_EQUALIZER_Effect_set(long j, int i);

    public static final native float EAX_EQUALIZER_HighCutOff_get(long j);

    public static final native void EAX_EQUALIZER_HighCutOff_set(long j, float f);

    public static final native int EAX_EQUALIZER_HighGain_get(long j);

    public static final native void EAX_EQUALIZER_HighGain_set(long j, int i);

    public static final native float EAX_EQUALIZER_LowCutOff_get(long j);

    public static final native void EAX_EQUALIZER_LowCutOff_set(long j, float f);

    public static final native int EAX_EQUALIZER_LowGain_get(long j);

    public static final native void EAX_EQUALIZER_LowGain_set(long j, int i);

    public static final native float EAX_EQUALIZER_Mid1Center_get(long j);

    public static final native void EAX_EQUALIZER_Mid1Center_set(long j, float f);

    public static final native int EAX_EQUALIZER_Mid1Gain_get(long j);

    public static final native void EAX_EQUALIZER_Mid1Gain_set(long j, int i);

    public static final native float EAX_EQUALIZER_Mid1Width_get(long j);

    public static final native void EAX_EQUALIZER_Mid1Width_set(long j, float f);

    public static final native float EAX_EQUALIZER_Mid2Center_get(long j);

    public static final native void EAX_EQUALIZER_Mid2Center_set(long j, float f);

    public static final native float EAX_EQUALIZER_Mid2Gain_get(long j);

    public static final native void EAX_EQUALIZER_Mid2Gain_set(long j, float f);

    public static final native float EAX_EQUALIZER_Mid2Width_get(long j);

    public static final native void EAX_EQUALIZER_Mid2Width_set(long j, float f);

    public static final native int EAX_EQUALIZER_Volume_get(long j);

    public static final native void EAX_EQUALIZER_Volume_set(long j, int i);

    public static final native float EAX_FLANGER_Delay_get(long j);

    public static final native void EAX_FLANGER_Delay_set(long j, float f);

    public static final native float EAX_FLANGER_Depth_get(long j);

    public static final native void EAX_FLANGER_Depth_set(long j, float f);

    public static final native int EAX_FLANGER_Effect_get(long j);

    public static final native void EAX_FLANGER_Effect_set(long j, int i);

    public static final native float EAX_FLANGER_Feedback_get(long j);

    public static final native void EAX_FLANGER_Feedback_set(long j, float f);

    public static final native int EAX_FLANGER_Phase_get(long j);

    public static final native void EAX_FLANGER_Phase_set(long j, int i);

    public static final native float EAX_FLANGER_Rate_get(long j);

    public static final native void EAX_FLANGER_Rate_set(long j, float f);

    public static final native int EAX_FLANGER_Volume_get(long j);

    public static final native void EAX_FLANGER_Volume_set(long j, int i);

    public static final native long EAX_FLANGER_Waveform_get(long j);

    public static final native void EAX_FLANGER_Waveform_set(long j, long j2);

    public static final native int EAX_FSHIFTER_Effect_get(long j);

    public static final native void EAX_FSHIFTER_Effect_set(long j, int i);

    public static final native float EAX_FSHIFTER_Frequency_get(long j);

    public static final native void EAX_FSHIFTER_Frequency_set(long j, float f);

    public static final native long EAX_FSHIFTER_LeftDirection_get(long j);

    public static final native void EAX_FSHIFTER_LeftDirection_set(long j, long j2);

    public static final native long EAX_FSHIFTER_RightDirection_get(long j);

    public static final native void EAX_FSHIFTER_RightDirection_set(long j, long j2);

    public static final native int EAX_FSHIFTER_Volume_get(long j);

    public static final native void EAX_FSHIFTER_Volume_set(long j, int i);

    public static final native int EAX_PSHIFTER_CoarseTune_get(long j);

    public static final native void EAX_PSHIFTER_CoarseTune_set(long j, int i);

    public static final native int EAX_PSHIFTER_Effect_get(long j);

    public static final native void EAX_PSHIFTER_Effect_set(long j, int i);

    public static final native int EAX_PSHIFTER_FineTune_get(long j);

    public static final native void EAX_PSHIFTER_FineTune_set(long j, int i);

    public static final native int EAX_PSHIFTER_Volume_get(long j);

    public static final native void EAX_PSHIFTER_Volume_set(long j, int i);

    public static final native float EAX_REVERB_AirAbsorptionHF_get(long j);

    public static final native void EAX_REVERB_AirAbsorptionHF_set(long j, float f);

    public static final native float EAX_REVERB_DecayHFRatio_get(long j);

    public static final native void EAX_REVERB_DecayHFRatio_set(long j, float f);

    public static final native float EAX_REVERB_DecayLFRatio_get(long j);

    public static final native void EAX_REVERB_DecayLFRatio_set(long j, float f);

    public static final native float EAX_REVERB_DecayTime_get(long j);

    public static final native void EAX_REVERB_DecayTime_set(long j, float f);

    public static final native float EAX_REVERB_EchoDepth_get(long j);

    public static final native void EAX_REVERB_EchoDepth_set(long j, float f);

    public static final native float EAX_REVERB_EchoTime_get(long j);

    public static final native void EAX_REVERB_EchoTime_set(long j, float f);

    public static final native int EAX_REVERB_Effect_get(long j);

    public static final native void EAX_REVERB_Effect_set(long j, int i);

    public static final native float EAX_REVERB_EnvironmentDiffusion_get(long j);

    public static final native void EAX_REVERB_EnvironmentDiffusion_set(long j, float f);

    public static final native float EAX_REVERB_EnvironmentSize_get(long j);

    public static final native void EAX_REVERB_EnvironmentSize_set(long j, float f);

    public static final native long EAX_REVERB_Environment_get(long j);

    public static final native void EAX_REVERB_Environment_set(long j, long j2);

    public static final native long EAX_REVERB_Flags_get(long j);

    public static final native void EAX_REVERB_Flags_set(long j, long j2);

    public static final native float EAX_REVERB_HFReference_get(long j);

    public static final native void EAX_REVERB_HFReference_set(long j, float f);

    public static final native float EAX_REVERB_LFReference_get(long j);

    public static final native void EAX_REVERB_LFReference_set(long j, float f);

    public static final native float EAX_REVERB_ModulationDepth_get(long j);

    public static final native void EAX_REVERB_ModulationDepth_set(long j, float f);

    public static final native float EAX_REVERB_ModulationTime_get(long j);

    public static final native void EAX_REVERB_ModulationTime_set(long j, float f);

    public static final native float EAX_REVERB_ReflectionsDelay_get(long j);

    public static final native void EAX_REVERB_ReflectionsDelay_set(long j, float f);

    public static final native float EAX_REVERB_ReflectionsPanX_get(long j);

    public static final native void EAX_REVERB_ReflectionsPanX_set(long j, float f);

    public static final native float EAX_REVERB_ReflectionsPanY_get(long j);

    public static final native void EAX_REVERB_ReflectionsPanY_set(long j, float f);

    public static final native float EAX_REVERB_ReflectionsPanZ_get(long j);

    public static final native void EAX_REVERB_ReflectionsPanZ_set(long j, float f);

    public static final native int EAX_REVERB_Reflections_get(long j);

    public static final native void EAX_REVERB_Reflections_set(long j, int i);

    public static final native float EAX_REVERB_ReverbDelay_get(long j);

    public static final native void EAX_REVERB_ReverbDelay_set(long j, float f);

    public static final native float EAX_REVERB_ReverbPanX_get(long j);

    public static final native void EAX_REVERB_ReverbPanX_set(long j, float f);

    public static final native float EAX_REVERB_ReverbPanY_get(long j);

    public static final native void EAX_REVERB_ReverbPanY_set(long j, float f);

    public static final native float EAX_REVERB_ReverbPanZ_get(long j);

    public static final native void EAX_REVERB_ReverbPanZ_set(long j, float f);

    public static final native int EAX_REVERB_Reverb_get(long j);

    public static final native void EAX_REVERB_Reverb_set(long j, int i);

    public static final native int EAX_REVERB_RoomHF_get(long j);

    public static final native void EAX_REVERB_RoomHF_set(long j, int i);

    public static final native int EAX_REVERB_RoomLF_get(long j);

    public static final native void EAX_REVERB_RoomLF_set(long j, int i);

    public static final native float EAX_REVERB_RoomRolloffFactor_get(long j);

    public static final native void EAX_REVERB_RoomRolloffFactor_set(long j, float f);

    public static final native int EAX_REVERB_Room_get(long j);

    public static final native void EAX_REVERB_Room_set(long j, int i);

    public static final native int EAX_REVERB_Volume_get(long j);

    public static final native void EAX_REVERB_Volume_set(long j, int i);

    public static final native int EAX_RMODULATOR_Effect_get(long j);

    public static final native void EAX_RMODULATOR_Effect_set(long j, int i);

    public static final native float EAX_RMODULATOR_Frequency_get(long j);

    public static final native void EAX_RMODULATOR_Frequency_set(long j, float f);

    public static final native float EAX_RMODULATOR_HighPassCutOff_get(long j);

    public static final native void EAX_RMODULATOR_HighPassCutOff_set(long j, float f);

    public static final native int EAX_RMODULATOR_Volume_get(long j);

    public static final native void EAX_RMODULATOR_Volume_set(long j, int i);

    public static final native long EAX_RMODULATOR_Waveform_get(long j);

    public static final native void EAX_RMODULATOR_Waveform_set(long j, long j2);

    public static final native long EAX_SAMPLE_SLOT_VOLUMES_NumVolumes_get(long j);

    public static final native void EAX_SAMPLE_SLOT_VOLUMES_NumVolumes_set(long j, long j2);

    public static final native long EAX_SAMPLE_SLOT_VOLUMES_volumes_get(long j);

    public static final native void EAX_SAMPLE_SLOT_VOLUMES_volumes_set(long j, long j2);

    public static final native float EAX_SAMPLE_SLOT_VOLUME_OcclusionDirectRatio_get(long j);

    public static final native void EAX_SAMPLE_SLOT_VOLUME_OcclusionDirectRatio_set(long j, float f);

    public static final native float EAX_SAMPLE_SLOT_VOLUME_OcclusionLFRatio_get(long j);

    public static final native void EAX_SAMPLE_SLOT_VOLUME_OcclusionLFRatio_set(long j, float f);

    public static final native float EAX_SAMPLE_SLOT_VOLUME_OcclusionRoomRatio_get(long j);

    public static final native void EAX_SAMPLE_SLOT_VOLUME_OcclusionRoomRatio_set(long j, float f);

    public static final native int EAX_SAMPLE_SLOT_VOLUME_Occlusion_get(long j);

    public static final native void EAX_SAMPLE_SLOT_VOLUME_Occlusion_set(long j, int i);

    public static final native int EAX_SAMPLE_SLOT_VOLUME_SendHF_get(long j);

    public static final native void EAX_SAMPLE_SLOT_VOLUME_SendHF_set(long j, int i);

    public static final native int EAX_SAMPLE_SLOT_VOLUME_Send_get(long j);

    public static final native void EAX_SAMPLE_SLOT_VOLUME_Send_set(long j, int i);

    public static final native int EAX_SAMPLE_SLOT_VOLUME_Slot_get(long j);

    public static final native void EAX_SAMPLE_SLOT_VOLUME_Slot_set(long j, int i);

    public static final native int EAX_VMORPHER_Effect_get(long j);

    public static final native void EAX_VMORPHER_Effect_set(long j, int i);

    public static final native int EAX_VMORPHER_PhonemeACoarseTuning_get(long j);

    public static final native void EAX_VMORPHER_PhonemeACoarseTuning_set(long j, int i);

    public static final native long EAX_VMORPHER_PhonemeA_get(long j);

    public static final native void EAX_VMORPHER_PhonemeA_set(long j, long j2);

    public static final native int EAX_VMORPHER_PhonemeBCoarseTuning_get(long j);

    public static final native void EAX_VMORPHER_PhonemeBCoarseTuning_set(long j, int i);

    public static final native long EAX_VMORPHER_PhonemeB_get(long j);

    public static final native void EAX_VMORPHER_PhonemeB_set(long j, long j2);

    public static final native float EAX_VMORPHER_Rate_get(long j);

    public static final native void EAX_VMORPHER_Rate_set(long j, float f);

    public static final native int EAX_VMORPHER_Volume_get(long j);

    public static final native void EAX_VMORPHER_Volume_set(long j, int i);

    public static final native long EAX_VMORPHER_Waveform_get(long j);

    public static final native void EAX_VMORPHER_Waveform_set(long j, long j2);

    public static final native int ENVIRONMENT_ALLEY_get();

    public static final native int ENVIRONMENT_ARENA_get();

    public static final native int ENVIRONMENT_AUDITORIUM_get();

    public static final native int ENVIRONMENT_BATHROOM_get();

    public static final native int ENVIRONMENT_CARPETEDHALLWAY_get();

    public static final native int ENVIRONMENT_CAVE_get();

    public static final native int ENVIRONMENT_CITY_get();

    public static final native int ENVIRONMENT_CONCERTHALL_get();

    public static final native int ENVIRONMENT_COUNT_get();

    public static final native int ENVIRONMENT_DIZZY_get();

    public static final native int ENVIRONMENT_DRUGGED_get();

    public static final native int ENVIRONMENT_FOREST_get();

    public static final native int ENVIRONMENT_GENERIC_get();

    public static final native int ENVIRONMENT_HALLWAY_get();

    public static final native int ENVIRONMENT_HANGAR_get();

    public static final native int ENVIRONMENT_LIVINGROOM_get();

    public static final native int ENVIRONMENT_MOUNTAINS_get();

    public static final native int ENVIRONMENT_PADDEDCELL_get();

    public static final native int ENVIRONMENT_PARKINGLOT_get();

    public static final native int ENVIRONMENT_PLAIN_get();

    public static final native int ENVIRONMENT_PSYCHOTIC_get();

    public static final native int ENVIRONMENT_QUARRY_get();

    public static final native int ENVIRONMENT_ROOM_get();

    public static final native int ENVIRONMENT_SEWERPIPE_get();

    public static final native int ENVIRONMENT_STONECORRIDOR_get();

    public static final native int ENVIRONMENT_STONEROOM_get();

    public static final native int ENVIRONMENT_UNDERWATER_get();

    public static final native long FLTPROVIDER_PROVIDER_property_get(long j);

    public static final native void FLTPROVIDER_PROVIDER_property_set(long j, long j2);

    public static final native long FLTPROVIDER_assign_sample_voice_get(long j);

    public static final native void FLTPROVIDER_assign_sample_voice_set(long j, long j2);

    public static final native long FLTPROVIDER_close_driver_get(long j);

    public static final native void FLTPROVIDER_close_driver_set(long j, long j2);

    public static final native long FLTPROVIDER_close_sample_get(long j);

    public static final native void FLTPROVIDER_close_sample_set(long j, long j2);

    public static final native long FLTPROVIDER_dig_get(long j);

    public static final native void FLTPROVIDER_dig_set(long j, long j2);

    public static final native long FLTPROVIDER_driver_property_get(long j);

    public static final native void FLTPROVIDER_driver_property_set(long j, long j2);

    public static final native int FLTPROVIDER_driver_state_get(long j);

    public static final native void FLTPROVIDER_driver_state_set(long j, int i);

    public static final native long FLTPROVIDER_error_get(long j);

    public static final native void FLTPROVIDER_error_set(long j, long j2);

    public static final native long FLTPROVIDER_force_update_get(long j);

    public static final native void FLTPROVIDER_force_update_set(long j, long j2);

    public static final native long FLTPROVIDER_next_get(long j);

    public static final native void FLTPROVIDER_next_set(long j, long j2);

    public static final native long FLTPROVIDER_open_driver_get(long j);

    public static final native void FLTPROVIDER_open_driver_set(long j, long j2);

    public static final native long FLTPROVIDER_open_sample_get(long j);

    public static final native void FLTPROVIDER_open_sample_set(long j, long j2);

    public static final native long FLTPROVIDER_output_sample_property_get(long j);

    public static final native void FLTPROVIDER_output_sample_property_set(long j, long j2);

    public static final native long FLTPROVIDER_postmix_process_get(long j);

    public static final native void FLTPROVIDER_postmix_process_set(long j, long j2);

    public static final native long FLTPROVIDER_premix_process_get(long j);

    public static final native void FLTPROVIDER_premix_process_set(long j, long j2);

    public static final native int FLTPROVIDER_provider_flags_get(long j);

    public static final native void FLTPROVIDER_provider_flags_set(long j, int i);

    public static final native long FLTPROVIDER_provider_get(long j);

    public static final native void FLTPROVIDER_provider_set(long j, long j2);

    public static final native long FLTPROVIDER_release_sample_voice_get(long j);

    public static final native void FLTPROVIDER_release_sample_voice_set(long j, long j2);

    public static final native long FLTPROVIDER_sample_process_get(long j);

    public static final native void FLTPROVIDER_sample_process_set(long j, long j2);

    public static final native long FLTPROVIDER_sample_property_get(long j);

    public static final native void FLTPROVIDER_sample_property_set(long j, long j2);

    public static final native long FLTPROVIDER_shutdown_get(long j);

    public static final native void FLTPROVIDER_shutdown_set(long j, long j2);

    public static final native long FLTPROVIDER_start_sample_voice_get(long j);

    public static final native void FLTPROVIDER_start_sample_voice_set(long j, long j2);

    public static final native long FLTPROVIDER_startup_get(long j);

    public static final native void FLTPROVIDER_startup_set(long j, long j2);

    public static final native long FLTSTAGE_provider_get(long j);

    public static final native void FLTSTAGE_provider_set(long j, long j2);

    public static final native int FLTSTAGE_sample_state_get(long j);

    public static final native void FLTSTAGE_sample_state_set(long j, int i);

    public static final native int LOWPASS_CONSTANT_INFO_A_get(long j);

    public static final native void LOWPASS_CONSTANT_INFO_A_set(long j, int i);

    public static final native int LOWPASS_CONSTANT_INFO_B0_get(long j);

    public static final native void LOWPASS_CONSTANT_INFO_B0_set(long j, int i);

    public static final native int LOWPASS_CONSTANT_INFO_B1_get(long j);

    public static final native void LOWPASS_CONSTANT_INFO_B1_set(long j, int i);

    public static final native long LOWPASS_INFO_c_get(long j);

    public static final native void LOWPASS_INFO_c_set(long j, long j2);

    public static final native float LOWPASS_INFO_cutoff_get(long j);

    public static final native void LOWPASS_INFO_cutoff_set(long j, float f);

    public static final native int LOWPASS_INFO_on_get(long j);

    public static final native void LOWPASS_INFO_on_set(long j, int i);

    public static final native long LOWPASS_INFO_u_get(long j);

    public static final native void LOWPASS_INFO_u_set(long j, long j2);

    public static final native int LOWPASS_UPDATED_INFO_XL0_get(long j);

    public static final native void LOWPASS_UPDATED_INFO_XL0_set(long j, int i);

    public static final native int LOWPASS_UPDATED_INFO_XL1_get(long j);

    public static final native void LOWPASS_UPDATED_INFO_XL1_set(long j, int i);

    public static final native int LOWPASS_UPDATED_INFO_XR0_get(long j);

    public static final native void LOWPASS_UPDATED_INFO_XR0_set(long j, int i);

    public static final native int LOWPASS_UPDATED_INFO_XR1_get(long j);

    public static final native void LOWPASS_UPDATED_INFO_XR1_set(long j, int i);

    public static final native int LOWPASS_UPDATED_INFO_YL0_get(long j);

    public static final native void LOWPASS_UPDATED_INFO_YL0_set(long j, int i);

    public static final native int LOWPASS_UPDATED_INFO_YL1_get(long j);

    public static final native void LOWPASS_UPDATED_INFO_YL1_set(long j, int i);

    public static final native int LOWPASS_UPDATED_INFO_YR0_get(long j);

    public static final native void LOWPASS_UPDATED_INFO_YR0_set(long j, int i);

    public static final native int LOWPASS_UPDATED_INFO_YR1_get(long j);

    public static final native void LOWPASS_UPDATED_INFO_YR1_set(long j, int i);

    public static final native int MDI_DRIVER_callingCT_get(long j);

    public static final native void MDI_DRIVER_callingCT_set(long j, int i);

    public static final native int MDI_DRIVER_callingDS_get(long j);

    public static final native void MDI_DRIVER_callingDS_set(long j, int i);

    public static final native long MDI_DRIVER_deviceid_get(long j);

    public static final native void MDI_DRIVER_deviceid_set(long j, long j2);

    public static final native int MDI_DRIVER_disable_get(long j);

    public static final native void MDI_DRIVER_disable_set(long j, int i);

    public static final native long MDI_DRIVER_event_trap_get(long j);

    public static final native void MDI_DRIVER_event_trap_set(long j, long j2);

    public static final native long MDI_DRIVER_hMidiOut_get(long j);

    public static final native void MDI_DRIVER_hMidiOut_set(long j, long j2);

    public static final native int MDI_DRIVER_interval_time_get(long j);

    public static final native void MDI_DRIVER_interval_time_set(long j, int i);

    public static final native long MDI_DRIVER_lock_get(long j);

    public static final native void MDI_DRIVER_lock_set(long j, long j2);

    public static final native long MDI_DRIVER_locker_get(long j);

    public static final native void MDI_DRIVER_locker_set(long j, long j2);

    public static final native int MDI_DRIVER_master_volume_get(long j);

    public static final native void MDI_DRIVER_master_volume_set(long j, int i);

    public static final native long MDI_DRIVER_mhdr_get(long j);

    public static final native void MDI_DRIVER_mhdr_set(long j, long j2);

    public static final native int MDI_DRIVER_n_sequences_get(long j);

    public static final native void MDI_DRIVER_n_sequences_set(long j, int i);

    public static final native long MDI_DRIVER_next_get(long j);

    public static final native void MDI_DRIVER_next_set(long j, long j2);

    public static final native long MDI_DRIVER_notes_get(long j);

    public static final native void MDI_DRIVER_notes_set(long j, long j2);

    public static final native long MDI_DRIVER_owner_get(long j);

    public static final native void MDI_DRIVER_owner_set(long j, long j2);

    public static final native int MDI_DRIVER_released_get(long j);

    public static final native void MDI_DRIVER_released_set(long j, int i);

    public static final native long MDI_DRIVER_sequences_get(long j);

    public static final native void MDI_DRIVER_sequences_set(long j, long j2);

    public static final native long MDI_DRIVER_state_get(long j);

    public static final native void MDI_DRIVER_state_set(long j, long j2);

    public static final native long MDI_DRIVER_sysdata_get(long j);

    public static final native void MDI_DRIVER_sysdata_set(long j, long j2);

    public static final native long MDI_DRIVER_system_data_get(long j);

    public static final native void MDI_DRIVER_system_data_set(long j, long j2);

    public static final native String MDI_DRIVER_tag_get(long j);

    public static final native void MDI_DRIVER_tag_set(long j, String str);

    public static final native long MDI_DRIVER_timbre_trap_get(long j);

    public static final native void MDI_DRIVER_timbre_trap_set(long j, long j2);

    public static final native int MDI_DRIVER_timer_get(long j);

    public static final native void MDI_DRIVER_timer_set(long j, int i);

    public static final native long MDI_DRIVER_user_get(long j);

    public static final native void MDI_DRIVER_user_set(long j, long j2);

    public static final native long MP3_INFO_ID3v1_get(long j);

    public static final native void MP3_INFO_ID3v1_set(long j, long j2);

    public static final native long MP3_INFO_ID3v2_get(long j);

    public static final native void MP3_INFO_ID3v2_set(long j, long j2);

    public static final native int MP3_INFO_ID3v2_size_get(long j);

    public static final native void MP3_INFO_ID3v2_size_set(long j, int i);

    public static final native int MP3_INFO_Info_valid_get(long j);

    public static final native void MP3_INFO_Info_valid_set(long j, int i);

    public static final native long MP3_INFO_MP3_file_image_get(long j);

    public static final native void MP3_INFO_MP3_file_image_set(long j, long j2);

    public static final native int MP3_INFO_MP3_image_size_get(long j);

    public static final native void MP3_INFO_MP3_image_size_set(long j, int i);

    public static final native int MP3_INFO_MPEG1_get(long j);

    public static final native void MP3_INFO_MPEG1_set(long j, int i);

    public static final native int MP3_INFO_MPEG25_get(long j);

    public static final native void MP3_INFO_MPEG25_set(long j, int i);

    public static final native long MP3_INFO_TOC_get(long j);

    public static final native void MP3_INFO_TOC_set(long j, long j2);

    public static final native int MP3_INFO_VBR_scale_get(long j);

    public static final native void MP3_INFO_VBR_scale_set(long j, int i);

    public static final native int MP3_INFO_Xing_valid_get(long j);

    public static final native void MP3_INFO_Xing_valid_set(long j, int i);

    public static final native int MP3_INFO_average_frame_size_get(long j);

    public static final native void MP3_INFO_average_frame_size_set(long j, int i);

    public static final native int MP3_INFO_bit_rate_get(long j);

    public static final native void MP3_INFO_bit_rate_set(long j, int i);

    public static final native int MP3_INFO_bitrate_index_get(long j);

    public static final native void MP3_INFO_bitrate_index_set(long j, int i);

    public static final native int MP3_INFO_byte_count_get(long j);

    public static final native void MP3_INFO_byte_count_set(long j, int i);

    public static final native int MP3_INFO_byte_offset_get(long j);

    public static final native void MP3_INFO_byte_offset_set(long j, int i);

    public static final native int MP3_INFO_bytes_left_get(long j);

    public static final native void MP3_INFO_bytes_left_set(long j, int i);

    public static final native int MP3_INFO_channels_per_sample_get(long j);

    public static final native void MP3_INFO_channels_per_sample_set(long j, int i);

    public static final native int MP3_INFO_check_MPEG1_get(long j);

    public static final native void MP3_INFO_check_MPEG1_set(long j, int i);

    public static final native int MP3_INFO_check_MPEG25_get(long j);

    public static final native void MP3_INFO_check_MPEG25_set(long j, int i);

    public static final native int MP3_INFO_check_copyright_get(long j);

    public static final native void MP3_INFO_check_copyright_set(long j, int i);

    public static final native int MP3_INFO_check_layer_get(long j);

    public static final native void MP3_INFO_check_layer_set(long j, int i);

    public static final native int MP3_INFO_check_mode_get(long j);

    public static final native void MP3_INFO_check_mode_set(long j, int i);

    public static final native int MP3_INFO_check_original_get(long j);

    public static final native void MP3_INFO_check_original_set(long j, int i);

    public static final native int MP3_INFO_check_protection_bit_get(long j);

    public static final native void MP3_INFO_check_protection_bit_set(long j, int i);

    public static final native int MP3_INFO_check_sampling_frequency_get(long j);

    public static final native void MP3_INFO_check_sampling_frequency_set(long j, int i);

    public static final native int MP3_INFO_check_valid_get(long j);

    public static final native void MP3_INFO_check_valid_set(long j, int i);

    public static final native int MP3_INFO_copyright_get(long j);

    public static final native void MP3_INFO_copyright_set(long j, int i);

    public static final native int MP3_INFO_data_size_get(long j);

    public static final native void MP3_INFO_data_size_set(long j, int i);

    public static final native int MP3_INFO_emphasis_get(long j);

    public static final native void MP3_INFO_emphasis_set(long j, int i);

    public static final native int MP3_INFO_enc_delay_get(long j);

    public static final native void MP3_INFO_enc_delay_set(long j, int i);

    public static final native int MP3_INFO_enc_padding_get(long j);

    public static final native void MP3_INFO_enc_padding_set(long j, int i);

    public static final native long MP3_INFO_end_MP3_data_get(long j);

    public static final native void MP3_INFO_end_MP3_data_set(long j, long j2);

    public static final native int MP3_INFO_frame_count_get(long j);

    public static final native void MP3_INFO_frame_count_set(long j, int i);

    public static final native long MP3_INFO_header_flags_get(long j);

    public static final native void MP3_INFO_header_flags_set(long j, long j2);

    public static final native int MP3_INFO_header_size_get(long j);

    public static final native void MP3_INFO_header_size_set(long j, int i);

    public static final native int MP3_INFO_hpos_get(long j);

    public static final native void MP3_INFO_hpos_set(long j, int i);

    public static final native int MP3_INFO_layer_get(long j);

    public static final native void MP3_INFO_layer_set(long j, int i);

    public static final native int MP3_INFO_main_data_begin_get(long j);

    public static final native void MP3_INFO_main_data_begin_set(long j, int i);

    public static final native int MP3_INFO_mode_extension_get(long j);

    public static final native void MP3_INFO_mode_extension_set(long j, int i);

    public static final native int MP3_INFO_mode_get(long j);

    public static final native void MP3_INFO_mode_set(long j, int i);

    public static final native int MP3_INFO_next_frame_expected_get(long j);

    public static final native void MP3_INFO_next_frame_expected_set(long j, int i);

    public static final native int MP3_INFO_ngr_get(long j);

    public static final native void MP3_INFO_ngr_set(long j, int i);

    public static final native int MP3_INFO_original_get(long j);

    public static final native void MP3_INFO_original_set(long j, int i);

    public static final native int MP3_INFO_padding_bit_get(long j);

    public static final native void MP3_INFO_padding_bit_set(long j, int i);

    public static final native int MP3_INFO_private_bit_get(long j);

    public static final native void MP3_INFO_private_bit_set(long j, int i);

    public static final native int MP3_INFO_protection_bit_get(long j);

    public static final native void MP3_INFO_protection_bit_set(long j, int i);

    public static final native long MP3_INFO_ptr_get(long j);

    public static final native void MP3_INFO_ptr_set(long j, long j2);

    public static final native int MP3_INFO_sample_rate_get(long j);

    public static final native void MP3_INFO_sample_rate_set(long j, int i);

    public static final native int MP3_INFO_samples_per_frame_get(long j);

    public static final native void MP3_INFO_samples_per_frame_set(long j, int i);

    public static final native int MP3_INFO_sampling_frequency_get(long j);

    public static final native void MP3_INFO_sampling_frequency_set(long j, int i);

    public static final native int MP3_INFO_side_info_size_get(long j);

    public static final native void MP3_INFO_side_info_size_set(long j, int i);

    public static final native long MP3_INFO_start_MP3_data_get(long j);

    public static final native void MP3_INFO_start_MP3_data_set(long j, long j2);

    public static final native int MPCMWAVEFORMAT_wBitsPerSample_get(long j);

    public static final native void MPCMWAVEFORMAT_wBitsPerSample_set(long j, int i);

    public static final native long MPCMWAVEFORMAT_wf_get(long j);

    public static final native void MPCMWAVEFORMAT_wf_set(long j, long j2);

    public static final native float MSSVECTOR3D_x_get(long j);

    public static final native void MSSVECTOR3D_x_set(long j, float f);

    public static final native float MSSVECTOR3D_y_get(long j);

    public static final native void MSSVECTOR3D_y_set(long j, float f);

    public static final native float MSSVECTOR3D_z_get(long j);

    public static final native void MSSVECTOR3D_z_set(long j, float f);

    public static final native long MSS_BB_buffer_get(long j);

    public static final native void MSS_BB_buffer_set(long j, long j2);

    public static final native int MSS_BB_bytes_get(long j);

    public static final native void MSS_BB_bytes_set(long j, int i);

    public static final native int MSS_BB_chans_get(long j);

    public static final native void MSS_BB_chans_set(long j, int i);

    public static final native int MSS_BB_speaker_offset_get(long j);

    public static final native void MSS_BB_speaker_offset_set(long j, int i);

    public static final native int MSS_MC_40_DISCRETE_get();

    public static final native int MSS_MC_40_DTS_get();

    public static final native int MSS_MC_51_DISCRETE_get();

    public static final native int MSS_MC_51_DTS_get();

    public static final native int MSS_MC_61_DISCRETE_get();

    public static final native int MSS_MC_71_DISCRETE_get();

    public static final native int MSS_MC_81_DISCRETE_get();

    public static final native int MSS_MC_DIRECTSOUND3D_get();

    public static final native int MSS_MC_DOLBY_SURROUND_get();

    public static final native int MSS_MC_EAX2_get();

    public static final native int MSS_MC_EAX3_get();

    public static final native int MSS_MC_EAX4_get();

    public static final native int MSS_MC_HEADPHONES_get();

    public static final native int MSS_MC_INVALID_get();

    public static final native int MSS_MC_MONO_get();

    public static final native int MSS_MC_SRS_CIRCLE_SURROUND_get();

    public static final native int MSS_MC_STEREO_get();

    public static final native int MSS_MC_USE_SYSTEM_CONFIG_get();

    public static final native long MSS_RECEIVER_LIST_direction_get(long j);

    public static final native void MSS_RECEIVER_LIST_direction_set(long j, long j2);

    public static final native int MSS_RECEIVER_LIST_n_speakers_affected_get(long j);

    public static final native void MSS_RECEIVER_LIST_n_speakers_affected_set(long j, int i);

    public static final native long MSS_RECEIVER_LIST_speaker_index_get(long j);

    public static final native void MSS_RECEIVER_LIST_speaker_index_set(long j, long j2);

    public static final native long MSS_RECEIVER_LIST_speaker_level_get(long j);

    public static final native void MSS_RECEIVER_LIST_speaker_level_set(long j, long j2);

    public static final native int MSS_SPEAKER_BACK_CENTER_get();

    public static final native int MSS_SPEAKER_BACK_LEFT_get();

    public static final native int MSS_SPEAKER_BACK_RIGHT_get();

    public static final native int MSS_SPEAKER_FRONT_CENTER_get();

    public static final native int MSS_SPEAKER_FRONT_LEFT_OF_CENTER_get();

    public static final native int MSS_SPEAKER_FRONT_LEFT_get();

    public static final native int MSS_SPEAKER_FRONT_RIGHT_OF_CENTER_get();

    public static final native int MSS_SPEAKER_FRONT_RIGHT_get();

    public static final native int MSS_SPEAKER_LOW_FREQUENCY_get();

    public static final native int MSS_SPEAKER_MAX_INDEX_get();

    public static final native int MSS_SPEAKER_SIDE_LEFT_get();

    public static final native int MSS_SPEAKER_SIDE_RIGHT_get();

    public static final native int MSS_SPEAKER_TOP_BACK_CENTER_get();

    public static final native int MSS_SPEAKER_TOP_BACK_LEFT_get();

    public static final native int MSS_SPEAKER_TOP_BACK_RIGHT_get();

    public static final native int MSS_SPEAKER_TOP_CENTER_get();

    public static final native int MSS_SPEAKER_TOP_FRONT_CENTER_get();

    public static final native int MSS_SPEAKER_TOP_FRONT_LEFT_get();

    public static final native int MSS_SPEAKER_TOP_FRONT_RIGHT_get();

    public static final native long MSTREAM_TYPE_ASI_get(long j);

    public static final native void MSTREAM_TYPE_ASI_set(long j, long j2);

    public static final native int MSTREAM_TYPE_alldone_get(long j);

    public static final native void MSTREAM_TYPE_alldone_set(long j, int i);

    public static final native long MSTREAM_TYPE_asyncs_get(long j);

    public static final native void MSTREAM_TYPE_asyncs_set(long j, long j2);

    public static final native int MSTREAM_TYPE_autostreaming_get(long j);

    public static final native void MSTREAM_TYPE_autostreaming_set(long j, int i);

    public static final native int MSTREAM_TYPE_block_oriented_get(long j);

    public static final native void MSTREAM_TYPE_block_oriented_set(long j, int i);

    public static final native long MSTREAM_TYPE_blocksize_get(long j);

    public static final native void MSTREAM_TYPE_blocksize_set(long j, long j2);

    public static final native long MSTREAM_TYPE_buf1_get(long j);

    public static final native void MSTREAM_TYPE_buf1_set(long j, long j2);

    public static final native long MSTREAM_TYPE_buf2_get(long j);

    public static final native void MSTREAM_TYPE_buf2_set(long j, long j2);

    public static final native long MSTREAM_TYPE_buf3_get(long j);

    public static final native void MSTREAM_TYPE_buf3_set(long j, long j2);

    public static final native long MSTREAM_TYPE_bufs_get(long j);

    public static final native void MSTREAM_TYPE_bufs_set(long j, long j2);

    public static final native int MSTREAM_TYPE_bufsize_get(long j);

    public static final native void MSTREAM_TYPE_bufsize_set(long j, int i);

    public static final native long MSTREAM_TYPE_bufsizes_get(long j);

    public static final native void MSTREAM_TYPE_bufsizes_set(long j, long j2);

    public static final native long MSTREAM_TYPE_bufstart_get(long j);

    public static final native void MSTREAM_TYPE_bufstart_set(long j, long j2);

    public static final native long MSTREAM_TYPE_callback_get(long j);

    public static final native void MSTREAM_TYPE_callback_set(long j, long j2);

    public static final native long MSTREAM_TYPE_datarate_get(long j);

    public static final native void MSTREAM_TYPE_datarate_set(long j, long j2);

    public static final native int MSTREAM_TYPE_docallback_get(long j);

    public static final native void MSTREAM_TYPE_docallback_set(long j, int i);

    public static final native long MSTREAM_TYPE_error_get(long j);

    public static final native void MSTREAM_TYPE_error_set(long j, long j2);

    public static final native long MSTREAM_TYPE_fileflags_get(long j);

    public static final native void MSTREAM_TYPE_fileflags_set(long j, long j2);

    public static final native long MSTREAM_TYPE_fileh_get(long j);

    public static final native void MSTREAM_TYPE_fileh_set(long j, long j2);

    public static final native long MSTREAM_TYPE_filemask_get(long j);

    public static final native void MSTREAM_TYPE_filemask_set(long j, long j2);

    public static final native int MSTREAM_TYPE_filerate_get(long j);

    public static final native void MSTREAM_TYPE_filerate_set(long j, int i);

    public static final native int MSTREAM_TYPE_filetype_get(long j);

    public static final native void MSTREAM_TYPE_filetype_set(long j, int i);

    public static final native long MSTREAM_TYPE_loadedbufstart_get(long j);

    public static final native void MSTREAM_TYPE_loadedbufstart_set(long j, long j2);

    public static final native long MSTREAM_TYPE_loadedorder_get(long j);

    public static final native void MSTREAM_TYPE_loadedorder_set(long j, long j2);

    public static final native int MSTREAM_TYPE_loadedsome_get(long j);

    public static final native void MSTREAM_TYPE_loadedsome_set(long j, int i);

    public static final native int MSTREAM_TYPE_loadorder_get(long j);

    public static final native void MSTREAM_TYPE_loadorder_set(long j, int i);

    public static final native long MSTREAM_TYPE_loopsleft_get(long j);

    public static final native void MSTREAM_TYPE_loopsleft_set(long j, long j2);

    public static final native long MSTREAM_TYPE_next_get(long j);

    public static final native void MSTREAM_TYPE_next_set(long j, long j2);

    public static final native int MSTREAM_TYPE_noback_get(long j);

    public static final native void MSTREAM_TYPE_noback_set(long j, int i);

    public static final native int MSTREAM_TYPE_padded_get(long j);

    public static final native void MSTREAM_TYPE_padded_set(long j, int i);

    public static final native int MSTREAM_TYPE_padding_get(long j);

    public static final native void MSTREAM_TYPE_padding_set(long j, int i);

    public static final native int MSTREAM_TYPE_playcontrol_get(long j);

    public static final native void MSTREAM_TYPE_playcontrol_set(long j, int i);

    public static final native int MSTREAM_TYPE_preload_get(long j);

    public static final native void MSTREAM_TYPE_preload_set(long j, int i);

    public static final native long MSTREAM_TYPE_preloadpos_get(long j);

    public static final native void MSTREAM_TYPE_preloadpos_set(long j, long j2);

    public static final native int MSTREAM_TYPE_primeamount_get(long j);

    public static final native void MSTREAM_TYPE_primeamount_set(long j, int i);

    public static final native int MSTREAM_TYPE_readatleast_get(long j);

    public static final native void MSTREAM_TYPE_readatleast_set(long j, int i);

    public static final native int MSTREAM_TYPE_readsize_get(long j);

    public static final native void MSTREAM_TYPE_readsize_set(long j, int i);

    public static final native long MSTREAM_TYPE_reset_ASI_get(long j);

    public static final native void MSTREAM_TYPE_reset_ASI_set(long j, long j2);

    public static final native long MSTREAM_TYPE_reset_seek_pos_get(long j);

    public static final native void MSTREAM_TYPE_reset_seek_pos_set(long j, long j2);

    public static final native long MSTREAM_TYPE_samp_get(long j);

    public static final native void MSTREAM_TYPE_samp_set(long j, long j2);

    public static final native int MSTREAM_TYPE_size1_get(long j);

    public static final native void MSTREAM_TYPE_size1_set(long j, int i);

    public static final native int MSTREAM_TYPE_size2_get(long j);

    public static final native void MSTREAM_TYPE_size2_set(long j, int i);

    public static final native int MSTREAM_TYPE_size3_get(long j);

    public static final native void MSTREAM_TYPE_size3_set(long j, int i);

    public static final native long MSTREAM_TYPE_startpos_get(long j);

    public static final native void MSTREAM_TYPE_startpos_set(long j, long j2);

    public static final native int MSTREAM_TYPE_sublen_get(long j);

    public static final native void MSTREAM_TYPE_sublen_set(long j, int i);

    public static final native int MSTREAM_TYPE_subpadding_get(long j);

    public static final native void MSTREAM_TYPE_subpadding_set(long j, int i);

    public static final native int MSTREAM_TYPE_substart_get(long j);

    public static final native void MSTREAM_TYPE_substart_set(long j, int i);

    public static final native int MSTREAM_TYPE_totallen_get(long j);

    public static final native void MSTREAM_TYPE_totallen_set(long j, int i);

    public static final native long MSTREAM_TYPE_totalread_get(long j);

    public static final native void MSTREAM_TYPE_totalread_set(long j, long j2);

    public static final native long MSTREAM_TYPE_user_data_get(long j);

    public static final native void MSTREAM_TYPE_user_data_set(long j, long j2);

    public static final native int MSTREAM_TYPE_using_ASI_get(long j);

    public static final native void MSTREAM_TYPE_using_ASI_set(long j, int i);

    public static final native long MWAVEFORMATEXTENSIBLE_Format_get(long j);

    public static final native void MWAVEFORMATEXTENSIBLE_Format_set(long j, long j2);

    public static final native long MWAVEFORMATEXTENSIBLE_Samples_get(long j);

    public static final native int MWAVEFORMATEXTENSIBLE_Samples_wReserved_get(long j);

    public static final native void MWAVEFORMATEXTENSIBLE_Samples_wReserved_set(long j, int i);

    public static final native int MWAVEFORMATEXTENSIBLE_Samples_wSamplesPerBlock_get(long j);

    public static final native void MWAVEFORMATEXTENSIBLE_Samples_wSamplesPerBlock_set(long j, int i);

    public static final native int MWAVEFORMATEXTENSIBLE_Samples_wValidBitsPerSample_get(long j);

    public static final native void MWAVEFORMATEXTENSIBLE_Samples_wValidBitsPerSample_set(long j, int i);

    public static final native long MWAVEFORMATEXTENSIBLE_SubFormat_get(long j);

    public static final native void MWAVEFORMATEXTENSIBLE_SubFormat_set(long j, long j2);

    public static final native long MWAVEFORMATEXTENSIBLE_dwChannelMask_get(long j);

    public static final native void MWAVEFORMATEXTENSIBLE_dwChannelMask_set(long j, long j2);

    public static final native int MWAVEFORMATEX_cbSize_get(long j);

    public static final native void MWAVEFORMATEX_cbSize_set(long j, int i);

    public static final native long MWAVEFORMATEX_nAvgBytesPerSec_get(long j);

    public static final native void MWAVEFORMATEX_nAvgBytesPerSec_set(long j, long j2);

    public static final native int MWAVEFORMATEX_nBlockAlign_get(long j);

    public static final native void MWAVEFORMATEX_nBlockAlign_set(long j, int i);

    public static final native int MWAVEFORMATEX_nChannels_get(long j);

    public static final native void MWAVEFORMATEX_nChannels_set(long j, int i);

    public static final native long MWAVEFORMATEX_nSamplesPerSec_get(long j);

    public static final native void MWAVEFORMATEX_nSamplesPerSec_set(long j, long j2);

    public static final native int MWAVEFORMATEX_wBitsPerSample_get(long j);

    public static final native void MWAVEFORMATEX_wBitsPerSample_set(long j, int i);

    public static final native int MWAVEFORMATEX_wFormatTag_get(long j);

    public static final native void MWAVEFORMATEX_wFormatTag_set(long j, int i);

    public static final native long MWAVEFORMAT_nAvgBytesPerSec_get(long j);

    public static final native void MWAVEFORMAT_nAvgBytesPerSec_set(long j, long j2);

    public static final native int MWAVEFORMAT_nBlockAlign_get(long j);

    public static final native void MWAVEFORMAT_nBlockAlign_set(long j, int i);

    public static final native int MWAVEFORMAT_nChannels_get(long j);

    public static final native void MWAVEFORMAT_nChannels_set(long j, int i);

    public static final native long MWAVEFORMAT_nSamplesPerSec_get(long j);

    public static final native void MWAVEFORMAT_nSamplesPerSec_set(long j, long j2);

    public static final native int MWAVEFORMAT_wFormatTag_get(long j);

    public static final native void MWAVEFORMAT_wFormatTag_set(long j, int i);

    public static final native long MWAVEHDR_dwBufferLength_get(long j);

    public static final native void MWAVEHDR_dwBufferLength_set(long j, long j2);

    public static final native long MWAVEHDR_dwBytesRecorded_get(long j);

    public static final native void MWAVEHDR_dwBytesRecorded_set(long j, long j2);

    public static final native long MWAVEHDR_dwFlags_get(long j);

    public static final native void MWAVEHDR_dwFlags_set(long j, long j2);

    public static final native long MWAVEHDR_dwLoops_get(long j);

    public static final native void MWAVEHDR_dwLoops_set(long j, long j2);

    public static final native long MWAVEHDR_dwUser_get(long j);

    public static final native void MWAVEHDR_dwUser_set(long j, long j2);

    public static final native String MWAVEHDR_lpData_get(long j);

    public static final native void MWAVEHDR_lpData_set(long j, String str);

    public static final native long MWAVEHDR_lpNext_get(long j);

    public static final native void MWAVEHDR_lpNext_set(long j, long j2);

    public static final native long MWAVEHDR_reserved_get(long j);

    public static final native void MWAVEHDR_reserved_set(long j, long j2);

    public static final native short RBRN_entry_bnum_get(long j);

    public static final native void RBRN_entry_bnum_set(long j, short s);

    public static final native long RBRN_entry_offset_get(long j);

    public static final native void RBRN_entry_offset_set(long j, long j2);

    public static final native long REDBOOK_DeviceID_get(long j);

    public static final native void REDBOOK_DeviceID_set(long j, long j2);

    public static final native long REDBOOK_lastendsec_get(long j);

    public static final native void REDBOOK_lastendsec_set(long j, long j2);

    public static final native long REDBOOK_paused_get(long j);

    public static final native void REDBOOK_paused_set(long j, long j2);

    public static final native long REDBOOK_pausedsec_get(long j);

    public static final native void REDBOOK_pausedsec_set(long j, long j2);

    public static final native float REVERB_CONSTANT_INFO_A_get(long j);

    public static final native void REVERB_CONSTANT_INFO_A_set(long j, float f);

    public static final native float REVERB_CONSTANT_INFO_B0_get(long j);

    public static final native void REVERB_CONSTANT_INFO_B0_set(long j, float f);

    public static final native float REVERB_CONSTANT_INFO_B1_get(long j);

    public static final native void REVERB_CONSTANT_INFO_B1_set(long j, float f);

    public static final native float REVERB_CONSTANT_INFO_C0_get(long j);

    public static final native void REVERB_CONSTANT_INFO_C0_set(long j, float f);

    public static final native float REVERB_CONSTANT_INFO_C1_get(long j);

    public static final native void REVERB_CONSTANT_INFO_C1_set(long j, float f);

    public static final native float REVERB_CONSTANT_INFO_C2_get(long j);

    public static final native void REVERB_CONSTANT_INFO_C2_set(long j, float f);

    public static final native float REVERB_CONSTANT_INFO_C3_get(long j);

    public static final native void REVERB_CONSTANT_INFO_C3_set(long j, float f);

    public static final native float REVERB_CONSTANT_INFO_C4_get(long j);

    public static final native void REVERB_CONSTANT_INFO_C4_set(long j, float f);

    public static final native float REVERB_CONSTANT_INFO_C5_get(long j);

    public static final native void REVERB_CONSTANT_INFO_C5_set(long j, float f);

    public static final native long REVERB_CONSTANT_INFO_end0_get(long j);

    public static final native void REVERB_CONSTANT_INFO_end0_set(long j, long j2);

    public static final native long REVERB_CONSTANT_INFO_end1_get(long j);

    public static final native void REVERB_CONSTANT_INFO_end1_set(long j, long j2);

    public static final native long REVERB_CONSTANT_INFO_end2_get(long j);

    public static final native void REVERB_CONSTANT_INFO_end2_set(long j, long j2);

    public static final native long REVERB_CONSTANT_INFO_end3_get(long j);

    public static final native void REVERB_CONSTANT_INFO_end3_set(long j, long j2);

    public static final native long REVERB_CONSTANT_INFO_end4_get(long j);

    public static final native void REVERB_CONSTANT_INFO_end4_set(long j, long j2);

    public static final native long REVERB_CONSTANT_INFO_end5_get(long j);

    public static final native void REVERB_CONSTANT_INFO_end5_set(long j, long j2);

    public static final native long REVERB_CONSTANT_INFO_start0_get(long j);

    public static final native void REVERB_CONSTANT_INFO_start0_set(long j, long j2);

    public static final native long REVERB_CONSTANT_INFO_start1_get(long j);

    public static final native void REVERB_CONSTANT_INFO_start1_set(long j, long j2);

    public static final native long REVERB_CONSTANT_INFO_start2_get(long j);

    public static final native void REVERB_CONSTANT_INFO_start2_set(long j, long j2);

    public static final native long REVERB_CONSTANT_INFO_start3_get(long j);

    public static final native void REVERB_CONSTANT_INFO_start3_set(long j, long j2);

    public static final native long REVERB_CONSTANT_INFO_start4_get(long j);

    public static final native void REVERB_CONSTANT_INFO_start4_set(long j, long j2);

    public static final native long REVERB_CONSTANT_INFO_start5_get(long j);

    public static final native void REVERB_CONSTANT_INFO_start5_set(long j, long j2);

    public static final native long REVERB_INFO_c_get(long j);

    public static final native void REVERB_INFO_c_set(long j, long j2);

    public static final native long REVERB_INFO_u_get(long j);

    public static final native void REVERB_INFO_u_set(long j, long j2);

    public static final native float REVERB_UPDATED_INFO_X0_get(long j);

    public static final native void REVERB_UPDATED_INFO_X0_set(long j, float f);

    public static final native float REVERB_UPDATED_INFO_X1_get(long j);

    public static final native void REVERB_UPDATED_INFO_X1_set(long j, float f);

    public static final native float REVERB_UPDATED_INFO_Y0_get(long j);

    public static final native void REVERB_UPDATED_INFO_Y0_set(long j, float f);

    public static final native float REVERB_UPDATED_INFO_Y1_get(long j);

    public static final native void REVERB_UPDATED_INFO_Y1_set(long j, float f);

    public static final native long REVERB_UPDATED_INFO_address0_get(long j);

    public static final native void REVERB_UPDATED_INFO_address0_set(long j, long j2);

    public static final native long REVERB_UPDATED_INFO_address1_get(long j);

    public static final native void REVERB_UPDATED_INFO_address1_set(long j, long j2);

    public static final native long REVERB_UPDATED_INFO_address2_get(long j);

    public static final native void REVERB_UPDATED_INFO_address2_set(long j, long j2);

    public static final native long REVERB_UPDATED_INFO_address3_get(long j);

    public static final native void REVERB_UPDATED_INFO_address3_set(long j, long j2);

    public static final native long REVERB_UPDATED_INFO_address4_get(long j);

    public static final native void REVERB_UPDATED_INFO_address4_set(long j, long j2);

    public static final native long REVERB_UPDATED_INFO_address5_get(long j);

    public static final native void REVERB_UPDATED_INFO_address5_set(long j, long j2);

    public static final native int RIB_FUNCTION_get();

    public static final native String RIB_INTERFACE_ENTRY_entry_name_get(long j);

    public static final native void RIB_INTERFACE_ENTRY_entry_name_set(long j, String str);

    public static final native int RIB_INTERFACE_ENTRY_subtype_get(long j);

    public static final native void RIB_INTERFACE_ENTRY_subtype_set(long j, int i);

    public static final native long RIB_INTERFACE_ENTRY_token_get(long j);

    public static final native void RIB_INTERFACE_ENTRY_token_set(long j, long j2);

    public static final native int RIB_INTERFACE_ENTRY_type_get(long j);

    public static final native void RIB_INTERFACE_ENTRY_type_set(long j, int i);

    public static final native int RIB_NONE_get();

    public static final native int RIB_READONLY_get();

    public static final native long RIB_alloc_provider_handle(int i);

    public static final native int RIB_enumerate_interface(long j, String str, int i, long j2, long j3);

    public static final native int RIB_enumerate_providers(String str, long j, long j2);

    public static final native String RIB_error();

    public static final native long RIB_find_file_dec_provider(String str, String str2, long j, String str3, String str4);

    public static final native long RIB_find_file_provider(String str, String str2, String str3);

    public static final native long RIB_find_files_provider(String str, String str2, String str3, String str4, String str5);

    public static final native long RIB_find_provider(String str, String str2, long j);

    public static final native void RIB_free_provider_handle(long j);

    public static final native void RIB_free_provider_library(long j);

    public static final native int RIB_load_application_providers(String str);

    public static final native long RIB_load_provider_library(String str);

    public static final native long RIB_load_static_provider_library(long j, String str);

    public static final native int RIB_provider_system_data(long j, long j2);

    public static final native int RIB_provider_user_data(long j, long j2);

    public static final native int RIB_register_interface(long j, String str, int i, long j2);

    public static final native int RIB_request_interface(long j, String str, int i, long j2);

    public static final native void RIB_set_provider_system_data(long j, long j2, int i);

    public static final native void RIB_set_provider_user_data(long j, long j2, int i);

    public static final native String RIB_type_string(Buffer buffer, int i);

    public static final native int RIB_unregister_interface(long j, String str, int i, long j2);

    public static final native float S3DSTATE_atten_3D_get(long j);

    public static final native void S3DSTATE_atten_3D_set(long j, float f);

    public static final native int S3DSTATE_auto_3D_atten_get(long j);

    public static final native void S3DSTATE_auto_3D_atten_set(long j, int i);

    public static final native int S3DSTATE_cone_enabled_get(long j);

    public static final native void S3DSTATE_cone_enabled_set(long j, int i);

    public static final native int S3DSTATE_dist_changed_get(long j);

    public static final native void S3DSTATE_dist_changed_set(long j, int i);

    public static final native float S3DSTATE_doppler_shift_get(long j);

    public static final native void S3DSTATE_doppler_shift_set(long j, float f);

    public static final native int S3DSTATE_doppler_valid_get(long j);

    public static final native void S3DSTATE_doppler_valid_set(long j, int i);

    public static final native long S3DSTATE_face_get(long j);

    public static final native void S3DSTATE_face_set(long j, long j2);

    public static final native long S3DSTATE_falloff_function_get(long j);

    public static final native void S3DSTATE_falloff_function_set(long j, long j2);

    public static final native float S3DSTATE_inner_angle_get(long j);

    public static final native void S3DSTATE_inner_angle_set(long j, float f);

    public static final native float S3DSTATE_max_dist_get(long j);

    public static final native void S3DSTATE_max_dist_set(long j, float f);

    public static final native float S3DSTATE_min_dist_get(long j);

    public static final native void S3DSTATE_min_dist_set(long j, float f);

    public static final native float S3DSTATE_outer_angle_get(long j);

    public static final native void S3DSTATE_outer_angle_set(long j, float f);

    public static final native float S3DSTATE_outer_volume_get(long j);

    public static final native void S3DSTATE_outer_volume_set(long j, float f);

    public static final native long S3DSTATE_owner_get(long j);

    public static final native void S3DSTATE_owner_set(long j, long j2);

    public static final native long S3DSTATE_position_get(long j);

    public static final native void S3DSTATE_position_set(long j, long j2);

    public static final native long S3DSTATE_up_get(long j);

    public static final native void S3DSTATE_up_set(long j, long j2);

    public static final native long S3DSTATE_velocity_get(long j);

    public static final native void S3DSTATE_velocity_set(long j, long j2);

    public static final native long SAMPLE_EOB_get(long j);

    public static final native void SAMPLE_EOB_set(long j, long j2);

    public static final native long SAMPLE_EOS_get(long j);

    public static final native void SAMPLE_EOS_set(long j, long j2);

    public static final native long SAMPLE_S3D_get(long j);

    public static final native void SAMPLE_S3D_set(long j, long j2);

    public static final native long SAMPLE_SOB_get(long j);

    public static final native void SAMPLE_SOB_set(long j, long j2);

    public static final native long SAMPLE_adpcm_get(long j);

    public static final native void SAMPLE_adpcm_set(long j, long j2);

    public static final native long SAMPLE_auto_3D_channel_levels_get(long j);

    public static final native void SAMPLE_auto_3D_channel_levels_set(long j, long j2);

    public static final native long SAMPLE_buf_get(long j);

    public static final native void SAMPLE_buf_set(long j, long j2);

    public static final native int SAMPLE_buffer_segment_size_get(long j);

    public static final native void SAMPLE_buffer_segment_size_set(long j, int i);

    public static final native int SAMPLE_bytes_remaining_get(long j);

    public static final native void SAMPLE_bytes_remaining_set(long j, int i);

    public static final native float SAMPLE_calculated_cut_get(long j);

    public static final native void SAMPLE_calculated_cut_set(long j, float f);

    public static final native float SAMPLE_center_volume_get(long j);

    public static final native void SAMPLE_center_volume_set(long j, float f);

    public static final native long SAMPLE_channel_mask_get(long j);

    public static final native void SAMPLE_channel_mask_set(long j, long j2);

    public static final native long SAMPLE_cur_scale_get(long j);

    public static final native void SAMPLE_cur_scale_set(long j, long j2);

    public static final native float SAMPLE_cutoff_param_get(long j);

    public static final native void SAMPLE_cutoff_param_set(long j, float f);

    public static final native int SAMPLE_direct_control_get(long j);

    public static final native void SAMPLE_direct_control_set(long j, int i);

    public static final native int SAMPLE_doeob_get(long j);

    public static final native void SAMPLE_doeob_set(long j, int i);

    public static final native int SAMPLE_doeos_get(long j);

    public static final native void SAMPLE_doeos_set(long j, int i);

    public static final native int SAMPLE_dosob_get(long j);

    public static final native void SAMPLE_dosob_set(long j, int i);

    public static final native long SAMPLE_driver_get(long j);

    public static final native void SAMPLE_driver_set(long j, long j2);

    public static final native float SAMPLE_dry_level_get(long j);

    public static final native void SAMPLE_dry_level_set(long j, float f);

    public static final native float SAMPLE_exclusion_get(long j);

    public static final native void SAMPLE_exclusion_set(long j, float f);

    public static final native int SAMPLE_exhaust_ASI_get(long j);

    public static final native void SAMPLE_exhaust_ASI_set(long j, int i);

    public static final native long SAMPLE_flags_get(long j);

    public static final native void SAMPLE_flags_set(long j, long j2);

    public static final native int SAMPLE_format_get(long j);

    public static final native void SAMPLE_format_set(long j, int i);

    public static final native int SAMPLE_head_get(long j);

    public static final native void SAMPLE_head_set(long j, int i);

    public static final native int SAMPLE_index_get(long j);

    public static final native void SAMPLE_index_set(long j, int i);

    public static final native int SAMPLE_is_3D_get(long j);

    public static final native void SAMPLE_is_3D_set(long j, int i);

    public static final native long SAMPLE_last_decomp_get(long j);

    public static final native void SAMPLE_last_decomp_set(long j, long j2);

    public static final native long SAMPLE_left_val_get(long j);

    public static final native void SAMPLE_left_val_set(long j, long j2);

    public static final native float SAMPLE_left_volume_get(long j);

    public static final native void SAMPLE_left_volume_set(long j, float f);

    public static final native float SAMPLE_leftb_volume_get(long j);

    public static final native void SAMPLE_leftb_volume_set(long j, float f);

    public static final native int SAMPLE_loop_count_get(long j);

    public static final native void SAMPLE_loop_count_set(long j, int i);

    public static final native int SAMPLE_loop_end_get(long j);

    public static final native void SAMPLE_loop_end_set(long j, int i);

    public static final native int SAMPLE_loop_start_get(long j);

    public static final native void SAMPLE_loop_start_set(long j, int i);

    public static final native float SAMPLE_low_volume_get(long j);

    public static final native void SAMPLE_low_volume_set(long j, float f);

    public static final native long SAMPLE_lp_get(long j);

    public static final native void SAMPLE_lp_set(long j, long j2);

    public static final native int SAMPLE_n_active_filters_get(long j);

    public static final native void SAMPLE_n_active_filters_set(long j, int i);

    public static final native int SAMPLE_n_buffers_get(long j);

    public static final native void SAMPLE_n_buffers_set(long j, int i);

    public static final native int SAMPLE_n_channels_get(long j);

    public static final native void SAMPLE_n_channels_set(long j, int i);

    public static final native float SAMPLE_obstruction_get(long j);

    public static final native void SAMPLE_obstruction_set(long j, float f);

    public static final native float SAMPLE_occlusion_get(long j);

    public static final native void SAMPLE_occlusion_set(long j, float f);

    public static final native int SAMPLE_orig_loop_count_get(long j);

    public static final native void SAMPLE_orig_loop_count_set(long j, int i);

    public static final native int SAMPLE_orig_loop_end_get(long j);

    public static final native void SAMPLE_orig_loop_end_set(long j, int i);

    public static final native int SAMPLE_orig_loop_start_get(long j);

    public static final native void SAMPLE_orig_loop_start_set(long j, int i);

    public static final native long SAMPLE_pipeline_get(long j);

    public static final native void SAMPLE_pipeline_set(long j, long j2);

    public static final native int SAMPLE_playback_rate_get(long j);

    public static final native void SAMPLE_playback_rate_set(long j, int i);

    public static final native int SAMPLE_prev_cursor_get(long j);

    public static final native void SAMPLE_prev_cursor_set(long j, int i);

    public static final native long SAMPLE_prev_scale_get(long j);

    public static final native void SAMPLE_prev_scale_set(long j, long j2);

    public static final native int SAMPLE_prev_segment_get(long j);

    public static final native void SAMPLE_prev_segment_set(long j, int i);

    public static final native long SAMPLE_ramps_left_get(long j);

    public static final native void SAMPLE_ramps_left_set(long j, long j2);

    public static final native long SAMPLE_right_val_get(long j);

    public static final native void SAMPLE_right_val_set(long j, long j2);

    public static final native float SAMPLE_right_volume_get(long j);

    public static final native void SAMPLE_right_volume_set(long j, float f);

    public static final native float SAMPLE_rightb_volume_get(long j);

    public static final native void SAMPLE_rightb_volume_set(long j, float f);

    public static final native float SAMPLE_save_center_get(long j);

    public static final native void SAMPLE_save_center_set(long j, float f);

    public static final native float SAMPLE_save_fb_pan_get(long j);

    public static final native void SAMPLE_save_fb_pan_set(long j, float f);

    public static final native float SAMPLE_save_low_get(long j);

    public static final native void SAMPLE_save_low_set(long j, float f);

    public static final native float SAMPLE_save_pan_get(long j);

    public static final native void SAMPLE_save_pan_set(long j, float f);

    public static final native float SAMPLE_save_volume_get(long j);

    public static final native void SAMPLE_save_volume_set(long j, float f);

    public static final native int SAMPLE_service_interval_get(long j);

    public static final native void SAMPLE_service_interval_set(long j, int i);

    public static final native int SAMPLE_service_tick_get(long j);

    public static final native void SAMPLE_service_tick_set(long j, int i);

    public static final native int SAMPLE_service_type_get(long j);

    public static final native void SAMPLE_service_type_set(long j, int i);

    public static final native long SAMPLE_speaker_enum_to_source_chan_get(long j);

    public static final native void SAMPLE_speaker_enum_to_source_chan_set(long j, long j2);

    public static final native long SAMPLE_src_fract_get(long j);

    public static final native void SAMPLE_src_fract_set(long j, long j2);

    public static final native int SAMPLE_starved_get(long j);

    public static final native void SAMPLE_starved_set(long j, int i);

    public static final native long SAMPLE_system_data_get(long j);

    public static final native void SAMPLE_system_data_set(long j, long j2);

    public static final native String SAMPLE_tag_get(long j);

    public static final native void SAMPLE_tag_set(long j, String str);

    public static final native int SAMPLE_tail_get(long j);

    public static final native void SAMPLE_tail_set(long j, int i);

    public static final native long SAMPLE_user_channel_levels_get(long j);

    public static final native void SAMPLE_user_channel_levels_set(long j, long j2);

    public static final native long SAMPLE_user_data_get(long j);

    public static final native void SAMPLE_user_data_set(long j, long j2);

    public static final native long SAMPLE_voice_get(long j);

    public static final native void SAMPLE_voice_set(long j, long j2);

    public static final native float SAMPLE_wet_level_get(long j);

    public static final native void SAMPLE_wet_level_set(long j, float f);

    public static final native long SEQUENCE_EOS_get(long j);

    public static final native void SEQUENCE_EOS_set(long j, long j2);

    public static final native long SEQUENCE_EVNT_get(long j);

    public static final native long SEQUENCE_EVNT_ptr_get(long j);

    public static final native void SEQUENCE_EVNT_ptr_set(long j, long j2);

    public static final native void SEQUENCE_EVNT_set(long j, long j2);

    public static final native long SEQUENCE_FOR_loop_count_get(long j);

    public static final native void SEQUENCE_FOR_loop_count_set(long j, long j2);

    public static final native long SEQUENCE_FOR_ptrs_get(long j);

    public static final native void SEQUENCE_FOR_ptrs_set(long j, long j2);

    public static final native long SEQUENCE_ICA_get(long j);

    public static final native void SEQUENCE_ICA_set(long j, long j2);

    public static final native long SEQUENCE_RBRN_get(long j);

    public static final native void SEQUENCE_RBRN_set(long j, long j2);

    public static final native long SEQUENCE_TIMB_get(long j);

    public static final native void SEQUENCE_TIMB_set(long j, long j2);

    public static final native long SEQUENCE_beat_callback_get(long j);

    public static final native void SEQUENCE_beat_callback_set(long j, long j2);

    public static final native int SEQUENCE_beat_count_get(long j);

    public static final native void SEQUENCE_beat_count_set(long j, int i);

    public static final native int SEQUENCE_beat_fraction_get(long j);

    public static final native void SEQUENCE_beat_fraction_set(long j, int i);

    public static final native long SEQUENCE_chan_map_get(long j);

    public static final native void SEQUENCE_chan_map_set(long j, long j2);

    public static final native long SEQUENCE_driver_get(long j);

    public static final native void SEQUENCE_driver_set(long j, long j2);

    public static final native int SEQUENCE_interval_count_get(long j);

    public static final native void SEQUENCE_interval_count_set(long j, int i);

    public static final native int SEQUENCE_interval_num_get(long j);

    public static final native void SEQUENCE_interval_num_set(long j, int i);

    public static final native int SEQUENCE_loop_count_get(long j);

    public static final native void SEQUENCE_loop_count_set(long j, int i);

    public static final native int SEQUENCE_measure_count_get(long j);

    public static final native void SEQUENCE_measure_count_set(long j, int i);

    public static final native long SEQUENCE_note_chan_get(long j);

    public static final native void SEQUENCE_note_chan_set(long j, long j2);

    public static final native int SEQUENCE_note_count_get(long j);

    public static final native void SEQUENCE_note_count_set(long j, int i);

    public static final native long SEQUENCE_note_num_get(long j);

    public static final native void SEQUENCE_note_num_set(long j, long j2);

    public static final native long SEQUENCE_note_time_get(long j);

    public static final native void SEQUENCE_note_time_set(long j, long j2);

    public static final native long SEQUENCE_prefix_callback_get(long j);

    public static final native void SEQUENCE_prefix_callback_set(long j, long j2);

    public static final native long SEQUENCE_shadow_get(long j);

    public static final native void SEQUENCE_shadow_set(long j, long j2);

    public static final native long SEQUENCE_status_get(long j);

    public static final native void SEQUENCE_status_set(long j, long j2);

    public static final native long SEQUENCE_system_data_get(long j);

    public static final native void SEQUENCE_system_data_set(long j, long j2);

    public static final native String SEQUENCE_tag_get(long j);

    public static final native void SEQUENCE_tag_set(long j, String str);

    public static final native int SEQUENCE_tempo_accum_get(long j);

    public static final native void SEQUENCE_tempo_accum_set(long j, int i);

    public static final native int SEQUENCE_tempo_error_get(long j);

    public static final native void SEQUENCE_tempo_error_set(long j, int i);

    public static final native int SEQUENCE_tempo_percent_get(long j);

    public static final native void SEQUENCE_tempo_percent_set(long j, int i);

    public static final native int SEQUENCE_tempo_period_get(long j);

    public static final native void SEQUENCE_tempo_period_set(long j, int i);

    public static final native int SEQUENCE_tempo_target_get(long j);

    public static final native void SEQUENCE_tempo_target_set(long j, int i);

    public static final native int SEQUENCE_time_fraction_get(long j);

    public static final native void SEQUENCE_time_fraction_set(long j, int i);

    public static final native int SEQUENCE_time_numerator_get(long j);

    public static final native void SEQUENCE_time_numerator_set(long j, int i);

    public static final native int SEQUENCE_time_per_beat_get(long j);

    public static final native void SEQUENCE_time_per_beat_set(long j, int i);

    public static final native long SEQUENCE_trigger_callback_get(long j);

    public static final native void SEQUENCE_trigger_callback_set(long j, long j2);

    public static final native long SEQUENCE_user_data_get(long j);

    public static final native void SEQUENCE_user_data_set(long j, long j2);

    public static final native int SEQUENCE_volume_accum_get(long j);

    public static final native void SEQUENCE_volume_accum_set(long j, int i);

    public static final native int SEQUENCE_volume_get(long j);

    public static final native int SEQUENCE_volume_period_get(long j);

    public static final native void SEQUENCE_volume_period_set(long j, int i);

    public static final native void SEQUENCE_volume_set(long j, int i);

    public static final native int SEQUENCE_volume_target_get(long j);

    public static final native void SEQUENCE_volume_target_set(long j, int i);

    public static final native float SMPATTRIBS_BL_51_level_get(long j);

    public static final native void SMPATTRIBS_BL_51_level_set(long j, float f);

    public static final native float SMPATTRIBS_BR_51_level_get(long j);

    public static final native void SMPATTRIBS_BR_51_level_set(long j, float f);

    public static final native float SMPATTRIBS_C_51_level_get(long j);

    public static final native void SMPATTRIBS_C_51_level_set(long j, float f);

    public static final native float SMPATTRIBS_FL_51_level_get(long j);

    public static final native void SMPATTRIBS_FL_51_level_set(long j, float f);

    public static final native float SMPATTRIBS_FR_51_level_get(long j);

    public static final native void SMPATTRIBS_FR_51_level_set(long j, float f);

    public static final native float SMPATTRIBS_LFE_51_level_get(long j);

    public static final native void SMPATTRIBS_LFE_51_level_set(long j, float f);

    public static final native float SMPATTRIBS_LPF_cutoff_get(long j);

    public static final native void SMPATTRIBS_LPF_cutoff_set(long j, float f);

    public static final native float SMPATTRIBS_X_face_get(long j);

    public static final native void SMPATTRIBS_X_face_set(long j, float f);

    public static final native float SMPATTRIBS_X_meters_per_ms_get(long j);

    public static final native void SMPATTRIBS_X_meters_per_ms_set(long j, float f);

    public static final native float SMPATTRIBS_X_pos_get(long j);

    public static final native void SMPATTRIBS_X_pos_set(long j, float f);

    public static final native float SMPATTRIBS_X_up_get(long j);

    public static final native void SMPATTRIBS_X_up_set(long j, float f);

    public static final native float SMPATTRIBS_Y_face_get(long j);

    public static final native void SMPATTRIBS_Y_face_set(long j, float f);

    public static final native float SMPATTRIBS_Y_meters_per_ms_get(long j);

    public static final native void SMPATTRIBS_Y_meters_per_ms_set(long j, float f);

    public static final native float SMPATTRIBS_Y_pos_get(long j);

    public static final native void SMPATTRIBS_Y_pos_set(long j, float f);

    public static final native float SMPATTRIBS_Y_up_get(long j);

    public static final native void SMPATTRIBS_Y_up_set(long j, float f);

    public static final native float SMPATTRIBS_Z_face_get(long j);

    public static final native void SMPATTRIBS_Z_face_set(long j, float f);

    public static final native float SMPATTRIBS_Z_meters_per_ms_get(long j);

    public static final native void SMPATTRIBS_Z_meters_per_ms_set(long j, float f);

    public static final native float SMPATTRIBS_Z_pos_get(long j);

    public static final native void SMPATTRIBS_Z_pos_set(long j, float f);

    public static final native float SMPATTRIBS_Z_up_get(long j);

    public static final native void SMPATTRIBS_Z_up_set(long j, float f);

    public static final native int SMPATTRIBS_auto_3D_wet_atten_get(long j);

    public static final native void SMPATTRIBS_auto_3D_wet_atten_set(long j, int i);

    public static final native long SMPATTRIBS_channel_levels_get(long j);

    public static final native void SMPATTRIBS_channel_levels_set(long j, long j2);

    public static final native float SMPATTRIBS_cone_inner_angle_get(long j);

    public static final native void SMPATTRIBS_cone_inner_angle_set(long j, float f);

    public static final native float SMPATTRIBS_cone_outer_angle_get(long j);

    public static final native void SMPATTRIBS_cone_outer_angle_set(long j, float f);

    public static final native float SMPATTRIBS_cone_outer_volume_get(long j);

    public static final native void SMPATTRIBS_cone_outer_volume_set(long j, float f);

    public static final native float SMPATTRIBS_dry_level_get(long j);

    public static final native void SMPATTRIBS_dry_level_set(long j, float f);

    public static final native float SMPATTRIBS_exclusion_get(long j);

    public static final native void SMPATTRIBS_exclusion_set(long j, float f);

    public static final native long SMPATTRIBS_filter_property_tokens_get(long j);

    public static final native void SMPATTRIBS_filter_property_tokens_set(long j, long j2);

    public static final native long SMPATTRIBS_filter_property_values_get(long j);

    public static final native void SMPATTRIBS_filter_property_values_set(long j, long j2);

    public static final native long SMPATTRIBS_filter_providers_get(long j);

    public static final native void SMPATTRIBS_filter_providers_set(long j, long j2);

    public static final native long SMPATTRIBS_filter_stages_get(long j);

    public static final native void SMPATTRIBS_filter_stages_set(long j, long j2);

    public static final native int SMPATTRIBS_is_3D_get(long j);

    public static final native void SMPATTRIBS_is_3D_set(long j, int i);

    public static final native float SMPATTRIBS_left_volume_get(long j);

    public static final native void SMPATTRIBS_left_volume_set(long j, float f);

    public static final native int SMPATTRIBS_loop_count_get(long j);

    public static final native void SMPATTRIBS_loop_count_set(long j, int i);

    public static final native int SMPATTRIBS_loop_end_offset_get(long j);

    public static final native void SMPATTRIBS_loop_end_offset_set(long j, int i);

    public static final native int SMPATTRIBS_loop_start_offset_get(long j);

    public static final native void SMPATTRIBS_loop_start_offset_set(long j, int i);

    public static final native float SMPATTRIBS_max_distance_get(long j);

    public static final native void SMPATTRIBS_max_distance_set(long j, float f);

    public static final native float SMPATTRIBS_min_distance_get(long j);

    public static final native void SMPATTRIBS_min_distance_set(long j, float f);

    public static final native int SMPATTRIBS_n_channel_levels_get(long j);

    public static final native void SMPATTRIBS_n_channel_levels_set(long j, int i);

    public static final native int SMPATTRIBS_n_filter_properties_get(long j);

    public static final native void SMPATTRIBS_n_filter_properties_set(long j, int i);

    public static final native float SMPATTRIBS_obstruction_get(long j);

    public static final native void SMPATTRIBS_obstruction_set(long j, float f);

    public static final native float SMPATTRIBS_occlusion_get(long j);

    public static final native void SMPATTRIBS_occlusion_set(long j, float f);

    public static final native int SMPATTRIBS_playback_rate_get(long j);

    public static final native void SMPATTRIBS_playback_rate_set(long j, int i);

    public static final native long SMPATTRIBS_playback_status_get(long j);

    public static final native void SMPATTRIBS_playback_status_set(long j, long j2);

    public static final native long SMPATTRIBS_position_get(long j);

    public static final native void SMPATTRIBS_position_set(long j, long j2);

    public static final native float SMPATTRIBS_right_volume_get(long j);

    public static final native void SMPATTRIBS_right_volume_set(long j, float f);

    public static final native float SMPATTRIBS_wet_level_get(long j);

    public static final native void SMPATTRIBS_wet_level_set(long j, float f);

    public static final native long SMPBUF_done_get(long j);

    public static final native void SMPBUF_done_set(long j, long j2);

    public static final native long SMPBUF_len_get(long j);

    public static final native void SMPBUF_len_set(long j, long j2);

    public static final native long SMPBUF_pos_get(long j);

    public static final native void SMPBUF_pos_set(long j, long j2);

    public static final native int SMPBUF_reset_ASI_get(long j);

    public static final native void SMPBUF_reset_ASI_set(long j, int i);

    public static final native int SMPBUF_reset_seek_pos_get(long j);

    public static final native void SMPBUF_reset_seek_pos_set(long j, int i);

    public static final native Buffer SMPBUF_start_get(long j);

    public static final native void SMPBUF_start_set(long j, Buffer buffer);

    public static final native long SOUNDLIB_B_get(long j);

    public static final native void SOUNDLIB_B_set(long j, long j2);

    public static final native long SOUNDLIB_next_get(long j);

    public static final native void SOUNDLIB_next_set(long j, long j2);

    public static final native long SPINFO_TYPE_ASI_get(long j);

    public static final native void SPINFO_TYPE_ASI_set(long j, long j2);

    public static final native long SPINFO_TYPE_FLT_get(long j);

    public static final native void SPINFO_TYPE_FLT_set(long j, long j2);

    public static final native long SPINFO_TYPE_MSS_mixer_merge_get(long j);

    public static final native void SPINFO_TYPE_MSS_mixer_merge_set(long j, long j2);

    public static final native long SPINFO_TYPE_get(long j);

    public static final native int SPINFO_active_get(long j);

    public static final native void SPINFO_active_set(long j, int i);

    public static final native long SPINFO_provider_get(long j);

    public static final native void SPINFO_provider_set(long j, long j2);

    public static final native int SP_ASI_DECODER_get();

    public static final native int SP_FILTER_0_get();

    public static final native int SP_OUTPUT_get();

    public static final native long STAGE_BUFFER_data_get(long j);

    public static final native void STAGE_BUFFER_data_set(long j, long j2);

    public static final native long STAGE_BUFFER_next_get(long j);

    public static final native void STAGE_BUFFER_next_set(long j, long j2);

    public static final native short TIMB_chunk_lsb2_get(long j);

    public static final native void TIMB_chunk_lsb2_set(long j, short s);

    public static final native short TIMB_chunk_lsb3_get(long j);

    public static final native void TIMB_chunk_lsb3_set(long j, short s);

    public static final native short TIMB_chunk_lsb_get(long j);

    public static final native void TIMB_chunk_lsb_set(long j, short s);

    public static final native short TIMB_chunk_msb_get(long j);

    public static final native void TIMB_chunk_msb_set(long j, short s);

    public static final native int TIMB_chunk_n_entries_get(long j);

    public static final native void TIMB_chunk_n_entries_set(long j, int i);

    public static final native long TIMB_chunk_name_get(long j);

    public static final native void TIMB_chunk_name_set(long j, long j2);

    public static final native long TIMB_chunk_timbre_get(long j);

    public static final native void TIMB_chunk_timbre_set(long j, long j2);

    public static final native void VOIDFUNC();

    public static final native int WAVE_ENTRY_bank_get(long j);

    public static final native void WAVE_ENTRY_bank_set(long j, int i);

    public static final native long WAVE_ENTRY_file_offset_get(long j);

    public static final native void WAVE_ENTRY_file_offset_set(long j, long j2);

    public static final native long WAVE_ENTRY_flags_get(long j);

    public static final native void WAVE_ENTRY_flags_set(long j, long j2);

    public static final native int WAVE_ENTRY_format_get(long j);

    public static final native void WAVE_ENTRY_format_set(long j, int i);

    public static final native int WAVE_ENTRY_patch_get(long j);

    public static final native void WAVE_ENTRY_patch_set(long j, int i);

    public static final native int WAVE_ENTRY_playback_rate_get(long j);

    public static final native void WAVE_ENTRY_playback_rate_set(long j, int i);

    public static final native int WAVE_ENTRY_root_key_get(long j);

    public static final native void WAVE_ENTRY_root_key_set(long j, int i);

    public static final native long WAVE_ENTRY_size_get(long j);

    public static final native void WAVE_ENTRY_size_set(long j, long j2);

    public static final native long WAVE_SYNTH_S_get(long j);

    public static final native void WAVE_SYNTH_S_set(long j, long j2);

    public static final native long WAVE_SYNTH_chan_get(long j);

    public static final native void WAVE_SYNTH_chan_set(long j, long j2);

    public static final native long WAVE_SYNTH_controls_get(long j);

    public static final native void WAVE_SYNTH_controls_set(long j, long j2);

    public static final native long WAVE_SYNTH_dig_get(long j);

    public static final native void WAVE_SYNTH_dig_set(long j, long j2);

    public static final native long WAVE_SYNTH_event_get(long j);

    public static final native void WAVE_SYNTH_event_set(long j, long j2);

    public static final native long WAVE_SYNTH_library_get(long j);

    public static final native void WAVE_SYNTH_library_set(long j, long j2);

    public static final native long WAVE_SYNTH_mdi_get(long j);

    public static final native void WAVE_SYNTH_mdi_set(long j, long j2);

    public static final native int WAVE_SYNTH_n_voices_get(long j);

    public static final native void WAVE_SYNTH_n_voices_set(long j, int i);

    public static final native long WAVE_SYNTH_note_get(long j);

    public static final native void WAVE_SYNTH_note_set(long j, long j2);

    public static final native long WAVE_SYNTH_prev_event_fn_get(long j);

    public static final native void WAVE_SYNTH_prev_event_fn_set(long j, long j2);

    public static final native long WAVE_SYNTH_prev_timb_fn_get(long j);

    public static final native void WAVE_SYNTH_prev_timb_fn_set(long j, long j2);

    public static final native long WAVE_SYNTH_rate_get(long j);

    public static final native void WAVE_SYNTH_rate_set(long j, long j2);

    public static final native long WAVE_SYNTH_root_get(long j);

    public static final native void WAVE_SYNTH_root_set(long j, long j2);

    public static final native long WAVE_SYNTH_time_get(long j);

    public static final native void WAVE_SYNTH_time_set(long j, long j2);

    public static final native long WAVE_SYNTH_vel_get(long j);

    public static final native void WAVE_SYNTH_vel_set(long j, long j2);

    public static final native long WAVE_SYNTH_wave_get(long j);

    public static final native void WAVE_SYNTH_wave_set(long j, long j2);

    public static final native void delete_ADPCMDATA(long j);

    public static final native void delete_AILDLSINFO(long j);

    public static final native void delete_AILMIXINFO(long j);

    public static final native void delete_AILSOUNDINFO(long j);

    public static final native void delete_AILTIMERSTR(long j);

    public static final native void delete_AIL_INPUT_INFO(long j);

    public static final native void delete_ASISTAGE(long j);

    public static final native void delete_AUDIO_TYPE(long j);

    public static final native void delete_CTRL_LOG(long j);

    public static final native void delete_D3DSTATE(long j);

    public static final native void delete_DIG_DRIVER(long j);

    public static final native void delete_DIG_INPUT_DRIVER(long j);

    public static final native void delete_DLSDEVICE(long j);

    public static final native void delete_DLSFILEID(long j);

    public static final native void delete_DPINFO(long j);

    public static final native void delete_DPINFO_TYPE(long j);

    public static final native void delete_EAX_AUTOGAIN(long j);

    public static final native void delete_EAX_AUTOWAH(long j);

    public static final native void delete_EAX_CHORUS(long j);

    public static final native void delete_EAX_DISTORTION(long j);

    public static final native void delete_EAX_ECHO(long j);

    public static final native void delete_EAX_EQUALIZER(long j);

    public static final native void delete_EAX_FLANGER(long j);

    public static final native void delete_EAX_FSHIFTER(long j);

    public static final native void delete_EAX_PSHIFTER(long j);

    public static final native void delete_EAX_REVERB(long j);

    public static final native void delete_EAX_RMODULATOR(long j);

    public static final native void delete_EAX_SAMPLE_SLOT_VOLUME(long j);

    public static final native void delete_EAX_SAMPLE_SLOT_VOLUMES(long j);

    public static final native void delete_EAX_VMORPHER(long j);

    public static final native void delete_FLTPROVIDER(long j);

    public static final native void delete_FLTSTAGE(long j);

    public static final native void delete_LOWPASS_CONSTANT_INFO(long j);

    public static final native void delete_LOWPASS_INFO(long j);

    public static final native void delete_LOWPASS_UPDATED_INFO(long j);

    public static final native void delete_MDI_DRIVER(long j);

    public static final native void delete_MP3_INFO(long j);

    public static final native void delete_MPCMWAVEFORMAT(long j);

    public static final native void delete_MSSVECTOR3D(long j);

    public static final native void delete_MSS_BB(long j);

    public static final native void delete_MSS_RECEIVER_LIST(long j);

    public static final native void delete_MSTREAM_TYPE(long j);

    public static final native void delete_MWAVEFORMAT(long j);

    public static final native void delete_MWAVEFORMATEX(long j);

    public static final native void delete_MWAVEFORMATEXTENSIBLE(long j);

    public static final native void delete_MWAVEFORMATEXTENSIBLE_Samples(long j);

    public static final native void delete_MWAVEHDR(long j);

    public static final native void delete_RBRN_entry(long j);

    public static final native void delete_REDBOOK(long j);

    public static final native void delete_REVERB_CONSTANT_INFO(long j);

    public static final native void delete_REVERB_INFO(long j);

    public static final native void delete_REVERB_UPDATED_INFO(long j);

    public static final native void delete_RIB_INTERFACE_ENTRY(long j);

    public static final native void delete_S3DSTATE(long j);

    public static final native void delete_SAMPLE(long j);

    public static final native void delete_SEQUENCE(long j);

    public static final native void delete_SMPATTRIBS(long j);

    public static final native void delete_SMPBUF(long j);

    public static final native void delete_SOUNDLIB(long j);

    public static final native void delete_SPINFO(long j);

    public static final native void delete_SPINFO_TYPE(long j);

    public static final native void delete_STAGE_BUFFER(long j);

    public static final native void delete_TIMB_chunk(long j);

    public static final native void delete_WAVE_ENTRY(long j);

    public static final native void delete_WAVE_SYNTH(long j);

    public static final native long new_ADPCMDATA();

    public static final native long new_AILDLSINFO();

    public static final native long new_AILMIXINFO();

    public static final native long new_AILSOUNDINFO();

    public static final native long new_AILTIMERSTR();

    public static final native long new_AIL_INPUT_INFO();

    public static final native long new_ASISTAGE();

    public static final native long new_AUDIO_TYPE();

    public static final native long new_CTRL_LOG();

    public static final native long new_D3DSTATE();

    public static final native long new_DIG_DRIVER();

    public static final native long new_DIG_INPUT_DRIVER();

    public static final native long new_DLSDEVICE();

    public static final native long new_DLSFILEID();

    public static final native long new_DPINFO();

    public static final native long new_DPINFO_TYPE();

    public static final native long new_EAX_AUTOGAIN();

    public static final native long new_EAX_AUTOWAH();

    public static final native long new_EAX_CHORUS();

    public static final native long new_EAX_DISTORTION();

    public static final native long new_EAX_ECHO();

    public static final native long new_EAX_EQUALIZER();

    public static final native long new_EAX_FLANGER();

    public static final native long new_EAX_FSHIFTER();

    public static final native long new_EAX_PSHIFTER();

    public static final native long new_EAX_REVERB();

    public static final native long new_EAX_RMODULATOR();

    public static final native long new_EAX_SAMPLE_SLOT_VOLUME();

    public static final native long new_EAX_SAMPLE_SLOT_VOLUMES();

    public static final native long new_EAX_VMORPHER();

    public static final native long new_FLTPROVIDER();

    public static final native long new_FLTSTAGE();

    public static final native long new_LOWPASS_CONSTANT_INFO();

    public static final native long new_LOWPASS_INFO();

    public static final native long new_LOWPASS_UPDATED_INFO();

    public static final native long new_MDI_DRIVER();

    public static final native long new_MP3_INFO();

    public static final native long new_MPCMWAVEFORMAT();

    public static final native long new_MSSVECTOR3D();

    public static final native long new_MSS_BB();

    public static final native long new_MSS_RECEIVER_LIST();

    public static final native long new_MSTREAM_TYPE();

    public static final native long new_MWAVEFORMAT();

    public static final native long new_MWAVEFORMATEX();

    public static final native long new_MWAVEFORMATEXTENSIBLE();

    public static final native long new_MWAVEFORMATEXTENSIBLE_Samples();

    public static final native long new_MWAVEHDR();

    public static final native long new_RBRN_entry();

    public static final native long new_REDBOOK();

    public static final native long new_REVERB_CONSTANT_INFO();

    public static final native long new_REVERB_INFO();

    public static final native long new_REVERB_UPDATED_INFO();

    public static final native long new_RIB_INTERFACE_ENTRY();

    public static final native long new_S3DSTATE();

    public static final native long new_SAMPLE();

    public static final native long new_SEQUENCE();

    public static final native long new_SMPATTRIBS();

    public static final native long new_SMPBUF();

    public static final native long new_SOUNDLIB();

    public static final native long new_SPINFO();

    public static final native long new_SPINFO_TYPE();

    public static final native long new_STAGE_BUFFER();

    public static final native long new_TIMB_chunk();

    public static final native long new_WAVE_ENTRY();

    public static final native long new_WAVE_SYNTH();
}
