package utaikodom.render.granny;

import java.nio.Buffer;

/* compiled from: a */
public class grannyJNI {
    grannyJNI() {
    }

    public static final native long GrannyARGB8888PixelFormat_get();

    public static final native void GrannyARGB8888PixelFormat_set(long j);

    public static final native void GrannyARGB8888SwizzleNGC(long j, long j2, long j3, long j4, long j5);

    public static final native void GrannyAbortCurveBuilder(long j);

    public static final native void GrannyAbortFile(long j);

    public static final native void GrannyAbortVariant(long j);

    public static final native void GrannyAccumulateLocalTransform(long j, int i, int i2, float f, long j2, int i3, long j3);

    public static final native void GrannyAccumulateModelAnimations(long j, int i, int i2, long j2);

    public static final native void GrannyAccumulateModelAnimationsLOD(long j, int i, int i2, long j2, float f);

    public static final native void GrannyAccumulateModelAnimationsLODSparse(long j, int i, int i2, long j2, float f, long j3);

    public static final native int GrannyAccumulationExtracted_get();

    public static final native int GrannyAccumulationIsVDA_get();

    public static final native long GrannyAcquireAnimationBinding(long j);

    public static final native long GrannyAcquireAnimationBindingFromID(long j);

    public static final native void GrannyAcquireMemorySpinlock();

    public static final native void GrannyAddBlendDagNodeChild(long j, long j2);

    public static final native void GrannyAddBone(long j, long j2, long j3, long j4, long j5, long j6, long j7);

    public static final native void GrannyAddBoolMember(long j, String str, int i);

    public static final native void GrannyAddDynamicArrayMember(long j, String str, int i, long j2, long j3);

    public static final native void GrannyAddIntegerArrayMember(long j, String str, int i, long j2);

    public static final native void GrannyAddIntegerMember(long j, String str, int i);

    public static final native void GrannyAddPointerToHash(long j, long j2, long j3);

    public static final native void GrannyAddReferenceMember(long j, String str, long j2, long j3);

    public static final native void GrannyAddScalarArrayMember(long j, String str, int i, long j2);

    public static final native void GrannyAddScalarMember(long j, String str, float f);

    public static final native void GrannyAddStringMember(long j, String str, String str2);

    public static final native void GrannyAddTextEntry(long j, float f, String str);

    public static final native void GrannyAddToCRC32(long j, long j2, long j3);

    public static final native void GrannyAddUnsignedIntegerMember(long j, String str, long j2);

    public static final native void GrannyAddWeight(long j, int i, float f);

    public static final native void GrannyAdjustFileFixup(long j, long j2, long j3);

    public static final native int GrannyAlignWriter(long j);

    public static final native void GrannyAll16SwizzleNGC(long j, long j2, long j3, long j4, long j5);

    public static final native int GrannyAllDOFs_get();

    public static final native long GrannyAllocateBSplineSolver(int i, int i2, int i3);

    public static final native long GrannyAllocateFixed(long j);

    public static final native void GrannyAllocateLODErrorSpace(long j);

    public static final native long GrannyAllocationsBegin();

    public static final native long GrannyAllocationsEnd();

    public static final native long GrannyAnimationType_get();

    public static final native void GrannyAnimationType_set(long j);

    public static final native int GrannyAnyMarshalling_get();

    public static final native void GrannyApplyRootMotionVectorsToLocalPose(long j, long j2, long j3);

    public static final native void GrannyApplyRootMotionVectorsToMatrix(Buffer buffer, long j, long j2, Buffer buffer2);

    public static final native long GrannyArtToolInfoType_get();

    public static final native void GrannyArtToolInfoType_set(long j);

    public static final native long GrannyBGR555PixelFormat_get();

    public static final native void GrannyBGR555PixelFormat_set(long j);

    public static final native long GrannyBGR565PixelFormat_get();

    public static final native void GrannyBGR565PixelFormat_set(long j);

    public static final native long GrannyBGR888PixelFormat_get();

    public static final native void GrannyBGR888PixelFormat_set(long j);

    public static final native long GrannyBGRA4444PixelFormat_get();

    public static final native void GrannyBGRA4444PixelFormat_set(long j);

    public static final native long GrannyBGRA5551PixelFormat_get();

    public static final native void GrannyBGRA5551PixelFormat_set(long j);

    public static final native long GrannyBGRA8888PixelFormat_get();

    public static final native void GrannyBGRA8888PixelFormat_set(long j);

    public static final native int GrannyBSplineSolverAllowC0Splitting_get();

    public static final native int GrannyBSplineSolverAllowC1Splitting_get();

    public static final native int GrannyBSplineSolverAllowReduceKeys_get();

    public static final native int GrannyBSplineSolverEvaluateAsQuaternions_get();

    public static final native int GrannyBSplineSolverExtraDOFKnotZero_get();

    public static final native int GrannyBSplineSolverForceEndpointAlignment_get();

    public static final native boolean GrannyBasisConversionRequiresCurveDecompression(long j, long j2, long j3, float f, float f2, boolean z);

    public static final native long GrannyBeginAllocationCheck();

    public static final native long GrannyBeginBestMatchS3TCTexture(int i, int i2);

    public static final native long GrannyBeginBinkTexture(int i, int i2, int i3, long j);

    public static final native void GrannyBeginCRC32(long j);

    public static final native long GrannyBeginControlledAnimation(float f, long j);

    public static final native long GrannyBeginCurve(long j, int i, int i2, int i3);

    public static final native long GrannyBeginCurveCopy(long j);

    public static final native long GrannyBeginFile(int i, long j, long j2, String str, String str2);

    public static final native long GrannyBeginFileCompression(long j, int i, int i2, boolean z, long j2);

    public static final native long GrannyBeginFileDataTreeWriting(long j, long j2, int i, int i2);

    public static final native long GrannyBeginFileInMemory(int i, long j, long j2, int i2);

    public static final native void GrannyBeginLocalPoseAccumulation(long j, int i, int i2, long j2);

    public static final native long GrannyBeginMesh(int i, int i2, int i3, int i4, long j);

    public static final native long GrannyBeginRawTexture(int i, int i2, long j, int i3);

    public static final native long GrannyBeginS3TCTexture(int i, int i2, int i3);

    public static final native long GrannyBeginSampledAnimation(int i, int i2);

    public static final native long GrannyBeginSampledAnimationNonBlocked(int i, int i2);

    public static final native long GrannyBeginSkeleton(int i);

    public static final native void GrannyBeginTextTrack(long j, String str);

    public static final native long GrannyBeginTrackGroup(String str, int i, int i2, int i3, boolean z);

    public static final native void GrannyBeginTransformTrack(long j, String str, int i);

    public static final native long GrannyBeginVariant(long j);

    public static final native void GrannyBindTrackmaskToModel(long j, long j2, long j3);

    public static final native void GrannyBindTrackmaskToTrackGroup(long j, long j2, long j3);

    public static final native int GrannyBinkCompressTexture(int i, int i2, int i3, long j, long j2, int i4, long j3);

    public static final native void GrannyBinkDecompressTexture(int i, int i2, long j, int i3, long j2, long j3, int i4, long j4);

    public static final native int GrannyBinkEncodeAlpha_get();

    public static final native int GrannyBinkUseBink1_get();

    public static final native int GrannyBinkUseScaledRGBInsteadOfYUV_get();

    public static final native void GrannyBlendDagFreeCompletedControlsEntireTree(long j);

    public static final native void GrannyBlendDagNodeAnimationBlendFreeCompletedControls(long j);

    public static final native int GrannyBlendQuaternionAccumNeighborhooded_get();

    public static final native int GrannyBlendQuaternionDirectly_get();

    public static final native int GrannyBlendQuaternionInverted_get();

    public static final native int GrannyBlendQuaternionNeighborhooded_get();

    public static final native long GrannyBoneBindingType_get();

    public static final native void GrannyBoneBindingType_set(long j);

    public static final native long GrannyBoneType_get();

    public static final native void GrannyBoneType_set(long j);

    public static final native int GrannyBool32Member_get();

    public static final native int GrannyBoundOrientationCurveFlagMask_get();

    public static final native int GrannyBoundOrientationCurveIsConstant_get();

    public static final native int GrannyBoundOrientationCurveIsGeneral_get();

    public static final native int GrannyBoundOrientationCurveIsIdentity_get();

    public static final native int GrannyBoundOrientationCurveIsKeyframed_get();

    public static final native int GrannyBoundPositionCurveFlagMask_get();

    public static final native int GrannyBoundPositionCurveIsConstant_get();

    public static final native int GrannyBoundPositionCurveIsGeneral_get();

    public static final native int GrannyBoundPositionCurveIsIdentity_get();

    public static final native int GrannyBoundPositionCurveIsKeyframed_get();

    public static final native int GrannyBoundScaleShearCurveFlagMask_get();

    public static final native int GrannyBoundScaleShearCurveIsConstant_get();

    public static final native int GrannyBoundScaleShearCurveIsGeneral_get();

    public static final native int GrannyBoundScaleShearCurveIsIdentity_get();

    public static final native int GrannyBoundScaleShearCurveIsKeyframed_get();

    public static final native void GrannyBuildCameraMatrices(long j);

    public static final native void GrannyBuildCompositeBuffer(long j, int i, int i2, long j2, long j3);

    public static final native void GrannyBuildCompositeBufferTransposed(long j, int i, int i2, long j2, long j3);

    public static final native void GrannyBuildCompositeTransform(long j, int i, long j2);

    public static final native void GrannyBuildCompositeTransform4x3(long j, long j2);

    public static final native void GrannyBuildCompositeTransform4x4(long j, Buffer buffer);

    public static final native void GrannyBuildIndexedCompositeBuffer(long j, long j2, long j3, int i, long j4);

    public static final native void GrannyBuildIndexedCompositeBufferTransposed(long j, long j2, long j3, int i, long j4);

    public static final native void GrannyBuildInverse(long j, long j2);

    public static final native void GrannyBuildMeshBinding4x4Array(long j, long j2, int i, int i2, Buffer buffer);

    public static final native void GrannyBuildRestLocalPose(long j, int i, int i2, long j2);

    public static final native void GrannyBuildRestWorldPose(long j, int i, int i2, Buffer buffer, long j2);

    public static final native void GrannyBuildSkeletonRelativeTransform(int i, long j, int i2, long j2, int i3, long j3);

    public static final native void GrannyBuildSkeletonRelativeTransforms(int i, long j, int i2, long j2, int i3, int i4, long j3);

    public static final native boolean GrannyBuildTangentSpace(long j);

    public static final native void GrannyBuildWorldPose(long j, int i, int i2, long j2, Buffer buffer, long j3);

    public static final native void GrannyBuildWorldPoseComposites(long j, int i, int i2, long j2);

    public static final native void GrannyBuildWorldPoseLOD(long j, int i, int i2, int i3, int i4, long j2, Buffer buffer, long j3);

    public static final native void GrannyBuildWorldPoseNoComposite(long j, int i, int i2, long j2, Buffer buffer, long j3);

    public static final native void GrannyBuildWorldPoseNoCompositeLOD(long j, int i, int i2, int i3, int i4, long j2, Buffer buffer, long j3);

    public static final native void GrannyBuildWorldPoseNoCompositeSparse(long j, int i, int i2, long j2, long j3, long j4, Buffer buffer, long j5);

    public static final native void GrannyBuildWorldPoseSparse(long j, int i, int i2, long j2, long j3, long j4, Buffer buffer, long j5);

    public static final native void GrannyCalculateAnimationLODAddMeshBinding(long j, long j2, long j3, float f);

    public static final native long GrannyCalculateAnimationLODBegin(long j, float f);

    public static final native void GrannyCalculateAnimationLODCleanup(long j);

    public static final native void GrannyCalculateAnimationLODEnd(long j);

    public static final native void GrannyCalculateLODErrorValues(long j, long j2, boolean z, float f);

    public static final native void GrannyCalculateLODErrorValuesAllBindings(long j, long j2, boolean z, float f);

    public static final native long GrannyCaptureCurrentStats(int i);

    public static final native long GrannyCheckedAllocationsEnd(long j);

    public static final native void GrannyClearArena(long j);

    public static final native void GrannyClearBlendDagNodeChildren(long j);

    public static final native void GrannyClearMostSeriousMessage();

    public static final native boolean GrannyClipAngularVelocityDOFs(long j, long j2);

    public static final native boolean GrannyClipOrientationDOFs(long j, long j2);

    public static final native boolean GrannyClipPositionDOFs(long j, long j2);

    public static final native void GrannyClipRootMotionVectors(long j, long j2, long j3, long j4, long j5, long j6, long j7);

    public static final native void GrannyClipTransformDOFs(long j, long j2);

    public static final native void GrannyColumnMatrixMultiply4x3(long j, long j2, long j3);

    public static final native void GrannyColumnMatrixMultiply4x3Transpose(long j, long j2, long j3);

    public static final native void GrannyColumnMatrixMultiply4x4(long j, long j2, long j3);

    public static final native void GrannyCompleteControlAt(long j, float f);

    public static final native boolean GrannyCompressContentsOfFile(long j, int i, String str, int i2, long j2);

    public static final native boolean GrannyCompressContentsOfMemory(long j, int i, long j2);

    public static final native boolean GrannyCompressContentsOfReader(long j, int i, long j2, int i2, long j3);

    public static final native long GrannyCompressCurve(long j, long j2, long j3, long j4, int i, int i2, float f, long j5);

    public static final native boolean GrannyComputeBasisConversion(long j, float f, long j2, long j3, long j4, long j5, long j6, long j7, long j8);

    public static final native void GrannyComputePeriodicLoopLog(long j, float f, long j2);

    public static final native void GrannyComputePeriodicLoopVector(long j, float f, long j2);

    public static final native boolean GrannyConstructBSplineBuffers(int i, long j, long j2, long j3, float f, float f2, float f3, int i2, long j4, long j5, long j6, long j7, long j8);

    public static final native boolean GrannyControlIsActive(long j);

    public static final native boolean GrannyControlIsComplete(long j);

    public static final native long GrannyControlModelsBegin(long j);

    public static final native long GrannyControlModelsEnd(long j);

    public static final native long GrannyControlModelsNext(long j);

    public static final native boolean GrannyConvertFileInfoToRaw(long j, String str);

    public static final native boolean GrannyConvertFileToRaw(String str, String str2);

    public static final native void GrannyConvertIndices(int i, int i2, long j, int i3, long j2);

    public static final native void GrannyConvertPixelFormat(int i, int i2, long j, int i3, long j2, long j3, int i4, long j4);

    public static final native void GrannyConvertSingleObject(long j, long j2, long j3, long j4);

    public static final native long GrannyConvertTree(long j, long j2, long j3);

    public static final native long GrannyConvertTreeInPlace(long j, long j2, long j3, Buffer buffer);

    public static final native void GrannyConvertVertexLayouts(int i, long j, long j2, long j3, Buffer buffer);

    public static final native void GrannyCopyLODErrorValuesFromAllAnimations(long j, float f);

    public static final native void GrannyCopyLODErrorValuesFromAnimation(long j, float f);

    public static final native void GrannyCopyLODErrorValuesToAllAnimations(long j, float f, boolean z);

    public static final native void GrannyCopyLODErrorValuesToAnimation(long j, float f, boolean z);

    public static final native void GrannyCopyLocalPose(long j, long j2);

    public static final native void GrannyCopyMeshIndices(long j, int i, Buffer buffer);

    public static final native void GrannyCopyMeshMorphVertices(long j, int i, long j2, Buffer buffer);

    public static final native void GrannyCopyMeshVertices(long j, long j2, Buffer buffer);

    public static final native boolean GrannyCopyTextureImage(long j, int i, int i2, long j2, int i3, int i4, int i5, long j3);

    public static final native long GrannyCopyTrackMask(long j);

    public static final native long GrannyCreateBlendDagNodeAnimationBlend(long j, boolean z);

    public static final native long GrannyCreateBlendDagNodeCallback(long j, long j2, long j3, long j4);

    public static final native long GrannyCreateBlendDagNodeCrossfade(long j, long j2, float f, float f2, long j3, boolean z, int i);

    public static final native long GrannyCreateBlendDagNodeLocalPose(long j, boolean z);

    public static final native long GrannyCreateBlendDagNodeWeightedBlend(long j, float f, int i, int i2);

    public static final native long GrannyCreateBlendDagNodeWeightedBlendChildren(long j, float f, int i, int i2, int i3, long j2);

    public static final native long GrannyCreateControl(float f, float f2);

    public static final native long GrannyCreateDagPoseCache(int i, int i2);

    public static final native long GrannyCreateMemoryFileReader(String str, int i, int i2, int i3, Buffer buffer);

    public static final native long GrannyCreateMemoryFileWriter(String str, int i, int i2);

    public static final native long GrannyCreatePlatformFileReader(String str, int i, String str2);

    public static final native long GrannyCreatePlatformFileWriter(String str, int i, String str2, boolean z);

    public static final native long GrannyCurve2Type_get();

    public static final native void GrannyCurve2Type_set(long j);

    public static final native long GrannyCurveConvertToDaK32fC32f(long j, long j2);

    public static final native long GrannyCurveConvertToDaK32fC32fInPlace(long j, Buffer buffer, long j2);

    public static final native long GrannyCurveDataD3Constant32fType_get();

    public static final native void GrannyCurveDataD3Constant32fType_set(long j);

    public static final native long GrannyCurveDataD3I1K16uC16uType_get();

    public static final native void GrannyCurveDataD3I1K16uC16uType_set(long j);

    public static final native long GrannyCurveDataD3I1K32fC32fType_get();

    public static final native void GrannyCurveDataD3I1K32fC32fType_set(long j);

    public static final native long GrannyCurveDataD3I1K8uC8uType_get();

    public static final native void GrannyCurveDataD3I1K8uC8uType_set(long j);

    public static final native long GrannyCurveDataD3K16uC16uType_get();

    public static final native void GrannyCurveDataD3K16uC16uType_set(long j);

    public static final native long GrannyCurveDataD3K8uC8uType_get();

    public static final native void GrannyCurveDataD3K8uC8uType_set(long j);

    public static final native long GrannyCurveDataD4Constant32fType_get();

    public static final native void GrannyCurveDataD4Constant32fType_set(long j);

    public static final native long GrannyCurveDataD4nK16uC15uType_get();

    public static final native void GrannyCurveDataD4nK16uC15uType_set(long j);

    public static final native long GrannyCurveDataD4nK8uC7uType_get();

    public static final native void GrannyCurveDataD4nK8uC7uType_set(long j);

    public static final native long GrannyCurveDataD9I1K16uC16uType_get();

    public static final native void GrannyCurveDataD9I1K16uC16uType_set(long j);

    public static final native long GrannyCurveDataD9I1K8uC8uType_get();

    public static final native void GrannyCurveDataD9I1K8uC8uType_set(long j);

    public static final native long GrannyCurveDataD9I3K16uC16uType_get();

    public static final native void GrannyCurveDataD9I3K16uC16uType_set(long j);

    public static final native long GrannyCurveDataD9I3K8uC8uType_get();

    public static final native void GrannyCurveDataD9I3K8uC8uType_set(long j);

    public static final native long GrannyCurveDataDaConstant32fType_get();

    public static final native void GrannyCurveDataDaConstant32fType_set(long j);

    public static final native long GrannyCurveDataDaIdentityType_get();

    public static final native void GrannyCurveDataDaIdentityType_set(long j);

    public static final native long GrannyCurveDataDaK16uC16uType_get();

    public static final native void GrannyCurveDataDaK16uC16uType_set(long j);

    public static final native long GrannyCurveDataDaK32fC32fType_get();

    public static final native void GrannyCurveDataDaK32fC32fType_set(long j);

    public static final native long GrannyCurveDataDaK8uC8uType_get();

    public static final native void GrannyCurveDataDaK8uC8uType_set(long j);

    public static final native long GrannyCurveDataDaKeyframes32fType_get();

    public static final native void GrannyCurveDataDaKeyframes32fType_set(long j);

    public static final native long GrannyCurveDataHeaderType_get();

    public static final native void GrannyCurveDataHeaderType_set(long j);

    public static final native float GrannyCurveExtractKnotValue(long j, int i, long j2, long j3);

    public static final native void GrannyCurveExtractKnotValues(long j, int i, int i2, long j2, long j3, long j4);

    public static final native int GrannyCurveFindCloseKnot(long j, float f, int i);

    public static final native int GrannyCurveFindKnot(long j, float f);

    public static final native boolean GrannyCurveFormatIsInitializedCorrectly(long j);

    public static final native long GrannyCurveGetContentsOfDaK32fC32f(long j);

    public static final native long GrannyCurveGetDataTypeDefinition(long j);

    public static final native int GrannyCurveGetDegree(long j);

    public static final native int GrannyCurveGetDimension(long j);

    public static final native int GrannyCurveGetKnotCount(long j);

    public static final native int GrannyCurveGetSize(long j);

    public static final native long GrannyCurveIdentityNormal_get();

    public static final native void GrannyCurveIdentityNormal_set(long j);

    public static final native long GrannyCurveIdentityOrientation_get();

    public static final native void GrannyCurveIdentityOrientation_set(long j);

    public static final native long GrannyCurveIdentityPosition_get();

    public static final native void GrannyCurveIdentityPosition_set(long j);

    public static final native long GrannyCurveIdentityScaleShear_get();

    public static final native void GrannyCurveIdentityScaleShear_set(long j);

    public static final native long GrannyCurveIdentityScale_get();

    public static final native void GrannyCurveIdentityScale_set(long j);

    public static final native long GrannyCurveIdentityShear_get();

    public static final native void GrannyCurveIdentityShear_set(long j);

    public static final native void GrannyCurveInitializeFormat(long j);

    public static final native boolean GrannyCurveIsConstantNotIdentity(long j);

    public static final native boolean GrannyCurveIsConstantOrIdentity(long j);

    public static final native boolean GrannyCurveIsIdentity(long j);

    public static final native boolean GrannyCurveIsKeyframed(long j);

    public static final native boolean GrannyCurveIsReducible(long j, long j2, float f, int i, long j3, long j4);

    public static final native boolean GrannyCurveIsTypeDaK32fC32f(long j);

    public static final native void GrannyCurveMakeStaticDaK32fC32f(long j, long j2, int i, int i2, int i3, long j3, long j4);

    public static final native void GrannyCurveScaleOffsetSwizzle(long j, int i, long j2, long j3, long j4);

    public static final native void GrannyCurveSetAllKnotValues(long j, int i, int i2, long j2, long j3);

    public static final native long GrannyDataTypeBeginsWith(long j, long j2);

    public static final native boolean GrannyDataTypesAreEqual(long j, long j2);

    public static final native boolean GrannyDataTypesAreEqualWithNameCallback(long j, long j2, long j3);

    public static final native boolean GrannyDataTypesAreEqualWithNames(long j, long j2);

    public static final native void GrannyDeallocateAllFixed(long j);

    public static final native void GrannyDeallocateBSplineSolver(long j);

    public static final native void GrannyDeallocateFixed(long j, Buffer buffer);

    public static final native long GrannyDecodeGRNReference(long j, long j2);

    public static final native boolean GrannyDecompressData(int i, boolean z, int i2, long j, int i3, int i4, int i5, long j2);

    public static final native boolean GrannyDecompressDataChunk(int i, boolean z, int i2, long j, int i3, long j2);

    public static final native long GrannyDefinedTypes_get();

    public static final native void GrannyDefinedTypes_set(long j);

    public static final native int GrannyDeformPosition_get();

    public static final native void GrannyDeformVertices(long j, long j2, Buffer buffer, int i, long j3, Buffer buffer2);

    public static final native float GrannyDegreesFromOrientationTolerance(float f);

    public static final native void GrannyDeletePointerHash(long j);

    public static final native long GrannyDumpStatHUD(long j);

    public static final native long GrannyDuplicateBlendDagTree(long j, long j2, long j3, int i, boolean z);

    public static final native float GrannyEaseControlIn(long j, float f, boolean z);

    public static final native float GrannyEaseControlOut(long j, float f);

    public static final native long GrannyEmptyType_get();

    public static final native void GrannyEmptyType_set(long j);

    public static final native void GrannyEncodeImage(long j, int i, int i2, int i3, int i4, long j2);

    public static final native boolean GrannyEndAllocationCheck(long j);

    public static final native void GrannyEndCRC32(long j);

    public static final native long GrannyEndControlledAnimation(long j);

    public static final native long GrannyEndCurve(long j);

    public static final native long GrannyEndCurveDataInPlace(long j, long j2, long j3);

    public static final native long GrannyEndCurveInPlace(long j, Buffer buffer);

    public static final native boolean GrannyEndFile(long j, String str);

    public static final native boolean GrannyEndFileCompression(long j, long j2);

    public static final native void GrannyEndFileDataTreeWriting(long j);

    public static final native boolean GrannyEndFileRaw(long j, String str);

    public static final native boolean GrannyEndFileRawToWriter(long j, long j2);

    public static final native boolean GrannyEndFileToWriter(long j, long j2);

    public static final native void GrannyEndLocalPoseAccumulation(long j, int i, int i2, long j2, long j3);

    public static final native void GrannyEndLocalPoseAccumulationLOD(long j, int i, int i2, long j2, float f, long j3);

    public static final native boolean GrannyEndMesh(long j, long j2, long j3);

    public static final native void GrannyEndMeshInPlace(long j, long j2, long j3, long j4, long j5);

    public static final native void GrannyEndSampledAnimation(long j);

    public static final native long GrannyEndSkeleton(long j, long j2);

    public static final native long GrannyEndSkeletonInPlace(long j, long j2, long j3);

    public static final native void GrannyEndTextTrack(long j);

    public static final native long GrannyEndTexture(long j);

    public static final native long GrannyEndTextureInPlace(long j, Buffer buffer);

    public static final native long GrannyEndTrackGroup(long j);

    public static final native long GrannyEndTrackGroupInPlace(long j, Buffer buffer);

    public static final native void GrannyEndTransformTrack(long j);

    public static final native boolean GrannyEndVariant(long j, long j2, long j3);

    public static final native boolean GrannyEndVariantInPlace(long j, long j2, long j3, long j4, long j5);

    public static final native void GrannyEnsureExactOneNorm(long j, long j2);

    public static final native void GrannyEnsureQuaternionContinuity(int i, long j);

    public static final native int GrannyEstimatedLOD_get();

    public static final native void GrannyEvaluateCurveAtKnotIndex(int i, boolean z, boolean z2, long j, boolean z3, float f, int i2, float f2, long j2, long j3);

    public static final native void GrannyEvaluateCurveAtT(int i, boolean z, boolean z2, long j, boolean z3, float f, float f2, long j2, long j3);

    public static final native int GrannyExcludeComposites_get();

    public static final native int GrannyExcludeTypeTree_get();

    public static final native long GrannyExporterInfoType_get();

    public static final native void GrannyExporterInfoType_set(long j);

    public static final native int GrannyExtractTrackMask(long j, int i, long j2, String str, float f, boolean z);

    public static final native boolean GrannyFileCRCIsValid(String str);

    public static final native long GrannyFileInfoType_get();

    public static final native void GrannyFileInfoType_set(long j);

    public static final native void GrannyFilterAllMessages(boolean z);

    public static final native void GrannyFilterMessage(int i, boolean z);

    public static final native float GrannyFindAllowedLODError(float f, int i, float f2, float f3);

    public static final native int GrannyFindBlendDagTreeDepth(long j);

    public static final native boolean GrannyFindBoneByName(long j, String str, Buffer buffer);

    public static final native boolean GrannyFindBoneByNameLowercase(long j, String str, Buffer buffer);

    public static final native int GrannyFindCloseKnot(int i, long j, float f, int i2);

    public static final native int GrannyFindKnot(int i, long j, float f);

    public static final native int GrannyFindMaskIndexForName(long j, String str);

    public static final native boolean GrannyFindMatchingMember(long j, long j2, String str, long j3);

    public static final native boolean GrannyFindTrackByName(long j, String str, long j2);

    public static final native boolean GrannyFindTrackByRule(long j, String str, String str2, long j2);

    public static final native boolean GrannyFindTrackGroupForModel(long j, String str, long j2);

    public static final native boolean GrannyFindVectorTrackByName(long j, String str, long j2);

    public static final native boolean GrannyFindVectorTrackByRule(long j, String str, String str2, long j2);

    public static final native int GrannyFirstGRNStandardTag_get();

    public static final native int GrannyFirstGRNUserTag_get();

    public static final native long GrannyFitBSplineToSamples(long j, long j2, int i, float f, float f2, float f3, long j3, int i2, int i3, float f4, long j4, int i4, long j5, long j6);

    public static final native void GrannyFitPeriodicLoop(long j, long j2, long j3, long j4, float f, long j5);

    public static final native void GrannyFixupFileSectionPhase1(long j, int i, long j2);

    public static final native void GrannyFixupFileSectionPhase2(long j, long j2, int i, long j3);

    public static final native void GrannyFlushAllBindingsForAnimation(long j);

    public static final native void GrannyFlushAllUnusedAnimationBindings();

    public static final native void GrannyFlushAnimationBinding(long j);

    public static final native void GrannyFreeAllFileSections(long j);

    public static final native void GrannyFreeBlendDagEntireTree(long j);

    public static final native void GrannyFreeBlendDagLocalPoseCache();

    public static final native void GrannyFreeBlendDagNode(long j);

    public static final native void GrannyFreeBuilderResult(long j);

    public static final native void GrannyFreeCompletedModelControls(long j);

    public static final native void GrannyFreeControl(long j);

    public static final native boolean GrannyFreeControlIfComplete(long j);

    public static final native void GrannyFreeControlOnceUnused(long j);

    public static final native void GrannyFreeCurve(long j);

    public static final native void GrannyFreeDagPoseCache(long j);

    public static final native void GrannyFreeFile(long j);

    public static final native void GrannyFreeFileSection(long j, int i);

    public static final native void GrannyFreeLODErrorSpace(long j);

    public static final native void GrannyFreeLocalPose(long j);

    public static final native void GrannyFreeMemoryArena(long j);

    public static final native void GrannyFreeMemoryWriterBuffer(long j);

    public static final native void GrannyFreeMeshBinding(long j);

    public static final native void GrannyFreeMeshDeformer(long j);

    public static final native void GrannyFreeModelInstance(long j);

    public static final native void GrannyFreeStatHUDDump(long j);

    public static final native void GrannyFreeStats(long j);

    public static final native void GrannyFreeStringTable(long j);

    public static final native void GrannyFreeTrackMask(long j);

    public static final native void GrannyFreeWorldPose(long j);

    public static final native long GrannyGBX333VertexType_get();

    public static final native void GrannyGBX333VertexType_set(long j);

    public static final native long GrannyGRNFileMV_32Bit_BigEndian_get();

    public static final native void GrannyGRNFileMV_32Bit_BigEndian_set(long j);

    public static final native long GrannyGRNFileMV_32Bit_LittleEndian_get();

    public static final native void GrannyGRNFileMV_32Bit_LittleEndian_set(long j);

    public static final native long GrannyGRNFileMV_64Bit_BigEndian_get();

    public static final native void GrannyGRNFileMV_64Bit_BigEndian_set(long j);

    public static final native long GrannyGRNFileMV_64Bit_LittleEndian_get();

    public static final native void GrannyGRNFileMV_64Bit_LittleEndian_set(long j);

    public static final native long GrannyGRNFileMV_Old_get();

    public static final native void GrannyGRNFileMV_Old_set(long j);

    public static final native long GrannyGRNFileMV_ThisPlatform_get();

    public static final native void GrannyGRNFileMV_ThisPlatform_set(long j);

    public static final native void GrannyGRNFixUp(long j, long j2, long j3, long j4);

    public static final native void GrannyGRNMarshall(long j, long j2, long j3, long j4);

    public static final native void GrannyGenerateTangentSpaceFromUVs(long j);

    public static final native void GrannyGetAllocationInformation(long j, long j2);

    public static final native void GrannyGetAllocator(long j, long j2);

    public static final native boolean GrannyGetAllowGlobalStateChanges();

    public static final native void GrannyGetAnimationBindingCacheStatus(long j);

    public static final native long GrannyGetAnimationBindingFromControlBinding(long j);

    public static final native void GrannyGetAttachmentOffset(long j, int i, long j2, Buffer buffer, long j3, long j4, long j5);

    public static final native long GrannyGetBinkPixelLayout(boolean z);

    public static final native long GrannyGetBlendDagNodeAnimationBlend(long j);

    public static final native long GrannyGetBlendDagNodeCallbackMotionVectorsCallback(long j);

    public static final native long GrannyGetBlendDagNodeCallbackSampleCallback(long j);

    public static final native long GrannyGetBlendDagNodeCallbackSetClockCallback(long j);

    public static final native long GrannyGetBlendDagNodeCallbackUserData(long j);

    public static final native long GrannyGetBlendDagNodeChild(long j, int i);

    public static final native void GrannyGetBlendDagNodeChildren(long j, int i, long j2);

    public static final native int GrannyGetBlendDagNodeChildrenCount(long j);

    public static final native long GrannyGetBlendDagNodeCrossfadeTrackMask(long j);

    public static final native boolean GrannyGetBlendDagNodeCrossfadeWeights(long j, long j2, long j3);

    public static final native long GrannyGetBlendDagNodeLocalPose(long j);

    public static final native long GrannyGetBlendDagNodeParent(long j);

    public static final native long GrannyGetBlendDagNodeResultTrackMask(long j);

    public static final native float GrannyGetBlendDagNodeResultWeight(long j);

    public static final native int GrannyGetBlendDagNodeType(long j);

    public static final native long GrannyGetBlendDagNodeWeightedBlendSkeleton(long j);

    public static final native void GrannyGetBlendDagTreeMotionVectors(long j, float f, long j2, long j3, boolean z);

    public static final native int GrannyGetBoneCountForLOD(long j, float f);

    public static final native void GrannyGetCameraBack(long j, long j2);

    public static final native void GrannyGetCameraDown(long j, long j2);

    public static final native void GrannyGetCameraForward(long j, long j2);

    public static final native void GrannyGetCameraLeft(long j, long j2);

    public static final native void GrannyGetCameraLocation(long j, long j2);

    public static final native void GrannyGetCameraRelativePlanarBases(long j, boolean z, long j2, long j3, long j4, long j5);

    public static final native void GrannyGetCameraRight(long j, long j2);

    public static final native void GrannyGetCameraUp(long j, long j2);

    public static final native int GrannyGetChannelComponentCount(long j, int i);

    public static final native int GrannyGetCompressedBytesPaddingSize(int i);

    public static final native float GrannyGetControlClampedLocalClock(long j);

    public static final native float GrannyGetControlClock(long j);

    public static final native boolean GrannyGetControlCompletionCheckFlag(long j);

    public static final native float GrannyGetControlCompletionClock(long j);

    public static final native float GrannyGetControlDuration(long j);

    public static final native float GrannyGetControlDurationLeft(long j);

    public static final native float GrannyGetControlEaseCurveMultiplier(long j);

    public static final native float GrannyGetControlEffectiveWeight(long j);

    public static final native long GrannyGetControlFromBinding(long j);

    public static final native float GrannyGetControlLocalDuration(long j);

    public static final native int GrannyGetControlLoopCount(long j);

    public static final native int GrannyGetControlLoopIndex(long j);

    public static final native void GrannyGetControlLoopState(long j, long j2, long j3);

    public static final native float GrannyGetControlRawLocalClock(long j);

    public static final native float GrannyGetControlSpeed(long j);

    public static final native long GrannyGetControlTrackGroupModelMask(long j, long j2);

    public static final native long GrannyGetControlTrackGroupTrackMask(long j, long j2, int i);

    public static final native long GrannyGetControlUserDataArray(long j);

    public static final native float GrannyGetControlWeight(long j);

    public static final native int GrannyGetConvertedTreeSize(long j, long j2, long j3);

    public static final native int GrannyGetCounterCount();

    public static final native void GrannyGetCounterResults(int i, long j);

    public static final native double GrannyGetCounterTicksPerSecond();

    public static final native void GrannyGetDataTreeFromFile(long j, long j2);

    public static final native long GrannyGetDefaultFileReaderOpenCallback();

    public static final native long GrannyGetDefaultFileWriterOpenCallback();

    public static final native int GrannyGetDefinedTypeCount();

    public static final native long GrannyGetFileInfo(long j);

    public static final native int GrannyGetFileSectionOfLoadedObject(long j, long j2);

    public static final native long GrannyGetFileTypeTag(long j);

    public static final native long GrannyGetFirstBindingForAnimation(long j);

    public static final native long GrannyGetFirstUnusedAnimationBinding();

    public static final native long GrannyGetGRNSectionArray(long j);

    public static final native long GrannyGetGlobalControlsBegin();

    public static final native long GrannyGetGlobalControlsEnd();

    public static final native float GrannyGetGlobalLODFadingFactor();

    public static final native long GrannyGetGlobalModelInstancesBegin();

    public static final native long GrannyGetGlobalModelInstancesEnd();

    public static final native long GrannyGetGlobalNextControl(long j);

    public static final native long GrannyGetGlobalNextModelInstance(long j);

    public static final native boolean GrannyGetHashedPointerData(long j, long j2, long j3);

    public static final native long GrannyGetInMemoryFileCRC(long j);

    public static final native float GrannyGetLODErrorValue(long j, int i);

    public static final native int GrannyGetLocalPoseBoneCount(long j);

    public static final native float GrannyGetLocalPoseFillThreshold(long j);

    public static final native long GrannyGetLocalPoseFromControlBinding(long j);

    public static final native long GrannyGetLocalPoseTransform(long j, int i);

    public static final native void GrannyGetLogCallback(long j);

    public static final native String GrannyGetLogMessageOriginString(int i);

    public static final native String GrannyGetLogMessageTypeString(int i);

    public static final native boolean GrannyGetMagicValueForPlatform(int i, boolean z, long j);

    public static final native boolean GrannyGetMagicValueProperties(long j, long j2, long j3);

    public static final native long GrannyGetMaterialTextureByChannelName(long j, String str);

    public static final native long GrannyGetMaterialTextureByType(long j, int i);

    public static final native int GrannyGetMaximumAnimationBindingCount();

    public static final native int GrannyGetMaximumBinkImageSize(int i, int i2, long j, int i3);

    public static final native int GrannyGetMaximumKnotCountForSampleCount(int i, int i2);

    public static final native int GrannyGetMemberArrayWidth(long j);

    public static final native String GrannyGetMemberCTypeName(int i);

    public static final native long GrannyGetMemberMarshalling(long j);

    public static final native String GrannyGetMemberTypeName(int i);

    public static final native int GrannyGetMemberTypeSize(long j);

    public static final native int GrannyGetMemberUnitSize(long j);

    public static final native int GrannyGetMeshBinding4x4ArraySize(long j, int i);

    public static final native int GrannyGetMeshBindingBoneCount(long j);

    public static final native long GrannyGetMeshBindingFromBoneIndices(long j);

    public static final native long GrannyGetMeshBindingFromSkeleton(long j);

    public static final native long GrannyGetMeshBindingSourceMesh(long j);

    public static final native long GrannyGetMeshBindingToBoneIndices(long j);

    public static final native long GrannyGetMeshBindingToSkeleton(long j);

    public static final native int GrannyGetMeshBytesPerIndex(long j);

    public static final native int GrannyGetMeshIndexCount(long j);

    public static final native long GrannyGetMeshIndices(long j);

    public static final native int GrannyGetMeshMorphTargetCount(long j);

    public static final native int GrannyGetMeshMorphVertexCount(long j, int i);

    public static final native long GrannyGetMeshMorphVertexType(long j, int i);

    public static final native long GrannyGetMeshMorphVertices(long j, int i);

    public static final native int GrannyGetMeshTriangleCount(long j);

    public static final native int GrannyGetMeshTriangleGroupCount(long j);

    public static final native long GrannyGetMeshTriangleGroups(long j);

    public static final native int GrannyGetMeshVertexCount(long j);

    public static final native long GrannyGetMeshVertexType(long j);

    public static final native long GrannyGetMeshVertices(long j);

    public static final native void GrannyGetModelInitialPlacement4x4(long j, long j2);

    public static final native long GrannyGetModelInstanceFromBinding(long j);

    public static final native long GrannyGetModelUserDataArray(long j);

    public static final native float GrannyGetMostLikelyPhysicalAspectRatio(int i, int i2);

    public static final native String GrannyGetMostSeriousMessage();

    public static final native int GrannyGetMostSeriousMessageType();

    public static final native long GrannyGetNextBindingForAnimation(long j, long j2);

    public static final native long GrannyGetNextUnusedAnimationBinding(long j);

    public static final native long GrannyGetObjectMarshalling(long j);

    public static final native int GrannyGetOodle1CompressBufferPaddingSize();

    public static final native int GrannyGetOodle1DecompressBufferPaddingSize();

    public static final native long GrannyGetOrientationSamples(long j, int i);

    public static final native int GrannyGetOriginalVertex(long j, int i);

    public static final native void GrannyGetPickingRay(long j, float f, float f2, float f3, float f4, long j2, long j3);

    public static final native long GrannyGetPositionSamples(long j, int i);

    public static final native int GrannyGetRawImageSize(long j, int i, int i2, int i3);

    public static final native boolean GrannyGetRecommendedPixelLayout(long j, long j2);

    public static final native int GrannyGetResultingCurveDataSize(long j);

    public static final native int GrannyGetResultingCurveSize(long j);

    public static final native int GrannyGetResultingDaK32fC32fCurveSize(long j);

    public static final native int GrannyGetResultingLocalPoseSize(int i);

    public static final native int GrannyGetResultingMeshBindingSize(long j, long j2, long j3);

    public static final native int GrannyGetResultingSkeletonSize(long j);

    public static final native int GrannyGetResultingTextureSize(long j);

    public static final native int GrannyGetResultingTopologySize(long j);

    public static final native int GrannyGetResultingTrackGroupSize(long j);

    public static final native int GrannyGetResultingVariantObjectSize(long j);

    public static final native int GrannyGetResultingVariantTypeSize(long j);

    public static final native int GrannyGetResultingVertexCount(long j);

    public static final native int GrannyGetResultingVertexDataSize(long j);

    public static final native int GrannyGetResultingWorldPoseSize(int i, int i2);

    public static final native void GrannyGetRootMotionVectors(long j, float f, long j2, long j3, boolean z);

    public static final native int GrannyGetS3TCImageSize(int i, int i2, int i3);

    public static final native long GrannyGetS3TCPixelLayout(int i);

    public static final native String GrannyGetS3TCTextureFormatName(int i);

    public static final native long GrannyGetScaleShearSamples(long j, int i);

    public static final native float GrannyGetSecondsElapsed(long j, long j2);

    public static final native void GrannyGetSingleVertex(long j, int i, long j2, long j3);

    public static final native long GrannyGetSourceModel(long j);

    public static final native long GrannyGetSourceSkeleton(long j);

    public static final native void GrannyGetSquaredErrorOverCurve(int i, int i2, int i3, float f, long j, int i4, long j2, long j3, long j4);

    public static final native long GrannyGetStackUnit(long j, int i);

    public static final native int GrannyGetStackUnitCount(long j);

    public static final native String GrannyGetStandardSectionName(int i);

    public static final native long GrannyGetStringDatabase(long j);

    public static final native void GrannyGetSystemSeconds(long j);

    public static final native String GrannyGetTemporaryDirectory();

    public static final native String GrannyGetTextureEncodingName(int i);

    public static final native String GrannyGetTextureTypeName(int i);

    public static final native long GrannyGetTexturedMaterialByChannelName(long j, String str);

    public static final native void GrannyGetThisPlatformProperties(long j, long j2);

    public static final native int GrannyGetTotalObjectSize(long j);

    public static final native int GrannyGetTotalTypeSize(long j);

    public static final native void GrannyGetTrackGroupFlags(long j, long j2, long j3);

    public static final native void GrannyGetTrackGroupInitialPlacement4x4(long j, long j2);

    public static final native void GrannyGetTrackInitialTransform(long j, long j2);

    public static final native float GrannyGetTrackMaskBoneWeight(long j, int i);

    public static final native long GrannyGetTrackSamplerIII();

    public static final native long GrannyGetTrackSamplerIIU();

    public static final native long GrannyGetTrackSamplerSSS();

    public static final native long GrannyGetTrackSamplerUUU();

    public static final native float GrannyGetTransformDeterminant(long j);

    public static final native int GrannyGetTypeTableCount(long j);

    public static final native void GrannyGetVectorDifferences(int i, int i2, long j, long j2, int i3, long j3, long j4);

    public static final native void GrannyGetVersion(long j, long j2, long j3, long j4);

    public static final native String GrannyGetVersionString();

    public static final native int GrannyGetVertexBoneCount(long j);

    public static final native int GrannyGetVertexChannelCount(long j);

    public static final native int GrannyGetVertexComponentCount(long j);

    public static final native int GrannyGetVertexComponentIndex(String str, long j);

    public static final native String GrannyGetVertexComponentToolName(String str, long j);

    public static final native int GrannyGetVertexDiffuseColorName(int i, String str);

    public static final native int GrannyGetVertexSpecularColorName(int i, String str);

    public static final native int GrannyGetVertexTextureCoordinatesName(int i, String str);

    public static final native void GrannyGetWorldMatrixFromLocalPose(long j, int i, long j2, Buffer buffer, long j3, long j4, long j5);

    public static final native long GrannyGetWorldPose4x4(long j, int i);

    public static final native long GrannyGetWorldPose4x4Array(long j);

    public static final native int GrannyGetWorldPoseBoneCount(long j);

    public static final native long GrannyGetWorldPoseComposite4x4(long j, int i);

    public static final native long GrannyGetWorldPoseComposite4x4Array(long j);

    public static final native int GrannyHasOrientation_get();

    public static final native int GrannyHasPosition_get();

    public static final native int GrannyHasScaleShear_get();

    public static final native boolean GrannyHashedPointerKeyExists(long j, long j2);

    public static final native void GrannyIKUpdate(int i, int i2, long j, int i3, long j2, long j3, long j4, long j5);

    public static final native boolean GrannyIKUpdate2Bone(int i, long j, long j2, long j3, long j4, long j5, long j6, float f, float f2);

    public static final native long GrannyIdentityTrackMask_get();

    public static final native void GrannyIdentityTrackMask_set(long j);

    public static final native void GrannyInPlaceSimilarityTransform(long j, long j2, long j3, long j4, long j5, long j6);

    public static final native void GrannyInPlaceSimilarityTransform4x3(long j, long j2, long j3, long j4);

    public static final native void GrannyInPlaceSimilarityTransformOrientation(long j, long j2, long j3);

    public static final native void GrannyInPlaceSimilarityTransformPosition(long j, long j2, long j3);

    public static final native void GrannyInPlaceSimilarityTransformScaleShear(long j, long j2, long j3);

    public static final native int GrannyIncludeComposites_get();

    public static final native void GrannyInitializeDefaultCamera(long j);

    public static final native void GrannyInitializeFileReader(String str, int i, long j, long j2, long j3);

    public static final native void GrannyInitializeFileWriter(String str, int i, long j, long j2, long j3, long j4, long j5, long j6);

    public static final native void GrannyInitializeFixedAllocator(long j, int i);

    public static final native long GrannyInstantiateModel(long j);

    public static final native int GrannyInt16Marshalling_get();

    public static final native long GrannyInt16Type_get();

    public static final native void GrannyInt16Type_set(long j);

    public static final native int GrannyInt32Marshalling_get();

    public static final native long GrannyInt32Type_get();

    public static final native void GrannyInt32Type_set(long j);

    public static final native int GrannyInt8Marshalling_get();

    public static final native void GrannyInvertTrackMask(long j);

    public static final native void GrannyInvertTriTopologyWinding(long j);

    public static final native boolean GrannyIsAnimationUsed(long j);

    public static final native boolean GrannyIsBlendDagLeafType(long j);

    public static final native boolean GrannyIsBlendDagNodeValid(long j, long j2, long j3);

    public static final native boolean GrannyIsBlendDagTreeValid(long j, long j2, long j3);

    public static final native boolean GrannyIsGrannyFile(long j, long j2, long j3, long j4);

    public static final native boolean GrannyIsMixedMarshalling(long j);

    public static final native boolean GrannyIsSpatialVertexMember(String str);

    public static final native boolean GrannyKnotsAreReducible(int i, int i2, int i3, long j, long j2, long j3, float f, int i4, long j4, long j5);

    public static final native int GrannyLastGRNStandardTag_get();

    public static final native int GrannyLastGRNUserTag_get();

    public static final native void GrannyLinearBlendTransform(long j, long j2, float f, long j3);

    public static final native long GrannyLoadFixupArray(long j, long j2, boolean z);

    public static final native void GrannyLocalPoseFromWorldPose(long j, long j2, long j3, Buffer buffer, int i, int i2);

    public static final native void GrannyLocalPoseFromWorldPoseNoScale(long j, long j2, long j3, Buffer buffer, int i, int i2);

    public static final native long GrannyLocalPoseTransformType_get();

    public static final native void GrannyLocalPoseTransformType_set(long j);

    public static final native long GrannyLocalPoseType_get();

    public static final native void GrannyLocalPoseType_set(long j);

    public static final native boolean GrannyLogging();

    public static final native void GrannyMakeDefaultAnimationBindingID(long j, long j2, int i);

    public static final native int GrannyMakeEmptyDataTypeMember(long j, long j2);

    public static final native int GrannyMakeEmptyDataTypeObject(long j, long j2);

    public static final native void GrannyMakeIdentity(long j);

    public static final native String GrannyMapString(long j, String str);

    public static final native long GrannyMarkFileFixup(long j, long j2, int i, long j3);

    public static final native void GrannyMarkFileRootObject(long j, long j2, long j3);

    public static final native void GrannyMarkMarshallingFixup(long j, long j2, long j3, int i);

    public static final native int GrannyMarshallingMask_get();

    public static final native long GrannyMaterialBindingType_get();

    public static final native void GrannyMaterialBindingType_set(long j);

    public static final native long GrannyMaterialMapType_get();

    public static final native void GrannyMaterialMapType_set(long j);

    public static final native long GrannyMaterialType_get();

    public static final native void GrannyMaterialType_set(long j);

    public static final native void GrannyMatrixEqualsQuaternion3x3(long j, long j2);

    public static final native int GrannyMeasuredLOD_get();

    public static final native boolean GrannyMemberHasPointers(long j);

    public static final native long GrannyMemoryArenaPush(long j, int i);

    public static final native void GrannyMergeSingleObject(long j, long j2, long j3, long j4);

    public static final native boolean GrannyMeshBindingIsTransferred(long j);

    public static final native boolean GrannyMeshIsRigid(long j);

    public static final native long GrannyMeshType_get();

    public static final native void GrannyMeshType_set(long j);

    public static final native long GrannyModelControlsBegin(long j);

    public static final native long GrannyModelControlsEnd(long j);

    public static final native long GrannyModelControlsNext(long j);

    public static final native long GrannyModelMeshBindingType_get();

    public static final native void GrannyModelMeshBindingType_set(long j);

    public static final native long GrannyModelType_get();

    public static final native void GrannyModelType_set(long j);

    public static final native void GrannyModulationCompositeLocalPose(long j, float f, float f2, long j2, long j3);

    public static final native void GrannyModulationCompositeLocalPoseSparse(long j, float f, float f2, long j2, long j3, long j4);

    public static final native long GrannyMorphTargetType_get();

    public static final native void GrannyMorphTargetType_set(long j);

    public static final native void GrannyMoveCameraRelative(long j, float f, float f2, float f3);

    public static final native boolean GrannyMultipleNewStackUnits(long j, int i, long j2, long j3);

    public static final native void GrannyMultiply(long j, long j2, long j3);

    public static final native long GrannyNewLocalPose(int i);

    public static final native long GrannyNewLocalPoseInPlace(int i, Buffer buffer);

    public static final native long GrannyNewMemoryArena();

    public static final native long GrannyNewMeshBinding(long j, long j2, long j3);

    public static final native long GrannyNewMeshBindingInPlace(long j, long j2, long j3, Buffer buffer);

    public static final native long GrannyNewMeshDeformer(long j, long j2, int i, int i2);

    public static final native long GrannyNewPointerHash();

    public static final native long GrannyNewStackUnit(long j, long j2);

    public static final native long GrannyNewStringTable();

    public static final native long GrannyNewTrackMask(float f, int i);

    public static final native long GrannyNewWorldPose(int i);

    public static final native long GrannyNewWorldPoseInPlace(int i, int i2, Buffer buffer);

    public static final native long GrannyNewWorldPoseNoComposite(int i);

    public static final native long GrannyNextAllocation(long j);

    public static final native int GrannyNoDOFs_get();

    public static final native int GrannyNoSkeletonLOD_get();

    public static final native void GrannyNormalizeVertices(int i, long j, long j2);

    public static final native long GrannyNullTrackMask_get();

    public static final native void GrannyNullTrackMask_set(long j);

    public static final native void GrannyOffsetFileLocation(long j, long j2, long j3, long j4);

    public static final native long GrannyOldCurveType_get();

    public static final native void GrannyOldCurveType_set(long j);

    public static final native void GrannyOneNormalizeWeights(int i, long j, long j2);

    public static final native long GrannyOodle1BeginSimple(long j, int i);

    public static final native void GrannyOodle1Compress(long j, int i, long j2);

    public static final native void GrannyOodle1Decompress(boolean z, int i, long j, int i2, long j2);

    public static final native int GrannyOodle1End(long j, long j2, boolean z);

    public static final native void GrannyOodle1FreeSimple(long j);

    public static final native float GrannyOrientationToleranceFromDegrees(float f);

    public static final native long GrannyP3VertexType_get();

    public static final native void GrannyP3VertexType_set(long j);

    public static final native long GrannyPN33VertexType_get();

    public static final native void GrannyPN33VertexType_set(long j);

    public static final native long GrannyPNG333VertexType_get();

    public static final native void GrannyPNG333VertexType_set(long j);

    public static final native long GrannyPNGB3333VertexType_get();

    public static final native void GrannyPNGB3333VertexType_set(long j);

    public static final native long GrannyPNGBT33332VertexType_get();

    public static final native void GrannyPNGBT33332VertexType_set(long j);

    public static final native long GrannyPNGBT33333VertexType_get();

    public static final native void GrannyPNGBT33333VertexType_set(long j);

    public static final native long GrannyPNGT3332VertexType_get();

    public static final native void GrannyPNGT3332VertexType_set(long j);

    public static final native long GrannyPNT332VertexType_get();

    public static final native void GrannyPNT332VertexType_set(long j);

    public static final native long GrannyPNT333VertexType_get();

    public static final native void GrannyPNT333VertexType_set(long j);

    public static final native long GrannyPNTG3323VertexType_get();

    public static final native void GrannyPNTG3323VertexType_set(long j);

    public static final native long GrannyPT32VertexType_get();

    public static final native void GrannyPT32VertexType_set(long j);

    public static final native long GrannyPWN313VertexType_get();

    public static final native void GrannyPWN313VertexType_set(long j);

    public static final native long GrannyPWN323VertexType_get();

    public static final native void GrannyPWN323VertexType_set(long j);

    public static final native long GrannyPWN343VertexType_get();

    public static final native void GrannyPWN343VertexType_set(long j);

    public static final native long GrannyPWNG3133VertexType_get();

    public static final native void GrannyPWNG3133VertexType_set(long j);

    public static final native long GrannyPWNG3233VertexType_get();

    public static final native void GrannyPWNG3233VertexType_set(long j);

    public static final native long GrannyPWNG3433VertexType_get();

    public static final native void GrannyPWNG3433VertexType_set(long j);

    public static final native long GrannyPWNGB31333VertexType_get();

    public static final native void GrannyPWNGB31333VertexType_set(long j);

    public static final native long GrannyPWNGB32333VertexType_get();

    public static final native void GrannyPWNGB32333VertexType_set(long j);

    public static final native long GrannyPWNGB34333VertexType_get();

    public static final native void GrannyPWNGB34333VertexType_set(long j);

    public static final native long GrannyPWNGBT313332VertexType_get();

    public static final native void GrannyPWNGBT313332VertexType_set(long j);

    public static final native long GrannyPWNGBT323332VertexType_get();

    public static final native void GrannyPWNGBT323332VertexType_set(long j);

    public static final native long GrannyPWNGBT343332VertexType_get();

    public static final native void GrannyPWNGBT343332VertexType_set(long j);

    public static final native long GrannyPWNGT31332VertexType_get();

    public static final native void GrannyPWNGT31332VertexType_set(long j);

    public static final native long GrannyPWNGT32332VertexType_get();

    public static final native void GrannyPWNGT32332VertexType_set(long j);

    public static final native long GrannyPWNGT34332VertexType_get();

    public static final native void GrannyPWNGT34332VertexType_set(long j);

    public static final native long GrannyPWNT3132VertexType_get();

    public static final native void GrannyPWNT3132VertexType_set(long j);

    public static final native long GrannyPWNT3232VertexType_get();

    public static final native void GrannyPWNT3232VertexType_set(long j);

    public static final native long GrannyPWNT3432VertexType_get();

    public static final native void GrannyPWNT3432VertexType_set(long j);

    public static final native long GrannyPeriodicLoopType_get();

    public static final native void GrannyPeriodicLoopType_set(long j);

    public static final native boolean GrannyPixelLayoutHasAlpha(long j);

    public static final native long GrannyPixelLayoutType_get();

    public static final native void GrannyPixelLayoutType_set(long j);

    public static final native boolean GrannyPixelLayoutsAreEqual(long j, long j2);

    public static final native boolean GrannyPlatformConvertReaderToWriter(long j, long j2, long j3, boolean z);

    public static final native long GrannyPlayControlledAnimation(float f, long j, long j2);

    public static final native long GrannyPlayControlledAnimationBinding(float f, long j, long j2, long j3);

    public static final native long GrannyPlayControlledPose(float f, float f2, long j, long j2, long j3);

    public static final native boolean GrannyPolarDecompose(long j, float f, long j2, long j3);

    public static final native void GrannyPopStackUnits(long j, int i);

    public static final native void GrannyPostMultiplyBy(long j, long j2);

    public static final native void GrannyPreMultiplyBy(long j, long j2);

    public static final native int GrannyPredictWriterAlignment(int i);

    public static final native void GrannyPreserveFileSectionFormats(long j, long j2);

    public static final native void GrannyPreserveObjectFileSections(long j, long j2);

    public static final native void GrannyPrimeBlendDagLocalPoseCache(int i, int i2);

    public static final native void GrannyPushCurveControlArray(long j, long j2);

    public static final native void GrannyPushCurveKnotArray(long j, long j2);

    public static final native void GrannyPushCurveSampleArrays(long j, int i, int i2, long j2, long j3);

    public static final native void GrannyPushSampledFrame(long j);

    public static final native void GrannyPushTriangle(long j);

    public static final native void GrannyPushVectorTrackCurve(long j, String str, long j2, long j3);

    public static final native void GrannyPushVertex(long j);

    public static final native long GrannyQuadType_get();

    public static final native void GrannyQuadType_set(long j);

    public static final native void GrannyQuaternionEqualsMatrix3x3(long j, long j2);

    public static final native long GrannyRGB555PixelFormat_get();

    public static final native void GrannyRGB555PixelFormat_set(long j);

    public static final native long GrannyRGB565PixelFormat_get();

    public static final native void GrannyRGB565PixelFormat_set(long j);

    public static final native long GrannyRGB888PixelFormat_get();

    public static final native void GrannyRGB888PixelFormat_set(long j);

    public static final native long GrannyRGBA4444PixelFormat_get();

    public static final native void GrannyRGBA4444PixelFormat_set(long j);

    public static final native long GrannyRGBA5551PixelFormat_get();

    public static final native void GrannyRGBA5551PixelFormat_set(long j);

    public static final native long GrannyRGBA8888PixelFormat_get();

    public static final native void GrannyRGBA8888PixelFormat_set(long j);

    public static final native int GrannyRayIntersectsBox(long j, long j2, long j3, long j4, long j5);

    public static final native int GrannyRayIntersectsBoxAt(long j, long j2, long j3, long j4, long j5, long j6);

    public static final native int GrannyRayIntersectsPlaneAt(long j, float f, long j2, long j3, long j4);

    public static final native boolean GrannyRayIntersectsSphere(long j, float f, long j2, long j3);

    public static final native int GrannyRayIntersectsSphereAt(long j, float f, long j2, long j3, long j4, long j5);

    public static final native int GrannyRayIntersectsTriangleAt(long j, long j2, long j3, long j4, long j5, long j6);

    public static final native long GrannyReadEntireFile(String str);

    public static final native long GrannyReadEntireFileFromMemory(int i, Buffer buffer);

    public static final native long GrannyReadEntireFileFromReader(long j);

    public static final native void GrannyReadFileSection(long j, long j2, int i);

    public static final native void GrannyReadFileSectionInPlace(long j, long j2, int i, Buffer buffer);

    public static final native long GrannyReadPartialFileFromReader(long j);

    public static final native void GrannyReal16ToReal32(int i, long j);

    public static final native int GrannyReal32ToReal16(float f);

    public static final native long GrannyReal32Type_get();

    public static final native void GrannyReal32Type_set(long j);

    public static final native boolean GrannyRebasePointers(long j, long j2, int i, boolean z);

    public static final native boolean GrannyRebasePointersStringCallback(long j, long j2, int i, long j3, long j4);

    public static final native String GrannyRebaseToStringDatabase(long j, long j2);

    public static final native void GrannyRecenterAllControlClocks(float f);

    public static final native void GrannyRecenterAllModelInstanceControlClocks(long j, float f);

    public static final native void GrannyRecenterControlClocks(long j, float f);

    public static final native boolean GrannyRecompressFile(String str, String str2, int i, long j);

    public static final native void GrannyReleaseAnimationBinding(long j);

    public static final native void GrannyReleaseMemorySpinlock();

    public static final native void GrannyRemapAllAnimationBindingPointers(long j, long j2);

    public static final native void GrannyRemapAnimationBindingPointers(long j, long j2);

    public static final native boolean GrannyRemapFileStrings(long j, long j2);

    public static final native void GrannyRemapTopologyMaterials(long j, int i, long j2);

    public static final native void GrannyRemoveBlendDagNodeChild(long j, long j2);

    public static final native void GrannyRemoveTrackInitialTransform(long j);

    public static final native int GrannyRenormalizeNormals_get();

    public static final native int GrannyReorderTriangleIndices_get();

    public static final native void GrannyResetCounterPeaks();

    public static final native void GrannyResetCounters();

    public static final native void GrannyResetLODErrorSpace(long j);

    public static final native void GrannyResetLODErrorValues(long j);

    public static final native void GrannyResortAllAnimationTrackGroups(long j);

    public static final native void GrannyResortAllFileTrackGroups(long j);

    public static final native void GrannyResortTrackGroup(long j);

    public static final native void GrannyReverseSection(int i, int i2, int i3, long j);

    public static final native void GrannyReverseTypeArray(long j, int i, long j2);

    public static final native int GrannyRotationDOFs_get();

    public static final native void GrannySampleBSpline(int i, int i2, boolean z, long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline0x1(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline0x2(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline0x3(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline0x4(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline0x9(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline1x1(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline1x2(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline1x3(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline1x3n(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline1x4(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline1x4n(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline1x9(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline2x1(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline2x2(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline2x3(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline2x3n(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline2x4(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline2x4n(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline2x9(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline3x1(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline3x2(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline3x3(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline3x3n(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline3x4(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline3x4n(long j, long j2, float f, long j3);

    public static final native void GrannySampleBSpline3x9(long j, long j2, float f, long j3);

    public static final native long GrannySampleBlendDagTree(long j, int i);

    public static final native long GrannySampleBlendDagTreeLOD(long j, int i, float f);

    public static final native long GrannySampleBlendDagTreeLODReentrant(long j, int i, float f, long j2);

    public static final native long GrannySampleBlendDagTreeLODSparse(long j, int i, float f, long j2);

    public static final native long GrannySampleBlendDagTreeLODSparseReentrant(long j, int i, float f, long j2, long j3);

    public static final native long GrannySampleBlendDagTreeReentrant(long j, int i, long j2);

    public static final native void GrannySampleModelAnimations(long j, int i, int i2, long j2);

    public static final native void GrannySampleModelAnimationsAccelerated(long j, int i, Buffer buffer, long j2, long j3);

    public static final native void GrannySampleModelAnimationsAcceleratedLOD(long j, int i, Buffer buffer, long j2, long j3, float f);

    public static final native void GrannySampleModelAnimationsLOD(long j, int i, int i2, long j2, float f);

    public static final native void GrannySampleModelAnimationsLODSparse(long j, int i, int i2, long j2, float f, long j3);

    public static final native boolean GrannySampleSingleModelAnimation(long j, long j2, int i, int i2, long j3);

    public static final native boolean GrannySampleSingleModelAnimationLOD(long j, long j2, int i, int i2, long j3, float f);

    public static final native boolean GrannySampleSingleModelAnimationLODSparse(long j, long j2, int i, int i2, long j3, float f, long j4);

    public static final native void GrannySampleTrackPOLocal(long j, long j2, long j3, long j4, long j5);

    public static final native void GrannySampleTrackUUULocal(long j, long j2, long j3, long j4);

    public static final native void GrannyScaleImage(int i, int i2, int i3, int i4, long j, int i5, int i6, int i7, long j2);

    public static final native int GrannyScaleShearDOFs_get();

    public static final native int GrannySeekWriterFromCurrentPositionStub(String str, int i, long j, int i2);

    public static final native int GrannySeekWriterFromEndStub(String str, int i, long j, int i2);

    public static final native int GrannySeekWriterFromStartStub(String str, int i, long j, int i2);

    public static final native void GrannySerializeResultingCoincidentVertexMap(long j, long j2);

    public static final native void GrannySerializeResultingVertexToTriangleMap(long j, long j2);

    public static final native void GrannySerializeResultingVertices(long j, long j2);

    public static final native void GrannySerializeStack(long j, long j2);

    public static final native void GrannySetAllLODErrorSpace(long j, float f);

    public static final native void GrannySetAllLODErrorValues(long j, float f);

    public static final native void GrannySetAllocator(long j, long j2);

    public static final native void GrannySetAllowGlobalStateChanges(boolean z);

    public static final native void GrannySetArenaAlignment(long j, int i);

    public static final native void GrannySetBinormal(long j, int i, float f, float f2, float f3);

    public static final native void GrannySetBinormalTolerance(long j, float f);

    public static final native void GrannySetBlendDagNodeAnimationBlend(long j, long j2, float f, boolean z);

    public static final native void GrannySetBlendDagNodeCallbacks(long j, long j2, long j3, long j4, long j5);

    public static final native void GrannySetBlendDagNodeChild(long j, int i, long j2);

    public static final native void GrannySetBlendDagNodeChildren(long j, int i, long j2);

    public static final native void GrannySetBlendDagNodeCrossfade(long j, float f, float f2, long j2, boolean z);

    public static final native void GrannySetBlendDagNodeCrossfadeWeights(long j, float f, float f2);

    public static final native void GrannySetBlendDagNodeLocalPose(long j, long j2, boolean z);

    public static final native void GrannySetBlendDagNodeResultTrackMask(long j, long j2, boolean z);

    public static final native void GrannySetBlendDagNodeResultWeight(long j, float f);

    public static final native void GrannySetBlendDagNodeWeightedBlend(long j, long j2, float f, int i);

    public static final native void GrannySetBlendDagTreeClock(long j, float f);

    public static final native void GrannySetBoneLODError(long j, int i, float f);

    public static final native void GrannySetBoneParent(long j, int i, int i2);

    public static final native void GrannySetCameraAspectRatios(long j, float f, float f2, float f3, float f4, float f5);

    public static final native void GrannySetChannel(long j, int i, int i2, long j2);

    public static final native void GrannySetChannelTolerance(long j, int i, float f);

    public static final native void GrannySetControlActive(long j, boolean z);

    public static final native void GrannySetControlClock(long j, float f);

    public static final native void GrannySetControlClockOnly(long j, float f);

    public static final native void GrannySetControlCompletionCheckFlag(long j, boolean z);

    public static final native void GrannySetControlEaseIn(long j, boolean z);

    public static final native void GrannySetControlEaseInCurve(long j, float f, float f2, float f3, float f4, float f5, float f6);

    public static final native void GrannySetControlEaseOut(long j, boolean z);

    public static final native void GrannySetControlEaseOutCurve(long j, float f, float f2, float f3, float f4, float f5, float f6);

    public static final native void GrannySetControlForceClampedLooping(long j, boolean z);

    public static final native void GrannySetControlLoopCount(long j, int i);

    public static final native void GrannySetControlLoopIndex(long j, int i);

    public static final native void GrannySetControlRawLocalClock(long j, float f);

    public static final native void GrannySetControlSpeed(long j, float f);

    public static final native void GrannySetControlTargetState(long j, float f, float f2, float f3, int i);

    public static final native void GrannySetControlWeight(long j, float f);

    public static final native void GrannySetDefaultFileReaderOpenCallback(long j);

    public static final native void GrannySetDefaultFileWriterOpenCallback(long j);

    public static final native void GrannySetFileDataTreeFlags(long j, long j2);

    public static final native void GrannySetFileExtraTag(long j, int i, long j2);

    public static final native void GrannySetFileSectionForObject(long j, long j2, int i);

    public static final native void GrannySetFileSectionForObjectsOfType(long j, long j2, int i);

    public static final native void GrannySetFileSectionFormat(long j, int i, int i2, int i3);

    public static final native void GrannySetFileStringDatabaseCRC(long j, long j2);

    public static final native void GrannySetFileWriterStringCallback(long j, long j2, long j3);

    public static final native void GrannySetGlobalLODFadingFactor(float f);

    public static final native boolean GrannySetHashedPointerData(long j, long j2, long j3);

    public static final native void GrannySetImageScalingFilter(long j, int i, int i2);

    public static final native void GrannySetLODErrorValue(long j, int i, float f);

    public static final native void GrannySetLocalPoseFillThreshold(long j, float f);

    public static final native void GrannySetLogCallback(long j);

    public static final native boolean GrannySetLogFileName(String str, boolean z);

    public static final native void GrannySetMaterial(long j, int i);

    public static final native void GrannySetMaximumAnimationBindingCount(int i);

    public static final native void GrannySetModelClock(long j, float f);

    public static final native void GrannySetNormal(long j, int i, float f, float f2, float f3);

    public static final native void GrannySetNormalTolerance(long j, float f);

    public static final native void GrannySetPosition(long j, float f, float f2, float f3);

    public static final native void GrannySetSkeletonTrackMaskChainDownwards(long j, long j2, int i, float f);

    public static final native void GrannySetSkeletonTrackMaskChainUpwards(long j, long j2, int i, float f);

    public static final native void GrannySetSkeletonTrackMaskFromTrackGroup(long j, long j2, long j3, float f, float f2, float f3);

    public static final native void GrannySetStockBGRASpecification(long j, int i, int i2, int i3, int i4);

    public static final native void GrannySetStockRGBASpecification(long j, int i, int i2, int i3, int i4);

    public static final native void GrannySetStockSpecification(long j, long j2, long j3);

    public static final native void GrannySetStringComparisonCallback(long j);

    public static final native void GrannySetTangent(long j, int i, float f, float f2, float f3);

    public static final native void GrannySetTangentBinormalCross(long j, int i, float f, float f2, float f3);

    public static final native void GrannySetTangentBinormalCrossTolerance(long j, float f);

    public static final native void GrannySetTangentMergingTolerance(long j, float f);

    public static final native void GrannySetTangentTolerance(long j, float f);

    public static final native long GrannySetThreadIDCallback(long j);

    public static final native void GrannySetTrackGroupAccumulation(long j, int i, int i2);

    public static final native void GrannySetTrackGroupBasisTransform(long j, int i, long j2, long j3);

    public static final native void GrannySetTrackGroupBinding(long j, int i, long j2);

    public static final native void GrannySetTrackGroupFlags(long j, long j2, long j3);

    public static final native void GrannySetTrackGroupLOD(long j, int i, boolean z, float f);

    public static final native void GrannySetTrackGroupModelMask(long j, int i, long j2);

    public static final native void GrannySetTrackGroupTarget(long j, int i, long j2);

    public static final native void GrannySetTrackGroupTrackMask(long j, int i, long j2);

    public static final native void GrannySetTrackMaskBoneWeight(long j, int i, float f);

    public static final native void GrannySetTrackMatchRule(long j, int i, String str, String str2);

    public static final native void GrannySetTransform(long j, long j2, long j3, long j4);

    public static final native void GrannySetTransformSample(long j, int i, long j2, long j3, long j4);

    public static final native void GrannySetTransformTrackOrientationCurve(long j, long j2);

    public static final native void GrannySetTransformTrackPositionCurve(long j, long j2);

    public static final native void GrannySetTransformTrackScaleShearCurve(long j, long j2);

    public static final native void GrannySetTransformWithIdentityCheck(long j, long j2, long j3, long j4);

    public static final native void GrannySetVertexChannelComponentNames(long j, int i, long j2);

    public static final native void GrannySetVertexColor(long j, long j2, int i, long j3);

    public static final native void GrannySetVertexIndex(long j, int i, int i2);

    public static final native void GrannySetVertexNormal(long j, long j2, long j3);

    public static final native void GrannySetVertexPosition(long j, long j2, long j3);

    public static final native void GrannySetVertexUVW(long j, long j2, int i, long j3);

    public static final native void GrannySimilarityTransform(long j, long j2, long j3, long j4);

    public static final native void GrannySimilarityTransformCurvePosition(long j, long j2, long j3, float f, float f2, long j4);

    public static final native void GrannySimilarityTransformCurveQuaternion(long j, long j2, long j3, float f, float f2, long j4);

    public static final native void GrannySimilarityTransformCurveScaleShear(long j, long j2, long j3, float f, float f2, long j4);

    public static final native void GrannySimilarityTransformTrackGroup(long j, long j2, long j3, long j4, float f, float f2);

    public static final native long GrannySkeletonType_get();

    public static final native void GrannySkeletonType_set(long j);

    public static final native void GrannySleepForSeconds(float f);

    public static final native int GrannySparseBoneArrayAddBone(long j, int i, int i2, long j2, long j3);

    public static final native int GrannySparseBoneArrayCreateSingleBone(long j, int i, long j2, long j3);

    public static final native void GrannySparseBoneArrayExpand(long j, long j2, int i, long j3, long j4, int i2, long j5);

    public static final native boolean GrannySparseBoneArrayIsValid(int i, int i2, long j, long j2);

    public static final native void GrannyStackCleanUp(long j);

    public static final native void GrannyStackInitialize(long j, int i, int i2);

    public static final native void GrannyStackInitializeWithDirectory(long j, int i, int i2, int i3);

    public static final native int GrannyStandardDeformableIndexSection_get();

    public static final native int GrannyStandardDeformableVertexSection_get();

    public static final native int GrannyStandardDiscardableSection_get();

    public static final native int GrannyStandardMainSection_get();

    public static final native int GrannyStandardRigidIndexSection_get();

    public static final native int GrannyStandardRigidVertexSection_get();

    public static final native int GrannyStandardTextureSection_get();

    public static final native int GrannyStandardUnloadedSection_get();

    public static final native boolean GrannyStealMemoryWriterBuffer(long j, long j2, long j3);

    public static final native void GrannyStepPeriodicLoop(long j, float f, long j2, long j3);

    public static final native long GrannyStringDatabaseType_get();

    public static final native void GrannyStringDatabaseType_set(long j);

    public static final native int GrannyStringDifference(String str, String str2);

    public static final native long GrannyStringType_get();

    public static final native void GrannyStringType_set(long j);

    public static final native void GrannySwapRGBAToBGRA(long j);

    public static final native long GrannyTextTrackEntryType_get();

    public static final native void GrannyTextTrackEntryType_set(long j);

    public static final native long GrannyTextTrackType_get();

    public static final native void GrannyTextTrackType_set(long j);

    public static final native boolean GrannyTextureHasAlpha(long j);

    public static final native long GrannyTextureImageType_get();

    public static final native void GrannyTextureImageType_set(long j);

    public static final native long GrannyTextureMIPLevelType_get();

    public static final native void GrannyTextureMIPLevelType_set(long j);

    public static final native long GrannyTextureType_get();

    public static final native void GrannyTextureType_set(long j);

    public static final native boolean GrannyThreadAllowedToCallGranny();

    public static final native int GrannyTrackGroupIsSorted_get();

    public static final native long GrannyTrackGroupType_get();

    public static final native void GrannyTrackGroupType_set(long j);

    public static final native long GrannyTrackMaskType_get();

    public static final native void GrannyTrackMaskType_set(long j);

    public static final native void GrannyTransformAnimation(long j, long j2, long j3, long j4, float f, float f2, long j5);

    public static final native void GrannyTransformBoundingBox(long j, long j2, long j3, long j4);

    public static final native void GrannyTransformCurve3(long j, int i, long j2);

    public static final native void GrannyTransformCurve3x3(long j, int i, long j2);

    public static final native void GrannyTransformCurve4(long j, int i, long j2);

    public static final native void GrannyTransformCurveVec3(long j, long j2, float f, float f2, long j3);

    public static final native void GrannyTransformFile(long j, long j2, long j3, long j4, float f, float f2, long j5);

    public static final native void GrannyTransformMesh(long j, long j2, long j3, long j4, float f, float f2, long j5);

    public static final native void GrannyTransformModel(long j, long j2, long j3, long j4, float f, float f2, long j5);

    public static final native void GrannyTransformPoint(long j, long j2, long j3);

    public static final native void GrannyTransformPointInPlace(long j, long j2);

    public static final native void GrannyTransformSkeleton(long j, long j2, long j3, long j4, float f, float f2, long j5);

    public static final native boolean GrannyTransformTrackHasKeyframedCurves(long j);

    public static final native boolean GrannyTransformTrackIsAnimated(long j);

    public static final native boolean GrannyTransformTrackIsIdentity(long j);

    public static final native long GrannyTransformTrackType_get();

    public static final native void GrannyTransformTrackType_set(long j);

    public static final native long GrannyTransformType_get();

    public static final native void GrannyTransformType_set(long j);

    public static final native void GrannyTransformVector(long j, long j2, long j3);

    public static final native void GrannyTransformVectorInPlace(long j, long j2);

    public static final native void GrannyTransformVectorInPlaceTransposed(long j, long j2);

    public static final native void GrannyTransformVertices(int i, long j, long j2, long j3, long j4, long j5, boolean z, boolean z2);

    public static final native int GrannyTranslationDOFs_get();

    public static final native long GrannyTriAnnotationSetType_get();

    public static final native void GrannyTriAnnotationSetType_set(long j);

    public static final native long GrannyTriMaterialGroupType_get();

    public static final native void GrannyTriMaterialGroupType_set(long j);

    public static final native long GrannyTriTopologyType_get();

    public static final native void GrannyTriTopologyType_set(long j);

    public static final native long GrannyTripleType_get();

    public static final native void GrannyTripleType_set(long j);

    public static final native boolean GrannyTypeHasPointers(long j);

    public static final native long GrannyUInt16Type_get();

    public static final native void GrannyUInt16Type_set(long j);

    public static final native long GrannyUInt32Type_get();

    public static final native void GrannyUInt32Type_set(long j);

    public static final native long GrannyUInt8Type_get();

    public static final native void GrannyUInt8Type_set(long j);

    public static final native long GrannyUnboundTrackMaskType_get();

    public static final native void GrannyUnboundTrackMaskType_set(long j);

    public static final native void GrannyUncheckedSampleBSpline(int i, int i2, long j, long j2, float f, long j3);

    public static final native void GrannyUncheckedSampleBSplineN(int i, int i2, long j, long j2, float f, long j3);

    public static final native void GrannyUpdateBlendDagTreeMatrix(long j, float f, Buffer buffer, Buffer buffer2, boolean z);

    public static final native void GrannyUpdateModelMatrix(long j, float f, Buffer buffer, Buffer buffer2, boolean z);

    public static final native void GrannyUpdateWorldPoseChildren(long j, int i, long j2, Buffer buffer, long j3);

    public static final native int GrannyUseAccumulatorNeighborhood_get();

    public static final native void GrannyUseExistingControlForAnimation(long j, long j2);

    public static final native long GrannyVectorTrackKeyForBone(long j, int i, String str);

    public static final native long GrannyVectorTrackType_get();

    public static final native void GrannyVectorTrackType_set(long j);

    public static final native boolean GrannyVersionsMatch_(int i, int i2, int i3, int i4);

    public static final native long GrannyVertexAnnotationSetType_get();

    public static final native void GrannyVertexAnnotationSetType_set(long j);

    public static final native long GrannyVertexDataType_get();

    public static final native void GrannyVertexDataType_set(long j);

    public static final native long GrannyVertexWeightArraysType_get();

    public static final native void GrannyVertexWeightArraysType_set(long j);

    public static final native void GrannyWindowSpaceToWorldSpace(long j, float f, float f2, long j2, long j3);

    public static final native void GrannyWorldSpaceToWindowSpace(long j, float f, float f2, long j2, long j3);

    public static final native boolean GrannyWriteDataTreeToFile(long j, long j2, long j3, String str, int i);

    public static final native boolean GrannyWriteDataTreeToFileBuilder(long j, long j2);

    public static final native void GrannyWriteFileChunk(long j, int i, long j2, long j3, long j4, long j5);

    public static final native int GrannyXRotation_get();

    public static final native int GrannyXScaleShear_get();

    public static final native int GrannyXTranslation_get();

    public static final native int GrannyYRotation_get();

    public static final native int GrannyYScaleShear_get();

    public static final native int GrannyYTranslation_get();

    public static final native int GrannyZRotation_get();

    public static final native int GrannyZScaleShear_get();

    public static final native int GrannyZTranslation_get();

    public static final native void GrannyZeroPeriodicLoop(long j);

    public static final native void GrannyZeroTransform(long j);

    public static final native String char_pointer_array_getitem(long j, int i);

    public static final native void char_pointer_array_setitem(long j, int i, String str);

    public static final native void delete_char_pointer_array(long j);

    public static final native void delete_granny_allocated_block_array(long j);

    public static final native void delete_granny_allocation_header_array(long j);

    public static final native void delete_granny_allocation_information(long j);

    public static final native void delete_granny_allocation_information_array(long j);

    public static final native void delete_granny_animation(long j);

    public static final native void delete_granny_animation_array(long j);

    public static final native void delete_granny_animation_binding(long j);

    public static final native void delete_granny_animation_binding_array(long j);

    public static final native void delete_granny_animation_binding_cache_status(long j);

    public static final native void delete_granny_animation_binding_cache_status_array(long j);

    public static final native void delete_granny_animation_binding_identifier(long j);

    public static final native void delete_granny_animation_binding_identifier_array(long j);

    public static final native void delete_granny_animation_lod_builder_array(long j);

    public static final native void delete_granny_art_tool_info(long j);

    public static final native void delete_granny_art_tool_info_array(long j);

    public static final native void delete_granny_blend_dag_node_array(long j);

    public static final native void delete_granny_bone(long j);

    public static final native void delete_granny_bone_array(long j);

    public static final native void delete_granny_bone_binding(long j);

    public static final native void delete_granny_bone_binding_array(long j);

    public static final native void delete_granny_bound_transform_track(long j);

    public static final native void delete_granny_bound_transform_track_array(long j);

    public static final native void delete_granny_box_intersection(long j);

    public static final native void delete_granny_box_intersection_array(long j);

    public static final native void delete_granny_bspline_error(long j);

    public static final native void delete_granny_bspline_error_array(long j);

    public static final native void delete_granny_bspline_solver_array(long j);

    public static final native void delete_granny_camera(long j);

    public static final native void delete_granny_camera_array(long j);

    public static final native void delete_granny_compress_curve_parameters(long j);

    public static final native void delete_granny_compress_curve_parameters_array(long j);

    public static final native void delete_granny_control_array(long j);

    public static final native void delete_granny_controlled_animation_array(long j);

    public static final native void delete_granny_controlled_animation_builder_array(long j);

    public static final native void delete_granny_controlled_pose_array(long j);

    public static final native void delete_granny_counter_results(long j);

    public static final native void delete_granny_counter_results_array(long j);

    public static final native void delete_granny_curve2(long j);

    public static final native void delete_granny_curve2_array(long j);

    public static final native void delete_granny_curve_builder_array(long j);

    public static final native void delete_granny_curve_data_d3_constant32f(long j);

    public static final native void delete_granny_curve_data_d3_constant32f_array(long j);

    public static final native void delete_granny_curve_data_d3_k16u_c16u(long j);

    public static final native void delete_granny_curve_data_d3_k16u_c16u_array(long j);

    public static final native void delete_granny_curve_data_d3_k8u_c8u(long j);

    public static final native void delete_granny_curve_data_d3_k8u_c8u_array(long j);

    public static final native void delete_granny_curve_data_d3i1_k16u_c16u(long j);

    public static final native void delete_granny_curve_data_d3i1_k16u_c16u_array(long j);

    public static final native void delete_granny_curve_data_d3i1_k32f_c32f(long j);

    public static final native void delete_granny_curve_data_d3i1_k32f_c32f_array(long j);

    public static final native void delete_granny_curve_data_d3i1_k8u_c8u(long j);

    public static final native void delete_granny_curve_data_d3i1_k8u_c8u_array(long j);

    public static final native void delete_granny_curve_data_d4_constant32f(long j);

    public static final native void delete_granny_curve_data_d4_constant32f_array(long j);

    public static final native void delete_granny_curve_data_d4n_k16u_c15u(long j);

    public static final native void delete_granny_curve_data_d4n_k16u_c15u_array(long j);

    public static final native void delete_granny_curve_data_d4n_k8u_c7u(long j);

    public static final native void delete_granny_curve_data_d4n_k8u_c7u_array(long j);

    public static final native void delete_granny_curve_data_d9i1_k16u_c16u(long j);

    public static final native void delete_granny_curve_data_d9i1_k16u_c16u_array(long j);

    public static final native void delete_granny_curve_data_d9i1_k8u_c8u(long j);

    public static final native void delete_granny_curve_data_d9i1_k8u_c8u_array(long j);

    public static final native void delete_granny_curve_data_d9i3_k16u_c16u(long j);

    public static final native void delete_granny_curve_data_d9i3_k16u_c16u_array(long j);

    public static final native void delete_granny_curve_data_d9i3_k8u_c8u(long j);

    public static final native void delete_granny_curve_data_d9i3_k8u_c8u_array(long j);

    public static final native void delete_granny_curve_data_da_constant32f(long j);

    public static final native void delete_granny_curve_data_da_constant32f_array(long j);

    public static final native void delete_granny_curve_data_da_identity(long j);

    public static final native void delete_granny_curve_data_da_identity_array(long j);

    public static final native void delete_granny_curve_data_da_k16u_c16u(long j);

    public static final native void delete_granny_curve_data_da_k16u_c16u_array(long j);

    public static final native void delete_granny_curve_data_da_k32f_c32f(long j);

    public static final native void delete_granny_curve_data_da_k32f_c32f_array(long j);

    public static final native void delete_granny_curve_data_da_k8u_c8u(long j);

    public static final native void delete_granny_curve_data_da_k8u_c8u_array(long j);

    public static final native void delete_granny_curve_data_da_keyframes32f(long j);

    public static final native void delete_granny_curve_data_da_keyframes32f_array(long j);

    public static final native void delete_granny_curve_data_header(long j);

    public static final native void delete_granny_curve_data_header_array(long j);

    public static final native void delete_granny_dag_pose_cache_array(long j);

    public static final native void delete_granny_data_type_definition(long j);

    public static final native void delete_granny_data_type_definition_array(long j);

    public static final native void delete_granny_defined_type(long j);

    public static final native void delete_granny_defined_type_array(long j);

    public static final native void delete_granny_exporter_info(long j);

    public static final native void delete_granny_exporter_info_array(long j);

    public static final native void delete_granny_file(long j);

    public static final native void delete_granny_file_array(long j);

    public static final native void delete_granny_file_builder_array(long j);

    public static final native void delete_granny_file_compressor_array(long j);

    public static final native void delete_granny_file_data_tree_writer_array(long j);

    public static final native void delete_granny_file_fixup_array(long j);

    public static final native void delete_granny_file_info(long j);

    public static final native void delete_granny_file_info_array(long j);

    public static final native void delete_granny_file_location(long j);

    public static final native void delete_granny_file_location_array(long j);

    public static final native void delete_granny_file_reader(long j);

    public static final native void delete_granny_file_reader_array(long j);

    public static final native void delete_granny_file_writer(long j);

    public static final native void delete_granny_file_writer_array(long j);

    public static final native void delete_granny_fixed_allocator(long j);

    public static final native void delete_granny_fixed_allocator_array(long j);

    public static final native void delete_granny_fixed_allocator_block(long j);

    public static final native void delete_granny_fixed_allocator_block_array(long j);

    public static final native void delete_granny_fixed_allocator_unit(long j);

    public static final native void delete_granny_fixed_allocator_unit_array(long j);

    public static final native void delete_granny_grn_file_header(long j);

    public static final native void delete_granny_grn_file_header_array(long j);

    public static final native void delete_granny_grn_file_magic_value(long j);

    public static final native void delete_granny_grn_file_magic_value_array(long j);

    public static final native void delete_granny_grn_mixed_marshalling_fixup(long j);

    public static final native void delete_granny_grn_mixed_marshalling_fixup_array(long j);

    public static final native void delete_granny_grn_pointer_fixup(long j);

    public static final native void delete_granny_grn_pointer_fixup_array(long j);

    public static final native void delete_granny_grn_reference(long j);

    public static final native void delete_granny_grn_reference_array(long j);

    public static final native void delete_granny_grn_section(long j);

    public static final native void delete_granny_grn_section_array(long j);

    public static final native void delete_granny_local_pose_array(long j);

    public static final native void delete_granny_log_callback(long j);

    public static final native void delete_granny_log_callback_array(long j);

    public static final native void delete_granny_material(long j);

    public static final native void delete_granny_material_array(long j);

    public static final native void delete_granny_material_binding(long j);

    public static final native void delete_granny_material_binding_array(long j);

    public static final native void delete_granny_material_map(long j);

    public static final native void delete_granny_material_map_array(long j);

    public static final native void delete_granny_memory_arena_array(long j);

    public static final native void delete_granny_mesh(long j);

    public static final native void delete_granny_mesh_array(long j);

    public static final native void delete_granny_mesh_binding_array(long j);

    public static final native void delete_granny_mesh_builder_array(long j);

    public static final native void delete_granny_mesh_deformer_array(long j);

    public static final native void delete_granny_model(long j);

    public static final native void delete_granny_model_array(long j);

    public static final native void delete_granny_model_control_binding_array(long j);

    public static final native void delete_granny_model_instance_array(long j);

    public static final native void delete_granny_model_mesh_binding(long j);

    public static final native void delete_granny_model_mesh_binding_array(long j);

    public static final native void delete_granny_morph_target(long j);

    public static final native void delete_granny_morph_target_array(long j);

    public static final native void delete_granny_old_curve(long j);

    public static final native void delete_granny_old_curve_array(long j);

    public static final native void delete_granny_oodle1_state_array(long j);

    public static final native void delete_granny_p3_vertex(long j);

    public static final native void delete_granny_p3_vertex_array(long j);

    public static final native void delete_granny_periodic_loop(long j);

    public static final native void delete_granny_periodic_loop_array(long j);

    public static final native void delete_granny_pixel_layout(long j);

    public static final native void delete_granny_pixel_layout_array(long j);

    public static final native void delete_granny_pn33_vertex(long j);

    public static final native void delete_granny_pn33_vertex_array(long j);

    public static final native void delete_granny_png333_vertex(long j);

    public static final native void delete_granny_png333_vertex_array(long j);

    public static final native void delete_granny_pngb3333_vertex(long j);

    public static final native void delete_granny_pngb3333_vertex_array(long j);

    public static final native void delete_granny_pngbt33332_vertex(long j);

    public static final native void delete_granny_pngbt33332_vertex_array(long j);

    public static final native void delete_granny_pngbt33333_vertex(long j);

    public static final native void delete_granny_pngbt33333_vertex_array(long j);

    public static final native void delete_granny_pngt3332_vertex(long j);

    public static final native void delete_granny_pngt3332_vertex_array(long j);

    public static final native void delete_granny_pnt332_vertex(long j);

    public static final native void delete_granny_pnt332_vertex_array(long j);

    public static final native void delete_granny_pnt333_vertex(long j);

    public static final native void delete_granny_pnt333_vertex_array(long j);

    public static final native void delete_granny_pntg3323_vertex(long j);

    public static final native void delete_granny_pntg3323_vertex_array(long j);

    public static final native void delete_granny_pointer_hash_array(long j);

    public static final native void delete_granny_pt32_vertex(long j);

    public static final native void delete_granny_pt32_vertex_array(long j);

    public static final native void delete_granny_pwn313_vertex(long j);

    public static final native void delete_granny_pwn313_vertex_array(long j);

    public static final native void delete_granny_pwn323_vertex(long j);

    public static final native void delete_granny_pwn323_vertex_array(long j);

    public static final native void delete_granny_pwn343_vertex(long j);

    public static final native void delete_granny_pwn343_vertex_array(long j);

    public static final native void delete_granny_pwng3133_vertex(long j);

    public static final native void delete_granny_pwng3133_vertex_array(long j);

    public static final native void delete_granny_pwng3233_vertex(long j);

    public static final native void delete_granny_pwng3233_vertex_array(long j);

    public static final native void delete_granny_pwng3433_vertex(long j);

    public static final native void delete_granny_pwng3433_vertex_array(long j);

    public static final native void delete_granny_pwngb31333_vertex(long j);

    public static final native void delete_granny_pwngb31333_vertex_array(long j);

    public static final native void delete_granny_pwngb32333_vertex(long j);

    public static final native void delete_granny_pwngb32333_vertex_array(long j);

    public static final native void delete_granny_pwngb34333_vertex(long j);

    public static final native void delete_granny_pwngb34333_vertex_array(long j);

    public static final native void delete_granny_pwngbt313332_vertex(long j);

    public static final native void delete_granny_pwngbt313332_vertex_array(long j);

    public static final native void delete_granny_pwngbt323332_vertex(long j);

    public static final native void delete_granny_pwngbt323332_vertex_array(long j);

    public static final native void delete_granny_pwngbt343332_vertex(long j);

    public static final native void delete_granny_pwngbt343332_vertex_array(long j);

    public static final native void delete_granny_pwngt31332_vertex(long j);

    public static final native void delete_granny_pwngt31332_vertex_array(long j);

    public static final native void delete_granny_pwngt32332_vertex(long j);

    public static final native void delete_granny_pwngt32332_vertex_array(long j);

    public static final native void delete_granny_pwngt34332_vertex(long j);

    public static final native void delete_granny_pwngt34332_vertex_array(long j);

    public static final native void delete_granny_pwnt3132_vertex(long j);

    public static final native void delete_granny_pwnt3132_vertex_array(long j);

    public static final native void delete_granny_pwnt3232_vertex(long j);

    public static final native void delete_granny_pwnt3232_vertex_array(long j);

    public static final native void delete_granny_pwnt3432_vertex(long j);

    public static final native void delete_granny_pwnt3432_vertex_array(long j);

    public static final native void delete_granny_sample_context(long j);

    public static final native void delete_granny_sample_context_array(long j);

    public static final native void delete_granny_skeleton(long j);

    public static final native void delete_granny_skeleton_array(long j);

    public static final native void delete_granny_skeleton_builder_array(long j);

    public static final native void delete_granny_stack_allocator(long j);

    public static final native void delete_granny_stack_allocator_array(long j);

    public static final native void delete_granny_stat_hud(long j);

    public static final native void delete_granny_stat_hud_alloc_point(long j);

    public static final native void delete_granny_stat_hud_alloc_point_array(long j);

    public static final native void delete_granny_stat_hud_animation_types(long j);

    public static final native void delete_granny_stat_hud_animation_types_array(long j);

    public static final native void delete_granny_stat_hud_array(long j);

    public static final native void delete_granny_stat_hud_footprint(long j);

    public static final native void delete_granny_stat_hud_footprint_array(long j);

    public static final native void delete_granny_stat_hud_model_controls(long j);

    public static final native void delete_granny_stat_hud_model_controls_array(long j);

    public static final native void delete_granny_stat_hud_model_instances(long j);

    public static final native void delete_granny_stat_hud_model_instances_array(long j);

    public static final native void delete_granny_stat_hud_perf(long j);

    public static final native void delete_granny_stat_hud_perf_array(long j);

    public static final native void delete_granny_stat_hud_perf_point(long j);

    public static final native void delete_granny_stat_hud_perf_point_array(long j);

    public static final native void delete_granny_string_database(long j);

    public static final native void delete_granny_string_database_array(long j);

    public static final native void delete_granny_string_table_array(long j);

    public static final native void delete_granny_system_clock(long j);

    public static final native void delete_granny_system_clock_array(long j);

    public static final native void delete_granny_tangent_frame(long j);

    public static final native void delete_granny_tangent_frame_array(long j);

    public static final native void delete_granny_text_track(long j);

    public static final native void delete_granny_text_track_array(long j);

    public static final native void delete_granny_text_track_entry(long j);

    public static final native void delete_granny_text_track_entry_array(long j);

    public static final native void delete_granny_texture(long j);

    public static final native void delete_granny_texture_array(long j);

    public static final native void delete_granny_texture_builder_array(long j);

    public static final native void delete_granny_texture_image(long j);

    public static final native void delete_granny_texture_image_array(long j);

    public static final native void delete_granny_texture_mip_level(long j);

    public static final native void delete_granny_texture_mip_level_array(long j);

    public static final native void delete_granny_track_group(long j);

    public static final native void delete_granny_track_group_array(long j);

    public static final native void delete_granny_track_group_builder_array(long j);

    public static final native void delete_granny_track_group_sampler_array(long j);

    public static final native void delete_granny_track_mask_array(long j);

    public static final native void delete_granny_transform(long j);

    public static final native void delete_granny_transform_array(long j);

    public static final native void delete_granny_transform_track(long j);

    public static final native void delete_granny_transform_track_array(long j);

    public static final native void delete_granny_tri_annotation_set(long j);

    public static final native void delete_granny_tri_annotation_set_array(long j);

    public static final native void delete_granny_tri_material_group(long j);

    public static final native void delete_granny_tri_material_group_array(long j);

    public static final native void delete_granny_tri_topology(long j);

    public static final native void delete_granny_tri_topology_array(long j);

    public static final native void delete_granny_triangle_intersection(long j);

    public static final native void delete_granny_triangle_intersection_array(long j);

    public static final native void delete_granny_unbound_track_mask(long j);

    public static final native void delete_granny_unbound_track_mask_array(long j);

    public static final native void delete_granny_unbound_weight(long j);

    public static final native void delete_granny_unbound_weight_array(long j);

    public static final native void delete_granny_variant(long j);

    public static final native void delete_granny_variant_array(long j);

    public static final native void delete_granny_variant_builder_array(long j);

    public static final native void delete_granny_vector_track(long j);

    public static final native void delete_granny_vector_track_array(long j);

    public static final native void delete_granny_vertex_annotation_set(long j);

    public static final native void delete_granny_vertex_annotation_set_array(long j);

    public static final native void delete_granny_vertex_data(long j);

    public static final native void delete_granny_vertex_data_array(long j);

    public static final native void delete_granny_vertex_weight_arrays(long j);

    public static final native void delete_granny_vertex_weight_arrays_array(long j);

    public static final native void delete_granny_world_pose_array(long j);

    public static final native float float_pointer_get(long j, int i);

    public static final native void float_pointer_set(long j, int i, float f);

    public static final native void getBoneTransform__SWIG_0(long j, Buffer buffer, Buffer buffer2);

    public static final native void getBoneTransform__SWIG_1(int i, Buffer buffer, Buffer buffer2);

    public static final native long getPNG333VertexType();

    public static final native void getWorldPoseArray(long j, int i, Buffer buffer);

    public static final native void getWorldPoseCompositeArray(long j, int i, Buffer buffer);

    public static final native long granny_allocated_block_array_getitem(long j, int i);

    public static final native void granny_allocated_block_array_setitem(long j, int i, long j2);

    public static final native long granny_allocation_header_array_getitem(long j, int i);

    public static final native void granny_allocation_header_array_setitem(long j, int i, long j2);

    public static final native int granny_allocation_information_ActualSize_get(long j);

    public static final native void granny_allocation_information_ActualSize_set(long j, int i);

    public static final native Buffer granny_allocation_information_Memory_get(long j);

    public static final native void granny_allocation_information_Memory_set(long j, Buffer buffer);

    public static final native int granny_allocation_information_RequestedSize_get(long j);

    public static final native void granny_allocation_information_RequestedSize_set(long j, int i);

    public static final native String granny_allocation_information_SourceFileName_get(long j);

    public static final native void granny_allocation_information_SourceFileName_set(long j, String str);

    public static final native int granny_allocation_information_SourceLineNumber_get(long j);

    public static final native void granny_allocation_information_SourceLineNumber_set(long j, int i);

    public static final native long granny_allocation_information_array_getitem(long j, int i);

    public static final native void granny_allocation_information_array_setitem(long j, int i, long j2);

    public static final native long granny_allocation_information_get(long j, int i);

    public static final native float granny_animation_Duration_get(long j);

    public static final native void granny_animation_Duration_set(long j, float f);

    public static final native String granny_animation_Name_get(long j);

    public static final native void granny_animation_Name_set(long j, String str);

    public static final native float granny_animation_Oversampling_get(long j);

    public static final native void granny_animation_Oversampling_set(long j, float f);

    public static final native float granny_animation_TimeStep_get(long j);

    public static final native void granny_animation_TimeStep_set(long j, float f);

    public static final native int granny_animation_TrackGroupCount_get(long j);

    public static final native void granny_animation_TrackGroupCount_set(long j, int i);

    public static final native long granny_animation_TrackGroups_get(long j);

    public static final native void granny_animation_TrackGroups_set(long j, long j2);

    public static final native long granny_animation_array_getitem(long j, int i);

    public static final native void granny_animation_array_setitem(long j, int i, long j2);

    public static final native long granny_animation_binding_ID_get(long j);

    public static final native void granny_animation_binding_ID_set(long j, long j2);

    public static final native long granny_animation_binding_Left_get(long j);

    public static final native void granny_animation_binding_Left_set(long j, long j2);

    public static final native long granny_animation_binding_NextUnused_get(long j);

    public static final native void granny_animation_binding_NextUnused_set(long j, long j2);

    public static final native long granny_animation_binding_PreviousUnused_get(long j);

    public static final native void granny_animation_binding_PreviousUnused_set(long j, long j2);

    public static final native long granny_animation_binding_RebasingMemory_get(long j);

    public static final native void granny_animation_binding_RebasingMemory_set(long j, long j2);

    public static final native long granny_animation_binding_Right_get(long j);

    public static final native void granny_animation_binding_Right_set(long j, long j2);

    public static final native int granny_animation_binding_TrackBindingCount_get(long j);

    public static final native void granny_animation_binding_TrackBindingCount_set(long j, int i);

    public static final native long granny_animation_binding_TrackBindings_get(long j);

    public static final native void granny_animation_binding_TrackBindings_set(long j, long j2);

    public static final native int granny_animation_binding_UsedBy_get(long j);

    public static final native void granny_animation_binding_UsedBy_set(long j, int i);

    public static final native long granny_animation_binding_array_getitem(long j, int i);

    public static final native void granny_animation_binding_array_setitem(long j, int i, long j2);

    public static final native int granny_animation_binding_cache_status_CacheHits_get(long j);

    public static final native void granny_animation_binding_cache_status_CacheHits_set(long j, int i);

    public static final native int granny_animation_binding_cache_status_CacheMisses_get(long j);

    public static final native void granny_animation_binding_cache_status_CacheMisses_set(long j, int i);

    /* renamed from: granny_animation_binding_cache_status_CurrentTotalBindingCount_get */
    public static final native int m45847x487fa84b(long j);

    /* renamed from: granny_animation_binding_cache_status_CurrentTotalBindingCount_set */
    public static final native void m45848x487fd557(long j, int i);

    /* renamed from: granny_animation_binding_cache_status_CurrentUsedBindingCount_get */
    public static final native int m45849xcfc30278(long j);

    /* renamed from: granny_animation_binding_cache_status_CurrentUsedBindingCount_set */
    public static final native void m45850xcfc32f84(long j, int i);

    public static final native int granny_animation_binding_cache_status_DirectAcquireCount_get(long j);

    public static final native void granny_animation_binding_cache_status_DirectAcquireCount_set(long j, int i);

    public static final native int granny_animation_binding_cache_status_ExplicitFlushCount_get(long j);

    public static final native void granny_animation_binding_cache_status_ExplicitFlushCount_set(long j, int i);

    public static final native int granny_animation_binding_cache_status_ExplicitFlushFrees_get(long j);

    public static final native void granny_animation_binding_cache_status_ExplicitFlushFrees_set(long j, int i);

    public static final native int granny_animation_binding_cache_status_IndirectAcquireCount_get(long j);

    public static final native void granny_animation_binding_cache_status_IndirectAcquireCount_set(long j, int i);

    public static final native int granny_animation_binding_cache_status_OverflowFrees_get(long j);

    public static final native void granny_animation_binding_cache_status_OverflowFrees_set(long j, int i);

    public static final native int granny_animation_binding_cache_status_ReleaseCount_get(long j);

    public static final native void granny_animation_binding_cache_status_ReleaseCount_set(long j, int i);

    public static final native int granny_animation_binding_cache_status_TotalBindingsCreated_get(long j);

    public static final native void granny_animation_binding_cache_status_TotalBindingsCreated_set(long j, int i);

    public static final native int granny_animation_binding_cache_status_TotalBindingsDestroyed_get(long j);

    public static final native void granny_animation_binding_cache_status_TotalBindingsDestroyed_set(long j, int i);

    public static final native long granny_animation_binding_cache_status_array_getitem(long j, int i);

    public static final native void granny_animation_binding_cache_status_array_setitem(long j, int i, long j2);

    public static final native long granny_animation_binding_cache_status_get(long j, int i);

    public static final native long granny_animation_binding_get(long j, int i);

    public static final native long granny_animation_binding_identifier_Animation_get(long j);

    public static final native void granny_animation_binding_identifier_Animation_set(long j, long j2);

    public static final native String granny_animation_binding_identifier_BonePattern_get(long j);

    public static final native void granny_animation_binding_identifier_BonePattern_set(long j, String str);

    public static final native long granny_animation_binding_identifier_FromBasis_get(long j);

    public static final native void granny_animation_binding_identifier_FromBasis_set(long j, long j2);

    public static final native long granny_animation_binding_identifier_OnModel_get(long j);

    public static final native void granny_animation_binding_identifier_OnModel_set(long j, long j2);

    public static final native int granny_animation_binding_identifier_SourceTrackGroupIndex_get(long j);

    public static final native void granny_animation_binding_identifier_SourceTrackGroupIndex_set(long j, int i);

    public static final native long granny_animation_binding_identifier_ToBasis_get(long j);

    public static final native void granny_animation_binding_identifier_ToBasis_set(long j, long j2);

    public static final native String granny_animation_binding_identifier_TrackPattern_get(long j);

    public static final native void granny_animation_binding_identifier_TrackPattern_set(long j, String str);

    public static final native long granny_animation_binding_identifier_array_getitem(long j, int i);

    public static final native void granny_animation_binding_identifier_array_setitem(long j, int i, long j2);

    public static final native long granny_animation_binding_identifier_get(long j, int i);

    public static final native long granny_animation_get(long j, int i);

    public static final native long granny_animation_lod_builder_array_getitem(long j, int i);

    public static final native void granny_animation_lod_builder_array_setitem(long j, int i, long j2);

    public static final native int granny_art_tool_info_ArtToolMajorRevision_get(long j);

    public static final native void granny_art_tool_info_ArtToolMajorRevision_set(long j, int i);

    public static final native int granny_art_tool_info_ArtToolMinorRevision_get(long j);

    public static final native void granny_art_tool_info_ArtToolMinorRevision_set(long j, int i);

    public static final native long granny_art_tool_info_BackVector_get(long j);

    public static final native void granny_art_tool_info_BackVector_set(long j, long j2);

    public static final native long granny_art_tool_info_ExtendedData_get(long j);

    public static final native void granny_art_tool_info_ExtendedData_set(long j, long j2);

    public static final native String granny_art_tool_info_FromArtToolName_get(long j);

    public static final native void granny_art_tool_info_FromArtToolName_set(long j, String str);

    public static final native long granny_art_tool_info_Origin_get(long j);

    public static final native void granny_art_tool_info_Origin_set(long j, long j2);

    public static final native long granny_art_tool_info_RightVector_get(long j);

    public static final native void granny_art_tool_info_RightVector_set(long j, long j2);

    public static final native float granny_art_tool_info_UnitsPerMeter_get(long j);

    public static final native void granny_art_tool_info_UnitsPerMeter_set(long j, float f);

    public static final native long granny_art_tool_info_UpVector_get(long j);

    public static final native void granny_art_tool_info_UpVector_set(long j, long j2);

    public static final native long granny_art_tool_info_array_getitem(long j, int i);

    public static final native void granny_art_tool_info_array_setitem(long j, int i, long j2);

    public static final native long granny_art_tool_info_get(long j, int i);

    public static final native long granny_blend_dag_node_array_getitem(long j, int i);

    public static final native void granny_blend_dag_node_array_setitem(long j, int i, long j2);

    public static final native long granny_bone_ExtendedData_get(long j);

    public static final native void granny_bone_ExtendedData_set(long j, long j2);

    public static final native long granny_bone_InverseWorld4x4_get(long j);

    public static final native void granny_bone_InverseWorld4x4_set(long j, long j2);

    public static final native float granny_bone_LODError_get(long j);

    public static final native void granny_bone_LODError_set(long j, float f);

    public static final native long granny_bone_LocalTransform_get(long j);

    public static final native void granny_bone_LocalTransform_set(long j, long j2);

    public static final native String granny_bone_Name_get(long j);

    public static final native void granny_bone_Name_set(long j, String str);

    public static final native int granny_bone_ParentIndex_get(long j);

    public static final native void granny_bone_ParentIndex_set(long j, int i);

    public static final native long granny_bone_array_getitem(long j, int i);

    public static final native void granny_bone_array_setitem(long j, int i, long j2);

    public static final native String granny_bone_binding_BoneName_get(long j);

    public static final native void granny_bone_binding_BoneName_set(long j, String str);

    public static final native long granny_bone_binding_OBBMax_get(long j);

    public static final native void granny_bone_binding_OBBMax_set(long j, long j2);

    public static final native long granny_bone_binding_OBBMin_get(long j);

    public static final native void granny_bone_binding_OBBMin_set(long j, long j2);

    public static final native int granny_bone_binding_TriangleCount_get(long j);

    public static final native void granny_bone_binding_TriangleCount_set(long j, int i);

    public static final native long granny_bone_binding_TriangleIndices_get(long j);

    public static final native void granny_bone_binding_TriangleIndices_set(long j, long j2);

    public static final native long granny_bone_binding_array_getitem(long j, int i);

    public static final native void granny_bone_binding_array_setitem(long j, int i, long j2);

    public static final native long granny_bone_binding_get(long j, int i);

    public static final native long granny_bone_get(long j, int i);

    public static final native char granny_bound_transform_track_Flags_get(long j);

    public static final native void granny_bound_transform_track_Flags_set(long j, char c);

    public static final native float granny_bound_transform_track_LODError_get(long j);

    public static final native void granny_bound_transform_track_LODError_set(long j, float f);

    public static final native long granny_bound_transform_track_LODTransform_get(long j);

    public static final native void granny_bound_transform_track_LODTransform_set(long j, long j2);

    public static final native char granny_bound_transform_track_QuaternionMode_get(long j);

    public static final native void granny_bound_transform_track_QuaternionMode_set(long j, char c);

    public static final native long granny_bound_transform_track_Sampler_get(long j);

    public static final native void granny_bound_transform_track_Sampler_set(long j, long j2);

    public static final native short granny_bound_transform_track_SourceTrackIndex_get(long j);

    public static final native void granny_bound_transform_track_SourceTrackIndex_set(long j, short s);

    public static final native long granny_bound_transform_track_SourceTrack_get(long j);

    public static final native void granny_bound_transform_track_SourceTrack_set(long j, long j2);

    public static final native long granny_bound_transform_track_array_getitem(long j, int i);

    public static final native void granny_bound_transform_track_array_setitem(long j, int i, long j2);

    public static final native long granny_bound_transform_track_get(long j, int i);

    public static final native long granny_box_intersection_MaxNormal_get(long j);

    public static final native void granny_box_intersection_MaxNormal_set(long j, long j2);

    public static final native float granny_box_intersection_MaxT_get(long j);

    public static final native void granny_box_intersection_MaxT_set(long j, float f);

    public static final native long granny_box_intersection_MinNormal_get(long j);

    public static final native void granny_box_intersection_MinNormal_set(long j, long j2);

    public static final native float granny_box_intersection_MinT_get(long j);

    public static final native void granny_box_intersection_MinT_set(long j, float f);

    public static final native long granny_box_intersection_array_getitem(long j, int i);

    public static final native void granny_box_intersection_array_setitem(long j, int i, long j2);

    public static final native long granny_box_intersection_get(long j, int i);

    public static final native float granny_bspline_error_AccumulatedSquaredError_get(long j);

    public static final native void granny_bspline_error_AccumulatedSquaredError_set(long j, float f);

    public static final native int granny_bspline_error_MaximumSquaredErrorKnotIndex_get(long j);

    public static final native void granny_bspline_error_MaximumSquaredErrorKnotIndex_set(long j, int i);

    public static final native int granny_bspline_error_MaximumSquaredErrorSampleIndex_get(long j);

    public static final native void granny_bspline_error_MaximumSquaredErrorSampleIndex_set(long j, int i);

    public static final native float granny_bspline_error_MaximumSquaredError_get(long j);

    public static final native void granny_bspline_error_MaximumSquaredError_set(long j, float f);

    public static final native long granny_bspline_error_array_getitem(long j, int i);

    public static final native void granny_bspline_error_array_setitem(long j, int i, long j2);

    public static final native long granny_bspline_error_get(long j, int i);

    public static final native long granny_bspline_solver_array_getitem(long j, int i);

    public static final native void granny_bspline_solver_array_setitem(long j, int i, long j2);

    public static final native long granny_camera_ElevAzimRoll_get(long j);

    public static final native void granny_camera_ElevAzimRoll_set(long j, long j2);

    public static final native float granny_camera_FOVY_get(long j);

    public static final native void granny_camera_FOVY_set(long j, float f);

    public static final native float granny_camera_FarClipPlane_get(long j);

    public static final native void granny_camera_FarClipPlane_set(long j, float f);

    public static final native long granny_camera_InverseProjection4x4_get(long j);

    public static final native void granny_camera_InverseProjection4x4_set(long j, long j2);

    public static final native long granny_camera_InverseView4x4_get(long j);

    public static final native void granny_camera_InverseView4x4_set(long j, long j2);

    public static final native float granny_camera_NearClipPlane_get(long j);

    public static final native void granny_camera_NearClipPlane_set(long j, float f);

    public static final native long granny_camera_Offset_get(long j);

    public static final native void granny_camera_Offset_set(long j, long j2);

    public static final native long granny_camera_OrientationMatrix_get(long j);

    public static final native void granny_camera_OrientationMatrix_set(long j, long j2);

    public static final native long granny_camera_Orientation_get(long j);

    public static final native void granny_camera_Orientation_set(long j, long j2);

    public static final native int granny_camera_OutputZRange_get(long j);

    public static final native void granny_camera_OutputZRange_set(long j, int i);

    public static final native long granny_camera_Position_get(long j);

    public static final native void granny_camera_Position_set(long j, long j2);

    public static final native long granny_camera_Projection4x4_get(long j);

    public static final native void granny_camera_Projection4x4_set(long j, long j2);

    public static final native boolean granny_camera_UseQuaternionOrientation_get(long j);

    public static final native void granny_camera_UseQuaternionOrientation_set(long j, boolean z);

    public static final native long granny_camera_View4x4_get(long j);

    public static final native void granny_camera_View4x4_set(long j, long j2);

    public static final native float granny_camera_WpOverHp_get(long j);

    public static final native void granny_camera_WpOverHp_set(long j, float f);

    public static final native float granny_camera_WrOverHr_get(long j);

    public static final native void granny_camera_WrOverHr_set(long j, float f);

    public static final native float granny_camera_WwOverHw_get(long j);

    public static final native void granny_camera_WwOverHw_set(long j, float f);

    public static final native float granny_camera_ZRangeEpsilon_get(long j);

    public static final native void granny_camera_ZRangeEpsilon_set(long j, float f);

    public static final native long granny_camera_array_getitem(long j, int i);

    public static final native void granny_camera_array_setitem(long j, int i, long j2);

    public static final native long granny_camera_get(long j, int i);

    public static final native boolean granny_compress_curve_parameters_AllowDegreeReduction_get(long j);

    public static final native void granny_compress_curve_parameters_AllowDegreeReduction_set(long j, boolean z);

    /* renamed from: granny_compress_curve_parameters_AllowReductionOnMissedTolerance_get */
    public static final native boolean m45851x3249edc(long j);

    /* renamed from: granny_compress_curve_parameters_AllowReductionOnMissedTolerance_set */
    public static final native void m45852x324cbe8(long j, boolean z);

    public static final native float granny_compress_curve_parameters_C0Threshold_get(long j);

    public static final native void granny_compress_curve_parameters_C0Threshold_set(long j, float f);

    public static final native float granny_compress_curve_parameters_C1Threshold_get(long j);

    public static final native void granny_compress_curve_parameters_C1Threshold_set(long j, float f);

    public static final native long granny_compress_curve_parameters_ConstantCompressionType_get(long j);

    public static final native void granny_compress_curve_parameters_ConstantCompressionType_set(long j, long j2);

    public static final native int granny_compress_curve_parameters_DesiredDegree_get(long j);

    public static final native void granny_compress_curve_parameters_DesiredDegree_set(long j, int i);

    public static final native float granny_compress_curve_parameters_ErrorTolerance_get(long j);

    public static final native void granny_compress_curve_parameters_ErrorTolerance_set(long j, float f);

    public static final native long granny_compress_curve_parameters_IdentityCompressionType_get(long j);

    public static final native void granny_compress_curve_parameters_IdentityCompressionType_set(long j, long j2);

    public static final native long granny_compress_curve_parameters_IdentityVector_get(long j);

    public static final native void granny_compress_curve_parameters_IdentityVector_set(long j, long j2);

    /* renamed from: granny_compress_curve_parameters_PossibleCompressionTypesCount_get */
    public static final native int m45853x3e3a4a3e(long j);

    /* renamed from: granny_compress_curve_parameters_PossibleCompressionTypesCount_set */
    public static final native void m45854x3e3a774a(long j, int i);

    public static final native long granny_compress_curve_parameters_PossibleCompressionTypes_get(long j);

    public static final native void granny_compress_curve_parameters_PossibleCompressionTypes_set(long j, long j2);

    public static final native long granny_compress_curve_parameters_array_getitem(long j, int i);

    public static final native void granny_compress_curve_parameters_array_setitem(long j, int i, long j2);

    public static final native long granny_compress_curve_parameters_get(long j, int i);

    public static final native long granny_control_array_getitem(long j, int i);

    public static final native void granny_control_array_setitem(long j, int i, long j2);

    public static final native long granny_controlled_animation_array_getitem(long j, int i);

    public static final native void granny_controlled_animation_array_setitem(long j, int i, long j2);

    public static final native long granny_controlled_animation_builder_array_getitem(long j, int i);

    public static final native void granny_controlled_animation_builder_array_setitem(long j, int i, long j2);

    public static final native long granny_controlled_pose_array_getitem(long j, int i);

    public static final native void granny_controlled_pose_array_setitem(long j, int i, long j2);

    public static final native int granny_counter_results_Count_get(long j);

    public static final native void granny_counter_results_Count_set(long j, int i);

    public static final native String granny_counter_results_Name_get(long j);

    public static final native void granny_counter_results_Name_set(long j, String str);

    public static final native int granny_counter_results_PeakCount_get(long j);

    public static final native void granny_counter_results_PeakCount_set(long j, int i);

    public static final native double granny_counter_results_PeakSecondsWithoutChildren_get(long j);

    public static final native void granny_counter_results_PeakSecondsWithoutChildren_set(long j, double d);

    public static final native double granny_counter_results_PeakTotalSeconds_get(long j);

    public static final native void granny_counter_results_PeakTotalSeconds_set(long j, double d);

    public static final native double granny_counter_results_SecondsWithoutChildren_get(long j);

    public static final native void granny_counter_results_SecondsWithoutChildren_set(long j, double d);

    public static final native double granny_counter_results_TotalCyclesWithoutChildren_get(long j);

    public static final native void granny_counter_results_TotalCyclesWithoutChildren_set(long j, double d);

    public static final native double granny_counter_results_TotalCycles_get(long j);

    public static final native void granny_counter_results_TotalCycles_set(long j, double d);

    public static final native double granny_counter_results_TotalSeconds_get(long j);

    public static final native void granny_counter_results_TotalSeconds_set(long j, double d);

    public static final native long granny_counter_results_array_getitem(long j, int i);

    public static final native void granny_counter_results_array_setitem(long j, int i, long j2);

    public static final native long granny_counter_results_get(long j, int i);

    public static final native long granny_curve2_CurveData_get(long j);

    public static final native void granny_curve2_CurveData_set(long j, long j2);

    public static final native long granny_curve2_array_getitem(long j, int i);

    public static final native void granny_curve2_array_setitem(long j, int i, long j2);

    public static final native long granny_curve2_get(long j, int i);

    public static final native long granny_curve_builder_array_getitem(long j, int i);

    public static final native void granny_curve_builder_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_d3_constant32f_Controls_get(long j);

    public static final native void granny_curve_data_d3_constant32f_Controls_set(long j, long j2);

    public static final native long granny_curve_data_d3_constant32f_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_d3_constant32f_CurveDataHeader_set(long j, long j2);

    public static final native short granny_curve_data_d3_constant32f_Padding_get(long j);

    public static final native void granny_curve_data_d3_constant32f_Padding_set(long j, short s);

    public static final native long granny_curve_data_d3_constant32f_array_getitem(long j, int i);

    public static final native void granny_curve_data_d3_constant32f_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_d3_constant32f_get(long j, int i);

    public static final native long granny_curve_data_d3_k16u_c16u_ControlOffsets_get(long j);

    public static final native void granny_curve_data_d3_k16u_c16u_ControlOffsets_set(long j, long j2);

    public static final native long granny_curve_data_d3_k16u_c16u_ControlScales_get(long j);

    public static final native void granny_curve_data_d3_k16u_c16u_ControlScales_set(long j, long j2);

    public static final native long granny_curve_data_d3_k16u_c16u_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_d3_k16u_c16u_CurveDataHeader_set(long j, long j2);

    public static final native int granny_curve_data_d3_k16u_c16u_KnotControlCount_get(long j);

    public static final native void granny_curve_data_d3_k16u_c16u_KnotControlCount_set(long j, int i);

    public static final native long granny_curve_data_d3_k16u_c16u_KnotsControls_get(long j);

    public static final native void granny_curve_data_d3_k16u_c16u_KnotsControls_set(long j, long j2);

    public static final native int granny_curve_data_d3_k16u_c16u_OneOverKnotScaleTrunc_get(long j);

    public static final native void granny_curve_data_d3_k16u_c16u_OneOverKnotScaleTrunc_set(long j, int i);

    public static final native long granny_curve_data_d3_k16u_c16u_array_getitem(long j, int i);

    public static final native void granny_curve_data_d3_k16u_c16u_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_d3_k16u_c16u_get(long j, int i);

    public static final native long granny_curve_data_d3_k8u_c8u_ControlOffsets_get(long j);

    public static final native void granny_curve_data_d3_k8u_c8u_ControlOffsets_set(long j, long j2);

    public static final native long granny_curve_data_d3_k8u_c8u_ControlScales_get(long j);

    public static final native void granny_curve_data_d3_k8u_c8u_ControlScales_set(long j, long j2);

    public static final native long granny_curve_data_d3_k8u_c8u_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_d3_k8u_c8u_CurveDataHeader_set(long j, long j2);

    public static final native int granny_curve_data_d3_k8u_c8u_KnotControlCount_get(long j);

    public static final native void granny_curve_data_d3_k8u_c8u_KnotControlCount_set(long j, int i);

    public static final native long granny_curve_data_d3_k8u_c8u_KnotsControls_get(long j);

    public static final native void granny_curve_data_d3_k8u_c8u_KnotsControls_set(long j, long j2);

    public static final native int granny_curve_data_d3_k8u_c8u_OneOverKnotScaleTrunc_get(long j);

    public static final native void granny_curve_data_d3_k8u_c8u_OneOverKnotScaleTrunc_set(long j, int i);

    public static final native long granny_curve_data_d3_k8u_c8u_array_getitem(long j, int i);

    public static final native void granny_curve_data_d3_k8u_c8u_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_d3_k8u_c8u_get(long j, int i);

    public static final native long granny_curve_data_d3i1_k16u_c16u_ControlOffsets_get(long j);

    public static final native void granny_curve_data_d3i1_k16u_c16u_ControlOffsets_set(long j, long j2);

    public static final native long granny_curve_data_d3i1_k16u_c16u_ControlScales_get(long j);

    public static final native void granny_curve_data_d3i1_k16u_c16u_ControlScales_set(long j, long j2);

    public static final native long granny_curve_data_d3i1_k16u_c16u_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_d3i1_k16u_c16u_CurveDataHeader_set(long j, long j2);

    public static final native int granny_curve_data_d3i1_k16u_c16u_KnotControlCount_get(long j);

    public static final native void granny_curve_data_d3i1_k16u_c16u_KnotControlCount_set(long j, int i);

    public static final native long granny_curve_data_d3i1_k16u_c16u_KnotsControls_get(long j);

    public static final native void granny_curve_data_d3i1_k16u_c16u_KnotsControls_set(long j, long j2);

    public static final native int granny_curve_data_d3i1_k16u_c16u_OneOverKnotScaleTrunc_get(long j);

    public static final native void granny_curve_data_d3i1_k16u_c16u_OneOverKnotScaleTrunc_set(long j, int i);

    public static final native long granny_curve_data_d3i1_k16u_c16u_array_getitem(long j, int i);

    public static final native void granny_curve_data_d3i1_k16u_c16u_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_d3i1_k16u_c16u_get(long j, int i);

    public static final native long granny_curve_data_d3i1_k32f_c32f_ControlOffsets_get(long j);

    public static final native void granny_curve_data_d3i1_k32f_c32f_ControlOffsets_set(long j, long j2);

    public static final native long granny_curve_data_d3i1_k32f_c32f_ControlScales_get(long j);

    public static final native void granny_curve_data_d3i1_k32f_c32f_ControlScales_set(long j, long j2);

    public static final native long granny_curve_data_d3i1_k32f_c32f_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_d3i1_k32f_c32f_CurveDataHeader_set(long j, long j2);

    public static final native int granny_curve_data_d3i1_k32f_c32f_KnotControlCount_get(long j);

    public static final native void granny_curve_data_d3i1_k32f_c32f_KnotControlCount_set(long j, int i);

    public static final native long granny_curve_data_d3i1_k32f_c32f_KnotsControls_get(long j);

    public static final native void granny_curve_data_d3i1_k32f_c32f_KnotsControls_set(long j, long j2);

    public static final native short granny_curve_data_d3i1_k32f_c32f_Padding_get(long j);

    public static final native void granny_curve_data_d3i1_k32f_c32f_Padding_set(long j, short s);

    public static final native long granny_curve_data_d3i1_k32f_c32f_array_getitem(long j, int i);

    public static final native void granny_curve_data_d3i1_k32f_c32f_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_d3i1_k32f_c32f_get(long j, int i);

    public static final native long granny_curve_data_d3i1_k8u_c8u_ControlOffsets_get(long j);

    public static final native void granny_curve_data_d3i1_k8u_c8u_ControlOffsets_set(long j, long j2);

    public static final native long granny_curve_data_d3i1_k8u_c8u_ControlScales_get(long j);

    public static final native void granny_curve_data_d3i1_k8u_c8u_ControlScales_set(long j, long j2);

    public static final native long granny_curve_data_d3i1_k8u_c8u_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_d3i1_k8u_c8u_CurveDataHeader_set(long j, long j2);

    public static final native int granny_curve_data_d3i1_k8u_c8u_KnotControlCount_get(long j);

    public static final native void granny_curve_data_d3i1_k8u_c8u_KnotControlCount_set(long j, int i);

    public static final native long granny_curve_data_d3i1_k8u_c8u_KnotsControls_get(long j);

    public static final native void granny_curve_data_d3i1_k8u_c8u_KnotsControls_set(long j, long j2);

    public static final native int granny_curve_data_d3i1_k8u_c8u_OneOverKnotScaleTrunc_get(long j);

    public static final native void granny_curve_data_d3i1_k8u_c8u_OneOverKnotScaleTrunc_set(long j, int i);

    public static final native long granny_curve_data_d3i1_k8u_c8u_array_getitem(long j, int i);

    public static final native void granny_curve_data_d3i1_k8u_c8u_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_d3i1_k8u_c8u_get(long j, int i);

    public static final native long granny_curve_data_d4_constant32f_Controls_get(long j);

    public static final native void granny_curve_data_d4_constant32f_Controls_set(long j, long j2);

    public static final native long granny_curve_data_d4_constant32f_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_d4_constant32f_CurveDataHeader_set(long j, long j2);

    public static final native short granny_curve_data_d4_constant32f_Padding_get(long j);

    public static final native void granny_curve_data_d4_constant32f_Padding_set(long j, short s);

    public static final native long granny_curve_data_d4_constant32f_array_getitem(long j, int i);

    public static final native void granny_curve_data_d4_constant32f_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_d4_constant32f_get(long j, int i);

    public static final native long granny_curve_data_d4n_k16u_c15u_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_d4n_k16u_c15u_CurveDataHeader_set(long j, long j2);

    public static final native int granny_curve_data_d4n_k16u_c15u_KnotControlCount_get(long j);

    public static final native void granny_curve_data_d4n_k16u_c15u_KnotControlCount_set(long j, int i);

    public static final native long granny_curve_data_d4n_k16u_c15u_KnotsControls_get(long j);

    public static final native void granny_curve_data_d4n_k16u_c15u_KnotsControls_set(long j, long j2);

    public static final native float granny_curve_data_d4n_k16u_c15u_OneOverKnotScale_get(long j);

    public static final native void granny_curve_data_d4n_k16u_c15u_OneOverKnotScale_set(long j, float f);

    public static final native int granny_curve_data_d4n_k16u_c15u_ScaleOffsetTableEntries_get(long j);

    public static final native void granny_curve_data_d4n_k16u_c15u_ScaleOffsetTableEntries_set(long j, int i);

    public static final native long granny_curve_data_d4n_k16u_c15u_array_getitem(long j, int i);

    public static final native void granny_curve_data_d4n_k16u_c15u_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_d4n_k16u_c15u_get(long j, int i);

    public static final native long granny_curve_data_d4n_k8u_c7u_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_d4n_k8u_c7u_CurveDataHeader_set(long j, long j2);

    public static final native int granny_curve_data_d4n_k8u_c7u_KnotControlCount_get(long j);

    public static final native void granny_curve_data_d4n_k8u_c7u_KnotControlCount_set(long j, int i);

    public static final native long granny_curve_data_d4n_k8u_c7u_KnotsControls_get(long j);

    public static final native void granny_curve_data_d4n_k8u_c7u_KnotsControls_set(long j, long j2);

    public static final native float granny_curve_data_d4n_k8u_c7u_OneOverKnotScale_get(long j);

    public static final native void granny_curve_data_d4n_k8u_c7u_OneOverKnotScale_set(long j, float f);

    public static final native int granny_curve_data_d4n_k8u_c7u_ScaleOffsetTableEntries_get(long j);

    public static final native void granny_curve_data_d4n_k8u_c7u_ScaleOffsetTableEntries_set(long j, int i);

    public static final native long granny_curve_data_d4n_k8u_c7u_array_getitem(long j, int i);

    public static final native void granny_curve_data_d4n_k8u_c7u_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_d4n_k8u_c7u_get(long j, int i);

    public static final native float granny_curve_data_d9i1_k16u_c16u_ControlOffset_get(long j);

    public static final native void granny_curve_data_d9i1_k16u_c16u_ControlOffset_set(long j, float f);

    public static final native float granny_curve_data_d9i1_k16u_c16u_ControlScale_get(long j);

    public static final native void granny_curve_data_d9i1_k16u_c16u_ControlScale_set(long j, float f);

    public static final native long granny_curve_data_d9i1_k16u_c16u_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_d9i1_k16u_c16u_CurveDataHeader_set(long j, long j2);

    public static final native int granny_curve_data_d9i1_k16u_c16u_KnotControlCount_get(long j);

    public static final native void granny_curve_data_d9i1_k16u_c16u_KnotControlCount_set(long j, int i);

    public static final native long granny_curve_data_d9i1_k16u_c16u_KnotsControls_get(long j);

    public static final native void granny_curve_data_d9i1_k16u_c16u_KnotsControls_set(long j, long j2);

    public static final native int granny_curve_data_d9i1_k16u_c16u_OneOverKnotScaleTrunc_get(long j);

    public static final native void granny_curve_data_d9i1_k16u_c16u_OneOverKnotScaleTrunc_set(long j, int i);

    public static final native long granny_curve_data_d9i1_k16u_c16u_array_getitem(long j, int i);

    public static final native void granny_curve_data_d9i1_k16u_c16u_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_d9i1_k16u_c16u_get(long j, int i);

    public static final native float granny_curve_data_d9i1_k8u_c8u_ControlOffset_get(long j);

    public static final native void granny_curve_data_d9i1_k8u_c8u_ControlOffset_set(long j, float f);

    public static final native float granny_curve_data_d9i1_k8u_c8u_ControlScale_get(long j);

    public static final native void granny_curve_data_d9i1_k8u_c8u_ControlScale_set(long j, float f);

    public static final native long granny_curve_data_d9i1_k8u_c8u_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_d9i1_k8u_c8u_CurveDataHeader_set(long j, long j2);

    public static final native int granny_curve_data_d9i1_k8u_c8u_KnotControlCount_get(long j);

    public static final native void granny_curve_data_d9i1_k8u_c8u_KnotControlCount_set(long j, int i);

    public static final native long granny_curve_data_d9i1_k8u_c8u_KnotsControls_get(long j);

    public static final native void granny_curve_data_d9i1_k8u_c8u_KnotsControls_set(long j, long j2);

    public static final native int granny_curve_data_d9i1_k8u_c8u_OneOverKnotScaleTrunc_get(long j);

    public static final native void granny_curve_data_d9i1_k8u_c8u_OneOverKnotScaleTrunc_set(long j, int i);

    public static final native long granny_curve_data_d9i1_k8u_c8u_array_getitem(long j, int i);

    public static final native void granny_curve_data_d9i1_k8u_c8u_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_d9i1_k8u_c8u_get(long j, int i);

    public static final native long granny_curve_data_d9i3_k16u_c16u_ControlOffsets_get(long j);

    public static final native void granny_curve_data_d9i3_k16u_c16u_ControlOffsets_set(long j, long j2);

    public static final native long granny_curve_data_d9i3_k16u_c16u_ControlScales_get(long j);

    public static final native void granny_curve_data_d9i3_k16u_c16u_ControlScales_set(long j, long j2);

    public static final native long granny_curve_data_d9i3_k16u_c16u_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_d9i3_k16u_c16u_CurveDataHeader_set(long j, long j2);

    public static final native int granny_curve_data_d9i3_k16u_c16u_KnotControlCount_get(long j);

    public static final native void granny_curve_data_d9i3_k16u_c16u_KnotControlCount_set(long j, int i);

    public static final native long granny_curve_data_d9i3_k16u_c16u_KnotsControls_get(long j);

    public static final native void granny_curve_data_d9i3_k16u_c16u_KnotsControls_set(long j, long j2);

    public static final native int granny_curve_data_d9i3_k16u_c16u_OneOverKnotScaleTrunc_get(long j);

    public static final native void granny_curve_data_d9i3_k16u_c16u_OneOverKnotScaleTrunc_set(long j, int i);

    public static final native long granny_curve_data_d9i3_k16u_c16u_array_getitem(long j, int i);

    public static final native void granny_curve_data_d9i3_k16u_c16u_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_d9i3_k16u_c16u_get(long j, int i);

    public static final native long granny_curve_data_d9i3_k8u_c8u_ControlOffsets_get(long j);

    public static final native void granny_curve_data_d9i3_k8u_c8u_ControlOffsets_set(long j, long j2);

    public static final native long granny_curve_data_d9i3_k8u_c8u_ControlScales_get(long j);

    public static final native void granny_curve_data_d9i3_k8u_c8u_ControlScales_set(long j, long j2);

    public static final native long granny_curve_data_d9i3_k8u_c8u_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_d9i3_k8u_c8u_CurveDataHeader_set(long j, long j2);

    public static final native int granny_curve_data_d9i3_k8u_c8u_KnotControlCount_get(long j);

    public static final native void granny_curve_data_d9i3_k8u_c8u_KnotControlCount_set(long j, int i);

    public static final native long granny_curve_data_d9i3_k8u_c8u_KnotsControls_get(long j);

    public static final native void granny_curve_data_d9i3_k8u_c8u_KnotsControls_set(long j, long j2);

    public static final native int granny_curve_data_d9i3_k8u_c8u_OneOverKnotScaleTrunc_get(long j);

    public static final native void granny_curve_data_d9i3_k8u_c8u_OneOverKnotScaleTrunc_set(long j, int i);

    public static final native long granny_curve_data_d9i3_k8u_c8u_array_getitem(long j, int i);

    public static final native void granny_curve_data_d9i3_k8u_c8u_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_d9i3_k8u_c8u_get(long j, int i);

    public static final native int granny_curve_data_da_constant32f_ControlCount_get(long j);

    public static final native void granny_curve_data_da_constant32f_ControlCount_set(long j, int i);

    public static final native long granny_curve_data_da_constant32f_Controls_get(long j);

    public static final native void granny_curve_data_da_constant32f_Controls_set(long j, long j2);

    public static final native long granny_curve_data_da_constant32f_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_da_constant32f_CurveDataHeader_set(long j, long j2);

    public static final native short granny_curve_data_da_constant32f_Padding_get(long j);

    public static final native void granny_curve_data_da_constant32f_Padding_set(long j, short s);

    public static final native long granny_curve_data_da_constant32f_array_getitem(long j, int i);

    public static final native void granny_curve_data_da_constant32f_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_da_constant32f_get(long j, int i);

    public static final native long granny_curve_data_da_identity_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_da_identity_CurveDataHeader_set(long j, long j2);

    public static final native short granny_curve_data_da_identity_Dimension_get(long j);

    public static final native void granny_curve_data_da_identity_Dimension_set(long j, short s);

    public static final native long granny_curve_data_da_identity_array_getitem(long j, int i);

    public static final native void granny_curve_data_da_identity_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_da_identity_get(long j, int i);

    public static final native int granny_curve_data_da_k16u_c16u_ControlScaleOffsetCount_get(long j);

    public static final native void granny_curve_data_da_k16u_c16u_ControlScaleOffsetCount_set(long j, int i);

    public static final native long granny_curve_data_da_k16u_c16u_ControlScaleOffsets_get(long j);

    public static final native void granny_curve_data_da_k16u_c16u_ControlScaleOffsets_set(long j, long j2);

    public static final native long granny_curve_data_da_k16u_c16u_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_da_k16u_c16u_CurveDataHeader_set(long j, long j2);

    public static final native int granny_curve_data_da_k16u_c16u_KnotControlCount_get(long j);

    public static final native void granny_curve_data_da_k16u_c16u_KnotControlCount_set(long j, int i);

    public static final native long granny_curve_data_da_k16u_c16u_KnotsControls_get(long j);

    public static final native void granny_curve_data_da_k16u_c16u_KnotsControls_set(long j, long j2);

    public static final native int granny_curve_data_da_k16u_c16u_OneOverKnotScaleTrunc_get(long j);

    public static final native void granny_curve_data_da_k16u_c16u_OneOverKnotScaleTrunc_set(long j, int i);

    public static final native long granny_curve_data_da_k16u_c16u_array_getitem(long j, int i);

    public static final native void granny_curve_data_da_k16u_c16u_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_da_k16u_c16u_get(long j, int i);

    public static final native int granny_curve_data_da_k32f_c32f_ControlCount_get(long j);

    public static final native void granny_curve_data_da_k32f_c32f_ControlCount_set(long j, int i);

    public static final native long granny_curve_data_da_k32f_c32f_Controls_get(long j);

    public static final native void granny_curve_data_da_k32f_c32f_Controls_set(long j, long j2);

    public static final native long granny_curve_data_da_k32f_c32f_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_da_k32f_c32f_CurveDataHeader_set(long j, long j2);

    public static final native int granny_curve_data_da_k32f_c32f_KnotCount_get(long j);

    public static final native void granny_curve_data_da_k32f_c32f_KnotCount_set(long j, int i);

    public static final native long granny_curve_data_da_k32f_c32f_Knots_get(long j);

    public static final native void granny_curve_data_da_k32f_c32f_Knots_set(long j, long j2);

    public static final native short granny_curve_data_da_k32f_c32f_Padding_get(long j);

    public static final native void granny_curve_data_da_k32f_c32f_Padding_set(long j, short s);

    public static final native long granny_curve_data_da_k32f_c32f_array_getitem(long j, int i);

    public static final native void granny_curve_data_da_k32f_c32f_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_da_k32f_c32f_get(long j, int i);

    public static final native int granny_curve_data_da_k8u_c8u_ControlScaleOffsetCount_get(long j);

    public static final native void granny_curve_data_da_k8u_c8u_ControlScaleOffsetCount_set(long j, int i);

    public static final native long granny_curve_data_da_k8u_c8u_ControlScaleOffsets_get(long j);

    public static final native void granny_curve_data_da_k8u_c8u_ControlScaleOffsets_set(long j, long j2);

    public static final native long granny_curve_data_da_k8u_c8u_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_da_k8u_c8u_CurveDataHeader_set(long j, long j2);

    public static final native int granny_curve_data_da_k8u_c8u_KnotControlCount_get(long j);

    public static final native void granny_curve_data_da_k8u_c8u_KnotControlCount_set(long j, int i);

    public static final native long granny_curve_data_da_k8u_c8u_KnotsControls_get(long j);

    public static final native void granny_curve_data_da_k8u_c8u_KnotsControls_set(long j, long j2);

    public static final native int granny_curve_data_da_k8u_c8u_OneOverKnotScaleTrunc_get(long j);

    public static final native void granny_curve_data_da_k8u_c8u_OneOverKnotScaleTrunc_set(long j, int i);

    public static final native long granny_curve_data_da_k8u_c8u_array_getitem(long j, int i);

    public static final native void granny_curve_data_da_k8u_c8u_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_da_k8u_c8u_get(long j, int i);

    public static final native int granny_curve_data_da_keyframes32f_ControlCount_get(long j);

    public static final native void granny_curve_data_da_keyframes32f_ControlCount_set(long j, int i);

    public static final native long granny_curve_data_da_keyframes32f_Controls_get(long j);

    public static final native void granny_curve_data_da_keyframes32f_Controls_set(long j, long j2);

    public static final native long granny_curve_data_da_keyframes32f_CurveDataHeader_get(long j);

    public static final native void granny_curve_data_da_keyframes32f_CurveDataHeader_set(long j, long j2);

    public static final native short granny_curve_data_da_keyframes32f_Dimension_get(long j);

    public static final native void granny_curve_data_da_keyframes32f_Dimension_set(long j, short s);

    public static final native long granny_curve_data_da_keyframes32f_array_getitem(long j, int i);

    public static final native void granny_curve_data_da_keyframes32f_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_da_keyframes32f_get(long j, int i);

    public static final native short granny_curve_data_header_Degree_get(long j);

    public static final native void granny_curve_data_header_Degree_set(long j, short s);

    public static final native short granny_curve_data_header_Format_get(long j);

    public static final native void granny_curve_data_header_Format_set(long j, short s);

    public static final native long granny_curve_data_header_array_getitem(long j, int i);

    public static final native void granny_curve_data_header_array_setitem(long j, int i, long j2);

    public static final native long granny_curve_data_header_get(long j, int i);

    public static final native long granny_dag_pose_cache_array_getitem(long j, int i);

    public static final native void granny_dag_pose_cache_array_setitem(long j, int i, long j2);

    public static final native int granny_data_type_definition_ArrayWidth_get(long j);

    public static final native void granny_data_type_definition_ArrayWidth_set(long j, int i);

    public static final native long granny_data_type_definition_Extra_get(long j);

    public static final native void granny_data_type_definition_Extra_set(long j, long j2);

    public static final native long granny_data_type_definition_Ignored__Ignored_get(long j);

    public static final native void granny_data_type_definition_Ignored__Ignored_set(long j, long j2);

    public static final native String granny_data_type_definition_Name_get(long j);

    public static final native void granny_data_type_definition_Name_set(long j, String str);

    public static final native long granny_data_type_definition_ReferenceType_get(long j);

    public static final native void granny_data_type_definition_ReferenceType_set(long j, long j2);

    public static final native int granny_data_type_definition_Type_get(long j);

    public static final native void granny_data_type_definition_Type_set(long j, int i);

    public static final native long granny_data_type_definition_allocate(int i);

    public static final native long granny_data_type_definition_array_getitem(long j, int i);

    public static final native void granny_data_type_definition_array_setitem(long j, int i, long j2);

    public static final native long granny_data_type_definition_get(long j, int i);

    public static final native long granny_data_type_definition_next(long j);

    public static final native void granny_data_type_definition_set(long j, int i, long j2);

    public static final native long granny_defined_type_Definition_get(long j);

    public static final native void granny_defined_type_Definition_set(long j, long j2);

    public static final native String granny_defined_type_Name_get(long j);

    public static final native void granny_defined_type_Name_set(long j, String str);

    public static final native int granny_defined_type_UIID_get(long j);

    public static final native void granny_defined_type_UIID_set(long j, int i);

    public static final native long granny_defined_type_array_getitem(long j, int i);

    public static final native void granny_defined_type_array_setitem(long j, int i, long j2);

    public static final native long granny_defined_type_get(long j, int i);

    public static final native int granny_exporter_info_ExporterBuildNumber_get(long j);

    public static final native void granny_exporter_info_ExporterBuildNumber_set(long j, int i);

    public static final native int granny_exporter_info_ExporterCustomization_get(long j);

    public static final native void granny_exporter_info_ExporterCustomization_set(long j, int i);

    public static final native int granny_exporter_info_ExporterMajorRevision_get(long j);

    public static final native void granny_exporter_info_ExporterMajorRevision_set(long j, int i);

    public static final native int granny_exporter_info_ExporterMinorRevision_get(long j);

    public static final native void granny_exporter_info_ExporterMinorRevision_set(long j, int i);

    public static final native String granny_exporter_info_ExporterName_get(long j);

    public static final native void granny_exporter_info_ExporterName_set(long j, String str);

    public static final native long granny_exporter_info_ExtendedData_get(long j);

    public static final native void granny_exporter_info_ExtendedData_set(long j, long j2);

    public static final native long granny_exporter_info_array_getitem(long j, int i);

    public static final native void granny_exporter_info_array_setitem(long j, int i, long j2);

    public static final native long granny_exporter_info_get(long j, int i);

    public static final native long granny_file_ConversionBuffer_get(long j);

    public static final native void granny_file_ConversionBuffer_set(long j, long j2);

    public static final native long granny_file_Header_get(long j);

    public static final native void granny_file_Header_set(long j, long j2);

    public static final native boolean granny_file_IsByteReversed_get(long j);

    public static final native void granny_file_IsByteReversed_set(long j, boolean z);

    public static final native long granny_file_IsUserMemory_get(long j);

    public static final native void granny_file_IsUserMemory_set(long j, long j2);

    public static final native long granny_file_Marshalled_get(long j);

    public static final native void granny_file_Marshalled_set(long j, long j2);

    public static final native int granny_file_SectionCount_get(long j);

    public static final native void granny_file_SectionCount_set(long j, int i);

    public static final native long granny_file_Sections_get(long j);

    public static final native void granny_file_Sections_set(long j, long j2);

    public static final native long granny_file_SourceMagicValue_get(long j);

    public static final native void granny_file_SourceMagicValue_set(long j, long j2);

    public static final native long granny_file_array_getitem(long j, int i);

    public static final native void granny_file_array_setitem(long j, int i, long j2);

    public static final native long granny_file_builder_array_getitem(long j, int i);

    public static final native void granny_file_builder_array_setitem(long j, int i, long j2);

    public static final native long granny_file_compressor_array_getitem(long j, int i);

    public static final native void granny_file_compressor_array_setitem(long j, int i, long j2);

    public static final native long granny_file_data_tree_writer_array_getitem(long j, int i);

    public static final native void granny_file_data_tree_writer_array_setitem(long j, int i, long j2);

    public static final native long granny_file_fixup_array_getitem(long j, int i);

    public static final native void granny_file_fixup_array_setitem(long j, int i, long j2);

    public static final native long granny_file_get(long j, int i);

    public static final native int granny_file_info_AnimationCount_get(long j);

    public static final native void granny_file_info_AnimationCount_set(long j, int i);

    public static final native long granny_file_info_Animations_get(long j);

    public static final native void granny_file_info_Animations_set(long j, long j2);

    public static final native long granny_file_info_ArtToolInfo_get(long j);

    public static final native void granny_file_info_ArtToolInfo_set(long j, long j2);

    public static final native long granny_file_info_ExporterInfo_get(long j);

    public static final native void granny_file_info_ExporterInfo_set(long j, long j2);

    public static final native long granny_file_info_ExtendedData_get(long j);

    public static final native void granny_file_info_ExtendedData_set(long j, long j2);

    public static final native String granny_file_info_FromFileName_get(long j);

    public static final native void granny_file_info_FromFileName_set(long j, String str);

    public static final native int granny_file_info_MaterialCount_get(long j);

    public static final native void granny_file_info_MaterialCount_set(long j, int i);

    public static final native long granny_file_info_Materials_get(long j);

    public static final native void granny_file_info_Materials_set(long j, long j2);

    public static final native int granny_file_info_MeshCount_get(long j);

    public static final native void granny_file_info_MeshCount_set(long j, int i);

    public static final native long granny_file_info_Meshes_get(long j);

    public static final native void granny_file_info_Meshes_set(long j, long j2);

    public static final native int granny_file_info_ModelCount_get(long j);

    public static final native void granny_file_info_ModelCount_set(long j, int i);

    public static final native long granny_file_info_Models_get(long j);

    public static final native void granny_file_info_Models_set(long j, long j2);

    public static final native int granny_file_info_SkeletonCount_get(long j);

    public static final native void granny_file_info_SkeletonCount_set(long j, int i);

    public static final native long granny_file_info_Skeletons_get(long j);

    public static final native void granny_file_info_Skeletons_set(long j, long j2);

    public static final native int granny_file_info_TextureCount_get(long j);

    public static final native void granny_file_info_TextureCount_set(long j, int i);

    public static final native long granny_file_info_Textures_get(long j);

    public static final native void granny_file_info_Textures_set(long j, long j2);

    public static final native int granny_file_info_TrackGroupCount_get(long j);

    public static final native void granny_file_info_TrackGroupCount_set(long j, int i);

    public static final native long granny_file_info_TrackGroups_get(long j);

    public static final native void granny_file_info_TrackGroups_set(long j, long j2);

    public static final native long granny_file_info_TriTopologies_get(long j);

    public static final native void granny_file_info_TriTopologies_set(long j, long j2);

    public static final native int granny_file_info_TriTopologyCount_get(long j);

    public static final native void granny_file_info_TriTopologyCount_set(long j, int i);

    public static final native int granny_file_info_VertexDataCount_get(long j);

    public static final native void granny_file_info_VertexDataCount_set(long j, int i);

    public static final native long granny_file_info_VertexDatas_get(long j);

    public static final native void granny_file_info_VertexDatas_set(long j, long j2);

    public static final native long granny_file_info_array_getitem(long j, int i);

    public static final native void granny_file_info_array_setitem(long j, int i, long j2);

    public static final native long granny_file_info_get(long j, int i);

    public static final native long granny_file_location_BufferIndex_get(long j);

    public static final native void granny_file_location_BufferIndex_set(long j, long j2);

    public static final native long granny_file_location_Offset_get(long j);

    public static final native void granny_file_location_Offset_set(long j, long j2);

    public static final native long granny_file_location_SectionIndex_get(long j);

    public static final native void granny_file_location_SectionIndex_set(long j, long j2);

    public static final native long granny_file_location_array_getitem(long j, int i);

    public static final native void granny_file_location_array_setitem(long j, int i, long j2);

    public static final native long granny_file_location_get(long j, int i);

    public static final native long granny_file_reader_CloseFileReaderCallback_get(long j);

    public static final native void granny_file_reader_CloseFileReaderCallback_set(long j, long j2);

    public static final native long granny_file_reader_ReadAtMostCallback_get(long j);

    public static final native void granny_file_reader_ReadAtMostCallback_set(long j, long j2);

    public static final native String granny_file_reader_SourceFileName_get(long j);

    public static final native void granny_file_reader_SourceFileName_set(long j, String str);

    public static final native int granny_file_reader_SourceLineNumber_get(long j);

    public static final native void granny_file_reader_SourceLineNumber_set(long j, int i);

    public static final native long granny_file_reader_array_getitem(long j, int i);

    public static final native void granny_file_reader_array_setitem(long j, int i, long j2);

    public static final native long granny_file_reader_get(long j, int i);

    public static final native long granny_file_writer_BeginCRCCallback_get(long j);

    public static final native void granny_file_writer_BeginCRCCallback_set(long j, long j2);

    public static final native long granny_file_writer_CRC_get(long j);

    public static final native void granny_file_writer_CRC_set(long j, long j2);

    public static final native boolean granny_file_writer_CRCing_get(long j);

    public static final native void granny_file_writer_CRCing_set(long j, boolean z);

    public static final native long granny_file_writer_DeleteFileWriterCallback_get(long j);

    public static final native void granny_file_writer_DeleteFileWriterCallback_set(long j, long j2);

    public static final native long granny_file_writer_EndCRCCallback_get(long j);

    public static final native void granny_file_writer_EndCRCCallback_set(long j, long j2);

    public static final native long granny_file_writer_SeekWriterCallback_get(long j);

    public static final native void granny_file_writer_SeekWriterCallback_set(long j, long j2);

    public static final native String granny_file_writer_SourceFileName_get(long j);

    public static final native void granny_file_writer_SourceFileName_set(long j, String str);

    public static final native int granny_file_writer_SourceLineNumber_get(long j);

    public static final native void granny_file_writer_SourceLineNumber_set(long j, int i);

    public static final native long granny_file_writer_WriteCallback_get(long j);

    public static final native void granny_file_writer_WriteCallback_set(long j, long j2);

    public static final native long granny_file_writer_array_getitem(long j, int i);

    public static final native void granny_file_writer_array_setitem(long j, int i, long j2);

    public static final native long granny_file_writer_get(long j, int i);

    public static final native long granny_fixed_allocator_Sentinel_get(long j);

    public static final native void granny_fixed_allocator_Sentinel_set(long j, long j2);

    public static final native int granny_fixed_allocator_UnitSize_get(long j);

    public static final native void granny_fixed_allocator_UnitSize_set(long j, int i);

    public static final native int granny_fixed_allocator_UnitsPerBlock_get(long j);

    public static final native void granny_fixed_allocator_UnitsPerBlock_set(long j, int i);

    public static final native long granny_fixed_allocator_array_getitem(long j, int i);

    public static final native void granny_fixed_allocator_array_setitem(long j, int i, long j2);

    public static final native long granny_fixed_allocator_block_FirstFreeUnit_get(long j);

    public static final native void granny_fixed_allocator_block_FirstFreeUnit_set(long j, long j2);

    public static final native long granny_fixed_allocator_block_Next_get(long j);

    public static final native void granny_fixed_allocator_block_Next_set(long j, long j2);

    public static final native long granny_fixed_allocator_block_Previous_get(long j);

    public static final native void granny_fixed_allocator_block_Previous_set(long j, long j2);

    public static final native int granny_fixed_allocator_block_UnitCount_get(long j);

    public static final native void granny_fixed_allocator_block_UnitCount_set(long j, int i);

    public static final native long granny_fixed_allocator_block_Units_get(long j);

    public static final native void granny_fixed_allocator_block_Units_set(long j, long j2);

    public static final native long granny_fixed_allocator_block_array_getitem(long j, int i);

    public static final native void granny_fixed_allocator_block_array_setitem(long j, int i, long j2);

    public static final native long granny_fixed_allocator_block_get(long j, int i);

    public static final native long granny_fixed_allocator_get(long j, int i);

    public static final native long granny_fixed_allocator_unit_Next_get(long j);

    public static final native void granny_fixed_allocator_unit_Next_set(long j, long j2);

    public static final native long granny_fixed_allocator_unit_array_getitem(long j, int i);

    public static final native void granny_fixed_allocator_unit_array_setitem(long j, int i, long j2);

    public static final native long granny_fixed_allocator_unit_get(long j, int i);

    public static final native long granny_grn_file_header_CRC_get(long j);

    public static final native void granny_grn_file_header_CRC_set(long j, long j2);

    public static final native long granny_grn_file_header_ExtraTags_get(long j);

    public static final native void granny_grn_file_header_ExtraTags_set(long j, long j2);

    public static final native long granny_grn_file_header_ReservedUnused_get(long j);

    public static final native void granny_grn_file_header_ReservedUnused_set(long j, long j2);

    public static final native long granny_grn_file_header_RootObjectTypeDefinition_get(long j);

    public static final native void granny_grn_file_header_RootObjectTypeDefinition_set(long j, long j2);

    public static final native long granny_grn_file_header_RootObject_get(long j);

    public static final native void granny_grn_file_header_RootObject_set(long j, long j2);

    public static final native long granny_grn_file_header_SectionArrayCount_get(long j);

    public static final native void granny_grn_file_header_SectionArrayCount_set(long j, long j2);

    public static final native long granny_grn_file_header_SectionArrayOffset_get(long j);

    public static final native void granny_grn_file_header_SectionArrayOffset_set(long j, long j2);

    public static final native long granny_grn_file_header_StringDatabaseCRC_get(long j);

    public static final native void granny_grn_file_header_StringDatabaseCRC_set(long j, long j2);

    public static final native long granny_grn_file_header_TotalSize_get(long j);

    public static final native void granny_grn_file_header_TotalSize_set(long j, long j2);

    public static final native long granny_grn_file_header_TypeTag_get(long j);

    public static final native void granny_grn_file_header_TypeTag_set(long j, long j2);

    public static final native long granny_grn_file_header_Version_get(long j);

    public static final native void granny_grn_file_header_Version_set(long j, long j2);

    public static final native long granny_grn_file_header_array_getitem(long j, int i);

    public static final native void granny_grn_file_header_array_setitem(long j, int i, long j2);

    public static final native long granny_grn_file_header_get(long j, int i);

    public static final native long granny_grn_file_magic_value_HeaderFormat_get(long j);

    public static final native void granny_grn_file_magic_value_HeaderFormat_set(long j, long j2);

    public static final native long granny_grn_file_magic_value_HeaderSize_get(long j);

    public static final native void granny_grn_file_magic_value_HeaderSize_set(long j, long j2);

    public static final native long granny_grn_file_magic_value_MagicValue_get(long j);

    public static final native void granny_grn_file_magic_value_MagicValue_set(long j, long j2);

    public static final native long granny_grn_file_magic_value_Reserved_get(long j);

    public static final native void granny_grn_file_magic_value_Reserved_set(long j, long j2);

    public static final native long granny_grn_file_magic_value_array_getitem(long j, int i);

    public static final native void granny_grn_file_magic_value_array_setitem(long j, int i, long j2);

    public static final native long granny_grn_file_magic_value_get(long j, int i);

    public static final native long granny_grn_mixed_marshalling_fixup_Count_get(long j);

    public static final native void granny_grn_mixed_marshalling_fixup_Count_set(long j, long j2);

    public static final native long granny_grn_mixed_marshalling_fixup_Offset_get(long j);

    public static final native void granny_grn_mixed_marshalling_fixup_Offset_set(long j, long j2);

    public static final native long granny_grn_mixed_marshalling_fixup_Type_get(long j);

    public static final native void granny_grn_mixed_marshalling_fixup_Type_set(long j, long j2);

    public static final native long granny_grn_mixed_marshalling_fixup_array_getitem(long j, int i);

    public static final native void granny_grn_mixed_marshalling_fixup_array_setitem(long j, int i, long j2);

    public static final native long granny_grn_mixed_marshalling_fixup_get(long j, int i);

    public static final native long granny_grn_pointer_fixup_FromOffset_get(long j);

    public static final native void granny_grn_pointer_fixup_FromOffset_set(long j, long j2);

    public static final native long granny_grn_pointer_fixup_To_get(long j);

    public static final native void granny_grn_pointer_fixup_To_set(long j, long j2);

    public static final native long granny_grn_pointer_fixup_array_getitem(long j, int i);

    public static final native void granny_grn_pointer_fixup_array_setitem(long j, int i, long j2);

    public static final native long granny_grn_pointer_fixup_get(long j, int i);

    public static final native long granny_grn_reference_Offset_get(long j);

    public static final native void granny_grn_reference_Offset_set(long j, long j2);

    public static final native long granny_grn_reference_SectionIndex_get(long j);

    public static final native void granny_grn_reference_SectionIndex_set(long j, long j2);

    public static final native long granny_grn_reference_array_getitem(long j, int i);

    public static final native void granny_grn_reference_array_setitem(long j, int i, long j2);

    public static final native long granny_grn_reference_get(long j, int i);

    public static final native long granny_grn_section_DataOffset_get(long j);

    public static final native void granny_grn_section_DataOffset_set(long j, long j2);

    public static final native long granny_grn_section_DataSize_get(long j);

    public static final native void granny_grn_section_DataSize_set(long j, long j2);

    public static final native long granny_grn_section_ExpandedDataSize_get(long j);

    public static final native void granny_grn_section_ExpandedDataSize_set(long j, long j2);

    public static final native long granny_grn_section_First16Bit_get(long j);

    public static final native void granny_grn_section_First16Bit_set(long j, long j2);

    public static final native long granny_grn_section_First8Bit_get(long j);

    public static final native void granny_grn_section_First8Bit_set(long j, long j2);

    public static final native long granny_grn_section_Format_get(long j);

    public static final native void granny_grn_section_Format_set(long j, long j2);

    public static final native long granny_grn_section_InternalAlignment_get(long j);

    public static final native void granny_grn_section_InternalAlignment_set(long j, long j2);

    public static final native long granny_grn_section_MixedMarshallingFixupArrayCount_get(long j);

    public static final native void granny_grn_section_MixedMarshallingFixupArrayCount_set(long j, long j2);

    public static final native long granny_grn_section_MixedMarshallingFixupArrayOffset_get(long j);

    public static final native void granny_grn_section_MixedMarshallingFixupArrayOffset_set(long j, long j2);

    public static final native long granny_grn_section_PointerFixupArrayCount_get(long j);

    public static final native void granny_grn_section_PointerFixupArrayCount_set(long j, long j2);

    public static final native long granny_grn_section_PointerFixupArrayOffset_get(long j);

    public static final native void granny_grn_section_PointerFixupArrayOffset_set(long j, long j2);

    public static final native long granny_grn_section_array_getitem(long j, int i);

    public static final native void granny_grn_section_array_setitem(long j, int i, long j2);

    public static final native long granny_grn_section_get(long j, int i);

    public static final native long granny_local_pose_array_getitem(long j, int i);

    public static final native void granny_local_pose_array_setitem(long j, int i, long j2);

    public static final native long granny_log_callback_Function_get(long j);

    public static final native void granny_log_callback_Function_set(long j, long j2);

    public static final native long granny_log_callback_UserData_get(long j);

    public static final native void granny_log_callback_UserData_set(long j, long j2);

    public static final native long granny_log_callback_array_getitem(long j, int i);

    public static final native void granny_log_callback_array_setitem(long j, int i, long j2);

    public static final native long granny_log_callback_get(long j, int i);

    public static final native long granny_material_ExtendedData_get(long j);

    public static final native void granny_material_ExtendedData_set(long j, long j2);

    public static final native int granny_material_MapCount_get(long j);

    public static final native void granny_material_MapCount_set(long j, int i);

    public static final native long granny_material_Maps_get(long j);

    public static final native void granny_material_Maps_set(long j, long j2);

    public static final native String granny_material_Name_get(long j);

    public static final native void granny_material_Name_set(long j, String str);

    public static final native long granny_material_Texture_get(long j);

    public static final native void granny_material_Texture_set(long j, long j2);

    public static final native long granny_material_array_getitem(long j, int i);

    public static final native void granny_material_array_setitem(long j, int i, long j2);

    public static final native long granny_material_binding_Material_get(long j);

    public static final native void granny_material_binding_Material_set(long j, long j2);

    public static final native long granny_material_binding_array_getitem(long j, int i);

    public static final native void granny_material_binding_array_setitem(long j, int i, long j2);

    public static final native long granny_material_binding_get(long j, int i);

    public static final native long granny_material_get(long j, int i);

    public static final native long granny_material_map_Material_get(long j);

    public static final native void granny_material_map_Material_set(long j, long j2);

    public static final native String granny_material_map_Usage_get(long j);

    public static final native void granny_material_map_Usage_set(long j, String str);

    public static final native long granny_material_map_array_getitem(long j, int i);

    public static final native void granny_material_map_array_setitem(long j, int i, long j2);

    public static final native long granny_material_map_get(long j, int i);

    public static final native long granny_memory_arena_array_getitem(long j, int i);

    public static final native void granny_memory_arena_array_setitem(long j, int i, long j2);

    public static final native int granny_mesh_BoneBindingCount_get(long j);

    public static final native void granny_mesh_BoneBindingCount_set(long j, int i);

    public static final native long granny_mesh_BoneBindings_get(long j);

    public static final native void granny_mesh_BoneBindings_set(long j, long j2);

    public static final native long granny_mesh_ExtendedData_get(long j);

    public static final native void granny_mesh_ExtendedData_set(long j, long j2);

    public static final native int granny_mesh_MaterialBindingCount_get(long j);

    public static final native void granny_mesh_MaterialBindingCount_set(long j, int i);

    public static final native long granny_mesh_MaterialBindings_get(long j);

    public static final native void granny_mesh_MaterialBindings_set(long j, long j2);

    public static final native int granny_mesh_MorphTargetCount_get(long j);

    public static final native void granny_mesh_MorphTargetCount_set(long j, int i);

    public static final native long granny_mesh_MorphTargets_get(long j);

    public static final native void granny_mesh_MorphTargets_set(long j, long j2);

    public static final native String granny_mesh_Name_get(long j);

    public static final native void granny_mesh_Name_set(long j, String str);

    public static final native long granny_mesh_PrimaryTopology_get(long j);

    public static final native void granny_mesh_PrimaryTopology_set(long j, long j2);

    public static final native long granny_mesh_PrimaryVertexData_get(long j);

    public static final native void granny_mesh_PrimaryVertexData_set(long j, long j2);

    public static final native long granny_mesh_array_getitem(long j, int i);

    public static final native void granny_mesh_array_setitem(long j, int i, long j2);

    public static final native long granny_mesh_binding_array_getitem(long j, int i);

    public static final native void granny_mesh_binding_array_setitem(long j, int i, long j2);

    public static final native long granny_mesh_builder_array_getitem(long j, int i);

    public static final native void granny_mesh_builder_array_setitem(long j, int i, long j2);

    public static final native long granny_mesh_deformer_array_getitem(long j, int i);

    public static final native void granny_mesh_deformer_array_setitem(long j, int i, long j2);

    public static final native long granny_mesh_get(long j, int i);

    public static final native long granny_model_InitialPlacement_get(long j);

    public static final native void granny_model_InitialPlacement_set(long j, long j2);

    public static final native int granny_model_MeshBindingCount_get(long j);

    public static final native void granny_model_MeshBindingCount_set(long j, int i);

    public static final native long granny_model_MeshBindings_get(long j);

    public static final native void granny_model_MeshBindings_set(long j, long j2);

    public static final native String granny_model_Name_get(long j);

    public static final native void granny_model_Name_set(long j, String str);

    public static final native long granny_model_Skeleton_get(long j);

    public static final native void granny_model_Skeleton_set(long j, long j2);

    public static final native long granny_model_array_getitem(long j, int i);

    public static final native void granny_model_array_setitem(long j, int i, long j2);

    public static final native long granny_model_control_binding_array_getitem(long j, int i);

    public static final native void granny_model_control_binding_array_setitem(long j, int i, long j2);

    public static final native long granny_model_get(long j, int i);

    public static final native long granny_model_instance_array_getitem(long j, int i);

    public static final native void granny_model_instance_array_setitem(long j, int i, long j2);

    public static final native long granny_model_mesh_binding_Mesh_get(long j);

    public static final native void granny_model_mesh_binding_Mesh_set(long j, long j2);

    public static final native long granny_model_mesh_binding_array_getitem(long j, int i);

    public static final native void granny_model_mesh_binding_array_setitem(long j, int i, long j2);

    public static final native long granny_model_mesh_binding_get(long j, int i);

    public static final native int granny_morph_target_DataIsDeltas_get(long j);

    public static final native void granny_morph_target_DataIsDeltas_set(long j, int i);

    public static final native String granny_morph_target_ScalarName_get(long j);

    public static final native void granny_morph_target_ScalarName_set(long j, String str);

    public static final native long granny_morph_target_VertexData_get(long j);

    public static final native void granny_morph_target_VertexData_set(long j, long j2);

    public static final native long granny_morph_target_array_getitem(long j, int i);

    public static final native void granny_morph_target_array_setitem(long j, int i, long j2);

    public static final native long granny_morph_target_get(long j, int i);

    public static final native int granny_old_curve_ControlCount_get(long j);

    public static final native void granny_old_curve_ControlCount_set(long j, int i);

    public static final native long granny_old_curve_Controls_get(long j);

    public static final native void granny_old_curve_Controls_set(long j, long j2);

    public static final native int granny_old_curve_Degree_get(long j);

    public static final native void granny_old_curve_Degree_set(long j, int i);

    public static final native int granny_old_curve_KnotCount_get(long j);

    public static final native void granny_old_curve_KnotCount_set(long j, int i);

    public static final native long granny_old_curve_Knots_get(long j);

    public static final native void granny_old_curve_Knots_set(long j, long j2);

    public static final native long granny_old_curve_array_getitem(long j, int i);

    public static final native void granny_old_curve_array_setitem(long j, int i, long j2);

    public static final native long granny_old_curve_get(long j, int i);

    public static final native long granny_oodle1_state_array_getitem(long j, int i);

    public static final native void granny_oodle1_state_array_setitem(long j, int i, long j2);

    public static final native long granny_p3_vertex_Position_get(long j);

    public static final native void granny_p3_vertex_Position_set(long j, long j2);

    public static final native long granny_p3_vertex_array_getitem(long j, int i);

    public static final native void granny_p3_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_p3_vertex_get(long j, int i);

    public static final native long granny_periodic_loop_Axis_get(long j);

    public static final native void granny_periodic_loop_Axis_set(long j, long j2);

    public static final native long granny_periodic_loop_BasisX_get(long j);

    public static final native void granny_periodic_loop_BasisX_set(long j, long j2);

    public static final native long granny_periodic_loop_BasisY_get(long j);

    public static final native void granny_periodic_loop_BasisY_set(long j, long j2);

    public static final native float granny_periodic_loop_Radius_get(long j);

    public static final native void granny_periodic_loop_Radius_set(long j, float f);

    public static final native long granny_periodic_loop_array_getitem(long j, int i);

    public static final native void granny_periodic_loop_array_setitem(long j, int i, long j2);

    public static final native float granny_periodic_loop_dAngle_get(long j);

    public static final native void granny_periodic_loop_dAngle_set(long j, float f);

    public static final native float granny_periodic_loop_dZ_get(long j);

    public static final native void granny_periodic_loop_dZ_set(long j, float f);

    public static final native long granny_periodic_loop_get(long j, int i);

    public static final native long granny_pixel_layout_BitsForComponent_get(long j);

    public static final native void granny_pixel_layout_BitsForComponent_set(long j, long j2);

    public static final native int granny_pixel_layout_BytesPerPixel_get(long j);

    public static final native void granny_pixel_layout_BytesPerPixel_set(long j, int i);

    public static final native long granny_pixel_layout_ShiftForComponent_get(long j);

    public static final native void granny_pixel_layout_ShiftForComponent_set(long j, long j2);

    public static final native long granny_pixel_layout_array_getitem(long j, int i);

    public static final native void granny_pixel_layout_array_setitem(long j, int i, long j2);

    public static final native long granny_pixel_layout_get(long j, int i);

    public static final native long granny_pn33_vertex_Normal_get(long j);

    public static final native void granny_pn33_vertex_Normal_set(long j, long j2);

    public static final native long granny_pn33_vertex_Position_get(long j);

    public static final native void granny_pn33_vertex_Position_set(long j, long j2);

    public static final native long granny_pn33_vertex_array_getitem(long j, int i);

    public static final native void granny_pn33_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pn33_vertex_get(long j, int i);

    public static final native long granny_png333_vertex_Normal_get(long j);

    public static final native void granny_png333_vertex_Normal_set(long j, long j2);

    public static final native long granny_png333_vertex_Position_get(long j);

    public static final native void granny_png333_vertex_Position_set(long j, long j2);

    public static final native long granny_png333_vertex_Tangent_get(long j);

    public static final native void granny_png333_vertex_Tangent_set(long j, long j2);

    public static final native long granny_png333_vertex_array_getitem(long j, int i);

    public static final native void granny_png333_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_png333_vertex_get(long j, int i);

    public static final native long granny_pngb3333_vertex_Binormal_get(long j);

    public static final native void granny_pngb3333_vertex_Binormal_set(long j, long j2);

    public static final native long granny_pngb3333_vertex_Normal_get(long j);

    public static final native void granny_pngb3333_vertex_Normal_set(long j, long j2);

    public static final native long granny_pngb3333_vertex_Position_get(long j);

    public static final native void granny_pngb3333_vertex_Position_set(long j, long j2);

    public static final native long granny_pngb3333_vertex_Tangent_get(long j);

    public static final native void granny_pngb3333_vertex_Tangent_set(long j, long j2);

    public static final native long granny_pngb3333_vertex_array_getitem(long j, int i);

    public static final native void granny_pngb3333_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pngb3333_vertex_get(long j, int i);

    public static final native long granny_pngbt33332_vertex_Binormal_get(long j);

    public static final native void granny_pngbt33332_vertex_Binormal_set(long j, long j2);

    public static final native long granny_pngbt33332_vertex_Normal_get(long j);

    public static final native void granny_pngbt33332_vertex_Normal_set(long j, long j2);

    public static final native long granny_pngbt33332_vertex_Position_get(long j);

    public static final native void granny_pngbt33332_vertex_Position_set(long j, long j2);

    public static final native long granny_pngbt33332_vertex_Tangent_get(long j);

    public static final native void granny_pngbt33332_vertex_Tangent_set(long j, long j2);

    public static final native long granny_pngbt33332_vertex_UV_get(long j);

    public static final native void granny_pngbt33332_vertex_UV_set(long j, long j2);

    public static final native long granny_pngbt33332_vertex_array_getitem(long j, int i);

    public static final native void granny_pngbt33332_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pngbt33332_vertex_get(long j, int i);

    public static final native long granny_pngbt33333_vertex_Binormal_get(long j);

    public static final native void granny_pngbt33333_vertex_Binormal_set(long j, long j2);

    public static final native long granny_pngbt33333_vertex_Normal_get(long j);

    public static final native void granny_pngbt33333_vertex_Normal_set(long j, long j2);

    public static final native long granny_pngbt33333_vertex_Position_get(long j);

    public static final native void granny_pngbt33333_vertex_Position_set(long j, long j2);

    public static final native long granny_pngbt33333_vertex_Tangent_get(long j);

    public static final native void granny_pngbt33333_vertex_Tangent_set(long j, long j2);

    public static final native long granny_pngbt33333_vertex_UVW_get(long j);

    public static final native void granny_pngbt33333_vertex_UVW_set(long j, long j2);

    public static final native long granny_pngbt33333_vertex_array_getitem(long j, int i);

    public static final native void granny_pngbt33333_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pngbt33333_vertex_get(long j, int i);

    public static final native long granny_pngt3332_vertex_Normal_get(long j);

    public static final native void granny_pngt3332_vertex_Normal_set(long j, long j2);

    public static final native long granny_pngt3332_vertex_Position_get(long j);

    public static final native void granny_pngt3332_vertex_Position_set(long j, long j2);

    public static final native long granny_pngt3332_vertex_Tangent_get(long j);

    public static final native void granny_pngt3332_vertex_Tangent_set(long j, long j2);

    public static final native long granny_pngt3332_vertex_UV_get(long j);

    public static final native void granny_pngt3332_vertex_UV_set(long j, long j2);

    public static final native long granny_pngt3332_vertex_array_getitem(long j, int i);

    public static final native void granny_pngt3332_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pngt3332_vertex_get(long j, int i);

    public static final native long granny_pnt332_vertex_Normal_get(long j);

    public static final native void granny_pnt332_vertex_Normal_set(long j, long j2);

    public static final native long granny_pnt332_vertex_Position_get(long j);

    public static final native void granny_pnt332_vertex_Position_set(long j, long j2);

    public static final native long granny_pnt332_vertex_UV_get(long j);

    public static final native void granny_pnt332_vertex_UV_set(long j, long j2);

    public static final native long granny_pnt332_vertex_array_getitem(long j, int i);

    public static final native void granny_pnt332_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pnt332_vertex_get(long j, int i);

    public static final native long granny_pnt333_vertex_Normal_get(long j);

    public static final native void granny_pnt333_vertex_Normal_set(long j, long j2);

    public static final native long granny_pnt333_vertex_Position_get(long j);

    public static final native void granny_pnt333_vertex_Position_set(long j, long j2);

    public static final native long granny_pnt333_vertex_UVW_get(long j);

    public static final native void granny_pnt333_vertex_UVW_set(long j, long j2);

    public static final native long granny_pnt333_vertex_array_getitem(long j, int i);

    public static final native void granny_pnt333_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pnt333_vertex_get(long j, int i);

    public static final native long granny_pntg3323_vertex_Normal_get(long j);

    public static final native void granny_pntg3323_vertex_Normal_set(long j, long j2);

    public static final native long granny_pntg3323_vertex_Position_get(long j);

    public static final native void granny_pntg3323_vertex_Position_set(long j, long j2);

    public static final native long granny_pntg3323_vertex_Tangent_get(long j);

    public static final native void granny_pntg3323_vertex_Tangent_set(long j, long j2);

    public static final native long granny_pntg3323_vertex_UV_get(long j);

    public static final native void granny_pntg3323_vertex_UV_set(long j, long j2);

    public static final native long granny_pntg3323_vertex_array_getitem(long j, int i);

    public static final native void granny_pntg3323_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pntg3323_vertex_get(long j, int i);

    public static final native long granny_pointer_hash_array_getitem(long j, int i);

    public static final native void granny_pointer_hash_array_setitem(long j, int i, long j2);

    public static final native long granny_pt32_vertex_Position_get(long j);

    public static final native void granny_pt32_vertex_Position_set(long j, long j2);

    public static final native long granny_pt32_vertex_UV_get(long j);

    public static final native void granny_pt32_vertex_UV_set(long j, long j2);

    public static final native long granny_pt32_vertex_array_getitem(long j, int i);

    public static final native void granny_pt32_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pt32_vertex_get(long j, int i);

    public static final native long granny_pwn313_vertex_BoneIndex_get(long j);

    public static final native void granny_pwn313_vertex_BoneIndex_set(long j, long j2);

    public static final native long granny_pwn313_vertex_Normal_get(long j);

    public static final native void granny_pwn313_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwn313_vertex_Position_get(long j);

    public static final native void granny_pwn313_vertex_Position_set(long j, long j2);

    public static final native long granny_pwn313_vertex_array_getitem(long j, int i);

    public static final native void granny_pwn313_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwn313_vertex_get(long j, int i);

    public static final native long granny_pwn323_vertex_BoneIndices_get(long j);

    public static final native void granny_pwn323_vertex_BoneIndices_set(long j, long j2);

    public static final native long granny_pwn323_vertex_BoneWeights_get(long j);

    public static final native void granny_pwn323_vertex_BoneWeights_set(long j, long j2);

    public static final native long granny_pwn323_vertex_Normal_get(long j);

    public static final native void granny_pwn323_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwn323_vertex_Position_get(long j);

    public static final native void granny_pwn323_vertex_Position_set(long j, long j2);

    public static final native long granny_pwn323_vertex_array_getitem(long j, int i);

    public static final native void granny_pwn323_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwn323_vertex_get(long j, int i);

    public static final native long granny_pwn343_vertex_BoneIndices_get(long j);

    public static final native void granny_pwn343_vertex_BoneIndices_set(long j, long j2);

    public static final native long granny_pwn343_vertex_BoneWeights_get(long j);

    public static final native void granny_pwn343_vertex_BoneWeights_set(long j, long j2);

    public static final native long granny_pwn343_vertex_Normal_get(long j);

    public static final native void granny_pwn343_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwn343_vertex_Position_get(long j);

    public static final native void granny_pwn343_vertex_Position_set(long j, long j2);

    public static final native long granny_pwn343_vertex_array_getitem(long j, int i);

    public static final native void granny_pwn343_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwn343_vertex_get(long j, int i);

    public static final native long granny_pwng3133_vertex_BoneIndex_get(long j);

    public static final native void granny_pwng3133_vertex_BoneIndex_set(long j, long j2);

    public static final native long granny_pwng3133_vertex_Normal_get(long j);

    public static final native void granny_pwng3133_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwng3133_vertex_Position_get(long j);

    public static final native void granny_pwng3133_vertex_Position_set(long j, long j2);

    public static final native long granny_pwng3133_vertex_Tangent_get(long j);

    public static final native void granny_pwng3133_vertex_Tangent_set(long j, long j2);

    public static final native long granny_pwng3133_vertex_array_getitem(long j, int i);

    public static final native void granny_pwng3133_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwng3133_vertex_get(long j, int i);

    public static final native long granny_pwng3233_vertex_BoneIndices_get(long j);

    public static final native void granny_pwng3233_vertex_BoneIndices_set(long j, long j2);

    public static final native long granny_pwng3233_vertex_BoneWeights_get(long j);

    public static final native void granny_pwng3233_vertex_BoneWeights_set(long j, long j2);

    public static final native long granny_pwng3233_vertex_Normal_get(long j);

    public static final native void granny_pwng3233_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwng3233_vertex_Position_get(long j);

    public static final native void granny_pwng3233_vertex_Position_set(long j, long j2);

    public static final native long granny_pwng3233_vertex_Tangent_get(long j);

    public static final native void granny_pwng3233_vertex_Tangent_set(long j, long j2);

    public static final native long granny_pwng3233_vertex_array_getitem(long j, int i);

    public static final native void granny_pwng3233_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwng3233_vertex_get(long j, int i);

    public static final native long granny_pwng3433_vertex_BoneIndices_get(long j);

    public static final native void granny_pwng3433_vertex_BoneIndices_set(long j, long j2);

    public static final native long granny_pwng3433_vertex_BoneWeights_get(long j);

    public static final native void granny_pwng3433_vertex_BoneWeights_set(long j, long j2);

    public static final native long granny_pwng3433_vertex_Normal_get(long j);

    public static final native void granny_pwng3433_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwng3433_vertex_Position_get(long j);

    public static final native void granny_pwng3433_vertex_Position_set(long j, long j2);

    public static final native long granny_pwng3433_vertex_Tangent_get(long j);

    public static final native void granny_pwng3433_vertex_Tangent_set(long j, long j2);

    public static final native long granny_pwng3433_vertex_array_getitem(long j, int i);

    public static final native void granny_pwng3433_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwng3433_vertex_get(long j, int i);

    public static final native long granny_pwngb31333_vertex_Binormal_get(long j);

    public static final native void granny_pwngb31333_vertex_Binormal_set(long j, long j2);

    public static final native long granny_pwngb31333_vertex_BoneIndex_get(long j);

    public static final native void granny_pwngb31333_vertex_BoneIndex_set(long j, long j2);

    public static final native long granny_pwngb31333_vertex_Normal_get(long j);

    public static final native void granny_pwngb31333_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwngb31333_vertex_Position_get(long j);

    public static final native void granny_pwngb31333_vertex_Position_set(long j, long j2);

    public static final native long granny_pwngb31333_vertex_Tangent_get(long j);

    public static final native void granny_pwngb31333_vertex_Tangent_set(long j, long j2);

    public static final native long granny_pwngb31333_vertex_array_getitem(long j, int i);

    public static final native void granny_pwngb31333_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwngb31333_vertex_get(long j, int i);

    public static final native long granny_pwngb32333_vertex_Binormal_get(long j);

    public static final native void granny_pwngb32333_vertex_Binormal_set(long j, long j2);

    public static final native long granny_pwngb32333_vertex_BoneIndices_get(long j);

    public static final native void granny_pwngb32333_vertex_BoneIndices_set(long j, long j2);

    public static final native long granny_pwngb32333_vertex_BoneWeights_get(long j);

    public static final native void granny_pwngb32333_vertex_BoneWeights_set(long j, long j2);

    public static final native long granny_pwngb32333_vertex_Normal_get(long j);

    public static final native void granny_pwngb32333_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwngb32333_vertex_Position_get(long j);

    public static final native void granny_pwngb32333_vertex_Position_set(long j, long j2);

    public static final native long granny_pwngb32333_vertex_Tangent_get(long j);

    public static final native void granny_pwngb32333_vertex_Tangent_set(long j, long j2);

    public static final native long granny_pwngb32333_vertex_array_getitem(long j, int i);

    public static final native void granny_pwngb32333_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwngb32333_vertex_get(long j, int i);

    public static final native long granny_pwngb34333_vertex_Binormal_get(long j);

    public static final native void granny_pwngb34333_vertex_Binormal_set(long j, long j2);

    public static final native long granny_pwngb34333_vertex_BoneIndices_get(long j);

    public static final native void granny_pwngb34333_vertex_BoneIndices_set(long j, long j2);

    public static final native long granny_pwngb34333_vertex_BoneWeights_get(long j);

    public static final native void granny_pwngb34333_vertex_BoneWeights_set(long j, long j2);

    public static final native long granny_pwngb34333_vertex_Normal_get(long j);

    public static final native void granny_pwngb34333_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwngb34333_vertex_Position_get(long j);

    public static final native void granny_pwngb34333_vertex_Position_set(long j, long j2);

    public static final native long granny_pwngb34333_vertex_Tangent_get(long j);

    public static final native void granny_pwngb34333_vertex_Tangent_set(long j, long j2);

    public static final native long granny_pwngb34333_vertex_array_getitem(long j, int i);

    public static final native void granny_pwngb34333_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwngb34333_vertex_get(long j, int i);

    public static final native long granny_pwngbt313332_vertex_Binormal_get(long j);

    public static final native void granny_pwngbt313332_vertex_Binormal_set(long j, long j2);

    public static final native long granny_pwngbt313332_vertex_BoneIndex_get(long j);

    public static final native void granny_pwngbt313332_vertex_BoneIndex_set(long j, long j2);

    public static final native long granny_pwngbt313332_vertex_Normal_get(long j);

    public static final native void granny_pwngbt313332_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwngbt313332_vertex_Position_get(long j);

    public static final native void granny_pwngbt313332_vertex_Position_set(long j, long j2);

    public static final native long granny_pwngbt313332_vertex_Tangent_get(long j);

    public static final native void granny_pwngbt313332_vertex_Tangent_set(long j, long j2);

    public static final native long granny_pwngbt313332_vertex_UV_get(long j);

    public static final native void granny_pwngbt313332_vertex_UV_set(long j, long j2);

    public static final native long granny_pwngbt313332_vertex_array_getitem(long j, int i);

    public static final native void granny_pwngbt313332_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwngbt313332_vertex_get(long j, int i);

    public static final native long granny_pwngbt323332_vertex_Binormal_get(long j);

    public static final native void granny_pwngbt323332_vertex_Binormal_set(long j, long j2);

    public static final native long granny_pwngbt323332_vertex_BoneIndices_get(long j);

    public static final native void granny_pwngbt323332_vertex_BoneIndices_set(long j, long j2);

    public static final native long granny_pwngbt323332_vertex_BoneWeights_get(long j);

    public static final native void granny_pwngbt323332_vertex_BoneWeights_set(long j, long j2);

    public static final native long granny_pwngbt323332_vertex_Normal_get(long j);

    public static final native void granny_pwngbt323332_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwngbt323332_vertex_Position_get(long j);

    public static final native void granny_pwngbt323332_vertex_Position_set(long j, long j2);

    public static final native long granny_pwngbt323332_vertex_Tangent_get(long j);

    public static final native void granny_pwngbt323332_vertex_Tangent_set(long j, long j2);

    public static final native long granny_pwngbt323332_vertex_UV_get(long j);

    public static final native void granny_pwngbt323332_vertex_UV_set(long j, long j2);

    public static final native long granny_pwngbt323332_vertex_array_getitem(long j, int i);

    public static final native void granny_pwngbt323332_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwngbt323332_vertex_get(long j, int i);

    public static final native long granny_pwngbt343332_vertex_Binormal_get(long j);

    public static final native void granny_pwngbt343332_vertex_Binormal_set(long j, long j2);

    public static final native long granny_pwngbt343332_vertex_BoneIndices_get(long j);

    public static final native void granny_pwngbt343332_vertex_BoneIndices_set(long j, long j2);

    public static final native long granny_pwngbt343332_vertex_BoneWeights_get(long j);

    public static final native void granny_pwngbt343332_vertex_BoneWeights_set(long j, long j2);

    public static final native long granny_pwngbt343332_vertex_Normal_get(long j);

    public static final native void granny_pwngbt343332_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwngbt343332_vertex_Position_get(long j);

    public static final native void granny_pwngbt343332_vertex_Position_set(long j, long j2);

    public static final native long granny_pwngbt343332_vertex_Tangent_get(long j);

    public static final native void granny_pwngbt343332_vertex_Tangent_set(long j, long j2);

    public static final native long granny_pwngbt343332_vertex_UV_get(long j);

    public static final native void granny_pwngbt343332_vertex_UV_set(long j, long j2);

    public static final native long granny_pwngbt343332_vertex_array_getitem(long j, int i);

    public static final native void granny_pwngbt343332_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwngbt343332_vertex_get(long j, int i);

    public static final native long granny_pwngt31332_vertex_BoneIndex_get(long j);

    public static final native void granny_pwngt31332_vertex_BoneIndex_set(long j, long j2);

    public static final native long granny_pwngt31332_vertex_Normal_get(long j);

    public static final native void granny_pwngt31332_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwngt31332_vertex_Position_get(long j);

    public static final native void granny_pwngt31332_vertex_Position_set(long j, long j2);

    public static final native long granny_pwngt31332_vertex_Tangent_get(long j);

    public static final native void granny_pwngt31332_vertex_Tangent_set(long j, long j2);

    public static final native long granny_pwngt31332_vertex_UV_get(long j);

    public static final native void granny_pwngt31332_vertex_UV_set(long j, long j2);

    public static final native long granny_pwngt31332_vertex_array_getitem(long j, int i);

    public static final native void granny_pwngt31332_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwngt31332_vertex_get(long j, int i);

    public static final native long granny_pwngt32332_vertex_BoneIndices_get(long j);

    public static final native void granny_pwngt32332_vertex_BoneIndices_set(long j, long j2);

    public static final native long granny_pwngt32332_vertex_BoneWeights_get(long j);

    public static final native void granny_pwngt32332_vertex_BoneWeights_set(long j, long j2);

    public static final native long granny_pwngt32332_vertex_Normal_get(long j);

    public static final native void granny_pwngt32332_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwngt32332_vertex_Position_get(long j);

    public static final native void granny_pwngt32332_vertex_Position_set(long j, long j2);

    public static final native long granny_pwngt32332_vertex_Tangent_get(long j);

    public static final native void granny_pwngt32332_vertex_Tangent_set(long j, long j2);

    public static final native long granny_pwngt32332_vertex_UV_get(long j);

    public static final native void granny_pwngt32332_vertex_UV_set(long j, long j2);

    public static final native long granny_pwngt32332_vertex_array_getitem(long j, int i);

    public static final native void granny_pwngt32332_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwngt32332_vertex_get(long j, int i);

    public static final native long granny_pwngt34332_vertex_BoneIndices_get(long j);

    public static final native void granny_pwngt34332_vertex_BoneIndices_set(long j, long j2);

    public static final native long granny_pwngt34332_vertex_BoneWeights_get(long j);

    public static final native void granny_pwngt34332_vertex_BoneWeights_set(long j, long j2);

    public static final native long granny_pwngt34332_vertex_Normal_get(long j);

    public static final native void granny_pwngt34332_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwngt34332_vertex_Position_get(long j);

    public static final native void granny_pwngt34332_vertex_Position_set(long j, long j2);

    public static final native long granny_pwngt34332_vertex_Tangent_get(long j);

    public static final native void granny_pwngt34332_vertex_Tangent_set(long j, long j2);

    public static final native long granny_pwngt34332_vertex_UV_get(long j);

    public static final native void granny_pwngt34332_vertex_UV_set(long j, long j2);

    public static final native long granny_pwngt34332_vertex_array_getitem(long j, int i);

    public static final native void granny_pwngt34332_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwngt34332_vertex_get(long j, int i);

    public static final native long granny_pwnt3132_vertex_BoneIndex_get(long j);

    public static final native void granny_pwnt3132_vertex_BoneIndex_set(long j, long j2);

    public static final native long granny_pwnt3132_vertex_Normal_get(long j);

    public static final native void granny_pwnt3132_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwnt3132_vertex_Position_get(long j);

    public static final native void granny_pwnt3132_vertex_Position_set(long j, long j2);

    public static final native long granny_pwnt3132_vertex_UV_get(long j);

    public static final native void granny_pwnt3132_vertex_UV_set(long j, long j2);

    public static final native long granny_pwnt3132_vertex_array_getitem(long j, int i);

    public static final native void granny_pwnt3132_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwnt3132_vertex_get(long j, int i);

    public static final native long granny_pwnt3232_vertex_BoneIndices_get(long j);

    public static final native void granny_pwnt3232_vertex_BoneIndices_set(long j, long j2);

    public static final native long granny_pwnt3232_vertex_BoneWeights_get(long j);

    public static final native void granny_pwnt3232_vertex_BoneWeights_set(long j, long j2);

    public static final native long granny_pwnt3232_vertex_Normal_get(long j);

    public static final native void granny_pwnt3232_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwnt3232_vertex_Position_get(long j);

    public static final native void granny_pwnt3232_vertex_Position_set(long j, long j2);

    public static final native long granny_pwnt3232_vertex_UV_get(long j);

    public static final native void granny_pwnt3232_vertex_UV_set(long j, long j2);

    public static final native long granny_pwnt3232_vertex_array_getitem(long j, int i);

    public static final native void granny_pwnt3232_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwnt3232_vertex_get(long j, int i);

    public static final native long granny_pwnt3432_vertex_BoneIndices_get(long j);

    public static final native void granny_pwnt3432_vertex_BoneIndices_set(long j, long j2);

    public static final native long granny_pwnt3432_vertex_BoneWeights_get(long j);

    public static final native void granny_pwnt3432_vertex_BoneWeights_set(long j, long j2);

    public static final native long granny_pwnt3432_vertex_Normal_get(long j);

    public static final native void granny_pwnt3432_vertex_Normal_set(long j, long j2);

    public static final native long granny_pwnt3432_vertex_Position_get(long j);

    public static final native void granny_pwnt3432_vertex_Position_set(long j, long j2);

    public static final native long granny_pwnt3432_vertex_UV_get(long j);

    public static final native void granny_pwnt3432_vertex_UV_set(long j, long j2);

    public static final native long granny_pwnt3432_vertex_array_getitem(long j, int i);

    public static final native void granny_pwnt3432_vertex_array_setitem(long j, int i, long j2);

    public static final native long granny_pwnt3432_vertex_get(long j, int i);

    public static final native int granny_sample_context_FrameIndex_get(long j);

    public static final native void granny_sample_context_FrameIndex_set(long j, int i);

    public static final native float granny_sample_context_LocalClock_get(long j);

    public static final native void granny_sample_context_LocalClock_set(long j, float f);

    public static final native float granny_sample_context_LocalDuration_get(long j);

    public static final native void granny_sample_context_LocalDuration_set(long j, float f);

    public static final native boolean granny_sample_context_OverflowLoop_get(long j);

    public static final native void granny_sample_context_OverflowLoop_set(long j, boolean z);

    public static final native boolean granny_sample_context_UnderflowLoop_get(long j);

    public static final native void granny_sample_context_UnderflowLoop_set(long j, boolean z);

    public static final native long granny_sample_context_array_getitem(long j, int i);

    public static final native void granny_sample_context_array_setitem(long j, int i, long j2);

    public static final native long granny_sample_context_get(long j, int i);

    public static final native int granny_skeleton_BoneCount_get(long j);

    public static final native void granny_skeleton_BoneCount_set(long j, int i);

    public static final native long granny_skeleton_Bones_get(long j);

    public static final native void granny_skeleton_Bones_set(long j, long j2);

    public static final native int granny_skeleton_LODType_get(long j);

    public static final native void granny_skeleton_LODType_set(long j, int i);

    public static final native String granny_skeleton_Name_get(long j);

    public static final native void granny_skeleton_Name_set(long j, String str);

    public static final native long granny_skeleton_array_getitem(long j, int i);

    public static final native void granny_skeleton_array_setitem(long j, int i, long j2);

    public static final native long granny_skeleton_builder_array_getitem(long j, int i);

    public static final native void granny_skeleton_builder_array_setitem(long j, int i, long j2);

    public static final native int granny_skeleton_findBoneByName(long j, String str);

    public static final native int granny_skeleton_findBoneByNameLowercase(long j, String str);

    public static final native long granny_skeleton_get(long j, int i);

    public static final native int granny_stack_allocator_ActiveBlocks_get(long j);

    public static final native void granny_stack_allocator_ActiveBlocks_set(long j, int i);

    public static final native long granny_stack_allocator_BlockDirectory_get(long j);

    public static final native void granny_stack_allocator_BlockDirectory_set(long j, long j2);

    public static final native long granny_stack_allocator_LastBlock_get(long j);

    public static final native void granny_stack_allocator_LastBlock_set(long j, long j2);

    public static final native int granny_stack_allocator_MaxActiveBlocks_get(long j);

    public static final native void granny_stack_allocator_MaxActiveBlocks_set(long j, int i);

    public static final native int granny_stack_allocator_MaxUnits_get(long j);

    public static final native void granny_stack_allocator_MaxUnits_set(long j, int i);

    public static final native int granny_stack_allocator_TotalUsedUnitCount_get(long j);

    public static final native void granny_stack_allocator_TotalUsedUnitCount_set(long j, int i);

    public static final native int granny_stack_allocator_UnitSize_get(long j);

    public static final native void granny_stack_allocator_UnitSize_set(long j, int i);

    public static final native int granny_stack_allocator_UnitsPerBlock_get(long j);

    public static final native void granny_stack_allocator_UnitsPerBlock_set(long j, int i);

    public static final native long granny_stack_allocator_array_getitem(long j, int i);

    public static final native void granny_stack_allocator_array_setitem(long j, int i, long j2);

    public static final native long granny_stack_allocator_get(long j, int i);

    public static final native int granny_stat_hud_AllocPointCount_get(long j);

    public static final native void granny_stat_hud_AllocPointCount_set(long j, int i);

    public static final native long granny_stat_hud_AllocPoints_get(long j);

    public static final native void granny_stat_hud_AllocPoints_set(long j, long j2);

    public static final native long granny_stat_hud_AnimBindingCacheStatus_get(long j);

    public static final native void granny_stat_hud_AnimBindingCacheStatus_set(long j, long j2);

    public static final native long granny_stat_hud_AnimTypes_get(long j);

    public static final native void granny_stat_hud_AnimTypes_set(long j, long j2);

    public static final native long granny_stat_hud_Controls_get(long j);

    public static final native void granny_stat_hud_Controls_set(long j, long j2);

    public static final native long granny_stat_hud_Footprint_get(long j);

    public static final native void granny_stat_hud_Footprint_set(long j, long j2);

    public static final native int granny_stat_hud_FrameCount_get(long j);

    public static final native void granny_stat_hud_FrameCount_set(long j, int i);

    public static final native long granny_stat_hud_ModelInstances_get(long j);

    public static final native void granny_stat_hud_ModelInstances_set(long j, long j2);

    public static final native int granny_stat_hud_PerfPointCount_get(long j);

    public static final native void granny_stat_hud_PerfPointCount_set(long j, int i);

    public static final native long granny_stat_hud_PerfPoints_get(long j);

    public static final native void granny_stat_hud_PerfPoints_set(long j, long j2);

    public static final native long granny_stat_hud_Perf_get(long j);

    public static final native void granny_stat_hud_Perf_set(long j, long j2);

    public static final native int granny_stat_hud_alloc_point_AllocationCount_get(long j);

    public static final native void granny_stat_hud_alloc_point_AllocationCount_set(long j, int i);

    public static final native int granny_stat_hud_alloc_point_BytesAllocated_get(long j);

    public static final native void granny_stat_hud_alloc_point_BytesAllocated_set(long j, int i);

    public static final native int granny_stat_hud_alloc_point_BytesRequested_get(long j);

    public static final native void granny_stat_hud_alloc_point_BytesRequested_set(long j, int i);

    public static final native String granny_stat_hud_alloc_point_SourceFilename_get(long j);

    public static final native void granny_stat_hud_alloc_point_SourceFilename_set(long j, String str);

    public static final native int granny_stat_hud_alloc_point_SourceLineNumber_get(long j);

    public static final native void granny_stat_hud_alloc_point_SourceLineNumber_set(long j, int i);

    public static final native long granny_stat_hud_alloc_point_array_getitem(long j, int i);

    public static final native void granny_stat_hud_alloc_point_array_setitem(long j, int i, long j2);

    public static final native long granny_stat_hud_alloc_point_get(long j, int i);

    public static final native int granny_stat_hud_animation_types_TotalTrackCount_get(long j);

    public static final native void granny_stat_hud_animation_types_TotalTrackCount_set(long j, int i);

    public static final native long granny_stat_hud_animation_types_TrackTypes_get(long j);

    public static final native void granny_stat_hud_animation_types_TrackTypes_set(long j, long j2);

    public static final native long granny_stat_hud_animation_types_array_getitem(long j, int i);

    public static final native void granny_stat_hud_animation_types_array_setitem(long j, int i, long j2);

    public static final native long granny_stat_hud_animation_types_get(long j, int i);

    public static final native long granny_stat_hud_array_getitem(long j, int i);

    public static final native void granny_stat_hud_array_setitem(long j, int i, long j2);

    public static final native int granny_stat_hud_footprint_TotalAllocationCount_get(long j);

    public static final native void granny_stat_hud_footprint_TotalAllocationCount_set(long j, int i);

    public static final native int granny_stat_hud_footprint_TotalBytesAllocated_get(long j);

    public static final native void granny_stat_hud_footprint_TotalBytesAllocated_set(long j, int i);

    public static final native int granny_stat_hud_footprint_TotalBytesRequested_get(long j);

    public static final native void granny_stat_hud_footprint_TotalBytesRequested_set(long j, int i);

    public static final native long granny_stat_hud_footprint_array_getitem(long j, int i);

    public static final native void granny_stat_hud_footprint_array_setitem(long j, int i, long j2);

    public static final native long granny_stat_hud_footprint_get(long j, int i);

    public static final native long granny_stat_hud_get(long j, int i);

    /* renamed from: granny_stat_hud_model_controls_ActiveAndWeightedButEasedOutControlCount_get */
    public static final native int m45855xb80ecbd6(long j);

    /* renamed from: granny_stat_hud_model_controls_ActiveAndWeightedButEasedOutControlCount_set */
    public static final native void m45856xb80ef8e2(long j, int i);

    public static final native int granny_stat_hud_model_controls_ActiveAndWeightedControlCount_get(long j);

    public static final native void granny_stat_hud_model_controls_ActiveAndWeightedControlCount_set(long j, int i);

    public static final native int granny_stat_hud_model_controls_ActiveControlCount_get(long j);

    public static final native void granny_stat_hud_model_controls_ActiveControlCount_set(long j, int i);

    public static final native int granny_stat_hud_model_controls_CompletableControlCount_get(long j);

    public static final native void granny_stat_hud_model_controls_CompletableControlCount_set(long j, int i);

    public static final native int granny_stat_hud_model_controls_CompletedControlCount_get(long j);

    public static final native void granny_stat_hud_model_controls_CompletedControlCount_set(long j, int i);

    public static final native float granny_stat_hud_model_controls_MaxClockTime_get(long j);

    public static final native void granny_stat_hud_model_controls_MaxClockTime_set(long j, float f);

    public static final native float granny_stat_hud_model_controls_MaxCompletionTime_get(long j);

    public static final native void granny_stat_hud_model_controls_MaxCompletionTime_set(long j, float f);

    public static final native float granny_stat_hud_model_controls_MinClockTime_get(long j);

    public static final native void granny_stat_hud_model_controls_MinClockTime_set(long j, float f);

    public static final native float granny_stat_hud_model_controls_MinCompletionTime_get(long j);

    public static final native void granny_stat_hud_model_controls_MinCompletionTime_set(long j, float f);

    public static final native int granny_stat_hud_model_controls_TotalControlCount_get(long j);

    public static final native void granny_stat_hud_model_controls_TotalControlCount_set(long j, int i);

    public static final native int granny_stat_hud_model_controls_UnusedControlCount_get(long j);

    public static final native void granny_stat_hud_model_controls_UnusedControlCount_set(long j, int i);

    public static final native long granny_stat_hud_model_controls_array_getitem(long j, int i);

    public static final native void granny_stat_hud_model_controls_array_setitem(long j, int i, long j2);

    public static final native long granny_stat_hud_model_controls_get(long j, int i);

    public static final native int granny_stat_hud_model_instances_MaxBoneCount_get(long j);

    public static final native void granny_stat_hud_model_instances_MaxBoneCount_set(long j, int i);

    public static final native int granny_stat_hud_model_instances_MaxInstanceControlCount_get(long j);

    public static final native void granny_stat_hud_model_instances_MaxInstanceControlCount_set(long j, int i);

    public static final native int granny_stat_hud_model_instances_MaxInstanceCount_get(long j);

    public static final native void granny_stat_hud_model_instances_MaxInstanceCount_set(long j, int i);

    public static final native int granny_stat_hud_model_instances_TotalInstanceCount_get(long j);

    public static final native void granny_stat_hud_model_instances_TotalInstanceCount_set(long j, int i);

    public static final native int granny_stat_hud_model_instances_TotalInstancedBoneCount_get(long j);

    public static final native void granny_stat_hud_model_instances_TotalInstancedBoneCount_set(long j, int i);

    public static final native int granny_stat_hud_model_instances_TotalUsedModelCount_get(long j);

    public static final native void granny_stat_hud_model_instances_TotalUsedModelCount_set(long j, int i);

    public static final native long granny_stat_hud_model_instances_array_getitem(long j, int i);

    public static final native void granny_stat_hud_model_instances_array_setitem(long j, int i, long j2);

    public static final native long granny_stat_hud_model_instances_get(long j, int i);

    public static final native double granny_stat_hud_perf_TotalCycles_get(long j);

    public static final native void granny_stat_hud_perf_TotalCycles_set(long j, double d);

    public static final native double granny_stat_hud_perf_TotalSeconds_get(long j);

    public static final native void granny_stat_hud_perf_TotalSeconds_set(long j, double d);

    public static final native long granny_stat_hud_perf_array_getitem(long j, int i);

    public static final native void granny_stat_hud_perf_array_setitem(long j, int i, long j2);

    public static final native long granny_stat_hud_perf_get(long j, int i);

    public static final native int granny_stat_hud_perf_point_Count_get(long j);

    public static final native void granny_stat_hud_perf_point_Count_set(long j, int i);

    public static final native String granny_stat_hud_perf_point_Name_get(long j);

    public static final native void granny_stat_hud_perf_point_Name_set(long j, String str);

    public static final native double granny_stat_hud_perf_point_TotalCycles_get(long j);

    public static final native void granny_stat_hud_perf_point_TotalCycles_set(long j, double d);

    public static final native double granny_stat_hud_perf_point_TotalSeconds_get(long j);

    public static final native void granny_stat_hud_perf_point_TotalSeconds_set(long j, double d);

    public static final native long granny_stat_hud_perf_point_array_getitem(long j, int i);

    public static final native void granny_stat_hud_perf_point_array_setitem(long j, int i, long j2);

    public static final native long granny_stat_hud_perf_point_get(long j, int i);

    public static final native long granny_string_database_DatabaseCRC_get(long j);

    public static final native void granny_string_database_DatabaseCRC_set(long j, long j2);

    public static final native long granny_string_database_ExtendedData_get(long j);

    public static final native void granny_string_database_ExtendedData_set(long j, long j2);

    public static final native int granny_string_database_StringCount_get(long j);

    public static final native void granny_string_database_StringCount_set(long j, int i);

    public static final native long granny_string_database_Strings_get(long j);

    public static final native void granny_string_database_Strings_set(long j, long j2);

    public static final native long granny_string_database_array_getitem(long j, int i);

    public static final native void granny_string_database_array_setitem(long j, int i, long j2);

    public static final native long granny_string_database_get(long j, int i);

    public static final native long granny_string_table_array_getitem(long j, int i);

    public static final native void granny_string_table_array_setitem(long j, int i, long j2);

    public static final native long granny_system_clock_Data_get(long j);

    public static final native void granny_system_clock_Data_set(long j, long j2);

    public static final native long granny_system_clock_array_getitem(long j, int i);

    public static final native void granny_system_clock_array_setitem(long j, int i, long j2);

    public static final native long granny_system_clock_get(long j, int i);

    public static final native long granny_tangent_frame_N_get(long j);

    public static final native void granny_tangent_frame_N_set(long j, long j2);

    public static final native long granny_tangent_frame_U_get(long j);

    public static final native void granny_tangent_frame_U_set(long j, long j2);

    public static final native long granny_tangent_frame_V_get(long j);

    public static final native void granny_tangent_frame_V_set(long j, long j2);

    public static final native long granny_tangent_frame_array_getitem(long j, int i);

    public static final native void granny_tangent_frame_array_setitem(long j, int i, long j2);

    public static final native long granny_tangent_frame_get(long j, int i);

    public static final native long granny_text_track_Entries_get(long j);

    public static final native void granny_text_track_Entries_set(long j, long j2);

    public static final native int granny_text_track_EntryCount_get(long j);

    public static final native void granny_text_track_EntryCount_set(long j, int i);

    public static final native String granny_text_track_Name_get(long j);

    public static final native void granny_text_track_Name_set(long j, String str);

    public static final native long granny_text_track_array_getitem(long j, int i);

    public static final native void granny_text_track_array_setitem(long j, int i, long j2);

    public static final native String granny_text_track_entry_Text_get(long j);

    public static final native void granny_text_track_entry_Text_set(long j, String str);

    public static final native float granny_text_track_entry_TimeStamp_get(long j);

    public static final native void granny_text_track_entry_TimeStamp_set(long j, float f);

    public static final native long granny_text_track_entry_array_getitem(long j, int i);

    public static final native void granny_text_track_entry_array_setitem(long j, int i, long j2);

    public static final native long granny_text_track_entry_get(long j, int i);

    public static final native long granny_text_track_get(long j, int i);

    public static final native int granny_texture_Encoding_get(long j);

    public static final native void granny_texture_Encoding_set(long j, int i);

    public static final native long granny_texture_ExtendedData_get(long j);

    public static final native void granny_texture_ExtendedData_set(long j, long j2);

    public static final native String granny_texture_FromFileName_get(long j);

    public static final native void granny_texture_FromFileName_set(long j, String str);

    public static final native int granny_texture_Height_get(long j);

    public static final native void granny_texture_Height_set(long j, int i);

    public static final native int granny_texture_ImageCount_get(long j);

    public static final native void granny_texture_ImageCount_set(long j, int i);

    public static final native long granny_texture_Images_get(long j);

    public static final native void granny_texture_Images_set(long j, long j2);

    public static final native long granny_texture_Layout_get(long j);

    public static final native void granny_texture_Layout_set(long j, long j2);

    public static final native int granny_texture_SubFormat_get(long j);

    public static final native void granny_texture_SubFormat_set(long j, int i);

    public static final native int granny_texture_TextureType_get(long j);

    public static final native void granny_texture_TextureType_set(long j, int i);

    public static final native int granny_texture_Width_get(long j);

    public static final native void granny_texture_Width_set(long j, int i);

    public static final native long granny_texture_array_getitem(long j, int i);

    public static final native void granny_texture_array_setitem(long j, int i, long j2);

    public static final native long granny_texture_builder_array_getitem(long j, int i);

    public static final native void granny_texture_builder_array_setitem(long j, int i, long j2);

    public static final native long granny_texture_get(long j, int i);

    public static final native int granny_texture_image_MIPLevelCount_get(long j);

    public static final native void granny_texture_image_MIPLevelCount_set(long j, int i);

    public static final native long granny_texture_image_MIPLevels_get(long j);

    public static final native void granny_texture_image_MIPLevels_set(long j, long j2);

    public static final native long granny_texture_image_array_getitem(long j, int i);

    public static final native void granny_texture_image_array_setitem(long j, int i, long j2);

    public static final native long granny_texture_image_get(long j, int i);

    public static final native int granny_texture_mip_level_PixelByteCount_get(long j);

    public static final native void granny_texture_mip_level_PixelByteCount_set(long j, int i);

    public static final native long granny_texture_mip_level_PixelBytes_get(long j);

    public static final native void granny_texture_mip_level_PixelBytes_set(long j, long j2);

    public static final native int granny_texture_mip_level_Stride_get(long j);

    public static final native void granny_texture_mip_level_Stride_set(long j, int i);

    public static final native long granny_texture_mip_level_array_getitem(long j, int i);

    public static final native void granny_texture_mip_level_array_setitem(long j, int i, long j2);

    public static final native long granny_texture_mip_level_get(long j, int i);

    public static final native long granny_track_group_ExtendedData_get(long j);

    public static final native void granny_track_group_ExtendedData_set(long j, long j2);

    public static final native int granny_track_group_Flags_get(long j);

    public static final native void granny_track_group_Flags_set(long j, int i);

    public static final native long granny_track_group_InitialPlacement_get(long j);

    public static final native void granny_track_group_InitialPlacement_set(long j, long j2);

    public static final native long granny_track_group_LoopTranslation_get(long j);

    public static final native void granny_track_group_LoopTranslation_set(long j, long j2);

    public static final native String granny_track_group_Name_get(long j);

    public static final native void granny_track_group_Name_set(long j, String str);

    public static final native long granny_track_group_PeriodicLoop_get(long j);

    public static final native void granny_track_group_PeriodicLoop_set(long j, long j2);

    public static final native long granny_track_group_RootMotion_get(long j);

    public static final native void granny_track_group_RootMotion_set(long j, long j2);

    public static final native int granny_track_group_TextTrackCount_get(long j);

    public static final native void granny_track_group_TextTrackCount_set(long j, int i);

    public static final native long granny_track_group_TextTracks_get(long j);

    public static final native void granny_track_group_TextTracks_set(long j, long j2);

    public static final native int granny_track_group_TransformLODErrorCount_get(long j);

    public static final native void granny_track_group_TransformLODErrorCount_set(long j, int i);

    public static final native long granny_track_group_TransformLODErrors_get(long j);

    public static final native void granny_track_group_TransformLODErrors_set(long j, long j2);

    public static final native int granny_track_group_TransformTrackCount_get(long j);

    public static final native void granny_track_group_TransformTrackCount_set(long j, int i);

    public static final native long granny_track_group_TransformTracks_get(long j);

    public static final native void granny_track_group_TransformTracks_set(long j, long j2);

    public static final native int granny_track_group_VectorTrackCount_get(long j);

    public static final native void granny_track_group_VectorTrackCount_set(long j, int i);

    public static final native long granny_track_group_VectorTracks_get(long j);

    public static final native void granny_track_group_VectorTracks_set(long j, long j2);

    public static final native long granny_track_group_array_getitem(long j, int i);

    public static final native void granny_track_group_array_setitem(long j, int i, long j2);

    public static final native long granny_track_group_builder_array_getitem(long j, int i);

    public static final native void granny_track_group_builder_array_setitem(long j, int i, long j2);

    public static final native long granny_track_group_get(long j, int i);

    public static final native long granny_track_group_sampler_array_getitem(long j, int i);

    public static final native void granny_track_group_sampler_array_setitem(long j, int i, long j2);

    public static final native long granny_track_mask_array_getitem(long j, int i);

    public static final native void granny_track_mask_array_setitem(long j, int i, long j2);

    public static final native long granny_transform_Flags_get(long j);

    public static final native void granny_transform_Flags_set(long j, long j2);

    public static final native long granny_transform_Orientation_get(long j);

    public static final native void granny_transform_Orientation_set(long j, long j2);

    public static final native long granny_transform_Position_get(long j);

    public static final native void granny_transform_Position_set(long j, long j2);

    public static final native long granny_transform_ScaleShear_get(long j);

    public static final native void granny_transform_ScaleShear_set(long j, long j2);

    public static final native long granny_transform_array_getitem(long j, int i);

    public static final native void granny_transform_array_setitem(long j, int i, long j2);

    public static final native long granny_transform_get(long j, int i);

    public static final native int granny_transform_track_Flags_get(long j);

    public static final native void granny_transform_track_Flags_set(long j, int i);

    public static final native String granny_transform_track_Name_get(long j);

    public static final native void granny_transform_track_Name_set(long j, String str);

    public static final native long granny_transform_track_OrientationCurve_get(long j);

    public static final native void granny_transform_track_OrientationCurve_set(long j, long j2);

    public static final native long granny_transform_track_PositionCurve_get(long j);

    public static final native void granny_transform_track_PositionCurve_set(long j, long j2);

    public static final native long granny_transform_track_ScaleShearCurve_get(long j);

    public static final native void granny_transform_track_ScaleShearCurve_set(long j, long j2);

    public static final native long granny_transform_track_array_getitem(long j, int i);

    public static final native void granny_transform_track_array_setitem(long j, int i, long j2);

    public static final native long granny_transform_track_get(long j, int i);

    public static final native int granny_tri_annotation_set_IndicesMapFromTriToAnnotation_get(long j);

    public static final native void granny_tri_annotation_set_IndicesMapFromTriToAnnotation_set(long j, int i);

    public static final native String granny_tri_annotation_set_Name_get(long j);

    public static final native void granny_tri_annotation_set_Name_set(long j, String str);

    public static final native int granny_tri_annotation_set_TriAnnotationCount_get(long j);

    public static final native void granny_tri_annotation_set_TriAnnotationCount_set(long j, int i);

    public static final native int granny_tri_annotation_set_TriAnnotationIndexCount_get(long j);

    public static final native void granny_tri_annotation_set_TriAnnotationIndexCount_set(long j, int i);

    public static final native long granny_tri_annotation_set_TriAnnotationIndices_get(long j);

    public static final native void granny_tri_annotation_set_TriAnnotationIndices_set(long j, long j2);

    public static final native long granny_tri_annotation_set_TriAnnotationType_get(long j);

    public static final native void granny_tri_annotation_set_TriAnnotationType_set(long j, long j2);

    public static final native long granny_tri_annotation_set_TriAnnotations_get(long j);

    public static final native void granny_tri_annotation_set_TriAnnotations_set(long j, long j2);

    public static final native long granny_tri_annotation_set_array_getitem(long j, int i);

    public static final native void granny_tri_annotation_set_array_setitem(long j, int i, long j2);

    public static final native long granny_tri_annotation_set_get(long j, int i);

    public static final native int granny_tri_annotation_set_getTriAnnotation(long j, int i);

    public static final native int granny_tri_annotation_set_getTriAnnotationIndex(long j, int i);

    public static final native int granny_tri_material_group_MaterialIndex_get(long j);

    public static final native void granny_tri_material_group_MaterialIndex_set(long j, int i);

    public static final native int granny_tri_material_group_TriCount_get(long j);

    public static final native void granny_tri_material_group_TriCount_set(long j, int i);

    public static final native int granny_tri_material_group_TriFirst_get(long j);

    public static final native void granny_tri_material_group_TriFirst_set(long j, int i);

    public static final native long granny_tri_material_group_array_getitem(long j, int i);

    public static final native void granny_tri_material_group_array_setitem(long j, int i, long j2);

    public static final native long granny_tri_material_group_get(long j, int i);

    public static final native int granny_tri_topology_BonesForTriangleCount_get(long j);

    public static final native void granny_tri_topology_BonesForTriangleCount_set(long j, int i);

    public static final native long granny_tri_topology_BonesForTriangle_get(long j);

    public static final native void granny_tri_topology_BonesForTriangle_set(long j, long j2);

    public static final native int granny_tri_topology_GroupCount_get(long j);

    public static final native void granny_tri_topology_GroupCount_set(long j, int i);

    public static final native long granny_tri_topology_Groups_get(long j);

    public static final native void granny_tri_topology_Groups_set(long j, long j2);

    public static final native int granny_tri_topology_Index16Count_get(long j);

    public static final native void granny_tri_topology_Index16Count_set(long j, int i);

    public static final native int granny_tri_topology_IndexCount_get(long j);

    public static final native void granny_tri_topology_IndexCount_set(long j, int i);

    public static final native long granny_tri_topology_Indices16_get(long j);

    public static final native void granny_tri_topology_Indices16_set(long j, long j2);

    public static final native long granny_tri_topology_Indices_get(long j);

    public static final native void granny_tri_topology_Indices_set(long j, long j2);

    public static final native int granny_tri_topology_SideToNeighborCount_get(long j);

    public static final native void granny_tri_topology_SideToNeighborCount_set(long j, int i);

    public static final native long granny_tri_topology_SideToNeighborMap_get(long j);

    public static final native void granny_tri_topology_SideToNeighborMap_set(long j, long j2);

    public static final native int granny_tri_topology_TriAnnotationSetCount_get(long j);

    public static final native void granny_tri_topology_TriAnnotationSetCount_set(long j, int i);

    public static final native long granny_tri_topology_TriAnnotationSets_get(long j);

    public static final native void granny_tri_topology_TriAnnotationSets_set(long j, long j2);

    public static final native int granny_tri_topology_TriangleToBoneCount_get(long j);

    public static final native void granny_tri_topology_TriangleToBoneCount_set(long j, int i);

    public static final native long granny_tri_topology_TriangleToBoneIndices_get(long j);

    public static final native void granny_tri_topology_TriangleToBoneIndices_set(long j, long j2);

    public static final native int granny_tri_topology_VertexToTriangleCount_get(long j);

    public static final native void granny_tri_topology_VertexToTriangleCount_set(long j, int i);

    public static final native long granny_tri_topology_VertexToTriangleMap_get(long j);

    public static final native void granny_tri_topology_VertexToTriangleMap_set(long j, long j2);

    public static final native int granny_tri_topology_VertexToVertexCount_get(long j);

    public static final native void granny_tri_topology_VertexToVertexCount_set(long j, int i);

    public static final native long granny_tri_topology_VertexToVertexMap_get(long j);

    public static final native void granny_tri_topology_VertexToVertexMap_set(long j, long j2);

    public static final native long granny_tri_topology_array_getitem(long j, int i);

    public static final native void granny_tri_topology_array_setitem(long j, int i, long j2);

    public static final native long granny_tri_topology_get(long j, int i);

    public static final native int granny_tri_topology_getVertexToTriangleMap(long j, int i);

    public static final native boolean granny_triangle_intersection_Backfacing_get(long j);

    public static final native void granny_triangle_intersection_Backfacing_set(long j, boolean z);

    public static final native long granny_triangle_intersection_EdgeU_get(long j);

    public static final native void granny_triangle_intersection_EdgeU_set(long j, long j2);

    public static final native long granny_triangle_intersection_EdgeV_get(long j);

    public static final native void granny_triangle_intersection_EdgeV_set(long j, long j2);

    public static final native float granny_triangle_intersection_T_get(long j);

    public static final native void granny_triangle_intersection_T_set(long j, float f);

    public static final native float granny_triangle_intersection_TriangleU_get(long j);

    public static final native void granny_triangle_intersection_TriangleU_set(long j, float f);

    public static final native float granny_triangle_intersection_TriangleV_get(long j);

    public static final native void granny_triangle_intersection_TriangleV_set(long j, float f);

    public static final native long granny_triangle_intersection_array_getitem(long j, int i);

    public static final native void granny_triangle_intersection_array_setitem(long j, int i, long j2);

    public static final native long granny_triangle_intersection_get(long j, int i);

    public static final native float granny_unbound_track_mask_DefaultWeight_get(long j);

    public static final native void granny_unbound_track_mask_DefaultWeight_set(long j, float f);

    public static final native int granny_unbound_track_mask_WeightCount_get(long j);

    public static final native void granny_unbound_track_mask_WeightCount_set(long j, int i);

    public static final native long granny_unbound_track_mask_Weights_get(long j);

    public static final native void granny_unbound_track_mask_Weights_set(long j, long j2);

    public static final native long granny_unbound_track_mask_array_getitem(long j, int i);

    public static final native void granny_unbound_track_mask_array_setitem(long j, int i, long j2);

    public static final native long granny_unbound_track_mask_get(long j, int i);

    public static final native String granny_unbound_weight_Name_get(long j);

    public static final native void granny_unbound_weight_Name_set(long j, String str);

    public static final native float granny_unbound_weight_Weight_get(long j);

    public static final native void granny_unbound_weight_Weight_set(long j, float f);

    public static final native long granny_unbound_weight_array_getitem(long j, int i);

    public static final native void granny_unbound_weight_array_setitem(long j, int i, long j2);

    public static final native long granny_unbound_weight_get(long j, int i);

    public static final native long granny_variant_Object_get(long j);

    public static final native void granny_variant_Object_set(long j, long j2);

    public static final native long granny_variant_Type_get(long j);

    public static final native void granny_variant_Type_set(long j, long j2);

    public static final native long granny_variant_array_getitem(long j, int i);

    public static final native void granny_variant_array_setitem(long j, int i, long j2);

    public static final native long granny_variant_builder_array_getitem(long j, int i);

    public static final native void granny_variant_builder_array_setitem(long j, int i, long j2);

    public static final native long granny_variant_get(long j, int i);

    public static final native int granny_vector_track_Dimension_get(long j);

    public static final native void granny_vector_track_Dimension_set(long j, int i);

    public static final native String granny_vector_track_Name_get(long j);

    public static final native void granny_vector_track_Name_set(long j, String str);

    public static final native long granny_vector_track_TrackKey_get(long j);

    public static final native void granny_vector_track_TrackKey_set(long j, long j2);

    public static final native long granny_vector_track_ValueCurve_get(long j);

    public static final native void granny_vector_track_ValueCurve_set(long j, long j2);

    public static final native long granny_vector_track_array_getitem(long j, int i);

    public static final native void granny_vector_track_array_setitem(long j, int i, long j2);

    public static final native long granny_vector_track_get(long j, int i);

    /* renamed from: granny_vertex_annotation_set_IndicesMapFromVertexToAnnotation_get */
    public static final native int m45857xa8aa8532(long j);

    /* renamed from: granny_vertex_annotation_set_IndicesMapFromVertexToAnnotation_set */
    public static final native void m45858xa8aab23e(long j, int i);

    public static final native String granny_vertex_annotation_set_Name_get(long j);

    public static final native void granny_vertex_annotation_set_Name_set(long j, String str);

    public static final native int granny_vertex_annotation_set_VertexAnnotationCount_get(long j);

    public static final native void granny_vertex_annotation_set_VertexAnnotationCount_set(long j, int i);

    public static final native int granny_vertex_annotation_set_VertexAnnotationIndexCount_get(long j);

    public static final native void granny_vertex_annotation_set_VertexAnnotationIndexCount_set(long j, int i);

    public static final native long granny_vertex_annotation_set_VertexAnnotationIndices_get(long j);

    public static final native void granny_vertex_annotation_set_VertexAnnotationIndices_set(long j, long j2);

    public static final native long granny_vertex_annotation_set_VertexAnnotationType_get(long j);

    public static final native void granny_vertex_annotation_set_VertexAnnotationType_set(long j, long j2);

    public static final native long granny_vertex_annotation_set_VertexAnnotations_get(long j);

    public static final native void granny_vertex_annotation_set_VertexAnnotations_set(long j, long j2);

    public static final native long granny_vertex_annotation_set_array_getitem(long j, int i);

    public static final native void granny_vertex_annotation_set_array_setitem(long j, int i, long j2);

    public static final native long granny_vertex_annotation_set_get(long j, int i);

    public static final native int granny_vertex_data_VertexAnnotationSetCount_get(long j);

    public static final native void granny_vertex_data_VertexAnnotationSetCount_set(long j, int i);

    public static final native long granny_vertex_data_VertexAnnotationSets_get(long j);

    public static final native void granny_vertex_data_VertexAnnotationSets_set(long j, long j2);

    public static final native int granny_vertex_data_VertexComponentNameCount_get(long j);

    public static final native void granny_vertex_data_VertexComponentNameCount_set(long j, int i);

    public static final native long granny_vertex_data_VertexComponentNames_get(long j);

    public static final native void granny_vertex_data_VertexComponentNames_set(long j, long j2);

    public static final native int granny_vertex_data_VertexCount_get(long j);

    public static final native void granny_vertex_data_VertexCount_set(long j, int i);

    public static final native long granny_vertex_data_VertexType_get(long j);

    public static final native void granny_vertex_data_VertexType_set(long j, long j2);

    public static final native long granny_vertex_data_Vertices_get(long j);

    public static final native void granny_vertex_data_Vertices_set(long j, long j2);

    public static final native long granny_vertex_data_array_getitem(long j, int i);

    public static final native void granny_vertex_data_array_setitem(long j, int i, long j2);

    public static final native long granny_vertex_data_get(long j, int i);

    public static final native long granny_vertex_weight_arrays_BoneIndices_get(long j);

    public static final native void granny_vertex_weight_arrays_BoneIndices_set(long j, long j2);

    public static final native long granny_vertex_weight_arrays_BoneWeights_get(long j);

    public static final native void granny_vertex_weight_arrays_BoneWeights_set(long j, long j2);

    public static final native long granny_vertex_weight_arrays_array_getitem(long j, int i);

    public static final native void granny_vertex_weight_arrays_array_setitem(long j, int i, long j2);

    public static final native long granny_vertex_weight_arrays_get(long j, int i);

    public static final native long granny_world_pose_array_getitem(long j, int i);

    public static final native void granny_world_pose_array_setitem(long j, int i, long j2);

    public static final native int int_pointer_get(long j, int i);

    public static final native void int_pointer_set(long j, int i, int i2);

    public static final native long new_char_pointer_array(int i);

    public static final native long new_granny_allocated_block_array(int i);

    public static final native long new_granny_allocation_header_array(int i);

    public static final native long new_granny_allocation_information();

    public static final native long new_granny_allocation_information_array(int i);

    public static final native long new_granny_animation();

    public static final native long new_granny_animation_array(int i);

    public static final native long new_granny_animation_binding();

    public static final native long new_granny_animation_binding_array(int i);

    public static final native long new_granny_animation_binding_cache_status();

    public static final native long new_granny_animation_binding_cache_status_array(int i);

    public static final native long new_granny_animation_binding_identifier();

    public static final native long new_granny_animation_binding_identifier_array(int i);

    public static final native long new_granny_animation_lod_builder_array(int i);

    public static final native long new_granny_art_tool_info();

    public static final native long new_granny_art_tool_info_array(int i);

    public static final native long new_granny_blend_dag_node_array(int i);

    public static final native long new_granny_bone();

    public static final native long new_granny_bone_array(int i);

    public static final native long new_granny_bone_binding();

    public static final native long new_granny_bone_binding_array(int i);

    public static final native long new_granny_bound_transform_track();

    public static final native long new_granny_bound_transform_track_array(int i);

    public static final native long new_granny_box_intersection();

    public static final native long new_granny_box_intersection_array(int i);

    public static final native long new_granny_bspline_error();

    public static final native long new_granny_bspline_error_array(int i);

    public static final native long new_granny_bspline_solver_array(int i);

    public static final native long new_granny_camera();

    public static final native long new_granny_camera_array(int i);

    public static final native long new_granny_compress_curve_parameters();

    public static final native long new_granny_compress_curve_parameters_array(int i);

    public static final native long new_granny_control_array(int i);

    public static final native long new_granny_controlled_animation_array(int i);

    public static final native long new_granny_controlled_animation_builder_array(int i);

    public static final native long new_granny_controlled_pose_array(int i);

    public static final native long new_granny_counter_results();

    public static final native long new_granny_counter_results_array(int i);

    public static final native long new_granny_curve2();

    public static final native long new_granny_curve2_array(int i);

    public static final native long new_granny_curve_builder_array(int i);

    public static final native long new_granny_curve_data_d3_constant32f();

    public static final native long new_granny_curve_data_d3_constant32f_array(int i);

    public static final native long new_granny_curve_data_d3_k16u_c16u();

    public static final native long new_granny_curve_data_d3_k16u_c16u_array(int i);

    public static final native long new_granny_curve_data_d3_k8u_c8u();

    public static final native long new_granny_curve_data_d3_k8u_c8u_array(int i);

    public static final native long new_granny_curve_data_d3i1_k16u_c16u();

    public static final native long new_granny_curve_data_d3i1_k16u_c16u_array(int i);

    public static final native long new_granny_curve_data_d3i1_k32f_c32f();

    public static final native long new_granny_curve_data_d3i1_k32f_c32f_array(int i);

    public static final native long new_granny_curve_data_d3i1_k8u_c8u();

    public static final native long new_granny_curve_data_d3i1_k8u_c8u_array(int i);

    public static final native long new_granny_curve_data_d4_constant32f();

    public static final native long new_granny_curve_data_d4_constant32f_array(int i);

    public static final native long new_granny_curve_data_d4n_k16u_c15u();

    public static final native long new_granny_curve_data_d4n_k16u_c15u_array(int i);

    public static final native long new_granny_curve_data_d4n_k8u_c7u();

    public static final native long new_granny_curve_data_d4n_k8u_c7u_array(int i);

    public static final native long new_granny_curve_data_d9i1_k16u_c16u();

    public static final native long new_granny_curve_data_d9i1_k16u_c16u_array(int i);

    public static final native long new_granny_curve_data_d9i1_k8u_c8u();

    public static final native long new_granny_curve_data_d9i1_k8u_c8u_array(int i);

    public static final native long new_granny_curve_data_d9i3_k16u_c16u();

    public static final native long new_granny_curve_data_d9i3_k16u_c16u_array(int i);

    public static final native long new_granny_curve_data_d9i3_k8u_c8u();

    public static final native long new_granny_curve_data_d9i3_k8u_c8u_array(int i);

    public static final native long new_granny_curve_data_da_constant32f();

    public static final native long new_granny_curve_data_da_constant32f_array(int i);

    public static final native long new_granny_curve_data_da_identity();

    public static final native long new_granny_curve_data_da_identity_array(int i);

    public static final native long new_granny_curve_data_da_k16u_c16u();

    public static final native long new_granny_curve_data_da_k16u_c16u_array(int i);

    public static final native long new_granny_curve_data_da_k32f_c32f();

    public static final native long new_granny_curve_data_da_k32f_c32f_array(int i);

    public static final native long new_granny_curve_data_da_k8u_c8u();

    public static final native long new_granny_curve_data_da_k8u_c8u_array(int i);

    public static final native long new_granny_curve_data_da_keyframes32f();

    public static final native long new_granny_curve_data_da_keyframes32f_array(int i);

    public static final native long new_granny_curve_data_header();

    public static final native long new_granny_curve_data_header_array(int i);

    public static final native long new_granny_dag_pose_cache_array(int i);

    public static final native long new_granny_data_type_definition();

    public static final native long new_granny_data_type_definition_array(int i);

    public static final native long new_granny_defined_type();

    public static final native long new_granny_defined_type_array(int i);

    public static final native long new_granny_exporter_info();

    public static final native long new_granny_exporter_info_array(int i);

    public static final native long new_granny_file();

    public static final native long new_granny_file_array(int i);

    public static final native long new_granny_file_builder_array(int i);

    public static final native long new_granny_file_compressor_array(int i);

    public static final native long new_granny_file_data_tree_writer_array(int i);

    public static final native long new_granny_file_fixup_array(int i);

    public static final native long new_granny_file_info();

    public static final native long new_granny_file_info_array(int i);

    public static final native long new_granny_file_location();

    public static final native long new_granny_file_location_array(int i);

    public static final native long new_granny_file_reader();

    public static final native long new_granny_file_reader_array(int i);

    public static final native long new_granny_file_writer();

    public static final native long new_granny_file_writer_array(int i);

    public static final native long new_granny_fixed_allocator();

    public static final native long new_granny_fixed_allocator_array(int i);

    public static final native long new_granny_fixed_allocator_block();

    public static final native long new_granny_fixed_allocator_block_array(int i);

    public static final native long new_granny_fixed_allocator_unit();

    public static final native long new_granny_fixed_allocator_unit_array(int i);

    public static final native long new_granny_grn_file_header();

    public static final native long new_granny_grn_file_header_array(int i);

    public static final native long new_granny_grn_file_magic_value();

    public static final native long new_granny_grn_file_magic_value_array(int i);

    public static final native long new_granny_grn_mixed_marshalling_fixup();

    public static final native long new_granny_grn_mixed_marshalling_fixup_array(int i);

    public static final native long new_granny_grn_pointer_fixup();

    public static final native long new_granny_grn_pointer_fixup_array(int i);

    public static final native long new_granny_grn_reference();

    public static final native long new_granny_grn_reference_array(int i);

    public static final native long new_granny_grn_section();

    public static final native long new_granny_grn_section_array(int i);

    public static final native long new_granny_local_pose_array(int i);

    public static final native long new_granny_log_callback();

    public static final native long new_granny_log_callback_array(int i);

    public static final native long new_granny_material();

    public static final native long new_granny_material_array(int i);

    public static final native long new_granny_material_binding();

    public static final native long new_granny_material_binding_array(int i);

    public static final native long new_granny_material_map();

    public static final native long new_granny_material_map_array(int i);

    public static final native long new_granny_memory_arena_array(int i);

    public static final native long new_granny_mesh();

    public static final native long new_granny_mesh_array(int i);

    public static final native long new_granny_mesh_binding_array(int i);

    public static final native long new_granny_mesh_builder_array(int i);

    public static final native long new_granny_mesh_deformer_array(int i);

    public static final native long new_granny_model();

    public static final native long new_granny_model_array(int i);

    public static final native long new_granny_model_control_binding_array(int i);

    public static final native long new_granny_model_instance_array(int i);

    public static final native long new_granny_model_mesh_binding();

    public static final native long new_granny_model_mesh_binding_array(int i);

    public static final native long new_granny_morph_target();

    public static final native long new_granny_morph_target_array(int i);

    public static final native long new_granny_old_curve();

    public static final native long new_granny_old_curve_array(int i);

    public static final native long new_granny_oodle1_state_array(int i);

    public static final native long new_granny_p3_vertex();

    public static final native long new_granny_p3_vertex_array(int i);

    public static final native long new_granny_periodic_loop();

    public static final native long new_granny_periodic_loop_array(int i);

    public static final native long new_granny_pixel_layout();

    public static final native long new_granny_pixel_layout_array(int i);

    public static final native long new_granny_pn33_vertex();

    public static final native long new_granny_pn33_vertex_array(int i);

    public static final native long new_granny_png333_vertex();

    public static final native long new_granny_png333_vertex_array(int i);

    public static final native long new_granny_pngb3333_vertex();

    public static final native long new_granny_pngb3333_vertex_array(int i);

    public static final native long new_granny_pngbt33332_vertex();

    public static final native long new_granny_pngbt33332_vertex_array(int i);

    public static final native long new_granny_pngbt33333_vertex();

    public static final native long new_granny_pngbt33333_vertex_array(int i);

    public static final native long new_granny_pngt3332_vertex();

    public static final native long new_granny_pngt3332_vertex_array(int i);

    public static final native long new_granny_pnt332_vertex();

    public static final native long new_granny_pnt332_vertex_array(int i);

    public static final native long new_granny_pnt333_vertex();

    public static final native long new_granny_pnt333_vertex_array(int i);

    public static final native long new_granny_pntg3323_vertex();

    public static final native long new_granny_pntg3323_vertex_array(int i);

    public static final native long new_granny_pointer_hash_array(int i);

    public static final native long new_granny_pt32_vertex();

    public static final native long new_granny_pt32_vertex_array(int i);

    public static final native long new_granny_pwn313_vertex();

    public static final native long new_granny_pwn313_vertex_array(int i);

    public static final native long new_granny_pwn323_vertex();

    public static final native long new_granny_pwn323_vertex_array(int i);

    public static final native long new_granny_pwn343_vertex();

    public static final native long new_granny_pwn343_vertex_array(int i);

    public static final native long new_granny_pwng3133_vertex();

    public static final native long new_granny_pwng3133_vertex_array(int i);

    public static final native long new_granny_pwng3233_vertex();

    public static final native long new_granny_pwng3233_vertex_array(int i);

    public static final native long new_granny_pwng3433_vertex();

    public static final native long new_granny_pwng3433_vertex_array(int i);

    public static final native long new_granny_pwngb31333_vertex();

    public static final native long new_granny_pwngb31333_vertex_array(int i);

    public static final native long new_granny_pwngb32333_vertex();

    public static final native long new_granny_pwngb32333_vertex_array(int i);

    public static final native long new_granny_pwngb34333_vertex();

    public static final native long new_granny_pwngb34333_vertex_array(int i);

    public static final native long new_granny_pwngbt313332_vertex();

    public static final native long new_granny_pwngbt313332_vertex_array(int i);

    public static final native long new_granny_pwngbt323332_vertex();

    public static final native long new_granny_pwngbt323332_vertex_array(int i);

    public static final native long new_granny_pwngbt343332_vertex();

    public static final native long new_granny_pwngbt343332_vertex_array(int i);

    public static final native long new_granny_pwngt31332_vertex();

    public static final native long new_granny_pwngt31332_vertex_array(int i);

    public static final native long new_granny_pwngt32332_vertex();

    public static final native long new_granny_pwngt32332_vertex_array(int i);

    public static final native long new_granny_pwngt34332_vertex();

    public static final native long new_granny_pwngt34332_vertex_array(int i);

    public static final native long new_granny_pwnt3132_vertex();

    public static final native long new_granny_pwnt3132_vertex_array(int i);

    public static final native long new_granny_pwnt3232_vertex();

    public static final native long new_granny_pwnt3232_vertex_array(int i);

    public static final native long new_granny_pwnt3432_vertex();

    public static final native long new_granny_pwnt3432_vertex_array(int i);

    public static final native long new_granny_sample_context();

    public static final native long new_granny_sample_context_array(int i);

    public static final native long new_granny_skeleton();

    public static final native long new_granny_skeleton_array(int i);

    public static final native long new_granny_skeleton_builder_array(int i);

    public static final native long new_granny_stack_allocator();

    public static final native long new_granny_stack_allocator_array(int i);

    public static final native long new_granny_stat_hud();

    public static final native long new_granny_stat_hud_alloc_point();

    public static final native long new_granny_stat_hud_alloc_point_array(int i);

    public static final native long new_granny_stat_hud_animation_types();

    public static final native long new_granny_stat_hud_animation_types_array(int i);

    public static final native long new_granny_stat_hud_array(int i);

    public static final native long new_granny_stat_hud_footprint();

    public static final native long new_granny_stat_hud_footprint_array(int i);

    public static final native long new_granny_stat_hud_model_controls();

    public static final native long new_granny_stat_hud_model_controls_array(int i);

    public static final native long new_granny_stat_hud_model_instances();

    public static final native long new_granny_stat_hud_model_instances_array(int i);

    public static final native long new_granny_stat_hud_perf();

    public static final native long new_granny_stat_hud_perf_array(int i);

    public static final native long new_granny_stat_hud_perf_point();

    public static final native long new_granny_stat_hud_perf_point_array(int i);

    public static final native long new_granny_string_database();

    public static final native long new_granny_string_database_array(int i);

    public static final native long new_granny_string_table_array(int i);

    public static final native long new_granny_system_clock();

    public static final native long new_granny_system_clock_array(int i);

    public static final native long new_granny_tangent_frame();

    public static final native long new_granny_tangent_frame_array(int i);

    public static final native long new_granny_text_track();

    public static final native long new_granny_text_track_array(int i);

    public static final native long new_granny_text_track_entry();

    public static final native long new_granny_text_track_entry_array(int i);

    public static final native long new_granny_texture();

    public static final native long new_granny_texture_array(int i);

    public static final native long new_granny_texture_builder_array(int i);

    public static final native long new_granny_texture_image();

    public static final native long new_granny_texture_image_array(int i);

    public static final native long new_granny_texture_mip_level();

    public static final native long new_granny_texture_mip_level_array(int i);

    public static final native long new_granny_track_group();

    public static final native long new_granny_track_group_array(int i);

    public static final native long new_granny_track_group_builder_array(int i);

    public static final native long new_granny_track_group_sampler_array(int i);

    public static final native long new_granny_track_mask_array(int i);

    public static final native long new_granny_transform();

    public static final native long new_granny_transform_array(int i);

    public static final native long new_granny_transform_track();

    public static final native long new_granny_transform_track_array(int i);

    public static final native long new_granny_tri_annotation_set();

    public static final native long new_granny_tri_annotation_set_array(int i);

    public static final native long new_granny_tri_material_group();

    public static final native long new_granny_tri_material_group_array(int i);

    public static final native long new_granny_tri_topology();

    public static final native long new_granny_tri_topology_array(int i);

    public static final native long new_granny_triangle_intersection();

    public static final native long new_granny_triangle_intersection_array(int i);

    public static final native long new_granny_unbound_track_mask();

    public static final native long new_granny_unbound_track_mask_array(int i);

    public static final native long new_granny_unbound_weight();

    public static final native long new_granny_unbound_weight_array(int i);

    public static final native long new_granny_variant();

    public static final native long new_granny_variant_array(int i);

    public static final native long new_granny_variant_builder_array(int i);

    public static final native long new_granny_vector_track();

    public static final native long new_granny_vector_track_array(int i);

    public static final native long new_granny_vertex_annotation_set();

    public static final native long new_granny_vertex_annotation_set_array(int i);

    public static final native long new_granny_vertex_data();

    public static final native long new_granny_vertex_data_array(int i);

    public static final native long new_granny_vertex_weight_arrays();

    public static final native long new_granny_vertex_weight_arrays_array(int i);

    public static final native long new_granny_world_pose_array(int i);

    public static final native void setTransformOrientation(long j, float f, float f2, float f3, float f4);
}
