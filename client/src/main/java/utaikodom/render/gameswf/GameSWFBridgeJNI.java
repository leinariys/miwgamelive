package utaikodom.render.gameswf;

import logic.render.gameswf.C1040PG;
import logic.render.gameswf.C1831aX;
import logic.render.gameswf.C2453fa;
import logic.render.gameswf.C3840ve;

import java.nio.Buffer;

/* compiled from: a */
public class GameSWFBridgeJNI {
    GameSWFBridgeJNI() {
    }

    public static final native void MemoryFile_setNextFile(int i, Buffer buffer);

    public static final native void clear_gameswf();

    public static final native void clear_log_callback();

    public static final native void clear_movie();

    public static final native long create_movie(String str);

    public static final native long create_render_handler_ogl();

    public static final native long create_sound_handler_miles(int i, int i2, int i3);

    public static final native long create_sound_handler_sdl();

    public static final native void delete_MemoryFile(long j);

    public static final native void delete_movie_definition(long j);

    public static final native void delete_movie_interface(long j);

    public static final native void delete_render_handler(long j);

    public static final native void delete_sound_handler(long j);

    public static final native void drop_ref__SWIG_0(long j, C1831aX aXVar);

    public static final native void drop_ref__SWIG_1(long j, C3840ve veVar);

    public static final native long get_current_root();

    public static final native float get_curve_max_pixel_error();

    public static final native void set_curve_max_pixel_error(float f);

    public static final native long get_sound_handler();

    public static final native boolean get_verbose_action();

    public static final native void set_verbose_action(boolean z);

    public static final native boolean get_verbose_debug();

    public static final native boolean get_verbose_parse();

    public static final native void set_verbose_parse(boolean z);

    public static final native void movie_definition_clear_instance(long j, C1831aX aXVar);

    public static final native long movie_definition_create_instance(long j, C1831aX aXVar);

    public static final native void movie_definition_generate_font_bitmaps(long j, C1831aX aXVar);

    public static final native long movie_definition_get_bitmap_info(long j, C1831aX aXVar, int i);

    public static final native int movie_definition_get_bitmap_info_count(long j, C1831aX aXVar);

    public static final native int movie_definition_get_frame_count(long j, C1831aX aXVar);

    public static final native float movie_definition_get_frame_rate(long j, C1831aX aXVar);

    public static final native float movie_definition_get_height_pixels(long j, C1831aX aXVar);

    public static final native int movie_definition_get_version(long j, C1831aX aXVar);

    public static final native float movie_definition_get_width_pixels(long j, C1831aX aXVar);

    public static final native void movie_definition_input_cached_data(long j, C1831aX aXVar, long j2);

    public static final native void movie_definition_output_cached_data(long j, C1831aX aXVar, long j2, long j3);

    public static final native void movie_definition_resolve_import(long j, C1831aX aXVar, String str, long j2, C1831aX aXVar2);

    public static final native void movie_definition_visit_imported_movies(long j, C1831aX aXVar, long j2);

    public static final native void movie_interface_advance(long j, C3840ve veVar, float f);

    public static final native void movie_interface_attach_display_callback(long j, C3840ve veVar, String str, long j2, long j3);

    public static final native void movie_interface_display(long j, C3840ve veVar);

    public static final native float movie_interface_get_background_alpha(long j, C3840ve veVar);

    public static final native int movie_interface_get_current_frame(long j, C3840ve veVar);

    public static final native long movie_interface_get_movie_definition(long j, C3840ve veVar);

    public static final native float movie_interface_get_movie_fps(long j, C3840ve veVar);

    public static final native int movie_interface_get_movie_height(long j, C3840ve veVar);

    public static final native int movie_interface_get_movie_version(long j, C3840ve veVar);

    public static final native int movie_interface_get_movie_width(long j, C3840ve veVar);

    public static final native int movie_interface_get_play_state(long j, C3840ve veVar);

    public static final native long movie_interface_get_root_movie(long j, C3840ve veVar);

    public static final native long movie_interface_get_userdata(long j, C3840ve veVar);

    public static final native String movie_interface_get_variable(long j, C3840ve veVar, String str);

    public static final native boolean movie_interface_get_visible(long j, C3840ve veVar);

    public static final native void movie_interface_goto_frame(long j, C3840ve veVar, int i);

    public static final native boolean movie_interface_goto_labeled_frame(long j, C3840ve veVar, String str);

    public static final native boolean movie_interface_has_looped(long j, C3840ve veVar);

    public static final native void movie_interface_notify_key_event(long j, C3840ve veVar, long j2, boolean z);

    public static final native void movie_interface_notify_mouse_state(long j, C3840ve veVar, int i, int i2, int i3);

    public static final native void movie_interface_set_background_alpha(long j, C3840ve veVar, float f);

    public static final native void movie_interface_set_background_color(long j, C3840ve veVar, long j2);

    public static final native void movie_interface_set_display_viewport(long j, C3840ve veVar, int i, int i2, int i3, int i4);

    public static final native void movie_interface_set_play_state(long j, C3840ve veVar, int i);

    public static final native void movie_interface_set_userdata(long j, C3840ve veVar, long j2);

    public static final native void movie_interface_set_variable__SWIG_0(long j, C3840ve veVar, String str, String str2);

    public static final native void movie_interface_set_variable__SWIG_1(long j, C3840ve veVar, String str, long j2);

    public static final native void movie_interface_set_visible(long j, C3840ve veVar, boolean z);

    public static final native long new_MemoryFile();

    public static final native void register_default_file_opener_callback();

    public static final native void register_file_opener_callback(long j);

    public static final native void register_fscommand_callback(long j);

    public static final native void register_log_callback(long j);

    public static final native void register_memory_file_opener_callback();

    public static final native void render_handler_begin_submit_mask(long j, C2453fa faVar);

    public static final native long render_handler_create_bitmap_info_alpha(long j, C2453fa faVar, int i, int i2, long j2);

    public static final native long render_handler_create_bitmap_info_empty(long j, C2453fa faVar);

    public static final native long render_handler_create_video_handler(long j, C2453fa faVar);

    public static final native void render_handler_disable_mask(long j, C2453fa faVar);

    public static final native void render_handler_draw_line_strip(long j, C2453fa faVar, long j2, int i);

    public static final native void render_handler_draw_mesh_strip(long j, C2453fa faVar, long j2, int i);

    public static final native void render_handler_draw_triangle_list(long j, C2453fa faVar, long j2, int i);

    public static final native void render_handler_end_submit_mask(long j, C2453fa faVar);

    public static final native void render_handler_fill_style_bitmap(long j, C2453fa faVar, int i, long j2, long j3, int i2);

    public static final native void render_handler_fill_style_disable(long j, C2453fa faVar, int i);

    public static final native void render_handler_line_style_disable(long j, C2453fa faVar);

    public static final native void render_handler_line_style_width(long j, C2453fa faVar, float f);

    public static final native void render_handler_set_antialiased(long j, C2453fa faVar, boolean z);

    public static final native void render_handler_set_cursor(long j, C2453fa faVar, int i);

    public static final native void render_handler_set_cxform(long j, C2453fa faVar, long j2);

    public static final native void render_handler_set_matrix(long j, C2453fa faVar, long j2);

    public static final native void set_current_root(long j, C3840ve veVar);

    public static final native void set_render_handler(long j, C2453fa faVar);

    public static final native void set_sound_handler(long j, C1040PG pg);

    public static final native int sound_handler_FORMAT_ADPCM_get();

    public static final native int sound_handler_FORMAT_MP3_get();

    public static final native int sound_handler_FORMAT_NATIVE16_get();

    public static final native int sound_handler_FORMAT_NELLYMOSER_get();

    public static final native int sound_handler_FORMAT_RAW_get();

    public static final native int sound_handler_FORMAT_UNCOMPRESSED_get();

    public static final native void sound_handler_append_sound(long j, C1040PG pg, int i, long j2, int i2);

    public static final native void sound_handler_attach_aux_streamer(long j, C1040PG pg, long j2, long j3);

    public static final native int sound_handler_create_sound(long j, C1040PG pg, long j2, int i, int i2, int i3, int i4, boolean z);

    public static final native void sound_handler_cvt(long j, C1040PG pg, long j2, long j3, long j4, int i, int i2, int i3);

    public static final native void sound_handler_delete_sound(long j, C1040PG pg, int i);

    public static final native void sound_handler_detach_aux_streamer(long j, C1040PG pg, long j2);

    public static final native boolean sound_handler_is_open(long j, C1040PG pg);

    public static final native void sound_handler_pause(long j, C1040PG pg, int i, boolean z);

    public static final native void sound_handler_play_sound(long j, C1040PG pg, int i, int i2);

    public static final native void sound_handler_set_volume(long j, C1040PG pg, int i, int i2);

    public static final native void sound_handler_stop_all_sounds(long j, C1040PG pg);

    public static final native void sound_handler_stop_sound(long j, C1040PG pg, int i);
}
