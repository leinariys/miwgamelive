package utaikodom.render.bink;

import logic.render.bink.*;

import java.nio.Buffer;

/* compiled from: a */
public class BinkBridgeJNI {
    BinkBridgeJNI() {
    }

    public static final native int BINKBUFFER_BufferPitch_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_BufferPitch_set(long j, C6673arV arv, int i);

    public static final native long BINKBUFFER_Buffer_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_Buffer_set(long j, C6673arV arv, long j2);

    public static final native int BINKBUFFER_ClientOffsetX_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_ClientOffsetX_set(long j, C6673arV arv, int i);

    public static final native int BINKBUFFER_ClientOffsetY_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_ClientOffsetY_set(long j, C6673arV arv, int i);

    public static final native long BINKBUFFER_ExtraWindowHeight_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_ExtraWindowHeight_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_ExtraWindowWidth_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_ExtraWindowWidth_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_Height_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_Height_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_ScaleFlags_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_ScaleFlags_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_ScreenDepth_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_ScreenDepth_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_ScreenHeight_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_ScreenHeight_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_ScreenWidth_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_ScreenWidth_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_StretchHeight_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_StretchHeight_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_StretchWidth_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_StretchWidth_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_SurfaceType_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_SurfaceType_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_Width_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_Width_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_WindowHeight_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_WindowHeight_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_WindowWidth_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_WindowWidth_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_buffertop_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_buffertop_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_cursorcount_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_cursorcount_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_ddclipper_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_ddclipper_set(long j, C6673arV arv, long j2);

    public static final native int BINKBUFFER_ddoffscreen_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_ddoffscreen_set(long j, C6673arV arv, int i);

    public static final native int BINKBUFFER_ddoverlay_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_ddoverlay_set(long j, C6673arV arv, int i);

    public static final native long BINKBUFFER_ddsurface_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_ddsurface_set(long j, C6673arV arv, long j2);

    public static final native int BINKBUFFER_destx_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_destx_set(long j, C6673arV arv, int i);

    public static final native int BINKBUFFER_desty_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_desty_set(long j, C6673arV arv, int i);

    public static final native long BINKBUFFER_dibbuffer_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_dibbuffer_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_dibdc_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_dibdc_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_dibh_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_dibh_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_dibinfo_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_dibinfo_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_diboldbitmap_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_diboldbitmap_set(long j, C6673arV arv, long j2);

    public static final native int BINKBUFFER_dibpitch_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_dibpitch_set(long j, C6673arV arv, int i);

    public static final native int BINKBUFFER_issoftcur_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_issoftcur_set(long j, C6673arV arv, int i);

    public static final native int BINKBUFFER_lastovershow_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_lastovershow_set(long j, C6673arV arv, int i);

    public static final native int BINKBUFFER_loadeddd_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_loadeddd_set(long j, C6673arV arv, int i);

    public static final native int BINKBUFFER_loadedwin_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_loadedwin_set(long j, C6673arV arv, int i);

    public static final native int BINKBUFFER_minimized_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_minimized_set(long j, C6673arV arv, int i);

    public static final native int BINKBUFFER_noclipping_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_noclipping_set(long j, C6673arV arv, int i);

    public static final native int BINKBUFFER_surface_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_surface_set(long j, C6673arV arv, int i);

    public static final native long BINKBUFFER_type_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_type_set(long j, C6673arV arv, long j2);

    public static final native long BINKBUFFER_wnd_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_wnd_set(long j, C6673arV arv, long j2);

    public static final native int BINKBUFFER_wndx_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_wndx_set(long j, C6673arV arv, int i);

    public static final native int BINKBUFFER_wndy_get(long j, C6673arV arv);

    public static final native void BINKBUFFER_wndy_set(long j, C6673arV arv, int i);

    public static final native long BINKFRAMEBUFFERS_FrameNum_get(long j, C2816kW kWVar);

    public static final native void BINKFRAMEBUFFERS_FrameNum_set(long j, C2816kW kWVar, long j2);

    public static final native long BINKFRAMEBUFFERS_Frames_get(long j, C2816kW kWVar);

    public static final native void BINKFRAMEBUFFERS_Frames_set(long j, C2816kW kWVar, long j2, C3121oA oAVar);

    public static final native int BINKFRAMEBUFFERS_TotalFrames_get(long j, C2816kW kWVar);

    public static final native void BINKFRAMEBUFFERS_TotalFrames_set(long j, C2816kW kWVar, int i);

    public static final native long BINKFRAMEBUFFERS_YABufferHeight_get(long j, C2816kW kWVar);

    public static final native void BINKFRAMEBUFFERS_YABufferHeight_set(long j, C2816kW kWVar, long j2);

    public static final native long BINKFRAMEBUFFERS_YABufferWidth_get(long j, C2816kW kWVar);

    public static final native void BINKFRAMEBUFFERS_YABufferWidth_set(long j, C2816kW kWVar, long j2);

    public static final native long BINKFRAMEBUFFERS_cRcBBufferHeight_get(long j, C2816kW kWVar);

    public static final native void BINKFRAMEBUFFERS_cRcBBufferHeight_set(long j, C2816kW kWVar, long j2);

    public static final native long BINKFRAMEBUFFERS_cRcBBufferWidth_get(long j, C2816kW kWVar);

    public static final native void BINKFRAMEBUFFERS_cRcBBufferWidth_set(long j, C2816kW kWVar, long j2);

    public static final native long BINKFRAMEPLANESET_APlane_get(long j, C3121oA oAVar);

    public static final native void BINKFRAMEPLANESET_APlane_set(long j, C3121oA oAVar, long j2, C2838kr krVar);

    public static final native long BINKFRAMEPLANESET_YPlane_get(long j, C3121oA oAVar);

    public static final native void BINKFRAMEPLANESET_YPlane_set(long j, C3121oA oAVar, long j2, C2838kr krVar);

    public static final native long BINKFRAMEPLANESET_cBPlane_get(long j, C3121oA oAVar);

    public static final native void BINKFRAMEPLANESET_cBPlane_set(long j, C3121oA oAVar, long j2, C2838kr krVar);

    public static final native long BINKFRAMEPLANESET_cRPlane_get(long j, C3121oA oAVar);

    public static final native void BINKFRAMEPLANESET_cRPlane_set(long j, C3121oA oAVar, long j2, C2838kr krVar);

    public static final native long BINKHDR_Flags_get(long j, aON aon);

    public static final native void BINKHDR_Flags_set(long j, aON aon, long j2);

    public static final native long BINKHDR_FrameRateDiv_get(long j, aON aon);

    public static final native void BINKHDR_FrameRateDiv_set(long j, aON aon, long j2);

    public static final native long BINKHDR_FrameRate_get(long j, aON aon);

    public static final native void BINKHDR_FrameRate_set(long j, aON aon, long j2);

    public static final native long BINKHDR_Frames_get(long j, aON aon);

    public static final native void BINKHDR_Frames_set(long j, aON aon, long j2);

    public static final native long BINKHDR_Height_get(long j, aON aon);

    public static final native void BINKHDR_Height_set(long j, aON aon, long j2);

    public static final native long BINKHDR_InternalFrames_get(long j, aON aon);

    public static final native void BINKHDR_InternalFrames_set(long j, aON aon, long j2);

    public static final native long BINKHDR_LargestFrameSize_get(long j, aON aon);

    public static final native void BINKHDR_LargestFrameSize_set(long j, aON aon, long j2);

    public static final native long BINKHDR_Marker_get(long j, aON aon);

    public static final native void BINKHDR_Marker_set(long j, aON aon, long j2);

    public static final native long BINKHDR_NumTracks_get(long j, aON aon);

    public static final native void BINKHDR_NumTracks_set(long j, aON aon, long j2);

    public static final native long BINKHDR_Size_get(long j, aON aon);

    public static final native void BINKHDR_Size_set(long j, aON aon, long j2);

    public static final native long BINKHDR_Width_get(long j, aON aon);

    public static final native void BINKHDR_Width_set(long j, aON aon, long j2);

    public static final native long BINKIO_BGControl_get(long j, C5340aFo afo);

    public static final native void BINKIO_BGControl_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_BufHighUsed_get(long j, C5340aFo afo);

    public static final native void BINKIO_BufHighUsed_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_BufSize_get(long j, C5340aFo afo);

    public static final native void BINKIO_BufSize_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_BytesRead_get(long j, C5340aFo afo);

    public static final native void BINKIO_BytesRead_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_Close_get(long j, C5340aFo afo);

    public static final native void BINKIO_Close_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_CurBufSize_get(long j, C5340aFo afo);

    public static final native void BINKIO_CurBufSize_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_CurBufUsed_get(long j, C5340aFo afo);

    public static final native void BINKIO_CurBufUsed_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_DoingARead_get(long j, C5340aFo afo);

    public static final native void BINKIO_DoingARead_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_ForegroundTime_get(long j, C5340aFo afo);

    public static final native void BINKIO_ForegroundTime_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_GetBufferSize_get(long j, C5340aFo afo);

    public static final native void BINKIO_GetBufferSize_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_IdleTime_get(long j, C5340aFo afo);

    public static final native void BINKIO_IdleTime_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_Idle_get(long j, C5340aFo afo);

    public static final native void BINKIO_Idle_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_ReadError_get(long j, C5340aFo afo);

    public static final native void BINKIO_ReadError_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_ReadFrame_get(long j, C5340aFo afo);

    public static final native void BINKIO_ReadFrame_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_ReadHeader_get(long j, C5340aFo afo);

    public static final native void BINKIO_ReadHeader_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_SetInfo_get(long j, C5340aFo afo);

    public static final native void BINKIO_SetInfo_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_Suspended_get(long j, C5340aFo afo);

    public static final native void BINKIO_Suspended_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_ThreadTime_get(long j, C5340aFo afo);

    public static final native void BINKIO_ThreadTime_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_TotalTime_get(long j, C5340aFo afo);

    public static final native void BINKIO_TotalTime_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_Working_get(long j, C5340aFo afo);

    public static final native void BINKIO_Working_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_bink_get(long j, C5340aFo afo);

    public static final native void BINKIO_bink_set(long j, C5340aFo afo, long j2, C2427fN fNVar);

    public static final native long BINKIO_callback_control_get(long j, C5340aFo afo);

    public static final native void BINKIO_callback_control_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_idle_on_callback_get(long j, C5340aFo afo);

    public static final native void BINKIO_idle_on_callback_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_iodata_get(long j, C5340aFo afo);

    public static final native void BINKIO_iodata_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_resume_callback_get(long j, C5340aFo afo);

    public static final native void BINKIO_resume_callback_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_suspend_callback_get(long j, C5340aFo afo);

    public static final native void BINKIO_suspend_callback_set(long j, C5340aFo afo, long j2);

    public static final native long BINKIO_try_suspend_callback_get(long j, C5340aFo afo);

    public static final native void BINKIO_try_suspend_callback_set(long j, C5340aFo afo, long j2);

    public static final native int BINKPLANE_Allocate_get(long j, C2838kr krVar);

    public static final native void BINKPLANE_Allocate_set(long j, C2838kr krVar, int i);

    public static final native long BINKPLANE_BufferPitch_get(long j, C2838kr krVar);

    public static final native void BINKPLANE_BufferPitch_set(long j, C2838kr krVar, long j2);

    public static final native long BINKPLANE_Buffer_get(long j, C2838kr krVar);

    public static final native void BINKPLANE_Buffer_set(long j, C2838kr krVar, long j2);

    public static final native long BINKREALTIME_FrameNum_get(long j, C6084agE age);

    public static final native void BINKREALTIME_FrameNum_set(long j, C6084agE age, long j2);

    public static final native long BINKREALTIME_FrameRateDiv_get(long j, C6084agE age);

    public static final native void BINKREALTIME_FrameRateDiv_set(long j, C6084agE age, long j2);

    public static final native long BINKREALTIME_FrameRate_get(long j, C6084agE age);

    public static final native void BINKREALTIME_FrameRate_set(long j, C6084agE age, long j2);

    public static final native long BINKREALTIME_FramesAudioDecompTime_get(long j, C6084agE age);

    public static final native void BINKREALTIME_FramesAudioDecompTime_set(long j, C6084agE age, long j2);

    public static final native long BINKREALTIME_FramesBlitTime_get(long j, C6084agE age);

    public static final native void BINKREALTIME_FramesBlitTime_set(long j, C6084agE age, long j2);

    public static final native long BINKREALTIME_FramesDataRate_get(long j, C6084agE age);

    public static final native void BINKREALTIME_FramesDataRate_set(long j, C6084agE age, long j2);

    public static final native long BINKREALTIME_FramesIdleReadTime_get(long j, C6084agE age);

    public static final native void BINKREALTIME_FramesIdleReadTime_set(long j, C6084agE age, long j2);

    public static final native long BINKREALTIME_FramesReadTime_get(long j, C6084agE age);

    public static final native void BINKREALTIME_FramesReadTime_set(long j, C6084agE age, long j2);

    public static final native long BINKREALTIME_FramesThreadReadTime_get(long j, C6084agE age);

    public static final native void BINKREALTIME_FramesThreadReadTime_set(long j, C6084agE age, long j2);

    public static final native long BINKREALTIME_FramesTime_get(long j, C6084agE age);

    public static final native void BINKREALTIME_FramesTime_set(long j, C6084agE age, long j2);

    public static final native long BINKREALTIME_FramesVideoDecompTime_get(long j, C6084agE age);

    public static final native void BINKREALTIME_FramesVideoDecompTime_set(long j, C6084agE age, long j2);

    public static final native long BINKREALTIME_Frames_get(long j, C6084agE age);

    public static final native void BINKREALTIME_Frames_set(long j, C6084agE age, long j2);

    public static final native long BINKREALTIME_ReadBufferSize_get(long j, C6084agE age);

    public static final native void BINKREALTIME_ReadBufferSize_set(long j, C6084agE age, long j2);

    public static final native long BINKREALTIME_ReadBufferUsed_get(long j, C6084agE age);

    public static final native void BINKREALTIME_ReadBufferUsed_set(long j, C6084agE age, long j2);

    public static final native int BINKRECT_Height_get(long j, C6679arb arb);

    public static final native void BINKRECT_Height_set(long j, C6679arb arb, int i);

    public static final native int BINKRECT_Left_get(long j, C6679arb arb);

    public static final native void BINKRECT_Left_set(long j, C6679arb arb, int i);

    public static final native int BINKRECT_Top_get(long j, C6679arb arb);

    public static final native void BINKRECT_Top_set(long j, C6679arb arb, int i);

    public static final native int BINKRECT_Width_get(long j, C6679arb arb);

    public static final native void BINKRECT_Width_set(long j, C6679arb arb, int i);

    public static final native int BINKSND_BestSizeIn16_get(long j, C2677iS iSVar);

    public static final native void BINKSND_BestSizeIn16_set(long j, C2677iS iSVar, int i);

    public static final native long BINKSND_BestSizeMask_get(long j, C2677iS iSVar);

    public static final native void BINKSND_BestSizeMask_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_Close_get(long j, C2677iS iSVar);

    public static final native void BINKSND_Close_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_Latency_get(long j, C2677iS iSVar);

    public static final native void BINKSND_Latency_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_Lock_get(long j, C2677iS iSVar);

    public static final native void BINKSND_Lock_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_MixBinVols_get(long j, C2677iS iSVar);

    public static final native void BINKSND_MixBinVols_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_MixBins_get(long j, C2677iS iSVar);

    public static final native void BINKSND_MixBins_set(long j, C2677iS iSVar, long j2);

    public static final native int BINKSND_NoThreadService_get(long j, C2677iS iSVar);

    public static final native void BINKSND_NoThreadService_set(long j, C2677iS iSVar, int i);

    public static final native int BINKSND_OnOff_get(long j, C2677iS iSVar);

    public static final native void BINKSND_OnOff_set(long j, C2677iS iSVar, int i);

    public static final native long BINKSND_Pan_get(long j, C2677iS iSVar);

    public static final native void BINKSND_Pan_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_Pause_get(long j, C2677iS iSVar);

    public static final native void BINKSND_Pause_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_Ready_get(long j, C2677iS iSVar);

    public static final native void BINKSND_Ready_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_SetOnOff_get(long j, C2677iS iSVar);

    public static final native void BINKSND_SetOnOff_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_SoundDroppedOut_get(long j, C2677iS iSVar);

    public static final native void BINKSND_SoundDroppedOut_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_Unlock_get(long j, C2677iS iSVar);

    public static final native void BINKSND_Unlock_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_VideoScale_get(long j, C2677iS iSVar);

    public static final native void BINKSND_VideoScale_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_Volume_get(long j, C2677iS iSVar);

    public static final native void BINKSND_Volume_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_audiodecompsize_get(long j, C2677iS iSVar);

    public static final native void BINKSND_audiodecompsize_set(long j, C2677iS iSVar, long j2);

    public static final native int BINKSND_bits_get(long j, C2677iS iSVar);

    public static final native void BINKSND_bits_set(long j, C2677iS iSVar, int i);

    public static final native int BINKSND_chans_get(long j, C2677iS iSVar);

    public static final native void BINKSND_chans_set(long j, C2677iS iSVar, int i);

    public static final native long BINKSND_freq_get(long j, C2677iS iSVar);

    public static final native void BINKSND_freq_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_orig_freq_get(long j, C2677iS iSVar);

    public static final native void BINKSND_orig_freq_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_padding_get(long j, C2677iS iSVar);

    public static final native void BINKSND_padding_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_sndbuf_get(long j, C2677iS iSVar);

    public static final native void BINKSND_sndbuf_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_sndbufsize_get(long j, C2677iS iSVar);

    public static final native void BINKSND_sndbufsize_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_sndcomp_get(long j, C2677iS iSVar);

    public static final native void BINKSND_sndcomp_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_sndconvert8_get(long j, C2677iS iSVar);

    public static final native void BINKSND_sndconvert8_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_snddata_get(long j, C2677iS iSVar);

    public static final native void BINKSND_snddata_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_sndend_get(long j, C2677iS iSVar);

    public static final native void BINKSND_sndend_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_sndendframe_get(long j, C2677iS iSVar);

    public static final native void BINKSND_sndendframe_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_sndpad_get(long j, C2677iS iSVar);

    public static final native void BINKSND_sndpad_set(long j, C2677iS iSVar, long j2);

    public static final native int BINKSND_sndprime_get(long j, C2677iS iSVar);

    public static final native void BINKSND_sndprime_set(long j, C2677iS iSVar, int i);

    public static final native long BINKSND_sndreadpos_get(long j, C2677iS iSVar);

    public static final native void BINKSND_sndreadpos_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSND_sndwritepos_get(long j, C2677iS iSVar);

    public static final native void BINKSND_sndwritepos_set(long j, C2677iS iSVar, long j2);

    public static final native long BINKSUMMARY_AverageDataRate_get(long j, aCW acw);

    public static final native void BINKSUMMARY_AverageDataRate_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_AverageFrameSize_get(long j, aCW acw);

    public static final native void BINKSUMMARY_AverageFrameSize_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_FileFrameRateDiv_get(long j, aCW acw);

    public static final native void BINKSUMMARY_FileFrameRateDiv_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_FileFrameRate_get(long j, aCW acw);

    public static final native void BINKSUMMARY_FileFrameRate_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_FrameRateDiv_get(long j, aCW acw);

    public static final native void BINKSUMMARY_FrameRateDiv_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_FrameRate_get(long j, aCW acw);

    public static final native void BINKSUMMARY_FrameRate_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_Height_get(long j, aCW acw);

    public static final native void BINKSUMMARY_Height_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_Highest1SecFrame_get(long j, aCW acw);

    public static final native void BINKSUMMARY_Highest1SecFrame_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_Highest1SecRate_get(long j, aCW acw);

    public static final native void BINKSUMMARY_Highest1SecRate_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_HighestIOUsed_get(long j, aCW acw);

    public static final native void BINKSUMMARY_HighestIOUsed_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_HighestMemAmount_get(long j, aCW acw);

    public static final native void BINKSUMMARY_HighestMemAmount_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_SkippedBlits_get(long j, aCW acw);

    public static final native void BINKSUMMARY_SkippedBlits_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_SkippedFrames_get(long j, aCW acw);

    public static final native void BINKSUMMARY_SkippedFrames_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_Slowest2FrameNum_get(long j, aCW acw);

    public static final native void BINKSUMMARY_Slowest2FrameNum_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_Slowest2FrameTime_get(long j, aCW acw);

    public static final native void BINKSUMMARY_Slowest2FrameTime_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_SlowestFrameNum_get(long j, aCW acw);

    public static final native void BINKSUMMARY_SlowestFrameNum_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_SlowestFrameTime_get(long j, aCW acw);

    public static final native void BINKSUMMARY_SlowestFrameTime_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_SoundSkips_get(long j, aCW acw);

    public static final native void BINKSUMMARY_SoundSkips_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_TotalAudioDecompTime_get(long j, aCW acw);

    public static final native void BINKSUMMARY_TotalAudioDecompTime_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_TotalBackReadTime_get(long j, aCW acw);

    public static final native void BINKSUMMARY_TotalBackReadTime_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_TotalBlitTime_get(long j, aCW acw);

    public static final native void BINKSUMMARY_TotalBlitTime_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_TotalFrames_get(long j, aCW acw);

    public static final native void BINKSUMMARY_TotalFrames_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_TotalIOMemory_get(long j, aCW acw);

    public static final native void BINKSUMMARY_TotalIOMemory_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_TotalIdleReadTime_get(long j, aCW acw);

    public static final native void BINKSUMMARY_TotalIdleReadTime_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_TotalOpenTime_get(long j, aCW acw);

    public static final native void BINKSUMMARY_TotalOpenTime_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_TotalPlayedFrames_get(long j, aCW acw);

    public static final native void BINKSUMMARY_TotalPlayedFrames_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_TotalReadSpeed_get(long j, aCW acw);

    public static final native void BINKSUMMARY_TotalReadSpeed_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_TotalReadTime_get(long j, aCW acw);

    public static final native void BINKSUMMARY_TotalReadTime_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_TotalTime_get(long j, aCW acw);

    public static final native void BINKSUMMARY_TotalTime_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_TotalVideoDecompTime_get(long j, aCW acw);

    public static final native void BINKSUMMARY_TotalVideoDecompTime_set(long j, aCW acw, long j2);

    public static final native long BINKSUMMARY_Width_get(long j, aCW acw);

    public static final native void BINKSUMMARY_Width_set(long j, aCW acw, long j2);

    public static final native long BINKTRACK_Bits_get(long j, C6500aoE aoe);

    public static final native void BINKTRACK_Bits_set(long j, C6500aoE aoe, long j2);

    public static final native long BINKTRACK_Channels_get(long j, C6500aoE aoe);

    public static final native void BINKTRACK_Channels_set(long j, C6500aoE aoe, long j2);

    public static final native long BINKTRACK_Frequency_get(long j, C6500aoE aoe);

    public static final native void BINKTRACK_Frequency_set(long j, C6500aoE aoe, long j2);

    public static final native long BINKTRACK_MaxSize_get(long j, C6500aoE aoe);

    public static final native void BINKTRACK_MaxSize_set(long j, C6500aoE aoe, long j2);

    public static final native long BINKTRACK_bink_get(long j, C6500aoE aoe);

    public static final native void BINKTRACK_bink_set(long j, C6500aoE aoe, long j2, C2427fN fNVar);

    public static final native long BINKTRACK_sndcomp_get(long j, C6500aoE aoe);

    public static final native void BINKTRACK_sndcomp_set(long j, C6500aoE aoe, long j2);

    public static final native int BINKTRACK_trackindex_get(long j, C6500aoE aoe);

    public static final native void BINKTRACK_trackindex_set(long j, C6500aoE aoe, int i);

    public static final native long BINK_AsyncMaskPlane_get(long j, C2427fN fNVar);

    public static final native void BINK_AsyncMaskPlane_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_BinkType_get(long j, C2427fN fNVar);

    public static final native void BINK_BinkType_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_FrameBuffers_get(long j, C2427fN fNVar);

    public static final native void BINK_FrameBuffers_set(long j, C2427fN fNVar, long j2, C2816kW kWVar);

    public static final native long BINK_FrameChangePercent_get(long j, C2427fN fNVar);

    public static final native void BINK_FrameChangePercent_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_FrameNum_get(long j, C2427fN fNVar);

    public static final native void BINK_FrameNum_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_FrameRateDiv_get(long j, C2427fN fNVar);

    public static final native void BINK_FrameRateDiv_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_FrameRate_get(long j, C2427fN fNVar);

    public static final native void BINK_FrameRate_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_FrameRects_get(long j, C2427fN fNVar);

    public static final native void BINK_FrameRects_set(long j, C2427fN fNVar, long j2, C6679arb arb);

    public static final native long BINK_FrameSize_get(long j, C2427fN fNVar);

    public static final native void BINK_FrameSize_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_Frames_get(long j, C2427fN fNVar);

    public static final native void BINK_Frames_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_Height_get(long j, C2427fN fNVar);

    public static final native void BINK_Height_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_Highest1SecFrame_get(long j, C2427fN fNVar);

    public static final native void BINK_Highest1SecFrame_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_Highest1SecRate_get(long j, C2427fN fNVar);

    public static final native void BINK_Highest1SecRate_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_InUseMaskPlane_get(long j, C2427fN fNVar);

    public static final native void BINK_InUseMaskPlane_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_InternalFrames_get(long j, C2427fN fNVar);

    public static final native void BINK_InternalFrames_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_LargestFrameSize_get(long j, C2427fN fNVar);

    public static final native void BINK_LargestFrameSize_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_LastFrameNum_get(long j, C2427fN fNVar);

    public static final native void BINK_LastFrameNum_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_LastMaskPlane_get(long j, C2427fN fNVar);

    public static final native void BINK_LastMaskPlane_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_MaskLength_get(long j, C2427fN fNVar);

    public static final native void BINK_MaskLength_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_MaskPitch_get(long j, C2427fN fNVar);

    public static final native void BINK_MaskPitch_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_MaskPlane_get(long j, C2427fN fNVar);

    public static final native void BINK_MaskPlane_set(long j, C2427fN fNVar, long j2);

    public static final native int BINK_NumRects_get(long j, C2427fN fNVar);

    public static final native void BINK_NumRects_set(long j, C2427fN fNVar, int i);

    public static final native int BINK_NumTracks_get(long j, C2427fN fNVar);

    public static final native void BINK_NumTracks_set(long j, C2427fN fNVar, int i);

    public static final native long BINK_OpenFlags_get(long j, C2427fN fNVar);

    public static final native void BINK_OpenFlags_set(long j, C2427fN fNVar, long j2);

    public static final native int BINK_Paused_get(long j, C2427fN fNVar);

    public static final native void BINK_Paused_set(long j, C2427fN fNVar, int i);

    public static final native long BINK_ReadError_get(long j, C2427fN fNVar);

    public static final native void BINK_ReadError_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_Size_get(long j, C2427fN fNVar);

    public static final native void BINK_Size_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_SndSize_get(long j, C2427fN fNVar);

    public static final native void BINK_SndSize_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_Width_get(long j, C2427fN fNVar);

    public static final native void BINK_Width_set(long j, C2427fN fNVar, long j2);

    public static final native int BINK_allkeys_get(long j, C2427fN fNVar);

    public static final native void BINK_allkeys_set(long j, C2427fN fNVar, int i);

    public static final native long BINK_allocatedframebuffers_get(long j, C2427fN fNVar);

    public static final native void BINK_allocatedframebuffers_set(long j, C2427fN fNVar, long j2, C2816kW kWVar);

    public static final native long BINK_async_in_progress_get(long j, C2427fN fNVar);

    public static final native void BINK_async_in_progress_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_bio_get(long j, C2427fN fNVar);

    public static final native void BINK_bio_set(long j, C2427fN fNVar, long j2, C5340aFo afo);

    public static final native long BINK_bsnd_get(long j, C2427fN fNVar);

    public static final native void BINK_bsnd_set(long j, C2427fN fNVar, long j2, C2677iS iSVar);

    public static final native long BINK_bunp_get(long j, C2427fN fNVar);

    public static final native void BINK_bunp_set(long j, C2427fN fNVar, long j2, C3000mo moVar);

    public static final native long BINK_changepercent_get(long j, C2427fN fNVar);

    public static final native void BINK_changepercent_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_compframe_get(long j, C2427fN fNVar);

    public static final native void BINK_compframe_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_compframekey_get(long j, C2427fN fNVar);

    public static final native void BINK_compframekey_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_compframeoffset_get(long j, C2427fN fNVar);

    public static final native void BINK_compframeoffset_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_compframesize_get(long j, C2427fN fNVar);

    public static final native void BINK_compframesize_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_decompheight_get(long j, C2427fN fNVar);

    public static final native void BINK_decompheight_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_decompwidth_get(long j, C2427fN fNVar);

    public static final native void BINK_decompwidth_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_doresync_get(long j, C2427fN fNVar);

    public static final native void BINK_doresync_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_fileframerate_get(long j, C2427fN fNVar);

    public static final native void BINK_fileframerate_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_fileframeratediv_get(long j, C2427fN fNVar);

    public static final native void BINK_fileframeratediv_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_firstframetime_get(long j, C2427fN fNVar);

    public static final native void BINK_firstframetime_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_frameoffsets_get(long j, C2427fN fNVar);

    public static final native void BINK_frameoffsets_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_ioptr_get(long j, C2427fN fNVar);

    public static final native void BINK_ioptr_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_iosize_get(long j, C2427fN fNVar);

    public static final native void BINK_iosize_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_last_read_count_get(long j, C2427fN fNVar);

    public static final native void BINK_last_read_count_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_last_sound_count_get(long j, C2427fN fNVar);

    public static final native void BINK_last_sound_count_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_last_time_almost_empty_get(long j, C2427fN fNVar);

    public static final native void BINK_last_time_almost_empty_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_lastblitflags_get(long j, C2427fN fNVar);

    public static final native void BINK_lastblitflags_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_lastdecompframe_get(long j, C2427fN fNVar);

    public static final native void BINK_lastdecompframe_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_lastfinisheddoframe_get(long j, C2427fN fNVar);

    public static final native void BINK_lastfinisheddoframe_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_lastresynctime_get(long j, C2427fN fNVar);

    public static final native void BINK_lastresynctime_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_numrects_get(long j, C2427fN fNVar);

    public static final native void BINK_numrects_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_paused_sync_diff_get(long j, C2427fN fNVar);

    public static final native void BINK_paused_sync_diff_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_playedframes_get(long j, C2427fN fNVar);

    public static final native void BINK_playedframes_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_playingtracks_get(long j, C2427fN fNVar);

    public static final native void BINK_playingtracks_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_preloadptr_get(long j, C2427fN fNVar);

    public static final native void BINK_preloadptr_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_rtadecomptimes_get(long j, C2427fN fNVar);

    public static final native void BINK_rtadecomptimes_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_rtblittimes_get(long j, C2427fN fNVar);

    public static final native void BINK_rtblittimes_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_rtframetimes_get(long j, C2427fN fNVar);

    public static final native void BINK_rtframetimes_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_rtidlereadtimes_get(long j, C2427fN fNVar);

    public static final native void BINK_rtidlereadtimes_set(long j, C2427fN fNVar, long j2);

    public static final native int BINK_rtindex_get(long j, C2427fN fNVar);

    public static final native void BINK_rtindex_set(long j, C2427fN fNVar, int i);

    public static final native long BINK_rtreadtimes_get(long j, C2427fN fNVar);

    public static final native void BINK_rtreadtimes_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_rtthreadreadtimes_get(long j, C2427fN fNVar);

    public static final native void BINK_rtthreadreadtimes_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_rtvdecomptimes_get(long j, C2427fN fNVar);

    public static final native void BINK_rtvdecomptimes_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_runtimeframes_get(long j, C2427fN fNVar);

    public static final native void BINK_runtimeframes_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_skipped_in_a_row_get(long j, C2427fN fNVar);

    public static final native void BINK_skipped_in_a_row_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_skipped_status_this_frame_get(long j, C2427fN fNVar);

    public static final native void BINK_skipped_status_this_frame_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_skippedblits_get(long j, C2427fN fNVar);

    public static final native void BINK_skippedblits_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_skippedlastblit_get(long j, C2427fN fNVar);

    public static final native void BINK_skippedlastblit_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_slowest2frame_get(long j, C2427fN fNVar);

    public static final native void BINK_slowest2frame_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_slowest2frametime_get(long j, C2427fN fNVar);

    public static final native void BINK_slowest2frametime_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_slowestframe_get(long j, C2427fN fNVar);

    public static final native void BINK_slowestframe_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_slowestframetime_get(long j, C2427fN fNVar);

    public static final native void BINK_slowestframetime_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_snd_callback_buffer_get(long j, C2427fN fNVar);

    public static final native void BINK_snd_callback_buffer_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_soundon_get(long j, C2427fN fNVar);

    public static final native void BINK_soundon_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_soundskips_get(long j, C2427fN fNVar);

    public static final native void BINK_soundskips_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_startblittime_get(long j, C2427fN fNVar);

    public static final native void BINK_startblittime_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_startsyncframe_get(long j, C2427fN fNVar);

    public static final native void BINK_startsyncframe_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_startsynctime_get(long j, C2427fN fNVar);

    public static final native void BINK_startsynctime_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_timeadecomp_get(long j, C2427fN fNVar);

    public static final native void BINK_timeadecomp_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_timeblit_get(long j, C2427fN fNVar);

    public static final native void BINK_timeblit_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_timeopen_get(long j, C2427fN fNVar);

    public static final native void BINK_timeopen_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_timevdecomp_get(long j, C2427fN fNVar);

    public static final native void BINK_timevdecomp_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_totalmem_get(long j, C2427fN fNVar);

    public static final native void BINK_totalmem_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_trackIDs_get(long j, C2427fN fNVar);

    public static final native void BINK_trackIDs_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_trackindexes_get(long j, C2427fN fNVar);

    public static final native void BINK_trackindexes_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_tracksizes_get(long j, C2427fN fNVar);

    public static final native void BINK_tracksizes_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_tracktypes_get(long j, C2427fN fNVar);

    public static final native void BINK_tracktypes_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_twoframestime_get(long j, C2427fN fNVar);

    public static final native void BINK_twoframestime_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_very_delayed_get(long j, C2427fN fNVar);

    public static final native void BINK_very_delayed_set(long j, C2427fN fNVar, long j2);

    public static final native long BINK_videoon_get(long j, C2427fN fNVar);

    public static final native void BINK_videoon_set(long j, C2427fN fNVar, long j2);

    public static final native long BUNDLEPOINTERS_bits2ptr_get(long j, C3000mo moVar);

    public static final native void BUNDLEPOINTERS_bits2ptr_set(long j, C3000mo moVar, long j2);

    public static final native long BUNDLEPOINTERS_colorptr_get(long j, C3000mo moVar);

    public static final native void BUNDLEPOINTERS_colorptr_set(long j, C3000mo moVar, long j2);

    public static final native long BUNDLEPOINTERS_dctptr_get(long j, C3000mo moVar);

    public static final native void BUNDLEPOINTERS_dctptr_set(long j, C3000mo moVar, long j2);

    public static final native long BUNDLEPOINTERS_mdctptr_get(long j, C3000mo moVar);

    public static final native void BUNDLEPOINTERS_mdctptr_set(long j, C3000mo moVar, long j2);

    public static final native long BUNDLEPOINTERS_motionXptr_get(long j, C3000mo moVar);

    public static final native void BUNDLEPOINTERS_motionXptr_set(long j, C3000mo moVar, long j2);

    public static final native long BUNDLEPOINTERS_motionYptr_get(long j, C3000mo moVar);

    public static final native void BUNDLEPOINTERS_motionYptr_set(long j, C3000mo moVar, long j2);

    public static final native long BUNDLEPOINTERS_patptr_get(long j, C3000mo moVar);

    public static final native void BUNDLEPOINTERS_patptr_set(long j, C3000mo moVar, long j2);

    public static final native long BUNDLEPOINTERS_type16ptr_get(long j, C3000mo moVar);

    public static final native void BUNDLEPOINTERS_type16ptr_set(long j, C3000mo moVar, long j2);

    public static final native long BUNDLEPOINTERS_typeptr_get(long j, C3000mo moVar);

    public static final native void BUNDLEPOINTERS_typeptr_set(long j, C3000mo moVar, long j2);

    public static final native void BinkBufferBlit(long j, C6673arV arv, long j2, C6679arb arb, long j3);

    public static final native void BinkBufferCheckWinPos(long j, C6673arV arv, long j2, long j3);

    public static final native int BinkBufferClear(long j, C6673arV arv, long j2);

    public static final native void BinkBufferClose(long j, C6673arV arv);

    public static final native String BinkBufferGetDescription(long j, C6673arV arv);

    public static final native String BinkBufferGetError();

    public static final native int BinkBufferLock(long j, C6673arV arv);

    public static final native long BinkBufferOpen(long j, long j2, long j3, long j4);

    public static final native int BinkBufferSetDirectDraw(long j, long j2);

    public static final native int BinkBufferSetHWND(long j, C6673arV arv, long j2);

    public static final native int BinkBufferSetOffset(long j, C6673arV arv, int i, int i2);

    public static final native void BinkBufferSetResolution(int i, int i2, int i3);

    public static final native int BinkBufferSetScale(long j, C6673arV arv, long j2, long j3);

    public static final native int BinkBufferUnlock(long j, C6673arV arv);

    public static final native int BinkCheckCursor(long j, int i, int i2, int i3, int i4);

    public static final native void BinkClose(long j, C2427fN fNVar);

    public static final native void BinkCloseTrack(long j, C6500aoE aoe);

    public static final native int BinkControlBackgroundIO(long j, C2427fN fNVar, long j2);

    public static final native int BinkControlPlatformFeatures(int i, int i2);

    public static final native int BinkCopyToBuffer(long j, C2427fN fNVar, Buffer buffer, int i, long j2, long j3, long j4, long j5);

    public static final native int BinkCopyToBufferRect(long j, C2427fN fNVar, Buffer buffer, int i, long j2, long j3, long j4, long j5, long j6, long j7, long j8, long j9);

    public static final native int BinkDDSurfaceType(long j);

    public static final native int BinkDX8SurfaceType(long j);

    public static final native int BinkDX9SurfaceType(long j);

    public static final native int BinkDoFrame(long j, C2427fN fNVar);

    public static final native int BinkDoFrameAsync(long j, C2427fN fNVar, long j2, long j3);

    public static final native int BinkDoFrameAsyncWait(long j, C2427fN fNVar, int i);

    public static final native String BinkGetError();

    public static final native void BinkGetFrameBuffersInfo(long j, C2427fN fNVar, long j2, C2816kW kWVar);

    public static final native long BinkGetKeyFrame(long j, C2427fN fNVar, long j2, int i);

    public static final native void BinkGetPalette(long j);

    public static final native void BinkGetRealtime(long j, C2427fN fNVar, long j2, C6084agE age, long j3);

    public static final native int BinkGetRects(long j, C2427fN fNVar, long j2);

    public static final native void BinkGetSummary(long j, C2427fN fNVar, long j2, aCW acw);

    public static final native long BinkGetTrackData(long j, C6500aoE aoe, Buffer buffer);

    public static final native long BinkGetTrackID(long j, C2427fN fNVar, long j2);

    public static final native long BinkGetTrackMaxSize(long j, C2427fN fNVar, long j2);

    public static final native long BinkGetTrackType(long j, C2427fN fNVar, long j2);

    public static final native void BinkGoto(long j, C2427fN fNVar, long j2, int i);

    public static final native int BinkIsSoftwareCursor(long j, long j2);

    public static final native long BinkLogoAddress();

    public static final native void BinkNextFrame(long j, C2427fN fNVar);

    public static final native long BinkOpen(String str, long j);

    public static final native long BinkOpenDirectSound(long j);

    public static final native long BinkOpenMiles(long j);

    public static final native long BinkOpenTrack(long j, C2427fN fNVar, long j2);

    public static final native long BinkOpenWaveOut(long j);

    public static final native int BinkPause(long j, C2427fN fNVar, int i);

    public static final native void BinkRegisterFrameBuffers(long j, C2427fN fNVar, long j2, C2816kW kWVar);

    public static final native int BinkRequestStopAsyncThread(int i);

    public static final native void BinkRestoreCursor(int i);

    public static final native void BinkService(long j, C2427fN fNVar);

    public static final native void BinkSetError(String str);

    public static final native void BinkSetFrameRate(long j, long j2);

    public static final native void BinkSetIO(long j);

    public static final native void BinkSetIOSize(long j);

    public static final native void BinkSetMemory(long j, long j2);

    public static final native void BinkSetMixBinVolumes(long j, C2427fN fNVar, long j2, long j3, long j4, long j5);

    public static final native void BinkSetMixBins(long j, C2427fN fNVar, long j2, long j3, long j4);

    public static final native void BinkSetPan(long j, C2427fN fNVar, long j2, int i);

    public static final native void BinkSetSimulate(long j);

    public static final native int BinkSetSoundOnOff(long j, C2427fN fNVar, int i);

    public static final native int BinkSetSoundSystem(long j, long j2);

    public static final native void BinkSetSoundTrack(long j, long j2);

    public static final native int BinkSetVideoOnOff(long j, C2427fN fNVar, int i);

    public static final native void BinkSetVolume(long j, C2427fN fNVar, long j2, int i);

    public static final native int BinkShouldSkip(long j, C2427fN fNVar);

    public static final native int BinkStartAsyncThread(int i, long j);

    public static final native int BinkWait(long j, C2427fN fNVar);

    public static final native int BinkWaitStopAsyncThread(int i);

    public static final native void delete_BINK(long j);

    public static final native void delete_BINKBUFFER(long j);

    public static final native void delete_BINKFRAMEBUFFERS(long j);

    public static final native void delete_BINKFRAMEPLANESET(long j);

    public static final native void delete_BINKHDR(long j);

    public static final native void delete_BINKIO(long j);

    public static final native void delete_BINKPLANE(long j);

    public static final native void delete_BINKREALTIME(long j);

    public static final native void delete_BINKRECT(long j);

    public static final native void delete_BINKSND(long j);

    public static final native void delete_BINKSUMMARY(long j);

    public static final native void delete_BINKTRACK(long j);

    public static final native void delete_BUNDLEPOINTERS(long j);

    public static final native long new_BINK();

    public static final native long new_BINKBUFFER();

    public static final native long new_BINKFRAMEBUFFERS();

    public static final native long new_BINKFRAMEPLANESET();

    public static final native long new_BINKHDR();

    public static final native long new_BINKIO();

    public static final native long new_BINKPLANE();

    public static final native long new_BINKREALTIME();

    public static final native long new_BINKRECT();

    public static final native long new_BINKSND();

    public static final native long new_BINKSUMMARY();

    public static final native long new_BINKTRACK();

    public static final native long new_BUNDLEPOINTERS();

    public static final native int wBinkSoundUseDirectSound(int i);
}
