package org.w3c.css.sac;

/* renamed from: a.qM */
/* compiled from: a */
public interface ProcessingInstructionSelector extends SimpleSelector {
    String getData();

    String getTarget();
}
