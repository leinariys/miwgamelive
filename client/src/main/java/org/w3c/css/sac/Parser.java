package org.w3c.css.sac;

import com.steadystate.css.sac.DocumentHandler;
import logic.res.css.C0864MW;
import logic.res.css.C1704ZE;

import java.util.Locale;

/* renamed from: a.adu  reason: case insensitive filesystem */
/* compiled from: a */
public interface Parser {
    /* renamed from: a */
    void mo5996a(C0864MW mw);

    /* renamed from: a */
    void mo5997a(C1704ZE ze);

    /* renamed from: a */
    void mo6001a(ErrorHandler mLVar);

    /* renamed from: a */
    void setDocumentHandler(DocumentHandler tvVar);

    String getParserVersion();

    /* renamed from: g */
    SelectorList parseSelectors(InputSource fh);

    /* renamed from: h */
    void parseStyleSheet(InputSource fh);

    /* renamed from: hl */
    void parseStyleSheet(String str);

    /* renamed from: i */
    void parseStyleDeclaration(InputSource fh);

    /* renamed from: j */
    void parseRule(InputSource fh);

    /* renamed from: k */
    LexicalUnit parsePropertyValue(InputSource fh);

    /* renamed from: l */
    boolean parsePriority(InputSource fh);

    void setLocale(Locale locale);
}
