package org.w3c.css.sac;

/* renamed from: a.Vu */
/* compiled from: a */
public interface ConditionalSelector extends SimpleSelector {
    /* renamed from: Bv */
    SimpleSelector getSimpleSelector();

    /* renamed from: SW */
    Condition getCondition();
}
