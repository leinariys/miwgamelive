package org.w3c.css.sac;

/* renamed from: a.ny */
/* compiled from: a */
public interface ElementSelector extends SimpleSelector {
    String getLocalName();

    String getNamespaceURI();
}
