package org.w3c.css.sac;

/* renamed from: a.akt  reason: case insensitive filesystem */
/* compiled from: a */
public interface AttributeCondition extends Condition {
    String getLocalName();

    String getNamespaceURI();

    boolean getSpecified();

    String getValue();
}
