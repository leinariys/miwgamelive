package org.w3c.css.sac;

import java.io.InputStream;
import java.io.Reader;

/* renamed from: a.FH */
/* compiled from: a */
public class InputSource {
    private InputStream byteStream;
    private Reader characterStream;
    private String media;
    private String encoding;
    private String title;
    private String uri;

    public InputSource() {
    }

    public InputSource(String str) {
        setURI(str);
    }

    public InputSource(Reader reader) {
        setCharacterStream(reader);
    }

    public String getURI() {
        return this.uri;
    }

    public void setURI(String str) {
        this.uri = str;
    }

    public InputStream getByteStream() {
        return this.byteStream;
    }

    public void setByteStream(InputStream inputStream) {
        this.byteStream = inputStream;
    }

    public String getEncoding() {
        return this.encoding;
    }

    public void setEncoding(String str) {
        this.encoding = str;
    }

    public Reader getCharacterStream() {
        return this.characterStream;
    }

    public void setCharacterStream(Reader reader) {
        this.characterStream = reader;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public String getMedia() {
        if (this.media == null) {
            return "all";
        }
        return this.media;
    }

    public void setMedia(String str) {
        this.media = str;
    }
}
