package org.mozilla1.javascript.regexp;

import org.mozilla1.javascript.Context;

/* renamed from: a.Pk */
/* compiled from: a */
public class CompilerState {

    /* renamed from: cp */
    int f1382cp = 0;
    Context dPJ;
    char[] dPK;
    int dPL;
    int dPM;
    int dPN;
    int dPO;
    int dPP;
    RENode dPQ;
    int flags;

    CompilerState(Context lhVar, char[] cArr, int i, int i2) {
        this.dPJ = lhVar;
        this.dPK = cArr;
        this.dPL = i;
        this.flags = i2;
        this.dPM = 0;
        this.dPO = 0;
        this.dPP = 0;
    }
}
