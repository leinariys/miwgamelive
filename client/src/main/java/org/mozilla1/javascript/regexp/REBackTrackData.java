package org.mozilla1.javascript.regexp;

/* renamed from: a.UU */
/* compiled from: a */
public class REBackTrackData {

    /* renamed from: cp */
    int f1768cp;
    REBackTrackData erb;
    int erc;
    int erd;
    int ere;
    long[] erf;
    org.mozilla1.javascript.regexp.REProgState erg;

    REBackTrackData(org.mozilla1.javascript.regexp.REGlobalData aij, int i, int i2) {
        this.erb = aij.fOz;
        this.erc = i;
        this.erd = i2;
        this.ere = aij.ere;
        if (aij.erf != null) {
            this.erf = (long[]) aij.erf.clone();
        }
        this.f1768cp = aij.f4604cp;
        this.erg = aij.erg;
    }
}
