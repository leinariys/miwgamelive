package org.mozilla1.javascript.regexp;

/* renamed from: a.aiJ  reason: case insensitive filesystem */
/* compiled from: a */
public class REGlobalData {

    /* renamed from: cp */
    int f4604cp;
    int ere;
    long[] erf;
    REProgState erg;
    boolean fOw;
    RECompiled fOx;
    int fOy;
    REBackTrackData fOz;

    REGlobalData() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: rj */
    public int mo13662rj(int i) {
        return (int) this.erf[i];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: rk */
    public int mo13663rk(int i) {
        return (int) (this.erf[i] >>> 32);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: t */
    public void mo13664t(int i, int i2, int i3) {
        this.erf[i] = (((long) i2) & 4294967295L) | (((long) i3) << 32);
    }
}
