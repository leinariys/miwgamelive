package org.mozilla1.javascript.regexp;

import java.io.Serializable;

/* renamed from: a.aQo  reason: case insensitive filesystem */
/* compiled from: a */
public final class RECharSet implements Serializable {
    static final long serialVersionUID = 7931787979395898394L;
    int iEv;
    volatile transient boolean iEw;
    volatile transient boolean iEx;
    volatile transient byte[] iEy;
    int length;
    int startIndex;

    RECharSet(int i, int i2, int i3) {
        this.length = i;
        this.startIndex = i2;
        this.iEv = i3;
    }
}
