package org.mozilla1.javascript.regexp;

/* renamed from: a.asW  reason: case insensitive filesystem */
/* compiled from: a */
public class REProgState {
    int erc;
    int erd;
    REProgState gxA;
    REBackTrackData gxB;
    int index;
    int max;
    int min;

    REProgState(REProgState asw, int i, int i2, int i3, REBackTrackData uu, int i4, int i5) {
        this.gxA = asw;
        this.min = i;
        this.max = i2;
        this.index = i3;
        this.erc = i5;
        this.erd = i4;
        this.gxB = uu;
    }
}
