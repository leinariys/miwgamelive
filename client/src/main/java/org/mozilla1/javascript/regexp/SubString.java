package org.mozilla1.javascript.regexp;

/* renamed from: a.tq */
/* compiled from: a */
public class SubString {
    static final SubString bmi = new SubString();
    char[] bmj;
    int index;
    int length;

    public SubString() {
    }

    public SubString(String str) {
        this.index = 0;
        this.bmj = str.toCharArray();
        this.length = str.length();
    }

    public SubString(char[] cArr, int i, int i2) {
        this.index = 0;
        this.length = i2;
        this.bmj = new char[i2];
        for (int i3 = 0; i3 < i2; i3++) {
            this.bmj[i3] = cArr[i + i3];
        }
    }

    public String toString() {
        if (this.bmj == null) {
            return "";
        }
        return new String(this.bmj, this.index, this.length);
    }
}
