package org.mozilla1.javascript.regexp;

import org.mozilla1.javascript.ScriptRuntime;

/* renamed from: a.avk  reason: case insensitive filesystem */
/* compiled from: a */
public class NativeRegExpCtor extends org.mozilla1.javascript.BaseFunction {
    static final long serialVersionUID = -5733330028285400526L;
    private static final int eVp = 21;
    private static final int fpr = 1;
    private static final int gGH = 2;
    private static final int gGI = 3;
    private static final int gGJ = 4;
    private static final int gGK = 5;
    private static final int gGL = 6;
    private static final int gGM = 7;
    private static final int gGN = 8;
    private static final int gGO = 9;
    private static final int gGP = 10;
    private static final int gGQ = 11;
    private static final int gGR = 12;
    private static final int gGS = 12;
    private static final int gGT = 13;
    private static final int gGU = 14;
    private static final int gGV = 15;
    private static final int gGW = 16;
    private static final int gGX = 17;
    private static final int gGY = 18;
    private static final int gGZ = 19;
    private static final int gHa = 20;
    private static final int gHb = 21;

    NativeRegExpCtor() {
    }

    private static org.mozilla1.javascript.regexp.RegExpImpl czb() {
        return (org.mozilla1.javascript.regexp.RegExpImpl) org.mozilla1.javascript.ScriptRuntime.m7592l(org.mozilla1.javascript.Context.bwq());
    }

    public String getFunctionName() {
        return "RegExp";
    }

    public Object call(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        if (objArr.length <= 0 || !(objArr[0] instanceof org.mozilla1.javascript.regexp.NativeRegExp) || (objArr.length != 1 && objArr[1] != org.mozilla1.javascript.Undefined.instance)) {
            return construct(lhVar, avf, objArr);
        }
        return objArr[0];
    }

    public org.mozilla1.javascript.Scriptable construct(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr) {
        org.mozilla1.javascript.regexp.NativeRegExp aex = new NativeRegExp();
        aex.mo13195c(lhVar, avf, objArr);
        org.mozilla1.javascript.ScriptRuntime.m7528a((org.mozilla1.javascript.ScriptableObject) aex, avf);
        return aex;
    }

    /* access modifiers changed from: protected */
    public int getMaxInstanceId() {
        return super.getMaxInstanceId() + 21;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x000e, code lost:
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000f, code lost:
        if (r4 == null) goto L_0x00ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0011, code lost:
        if (r4 == r8) goto L_0x00ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00df, code lost:
        switch(r2) {
            case 1: goto L_0x00e3;
            case 2: goto L_0x00e3;
            case 3: goto L_0x00e3;
            case 4: goto L_0x00e3;
            default: goto L_0x00e2;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
        if (r4.equals(r8) != false) goto L_0x00ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x00e2, code lost:
        r0 = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x00ee, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        return super.findInstanceIdInfo(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
        return instanceIdInfo(r0, super.getMaxInstanceId() + r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0019, code lost:
        if (r2 != 0) goto L_0x00df;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int findInstanceIdInfo(java.lang.String r8) {
        /*
            r7 = this;
            r1 = 5
            r3 = 1
            r0 = 4
            r6 = 36
            r2 = 0
            r4 = 0
            int r5 = r8.length()
            switch(r5) {
                case 2: goto L_0x0020;
                case 3: goto L_0x000e;
                case 4: goto L_0x000e;
                case 5: goto L_0x00b0;
                case 6: goto L_0x000e;
                case 7: goto L_0x000e;
                case 8: goto L_0x000e;
                case 9: goto L_0x00b5;
                case 10: goto L_0x000e;
                case 11: goto L_0x00d3;
                case 12: goto L_0x00d9;
                default: goto L_0x000e;
            }
        L_0x000e:
            r3 = r2
        L_0x000f:
            if (r4 == 0) goto L_0x00ee
            if (r4 == r8) goto L_0x00ee
            boolean r4 = r4.equals(r8)
            if (r4 != 0) goto L_0x00ee
        L_0x0019:
            if (r2 != 0) goto L_0x00df
            int r0 = super.findInstanceIdInfo(r8)
        L_0x001f:
            return r0
        L_0x0020:
            char r3 = r8.charAt(r3)
            switch(r3) {
                case 38: goto L_0x0029;
                case 39: goto L_0x0031;
                case 42: goto L_0x003a;
                case 43: goto L_0x0042;
                case 49: goto L_0x004b;
                case 50: goto L_0x0054;
                case 51: goto L_0x005d;
                case 52: goto L_0x0066;
                case 53: goto L_0x006f;
                case 54: goto L_0x0078;
                case 55: goto L_0x0081;
                case 56: goto L_0x008a;
                case 57: goto L_0x0093;
                case 95: goto L_0x009d;
                case 96: goto L_0x00a6;
                default: goto L_0x0027;
            }
        L_0x0027:
            r3 = r2
            goto L_0x000f
        L_0x0029:
            char r3 = r8.charAt(r2)
            if (r3 != r6) goto L_0x000e
            r2 = 6
            goto L_0x0019
        L_0x0031:
            char r3 = r8.charAt(r2)
            if (r3 != r6) goto L_0x000e
            r2 = 12
            goto L_0x0019
        L_0x003a:
            char r3 = r8.charAt(r2)
            if (r3 != r6) goto L_0x000e
            r2 = 2
            goto L_0x0019
        L_0x0042:
            char r3 = r8.charAt(r2)
            if (r3 != r6) goto L_0x000e
            r2 = 8
            goto L_0x0019
        L_0x004b:
            char r3 = r8.charAt(r2)
            if (r3 != r6) goto L_0x000e
            r2 = 13
            goto L_0x0019
        L_0x0054:
            char r3 = r8.charAt(r2)
            if (r3 != r6) goto L_0x000e
            r2 = 14
            goto L_0x0019
        L_0x005d:
            char r3 = r8.charAt(r2)
            if (r3 != r6) goto L_0x000e
            r2 = 15
            goto L_0x0019
        L_0x0066:
            char r3 = r8.charAt(r2)
            if (r3 != r6) goto L_0x000e
            r2 = 16
            goto L_0x0019
        L_0x006f:
            char r3 = r8.charAt(r2)
            if (r3 != r6) goto L_0x000e
            r2 = 17
            goto L_0x0019
        L_0x0078:
            char r3 = r8.charAt(r2)
            if (r3 != r6) goto L_0x000e
            r2 = 18
            goto L_0x0019
        L_0x0081:
            char r3 = r8.charAt(r2)
            if (r3 != r6) goto L_0x000e
            r2 = 19
            goto L_0x0019
        L_0x008a:
            char r3 = r8.charAt(r2)
            if (r3 != r6) goto L_0x000e
            r2 = 20
            goto L_0x0019
        L_0x0093:
            char r3 = r8.charAt(r2)
            if (r3 != r6) goto L_0x000e
            r2 = 21
            goto L_0x0019
        L_0x009d:
            char r3 = r8.charAt(r2)
            if (r3 != r6) goto L_0x000e
            r2 = r0
            goto L_0x0019
        L_0x00a6:
            char r3 = r8.charAt(r2)
            if (r3 != r6) goto L_0x000e
            r2 = 10
            goto L_0x0019
        L_0x00b0:
            java.lang.String r4 = "input"
            r3 = 3
            goto L_0x000f
        L_0x00b5:
            char r5 = r8.charAt(r0)
            r6 = 77
            if (r5 != r6) goto L_0x00c2
            java.lang.String r4 = "lastMatch"
            r3 = r1
            goto L_0x000f
        L_0x00c2:
            r6 = 80
            if (r5 != r6) goto L_0x00cb
            java.lang.String r4 = "lastParen"
            r3 = 7
            goto L_0x000f
        L_0x00cb:
            r6 = 105(0x69, float:1.47E-43)
            if (r5 != r6) goto L_0x000e
            java.lang.String r4 = "multiline"
            goto L_0x000f
        L_0x00d3:
            java.lang.String r4 = "leftContext"
            r3 = 9
            goto L_0x000f
        L_0x00d9:
            java.lang.String r4 = "rightContext"
            r3 = 11
            goto L_0x000f
        L_0x00df:
            switch(r2) {
                case 1: goto L_0x00e3;
                case 2: goto L_0x00e3;
                case 3: goto L_0x00e3;
                case 4: goto L_0x00e3;
                default: goto L_0x00e2;
            }
        L_0x00e2:
            r0 = r1
        L_0x00e3:
            int r1 = super.getMaxInstanceId()
            int r1 = r1 + r2
            int r0 = instanceIdInfo(r0, r1)
            goto L_0x001f
        L_0x00ee:
            r2 = r3
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: org.mozilla.javascript.regexp.C6896avk.findInstanceIdInfo(java.lang.String):int");
    }

    /* access modifiers changed from: protected */
    public String getInstanceIdName(int i) {
        int maxInstanceId = i - super.getMaxInstanceId();
        if (1 > maxInstanceId || maxInstanceId > 21) {
            return super.getInstanceIdName(i);
        }
        switch (maxInstanceId) {
            case 1:
                return "multiline";
            case 2:
                return "$*";
            case 3:
                return "input";
            case 4:
                return "$_";
            case 5:
                return "lastMatch";
            case 6:
                return "$&";
            case 7:
                return "lastParen";
            case 8:
                return "$+";
            case 9:
                return "leftContext";
            case 10:
                return "$`";
            case 11:
                return "rightContext";
            case 12:
                return "$'";
            default:
                return new String(new char[]{'$', (char) (((maxInstanceId - 12) - 1) + 49)});
        }
    }

    /* access modifiers changed from: protected */
    public Object getInstanceIdValue(int i) {
        Object obj;
        int maxInstanceId = i - super.getMaxInstanceId();
        if (1 > maxInstanceId || maxInstanceId > 21) {
            return super.getInstanceIdValue(i);
        }
        RegExpImpl czb = czb();
        switch (maxInstanceId) {
            case 1:
            case 2:
                return org.mozilla1.javascript.ScriptRuntime.wrapBoolean(czb.fOw);
            case 3:
            case 4:
                obj = czb.iAM;
                break;
            case 5:
            case 6:
                obj = czb.iAO;
                break;
            case 7:
            case 8:
                obj = czb.iAP;
                break;
            case 9:
            case 10:
                obj = czb.iAQ;
                break;
            case 11:
            case 12:
                obj = czb.iAR;
                break;
            default:
                obj = czb.mo10826zV((maxInstanceId - 12) - 1);
                break;
        }
        return obj == null ? "" : obj.toString();
    }

    /* access modifiers changed from: protected */
    public void setInstanceIdValue(int i, Object obj) {
        switch (i - super.getMaxInstanceId()) {
            case 1:
            case 2:
                czb().fOw = org.mozilla1.javascript.ScriptRuntime.toBoolean(obj);
                return;
            case 3:
            case 4:
                czb().iAM = ScriptRuntime.toString(obj);
                return;
            default:
                super.setInstanceIdValue(i, obj);
                return;
        }
    }
}
