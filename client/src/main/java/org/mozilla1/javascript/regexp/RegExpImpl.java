package org.mozilla1.javascript.regexp;

import org.mozilla1.javascript.ScriptRuntime;

/* renamed from: a.aPd  reason: case insensitive filesystem */
/* compiled from: a */
public class RegExpImpl implements org.mozilla1.javascript.RegExpProxy {
    boolean fOw;
    String iAM;
    SubString[] iAN;
    SubString iAO;
    SubString iAP;
    SubString iAQ;
    SubString iAR;

    /* renamed from: a */
    private static Object m17293a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr, RegExpImpl apd, org.mozilla1.javascript.regexp.GlobData sNVar, boolean z) {
        String str;
        org.mozilla1.javascript.regexp.NativeRegExp aex;
        int i;
        String nh = org.mozilla1.javascript.ScriptRuntime.toString((Object) avf2);
        sNVar.str = nh;
        org.mozilla1.javascript.Scriptable x = org.mozilla1.javascript.ScriptableObject.m23595x(avf);
        if (objArr.length == 0) {
            aex = new org.mozilla1.javascript.regexp.NativeRegExp(x, org.mozilla1.javascript.regexp.NativeRegExp.m21387a(lhVar, "", "", false));
        } else if (objArr[0] instanceof org.mozilla1.javascript.regexp.NativeRegExp) {
            aex = (org.mozilla1.javascript.regexp.NativeRegExp) objArr[0];
        } else {
            String nh2 = org.mozilla1.javascript.ScriptRuntime.toString(objArr[0]);
            if (sNVar.biy < objArr.length) {
                objArr[0] = nh2;
                str = org.mozilla1.javascript.ScriptRuntime.toString(objArr[sNVar.biy]);
            } else {
                str = null;
            }
            aex = new org.mozilla1.javascript.regexp.NativeRegExp(x, org.mozilla1.javascript.regexp.NativeRegExp.m21387a(lhVar, nh2, str, z));
        }
        sNVar.biA = aex;
        sNVar.biz = (aex.getFlags() & 1) != 0;
        int[] iArr = new int[1];
        Object obj = null;
        if (sNVar.mode == 3) {
            Object a = aex.mo13194a(lhVar, avf, apd, nh, iArr, 0);
            if (a == null || !a.equals(Boolean.TRUE)) {
                return new Integer(-1);
            }
            return new Integer(apd.iAQ.length);
        } else if (sNVar.biz) {
            aex.fpw = org.mozilla1.javascript.ScriptRuntime.NaN;
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (iArr[0] > nh.length()) {
                    return obj;
                }
                obj = aex.mo13194a(lhVar, avf, apd, nh, iArr, 0);
                if (obj == null || !obj.equals(Boolean.TRUE)) {
                    return obj;
                }
                if (sNVar.mode == 1) {
                    m17295a(sNVar, lhVar, avf, i3, apd);
                } else {
                    if (sNVar.mode != 2) {
                        org.mozilla1.javascript.Kit.codeBug();
                    }
                    SubString tqVar = apd.iAO;
                    int i4 = sNVar.biG;
                    int i5 = tqVar.index - i4;
                    sNVar.biG = tqVar.length + tqVar.index;
                    m17296a(sNVar, lhVar, avf, apd, i4, i5);
                }
                if (apd.iAO.length == 0) {
                    if (iArr[0] == nh.length()) {
                        return obj;
                    }
                    iArr[0] = iArr[0] + 1;
                }
                i2 = i3 + 1;
            }
        } else {
            if (sNVar.mode == 2) {
                i = 0;
            } else {
                i = 1;
            }
            return aex.mo13194a(lhVar, avf, apd, nh, iArr, i);
        }
    }

    /* renamed from: a */
    private static void m17295a(org.mozilla1.javascript.regexp.GlobData sNVar, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, int i, RegExpImpl apd) {
        if (sNVar.biB == null) {
            sNVar.biB = org.mozilla1.javascript.ScriptRuntime.m7539b(lhVar, org.mozilla1.javascript.ScriptableObject.m23595x(avf), "Array", (Object[]) null);
        }
        sNVar.biB.put(i, sNVar.biB, (Object) apd.iAO.toString());
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    private static void m17296a(org.mozilla1.javascript.regexp.GlobData sNVar, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, RegExpImpl apd, int i, int i2) {
        int length;
        String str;
        int i3;
        if (sNVar.biC != null) {
            SubString[] tqVarArr = apd.iAN;
            int length2 = tqVarArr == null ? 0 : tqVarArr.length;
            Object[] objArr = new Object[(length2 + 3)];
            objArr[0] = apd.iAO.toString();
            for (int i4 = 0; i4 < length2; i4++) {
                SubString tqVar = tqVarArr[i4];
                if (tqVar != null) {
                    objArr[i4 + 1] = tqVar.toString();
                } else {
                    objArr[i4 + 1] = org.mozilla1.javascript.Undefined.instance;
                }
            }
            objArr[length2 + 1] = new Integer(apd.iAQ.length);
            objArr[length2 + 2] = sNVar.str;
            if (apd != org.mozilla1.javascript.ScriptRuntime.m7592l(lhVar)) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            RegExpImpl apd2 = new RegExpImpl();
            apd2.fOw = apd.fOw;
            apd2.iAM = apd.iAM;
            org.mozilla1.javascript.ScriptRuntime.m7531a(lhVar, (org.mozilla1.javascript.RegExpProxy) apd2);
            try {
                org.mozilla1.javascript.Scriptable x = org.mozilla1.javascript.ScriptableObject.m23595x(avf);
                str = org.mozilla1.javascript.ScriptRuntime.toString(sNVar.biC.call(lhVar, x, x, objArr));
                org.mozilla1.javascript.ScriptRuntime.m7531a(lhVar, (org.mozilla1.javascript.RegExpProxy) apd);
                length = str.length();
            } catch (Throwable th) {
                org.mozilla1.javascript.ScriptRuntime.m7531a(lhVar, (org.mozilla1.javascript.RegExpProxy) apd);
                throw th;
            }
        } else {
            length = sNVar.biD.length();
            if (sNVar.biE >= 0) {
                int[] iArr = new int[1];
                int i5 = sNVar.biE;
                do {
                    SubString a = m17292a(lhVar, apd, sNVar.biD, i5, iArr);
                    if (a != null) {
                        length += a.length - iArr[0];
                        i3 = i5 + iArr[0];
                    } else {
                        i3 = i5 + 1;
                    }
                    i5 = sNVar.biD.indexOf(36, i3);
                } while (i5 >= 0);
            }
            str = null;
        }
        int i6 = apd.iAR.length + i2 + length;
        StringBuffer stringBuffer = sNVar.biF;
        if (stringBuffer == null) {
            stringBuffer = new StringBuffer(i6);
            sNVar.biF = stringBuffer;
        } else {
            stringBuffer.ensureCapacity(i6 + sNVar.biF.length());
        }
        stringBuffer.append(apd.iAQ.bmj, i, i2);
        if (sNVar.biC != null) {
            stringBuffer.append(str);
        } else {
            m17294a(sNVar, lhVar, apd);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:44:0x0095  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static SubString m17292a(org.mozilla1.javascript.Context r7, RegExpImpl r8, java.lang.String r9, int r10, int[] r11) {
        /*
            r4 = 140(0x8c, float:1.96E-43)
            r5 = 0
            r2 = 0
            char r0 = r9.charAt(r10)
            r1 = 36
            if (r0 == r1) goto L_0x000f
            p001a.C1520WN.codeBug()
        L_0x000f:
            int r0 = r7.getLanguageVersion()
            if (r0 == 0) goto L_0x0025
            if (r0 > r4) goto L_0x0025
            if (r10 <= 0) goto L_0x0025
            int r1 = r10 + -1
            char r1 = r9.charAt(r1)
            r3 = 92
            if (r1 != r3) goto L_0x0025
            r0 = r2
        L_0x0024:
            return r0
        L_0x0025:
            int r6 = r9.length()
            int r1 = r10 + 1
            if (r1 < r6) goto L_0x002f
            r0 = r2
            goto L_0x0024
        L_0x002f:
            int r1 = r10 + 1
            char r1 = r9.charAt(r1)
            boolean r3 = org.mozilla.javascript.regexp.C6025aex.isDigit(r1)
            if (r3 == 0) goto L_0x0097
            if (r0 == 0) goto L_0x0068
            if (r0 > r4) goto L_0x0068
            r0 = 48
            if (r1 != r0) goto L_0x0045
            r0 = r2
            goto L_0x0024
        L_0x0045:
            r0 = r10
            r1 = r5
        L_0x0047:
            int r0 = r0 + 1
            if (r0 >= r6) goto L_0x0055
            char r2 = r9.charAt(r0)
            boolean r3 = org.mozilla.javascript.regexp.C6025aex.isDigit(r2)
            if (r3 != 0) goto L_0x005f
        L_0x0055:
            int r1 = r1 + -1
            int r0 = r0 - r10
            r11[r5] = r0
            a.tq r0 = r8.mo10826zV(r1)
            goto L_0x0024
        L_0x005f:
            int r3 = r1 * 10
            int r2 = r2 + -48
            int r2 = r2 + r3
            if (r2 < r1) goto L_0x0055
            r1 = r2
            goto L_0x0047
        L_0x0068:
            a.tq[] r0 = r8.iAN
            if (r0 != 0) goto L_0x0073
            r4 = r5
        L_0x006d:
            int r3 = r1 + -48
            if (r3 <= r4) goto L_0x0078
            r0 = r2
            goto L_0x0024
        L_0x0073:
            a.tq[] r0 = r8.iAN
            int r0 = r0.length
            r4 = r0
            goto L_0x006d
        L_0x0078:
            int r0 = r10 + 2
            int r1 = r10 + 2
            if (r1 >= r6) goto L_0x00c8
            int r1 = r10 + 2
            char r1 = r9.charAt(r1)
            boolean r6 = org.mozilla.javascript.regexp.C6025aex.isDigit(r1)
            if (r6 == 0) goto L_0x00c8
            int r6 = r3 * 10
            int r1 = r1 + -48
            int r1 = r1 + r6
            if (r1 > r4) goto L_0x00c8
            int r0 = r0 + 1
        L_0x0093:
            if (r1 != 0) goto L_0x0055
            r0 = r2
            goto L_0x0024
        L_0x0097:
            r3 = 2
            r11[r5] = r3
            switch(r1) {
                case 36: goto L_0x009f;
                case 38: goto L_0x00a8;
                case 39: goto L_0x00c4;
                case 43: goto L_0x00ac;
                case 96: goto L_0x00b0;
                default: goto L_0x009d;
            }
        L_0x009d:
            r0 = r2
            goto L_0x0024
        L_0x009f:
            a.tq r0 = new a.tq
            java.lang.String r1 = "$"
            r0.<init>(r1)
            goto L_0x0024
        L_0x00a8:
            a.tq r0 = r8.iAO
            goto L_0x0024
        L_0x00ac:
            a.tq r0 = r8.iAP
            goto L_0x0024
        L_0x00b0:
            r1 = 120(0x78, float:1.68E-43)
            if (r0 != r1) goto L_0x00c0
            a.tq r0 = r8.iAQ
            r0.index = r5
            a.tq r0 = r8.iAQ
            a.tq r1 = r8.iAO
            int r1 = r1.index
            r0.length = r1
        L_0x00c0:
            a.tq r0 = r8.iAQ
            goto L_0x0024
        L_0x00c4:
            a.tq r0 = r8.iAR
            goto L_0x0024
        L_0x00c8:
            r1 = r3
            goto L_0x0093
        */
        throw new UnsupportedOperationException("Method not decompiled: org.mozilla.javascript.regexp.C5589aPd.m17292a(a.lh, a.aPd, java.lang.String, int, int[]):a.tq");
    }

    /* renamed from: a */
    private static void m17294a(org.mozilla1.javascript.regexp.GlobData sNVar, org.mozilla1.javascript.Context lhVar, RegExpImpl apd) {
        int i;
        int i2;
        StringBuffer stringBuffer = sNVar.biF;
        String str = sNVar.biD;
        int i3 = sNVar.biE;
        if (i3 != -1) {
            int[] iArr = new int[1];
            i = 0;
            do {
                int i4 = i3 - i;
                stringBuffer.append(str.substring(i, i3));
                SubString a = m17292a(lhVar, apd, str, i3, iArr);
                if (a != null) {
                    int i5 = a.length;
                    if (i5 > 0) {
                        stringBuffer.append(a.bmj, a.index, i5);
                    }
                    i = iArr[0] + i3;
                    i2 = i3 + iArr[0];
                } else {
                    i2 = i3 + 1;
                    i = i3;
                }
                i3 = str.indexOf(36, i2);
            } while (i3 >= 0);
        } else {
            i = 0;
        }
        int length = str.length();
        if (length > i) {
            stringBuffer.append(str.substring(i, length));
        }
    }

    /* renamed from: m */
    public boolean mo10825m(org.mozilla1.javascript.Scriptable avf) {
        return avf instanceof org.mozilla1.javascript.regexp.NativeRegExp;
    }

    /* renamed from: b */
    public Object mo10823b(org.mozilla1.javascript.Context lhVar, String str, String str2) {
        return org.mozilla1.javascript.regexp.NativeRegExp.m21387a(lhVar, str, str2, false);
    }

    /* renamed from: c */
    public org.mozilla1.javascript.Scriptable mo10824c(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object obj) {
        return new org.mozilla1.javascript.regexp.NativeRegExp(avf, obj);
    }

    /* renamed from: a */
    public Object mo10822a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr, int i) {
        org.mozilla1.javascript.Function azg;
        int indexOf;
        org.mozilla1.javascript.regexp.GlobData sNVar = new org.mozilla1.javascript.regexp.GlobData();
        sNVar.mode = i;
        switch (i) {
            case 1:
                sNVar.biy = 1;
                Object a = m17293a(lhVar, avf, avf2, objArr, this, sNVar, false);
                if (sNVar.biB == null) {
                    return a;
                }
                return sNVar.biB;
            case 2:
                Object obj = objArr.length < 2 ? org.mozilla1.javascript.Undefined.instance : objArr[1];
                String str = null;
                if (obj instanceof org.mozilla1.javascript.Function) {
                    azg = (org.mozilla1.javascript.Function) obj;
                } else {
                    str = ScriptRuntime.toString(obj);
                    azg = null;
                }
                sNVar.biy = 2;
                sNVar.biC = azg;
                sNVar.biD = str;
                if (str == null) {
                    indexOf = -1;
                } else {
                    indexOf = str.indexOf(36);
                }
                sNVar.biE = indexOf;
                sNVar.biF = null;
                sNVar.biG = 0;
                Object a2 = m17293a(lhVar, avf, avf2, objArr, this, sNVar, true);
                SubString tqVar = this.iAR;
                if (sNVar.biF == null) {
                    if (sNVar.biz || a2 == null || !a2.equals(Boolean.TRUE)) {
                        return sNVar.str;
                    }
                    SubString tqVar2 = this.iAQ;
                    m17296a(sNVar, lhVar, avf, this, tqVar2.index, tqVar2.length);
                }
                sNVar.biF.append(tqVar.bmj, tqVar.index, tqVar.length);
                return sNVar.biF.toString();
            case 3:
                sNVar.biy = 1;
                return m17293a(lhVar, avf, avf2, objArr, this, sNVar, false);
            default:
                throw org.mozilla1.javascript.Kit.codeBug();
        }
    }

    /* renamed from: a */
    public int mo10821a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, String str, String str2, org.mozilla1.javascript.Scriptable avf2, int[] iArr, int[] iArr2, boolean[] zArr, String[][] strArr) {
        int i;
        int i2 = iArr[0];
        int length = str.length();
        int languageVersion = lhVar.getLanguageVersion();
        org.mozilla1.javascript.regexp.NativeRegExp aex = (NativeRegExp) avf2;
        while (true) {
            int i3 = iArr[0];
            iArr[0] = i2;
            if (aex.mo13194a(lhVar, avf, this, str, iArr, 0) != Boolean.TRUE) {
                iArr[0] = i3;
                iArr2[0] = 1;
                zArr[0] = false;
                return length;
            }
            int i4 = iArr[0];
            iArr[0] = i3;
            zArr[0] = true;
            iArr2[0] = this.iAO.length;
            if (iArr2[0] != 0 || i4 != iArr[0]) {
                i = i4 - iArr2[0];
            } else if (i4 != length) {
                i2 = i4 + 1;
            } else if (languageVersion == 120) {
                iArr2[0] = 1;
                i = i4;
            } else {
                i = -1;
            }
        }
        int length2 = this.iAN == null ? 0 : this.iAN.length;
        strArr[0] = new String[length2];
        for (int i5 = 0; i5 < length2; i5++) {
            strArr[0][i5] = mo10826zV(i5).toString();
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0009, code lost:
        r0 = r1.iAN[r2];
     */
    /* renamed from: zV */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SubString mo10826zV(int r2) {
        /*
            r1 = this;
            a.tq[] r0 = r1.iAN
            if (r0 == 0) goto L_0x0010
            a.tq[] r0 = r1.iAN
            int r0 = r0.length
            if (r2 >= r0) goto L_0x0010
            a.tq[] r0 = r1.iAN
            r0 = r0[r2]
            if (r0 == 0) goto L_0x0010
        L_0x000f:
            return r0
        L_0x0010:
            a.tq r0 = org.mozilla.javascript.regexp.C3692tq.bmi
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.mozilla.javascript.regexp.C5589aPd.mo10826zV(int):a.tq");
    }
}
