package org.mozilla1.javascript.regexp;


import org.mozilla1.classfile.C0147Bi;
import org.mozilla1.javascript.ScriptRuntime;


/* renamed from: a.aex  reason: case insensitive filesystem */
/* compiled from: a */
public class NativeRegExp extends org.mozilla1.javascript.IdScriptableObject implements org.mozilla1.javascript.Function {
    public static final int JSREG_FOLD = 2;
    public static final int JSREG_GLOB = 1;
    public static final int JSREG_MULTILINE = 4;
    public static final int MATCH = 1;
    public static final int PREFIX = 2;
    public static final int TEST = 0;
    static final long serialVersionUID = 4965263491464903264L;
    private static final int ceN = 3;
    private static final boolean debug = false;
    private static final int eVp = 5;
    private static final byte foA = 3;
    private static final byte foB = 4;
    private static final byte foC = 5;
    private static final byte foD = 6;
    private static final byte foE = 7;
    private static final byte foF = 8;
    private static final byte foG = 9;
    private static final byte foH = 10;
    private static final byte foI = 11;
    private static final byte foJ = 12;
    private static final byte foK = 14;
    private static final byte foL = 15;
    private static final byte foM = 16;
    private static final byte foN = 17;
    private static final byte foO = 18;
    private static final byte foP = 19;
    private static final byte foQ = 20;
    private static final byte foR = 21;
    private static final byte foS = 22;
    private static final byte foT = 23;
    private static final byte foU = 28;
    private static final byte foV = 32;
    private static final byte foW = 33;
    private static final byte foX = 35;
    private static final byte foY = 41;
    private static final byte foZ = 42;
    private static final Object fow = new Integer(5);
    private static final byte fox = 0;
    private static final byte foy = 1;
    private static final byte foz = 2;
    private static final byte fpa = 43;
    private static final byte fpb = 44;
    private static final byte fpc = 45;
    private static final byte fpd = 46;
    private static final byte fpe = 47;
    private static final byte fpf = 48;
    private static final byte fpg = 49;
    private static final byte fph = 50;
    private static final byte fpi = 51;
    private static final byte fpj = 52;
    private static final byte fpk = 53;
    private static final int fpl = 2;
    private static final int fpm = 2;
    private static final int fpn = 1;
    private static final int fpo = 2;
    private static final int fpp = 3;
    private static final int fpq = 4;
    private static final int fpr = 5;
    private static final int fpt = 5;
    private static final int fpu = 6;
    /* renamed from: nt */
    private static final int f4438nt = 6;
    /* renamed from: xT */
    private static final int f4439xT = 2;

    /* renamed from: xU */
    private static final int f4440xU = 1;

    /* renamed from: xV */
    private static final int f4441xV = 4;
    double fpw;
    private org.mozilla1.javascript.regexp.RECompiled fpv;

    NativeRegExp(org.mozilla1.javascript.Scriptable avf, Object obj) {
        this.fpv = (org.mozilla1.javascript.regexp.RECompiled) obj;
        this.fpw = org.mozilla1.javascript.ScriptRuntime.NaN;
        org.mozilla1.javascript.ScriptRuntime.m7528a((org.mozilla1.javascript.ScriptableObject) this, avf);
    }

    NativeRegExp() {
    }

    public static void init(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, boolean z) {
        NativeRegExp aex = new NativeRegExp();
        aex.fpv = (org.mozilla1.javascript.regexp.RECompiled) m21387a(lhVar, "", (String) null, false);
        aex.activatePrototypeMap(6);
        aex.setParentScope(avf);
        aex.setPrototype(org.mozilla1.javascript.ScriptableObject.m23593v(avf));
        NativeRegExpCtor avk = new NativeRegExpCtor();
        aex.put("constructor", aex, avk);
        org.mozilla1.javascript.ScriptRuntime.m7527a((org.mozilla1.javascript.BaseFunction) avk, avf);
        avk.setImmunePrototypeProperty(aex);
        if (z) {
            aex.sealObject();
            avk.sealObject();
        }
        org.mozilla1.javascript.ScriptableObject.m23567a(avf, "RegExp", (Object) avk, 2);
    }

    /* renamed from: d */
    private static RegExpImpl m21409d(org.mozilla1.javascript.Context lhVar) {
        return (RegExpImpl) org.mozilla1.javascript.ScriptRuntime.m7592l(lhVar);
    }

    /* renamed from: a */
    static Object m21387a(org.mozilla1.javascript.Context lhVar, String str, String str2, boolean z) {
        int i;
        org.mozilla1.javascript.regexp.RECompiled sa = new org.mozilla1.javascript.regexp.RECompiled();
        sa.ect = str.toCharArray();
        int length = str.length();
        if (str2 != null) {
            i = 0;
            for (int i2 = 0; i2 < str2.length(); i2++) {
                char charAt = str2.charAt(i2);
                if (charAt == 'g') {
                    i |= 1;
                } else if (charAt == 'i') {
                    i |= 2;
                } else if (charAt == 'm') {
                    i |= 4;
                } else {
                    m21402ao("msg.invalid.re.flag", String.valueOf(charAt));
                }
            }
        } else {
            i = 0;
        }
        sa.flags = i;
        CompilerState pk = new CompilerState(lhVar, sa.ect, length, i);
        if (z && length > 0) {
            pk.dPQ = new RENode((byte) 21);
            pk.dPQ.fYb = pk.dPK[0];
            pk.dPQ.length = length;
            pk.dPQ.fYc = 0;
            pk.dPP += 5;
        } else if (!m21395a(pk)) {
            return null;
        }
        sa.ecu = new byte[(pk.dPP + 1)];
        if (pk.dPO != 0) {
            sa.ecv = new RECharSet[pk.dPO];
            sa.dPO = pk.dPO;
        }
        int a = m21384a(pk, sa, 0, pk.dPQ);
        int i3 = a + 1;
        sa.ecu[a] = fpk;
        sa.dPM = pk.dPM;
        switch (sa.ecu[0]) {
            case 21:
            case 32:
                sa.ecw = sa.ect[m21408d(sa.ecu, 1)];
                break;
            case 22:
            case 33:
                sa.ecw = (char) (sa.ecu[1] & 255);
                break;
            case 28:
            case 35:
                sa.ecw = (char) m21408d(sa.ecu, 1);
                break;
        }
        return sa;
    }

    static boolean isDigit(char c) {
        return '0' <= c && c <= '9';
    }

    /* renamed from: d */
    private static boolean m21411d(char c) {
        return Character.isLetter(c) || isDigit(c) || c == '_';
    }

    /* renamed from: e */
    private static boolean m21413e(char c) {
        return org.mozilla1.javascript.ScriptRuntime.isJSLineTerminator(c);
    }

    /* renamed from: ql */
    private static boolean m21417ql(int i) {
        return i == 32 || i == 9 || i == 10 || i == 13 || i == 8232 || i == 8233 || i == 12 || i == 11 || i == 160 || Character.getType((char) i) == 12;
    }

    /* renamed from: f */
    private static char m21414f(char c) {
        if (c >= 128) {
            char upperCase = Character.toUpperCase(c);
            if (c < 128 || upperCase >= 128) {
                return upperCase;
            }
            return c;
        } else if ('a' > c || c > 'z') {
            return c;
        } else {
            return (char) (c - ' ');
        }
    }

    /* renamed from: g */
    private static char m21416g(char c) {
        if (c >= 128) {
            char lowerCase = Character.toLowerCase(c);
            if (c < 128 || lowerCase >= 128) {
                return lowerCase;
            }
            return c;
        } else if ('A' > c || c > 'Z') {
            return c;
        } else {
            return (char) (c + ' ');
        }
    }

    /* renamed from: qm */
    private static int m21418qm(int i) {
        if (i < 48) {
            return -1;
        }
        if (i <= 57) {
            return i - 48;
        }
        int i2 = i | 32;
        if (97 > i2 || i2 > 102) {
            return -1;
        }
        return (i2 - 97) + 10;
    }

    /* renamed from: a */
    private static boolean m21395a(CompilerState pk) {
        if (!m21404b(pk)) {
            return false;
        }
        char[] cArr = pk.dPK;
        int i = pk.f1382cp;
        if (i != cArr.length && cArr[i] == '|') {
            pk.f1382cp++;
            RENode alp = new RENode((byte) 1);
            alp.fXU = pk.dPQ;
            if (!m21395a(pk)) {
                return false;
            }
            alp.fXV = pk.dPQ;
            pk.dPQ = alp;
            pk.dPP += 9;
        }
        return true;
    }

    /* renamed from: b */
    private static boolean m21404b(CompilerState pk) {
        char[] cArr = pk.dPK;
        RENode alp = null;
        RENode alp2 = null;
        while (pk.f1382cp != pk.dPL && cArr[pk.f1382cp] != '|' && (pk.dPN == 0 || cArr[pk.f1382cp] != ')')) {
            if (!m21407c(pk)) {
                return false;
            }
            if (alp2 == null) {
                alp2 = pk.dPQ;
            } else if (alp == null) {
                alp2.fXT = pk.dPQ;
                alp = pk.dPQ;
                while (alp.fXT != null) {
                    alp = alp.fXT;
                }
            } else {
                alp.fXT = pk.dPQ;
                RENode alp3 = alp.fXT;
                while (alp.fXT != null) {
                    alp3 = alp.fXT;
                }
            }
        }
        if (alp2 == null) {
            pk.dPQ = new RENode((byte) 0);
        } else {
            pk.dPQ = alp2;
        }
        return true;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v3, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v9, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v8, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v9, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v19, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v12, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v20, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v22, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v14, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v15, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v16, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v17, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v18, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v19, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v20, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v21, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v22, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v17, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v18, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v3, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v20, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v21, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v23, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v4, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v5, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v23, resolved type: char} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean m21396a(CompilerState r8, RENode r9, char[] r10, int r11, int r12) {
        /*
            r2 = 0
            r1 = 0
            r0 = 0
            r3 = 0
            r9.fYa = r3
            if (r11 != r12) goto L_0x000a
            r0 = 1
        L_0x0009:
            return r0
        L_0x000a:
            char r3 = r10[r11]
            r4 = 94
            if (r3 != r4) goto L_0x0127
            int r5 = r11 + 1
            r3 = r0
            r6 = r1
            r7 = r2
        L_0x0015:
            if (r5 != r12) goto L_0x001b
            r9.fYa = r6
            r0 = 1
            goto L_0x0009
        L_0x001b:
            r0 = 2
            char r1 = r10[r5]
            switch(r1) {
                case 92: goto L_0x0034;
                default: goto L_0x0021;
            }
        L_0x0021:
            int r2 = r5 + 1
            char r0 = r10[r5]
            r4 = r0
            r5 = r2
        L_0x0027:
            if (r3 == 0) goto L_0x010a
            if (r7 <= r4) goto L_0x00f1
            java.lang.String r0 = "msg.bad.range"
            java.lang.String r1 = ""
            m21402ao(r0, r1)
            r0 = 0
            goto L_0x0009
        L_0x0034:
            int r1 = r5 + 1
            int r2 = r1 + 1
            char r4 = r10[r1]
            switch(r4) {
                case 48: goto L_0x00c1;
                case 49: goto L_0x00c1;
                case 50: goto L_0x00c1;
                case 51: goto L_0x00c1;
                case 52: goto L_0x00c1;
                case 53: goto L_0x00c1;
                case 54: goto L_0x00c1;
                case 55: goto L_0x00c1;
                case 68: goto L_0x00ad;
                case 83: goto L_0x00ad;
                case 87: goto L_0x00ad;
                case 98: goto L_0x003f;
                case 99: goto L_0x005d;
                case 100: goto L_0x009b;
                case 102: goto L_0x0044;
                case 110: goto L_0x0049;
                case 114: goto L_0x004e;
                case 115: goto L_0x00ad;
                case 116: goto L_0x0053;
                case 117: goto L_0x007a;
                case 118: goto L_0x0058;
                case 119: goto L_0x00ad;
                case 120: goto L_0x007b;
                default: goto L_0x003d;
            }
        L_0x003d:
            r5 = r2
            goto L_0x0027
        L_0x003f:
            r0 = 8
            r4 = r0
            r5 = r2
            goto L_0x0027
        L_0x0044:
            r0 = 12
            r4 = r0
            r5 = r2
            goto L_0x0027
        L_0x0049:
            r0 = 10
            r4 = r0
            r5 = r2
            goto L_0x0027
        L_0x004e:
            r0 = 13
            r4 = r0
            r5 = r2
            goto L_0x0027
        L_0x0053:
            r0 = 9
            r4 = r0
            r5 = r2
            goto L_0x0027
        L_0x0058:
            r0 = 11
            r4 = r0
            r5 = r2
            goto L_0x0027
        L_0x005d:
            int r0 = r2 + 1
            if (r0 >= r12) goto L_0x0075
            int r0 = r2 + 1
            char r0 = r10[r0]
            boolean r0 = java.lang.Character.isLetter(r0)
            if (r0 == 0) goto L_0x0075
            int r1 = r2 + 1
            char r0 = r10[r2]
            r0 = r0 & 31
            char r0 = (char) r0
            r4 = r0
            r5 = r1
            goto L_0x0027
        L_0x0075:
            r0 = 92
            r4 = r0
            r5 = r2
            goto L_0x0027
        L_0x007a:
            r0 = 4
        L_0x007b:
            r1 = 0
            r4 = 0
        L_0x007d:
            if (r4 >= r0) goto L_0x0081
            if (r2 < r12) goto L_0x0085
        L_0x0081:
            r0 = r1
        L_0x0082:
            r4 = r0
            r5 = r2
            goto L_0x0027
        L_0x0085:
            int r5 = r2 + 1
            char r2 = r10[r2]
            int r1 = p001a.C1520WN.xDigitToInt(r2, r1)
            if (r1 >= 0) goto L_0x0097
            int r0 = r4 + 1
            int r1 = r5 - r0
            r0 = 92
            r2 = r1
            goto L_0x0082
        L_0x0097:
            int r4 = r4 + 1
            r2 = r5
            goto L_0x007d
        L_0x009b:
            if (r3 == 0) goto L_0x00a7
            java.lang.String r0 = "msg.bad.range"
            java.lang.String r1 = ""
            m21402ao(r0, r1)
            r0 = 0
            goto L_0x0009
        L_0x00a7:
            r0 = 57
            r4 = r0
            r5 = r2
            goto L_0x0027
        L_0x00ad:
            if (r3 == 0) goto L_0x00b9
            java.lang.String r0 = "msg.bad.range"
            java.lang.String r1 = ""
            m21402ao(r0, r1)
            r0 = 0
            goto L_0x0009
        L_0x00b9:
            r0 = 65535(0xffff, float:9.1834E-41)
            r9.fYa = r0
            r0 = 1
            goto L_0x0009
        L_0x00c1:
            int r0 = r4 + -48
            char r1 = r10[r2]
            r4 = 48
            if (r4 > r1) goto L_0x00e9
            r4 = 55
            if (r1 > r4) goto L_0x00e9
            int r2 = r2 + 1
            int r0 = r0 * 8
            int r1 = r1 + -48
            int r1 = r1 + r0
            char r0 = r10[r2]
            r4 = 48
            if (r4 > r0) goto L_0x0125
            r4 = 55
            if (r0 > r4) goto L_0x0125
            int r2 = r2 + 1
            int r4 = r1 * 8
            int r0 = r0 + -48
            int r0 = r0 + r4
            r4 = 255(0xff, float:3.57E-43)
            if (r0 > r4) goto L_0x00ed
        L_0x00e9:
            r4 = r0
            r5 = r2
            goto L_0x0027
        L_0x00ed:
            int r2 = r2 + -1
            r0 = r1
            goto L_0x00e9
        L_0x00f1:
            r0 = 0
        L_0x00f2:
            int r1 = r8.flags
            r1 = r1 & 2
            if (r1 == 0) goto L_0x0121
            char r1 = (char) r4
            char r1 = m21414f(r1)
            char r2 = (char) r4
            char r2 = m21416g(r2)
            if (r1 < r2) goto L_0x011c
        L_0x0104:
            if (r1 <= r6) goto L_0x011e
            r3 = r0
            r6 = r1
            goto L_0x0015
        L_0x010a:
            int r0 = r12 + -1
            if (r5 >= r0) goto L_0x0123
            char r0 = r10[r5]
            r1 = 45
            if (r0 != r1) goto L_0x0123
            int r5 = r5 + 1
            r0 = 1
            char r1 = (char) r4
            r3 = r0
            r7 = r1
            goto L_0x0015
        L_0x011c:
            r1 = r2
            goto L_0x0104
        L_0x011e:
            r3 = r0
            goto L_0x0015
        L_0x0121:
            r1 = r4
            goto L_0x0104
        L_0x0123:
            r0 = r3
            goto L_0x00f2
        L_0x0125:
            r0 = r1
            goto L_0x00e9
        L_0x0127:
            r3 = r0
            r6 = r1
            r7 = r2
            r5 = r11
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: org.mozilla.javascript.regexp.C6025aex.m21396a(a.Pk, a.alP, char[], int, int):boolean");
    }

    /* renamed from: a */
    private static void m21388a(CompilerState pk, char c) {
        pk.dPQ = new RENode((byte) 21);
        pk.dPQ.fYb = c;
        pk.dPQ.length = 1;
        pk.dPQ.fYc = -1;
        pk.dPP += 3;
    }

    /* renamed from: a */
    private static int m21383a(char c, CompilerState pk, int i, String str) {
        boolean z = false;
        int i2 = pk.f1382cp;
        char[] cArr = pk.dPK;
        int i3 = c - '0';
        while (pk.f1382cp != pk.dPL) {
            char c2 = cArr[pk.f1382cp];
            if (!isDigit(c2)) {
                break;
            }
            if (!z) {
                int i4 = c2 - '0';
                if (i3 < (i - i4) / 10) {
                    i3 = (i3 * 10) + i4;
                } else {
                    z = true;
                    i3 = i;
                }
            }
            pk.f1382cp++;
        }
        if (z) {
            m21402ao(str, String.valueOf(cArr, i2, pk.f1382cp - i2));
        }
        return i3;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v63, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v64, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v101, resolved type: char} */
    /* JADX WARNING: Incorrect type for immutable var: ssa=char, code=int, for r0v45, types: [char] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x0424  */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean m21407c(CompilerState r12) {
        /*
            r11 = 65535(0xffff, float:9.1834E-41)
            r4 = -1
            r10 = 6
            r3 = 1
            r1 = 0
            char[] r6 = r12.dPK
            int r0 = r12.f1382cp
            int r2 = r0 + 1
            r12.f1382cp = r2
            char r2 = r6[r0]
            r0 = 2
            int r7 = r12.dPM
            switch(r2) {
                case 36: goto L_0x0050;
                case 40: goto L_0x0238;
                case 41: goto L_0x02d5;
                case 42: goto L_0x0351;
                case 43: goto L_0x0351;
                case 46: goto L_0x0340;
                case 63: goto L_0x0351;
                case 91: goto L_0x02de;
                case 92: goto L_0x0060;
                case 94: goto L_0x0040;
                default: goto L_0x0017;
            }
        L_0x0017:
            a.alP r0 = new a.alP
            r5 = 21
            r0.<init>(r5)
            r12.dPQ = r0
            a.alP r0 = r12.dPQ
            r0.fYb = r2
            a.alP r0 = r12.dPQ
            r0.length = r3
            a.alP r0 = r12.dPQ
            int r2 = r12.f1382cp
            int r2 = r2 + -1
            r0.fYc = r2
            int r0 = r12.dPP
            int r0 = r0 + 3
            r12.dPP = r0
        L_0x0036:
            a.alP r8 = r12.dPQ
            int r0 = r12.f1382cp
            int r2 = r12.dPL
            if (r0 != r2) goto L_0x0362
            r1 = r3
        L_0x003f:
            return r1
        L_0x0040:
            a.alP r0 = new a.alP
            r1 = 2
            r0.<init>(r1)
            r12.dPQ = r0
            int r0 = r12.dPP
            int r0 = r0 + 1
            r12.dPP = r0
            r1 = r3
            goto L_0x003f
        L_0x0050:
            a.alP r0 = new a.alP
            r1 = 3
            r0.<init>(r1)
            r12.dPQ = r0
            int r0 = r12.dPP
            int r0 = r0 + 1
            r12.dPP = r0
            r1 = r3
            goto L_0x003f
        L_0x0060:
            int r2 = r12.f1382cp
            int r5 = r12.dPL
            if (r2 >= r5) goto L_0x022f
            int r2 = r12.f1382cp
            int r5 = r2 + 1
            r12.f1382cp = r5
            char r2 = r6[r2]
            switch(r2) {
                case 48: goto L_0x00b1;
                case 49: goto L_0x00e4;
                case 50: goto L_0x00e4;
                case 51: goto L_0x00e4;
                case 52: goto L_0x00e4;
                case 53: goto L_0x00e4;
                case 54: goto L_0x00e4;
                case 55: goto L_0x00e4;
                case 56: goto L_0x00e4;
                case 57: goto L_0x00e4;
                case 66: goto L_0x00a1;
                case 68: goto L_0x01da;
                case 83: goto L_0x01fc;
                case 87: goto L_0x021e;
                case 98: goto L_0x0091;
                case 99: goto L_0x0169;
                case 100: goto L_0x01c9;
                case 102: goto L_0x0146;
                case 110: goto L_0x014d;
                case 114: goto L_0x0154;
                case 115: goto L_0x01eb;
                case 116: goto L_0x015b;
                case 117: goto L_0x0196;
                case 118: goto L_0x0162;
                case 119: goto L_0x020d;
                case 120: goto L_0x0197;
                default: goto L_0x0071;
            }
        L_0x0071:
            a.alP r0 = new a.alP
            r5 = 21
            r0.<init>(r5)
            r12.dPQ = r0
            a.alP r0 = r12.dPQ
            r0.fYb = r2
            a.alP r0 = r12.dPQ
            r0.length = r3
            a.alP r0 = r12.dPQ
            int r2 = r12.f1382cp
            int r2 = r2 + -1
            r0.fYc = r2
            int r0 = r12.dPP
            int r0 = r0 + 3
            r12.dPP = r0
            goto L_0x0036
        L_0x0091:
            a.alP r0 = new a.alP
            r1 = 4
            r0.<init>(r1)
            r12.dPQ = r0
            int r0 = r12.dPP
            int r0 = r0 + 1
            r12.dPP = r0
            r1 = r3
            goto L_0x003f
        L_0x00a1:
            a.alP r0 = new a.alP
            r1 = 5
            r0.<init>(r1)
            r12.dPQ = r0
            int r0 = r12.dPP
            int r0 = r0 + 1
            r12.dPP = r0
            r1 = r3
            goto L_0x003f
        L_0x00b1:
            a.lh r0 = r12.dPJ
            java.lang.String r2 = "msg.bad.backref"
            java.lang.String r5 = ""
            m21394a((p001a.C2909lh) r0, (java.lang.String) r2, (java.lang.String) r5)
            r0 = r1
        L_0x00bb:
            int r2 = r12.f1382cp
            int r5 = r12.dPL
            if (r2 < r5) goto L_0x00c7
        L_0x00c1:
            char r0 = (char) r0
            m21388a((org.mozilla.javascript.regexp.C1076Pk) r12, (char) r0)
            goto L_0x0036
        L_0x00c7:
            int r2 = r12.f1382cp
            char r2 = r6[r2]
            r5 = 48
            if (r2 < r5) goto L_0x00c1
            r5 = 55
            if (r2 > r5) goto L_0x00c1
            int r5 = r12.f1382cp
            int r5 = r5 + 1
            r12.f1382cp = r5
            int r5 = r0 * 8
            int r2 = r2 + -48
            int r2 = r2 + r5
            r5 = 255(0xff, float:3.57E-43)
            if (r2 > r5) goto L_0x00c1
            r0 = r2
            goto L_0x00bb
        L_0x00e4:
            int r0 = r12.f1382cp
            int r0 = r0 + -1
            java.lang.String r5 = "msg.overlarge.backref"
            int r2 = m21383a((char) r2, (org.mozilla.javascript.regexp.C1076Pk) r12, (int) r11, (java.lang.String) r5)
            int r5 = r12.dPM
            if (r2 <= r5) goto L_0x00fb
            a.lh r5 = r12.dPJ
            java.lang.String r8 = "msg.bad.backref"
            java.lang.String r9 = ""
            m21394a((p001a.C2909lh) r5, (java.lang.String) r8, (java.lang.String) r9)
        L_0x00fb:
            r5 = 9
            if (r2 <= r5) goto L_0x012f
            int r5 = r12.dPM
            if (r2 <= r5) goto L_0x012f
            r12.f1382cp = r0
            r0 = r1
        L_0x0106:
            int r2 = r12.f1382cp
            int r5 = r12.dPL
            if (r2 < r5) goto L_0x0112
        L_0x010c:
            char r0 = (char) r0
            m21388a((org.mozilla.javascript.regexp.C1076Pk) r12, (char) r0)
            goto L_0x0036
        L_0x0112:
            int r2 = r12.f1382cp
            char r2 = r6[r2]
            r5 = 48
            if (r2 < r5) goto L_0x010c
            r5 = 55
            if (r2 > r5) goto L_0x010c
            int r5 = r12.f1382cp
            int r5 = r5 + 1
            r12.f1382cp = r5
            int r5 = r0 * 8
            int r2 = r2 + -48
            int r2 = r2 + r5
            r5 = 255(0xff, float:3.57E-43)
            if (r2 > r5) goto L_0x010c
            r0 = r2
            goto L_0x0106
        L_0x012f:
            a.alP r0 = new a.alP
            r5 = 20
            r0.<init>(r5)
            r12.dPQ = r0
            a.alP r0 = r12.dPQ
            int r2 = r2 + -1
            r0.fXX = r2
            int r0 = r12.dPP
            int r0 = r0 + 3
            r12.dPP = r0
            goto L_0x0036
        L_0x0146:
            r0 = 12
            m21388a((org.mozilla.javascript.regexp.C1076Pk) r12, (char) r0)
            goto L_0x0036
        L_0x014d:
            r0 = 10
            m21388a((org.mozilla.javascript.regexp.C1076Pk) r12, (char) r0)
            goto L_0x0036
        L_0x0154:
            r0 = 13
            m21388a((org.mozilla.javascript.regexp.C1076Pk) r12, (char) r0)
            goto L_0x0036
        L_0x015b:
            r0 = 9
            m21388a((org.mozilla.javascript.regexp.C1076Pk) r12, (char) r0)
            goto L_0x0036
        L_0x0162:
            r0 = 11
            m21388a((org.mozilla.javascript.regexp.C1076Pk) r12, (char) r0)
            goto L_0x0036
        L_0x0169:
            int r0 = r12.f1382cp
            int r0 = r0 + 1
            int r2 = r12.dPL
            if (r0 >= r2) goto L_0x018d
            int r0 = r12.f1382cp
            int r0 = r0 + 1
            char r0 = r6[r0]
            boolean r0 = java.lang.Character.isLetter(r0)
            if (r0 == 0) goto L_0x018d
            int r0 = r12.f1382cp
            int r2 = r0 + 1
            r12.f1382cp = r2
            char r0 = r6[r0]
            r0 = r0 & 31
            char r0 = (char) r0
        L_0x0188:
            m21388a((org.mozilla.javascript.regexp.C1076Pk) r12, (char) r0)
            goto L_0x0036
        L_0x018d:
            int r0 = r12.f1382cp
            int r0 = r0 + -1
            r12.f1382cp = r0
            r0 = 92
            goto L_0x0188
        L_0x0196:
            r0 = 4
        L_0x0197:
            r5 = r1
            r2 = r1
        L_0x0199:
            if (r5 >= r0) goto L_0x01a1
            int r8 = r12.f1382cp
            int r9 = r12.dPL
            if (r8 < r9) goto L_0x01a8
        L_0x01a1:
            r0 = r2
        L_0x01a2:
            char r0 = (char) r0
            m21388a((org.mozilla.javascript.regexp.C1076Pk) r12, (char) r0)
            goto L_0x0036
        L_0x01a8:
            int r8 = r12.f1382cp
            int r9 = r8 + 1
            r12.f1382cp = r9
            char r8 = r6[r8]
            int r2 = p001a.C1520WN.xDigitToInt(r8, r2)
            if (r2 >= 0) goto L_0x01c6
            int r0 = r12.f1382cp
            int r2 = r5 + 2
            int r0 = r0 - r2
            r12.f1382cp = r0
            int r0 = r12.f1382cp
            int r2 = r0 + 1
            r12.f1382cp = r2
            char r0 = r6[r0]
            goto L_0x01a2
        L_0x01c6:
            int r5 = r5 + 1
            goto L_0x0199
        L_0x01c9:
            a.alP r0 = new a.alP
            r2 = 14
            r0.<init>(r2)
            r12.dPQ = r0
            int r0 = r12.dPP
            int r0 = r0 + 1
            r12.dPP = r0
            goto L_0x0036
        L_0x01da:
            a.alP r0 = new a.alP
            r2 = 15
            r0.<init>(r2)
            r12.dPQ = r0
            int r0 = r12.dPP
            int r0 = r0 + 1
            r12.dPP = r0
            goto L_0x0036
        L_0x01eb:
            a.alP r0 = new a.alP
            r2 = 18
            r0.<init>(r2)
            r12.dPQ = r0
            int r0 = r12.dPP
            int r0 = r0 + 1
            r12.dPP = r0
            goto L_0x0036
        L_0x01fc:
            a.alP r0 = new a.alP
            r2 = 19
            r0.<init>(r2)
            r12.dPQ = r0
            int r0 = r12.dPP
            int r0 = r0 + 1
            r12.dPP = r0
            goto L_0x0036
        L_0x020d:
            a.alP r0 = new a.alP
            r2 = 16
            r0.<init>(r2)
            r12.dPQ = r0
            int r0 = r12.dPP
            int r0 = r0 + 1
            r12.dPP = r0
            goto L_0x0036
        L_0x021e:
            a.alP r0 = new a.alP
            r2 = 17
            r0.<init>(r2)
            r12.dPQ = r0
            int r0 = r12.dPP
            int r0 = r0 + 1
            r12.dPP = r0
            goto L_0x0036
        L_0x022f:
            java.lang.String r0 = "msg.trail.backslash"
            java.lang.String r2 = ""
            m21402ao(r0, r2)
            goto L_0x003f
        L_0x0238:
            r0 = 0
            int r2 = r12.f1382cp
            int r2 = r12.f1382cp
            int r2 = r2 + 1
            int r5 = r12.dPL
            if (r2 >= r5) goto L_0x02a9
            int r2 = r12.f1382cp
            char r2 = r6[r2]
            r5 = 63
            if (r2 != r5) goto L_0x02a9
            int r2 = r12.f1382cp
            int r2 = r2 + 1
            char r2 = r6[r2]
            r5 = 61
            if (r2 == r5) goto L_0x025d
            r5 = 33
            if (r2 == r5) goto L_0x025d
            r5 = 58
            if (r2 != r5) goto L_0x02a9
        L_0x025d:
            int r5 = r12.f1382cp
            int r5 = r5 + 2
            r12.f1382cp = r5
            r5 = 61
            if (r2 != r5) goto L_0x0297
            a.alP r0 = new a.alP
            r2 = 41
            r0.<init>(r2)
            int r2 = r12.dPP
            int r2 = r2 + 4
            r12.dPP = r2
        L_0x0274:
            int r2 = r12.dPN
            int r2 = r2 + 1
            r12.dPN = r2
            boolean r2 = m21395a((org.mozilla.javascript.regexp.C1076Pk) r12)
            if (r2 == 0) goto L_0x003f
            int r2 = r12.f1382cp
            int r5 = r12.dPL
            if (r2 == r5) goto L_0x028e
            int r2 = r12.f1382cp
            char r2 = r6[r2]
            r5 = 41
            if (r2 == r5) goto L_0x02bf
        L_0x028e:
            java.lang.String r0 = "msg.unterm.paren"
            java.lang.String r2 = ""
            m21402ao(r0, r2)
            goto L_0x003f
        L_0x0297:
            r5 = 33
            if (r2 != r5) goto L_0x0274
            a.alP r0 = new a.alP
            r2 = 42
            r0.<init>(r2)
            int r2 = r12.dPP
            int r2 = r2 + 4
            r12.dPP = r2
            goto L_0x0274
        L_0x02a9:
            a.alP r0 = new a.alP
            r2 = 10
            r0.<init>(r2)
            int r2 = r12.dPP
            int r2 = r2 + 6
            r12.dPP = r2
            int r2 = r12.dPM
            int r5 = r2 + 1
            r12.dPM = r5
            r0.fXX = r2
            goto L_0x0274
        L_0x02bf:
            int r2 = r12.f1382cp
            int r2 = r2 + 1
            r12.f1382cp = r2
            int r2 = r12.dPN
            int r2 = r2 + -1
            r12.dPN = r2
            if (r0 == 0) goto L_0x0036
            a.alP r2 = r12.dPQ
            r0.fXU = r2
            r12.dPQ = r0
            goto L_0x0036
        L_0x02d5:
            java.lang.String r0 = "msg.re.unmatched.right.paren"
            java.lang.String r2 = ""
            m21402ao(r0, r2)
            goto L_0x003f
        L_0x02de:
            a.alP r0 = new a.alP
            r2 = 50
            r0.<init>(r2)
            r12.dPQ = r0
            int r0 = r12.f1382cp
            a.alP r2 = r12.dPQ
            r2.startIndex = r0
        L_0x02ed:
            int r2 = r12.f1382cp
            int r5 = r12.dPL
            if (r2 != r5) goto L_0x02fc
            java.lang.String r0 = "msg.unterm.class"
            java.lang.String r2 = ""
            m21402ao(r0, r2)
            goto L_0x003f
        L_0x02fc:
            int r2 = r12.f1382cp
            char r2 = r6[r2]
            r5 = 92
            if (r2 != r5) goto L_0x0311
            int r2 = r12.f1382cp
            int r2 = r2 + 1
            r12.f1382cp = r2
        L_0x030a:
            int r2 = r12.f1382cp
            int r2 = r2 + 1
            r12.f1382cp = r2
            goto L_0x02ed
        L_0x0311:
            int r2 = r12.f1382cp
            char r2 = r6[r2]
            r5 = 93
            if (r2 != r5) goto L_0x030a
            a.alP r2 = r12.dPQ
            int r5 = r12.f1382cp
            int r5 = r5 - r0
            r2.fXZ = r5
            a.alP r2 = r12.dPQ
            int r5 = r12.dPO
            int r8 = r5 + 1
            r12.dPO = r8
            r2.index = r5
            a.alP r2 = r12.dPQ
            int r5 = r12.f1382cp
            int r8 = r5 + 1
            r12.f1382cp = r8
            boolean r0 = m21396a((org.mozilla.javascript.regexp.C1076Pk) r12, (org.mozilla.javascript.regexp.C6355alP) r2, (char[]) r6, (int) r0, (int) r5)
            if (r0 == 0) goto L_0x003f
            int r0 = r12.dPP
            int r0 = r0 + 3
            r12.dPP = r0
            goto L_0x0036
        L_0x0340:
            a.alP r0 = new a.alP
            r2 = 12
            r0.<init>(r2)
            r12.dPQ = r0
            int r0 = r12.dPP
            int r0 = r0 + 1
            r12.dPP = r0
            goto L_0x0036
        L_0x0351:
            java.lang.String r0 = "msg.bad.quant"
            int r2 = r12.f1382cp
            int r2 = r2 + -1
            char r2 = r6[r2]
            java.lang.String r2 = java.lang.String.valueOf(r2)
            m21402ao(r0, r2)
            goto L_0x003f
        L_0x0362:
            int r0 = r12.f1382cp
            char r0 = r6[r0]
            switch(r0) {
                case 42: goto L_0x0386;
                case 43: goto L_0x036f;
                case 63: goto L_0x039d;
                case 123: goto L_0x03b4;
                default: goto L_0x0369;
            }
        L_0x0369:
            r0 = r1
        L_0x036a:
            if (r0 != 0) goto L_0x0428
            r1 = r3
            goto L_0x003f
        L_0x036f:
            a.alP r0 = new a.alP
            r0.<init>(r10)
            r12.dPQ = r0
            a.alP r0 = r12.dPQ
            r0.min = r3
            a.alP r0 = r12.dPQ
            r0.max = r4
            int r0 = r12.dPP
            int r0 = r0 + 8
            r12.dPP = r0
            r0 = r3
            goto L_0x036a
        L_0x0386:
            a.alP r0 = new a.alP
            r0.<init>(r10)
            r12.dPQ = r0
            a.alP r0 = r12.dPQ
            r0.min = r1
            a.alP r0 = r12.dPQ
            r0.max = r4
            int r0 = r12.dPP
            int r0 = r0 + 8
            r12.dPP = r0
            r0 = r3
            goto L_0x036a
        L_0x039d:
            a.alP r0 = new a.alP
            r0.<init>(r10)
            r12.dPQ = r0
            a.alP r0 = r12.dPQ
            r0.min = r1
            a.alP r0 = r12.dPQ
            r0.max = r3
            int r0 = r12.dPP
            int r0 = r0 + 8
            r12.dPP = r0
            r0 = r3
            goto L_0x036a
        L_0x03b4:
            int r9 = r12.f1382cp
            int r0 = r12.f1382cp
            int r0 = r0 + 1
            r12.f1382cp = r0
            char r0 = r6[r0]
            boolean r2 = isDigit(r0)
            if (r2 == 0) goto L_0x045d
            int r2 = r12.f1382cp
            int r2 = r2 + 1
            r12.f1382cp = r2
            java.lang.String r2 = "msg.overlarge.min"
            int r2 = m21383a((char) r0, (org.mozilla.javascript.regexp.C1076Pk) r12, (int) r11, (java.lang.String) r2)
            int r0 = r12.f1382cp
            char r5 = r6[r0]
            r0 = 44
            if (r5 != r0) goto L_0x0407
            int r0 = r12.f1382cp
            int r0 = r0 + 1
            r12.f1382cp = r0
            char r5 = r6[r0]
            boolean r0 = isDigit(r5)
            if (r0 == 0) goto L_0x0461
            int r0 = r12.f1382cp
            int r0 = r0 + 1
            r12.f1382cp = r0
            java.lang.String r0 = "msg.overlarge.max"
            int r0 = m21383a((char) r5, (org.mozilla.javascript.regexp.C1076Pk) r12, (int) r11, (java.lang.String) r0)
            int r4 = r12.f1382cp
            char r4 = r6[r4]
            if (r2 <= r0) goto L_0x045f
            java.lang.String r0 = "msg.max.lt.min"
            int r2 = r12.f1382cp
            char r2 = r6[r2]
            java.lang.String r2 = java.lang.String.valueOf(r2)
            m21402ao(r0, r2)
            goto L_0x003f
        L_0x0407:
            r0 = r2
        L_0x0408:
            r4 = 125(0x7d, float:1.75E-43)
            if (r5 != r4) goto L_0x045d
            a.alP r4 = new a.alP
            r4.<init>(r10)
            r12.dPQ = r4
            a.alP r4 = r12.dPQ
            r4.min = r2
            a.alP r2 = r12.dPQ
            r2.max = r0
            int r0 = r12.dPP
            int r0 = r0 + 12
            r12.dPP = r0
            r0 = r3
        L_0x0422:
            if (r0 != 0) goto L_0x036a
            r12.f1382cp = r9
            goto L_0x036a
        L_0x0428:
            int r0 = r12.f1382cp
            int r0 = r0 + 1
            r12.f1382cp = r0
            a.alP r0 = r12.dPQ
            r0.fXU = r8
            a.alP r0 = r12.dPQ
            r0.fXX = r7
            a.alP r0 = r12.dPQ
            int r2 = r12.dPM
            int r2 = r2 - r7
            r0.dPM = r2
            int r0 = r12.f1382cp
            int r2 = r12.dPL
            if (r0 >= r2) goto L_0x0458
            int r0 = r12.f1382cp
            char r0 = r6[r0]
            r2 = 63
            if (r0 != r2) goto L_0x0458
            int r0 = r12.f1382cp
            int r0 = r0 + 1
            r12.f1382cp = r0
            a.alP r0 = r12.dPQ
            r0.fXY = r1
        L_0x0455:
            r1 = r3
            goto L_0x003f
        L_0x0458:
            a.alP r0 = r12.dPQ
            r0.fXY = r3
            goto L_0x0455
        L_0x045d:
            r0 = r1
            goto L_0x0422
        L_0x045f:
            r5 = r4
            goto L_0x0408
        L_0x0461:
            r0 = r4
            goto L_0x0408
        */
        throw new UnsupportedOperationException("Method not decompiled: org.mozilla.javascript.regexp.C6025aex.m21407c(a.Pk):boolean");
    }

    /* renamed from: e */
    private static void m21412e(byte[] bArr, int i, int i2) {
        if (i > i2) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        m21415f(bArr, i, i2 - i);
    }

    /* renamed from: c */
    private static int m21406c(byte[] bArr, int i) {
        return m21408d(bArr, i);
    }

    /* renamed from: f */
    private static int m21415f(byte[] bArr, int i, int i2) {
        if (i2 < 0) {
            throw org.mozilla1.javascript.Kit.codeBug();
        } else if (i2 > 65535) {
            throw org.mozilla1.javascript.Context.m35245gF("Too complex regexp");
        } else {
            bArr[i] = (byte) (i2 >> 8);
            bArr[i + 1] = (byte) i2;
            return i + 2;
        }
    }

    /* renamed from: d */
    private static int m21408d(byte[] bArr, int i) {
        return ((bArr[i] & 255) << 8) | (bArr[i + 1] & 255);
    }

    /* renamed from: a */
    private static int m21384a(CompilerState pk, org.mozilla1.javascript.regexp.RECompiled sa, int i, RENode alp) {
        int f;
        byte b;
        byte[] bArr = sa.ecu;
        while (alp != null) {
            int i2 = i + 1;
            bArr[i] = alp.cli;
            switch (alp.cli) {
                case 0:
                    i2--;
                    break;
                case 1:
                    RENode alp2 = alp.fXV;
                    int a = m21384a(pk, sa, i2 + 2, alp.fXU);
                    int i3 = a + 1;
                    bArr[a] = 23;
                    int i4 = i3 + 2;
                    m21412e(bArr, i2, i4);
                    int a2 = m21384a(pk, sa, i4, alp2);
                    int i5 = a2 + 1;
                    bArr[a2] = 23;
                    i2 = i5 + 2;
                    m21412e(bArr, i3, i2);
                    m21412e(bArr, i5, i2);
                    break;
                case 6:
                    if (alp.min == 0 && alp.max == -1) {
                        int i6 = i2 - 1;
                        if (alp.fXY) {
                            b = 7;
                        } else {
                            b = fpc;
                        }
                        bArr[i6] = b;
                        f = i2;
                    } else if (alp.min == 0 && alp.max == 1) {
                        bArr[i2 - 1] = alp.fXY ? 9 : fpe;
                        f = i2;
                    } else if (alp.min == 1 && alp.max == -1) {
                        bArr[i2 - 1] = alp.fXY ? 8 : fpd;
                        f = i2;
                    } else {
                        if (!alp.fXY) {
                            bArr[i2 - 1] = fpf;
                        }
                        f = m21415f(bArr, m21415f(bArr, i2, alp.min), alp.max + 1);
                    }
                    int f2 = m21415f(bArr, m21415f(bArr, f, alp.dPM), alp.fXX);
                    int a3 = m21384a(pk, sa, f2 + 2, alp.fXU);
                    i2 = a3 + 1;
                    bArr[a3] = fpg;
                    m21412e(bArr, f2, i2);
                    break;
                case 10:
                    int a4 = m21384a(pk, sa, m21415f(bArr, i2, alp.fXX), alp.fXU);
                    bArr[a4] = 11;
                    i2 = m21415f(bArr, a4 + 1, alp.fXX);
                    break;
                case 20:
                    i2 = m21415f(bArr, i2, alp.fXX);
                    break;
                case 21:
                    if (alp.fYc != -1) {
                        while (alp.fXT != null && alp.fXT.cli == 21 && alp.fYc + alp.length == alp.fXT.fYc) {
                            alp.length += alp.fXT.length;
                            alp.fXT = alp.fXT.fXT;
                        }
                    }
                    if (alp.fYc == -1 || alp.length <= 1) {
                        if (alp.fYb >= 256) {
                            if ((pk.flags & 2) != 0) {
                                bArr[i2 - 1] = foX;
                            } else {
                                bArr[i2 - 1] = foU;
                            }
                            i2 = m21415f(bArr, i2, alp.fYb);
                            break;
                        } else {
                            if ((pk.flags & 2) != 0) {
                                bArr[i2 - 1] = foW;
                            } else {
                                bArr[i2 - 1] = 22;
                            }
                            bArr[i2] = (byte) alp.fYb;
                            i2++;
                            break;
                        }
                    } else {
                        if ((pk.flags & 2) != 0) {
                            bArr[i2 - 1] = 32;
                        } else {
                            bArr[i2 - 1] = 21;
                        }
                        i2 = m21415f(bArr, m21415f(bArr, i2, alp.fYc), alp.length);
                        break;
                    }
                    break;
                case 41:
                    int a5 = m21384a(pk, sa, i2 + 2, alp.fXU);
                    int i7 = a5 + 1;
                    bArr[a5] = fpa;
                    m21412e(bArr, i2, i7);
                    i2 = i7;
                    break;
                case 42:
                    int a6 = m21384a(pk, sa, i2 + 2, alp.fXU);
                    int i8 = a6 + 1;
                    bArr[a6] = fpb;
                    m21412e(bArr, i2, i8);
                    i2 = i8;
                    break;
                case 50:
                    i2 = m21415f(bArr, i2, alp.index);
                    sa.ecv[alp.index] = new RECharSet(alp.fYa, alp.startIndex, alp.fXZ);
                    break;
            }
            alp = alp.fXT;
            i = i2;
        }
        return i;
    }

    /* renamed from: a */
    private static void m21392a(org.mozilla1.javascript.regexp.REGlobalData aij, int i, int i2, REBackTrackData uu, int i3, int i4) {
        aij.erg = new org.mozilla1.javascript.regexp.REProgState(aij.erg, i, i2, aij.f4604cp, uu, i3, i4);
    }

    /* renamed from: a */
    private static org.mozilla1.javascript.regexp.REProgState m21385a(org.mozilla1.javascript.regexp.REGlobalData aij) {
        org.mozilla1.javascript.regexp.REProgState asw = aij.erg;
        aij.erg = asw.gxA;
        return asw;
    }

    /* renamed from: a */
    private static void m21391a(org.mozilla1.javascript.regexp.REGlobalData aij, byte b, int i) {
        aij.fOz = new REBackTrackData(aij, b, i);
    }

    /* renamed from: a */
    private static boolean m21397a(org.mozilla1.javascript.regexp.REGlobalData aij, int i, int i2, char[] cArr, int i3) {
        if (aij.f4604cp + i2 > i3) {
            return false;
        }
        for (int i4 = 0; i4 < i2; i4++) {
            if (aij.fOx.ect[i + i4] != cArr[aij.f4604cp + i4]) {
                return false;
            }
        }
        aij.f4604cp += i2;
        return true;
    }

    /* renamed from: b */
    private static boolean m21405b(org.mozilla1.javascript.regexp.REGlobalData aij, int i, int i2, char[] cArr, int i3) {
        if (aij.f4604cp + i2 > i3) {
            return false;
        }
        for (int i4 = 0; i4 < i2; i4++) {
            if (m21414f(aij.fOx.ect[i + i4]) != m21414f(cArr[aij.f4604cp + i4])) {
                return false;
            }
        }
        aij.f4604cp += i2;
        return true;
    }

    /* renamed from: a */
    private static boolean m21398a(org.mozilla1.javascript.regexp.REGlobalData aij, int i, char[] cArr, int i2) {
        int rj = aij.mo13662rj(i);
        if (rj == -1) {
            return true;
        }
        int rk = aij.mo13663rk(i);
        if (aij.f4604cp + rk > i2) {
            return false;
        }
        if ((aij.fOx.flags & 2) != 0) {
            for (int i3 = 0; i3 < rk; i3++) {
                if (m21414f(cArr[rj + i3]) != m21414f(cArr[aij.f4604cp + i3])) {
                    return false;
                }
            }
        } else {
            for (int i4 = 0; i4 < rk; i4++) {
                if (cArr[rj + i4] != cArr[aij.f4604cp + i4]) {
                    return false;
                }
            }
        }
        aij.f4604cp += rk;
        return true;
    }

    /* renamed from: a */
    private static void m21389a(RECharSet aqo, char c) {
        int i = c / 8;
        if (c > aqo.length) {
            throw new RuntimeException();
        }
        byte[] bArr = aqo.iEy;
        bArr[i] = (byte) (bArr[i] | (1 << (c & 7)));
    }

    /* renamed from: a */
    private static void m21390a(RECharSet aqo, char c, char c2) {
        int i = c / 8;
        int i2 = c2 / 8;
        if (c2 > aqo.length || c > c2) {
            throw new RuntimeException();
        }
        char c3 = (char) (c & 7);
        char c4 = (char) (c2 & 7);
        if (i == i2) {
            byte[] bArr = aqo.iEy;
            bArr[i] = (byte) (((255 >> (7 - (c4 - c3))) << c3) | bArr[i]);
            return;
        }
        byte[] bArr2 = aqo.iEy;
        bArr2[i] = (byte) ((255 << c3) | bArr2[i]);
        for (int i3 = i + 1; i3 < i2; i3++) {
            aqo.iEy[i3] = -1;
        }
        byte[] bArr3 = aqo.iEy;
        bArr3[i2] = (byte) (bArr3[i2] | (255 >> (7 - c4)));
    }

    /* renamed from: a */
    private static void m21393a(org.mozilla1.javascript.regexp.REGlobalData aij, RECharSet aqo) {
        synchronized (aqo) {
            if (!aqo.iEw) {
                m21403b(aij, aqo);
                aqo.iEw = true;
            }
        }
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m21403b(org.mozilla1.javascript.regexp.REGlobalData r14, RECharSet r15) {
        /*
            r1 = 92
            r13 = 55
            r8 = 1
            r12 = 48
            r5 = 0
            int r3 = r15.startIndex
            int r0 = r15.iEv
            int r10 = r3 + r0
            r15.iEx = r8
            int r0 = r15.length
            int r0 = r0 / 8
            int r0 = r0 + 1
            byte[] r0 = new byte[r0]
            r15.iEy = r0
            if (r3 != r10) goto L_0x001d
        L_0x001c:
            return
        L_0x001d:
            a.Sa r0 = r14.fOx
            char[] r0 = r0.ect
            char r0 = r0[r3]
            r2 = 94
            if (r0 != r2) goto L_0x01a2
            r15.iEx = r5
            int r3 = r3 + 1
            r7 = r5
            r9 = r5
            r2 = r3
        L_0x002e:
            if (r2 == r10) goto L_0x001c
            r0 = 2
            a.Sa r3 = r14.fOx
            char[] r3 = r3.ect
            char r3 = r3[r2]
            switch(r3) {
                case 92: goto L_0x0065;
                default: goto L_0x003a;
            }
        L_0x003a:
            a.Sa r0 = r14.fOx
            char[] r0 = r0.ect
            int r3 = r2 + 1
            char r0 = r0[r2]
        L_0x0042:
            if (r7 == 0) goto L_0x016d
            a.Sa r2 = r14.fOx
            int r2 = r2.flags
            r2 = r2 & 2
            if (r2 == 0) goto L_0x0168
            char r2 = m21414f(r9)
            char r4 = m21414f(r0)
            m21390a((org.mozilla.javascript.regexp.C5626aQo) r15, (char) r2, (char) r4)
            char r2 = m21416g(r9)
            char r0 = m21416g(r0)
            m21390a((org.mozilla.javascript.regexp.C5626aQo) r15, (char) r2, (char) r0)
        L_0x0062:
            r7 = r5
            r2 = r3
            goto L_0x002e
        L_0x0065:
            int r2 = r2 + 1
            a.Sa r3 = r14.fOx
            char[] r4 = r3.ect
            int r3 = r2 + 1
            char r2 = r4[r2]
            switch(r2) {
                case 48: goto L_0x00cf;
                case 49: goto L_0x00cf;
                case 50: goto L_0x00cf;
                case 51: goto L_0x00cf;
                case 52: goto L_0x00cf;
                case 53: goto L_0x00cf;
                case 54: goto L_0x00cf;
                case 55: goto L_0x00cf;
                case 68: goto L_0x0106;
                case 83: goto L_0x012a;
                case 87: goto L_0x0153;
                case 98: goto L_0x0074;
                case 99: goto L_0x0086;
                case 100: goto L_0x00fe;
                case 102: goto L_0x0077;
                case 110: goto L_0x007a;
                case 114: goto L_0x007d;
                case 115: goto L_0x0116;
                case 116: goto L_0x0080;
                case 117: goto L_0x00a9;
                case 118: goto L_0x0083;
                case 119: goto L_0x013e;
                case 120: goto L_0x00aa;
                default: goto L_0x0072;
            }
        L_0x0072:
            r0 = r2
            goto L_0x0042
        L_0x0074:
            r0 = 8
            goto L_0x0042
        L_0x0077:
            r0 = 12
            goto L_0x0042
        L_0x007a:
            r0 = 10
            goto L_0x0042
        L_0x007d:
            r0 = 13
            goto L_0x0042
        L_0x0080:
            r0 = 9
            goto L_0x0042
        L_0x0083:
            r0 = 11
            goto L_0x0042
        L_0x0086:
            int r0 = r3 + 1
            if (r0 >= r10) goto L_0x00a5
            a.Sa r0 = r14.fOx
            char[] r0 = r0.ect
            int r2 = r3 + 1
            char r0 = r0[r2]
            boolean r0 = m21411d((char) r0)
            if (r0 == 0) goto L_0x00a5
            a.Sa r0 = r14.fOx
            char[] r0 = r0.ect
            int r2 = r3 + 1
            char r0 = r0[r3]
            r0 = r0 & 31
            char r0 = (char) r0
            r3 = r2
            goto L_0x0042
        L_0x00a5:
            int r3 = r3 + -1
            r0 = r1
            goto L_0x0042
        L_0x00a9:
            r0 = 4
        L_0x00aa:
            r4 = r5
            r2 = r5
        L_0x00ac:
            if (r4 >= r0) goto L_0x00b0
            if (r3 < r10) goto L_0x00b3
        L_0x00b0:
            r0 = r2
        L_0x00b1:
            char r0 = (char) r0
            goto L_0x0042
        L_0x00b3:
            a.Sa r6 = r14.fOx
            char[] r11 = r6.ect
            int r6 = r3 + 1
            char r3 = r11[r3]
            int r3 = m21418qm(r3)
            if (r3 >= 0) goto L_0x00c8
            int r0 = r4 + 1
            int r2 = r6 - r0
            r0 = r1
            r3 = r2
            goto L_0x00b1
        L_0x00c8:
            int r2 = r2 << 4
            r2 = r2 | r3
            int r4 = r4 + 1
            r3 = r6
            goto L_0x00ac
        L_0x00cf:
            int r0 = r2 + -48
            a.Sa r2 = r14.fOx
            char[] r2 = r2.ect
            char r2 = r2[r3]
            if (r12 > r2) goto L_0x00f7
            if (r2 > r13) goto L_0x00f7
            int r3 = r3 + 1
            int r0 = r0 * 8
            int r2 = r2 + -48
            int r2 = r2 + r0
            a.Sa r0 = r14.fOx
            char[] r0 = r0.ect
            char r0 = r0[r3]
            if (r12 > r0) goto L_0x019f
            if (r0 > r13) goto L_0x019f
            int r3 = r3 + 1
            int r4 = r2 * 8
            int r0 = r0 + -48
            int r0 = r0 + r4
            r4 = 255(0xff, float:3.57E-43)
            if (r0 > r4) goto L_0x00fa
        L_0x00f7:
            char r0 = (char) r0
            goto L_0x0042
        L_0x00fa:
            int r3 = r3 + -1
            r0 = r2
            goto L_0x00f7
        L_0x00fe:
            r0 = 57
            m21390a((org.mozilla.javascript.regexp.C5626aQo) r15, (char) r12, (char) r0)
            r2 = r3
            goto L_0x002e
        L_0x0106:
            r0 = 47
            m21390a((org.mozilla.javascript.regexp.C5626aQo) r15, (char) r5, (char) r0)
            r0 = 58
            int r2 = r15.length
            char r2 = (char) r2
            m21390a((org.mozilla.javascript.regexp.C5626aQo) r15, (char) r0, (char) r2)
            r2 = r3
            goto L_0x002e
        L_0x0116:
            int r0 = r15.length
        L_0x0118:
            if (r0 >= 0) goto L_0x011d
            r2 = r3
            goto L_0x002e
        L_0x011d:
            boolean r2 = m21417ql(r0)
            if (r2 == 0) goto L_0x0127
            char r2 = (char) r0
            m21389a((org.mozilla.javascript.regexp.C5626aQo) r15, (char) r2)
        L_0x0127:
            int r0 = r0 + -1
            goto L_0x0118
        L_0x012a:
            int r0 = r15.length
        L_0x012c:
            if (r0 >= 0) goto L_0x0131
            r2 = r3
            goto L_0x002e
        L_0x0131:
            boolean r2 = m21417ql(r0)
            if (r2 != 0) goto L_0x013b
            char r2 = (char) r0
            m21389a((org.mozilla.javascript.regexp.C5626aQo) r15, (char) r2)
        L_0x013b:
            int r0 = r0 + -1
            goto L_0x012c
        L_0x013e:
            int r0 = r15.length
        L_0x0140:
            if (r0 >= 0) goto L_0x0145
            r2 = r3
            goto L_0x002e
        L_0x0145:
            char r2 = (char) r0
            boolean r2 = m21411d((char) r2)
            if (r2 == 0) goto L_0x0150
            char r2 = (char) r0
            m21389a((org.mozilla.javascript.regexp.C5626aQo) r15, (char) r2)
        L_0x0150:
            int r0 = r0 + -1
            goto L_0x0140
        L_0x0153:
            int r0 = r15.length
        L_0x0155:
            if (r0 >= 0) goto L_0x015a
            r2 = r3
            goto L_0x002e
        L_0x015a:
            char r2 = (char) r0
            boolean r2 = m21411d((char) r2)
            if (r2 != 0) goto L_0x0165
            char r2 = (char) r0
            m21389a((org.mozilla.javascript.regexp.C5626aQo) r15, (char) r2)
        L_0x0165:
            int r0 = r0 + -1
            goto L_0x0155
        L_0x0168:
            m21390a((org.mozilla.javascript.regexp.C5626aQo) r15, (char) r9, (char) r0)
            goto L_0x0062
        L_0x016d:
            a.Sa r2 = r14.fOx
            int r2 = r2.flags
            r2 = r2 & 2
            if (r2 == 0) goto L_0x0198
            char r2 = m21414f(r0)
            m21389a((org.mozilla.javascript.regexp.C5626aQo) r15, (char) r2)
            char r2 = m21416g(r0)
            m21389a((org.mozilla.javascript.regexp.C5626aQo) r15, (char) r2)
        L_0x0183:
            int r2 = r10 + -1
            if (r3 >= r2) goto L_0x019c
            a.Sa r2 = r14.fOx
            char[] r2 = r2.ect
            char r2 = r2[r3]
            r4 = 45
            if (r2 != r4) goto L_0x019c
            int r3 = r3 + 1
            r7 = r8
            r9 = r0
            r2 = r3
            goto L_0x002e
        L_0x0198:
            m21389a((org.mozilla.javascript.regexp.C5626aQo) r15, (char) r0)
            goto L_0x0183
        L_0x019c:
            r2 = r3
            goto L_0x002e
        L_0x019f:
            r0 = r2
            goto L_0x00f7
        L_0x01a2:
            r7 = r5
            r9 = r5
            r2 = r3
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.mozilla.javascript.regexp.C6025aex.m21403b(a.aiJ, a.aQo):void");
    }

    /* renamed from: a */
    private static boolean m21400a(org.mozilla1.javascript.regexp.REGlobalData aij, RECharSet aqo, char c) {
        if (!aqo.iEw) {
            m21393a(aij, aqo);
        }
        int i = c / 8;
        if (aqo.iEx) {
            if (aqo.length == 0 || c > aqo.length || (aqo.iEy[i] & (1 << (c & 7))) == 0) {
                return false;
            }
        } else if (!(aqo.length == 0 || c > aqo.length || (aqo.iEy[i] & (1 << (c & 7))) == 0)) {
            return false;
        }
        return true;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v0, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v5, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v5, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v7, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v12, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v9, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v11, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v16, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v16, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v17, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v17, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v18, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v20, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v21, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v22, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v23, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v24, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v20, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v25, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v26, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v22, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v23, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v24, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v25, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v26, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v27, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v28, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v29, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v30, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v31, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v32, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v33, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v34, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v35, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v36, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v37, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v38, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v39, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v40, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v41, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v42, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v43, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v44, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v45, resolved type: byte} */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x0326, code lost:
        r1 = 0;
        r2 = -1;
        r6 = r0;
        r7 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:184:0x032a, code lost:
        m21392a(r12, r1, r2, (org.mozilla.javascript.regexp.C1397UU) null, r4, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x032f, code lost:
        if (r6 == false) goto L_0x0360;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x0331, code lost:
        r5 = 51;
        m21391a(r12, (byte) fpi, r7);
        r0 = r7 + 6;
        r8 = r0 + 1;
        r6 = r10[r0];
        r4 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:0x0343, code lost:
        r1 = 1;
        r2 = -1;
        r6 = r0;
        r7 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x0349, code lost:
        r1 = 0;
        r2 = 1;
        r6 = r0;
        r7 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x034f, code lost:
        r1 = m21406c(r10, r8);
        r3 = r8 + 2;
        r2 = m21406c(r10, r3) - 1;
        r6 = r0;
        r7 = r3 + 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:193:0x0360, code lost:
        if (r1 == 0) goto L_0x036e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x0362, code lost:
        r5 = 52;
        r0 = r7 + 6;
        r8 = r0 + 1;
        r6 = r10[r0];
        r4 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x036e, code lost:
        m21391a(r12, (byte) fpj, r7);
        m21385a(r12);
        r0 = r7 + 4;
        r0 = r0 + m21406c(r10, r0);
        r8 = r0 + 1;
        r6 = r10[r0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:253:0x04c7, code lost:
        r8 = r0;
        r5 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:265:0x000d, code lost:
        continue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:267:0x000d, code lost:
        r5 = r5;
     */
    /* JADX WARNING: Failed to insert additional move for type inference */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean m21401a(org.mozilla1.javascript.regexp.REGlobalData r12, char[] r13, int r14) {
        /*
            r0 = 0
            a.Sa r1 = r12.fOx
            byte[] r10 = r1.ecu
            r9 = 0
            r4 = 0
            r5 = 53
            r8 = 1
            byte r0 = r10[r0]
            r6 = r0
        L_0x000d:
            switch(r6) {
                case 0: goto L_0x0015;
                case 1: goto L_0x0245;
                case 2: goto L_0x0048;
                case 3: goto L_0x006a;
                case 4: goto L_0x008a;
                case 5: goto L_0x00b2;
                case 6: goto L_0x031c;
                case 7: goto L_0x031c;
                case 8: goto L_0x031c;
                case 9: goto L_0x031c;
                case 10: goto L_0x0275;
                case 11: goto L_0x0288;
                case 12: goto L_0x00da;
                case 13: goto L_0x0010;
                case 14: goto L_0x00f5;
                case 15: goto L_0x0110;
                case 16: goto L_0x0161;
                case 17: goto L_0x017c;
                case 18: goto L_0x012b;
                case 19: goto L_0x0146;
                case 20: goto L_0x02a5;
                case 21: goto L_0x0197;
                case 22: goto L_0x01bb;
                case 23: goto L_0x0261;
                case 24: goto L_0x0010;
                case 25: goto L_0x0010;
                case 26: goto L_0x0010;
                case 27: goto L_0x0010;
                case 28: goto L_0x0201;
                case 29: goto L_0x0010;
                case 30: goto L_0x0010;
                case 31: goto L_0x0010;
                case 32: goto L_0x01a9;
                case 33: goto L_0x01da;
                case 34: goto L_0x0010;
                case 35: goto L_0x021f;
                case 36: goto L_0x0010;
                case 37: goto L_0x0010;
                case 38: goto L_0x0010;
                case 39: goto L_0x0010;
                case 40: goto L_0x0010;
                case 41: goto L_0x02d7;
                case 42: goto L_0x02d7;
                case 43: goto L_0x02f9;
                case 44: goto L_0x02f9;
                case 45: goto L_0x031c;
                case 46: goto L_0x031c;
                case 47: goto L_0x031c;
                case 48: goto L_0x031c;
                case 49: goto L_0x0384;
                case 50: goto L_0x02b1;
                case 51: goto L_0x0388;
                case 52: goto L_0x040b;
                case 53: goto L_0x04b8;
                default: goto L_0x0010;
            }
        L_0x0010:
            java.lang.RuntimeException r0 = p001a.C1520WN.codeBug()
            throw r0
        L_0x0015:
            r9 = 1
        L_0x0016:
            if (r9 != 0) goto L_0x04bc
            a.UU r1 = r12.fOz
            if (r1 == 0) goto L_0x04ba
            a.UU r0 = r1.erb
            r12.fOz = r0
            int r0 = r1.ere
            r12.ere = r0
            long[] r0 = r1.erf
            if (r0 == 0) goto L_0x0032
            long[] r0 = r1.erf
            java.lang.Object r0 = r0.clone()
            long[] r0 = (long[]) r0
            r12.erf = r0
        L_0x0032:
            int r0 = r1.f1768cp
            r12.f4604cp = r0
            a.asW r0 = r1.erg
            r12.erg = r0
            a.asW r0 = r12.erg
            int r5 = r0.erc
            a.asW r0 = r12.erg
            int r4 = r0.erd
            int r8 = r1.erd
            int r0 = r1.erc
            r6 = r0
            goto L_0x000d
        L_0x0048:
            int r0 = r12.f4604cp
            if (r0 == 0) goto L_0x0068
            boolean r0 = r12.fOw
            if (r0 != 0) goto L_0x0058
            a.Sa r0 = r12.fOx
            int r0 = r0.flags
            r0 = r0 & 4
            if (r0 == 0) goto L_0x0066
        L_0x0058:
            int r0 = r12.f4604cp
            int r0 = r0 + -1
            char r0 = r13[r0]
            boolean r0 = m21413e(r0)
            if (r0 != 0) goto L_0x0068
            r9 = 0
            goto L_0x0016
        L_0x0066:
            r9 = 0
            goto L_0x0016
        L_0x0068:
            r9 = 1
            goto L_0x0016
        L_0x006a:
            int r0 = r12.f4604cp
            if (r0 == r14) goto L_0x0088
            boolean r0 = r12.fOw
            if (r0 != 0) goto L_0x007a
            a.Sa r0 = r12.fOx
            int r0 = r0.flags
            r0 = r0 & 4
            if (r0 == 0) goto L_0x0086
        L_0x007a:
            int r0 = r12.f4604cp
            char r0 = r13[r0]
            boolean r0 = m21413e(r0)
            if (r0 != 0) goto L_0x0088
            r9 = 0
            goto L_0x0016
        L_0x0086:
            r9 = 0
            goto L_0x0016
        L_0x0088:
            r9 = 1
            goto L_0x0016
        L_0x008a:
            int r0 = r12.f4604cp
            if (r0 == 0) goto L_0x00ae
            int r0 = r12.f4604cp
            int r0 = r0 + -1
            char r0 = r13[r0]
            boolean r0 = m21411d((char) r0)
            if (r0 == 0) goto L_0x00ae
            r0 = 0
        L_0x009b:
            int r1 = r12.f4604cp
            if (r1 >= r14) goto L_0x00b0
            int r1 = r12.f4604cp
            char r1 = r13[r1]
            boolean r1 = m21411d((char) r1)
            if (r1 == 0) goto L_0x00b0
            r1 = 0
        L_0x00aa:
            r9 = r0 ^ r1
            goto L_0x0016
        L_0x00ae:
            r0 = 1
            goto L_0x009b
        L_0x00b0:
            r1 = 1
            goto L_0x00aa
        L_0x00b2:
            int r0 = r12.f4604cp
            if (r0 == 0) goto L_0x00d6
            int r0 = r12.f4604cp
            int r0 = r0 + -1
            char r0 = r13[r0]
            boolean r0 = m21411d((char) r0)
            if (r0 == 0) goto L_0x00d6
            r0 = 0
        L_0x00c3:
            int r1 = r12.f4604cp
            if (r1 >= r14) goto L_0x00d8
            int r1 = r12.f4604cp
            char r1 = r13[r1]
            boolean r1 = m21411d((char) r1)
            if (r1 == 0) goto L_0x00d8
            r1 = 1
        L_0x00d2:
            r9 = r0 ^ r1
            goto L_0x0016
        L_0x00d6:
            r0 = 1
            goto L_0x00c3
        L_0x00d8:
            r1 = 0
            goto L_0x00d2
        L_0x00da:
            int r0 = r12.f4604cp
            if (r0 == r14) goto L_0x00f3
            int r0 = r12.f4604cp
            char r0 = r13[r0]
            boolean r0 = m21413e(r0)
            if (r0 != 0) goto L_0x00f3
            r9 = 1
        L_0x00e9:
            if (r9 == 0) goto L_0x0016
            int r0 = r12.f4604cp
            int r0 = r0 + 1
            r12.f4604cp = r0
            goto L_0x0016
        L_0x00f3:
            r9 = 0
            goto L_0x00e9
        L_0x00f5:
            int r0 = r12.f4604cp
            if (r0 == r14) goto L_0x010e
            int r0 = r12.f4604cp
            char r0 = r13[r0]
            boolean r0 = isDigit(r0)
            if (r0 == 0) goto L_0x010e
            r9 = 1
        L_0x0104:
            if (r9 == 0) goto L_0x0016
            int r0 = r12.f4604cp
            int r0 = r0 + 1
            r12.f4604cp = r0
            goto L_0x0016
        L_0x010e:
            r9 = 0
            goto L_0x0104
        L_0x0110:
            int r0 = r12.f4604cp
            if (r0 == r14) goto L_0x0129
            int r0 = r12.f4604cp
            char r0 = r13[r0]
            boolean r0 = isDigit(r0)
            if (r0 != 0) goto L_0x0129
            r9 = 1
        L_0x011f:
            if (r9 == 0) goto L_0x0016
            int r0 = r12.f4604cp
            int r0 = r0 + 1
            r12.f4604cp = r0
            goto L_0x0016
        L_0x0129:
            r9 = 0
            goto L_0x011f
        L_0x012b:
            int r0 = r12.f4604cp
            if (r0 == r14) goto L_0x0144
            int r0 = r12.f4604cp
            char r0 = r13[r0]
            boolean r0 = m21417ql(r0)
            if (r0 == 0) goto L_0x0144
            r9 = 1
        L_0x013a:
            if (r9 == 0) goto L_0x0016
            int r0 = r12.f4604cp
            int r0 = r0 + 1
            r12.f4604cp = r0
            goto L_0x0016
        L_0x0144:
            r9 = 0
            goto L_0x013a
        L_0x0146:
            int r0 = r12.f4604cp
            if (r0 == r14) goto L_0x015f
            int r0 = r12.f4604cp
            char r0 = r13[r0]
            boolean r0 = m21417ql(r0)
            if (r0 != 0) goto L_0x015f
            r9 = 1
        L_0x0155:
            if (r9 == 0) goto L_0x0016
            int r0 = r12.f4604cp
            int r0 = r0 + 1
            r12.f4604cp = r0
            goto L_0x0016
        L_0x015f:
            r9 = 0
            goto L_0x0155
        L_0x0161:
            int r0 = r12.f4604cp
            if (r0 == r14) goto L_0x017a
            int r0 = r12.f4604cp
            char r0 = r13[r0]
            boolean r0 = m21411d((char) r0)
            if (r0 == 0) goto L_0x017a
            r9 = 1
        L_0x0170:
            if (r9 == 0) goto L_0x0016
            int r0 = r12.f4604cp
            int r0 = r0 + 1
            r12.f4604cp = r0
            goto L_0x0016
        L_0x017a:
            r9 = 0
            goto L_0x0170
        L_0x017c:
            int r0 = r12.f4604cp
            if (r0 == r14) goto L_0x0195
            int r0 = r12.f4604cp
            char r0 = r13[r0]
            boolean r0 = m21411d((char) r0)
            if (r0 != 0) goto L_0x0195
            r9 = 1
        L_0x018b:
            if (r9 == 0) goto L_0x0016
            int r0 = r12.f4604cp
            int r0 = r0 + 1
            r12.f4604cp = r0
            goto L_0x0016
        L_0x0195:
            r9 = 0
            goto L_0x018b
        L_0x0197:
            int r0 = m21408d((byte[]) r10, (int) r8)
            int r1 = r8 + 2
            int r2 = m21408d((byte[]) r10, (int) r1)
            int r8 = r1 + 2
            boolean r9 = m21397a((org.mozilla.javascript.regexp.C6193aiJ) r12, (int) r0, (int) r2, (char[]) r13, (int) r14)
            goto L_0x0016
        L_0x01a9:
            int r0 = m21408d((byte[]) r10, (int) r8)
            int r1 = r8 + 2
            int r2 = m21408d((byte[]) r10, (int) r1)
            int r8 = r1 + 2
            boolean r9 = m21405b(r12, r0, r2, r13, r14)
            goto L_0x0016
        L_0x01bb:
            int r0 = r8 + 1
            byte r1 = r10[r8]
            r1 = r1 & 255(0xff, float:3.57E-43)
            char r1 = (char) r1
            int r2 = r12.f4604cp
            if (r2 == r14) goto L_0x01d8
            int r2 = r12.f4604cp
            char r2 = r13[r2]
            if (r2 != r1) goto L_0x01d8
            r9 = 1
        L_0x01cd:
            if (r9 == 0) goto L_0x04c7
            int r1 = r12.f4604cp
            int r1 = r1 + 1
            r12.f4604cp = r1
            r8 = r0
            goto L_0x0016
        L_0x01d8:
            r9 = 0
            goto L_0x01cd
        L_0x01da:
            int r0 = r8 + 1
            byte r1 = r10[r8]
            r1 = r1 & 255(0xff, float:3.57E-43)
            char r1 = (char) r1
            int r2 = r12.f4604cp
            if (r2 == r14) goto L_0x01ff
            int r2 = r12.f4604cp
            char r2 = r13[r2]
            char r2 = m21414f(r2)
            char r1 = m21414f(r1)
            if (r2 != r1) goto L_0x01ff
            r9 = 1
        L_0x01f4:
            if (r9 == 0) goto L_0x04c7
            int r1 = r12.f4604cp
            int r1 = r1 + 1
            r12.f4604cp = r1
            r8 = r0
            goto L_0x0016
        L_0x01ff:
            r9 = 0
            goto L_0x01f4
        L_0x0201:
            int r0 = m21408d((byte[]) r10, (int) r8)
            char r0 = (char) r0
            int r8 = r8 + 2
            int r1 = r12.f4604cp
            if (r1 == r14) goto L_0x021d
            int r1 = r12.f4604cp
            char r1 = r13[r1]
            if (r1 != r0) goto L_0x021d
            r9 = 1
        L_0x0213:
            if (r9 == 0) goto L_0x0016
            int r0 = r12.f4604cp
            int r0 = r0 + 1
            r12.f4604cp = r0
            goto L_0x0016
        L_0x021d:
            r9 = 0
            goto L_0x0213
        L_0x021f:
            int r0 = m21408d((byte[]) r10, (int) r8)
            char r0 = (char) r0
            int r8 = r8 + 2
            int r1 = r12.f4604cp
            if (r1 == r14) goto L_0x0243
            int r1 = r12.f4604cp
            char r1 = r13[r1]
            char r1 = m21414f(r1)
            char r0 = m21414f(r0)
            if (r1 != r0) goto L_0x0243
            r9 = 1
        L_0x0239:
            if (r9 == 0) goto L_0x0016
            int r0 = r12.f4604cp
            int r0 = r0 + 1
            r12.f4604cp = r0
            goto L_0x0016
        L_0x0243:
            r9 = 0
            goto L_0x0239
        L_0x0245:
            r1 = 0
            r2 = 0
            r3 = 0
            r0 = r12
            m21392a((org.mozilla.javascript.regexp.C6193aiJ) r0, (int) r1, (int) r2, (org.mozilla.javascript.regexp.C1397UU) r3, (int) r4, (int) r5)
            int r0 = m21406c(r10, r8)
            int r0 = r0 + r8
            int r1 = r0 + 1
            byte r0 = r10[r0]
            m21391a((org.mozilla.javascript.regexp.C6193aiJ) r12, (byte) r0, (int) r1)
            int r0 = r8 + 2
            int r8 = r0 + 1
            byte r0 = r10[r0]
            r6 = r0
            goto L_0x000d
        L_0x0261:
            a.asW r0 = m21385a((org.mozilla.javascript.regexp.C6193aiJ) r12)
            int r4 = r0.erd
            int r5 = r0.erc
            int r0 = m21406c(r10, r8)
            int r0 = r0 + r8
            int r8 = r0 + 1
            byte r0 = r10[r0]
            r6 = r0
            goto L_0x000d
        L_0x0275:
            int r0 = m21408d((byte[]) r10, (int) r8)
            int r1 = r8 + 2
            int r2 = r12.f4604cp
            r3 = 0
            r12.mo13664t(r0, r2, r3)
            int r8 = r1 + 1
            byte r0 = r10[r1]
            r6 = r0
            goto L_0x000d
        L_0x0288:
            int r0 = m21408d((byte[]) r10, (int) r8)
            int r1 = r8 + 2
            int r2 = r12.mo13662rj(r0)
            int r3 = r12.f4604cp
            int r3 = r3 - r2
            r12.mo13664t(r0, r2, r3)
            int r2 = r12.ere
            if (r0 <= r2) goto L_0x029e
            r12.ere = r0
        L_0x029e:
            int r8 = r1 + 1
            byte r0 = r10[r1]
            r6 = r0
            goto L_0x000d
        L_0x02a5:
            int r0 = m21408d((byte[]) r10, (int) r8)
            int r8 = r8 + 2
            boolean r9 = m21398a((org.mozilla.javascript.regexp.C6193aiJ) r12, (int) r0, (char[]) r13, (int) r14)
            goto L_0x0016
        L_0x02b1:
            int r0 = m21408d((byte[]) r10, (int) r8)
            int r8 = r8 + 2
            int r1 = r12.f4604cp
            if (r1 == r14) goto L_0x02d4
            a.Sa r1 = r12.fOx
            a.aQo[] r1 = r1.ecv
            r0 = r1[r0]
            int r1 = r12.f4604cp
            char r1 = r13[r1]
            boolean r0 = m21400a((org.mozilla.javascript.regexp.C6193aiJ) r12, (org.mozilla.javascript.regexp.C5626aQo) r0, (char) r1)
            if (r0 == 0) goto L_0x02d4
            int r0 = r12.f4604cp
            int r0 = r0 + 1
            r12.f4604cp = r0
            r9 = 1
            goto L_0x0016
        L_0x02d4:
            r9 = 0
            goto L_0x0016
        L_0x02d7:
            r1 = 0
            r2 = 0
            a.UU r3 = r12.fOz
            r0 = r12
            m21392a((org.mozilla.javascript.regexp.C6193aiJ) r0, (int) r1, (int) r2, (org.mozilla.javascript.regexp.C1397UU) r3, (int) r4, (int) r5)
            r0 = 41
            if (r6 != r0) goto L_0x02f6
            r0 = 43
        L_0x02e5:
            int r1 = m21406c(r10, r8)
            int r1 = r1 + r8
            m21391a((org.mozilla.javascript.regexp.C6193aiJ) r12, (byte) r0, (int) r1)
            int r0 = r8 + 2
            int r8 = r0 + 1
            byte r0 = r10[r0]
            r6 = r0
            goto L_0x000d
        L_0x02f6:
            r0 = 44
            goto L_0x02e5
        L_0x02f9:
            a.asW r0 = m21385a((org.mozilla.javascript.regexp.C6193aiJ) r12)
            int r1 = r0.index
            r12.f4604cp = r1
            a.UU r1 = r0.gxB
            r12.fOz = r1
            int r4 = r0.erd
            int r5 = r0.erc
            if (r9 == 0) goto L_0x0315
            r0 = 43
            if (r6 != r0) goto L_0x0312
            r9 = 1
            goto L_0x0016
        L_0x0312:
            r9 = 0
            goto L_0x0016
        L_0x0315:
            r0 = 43
            if (r6 == r0) goto L_0x0016
            r9 = 1
            goto L_0x0016
        L_0x031c:
            r0 = 0
            switch(r6) {
                case 6: goto L_0x034e;
                case 7: goto L_0x0325;
                case 8: goto L_0x0342;
                case 9: goto L_0x0348;
                case 45: goto L_0x0326;
                case 46: goto L_0x0343;
                case 47: goto L_0x0349;
                case 48: goto L_0x034f;
                default: goto L_0x0320;
            }
        L_0x0320:
            java.lang.RuntimeException r0 = p001a.C1520WN.codeBug()
            throw r0
        L_0x0325:
            r0 = 1
        L_0x0326:
            r1 = 0
            r2 = -1
            r6 = r0
            r7 = r8
        L_0x032a:
            r3 = 0
            r0 = r12
            m21392a((org.mozilla.javascript.regexp.C6193aiJ) r0, (int) r1, (int) r2, (org.mozilla.javascript.regexp.C1397UU) r3, (int) r4, (int) r5)
            if (r6 == 0) goto L_0x0360
            r5 = 51
            r0 = 51
            m21391a((org.mozilla.javascript.regexp.C6193aiJ) r12, (byte) r0, (int) r7)
            int r0 = r7 + 6
            int r8 = r0 + 1
            byte r0 = r10[r0]
            r6 = r0
            r4 = r7
            goto L_0x000d
        L_0x0342:
            r0 = 1
        L_0x0343:
            r1 = 1
            r2 = -1
            r6 = r0
            r7 = r8
            goto L_0x032a
        L_0x0348:
            r0 = 1
        L_0x0349:
            r1 = 0
            r2 = 1
            r6 = r0
            r7 = r8
            goto L_0x032a
        L_0x034e:
            r0 = 1
        L_0x034f:
            int r1 = m21406c(r10, r8)
            int r3 = r8 + 2
            int r2 = m21406c(r10, r3)
            int r2 = r2 + -1
            int r3 = r3 + 2
            r6 = r0
            r7 = r3
            goto L_0x032a
        L_0x0360:
            if (r1 == 0) goto L_0x036e
            r5 = 52
            int r0 = r7 + 6
            int r8 = r0 + 1
            byte r0 = r10[r0]
            r6 = r0
            r4 = r7
            goto L_0x000d
        L_0x036e:
            r0 = 52
            m21391a((org.mozilla.javascript.regexp.C6193aiJ) r12, (byte) r0, (int) r7)
            m21385a((org.mozilla.javascript.regexp.C6193aiJ) r12)
            int r0 = r7 + 4
            int r1 = m21406c(r10, r0)
            int r0 = r0 + r1
            int r8 = r0 + 1
            byte r0 = r10[r0]
            r6 = r0
            goto L_0x000d
        L_0x0384:
            r6 = r5
            r8 = r4
            goto L_0x000d
        L_0x0388:
            a.asW r5 = m21385a((org.mozilla.javascript.regexp.C6193aiJ) r12)
            if (r9 != 0) goto L_0x03a2
            int r0 = r5.min
            if (r0 != 0) goto L_0x04c4
            r0 = 1
        L_0x0393:
            int r4 = r5.erd
            int r5 = r5.erc
            int r1 = r8 + 4
            int r2 = m21406c(r10, r1)
            int r8 = r1 + r2
            r9 = r0
            goto L_0x0016
        L_0x03a2:
            int r0 = r5.min
            if (r0 != 0) goto L_0x03bb
            int r0 = r12.f4604cp
            int r1 = r5.index
            if (r0 != r1) goto L_0x03bb
            r9 = 0
            int r4 = r5.erd
            int r5 = r5.erc
            int r0 = r8 + 4
            int r1 = m21406c(r10, r0)
            int r8 = r0 + r1
            goto L_0x0016
        L_0x03bb:
            int r1 = r5.min
            int r2 = r5.max
            if (r1 == 0) goto L_0x03c3
            int r1 = r1 + -1
        L_0x03c3:
            r0 = -1
            if (r2 == r0) goto L_0x03c8
            int r2 = r2 + -1
        L_0x03c8:
            if (r2 != 0) goto L_0x03d9
            r9 = 1
            int r4 = r5.erd
            int r5 = r5.erc
            int r0 = r8 + 4
            int r1 = m21406c(r10, r0)
            int r8 = r0 + r1
            goto L_0x0016
        L_0x03d9:
            r3 = 0
            int r4 = r5.erd
            int r5 = r5.erc
            r0 = r12
            m21392a((org.mozilla.javascript.regexp.C6193aiJ) r0, (int) r1, (int) r2, (org.mozilla.javascript.regexp.C1397UU) r3, (int) r4, (int) r5)
            r5 = 51
            r0 = 51
            m21391a((org.mozilla.javascript.regexp.C6193aiJ) r12, (byte) r0, (int) r8)
            int r3 = m21408d((byte[]) r10, (int) r8)
            int r0 = r8 + 2
            int r4 = m21408d((byte[]) r10, (int) r0)
            int r0 = r0 + 4
            int r2 = r0 + 1
            byte r1 = r10[r0]
            r0 = 0
        L_0x03fa:
            if (r0 < r3) goto L_0x0401
            r6 = r1
            r4 = r8
            r8 = r2
            goto L_0x000d
        L_0x0401:
            int r6 = r4 + r0
            r7 = -1
            r11 = 0
            r12.mo13664t(r6, r7, r11)
            int r0 = r0 + 1
            goto L_0x03fa
        L_0x040b:
            a.asW r6 = m21385a((org.mozilla.javascript.regexp.C6193aiJ) r12)
            if (r9 != 0) goto L_0x0451
            int r0 = r6.max
            r1 = -1
            if (r0 == r1) goto L_0x041a
            int r0 = r6.max
            if (r0 <= 0) goto L_0x044b
        L_0x041a:
            int r1 = r6.min
            int r2 = r6.max
            r3 = 0
            int r4 = r6.erd
            int r5 = r6.erc
            r0 = r12
            m21392a((org.mozilla.javascript.regexp.C6193aiJ) r0, (int) r1, (int) r2, (org.mozilla.javascript.regexp.C1397UU) r3, (int) r4, (int) r5)
            r5 = 52
            int r1 = m21408d((byte[]) r10, (int) r8)
            int r0 = r8 + 2
            int r2 = m21408d((byte[]) r10, (int) r0)
            int r3 = r0 + 4
            r0 = 0
        L_0x0436:
            if (r0 < r1) goto L_0x0441
            int r1 = r3 + 1
            byte r0 = r10[r3]
            r6 = r0
            r4 = r8
            r8 = r1
            goto L_0x000d
        L_0x0441:
            int r4 = r2 + r0
            r6 = -1
            r7 = 0
            r12.mo13664t(r4, r6, r7)
            int r0 = r0 + 1
            goto L_0x0436
        L_0x044b:
            int r4 = r6.erd
            int r5 = r6.erc
            goto L_0x0016
        L_0x0451:
            int r0 = r6.min
            if (r0 != 0) goto L_0x0462
            int r0 = r12.f4604cp
            int r1 = r6.index
            if (r0 != r1) goto L_0x0462
            r9 = 0
            int r4 = r6.erd
            int r5 = r6.erc
            goto L_0x0016
        L_0x0462:
            int r1 = r6.min
            int r2 = r6.max
            if (r1 == 0) goto L_0x046a
            int r1 = r1 + -1
        L_0x046a:
            r0 = -1
            if (r2 == r0) goto L_0x046f
            int r2 = r2 + -1
        L_0x046f:
            r3 = 0
            int r4 = r6.erd
            int r5 = r6.erc
            r0 = r12
            m21392a((org.mozilla.javascript.regexp.C6193aiJ) r0, (int) r1, (int) r2, (org.mozilla.javascript.regexp.C1397UU) r3, (int) r4, (int) r5)
            if (r1 == 0) goto L_0x049e
            r5 = 52
            int r1 = m21408d((byte[]) r10, (int) r8)
            int r0 = r8 + 2
            int r2 = m21408d((byte[]) r10, (int) r0)
            int r3 = r0 + 4
            r0 = 0
        L_0x0489:
            if (r0 < r1) goto L_0x0494
            int r1 = r3 + 1
            byte r0 = r10[r3]
            r6 = r0
            r4 = r8
            r8 = r1
            goto L_0x000d
        L_0x0494:
            int r4 = r2 + r0
            r6 = -1
            r7 = 0
            r12.mo13664t(r4, r6, r7)
            int r0 = r0 + 1
            goto L_0x0489
        L_0x049e:
            int r4 = r6.erd
            int r5 = r6.erc
            r0 = 52
            m21391a((org.mozilla.javascript.regexp.C6193aiJ) r12, (byte) r0, (int) r8)
            m21385a((org.mozilla.javascript.regexp.C6193aiJ) r12)
            int r0 = r8 + 4
            int r1 = m21406c(r10, r0)
            int r0 = r0 + r1
            int r8 = r0 + 1
            byte r0 = r10[r0]
            r6 = r0
            goto L_0x000d
        L_0x04b8:
            r0 = 1
        L_0x04b9:
            return r0
        L_0x04ba:
            r0 = 0
            goto L_0x04b9
        L_0x04bc:
            int r1 = r8 + 1
            byte r0 = r10[r8]
            r6 = r0
            r8 = r1
            goto L_0x000d
        L_0x04c4:
            r0 = r9
            goto L_0x0393
        L_0x04c7:
            r8 = r0
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: org.mozilla.javascript.regexp.C6025aex.m21401a(a.aiJ, char[], int):boolean");
    }

    /* renamed from: a */
    private static boolean m21399a(org.mozilla1.javascript.regexp.REGlobalData aij, org.mozilla1.javascript.regexp.RECompiled sa, char[] cArr, int i, int i2, boolean z) {
        if (sa.dPM != 0) {
            aij.erf = new long[sa.dPM];
        } else {
            aij.erf = null;
        }
        aij.fOz = null;
        aij.erg = null;
        aij.fOw = z;
        aij.fOx = sa;
        aij.ere = 0;
        int i3 = aij.fOx.ecw;
        int i4 = i;
        while (i4 <= i2) {
            if (i3 >= 0) {
                while (i4 != i2) {
                    char c = cArr[i4];
                    if (c != i3 && ((aij.fOx.flags & 2) == 0 || m21414f(c) != m21414f((char) i3))) {
                        i4++;
                    }
                }
                return false;
            }
            aij.f4604cp = i4;
            for (int i5 = 0; i5 < sa.dPM; i5++) {
                aij.mo13664t(i5, -1, 0);
            }
            boolean a = m21401a(aij, cArr, i2);
            aij.fOz = null;
            aij.erg = null;
            if (a) {
                aij.fOy = i4 - i;
                return true;
            }
            i4++;
        }
        return false;
    }

    /* renamed from: a */
    private static void m21394a(org.mozilla1.javascript.Context lhVar, String str, String str2) {
        if (lhVar.hasFeature(11)) {
            org.mozilla1.javascript.Context.reportWarning(org.mozilla1.javascript.ScriptRuntime.getMessage1(str, str2));
        }
    }

    /* renamed from: ao */
    private static void m21402ao(String str, String str2) {
        throw org.mozilla1.javascript.ScriptRuntime.m7535aY("SyntaxError", org.mozilla1.javascript.ScriptRuntime.getMessage1(str, str2));
    }

    /* renamed from: d */
    private static NativeRegExp m21410d(org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.IdFunctionObject yy) {
        if (avf instanceof NativeRegExp) {
            return (NativeRegExp) avf;
        }
        throw m25341f(yy);
    }

    public String getClassName() {
        return "RegExp";
    }

    public Object call(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        return m21386a(lhVar, avf, objArr, 1);
    }

    public org.mozilla1.javascript.Scriptable construct(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr) {
        return (org.mozilla1.javascript.Scriptable) m21386a(lhVar, avf, objArr, 1);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public org.mozilla1.javascript.Scriptable mo13195c(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr) {
        String str;
        if (objArr.length <= 0 || !(objArr[0] instanceof NativeRegExp)) {
            String nh = objArr.length == 0 ? "" : org.mozilla1.javascript.ScriptRuntime.toString(objArr[0]);
            if (objArr.length <= 1 || objArr[1] == org.mozilla1.javascript.Undefined.instance) {
                str = null;
            } else {
                str = org.mozilla1.javascript.ScriptRuntime.toString(objArr[1]);
            }
            this.fpv = (org.mozilla1.javascript.regexp.RECompiled) m21387a(lhVar, nh, str, false);
            this.fpw = org.mozilla1.javascript.ScriptRuntime.NaN;
        } else if (objArr.length <= 1 || objArr[1] == org.mozilla1.javascript.Undefined.instance) {
            NativeRegExp aex = objArr[0];
            this.fpv = aex.fpv;
            this.fpw = aex.fpw;
        } else {
            throw org.mozilla1.javascript.ScriptRuntime.m7600no("msg.bad.regexp.compile");
        }
        return this;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(C0147Bi.cla);
        if (this.fpv.ect.length != 0) {
            stringBuffer.append(this.fpv.ect);
        } else {
            stringBuffer.append("(?:)");
        }
        stringBuffer.append(C0147Bi.cla);
        if ((this.fpv.flags & 1) != 0) {
            stringBuffer.append('g');
        }
        if ((this.fpv.flags & 2) != 0) {
            stringBuffer.append('i');
        }
        if ((this.fpv.flags & 4) != 0) {
            stringBuffer.append('m');
        }
        return stringBuffer.toString();
    }

    /* renamed from: a */
    private Object m21386a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr, int i) {
        String nh;
        RegExpImpl d = m21409d(lhVar);
        if (objArr.length == 0) {
            nh = d.iAM;
            if (nh == null) {
                m21402ao("msg.no.re.input.for", toString());
            }
        } else {
            nh = org.mozilla1.javascript.ScriptRuntime.toString(objArr[0]);
        }
        double d2 = (this.fpv.flags & 1) != 0 ? this.fpw : org.mozilla1.javascript.ScriptRuntime.NaN;
        if (d2 < org.mozilla1.javascript.ScriptRuntime.NaN || ((double) nh.length()) < d2) {
            this.fpw = org.mozilla1.javascript.ScriptRuntime.NaN;
            return null;
        }
        int[] iArr = {(int) d2};
        Object a = mo13194a(lhVar, avf, d, nh, iArr, i);
        if ((this.fpv.flags & 1) != 0) {
            this.fpw = (double) ((a == null || a == org.mozilla1.javascript.Undefined.instance) ? 0 : iArr[0]);
        }
        return a;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Object mo13194a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, RegExpImpl apd, String str, int[] iArr, int i) {
        org.mozilla1.javascript.Scriptable avf2;
        Object obj;
        org.mozilla1.javascript.regexp.REGlobalData aij = new org.mozilla1.javascript.regexp.REGlobalData();
        int i2 = iArr[0];
        char[] charArray = str.toCharArray();
        int length = charArray.length;
        if (i2 > length) {
            i2 = length;
        }
        if (m21399a(aij, this.fpv, charArray, i2, length, apd.fOw)) {
            int i3 = aij.f4604cp;
            iArr[0] = i3;
            int i4 = i3 - (aij.fOy + i2);
            int i5 = i3 - i4;
            if (i == 0) {
                avf2 = null;
                obj = Boolean.TRUE;
            } else {
                Object b = org.mozilla1.javascript.ScriptRuntime.m7539b(lhVar, org.mozilla1.javascript.ScriptableObject.m23595x(avf), "Array", (Object[]) null);
                avf2 = (org.mozilla1.javascript.Scriptable) b;
                avf2.put(0, avf2, (Object) new String(charArray, i5, i4));
                obj = b;
            }
            if (this.fpv.dPM == 0) {
                apd.iAN = null;
                apd.iAP = SubString.bmi;
            } else {
                SubString tqVar = null;
                apd.iAN = new SubString[this.fpv.dPM];
                for (int i6 = 0; i6 < this.fpv.dPM; i6++) {
                    int rj = aij.mo13662rj(i6);
                    if (rj != -1) {
                        tqVar = new SubString(charArray, rj, aij.mo13663rk(i6));
                        apd.iAN[i6] = tqVar;
                        if (i != 0) {
                            avf2.put(i6 + 1, avf2, (Object) tqVar.toString());
                        }
                    } else if (i != 0) {
                        avf2.put(i6 + 1, avf2, org.mozilla1.javascript.Undefined.instance);
                    }
                }
                apd.iAP = tqVar;
            }
            if (i != 0) {
                avf2.put("index", avf2, (Object) new Integer(aij.fOy + i2));
                avf2.put("input", avf2, (Object) str);
            }
            if (apd.iAO == null) {
                apd.iAO = new SubString();
                apd.iAQ = new SubString();
                apd.iAR = new SubString();
            }
            apd.iAO.bmj = charArray;
            apd.iAO.index = i5;
            apd.iAO.length = i4;
            apd.iAQ.bmj = charArray;
            if (lhVar.getLanguageVersion() == 120) {
                apd.iAQ.index = i2;
                apd.iAQ.length = aij.fOy;
            } else {
                apd.iAQ.index = 0;
                apd.iAQ.length = aij.fOy + i2;
            }
            apd.iAR.bmj = charArray;
            apd.iAR.index = i3;
            apd.iAR.length = length - i3;
            return obj;
        } else if (i != 2) {
            return null;
        } else {
            return org.mozilla1.javascript.Undefined.instance;
        }
    }

    /* access modifiers changed from: package-private */
    public int getFlags() {
        return this.fpv.flags;
    }

    /* access modifiers changed from: protected */
    public int getMaxInstanceId() {
        return 5;
    }

    /* access modifiers changed from: protected */
    public int findInstanceIdInfo(String str) {
        int i;
        int i2 = 6;
        int i3 = 0;
        String str2 = null;
        int length = str.length();
        if (length == 6) {
            char charAt = str.charAt(0);
            if (charAt == 'g') {
                str2 = "global";
                i = 3;
            } else {
                if (charAt == 's') {
                    str2 = "source";
                    i = 2;
                }
                i = 0;
            }
        } else if (length == 9) {
            char charAt2 = str.charAt(0);
            if (charAt2 == 'l') {
                str2 = "lastIndex";
                i = 1;
            } else {
                if (charAt2 == 'm') {
                    str2 = "multiline";
                    i = 5;
                }
                i = 0;
            }
        } else {
            if (length == 10) {
                str2 = "ignoreCase";
                i = 4;
            }
            i = 0;
        }
        if (str2 == null || str2 == str || str2.equals(str)) {
            i3 = i;
        }
        if (i3 == 0) {
            return super.findInstanceIdInfo(str);
        }
        switch (i3) {
            case 1:
                break;
            case 2:
            case 3:
            case 4:
            case 5:
                i2 = 7;
                break;
            default:
                throw new IllegalStateException();
        }
        return instanceIdInfo(i2, i3);
    }

    /* access modifiers changed from: protected */
    public String getInstanceIdName(int i) {
        switch (i) {
            case 1:
                return "lastIndex";
            case 2:
                return "source";
            case 3:
                return "global";
            case 4:
                return "ignoreCase";
            case 5:
                return "multiline";
            default:
                return super.getInstanceIdName(i);
        }
    }

    /* access modifiers changed from: protected */
    public Object getInstanceIdValue(int i) {
        boolean z = true;
        switch (i) {
            case 1:
                return org.mozilla1.javascript.ScriptRuntime.wrapNumber(this.fpw);
            case 2:
                return new String(this.fpv.ect);
            case 3:
                if ((this.fpv.flags & 1) == 0) {
                    z = false;
                }
                return org.mozilla1.javascript.ScriptRuntime.wrapBoolean(z);
            case 4:
                if ((this.fpv.flags & 2) == 0) {
                    z = false;
                }
                return org.mozilla1.javascript.ScriptRuntime.wrapBoolean(z);
            case 5:
                if ((this.fpv.flags & 4) == 0) {
                    z = false;
                }
                return org.mozilla1.javascript.ScriptRuntime.wrapBoolean(z);
            default:
                return super.getInstanceIdValue(i);
        }
    }

    /* access modifiers changed from: protected */
    public void setInstanceIdValue(int i, Object obj) {
        if (i == 1) {
            this.fpw = ScriptRuntime.toNumber(obj);
        } else {
            super.setInstanceIdValue(i, obj);
        }
    }

    /* access modifiers changed from: protected */
    public void initPrototypeId(int i) {
        String str;
        int i2 = 1;
        switch (i) {
            case 1:
                str = "compile";
                break;
            case 2:
                str = "toString";
                i2 = 0;
                break;
            case 3:
                str = "toSource";
                i2 = 0;
                break;
            case 4:
                str = "exec";
                break;
            case 5:
                str = "test";
                break;
            case 6:
                str = "prefix";
                break;
            default:
                throw new IllegalArgumentException(String.valueOf(i));
        }
        initPrototypeMethod(fow, i, str, i2);
    }

    public Object execIdCall(org.mozilla1.javascript.IdFunctionObject yy, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        if (!yy.hasTag(fow)) {
            return super.execIdCall(yy, lhVar, avf, avf2, objArr);
        }
        int methodId = yy.methodId();
        switch (methodId) {
            case 1:
                return m21410d(avf2, yy).mo13195c(lhVar, avf, objArr);
            case 2:
            case 3:
                return m21410d(avf2, yy).toString();
            case 4:
                return m21410d(avf2, yy).m21386a(lhVar, avf, objArr, 1);
            case 5:
                return Boolean.TRUE.equals(m21410d(avf2, yy).m21386a(lhVar, avf, objArr, 0)) ? Boolean.TRUE : Boolean.FALSE;
            case 6:
                return m21410d(avf2, yy).m21386a(lhVar, avf, objArr, 2);
            default:
                throw new IllegalArgumentException(String.valueOf(methodId));
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int findPrototypeId(java.lang.String r7) {
        /*
            r6 = this;
            r5 = 116(0x74, float:1.63E-43)
            r1 = 3
            r0 = 0
            r2 = 0
            int r3 = r7.length()
            switch(r3) {
                case 4: goto L_0x0018;
                case 5: goto L_0x000c;
                case 6: goto L_0x002a;
                case 7: goto L_0x002e;
                case 8: goto L_0x0032;
                default: goto L_0x000c;
            }
        L_0x000c:
            r1 = r0
        L_0x000d:
            if (r2 == 0) goto L_0x0043
            if (r2 == r7) goto L_0x0043
            boolean r2 = r2.equals(r7)
            if (r2 != 0) goto L_0x0043
        L_0x0017:
            return r0
        L_0x0018:
            char r1 = r7.charAt(r0)
            r3 = 101(0x65, float:1.42E-43)
            if (r1 != r3) goto L_0x0024
            java.lang.String r2 = "exec"
            r1 = 4
            goto L_0x000d
        L_0x0024:
            if (r1 != r5) goto L_0x000c
            java.lang.String r2 = "test"
            r1 = 5
            goto L_0x000d
        L_0x002a:
            java.lang.String r2 = "prefix"
            r1 = 6
            goto L_0x000d
        L_0x002e:
            java.lang.String r2 = "compile"
            r1 = 1
            goto L_0x000d
        L_0x0032:
            char r3 = r7.charAt(r1)
            r4 = 111(0x6f, float:1.56E-43)
            if (r3 != r4) goto L_0x003d
            java.lang.String r2 = "toSource"
            goto L_0x000d
        L_0x003d:
            if (r3 != r5) goto L_0x000c
            java.lang.String r2 = "toString"
            r1 = 2
            goto L_0x000d
        L_0x0043:
            r0 = r1
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: org.mozilla.javascript.regexp.C6025aex.findPrototypeId(java.lang.String):int");
    }
}
