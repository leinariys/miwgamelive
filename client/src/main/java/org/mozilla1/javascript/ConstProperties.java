package org.mozilla1.javascript;

/* renamed from: a.EH */
/* compiled from: a */
public interface ConstProperties {
    /* renamed from: a */
    void putConst(String str, Scriptable avf, Object obj);

    /* renamed from: b */
    void defineConst(String str, Scriptable avf);

    /* renamed from: em */
    boolean isConst(String str);
}
