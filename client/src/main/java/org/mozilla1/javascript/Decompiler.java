package org.mozilla1.javascript;


/* renamed from: a.aRu  reason: case insensitive filesystem */
/* compiled from: a */
public class Decompiler {
    public static final int CASE_GAP_PROP = 3;
    public static final int INDENT_GAP_PROP = 2;
    public static final int INITIAL_INDENT_PROP = 1;
    public static final int ONLY_BODY_FLAG = 1;
    public static final int TO_SOURCE_FLAG = 2;
    private static final int iJa = 160;
    private static final boolean iJc = false;

    /* renamed from: iA */
    private char[] f3727iA = new char[128];
    private int iJb;

    /* renamed from: a */
    public static String m18107a(String str, int i, UintMap akl) {
        boolean z;
        char charAt;
        int i2 = 0;
        boolean z2;
        int i3;
        int length = str.length();
        if (length == 0) {
            return "";
        }
        int i4 = akl.getInt(1, 0);
        if (i4 < 0) {
            throw new IllegalArgumentException();
        }
        int i5 = akl.getInt(2, 4);
        if (i5 < 0) {
            throw new IllegalArgumentException();
        }
        int i6 = akl.getInt(3, 2);
        if (i6 < 0) {
            throw new IllegalArgumentException();
        }
        StringBuffer stringBuffer = new StringBuffer();
        boolean z3 = (i & 1) != 0;
        if ((i & 2) != 0) {
            z = true;
        } else {
            z = false;
        }
        int i7 = 0;
        boolean z4 = false;
        int i8 = 0;
        if (str.charAt(0) == 135) {
            i8 = 1;
            charAt = 65535;
        } else {
            charAt = str.charAt(1);
        }
        if (!z) {
            stringBuffer.append(10);
            for (int i9 = 0; i9 < i4; i9++) {
                stringBuffer.append(' ');
            }
        } else if (charAt == 2) {
            stringBuffer.append('(');
        }
        while (i2 < length) {
            switch (str.charAt(i2)) {
                case 1:
                    if (!z) {
                        boolean z5 = true;
                        if (!z4) {
                            z2 = true;
                            if (z3) {
                                stringBuffer.setLength(0);
                                i4 -= i5;
                                z5 = false;
                            }
                        } else {
                            z2 = z4;
                        }
                        if (z5) {
                            stringBuffer.append(10);
                        }
                        if (i2 + 1 >= length) {
                            z4 = z2;
                            break;
                        } else {
                            int i10 = 0;
                            char charAt2 = str.charAt(i2 + 1);
                            if (charAt2 == 'r' || charAt2 == 's') {
                                i10 = i5 - i6;
                            } else if (charAt2 == 'U') {
                                i10 = i5;
                            } else if (charAt2 == '\'' && str.charAt(m18104B(str, i2 + 2)) == 'f') {
                                i10 = i5;
                            }
                            while (i10 < i4) {
                                stringBuffer.append(' ');
                                i10++;
                            }
                            z4 = z2;
                            break;
                        }
                    }
                    break;
                case 4:
                    stringBuffer.append("return");
                    if (81 != m18108i(str, length, i2)) {
                        stringBuffer.append(' ');
                        break;
                    }
                    break;
                case 9:
                    stringBuffer.append(" | ");
                    break;
                case 10:
                    stringBuffer.append(" ^ ");
                    break;
                case 11:
                    stringBuffer.append(" & ");
                    break;
                case 12:
                    stringBuffer.append(" == ");
                    break;
                case 13:
                    stringBuffer.append(" != ");
                    break;
                case 14:
                    stringBuffer.append(" < ");
                    break;
                case 15:
                    stringBuffer.append(" <= ");
                    break;
                case 16:
                    stringBuffer.append(" > ");
                    break;
                case 17:
                    stringBuffer.append(" >= ");
                    break;
                case 18:
                    stringBuffer.append(" << ");
                    break;
                case 19:
                    stringBuffer.append(" >> ");
                    break;
                case 20:
                    stringBuffer.append(" >>> ");
                    break;
                case 21:
                    stringBuffer.append(" + ");
                    break;
                case 22:
                    stringBuffer.append(" - ");
                    break;
                case 23:
                    stringBuffer.append(" * ");
                    break;
                case 24:
                    stringBuffer.append(" / ");
                    break;
                case 25:
                    stringBuffer.append(" % ");
                    break;
                case 26:
                    stringBuffer.append('!');
                    break;
                case 27:
                    stringBuffer.append('~');
                    break;
                case 28:
                    stringBuffer.append('+');
                    break;
                case 29:
                    stringBuffer.append('-');
                    break;
                case 30:
                    stringBuffer.append("new ");
                    break;
                case 31:
                    stringBuffer.append("delete ");
                    break;
                case ' ':
                    stringBuffer.append("typeof ");
                    break;
                case '\'':
                case '0':
                    i2 = m18106a(str, i2 + 1, false, stringBuffer);
                    continue;
                case '(':
                    i2 = m18105a(str, i2 + 1, stringBuffer);
                    continue;
                case ')':
                    i2 = m18106a(str, i2 + 1, true, stringBuffer);
                    continue;
                case '*':
                    stringBuffer.append("null");
                    break;
                case '+':
                    stringBuffer.append("this");
                    break;
                case ',':
                    stringBuffer.append("false");
                    break;
                case '-':
                    stringBuffer.append("true");
                    break;
                case '.':
                    stringBuffer.append(" === ");
                    break;
                case '/':
                    stringBuffer.append(" !== ");
                    break;
                case '2':
                    stringBuffer.append("throw ");
                    break;
                case '4':
                    stringBuffer.append(" in ");
                    break;
                case '5':
                    stringBuffer.append(" instanceof ");
                    break;
                case 'B':
                    stringBuffer.append(':');
                    break;
                case 'H':
                    stringBuffer.append("yield ");
                    break;
                case 'P':
                    stringBuffer.append("try ");
                    break;
                case 'Q':
                    stringBuffer.append(';');
                    if (1 != m18108i(str, length, i2)) {
                        stringBuffer.append(' ');
                        break;
                    }
                    break;
                case 'R':
                    stringBuffer.append('[');
                    break;
                case 'S':
                    stringBuffer.append(']');
                    break;
                case 'T':
                    i7++;
                    if (1 == m18108i(str, length, i2)) {
                        i3 = i4 + i5;
                    } else {
                        i3 = i4;
                    }
                    stringBuffer.append('{');
                    i4 = i3;
                    break;
                case 'U':
                    int i11 = i7 - 1;
                    if (z3 && i11 == 0) {
                        i7 = i11;
                        break;
                    } else {
                        stringBuffer.append('}');
                        switch (m18108i(str, length, i2)) {
                            case 1:
                            case 160:
                                i4 -= i5;
                                i7 = i11;
                                break;
                            case 112:
                            case 116:
                                i4 -= i5;
                                stringBuffer.append(' ');
                                break;
                        }
                        i7 = i11;
                        break;
                    }
                case 'V':
                    stringBuffer.append('(');
                    break;
                case 'W':
                    stringBuffer.append(')');
                    if (84 == m18108i(str, length, i2)) {
                        stringBuffer.append(' ');
                        break;
                    }
                    break;
                case 'X':
                    stringBuffer.append(", ");
                    break;
                case 'Y':
                    stringBuffer.append(" = ");
                    break;
                case 'Z':
                    stringBuffer.append(" |= ");
                    break;
                case '[':
                    stringBuffer.append(" ^= ");
                    break;
                case '\\':
                    stringBuffer.append(" &= ");
                    break;
                case ']':
                    stringBuffer.append(" <<= ");
                    break;
                case '^':
                    stringBuffer.append(" >>= ");
                    break;
                case '_':
                    stringBuffer.append(" >>>= ");
                    break;
                case '`':
                    stringBuffer.append(" += ");
                    break;
                case 'a':
                    stringBuffer.append(" -= ");
                    break;
                case 'b':
                    stringBuffer.append(" *= ");
                    break;
                case 'c':
                    stringBuffer.append(" /= ");
                    break;
                case 'd':
                    stringBuffer.append(" %= ");
                    break;
                case 'e':
                    stringBuffer.append(" ? ");
                    break;
                case 'f':
                    if (1 != m18108i(str, length, i2)) {
                        stringBuffer.append(" : ");
                        break;
                    } else {
                        stringBuffer.append(':');
                        break;
                    }
                case 'g':
                    stringBuffer.append(" || ");
                    break;
                case 'h':
                    stringBuffer.append(" && ");
                    break;
                case 'i':
                    stringBuffer.append("++");
                    break;
                case 'j':
                    stringBuffer.append("--");
                    break;
                case 'k':
                    stringBuffer.append('.');
                    break;
                case 'l':
                    stringBuffer.append("function ");
                    i2++;
                    break;
                case 'o':
                    stringBuffer.append("if ");
                    break;
                case 'p':
                    stringBuffer.append("else ");
                    break;
                case 'q':
                    stringBuffer.append("switch ");
                    break;
                case 'r':
                    stringBuffer.append("case ");
                    break;
                case 's':
                    stringBuffer.append("default");
                    break;
                case 't':
                    stringBuffer.append("while ");
                    break;
                case 'u':
                    stringBuffer.append("do ");
                    break;
                case 'v':
                    stringBuffer.append("for ");
                    break;
                case 'w':
                    stringBuffer.append("break");
                    if (39 == m18108i(str, length, i2)) {
                        stringBuffer.append(' ');
                        break;
                    }
                    break;
                case 'x':
                    stringBuffer.append("continue");
                    if (39 == m18108i(str, length, i2)) {
                        stringBuffer.append(' ');
                        break;
                    }
                    break;
                case 'y':
                    stringBuffer.append("var ");
                    break;
                case 'z':
                    stringBuffer.append("with ");
                    break;
                case '{':
                    stringBuffer.append("catch ");
                    break;
                case '|':
                    stringBuffer.append("finally ");
                    break;
                case '}':
                    stringBuffer.append("void ");
                    break;
                case 142:
                    stringBuffer.append("..");
                    break;
                case 143:
                    stringBuffer.append("::");
                    break;
                case 145:
                    stringBuffer.append(".(");
                    break;
                case 146:
                    stringBuffer.append('@');
                    break;
                case 150:
                case 151:
                    stringBuffer.append(str.charAt(i2) == 150 ? "get " : "set ");
                    i2 = m18106a(str, i2 + 1 + 1, false, stringBuffer) + 1;
                    break;
                case 152:
                    stringBuffer.append("let ");
                    break;
                case 153:
                    stringBuffer.append("const ");
                    break;
                case 160:
                    break;
                default:
                    throw new RuntimeException("Token: " + Token.name(str.charAt(i2)));
            }
            i2++;
        }
        if (!z) {
            if (!z3) {
                stringBuffer.append(10);
            }
        } else if (charAt == 2) {
            stringBuffer.append(')');
        }
        return stringBuffer.toString();
    }

    /* renamed from: i */
    private static int m18108i(String str, int i, int i2) {
        if (i2 + 1 < i) {
            return str.charAt(i2 + 1);
        }
        return 0;
    }

    /* renamed from: B */
    private static int m18104B(String str, int i) {
        return m18106a(str, i, false, (StringBuffer) null);
    }

    /* renamed from: a */
    private static int m18106a(String str, int i, boolean z, StringBuffer stringBuffer) {
        char charAt = str.charAt(i);
        int i2 = i + 1;
        if ((32768 & charAt) != 0) {
            charAt = (char) (((charAt & 32767) << 16) | str.charAt(i2));
            i2++;
        }
        if (stringBuffer != null) {
            String substring = str.substring(i2, i2 + charAt);
            if (!z) {
                stringBuffer.append(substring);
            } else {
                stringBuffer.append('\"');
                stringBuffer.append(ScriptRuntime.escapeString(substring));
                stringBuffer.append('\"');
            }
        }
        return charAt + i2;
    }

    /* renamed from: a */
    private static int m18105a(String str, int i, StringBuffer stringBuffer) {
        int i2;
        double d = ScriptRuntime.NaN;
        char charAt = str.charAt(i);
        int i3 = i + 1;
        if (charAt == 'S') {
            if (stringBuffer != null) {
                d = (double) str.charAt(i3);
            }
            i2 = i3 + 1;
        } else if (charAt == 'J' || charAt == 'D') {
            if (stringBuffer != null) {
                long charAt2 = (((long) str.charAt(i3)) << 48) | (((long) str.charAt(i3 + 1)) << 32) | (((long) str.charAt(i3 + 2)) << 16) | ((long) str.charAt(i3 + 3));
                if (charAt == 'J') {
                    d = (double) charAt2;
                } else {
                    d = Double.longBitsToDouble(charAt2);
                }
            }
            i2 = i3 + 4;
        } else {
            throw new RuntimeException();
        }
        if (stringBuffer != null) {
            stringBuffer.append(ScriptRuntime.numberToString(d, 10));
        }
        return i2;
    }

    /* access modifiers changed from: package-private */
    public String getEncodedSource() {
        return m18103Ay(0);
    }

    /* access modifiers changed from: package-private */
    public int dte() {
        return this.iJb;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: Au */
    public int mo11279Au(int i) {
        int dte = dte();
        addToken(108);
        append((char) i);
        return dte;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: Av */
    public int mo11280Av(int i) {
        int dte = dte();
        append((char) 160);
        return dte;
    }

    /* access modifiers changed from: package-private */
    public void addToken(int i) {
        if (i < 0 || i > 159) {
            throw new IllegalArgumentException();
        }
        append((char) i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: Aw */
    public void mo11281Aw(int i) {
        if (i < 0 || i > 159) {
            throw new IllegalArgumentException();
        }
        append((char) i);
        append((char) 1);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: nw */
    public void mo11287nw(String str) {
        addToken(39);
        m18109ny(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: nx */
    public void mo11288nx(String str) {
        addToken(41);
        m18109ny(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ba */
    public void mo11284ba(String str, String str2) {
        addToken(48);
        m18109ny('/' + str + '/' + str2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ae */
    public void mo11283ae(double d) {
        addToken(40);
        long j = (long) d;
        if (((double) j) != d) {
            long doubleToLongBits = Double.doubleToLongBits(d);
            append('D');
            append((char) ((int) (doubleToLongBits >> 48)));
            append((char) ((int) (doubleToLongBits >> 32)));
            append((char) ((int) (doubleToLongBits >> 16)));
            append((char) ((int) doubleToLongBits));
            return;
        }
        if (j < 0) {
            Kit.codeBug();
        }
        if (j <= 65535) {
            append('S');
            append((char) ((int) j));
            return;
        }
        append('J');
        append((char) ((int) (j >> 48)));
        append((char) ((int) (j >> 32)));
        append((char) ((int) (j >> 16)));
        append((char) ((int) j));
    }

    /* renamed from: ny */
    private void m18109ny(String str) {
        int length = str.length();
        int i = 1;
        if (length >= 32768) {
            i = 2;
        }
        int i2 = i + this.iJb + length;
        if (i2 > this.f3727iA.length) {
            m18102Ax(i2);
        }
        if (length >= 32768) {
            this.f3727iA[this.iJb] = (char) ((length >>> 16) | 32768);
            this.iJb++;
        }
        this.f3727iA[this.iJb] = (char) length;
        this.iJb++;
        str.getChars(0, length, this.f3727iA, this.iJb);
        this.iJb = i2;
    }

    private void append(char c) {
        if (this.iJb == this.f3727iA.length) {
            m18102Ax(this.iJb + 1);
        }
        this.f3727iA[this.iJb] = c;
        this.iJb++;
    }

    /* renamed from: Ax */
    private void m18102Ax(int i) {
        if (i <= this.f3727iA.length) {
            Kit.codeBug();
        }
        int length = this.f3727iA.length * 2;
        if (length >= i) {
            i = length;
        }
        char[] cArr = new char[i];
        System.arraycopy(this.f3727iA, 0, cArr, 0, this.iJb);
        this.f3727iA = cArr;
    }

    /* renamed from: Ay */
    private String m18103Ay(int i) {
        if (i < 0 || this.iJb < i) {
            Kit.codeBug();
        }
        return new String(this.f3727iA, i, this.iJb - i);
    }
}
