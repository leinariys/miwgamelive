package org.mozilla1.javascript;

/* renamed from: a.auo  reason: case insensitive filesystem */
/* compiled from: a */
public interface GeneratedClassLoader {
    Class<?> defineClass(String str, byte[] bArr);

    void linkClass(Class<?> cls);
}
