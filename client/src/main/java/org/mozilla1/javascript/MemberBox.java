package org.mozilla1.javascript;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.*;

/* renamed from: a.amt  reason: case insensitive filesystem */
/* compiled from: a */
final class MemberBox implements Serializable {
    static final long serialVersionUID = 6358550398665688245L;
    private static final Class<?>[] gaP = {Boolean.TYPE, Byte.TYPE, Character.TYPE, Double.TYPE, Float.TYPE, Integer.TYPE, Long.TYPE, Short.TYPE, Void.TYPE};
    transient Class<?>[] gaM;
    transient Object gaN;
    transient boolean gaO;
    private transient Member gaL;

    MemberBox(Method method) {
        m24017e(method);
    }

    MemberBox(Constructor<?> constructor) {
        m24014a(constructor);
    }

    /* renamed from: b */
    private static Method m24015b(Method method, Class<?>[] clsArr) {
        int modifiers = method.getModifiers();
        if (Modifier.isPublic(modifiers) && !Modifier.isStatic(modifiers)) {
            Class declaringClass = method.getDeclaringClass();
            if (!Modifier.isPublic(declaringClass.getModifiers())) {
                String name = method.getName();
                Class[] interfaces = declaringClass.getInterfaces();
                int i = 0;
                int length = interfaces.length;
                while (i != length) {
                    Class cls = interfaces[i];
                    if (Modifier.isPublic(cls.getModifiers())) {
                        try {
                            return cls.getMethod(name, clsArr);
                        } catch (NoSuchMethodException | SecurityException e) {
                        }
                    } else {
                        i++;
                    }
                }
                while (true) {
                    declaringClass = declaringClass.getSuperclass();
                    if (declaringClass == null) {
                        break;
                    } else if (Modifier.isPublic(declaringClass.getModifiers())) {
                        try {
                            Method method2 = declaringClass.getMethod(name, clsArr);
                            int modifiers2 = method2.getModifiers();
                            if (Modifier.isPublic(modifiers2) && !Modifier.isStatic(modifiers2)) {
                                return method2;
                            }
                        } catch (NoSuchMethodException | SecurityException e2) {
                        }
                    }
                }
            }
        }
        return null;
    }

    /* renamed from: a */
    private static void m24012a(ObjectOutputStream objectOutputStream, Member member) {
        if (member == null) {
            objectOutputStream.writeBoolean(false);
            return;
        }
        objectOutputStream.writeBoolean(true);
        if ((member instanceof Method) || (member instanceof Constructor)) {
            objectOutputStream.writeBoolean(member instanceof Method);
            objectOutputStream.writeObject(member.getName());
            objectOutputStream.writeObject(member.getDeclaringClass());
            if (member instanceof Method) {
                m24013a(objectOutputStream, (Class<?>[]) ((Method) member).getParameterTypes());
            } else {
                m24013a(objectOutputStream, (Class<?>[]) ((Constructor) member).getParameterTypes());
            }
        } else {
            throw new IllegalArgumentException("not Method or Constructor");
        }
    }

    /* renamed from: a */
    private static Member m24011a(ObjectInputStream objectInputStream) {
        if (!objectInputStream.readBoolean()) {
            return null;
        }
        boolean readBoolean = objectInputStream.readBoolean();
        String str = (String) objectInputStream.readObject();
        Class cls = (Class) objectInputStream.readObject();
        Class[] b = m24016b(objectInputStream);
        if (!readBoolean) {
            return cls.getConstructor(b);
        }
        try {
            return cls.getMethod(str, b);
        } catch (NoSuchMethodException e) {
            throw new IOException("Cannot find member: " + e);
        }
    }

    /* renamed from: a */
    private static void m24013a(ObjectOutputStream objectOutputStream, Class<?>[] clsArr) {
        objectOutputStream.writeShort(clsArr.length);
        for (Class<?> cls : clsArr) {
            boolean isPrimitive = cls.isPrimitive();
            objectOutputStream.writeBoolean(isPrimitive);
            if (!isPrimitive) {
                objectOutputStream.writeObject(cls);
            } else {
                int i = 0;
                while (i < gaP.length) {
                    if (cls.equals(gaP[i])) {
                        objectOutputStream.writeByte(i);
                    } else {
                        i++;
                    }
                }
                throw new IllegalArgumentException("Primitive " + cls + " not found");
            }
        }
    }

    /* renamed from: b */
    private static Class<?>[] m24016b(ObjectInputStream objectInputStream) {
        Class<?>[] clsArr = new Class[objectInputStream.readShort()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= clsArr.length) {
                return clsArr;
            }
            if (!objectInputStream.readBoolean()) {
                clsArr[i2] = (Class) objectInputStream.readObject();
            } else {
                clsArr[i2] = gaP[objectInputStream.readByte()];
            }
            i = i2 + 1;
        }
    }

    /* renamed from: e */
    private void m24017e(Method method) {
        this.gaL = method;
        this.gaM = method.getParameterTypes();
        this.gaO = org.mozilla1.javascript.VMBridge.iEO.mo2807a(method);
    }

    /* renamed from: a */
    private void m24014a(Constructor<?> constructor) {
        this.gaL = constructor;
        this.gaM = constructor.getParameterTypes();
        this.gaO = org.mozilla1.javascript.VMBridge.iEO.mo2807a(constructor);
    }

    /* access modifiers changed from: package-private */
    public Method cjw() {
        return (Method) this.gaL;
    }

    /* access modifiers changed from: package-private */
    public Constructor<?> cjx() {
        return (Constructor) this.gaL;
    }

    /* access modifiers changed from: package-private */
    public Member cjy() {
        return this.gaL;
    }

    /* access modifiers changed from: package-private */
    public boolean isMethod() {
        return this.gaL instanceof Method;
    }

    /* access modifiers changed from: package-private */
    public boolean cjz() {
        return this.gaL instanceof Constructor;
    }

    /* access modifiers changed from: package-private */
    public boolean isStatic() {
        return Modifier.isStatic(this.gaL.getModifiers());
    }

    /* access modifiers changed from: package-private */
    public String getName() {
        return this.gaL.getName();
    }

    /* access modifiers changed from: package-private */
    public Class<?> getDeclaringClass() {
        return this.gaL.getDeclaringClass();
    }

    /* access modifiers changed from: package-private */
    public String cjA() {
        StringBuffer stringBuffer = new StringBuffer();
        if (isMethod()) {
            Method cjw = cjw();
            stringBuffer.append(cjw.getReturnType());
            stringBuffer.append(' ');
            stringBuffer.append(cjw.getName());
        } else {
            String name = cjx().getDeclaringClass().getName();
            int lastIndexOf = name.lastIndexOf(46);
            if (lastIndexOf >= 0) {
                name = name.substring(lastIndexOf + 1);
            }
            stringBuffer.append(name);
        }
        stringBuffer.append(JavaMembers.liveConnectSignature(this.gaM));
        return stringBuffer.toString();
    }

    public String toString() {
        return this.gaL.toString();
    }

    /* access modifiers changed from: package-private */
    public Object invoke(Object obj, Object[] objArr) {
        Method cjw = cjw();
        try {
            return cjw.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            Method b = m24015b(cjw, this.gaM);
            if (b != null) {
                this.gaL = b;
            } else if (!org.mozilla1.javascript.VMBridge.iEO.tryToMakeAccessible(cjw)) {
                throw Context.throwAsScriptRuntimeEx(e);
            } else {
                b = cjw;
            }
            return b.invoke(obj, objArr);
        } catch (InvocationTargetException e2) {
            e = e2;
            do {
                e = ((InvocationTargetException) e).getTargetException();
            } while (e instanceof InvocationTargetException);
            if (e instanceof ContinuationPending) {
                throw ((ContinuationPending) e);
            }
            throw Context.throwAsScriptRuntimeEx(e);
        } catch (Exception e3) {
            throw Context.throwAsScriptRuntimeEx(e3);
        }
    }

    /* access modifiers changed from: package-private */
    public Object newInstance(Object[] objArr) {
        Constructor<?> cjx = cjx();
        try {
            return cjx.newInstance(objArr);
        } catch (IllegalAccessException e) {
            if (VMBridge.iEO.tryToMakeAccessible(cjx)) {
                return cjx.newInstance(objArr);
            }
            throw Context.throwAsScriptRuntimeEx(e);
        } catch (Exception e2) {
            throw Context.throwAsScriptRuntimeEx(e2);
        }
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        Member a = m24011a(objectInputStream);
        if (a instanceof Method) {
            m24017e((Method) a);
        } else {
            m24014a((Constructor<?>) (Constructor) a);
        }
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        m24012a(objectOutputStream, this.gaL);
    }
}
