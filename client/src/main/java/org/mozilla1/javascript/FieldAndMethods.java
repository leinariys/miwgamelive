package org.mozilla1.javascript;


import java.lang.reflect.Field;

/* renamed from: a.TR */
/* compiled from: a */
class FieldAndMethods extends NativeJavaMethod {
    static final long serialVersionUID = -9222428244284796755L;
    Field field;
    Object javaObject;

    FieldAndMethods(Scriptable avf, org.mozilla1.javascript.MemberBox[] amtArr, Field field2) {
        super(amtArr);
        this.field = field2;
        setParentScope(avf);
        setPrototype(ScriptableObject.m23594w(avf));
    }

    public Object getDefaultValue(Class<?> cls) {
        if (cls == ScriptRuntime.FunctionClass) {
            return this;
        }
        try {
            Object obj = this.field.get(this.javaObject);
            Class<?> type = this.field.getType();
            org.mozilla1.javascript.Context bwA = org.mozilla1.javascript.Context.bwA();
            Object a = bwA.bwy().mo6829a(bwA, this, obj, type);
            if (a instanceof Scriptable) {
                a = ((Scriptable) a).getDefaultValue(cls);
            }
            return a;
        } catch (IllegalAccessException e) {
            throw Context.m35246q("msg.java.internal.private", this.field.getName());
        }
    }
}
