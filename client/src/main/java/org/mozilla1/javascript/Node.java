package org.mozilla1.javascript;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: a.ql */
/* compiled from: a */
public class Node {
    public static final int ATTRIBUTE_FLAG = 2;
    public static final int BOTH = 0;
    public static final int CASEARRAY_PROP = 5;
    public static final int CATCH_SCOPE_PROP = 14;
    public static final int DECR_FLAG = 1;
    public static final int DESCENDANTS_FLAG = 4;
    public static final int DIRECTCALL_PROP = 9;
    public static final int FUNCTION_PROP = 1;
    public static final int INCRDECR_PROP = 13;
    public static final int ISNUMBER_PROP = 8;
    public static final int LABEL_ID_PROP = 15;
    public static final int LAST_PROP = 22;
    public static final int LEFT = 1;
    public static final int LOCAL_BLOCK_PROP = 3;
    public static final int LOCAL_PROP = 2;
    public static final int MEMBER_TYPE_PROP = 16;
    public static final int NAME_PROP = 17;
    public static final int NON_SPECIALCALL = 0;
    public static final int OBJECT_IDS_PROP = 12;
    public static final int POST_FLAG = 2;
    public static final int PROPERTY_FLAG = 1;
    public static final int REGEXP_PROP = 4;
    public static final int RIGHT = 2;
    public static final int SKIP_INDEXES_PROP = 11;
    public static final int SPECIALCALL_EVAL = 1;
    public static final int SPECIALCALL_PROP = 10;
    public static final int SPECIALCALL_WITH = 2;
    public static final int TARGETBLOCK_PROP = 6;
    public static final int VARIABLE_PROP = 7;
    public static final int gZc = 18;
    public static final int gZd = 19;
    public static final int gZe = 20;
    public static final int gZf = 21;
    public static final int gZg = 22;
    static final int gZh = 0;
    static final int gZi = 1;
    static final int gZj = 2;
    static final int gZk = 4;
    static final int gZl = 8;
    /* renamed from: iw */
    public int f8955iw;
    Node gZm;
    int type;
    private Node gZn;
    private Node gZo;
    private PropListItem gZp;

    public Node(int i) {
        this.f8955iw = -1;
        this.type = i;
    }

    public Node(int i, Node qlVar) {
        this.f8955iw = -1;
        this.type = i;
        this.gZo = qlVar;
        this.gZn = qlVar;
        qlVar.gZm = null;
    }

    public Node(int i, Node qlVar, Node qlVar2) {
        this.f8955iw = -1;
        this.type = i;
        this.gZn = qlVar;
        this.gZo = qlVar2;
        qlVar.gZm = qlVar2;
        qlVar2.gZm = null;
    }

    public Node(int i, Node qlVar, Node qlVar2, Node qlVar3) {
        this.f8955iw = -1;
        this.type = i;
        this.gZn = qlVar;
        this.gZo = qlVar3;
        qlVar.gZm = qlVar2;
        qlVar2.gZm = qlVar3;
        qlVar3.gZm = null;
    }

    public Node(int i, int i2) {
        this.f8955iw = -1;
        this.type = i;
        this.f8955iw = i2;
    }

    public Node(int i, Node qlVar, int i2) {
        this(i, qlVar);
        this.f8955iw = i2;
    }

    public Node(int i, Node qlVar, Node qlVar2, int i2) {
        this(i, qlVar, qlVar2);
        this.f8955iw = i2;
    }

    public Node(int i, Node qlVar, Node qlVar2, Node qlVar3, int i2) {
        this(i, qlVar, qlVar2, qlVar3);
        this.f8955iw = i2;
    }

    /* renamed from: W */
    public static Node m37823W(double d) {
        return new C3368b(d);
    }

    /* renamed from: kM */
    public static Node m37829kM(String str) {
        return new C3371e(41, str);
    }

    /* renamed from: e */
    public static Node m37828e(int i, String str) {
        return new C3371e(i, str);
    }

    /* renamed from: wA */
    private static final String m37830wA(int i) {
        return null;
    }

    public static Node cFJ() {
        return new Node(130);
    }

    /* renamed from: a */
    private static void m37824a(org.mozilla1.javascript.ScriptOrFnNode aak, Node qlVar, org.mozilla1.javascript.ObjToIntMap uSVar, int i, StringBuffer stringBuffer) {
    }

    /* renamed from: a */
    private static void m37825a(Node qlVar, org.mozilla1.javascript.ObjToIntMap uSVar) {
    }

    /* renamed from: a */
    private static void m37826a(Node qlVar, org.mozilla1.javascript.ObjToIntMap uSVar, StringBuffer stringBuffer) {
    }

    public int getType() {
        return this.type;
    }

    public void setType(int i) {
        this.type = i;
    }

    public boolean hasChildren() {
        return this.gZn != null;
    }

    public Node cFE() {
        return this.gZn;
    }

    public Node cFF() {
        return this.gZo;
    }

    public Node cFG() {
        return this.gZm;
    }

    /* renamed from: E */
    public Node mo21459E(Node qlVar) {
        if (qlVar == this.gZn) {
            return null;
        }
        Node qlVar2 = this.gZn;
        while (qlVar2.gZm != qlVar) {
            qlVar2 = qlVar2.gZm;
            if (qlVar2 == null) {
                throw new RuntimeException("node is not a child");
            }
        }
        return qlVar2;
    }

    public Node cFH() {
        while (this.gZm != null) {
            this = this.gZm;
        }
        return this;
    }

    /* renamed from: F */
    public void mo21460F(Node qlVar) {
        qlVar.gZm = this.gZn;
        this.gZn = qlVar;
        if (this.gZo == null) {
            this.gZo = qlVar;
        }
    }

    /* renamed from: G */
    public void mo21461G(Node qlVar) {
        qlVar.gZm = null;
        if (this.gZo == null) {
            this.gZo = qlVar;
            this.gZn = qlVar;
            return;
        }
        this.gZo.gZm = qlVar;
        this.gZo = qlVar;
    }

    /* renamed from: H */
    public void mo21462H(Node qlVar) {
        Node cFH = qlVar.cFH();
        cFH.gZm = this.gZn;
        this.gZn = qlVar;
        if (this.gZo == null) {
            this.gZo = cFH;
        }
    }

    /* renamed from: I */
    public void mo21463I(Node qlVar) {
        if (this.gZo != null) {
            this.gZo.gZm = qlVar;
        }
        this.gZo = qlVar.cFH();
        if (this.gZn == null) {
            this.gZn = qlVar;
        }
    }

    /* renamed from: o */
    public void mo21484o(Node qlVar, Node qlVar2) {
        if (qlVar.gZm != null) {
            throw new RuntimeException("newChild had siblings in addChildBefore");
        } else if (this.gZn == qlVar2) {
            qlVar.gZm = this.gZn;
            this.gZn = qlVar;
        } else {
            mo21485p(qlVar, mo21459E(qlVar2));
        }
    }

    /* renamed from: p */
    public void mo21485p(Node qlVar, Node qlVar2) {
        if (qlVar.gZm != null) {
            throw new RuntimeException("newChild had siblings in addChildAfter");
        }
        qlVar.gZm = qlVar2.gZm;
        qlVar2.gZm = qlVar;
        if (this.gZo == qlVar2) {
            this.gZo = qlVar;
        }
    }

    /* renamed from: J */
    public void mo21464J(Node qlVar) {
        Node E = mo21459E(qlVar);
        if (E == null) {
            this.gZn = this.gZn.gZm;
        } else {
            E.gZm = qlVar.gZm;
        }
        if (qlVar == this.gZo) {
            this.gZo = E;
        }
        qlVar.gZm = null;
    }

    /* renamed from: q */
    public void mo21488q(Node qlVar, Node qlVar2) {
        qlVar2.gZm = qlVar.gZm;
        if (qlVar == this.gZn) {
            this.gZn = qlVar2;
        } else {
            mo21459E(qlVar).gZm = qlVar2;
        }
        if (qlVar == this.gZo) {
            this.gZo = qlVar2;
        }
        qlVar.gZm = null;
    }

    /* renamed from: r */
    public void mo21489r(Node qlVar, Node qlVar2) {
        Node qlVar3 = qlVar.gZm;
        qlVar2.gZm = qlVar3.gZm;
        qlVar.gZm = qlVar2;
        if (qlVar3 == this.gZo) {
            this.gZo = qlVar2;
        }
        qlVar3.gZm = null;
    }

    /* renamed from: wB */
    private PropListItem m37831wB(int i) {
        PropListItem fVar = this.gZp;
        while (fVar != null && i != fVar.type) {
            fVar = fVar.ibH;
        }
        return fVar;
    }

    /* renamed from: wC */
    private PropListItem m37832wC(int i) {
        PropListItem wB = m37831wB(i);
        if (wB != null) {
            return wB;
        }
        PropListItem fVar = new PropListItem((PropListItem) null);
        fVar.type = i;
        fVar.ibH = this.gZp;
        this.gZp = fVar;
        return fVar;
    }

    public void removeProp(int i) {
        PropListItem fVar = this.gZp;
        if (fVar != null) {
            PropListItem fVar2 = null;
            while (fVar.type != i) {
                PropListItem fVar3 = fVar.ibH;
                if (fVar3 != null) {
                    fVar2 = fVar;
                    fVar = fVar3;
                } else {
                    return;
                }
            }
            if (fVar2 == null) {
                this.gZp = fVar.ibH;
            } else {
                fVar2.ibH = fVar.ibH;
            }
        }
    }

    public Object getProp(int i) {
        PropListItem wB = m37831wB(i);
        if (wB == null) {
            return null;
        }
        return wB.ibI;
    }

    public int getIntProp(int i, int i2) {
        PropListItem wB = m37831wB(i);
        return wB == null ? i2 : wB.intValue;
    }

    public int getExistingIntProp(int i) {
        PropListItem wB = m37831wB(i);
        if (wB == null) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        return wB.intValue;
    }

    public void putProp(int i, Object obj) {
        if (obj == null) {
            removeProp(i);
        } else {
            m37832wC(i).ibI = obj;
        }
    }

    public void putIntProp(int i, int i2) {
        m37832wC(i).intValue = i2;
    }

    public int getLineno() {
        return this.f8955iw;
    }

    public final double getDouble() {
        return ((C3368b) this).f8956io;
    }

    public final void setDouble(double d) {
        ((C3368b) this).f8956io = d;
    }

    public final String getString() {
        return ((C3371e) this).str;
    }

    public final void setString(String str) {
        if (str == null) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        ((C3371e) this).str = str;
    }

    public final Scope cFI() {
        return ((C3371e) this).hmI;
    }

    /* renamed from: c */
    public final void mo21465c(Scope cVar) {
        if (cVar == null) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        if (!(this instanceof C3371e)) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        ((C3371e) this).hmI = cVar;
    }

    public final int labelId() {
        if (!(this.type == 130 || this.type == 72)) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        return getIntProp(15, -1);
    }

    public void labelId(int i) {
        if (!(this.type == 130 || this.type == 72)) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        putIntProp(15, i);
    }

    public boolean cFK() {
        int cFS = cFS();
        return (cFS & 4) == 0 || (cFS & 11) == 0;
    }

    private int cFL() {
        Node qlVar = this.gZm;
        Node qlVar2 = ((C3367a) this).aUM;
        int cFS = qlVar.cFS();
        if (qlVar2 != null) {
            return cFS | qlVar2.cFS();
        }
        return cFS | 1;
    }

    private int cFM() {
        int i;
        Node qlVar = this.gZn.gZm;
        int i2 = 0;
        while (qlVar != null && qlVar.type == 114) {
            qlVar = qlVar.gZm;
            i2 = ((C3367a) qlVar).aUM.cFS() | i2;
        }
        int i3 = i2 & -2;
        Node WO = ((C3367a) this).mo21496WO();
        if (WO != null) {
            i = WO.cFS() | i3;
        } else {
            i = i3 | 1;
        }
        return i | getIntProp(18, 0);
    }

    private int cFN() {
        int i;
        Node WP = ((C3367a) this).mo21497WP();
        if (WP != null) {
            i = WP.gZm.gZn.cFS();
        } else {
            i = 1;
        }
        if ((i & 1) != 0) {
            i = (i & -2) | this.gZn.cFS();
            Node qlVar = ((C3367a) this).aUM;
            if (qlVar != null) {
                for (Node qlVar2 = qlVar.gZm.gZn; qlVar2 != null; qlVar2 = qlVar2.gZm.gZm) {
                    i |= qlVar2.gZm.gZn.gZm.gZn.cFS();
                }
            }
        }
        return i;
    }

    private int cFO() {
        Node qlVar = this.gZn;
        while (qlVar.gZm != this.gZo) {
            qlVar = qlVar.gZm;
        }
        if (qlVar.type != 6) {
            return 1;
        }
        int cFS = ((C3367a) qlVar).aUM.gZm.cFS();
        if (qlVar.gZn.type == 45) {
            cFS &= -2;
        }
        return cFS | getIntProp(18, 0);
    }

    private int cFP() {
        int i = 1;
        Node qlVar = this.gZn;
        while ((i & 1) != 0 && qlVar != null) {
            i = (i & -2) | qlVar.cFS();
            qlVar = qlVar.gZm;
        }
        return i;
    }

    private int cFQ() {
        return this.gZm.cFS() | getIntProp(18, 0);
    }

    private int cFR() {
        ((C3367a) this).aUO.putIntProp(18, 1);
        return 0;
    }

    private int cFS() {
        switch (this.type) {
            case 4:
                if (this.gZn != null) {
                    return 4;
                }
                return 2;
            case 50:
            case 120:
                return 0;
            case 72:
                return 8;
            case 119:
                return cFR();
            case 128:
            case 140:
                if (this.gZn == null) {
                    return 1;
                }
                switch (this.gZn.type) {
                    case 7:
                        return this.gZn.cFL();
                    case 80:
                        return this.gZn.cFN();
                    case 113:
                        return this.gZn.cFM();
                    case 129:
                        return this.gZn.cFQ();
                    default:
                        return cFP();
                }
            case 130:
                if (this.gZm != null) {
                    return this.gZm.cFS();
                }
                return 1;
            case 131:
                return cFO();
            case 132:
                if (this.gZn != null) {
                    return this.gZn.cFS();
                }
                return 1;
            default:
                return 1;
        }
    }

    public boolean cFT() {
        switch (this.type) {
            case -1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 30:
            case 31:
            case 35:
            case 37:
            case 38:
            case 50:
            case 51:
            case 56:
            case 57:
            case 64:
            case 68:
            case 69:
            case 70:
            case 72:
            case 80:
            case 81:
            case 89:
            case 90:
            case 91:
            case 92:
            case 93:
            case 94:
            case 95:
            case 96:
            case 97:
            case 98:
            case 99:
            case 100:
            case 105:
            case 106:
            case 109:
            case 110:
            case 111:
            case 112:
            case 113:
            case 116:
            case 117:
            case 118:
            case 119:
            case 120:
            case 121:
            case 122:
            case 123:
            case 124:
            case 128:
            case 129:
            case 130:
            case 131:
            case 133:
            case 134:
            case 138:
            case 139:
            case 140:
            case 141:
            case 152:
            case 153:
            case 157:
            case 158:
                return true;
            case 88:
            case 132:
                if (this.gZo != null) {
                    return this.gZo.cFT();
                }
                return true;
            case 101:
                if (this.gZn == null || this.gZn.gZm == null || this.gZn.gZm.gZm == null) {
                    org.mozilla1.javascript.Kit.codeBug();
                }
                if (!this.gZn.gZm.cFT() || !this.gZn.gZm.gZm.cFT()) {
                    return false;
                }
                return true;
            case 103:
            case 104:
                if (this.gZn == null || this.gZo == null) {
                    org.mozilla1.javascript.Kit.codeBug();
                }
                return this.gZn.cFT() || this.gZo.cFT();
            default:
                return false;
        }
    }

    public String toString() {
        return String.valueOf(this.type);
    }

    /* renamed from: a */
    private void m37827a(ObjToIntMap uSVar, StringBuffer stringBuffer) {
    }

    /* renamed from: l */
    public String mo21481l(org.mozilla1.javascript.ScriptOrFnNode aak) {
        return null;
    }

    /* renamed from: a.ql$b */
    /* compiled from: a */
    private static class C3368b extends Node {

        /* renamed from: io */
        double f8956io;

        C3368b(double d) {
            super(40);
            this.f8956io = d;
        }
    }

    /* renamed from: a.ql$e */
    /* compiled from: a */
    private static class C3371e extends Node {
        Scope hmI;
        String str;

        C3371e(int i, String str2) {
            super(i);
            this.str = str2;
        }
    }

    /* renamed from: a.ql$a */
    public static class C3367a extends Node {
        public Node aUM;
        /* access modifiers changed from: private */
        public C3367a aUO;
        private Node aUN;

        public C3367a(int i) {
            super(i);
        }

        C3367a(int i, int i2) {
            super(i, i2);
        }

        C3367a(int i, Node qlVar) {
            super(i, qlVar);
        }

        C3367a(int i, Node qlVar, int i2) {
            super(i, qlVar, i2);
        }

        /* renamed from: WN */
        public final C3367a mo21495WN() {
            if (!(this.type == 119 || this.type == 120)) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            return this.aUO;
        }

        /* renamed from: a */
        public final void mo21500a(C3367a aVar) {
            if (!(this.type == 119 || this.type == 120)) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            if (aVar == null) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            if (this.aUO != null) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            this.aUO = aVar;
        }

        /* renamed from: WO */
        public final Node mo21496WO() {
            if (this.type != 113) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            return this.aUN;
        }

        /* renamed from: d */
        public final void mo21502d(Node qlVar) {
            if (this.type != 113) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            if (qlVar.type != 130) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            if (this.aUN != null) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            this.aUN = qlVar;
        }

        /* renamed from: WP */
        public final Node mo21497WP() {
            if (this.type != 80) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            return this.aUN;
        }

        /* renamed from: e */
        public final void mo21503e(Node qlVar) {
            if (this.type != 80) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            if (qlVar.type != 130) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            if (this.aUN != null) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            this.aUN = qlVar;
        }

        /* renamed from: WQ */
        public final C3367a mo21498WQ() {
            if (this.type != 129) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            return this.aUO;
        }

        /* renamed from: b */
        public final void mo21501b(C3367a aVar) {
            if (this.type != 129) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            if (aVar == null) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            if (this.aUO != null) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            this.aUO = aVar;
        }

        /* renamed from: WR */
        public final Node mo21499WR() {
            if (this.type != 131) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            return this.aUN;
        }

        /* renamed from: f */
        public final void mo21504f(Node qlVar) {
            if (this.type != 131) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            if (qlVar.type != 130) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            if (this.aUN != null) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            this.aUN = qlVar;
        }
    }

    /* renamed from: a.ql$d */
    /* compiled from: a */
    static class C3370d {
        int heu;
        Scope hev;
        int index = -1;
        String name;

        C3370d(int i, String str) {
            this.heu = i;
            this.name = str;
        }
    }

    /* renamed from: a.ql$c */
    /* compiled from: a */
    public static class Scope extends C3367a {
        public LinkedHashMap<String, C3370d> fFc;
        private Scope fFd;
        private org.mozilla1.javascript.ScriptOrFnNode fFe;

        public Scope(int i) {
            super(i);
        }

        public Scope(int i, int i2) {
            super(i, i2);
        }

        public Scope(int i, Node qlVar, int i2) {
            super(i, qlVar, i2);
        }

        /* renamed from: a */
        public static Scope m37856a(Scope cVar) {
            Scope cVar2 = new Scope(cVar.getType());
            cVar2.fFc = cVar.fFc;
            cVar.fFc = null;
            cVar2.fFd = cVar.fFd;
            cVar.fFd = cVar2;
            cVar2.fFe = cVar.fFe;
            return cVar2;
        }

        /* renamed from: a */
        public static void m37857a(Scope cVar, Scope cVar2) {
            cVar.bXC();
            cVar2.bXC();
            if (!Collections.disjoint(cVar.fFc.keySet(), cVar2.fFc.keySet())) {
                throw Kit.codeBug();
            }
            cVar2.fFc.putAll(cVar.fFc);
        }

        /* renamed from: b */
        public void mo21506b(Scope cVar) {
            this.fFd = cVar;
            this.fFe = cVar == null ? (ScriptOrFnNode) this : cVar.fFe;
        }

        public Scope bXA() {
            return this.fFd;
        }

        /* renamed from: iL */
        public Scope mo21509iL(String str) {
            for (Scope cVar = this; cVar != null; cVar = cVar.fFd) {
                if (cVar.fFc != null && cVar.fFc.containsKey(str)) {
                    return cVar;
                }
            }
            return null;
        }

        /* renamed from: iM */
        public C3370d mo21510iM(String str) {
            if (this.fFc == null) {
                return null;
            }
            return this.fFc.get(str);
        }

        /* renamed from: a */
        public void mo21505a(String str, C3370d dVar) {
            bXC();
            this.fFc.put(str, dVar);
            dVar.hev = this;
            this.fFe.mo7680a(dVar);
        }

        public Map<String, C3370d> bXB() {
            return this.fFc;
        }

        private void bXC() {
            if (this.fFc == null) {
                this.fFc = new LinkedHashMap<>(5);
            }
        }
    }

    /* renamed from: a.ql$f */
    /* compiled from: a */
    private static class PropListItem {
        PropListItem ibH;
        Object ibI;
        int intValue;
        int type;

        private PropListItem() {
        }

        /* synthetic */ PropListItem(PropListItem fVar) {
            this();
        }
    }
}
