package org.mozilla1.javascript;

/* renamed from: a.aaS  reason: case insensitive filesystem */
/* compiled from: a */
public class DefiningClassLoader extends ClassLoader implements GeneratedClassLoader {
    private final ClassLoader eXk;

    public DefiningClassLoader() {
        this.eXk = getClass().getClassLoader();
    }

    public DefiningClassLoader(ClassLoader classLoader) {
        this.eXk = classLoader;
    }

    public Class<?> defineClass(String str, byte[] bArr) {
        return super.defineClass(str, bArr, 0, bArr.length, SecurityUtilities.m41206l(getClass()));
    }

    public void linkClass(Class<?> cls) {
        resolveClass(cls);
    }

    public Class<?> loadClass(String str, boolean z) throws ClassNotFoundException {
        Class<?> findLoadedClass = findLoadedClass(str);
        if (findLoadedClass == null) {
            if (this.eXk != null) {
                findLoadedClass = this.eXk.loadClass(str);
            } else {
                findLoadedClass = findSystemClass(str);
            }
        }
        if (z) {
            resolveClass(findLoadedClass);
        }
        return findLoadedClass;
    }
}
