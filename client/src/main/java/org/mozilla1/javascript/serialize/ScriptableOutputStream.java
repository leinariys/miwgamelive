package org.mozilla1.javascript.serialize;

import org.mozilla1.javascript.Scriptable;
import org.mozilla1.javascript.UniqueTag;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.aaO  reason: case insensitive filesystem */
/* compiled from: a */
public class ScriptableOutputStream extends ObjectOutputStream {

    /* renamed from: TR */
    private org.mozilla1.javascript.Scriptable scope;
    private Map<Object, String> table = new HashMap();

    public ScriptableOutputStream(OutputStream outputStream, org.mozilla1.javascript.Scriptable avf) throws IOException {
        super(outputStream);
        this.scope = avf;
        this.table.put(avf, "");
        enableReplaceObject(true);
        excludeStandardObjectNames();
    }

    /* JADX WARNING: Removed duplicated region for block: B:1:0x0008 A[LOOP:0: B:1:0x0008->B:7:0x001d, LOOP_START, PHI: r0
  PHI: (r0v2 java.lang.Object) = (r0v1 java.lang.Object), (r0v4 java.lang.Object) binds: [B:0:0x0000, B:7:0x001d] A[DONT_GENERATE, DONT_INLINE]] */
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.lang.Object m19438d(org.mozilla1.javascript.Scriptable r3, java.lang.String r4) {
        /*
            java.util.StringTokenizer r1 = new java.util.StringTokenizer
            java.lang.String r0 = "."
            r1.<init>(r4, r0)
            r0 = r3
        L_0x0008:
            boolean r2 = r1.hasMoreTokens()
            if (r2 != 0) goto L_0x000f
        L_0x000e:
            return r0
        L_0x000f:
            java.lang.String r2 = r1.nextToken()
            a.aVF r0 = (p001a.aVF) r0
            java.lang.Object r0 = p001a.C6327akn.m23587j(r0, r2)
            if (r0 == 0) goto L_0x000e
            boolean r2 = r0 instanceof p001a.aVF
            if (r2 != 0) goto L_0x0008
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.mozilla.javascript.serialize.C5782aaO.m19438d(a.aVF, java.lang.String):java.lang.Object");
    }

    /* renamed from: na */
    public void mo12190na(String str) {
        Object d = m19438d(this.scope, str);
        if (d != null && d != UniqueTag.f5854nb) {
            if (!(d instanceof org.mozilla1.javascript.Scriptable)) {
                throw new IllegalArgumentException("Object for excluded name " + str + " is not a Scriptable, it is " + d.getClass().getName());
            }
            this.table.put(d, str);
        }
    }

    public void addExcludedName(String str) {
        Object d = m19438d(this.scope, str);
        if (!(d instanceof Scriptable)) {
            throw new IllegalArgumentException("Object for excluded name " + str + " not found.");
        }
        this.table.put(d, str);
    }

    public boolean hasExcludedName(String str) {
        return this.table.get(str) != null;
    }

    public void removeExcludedName(String str) {
        this.table.remove(str);
    }

    public void excludeStandardObjectNames() {
        String[] strArr = {"Object", "Object.prototype", "Function", "Function.prototype", "String", "String.prototype", "Math", "Array", "Array.prototype", "Error", "Error.prototype", "Number", "Number.prototype", "Date", "Date.prototype", "RegExp", "RegExp.prototype", "Script", "Script.prototype", "Continuation", "Continuation.prototype"};
        for (String addExcludedName : strArr) {
            addExcludedName(addExcludedName);
        }
        String[] strArr2 = {"XML", "XML.prototype", "XMLList", "XMLList.prototype"};
        for (String na : strArr2) {
            mo12190na(na);
        }
    }

    /* access modifiers changed from: protected */
    public Object replaceObject(Object obj) {
        String str = this.table.get(obj);
        return str == null ? obj : new PendingLookup(str);
    }

    /* renamed from: a.aaO$a */
    public static class PendingLookup implements Serializable {
        static final long serialVersionUID = -2692990309789917727L;
        private String name;

        PendingLookup(String str) {
            this.name = str;
        }

        /* access modifiers changed from: package-private */
        public String getName() {
            return this.name;
        }
    }
}
