package org.mozilla1.javascript.serialize;

import org.mozilla1.javascript.Context;
import org.mozilla1.javascript.Scriptable;
import org.mozilla1.javascript.Undefined;
import org.mozilla1.javascript.UniqueTag;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

/* renamed from: a.aTQ */
/* compiled from: a */
public class ScriptableInputStream extends ObjectInputStream {

    /* renamed from: TR */
    private org.mozilla1.javascript.Scriptable f3796TR;
    private ClassLoader bJF;

    public ScriptableInputStream(InputStream inputStream, org.mozilla1.javascript.Scriptable avf) throws IOException {
        super(inputStream);
        this.f3796TR = avf;
        enableResolveObject(true);
        Context bwq = Context.bwq();
        if (bwq != null) {
            this.bJF = bwq.getApplicationClassLoader();
        }
    }

    /* access modifiers changed from: protected */
    public Class<?> resolveClass(ObjectStreamClass objectStreamClass) throws IOException, ClassNotFoundException {
        String name = objectStreamClass.getName();
        if (this.bJF != null) {
            try {
                return this.bJF.loadClass(name);
            } catch (ClassNotFoundException e) {
            }
        }
        return super.resolveClass(objectStreamClass);
    }

    /* access modifiers changed from: protected */
    public Object resolveObject(Object obj) throws IOException {
        if (obj instanceof ScriptableOutputStream.PendingLookup) {
            String name = ((ScriptableOutputStream.PendingLookup) obj).getName();
            Object d = ScriptableOutputStream.m19438d(this.f3796TR, name);
            if (d != Scriptable.NOT_FOUND) {
                return d;
            }
            throw new IOException("Object " + name + " not found upon " + "deserialization.");
        } else if (obj instanceof UniqueTag) {
            return ((UniqueTag) obj).readResolve();
        } else {
            if (obj instanceof Undefined) {
                return ((Undefined) obj).readResolve();
            }
            return obj;
        }
    }
}
