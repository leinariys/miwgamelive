package org.mozilla1.javascript;

/* renamed from: a.aIT */
/* compiled from: a */
class SpecialRef extends org.mozilla1.javascript.Ref {
    static final long serialVersionUID = -7521596632456797847L;
    private static final int idt = 0;
    private static final int idu = 1;
    private static final int idv = 2;
    private Scriptable idw;
    private String name;
    private int type;

    private SpecialRef(Scriptable avf, int i, String str) {
        this.idw = avf;
        this.type = i;
        this.name = str;
    }

    /* renamed from: a */
    static Ref m15406a(org.mozilla1.javascript.Context lhVar, Object obj, String str) {
        int i;
        Scriptable a = org.mozilla1.javascript.ScriptRuntime.m7477a(lhVar, obj);
        if (a == null) {
            throw org.mozilla1.javascript.ScriptRuntime.undefReadError(obj, str);
        }
        if (str.equals("__proto__")) {
            i = 1;
        } else if (str.equals("__parent__")) {
            i = 2;
        } else {
            throw new IllegalArgumentException(str);
        }
        if (!lhVar.hasFeature(5)) {
            i = 0;
        }
        return new SpecialRef(a, i, str);
    }

    public Object get(org.mozilla1.javascript.Context lhVar) {
        switch (this.type) {
            case 0:
                return org.mozilla1.javascript.ScriptRuntime.m7499a(this.idw, this.name, lhVar);
            case 1:
                return this.idw.getPrototype();
            case 2:
                return this.idw.getParentScope();
            default:
                throw org.mozilla1.javascript.Kit.codeBug();
        }
    }

    public Object set(org.mozilla1.javascript.Context lhVar, Object obj) {
        switch (this.type) {
            case 0:
                return org.mozilla1.javascript.ScriptRuntime.m7501a(this.idw, this.name, obj, lhVar);
            case 1:
            case 2:
                Scriptable a = org.mozilla1.javascript.ScriptRuntime.m7477a(lhVar, obj);
                if (a != null) {
                    Scriptable avf = a;
                    while (avf != this.idw) {
                        if (this.type == 1) {
                            avf = avf.getPrototype();
                            continue;
                        } else {
                            avf = avf.getParentScope();
                            continue;
                        }
                        if (avf == null) {
                        }
                    }
                    throw org.mozilla1.javascript.Context.m35246q("msg.cyclic.value", this.name);
                }
                if (this.type == 1) {
                    this.idw.setPrototype(a);
                    return a;
                }
                this.idw.setParentScope(a);
                return a;
            default:
                throw Kit.codeBug();
        }
    }

    public boolean has(org.mozilla1.javascript.Context lhVar) {
        if (this.type == 0) {
            return org.mozilla1.javascript.ScriptRuntime.m7562c(this.idw, (Object) this.name, lhVar);
        }
        return true;
    }

    public boolean delete(Context lhVar) {
        if (this.type == 0) {
            return ScriptRuntime.m7552b(this.idw, (Object) this.name, lhVar);
        }
        return false;
    }
}
