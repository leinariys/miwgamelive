package org.mozilla1.javascript;

/* renamed from: a.pO */
/* compiled from: a */
public interface ContextListener extends ContextFactory.C2715a {
    void contextEntered(Context lhVar);

    void contextExited(Context lhVar);
}
