package org.mozilla1.javascript;

import org.mozilla1.javascript.xml.XMLLib;

import java.io.Serializable;

/* renamed from: a.iw */
/* compiled from: a */
public class ContextFactory implements Serializable {
    private static volatile boolean dIl;
    private static ContextFactory dIm = new ContextFactory();
    private final Object dIo = new Integer(7);
    private volatile boolean dIn;
    private volatile Object dIp;
    private boolean dIq;
    private ClassLoader dIr;

    public static ContextFactory getGlobal() {
        return dIm;
    }

    public static boolean hasExplicitGlobal() {
        return dIl;
    }

    /* renamed from: a */
    public static synchronized void m33619a(ContextFactory iwVar) {
        synchronized (ContextFactory.class) {
            if (iwVar == null) {
                throw new IllegalArgumentException();
            } else if (dIl) {
                throw new IllegalStateException();
            } else {
                dIl = true;
                dIm = iwVar;
            }
        }
    }

    /* access modifiers changed from: protected */
    public org.mozilla1.javascript.Context bkK() {
        return new org.mozilla1.javascript.Context(this);
    }

    /* access modifiers changed from: protected */
    public boolean hasFeature(org.mozilla1.javascript.Context lhVar, int i) {
        switch (i) {
            case 1:
                int languageVersion = lhVar.getLanguageVersion();
                if (languageVersion == 100 || languageVersion == 110 || languageVersion == 120) {
                    return true;
                }
                return false;
            case 2:
            case 3:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
                return false;
            case 4:
                if (lhVar.getLanguageVersion() == 120) {
                    return true;
                }
                return false;
            case 5:
                return true;
            case 6:
                int languageVersion2 = lhVar.getLanguageVersion();
                if (languageVersion2 == 0 || languageVersion2 >= 160) {
                    return true;
                }
                return false;
            default:
                throw new IllegalArgumentException(String.valueOf(i));
        }
    }

    private boolean bkL() {
        Class<?> classOrNull = org.mozilla1.javascript.Kit.classOrNull("org.w3c.dom.Node");
        if (classOrNull == null) {
            return false;
        }
        try {
            classOrNull.getMethod("getUserData", new Class[]{String.class});
            return true;
        } catch (NoSuchMethodException e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public XMLLib.C0026a bkM() {
        if (bkL()) {
            return XMLLib.C0026a.m372lp("org.mozilla.javascript.xmlimpl.XMLLibImpl");
        }
        if (org.mozilla1.javascript.Kit.classOrNull("org.apache.xmlbeans.XmlCursor") != null) {
            return XMLLib.C0026a.m372lp("org.mozilla.javascript.xml.impl.xmlbeans.XMLLibImpl");
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public GeneratedClassLoader mo19792a(ClassLoader classLoader) {
        return new DefiningClassLoader(classLoader);
    }

    public final ClassLoader getApplicationClassLoader() {
        return this.dIr;
    }

    public final void initApplicationClassLoader(ClassLoader classLoader) {
        if (classLoader == null) {
            throw new IllegalArgumentException("loader is null");
        } else if (!org.mozilla1.javascript.Kit.m11149b(classLoader)) {
            throw new IllegalArgumentException("Loader can not resolve Rhino classes");
        } else if (this.dIr != null) {
            throw new IllegalStateException("applicationClassLoader can only be set once");
        } else {
            checkNotSealed();
            this.dIr = classLoader;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Object mo19794a(Callable mFVar, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Scriptable avf2, Object[] objArr) {
        return mFVar.call(lhVar, avf, avf2, objArr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo19797a(org.mozilla1.javascript.Context lhVar, int i) {
    }

    /* access modifiers changed from: protected */
    public void onContextCreated(org.mozilla1.javascript.Context lhVar) {
        Object obj = this.dIp;
        int i = 0;
        while (true) {
            int i2 = i;
            C2715a aVar = (C2715a) org.mozilla1.javascript.Kit.getListener(obj, i2);
            if (aVar != null) {
                aVar.contextCreated(lhVar);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo19796a(org.mozilla1.javascript.Context lhVar) {
        Object obj = this.dIp;
        int i = 0;
        while (true) {
            int i2 = i;
            C2715a aVar = (C2715a) org.mozilla1.javascript.Kit.getListener(obj, i2);
            if (aVar != null) {
                aVar.contextReleased(lhVar);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    public final void mo19795a(C2715a aVar) {
        checkNotSealed();
        synchronized (this.dIo) {
            if (this.dIq) {
                throw new IllegalStateException();
            }
            this.dIp = org.mozilla1.javascript.Kit.addListener(this.dIp, aVar);
        }
    }

    /* renamed from: b */
    public final void mo19799b(C2715a aVar) {
        checkNotSealed();
        synchronized (this.dIo) {
            if (this.dIq) {
                throw new IllegalStateException();
            }
            this.dIp = Kit.removeListener(this.dIp, aVar);
        }
    }

    /* access modifiers changed from: package-private */
    public final void bkN() {
        checkNotSealed();
        synchronized (this.dIo) {
            this.dIq = true;
            this.dIp = null;
        }
    }

    public final boolean isSealed() {
        return this.dIn;
    }

    public final void seal() {
        checkNotSealed();
        this.dIn = true;
    }

    /* access modifiers changed from: protected */
    public final void checkNotSealed() {
        if (this.dIn) {
            throw new IllegalStateException();
        }
    }

    /* renamed from: a */
    public final Object mo19793a(ContextAction jg) {
        return org.mozilla1.javascript.Context.m35233a(this, jg);
    }

    public org.mozilla1.javascript.Context enterContext() {
        return mo19798b((org.mozilla1.javascript.Context) null);
    }

    public final org.mozilla1.javascript.Context bkP() {
        return mo19798b((org.mozilla1.javascript.Context) null);
    }

    public final void exit() {
        org.mozilla1.javascript.Context.exit();
    }

    /* renamed from: b */
    public final org.mozilla1.javascript.Context mo19798b(org.mozilla1.javascript.Context lhVar) {
        return org.mozilla1.javascript.Context.call(lhVar, this);
    }

    /* renamed from: a.iw$a */
    public interface C2715a {
        void contextCreated(org.mozilla1.javascript.Context lhVar);

        void contextReleased(Context lhVar);
    }
}
