package org.mozilla1.javascript.debug;

/* renamed from: a.En */
/* compiled from: a */
public interface DebuggableScript {
    DebuggableScript aNR();

    /* renamed from: gW */
    DebuggableScript mo1978gW(int i);

    int getFunctionCount();

    String getFunctionName();

    int[] getLineNumbers();

    int getParamAndVarCount();

    int getParamCount();

    String getParamOrVarName(int i);

    String getSourceName();

    boolean isFunction();

    boolean isGeneratedScript();

    boolean isTopLevel();
}
