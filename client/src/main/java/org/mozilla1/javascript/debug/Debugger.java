package org.mozilla1.javascript.debug;

import org.mozilla1.javascript.Context;

/* renamed from: a.axE */
/* compiled from: a */
public interface Debugger {
    DebugFrame getFrame(Context lhVar, org.mozilla1.javascript.debug.DebuggableScript en);

    void handleCompilationDone(Context lhVar, DebuggableScript en, String str);
}
