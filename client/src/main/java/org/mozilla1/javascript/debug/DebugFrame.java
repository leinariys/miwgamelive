package org.mozilla1.javascript.debug;

import org.mozilla1.javascript.Context;
import org.mozilla1.javascript.Scriptable;

/* renamed from: a.Tp */
/* compiled from: a */
public interface DebugFrame {
    void onDebuggerStatement(Context lhVar);

    void onEnter(Context lhVar, org.mozilla1.javascript.Scriptable avf, Scriptable avf2, Object[] objArr);

    void onExceptionThrown(Context lhVar, Throwable th);

    void onExit(Context lhVar, boolean z, Object obj);

    void onLineChange(Context lhVar, int i);
}
