package org.mozilla1.javascript;

import java.util.Iterator;

/* renamed from: a.rp */
/* compiled from: a */
public final class NativeIterator extends IdScriptableObject {
    public static final String hOg = "__iterator__";
    private static final Object hOe = new Integer(3);
    private static final String hOf = "StopIteration";
    private static final int hOh = 2;
    private static final int hOi = 3;

    /* renamed from: ns */
    private static final int f9070ns = 1;

    /* renamed from: nt */
    private static final int f9071nt = 3;
    private static final long serialVersionUID = -4136968203581667681L;
    private Object hOj;

    private NativeIterator() {
    }

    private NativeIterator(Object obj) {
        this.hOj = obj;
    }

    /* renamed from: b */
    static void m38640b(ScriptableObject akn, boolean z) {
        new NativeIterator().mo15695a(3, akn, z);
        NativeGenerator.m35874c(akn, z);
        C3515a aVar = new C3515a();
        aVar.setPrototype(ScriptableObject.m23593v(akn));
        aVar.setParentScope(akn);
        if (z) {
            aVar.sealObject();
        }
        ScriptableObject.m23567a((Scriptable) akn, hOf, (Object) aVar, 2);
        akn.associateValue(hOe, aVar);
    }

    /* renamed from: o */
    public static Object m38643o(Scriptable avf) {
        return ScriptableObject.m23581d(ScriptableObject.m23595x(avf), hOe);
    }

    /* renamed from: e */
    private static Object m38641e(org.mozilla1.javascript.Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        int i;
        if (objArr.length == 0 || objArr[0] == null || objArr[0] == Undefined.instance) {
            throw org.mozilla1.javascript.ScriptRuntime.m7536aZ("msg.no.properties", org.mozilla1.javascript.ScriptRuntime.toString(objArr[0]));
        }
        Scriptable c = org.mozilla1.javascript.ScriptRuntime.m7554c(avf, objArr[0]);
        boolean z = objArr.length > 1 && org.mozilla1.javascript.ScriptRuntime.toBoolean(objArr[1]);
        if (avf2 != null) {
            Iterator<?> b = VMBridge.iEO.mo10867b(lhVar, avf, c);
            if (b != null) {
                Scriptable x = ScriptableObject.m23595x(avf);
                return lhVar.bwy().mo6829a(lhVar, x, new C3516b(b, x), C3516b.class);
            }
            Scriptable a = org.mozilla1.javascript.ScriptRuntime.m7476a(lhVar, avf, c, z);
            if (a != null) {
                return a;
            }
        }
        if (z) {
            i = 3;
        } else {
            i = 5;
        }
        Object a2 = org.mozilla1.javascript.ScriptRuntime.m7513a((Object) c, lhVar, i);
        org.mozilla1.javascript.ScriptRuntime.m7550b(a2, true);
        NativeIterator rpVar = new NativeIterator(a2);
        rpVar.setPrototype(ScriptableObject.m23585i(avf, rpVar.getClassName()));
        rpVar.setParentScope(avf);
        return rpVar;
    }

    public String getClassName() {
        return "Iterator";
    }

    /* access modifiers changed from: protected */
    public void initPrototypeId(int i) {
        int i2;
        String str;
        switch (i) {
            case 1:
                i2 = 2;
                str = "constructor";
                break;
            case 2:
                i2 = 0;
                str = "next";
                break;
            case 3:
                i2 = 1;
                str = hOg;
                break;
            default:
                throw new IllegalArgumentException(String.valueOf(i));
        }
        initPrototypeMethod(hOe, i, str, i2);
    }

    public Object execIdCall(IdFunctionObject yy, org.mozilla1.javascript.Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        if (!yy.hasTag(hOe)) {
            return super.execIdCall(yy, lhVar, avf, avf2, objArr);
        }
        int methodId = yy.methodId();
        if (methodId == 1) {
            return m38641e(lhVar, avf, avf2, objArr);
        }
        if (!(avf2 instanceof NativeIterator)) {
            throw m25341f(yy);
        }
        NativeIterator rpVar = (NativeIterator) avf2;
        switch (methodId) {
            case 2:
                return rpVar.m38642g(lhVar, avf);
            case 3:
                return avf2;
            default:
                throw new IllegalArgumentException(String.valueOf(methodId));
        }
    }

    /* renamed from: g */
    private Object m38642g(Context lhVar, Scriptable avf) {
        if (org.mozilla1.javascript.ScriptRuntime.enumNext(this.hOj).booleanValue()) {
            return ScriptRuntime.m7559c(this.hOj, lhVar);
        }
        throw new JavaScriptException(m38643o(avf), (String) null, 0);
    }

    /* access modifiers changed from: protected */
    public int findPrototypeId(String str) {
        int i;
        String str2 = null;
        int length = str.length();
        if (length == 4) {
            str2 = "next";
            i = 2;
        } else if (length == 11) {
            str2 = "constructor";
            i = 1;
        } else if (length == 12) {
            str2 = hOg;
            i = 3;
        } else {
            i = 0;
        }
        if (str2 == null || str2 == str || str2.equals(str)) {
            return i;
        }
        return 0;
    }

    /* renamed from: a.rp$a */
    static class C3515a extends NativeObject {
        private static final long serialVersionUID = 2485151085722377663L;

        C3515a() {
        }

        public String getClassName() {
            return NativeIterator.hOf;
        }

        public boolean hasInstance(Scriptable avf) {
            return avf instanceof C3515a;
        }
    }

    /* renamed from: a.rp$b */
    /* compiled from: a */
    public static class C3516b {

        /* renamed from: TR */
        private Scriptable f9072TR;
        private Iterator<?> iterator;

        C3516b(Iterator<?> it, Scriptable avf) {
            this.iterator = it;
            this.f9072TR = avf;
        }

        public Object next() {
            if (this.iterator.hasNext()) {
                return this.iterator.next();
            }
            throw new JavaScriptException(NativeIterator.m38643o(this.f9072TR), (String) null, 0);
        }

        /* renamed from: bj */
        public Object mo21847bj(boolean z) {
            return this;
        }
    }
}
