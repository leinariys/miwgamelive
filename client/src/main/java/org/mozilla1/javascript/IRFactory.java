package org.mozilla1.javascript;

import java.util.ArrayList;

/* renamed from: a.ayw  reason: case insensitive filesystem */
/* compiled from: a */
public final class IRFactory {
    private static final int gTK = 0;
    private static final int gTL = 1;
    private static final int gTM = 2;
    private static final int gTN = 1;
    private static final int gTO = -1;

    /* renamed from: iG */
    private org.mozilla1.javascript.Parser f5656iG;

    IRFactory(Parser axf) {
        this.f5656iG = axf;
    }

    /* renamed from: D */
    private static int m27500D(org.mozilla1.javascript.Node qlVar) {
        switch (qlVar.getType()) {
            case 40:
                double d = qlVar.getDouble();
                if (d != d || d == org.mozilla1.javascript.ScriptRuntime.NaN) {
                    return -1;
                }
                return 1;
            case 42:
            case 44:
                return -1;
            case 45:
                return 1;
            default:
                return 0;
        }
    }

    /* access modifiers changed from: package-private */
    public org.mozilla1.javascript.ScriptOrFnNode cDG() {
        return new org.mozilla1.javascript.ScriptOrFnNode(135);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo17105a(ScriptOrFnNode aak, org.mozilla1.javascript.Node qlVar) {
        org.mozilla1.javascript.Node cFE = qlVar.cFE();
        if (cFE != null) {
            aak.mo21463I(cFE);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: wb */
    public org.mozilla1.javascript.Node mo17134wb(int i) {
        return new org.mozilla1.javascript.Node(i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public org.mozilla1.javascript.Node mo17117d(org.mozilla1.javascript.Node qlVar, int i) {
        return new org.mozilla1.javascript.Node(128, (org.mozilla1.javascript.Node) new org.mozilla1.javascript.Node.C3367a(113, qlVar, i));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo17106a(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, org.mozilla1.javascript.Node qlVar3) {
        if (qlVar.getType() != 128) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        org.mozilla1.javascript.Node.C3367a aVar = (org.mozilla1.javascript.Node.C3367a) qlVar.cFE();
        if (aVar.getType() != 113) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        org.mozilla1.javascript.Node cFJ = org.mozilla1.javascript.Node.cFJ();
        if (qlVar2 != null) {
            org.mozilla1.javascript.Node.C3367a aVar2 = new org.mozilla1.javascript.Node.C3367a(114, qlVar2);
            aVar2.aUM = cFJ;
            aVar.mo21461G(aVar2);
        } else {
            aVar.mo21502d(cFJ);
        }
        qlVar.mo21461G(cFJ);
        qlVar.mo21461G(qlVar3);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: y */
    public void mo17139y(org.mozilla1.javascript.Node qlVar) {
        if (qlVar.getType() != 128) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        org.mozilla1.javascript.Node.C3367a aVar = (org.mozilla1.javascript.Node.C3367a) qlVar.cFE();
        if (aVar.getType() != 113) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        org.mozilla1.javascript.Node cFJ = org.mozilla1.javascript.Node.cFJ();
        aVar.aUM = cFJ;
        org.mozilla1.javascript.Node WO = aVar.mo21496WO();
        if (WO == null) {
            WO = cFJ;
        }
        qlVar.mo21485p(m27505c(5, WO), aVar);
        qlVar.mo21461G(cFJ);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: al */
    public org.mozilla1.javascript.Node mo17107al(int i, int i2) {
        return new org.mozilla1.javascript.Node(i, i2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public org.mozilla1.javascript.Node mo17120e(org.mozilla1.javascript.Node qlVar, int i) {
        int i2;
        if (this.f5656iG.cCk()) {
            i2 = 132;
        } else {
            i2 = 133;
        }
        return new org.mozilla1.javascript.Node(i2, qlVar, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: f */
    public org.mozilla1.javascript.Node mo17121f(org.mozilla1.javascript.Node qlVar, int i) {
        return new org.mozilla1.javascript.Node(132, qlVar, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public org.mozilla1.javascript.Node mo17122g(org.mozilla1.javascript.Node qlVar, int i) {
        cDH();
        return mo17120e(mo17093a(73, qlVar), i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: kF */
    public org.mozilla1.javascript.Node mo17127kF(String str) {
        m27507x(str, 39);
        return org.mozilla1.javascript.Node.m37828e(39, str);
    }

    /* renamed from: a */
    private org.mozilla1.javascript.Node m27502a(int i, String str, org.mozilla1.javascript.Node qlVar) {
        org.mozilla1.javascript.Node kF = mo17127kF(str);
        kF.setType(i);
        if (qlVar != null) {
            kF.mo21461G(qlVar);
        }
        return kF;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: kG */
    public org.mozilla1.javascript.Node mo17128kG(String str) {
        return org.mozilla1.javascript.Node.m37829kM(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: V */
    public org.mozilla1.javascript.Node mo17092V(double d) {
        return org.mozilla1.javascript.Node.m37823W(d);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public org.mozilla1.javascript.Node mo17104a(String str, org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, int i) {
        org.mozilla1.javascript.Node qlVar3;
        if (qlVar == null) {
            qlVar3 = new org.mozilla1.javascript.Node(127);
        } else {
            qlVar3 = qlVar;
        }
        return new org.mozilla1.javascript.Node(123, mo17127kF(str), qlVar3, qlVar2, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h */
    public org.mozilla1.javascript.Node mo17123h(org.mozilla1.javascript.Node qlVar, int i) {
        return new org.mozilla1.javascript.Node(50, qlVar, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: i */
    public org.mozilla1.javascript.Node mo17124i(org.mozilla1.javascript.Node qlVar, int i) {
        if (qlVar == null) {
            return new org.mozilla1.javascript.Node(4, i);
        }
        return new org.mozilla1.javascript.Node(4, qlVar, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: wc */
    public org.mozilla1.javascript.Node mo17135wc(int i) {
        return new org.mozilla1.javascript.Node(159, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: wd */
    public org.mozilla1.javascript.Node mo17136wd(int i) {
        return new org.mozilla1.javascript.Node.C3367a(129, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: z */
    public org.mozilla1.javascript.Node mo17140z(org.mozilla1.javascript.Node qlVar) {
        return ((org.mozilla1.javascript.Node.C3367a) qlVar).mo21498WQ();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: l */
    public org.mozilla1.javascript.Node mo17131l(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        org.mozilla1.javascript.Node.C3367a aVar = (org.mozilla1.javascript.Node.C3367a) qlVar;
        org.mozilla1.javascript.Node cFJ = org.mozilla1.javascript.Node.cFJ();
        org.mozilla1.javascript.Node qlVar3 = new org.mozilla1.javascript.Node(128, (org.mozilla1.javascript.Node) aVar, qlVar2, cFJ);
        aVar.aUM = cFJ;
        return qlVar3;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: j */
    public org.mozilla1.javascript.Node mo17125j(org.mozilla1.javascript.Node qlVar, int i) {
        org.mozilla1.javascript.Node.C3367a aVar;
        org.mozilla1.javascript.Node.C3367a aVar2 = new org.mozilla1.javascript.Node.C3367a(119, i);
        int type = qlVar.getType();
        if (type == 131 || type == 129) {
            aVar = (org.mozilla1.javascript.Node.C3367a) qlVar;
        } else if (type == 128 && qlVar.cFE().getType() == 113) {
            aVar = (org.mozilla1.javascript.Node.C3367a) qlVar.cFE();
        } else {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        aVar2.mo21500a(aVar);
        return aVar2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: k */
    public org.mozilla1.javascript.Node mo17126k(org.mozilla1.javascript.Node qlVar, int i) {
        if (qlVar.getType() != 131) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        org.mozilla1.javascript.Node.C3367a aVar = new org.mozilla1.javascript.Node.C3367a(120, i);
        aVar.mo21500a((org.mozilla1.javascript.Node.C3367a) qlVar);
        return aVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: we */
    public org.mozilla1.javascript.Node mo17137we(int i) {
        return new org.mozilla1.javascript.Node(128, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: kH */
    public FunctionNode mo17129kH(String str) {
        return new FunctionNode(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public org.mozilla1.javascript.Node mo17096a(FunctionNode yq, int i, org.mozilla1.javascript.Node qlVar, int i2) {
        String functionName;
        yq.eMN = i2;
        yq.mo21461G(qlVar);
        if (yq.getFunctionCount() != 0) {
            yq.eMO = true;
        }
        if (!(i2 != 2 || (functionName = yq.getFunctionName()) == null || functionName.length() == 0)) {
            qlVar.mo21462H(new org.mozilla1.javascript.Node(132, new org.mozilla1.javascript.Node(8, org.mozilla1.javascript.Node.m37828e(49, functionName), new org.mozilla1.javascript.Node(63))));
        }
        org.mozilla1.javascript.Node cFF = qlVar.cFF();
        if (cFF == null || cFF.getType() != 4) {
            qlVar.mo21461G(new org.mozilla1.javascript.Node(4));
        }
        org.mozilla1.javascript.Node e = org.mozilla1.javascript.Node.m37828e(108, yq.getFunctionName());
        e.putIntProp(1, i);
        return e;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: m */
    public void mo17133m(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        qlVar.mo21461G(qlVar2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: am */
    public org.mozilla1.javascript.Node mo17108am(int i, int i2) {
        return new org.mozilla1.javascript.Node.Scope(i, i2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: l */
    public org.mozilla1.javascript.Node mo17130l(org.mozilla1.javascript.Node qlVar, int i) {
        org.mozilla1.javascript.Node.Scope cVar = new org.mozilla1.javascript.Node.Scope(131, i);
        if (qlVar != null) {
            ((org.mozilla1.javascript.Node.C3367a) qlVar).mo21501b(cVar);
        }
        return cVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public org.mozilla1.javascript.Node mo17111b(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, org.mozilla1.javascript.Node qlVar3) {
        return m27503a((org.mozilla1.javascript.Node.C3367a) qlVar, 1, qlVar3, qlVar2, (org.mozilla1.javascript.Node) null, (org.mozilla1.javascript.Node) null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public org.mozilla1.javascript.Node mo17114c(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, org.mozilla1.javascript.Node qlVar3) {
        return m27503a((org.mozilla1.javascript.Node.C3367a) qlVar, 0, qlVar2, qlVar3, (org.mozilla1.javascript.Node) null, (org.mozilla1.javascript.Node) null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public org.mozilla1.javascript.Node mo17101a(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, org.mozilla1.javascript.Node qlVar3, org.mozilla1.javascript.Node qlVar4, org.mozilla1.javascript.Node qlVar5) {
        if (qlVar2.getType() == 152) {
            org.mozilla1.javascript.Node.Scope a = org.mozilla1.javascript.Node.Scope.m37856a((org.mozilla1.javascript.Node.Scope) qlVar);
            a.setType(152);
            a.mo21463I(qlVar2);
            a.mo21461G(m27503a((org.mozilla1.javascript.Node.C3367a) qlVar, 2, qlVar5, qlVar3, new org.mozilla1.javascript.Node(127), qlVar4));
            return a;
        }
        return m27503a((org.mozilla1.javascript.Node.C3367a) qlVar, 2, qlVar5, qlVar3, qlVar2, qlVar4);
    }

    /* renamed from: a */
    private org.mozilla1.javascript.Node m27503a(org.mozilla1.javascript.Node.C3367a aVar, int i, org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, org.mozilla1.javascript.Node qlVar3, org.mozilla1.javascript.Node qlVar4) {
        org.mozilla1.javascript.Node cFJ = org.mozilla1.javascript.Node.cFJ();
        org.mozilla1.javascript.Node cFJ2 = org.mozilla1.javascript.Node.cFJ();
        if (i == 2 && qlVar2.getType() == 127) {
            qlVar2 = new org.mozilla1.javascript.Node(45);
        }
        org.mozilla1.javascript.Node.C3367a aVar2 = new org.mozilla1.javascript.Node.C3367a(6, qlVar2);
        aVar2.aUM = cFJ;
        org.mozilla1.javascript.Node cFJ3 = org.mozilla1.javascript.Node.cFJ();
        aVar.mo21461G(cFJ);
        aVar.mo21463I(qlVar);
        if (i == 1 || i == 2) {
            aVar.mo21463I(new org.mozilla1.javascript.Node(127, aVar.getLineno()));
        }
        aVar.mo21461G(cFJ2);
        aVar.mo21461G(aVar2);
        aVar.mo21461G(cFJ3);
        aVar.aUM = cFJ3;
        if (i == 1 || i == 2) {
            aVar.mo21460F(m27505c(5, cFJ2));
            if (i == 2) {
                int type = qlVar3.getType();
                if (type != 127) {
                    if (!(type == 121 || type == 152)) {
                        qlVar3 = new org.mozilla1.javascript.Node(132, qlVar3);
                    }
                    aVar.mo21460F(qlVar3);
                }
                cFJ2 = org.mozilla1.javascript.Node.cFJ();
                aVar.mo21485p(cFJ2, qlVar);
                if (qlVar4.getType() != 127) {
                    aVar.mo21485p(new org.mozilla1.javascript.Node(132, qlVar4), cFJ2);
                }
            }
        }
        aVar.mo21504f(cFJ2);
        return aVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public org.mozilla1.javascript.Node mo17094a(int i, org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, org.mozilla1.javascript.Node qlVar3, org.mozilla1.javascript.Node qlVar4, boolean z) {
        org.mozilla1.javascript.Node qlVar5;
        int i2;
        int i3;
        org.mozilla1.javascript.Node n;
        int i4 = -1;
        int i5 = 0;
        int type = qlVar2.getType();
        if (type == 121 || type == 152) {
            qlVar5 = qlVar2.cFF();
            if (qlVar2.cFE() != qlVar5) {
                this.f5656iG.mo16821kC("msg.mult.index");
            }
            if (qlVar5.getType() == 65 || qlVar5.getType() == 66) {
                int type2 = qlVar5.getType();
                i5 = qlVar5.getIntProp(21, 0);
                i2 = type2;
                i4 = type2;
            } else if (qlVar5.getType() == 39) {
                qlVar5 = org.mozilla1.javascript.Node.m37828e(39, qlVar5.getString());
                i2 = type;
            } else {
                this.f5656iG.mo16821kC("msg.bad.for.in.lhs");
                return qlVar3;
            }
        } else if (type == 65 || type == 66) {
            i5 = qlVar2.getIntProp(21, 0);
            i2 = type;
            qlVar5 = qlVar2;
            i4 = type;
        } else {
            qlVar5 = m27499C(qlVar2);
            if (qlVar5 == null) {
                this.f5656iG.mo16821kC("msg.bad.for.in.lhs");
                return qlVar3;
            }
            i2 = type;
        }
        org.mozilla1.javascript.Node qlVar6 = new org.mozilla1.javascript.Node(140);
        if (z) {
            i3 = 59;
        } else if (i4 != -1) {
            i3 = 60;
        } else {
            i3 = 58;
        }
        org.mozilla1.javascript.Node qlVar7 = new org.mozilla1.javascript.Node(i3, qlVar3);
        qlVar7.putProp(3, qlVar6);
        org.mozilla1.javascript.Node qlVar8 = new org.mozilla1.javascript.Node(61);
        qlVar8.putProp(3, qlVar6);
        org.mozilla1.javascript.Node qlVar9 = new org.mozilla1.javascript.Node(62);
        qlVar9.putProp(3, qlVar6);
        org.mozilla1.javascript.Node qlVar10 = new org.mozilla1.javascript.Node(128);
        if (i4 != -1) {
            n = mo17119e(i, qlVar5, qlVar9);
            if (!z && (i4 == 66 || i5 != 2)) {
                this.f5656iG.mo16821kC("msg.bad.for.in.destruct");
            }
        } else {
            n = m27506n(qlVar5, qlVar9);
        }
        qlVar10.mo21461G(new org.mozilla1.javascript.Node(132, n));
        qlVar10.mo21461G(qlVar4);
        org.mozilla1.javascript.Node b = mo17111b(qlVar, qlVar8, qlVar10);
        b.mo21460F(qlVar7);
        if (i2 == 121 || i2 == 152) {
            b.mo21460F(qlVar2);
        }
        qlVar6.mo21461G(b);
        return qlVar6;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public org.mozilla1.javascript.Node mo17100a(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, org.mozilla1.javascript.Node qlVar3, int i) {
        boolean z = qlVar3 != null && (qlVar3.getType() != 128 || qlVar3.hasChildren());
        if (qlVar.getType() == 128 && !qlVar.hasChildren() && !z) {
            return qlVar;
        }
        boolean hasChildren = qlVar2.hasChildren();
        if (!z && !hasChildren) {
            return qlVar;
        }
        org.mozilla1.javascript.Node qlVar4 = new org.mozilla1.javascript.Node(140);
        org.mozilla1.javascript.Node.C3367a aVar = new org.mozilla1.javascript.Node.C3367a(80, qlVar, i);
        aVar.putProp(3, qlVar4);
        if (hasChildren) {
            org.mozilla1.javascript.Node cFJ = org.mozilla1.javascript.Node.cFJ();
            aVar.mo21461G(m27505c(5, cFJ));
            org.mozilla1.javascript.Node cFJ2 = org.mozilla1.javascript.Node.cFJ();
            aVar.aUM = cFJ2;
            aVar.mo21461G(cFJ2);
            org.mozilla1.javascript.Node qlVar5 = new org.mozilla1.javascript.Node(140);
            org.mozilla1.javascript.Node cFE = qlVar2.cFE();
            boolean z2 = false;
            int i2 = 0;
            while (cFE != null) {
                int lineno = cFE.getLineno();
                org.mozilla1.javascript.Node cFE2 = cFE.cFE();
                org.mozilla1.javascript.Node cFG = cFE2.cFG();
                org.mozilla1.javascript.Node cFG2 = cFG.cFG();
                cFE.mo21464J(cFE2);
                cFE.mo21464J(cFG);
                cFE.mo21464J(cFG2);
                cFG2.mo21461G(new org.mozilla1.javascript.Node(3));
                cFG2.mo21461G(m27505c(5, cFJ));
                if (cFG.getType() == 127) {
                    z2 = true;
                } else {
                    cFG2 = mo17112b(cFG, cFG2, (org.mozilla1.javascript.Node) null, lineno);
                }
                org.mozilla1.javascript.Node qlVar6 = new org.mozilla1.javascript.Node(57, cFE2, mo17091B(qlVar4));
                qlVar6.putProp(3, qlVar5);
                qlVar6.putIntProp(14, i2);
                qlVar5.mo21461G(qlVar6);
                qlVar5.mo21461G(mo17099a(mo17091B(qlVar5), cFG2, lineno));
                cFE = cFE.cFG();
                i2++;
            }
            aVar.mo21461G(qlVar5);
            if (!z2) {
                org.mozilla1.javascript.Node qlVar7 = new org.mozilla1.javascript.Node(51);
                qlVar7.putProp(3, qlVar4);
                aVar.mo21461G(qlVar7);
            }
            aVar.mo21461G(cFJ);
        }
        if (z) {
            org.mozilla1.javascript.Node cFJ3 = org.mozilla1.javascript.Node.cFJ();
            aVar.mo21503e(cFJ3);
            aVar.mo21461G(m27505c(134, cFJ3));
            org.mozilla1.javascript.Node cFJ4 = org.mozilla1.javascript.Node.cFJ();
            aVar.mo21461G(m27505c(5, cFJ4));
            aVar.mo21461G(cFJ3);
            org.mozilla1.javascript.Node qlVar8 = new org.mozilla1.javascript.Node(124, qlVar3);
            qlVar8.putProp(3, qlVar4);
            aVar.mo21461G(qlVar8);
            aVar.mo21461G(cFJ4);
        }
        qlVar4.mo21461G(aVar);
        return qlVar4;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public org.mozilla1.javascript.Node mo17099a(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, int i) {
        cDH();
        org.mozilla1.javascript.Node qlVar3 = new org.mozilla1.javascript.Node(128, i);
        qlVar3.mo21461G(new org.mozilla1.javascript.Node(2, qlVar));
        qlVar3.mo21463I(new org.mozilla1.javascript.Node(122, qlVar2, i));
        qlVar3.mo21461G(new org.mozilla1.javascript.Node(3));
        return qlVar3;
    }

    /* renamed from: b */
    public org.mozilla1.javascript.Node mo17110b(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, int i) {
        cDH();
        return new org.mozilla1.javascript.Node(145, qlVar, qlVar2, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public org.mozilla1.javascript.Node mo17098a(ObjArray acx, int i, int i2) {
        int[] iArr;
        int i3;
        int size = acx.size();
        int[] iArr2 = null;
        if (i != 0) {
            iArr = new int[i];
        } else {
            iArr = iArr2;
        }
        org.mozilla1.javascript.Node qlVar = new org.mozilla1.javascript.Node(65);
        int i4 = 0;
        for (int i5 = 0; i5 != size; i5++) {
            org.mozilla1.javascript.Node qlVar2 = (org.mozilla1.javascript.Node) acx.get(i5);
            if (qlVar2 != null) {
                qlVar.mo21461G(qlVar2);
                i3 = i4;
            } else {
                iArr[i4] = i5;
                i3 = i4 + 1;
            }
            i4 = i3;
        }
        if (i != 0) {
            qlVar.putProp(11, iArr);
        }
        qlVar.putIntProp(21, i2);
        return qlVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public org.mozilla1.javascript.Node mo17097a(ObjArray acx) {
        Object[] objArr;
        int size = acx.size() / 2;
        org.mozilla1.javascript.Node qlVar = new org.mozilla1.javascript.Node(66);
        if (size == 0) {
            objArr = org.mozilla1.javascript.ScriptRuntime.emptyArgs;
        } else {
            Object[] objArr2 = new Object[size];
            for (int i = 0; i != size; i++) {
                objArr2[i] = acx.get(i * 2);
                qlVar.mo21461G((org.mozilla1.javascript.Node) acx.get((i * 2) + 1));
            }
            objArr = objArr2;
        }
        qlVar.putProp(12, objArr);
        return qlVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: wf */
    public org.mozilla1.javascript.Node mo17138wf(int i) {
        org.mozilla1.javascript.Node qlVar = new org.mozilla1.javascript.Node(48);
        qlVar.putIntProp(4, i);
        return qlVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public org.mozilla1.javascript.Node mo17112b(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, org.mozilla1.javascript.Node qlVar3, int i) {
        int D = m27500D(qlVar);
        if (D == 1) {
            return qlVar2;
        }
        if (D != -1) {
            org.mozilla1.javascript.Node qlVar4 = new org.mozilla1.javascript.Node(128, i);
            org.mozilla1.javascript.Node cFJ = org.mozilla1.javascript.Node.cFJ();
            org.mozilla1.javascript.Node.C3367a aVar = new org.mozilla1.javascript.Node.C3367a(7, qlVar);
            aVar.aUM = cFJ;
            qlVar4.mo21461G(aVar);
            qlVar4.mo21463I(qlVar2);
            if (qlVar3 != null) {
                org.mozilla1.javascript.Node cFJ2 = org.mozilla1.javascript.Node.cFJ();
                qlVar4.mo21461G(m27505c(5, cFJ2));
                qlVar4.mo21461G(cFJ);
                qlVar4.mo21463I(qlVar3);
                qlVar4.mo21461G(cFJ2);
            } else {
                qlVar4.mo21461G(cFJ);
            }
            return qlVar4;
        } else if (qlVar3 != null) {
            return qlVar3;
        } else {
            return new org.mozilla1.javascript.Node(128, i);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public org.mozilla1.javascript.Node mo17118d(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, org.mozilla1.javascript.Node qlVar3) {
        int D = m27500D(qlVar);
        if (D == 1) {
            return qlVar2;
        }
        if (D == -1) {
            return qlVar3;
        }
        return new org.mozilla1.javascript.Node(101, qlVar, qlVar2, qlVar3);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public org.mozilla1.javascript.Node mo17093a(int i, org.mozilla1.javascript.Node qlVar) {
        int i2;
        org.mozilla1.javascript.Node qlVar2;
        int type = qlVar.getType();
        switch (i) {
            case 26:
                int D = m27500D(qlVar);
                if (D != 0) {
                    if (D == 1) {
                        i2 = 44;
                    } else {
                        i2 = 45;
                    }
                    if (type != 45 && type != 44) {
                        return new org.mozilla1.javascript.Node(i2);
                    }
                    qlVar.setType(i2);
                    return qlVar;
                }
                break;
            case 27:
                if (type == 40) {
                    qlVar.setDouble((double) (org.mozilla1.javascript.ScriptRuntime.toInt32(qlVar.getDouble()) ^ -1));
                    return qlVar;
                }
                break;
            case 29:
                if (type == 40) {
                    qlVar.setDouble(-qlVar.getDouble());
                    return qlVar;
                }
                break;
            case 31:
                if (type == 39) {
                    qlVar.setType(49);
                    qlVar2 = new org.mozilla1.javascript.Node(i, qlVar, org.mozilla1.javascript.Node.m37829kM(qlVar.getString()));
                } else if (type == 33 || type == 36) {
                    org.mozilla1.javascript.Node cFE = qlVar.cFE();
                    org.mozilla1.javascript.Node cFF = qlVar.cFF();
                    qlVar.mo21464J(cFE);
                    qlVar.mo21464J(cFF);
                    qlVar2 = new org.mozilla1.javascript.Node(i, cFE, cFF);
                } else if (type == 67) {
                    org.mozilla1.javascript.Node cFE2 = qlVar.cFE();
                    qlVar.mo21464J(cFE2);
                    qlVar2 = new org.mozilla1.javascript.Node(69, cFE2);
                } else {
                    qlVar2 = new org.mozilla1.javascript.Node(45);
                }
                return qlVar2;
            case 32:
                if (type == 39) {
                    qlVar.setType(136);
                    return qlVar;
                }
                break;
        }
        return new org.mozilla1.javascript.Node(i, qlVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: m */
    public org.mozilla1.javascript.Node mo17132m(org.mozilla1.javascript.Node qlVar, int i) {
        if (!this.f5656iG.cCk()) {
            this.f5656iG.mo16821kC("msg.bad.yield");
        }
        cDH();
        cDI();
        if (qlVar != null) {
            return new org.mozilla1.javascript.Node(72, qlVar, i);
        }
        return new org.mozilla1.javascript.Node(72, i);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0046, code lost:
        if (r6.cFF().getString().equals("eval") != false) goto L_0x0016;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.mozilla1.javascript.Node mo17109b(int r5, org.mozilla1.javascript.Node r6) {
        /*
            r4 = this;
            r0 = 1
            r1 = 0
            int r2 = r6.getType()
            r3 = 39
            if (r2 != r3) goto L_0x0030
            java.lang.String r2 = r6.getString()
            java.lang.String r3 = "eval"
            boolean r3 = r2.equals(r3)
            if (r3 == 0) goto L_0x0026
        L_0x0016:
            a.ql r1 = new a.ql
            r1.<init>((int) r5, (org.mozilla.javascript.C3366ql) r6)
            if (r0 == 0) goto L_0x0025
            r4.cDH()
            r2 = 10
            r1.putIntProp(r2, r0)
        L_0x0025:
            return r1
        L_0x0026:
            java.lang.String r0 = "With"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0048
            r0 = 2
            goto L_0x0016
        L_0x0030:
            int r2 = r6.getType()
            r3 = 33
            if (r2 != r3) goto L_0x0048
            a.ql r2 = r6.cFF()
            java.lang.String r2 = r2.getString()
            java.lang.String r3 = "eval"
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x0016
        L_0x0048:
            r0 = r1
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C7009ayw.mo17109b(int, a.ql):a.ql");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public org.mozilla1.javascript.Node mo17095a(int i, boolean z, org.mozilla1.javascript.Node qlVar) {
        String str;
        org.mozilla1.javascript.Node C = m27499C(qlVar);
        if (C == null) {
            if (i == 106) {
                str = "msg.bad.decr";
            } else {
                str = "msg.bad.incr";
            }
            this.f5656iG.mo16821kC(str);
            return null;
        }
        switch (C.getType()) {
            case 33:
            case 36:
            case 39:
            case 67:
                org.mozilla1.javascript.Node qlVar2 = new org.mozilla1.javascript.Node(i, C);
                int i2 = 0;
                if (i == 106) {
                    i2 = 1;
                }
                if (z) {
                    i2 |= 2;
                }
                qlVar2.putIntProp(13, i2);
                return qlVar2;
            default:
                throw org.mozilla1.javascript.Kit.codeBug();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public org.mozilla1.javascript.Node mo17103a(org.mozilla1.javascript.Node qlVar, String str, String str2, int i) {
        if (str != null || i != 0) {
            return m27504b(qlVar, str, mo17128kG(str2), i | 1);
        }
        if (qlVar == null) {
            return mo17127kF(str2);
        }
        m27507x(str2, 33);
        if (!org.mozilla1.javascript.ScriptRuntime.m7598nm(str2)) {
            return new org.mozilla1.javascript.Node(33, qlVar, mo17128kG(str2));
        }
        org.mozilla1.javascript.Node qlVar2 = new org.mozilla1.javascript.Node(71, qlVar);
        qlVar2.putProp(17, str2);
        return new org.mozilla1.javascript.Node(67, qlVar2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public org.mozilla1.javascript.Node mo17102a(org.mozilla1.javascript.Node qlVar, String str, org.mozilla1.javascript.Node qlVar2, int i) {
        if (str != null || i != 0) {
            return m27504b(qlVar, str, qlVar2, i);
        }
        if (qlVar != null) {
            return new org.mozilla1.javascript.Node(36, qlVar, qlVar2);
        }
        throw org.mozilla1.javascript.Kit.codeBug();
    }

    /* renamed from: b */
    private org.mozilla1.javascript.Node m27504b(org.mozilla1.javascript.Node qlVar, String str, org.mozilla1.javascript.Node qlVar2, int i) {
        org.mozilla1.javascript.Node qlVar3;
        org.mozilla1.javascript.Node qlVar4;
        if (str == null) {
            qlVar3 = null;
        } else if (str.equals("*")) {
            qlVar3 = new org.mozilla1.javascript.Node(42);
        } else {
            qlVar3 = mo17127kF(str);
        }
        if (qlVar == null) {
            if (str == null) {
                qlVar4 = new org.mozilla1.javascript.Node(78, qlVar2);
            } else {
                qlVar4 = new org.mozilla1.javascript.Node(79, qlVar3, qlVar2);
            }
        } else if (str == null) {
            qlVar4 = new org.mozilla1.javascript.Node(76, qlVar, qlVar2);
        } else {
            qlVar4 = new org.mozilla1.javascript.Node(77, qlVar, qlVar3, qlVar2);
        }
        if (i != 0) {
            qlVar4.putIntProp(16, i);
        }
        return new org.mozilla1.javascript.Node(67, qlVar4);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public org.mozilla1.javascript.Node mo17113c(int i, org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        String numberToString = null;
        switch (i) {
            case 21:
                if (qlVar.type == 41) {
                    if (qlVar2.type == 41) {
                        numberToString = qlVar2.getString();
                    } else if (qlVar2.type == 40) {
                        numberToString = org.mozilla1.javascript.ScriptRuntime.numberToString(qlVar2.getDouble(), 10);
                    }
                    qlVar.setString(qlVar.getString().concat(numberToString));
                    return qlVar;
                } else if (qlVar.type == 40) {
                    if (qlVar2.type == 40) {
                        qlVar.setDouble(qlVar.getDouble() + qlVar2.getDouble());
                        return qlVar;
                    } else if (qlVar2.type == 41) {
                        qlVar2.setString(org.mozilla1.javascript.ScriptRuntime.numberToString(qlVar.getDouble(), 10).concat(qlVar2.getString()));
                        return qlVar2;
                    }
                }
                break;
            case 22:
                if (qlVar.type == 40) {
                    double d = qlVar.getDouble();
                    if (qlVar2.type == 40) {
                        qlVar.setDouble(d - qlVar2.getDouble());
                        return qlVar;
                    } else if (d == org.mozilla1.javascript.ScriptRuntime.NaN) {
                        return new org.mozilla1.javascript.Node(29, qlVar2);
                    }
                } else if (qlVar2.type == 40 && qlVar2.getDouble() == org.mozilla1.javascript.ScriptRuntime.NaN) {
                    return new org.mozilla1.javascript.Node(28, qlVar);
                }
                break;
            case 23:
                if (qlVar.type == 40) {
                    double d2 = qlVar.getDouble();
                    if (qlVar2.type == 40) {
                        qlVar.setDouble(d2 * qlVar2.getDouble());
                        return qlVar;
                    } else if (d2 == 1.0d) {
                        return new org.mozilla1.javascript.Node(28, qlVar2);
                    }
                } else if (qlVar2.type == 40 && qlVar2.getDouble() == 1.0d) {
                    return new org.mozilla1.javascript.Node(28, qlVar);
                }
                break;
            case 24:
                if (qlVar2.type == 40) {
                    double d3 = qlVar2.getDouble();
                    if (qlVar.type == 40) {
                        qlVar.setDouble(qlVar.getDouble() / d3);
                        return qlVar;
                    } else if (d3 == 1.0d) {
                        return new org.mozilla1.javascript.Node(28, qlVar);
                    }
                }
                break;
            case 103:
                int D = m27500D(qlVar);
                if (D == 1) {
                    return qlVar;
                }
                if (D == -1) {
                    return qlVar2;
                }
                break;
            case 104:
                int D2 = m27500D(qlVar);
                if (D2 == -1) {
                    return qlVar;
                }
                if (D2 == 1) {
                    return qlVar2;
                }
                break;
        }
        return new org.mozilla1.javascript.Node(i, qlVar, qlVar2);
    }

    /* renamed from: n */
    private org.mozilla1.javascript.Node m27506n(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        int i;
        int type = qlVar.getType();
        switch (type) {
            case 33:
            case 36:
                org.mozilla1.javascript.Node cFE = qlVar.cFE();
                org.mozilla1.javascript.Node cFF = qlVar.cFF();
                if (type == 33) {
                    i = 35;
                } else {
                    i = 37;
                }
                return new org.mozilla1.javascript.Node(i, cFE, cFF, qlVar2);
            case 39:
                qlVar.setType(49);
                return new org.mozilla1.javascript.Node(8, qlVar, qlVar2);
            case 67:
                org.mozilla1.javascript.Node cFE2 = qlVar.cFE();
                m27498A(cFE2);
                return new org.mozilla1.javascript.Node(68, cFE2, qlVar2);
            default:
                throw org.mozilla1.javascript.Kit.codeBug();
        }
    }

    /* renamed from: A */
    private void m27498A(org.mozilla1.javascript.Node qlVar) {
        if ((qlVar.getIntProp(16, 0) & 4) != 0) {
            this.f5656iG.mo16821kC("msg.bad.assign.left");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public org.mozilla1.javascript.Node mo17116d(int i, org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        int i2;
        int i3;
        org.mozilla1.javascript.Node C = m27499C(qlVar);
        if (C != null) {
            switch (i) {
                case 89:
                    return m27506n(C, qlVar2);
                case 90:
                    i2 = 9;
                    break;
                case 91:
                    i2 = 10;
                    break;
                case 92:
                    i2 = 11;
                    break;
                case 93:
                    i2 = 18;
                    break;
                case 94:
                    i2 = 19;
                    break;
                case 95:
                    i2 = 20;
                    break;
                case 96:
                    i2 = 21;
                    break;
                case 97:
                    i2 = 22;
                    break;
                case 98:
                    i2 = 23;
                    break;
                case 99:
                    i2 = 24;
                    break;
                case 100:
                    i2 = 25;
                    break;
                default:
                    throw org.mozilla1.javascript.Kit.codeBug();
            }
            int type = C.getType();
            switch (type) {
                case 33:
                case 36:
                    org.mozilla1.javascript.Node cFE = C.cFE();
                    org.mozilla1.javascript.Node cFF = C.cFF();
                    if (type == 33) {
                        i3 = 138;
                    } else {
                        i3 = 139;
                    }
                    return new org.mozilla1.javascript.Node(i3, cFE, cFF, new org.mozilla1.javascript.Node(i2, new org.mozilla1.javascript.Node(137), qlVar2));
                case 39:
                    return new org.mozilla1.javascript.Node(8, org.mozilla1.javascript.Node.m37828e(49, C.getString()), new org.mozilla1.javascript.Node(i2, C, qlVar2));
                case 67:
                    org.mozilla1.javascript.Node cFE2 = C.cFE();
                    m27498A(cFE2);
                    return new org.mozilla1.javascript.Node(141, cFE2, new org.mozilla1.javascript.Node(i2, new org.mozilla1.javascript.Node(137), qlVar2));
                default:
                    throw org.mozilla1.javascript.Kit.codeBug();
            }
        } else if (qlVar.getType() != 65 && qlVar.getType() != 66) {
            this.f5656iG.mo16821kC("msg.bad.assign.left");
            return qlVar2;
        } else if (i == 89) {
            return mo17119e(-1, qlVar, qlVar2);
        } else {
            this.f5656iG.mo16821kC("msg.bad.destruct.op");
            return qlVar2;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public org.mozilla1.javascript.Node mo17119e(int i, org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        String cHs = this.f5656iG.gPl.cHs();
        org.mozilla1.javascript.Node a = m27501a(i, qlVar, qlVar2, cHs);
        a.cFF().mo21461G(mo17127kF(cHs));
        return a;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    private org.mozilla1.javascript.Node m27501a(int i, org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, String str) {
        int i2;
        boolean z;
        org.mozilla1.javascript.Node qlVar3;
        org.mozilla1.javascript.Node am = mo17108am(157, this.f5656iG.cCd());
        am.mo21460F(new org.mozilla1.javascript.Node(152, m27502a(39, str, qlVar2)));
        try {
            this.f5656iG.mo16823u(am);
            this.f5656iG.mo16811a(152, true, str);
            this.f5656iG.popScope();
            org.mozilla1.javascript.Node qlVar4 = new org.mozilla1.javascript.Node(88);
            am.mo21461G(qlVar4);
            if (i == 153) {
                i2 = 154;
            } else {
                i2 = 8;
            }
            ArrayList arrayList = new ArrayList();
            boolean z2 = true;
            int type = qlVar.getType();
            if (type == 65) {
                int i3 = 0;
                int[] iArr = (int[]) qlVar.getProp(11);
                int i4 = 0;
                org.mozilla1.javascript.Node cFE = qlVar.cFE();
                while (true) {
                    if (iArr != null) {
                        while (i4 < iArr.length && iArr[i4] == i3) {
                            i4++;
                            i3++;
                        }
                    }
                    if (cFE == null) {
                        break;
                    }
                    org.mozilla1.javascript.Node qlVar5 = new org.mozilla1.javascript.Node(36, mo17127kF(str), mo17092V((double) i3));
                    if (cFE.getType() == 39) {
                        String string = cFE.getString();
                        qlVar4.mo21461G(new org.mozilla1.javascript.Node(i2, m27502a(49, string, (org.mozilla1.javascript.Node) null), qlVar5));
                        if (i != -1) {
                            this.f5656iG.mo16811a(i, true, string);
                            arrayList.add(string);
                        }
                    } else {
                        qlVar4.mo21461G(m27501a(i, cFE, qlVar5, this.f5656iG.gPl.cHs()));
                    }
                    i3++;
                    z2 = false;
                    cFE = cFE.cFG();
                }
                z = z2;
            } else if (type == 66) {
                int i5 = 0;
                Object[] objArr = (Object[]) qlVar.getProp(12);
                org.mozilla1.javascript.Node cFE2 = qlVar.cFE();
                boolean z3 = true;
                while (cFE2 != null) {
                    Object obj = objArr[i5];
                    if (obj instanceof String) {
                        qlVar3 = new org.mozilla1.javascript.Node(33, mo17127kF(str), mo17128kG((String) obj));
                    } else {
                        qlVar3 = new org.mozilla1.javascript.Node(36, mo17127kF(str), mo17092V((double) ((Number) obj).intValue()));
                    }
                    if (cFE2.getType() == 39) {
                        String string2 = cFE2.getString();
                        qlVar4.mo21461G(new org.mozilla1.javascript.Node(i2, m27502a(49, string2, (org.mozilla1.javascript.Node) null), qlVar3));
                        if (i != -1) {
                            this.f5656iG.mo16811a(i, true, string2);
                            arrayList.add(string2);
                        }
                    } else {
                        qlVar4.mo21461G(m27501a(i, cFE2, qlVar3, this.f5656iG.gPl.cHs()));
                    }
                    z3 = false;
                    cFE2 = cFE2.cFG();
                    i5++;
                }
                z = z3;
            } else if (type == 33 || type == 36) {
                qlVar4.mo21461G(m27506n(qlVar, mo17127kF(str)));
                z = true;
            } else {
                this.f5656iG.mo16821kC("msg.bad.assign.left");
                z = true;
            }
            if (z) {
                qlVar4.mo21461G(mo17092V(ScriptRuntime.NaN));
            }
            am.putProp(22, arrayList);
            return am;
        } catch (Throwable th) {
            this.f5656iG.popScope();
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: B */
    public org.mozilla1.javascript.Node mo17091B(org.mozilla1.javascript.Node qlVar) {
        if (140 != qlVar.getType()) {
            throw Kit.codeBug();
        }
        org.mozilla1.javascript.Node qlVar2 = new org.mozilla1.javascript.Node(54);
        qlVar2.putProp(3, qlVar);
        return qlVar2;
    }

    /* renamed from: c */
    private org.mozilla1.javascript.Node.C3367a m27505c(int i, org.mozilla1.javascript.Node qlVar) {
        org.mozilla1.javascript.Node.C3367a aVar = new org.mozilla1.javascript.Node.C3367a(i);
        aVar.aUM = qlVar;
        return aVar;
    }

    /* renamed from: C */
    private org.mozilla1.javascript.Node m27499C(org.mozilla1.javascript.Node qlVar) {
        switch (qlVar.getType()) {
            case 33:
            case 36:
            case 39:
            case 67:
                return qlVar;
            case 38:
                qlVar.setType(70);
                return new Node(67, qlVar);
            default:
                return null;
        }
    }

    /* renamed from: x */
    private void m27507x(String str, int i) {
        boolean z = true;
        if (this.f5656iG.cCk()) {
            if (!"arguments".equals(str) && ((this.f5656iG.f5571he.elF == null || !this.f5656iG.f5571he.elF.contains(str)) && !("length".equals(str) && i == 33 && this.f5656iG.f5571he.getLanguageVersion() == 120))) {
                z = false;
            }
            if (z) {
                cDH();
            }
        }
    }

    private void cDH() {
        if (this.f5656iG.cCk()) {
            ((FunctionNode) this.f5656iG.gPl).eMO = true;
        }
    }

    private void cDI() {
        if (this.f5656iG.cCk()) {
            ((FunctionNode) this.f5656iG.gPl).eMQ = true;
        }
    }
}
