package org.mozilla1.javascript;

import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.ProtectionDomain;

/* renamed from: a.yB */
/* compiled from: a */
public class SecurityUtilities {
    public static String getSystemProperty(String str) {
        return (String) AccessController.doPrivileged(new C4013b(str));
    }

    /* renamed from: l */
    public static ProtectionDomain m41206l(Class<?> cls) {
        return (ProtectionDomain) AccessController.doPrivileged(new C4012a(cls));
    }

    /* renamed from: a.yB$b */
    /* compiled from: a */
    class C4013b implements PrivilegedAction<Object> {
        private final /* synthetic */ String fRE;

        C4013b(String str) {
            this.fRE = str;
        }

        public Object run() {
            return System.getProperty(this.fRE);
        }
    }

    /* renamed from: a.yB$a */
    class C4012a implements PrivilegedAction<Object> {
        private final /* synthetic */ Class fOZ;

        C4012a(Class cls) {
            this.fOZ = cls;
        }

        public Object run() {
            return this.fOZ.getProtectionDomain();
        }
    }
}
