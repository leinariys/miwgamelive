package org.mozilla1.javascript;

/* renamed from: a.aVF */
/* compiled from: a */
public interface Scriptable {
    public static final Object NOT_FOUND = UniqueTag.f5854nb;

    void delete(int i);

    void delete(String str);

    Object get(int i, Scriptable avf);

    Object get(String str, Scriptable avf);

    String getClassName();

    Object getDefaultValue(Class<?> cls);

    Object[] getIds();

    Scriptable getParentScope();

    void setParentScope(Scriptable avf);

    Scriptable getPrototype();

    void setPrototype(Scriptable avf);

    boolean has(int i, Scriptable avf);

    boolean has(String str, Scriptable avf);

    boolean hasInstance(Scriptable avf);

    void put(int i, Scriptable avf, Object obj);

    void put(String str, Scriptable avf, Object obj);
}
