package org.mozilla1.javascript;

import org.mozilla1.javascript.debug.DebuggableScript;
import org.mozilla1.javascript.debug.Debugger;
import org.mozilla1.javascript.xml.XMLLib;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

/* renamed from: a.lh */
/* compiled from: a */
public class Context implements Serializable {
    public static final int FEATURE_DYNAMIC_SCOPE = 7;
    public static final int FEATURE_E4X = 6;
    public static final int FEATURE_MEMBER_EXPR_AS_FUNCTION_NAME = 2;
    public static final int FEATURE_NON_ECMA_GET_YEAR = 1;
    public static final int FEATURE_PARENT_PROTO_PROPRTIES = 5;
    public static final int FEATURE_RESERVED_KEYWORD_AS_IDENTIFIER = 3;
    public static final int FEATURE_STRICT_EVAL = 9;
    public static final int FEATURE_STRICT_VARS = 8;
    public static final int FEATURE_TO_STRING_AS_SOURCE = 4;
    public static final int VERSION_1_0 = 100;
    public static final int VERSION_1_1 = 110;
    public static final int VERSION_1_2 = 120;
    public static final int VERSION_1_3 = 130;
    public static final int VERSION_1_4 = 140;
    public static final int VERSION_1_5 = 150;
    public static final int VERSION_1_6 = 160;
    public static final int VERSION_DEFAULT = 0;
    public static final int VERSION_UNKNOWN = -1;
    public static final int ekY = 170;
    public static final int ekZ = 5;
    public static final int ela = 10;
    public static final int elb = 11;
    public static final int elc = 12;
    public static final int eld = 13;
    public static final Object[] emptyArgs = org.mozilla1.javascript.ScriptRuntime.emptyArgs;
    public static final String errorReporterProperty = "error reporter";
    public static final String languageVersionProperty = "language version";
    /* renamed from: jR */
    static final /* synthetic */ boolean f8582jR;
    private static Class<?> ele = org.mozilla1.javascript.Kit.classOrNull("a.ae");
    private static Class<?> elf = org.mozilla1.javascript.Kit.classOrNull("a.sq");
    private static String implementationVersion = null;

    static {
        boolean z;
        if (!Context.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        f8582jR = z;
    }

    private final ContextFactory elg;
    public boolean elN;
    org.mozilla1.javascript.debug.Debugger elA;
    Set<String> elF;
    Object elG;
    ObjArray elH;
    int elI;
    int elJ;
    int elK;
    long elL;
    Scriptable elM;
    Scriptable topCallScope;
    boolean elj;
    NativeCall elk;
    org.mozilla1.javascript.xml.XMLLib ell;
    ObjToIntMap elm;
    Object eln;
    RegExpProxy elr;
    boolean elv;
    boolean elw;
    int version;
    private boolean dIn;
    private ClassLoader dIr;
    private Object elB;
    private int elC;
    private Object elD;
    private Map<Object, Object> elE;
    private Object elh;
    private SecurityController elo;
    private org.mozilla1.javascript.ClassShutter elp;
    private ErrorReporter elq;
    private boolean els;
    private boolean elt;
    private boolean elu;
    private int elx;
    private int ely;
    private C1626Xo elz;
    private Locale locale;

    public Context() {
        this(ContextFactory.getGlobal());
    }

    Context(ContextFactory iwVar) {
        int i = 0;
        this.elu = true;
        this.elN = false;
        if (f8582jR || iwVar != null) {
            this.elg = iwVar;
            setLanguageVersion(0);
            this.elx = ele == null ? -1 : i;
            this.ely = Integer.MAX_VALUE;
            return;
        }
        throw new AssertionError();
    }

    public static Context bwq() {
        return org.mozilla1.javascript.VMBridge.iEO.mo2803R(org.mozilla1.javascript.VMBridge.iEO.getThreadContextHelper());
    }

    public static Context call() {
        return call((Context) null);
    }

    /* renamed from: c */
    public static Context call(Context lhVar) {
        return call(lhVar, ContextFactory.getGlobal());
    }

    /* renamed from: a */
    static final Context call(Context lhVar, ContextFactory iwVar) {
        Object threadContextHelper = org.mozilla1.javascript.VMBridge.iEO.getThreadContextHelper();
        Context R = org.mozilla1.javascript.VMBridge.iEO.mo2803R(threadContextHelper);
        if (R != null) {
            lhVar = R;
        } else {
            if (lhVar == null) {
                lhVar = iwVar.bkK();
                if (lhVar.elC != 0) {
                    throw new IllegalStateException("factory.makeContext() returned Context instance already associated with some thread");
                }
                iwVar.onContextCreated(lhVar);
                if (iwVar.isSealed() && !lhVar.isSealed()) {
                    lhVar.seal((Object) null);
                }
            } else if (lhVar.elC != 0) {
                throw new IllegalStateException("can not use Context instance already associated with some thread");
            }
            org.mozilla1.javascript.VMBridge.iEO.mo2806a(threadContextHelper, lhVar);
        }
        lhVar.elC++;
        return lhVar;
    }

    public static void exit() {
        Object threadContextHelper = org.mozilla1.javascript.VMBridge.iEO.getThreadContextHelper();
        Context R = org.mozilla1.javascript.VMBridge.iEO.mo2803R(threadContextHelper);
        if (R == null) {
            throw new IllegalStateException("Calling Context.exit without previous Context.enter");
        }
        if (R.elC < 1) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        int i = R.elC - 1;
        R.elC = i;
        if (i == 0) {
            org.mozilla1.javascript.VMBridge.iEO.mo2806a(threadContextHelper, (Context) null);
            R.elg.mo19796a(R);
        }
    }

    /* renamed from: a */
    public static Object m35231a(ContextAction jg) {
        return m35233a(ContextFactory.getGlobal(), jg);
    }

    /* renamed from: a */
    public static Object m35234a(ContextFactory iwVar, Callable mFVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        if (iwVar == null) {
            iwVar = ContextFactory.getGlobal();
        }
        return m35233a(iwVar, (ContextAction) new C2910a(mFVar, avf, avf2, objArr));
    }

    /* renamed from: a */
    static Object m35233a(ContextFactory iwVar, ContextAction jg) {
        try {
            return jg.run(call((Context) null, iwVar));
        } finally {
            exit();
        }
    }

    /* renamed from: a */
    public static void m35236a(org.mozilla1.javascript.ContextListener pOVar) {
        if ("org.mozilla.javascript.tools.debugger.Main".equals(pOVar.getClass().getName())) {
            Class<?> cls = pOVar.getClass();
            Class[] clsArr = {org.mozilla1.javascript.Kit.classOrNull("a.iw")};
            try {
                cls.getMethod("attachTo", clsArr).invoke(pOVar, new Object[]{ContextFactory.getGlobal()});
            } catch (Exception e) {
                RuntimeException runtimeException = new RuntimeException();
                org.mozilla1.javascript.Kit.initCause(runtimeException, e);
                throw runtimeException;
            }
        } else {
            ContextFactory.getGlobal().mo19795a((ContextFactory.C2715a) pOVar);
        }
    }

    /* renamed from: b */
    public static void m35240b(ContextListener pOVar) {
        ContextFactory.getGlobal().mo19795a((ContextFactory.C2715a) pOVar);
    }

    public static void bws() {
        throw new IllegalStateException();
    }

    public static boolean isValidLanguageVersion(int i) {
        switch (i) {
            case 0:
            case 100:
            case 110:
            case 120:
            case 130:
            case 140:
            case 150:
            case 160:
            case 170:
                return true;
            default:
                return false;
        }
    }

    public static void checkLanguageVersion(int i) {
        if (!isValidLanguageVersion(i)) {
            throw new IllegalArgumentException("Bad language version: " + i);
        }
    }

    public static void reportWarning(String str, String str2, int i, String str3, int i2) {
        Context bwA = bwA();
        if (bwA.hasFeature(12)) {
            reportError(str, str2, i, str3, i2);
        } else {
            bwA.bwt().warning(str, str2, i, str3, i2);
        }
    }

    public static void reportWarning(String str) {
        int[] iArr = new int[1];
        reportWarning(str, m35242c(iArr), iArr[0], (String) null, 0);
    }

    /* renamed from: c */
    public static void m35243c(String str, Throwable th) {
        int[] iArr = new int[1];
        String c = m35242c(iArr);
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        printWriter.println(str);
        th.printStackTrace(printWriter);
        printWriter.flush();
        reportWarning(stringWriter.toString(), c, iArr[0], (String) null, 0);
    }

    public static void reportError(String str, String str2, int i, String str3, int i2) {
        Context bwq = bwq();
        if (bwq != null) {
            bwq.bwt().error(str, str2, i, str3, i2);
            return;
        }
        throw new EvaluatorException(str, str2, i, str3, i2);
    }

    public static void reportError(String str) {
        int[] iArr = new int[1];
        reportError(str, m35242c(iArr), iArr[0], (String) null, 0);
    }

    /* renamed from: a */
    public static EvaluatorException m35227a(String str, String str2, int i, String str3, int i2) {
        Context bwq = bwq();
        if (bwq != null) {
            return bwq.bwt().runtimeError(str, str2, i, str3, i2);
        }
        throw new EvaluatorException(str, str2, i, str3, i2);
    }

    /* renamed from: gE */
    static EvaluatorException m35244gE(String str) {
        return m35245gF(org.mozilla1.javascript.ScriptRuntime.getMessage0(str));
    }

    /* renamed from: q */
    static EvaluatorException m35246q(String str, Object obj) {
        return m35245gF(org.mozilla1.javascript.ScriptRuntime.getMessage1(str, obj));
    }

    /* renamed from: a */
    public static EvaluatorException m35224a(String str, Object obj, Object obj2) {
        return m35245gF(org.mozilla1.javascript.ScriptRuntime.getMessage2(str, obj, obj2));
    }

    /* renamed from: a */
    static EvaluatorException m35225a(String str, Object obj, Object obj2, Object obj3) {
        return m35245gF(org.mozilla1.javascript.ScriptRuntime.getMessage3(str, obj, obj2, obj3));
    }

    /* renamed from: a */
    static EvaluatorException m35226a(String str, Object obj, Object obj2, Object obj3, Object obj4) {
        return m35245gF(org.mozilla1.javascript.ScriptRuntime.getMessage4(str, obj, obj2, obj3, obj4));
    }

    /* renamed from: gF */
    public static EvaluatorException m35245gF(String str) {
        int[] iArr = new int[1];
        return m35227a(str, m35242c(iArr), iArr[0], (String) null, 0);
    }

    public static Object getUndefinedValue() {
        return Undefined.instance;
    }

    public static boolean toBoolean(Object obj) {
        return org.mozilla1.javascript.ScriptRuntime.toBoolean(obj);
    }

    public static double toNumber(Object obj) {
        return org.mozilla1.javascript.ScriptRuntime.toNumber(obj);
    }

    public static String toString(Object obj) {
        return org.mozilla1.javascript.ScriptRuntime.toString(obj);
    }

    /* renamed from: a */
    public static Scriptable m35228a(Object obj, Scriptable avf) {
        return org.mozilla1.javascript.ScriptRuntime.m7554c(avf, obj);
    }

    /* renamed from: a */
    public static Scriptable m35229a(Object obj, Scriptable avf, Class<?> cls) {
        return org.mozilla1.javascript.ScriptRuntime.m7554c(avf, obj);
    }

    /* renamed from: b */
    public static Object m35239b(Object obj, Scriptable avf) {
        if ((obj instanceof String) || (obj instanceof Number) || (obj instanceof Boolean) || (obj instanceof Scriptable)) {
            return obj;
        }
        if (obj instanceof Character) {
            return String.valueOf(((Character) obj).charValue());
        }
        Context bwA = bwA();
        return bwA.bwy().mo6829a(bwA, avf, obj, (Class<?>) null);
    }

    public static Object jsToJava(Object obj, Class<?> cls) {
        return NativeJavaObject.m29542a(cls, obj);
    }

    public static Object toType(Object obj, Class<?> cls) {
        try {
            return jsToJava(obj, cls);
        } catch (EvaluatorException e) {
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException(e.getMessage());
            org.mozilla1.javascript.Kit.initCause(illegalArgumentException, e);
            throw illegalArgumentException;
        }
    }

    public static RuntimeException throwAsScriptRuntimeEx(Throwable th) {
        Context bwA;
        Throwable th2 = th;
        while (th2 instanceof InvocationTargetException) {
            th2 = ((InvocationTargetException) th2).getTargetException();
        }
        if ((th2 instanceof Error) && ((bwA = bwA()) == null || !bwA.hasFeature(13))) {
            throw ((Error) th2);
        } else if (th2 instanceof RhinoException) {
            throw ((RhinoException) th2);
        } else {
            throw new WrappedException(th2);
        }
    }

    public static boolean isValidOptimizationLevel(int i) {
        return -1 <= i && i <= 9;
    }

    public static void checkOptimizationLevel(int i) {
        if (!isValidOptimizationLevel(i)) {
            throw new IllegalArgumentException("Optimization level outside [-1..9]: " + i);
        }
    }

    public static void setCachingEnabled(boolean z) {
    }

    /* renamed from: b */
    public static org.mozilla1.javascript.debug.DebuggableScript m35238b(Script afe) {
        if (afe instanceof org.mozilla1.javascript.NativeFunction) {
            return ((org.mozilla1.javascript.NativeFunction) afe).aVK();
        }
        return null;
    }

    static Context bwA() {
        Context bwq = bwq();
        if (bwq != null) {
            return bwq;
        }
        throw new RuntimeException("No Context associated with current Thread");
    }

    /* renamed from: a */
    private static void m35235a(Context lhVar, org.mozilla1.javascript.debug.DebuggableScript en, String str) {
        lhVar.elA.handleCompilationDone(lhVar, en, str);
        for (int i = 0; i != en.getFunctionCount(); i++) {
            m35235a(lhVar, en.mo1978gW(i), str);
        }
    }

    static Evaluator bwC() {
        return (Evaluator) org.mozilla1.javascript.Kit.m11148as(elf);
    }

    /* renamed from: c */
    static String m35242c(int[] iArr) {
        Evaluator bwC;
        Context bwq = bwq();
        if (bwq == null) {
            return null;
        }
        if (bwq.elG != null && (bwC = bwC()) != null) {
            return bwC.mo5924a(bwq, iArr);
        }
        CharArrayWriter charArrayWriter = new CharArrayWriter();
        new RuntimeException().printStackTrace(new PrintWriter(charArrayWriter));
        String charArrayWriter2 = charArrayWriter.toString();
        int i = -1;
        int i2 = -1;
        int i3 = -1;
        for (int i4 = 0; i4 < charArrayWriter2.length(); i4++) {
            char charAt = charArrayWriter2.charAt(i4);
            if (charAt == ':') {
                i = i4;
            } else if (charAt == '(') {
                i3 = i4;
            } else if (charAt == ')') {
                i2 = i4;
            } else if (charAt == 10 && i3 != -1 && i2 != -1 && i != -1 && i3 < i && i < i2) {
                String substring = charArrayWriter2.substring(i3 + 1, i);
                if (!substring.endsWith(".java")) {
                    try {
                        iArr[0] = Integer.parseInt(charArrayWriter2.substring(i + 1, i2));
                        if (iArr[0] < 0) {
                            iArr[0] = 0;
                        }
                        return substring;
                    } catch (NumberFormatException e) {
                    }
                }
                i = -1;
                i2 = -1;
                i3 = -1;
            }
        }
        return null;
    }

    public final ContextFactory bwr() {
        return this.elg;
    }

    public final boolean isSealed() {
        return this.dIn;
    }

    public final void seal(Object obj) {
        if (this.dIn) {
            bws();
        }
        this.dIn = true;
        this.elh = obj;
    }

    public final void unseal(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException();
        } else if (this.elh != obj) {
            throw new IllegalArgumentException();
        } else if (!this.dIn) {
            throw new IllegalStateException();
        } else {
            this.dIn = false;
            this.elh = null;
        }
    }

    public final int getLanguageVersion() {
        return this.version;
    }

    public void setLanguageVersion(int i) {
        if (this.dIn) {
            bws();
        }
        checkLanguageVersion(i);
        Object obj = this.elD;
        if (!(obj == null || i == this.version)) {
            m35237a(obj, languageVersionProperty, (Object) new Integer(this.version), (Object) new Integer(i));
        }
        this.version = i;
    }

    public final String getImplementationVersion() {
        if (implementationVersion == null) {
            implementationVersion = org.mozilla1.javascript.ScriptRuntime.getMessage0("implementation.version");
        }
        return implementationVersion;
    }

    public final ErrorReporter bwt() {
        if (this.elq == null) {
            return DefaultErrorReporter.bkW;
        }
        return this.elq;
    }

    /* renamed from: b */
    public final ErrorReporter mo20352b(ErrorReporter ale) {
        if (this.dIn) {
            bws();
        }
        if (ale == null) {
            throw new IllegalArgumentException();
        }
        ErrorReporter bwt = bwt();
        if (ale != bwt) {
            Object obj = this.elD;
            if (obj != null) {
                m35237a(obj, errorReporterProperty, (Object) bwt, (Object) ale);
            }
            this.elq = ale;
        }
        return bwt;
    }

    public final Locale getLocale() {
        if (this.locale == null) {
            this.locale = Locale.getDefault();
        }
        return this.locale;
    }

    public final Locale setLocale(Locale locale2) {
        if (this.dIn) {
            bws();
        }
        Locale locale3 = this.locale;
        this.locale = locale2;
        return locale3;
    }

    public final void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        if (this.dIn) {
            bws();
        }
        this.elD = org.mozilla1.javascript.Kit.addListener(this.elD, propertyChangeListener);
    }

    public final void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        if (this.dIn) {
            bws();
        }
        this.elD = org.mozilla1.javascript.Kit.removeListener(this.elD, propertyChangeListener);
    }

    /* access modifiers changed from: package-private */
    public final void firePropertyChange(String str, Object obj, Object obj2) {
        Object obj3 = this.elD;
        if (obj3 != null) {
            m35237a(obj3, str, obj, obj2);
        }
    }

    /* renamed from: a */
    private void m35237a(Object obj, String str, Object obj2, Object obj3) {
        int i = 0;
        while (true) {
            int i2 = i;
            Object listener = org.mozilla1.javascript.Kit.getListener(obj, i2);
            if (listener != null) {
                if (listener instanceof PropertyChangeListener) {
                    ((PropertyChangeListener) listener).propertyChange(new PropertyChangeEvent(this, str, obj2, obj3));
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public final ScriptableObject initStandardObjects() {
        return initStandardObjects((ScriptableObject) null, false);
    }

    /* renamed from: a */
    public final Scriptable initStandardObjects(ScriptableObject akn) {
        return initStandardObjects(akn, false);
    }

    /* renamed from: a */
    public ScriptableObject initStandardObjects(ScriptableObject akn, boolean z) {
        return org.mozilla1.javascript.ScriptRuntime.m7484a(this, akn, z);
    }

    /* renamed from: a */
    public final Object evaluateString(Scriptable avf, String str, String str2, int i, Object obj) {
        Script a = mo20329a(str, str2, i, obj);
        if (a != null) {
            return a.mo8800a(this, avf);
        }
        return null;
    }

    /* renamed from: a */
    public final Object evaluateReader(Scriptable avf, Reader reader, String str, int i, Object obj) {
        Script b = mo20350b(avf, reader, str, i, obj);
        if (b != null) {
            return b.mo8800a(this, avf);
        }
        return null;
    }

    /* renamed from: a */
    public Object executeScriptWithContinuations(Script afe, Scriptable avf) {
        if ((afe instanceof InterpretedFunction) && ((InterpretedFunction) afe).aBz()) {
            return mo20341a((Callable) (InterpretedFunction) afe, avf, org.mozilla1.javascript.ScriptRuntime.emptyArgs);
        }
        throw new IllegalArgumentException("Script argument was not a game.script or was not created by interpreted mode ");
    }

    /* renamed from: a */
    public Object mo20341a(Callable mFVar, Scriptable avf, Object[] objArr) {
        if (!(mFVar instanceof InterpretedFunction)) {
            throw new IllegalArgumentException("Function argument was not created by interpreted mode ");
        } else if (org.mozilla1.javascript.ScriptRuntime.m7588i(this)) {
            throw new IllegalStateException("Cannot have any pending top calls when executing a game.script with continuations");
        } else {
            this.elj = true;
            try {
                return org.mozilla1.javascript.ScriptRuntime.m7510a(mFVar, this, avf, avf, objArr);
            } catch (ContinuationPending e) {
                throw e;
            }
        }
    }

    public ContinuationPending bwv() {
        ContinuationPending hjVar = new ContinuationPending(Interpreter.m39160e(this));
        hjVar.setScope(org.mozilla1.javascript.ScriptRuntime.getTopCallScope(this));
        return hjVar;
    }

    /* renamed from: a */
    public Object mo20340a(ContinuationPending hjVar, Object obj) {
        return Interpreter.m39132a(hjVar.mo19348ya(), this, hjVar.getScope(), new Object[]{obj});
    }

    public final boolean stringIsCompilableUnit(String str) {
        boolean z;
        CompilerEnvirons aki = new CompilerEnvirons();
        aki.mo9683f(this);
        aki.setGeneratingSource(false);
        org.mozilla1.javascript.Parser axf = new org.mozilla1.javascript.Parser(aki, DefaultErrorReporter.bkW);
        try {
            axf.mo16815c(str, (String) null, 1);
            z = false;
        } catch (EvaluatorException e) {
            z = true;
        }
        if (!z || !axf.eof()) {
            return true;
        }
        return false;
    }

    /* renamed from: b */
    public final Script mo20350b(Scriptable avf, Reader reader, String str, int i, Object obj) {
        return mo20327a(reader, str, i, obj);
    }

    /* renamed from: a */
    public final Script mo20327a(Reader reader, String str, int i, Object obj) {
        int i2;
        if (i < 0) {
            i2 = 0;
        } else {
            i2 = i;
        }
        return (Script) m35232a((Scriptable) null, reader, (String) null, str, i2, obj, false, (Evaluator) null, (ErrorReporter) null);
    }

    /* renamed from: a */
    public final Script mo20329a(String str, String str2, int i, Object obj) {
        int i2;
        if (i < 0) {
            i2 = 0;
        } else {
            i2 = i;
        }
        return mo20328a(str, (Evaluator) null, (ErrorReporter) null, str2, i2, obj);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final Script mo20328a(String str, Evaluator uw, ErrorReporter ale, String str2, int i, Object obj) {
        try {
            return (Script) m35232a((Scriptable) null, (Reader) null, str, str2, i, obj, false, uw, ale);
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    /* renamed from: b */
    public final Function mo20353b(Scriptable avf, String str, String str2, int i, Object obj) {
        return mo20336a(avf, str, (Evaluator) null, (ErrorReporter) null, str2, i, obj);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final Function mo20336a(Scriptable avf, String str, Evaluator uw, ErrorReporter ale, String str2, int i, Object obj) {
        try {
            return (Function) m35232a(avf, (Reader) null, str, str2, i, obj, true, uw, ale);
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    /* renamed from: a */
    public final String mo20342a(Script afe, int i) {
        return ((NativeFunction) afe).mo2858e(i, 0);
    }

    /* renamed from: a */
    public final String mo20343a(Function azg, int i) {
        if (azg instanceof org.mozilla1.javascript.BaseFunction) {
            return ((org.mozilla1.javascript.BaseFunction) azg).mo2858e(i, 0);
        }
        return "function " + azg.getClassName() + "() {\n\t[native code]\n}\n";
    }

    /* renamed from: b */
    public final String mo20354b(Function azg, int i) {
        if (azg instanceof org.mozilla1.javascript.BaseFunction) {
            return ((BaseFunction) azg).mo2858e(i, 1);
        }
        return "[native code]\n";
    }

    /* renamed from: f */
    public final Scriptable mo20368f(Scriptable avf) {
        return mo20332a(avf, "Object", org.mozilla1.javascript.ScriptRuntime.emptyArgs);
    }

    /* renamed from: a */
    public final Scriptable mo20331a(Scriptable avf, String str) {
        return mo20332a(avf, str, org.mozilla1.javascript.ScriptRuntime.emptyArgs);
    }

    /* renamed from: a */
    public final Scriptable mo20332a(Scriptable avf, String str, Object[] objArr) {
        Scriptable x = ScriptableObject.m23595x(avf);
        Function a = org.mozilla1.javascript.ScriptRuntime.m7485a(this, x, str);
        if (objArr == null) {
            objArr = org.mozilla1.javascript.ScriptRuntime.emptyArgs;
        }
        return a.construct(this, x, objArr);
    }

    /* renamed from: a */
    public final Scriptable mo20330a(Scriptable avf, int i) {
        NativeArray ayt = new NativeArray((long) i);
        org.mozilla1.javascript.ScriptRuntime.m7528a((ScriptableObject) ayt, avf);
        return ayt;
    }

    /* renamed from: b */
    public final Scriptable mo20351b(Scriptable avf, Object[] objArr) {
        if (objArr.getClass().getComponentType() != org.mozilla1.javascript.ScriptRuntime.ObjectClass) {
            throw new IllegalArgumentException();
        }
        NativeArray ayt = new NativeArray(objArr);
        org.mozilla1.javascript.ScriptRuntime.m7528a((ScriptableObject) ayt, avf);
        return ayt;
    }

    /* renamed from: g */
    public final Object[] mo20370g(Scriptable avf) {
        return org.mozilla1.javascript.ScriptRuntime.m7611u(avf);
    }

    public final boolean isGeneratingDebug() {
        return this.els;
    }

    public final void setGeneratingDebug(boolean z) {
        if (this.dIn) {
            bws();
        }
        this.elt = true;
        if (z && getOptimizationLevel() > 0) {
            setOptimizationLevel(0);
        }
        this.els = z;
    }

    public final boolean isGeneratingSource() {
        return this.elu;
    }

    public final void setGeneratingSource(boolean z) {
        if (this.dIn) {
            bws();
        }
        this.elu = z;
    }

    public final int getOptimizationLevel() {
        return this.elx;
    }

    public final void setOptimizationLevel(int i) {
        int i2 = -1;
        if (this.dIn) {
            bws();
        }
        if (i == -2) {
            i = -1;
        }
        checkOptimizationLevel(i);
        if (ele != null) {
            i2 = i;
        }
        this.elx = i2;
    }

    public final int bww() {
        return this.ely;
    }

    /* renamed from: nv */
    public final void mo20386nv(int i) {
        if (this.dIn) {
            bws();
        }
        if (this.elx != -1) {
            throw new IllegalStateException("Cannot set maximumInterpreterStackDepth when optimizationLevel != -1");
        } else if (i < 1) {
            throw new IllegalArgumentException("Cannot set maximumInterpreterStackDepth to less than 1");
        } else {
            this.ely = i;
        }
    }

    /* renamed from: a */
    public final void mo20347a(SecurityController vzVar) {
        if (this.dIn) {
            bws();
        }
        if (vzVar == null) {
            throw new IllegalArgumentException();
        } else if (this.elo != null) {
            throw new SecurityException("Can not overwrite existing SecurityController object");
        } else if (SecurityController.hasGlobal()) {
            throw new SecurityException("Can not overwrite existing global SecurityController object");
        } else {
            this.elo = vzVar;
        }
    }

    /* renamed from: a */
    public final void mo20344a(org.mozilla1.javascript.ClassShutter ib) {
        if (this.dIn) {
            bws();
        }
        if (ib == null) {
            throw new IllegalArgumentException();
        } else if (this.elp != null) {
            throw new SecurityException("Cannot overwrite existing ClassShutter object");
        } else {
            this.elp = ib;
        }
    }

    /* access modifiers changed from: package-private */
    public final ClassShutter getClassShutter() {
        return this.elp;
    }

    public final Object getThreadLocal(Object obj) {
        if (this.elE == null) {
            return null;
        }
        return this.elE.get(obj);
    }

    public final synchronized void putThreadLocal(Object obj, Object obj2) {
        if (this.dIn) {
            bws();
        }
        if (this.elE == null) {
            this.elE = new HashMap();
        }
        this.elE.put(obj, obj2);
    }

    public final void removeThreadLocal(Object obj) {
        if (this.dIn) {
            bws();
        }
        if (this.elE != null) {
            this.elE.remove(obj);
        }
    }

    public final boolean hasCompileFunctionsWithDynamicScope() {
        return this.elv;
    }

    public final void setCompileFunctionsWithDynamicScope(boolean z) {
        if (this.dIn) {
            bws();
        }
        this.elv = z;
    }

    /* renamed from: a */
    public final void mo20345a(C1626Xo xo) {
        if (this.dIn) {
            bws();
        }
        if (xo == null) {
            throw new IllegalArgumentException();
        }
        this.elz = xo;
    }

    public final C1626Xo bwy() {
        if (this.elz == null) {
            this.elz = new C1626Xo();
        }
        return this.elz;
    }

    public final org.mozilla1.javascript.debug.Debugger bwz() {
        return this.elA;
    }

    public final Object getDebuggerContextData() {
        return this.elB;
    }

    /* renamed from: a */
    public final void mo20346a(Debugger axe, Object obj) {
        if (this.dIn) {
            bws();
        }
        this.elA = axe;
        this.elB = obj;
    }

    public boolean hasFeature(int i) {
        return bwr().hasFeature(this, i);
    }

    public XMLLib.C0026a bkM() {
        return bwr().bkM();
    }

    public final int getInstructionObserverThreshold() {
        return this.elJ;
    }

    public final void setInstructionObserverThreshold(int i) {
        if (this.dIn) {
            bws();
        }
        if (i < 0) {
            throw new IllegalArgumentException();
        }
        this.elJ = i;
        mo20367dv(i > 0);
    }

    /* renamed from: dv */
    public void mo20367dv(boolean z) {
        this.elN = z;
    }

    /* access modifiers changed from: protected */
    public void observeInstructionCount(int i) {
        bwr().mo19797a(this, i);
    }

    /* renamed from: a */
    public GeneratedClassLoader mo20335a(ClassLoader classLoader) {
        return bwr().mo19792a(classLoader);
    }

    public final ClassLoader getApplicationClassLoader() {
        if (this.dIr == null) {
            ContextFactory bwr = bwr();
            ClassLoader applicationClassLoader = bwr.getApplicationClassLoader();
            if (applicationClassLoader == null) {
                ClassLoader currentThreadClassLoader = VMBridge.iEO.getCurrentThreadClassLoader();
                if (currentThreadClassLoader != null && org.mozilla1.javascript.Kit.m11149b(currentThreadClassLoader)) {
                    return currentThreadClassLoader;
                }
                Class<?> cls = bwr.getClass();
                if (cls != ScriptRuntime.ContextFactoryClass) {
                    applicationClassLoader = cls.getClassLoader();
                } else {
                    applicationClassLoader = getClass().getClassLoader();
                }
            }
            this.dIr = applicationClassLoader;
        }
        return this.dIr;
    }

    public final void setApplicationClassLoader(ClassLoader classLoader) {
        if (this.dIn) {
            bws();
        }
        if (classLoader == null) {
            this.dIr = null;
        } else if (!org.mozilla1.javascript.Kit.m11149b(classLoader)) {
            throw new IllegalArgumentException("Loader can not resolve Rhino classes");
        } else {
            this.dIr = classLoader;
        }
    }

    /* renamed from: a */
    private Object m35232a(Scriptable avf, Reader reader, String str, String str2, int i, Object obj, boolean z, Evaluator uw, ErrorReporter ale) {
        boolean z2;
        ScriptOrFnNode a;
        boolean z3 = false;
        if (str2 == null) {
            str2 = "unnamed game.script";
        }
        if (obj == null || bwF() != null) {
            boolean z4 = reader == null;
            if (str == null) {
                z2 = true;
            } else {
                z2 = false;
            }
            if (!(z2 ^ z4)) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            if (avf == null) {
                z3 = true;
            }
            if (!(z3 ^ z)) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            CompilerEnvirons aki = new CompilerEnvirons();
            aki.mo9683f(this);
            if (ale == null) {
                ale = aki.bwt();
            }
            if (!(this.elA == null || reader == null)) {
                str = org.mozilla1.javascript.Kit.readReader(reader);
                reader = null;
            }
            org.mozilla1.javascript.Parser axf = new Parser(aki, ale);
            if (z) {
                axf.gPe = true;
            }
            if (str != null) {
                a = axf.mo16815c(str, str2, i);
            } else {
                a = axf.mo16809a(reader, str2, i);
            }
            if (!z || (a.getFunctionCount() == 1 && a.cFE() != null && a.cFE().getType() == 108)) {
                if (uw == null) {
                    uw = bwB();
                }
                Object a2 = uw.mo5922a(aki, a, axf.getEncodedSource(), z);
                if (this.elA != null) {
                    if (str == null) {
                        org.mozilla1.javascript.Kit.codeBug();
                    }
                    if (a2 instanceof org.mozilla1.javascript.debug.DebuggableScript) {
                        m35235a(this, (DebuggableScript) a2, str);
                    } else {
                        throw new RuntimeException("NOT SUPPORTED");
                    }
                }
                if (z) {
                    return uw.mo5921a(this, avf, a2, obj);
                }
                return uw.mo5920a(a2, obj);
            }
            throw new IllegalArgumentException("compileFunction only accepts source with single JS function: " + str);
        }
        throw new IllegalArgumentException("securityDomain should be null if setSecurityController() was never called");
    }

    private Evaluator bwB() {
        Evaluator uw = null;
        if (this.elx >= 0 && ele != null) {
            uw = (Evaluator) org.mozilla1.javascript.Kit.m11148as(ele);
        }
        if (uw == null) {
            return bwC();
        }
        return uw;
    }

    /* access modifiers changed from: package-private */
    public RegExpProxy bwD() {
        Class<?> classOrNull;
        if (this.elr == null && (classOrNull = org.mozilla1.javascript.Kit.classOrNull("a.aPd")) != null) {
            this.elr = (RegExpProxy) Kit.m11148as(classOrNull);
        }
        return this.elr;
    }

    /* access modifiers changed from: package-private */
    public final boolean bwE() {
        return this.version == 0 || this.version >= 130;
    }

    /* access modifiers changed from: package-private */
    public SecurityController bwF() {
        SecurityController chn = SecurityController.chn();
        return chn != null ? chn : this.elo;
    }

    public final boolean isGeneratingDebugChanged() {
        return this.elt;
    }

    public void addActivationName(String str) {
        if (this.dIn) {
            bws();
        }
        if (this.elF == null) {
            this.elF = new HashSet();
        }
        this.elF.add(str);
    }

    public final boolean isActivationNeeded(String str) {
        return this.elF != null && this.elF.contains(str);
    }

    public void removeActivationName(String str) {
        if (this.dIn) {
            bws();
        }
        if (this.elF != null) {
            this.elF.remove(str);
        }
    }

    /* renamed from: a.lh$a */
    static class C2910a implements ContextAction {
        private final /* synthetic */ Callable awX;
        private final /* synthetic */ Scriptable awY;
        private final /* synthetic */ Scriptable awZ;
        private final /* synthetic */ Object[] axa;

        C2910a(Callable mFVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
            this.awX = mFVar;
            this.awY = avf;
            this.awZ = avf2;
            this.axa = objArr;
        }

        public Object run(Context lhVar) {
            return true;
        }
    }
}