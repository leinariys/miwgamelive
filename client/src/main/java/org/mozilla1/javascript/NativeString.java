package org.mozilla1.javascript;

import java.text.Collator;

/* renamed from: a.aqq  reason: case insensitive filesystem */
/* compiled from: a */
final class NativeString extends IdScriptableObject {
    static final long serialVersionUID = 920268368584188687L;
    private static final int cBF = 4;
    private static final int ceN = 3;
    private static final int eVn = 1;
    private static final int eVp = 1;
    private static final int gpA = 6;
    private static final int gpB = 7;
    private static final int gpC = 8;
    private static final int gpD = 9;
    private static final int gpE = 10;
    private static final int gpF = 11;
    private static final int gpG = 12;
    private static final int gpH = 13;
    private static final int gpI = 14;
    private static final int gpJ = 15;
    private static final int gpK = 16;
    private static final int gpL = 17;
    private static final int gpM = 18;
    private static final int gpN = 19;
    private static final int gpO = 20;
    private static final int gpP = 21;
    private static final int gpQ = 22;
    private static final int gpR = 23;
    private static final int gpS = 24;
    private static final int gpT = 25;
    private static final int gpU = 26;
    private static final int gpV = 27;
    private static final int gpW = 28;
    private static final int gpX = 29;
    private static final int gpY = 30;
    private static final int gpZ = 31;
    private static final Object gpx = new Integer(22);
    private static final int gpy = -1;
    private static final int gpz = 5;
    private static final int gqa = 32;
    private static final int gqb = 33;
    private static final int gqc = 34;
    private static final int gqd = 35;
    private static final int gqe = 36;
    private static final int gqf = -5;
    private static final int gqg = -6;
    private static final int gqh = -7;
    private static final int gqi = -8;
    private static final int gqj = -9;
    private static final int gqk = -10;
    private static final int gql = -11;
    private static final int gqm = -12;
    private static final int gqn = -13;
    private static final int gqo = -14;
    private static final int gqp = -15;
    private static final int gqq = -30;
    private static final int gqr = -31;
    private static final int gqs = -32;
    private static final int gqt = -33;
    private static final int gqu = -34;
    private static final int gqv = -35;
    /* renamed from: ns */
    private static final int f5190ns = 1;
    /* renamed from: nt */
    private static final int f5191nt = 36;
    /* renamed from: xT */
    private static final int f5192xT = 2;
    private String string;

    private NativeString(String str) {
        this.string = str;
    }

    /* renamed from: a */
    static void m25234a(org.mozilla1.javascript.Scriptable avf, boolean z) {
        new NativeString("").mo15695a(36, avf, z);
    }

    /* renamed from: e */
    private static NativeString m25235e(org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.IdFunctionObject yy) {
        if (avf instanceof NativeString) {
            return (NativeString) avf;
        }
        throw m25341f(yy);
    }

    /* renamed from: a */
    private static String m25233a(Object obj, String str, String str2, Object[] objArr) {
        String nh = org.mozilla1.javascript.ScriptRuntime.toString(obj);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append('<');
        stringBuffer.append(str);
        if (str2 != null) {
            stringBuffer.append(' ');
            stringBuffer.append(str2);
            stringBuffer.append("=\"");
            stringBuffer.append(org.mozilla1.javascript.ScriptRuntime.toString(objArr, 0));
            stringBuffer.append('\"');
        }
        stringBuffer.append('>');
        stringBuffer.append(nh);
        stringBuffer.append("</");
        stringBuffer.append(str);
        stringBuffer.append('>');
        return stringBuffer.toString();
    }

    /* renamed from: i */
    private static int m25236i(String str, Object[] objArr) {
        double d = org.mozilla1.javascript.ScriptRuntime.NaN;
        String nh = org.mozilla1.javascript.ScriptRuntime.toString(objArr, 0);
        double integer = org.mozilla1.javascript.ScriptRuntime.toInteger(objArr, 1);
        if (integer > ((double) str.length())) {
            return -1;
        }
        if (integer >= org.mozilla1.javascript.ScriptRuntime.NaN) {
            d = integer;
        }
        return str.indexOf(nh, (int) d);
    }

    /* renamed from: j */
    private static int m25237j(String str, Object[] objArr) {
        double d = org.mozilla1.javascript.ScriptRuntime.NaN;
        String nh = org.mozilla1.javascript.ScriptRuntime.toString(objArr, 0);
        double number = org.mozilla1.javascript.ScriptRuntime.toNumber(objArr, 1);
        if (number != number || number > ((double) str.length())) {
            d = (double) str.length();
        } else if (number >= org.mozilla1.javascript.ScriptRuntime.NaN) {
            d = number;
        }
        return str.lastIndexOf(nh, (int) d);
    }

    /* renamed from: a */
    private static int m25230a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, String str, String str2, int i, RegExpProxy aqh, org.mozilla1.javascript.Scriptable avf2, int[] iArr, int[] iArr2, boolean[] zArr, String[][] strArr) {
        int i2 = iArr[0];
        int length = str.length();
        if (i == 120 && avf2 == null && str2.length() == 1 && str2.charAt(0) == ' ') {
            if (i2 == 0) {
                while (i2 < length && Character.isWhitespace(str.charAt(i2))) {
                    i2++;
                }
                iArr[0] = i2;
            }
            if (i2 == length) {
                return -1;
            }
            while (i2 < length && !Character.isWhitespace(str.charAt(i2))) {
                i2++;
            }
            int i3 = i2;
            while (i3 < length && Character.isWhitespace(str.charAt(i3))) {
                i3++;
            }
            iArr2[0] = i3 - i2;
            return i2;
        } else if (i2 > length) {
            return -1;
        } else {
            if (avf2 != null) {
                return aqh.mo10821a(lhVar, avf, str, str2, avf2, iArr, iArr2, zArr, strArr);
            }
            if (i != 0 && i < 130 && length == 0) {
                return -1;
            }
            if (str2.length() == 0) {
                if (i == 120) {
                    if (i2 != length) {
                        return i2 + 1;
                    }
                    iArr2[0] = 1;
                    return i2;
                } else if (i2 == length) {
                    return -1;
                } else {
                    return i2 + 1;
                }
            } else if (iArr[0] >= length) {
                return length;
            } else {
                int indexOf = str.indexOf(str2, iArr[0]);
                return indexOf == -1 ? length : indexOf;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00e8  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Object m25231a(org.mozilla1.javascript.Context r22, org.mozilla1.javascript.Scriptable r23, java.lang.String r24, java.lang.Object[] r25) {
        /*
            a.aVF r4 = m23595x(r23)
            java.lang.String r5 = "Array"
            r6 = 0
            r0 = r22
            a.aVF r18 = p001a.C0903NH.m7539b((p001a.C2909lh) r0, (p001a.aVF) r4, (java.lang.String) r5, (java.lang.Object[]) r6)
            r0 = r25
            int r4 = r0.length
            r5 = 1
            if (r4 >= r5) goto L_0x0020
            r4 = 0
            r0 = r18
            r1 = r18
            r2 = r24
            r0.put((int) r4, (p001a.aVF) r1, (java.lang.Object) r2)
            r4 = r18
        L_0x001f:
            return r4
        L_0x0020:
            r0 = r25
            int r4 = r0.length
            r5 = 1
            if (r4 <= r5) goto L_0x009a
            r4 = 1
            r4 = r25[r4]
            java.lang.Object r5 = org.mozilla.javascript.C0321EM.instance
            if (r4 == r5) goto L_0x009a
            r4 = 1
            r19 = r4
        L_0x0030:
            r4 = 0
            if (r19 == 0) goto L_0x010f
            r4 = 1
            r4 = r25[r4]
            long r4 = p001a.C0903NH.toUint32((java.lang.Object) r4)
            int r6 = r24.length()
            long r6 = (long) r6
            int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r6 <= 0) goto L_0x010f
            int r4 = r24.length()
            int r4 = r4 + 1
            long r4 = (long) r4
            r16 = r4
        L_0x004d:
            r7 = 0
            r4 = 1
            int[] r12 = new int[r4]
            r10 = 0
            r9 = 0
            r4 = 0
            r4 = r25[r4]
            boolean r4 = r4 instanceof p001a.aVF
            if (r4 == 0) goto L_0x006c
            a.aqH r9 = p001a.C0903NH.m7592l(r22)
            if (r9 == 0) goto L_0x006c
            r4 = 0
            r4 = r25[r4]
            a.aVF r4 = (p001a.aVF) r4
            boolean r5 = r9.mo10825m(r4)
            if (r5 == 0) goto L_0x006c
            r10 = r4
        L_0x006c:
            if (r10 != 0) goto L_0x007c
            r4 = 0
            r4 = r25[r4]
            java.lang.String r7 = p001a.C0903NH.toString((java.lang.Object) r4)
            r4 = 0
            int r5 = r7.length()
            r12[r4] = r5
        L_0x007c:
            r4 = 1
            int[] r11 = new int[r4]
            r4 = 0
            r5 = 1
            boolean[] r13 = new boolean[r5]
            r5 = 1
            java.lang.String[][] r14 = new java.lang.String[r5][]
            int r8 = r22.getLanguageVersion()
            r15 = r4
        L_0x008b:
            r4 = r22
            r5 = r23
            r6 = r24
            int r6 = m25230a(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            if (r6 >= 0) goto L_0x009e
        L_0x0097:
            r4 = r18
            goto L_0x001f
        L_0x009a:
            r4 = 0
            r19 = r4
            goto L_0x0030
        L_0x009e:
            if (r19 == 0) goto L_0x00a5
            long r4 = (long) r15
            int r4 = (r4 > r16 ? 1 : (r4 == r16 ? 0 : -1))
            if (r4 >= 0) goto L_0x0097
        L_0x00a5:
            int r4 = r24.length()
            if (r6 > r4) goto L_0x0097
            int r4 = r24.length()
            if (r4 != 0) goto L_0x00e8
            r4 = r24
        L_0x00b3:
            r0 = r18
            r1 = r18
            r0.put((int) r15, (p001a.aVF) r1, (java.lang.Object) r4)
            int r4 = r15 + 1
            if (r10 == 0) goto L_0x00ce
            r5 = 0
            boolean r5 = r13[r5]
            if (r5 == 0) goto L_0x00ce
            r5 = 0
            r5 = r14[r5]
            int r15 = r5.length
            r5 = 0
        L_0x00c8:
            if (r5 < r15) goto L_0x00f2
        L_0x00ca:
            r5 = 0
            r15 = 0
            r13[r5] = r15
        L_0x00ce:
            r5 = 0
            r15 = 0
            r15 = r12[r15]
            int r6 = r6 + r15
            r11[r5] = r6
            r5 = 130(0x82, float:1.82E-43)
            if (r8 >= r5) goto L_0x00e6
            if (r8 == 0) goto L_0x00e6
            if (r19 != 0) goto L_0x00e6
            r5 = 0
            r5 = r11[r5]
            int r6 = r24.length()
            if (r5 == r6) goto L_0x0097
        L_0x00e6:
            r15 = r4
            goto L_0x008b
        L_0x00e8:
            r4 = 0
            r4 = r11[r4]
            r0 = r24
            java.lang.String r4 = r0.substring(r4, r6)
            goto L_0x00b3
        L_0x00f2:
            if (r19 == 0) goto L_0x00fb
            long r0 = (long) r4
            r20 = r0
            int r20 = (r20 > r16 ? 1 : (r20 == r16 ? 0 : -1))
            if (r20 >= 0) goto L_0x00ca
        L_0x00fb:
            r20 = 0
            r20 = r14[r20]
            r20 = r20[r5]
            r0 = r18
            r1 = r18
            r2 = r20
            r0.put((int) r4, (p001a.aVF) r1, (java.lang.Object) r2)
            int r4 = r4 + 1
            int r5 = r5 + 1
            goto L_0x00c8
        L_0x010f:
            r16 = r4
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: NativeString.m25231a(a.lh, a.aVF, java.lang.String, java.lang.Object[]):java.lang.Object");
    }

    /* renamed from: a */
    private static String m25232a(org.mozilla1.javascript.Context lhVar, String str, Object[] objArr) {
        double d;
        double d2;
        int length = str.length();
        double integer = org.mozilla1.javascript.ScriptRuntime.toInteger(objArr, 0);
        if (integer < org.mozilla1.javascript.ScriptRuntime.NaN) {
            integer = 0.0d;
        } else if (integer > ((double) length)) {
            integer = (double) length;
        }
        if (objArr.length <= 1 || objArr[1] == Undefined.instance) {
            d2 = (double) length;
            d = integer;
        } else {
            double integer2 = org.mozilla1.javascript.ScriptRuntime.toInteger(objArr[1]);
            if (integer2 < org.mozilla1.javascript.ScriptRuntime.NaN) {
                integer2 = 0.0d;
            } else if (integer2 > ((double) length)) {
                integer2 = (double) length;
            }
            if (integer2 >= integer) {
                d2 = integer2;
                d = integer;
            } else if (lhVar.getLanguageVersion() != 120) {
                d2 = integer;
                d = integer2;
            } else {
                d2 = integer;
                d = integer;
            }
        }
        return str.substring((int) d, (int) d2);
    }

    /* renamed from: k */
    private static String m25238k(String str, Object[] objArr) {
        double d;
        double d2 = org.mozilla1.javascript.ScriptRuntime.NaN;
        if (objArr.length < 1) {
            return str;
        }
        double integer = org.mozilla1.javascript.ScriptRuntime.toInteger(objArr[0]);
        int length = str.length();
        if (integer < org.mozilla1.javascript.ScriptRuntime.NaN) {
            integer += (double) length;
            if (integer < org.mozilla1.javascript.ScriptRuntime.NaN) {
                integer = 0.0d;
            }
        } else if (integer > ((double) length)) {
            integer = (double) length;
        }
        if (objArr.length == 1) {
            d = (double) length;
        } else {
            double integer2 = org.mozilla1.javascript.ScriptRuntime.toInteger(objArr[1]);
            if (integer2 >= org.mozilla1.javascript.ScriptRuntime.NaN) {
                d2 = integer2;
            }
            d = d2 + integer;
            if (d > ((double) length)) {
                d = (double) length;
            }
        }
        return str.substring((int) integer, (int) d);
    }

    /* renamed from: l */
    private static String m25239l(String str, Object[] objArr) {
        int length = objArr.length;
        if (length == 0) {
            return str;
        }
        if (length == 1) {
            return str.concat(org.mozilla1.javascript.ScriptRuntime.toString(objArr[0]));
        }
        int length2 = str.length();
        String[] strArr = new String[length];
        for (int i = 0; i != length; i++) {
            String nh = org.mozilla1.javascript.ScriptRuntime.toString(objArr[i]);
            strArr[i] = nh;
            length2 += nh.length();
        }
        StringBuffer stringBuffer = new StringBuffer(length2);
        stringBuffer.append(str);
        for (int i2 = 0; i2 != length; i2++) {
            stringBuffer.append(strArr[i2]);
        }
        return stringBuffer.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003c, code lost:
        if (r4 < p001a.C0903NH.NaN) goto L_0x003e;
     */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String m25240m(java.lang.String r8, java.lang.Object[] r9) {
        /*
            r7 = 1
            r2 = 0
            int r0 = r9.length
            if (r0 == 0) goto L_0x0026
            r0 = 0
            r0 = r9[r0]
            double r0 = p001a.C0903NH.toInteger((java.lang.Object) r0)
            int r6 = r8.length()
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 >= 0) goto L_0x0027
            double r4 = (double) r6
            double r0 = r0 + r4
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 >= 0) goto L_0x001c
            r0 = r2
        L_0x001c:
            int r4 = r9.length
            if (r4 != r7) goto L_0x002e
            double r2 = (double) r6
        L_0x0020:
            int r0 = (int) r0
            int r1 = (int) r2
            java.lang.String r8 = r8.substring(r0, r1)
        L_0x0026:
            return r8
        L_0x0027:
            double r4 = (double) r6
            int r4 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x001c
            double r0 = (double) r6
            goto L_0x001c
        L_0x002e:
            r4 = r9[r7]
            double r4 = p001a.C0903NH.toInteger((java.lang.Object) r4)
            int r7 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r7 >= 0) goto L_0x0044
            double r6 = (double) r6
            double r4 = r4 + r6
            int r6 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r6 >= 0) goto L_0x004b
        L_0x003e:
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r4 >= 0) goto L_0x0020
            r2 = r0
            goto L_0x0020
        L_0x0044:
            double r2 = (double) r6
            int r2 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r2 <= 0) goto L_0x004b
            double r2 = (double) r6
            goto L_0x003e
        L_0x004b:
            r2 = r4
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: NativeString.m25240m(java.lang.String, java.lang.Object[]):java.lang.String");
    }

    public String getClassName() {
        return "String";
    }

    /* access modifiers changed from: protected */
    public int getMaxInstanceId() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public int findInstanceIdInfo(String str) {
        if (str.equals("length")) {
            return instanceIdInfo(7, 1);
        }
        return super.findInstanceIdInfo(str);
    }

    /* access modifiers changed from: protected */
    public String getInstanceIdName(int i) {
        if (i == 1) {
            return "length";
        }
        return super.getInstanceIdName(i);
    }

    /* access modifiers changed from: protected */
    public Object getInstanceIdValue(int i) {
        if (i == 1) {
            return org.mozilla1.javascript.ScriptRuntime.wrapInt(this.string.length());
        }
        return super.getInstanceIdValue(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo1118a(org.mozilla1.javascript.IdFunctionObject yy) {
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, -1, "fromCharCode", 1);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, (int) gqf, "charAt", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, (int) gqg, "charCodeAt", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, (int) gqh, "indexOf", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, (int) gqi, "lastIndexOf", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, (int) gqj, "split", 3);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, -10, "substring", 3);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, (int) gql, "toLowerCase", 1);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, (int) gqm, "toUpperCase", 1);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, (int) gqn, "substr", 3);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, (int) gqo, "concat", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, (int) gqp, "slice", 3);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, (int) gqq, "equalsIgnoreCase", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, (int) gqr, "match", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, (int) gqs, "search", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, (int) gqt, "replace", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, (int) gqu, "localeCompare", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, (int) gqv, "toLocaleLowerCase", 1);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gpx, -1, "fromCharCode", 1);
        super.mo1118a(yy);
    }

    /* access modifiers changed from: protected */
    public void initPrototypeId(int i) {
        String str;
        int i2 = 1;
        switch (i) {
            case 1:
                str = "constructor";
                break;
            case 2:
                i2 = 0;
                str = "toString";
                break;
            case 3:
                i2 = 0;
                str = "toSource";
                break;
            case 4:
                i2 = 0;
                str = "valueOf";
                break;
            case 5:
                str = "charAt";
                break;
            case 6:
                str = "charCodeAt";
                break;
            case 7:
                str = "indexOf";
                break;
            case 8:
                str = "lastIndexOf";
                break;
            case 9:
                i2 = 2;
                str = "split";
                break;
            case 10:
                i2 = 2;
                str = "substring";
                break;
            case 11:
                i2 = 0;
                str = "toLowerCase";
                break;
            case 12:
                i2 = 0;
                str = "toUpperCase";
                break;
            case 13:
                i2 = 2;
                str = "substr";
                break;
            case 14:
                str = "concat";
                break;
            case 15:
                i2 = 2;
                str = "slice";
                break;
            case 16:
                i2 = 0;
                str = "bold";
                break;
            case 17:
                i2 = 0;
                str = "italics";
                break;
            case 18:
                i2 = 0;
                str = "fixed";
                break;
            case 19:
                i2 = 0;
                str = "strike";
                break;
            case 20:
                i2 = 0;
                str = "small";
                break;
            case 21:
                i2 = 0;
                str = "big";
                break;
            case 22:
                i2 = 0;
                str = "blink";
                break;
            case 23:
                i2 = 0;
                str = "sup";
                break;
            case 24:
                i2 = 0;
                str = "sub";
                break;
            case 25:
                i2 = 0;
                str = "fontsize";
                break;
            case 26:
                i2 = 0;
                str = "fontcolor";
                break;
            case 27:
                i2 = 0;
                str = "link";
                break;
            case 28:
                i2 = 0;
                str = "anchor";
                break;
            case 29:
                str = "equals";
                break;
            case 30:
                str = "equalsIgnoreCase";
                break;
            case 31:
                str = "match";
                break;
            case 32:
                str = "search";
                break;
            case 33:
                str = "replace";
                break;
            case 34:
                str = "localeCompare";
                break;
            case 35:
                i2 = 0;
                str = "toLocaleLowerCase";
                break;
            case 36:
                i2 = 0;
                str = "toLocaleUpperCase";
                break;
            default:
                throw new IllegalArgumentException(String.valueOf(i));
        }
        initPrototypeMethod(gpx, i, str, i2);
    }

    public Object execIdCall(IdFunctionObject yy, Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        boolean equalsIgnoreCase;
        int i = 1;
        if (!yy.hasTag(gpx)) {
            return super.execIdCall(yy, lhVar, avf, avf2, objArr);
        }
        int methodId = yy.methodId();
        Object[] objArr2 = objArr;
        org.mozilla1.javascript.Scriptable avf3 = avf2;
        while (true) {
            switch (methodId) {
                case gqv /*-35*/:
                case gqu /*-34*/:
                case gqt /*-33*/:
                case gqs /*-32*/:
                case gqr /*-31*/:
                case gqq /*-30*/:
                case gqp /*-15*/:
                case gqo /*-14*/:
                case gqn /*-13*/:
                case gqm /*-12*/:
                case gql /*-11*/:
                case -10:
                case gqj /*-9*/:
                case gqi /*-8*/:
                case gqh /*-7*/:
                case gqg /*-6*/:
                case gqf /*-5*/:
                    avf3 = org.mozilla1.javascript.ScriptRuntime.m7554c(avf, (Object) org.mozilla1.javascript.ScriptRuntime.toString(objArr2[0]));
                    Object[] objArr3 = new Object[(objArr2.length - 1)];
                    for (int i2 = 0; i2 < objArr3.length; i2++) {
                        objArr3[i2] = objArr2[i2 + 1];
                    }
                    methodId = -methodId;
                    objArr2 = objArr3;
                case -1:
                    int length = objArr2.length;
                    if (length < 1) {
                        return "";
                    }
                    StringBuffer stringBuffer = new StringBuffer(length);
                    for (int i3 = 0; i3 != length; i3++) {
                        stringBuffer.append(org.mozilla1.javascript.ScriptRuntime.toUint16(objArr2[i3]));
                    }
                    return stringBuffer.toString();
                case 1:
                    String nh = objArr2.length >= 1 ? org.mozilla1.javascript.ScriptRuntime.toString(objArr2[0]) : "";
                    if (avf3 == null) {
                        return new NativeString(nh);
                    }
                    return nh;
                case 2:
                case 4:
                    return m25235e(avf3, yy).string;
                case 3:
                    return "(new String(\"" + org.mozilla1.javascript.ScriptRuntime.escapeString(m25235e(avf3, yy).string) + "\"))";
                case 5:
                case 6:
                    String nh2 = org.mozilla1.javascript.ScriptRuntime.toString((Object) avf3);
                    double integer = org.mozilla1.javascript.ScriptRuntime.toInteger(objArr2, 0);
                    if (integer >= org.mozilla1.javascript.ScriptRuntime.NaN && integer < ((double) nh2.length())) {
                        char charAt = nh2.charAt((int) integer);
                        if (methodId == 5) {
                            return String.valueOf(charAt);
                        }
                        return org.mozilla1.javascript.ScriptRuntime.wrapInt(charAt);
                    } else if (methodId == 5) {
                        return "";
                    } else {
                        return org.mozilla1.javascript.ScriptRuntime.NaNobj;
                    }
                case 7:
                    return org.mozilla1.javascript.ScriptRuntime.wrapInt(m25236i(org.mozilla1.javascript.ScriptRuntime.toString((Object) avf3), objArr2));
                case 8:
                    return org.mozilla1.javascript.ScriptRuntime.wrapInt(m25237j(org.mozilla1.javascript.ScriptRuntime.toString((Object) avf3), objArr2));
                case 9:
                    return m25231a(lhVar, avf, org.mozilla1.javascript.ScriptRuntime.toString((Object) avf3), objArr2);
                case 10:
                    return m25232a(lhVar, org.mozilla1.javascript.ScriptRuntime.toString((Object) avf3), objArr2);
                case 11:
                    return org.mozilla1.javascript.ScriptRuntime.toString((Object) avf3).toLowerCase();
                case 12:
                    return org.mozilla1.javascript.ScriptRuntime.toString((Object) avf3).toUpperCase();
                case 13:
                    return m25238k(org.mozilla1.javascript.ScriptRuntime.toString((Object) avf3), objArr2);
                case 14:
                    return m25239l(org.mozilla1.javascript.ScriptRuntime.toString((Object) avf3), objArr2);
                case 15:
                    return m25240m(org.mozilla1.javascript.ScriptRuntime.toString((Object) avf3), objArr2);
                case 16:
                    return m25233a((Object) avf3, "b", (String) null, (Object[]) null);
                case 17:
                    return m25233a((Object) avf3, "i", (String) null, (Object[]) null);
                case 18:
                    return m25233a((Object) avf3, "tt", (String) null, (Object[]) null);
                case 19:
                    return m25233a((Object) avf3, "strike", (String) null, (Object[]) null);
                case 20:
                    return m25233a((Object) avf3, "small", (String) null, (Object[]) null);
                case 21:
                    return m25233a((Object) avf3, "big", (String) null, (Object[]) null);
                case 22:
                    return m25233a((Object) avf3, "blink", (String) null, (Object[]) null);
                case 23:
                    return m25233a((Object) avf3, "sup", (String) null, (Object[]) null);
                case 24:
                    return m25233a((Object) avf3, "sub", (String) null, (Object[]) null);
                case 25:
                    return m25233a((Object) avf3, "font", "size", objArr2);
                case 26:
                    return m25233a((Object) avf3, "font", "color", objArr2);
                case 27:
                    return m25233a((Object) avf3, "a", "href", objArr2);
                case 28:
                    return m25233a((Object) avf3, "a", "name", objArr2);
                case 29:
                case 30:
                    String nh3 = org.mozilla1.javascript.ScriptRuntime.toString((Object) avf3);
                    String nh4 = org.mozilla1.javascript.ScriptRuntime.toString(objArr2, 0);
                    if (methodId == 29) {
                        equalsIgnoreCase = nh3.equals(nh4);
                    } else {
                        equalsIgnoreCase = nh3.equalsIgnoreCase(nh4);
                    }
                    return org.mozilla1.javascript.ScriptRuntime.wrapBoolean(equalsIgnoreCase);
                case 31:
                case 32:
                case 33:
                    if (methodId != 31) {
                        if (methodId == 32) {
                            i = 3;
                        } else {
                            i = 2;
                        }
                    }
                    return org.mozilla1.javascript.ScriptRuntime.m7593m(lhVar).mo10822a(lhVar, avf, avf3, objArr2, i);
                case 34:
                    Collator instance = Collator.getInstance(lhVar.getLocale());
                    instance.setStrength(3);
                    instance.setDecomposition(1);
                    return org.mozilla1.javascript.ScriptRuntime.wrapNumber((double) instance.compare(org.mozilla1.javascript.ScriptRuntime.toString((Object) avf3), org.mozilla1.javascript.ScriptRuntime.toString(objArr2, 0)));
                case 35:
                    return org.mozilla1.javascript.ScriptRuntime.toString((Object) avf3).toLowerCase(lhVar.getLocale());
                case 36:
                    return ScriptRuntime.toString((Object) avf3).toUpperCase(lhVar.getLocale());
                default:
                    throw new IllegalArgumentException(String.valueOf(methodId));
            }
        }
    }

    public String toString() {
        return this.string;
    }

    public Object get(int i, org.mozilla1.javascript.Scriptable avf) {
        if (i < 0 || i >= this.string.length()) {
            return super.get(i, avf);
        }
        return this.string.substring(i, i + 1);
    }

    public void put(int i, Scriptable avf, Object obj) {
        if (i < 0 || i >= this.string.length()) {
            super.put(i, avf, obj);
        }
    }

    /* access modifiers changed from: package-private */
    public int getLength() {
        return this.string.length();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int findPrototypeId(java.lang.String r8) {
        /*
            r7 = this;
            r1 = 4
            r3 = 2
            r6 = 115(0x73, float:1.61E-43)
            r4 = 1
            r0 = 0
            r2 = 0
            int r5 = r8.length()
            switch(r5) {
                case 3: goto L_0x001a;
                case 4: goto L_0x005f;
                case 5: goto L_0x0075;
                case 6: goto L_0x009f;
                case 7: goto L_0x00d2;
                case 8: goto L_0x00f1;
                case 9: goto L_0x010f;
                case 10: goto L_0x0125;
                case 11: goto L_0x012a;
                case 12: goto L_0x000e;
                case 13: goto L_0x014b;
                case 14: goto L_0x000e;
                case 15: goto L_0x000e;
                case 16: goto L_0x0151;
                case 17: goto L_0x0157;
                default: goto L_0x000e;
            }
        L_0x000e:
            r1 = r0
        L_0x000f:
            if (r2 == 0) goto L_0x0171
            if (r2 == r8) goto L_0x0171
            boolean r2 = r2.equals(r8)
            if (r2 != 0) goto L_0x0171
        L_0x0019:
            return r0
        L_0x001a:
            char r1 = r8.charAt(r3)
            r3 = 98
            if (r1 != r3) goto L_0x0033
            char r1 = r8.charAt(r0)
            if (r1 != r6) goto L_0x000e
            char r1 = r8.charAt(r4)
            r3 = 117(0x75, float:1.64E-43)
            if (r1 != r3) goto L_0x000e
            r0 = 24
            goto L_0x0019
        L_0x0033:
            r3 = 103(0x67, float:1.44E-43)
            if (r1 != r3) goto L_0x004a
            char r1 = r8.charAt(r0)
            r3 = 98
            if (r1 != r3) goto L_0x000e
            char r1 = r8.charAt(r4)
            r3 = 105(0x69, float:1.47E-43)
            if (r1 != r3) goto L_0x000e
            r0 = 21
            goto L_0x0019
        L_0x004a:
            r3 = 112(0x70, float:1.57E-43)
            if (r1 != r3) goto L_0x000e
            char r1 = r8.charAt(r0)
            if (r1 != r6) goto L_0x000e
            char r1 = r8.charAt(r4)
            r3 = 117(0x75, float:1.64E-43)
            if (r1 != r3) goto L_0x000e
            r0 = 23
            goto L_0x0019
        L_0x005f:
            char r1 = r8.charAt(r0)
            r3 = 98
            if (r1 != r3) goto L_0x006c
            java.lang.String r2 = "bold"
            r1 = 16
            goto L_0x000f
        L_0x006c:
            r3 = 108(0x6c, float:1.51E-43)
            if (r1 != r3) goto L_0x000e
            java.lang.String r2 = "link"
            r1 = 27
            goto L_0x000f
        L_0x0075:
            char r1 = r8.charAt(r1)
            switch(r1) {
                case 100: goto L_0x007e;
                case 101: goto L_0x0083;
                case 104: goto L_0x0088;
                case 107: goto L_0x008d;
                case 108: goto L_0x0093;
                case 116: goto L_0x0099;
                default: goto L_0x007c;
            }
        L_0x007c:
            r1 = r0
            goto L_0x000f
        L_0x007e:
            java.lang.String r2 = "fixed"
            r1 = 18
            goto L_0x000f
        L_0x0083:
            java.lang.String r2 = "slice"
            r1 = 15
            goto L_0x000f
        L_0x0088:
            java.lang.String r2 = "match"
            r1 = 31
            goto L_0x000f
        L_0x008d:
            java.lang.String r2 = "blink"
            r1 = 22
            goto L_0x000f
        L_0x0093:
            java.lang.String r2 = "small"
            r1 = 20
            goto L_0x000f
        L_0x0099:
            java.lang.String r2 = "split"
            r1 = 9
            goto L_0x000f
        L_0x009f:
            char r1 = r8.charAt(r4)
            switch(r1) {
                case 101: goto L_0x00a9;
                case 104: goto L_0x00af;
                case 110: goto L_0x00b4;
                case 111: goto L_0x00ba;
                case 113: goto L_0x00c0;
                case 116: goto L_0x00c6;
                case 117: goto L_0x00cc;
                default: goto L_0x00a6;
            }
        L_0x00a6:
            r1 = r0
            goto L_0x000f
        L_0x00a9:
            java.lang.String r2 = "search"
            r1 = 32
            goto L_0x000f
        L_0x00af:
            java.lang.String r2 = "charAt"
            r1 = 5
            goto L_0x000f
        L_0x00b4:
            java.lang.String r2 = "anchor"
            r1 = 28
            goto L_0x000f
        L_0x00ba:
            java.lang.String r2 = "concat"
            r1 = 14
            goto L_0x000f
        L_0x00c0:
            java.lang.String r2 = "equals"
            r1 = 29
            goto L_0x000f
        L_0x00c6:
            java.lang.String r2 = "strike"
            r1 = 19
            goto L_0x000f
        L_0x00cc:
            java.lang.String r2 = "substr"
            r1 = 13
            goto L_0x000f
        L_0x00d2:
            char r3 = r8.charAt(r4)
            switch(r3) {
                case 97: goto L_0x00dc;
                case 101: goto L_0x00e0;
                case 110: goto L_0x00e6;
                case 116: goto L_0x00eb;
                default: goto L_0x00d9;
            }
        L_0x00d9:
            r1 = r0
            goto L_0x000f
        L_0x00dc:
            java.lang.String r2 = "valueOf"
            goto L_0x000f
        L_0x00e0:
            java.lang.String r2 = "replace"
            r1 = 33
            goto L_0x000f
        L_0x00e6:
            java.lang.String r2 = "indexOf"
            r1 = 7
            goto L_0x000f
        L_0x00eb:
            java.lang.String r2 = "italics"
            r1 = 17
            goto L_0x000f
        L_0x00f1:
            char r1 = r8.charAt(r1)
            r4 = 114(0x72, float:1.6E-43)
            if (r1 != r4) goto L_0x00fe
            java.lang.String r2 = "toString"
            r1 = r3
            goto L_0x000f
        L_0x00fe:
            if (r1 != r6) goto L_0x0106
            java.lang.String r2 = "fontsize"
            r1 = 25
            goto L_0x000f
        L_0x0106:
            r3 = 117(0x75, float:1.64E-43)
            if (r1 != r3) goto L_0x000e
            java.lang.String r2 = "toSource"
            r1 = 3
            goto L_0x000f
        L_0x010f:
            char r1 = r8.charAt(r0)
            r3 = 102(0x66, float:1.43E-43)
            if (r1 != r3) goto L_0x011d
            java.lang.String r2 = "fontcolor"
            r1 = 26
            goto L_0x000f
        L_0x011d:
            if (r1 != r6) goto L_0x000e
            java.lang.String r2 = "substring"
            r1 = 10
            goto L_0x000f
        L_0x0125:
            java.lang.String r2 = "charCodeAt"
            r1 = 6
            goto L_0x000f
        L_0x012a:
            char r1 = r8.charAt(r3)
            switch(r1) {
                case 76: goto L_0x0134;
                case 85: goto L_0x013a;
                case 110: goto L_0x0140;
                case 115: goto L_0x0145;
                default: goto L_0x0131;
            }
        L_0x0131:
            r1 = r0
            goto L_0x000f
        L_0x0134:
            java.lang.String r2 = "toLowerCase"
            r1 = 11
            goto L_0x000f
        L_0x013a:
            java.lang.String r2 = "toUpperCase"
            r1 = 12
            goto L_0x000f
        L_0x0140:
            java.lang.String r2 = "constructor"
            r1 = r4
            goto L_0x000f
        L_0x0145:
            java.lang.String r2 = "lastIndexOf"
            r1 = 8
            goto L_0x000f
        L_0x014b:
            java.lang.String r2 = "localeCompare"
            r1 = 34
            goto L_0x000f
        L_0x0151:
            java.lang.String r2 = "equalsIgnoreCase"
            r1 = 30
            goto L_0x000f
        L_0x0157:
            r1 = 8
            char r1 = r8.charAt(r1)
            r3 = 76
            if (r1 != r3) goto L_0x0167
            java.lang.String r2 = "toLocaleLowerCase"
            r1 = 35
            goto L_0x000f
        L_0x0167:
            r3 = 85
            if (r1 != r3) goto L_0x000e
            java.lang.String r2 = "toLocaleUpperCase"
            r1 = 36
            goto L_0x000f
        L_0x0171:
            r0 = r1
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: NativeString.findPrototypeId(java.lang.String):int");
    }
}
