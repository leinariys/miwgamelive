package org.mozilla1.javascript;

import java.io.Serializable;

/* renamed from: a.EM */
/* compiled from: a */
public class Undefined implements Serializable {
    public static final Object instance = new Undefined();
    static final long serialVersionUID = 9195680630202616767L;

    private Undefined() {
    }

    public Object readResolve() {
        return instance;
    }
}
