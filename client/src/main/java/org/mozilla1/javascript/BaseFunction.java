package org.mozilla1.javascript;


/* renamed from: a.acp  reason: case insensitive filesystem */
/* compiled from: a */
public class BaseFunction extends IdScriptableObject implements Function {
    static final long serialVersionUID = 5311394446546053859L;
    private static final int ceN = 3;
    private static final int eVn = 1;
    private static final int eVp = 5;
    private static final Object fbj = new Integer(6);
    private static final int fbk = 2;
    private static final int fbl = 3;
    private static final int fbm = 4;
    private static final int fbn = 5;
    private static final int fbp = 4;
    private static final int fbq = 5;
    /* renamed from: ns */
    private static final int f4272ns = 1;
    /* renamed from: nt */
    private static final int f4273nt = 5;
    /* renamed from: xT */
    private static final int f4274xT = 2;
    private Object fbr;
    private int fbs = 4;

    public BaseFunction() {
    }

    public BaseFunction(Scriptable avf, Scriptable avf2) {
        super(avf, avf2);
    }

    /* renamed from: a */
    static void m20587a(Scriptable avf, boolean z) {
        BaseFunction acp = new BaseFunction();
        acp.fbs = 7;
        acp.mo15695a(5, avf, z);
    }

    /* renamed from: b */
    static boolean m20589b(IdFunctionObject yy) {
        return yy.hasTag(fbj) && yy.methodId() == 4;
    }

    /* renamed from: c */
    static boolean m20591c(IdFunctionObject yy) {
        if (yy.hasTag(fbj)) {
            switch (yy.methodId()) {
                case 4:
                case 5:
                    return true;
            }
        }
        return false;
    }

    /* renamed from: b */
    private static Object m20588b(Context lhVar, Scriptable avf, Object[] objArr) {
        int length = objArr.length;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("function ");
        if (lhVar.getLanguageVersion() != 120) {
            stringBuffer.append("anonymous");
        }
        stringBuffer.append('(');
        for (int i = 0; i < length - 1; i++) {
            if (i > 0) {
                stringBuffer.append(',');
            }
            stringBuffer.append(ScriptRuntime.toString(objArr[i]));
        }
        stringBuffer.append(") {");
        if (length != 0) {
            stringBuffer.append(ScriptRuntime.toString(objArr[length - 1]));
        }
        stringBuffer.append('}');
        String stringBuffer2 = stringBuffer.toString();
        int[] iArr = new int[1];
        String c = Context.m35242c(iArr);
        if (c == null) {
            c = "<eval'ed string>";
            iArr[0] = 1;
        }
        String a = ScriptRuntime.m7524a(false, c, iArr[0]);
        Scriptable x = ScriptableObject.m23595x(avf);
        ErrorReporter a2 = DefaultErrorReporter.m39748a(lhVar.bwt());
        Evaluator bwC = Context.bwC();
        if (bwC != null) {
            return lhVar.mo20336a(x, stringBuffer2, bwC, a2, a, 1, (Object) null);
        }
        throw new JavaScriptException("Interpreter not present", c, iArr[0]);
    }

    public String getClassName() {
        return "Function";
    }

    public boolean hasInstance(Scriptable avf) {
        Object j = ScriptableObject.m23587j(this, "prototype");
        if (j instanceof Scriptable) {
            return ScriptRuntime.m7561c(avf, (Scriptable) j);
        }
        throw ScriptRuntime.m7536aZ("msg.instanceof.bad.prototype", getFunctionName());
    }

    /* access modifiers changed from: protected */
    public int getMaxInstanceId() {
        return 5;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int findInstanceIdInfo(java.lang.String r5) {
        /*
            r4 = this;
            r0 = 0
            r2 = 0
            int r1 = r5.length()
            switch(r1) {
                case 4: goto L_0x001c;
                case 5: goto L_0x0020;
                case 6: goto L_0x0024;
                case 7: goto L_0x0009;
                case 8: goto L_0x0009;
                case 9: goto L_0x0028;
                default: goto L_0x0009;
            }
        L_0x0009:
            r1 = r0
        L_0x000a:
            if (r2 == 0) goto L_0x0015
            if (r2 == r5) goto L_0x0015
            boolean r2 = r2.equals(r5)
            if (r2 != 0) goto L_0x0015
            r1 = r0
        L_0x0015:
            if (r1 != 0) goto L_0x003c
            int r0 = super.findInstanceIdInfo(r5)
        L_0x001b:
            return r0
        L_0x001c:
            java.lang.String r2 = "name"
            r1 = 3
            goto L_0x000a
        L_0x0020:
            java.lang.String r2 = "arity"
            r1 = 2
            goto L_0x000a
        L_0x0024:
            java.lang.String r2 = "length"
            r1 = 1
            goto L_0x000a
        L_0x0028:
            char r1 = r5.charAt(r0)
            r3 = 97
            if (r1 != r3) goto L_0x0034
            java.lang.String r2 = "arguments"
            r1 = 5
            goto L_0x000a
        L_0x0034:
            r3 = 112(0x70, float:1.57E-43)
            if (r1 != r3) goto L_0x0009
            java.lang.String r2 = "prototype"
            r1 = 4
            goto L_0x000a
        L_0x003c:
            switch(r1) {
                case 1: goto L_0x0045;
                case 2: goto L_0x0045;
                case 3: goto L_0x0045;
                case 4: goto L_0x004b;
                case 5: goto L_0x004e;
                default: goto L_0x003f;
            }
        L_0x003f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        L_0x0045:
            r0 = 7
        L_0x0046:
            int r0 = instanceIdInfo(r0, r1)
            goto L_0x001b
        L_0x004b:
            int r0 = r4.fbs
            goto L_0x0046
        L_0x004e:
            r0 = 6
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C5913acp.findInstanceIdInfo(java.lang.String):int");
    }

    /* access modifiers changed from: protected */
    public String getInstanceIdName(int i) {
        switch (i) {
            case 1:
                return "length";
            case 2:
                return "arity";
            case 3:
                return "name";
            case 4:
                return "prototype";
            case 5:
                return "arguments";
            default:
                return super.getInstanceIdName(i);
        }
    }

    /* access modifiers changed from: protected */
    public Object getInstanceIdValue(int i) {
        switch (i) {
            case 1:
                return ScriptRuntime.wrapInt(getLength());
            case 2:
                return ScriptRuntime.wrapInt(getArity());
            case 3:
                return getFunctionName();
            case 4:
                return bOI();
            case 5:
                return bOK();
            default:
                return super.getInstanceIdValue(i);
        }
    }

    /* access modifiers changed from: protected */
    public void setInstanceIdValue(int i, Object obj) {
        if (i != 4) {
            if (i == 5) {
                if (obj == NOT_FOUND) {
                    Kit.codeBug();
                }
                defaultPut("arguments", obj);
            }
            super.setInstanceIdValue(i, obj);
        } else if ((this.fbs & 1) == 0) {
            if (obj == null) {
                obj = UniqueTag.f5855nc;
            }
            this.fbr = obj;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo1118a(IdFunctionObject yy) {
        yy.setPrototype(this);
        super.mo1118a(yy);
    }

    /* access modifiers changed from: protected */
    public void initPrototypeId(int i) {
        String str;
        int i2 = 1;
        switch (i) {
            case 1:
                str = "constructor";
                break;
            case 2:
                str = "toString";
                break;
            case 3:
                str = "toSource";
                break;
            case 4:
                i2 = 2;
                str = "apply";
                break;
            case 5:
                str = "call";
                break;
            default:
                throw new IllegalArgumentException(String.valueOf(i));
        }
        initPrototypeMethod(fbj, i, str, i2);
    }

    public Object execIdCall(IdFunctionObject yy, Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        int i;
        boolean z = false;
        if (!yy.hasTag(fbj)) {
            return super.execIdCall(yy, lhVar, avf, avf2, objArr);
        }
        int methodId = yy.methodId();
        switch (methodId) {
            case 1:
                return m20588b(lhVar, avf, objArr);
            case 2:
                return m20590c(avf2, yy).mo2858e(ScriptRuntime.toInt32(objArr, 0), 0);
            case 3:
                BaseFunction c = m20590c(avf2, yy);
                int i2 = 2;
                if (objArr.length != 0) {
                    i = ScriptRuntime.toInt32(objArr[0]);
                    if (i >= 0) {
                        i2 = 0;
                    } else {
                        i = 0;
                    }
                } else {
                    i = 0;
                }
                return c.mo2858e(i, i2);
            case 4:
            case 5:
                if (methodId == 4) {
                    z = true;
                }
                return ScriptRuntime.m7522a(z, lhVar, avf, avf2, objArr);
            default:
                throw new IllegalArgumentException(String.valueOf(methodId));
        }
    }

    /* renamed from: c */
    private BaseFunction m20590c(Scriptable avf, IdFunctionObject yy) {
        Object defaultValue = avf.getDefaultValue(ScriptRuntime.FunctionClass);
        if (defaultValue instanceof BaseFunction) {
            return (BaseFunction) defaultValue;
        }
        throw ScriptRuntime.m7536aZ("msg.incompat.call", yy.getFunctionName());
    }

    public void setImmunePrototypeProperty(Object obj) {
        if ((this.fbs & 1) != 0) {
            throw new IllegalStateException();
        }
        if (obj == null) {
            obj = UniqueTag.f5855nc;
        }
        this.fbr = obj;
        this.fbs = 7;
    }

    /* access modifiers changed from: protected */
    public Scriptable bOH() {
        Object bOI = bOI();
        if (bOI instanceof Scriptable) {
            return (Scriptable) bOI;
        }
        return ScriptableObject.m23585i(this, "Object");
    }

    public Object call(Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        return Undefined.instance;
    }

    public Scriptable construct(Context lhVar, Scriptable avf, Object[] objArr) {
        Scriptable parentScope;
        Scriptable c = mo7313c(lhVar, avf);
        if (c != null) {
            Object call = call(lhVar, avf, c, objArr);
            if (call instanceof Scriptable) {
                return (Scriptable) call;
            }
            return c;
        }
        Object call2 = call(lhVar, avf, (Scriptable) null, objArr);
        if (!(call2 instanceof Scriptable)) {
            throw new IllegalStateException("Bad implementaion of call as constructor, name=" + getFunctionName() + " in " + getClass().getName());
        }
        Scriptable avf2 = (Scriptable) call2;
        if (avf2.getPrototype() == null) {
            avf2.setPrototype(bOH());
        }
        if (avf2.getParentScope() != null || avf2 == (parentScope = getParentScope())) {
            return avf2;
        }
        avf2.setParentScope(parentScope);
        return avf2;
    }

    /* renamed from: c */
    public Scriptable mo7313c(Context lhVar, Scriptable avf) {
        NativeObject pd = new NativeObject();
        pd.setPrototype(bOH());
        pd.setParentScope(getParentScope());
        return pd;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public String mo2858e(int i, int i2) {
        StringBuffer stringBuffer = new StringBuffer();
        boolean z = (i2 & 1) != 0;
        if (!z) {
            stringBuffer.append("function ");
            stringBuffer.append(getFunctionName());
            stringBuffer.append("() {\n\t");
        }
        stringBuffer.append("[native code, arity=");
        stringBuffer.append(getArity());
        stringBuffer.append("]\n");
        if (!z) {
            stringBuffer.append("}\n");
        }
        return stringBuffer.toString();
    }

    public int getArity() {
        return 0;
    }

    public int getLength() {
        return 0;
    }

    public String getFunctionName() {
        return "";
    }

    /* access modifiers changed from: package-private */
    public final Object bOI() {
        Object obj;
        Object obj2 = this.fbr;
        if (obj2 == null) {
            synchronized (this) {
                obj = this.fbr;
                if (obj == null) {
                    bOJ();
                    obj = this.fbr;
                }
            }
            return obj;
        } else if (obj2 == UniqueTag.f5855nc) {
            return null;
        } else {
            return obj2;
        }
    }

    private void bOJ() {
        NativeObject pd = new NativeObject();
        pd.defineProperty("constructor", (Object) this, 2);
        this.fbr = pd;
        Scriptable v = ScriptableObject.m23593v(this);
        if (v != pd) {
            pd.setPrototype(v);
        }
    }

    private Object bOK() {
        Object defaultGet = defaultGet("arguments");
        if (defaultGet != NOT_FOUND) {
            return defaultGet;
        }
        NativeCall a = ScriptRuntime.m7490a(Context.bwA(), (Function) this);
        if (a == null) {
            return null;
        }
        return a.get("arguments", a);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int findPrototypeId(java.lang.String r6) {
        /*
            r5 = this;
            r1 = 3
            r0 = 0
            r2 = 0
            int r3 = r6.length()
            switch(r3) {
                case 4: goto L_0x0016;
                case 5: goto L_0x001a;
                case 6: goto L_0x000a;
                case 7: goto L_0x000a;
                case 8: goto L_0x001e;
                case 9: goto L_0x000a;
                case 10: goto L_0x000a;
                case 11: goto L_0x0031;
                default: goto L_0x000a;
            }
        L_0x000a:
            r1 = r0
        L_0x000b:
            if (r2 == 0) goto L_0x0035
            if (r2 == r6) goto L_0x0035
            boolean r2 = r2.equals(r6)
            if (r2 != 0) goto L_0x0035
        L_0x0015:
            return r0
        L_0x0016:
            java.lang.String r2 = "call"
            r1 = 5
            goto L_0x000b
        L_0x001a:
            java.lang.String r2 = "apply"
            r1 = 4
            goto L_0x000b
        L_0x001e:
            char r3 = r6.charAt(r1)
            r4 = 111(0x6f, float:1.56E-43)
            if (r3 != r4) goto L_0x0029
            java.lang.String r2 = "toSource"
            goto L_0x000b
        L_0x0029:
            r1 = 116(0x74, float:1.63E-43)
            if (r3 != r1) goto L_0x000a
            java.lang.String r2 = "toString"
            r1 = 2
            goto L_0x000b
        L_0x0031:
            java.lang.String r2 = "constructor"
            r1 = 1
            goto L_0x000b
        L_0x0035:
            r0 = r1
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C5913acp.findPrototypeId(java.lang.String):int");
    }
}
