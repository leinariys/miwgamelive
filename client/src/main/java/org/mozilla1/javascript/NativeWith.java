package org.mozilla1.javascript;

import java.io.Serializable;

/* renamed from: a.atx  reason: case insensitive filesystem */
/* compiled from: a */
public class NativeWith implements IdFunctionCall, Scriptable, Serializable {
    private static final Object cHe = new Integer(23);

    /* renamed from: ns */
    private static final int f5341ns = 1;


    /* renamed from: EX */
    public Scriptable f5342EX;

    /* renamed from: EY */
    public Scriptable f5343EY;

    private NativeWith() {
    }

    public NativeWith(Scriptable avf, Scriptable avf2) {
        this.f5343EY = avf;
        this.f5342EX = avf2;
    }

    /* renamed from: a */
    static void m26125a(Scriptable avf, boolean z) {
        NativeWith atx = new NativeWith();
        atx.setParentScope(avf);
        atx.setPrototype(ScriptableObject.m23593v(avf));
        IdFunctionObject yy = new IdFunctionObject(atx, cHe, 1, "With", 0, avf);
        yy.mo7318h(atx);
        if (z) {
            yy.sealObject();
        }
        yy.exportAsScopeProperty();
    }

    /* renamed from: at */
    static boolean m26126at(Object obj) {
        if (!(obj instanceof IdFunctionObject)) {
            return false;
        }
        IdFunctionObject yy = (IdFunctionObject) obj;
        if (!yy.hasTag(cHe) || yy.methodId() != 1) {
            return false;
        }
        return true;
    }

    /* renamed from: f */
    static Object m26127f(org.mozilla1.javascript.Context lhVar, Scriptable avf, Object[] objArr) {
        Scriptable e;
        org.mozilla1.javascript.ScriptRuntime.m7549b(lhVar, "With");
        Scriptable x = ScriptableObject.m23595x(avf);
        NativeWith atx = new NativeWith();
        if (objArr.length == 0) {
            e = ScriptableObject.m23585i(x, "Object");
        } else {
            e = ScriptRuntime.m7572e(lhVar, x, objArr[0]);
        }
        atx.setPrototype(e);
        atx.setParentScope(x);
        return atx;
    }

    public String getClassName() {
        return "With";
    }

    public boolean has(String str, Scriptable avf) {
        return this.f5342EX.has(str, this.f5342EX);
    }

    public boolean has(int i, Scriptable avf) {
        return this.f5342EX.has(i, this.f5342EX);
    }

    public Object get(String str, Scriptable avf) {
        if (avf == this) {
            avf = this.f5342EX;
        }
        return this.f5342EX.get(str, avf);
    }

    public Object get(int i, Scriptable avf) {
        if (avf == this) {
            avf = this.f5342EX;
        }
        return this.f5342EX.get(i, avf);
    }

    public void put(String str, Scriptable avf, Object obj) {
        if (avf == this) {
            avf = this.f5342EX;
        }
        this.f5342EX.put(str, avf, obj);
    }

    public void put(int i, Scriptable avf, Object obj) {
        if (avf == this) {
            avf = this.f5342EX;
        }
        this.f5342EX.put(i, avf, obj);
    }

    public void delete(String str) {
        this.f5342EX.delete(str);
    }

    public void delete(int i) {
        this.f5342EX.delete(i);
    }

    public Scriptable getPrototype() {
        return this.f5342EX;
    }

    public void setPrototype(Scriptable avf) {
        this.f5342EX = avf;
    }

    public Scriptable getParentScope() {
        return this.f5343EY;
    }

    public void setParentScope(Scriptable avf) {
        this.f5343EY = avf;
    }

    public Object[] getIds() {
        return this.f5342EX.getIds();
    }

    public Object getDefaultValue(Class<?> cls) {
        return this.f5342EX.getDefaultValue(cls);
    }

    public boolean hasInstance(Scriptable avf) {
        return this.f5342EX.hasInstance(avf);
    }

    /* access modifiers changed from: protected */
    public Object updateDotQuery(boolean z) {
        throw new IllegalStateException();
    }

    public Object execIdCall(IdFunctionObject yy, org.mozilla1.javascript.Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        if (!yy.hasTag(cHe) || yy.methodId() != 1) {
            throw yy.unknown();
        }
        throw Context.m35246q("msg.cant.call.indirect", "With");
    }
}
