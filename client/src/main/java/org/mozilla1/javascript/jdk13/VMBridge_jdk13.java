package org.mozilla1.javascript.jdk13;

import org.mozilla1.javascript.VMBridge;

import java.lang.reflect.*;

/* renamed from: a.IO */
/* compiled from: a */
public class VMBridge_jdk13 extends VMBridge {
    private ThreadLocal<Object[]> dgY = new ThreadLocal<>();

    /* access modifiers changed from: protected */
    public Object getThreadContextHelper() {
        Object[] objArr = this.dgY.get();
        if (objArr != null) {
            return objArr;
        }
        Object[] objArr2 = new Object[1];
        this.dgY.set(objArr2);
        return objArr2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: R */
    public org.mozilla1.javascript.Context mo2803R(Object obj) {
        return (org.mozilla1.javascript.Context) ((Object[]) obj)[0];
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo2806a(Object obj, org.mozilla1.javascript.Context lhVar) {
        ((Object[]) obj)[0] = lhVar;
    }

    /* access modifiers changed from: protected */
    public ClassLoader getCurrentThreadClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

    /* access modifiers changed from: protected */
    public boolean tryToMakeAccessible(Object obj) {
        if (!(obj instanceof AccessibleObject)) {
            return false;
        }
        AccessibleObject accessibleObject = (AccessibleObject) obj;
        if (accessibleObject.isAccessible()) {
            return true;
        }
        try {
            accessibleObject.setAccessible(true);
        } catch (Exception e) {
        }
        return accessibleObject.isAccessible();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Object mo2804a(org.mozilla1.javascript.ContextFactory iwVar, Class<?>[] clsArr) {
        try {
            return Proxy.getProxyClass(clsArr[0].getClassLoader(), clsArr).getConstructor(new Class[]{InvocationHandler.class});
        } catch (NoSuchMethodException e) {
            throw org.mozilla1.javascript.Kit.initCause(new IllegalStateException(), e);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Object mo2805a(Object obj, org.mozilla1.javascript.ContextFactory iwVar, org.mozilla1.javascript.InterfaceAdapter ks, Object obj2, org.mozilla1.javascript.Scriptable avf) {
        try {
            return ((Constructor) obj).newInstance(new Object[]{new C0594a(ks, iwVar, obj2, avf)});
        } catch (InvocationTargetException e) {
            throw org.mozilla1.javascript.Context.throwAsScriptRuntimeEx(e);
        } catch (IllegalAccessException e2) {
            throw org.mozilla1.javascript.Kit.initCause(new IllegalStateException(), e2);
        } catch (InstantiationException e3) {
            throw org.mozilla1.javascript.Kit.initCause(new IllegalStateException(), e3);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo2807a(Member member) {
        return false;
    }

    /* renamed from: a.IO$a */
    class C0594a implements InvocationHandler {
        private final /* synthetic */ Object eyd;
        private final /* synthetic */ org.mozilla1.javascript.Scriptable eye;
        private final /* synthetic */ org.mozilla1.javascript.InterfaceAdapter iYs;
        private final /* synthetic */ org.mozilla1.javascript.ContextFactory iYt;

        C0594a(org.mozilla1.javascript.InterfaceAdapter ks, org.mozilla1.javascript.ContextFactory iwVar, Object obj, org.mozilla1.javascript.Scriptable avf) {
            this.iYs = ks;
            this.iYt = iwVar;
            this.eyd = obj;
            this.eye = avf;
        }

        public Object invoke(Object obj, Method method, Object[] objArr) {
            return this.iYs.mo3543a(this.iYt, this.eyd, this.eye, method, objArr);
        }
    }
}
