package org.mozilla1.javascript;

/* renamed from: a.hj */
/* compiled from: a */
public class ContinuationPending extends RuntimeException {

    /* renamed from: TP */
    private NativeContinuation f8028TP;

    /* renamed from: TQ */
    private Object f8029TQ;

    /* renamed from: TR */
    private Scriptable f8030TR;

    ContinuationPending(NativeContinuation ahs) {
        this.f8028TP = ahs;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ya */
    public NativeContinuation mo19348ya() {
        return this.f8028TP;
    }

    /* renamed from: y */
    public void mo19347y(Object obj) {
        this.f8029TQ = obj;
    }

    /* renamed from: yb */
    public Object mo19349yb() {
        return this.f8029TQ;
    }

    public Scriptable getScope() {
        return this.f8030TR;
    }

    public void setScope(Scriptable avf) {
        this.f8030TR = avf;
    }
}
