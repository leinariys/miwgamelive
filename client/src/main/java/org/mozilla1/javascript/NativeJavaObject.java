package org.mozilla1.javascript;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Map;

/* renamed from: a.eS */
/* compiled from: a */
public class NativeJavaObject implements org.mozilla1.javascript.Wrapper, org.mozilla1.javascript.Scriptable, Serializable {

    /* renamed from: EU */
    static final byte f6793EU = 1;
    /* renamed from: EV */
    static final byte f6794EV = 0;
    /* renamed from: EW */
    static final byte f6795EW = 99;
    static final long serialVersionUID = -6948590651130498591L;
    /* renamed from: EK */
    private static final int f6784EK = 0;
    /* renamed from: EL */
    private static final int f6785EL = 1;
    /* renamed from: EM */
    private static final int f6786EM = 2;
    /* renamed from: EN */
    private static final int f6787EN = 3;
    /* renamed from: EO */
    private static final int f6788EO = 4;
    /* renamed from: EP */
    private static final int f6789EP = 5;
    /* renamed from: ER */
    private static final int f6790ER = 6;
    /* renamed from: ES */
    private static final int f6791ES = 7;
    /* renamed from: ET */
    private static final int f6792ET = 8;
    /* renamed from: Fc */
    private static final Object f6796Fc = new Integer(16);
    /* renamed from: Fd */
    private static Method f6797Fd = null;
    /* renamed from: Fe */
    private static Method f6798Fe = null;

    static {
        Class[] clsArr = new Class[2];
        Class<?> classOrNull = org.mozilla1.javascript.Kit.classOrNull("a.QL");
        if (classOrNull != null) {
            try {
                clsArr[0] = org.mozilla1.javascript.ScriptRuntime.ObjectClass;
                clsArr[1] = org.mozilla1.javascript.Kit.classOrNull("java.io.ObjectOutputStream");
                f6797Fd = classOrNull.getMethod("writeAdapterObject", clsArr);
                clsArr[0] = org.mozilla1.javascript.ScriptRuntime.ScriptableClass;
                clsArr[1] = org.mozilla1.javascript.Kit.classOrNull("java.io.ObjectInputStream");
                f6798Fe = classOrNull.getMethod("readAdapterObject", clsArr);
            } catch (Exception e) {
                f6797Fd = null;
                f6798Fe = null;
            }
        }
    }

    /* renamed from: EX */
    public org.mozilla1.javascript.Scriptable f6799EX;
    /* renamed from: EY */
    public org.mozilla1.javascript.Scriptable f6800EY;
    /* renamed from: EZ */
    public transient org.mozilla1.javascript.JavaMembers f6801EZ;
    public transient Object javaObject;
    public transient Class<?> staticType;
    /* renamed from: Fa */
    private transient Map<String, org.mozilla1.javascript.FieldAndMethods> f6802Fa;
    /* renamed from: Fb */
    private transient boolean f6803Fb;

    public NativeJavaObject() {
    }

    public NativeJavaObject(org.mozilla1.javascript.Scriptable avf, Object obj, Class<?> cls) {
        this(avf, obj, cls, false);
    }

    public NativeJavaObject(org.mozilla1.javascript.Scriptable avf, Object obj, Class<?> cls, boolean z) {
        this.f6800EY = avf;
        this.javaObject = obj;
        this.staticType = cls;
        this.f6803Fb = z;
        initMembers();
    }

    /* renamed from: a */
    public static Object m29541a(org.mozilla1.javascript.Scriptable avf, Object obj, Class<?> cls) {
        org.mozilla1.javascript.Context bwA = org.mozilla1.javascript.Context.bwA();
        return bwA.bwy().mo6829a(bwA, avf, obj, cls);
    }

    public static boolean canConvert(Object obj, Class<?> cls) {
        return m29539a(obj, cls) < 99;
    }

    /* renamed from: a */
    public static int m29539a(Object obj, Class<?> cls) {
        int o = m29546o(obj);
        switch (o) {
            case 0:
                if (cls == org.mozilla1.javascript.ScriptRuntime.StringClass || cls == org.mozilla1.javascript.ScriptRuntime.ObjectClass) {
                    return 1;
                }
                return 99;
            case 1:
                if (!cls.isPrimitive()) {
                    return 1;
                }
                return 99;
            case 2:
                if (cls == Boolean.TYPE) {
                    return 1;
                }
                if (cls == org.mozilla1.javascript.ScriptRuntime.BooleanClass) {
                    return 2;
                }
                if (cls == org.mozilla1.javascript.ScriptRuntime.ObjectClass) {
                    return 3;
                }
                if (cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
                    return 4;
                }
                return 99;
            case 3:
                if (cls.isPrimitive()) {
                    if (cls == Double.TYPE) {
                        return 1;
                    }
                    if (cls != Boolean.TYPE) {
                        return m29545d(cls) + 1;
                    }
                    return 99;
                } else if (cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
                    return 9;
                } else {
                    if (cls == org.mozilla1.javascript.ScriptRuntime.ObjectClass) {
                        return 10;
                    }
                    if (org.mozilla1.javascript.ScriptRuntime.NumberClass.isAssignableFrom(cls)) {
                        return 2;
                    }
                    return 99;
                }
            case 4:
                if (cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
                    return 1;
                }
                if (cls.isInstance(obj)) {
                    return 2;
                }
                if (!cls.isPrimitive()) {
                    return 99;
                }
                if (cls == Character.TYPE) {
                    return 3;
                }
                if (cls != Boolean.TYPE) {
                    return 4;
                }
                return 99;
            case 5:
                if (cls == org.mozilla1.javascript.ScriptRuntime.ClassClass) {
                    return 1;
                }
                if (cls == org.mozilla1.javascript.ScriptRuntime.ObjectClass) {
                    return 3;
                }
                if (cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
                    return 4;
                }
                return 99;
            case 6:
            case 7:
                if (obj instanceof org.mozilla1.javascript.Wrapper) {
                    obj = ((org.mozilla1.javascript.Wrapper) obj).unwrap();
                }
                if (cls.isInstance(obj)) {
                    return 0;
                }
                if (cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
                    return 2;
                }
                if (!cls.isPrimitive() || cls == Boolean.TYPE || o == 7) {
                    return 99;
                }
                return m29545d(cls) + 2;
            case 8:
                if (cls == obj.getClass()) {
                    return 1;
                }
                if (cls.isArray()) {
                    if (obj instanceof NativeArray) {
                        return 1;
                    }
                    return 99;
                } else if (cls == org.mozilla1.javascript.ScriptRuntime.ObjectClass) {
                    return 2;
                } else {
                    if (cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
                        return 3;
                    }
                    if (cls == org.mozilla1.javascript.ScriptRuntime.DateClass) {
                        if (obj instanceof NativeDate) {
                            return 1;
                        }
                        return 99;
                    } else if (cls.isInterface()) {
                        if (!(obj instanceof org.mozilla1.javascript.Function) || cls.getMethods().length != 1) {
                            return 11;
                        }
                        return 1;
                    } else if (!cls.isPrimitive() || cls == Boolean.TYPE) {
                        return 99;
                    } else {
                        return m29545d(cls) + 3;
                    }
                }
            default:
                return 99;
        }
    }

    /* renamed from: d */
    static int m29545d(Class<?> cls) {
        if (cls == Double.TYPE) {
            return 1;
        }
        if (cls == Float.TYPE) {
            return 2;
        }
        if (cls == Long.TYPE) {
            return 3;
        }
        if (cls == Integer.TYPE) {
            return 4;
        }
        if (cls == Short.TYPE) {
            return 5;
        }
        if (cls == Character.TYPE) {
            return 6;
        }
        if (cls == Byte.TYPE) {
            return 7;
        }
        if (cls == Boolean.TYPE) {
            return 99;
        }
        return 8;
    }

    /* renamed from: o */
    private static int m29546o(Object obj) {
        if (obj == null) {
            return 1;
        }
        if (obj == Undefined.instance) {
            return 0;
        }
        if (obj instanceof String) {
            return 4;
        }
        if (obj instanceof Number) {
            return 3;
        }
        if (obj instanceof Boolean) {
            return 2;
        }
        if (obj instanceof org.mozilla1.javascript.Scriptable) {
            if (obj instanceof NativeJavaClass) {
                return 5;
            }
            if (obj instanceof NativeJavaArray) {
                return 7;
            }
            if (obj instanceof org.mozilla1.javascript.Wrapper) {
                return 6;
            }
            return 8;
        } else if (obj instanceof Class) {
            return 5;
        } else {
            if (obj.getClass().isArray()) {
                return 7;
            }
            return 6;
        }
    }

    public static Object coerceType(Class<?> cls, Object obj) {
        return m29542a(cls, obj);
    }

    /* renamed from: a */
    static Object m29542a(Class<?> cls, Object obj) {
        if (obj != null && obj.getClass() == cls) {
            return obj;
        }
        switch (m29546o(obj)) {
            case 0:
                if (cls == org.mozilla1.javascript.ScriptRuntime.StringClass || cls == org.mozilla1.javascript.ScriptRuntime.ObjectClass) {
                    return "undefined";
                }
                m29544b((Object) "undefined", cls);
                return obj;
            case 1:
                if (cls.isPrimitive()) {
                    m29544b(obj, cls);
                }
                return null;
            case 2:
                if (cls == Boolean.TYPE || cls == org.mozilla1.javascript.ScriptRuntime.BooleanClass || cls == org.mozilla1.javascript.ScriptRuntime.ObjectClass) {
                    return obj;
                }
                if (cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
                    return obj.toString();
                }
                m29544b(obj, cls);
                return obj;
            case 3:
                if (cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
                    return org.mozilla1.javascript.ScriptRuntime.toString(obj);
                }
                if (cls == org.mozilla1.javascript.ScriptRuntime.ObjectClass) {
                    return m29543b((Class<?>) Double.TYPE, obj);
                }
                if ((cls.isPrimitive() && cls != Boolean.TYPE) || org.mozilla1.javascript.ScriptRuntime.NumberClass.isAssignableFrom(cls)) {
                    return m29543b(cls, obj);
                }
                m29544b(obj, cls);
                return obj;
            case 4:
                if (cls == org.mozilla1.javascript.ScriptRuntime.StringClass || cls.isInstance(obj)) {
                    return obj;
                }
                if (cls == Character.TYPE || cls == org.mozilla1.javascript.ScriptRuntime.CharacterClass) {
                    if (((String) obj).length() == 1) {
                        return new Character(((String) obj).charAt(0));
                    }
                    return m29543b(cls, obj);
                } else if ((cls.isPrimitive() && cls != Boolean.TYPE) || org.mozilla1.javascript.ScriptRuntime.NumberClass.isAssignableFrom(cls)) {
                    return m29543b(cls, obj);
                } else {
                    m29544b(obj, cls);
                    return obj;
                }
            case 5:
                if (obj instanceof org.mozilla1.javascript.Wrapper) {
                    obj = ((org.mozilla1.javascript.Wrapper) obj).unwrap();
                }
                if (cls == org.mozilla1.javascript.ScriptRuntime.ClassClass || cls == org.mozilla1.javascript.ScriptRuntime.ObjectClass) {
                    return obj;
                }
                if (cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
                    return obj.toString();
                }
                m29544b(obj, cls);
                return obj;
            case 6:
            case 7:
                if (obj instanceof org.mozilla1.javascript.Wrapper) {
                    obj = ((org.mozilla1.javascript.Wrapper) obj).unwrap();
                }
                if (cls.isPrimitive()) {
                    if (cls == Boolean.TYPE) {
                        m29544b(obj, cls);
                    }
                    return m29543b(cls, obj);
                } else if (cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
                    return obj.toString();
                } else {
                    if (cls.isInstance(obj)) {
                        return obj;
                    }
                    m29544b(obj, cls);
                    return obj;
                }
            case 8:
                if (cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
                    return org.mozilla1.javascript.ScriptRuntime.toString(obj);
                }
                if (cls.isPrimitive()) {
                    if (cls == Boolean.TYPE) {
                        m29544b(obj, cls);
                    }
                    return m29543b(cls, obj);
                } else if (cls.isInstance(obj)) {
                    return obj;
                } else {
                    if (cls == org.mozilla1.javascript.ScriptRuntime.DateClass && (obj instanceof NativeDate)) {
                        return new Date((long) ((NativeDate) obj).aDB());
                    }
                    if (cls.isArray() && (obj instanceof NativeArray)) {
                        NativeArray ayt = (NativeArray) obj;
                        long length = ayt.getLength();
                        Class<?> componentType = cls.getComponentType();
                        Object newInstance = Array.newInstance(componentType, (int) length);
                        for (int i = 0; ((long) i) < length; i++) {
                            try {
                                Array.set(newInstance, i, coerceType(componentType, ayt.get(i, ayt)));
                            } catch (EvaluatorException e) {
                                m29544b(obj, cls);
                            }
                        }
                        return newInstance;
                    } else if (obj instanceof org.mozilla1.javascript.Wrapper) {
                        Object unwrap = ((org.mozilla1.javascript.Wrapper) obj).unwrap();
                        if (cls.isInstance(unwrap)) {
                            return unwrap;
                        }
                        m29544b(unwrap, cls);
                        return unwrap;
                    } else if (!cls.isInterface() || !(obj instanceof org.mozilla1.javascript.Callable)) {
                        m29544b(obj, cls);
                        return obj;
                    } else if (obj instanceof org.mozilla1.javascript.ScriptableObject) {
                        org.mozilla1.javascript.ScriptableObject akn = (org.mozilla1.javascript.ScriptableObject) obj;
                        Object makeHashKeyFromPair = Kit.makeHashKeyFromPair(f6796Fc, cls);
                        Object associatedValue = akn.getAssociatedValue(makeHashKeyFromPair);
                        if (associatedValue != null) {
                            return associatedValue;
                        }
                        return akn.associateValue(makeHashKeyFromPair, InterfaceAdapter.m6381a(org.mozilla1.javascript.Context.bwA(), cls, (Callable) obj));
                    } else {
                        m29544b(obj, cls);
                        return obj;
                    }
                }
            default:
                return obj;
        }
    }

    /* renamed from: b */
    private static Object m29543b(Class<?> cls, Object obj) {
        float f;
        double d = org.mozilla1.javascript.ScriptRuntime.NaN;
        Class<?> cls2 = obj.getClass();
        if (cls == Character.TYPE || cls == org.mozilla1.javascript.ScriptRuntime.CharacterClass) {
            if (cls2 == org.mozilla1.javascript.ScriptRuntime.CharacterClass) {
                return obj;
            }
            return new Character((char) ((int) m29540a(obj, org.mozilla1.javascript.ScriptRuntime.CharacterClass, org.mozilla1.javascript.ScriptRuntime.NaN, 65535.0d)));
        } else if (cls == org.mozilla1.javascript.ScriptRuntime.ObjectClass || cls == org.mozilla1.javascript.ScriptRuntime.DoubleClass || cls == Double.TYPE) {
            if (cls2 != org.mozilla1.javascript.ScriptRuntime.DoubleClass) {
                return new Double(m29547q(obj));
            }
            return obj;
        } else if (cls == org.mozilla1.javascript.ScriptRuntime.FloatClass || cls == Float.TYPE) {
            if (cls2 == org.mozilla1.javascript.ScriptRuntime.FloatClass) {
                return obj;
            }
            double q = m29547q(obj);
            if (Double.isInfinite(q) || Double.isNaN(q) || q == org.mozilla1.javascript.ScriptRuntime.NaN) {
                return new Float((float) q);
            }
            double abs = Math.abs(q);
            if (abs < 1.401298464324817E-45d) {
                if (q <= org.mozilla1.javascript.ScriptRuntime.NaN) {
                    d = -0.0d;
                }
                return new Float(d);
            } else if (abs <= 3.4028234663852886E38d) {
                return new Float((float) q);
            } else {
                if (q > org.mozilla1.javascript.ScriptRuntime.NaN) {
                    f = Float.POSITIVE_INFINITY;
                } else {
                    f = Float.NEGATIVE_INFINITY;
                }
                return new Float(f);
            }
        } else if (cls == org.mozilla1.javascript.ScriptRuntime.IntegerClass || cls == Integer.TYPE) {
            if (cls2 == org.mozilla1.javascript.ScriptRuntime.IntegerClass) {
                return obj;
            }
            return new Integer((int) m29540a(obj, org.mozilla1.javascript.ScriptRuntime.IntegerClass, -2.147483648E9d, 2.147483647E9d));
        } else if (cls == org.mozilla1.javascript.ScriptRuntime.LongClass || cls == Long.TYPE) {
            if (cls2 == org.mozilla1.javascript.ScriptRuntime.LongClass) {
                return obj;
            }
            double longBitsToDouble = Double.longBitsToDouble(4890909195324358655L);
            return new Long(m29540a(obj, org.mozilla1.javascript.ScriptRuntime.LongClass, Double.longBitsToDouble(-4332462841530417152L), longBitsToDouble));
        } else if (cls == org.mozilla1.javascript.ScriptRuntime.ShortClass || cls == Short.TYPE) {
            if (cls2 == org.mozilla1.javascript.ScriptRuntime.ShortClass) {
                return obj;
            }
            return new Short((short) ((int) m29540a(obj, org.mozilla1.javascript.ScriptRuntime.ShortClass, -32768.0d, 32767.0d)));
        } else if (cls != org.mozilla1.javascript.ScriptRuntime.ByteClass && cls != Byte.TYPE) {
            return new Double(m29547q(obj));
        } else {
            if (cls2 == org.mozilla1.javascript.ScriptRuntime.ByteClass) {
                return obj;
            }
            return new Byte((byte) ((int) m29540a(obj, org.mozilla1.javascript.ScriptRuntime.ByteClass, -128.0d, 127.0d)));
        }
    }

    /* renamed from: q */
    private static double m29547q(Object obj) {
        Method method = null;
        if (obj instanceof Number) {
            return ((Number) obj).doubleValue();
        }
        if (obj instanceof String) {
            return org.mozilla1.javascript.ScriptRuntime.toNumber((String) obj);
        }
        if (!(obj instanceof org.mozilla1.javascript.Scriptable)) {
            try {
                method = obj.getClass().getMethod("doubleValue", (Class[]) null);
            } catch (NoSuchMethodException | SecurityException e) {
            }
            if (method != null) {
                try {
                    return ((Number) method.invoke(obj, (Object[]) null)).doubleValue();
                } catch (IllegalAccessException e2) {
                    m29544b(obj, (Class<?>) Double.TYPE);
                } catch (InvocationTargetException e3) {
                    m29544b(obj, (Class<?>) Double.TYPE);
                }
            }
            return org.mozilla1.javascript.ScriptRuntime.toNumber(obj.toString());
        } else if (obj instanceof org.mozilla1.javascript.Wrapper) {
            return m29547q(((Wrapper) obj).unwrap());
        } else {
            return org.mozilla1.javascript.ScriptRuntime.toNumber(obj);
        }
    }

    /* renamed from: a */
    private static long m29540a(Object obj, Class<?> cls, double d, double d2) {
        double ceil;
        double q = m29547q(obj);
        if (Double.isInfinite(q) || Double.isNaN(q)) {
            m29544b((Object) org.mozilla1.javascript.ScriptRuntime.toString(obj), cls);
        }
        if (q > org.mozilla1.javascript.ScriptRuntime.NaN) {
            ceil = Math.floor(q);
        } else {
            ceil = Math.ceil(q);
        }
        if (ceil < d || ceil > d2) {
            m29544b((Object) org.mozilla1.javascript.ScriptRuntime.toString(obj), cls);
        }
        return (long) ceil;
    }

    /* renamed from: b */
    static void m29544b(Object obj, Class<?> cls) {
        throw org.mozilla1.javascript.Context.m35224a("msg.conversion.not.allowed", (Object) String.valueOf(obj), (Object) org.mozilla1.javascript.JavaMembers.m8436ar(cls));
    }

    /* access modifiers changed from: protected */
    public void initMembers() {
        Class<?> cls;
        if (this.javaObject != null) {
            cls = this.javaObject.getClass();
        } else {
            cls = this.staticType;
        }
        this.f6801EZ = org.mozilla1.javascript.JavaMembers.m8430a(this.f6800EY, cls, this.staticType, this.f6803Fb);
        this.f6802Fa = this.f6801EZ.mo4679a((org.mozilla1.javascript.Scriptable) this, this.javaObject, false);
    }

    public boolean has(String str, org.mozilla1.javascript.Scriptable avf) {
        return this.f6801EZ.mo4683n(str, false);
    }

    public boolean has(int i, org.mozilla1.javascript.Scriptable avf) {
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r0 = r3.f6802Fa.get(r4);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object get(java.lang.String r4, org.mozilla1.javascript.Scriptable r5) {
        /*
            r3 = this;
            java.util.Map<java.lang.String, a.TR> r0 = r3.f6802Fa
            if (r0 == 0) goto L_0x000d
            java.util.Map<java.lang.String, a.TR> r0 = r3.f6802Fa
            java.lang.Object r0 = r0.get(r4)
            if (r0 == 0) goto L_0x000d
        L_0x000c:
            return r0
        L_0x000d:
            a.PT r0 = r3.f6801EZ
            java.lang.Object r1 = r3.javaObject
            r2 = 0
            java.lang.Object r0 = r0.mo4678a((p001a.aVF) r3, (java.lang.String) r4, (java.lang.Object) r1, (boolean) r2)
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C2359eS.get(java.lang.String, a.aVF):java.lang.Object");
    }

    public Object get(int i, org.mozilla1.javascript.Scriptable avf) {
        throw this.f6801EZ.mo4682jy(Integer.toString(i));
    }

    public void put(String str, org.mozilla1.javascript.Scriptable avf, Object obj) {
        if (this.f6799EX == null || this.f6801EZ.mo4683n(str, false)) {
            this.f6801EZ.mo4680a(this, str, this.javaObject, obj, false);
            return;
        }
        this.f6799EX.put(str, this.f6799EX, obj);
    }

    public void put(int i, org.mozilla1.javascript.Scriptable avf, Object obj) {
        throw this.f6801EZ.mo4682jy(Integer.toString(i));
    }

    public boolean hasInstance(org.mozilla1.javascript.Scriptable avf) {
        return false;
    }

    public void delete(String str) {
    }

    public void delete(int i) {
    }

    public org.mozilla1.javascript.Scriptable getPrototype() {
        if (this.f6799EX != null || !(this.javaObject instanceof String)) {
            return this.f6799EX;
        }
        return ScriptableObject.m23585i(this.f6800EY, "String");
    }

    public void setPrototype(org.mozilla1.javascript.Scriptable avf) {
        this.f6799EX = avf;
    }

    public org.mozilla1.javascript.Scriptable getParentScope() {
        return this.f6800EY;
    }

    public void setParentScope(org.mozilla1.javascript.Scriptable avf) {
        this.f6800EY = avf;
    }

    public Object[] getIds() {
        return this.f6801EZ.mo4681dP(false);
    }

    public Object unwrap() {
        return this.javaObject;
    }

    public String getClassName() {
        return "JavaObject";
    }

    public Object getDefaultValue(Class<?> cls) {
        String str;
        if (cls == null && (this.javaObject instanceof Boolean)) {
            cls = org.mozilla1.javascript.ScriptRuntime.BooleanClass;
        }
        if (cls == null || cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
            return this.javaObject.toString();
        }
        if (cls == org.mozilla1.javascript.ScriptRuntime.BooleanClass) {
            str = "booleanValue";
        } else if (cls == org.mozilla1.javascript.ScriptRuntime.NumberClass) {
            str = "doubleValue";
        } else {
            throw org.mozilla1.javascript.Context.m35244gE("msg.default.value");
        }
        Object obj = get(str, (Scriptable) this);
        if (obj instanceof org.mozilla1.javascript.Function) {
            org.mozilla1.javascript.Function azg = (Function) obj;
            return azg.call(Context.bwA(), azg.getParentScope(), this, org.mozilla1.javascript.ScriptRuntime.emptyArgs);
        } else if (cls != org.mozilla1.javascript.ScriptRuntime.NumberClass || !(this.javaObject instanceof Boolean)) {
            return this.javaObject.toString();
        } else {
            return org.mozilla1.javascript.ScriptRuntime.wrapNumber(((Boolean) this.javaObject).booleanValue() ? 1.0d : ScriptRuntime.NaN);
        }
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeBoolean(this.f6803Fb);
        if (!this.f6803Fb) {
            objectOutputStream.writeObject(this.javaObject);
        } else if (f6797Fd == null) {
            throw new IOException();
        } else {
            try {
                f6797Fd.invoke((Object) null, new Object[]{this.javaObject, objectOutputStream});
            } catch (Exception e) {
                throw new IOException();
            }
        }
        if (this.staticType != null) {
            objectOutputStream.writeObject(this.staticType.getClass().getName());
        } else {
            objectOutputStream.writeObject((Object) null);
        }
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.f6803Fb = objectInputStream.readBoolean();
        if (!this.f6803Fb) {
            this.javaObject = objectInputStream.readObject();
        } else if (f6798Fe == null) {
            throw new ClassNotFoundException();
        } else {
            try {
                this.javaObject = f6798Fe.invoke((Object) null, new Object[]{this, objectInputStream});
            } catch (Exception e) {
                throw new IOException();
            }
        }
        String str = (String) objectInputStream.readObject();
        if (str != null) {
            this.staticType = Class.forName(str);
        } else {
            this.staticType = null;
        }
        initMembers();
    }
}
