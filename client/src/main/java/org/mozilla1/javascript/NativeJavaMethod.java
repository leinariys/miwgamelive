package org.mozilla1.javascript;


import java.lang.reflect.Array;
import java.lang.reflect.Method;

/* renamed from: a.aiC  reason: case insensitive filesystem */
/* compiled from: a */
public class NativeJavaMethod extends BaseFunction {
    static final long serialVersionUID = -3440381785576412928L;
    private static final boolean debug = false;
    private static final int fNX = 0;
    private static final int fNY = 1;
    private static final int fNZ = 2;
    private static final int fOa = 3;
    org.mozilla1.javascript.MemberBox[] fOb;
    private String eMs;

    NativeJavaMethod(org.mozilla1.javascript.MemberBox[] amtArr) {
        this.eMs = amtArr[0].getName();
        this.fOb = amtArr;
    }

    NativeJavaMethod(org.mozilla1.javascript.MemberBox amt, String str) {
        this.eMs = str;
        this.fOb = new org.mozilla1.javascript.MemberBox[]{amt};
    }

    public NativeJavaMethod(Method method, String str) {
        this(new org.mozilla1.javascript.MemberBox(method), str);
    }

    /* renamed from: t */
    static String m22445t(Object[] objArr) {
        String ar;
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 == objArr.length) {
                return stringBuffer.toString();
            }
            Wrapper aem = (Wrapper) objArr[i2];
            if (aem == null) {
                ar = "null";
            } else if (aem instanceof Boolean) {
                ar = "boolean";
            } else if (aem instanceof String) {
                ar = "string";
            } else if (aem instanceof Number) {
                ar = "number";
            } else if (!(aem instanceof Scriptable)) {
                ar = JavaMembers.m8436ar(aem.getClass());
            } else if (aem instanceof Undefined) {
                ar = "undefined";
            } else if (aem instanceof Wrapper) {
                ar = aem.unwrap().getClass().getName();
            } else if (aem instanceof Function) {
                ar = "function";
            } else {
                ar = "object";
            }
            if (i2 != 0) {
                stringBuffer.append(',');
            }
            stringBuffer.append(ar);
            i = i2 + 1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:108:0x0059 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x005d  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static int m22442a(Context r13, org.mozilla1.javascript.MemberBox[] r14, java.lang.Object[] r15) {
        /*
            int r0 = r14.length
            if (r0 != 0) goto L_0x0005
            r4 = -1
        L_0x0004:
            return r4
        L_0x0005:
            int r0 = r14.length
            r1 = 1
            if (r0 != r1) goto L_0x0033
            r0 = 0
            r1 = r14[r0]
            java.lang.Class<?>[] r2 = r1.gaM
            int r0 = r2.length
            boolean r1 = r1.gaO
            if (r1 == 0) goto L_0x001a
            int r0 = r0 + -1
            int r1 = r15.length
            if (r0 <= r1) goto L_0x001f
            r4 = -1
            goto L_0x0004
        L_0x001a:
            int r1 = r15.length
            if (r0 == r1) goto L_0x001f
            r4 = -1
            goto L_0x0004
        L_0x001f:
            r1 = 0
        L_0x0020:
            if (r1 != r0) goto L_0x0024
            r4 = 0
            goto L_0x0004
        L_0x0024:
            r3 = r15[r1]
            r4 = r2[r1]
            boolean r3 = p001a.C2359eS.canConvert(r3, r4)
            if (r3 != 0) goto L_0x0030
            r4 = -1
            goto L_0x0004
        L_0x0030:
            int r1 = r1 + 1
            goto L_0x0020
        L_0x0033:
            r4 = -1
            r0 = 0
            int[] r0 = (int[]) r0
            r2 = 0
            r1 = 0
            r3 = r0
        L_0x003a:
            int r0 = r14.length
            if (r1 < r0) goto L_0x0041
            if (r4 >= 0) goto L_0x0102
            r4 = -1
            goto L_0x0004
        L_0x0041:
            r8 = r14[r1]
            java.lang.Class<?>[] r9 = r8.gaM
            int r0 = r9.length
            boolean r5 = r8.gaO
            if (r5 == 0) goto L_0x0053
            int r0 = r0 + -1
            int r5 = r15.length
            if (r0 <= r5) goto L_0x0056
        L_0x004f:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x003a
        L_0x0053:
            int r5 = r15.length
            if (r0 != r5) goto L_0x004f
        L_0x0056:
            r5 = 0
        L_0x0057:
            if (r5 < r0) goto L_0x005d
            if (r4 >= 0) goto L_0x006a
            r4 = r1
            goto L_0x004f
        L_0x005d:
            r6 = r15[r5]
            r7 = r9[r5]
            boolean r6 = p001a.C2359eS.canConvert(r6, r7)
            if (r6 == 0) goto L_0x004f
            int r5 = r5 + 1
            goto L_0x0057
        L_0x006a:
            r6 = 0
            r5 = 0
            r0 = -1
            r7 = r0
        L_0x006e:
            if (r7 != r2) goto L_0x0077
        L_0x0070:
            int r0 = r2 + 1
            if (r6 != r0) goto L_0x00f1
            r2 = 0
            r4 = r1
            goto L_0x004f
        L_0x0077:
            r0 = -1
            if (r7 != r0) goto L_0x00ae
            r0 = r4
        L_0x007b:
            r0 = r14[r0]
            r10 = 13
            boolean r10 = r13.hasFeature(r10)
            if (r10 == 0) goto L_0x00b4
            java.lang.reflect.Member r10 = r0.cjy()
            int r10 = r10.getModifiers()
            r10 = r10 & 1
            java.lang.reflect.Member r11 = r8.cjy()
            int r11 = r11.getModifiers()
            r11 = r11 & 1
            if (r10 == r11) goto L_0x00b4
            java.lang.reflect.Member r0 = r0.cjy()
            int r0 = r0.getModifiers()
            r0 = r0 & 1
            if (r0 != 0) goto L_0x00b1
            int r6 = r6 + 1
            r0 = r5
        L_0x00aa:
            int r7 = r7 + 1
            r5 = r0
            goto L_0x006e
        L_0x00ae:
            r0 = r3[r7]
            goto L_0x007b
        L_0x00b1:
            int r0 = r5 + 1
            goto L_0x00aa
        L_0x00b4:
            boolean r10 = r8.gaO
            java.lang.Class<?>[] r11 = r0.gaM
            boolean r12 = r0.gaO
            int r10 = m22443a(r15, r9, r10, r11, r12)
            r11 = 3
            if (r10 == r11) goto L_0x0070
            r11 = 1
            if (r10 != r11) goto L_0x00c8
            int r6 = r6 + 1
            r0 = r5
            goto L_0x00aa
        L_0x00c8:
            r11 = 2
            if (r10 != r11) goto L_0x00ce
            int r0 = r5 + 1
            goto L_0x00aa
        L_0x00ce:
            if (r10 == 0) goto L_0x00d3
            p001a.C1520WN.codeBug()
        L_0x00d3:
            boolean r5 = r0.isStatic()
            if (r5 == 0) goto L_0x004f
            java.lang.Class r0 = r0.getDeclaringClass()
            java.lang.Class r5 = r8.getDeclaringClass()
            boolean r0 = r0.isAssignableFrom(r5)
            if (r0 == 0) goto L_0x004f
            r0 = -1
            if (r7 != r0) goto L_0x00ed
            r4 = r1
            goto L_0x004f
        L_0x00ed:
            r3[r7] = r1
            goto L_0x004f
        L_0x00f1:
            int r0 = r2 + 1
            if (r5 == r0) goto L_0x004f
            if (r3 != 0) goto L_0x00fc
            int r0 = r14.length
            int r0 = r0 + -1
            int[] r3 = new int[r0]
        L_0x00fc:
            r3[r2] = r1
            int r2 = r2 + 1
            goto L_0x004f
        L_0x0102:
            if (r2 == 0) goto L_0x0004
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r5.<init>()
            r0 = -1
            r1 = r0
        L_0x010b:
            if (r1 != r2) goto L_0x0133
            r0 = r14[r4]
            java.lang.String r1 = r0.getName()
            java.lang.Class r0 = r0.getDeclaringClass()
            java.lang.String r0 = r0.getName()
            r2 = 0
            r2 = r14[r2]
            boolean r2 = r2.isMethod()
            if (r2 == 0) goto L_0x014c
            java.lang.String r0 = "msg.constructor.ambiguous"
            java.lang.String r2 = m22445t(r15)
            java.lang.String r3 = r5.toString()
            a.TJ r0 = p001a.C2909lh.m35225a((java.lang.String) r0, (java.lang.Object) r1, (java.lang.Object) r2, (java.lang.Object) r3)
            throw r0
        L_0x0133:
            r0 = -1
            if (r1 != r0) goto L_0x0149
            r0 = r4
        L_0x0137:
            java.lang.String r6 = "\n    "
            r5.append(r6)
            r0 = r14[r0]
            java.lang.String r0 = r0.cjA()
            r5.append(r0)
            int r0 = r1 + 1
            r1 = r0
            goto L_0x010b
        L_0x0149:
            r0 = r3[r1]
            goto L_0x0137
        L_0x014c:
            java.lang.String r2 = "msg.method.ambiguous"
            java.lang.String r3 = m22445t(r15)
            java.lang.String r4 = r5.toString()
            a.TJ r0 = p001a.C2909lh.m35226a((java.lang.String) r2, (java.lang.Object) r0, (java.lang.Object) r1, (java.lang.Object) r3, (java.lang.Object) r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6186aiC.m22442a(a.lh, a.amt[], java.lang.Object[]):int");
    }

    /* renamed from: a */
    private static int m22443a(Object[] objArr, Class<?>[] clsArr, boolean z, Class<?>[] clsArr2, boolean z2) {
        int i;
        int length = objArr.length;
        if (!z && z2) {
            return 1;
        }
        if (z && !z2) {
            return 2;
        }
        if (z && z2) {
            if (clsArr.length < clsArr2.length) {
                return 2;
            }
            if (clsArr.length > clsArr2.length) {
                return 1;
            }
            length = Math.min(objArr.length, clsArr.length - 1);
        }
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            Class<?> cls = clsArr[i3];
            Class<?> cls2 = clsArr2[i3];
            if (cls != cls2) {
                Object obj = objArr[i3];
                int a = NativeJavaObject.m29539a(obj, cls);
                int a2 = NativeJavaObject.m29539a(obj, cls2);
                if (a < a2) {
                    i = 1;
                } else if (a > a2) {
                    i = 2;
                } else if (a != 0) {
                    i = 3;
                } else if (cls.isAssignableFrom(cls2)) {
                    i = 2;
                } else if (cls2.isAssignableFrom(cls)) {
                    i = 1;
                } else {
                    i = 3;
                }
                i2 |= i;
                if (i2 == 3) {
                    return i2;
                }
            }
        }
        return i2;
    }

    /* renamed from: a */
    private static void m22444a(String str, org.mozilla1.javascript.MemberBox amt, Object[] objArr) {
    }

    public String getFunctionName() {
        return this.eMs;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public String mo2858e(int i, int i2) {
        StringBuffer stringBuffer = new StringBuffer();
        boolean z = (i2 & 1) != 0;
        if (!z) {
            stringBuffer.append("function ");
            stringBuffer.append(getFunctionName());
            stringBuffer.append("() {");
        }
        stringBuffer.append("/*\n");
        stringBuffer.append(toString());
        stringBuffer.append(z ? "*/\n" : "*/}\n");
        return stringBuffer.toString();
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        int length = this.fOb.length;
        for (int i = 0; i != length; i++) {
            Method cjw = this.fOb[i].cjw();
            stringBuffer.append(JavaMembers.m8436ar(cjw.getReturnType()));
            stringBuffer.append(' ');
            stringBuffer.append(cjw.getName());
            stringBuffer.append(JavaMembers.liveConnectSignature(this.fOb[i].gaM));
            stringBuffer.append(10);
        }
        return stringBuffer.toString();
    }

    public Object call(Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        Object[] objArr2;
        Object unwrap;
        Object newInstance;
        int i = 0;
        if (this.fOb.length == 0) {
            throw new RuntimeException("No methods defined for call");
        }
        int a = m22442a(lhVar, this.fOb, objArr);
        if (a < 0) {
            throw Context.m35246q("msg.java.no_such_method", String.valueOf(this.fOb[0].cjw().getDeclaringClass().getName()) + '.' + getFunctionName() + '(' + m22445t(objArr) + ')');
        }
        org.mozilla1.javascript.MemberBox amt = this.fOb[a];
        Class<?>[] clsArr = amt.gaM;
        if (amt.gaO) {
            objArr2 = new Object[clsArr.length];
            for (int i2 = 0; i2 < clsArr.length - 1; i2++) {
                objArr2[i2] = Context.jsToJava(objArr[i2], clsArr[i2]);
            }
            if (objArr.length != clsArr.length || (objArr[objArr.length - 1] != null && !(objArr[objArr.length - 1] instanceof NativeArray) && !(objArr[objArr.length - 1] instanceof NativeJavaArray))) {
                Class<?> componentType = clsArr[clsArr.length - 1].getComponentType();
                newInstance = Array.newInstance(componentType, (objArr.length - clsArr.length) + 1);
                while (i < Array.getLength(newInstance)) {
                    Array.set(newInstance, i, Context.jsToJava(objArr[(clsArr.length - 1) + i], componentType));
                    i++;
                }
            } else {
                newInstance = Context.jsToJava(objArr[objArr.length - 1], clsArr[clsArr.length - 1]);
            }
            objArr2[clsArr.length - 1] = newInstance;
        } else {
            Object[] objArr3 = objArr;
            while (i < objArr3.length) {
                Object obj = objArr3[i];
                Object jsToJava = Context.jsToJava(obj, clsArr[i]);
                if (jsToJava != obj) {
                    if (objArr == objArr3) {
                        objArr3 = (Object[]) objArr3.clone();
                    }
                    objArr3[i] = jsToJava;
                }
                i++;
            }
            objArr2 = objArr3;
        }
        if (amt.isStatic()) {
            unwrap = null;
        } else {
            Class<?> declaringClass = amt.getDeclaringClass();
            for (Scriptable avf3 = avf2; avf3 != null; avf3 = avf3.getPrototype()) {
                if (avf3 instanceof Wrapper) {
                    unwrap = ((Wrapper) avf3).unwrap();
                    if (!declaringClass.isInstance(unwrap)) {
                    }
                }
            }
            throw Context.m35225a("msg.nonjava.method", (Object) getFunctionName(), (Object) ScriptRuntime.toString((Object) avf2), (Object) declaringClass.getName());
        }
        Object invoke = amt.invoke(unwrap, objArr2);
        Class<?> returnType = amt.cjw().getReturnType();
        Object a2 = lhVar.bwy().mo6829a(lhVar, avf, invoke, returnType);
        if (a2 == null && returnType == Void.TYPE) {
            return Undefined.instance;
        }
        return a2;
    }
}
