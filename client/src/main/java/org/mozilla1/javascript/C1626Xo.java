package org.mozilla1.javascript;

/* renamed from: a.Xo */
/* compiled from: a */
public class C1626Xo {
    private boolean eID = true;

    /* renamed from: a */
    public Object mo6829a(org.mozilla1.javascript.Context lhVar, Scriptable avf, Object obj, Class<?> cls) {
        if (obj == null || obj == org.mozilla1.javascript.Undefined.instance || (obj instanceof Scriptable)) {
            return obj;
        }
        if (cls == null || !cls.isPrimitive()) {
            if (!isJavaPrimitiveWrap()) {
                if ((obj instanceof String) || (obj instanceof Number) || (obj instanceof Boolean)) {
                    return obj;
                }
                if (obj instanceof Character) {
                    return String.valueOf(((Character) obj).charValue());
                }
            }
            if (obj.getClass().isArray()) {
                return NativeJavaArray.m16491b(avf, obj);
            }
            return mo6830b(lhVar, avf, obj, cls);
        } else if (cls == Void.TYPE) {
            return Undefined.instance;
        } else {
            if (cls == Character.TYPE) {
                return new Integer(((Character) obj).charValue());
            }
            return obj;
        }
    }

    /* renamed from: a */
    public Scriptable mo6828a(org.mozilla1.javascript.Context lhVar, Scriptable avf, Object obj) {
        if (obj instanceof Scriptable) {
            return (Scriptable) obj;
        }
        if (obj.getClass().isArray()) {
            return NativeJavaArray.m16491b(avf, obj);
        }
        return mo6830b(lhVar, avf, obj, (Class<?>) null);
    }

    /* renamed from: b */
    public Scriptable mo6830b(org.mozilla1.javascript.Context lhVar, Scriptable avf, Object obj, Class<?> cls) {
        return new NativeJavaObject(avf, obj, cls);
    }

    public final boolean isJavaPrimitiveWrap() {
        return this.eID;
    }

    public final void setJavaPrimitiveWrap(boolean z) {
        org.mozilla1.javascript.Context bwq = org.mozilla1.javascript.Context.bwq();
        if (bwq != null && bwq.isSealed()) {
            Context.bws();
        }
        this.eID = z;
    }
}
