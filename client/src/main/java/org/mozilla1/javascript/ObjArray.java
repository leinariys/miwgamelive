package org.mozilla1.javascript;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/* renamed from: a.aCx  reason: case insensitive filesystem */
/* compiled from: a */
public class ObjArray implements Serializable {
    static final long serialVersionUID = 4174889037736658296L;
    private static final int huW = 5;
    private boolean dIn;
    private transient Object huX;
    private transient Object huY;
    private transient Object huZ;
    private transient Object hva;
    private transient Object hvb;

    /* renamed from: oj */
    private transient Object[] f2496oj;
    private int size;

    /* renamed from: ay */
    private static RuntimeException m13388ay(int i, int i2) {
        throw new IndexOutOfBoundsException(String.valueOf(i) + " ∉ [0, " + i2 + ')');
    }

    private static RuntimeException cPR() {
        throw new RuntimeException("Empty stack");
    }

    private static RuntimeException cPS() {
        throw new IllegalStateException("Attempt to modify sealed array");
    }

    public final boolean isSealed() {
        return this.dIn;
    }

    public final void seal() {
        this.dIn = true;
    }

    public final boolean isEmpty() {
        return this.size == 0;
    }

    public final int size() {
        return this.size;
    }

    public final void setSize(int i) {
        if (i < 0) {
            throw new IllegalArgumentException();
        } else if (this.dIn) {
            throw cPS();
        } else {
            int i2 = this.size;
            if (i < i2) {
                for (int i3 = i; i3 != i2; i3++) {
                    m13389g(i3, (Object) null);
                }
            } else if (i > i2 && i > 5) {
                ensureCapacity(i);
            }
            this.size = i;
        }
    }

    public final Object get(int i) {
        if (i >= 0 && i < this.size) {
            return m13390xi(i);
        }
        throw m13388ay(i, this.size);
    }

    public final void set(int i, Object obj) {
        if (i < 0 || i >= this.size) {
            throw m13388ay(i, this.size);
        } else if (this.dIn) {
            throw cPS();
        } else {
            m13389g(i, obj);
        }
    }

    /* renamed from: xi */
    private Object m13390xi(int i) {
        switch (i) {
            case 0:
                return this.huX;
            case 1:
                return this.huY;
            case 2:
                return this.huZ;
            case 3:
                return this.hva;
            case 4:
                return this.hvb;
            default:
                return this.f2496oj[i - 5];
        }
    }

    /* renamed from: g */
    private void m13389g(int i, Object obj) {
        switch (i) {
            case 0:
                this.huX = obj;
                return;
            case 1:
                this.huY = obj;
                return;
            case 2:
                this.huZ = obj;
                return;
            case 3:
                this.hva = obj;
                return;
            case 4:
                this.hvb = obj;
                return;
            default:
                this.f2496oj[i - 5] = obj;
                return;
        }
    }

    public int indexOf(Object obj) {
        int i = this.size;
        for (int i2 = 0; i2 != i; i2++) {
            Object xi = m13390xi(i2);
            if (xi == obj) {
                return i2;
            }
            if (xi != null && xi.equals(obj)) {
                return i2;
            }
        }
        return -1;
    }

    public int lastIndexOf(Object obj) {
        int i = this.size;
        while (i != 0) {
            i--;
            Object xi = m13390xi(i);
            if (xi == obj) {
                return i;
            }
            if (xi != null && xi.equals(obj)) {
                return i;
            }
        }
        return -1;
    }

    public final Object peek() {
        int i = this.size;
        if (i != 0) {
            return m13390xi(i - 1);
        }
        throw cPR();
    }

    public final Object pop() {
        Object obj;
        if (this.dIn) {
            throw cPS();
        }
        int i = this.size - 1;
        switch (i) {
            case -1:
                throw cPR();
            case 0:
                obj = this.huX;
                this.huX = null;
                break;
            case 1:
                obj = this.huY;
                this.huY = null;
                break;
            case 2:
                obj = this.huZ;
                this.huZ = null;
                break;
            case 3:
                obj = this.hva;
                this.hva = null;
                break;
            case 4:
                obj = this.hvb;
                this.hvb = null;
                break;
            default:
                obj = this.f2496oj[i - 5];
                this.f2496oj[i - 5] = null;
                break;
        }
        this.size = i;
        return obj;
    }

    public final void push(Object obj) {
        add(obj);
    }

    public final void add(Object obj) {
        if (this.dIn) {
            throw cPS();
        }
        int i = this.size;
        if (i >= 5) {
            ensureCapacity(i + 1);
        }
        this.size = i + 1;
        m13389g(i, obj);
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0035, code lost:
        r6.size = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0039, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004f, code lost:
        if (r1 != 2) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0051, code lost:
        r6.huZ = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0054, code lost:
        r8 = r6.huZ;
        r6.huZ = r0;
        r0 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005a, code lost:
        if (r1 != 3) goto L_0x005f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005c, code lost:
        r6.hva = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005f, code lost:
        r8 = r6.hva;
        r6.hva = r0;
        r0 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0065, code lost:
        if (r1 != 4) goto L_0x006a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0067, code lost:
        r6.hvb = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x006a, code lost:
        r8 = r6.hvb;
        r6.hvb = r0;
        r7 = 5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void add(int r7, java.lang.Object r8) {
        /*
            r6 = this;
            int r1 = r6.size
            if (r7 < 0) goto L_0x0006
            if (r7 <= r1) goto L_0x000d
        L_0x0006:
            int r0 = r1 + 1
            java.lang.RuntimeException r0 = m13388ay(r7, r0)
            throw r0
        L_0x000d:
            boolean r0 = r6.dIn
            if (r0 == 0) goto L_0x0016
            java.lang.RuntimeException r0 = cPS()
            throw r0
        L_0x0016:
            switch(r7) {
                case 0: goto L_0x003a;
                case 1: goto L_0x0076;
                case 2: goto L_0x0074;
                case 3: goto L_0x0072;
                case 4: goto L_0x0070;
                default: goto L_0x0019;
            }
        L_0x0019:
            int r0 = r1 + 1
            r6.ensureCapacity(r0)
            if (r7 == r1) goto L_0x002f
            java.lang.Object[] r0 = r6.f2496oj
            int r2 = r7 + -5
            java.lang.Object[] r3 = r6.f2496oj
            int r4 = r7 + -5
            int r4 = r4 + 1
            int r5 = r1 - r7
            java.lang.System.arraycopy(r0, r2, r3, r4, r5)
        L_0x002f:
            java.lang.Object[] r0 = r6.f2496oj
            int r2 = r7 + -5
            r0[r2] = r8
        L_0x0035:
            int r0 = r1 + 1
            r6.size = r0
            return
        L_0x003a:
            if (r1 != 0) goto L_0x003f
            r6.huX = r8
            goto L_0x0035
        L_0x003f:
            java.lang.Object r0 = r6.huX
            r6.huX = r8
        L_0x0043:
            r2 = 1
            if (r1 != r2) goto L_0x0049
            r6.huY = r0
            goto L_0x0035
        L_0x0049:
            java.lang.Object r8 = r6.huY
            r6.huY = r0
            r0 = r8
        L_0x004e:
            r2 = 2
            if (r1 != r2) goto L_0x0054
            r6.huZ = r0
            goto L_0x0035
        L_0x0054:
            java.lang.Object r8 = r6.huZ
            r6.huZ = r0
            r0 = r8
        L_0x0059:
            r2 = 3
            if (r1 != r2) goto L_0x005f
            r6.hva = r0
            goto L_0x0035
        L_0x005f:
            java.lang.Object r8 = r6.hva
            r6.hva = r0
            r0 = r8
        L_0x0064:
            r2 = 4
            if (r1 != r2) goto L_0x006a
            r6.hvb = r0
            goto L_0x0035
        L_0x006a:
            java.lang.Object r8 = r6.hvb
            r6.hvb = r0
            r7 = 5
            goto L_0x0019
        L_0x0070:
            r0 = r8
            goto L_0x0064
        L_0x0072:
            r0 = r8
            goto L_0x0059
        L_0x0074:
            r0 = r8
            goto L_0x004e
        L_0x0076:
            r0 = r8
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C5271aCx.add(int, java.lang.Object):void");
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void remove(int r8) {
        /*
            r7 = this;
            r6 = 0
            int r0 = r7.size
            if (r8 < 0) goto L_0x0007
            if (r8 < r0) goto L_0x000c
        L_0x0007:
            java.lang.RuntimeException r0 = m13388ay(r8, r0)
            throw r0
        L_0x000c:
            boolean r1 = r7.dIn
            if (r1 == 0) goto L_0x0015
            java.lang.RuntimeException r0 = cPS()
            throw r0
        L_0x0015:
            int r0 = r0 + -1
            switch(r8) {
                case 0: goto L_0x0034;
                case 1: goto L_0x003d;
                case 2: goto L_0x0047;
                case 3: goto L_0x0051;
                case 4: goto L_0x005b;
                default: goto L_0x001a;
            }
        L_0x001a:
            if (r8 == r0) goto L_0x002b
            java.lang.Object[] r1 = r7.f2496oj
            int r2 = r8 + -5
            int r2 = r2 + 1
            java.lang.Object[] r3 = r7.f2496oj
            int r4 = r8 + -5
            int r5 = r0 - r8
            java.lang.System.arraycopy(r1, r2, r3, r4, r5)
        L_0x002b:
            java.lang.Object[] r1 = r7.f2496oj
            int r2 = r0 + -5
            r1[r2] = r6
        L_0x0031:
            r7.size = r0
            return
        L_0x0034:
            if (r0 != 0) goto L_0x0039
            r7.huX = r6
            goto L_0x0031
        L_0x0039:
            java.lang.Object r1 = r7.huY
            r7.huX = r1
        L_0x003d:
            r1 = 1
            if (r0 != r1) goto L_0x0043
            r7.huY = r6
            goto L_0x0031
        L_0x0043:
            java.lang.Object r1 = r7.huZ
            r7.huY = r1
        L_0x0047:
            r1 = 2
            if (r0 != r1) goto L_0x004d
            r7.huZ = r6
            goto L_0x0031
        L_0x004d:
            java.lang.Object r1 = r7.hva
            r7.huZ = r1
        L_0x0051:
            r1 = 3
            if (r0 != r1) goto L_0x0057
            r7.hva = r6
            goto L_0x0031
        L_0x0057:
            java.lang.Object r1 = r7.hvb
            r7.hva = r1
        L_0x005b:
            r1 = 4
            if (r0 != r1) goto L_0x0061
            r7.hvb = r6
            goto L_0x0031
        L_0x0061:
            java.lang.Object[] r1 = r7.f2496oj
            r2 = 0
            r1 = r1[r2]
            r7.hvb = r1
            r8 = 5
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C5271aCx.remove(int):void");
    }

    public final void clear() {
        if (this.dIn) {
            throw cPS();
        }
        int i = this.size;
        for (int i2 = 0; i2 != i; i2++) {
            m13389g(i2, (Object) null);
        }
        this.size = 0;
    }

    public final Object[] toArray() {
        Object[] objArr = new Object[this.size];
        toArray(objArr, 0);
        return objArr;
    }

    public final void toArray(Object[] objArr) {
        toArray(objArr, 0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000f, code lost:
        r5[r6 + 4] = r4.hvb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0015, code lost:
        r5[r6 + 3] = r4.hva;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001b, code lost:
        r5[r6 + 2] = r4.huZ;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0021, code lost:
        r5[r6 + 1] = r4.huY;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0027, code lost:
        r5[r6 + 0] = r4.huX;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void toArray(java.lang.Object[] r5, int r6) {
        /*
            r4 = this;
            int r0 = r4.size
            switch(r0) {
                case 0: goto L_0x002d;
                case 1: goto L_0x0027;
                case 2: goto L_0x0021;
                case 3: goto L_0x001b;
                case 4: goto L_0x0015;
                case 5: goto L_0x000f;
                default: goto L_0x0005;
            }
        L_0x0005:
            java.lang.Object[] r1 = r4.f2496oj
            r2 = 0
            int r3 = r6 + 5
            int r0 = r0 + -5
            java.lang.System.arraycopy(r1, r2, r5, r3, r0)
        L_0x000f:
            int r0 = r6 + 4
            java.lang.Object r1 = r4.hvb
            r5[r0] = r1
        L_0x0015:
            int r0 = r6 + 3
            java.lang.Object r1 = r4.hva
            r5[r0] = r1
        L_0x001b:
            int r0 = r6 + 2
            java.lang.Object r1 = r4.huZ
            r5[r0] = r1
        L_0x0021:
            int r0 = r6 + 1
            java.lang.Object r1 = r4.huY
            r5[r0] = r1
        L_0x0027:
            int r0 = r6 + 0
            java.lang.Object r1 = r4.huX
            r5[r0] = r1
        L_0x002d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C5271aCx.toArray(java.lang.Object[], int):void");
    }

    private void ensureCapacity(int i) {
        int i2 = 10;
        int i3 = i - 5;
        if (i3 <= 0) {
            throw new IllegalArgumentException();
        } else if (this.f2496oj == null) {
            if (10 >= i3) {
                i3 = 10;
            }
            this.f2496oj = new Object[i3];
        } else {
            int length = this.f2496oj.length;
            if (length < i3) {
                if (length > 5) {
                    i2 = length * 2;
                }
                if (i2 >= i3) {
                    i3 = i2;
                }
                Object[] objArr = new Object[i3];
                if (this.size > 5) {
                    System.arraycopy(this.f2496oj, 0, objArr, 0, this.size - 5);
                }
                this.f2496oj = objArr;
            }
        }
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        int i = this.size;
        for (int i2 = 0; i2 != i; i2++) {
            objectOutputStream.writeObject(m13390xi(i2));
        }
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        int i = this.size;
        if (i > 5) {
            this.f2496oj = new Object[(i - 5)];
        }
        for (int i2 = 0; i2 != i; i2++) {
            m13389g(i2, objectInputStream.readObject());
        }
    }
}
