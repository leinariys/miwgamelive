package org.mozilla1.javascript;


import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;

/* renamed from: a.amL  reason: case insensitive filesystem */
/* compiled from: a */
public final class LazilyLoadedCtor implements Serializable {
    private static final int gbt = 0;
    private static final int gbu = 1;
    private static final int gbv = 2;

    private final String className;
    private final boolean dIn;
    private final org.mozilla1.javascript.ScriptableObject gbw;
    private final String gbx;
    private Object gby;
    private int state = 0;

    public LazilyLoadedCtor(org.mozilla1.javascript.ScriptableObject akn, String str, String str2, boolean z) {
        this.gbw = akn;
        this.gbx = str;
        this.className = str2;
        this.dIn = z;
        akn.mo14499a(str, 0, this, 2);
    }

    /* access modifiers changed from: package-private */
    public void init() {
        Object obj;
        synchronized (this) {
            if (this.state == 1) {
                throw new IllegalStateException("Recursive initialization for " + this.gbx);
            } else if (this.state == 0) {
                this.state = 1;
                Object obj2 = org.mozilla1.javascript.Scriptable.NOT_FOUND;
                try {
                    obj = cjT();
                } finally {
                    this.gby = obj;
                    this.state = 2;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Object getValue() {
        if (this.state == 2) {
            return this.gby;
        }
        throw new IllegalStateException(this.gbx);
    }

    private Object cjT() {
        Class<? extends org.mozilla1.javascript.Scriptable> aq = m23877aq(Kit.classOrNull(this.className));
        if (aq != null) {
            try {
                BaseFunction b = ScriptableObject.m23574b(this.gbw, aq, this.dIn, false);
                if (b != null) {
                    return b;
                }
                Object obj = this.gbw.get(this.gbx, (org.mozilla1.javascript.Scriptable) this.gbw);
                if (obj != org.mozilla1.javascript.Scriptable.NOT_FOUND) {
                    return obj;
                }
            } catch (InvocationTargetException e) {
                Throwable targetException = e.getTargetException();
                if (targetException instanceof RuntimeException) {
                    throw ((RuntimeException) targetException);
                }
            } catch (RhinoException | IllegalAccessException | InstantiationException | SecurityException e2) {
            }
        }
        return org.mozilla1.javascript.Scriptable.NOT_FOUND;
    }

    /* renamed from: aq */
    private Class<? extends Scriptable> m23877aq(Class<?> cls) {
        return cls;
    }
}
