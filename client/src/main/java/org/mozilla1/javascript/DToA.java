package org.mozilla1.javascript;


import java.math.BigInteger;

/* renamed from: a.Zv */
/* compiled from: a */
class DToA {
    static final int eNE = 0;
    static final int eNF = 1;
    static final int eNG = 2;
    static final int eNH = 3;
    static final int eNI = 4;
    private static final int eND = 1078;
    private static final int eNJ = 1048575;
    private static final int eNK = 20;
    private static final int eNL = 1048576;
    private static final long eNM = 4503599627370495L;
    private static final int eNN = 52;
    private static final long eNO = 4503599627370496L;
    private static final int eNP = 1023;
    private static final int eNQ = 53;
    private static final int eNR = 20;
    private static final int eNS = 2146435072;
    private static final int eNT = 2047;
    private static final int eNU = 1048575;
    private static final int eNV = 1;
    private static final int eNW = Integer.MIN_VALUE;
    private static final int eNX = 1072693248;
    private static final int eNY = 22;
    private static final int eNZ = 14;
    private static final int eOa = 16;
    private static final int eOb = 1048575;
    private static final int eOc = 14;
    private static final int eOd = 5;
    private static final double[] eOe = {1.0d, 10.0d, 100.0d, 1000.0d, 10000.0d, 100000.0d, 1000000.0d, 1.0E7d, 1.0E8d, 1.0E9d, 1.0E10d, 1.0E11d, 1.0E12d, 1.0E13d, 1.0E14d, 1.0E15d, 1.0E16d, 1.0E17d, 1.0E18d, 1.0E19d, 1.0E20d, 1.0E21d, 1.0E22d};
    private static final double[] eOf = {1.0E16d, 1.0E32d, 1.0E64d, 1.0E128d, 1.0E256d};
    private static final int[] eOg;

    static {
        int[] iArr = new int[5];
        iArr[2] = 3;
        iArr[3] = 2;
        iArr[4] = 2;
        eOg = iArr;
    }

    DToA() {
    }

    /* renamed from: pl */
    private static char m12252pl(int i) {
        return (char) (i >= 10 ? i + 87 : i + 48);
    }

    /* renamed from: pm */
    private static int m12253pm(int i) {
        int i2;
        int i3 = 0;
        if ((i & 7) == 0) {
            if ((65535 & i) == 0) {
                i3 = 16;
                i2 = i >>> 16;
            } else {
                i2 = i;
            }
            if ((i2 & 255) == 0) {
                i3 += 8;
                i2 >>>= 8;
            }
            if ((i2 & 15) == 0) {
                i3 += 4;
                i2 >>>= 4;
            }
            if ((i2 & 3) == 0) {
                i3 += 2;
                i2 >>>= 2;
            }
            if ((i2 & 1) != 0) {
                return i3;
            }
            int i4 = i3 + 1;
            if (((i2 >>> 1) & 1) == 0) {
                return 32;
            }
            return i4;
        } else if ((i & 1) != 0) {
            return 0;
        } else {
            if ((i & 2) != 0) {
                return 1;
            }
            return 2;
        }
    }

    /* renamed from: pn */
    private static int m12254pn(int i) {
        int i2;
        int i3 = 0;
        if ((-65536 & i) == 0) {
            i3 = 16;
            i2 = i << 16;
        } else {
            i2 = i;
        }
        if ((-16777216 & i2) == 0) {
            i3 += 8;
            i2 <<= 8;
        }
        if ((-268435456 & i2) == 0) {
            i3 += 4;
            i2 <<= 4;
        }
        if ((-1073741824 & i2) == 0) {
            i3 += 2;
            i2 <<= 2;
        }
        if ((Integer.MIN_VALUE & i2) != 0) {
            return i3;
        }
        int i4 = i3 + 1;
        if ((i2 & 1073741824) == 0) {
            return 32;
        }
        return i4;
    }

    /* renamed from: d */
    private static void m12251d(byte[] bArr, int i, int i2) {
        bArr[i] = (byte) (i2 >> 24);
        bArr[i + 1] = (byte) (i2 >> 16);
        bArr[i + 2] = (byte) (i2 >> 8);
        bArr[i + 3] = (byte) i2;
    }

    /* renamed from: a */
    private static BigInteger m12245a(double d, int[] iArr, int[] iArr2) {
        byte[] bArr;
        int i;
        int i2 = 1;
        long doubleToLongBits = Double.doubleToLongBits(d);
        int i3 = (int) (doubleToLongBits >>> 32);
        int i4 = (int) doubleToLongBits;
        int i5 = 1048575 & i3;
        int i6 = (Integer.MAX_VALUE & i3) >>> 20;
        if (i6 != 0) {
            i5 |= eNL;
        }
        if (i4 != 0) {
            bArr = new byte[8];
            i = m12253pm(i4);
            int i7 = i4 >>> i;
            if (i != 0) {
                m12251d(bArr, 4, i7 | (i5 << (32 - i)));
                i5 >>= i;
            } else {
                m12251d(bArr, 4, i7);
            }
            m12251d(bArr, 0, i5);
            if (i5 != 0) {
                i2 = 2;
            }
        } else {
            bArr = new byte[4];
            int pm = m12253pm(i5);
            i5 >>>= pm;
            m12251d(bArr, 0, i5);
            i = pm + 32;
        }
        if (i6 != 0) {
            iArr[0] = ((i6 - 1023) - 52) + i;
            iArr2[0] = 53 - i;
        } else {
            iArr[0] = i + ((i6 - 1023) - 52) + 1;
            iArr2[0] = (i2 * 32) - m12254pn(i5);
        }
        return new BigInteger(bArr);
    }

    /* renamed from: a */
    static String JS_dtobasestr(int i, double d) {
        boolean z;
        long j;
        long j2;
        String bigInteger;
        BigInteger bigInteger2;
        BigInteger multiply;
        BigInteger bigInteger3;
        boolean z2;
        int i2;
        int i3;
        long j3;
        if (2 > i || i > 36) {
            throw new IllegalArgumentException("Bad base: " + i);
        } else if (Double.isNaN(d)) {
            return "NaN";
        } else {
            if (Double.isInfinite(d)) {
                return d > org.mozilla1.javascript.ScriptRuntime.NaN ? "Infinity" : "-Infinity";
            }
            if (d == org.mozilla1.javascript.ScriptRuntime.NaN) {
                return "0";
            }
            if (d >= ScriptRuntime.NaN) {
                z = false;
            } else {
                z = true;
                d = -d;
            }
            double floor = Math.floor(d);
            long j4 = (long) floor;
            if (((double) j4) == floor) {
                if (z) {
                    j3 = -j4;
                } else {
                    j3 = j4;
                }
                bigInteger = Long.toString(j3, i);
            } else {
                long doubleToLongBits = Double.doubleToLongBits(floor);
                int i4 = ((int) (doubleToLongBits >> 52)) & eNT;
                if (i4 == 0) {
                    j = (doubleToLongBits & eNM) << 1;
                } else {
                    j = (doubleToLongBits & eNM) | eNO;
                }
                if (z) {
                    j2 = -j;
                } else {
                    j2 = j;
                }
                int i5 = i4 - 1075;
                BigInteger valueOf = BigInteger.valueOf(j2);
                if (i5 > 0) {
                    valueOf = valueOf.shiftLeft(i5);
                } else if (i5 < 0) {
                    valueOf = valueOf.shiftRight(-i5);
                }
                bigInteger = valueOf.toString(i);
            }
            if (d == floor) {
                return bigInteger;
            }
            char[] cArr = new char[eND];
            int i6 = 0;
            double d2 = d - floor;
            long doubleToLongBits2 = Double.doubleToLongBits(d);
            int i7 = (int) (doubleToLongBits2 >> 32);
            int i8 = (int) doubleToLongBits2;
            int[] iArr = new int[1];
            BigInteger a = m12245a(d2, iArr, new int[1]);
            int i9 = -((i7 >>> 20) & eNT);
            if (i9 == 0) {
                i9 = -1;
            }
            int i10 = i9 + 1076;
            BigInteger valueOf2 = BigInteger.valueOf(1);
            if (i8 == 0 && (1048575 & i7) == 0 && (2145386496 & i7) != 0) {
                i10++;
                bigInteger2 = BigInteger.valueOf(2);
            } else {
                bigInteger2 = valueOf2;
            }
            BigInteger shiftLeft = a.shiftLeft(iArr[0] + i10);
            BigInteger shiftLeft2 = BigInteger.valueOf(1).shiftLeft(i10);
            BigInteger valueOf3 = BigInteger.valueOf((long) i);
            boolean z3 = false;
            BigInteger bigInteger4 = bigInteger2;
            BigInteger bigInteger5 = valueOf2;
            while (true) {
                BigInteger[] divideAndRemainder = shiftLeft.multiply(valueOf3).divideAndRemainder(shiftLeft2);
                shiftLeft = divideAndRemainder[1];
                int intValue = (char) divideAndRemainder[0].intValue();
                if (bigInteger5 == bigInteger4) {
                    BigInteger multiply2 = bigInteger5.multiply(valueOf3);
                    multiply = multiply2;
                    bigInteger5 = multiply2;
                } else {
                    bigInteger5 = bigInteger5.multiply(valueOf3);
                    multiply = bigInteger4.multiply(valueOf3);
                }
                int compareTo = shiftLeft.compareTo(bigInteger5);
                BigInteger subtract = shiftLeft2.subtract(multiply);
                int compareTo2 = subtract.signum() <= 0 ? 1 : shiftLeft.compareTo(subtract);
                if (compareTo2 == 0 && (i8 & 1) == 0) {
                    if (compareTo > 0) {
                        i3 = intValue + 1;
                    } else {
                        i3 = intValue;
                    }
                    z2 = true;
                    i2 = i3;
                } else if (compareTo < 0 || (compareTo == 0 && (i8 & 1) == 0)) {
                    if (compareTo2 > 0) {
                        BigInteger shiftLeft3 = shiftLeft.shiftLeft(1);
                        if (shiftLeft3.compareTo(shiftLeft2) > 0) {
                            intValue++;
                            bigInteger3 = shiftLeft3;
                        } else {
                            bigInteger3 = shiftLeft3;
                        }
                    } else {
                        bigInteger3 = shiftLeft;
                    }
                    z2 = true;
                    shiftLeft = bigInteger3;
                    i2 = intValue;
                } else if (compareTo2 > 0) {
                    z2 = true;
                    i2 = intValue + 1;
                } else {
                    z2 = z3;
                    i2 = intValue;
                }
                int i11 = i6 + 1;
                cArr[i6] = m12252pl(i2);
                if (z2) {
                    StringBuffer stringBuffer = new StringBuffer(bigInteger.length() + 1 + i11);
                    stringBuffer.append(bigInteger);
                    stringBuffer.append('.');
                    stringBuffer.append(cArr, 0, i11);
                    return stringBuffer.toString();
                }
                z3 = z2;
                bigInteger4 = multiply;
                i6 = i11;
            }
        }
    }

    /* renamed from: H */
    static int m12241H(double d) {
        return (int) (Double.doubleToLongBits(d) >> 32);
    }

    /* renamed from: c */
    static double m12250c(double d, int i) {
        return Double.longBitsToDouble((Double.doubleToLongBits(d) & 4294967295L) | (((long) i) << 32));
    }

    /* renamed from: I */
    static int m12242I(double d) {
        return (int) Double.doubleToLongBits(d);
    }

    /* renamed from: a */
    static BigInteger m12246a(BigInteger bigInteger, int i) {
        return bigInteger.multiply(BigInteger.valueOf(5).pow(i));
    }

    /* renamed from: a */
    static boolean m12248a(StringBuffer stringBuffer) {
        int length = stringBuffer.length();
        while (length != 0) {
            length--;
            char charAt = stringBuffer.charAt(length);
            if (charAt != '9') {
                stringBuffer.setCharAt(length, (char) (charAt + 1));
                stringBuffer.setLength(length + 1);
                return false;
            }
        }
        stringBuffer.setLength(0);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:0x021e, code lost:
        if (r16 >= (-r30)) goto L_0x0230;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0220, code lost:
        r40.setLength(0);
        r40.append('0');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0230, code lost:
        r6 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0231, code lost:
        if (r6 != false) goto L_0x070c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0234, code lost:
        if (r23 == false) goto L_0x02ea;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0236, code lost:
        r14 = (0.5d / eOe[r8 - 1]) - r30;
        r6 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0243, code lost:
        r30 = (long) r16;
        r16 = r16 - ((double) r30);
        r40.append((char) ((int) (r30 + 48)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x025e, code lost:
        if (r16 >= r14) goto L_0x0264;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x026a, code lost:
        if ((1.0d - r16) >= r14) goto L_0x029e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x026c, code lost:
        r4 = r40.charAt(r40.length() - 1);
        r40.setLength(r40.length() - 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x0285, code lost:
        if (r4 == '9') goto L_0x0293;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0287, code lost:
        r40.append((char) (r4 + 1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0297, code lost:
        if (r40.length() != 0) goto L_0x026c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0299, code lost:
        r7 = r7 + 1;
        r4 = '0';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x029e, code lost:
        r6 = r6 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x02a0, code lost:
        if (r6 < r8) goto L_0x02e0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x02a2, code lost:
        r6 = true;
        r14 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x02a5, code lost:
        if (r6 == false) goto L_0x0703;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x02a7, code lost:
        r40.setLength(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x02b0, code lost:
        if (r27[0] < 0) goto L_0x03d4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x02b4, code lost:
        if (r4 > 14) goto L_0x03d4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x02b6, code lost:
        r6 = eOe[r4];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x02ba, code lost:
        if (r24 >= 0) goto L_0x0369;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x02bc, code lost:
        if (r11 > 0) goto L_0x0369;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x02be, code lost:
        if (r11 < 0) goto L_0x02d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x02c5, code lost:
        if (r34 < (5.0d * r6)) goto L_0x02d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x02c7, code lost:
        if (r37 != false) goto L_0x035c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x02ce, code lost:
        if (r34 != (r6 * 5.0d)) goto L_0x035c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x02d0, code lost:
        r40.setLength(0);
        r40.append('0');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x02e0, code lost:
        r14 = r14 * 10.0d;
        r16 = r16 * 10.0d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x02ea, code lost:
        r30 = r30 * eOe[r8 - 1];
        r6 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x02f3, code lost:
        r32 = (long) r16;
        r14 = r16 - ((double) r32);
        r40.append((char) ((int) (48 + r32)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x030a, code lost:
        if (r6 != r8) goto L_0x0355;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x0312, code lost:
        if (r14 <= (0.5d + r30)) goto L_0x0346;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x0314, code lost:
        r4 = r40.charAt(r40.length() - 1);
        r40.setLength(r40.length() - 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x032d, code lost:
        if (r4 == '9') goto L_0x033b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x032f, code lost:
        r40.append((char) (r4 + 1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x033f, code lost:
        if (r40.length() != 0) goto L_0x0314;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x0341, code lost:
        r7 = r7 + 1;
        r4 = '0';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x034c, code lost:
        if (r14 >= (0.5d - r30)) goto L_0x0709;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x034e, code lost:
        m12249b(r40);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x0355, code lost:
        r6 = r6 + 1;
        r16 = 10.0d * r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x035c, code lost:
        r40.append('1');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0369, code lost:
        r5 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x036a, code lost:
        r8 = (long) (r34 / r6);
        r12 = r34 - (((double) r8) * r6);
        r40.append((char) ((int) (48 + r8)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x037b, code lost:
        if (r5 != r11) goto L_0x03c7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x037d, code lost:
        r10 = r12 + r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x0381, code lost:
        if (r10 > r6) goto L_0x0392;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x0385, code lost:
        if (r10 != r6) goto L_0x03b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x038e, code lost:
        if ((1 & r8) != 0) goto L_0x0392;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x0390, code lost:
        if (r37 == false) goto L_0x03b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x0392, code lost:
        r5 = r40.charAt(r40.length() - 1);
        r40.setLength(r40.length() - 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x03ab, code lost:
        if (r5 == '9') goto L_0x03bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x03ad, code lost:
        r6 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x03ae, code lost:
        r40.append((char) (r5 + 1));
        r4 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x03bf, code lost:
        if (r40.length() != 0) goto L_0x0392;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x03c1, code lost:
        r6 = r4 + 1;
        r5 = '0';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x03c7, code lost:
        r34 = r12 * 10.0d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x03cf, code lost:
        if (r34 == p001a.C0903NH.NaN) goto L_0x03b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x03d1, code lost:
        r5 = r5 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x03d4, code lost:
        r14 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x03d5, code lost:
        if (r23 == false) goto L_0x06f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x03da, code lost:
        if (r25 >= 2) goto L_0x04e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x03dc, code lost:
        if (r26 == false) goto L_0x04dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x03de, code lost:
        r6 = r27[0] + 1075;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x03e3, code lost:
        r7 = r18;
        r8 = r19;
        r9 = r20;
        r10 = r6;
        r13 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x03ec, code lost:
        r16 = r20 + r10;
        r10 = r10 + r22;
        r14 = java.math.BigInteger.valueOf(1);
        r18 = r7;
        r17 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:184:0x03fa, code lost:
        if (r9 <= 0) goto L_0x06ea;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x03fc, code lost:
        if (r10 <= 0) goto L_0x06ea;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x03fe, code lost:
        if (r9 >= r10) goto L_0x0500;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x0400, code lost:
        r6 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:0x0401, code lost:
        r13 = r16 - r6;
        r10 = r10 - r6;
        r15 = r9 - r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:0x0406, code lost:
        if (r17 <= 0) goto L_0x06e5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x0408, code lost:
        if (r23 == false) goto L_0x0503;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x040a, code lost:
        if (r8 <= 0) goto L_0x06e0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x040c, code lost:
        r6 = m12246a(r14, r8);
        r7 = r6.multiply(r21);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:193:0x0416, code lost:
        r8 = r17 - r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x0418, code lost:
        if (r8 == 0) goto L_0x041e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x041a, code lost:
        r7 = m12246a(r7, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:196:0x041e, code lost:
        r14 = java.math.BigInteger.valueOf(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:0x0424, code lost:
        if (r18 <= 0) goto L_0x042c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x0426, code lost:
        r14 = m12246a(r14, r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x042c, code lost:
        r8 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:200:0x0430, code lost:
        if (r25 >= 2) goto L_0x06dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x0436, code lost:
        if (m12242I(r34) != 0) goto L_0x06dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:204:0x0441, code lost:
        if ((m12241H(r34) & 1048575) != 0) goto L_0x06dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:206:0x044b, code lost:
        if ((m12241H(r34) & 2145386496) == 0) goto L_0x06dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x044d, code lost:
        r13 = r13 + 1;
        r9 = r10 + 1;
        r8 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:208:0x0452, code lost:
        r19 = r14.toByteArray();
        r17 = 0;
        r16 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:210:0x045e, code lost:
        if (r16 < 4) goto L_0x050e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x0460, code lost:
        if (r18 == 0) goto L_0x052b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:212:0x0462, code lost:
        r10 = 32 - m12254pn(r17);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:213:0x0468, code lost:
        r10 = (r10 + r9) & 31;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:214:0x046b, code lost:
        if (r10 == 0) goto L_0x046f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:215:0x046d, code lost:
        r10 = 32 - r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:217:0x0473, code lost:
        if (r10 <= 4) goto L_0x052e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:218:0x0475, code lost:
        r10 = r10 - 4;
        r16 = r13 + r10;
        r13 = r15 + r10;
        r10 = r9 + r10;
        r15 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:219:0x047e, code lost:
        if (r16 <= 0) goto L_0x06d5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:220:0x0480, code lost:
        r9 = r7.shiftLeft(r16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:221:0x0486, code lost:
        if (r10 <= 0) goto L_0x06d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:222:0x0488, code lost:
        r13 = r14.shiftLeft(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:223:0x048d, code lost:
        if (r5 == false) goto L_0x06ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:225:0x0493, code lost:
        if (r9.compareTo(r13) >= 0) goto L_0x06ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:226:0x0495, code lost:
        r4 = r4 - 1;
        r9 = r9.multiply(java.math.BigInteger.valueOf(10));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:227:0x04a1, code lost:
        if (r23 == false) goto L_0x04ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:228:0x04a3, code lost:
        r6 = r6.multiply(java.math.BigInteger.valueOf(10));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:229:0x04ad, code lost:
        r5 = r6;
        r7 = r4;
        r11 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:230:0x04b0, code lost:
        if (r11 > 0) goto L_0x054c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:232:0x04b5, code lost:
        if (r25 <= 2) goto L_0x054c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:233:0x04b7, code lost:
        if (r11 < 0) goto L_0x04cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:234:0x04b9, code lost:
        r4 = r9.compareTo(r13.multiply(java.math.BigInteger.valueOf(5)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:235:0x04c7, code lost:
        if (r4 < 0) goto L_0x04cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:236:0x04c9, code lost:
        if (r4 != 0) goto L_0x053f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:237:0x04cb, code lost:
        if (r37 != false) goto L_0x053f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:238:0x04cd, code lost:
        r40.setLength(0);
        r40.append('0');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:239:0x04dd, code lost:
        r6 = 54 - r28[0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:240:0x04e4, code lost:
        r6 = r11 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:241:0x04e8, code lost:
        if (r19 < r6) goto L_0x04f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:242:0x04ea, code lost:
        r8 = r19 - r6;
        r6 = r18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:243:0x04ee, code lost:
        if (r11 >= 0) goto L_0x06ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:244:0x04f0, code lost:
        r9 = r20 - r11;
        r10 = 0;
        r7 = r6;
        r13 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:245:0x04f8, code lost:
        r7 = r6 - r19;
        r6 = r18 + r7;
        r19 = r19 + r7;
        r8 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:246:0x0500, code lost:
        r6 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:247:0x0503, code lost:
        r7 = m12246a(r21, r17);
        r6 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:248:0x050e, code lost:
        r10 = r17 << 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:249:0x0519, code lost:
        if (r16 >= r19.length) goto L_0x0525;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:250:0x051b, code lost:
        r10 = r10 | (r19[r16] & 255);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:251:0x0525, code lost:
        r16 = r16 + 1;
        r17 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:252:0x052b, code lost:
        r10 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:254:0x0532, code lost:
        if (r10 >= 4) goto L_0x06d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:255:0x0534, code lost:
        r10 = r10 + 28;
        r16 = r13 + r10;
        r13 = r15 + r10;
        r10 = r9 + r10;
        r15 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:256:0x053f, code lost:
        r40.append('1');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:257:0x054c, code lost:
        if (r23 == false) goto L_0x0699;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:258:0x054e, code lost:
        if (r15 <= 0) goto L_0x0554;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:259:0x0550, code lost:
        r5 = r5.shiftLeft(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:260:0x0554, code lost:
        if (r8 == false) goto L_0x06cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:261:0x0556, code lost:
        r4 = r5.shiftLeft(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:262:0x055b, code lost:
        r8 = r4;
        r10 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:263:0x055e, code lost:
        r4 = r9.divideAndRemainder(r13);
        r6 = r4[1];
        r4 = (char) (r4[0].intValue() + 48);
        r12 = r6.compareTo(r5);
        r9 = r13.subtract(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:264:0x057b, code lost:
        if (r9.signum() > 0) goto L_0x05a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:265:0x057d, code lost:
        r9 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:266:0x057e, code lost:
        if (r9 != 0) goto L_0x05bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:267:0x0580, code lost:
        if (r25 != 0) goto L_0x05bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:269:0x0588, code lost:
        if ((m12242I(r34) & 1) != 0) goto L_0x05bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:271:0x058c, code lost:
        if (r4 != '9') goto L_0x05ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:272:0x058e, code lost:
        r40.append('9');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:273:0x0599, code lost:
        if (m12248a(r40) == false) goto L_0x06c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:274:0x059b, code lost:
        r4 = r7 + 1;
        r40.append('1');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:276:0x05a8, code lost:
        r9 = r6.compareTo(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:277:0x05ad, code lost:
        if (r12 <= 0) goto L_0x05b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:278:0x05af, code lost:
        r4 = (char) (r4 + 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:279:0x05b2, code lost:
        r40.append(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:280:0x05bb, code lost:
        if (r12 < 0) goto L_0x05c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:281:0x05bd, code lost:
        if (r12 != 0) goto L_0x060a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:282:0x05bf, code lost:
        if (r25 != 0) goto L_0x060a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:284:0x05c7, code lost:
        if ((m12242I(r34) & 1) != 0) goto L_0x060a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:285:0x05c9, code lost:
        if (r9 <= 0) goto L_0x0601;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:286:0x05cb, code lost:
        r5 = r6.shiftLeft(1).compareTo(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:287:0x05d4, code lost:
        if (r5 > 0) goto L_0x05df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:288:0x05d6, code lost:
        if (r5 != 0) goto L_0x0601;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:290:0x05db, code lost:
        if ((r4 & 1) == 1) goto L_0x05df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:291:0x05dd, code lost:
        if (r37 == false) goto L_0x0601;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:292:0x05df, code lost:
        r5 = (char) (r4 + 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:293:0x05e4, code lost:
        if (r4 != '9') goto L_0x0600;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:294:0x05e6, code lost:
        r40.append('9');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:295:0x05f1, code lost:
        if (m12248a(r40) == false) goto L_0x05fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:296:0x05f3, code lost:
        r7 = r7 + 1;
        r40.append('1');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:298:0x0600, code lost:
        r4 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:299:0x0601, code lost:
        r40.append(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:300:0x060a, code lost:
        if (r9 <= 0) goto L_0x0636;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:302:0x060e, code lost:
        if (r4 != '9') goto L_0x062a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:303:0x0610, code lost:
        r40.append('9');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:304:0x061b, code lost:
        if (m12248a(r40) == false) goto L_0x0626;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:305:0x061d, code lost:
        r7 = r7 + 1;
        r40.append('1');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:307:0x062a, code lost:
        r40.append((char) (r4 + 1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:308:0x0636, code lost:
        r40.append(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:309:0x063b, code lost:
        if (r10 != r11) goto L_0x0665;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:310:0x063d, code lost:
        r5 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:311:0x063e, code lost:
        r5 = r5.shiftLeft(1).compareTo(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:312:0x0647, code lost:
        if (r5 > 0) goto L_0x0652;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:313:0x0649, code lost:
        if (r5 != 0) goto L_0x06c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:315:0x064e, code lost:
        if ((r4 & 1) == 1) goto L_0x0652;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:316:0x0650, code lost:
        if (r37 == false) goto L_0x06c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:318:0x0656, code lost:
        if (m12248a(r40) == false) goto L_0x06c4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:319:0x0658, code lost:
        r40.append('1');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:320:0x0665, code lost:
        r9 = r6.multiply(java.math.BigInteger.valueOf(10));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:321:0x066f, code lost:
        if (r5 != r8) goto L_0x0683;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:322:0x0671, code lost:
        r5 = r8.multiply(java.math.BigInteger.valueOf(10));
        r4 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:323:0x067d, code lost:
        r10 = r10 + 1;
        r8 = r4;
        r5 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:324:0x0683, code lost:
        r5 = r5.multiply(java.math.BigInteger.valueOf(10));
        r4 = r8.multiply(java.math.BigInteger.valueOf(10));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:325:0x0699, code lost:
        r4 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:326:0x069a, code lost:
        r6 = r9.divideAndRemainder(r13);
        r5 = r6[1];
        r6 = (char) (r6[0].intValue() + 48);
        r40.append(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:327:0x06b0, code lost:
        if (r4 < r11) goto L_0x06b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:328:0x06b2, code lost:
        r4 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:329:0x06b4, code lost:
        r9 = r5.multiply(java.math.BigInteger.valueOf(10));
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:330:0x06c1, code lost:
        m12249b(r40);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:332:0x06c8, code lost:
        r4 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:333:0x06cb, code lost:
        r4 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:334:0x06ce, code lost:
        r5 = r6;
        r7 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:335:0x06d2, code lost:
        r13 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:336:0x06d5, code lost:
        r9 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:337:0x06d8, code lost:
        r10 = r9;
        r16 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:338:0x06dd, code lost:
        r9 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:339:0x06e0, code lost:
        r6 = r14;
        r7 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:340:0x06e5, code lost:
        r6 = r14;
        r7 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:341:0x06ea, code lost:
        r15 = r9;
        r13 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:342:0x06ef, code lost:
        r7 = r6;
        r9 = r20;
        r10 = r11;
        r13 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:343:0x06f7, code lost:
        r10 = r22;
        r8 = r19;
        r9 = r20;
        r17 = r19;
        r16 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:344:0x0703, code lost:
        r4 = r7;
        r11 = r8;
        r34 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:345:0x0709, code lost:
        r6 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:346:0x070c, code lost:
        r14 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:347:0x0710, code lost:
        r16 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:348:0x0714, code lost:
        r6 = false;
        r7 = r4;
        r8 = r11;
        r9 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:349:0x071a, code lost:
        r14 = r34;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:350:0x071e, code lost:
        r8 = 2;
        r16 = r34;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:351:0x0723, code lost:
        r23 = r6;
        r24 = r38;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:387:?, code lost:
        return (r7 + 1) + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:388:?, code lost:
        return 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:389:?, code lost:
        return r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:390:?, code lost:
        return r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:391:?, code lost:
        return 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:392:?, code lost:
        return r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:393:?, code lost:
        return r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:394:?, code lost:
        return (r4 + 1) + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:395:?, code lost:
        return r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:396:?, code lost:
        return 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:397:?, code lost:
        return (r7 + 1) + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:398:?, code lost:
        return r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:399:?, code lost:
        return r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:400:?, code lost:
        return r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:401:?, code lost:
        return r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:402:?, code lost:
        return r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:403:?, code lost:
        return r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:404:?, code lost:
        return (r7 + 1) + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:405:?, code lost:
        return r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00fb, code lost:
        if (r11 < 0) goto L_0x02ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00ff, code lost:
        if (r11 > 14) goto L_0x02ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0101, code lost:
        if (r8 == false) goto L_0x02ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0103, code lost:
        r14 = 0;
        r13 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0105, code lost:
        if (r4 <= 0) goto L_0x01e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0107, code lost:
        r6 = eOe[r4 & 15];
        r9 = r4 >> 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0111, code lost:
        if ((r9 & true) == false) goto L_0x071e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0113, code lost:
        r9 = r9 & 15;
        r16 = r34 / eOf[4];
        r8 = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x011d, code lost:
        if (r9 != 0) goto L_0x01d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x011f, code lost:
        r14 = r16 / r6;
        r13 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0122, code lost:
        if (r5 == false) goto L_0x0714;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0128, code lost:
        if (r14 >= 1.0d) goto L_0x0714;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x012a, code lost:
        if (r11 <= 0) goto L_0x0714;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x012c, code lost:
        if (r12 > 0) goto L_0x020e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x012e, code lost:
        r6 = true;
        r7 = r4;
        r8 = r11;
        r9 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0132, code lost:
        r16 = (((double) r9) * r14) + 7.0d;
        r30 = m12250c(r16, m12241H(r16) - 54525952);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0148, code lost:
        if (r8 != 0) goto L_0x0710;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x014a, code lost:
        r16 = r14 - 5.0d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0150, code lost:
        if (r16 <= r30) goto L_0x0219;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0152, code lost:
        r40.append('1');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x01b9, code lost:
        if (r38 > 0) goto L_0x01bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01bb, code lost:
        r38 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01bd, code lost:
        r23 = r6;
        r12 = r38;
        r11 = r38;
        r24 = r38;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01c8, code lost:
        r11 = (r38 + r4) + 1;
        r12 = r11 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01ce, code lost:
        if (r11 > 0) goto L_0x0723;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01d0, code lost:
        r23 = r6;
        r24 = r38;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x01d8, code lost:
        if ((r9 & 1) == 0) goto L_0x01e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01da, code lost:
        r8 = r8 + 1;
        r6 = r6 * eOf[r14];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x01e2, code lost:
        r9 = r9 >> 1;
        r14 = r14 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01e8, code lost:
        r6 = -r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x01e9, code lost:
        if (r6 == 0) goto L_0x071a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01eb, code lost:
        r8 = eOe[r6 & 15] * r34;
        r7 = r6 >> 4;
        r6 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01f6, code lost:
        if (r7 != 0) goto L_0x01fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01f8, code lost:
        r13 = r6;
        r14 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01fe, code lost:
        if ((r7 & 1) == 0) goto L_0x0208;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0200, code lost:
        r6 = r6 + 1;
        r8 = r8 * eOf[r14];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0208, code lost:
        r7 = r7 >> 1;
        r14 = r14 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x020e, code lost:
        r7 = r4 - 1;
        r14 = r14 * 10.0d;
        r9 = r13 + 1;
        r6 = false;
        r8 = r12;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static int m12243a(double r34, int r36, boolean r37, int r38, boolean[] r39, java.lang.StringBuffer r40) {
        /*
            r4 = 1
            int[] r0 = new int[r4]
            r27 = r0
            r4 = 1
            int[] r0 = new int[r4]
            r28 = r0
            int r4 = m12241H(r34)
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = r4 & r5
            if (r4 == 0) goto L_0x004a
            r4 = 0
            r5 = 1
            r39[r4] = r5
            int r4 = m12241H(r34)
            r5 = 2147483647(0x7fffffff, float:NaN)
            r4 = r4 & r5
            r0 = r34
            double r34 = m12250c(r0, r4)
        L_0x0025:
            int r4 = m12241H(r34)
            r5 = 2146435072(0x7ff00000, float:NaN)
            r4 = r4 & r5
            r5 = 2146435072(0x7ff00000, float:NaN)
            if (r4 != r5) goto L_0x0052
            int r4 = m12242I(r34)
            if (r4 != 0) goto L_0x004f
            int r4 = m12241H(r34)
            r5 = 1048575(0xfffff, float:1.469367E-39)
            r4 = r4 & r5
            if (r4 != 0) goto L_0x004f
            java.lang.String r4 = "Infinity"
        L_0x0042:
            r0 = r40
            r0.append(r4)
            r4 = 9999(0x270f, float:1.4012E-41)
        L_0x0049:
            return r4
        L_0x004a:
            r4 = 0
            r5 = 0
            r39[r4] = r5
            goto L_0x0025
        L_0x004f:
            java.lang.String r4 = "NaN"
            goto L_0x0042
        L_0x0052:
            r4 = 0
            int r4 = (r34 > r4 ? 1 : (r34 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x0067
            r4 = 0
            r0 = r40
            r0.setLength(r4)
            r4 = 48
            r0 = r40
            r0.append(r4)
            r4 = 1
            goto L_0x0049
        L_0x0067:
            r0 = r34
            r2 = r27
            r3 = r28
            java.math.BigInteger r21 = m12245a(r0, r2, r3)
            int r4 = m12241H(r34)
            int r4 = r4 >>> 20
            r4 = r4 & 2047(0x7ff, float:2.868E-42)
            if (r4 == 0) goto L_0x015f
            int r5 = m12241H(r34)
            r6 = 1048575(0xfffff, float:1.469367E-39)
            r5 = r5 & r6
            r6 = 1072693248(0x3ff00000, float:1.875)
            r5 = r5 | r6
            r0 = r34
            double r6 = m12250c(r0, r5)
            int r5 = r4 + -1023
            r4 = 0
            r26 = r4
            r8 = r5
        L_0x0092:
            r4 = 4609434218613702656(0x3ff8000000000000, double:1.5)
            double r4 = r6 - r4
            r6 = 4598887322496222049(0x3fd287a7636f4361, double:0.289529654602168)
            double r4 = r4 * r6
            r6 = 4595512376519870643(0x3fc68a288b60c8b3, double:0.1760912590558)
            double r4 = r4 + r6
            double r6 = (double) r8
            r10 = 4599094494223104507(0x3fd34413509f79fb, double:0.301029995663981)
            double r6 = r6 * r10
            double r6 = r6 + r4
            int r4 = (int) r6
            r10 = 0
            int r5 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r5 >= 0) goto L_0x00b8
            double r10 = (double) r4
            int r5 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r5 == 0) goto L_0x00b8
            int r4 = r4 + -1
        L_0x00b8:
            r5 = 1
            if (r4 < 0) goto L_0x00ca
            r6 = 22
            if (r4 > r6) goto L_0x00ca
            double[] r5 = eOe
            r6 = r5[r4]
            int r5 = (r34 > r6 ? 1 : (r34 == r6 ? 0 : -1))
            if (r5 >= 0) goto L_0x00c9
            int r4 = r4 + -1
        L_0x00c9:
            r5 = 0
        L_0x00ca:
            r6 = 0
            r6 = r28[r6]
            int r6 = r6 - r8
            int r22 = r6 + -1
            if (r22 < 0) goto L_0x0199
            r20 = 0
        L_0x00d4:
            if (r4 < 0) goto L_0x01a2
            r19 = 0
            int r22 = r22 + r4
            r18 = r4
        L_0x00dc:
            if (r36 < 0) goto L_0x00e4
            r6 = 9
            r0 = r36
            if (r0 <= r6) goto L_0x072e
        L_0x00e4:
            r7 = 0
        L_0x00e5:
            r6 = 1
            r8 = 5
            if (r7 <= r8) goto L_0x0729
            int r7 = r7 + -4
            r6 = 0
            r8 = r6
            r25 = r7
        L_0x00ef:
            r6 = 1
            r7 = 0
            switch(r25) {
                case 0: goto L_0x01ab;
                case 1: goto L_0x01ab;
                case 2: goto L_0x01b8;
                case 3: goto L_0x01c7;
                case 4: goto L_0x01b9;
                case 5: goto L_0x01c8;
                default: goto L_0x00f4;
            }
        L_0x00f4:
            r23 = r6
            r12 = r7
            r11 = r7
            r24 = r38
        L_0x00fa:
            r10 = 0
            if (r11 < 0) goto L_0x02ad
            r6 = 14
            if (r11 > r6) goto L_0x02ad
            if (r8 == 0) goto L_0x02ad
            r14 = 0
            r13 = 2
            if (r4 <= 0) goto L_0x01e8
            double[] r6 = eOe
            r7 = r4 & 15
            r6 = r6[r7]
            int r9 = r4 >> 4
            r8 = r9 & 16
            if (r8 == 0) goto L_0x071e
            r9 = r9 & 15
            double[] r8 = eOf
            r13 = 4
            r16 = r8[r13]
            double r16 = r34 / r16
            r8 = 3
        L_0x011d:
            if (r9 != 0) goto L_0x01d6
            double r14 = r16 / r6
            r13 = r8
        L_0x0122:
            if (r5 == 0) goto L_0x0714
            r6 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r6 = (r14 > r6 ? 1 : (r14 == r6 ? 0 : -1))
            if (r6 >= 0) goto L_0x0714
            if (r11 <= 0) goto L_0x0714
            if (r12 > 0) goto L_0x020e
            r6 = 1
            r7 = r4
            r8 = r11
            r9 = r13
        L_0x0132:
            double r0 = (double) r9
            r16 = r0
            double r16 = r16 * r14
            r30 = 4619567317775286272(0x401c000000000000, double:7.0)
            double r16 = r16 + r30
            int r9 = m12241H(r16)
            r10 = 54525952(0x3400000, float:5.642373E-37)
            int r9 = r9 - r10
            r0 = r16
            double r30 = m12250c(r0, r9)
            if (r8 != 0) goto L_0x0710
            r16 = 4617315517961601024(0x4014000000000000, double:5.0)
            double r16 = r14 - r16
            int r6 = (r16 > r30 ? 1 : (r16 == r30 ? 0 : -1))
            if (r6 <= 0) goto L_0x0219
            r4 = 49
            r0 = r40
            r0.append(r4)
            int r4 = r7 + 1
            int r4 = r4 + 1
            goto L_0x0049
        L_0x015f:
            r4 = 0
            r4 = r28[r4]
            r5 = 0
            r5 = r27[r5]
            int r4 = r4 + r5
            int r5 = r4 + 1074
            r4 = 32
            if (r5 <= r4) goto L_0x0191
            int r4 = m12241H(r34)
            int r6 = 64 - r5
            int r4 = r4 << r6
            int r6 = m12242I(r34)
            int r7 = r5 + -32
            int r6 = r6 >>> r7
            r4 = r4 | r6
        L_0x017b:
            long r6 = (long) r4
            double r8 = (double) r6
            double r6 = (double) r6
            int r4 = m12241H(r6)
            r6 = 32505856(0x1f00000, float:8.8162076E-38)
            int r4 = r4 - r6
            double r6 = m12250c(r8, r4)
            int r5 = r5 + -1075
            r4 = 1
            r26 = r4
            r8 = r5
            goto L_0x0092
        L_0x0191:
            int r4 = m12242I(r34)
            int r6 = 32 - r5
            int r4 = r4 << r6
            goto L_0x017b
        L_0x0199:
            r0 = r22
            int r0 = -r0
            r20 = r0
            r22 = 0
            goto L_0x00d4
        L_0x01a2:
            int r20 = r20 - r4
            int r0 = -r4
            r19 = r0
            r18 = 0
            goto L_0x00dc
        L_0x01ab:
            r38 = -1
            r7 = 0
            r23 = r6
            r12 = r38
            r11 = r38
            r24 = r7
            goto L_0x00fa
        L_0x01b8:
            r6 = 0
        L_0x01b9:
            if (r38 > 0) goto L_0x01bd
            r38 = 1
        L_0x01bd:
            r23 = r6
            r12 = r38
            r11 = r38
            r24 = r38
            goto L_0x00fa
        L_0x01c7:
            r6 = 0
        L_0x01c8:
            int r7 = r38 + r4
            int r11 = r7 + 1
            int r12 = r11 + -1
            if (r11 > 0) goto L_0x0723
            r23 = r6
            r24 = r38
            goto L_0x00fa
        L_0x01d6:
            r13 = r9 & 1
            if (r13 == 0) goto L_0x01e2
            int r8 = r8 + 1
            double[] r13 = eOf
            r30 = r13[r14]
            double r6 = r6 * r30
        L_0x01e2:
            int r9 = r9 >> 1
            int r14 = r14 + 1
            goto L_0x011d
        L_0x01e8:
            int r6 = -r4
            if (r6 == 0) goto L_0x071a
            double[] r7 = eOe
            r8 = r6 & 15
            r8 = r7[r8]
            double r8 = r8 * r34
            int r7 = r6 >> 4
            r6 = r13
        L_0x01f6:
            if (r7 != 0) goto L_0x01fc
            r13 = r6
            r14 = r8
            goto L_0x0122
        L_0x01fc:
            r13 = r7 & 1
            if (r13 == 0) goto L_0x0208
            int r6 = r6 + 1
            double[] r13 = eOf
            r16 = r13[r14]
            double r8 = r8 * r16
        L_0x0208:
            int r7 = r7 >> 1
            int r13 = r14 + 1
            r14 = r13
            goto L_0x01f6
        L_0x020e:
            int r7 = r4 + -1
            r8 = 4621819117588971520(0x4024000000000000, double:10.0)
            double r14 = r14 * r8
            int r9 = r13 + 1
            r6 = r10
            r8 = r12
            goto L_0x0132
        L_0x0219:
            r0 = r30
            double r14 = -r0
            int r6 = (r16 > r14 ? 1 : (r16 == r14 ? 0 : -1))
            if (r6 >= 0) goto L_0x0230
            r4 = 0
            r0 = r40
            r0.setLength(r4)
            r4 = 48
            r0 = r40
            r0.append(r4)
            r4 = 1
            goto L_0x0049
        L_0x0230:
            r6 = 1
        L_0x0231:
            if (r6 != 0) goto L_0x070c
            r9 = 1
            if (r23 == 0) goto L_0x02ea
            r14 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            double[] r6 = eOe
            int r10 = r8 + -1
            r32 = r6[r10]
            double r14 = r14 / r32
            double r14 = r14 - r30
            r6 = 0
        L_0x0243:
            r0 = r16
            long r0 = (long) r0
            r30 = r0
            r0 = r30
            double r0 = (double) r0
            r32 = r0
            double r16 = r16 - r32
            r32 = 48
            long r30 = r30 + r32
            r0 = r30
            int r10 = (int) r0
            char r10 = (char) r10
            r0 = r40
            r0.append(r10)
            int r10 = (r16 > r14 ? 1 : (r16 == r14 ? 0 : -1))
            if (r10 >= 0) goto L_0x0264
            int r4 = r7 + 1
            goto L_0x0049
        L_0x0264:
            r30 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r30 = r30 - r16
            int r10 = (r30 > r14 ? 1 : (r30 == r14 ? 0 : -1))
            if (r10 >= 0) goto L_0x029e
        L_0x026c:
            int r4 = r40.length()
            int r4 = r4 + -1
            r0 = r40
            char r4 = r0.charAt(r4)
            int r5 = r40.length()
            int r5 = r5 + -1
            r0 = r40
            r0.setLength(r5)
            r5 = 57
            if (r4 == r5) goto L_0x0293
        L_0x0287:
            int r4 = r4 + 1
            char r4 = (char) r4
            r0 = r40
            r0.append(r4)
            int r4 = r7 + 1
            goto L_0x0049
        L_0x0293:
            int r4 = r40.length()
            if (r4 != 0) goto L_0x026c
            int r7 = r7 + 1
            r4 = 48
            goto L_0x0287
        L_0x029e:
            int r6 = r6 + 1
            if (r6 < r8) goto L_0x02e0
            r6 = r9
            r14 = r16
        L_0x02a5:
            if (r6 == 0) goto L_0x0703
            r6 = 0
            r0 = r40
            r0.setLength(r6)
        L_0x02ad:
            r6 = 0
            r6 = r27[r6]
            if (r6 < 0) goto L_0x03d4
            r6 = 14
            if (r4 > r6) goto L_0x03d4
            double[] r5 = eOe
            r6 = r5[r4]
            if (r24 >= 0) goto L_0x0369
            if (r11 > 0) goto L_0x0369
            if (r11 < 0) goto L_0x02d0
            r8 = 4617315517961601024(0x4014000000000000, double:5.0)
            double r8 = r8 * r6
            int r5 = (r34 > r8 ? 1 : (r34 == r8 ? 0 : -1))
            if (r5 < 0) goto L_0x02d0
            if (r37 != 0) goto L_0x035c
            r8 = 4617315517961601024(0x4014000000000000, double:5.0)
            double r6 = r6 * r8
            int r5 = (r34 > r6 ? 1 : (r34 == r6 ? 0 : -1))
            if (r5 != 0) goto L_0x035c
        L_0x02d0:
            r4 = 0
            r0 = r40
            r0.setLength(r4)
            r4 = 48
            r0 = r40
            r0.append(r4)
            r4 = 1
            goto L_0x0049
        L_0x02e0:
            r30 = 4621819117588971520(0x4024000000000000, double:10.0)
            double r14 = r14 * r30
            r30 = 4621819117588971520(0x4024000000000000, double:10.0)
            double r16 = r16 * r30
            goto L_0x0243
        L_0x02ea:
            double[] r6 = eOe
            int r10 = r8 + -1
            r14 = r6[r10]
            double r30 = r30 * r14
            r6 = 1
        L_0x02f3:
            r0 = r16
            long r0 = (long) r0
            r32 = r0
            r0 = r32
            double r14 = (double) r0
            double r14 = r16 - r14
            r16 = 48
            long r16 = r16 + r32
            r0 = r16
            int r10 = (int) r0
            char r10 = (char) r10
            r0 = r40
            r0.append(r10)
            if (r6 != r8) goto L_0x0355
            r16 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            double r16 = r16 + r30
            int r6 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r6 <= 0) goto L_0x0346
        L_0x0314:
            int r4 = r40.length()
            int r4 = r4 + -1
            r0 = r40
            char r4 = r0.charAt(r4)
            int r5 = r40.length()
            int r5 = r5 + -1
            r0 = r40
            r0.setLength(r5)
            r5 = 57
            if (r4 == r5) goto L_0x033b
        L_0x032f:
            int r4 = r4 + 1
            char r4 = (char) r4
            r0 = r40
            r0.append(r4)
            int r4 = r7 + 1
            goto L_0x0049
        L_0x033b:
            int r4 = r40.length()
            if (r4 != 0) goto L_0x0314
            int r7 = r7 + 1
            r4 = 48
            goto L_0x032f
        L_0x0346:
            r16 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            double r16 = r16 - r30
            int r6 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r6 >= 0) goto L_0x0709
            m12249b(r40)
            int r4 = r7 + 1
            goto L_0x0049
        L_0x0355:
            int r6 = r6 + 1
            r16 = 4621819117588971520(0x4024000000000000, double:10.0)
            double r16 = r16 * r14
            goto L_0x02f3
        L_0x035c:
            r5 = 49
            r0 = r40
            r0.append(r5)
            int r4 = r4 + 1
            int r4 = r4 + 1
            goto L_0x0049
        L_0x0369:
            r5 = 1
        L_0x036a:
            double r8 = r34 / r6
            long r8 = (long) r8
            double r12 = (double) r8
            double r12 = r12 * r6
            double r12 = r34 - r12
            r14 = 48
            long r14 = r14 + r8
            int r10 = (int) r14
            char r10 = (char) r10
            r0 = r40
            r0.append(r10)
            if (r5 != r11) goto L_0x03c7
            double r10 = r12 + r12
            int r5 = (r10 > r6 ? 1 : (r10 == r6 ? 0 : -1))
            if (r5 > 0) goto L_0x0392
            int r5 = (r10 > r6 ? 1 : (r10 == r6 ? 0 : -1))
            if (r5 != 0) goto L_0x03b7
            r6 = 1
            long r6 = r6 & r8
            r8 = 0
            int r5 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r5 != 0) goto L_0x0392
            if (r37 == 0) goto L_0x03b7
        L_0x0392:
            int r5 = r40.length()
            int r5 = r5 + -1
            r0 = r40
            char r5 = r0.charAt(r5)
            int r6 = r40.length()
            int r6 = r6 + -1
            r0 = r40
            r0.setLength(r6)
            r6 = 57
            if (r5 == r6) goto L_0x03bb
            r6 = r4
        L_0x03ae:
            int r4 = r5 + 1
            char r4 = (char) r4
            r0 = r40
            r0.append(r4)
            r4 = r6
        L_0x03b7:
            int r4 = r4 + 1
            goto L_0x0049
        L_0x03bb:
            int r5 = r40.length()
            if (r5 != 0) goto L_0x0392
            int r6 = r4 + 1
            r4 = 48
            r5 = r4
            goto L_0x03ae
        L_0x03c7:
            r8 = 4621819117588971520(0x4024000000000000, double:10.0)
            double r34 = r12 * r8
            r8 = 0
            int r8 = (r34 > r8 ? 1 : (r34 == r8 ? 0 : -1))
            if (r8 == 0) goto L_0x03b7
            int r5 = r5 + 1
            goto L_0x036a
        L_0x03d4:
            r14 = 0
            if (r23 == 0) goto L_0x06f7
            r6 = 2
            r0 = r25
            if (r0 >= r6) goto L_0x04e4
            if (r26 == 0) goto L_0x04dd
            r6 = 0
            r6 = r27[r6]
            int r6 = r6 + 1075
        L_0x03e3:
            r7 = r18
            r8 = r19
            r9 = r20
            r10 = r6
            r13 = r19
        L_0x03ec:
            int r16 = r20 + r10
            int r10 = r10 + r22
            r14 = 1
            java.math.BigInteger r14 = java.math.BigInteger.valueOf(r14)
            r18 = r7
            r17 = r13
        L_0x03fa:
            if (r9 <= 0) goto L_0x06ea
            if (r10 <= 0) goto L_0x06ea
            if (r9 >= r10) goto L_0x0500
            r6 = r9
        L_0x0401:
            int r13 = r16 - r6
            int r9 = r9 - r6
            int r10 = r10 - r6
            r15 = r9
        L_0x0406:
            if (r17 <= 0) goto L_0x06e5
            if (r23 == 0) goto L_0x0503
            if (r8 <= 0) goto L_0x06e0
            java.math.BigInteger r6 = m12246a((java.math.BigInteger) r14, (int) r8)
            r0 = r21
            java.math.BigInteger r7 = r6.multiply(r0)
        L_0x0416:
            int r8 = r17 - r8
            if (r8 == 0) goto L_0x041e
            java.math.BigInteger r7 = m12246a((java.math.BigInteger) r7, (int) r8)
        L_0x041e:
            r8 = 1
            java.math.BigInteger r14 = java.math.BigInteger.valueOf(r8)
            if (r18 <= 0) goto L_0x042c
            r0 = r18
            java.math.BigInteger r14 = m12246a((java.math.BigInteger) r14, (int) r0)
        L_0x042c:
            r8 = 0
            r9 = 2
            r0 = r25
            if (r0 >= r9) goto L_0x06dd
            int r9 = m12242I(r34)
            if (r9 != 0) goto L_0x06dd
            int r9 = m12241H(r34)
            r16 = 1048575(0xfffff, float:1.469367E-39)
            r9 = r9 & r16
            if (r9 != 0) goto L_0x06dd
            int r9 = m12241H(r34)
            r16 = 2145386496(0x7fe00000, float:NaN)
            r9 = r9 & r16
            if (r9 == 0) goto L_0x06dd
            int r13 = r13 + 1
            int r9 = r10 + 1
            r8 = 1
        L_0x0452:
            byte[] r19 = r14.toByteArray()
            r17 = 0
            r10 = 0
            r16 = r10
        L_0x045b:
            r10 = 4
            r0 = r16
            if (r0 < r10) goto L_0x050e
            if (r18 == 0) goto L_0x052b
            int r10 = m12254pn(r17)
            int r10 = 32 - r10
        L_0x0468:
            int r10 = r10 + r9
            r10 = r10 & 31
            if (r10 == 0) goto L_0x046f
            int r10 = 32 - r10
        L_0x046f:
            r16 = 4
            r0 = r16
            if (r10 <= r0) goto L_0x052e
            int r10 = r10 + -4
            int r16 = r13 + r10
            int r13 = r15 + r10
            int r9 = r9 + r10
            r10 = r9
            r15 = r13
        L_0x047e:
            if (r16 <= 0) goto L_0x06d5
            r0 = r16
            java.math.BigInteger r9 = r7.shiftLeft(r0)
        L_0x0486:
            if (r10 <= 0) goto L_0x06d2
            java.math.BigInteger r7 = r14.shiftLeft(r10)
            r13 = r7
        L_0x048d:
            if (r5 == 0) goto L_0x06ce
            int r5 = r9.compareTo(r13)
            if (r5 >= 0) goto L_0x06ce
            int r4 = r4 + -1
            r10 = 10
            java.math.BigInteger r5 = java.math.BigInteger.valueOf(r10)
            java.math.BigInteger r9 = r9.multiply(r5)
            if (r23 == 0) goto L_0x04ad
            r10 = 10
            java.math.BigInteger r5 = java.math.BigInteger.valueOf(r10)
            java.math.BigInteger r6 = r6.multiply(r5)
        L_0x04ad:
            r5 = r6
            r7 = r4
            r11 = r12
        L_0x04b0:
            if (r11 > 0) goto L_0x054c
            r4 = 2
            r0 = r25
            if (r0 <= r4) goto L_0x054c
            if (r11 < 0) goto L_0x04cd
            r4 = 5
            java.math.BigInteger r4 = java.math.BigInteger.valueOf(r4)
            java.math.BigInteger r4 = r13.multiply(r4)
            int r4 = r9.compareTo(r4)
            if (r4 < 0) goto L_0x04cd
            if (r4 != 0) goto L_0x053f
            if (r37 != 0) goto L_0x053f
        L_0x04cd:
            r4 = 0
            r0 = r40
            r0.setLength(r4)
            r4 = 48
            r0 = r40
            r0.append(r4)
            r4 = 1
            goto L_0x0049
        L_0x04dd:
            r6 = 0
            r6 = r28[r6]
            int r6 = 54 - r6
            goto L_0x03e3
        L_0x04e4:
            int r6 = r11 + -1
            r0 = r19
            if (r0 < r6) goto L_0x04f8
            int r8 = r19 - r6
            r6 = r18
        L_0x04ee:
            if (r11 >= 0) goto L_0x06ef
            int r9 = r20 - r11
            r10 = 0
            r7 = r6
            r13 = r19
            goto L_0x03ec
        L_0x04f8:
            int r7 = r6 - r19
            int r6 = r18 + r7
            int r19 = r19 + r7
            r8 = 0
            goto L_0x04ee
        L_0x0500:
            r6 = r10
            goto L_0x0401
        L_0x0503:
            r0 = r21
            r1 = r17
            java.math.BigInteger r7 = m12246a((java.math.BigInteger) r0, (int) r1)
            r6 = r14
            goto L_0x041e
        L_0x050e:
            int r10 = r17 << 8
            r0 = r19
            int r0 = r0.length
            r17 = r0
            r0 = r16
            r1 = r17
            if (r0 >= r1) goto L_0x0525
            byte r17 = r19[r16]
            r0 = r17
            r0 = r0 & 255(0xff, float:3.57E-43)
            r17 = r0
            r10 = r10 | r17
        L_0x0525:
            int r16 = r16 + 1
            r17 = r10
            goto L_0x045b
        L_0x052b:
            r10 = 1
            goto L_0x0468
        L_0x052e:
            r16 = 4
            r0 = r16
            if (r10 >= r0) goto L_0x06d8
            int r10 = r10 + 28
            int r16 = r13 + r10
            int r13 = r15 + r10
            int r9 = r9 + r10
            r10 = r9
            r15 = r13
            goto L_0x047e
        L_0x053f:
            r4 = 49
            r0 = r40
            r0.append(r4)
            int r4 = r7 + 1
            int r4 = r4 + 1
            goto L_0x0049
        L_0x054c:
            if (r23 == 0) goto L_0x0699
            if (r15 <= 0) goto L_0x0554
            java.math.BigInteger r5 = r5.shiftLeft(r15)
        L_0x0554:
            if (r8 == 0) goto L_0x06cb
            r4 = 1
            java.math.BigInteger r4 = r5.shiftLeft(r4)
        L_0x055b:
            r6 = 1
            r8 = r4
            r10 = r6
        L_0x055e:
            java.math.BigInteger[] r4 = r9.divideAndRemainder(r13)
            r6 = 1
            r6 = r4[r6]
            r9 = 0
            r4 = r4[r9]
            int r4 = r4.intValue()
            int r4 = r4 + 48
            char r4 = (char) r4
            int r12 = r6.compareTo(r5)
            java.math.BigInteger r9 = r13.subtract(r8)
            int r14 = r9.signum()
            if (r14 > 0) goto L_0x05a8
            r9 = 1
        L_0x057e:
            if (r9 != 0) goto L_0x05bb
            if (r25 != 0) goto L_0x05bb
            int r14 = m12242I(r34)
            r14 = r14 & 1
            if (r14 != 0) goto L_0x05bb
            r5 = 57
            if (r4 != r5) goto L_0x05ad
            r4 = 57
            r0 = r40
            r0.append(r4)
            boolean r4 = m12248a(r40)
            if (r4 == 0) goto L_0x06c8
            int r4 = r7 + 1
            r5 = 49
            r0 = r40
            r0.append(r5)
        L_0x05a4:
            int r4 = r4 + 1
            goto L_0x0049
        L_0x05a8:
            int r9 = r6.compareTo(r9)
            goto L_0x057e
        L_0x05ad:
            if (r12 <= 0) goto L_0x05b2
            int r4 = r4 + 1
            char r4 = (char) r4
        L_0x05b2:
            r0 = r40
            r0.append(r4)
            int r4 = r7 + 1
            goto L_0x0049
        L_0x05bb:
            if (r12 < 0) goto L_0x05c9
            if (r12 != 0) goto L_0x060a
            if (r25 != 0) goto L_0x060a
            int r12 = m12242I(r34)
            r12 = r12 & 1
            if (r12 != 0) goto L_0x060a
        L_0x05c9:
            if (r9 <= 0) goto L_0x0601
            r5 = 1
            java.math.BigInteger r5 = r6.shiftLeft(r5)
            int r5 = r5.compareTo(r13)
            if (r5 > 0) goto L_0x05df
            if (r5 != 0) goto L_0x0601
            r5 = r4 & 1
            r6 = 1
            if (r5 == r6) goto L_0x05df
            if (r37 == 0) goto L_0x0601
        L_0x05df:
            int r5 = r4 + 1
            char r5 = (char) r5
            r6 = 57
            if (r4 != r6) goto L_0x0600
            r4 = 57
            r0 = r40
            r0.append(r4)
            boolean r4 = m12248a(r40)
            if (r4 == 0) goto L_0x05fc
            int r7 = r7 + 1
            r4 = 49
            r0 = r40
            r0.append(r4)
        L_0x05fc:
            int r4 = r7 + 1
            goto L_0x0049
        L_0x0600:
            r4 = r5
        L_0x0601:
            r0 = r40
            r0.append(r4)
            int r4 = r7 + 1
            goto L_0x0049
        L_0x060a:
            if (r9 <= 0) goto L_0x0636
            r5 = 57
            if (r4 != r5) goto L_0x062a
            r4 = 57
            r0 = r40
            r0.append(r4)
            boolean r4 = m12248a(r40)
            if (r4 == 0) goto L_0x0626
            int r7 = r7 + 1
            r4 = 49
            r0 = r40
            r0.append(r4)
        L_0x0626:
            int r4 = r7 + 1
            goto L_0x0049
        L_0x062a:
            int r4 = r4 + 1
            char r4 = (char) r4
            r0 = r40
            r0.append(r4)
            int r4 = r7 + 1
            goto L_0x0049
        L_0x0636:
            r0 = r40
            r0.append(r4)
            if (r10 != r11) goto L_0x0665
            r5 = r6
        L_0x063e:
            r6 = 1
            java.math.BigInteger r5 = r5.shiftLeft(r6)
            int r5 = r5.compareTo(r13)
            if (r5 > 0) goto L_0x0652
            if (r5 != 0) goto L_0x06c1
            r4 = r4 & 1
            r5 = 1
            if (r4 == r5) goto L_0x0652
            if (r37 == 0) goto L_0x06c1
        L_0x0652:
            boolean r4 = m12248a(r40)
            if (r4 == 0) goto L_0x06c4
            int r4 = r7 + 1
            r5 = 49
            r0 = r40
            r0.append(r5)
            int r4 = r4 + 1
            goto L_0x0049
        L_0x0665:
            r14 = 10
            java.math.BigInteger r4 = java.math.BigInteger.valueOf(r14)
            java.math.BigInteger r9 = r6.multiply(r4)
            if (r5 != r8) goto L_0x0683
            r4 = 10
            java.math.BigInteger r4 = java.math.BigInteger.valueOf(r4)
            java.math.BigInteger r5 = r8.multiply(r4)
            r4 = r5
            r6 = r5
        L_0x067d:
            int r10 = r10 + 1
            r8 = r4
            r5 = r6
            goto L_0x055e
        L_0x0683:
            r14 = 10
            java.math.BigInteger r4 = java.math.BigInteger.valueOf(r14)
            java.math.BigInteger r5 = r5.multiply(r4)
            r14 = 10
            java.math.BigInteger r4 = java.math.BigInteger.valueOf(r14)
            java.math.BigInteger r4 = r8.multiply(r4)
            r6 = r5
            goto L_0x067d
        L_0x0699:
            r4 = 1
        L_0x069a:
            java.math.BigInteger[] r6 = r9.divideAndRemainder(r13)
            r5 = 1
            r5 = r6[r5]
            r8 = 0
            r6 = r6[r8]
            int r6 = r6.intValue()
            int r6 = r6 + 48
            char r6 = (char) r6
            r0 = r40
            r0.append(r6)
            if (r4 < r11) goto L_0x06b4
            r4 = r6
            goto L_0x063e
        L_0x06b4:
            r8 = 10
            java.math.BigInteger r6 = java.math.BigInteger.valueOf(r8)
            java.math.BigInteger r9 = r5.multiply(r6)
            int r4 = r4 + 1
            goto L_0x069a
        L_0x06c1:
            m12249b(r40)
        L_0x06c4:
            int r4 = r7 + 1
            goto L_0x0049
        L_0x06c8:
            r4 = r7
            goto L_0x05a4
        L_0x06cb:
            r4 = r5
            goto L_0x055b
        L_0x06ce:
            r5 = r6
            r7 = r4
            goto L_0x04b0
        L_0x06d2:
            r13 = r14
            goto L_0x048d
        L_0x06d5:
            r9 = r7
            goto L_0x0486
        L_0x06d8:
            r10 = r9
            r16 = r13
            goto L_0x047e
        L_0x06dd:
            r9 = r10
            goto L_0x0452
        L_0x06e0:
            r6 = r14
            r7 = r21
            goto L_0x0416
        L_0x06e5:
            r6 = r14
            r7 = r21
            goto L_0x041e
        L_0x06ea:
            r15 = r9
            r13 = r16
            goto L_0x0406
        L_0x06ef:
            r7 = r6
            r9 = r20
            r10 = r11
            r13 = r19
            goto L_0x03ec
        L_0x06f7:
            r10 = r22
            r8 = r19
            r9 = r20
            r17 = r19
            r16 = r20
            goto L_0x03fa
        L_0x0703:
            r4 = r7
            r11 = r8
            r34 = r14
            goto L_0x02ad
        L_0x0709:
            r6 = r9
            goto L_0x02a5
        L_0x070c:
            r14 = r16
            goto L_0x02a5
        L_0x0710:
            r16 = r14
            goto L_0x0231
        L_0x0714:
            r6 = r10
            r7 = r4
            r8 = r11
            r9 = r13
            goto L_0x0132
        L_0x071a:
            r14 = r34
            goto L_0x0122
        L_0x071e:
            r8 = r13
            r16 = r34
            goto L_0x011d
        L_0x0723:
            r23 = r6
            r24 = r38
            goto L_0x00fa
        L_0x0729:
            r8 = r6
            r25 = r7
            goto L_0x00ef
        L_0x072e:
            r7 = r36
            goto L_0x00e5
        */
        throw new UnsupportedOperationException("Method not decompiled: DToA.m12243a(double, int, boolean, int, boolean[], java.lang.StringBuffer):int");
    }

    /* renamed from: b */
    private static void m12249b(StringBuffer stringBuffer) {
        int i;
        int length = stringBuffer.length();
        while (true) {
            i = length - 1;
            if (length <= 0 || stringBuffer.charAt(i) != '0') {
                stringBuffer.setLength(i + 1);
            } else {
                length = i;
            }
        }
        stringBuffer.setLength(i + 1);
    }

    /* renamed from: a */
    static void JS_dtostr(StringBuffer stringBuffer, int i, int i2, double d) {
        int i3;
        boolean z;
        boolean[] zArr = new boolean[1];
        if (i == 2 && (d >= 1.0E21d || d <= -1.0E21d)) {
            i = 0;
        }
        int a = m12243a(d, eOg[i], i >= 2, i2, zArr, stringBuffer);
        int length = stringBuffer.length();
        if (a != 9999) {
            switch (i) {
                case 0:
                    if (a >= -5 && a <= 21) {
                        i3 = a;
                        z = false;
                        break;
                    } else {
                        i3 = 0;
                        z = true;
                        break;
                    }
                    break;
                case 1:
                    i2 = 0;
                    break;
                case 2:
                    if (i2 < 0) {
                        i3 = a;
                        z = false;
                        break;
                    } else {
                        i3 = i2 + a;
                        z = false;
                        break;
                    }
                case 3:
                    break;
                case 4:
                    if (a >= -5 && a <= i2) {
                        i3 = i2;
                        z = false;
                        break;
                    } else {
                        i3 = i2;
                        z = true;
                        break;
                    }
                default:
                    i3 = 0;
                    z = false;
                    break;
            }
            i3 = i2;
            z = true;
            if (length < i3) {
                do {
                    stringBuffer.append('0');
                } while (stringBuffer.length() != i3);
            } else {
                i3 = length;
            }
            if (z) {
                if (i3 != 1) {
                    stringBuffer.insert(1, '.');
                }
                stringBuffer.append('e');
                if (a - 1 >= 0) {
                    stringBuffer.append('+');
                }
                stringBuffer.append(a - 1);
            } else if (a != i3) {
                if (a > 0) {
                    stringBuffer.insert(a, '.');
                } else {
                    for (int i4 = 0; i4 < 1 - a; i4++) {
                        stringBuffer.insert(0, '0');
                    }
                    stringBuffer.insert(1, '.');
                }
            }
        }
        if (!zArr[0]) {
            return;
        }
        if (m12241H(d) != Integer.MIN_VALUE || m12242I(d) != 0) {
            if ((m12241H(d) & eNS) != eNS || (m12242I(d) == 0 && (m12241H(d) & 1048575) == 0)) {
                stringBuffer.insert(0, '-');
            }
        }
    }
}
