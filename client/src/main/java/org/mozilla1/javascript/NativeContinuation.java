package org.mozilla1.javascript;

/* renamed from: a.aHs  reason: case insensitive filesystem */
/* compiled from: a */
public final class NativeContinuation extends IdScriptableObject implements Function {
    static final long serialVersionUID = 1794167133757605367L;
    private static final Object cHe = new Integer(4);
    /* renamed from: ns */
    private static final int f3007ns = 1;
    /* renamed from: nt */
    private static final int f3008nt = 1;
    private Object hXe;

    public static void init(Context lhVar, Scriptable avf, boolean z) {
        new NativeContinuation().mo15695a(1, avf, z);
    }

    /* renamed from: d */
    public static boolean m15303d(IdFunctionObject yy) {
        if (!yy.hasTag(cHe) || yy.methodId() != 1) {
            return false;
        }
        return true;
    }

    public Object getImplementation() {
        return this.hXe;
    }

    public void initImplementation(Object obj) {
        this.hXe = obj;
    }

    public String getClassName() {
        return "Continuation";
    }

    public Scriptable construct(Context lhVar, Scriptable avf, Object[] objArr) {
        throw Context.m35245gF("Direct call is not supported");
    }

    public Object call(Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        return Interpreter.m39132a(this, lhVar, avf, objArr);
    }

    /* access modifiers changed from: protected */
    public void initPrototypeId(int i) {
        switch (i) {
            case 1:
                initPrototypeMethod(cHe, i, "constructor", 0);
                return;
            default:
                throw new IllegalArgumentException(String.valueOf(i));
        }
    }

    public Object execIdCall(IdFunctionObject yy, Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        if (!yy.hasTag(cHe)) {
            return super.execIdCall(yy, lhVar, avf, avf2, objArr);
        }
        int methodId = yy.methodId();
        switch (methodId) {
            case 1:
                throw Context.m35245gF("Direct call is not supported");
            default:
                throw new IllegalArgumentException(String.valueOf(methodId));
        }
    }

    /* access modifiers changed from: protected */
    public int findPrototypeId(String str) {
        int i;
        String str2 = null;
        if (str.length() == 11) {
            str2 = "constructor";
            i = 1;
        } else {
            i = 0;
        }
        if (str2 == null || str2 == str || str2.equals(str)) {
            return i;
        }
        return 0;
    }
}
