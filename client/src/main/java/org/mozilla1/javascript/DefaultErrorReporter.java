package org.mozilla1.javascript;

/* renamed from: a.tk */
/* compiled from: a */
class DefaultErrorReporter implements org.mozilla1.javascript.ErrorReporter {
    static final DefaultErrorReporter bkW = new DefaultErrorReporter();
    private boolean bkX;
    private org.mozilla1.javascript.ErrorReporter bkY;

    private DefaultErrorReporter() {
    }

    /* renamed from: a */
    static org.mozilla1.javascript.ErrorReporter m39748a(ErrorReporter ale) {
        DefaultErrorReporter tkVar = new DefaultErrorReporter();
        tkVar.bkX = true;
        tkVar.bkY = ale;
        return tkVar;
    }

    public void warning(String str, String str2, int i, String str3, int i2) {
        if (this.bkY != null) {
            this.bkY.warning(str, str2, i, str3, i2);
        }
    }

    public void error(String str, String str2, int i, String str3, int i2) {
        String str4;
        if (this.bkX) {
            String str5 = "SyntaxError";
            if (str.startsWith("TypeError: ")) {
                str5 = "TypeError";
                str4 = str.substring("TypeError: ".length());
            } else {
                str4 = str;
            }
            throw ScriptRuntime.m7474a(str5, str4, str2, i, str3, i2);
        } else if (this.bkY != null) {
            this.bkY.error(str, str2, i, str3, i2);
        } else {
            throw runtimeError(str, str2, i, str3, i2);
        }
    }

    public org.mozilla1.javascript.EvaluatorException runtimeError(String str, String str2, int i, String str3, int i2) {
        if (this.bkY != null) {
            return this.bkY.runtimeError(str, str2, i, str3, i2);
        }
        return new EvaluatorException(str, str2, i, str3, i2);
    }
}
