package org.mozilla1.javascript;


/* renamed from: a.PN */
/* compiled from: a */
public class WrappedException extends EvaluatorException {
    static final long serialVersionUID = -1551979216966520648L;
    private Throwable exception;

    public WrappedException(Throwable th) {
        super("Wrapped " + th.toString());
        this.exception = th;
        Kit.initCause(this, th);
        int[] iArr = new int[1];
        String c = Context.m35242c(iArr);
        int i = iArr[0];
        if (c != null) {
            initSourceName(c);
        }
        if (i != 0) {
            initLineNumber(i);
        }
    }

    public Throwable getWrappedException() {
        return this.exception;
    }

    public Object unwrap() {
        return getWrappedException();
    }
}
