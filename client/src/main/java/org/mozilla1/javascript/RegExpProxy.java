package org.mozilla1.javascript;


/* renamed from: a.aqH  reason: case insensitive filesystem */
/* compiled from: a */
public interface RegExpProxy {
    public static final int RA_MATCH = 1;
    public static final int RA_REPLACE = 2;
    public static final int RA_SEARCH = 3;

    /* renamed from: a */
    int mo10821a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, String str, String str2, org.mozilla1.javascript.Scriptable avf2, int[] iArr, int[] iArr2, boolean[] zArr, String[][] strArr);

    /* renamed from: a */
    Object mo10822a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr, int i);

    /* renamed from: b */
    Object mo10823b(org.mozilla1.javascript.Context lhVar, String str, String str2);

    /* renamed from: c */
    org.mozilla1.javascript.Scriptable mo10824c(Context lhVar, org.mozilla1.javascript.Scriptable avf, Object obj);

    /* renamed from: m */
    boolean mo10825m(Scriptable avf);
}
