package org.mozilla1.javascript;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.afb  reason: case insensitive filesystem */
/* compiled from: a */
public class NodeTransformer {
    private org.mozilla1.javascript.ObjArray fsD;
    private org.mozilla1.javascript.ObjArray fsE;
    private boolean fsF;

    /* renamed from: a */
    private static org.mozilla1.javascript.Node m21560a(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, org.mozilla1.javascript.Node qlVar3, org.mozilla1.javascript.Node qlVar4) {
        if (qlVar2 == null) {
            if (qlVar3 != qlVar.cFE()) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            qlVar.mo21460F(qlVar4);
        } else {
            if (qlVar3 != qlVar2.cFG()) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            qlVar.mo21485p(qlVar4, qlVar2);
        }
        return qlVar4;
    }

    /* renamed from: b */
    private static org.mozilla1.javascript.Node m21562b(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, org.mozilla1.javascript.Node qlVar3, org.mozilla1.javascript.Node qlVar4) {
        if (qlVar2 == null) {
            if (qlVar3 != qlVar.cFE()) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            qlVar.mo21488q(qlVar3, qlVar4);
        } else if (qlVar2.gZm == qlVar3) {
            qlVar.mo21489r(qlVar2, qlVar4);
        } else {
            qlVar.mo21488q(qlVar3, qlVar4);
        }
        return qlVar4;
    }

    /* renamed from: a */
    public final void mo13266a(org.mozilla1.javascript.ScriptOrFnNode aak) {
        m21563k(aak);
        for (int i = 0; i != aak.getFunctionCount(); i++) {
            mo13266a(aak.mo7704wM(i));
        }
    }

    /* renamed from: k */
    private void m21563k(org.mozilla1.javascript.ScriptOrFnNode aak) {
        boolean z;
        this.fsD = new org.mozilla1.javascript.ObjArray();
        this.fsE = new ObjArray();
        this.fsF = false;
        boolean z2 = aak.getType() != 108 || ((FunctionNode) aak).requiresActivation();
        if (z2) {
            z = false;
        } else {
            z = true;
        }
        aak.mo7698hC(z);
        m21561a(aak, (org.mozilla1.javascript.Node) aak, (org.mozilla1.javascript.Node.Scope) aak, z2);
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x021d A[FALL_THROUGH] */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x027c  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x028b  */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x02f1  */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x0361  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0185  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x01ed  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x01f2  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x01f7  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m21561a(org.mozilla1.javascript.ScriptOrFnNode r10, org.mozilla1.javascript.Node r11, org.mozilla1.javascript.Node.Scope r12, boolean r13) {
        /*
            r9 = this;
            r0 = 0
        L_0x0001:
            r5 = 0
            if (r0 != 0) goto L_0x000b
            a.ql r1 = r11.cFE()
        L_0x0008:
            if (r1 != 0) goto L_0x0011
            return
        L_0x000b:
            a.ql r1 = r0.cFG()
            r5 = r0
            goto L_0x0008
        L_0x0011:
            int r2 = r1.getType()
            if (r13 == 0) goto L_0x036d
            r0 = 128(0x80, float:1.794E-43)
            if (r2 == r0) goto L_0x0023
            r0 = 131(0x83, float:1.84E-43)
            if (r2 == r0) goto L_0x0023
            r0 = 156(0x9c, float:2.19E-43)
            if (r2 != r0) goto L_0x036d
        L_0x0023:
            boolean r0 = r1 instanceof org.mozilla.javascript.C3366ql.Scope
            if (r0 == 0) goto L_0x036d
            r0 = r1
            a.ql$c r0 = (org.mozilla.javascript.C3366ql.Scope) r0
            java.util.LinkedHashMap<java.lang.String, a.ql$d> r3 = r0.fFc
            if (r3 == 0) goto L_0x036d
            a.ql r3 = new a.ql
            r4 = 156(0x9c, float:2.19E-43)
            if (r2 != r4) goto L_0x0072
            r2 = 157(0x9d, float:2.2E-43)
        L_0x0036:
            r3.<init>(r2)
            a.ql r4 = new a.ql
            r2 = 152(0x98, float:2.13E-43)
            r4.<init>(r2)
            r3.mo21461G(r4)
            java.util.LinkedHashMap<java.lang.String, a.ql$d> r2 = r0.fFc
            java.util.Set r2 = r2.keySet()
            java.util.Iterator r6 = r2.iterator()
        L_0x004d:
            boolean r2 = r6.hasNext()
            if (r2 != 0) goto L_0x0075
            r2 = 0
            r0.fFc = r2
            a.ql r2 = m21562b(r11, r5, r1, r3)
            int r0 = r2.getType()
            r3.mo21461G(r1)
            r6 = r0
            r1 = r2
        L_0x0063:
            switch(r6) {
                case 3: goto L_0x00c5;
                case 4: goto L_0x00e7;
                case 7: goto L_0x028b;
                case 8: goto L_0x02f1;
                case 30: goto L_0x01f2;
                case 31: goto L_0x02f1;
                case 32: goto L_0x028b;
                case 38: goto L_0x01ed;
                case 39: goto L_0x02f1;
                case 72: goto L_0x00e0;
                case 80: goto L_0x00ae;
                case 113: goto L_0x0085;
                case 119: goto L_0x0185;
                case 120: goto L_0x0185;
                case 121: goto L_0x021d;
                case 122: goto L_0x0095;
                case 129: goto L_0x0085;
                case 130: goto L_0x00c5;
                case 131: goto L_0x0085;
                case 136: goto L_0x027c;
                case 152: goto L_0x01f7;
                case 153: goto L_0x021d;
                case 154: goto L_0x02f1;
                case 157: goto L_0x01f7;
                default: goto L_0x0066;
            }
        L_0x0066:
            boolean r0 = r1 instanceof org.mozilla.javascript.C3366ql.Scope
            if (r0 == 0) goto L_0x0361
            r0 = r1
            a.ql$c r0 = (org.mozilla.javascript.C3366ql.Scope) r0
        L_0x006d:
            r9.m21561a((p001a.C5206aAk) r10, (org.mozilla.javascript.C3366ql) r1, (org.mozilla.javascript.C3366ql.Scope) r0, (boolean) r13)
            r0 = r1
            goto L_0x0001
        L_0x0072:
            r2 = 152(0x98, float:2.13E-43)
            goto L_0x0036
        L_0x0075:
            java.lang.Object r2 = r6.next()
            java.lang.String r2 = (java.lang.String) r2
            r7 = 39
            a.ql r2 = org.mozilla.javascript.C3366ql.m37828e(r7, r2)
            r4.mo21461G(r2)
            goto L_0x004d
        L_0x0085:
            a.aCx r0 = r9.fsD
            r0.push(r1)
            a.aCx r2 = r9.fsE
            r0 = r1
            a.ql$a r0 = (org.mozilla.javascript.C3366ql.C3367a) r0
            a.ql r0 = r0.aUM
            r2.push(r0)
            goto L_0x0066
        L_0x0095:
            a.aCx r0 = r9.fsD
            r0.push(r1)
            a.ql r0 = r1.cFG()
            int r2 = r0.getType()
            r3 = 3
            if (r2 == r3) goto L_0x00a8
            p001a.C1520WN.codeBug()
        L_0x00a8:
            a.aCx r2 = r9.fsE
            r2.push(r0)
            goto L_0x0066
        L_0x00ae:
            r0 = r1
            a.ql$a r0 = (org.mozilla.javascript.C3366ql.C3367a) r0
            a.ql r0 = r0.mo21497WP()
            if (r0 == 0) goto L_0x0066
            r2 = 1
            r9.fsF = r2
            a.aCx r2 = r9.fsD
            r2.push(r1)
            a.aCx r2 = r9.fsE
            r2.push(r0)
            goto L_0x0066
        L_0x00c5:
            a.aCx r0 = r9.fsE
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0066
            a.aCx r0 = r9.fsE
            java.lang.Object r0 = r0.peek()
            if (r0 != r1) goto L_0x0066
            a.aCx r0 = r9.fsE
            r0.pop()
            a.aCx r0 = r9.fsD
            r0.pop()
            goto L_0x0066
        L_0x00e0:
            r0 = r10
            a.YQ r0 = (p001a.C1658YQ) r0
            r0.mo7187h(r1)
            goto L_0x0066
        L_0x00e7:
            int r0 = r10.getType()
            r2 = 108(0x6c, float:1.51E-43)
            if (r0 != r2) goto L_0x0125
            r0 = r10
            a.YQ r0 = (p001a.C1658YQ) r0
            boolean r0 = r0.bHQ()
            if (r0 == 0) goto L_0x0125
            r0 = 1
            r6 = r0
        L_0x00fa:
            if (r6 == 0) goto L_0x0102
            r0 = 20
            r2 = 1
            r1.putIntProp(r0, r2)
        L_0x0102:
            boolean r0 = r9.fsF
            if (r0 == 0) goto L_0x0066
            r2 = 0
            a.aCx r0 = r9.fsD
            int r0 = r0.size()
            int r0 = r0 + -1
            r4 = r0
        L_0x0110:
            if (r4 >= 0) goto L_0x0128
            if (r2 == 0) goto L_0x0066
            a.ql r3 = r1.cFE()
            a.ql r0 = m21562b(r11, r5, r1, r2)
            if (r3 == 0) goto L_0x0120
            if (r6 == 0) goto L_0x016c
        L_0x0120:
            r2.mo21461G(r1)
            goto L_0x0001
        L_0x0125:
            r0 = 0
            r6 = r0
            goto L_0x00fa
        L_0x0128:
            a.aCx r0 = r9.fsD
            java.lang.Object r0 = r0.get(r4)
            a.ql r0 = (org.mozilla.javascript.C3366ql) r0
            int r3 = r0.getType()
            r7 = 80
            if (r3 == r7) goto L_0x013c
            r7 = 122(0x7a, float:1.71E-43)
            if (r3 != r7) goto L_0x0160
        L_0x013c:
            r7 = 80
            if (r3 != r7) goto L_0x0164
            a.ql$a r3 = new a.ql$a
            r7 = 134(0x86, float:1.88E-43)
            r3.<init>(r7)
            a.ql$a r0 = (org.mozilla.javascript.C3366ql.C3367a) r0
            a.ql r0 = r0.mo21497WP()
            r3.aUM = r0
        L_0x014f:
            if (r2 != 0) goto L_0x036a
            a.ql r0 = new a.ql
            r2 = 128(0x80, float:1.794E-43)
            int r7 = r1.getLineno()
            r0.<init>((int) r2, (int) r7)
        L_0x015c:
            r0.mo21461G(r3)
            r2 = r0
        L_0x0160:
            int r0 = r4 + -1
            r4 = r0
            goto L_0x0110
        L_0x0164:
            a.ql r0 = new a.ql
            r3 = 3
            r0.<init>(r3)
            r3 = r0
            goto L_0x014f
        L_0x016c:
            a.ql r1 = new a.ql
            r4 = 133(0x85, float:1.86E-43)
            r1.<init>((int) r4, (org.mozilla.javascript.C3366ql) r3)
            r2.mo21460F(r1)
            a.ql r3 = new a.ql
            r4 = 64
            r3.<init>(r4)
            r2.mo21461G(r3)
            r9.m21561a((p001a.C5206aAk) r10, (org.mozilla.javascript.C3366ql) r1, (org.mozilla.javascript.C3366ql.Scope) r12, (boolean) r13)
            goto L_0x0001
        L_0x0185:
            r0 = r1
            a.ql$a r0 = (org.mozilla.javascript.C3366ql.C3367a) r0
            a.ql$a r7 = r0.mo21495WN()
            if (r7 != 0) goto L_0x0191
            p001a.C1520WN.codeBug()
        L_0x0191:
            a.aCx r2 = r9.fsD
            int r2 = r2.size()
            r4 = r5
        L_0x0198:
            if (r2 != 0) goto L_0x019f
            java.lang.RuntimeException r0 = p001a.C1520WN.codeBug()
            throw r0
        L_0x019f:
            int r3 = r2 + -1
            a.aCx r2 = r9.fsD
            java.lang.Object r2 = r2.get(r3)
            a.ql r2 = (org.mozilla.javascript.C3366ql) r2
            if (r2 != r7) goto L_0x01b9
            r2 = 119(0x77, float:1.67E-43)
            if (r6 != r2) goto L_0x01e6
            a.ql r2 = r7.aUM
            r0.aUM = r2
        L_0x01b3:
            r2 = 5
            r0.setType(r2)
            goto L_0x0066
        L_0x01b9:
            int r5 = r2.getType()
            r8 = 122(0x7a, float:1.71E-43)
            if (r5 != r8) goto L_0x01cd
            a.ql r2 = new a.ql
            r5 = 3
            r2.<init>(r5)
            a.ql r4 = m21560a((org.mozilla.javascript.C3366ql) r11, (org.mozilla.javascript.C3366ql) r4, (org.mozilla.javascript.C3366ql) r1, (org.mozilla.javascript.C3366ql) r2)
            r2 = r3
            goto L_0x0198
        L_0x01cd:
            r8 = 80
            if (r5 != r8) goto L_0x0367
            a.ql$a r2 = (org.mozilla.javascript.C3366ql.C3367a) r2
            a.ql$a r5 = new a.ql$a
            r8 = 134(0x86, float:1.88E-43)
            r5.<init>(r8)
            a.ql r2 = r2.mo21497WP()
            r5.aUM = r2
            a.ql r4 = m21560a((org.mozilla.javascript.C3366ql) r11, (org.mozilla.javascript.C3366ql) r4, (org.mozilla.javascript.C3366ql) r1, (org.mozilla.javascript.C3366ql) r5)
            r2 = r3
            goto L_0x0198
        L_0x01e6:
            a.ql r2 = r7.mo21499WR()
            r0.aUM = r2
            goto L_0x01b3
        L_0x01ed:
            r9.mo13268b(r1, r10)
            goto L_0x0066
        L_0x01f2:
            r9.mo13267a(r1, r10)
            goto L_0x0066
        L_0x01f7:
            a.ql r0 = r1.cFE()
            int r0 = r0.getType()
            r2 = 152(0x98, float:2.13E-43)
            if (r0 != r2) goto L_0x021d
            int r0 = r10.getType()
            r2 = 108(0x6c, float:1.51E-43)
            if (r0 != r2) goto L_0x021b
            r0 = r10
            a.YQ r0 = (p001a.C1658YQ) r0
            boolean r0 = r0.requiresActivation()
            if (r0 != 0) goto L_0x021b
            r0 = 0
        L_0x0215:
            a.ql r1 = r9.mo13265a((boolean) r0, (org.mozilla.javascript.C3366ql) r11, (org.mozilla.javascript.C3366ql) r5, (org.mozilla.javascript.C3366ql) r1)
            goto L_0x0066
        L_0x021b:
            r0 = 1
            goto L_0x0215
        L_0x021d:
            a.ql r7 = new a.ql
            r0 = 128(0x80, float:1.794E-43)
            r7.<init>(r0)
            a.ql r3 = r1.cFE()
        L_0x0228:
            if (r3 != 0) goto L_0x0230
            a.ql r1 = m21562b(r11, r5, r1, r7)
            goto L_0x0066
        L_0x0230:
            a.ql r4 = r3.cFG()
            int r0 = r3.getType()
            r2 = 39
            if (r0 != r2) goto L_0x026f
            boolean r0 = r3.hasChildren()
            if (r0 != 0) goto L_0x0244
            r3 = r4
            goto L_0x0228
        L_0x0244:
            a.ql r8 = r3.cFE()
            r3.mo21464J(r8)
            r0 = 49
            r3.setType(r0)
            a.ql r2 = new a.ql
            r0 = 153(0x99, float:2.14E-43)
            if (r6 != r0) goto L_0x026c
            r0 = 154(0x9a, float:2.16E-43)
        L_0x0258:
            r2.<init>((int) r0, (org.mozilla.javascript.C3366ql) r3, (org.mozilla.javascript.C3366ql) r8)
            r0 = r2
        L_0x025c:
            a.ql r2 = new a.ql
            r3 = 132(0x84, float:1.85E-43)
            int r8 = r1.getLineno()
            r2.<init>((int) r3, (org.mozilla.javascript.C3366ql) r0, (int) r8)
            r7.mo21461G(r2)
            r3 = r4
            goto L_0x0228
        L_0x026c:
            r0 = 8
            goto L_0x0258
        L_0x026f:
            int r0 = r3.getType()
            r2 = 157(0x9d, float:2.2E-43)
            if (r0 == r2) goto L_0x0364
            java.lang.RuntimeException r0 = p001a.C1520WN.codeBug()
            throw r0
        L_0x027c:
            java.lang.String r0 = r1.getString()
            a.ql$c r0 = r12.mo21509iL(r0)
            if (r0 == 0) goto L_0x0066
            r1.mo21465c(r0)
            goto L_0x0066
        L_0x028b:
            a.ql r0 = r1.cFE()
            r2 = 7
            if (r6 != r2) goto L_0x02c7
        L_0x0292:
            int r2 = r0.getType()
            r3 = 26
            if (r2 == r3) goto L_0x02d6
            int r2 = r0.getType()
            r3 = 12
            if (r2 == r3) goto L_0x02aa
            int r2 = r0.getType()
            r3 = 13
            if (r2 != r3) goto L_0x02c7
        L_0x02aa:
            a.ql r3 = r0.cFE()
            a.ql r2 = r0.cFF()
            int r4 = r3.getType()
            r5 = 39
            if (r4 != r5) goto L_0x02db
            java.lang.String r4 = r3.getString()
            java.lang.String r5 = "undefined"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x02db
            r0 = r2
        L_0x02c7:
            int r2 = r0.getType()
            r3 = 33
            if (r2 != r3) goto L_0x0066
            r2 = 34
            r0.setType(r2)
            goto L_0x0066
        L_0x02d6:
            a.ql r0 = r0.cFE()
            goto L_0x0292
        L_0x02db:
            int r4 = r2.getType()
            r5 = 39
            if (r4 != r5) goto L_0x02c7
            java.lang.String r2 = r2.getString()
            java.lang.String r4 = "undefined"
            boolean r2 = r2.equals(r4)
            if (r2 == 0) goto L_0x02c7
            r0 = r3
            goto L_0x02c7
        L_0x02f1:
            if (r13 != 0) goto L_0x0066
            r0 = 39
            if (r6 != r0) goto L_0x0316
            r0 = r1
        L_0x02f8:
            a.ql$c r2 = r0.cFI()
            if (r2 != 0) goto L_0x0066
            java.lang.String r2 = r0.getString()
            a.ql$c r2 = r12.mo21509iL(r2)
            if (r2 == 0) goto L_0x0066
            r0.mo21465c(r2)
            r2 = 39
            if (r6 != r2) goto L_0x032b
            r0 = 55
            r1.setType(r0)
            goto L_0x0066
        L_0x0316:
            a.ql r0 = r1.cFE()
            int r2 = r0.getType()
            r3 = 49
            if (r2 == r3) goto L_0x02f8
            r0 = 31
            if (r6 == r0) goto L_0x0066
            java.lang.RuntimeException r0 = p001a.C1520WN.codeBug()
            throw r0
        L_0x032b:
            r2 = 8
            if (r6 != r2) goto L_0x033b
            r2 = 56
            r1.setType(r2)
            r2 = 41
            r0.setType(r2)
            goto L_0x0066
        L_0x033b:
            r2 = 154(0x9a, float:2.16E-43)
            if (r6 != r2) goto L_0x034b
            r2 = 155(0x9b, float:2.17E-43)
            r1.setType(r2)
            r2 = 41
            r0.setType(r2)
            goto L_0x0066
        L_0x034b:
            r0 = 31
            if (r6 != r0) goto L_0x035c
            a.ql r0 = new a.ql
            r2 = 44
            r0.<init>(r2)
            a.ql r1 = m21562b(r11, r5, r1, r0)
            goto L_0x0066
        L_0x035c:
            java.lang.RuntimeException r0 = p001a.C1520WN.codeBug()
            throw r0
        L_0x0361:
            r0 = r12
            goto L_0x006d
        L_0x0364:
            r0 = r3
            goto L_0x025c
        L_0x0367:
            r2 = r3
            goto L_0x0198
        L_0x036a:
            r0 = r2
            goto L_0x015c
        L_0x036d:
            r6 = r2
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6055afb.m21561a(a.aAk, a.ql, a.ql$c, boolean):void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo13267a(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.ScriptOrFnNode aak) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo13268b(org.mozilla1.javascript.Node qlVar, ScriptOrFnNode aak) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public org.mozilla1.javascript.Node mo13265a(boolean z, org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, org.mozilla1.javascript.Node qlVar3) {
        org.mozilla1.javascript.Node qlVar4;
        org.mozilla1.javascript.Node qlVar5;
        org.mozilla1.javascript.Node qlVar6;
        org.mozilla1.javascript.Node cFE = qlVar3.cFE();
        org.mozilla1.javascript.Node cFG = cFE.cFG();
        qlVar3.mo21464J(cFE);
        qlVar3.mo21464J(cFG);
        boolean z2 = qlVar3.getType() == 157;
        if (z) {
            org.mozilla1.javascript.Node b = m21562b(qlVar, qlVar2, qlVar3, new org.mozilla1.javascript.Node(z2 ? 158 : 128));
            ArrayList arrayList = new ArrayList();
            org.mozilla1.javascript.Node qlVar7 = new org.mozilla1.javascript.Node(66);
            org.mozilla1.javascript.Node qlVar8 = cFG;
            for (org.mozilla1.javascript.Node cFE2 = cFE.cFE(); cFE2 != null; cFE2 = cFE2.cFG()) {
                if (cFE2.getType() == 157) {
                    List list = (List) cFE2.getProp(22);
                    org.mozilla1.javascript.Node cFE3 = cFE2.cFE();
                    if (cFE3.getType() != 152) {
                        throw org.mozilla1.javascript.Kit.codeBug();
                    }
                    if (z2) {
                        qlVar6 = new org.mozilla1.javascript.Node(88, cFE3.cFG(), qlVar8);
                    } else {
                        qlVar6 = new org.mozilla1.javascript.Node(128, new org.mozilla1.javascript.Node(132, cFE3.cFG()), qlVar8);
                    }
                    if (list != null) {
                        arrayList.addAll(list);
                        for (int i = 0; i < list.size(); i++) {
                            qlVar7.mo21461G(new org.mozilla1.javascript.Node(125, org.mozilla1.javascript.Node.m37823W(org.mozilla1.javascript.ScriptRuntime.NaN)));
                        }
                    }
                    qlVar5 = cFE3.cFE();
                    qlVar8 = qlVar6;
                } else {
                    qlVar5 = cFE2;
                }
                if (qlVar5.getType() != 39) {
                    throw org.mozilla1.javascript.Kit.codeBug();
                }
                arrayList.add(org.mozilla1.javascript.ScriptRuntime.m7597nl(qlVar5.getString()));
                org.mozilla1.javascript.Node cFE4 = qlVar5.cFE();
                if (cFE4 == null) {
                    cFE4 = new org.mozilla1.javascript.Node(125, org.mozilla1.javascript.Node.m37823W(org.mozilla1.javascript.ScriptRuntime.NaN));
                }
                qlVar7.mo21461G(cFE4);
            }
            qlVar7.putProp(12, arrayList.toArray());
            b.mo21461G(new org.mozilla1.javascript.Node(2, qlVar7));
            b.mo21461G(new org.mozilla1.javascript.Node(122, qlVar8));
            b.mo21461G(new org.mozilla1.javascript.Node(3));
            return b;
        }
        org.mozilla1.javascript.Node b2 = m21562b(qlVar, qlVar2, qlVar3, new org.mozilla1.javascript.Node(z2 ? 88 : 128));
        org.mozilla1.javascript.Node qlVar9 = new org.mozilla1.javascript.Node(88);
        for (org.mozilla1.javascript.Node cFE5 = cFE.cFE(); cFE5 != null; cFE5 = cFE5.cFG()) {
            if (cFE5.getType() == 157) {
                org.mozilla1.javascript.Node cFE6 = cFE5.cFE();
                if (cFE6.getType() != 152) {
                    throw org.mozilla1.javascript.Kit.codeBug();
                }
                if (z2) {
                    cFG = new org.mozilla1.javascript.Node(88, cFE6.cFG(), cFG);
                } else {
                    cFG = new org.mozilla1.javascript.Node(128, new org.mozilla1.javascript.Node(132, cFE6.cFG()), cFG);
                }
                org.mozilla1.javascript.Node.Scope.m37857a((org.mozilla1.javascript.Node.Scope) cFE5, (org.mozilla1.javascript.Node.Scope) qlVar3);
                qlVar4 = cFE6.cFE();
            } else {
                qlVar4 = cFE5;
            }
            if (qlVar4.getType() != 39) {
                throw Kit.codeBug();
            }
            org.mozilla1.javascript.Node kM = org.mozilla1.javascript.Node.m37829kM(qlVar4.getString());
            kM.mo21465c((org.mozilla1.javascript.Node.Scope) qlVar3);
            org.mozilla1.javascript.Node cFE7 = qlVar4.cFE();
            if (cFE7 == null) {
                cFE7 = new org.mozilla1.javascript.Node(125, org.mozilla1.javascript.Node.m37823W(ScriptRuntime.NaN));
            }
            qlVar9.mo21461G(new org.mozilla1.javascript.Node(56, kM, cFE7));
        }
        if (z2) {
            b2.mo21461G(qlVar9);
            qlVar3.setType(88);
            b2.mo21461G(qlVar3);
            qlVar3.mo21461G(cFG);
            return b2;
        }
        b2.mo21461G(new Node(132, qlVar9));
        qlVar3.setType(128);
        b2.mo21461G(qlVar3);
        qlVar3.mo21463I(cFG);
        return b2;
    }
}
