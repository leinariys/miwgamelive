package org.mozilla1.javascript;

/* renamed from: a.bh */
/* compiled from: a */
public final class NativeCall extends IdScriptableObject {

    static final long serialVersionUID = -7471457301304454454L;
    /* renamed from: nr */
    private static final Object f5872nr = new Integer(12);
    /* renamed from: ns */
    private static final int f5873ns = 1;
    /* renamed from: nt */
    private static final int f5874nt = 1;
    /* renamed from: nu */
    NativeFunction f5875nu;

    /* renamed from: nv */
    Object[] f5876nv;

    /* renamed from: nw */
    transient NativeCall f5877nw;

    NativeCall() {
    }

    NativeCall(NativeFunction ifR, Scriptable avf, Object[] objArr) {
        Object obj;
        this.f5875nu = ifR;
        setParentScope(avf);
        this.f5876nv = objArr == null ? ScriptRuntime.emptyArgs : objArr;
        int paramAndVarCount = ifR.getParamAndVarCount();
        int paramCount = ifR.getParamCount();
        if (paramAndVarCount != 0) {
            for (int i = 0; i < paramCount; i++) {
                String paramOrVarName = ifR.getParamOrVarName(i);
                if (i < objArr.length) {
                    obj = objArr[i];
                } else {
                    obj = Undefined.instance;
                }
                defineProperty(paramOrVarName, obj, 4);
            }
        }
        if (!super.has("arguments", this)) {
            defineProperty("arguments", (Object) new Arguments(this), 4);
        }
        if (paramAndVarCount != 0) {
            for (int i2 = paramCount; i2 < paramAndVarCount; i2++) {
                String paramOrVarName2 = ifR.getParamOrVarName(i2);
                if (!super.has(paramOrVarName2, this)) {
                    if (ifR.mo2867kH(i2)) {
                        defineProperty(paramOrVarName2, Undefined.instance, 13);
                    } else {
                        defineProperty(paramOrVarName2, Undefined.instance, 4);
                    }
                }
            }
        }
    }

    /* renamed from: a */
    static void m27962a(Scriptable avf, boolean z) {
        new NativeCall().mo15695a(1, avf, z);
    }

    public String getClassName() {
        return "Call";
    }

    /* access modifiers changed from: protected */
    public int findPrototypeId(String str) {
        return str.equals("constructor") ? 1 : 0;
    }

    /* access modifiers changed from: protected */
    public void initPrototypeId(int i) {
        if (i == 1) {
            initPrototypeMethod(f5872nr, i, "constructor", 1);
            return;
        }
        throw new IllegalArgumentException(String.valueOf(i));
    }

    public Object execIdCall(IdFunctionObject yy, Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        if (!yy.hasTag(f5872nr)) {
            return super.execIdCall(yy, lhVar, avf, avf2, objArr);
        }
        int methodId = yy.methodId();
        if (methodId != 1) {
            throw new IllegalArgumentException(String.valueOf(methodId));
        } else if (avf2 != null) {
            throw Context.m35246q("msg.only.from.new", "Call");
        } else {
            ScriptRuntime.m7549b(lhVar, "Call");
            NativeCall bhVar = new NativeCall();
            bhVar.setPrototype(m23593v(avf));
            return bhVar;
        }
    }
}
