package org.mozilla1.javascript;


import java.io.ObjectInputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/* renamed from: a.agz  reason: case insensitive filesystem */
/* compiled from: a */
public class FunctionObject extends org.mozilla1.javascript.BaseFunction {
    public static final int JAVA_BOOLEAN_TYPE = 3;
    public static final int JAVA_DOUBLE_TYPE = 4;
    public static final int JAVA_INT_TYPE = 2;
    public static final int JAVA_OBJECT_TYPE = 6;
    public static final int JAVA_SCRIPTABLE_TYPE = 5;
    public static final int JAVA_STRING_TYPE = 1;
    public static final int JAVA_UNSUPPORTED_TYPE = 0;
    static final long serialVersionUID = -5332312783643935019L;
    private static final short fyt = -1;
    private static final short fyu = -2;
    private static boolean fyv = false;
    C6437amt fyw;
    /* renamed from: Dz */
    private boolean f4542Dz;
    private String eMs;
    private transient int fyA;
    private transient byte[] fyx;
    private int fyy;
    private transient boolean fyz;

    public FunctionObject(String str, Member member, org.mozilla1.javascript.Scriptable avf) {
        if (member instanceof Constructor) {
            this.fyw = new C6437amt((Constructor<?>) (Constructor) member);
            this.f4542Dz = true;
        } else {
            this.fyw = new C6437amt((Method) member);
            this.f4542Dz = this.fyw.isStatic();
        }
        String name = this.fyw.getName();
        this.eMs = str;
        Class<?>[] clsArr = this.fyw.gaM;
        int length = clsArr.length;
        if (length != 4 || (!clsArr[1].isArray() && !clsArr[2].isArray())) {
            this.fyy = length;
            if (length > 0) {
                this.fyx = new byte[length];
                for (int i = 0; i != length; i++) {
                    int typeTag = getTypeTag(clsArr[i]);
                    if (typeTag == 0) {
                        throw org.mozilla1.javascript.Context.m35224a("msg.bad.parms", (Object) clsArr[i].getName(), (Object) name);
                    }
                    this.fyx[i] = (byte) typeTag;
                }
            }
        } else if (clsArr[1].isArray()) {
            if (this.f4542Dz && clsArr[0] == org.mozilla1.javascript.ScriptRuntime.ContextClass && clsArr[1].getComponentType() == org.mozilla1.javascript.ScriptRuntime.ObjectClass && clsArr[2] == org.mozilla1.javascript.ScriptRuntime.FunctionClass && clsArr[3] == Boolean.TYPE) {
                this.fyy = -2;
            } else {
                throw org.mozilla1.javascript.Context.m35246q("msg.varargs.ctor", name);
            }
        } else if (this.f4542Dz && clsArr[0] == org.mozilla1.javascript.ScriptRuntime.ContextClass && clsArr[1] == org.mozilla1.javascript.ScriptRuntime.ScriptableClass && clsArr[2].getComponentType() == org.mozilla1.javascript.ScriptRuntime.ObjectClass && clsArr[3] == org.mozilla1.javascript.ScriptRuntime.FunctionClass) {
            this.fyy = -1;
        } else {
            throw org.mozilla1.javascript.Context.m35246q("msg.varargs.fun", name);
        }
        if (this.fyw.isMethod()) {
            Class<?> returnType = this.fyw.cjw().getReturnType();
            if (returnType == Void.TYPE) {
                this.fyz = true;
            } else {
                this.fyA = getTypeTag(returnType);
            }
        } else {
            Class<?> declaringClass = this.fyw.getDeclaringClass();
            if (!org.mozilla1.javascript.ScriptRuntime.ScriptableClass.isAssignableFrom(declaringClass)) {
                throw org.mozilla1.javascript.Context.m35246q("msg.bad.ctor.return", declaringClass.getName());
            }
        }
        org.mozilla1.javascript.ScriptRuntime.m7527a((org.mozilla1.javascript.BaseFunction) this, avf);
    }

    public static int getTypeTag(Class<?> cls) {
        if (cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
            return 1;
        }
        if (cls == org.mozilla1.javascript.ScriptRuntime.IntegerClass || cls == Integer.TYPE) {
            return 2;
        }
        if (cls == org.mozilla1.javascript.ScriptRuntime.BooleanClass || cls == Boolean.TYPE) {
            return 3;
        }
        if (cls == org.mozilla1.javascript.ScriptRuntime.DoubleClass || cls == Double.TYPE) {
            return 4;
        }
        if (org.mozilla1.javascript.ScriptRuntime.ScriptableClass.isAssignableFrom(cls)) {
            return 5;
        }
        if (cls == org.mozilla1.javascript.ScriptRuntime.ObjectClass) {
            return 6;
        }
        return 0;
    }

    /* renamed from: a */
    public static Object m22160a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object obj, int i) {
        Boolean bool;
        switch (i) {
            case 1:
                if (obj instanceof String) {
                    return obj;
                }
                return org.mozilla1.javascript.ScriptRuntime.toString(obj);
            case 2:
                if (!(obj instanceof Integer)) {
                    return new Integer(org.mozilla1.javascript.ScriptRuntime.toInt32(obj));
                }
                return obj;
            case 3:
                if (obj instanceof Boolean) {
                    return obj;
                }
                if (org.mozilla1.javascript.ScriptRuntime.toBoolean(obj)) {
                    bool = Boolean.TRUE;
                } else {
                    bool = Boolean.FALSE;
                }
                return bool;
            case 4:
                if (!(obj instanceof Double)) {
                    return new Double(org.mozilla1.javascript.ScriptRuntime.toNumber(obj));
                }
                return obj;
            case 5:
                if (!(obj instanceof org.mozilla1.javascript.Scriptable)) {
                    return org.mozilla1.javascript.ScriptRuntime.m7572e(lhVar, avf, obj);
                }
                return obj;
            case 6:
                return obj;
            default:
                throw new IllegalArgumentException();
        }
    }

    /* renamed from: a */
    static Method m22161a(Method[] methodArr, String str) {
        Method method = null;
        int length = methodArr.length;
        int i = 0;
        while (i != length) {
            Method method2 = methodArr[i];
            if (method2 == null || !str.equals(method2.getName())) {
                method2 = method;
            } else if (method != null) {
                throw org.mozilla1.javascript.Context.m35224a("msg.no.overload", (Object) str, (Object) method2.getDeclaringClass().getName());
            }
            i++;
            method = method2;
        }
        return method;
    }

    /* renamed from: ah */
    static Method[] m22162ah(Class<?> cls) {
        Method[] methodArr = null;
        try {
            if (!fyv) {
                methodArr = cls.getDeclaredMethods();
            }
        } catch (SecurityException e) {
            fyv = true;
        }
        if (methodArr == null) {
            methodArr = cls.getMethods();
        }
        int i = 0;
        for (int i2 = 0; i2 < methodArr.length; i2++) {
            if (!fyv ? !Modifier.isPublic(methodArr[i2].getModifiers()) : methodArr[i2].getDeclaringClass() != cls) {
                methodArr[i2] = null;
            } else {
                i++;
            }
        }
        Method[] methodArr2 = new Method[i];
        int i3 = 0;
        for (int i4 = 0; i4 < methodArr.length; i4++) {
            if (methodArr[i4] != null) {
                methodArr2[i3] = methodArr[i4];
                i3++;
            }
        }
        return methodArr2;
    }

    /* renamed from: c */
    public static Object m22163c(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object obj, Class<?> cls) {
        int typeTag = getTypeTag(cls);
        if (typeTag != 0) {
            return m22160a(lhVar, avf, obj, typeTag);
        }
        throw org.mozilla1.javascript.Context.m35246q("msg.cant.convert", cls.getName());
    }

    public int getArity() {
        if (this.fyy < 0) {
            return 1;
        }
        return this.fyy;
    }

    public int getLength() {
        return getArity();
    }

    public String getFunctionName() {
        return this.eMs == null ? "" : this.eMs;
    }

    public Member getMethodOrConstructor() {
        if (this.fyw.isMethod()) {
            return this.fyw.cjw();
        }
        return this.fyw.cjx();
    }

    /* renamed from: a */
    public void mo13478a(org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2) {
        mo13479b(avf, avf2);
        m23567a(avf, avf2.getClassName(), (Object) this, 2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo13479b(org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2) {
        org.mozilla1.javascript.ScriptRuntime.m7527a((BaseFunction) this, avf);
        setImmunePrototypeProperty(avf2);
        avf2.setParentScope(this);
        m23567a(avf2, "constructor", (Object) this, 7);
        setParentScope(avf);
    }

    public Object call(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        Object[] objArr2;
        Object obj;
        Object newInstance;
        Object[] objArr3;
        boolean z;
        org.mozilla1.javascript.Scriptable parentScope;
        boolean z2;
        boolean z3 = false;
        if (this.fyy >= 0) {
            if (!this.f4542Dz) {
                Class<?> declaringClass = this.fyw.getDeclaringClass();
                if (!declaringClass.isInstance(avf2)) {
                    if (avf2 != avf || avf == (parentScope = getParentScope())) {
                        z = false;
                    } else {
                        z = declaringClass.isInstance(parentScope);
                        if (z) {
                            avf2 = parentScope;
                        }
                    }
                    if (!z) {
                        throw org.mozilla1.javascript.ScriptRuntime.m7536aZ("msg.incompat.call", this.eMs);
                    }
                }
            }
            if (this.fyy == objArr.length) {
                objArr2 = objArr;
                for (int i = 0; i != this.fyy; i++) {
                    Object obj2 = objArr[i];
                    Object a = m22160a(lhVar, avf, obj2, this.fyx[i]);
                    if (obj2 != a) {
                        if (objArr2 == objArr) {
                            objArr3 = (Object[]) objArr.clone();
                        } else {
                            objArr3 = objArr2;
                        }
                        objArr3[i] = a;
                        objArr2 = objArr3;
                    }
                }
            } else if (this.fyy == 0) {
                objArr2 = ScriptRuntime.emptyArgs;
            } else {
                objArr2 = new Object[this.fyy];
                for (int i2 = 0; i2 != this.fyy; i2++) {
                    if (i2 < objArr.length) {
                        obj = objArr[i2];
                    } else {
                        obj = org.mozilla1.javascript.Undefined.instance;
                    }
                    objArr2[i2] = m22160a(lhVar, avf, obj, this.fyx[i2]);
                }
            }
            if (this.fyw.isMethod()) {
                newInstance = this.fyw.invoke(avf2, objArr2);
                z3 = true;
            } else {
                newInstance = this.fyw.newInstance(objArr2);
            }
        } else if (this.fyy == -1) {
            newInstance = this.fyw.invoke((Object) null, new Object[]{lhVar, avf2, objArr, this});
            z3 = true;
        } else {
            if (avf2 == null) {
                z2 = true;
            } else {
                z2 = false;
            }
            Object[] objArr4 = {lhVar, objArr, this, z2 ? Boolean.TRUE : Boolean.FALSE};
            if (this.fyw.cjz()) {
                newInstance = this.fyw.newInstance(objArr4);
            } else {
                newInstance = this.fyw.invoke((Object) null, objArr4);
            }
        }
        if (!z3) {
            return newInstance;
        }
        if (this.fyz) {
            return Undefined.instance;
        }
        if (this.fyA == 0) {
            return lhVar.bwy().mo6829a(lhVar, avf, newInstance, (Class<?>) null);
        }
        return newInstance;
    }

    /* renamed from: c */
    public org.mozilla1.javascript.Scriptable mo7313c(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf) {
        if (this.fyw.cjz() || this.fyy == -2) {
            return null;
        }
        try {
            org.mozilla1.javascript.Scriptable avf2 = (Scriptable) this.fyw.getDeclaringClass().newInstance();
            avf2.setPrototype(bOH());
            avf2.setParentScope(getParentScope());
            return avf2;
        } catch (Exception e) {
            throw Context.throwAsScriptRuntimeEx(e);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean bWT() {
        return this.fyy == -1;
    }

    /* access modifiers changed from: package-private */
    public boolean bWU() {
        return this.fyy == -2;
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        if (this.fyy > 0) {
            Class<?>[] clsArr = this.fyw.gaM;
            this.fyx = new byte[this.fyy];
            for (int i = 0; i != this.fyy; i++) {
                this.fyx[i] = (byte) getTypeTag(clsArr[i]);
            }
        }
        if (this.fyw.isMethod()) {
            Class<?> returnType = this.fyw.cjw().getReturnType();
            if (returnType == Void.TYPE) {
                this.fyz = true;
            } else {
                this.fyA = getTypeTag(returnType);
            }
        }
    }
}
