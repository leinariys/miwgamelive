package org.mozilla1.javascript;


/* renamed from: a.dx */
/* compiled from: a */
public class JavaScriptException extends RhinoException {
    static final long serialVersionUID = -7666130513694669293L;
    private Object value;

    public JavaScriptException(Object obj) {
        this(obj, "", 0);
    }

    public JavaScriptException(Object obj, String str, int i) {
        mo4868a(str, i, (String) null, 0);
        this.value = obj;
    }

    public String details() {
        try {
            return org.mozilla1.javascript.ScriptRuntime.toString(this.value);
        } catch (RuntimeException e) {
            if (this.value == null) {
                return "null";
            }
            if (this.value instanceof org.mozilla1.javascript.Scriptable) {
                return ScriptRuntime.m7606q((Scriptable) this.value);
            }
            return this.value.toString();
        }
    }

    public Object getValue() {
        return this.value;
    }

    public String getSourceName() {
        return sourceName();
    }

    public int getLineNumber() {
        return lineNumber();
    }
}
