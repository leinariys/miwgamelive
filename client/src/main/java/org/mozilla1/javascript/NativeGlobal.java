package org.mozilla1.javascript;

import org.mozilla1.javascript.xml.XMLLib;

import java.io.Serializable;

/* renamed from: a.Du */
/* compiled from: a */
public class NativeGlobal implements IdFunctionCall, Serializable {
    static final long serialVersionUID = 6080442165748707530L;
    private static final String cHd = ";/?:@&=+$,#";
    private static final Object cHe = new Integer(15);
    private static final int cHf = 1;
    private static final int cHg = 2;
    private static final int cHh = 3;
    private static final int cHi = 4;
    private static final int cHj = 5;
    private static final int cHk = 6;
    private static final int cHl = 7;
    private static final int cHm = 8;
    private static final int cHn = 9;
    private static final int cHo = 10;
    private static final int cHp = 11;
    private static final int cHq = 12;
    private static final int cHr = 13;
    private static final int cHs = 13;
    private static final int cHt = 14;

    public static void init(org.mozilla1.javascript.Context lhVar, Scriptable avf, boolean z) {
        String str;
        int i;
        NativeGlobal du = new NativeGlobal();
        for (int i2 = 1; i2 <= 13; i2++) {
            switch (i2) {
                case 1:
                    str = "decodeURI";
                    i = 1;
                    break;
                case 2:
                    str = "decodeURIComponent";
                    i = 1;
                    break;
                case 3:
                    str = "encodeURI";
                    i = 1;
                    break;
                case 4:
                    str = "encodeURIComponent";
                    i = 1;
                    break;
                case 5:
                    str = "escape";
                    i = 1;
                    break;
                case 6:
                    str = "eval";
                    i = 1;
                    break;
                case 7:
                    str = "isFinite";
                    i = 1;
                    break;
                case 8:
                    str = "isNaN";
                    i = 1;
                    break;
                case 9:
                    str = "isXMLName";
                    i = 1;
                    break;
                case 10:
                    str = "parseFloat";
                    i = 1;
                    break;
                case 11:
                    str = "parseInt";
                    i = 2;
                    break;
                case 12:
                    str = "unescape";
                    i = 1;
                    break;
                case 13:
                    str = "uneval";
                    i = 1;
                    break;
                default:
                    throw org.mozilla1.javascript.Kit.codeBug();
            }
            IdFunctionObject yy = new IdFunctionObject(du, cHe, i2, str, i, avf);
            if (z) {
                yy.sealObject();
            }
            yy.exportAsScopeProperty();
        }
        ScriptableObject.m23567a(avf, "NaN", (Object) org.mozilla1.javascript.ScriptRuntime.NaNobj, 2);
        ScriptableObject.m23567a(avf, "Infinity", (Object) org.mozilla1.javascript.ScriptRuntime.wrapNumber(Double.POSITIVE_INFINITY), 2);
        ScriptableObject.m23567a(avf, "undefined", Undefined.instance, 2);
        String[] semicolonSplit = org.mozilla1.javascript.Kit.semicolonSplit("ConversionError;EvalError;RangeError;ReferenceError;SyntaxError;TypeError;URIError;InternalError;JavaException;");
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 < semicolonSplit.length) {
                String str2 = semicolonSplit[i4];
                Scriptable b = org.mozilla1.javascript.ScriptRuntime.m7539b(lhVar, avf, "Error", org.mozilla1.javascript.ScriptRuntime.emptyArgs);
                b.put("name", b, (Object) str2);
                if (z && (b instanceof ScriptableObject)) {
                    ((ScriptableObject) b).sealObject();
                }
                IdFunctionObject yy2 = new IdFunctionObject(du, cHe, 14, str2, 1, avf);
                yy2.mo7318h(b);
                if (z) {
                    yy2.sealObject();
                }
                yy2.exportAsScopeProperty();
                i3 = i4 + 1;
            } else {
                return;
            }
        }
    }

    /* renamed from: L */
    static boolean m2724L(Object obj) {
        if (obj instanceof IdFunctionObject) {
            IdFunctionObject yy = (IdFunctionObject) obj;
            if (!yy.hasTag(cHe) || yy.methodId() != 6) {
                return false;
            }
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public static EcmaError m2728a(org.mozilla1.javascript.Context lhVar, String str, String str2, Scriptable avf) {
        return org.mozilla1.javascript.ScriptRuntime.m7535aY(str, str2);
    }

    /* renamed from: a */
    public static EcmaError m2729a(org.mozilla1.javascript.Context lhVar, String str, String str2, Scriptable avf, String str3, int i, int i2, String str4) {
        return org.mozilla1.javascript.ScriptRuntime.m7474a(str, str2, str3, i, str4, i2);
    }

    /* renamed from: h */
    private static String m2734h(String str, boolean z) {
        StringBuffer stringBuffer;
        int i;
        char c = 0;
        StringBuffer stringBuffer2;
        StringBuffer stringBuffer3 = null;
        int length = str.length();
        int i2 = 0;
        byte[] bArr = null;
        while (i2 != length) {
            char charAt = str.charAt(i2);
            if (!m2731a(charAt, z)) {
                if (stringBuffer3 == null) {
                    stringBuffer = new StringBuffer(length + 3);
                    stringBuffer.append(str);
                    stringBuffer.setLength(i2);
                    bArr = new byte[6];
                } else {
                    stringBuffer = stringBuffer3;
                }
                if (56320 > charAt || charAt > 57343) {
                    if (charAt < 55296 || 56319 < charAt) {
                        i = i2;
                        c = charAt;
                    } else {
                        i = i2 + 1;
                        if (i == length) {
                            throw org.mozilla1.javascript.Context.m35244gE("msg.bad.uri");
                        }
                        char charAt2 = str.charAt(i);
                        if (56320 > charAt2 || charAt2 > 57343) {
                            throw org.mozilla1.javascript.Context.m35244gE("msg.bad.uri");
                        }
                        //  c = ((charAt - 55296) << 10) + (charAt2 - 56320) + C0677Jd.dij;
                    }
                    int a = m2727a(bArr, (int) c);
                    for (int i3 = 0; i3 < a; i3++) {
                        byte b = (byte) (bArr[i3] & 255);
                        stringBuffer.append('%');
                        stringBuffer.append(m2732gJ(b >>> 4));
                        stringBuffer.append(m2732gJ(b & 15));
                    }
                    i2 = i;
                    stringBuffer2 = stringBuffer;
                } else {
                    throw org.mozilla1.javascript.Context.m35244gE("msg.bad.uri");
                }
            } else if (stringBuffer3 != null) {
                stringBuffer3.append(charAt);
                stringBuffer2 = stringBuffer3;
            } else {
                stringBuffer2 = stringBuffer3;
            }
            i2++;
            stringBuffer3 = stringBuffer2;
        }
        if (stringBuffer3 == null) {
            return str;
        }
        return stringBuffer3.toString();
    }

    /* renamed from: gJ */
    private static char m2732gJ(int i) {
        if ((i >> 4) != 0) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        return (char) (i < 10 ? i + 48 : (i - 10) + 65);
    }

    /* renamed from: a */
    private static int m2725a(char c) {
        if ('A' <= c && c <= 'F') {
            return (c - 'A') + 10;
        }
        if ('a' <= c && c <= 'f') {
            return (c - 'a') + 10;
        }
        if ('0' > c || c > '9') {
            return -1;
        }
        return c - '0';
    }

    /* renamed from: a */
    private static int m2726a(char c, char c2) {
        int a = m2725a(c);
        int a2 = m2725a(c2);
        if (a < 0 || a2 < 0) {
            return -1;
        }
        return (a << 4) | a2;
    }

    /* renamed from: i */
    private static String m2736i(String str, boolean z) {
        char[] cArr;
        int i;
        int i2;
        int i3;
        char c;
        int i4;
        int i5 = 0;
        int length = str.length();
        int i6 = 0;
        char[] cArr2 = null;
        while (i6 != length) {
            char charAt = str.charAt(i6);
            if (charAt != '%') {
                if (cArr2 != null) {
                    i4 = i5 + 1;
                    cArr2[i5] = charAt;
                } else {
                    i4 = i5;
                }
                i6++;
                i5 = i4;
            } else {
                if (cArr2 == null) {
                    char[] cArr3 = new char[length];
                    str.getChars(0, i6, cArr3, 0);
                    i5 = i6;
                    cArr = cArr3;
                } else {
                    cArr = cArr2;
                }
                if (i6 + 3 > length) {
                    throw org.mozilla1.javascript.Context.m35244gE("msg.bad.uri");
                }
                int a = m2726a(str.charAt(i6 + 1), str.charAt(i6 + 2));
                if (a < 0) {
                    throw org.mozilla1.javascript.Context.m35244gE("msg.bad.uri");
                }
                int i7 = i6 + 3;
                if ((a & 128) == 0) {
                    c = (char) a;
                } else if ((a & 192) == 128) {
                    throw org.mozilla1.javascript.Context.m35244gE("msg.bad.uri");
                } else {
                    if ((a & 32) == 0) {
                        i = 1;
                        i2 = a & 31;
                        i3 = 128;
                    } else if ((a & 16) == 0) {
                        i = 2;
                        i2 = a & 15;
                        i3 = 2048;
                    } else if ((a & 8) == 0) {
                        i = 3;
                        i2 = a & 7;
                        i3 = 65536;
                    } else if ((a & 4) == 0) {
                        i = 4;
                        i2 = a & 3;
                        i3 = 2097152;
                    } else if ((a & 2) == 0) {
                        i = 5;
                        i2 = a & 1;
                        i3 = 67108864;
                    } else {
                        throw org.mozilla1.javascript.Context.m35244gE("msg.bad.uri");
                    }
                    if ((i * 3) + i7 > length) {
                        throw org.mozilla1.javascript.Context.m35244gE("msg.bad.uri");
                    }
                    for (int i8 = 0; i8 != i; i8++) {
                        if (str.charAt(i7) != '%') {
                            throw org.mozilla1.javascript.Context.m35244gE("msg.bad.uri");
                        }
                        int a2 = m2726a(str.charAt(i7 + 1), str.charAt(i7 + 2));
                        if (a2 < 0 || (a2 & 192) != 128) {
                            throw org.mozilla1.javascript.Context.m35244gE("msg.bad.uri");
                        }
                        i2 = (i2 << 6) | (a2 & 63);
                        i7 += 3;
                    }
                    if (i2 < i3 || i2 == 65534 || i2 == 65535) {
                        i2 = 65533;
                    }
                    if (i2 >= 65536) {
                        //  int i9 = i2 - C0677Jd.dij;
                        int i9 = 0;
                        if (i9 > 1048575) {
                            throw org.mozilla1.javascript.Context.m35244gE("msg.bad.uri");
                        }
                        char c2 = (char) ((i9 >>> 10) + 55296);
                        c = (char) ((i9 & 1023) + 56320);
                        cArr[i5] = c2;
                        i5++;
                    } else {
                        c = (char) i2;
                    }
                }
                if (!z || cHd.indexOf(c) < 0) {
                    cArr[i5] = c;
                    i6 = i7;
                    i5++;
                    cArr2 = cArr;
                } else {
                    int i10 = i6;
                    while (i10 != i7) {
                        cArr[i5] = str.charAt(i10);
                        i10++;
                        i5++;
                    }
                    i6 = i7;
                    cArr2 = cArr;
                }
            }
        }
        if (cArr2 == null) {
            return str;
        }
        return new String(cArr2, 0, i5);
    }

    /* renamed from: a */
    private static boolean m2731a(char c, boolean z) {
        if ('A' <= c && c <= 'Z') {
            return true;
        }
        if ('a' <= c && c <= 'z') {
            return true;
        }
        if (('0' <= c && c <= '9') || "-_.!~*'()".indexOf(c) >= 0) {
            return true;
        }
        if (!z) {
            return false;
        }
        if (cHd.indexOf(c) < 0) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    private static int m2727a(byte[] bArr, int i) {
        if ((i & -128) == 0) {
            bArr[0] = (byte) i;
            return 1;
        }
        int i2 = i >>> 11;
        int i3 = 2;
        while (i2 != 0) {
            i2 >>>= 5;
            i3++;
        }
        int i4 = i3;
        while (true) {
            i4--;
            if (i4 <= 0) {
                bArr[0] = (byte) ((256 - (1 << (8 - i3))) + i);
                return i3;
            }
            bArr[i4] = (byte) ((i & 63) | 128);
            i >>>= 6;
        }
    }

    public Object execIdCall(IdFunctionObject yy, org.mozilla1.javascript.Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        boolean z = true;
        boolean z2 = false;
        if (yy.hasTag(cHe)) {
            int methodId = yy.methodId();
            switch (methodId) {
                case 1:
                case 2:
                    String nh = org.mozilla1.javascript.ScriptRuntime.toString(objArr, 0);
                    if (methodId != 1) {
                        z = false;
                    }
                    return m2736i(nh, z);
                case 3:
                case 4:
                    String nh2 = org.mozilla1.javascript.ScriptRuntime.toString(objArr, 0);
                    if (methodId != 3) {
                        z = false;
                    }
                    return m2734h(nh2, z);
                case 5:
                    return m2737j(objArr);
                case 6:
                    return m2730a(lhVar, avf, avf2, objArr);
                case 7:
                    if (objArr.length >= 1) {
                        double number = org.mozilla1.javascript.ScriptRuntime.toNumber(objArr[0]);
                        if (number != number || number == Double.POSITIVE_INFINITY || number == Double.NEGATIVE_INFINITY) {
                            z = false;
                        }
                        z2 = z;
                    }
                    return org.mozilla1.javascript.ScriptRuntime.wrapBoolean(z2);
                case 8:
                    if (objArr.length >= 1) {
                        double number2 = org.mozilla1.javascript.ScriptRuntime.toNumber(objArr[0]);
                        if (number2 == number2) {
                            z = false;
                        }
                    }
                    return org.mozilla1.javascript.ScriptRuntime.wrapBoolean(z);
                case 9:
                    return org.mozilla1.javascript.ScriptRuntime.wrapBoolean(XMLLib.m370b(avf).isXMLName(lhVar, objArr.length == 0 ? Undefined.instance : objArr[0]));
                case 10:
                    return m2735i(objArr);
                case 11:
                    return m2733h(objArr);
                case 12:
                    return m2738k(objArr);
                case 13:
                    return org.mozilla1.javascript.ScriptRuntime.m7571d(lhVar, avf, objArr.length != 0 ? objArr[0] : Undefined.instance);
                case 14:
                    return NativeError.m14615a(lhVar, avf, yy, objArr);
            }
        }
        throw yy.unknown();
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0064  */
    /* renamed from: h */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object m2733h(java.lang.Object[] r12) {
        /*
            r11 = this;
            r2 = 16
            r3 = -1
            r10 = 48
            r6 = 1
            r1 = 0
            java.lang.String r7 = p001a.C0903NH.toString(r12, r1)
            int r4 = p001a.C0903NH.toInt32(r12, r6)
            int r8 = r7.length()
            if (r8 != 0) goto L_0x0018
            java.lang.Double r0 = p001a.C0903NH.NaNobj
        L_0x0017:
            return r0
        L_0x0018:
            r0 = r1
        L_0x0019:
            char r5 = r7.charAt(r0)
            boolean r9 = java.lang.Character.isWhitespace(r5)
            if (r9 != 0) goto L_0x005f
        L_0x0023:
            r9 = 43
            if (r5 == r9) goto L_0x002e
            r9 = 45
            if (r5 != r9) goto L_0x002c
            r1 = r6
        L_0x002c:
            if (r1 == 0) goto L_0x0099
        L_0x002e:
            int r0 = r0 + 1
            r5 = r1
        L_0x0031:
            if (r4 != 0) goto L_0x0064
            r1 = r3
        L_0x0034:
            if (r1 != r3) goto L_0x0053
            r1 = 10
            int r3 = r8 - r0
            if (r3 <= r6) goto L_0x0053
            char r3 = r7.charAt(r0)
            if (r3 != r10) goto L_0x0053
            int r3 = r0 + 1
            char r3 = r7.charAt(r3)
            r4 = 120(0x78, float:1.68E-43)
            if (r3 == r4) goto L_0x0050
            r4 = 88
            if (r3 != r4) goto L_0x008c
        L_0x0050:
            int r0 = r0 + 2
            r1 = r2
        L_0x0053:
            double r0 = p001a.C0903NH.m7577f((java.lang.String) r7, (int) r0, (int) r1)
            if (r5 == 0) goto L_0x005a
            double r0 = -r0
        L_0x005a:
            java.lang.Number r0 = p001a.C0903NH.wrapNumber(r0)
            goto L_0x0017
        L_0x005f:
            int r0 = r0 + 1
            if (r0 < r8) goto L_0x0019
            goto L_0x0023
        L_0x0064:
            r1 = 2
            if (r4 < r1) goto L_0x006b
            r1 = 36
            if (r4 <= r1) goto L_0x006e
        L_0x006b:
            java.lang.Double r0 = p001a.C0903NH.NaNobj
            goto L_0x0017
        L_0x006e:
            if (r4 != r2) goto L_0x0097
            int r1 = r8 - r0
            if (r1 <= r6) goto L_0x0097
            char r1 = r7.charAt(r0)
            if (r1 != r10) goto L_0x0097
            int r1 = r0 + 1
            char r1 = r7.charAt(r1)
            r9 = 120(0x78, float:1.68E-43)
            if (r1 == r9) goto L_0x0088
            r9 = 88
            if (r1 != r9) goto L_0x0097
        L_0x0088:
            int r0 = r0 + 2
            r1 = r4
            goto L_0x0034
        L_0x008c:
            if (r10 > r3) goto L_0x0053
            r2 = 57
            if (r3 > r2) goto L_0x0053
            r1 = 8
            int r0 = r0 + 1
            goto L_0x0053
        L_0x0097:
            r1 = r4
            goto L_0x0034
        L_0x0099:
            r5 = r1
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C0301Du.m2733h(java.lang.Object[]):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0077, code lost:
        r1 = r1 + 1;
     */
    /* renamed from: i */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object m2735i(java.lang.Object[] r10) {
        /*
            r9 = this;
            r8 = 45
            r2 = 0
            r3 = -1
            int r0 = r10.length
            r1 = 1
            if (r0 >= r1) goto L_0x000b
            java.lang.Double r0 = p001a.C0903NH.NaNobj
        L_0x000a:
            return r0
        L_0x000b:
            r0 = r10[r2]
            java.lang.String r5 = p001a.C0903NH.toString((java.lang.Object) r0)
            int r6 = r5.length()
            r4 = r2
        L_0x0016:
            if (r4 != r6) goto L_0x001b
            java.lang.Double r0 = p001a.C0903NH.NaNobj
            goto L_0x000a
        L_0x001b:
            char r0 = r5.charAt(r4)
            boolean r1 = TokenStream.m23671r(r0)
            if (r1 != 0) goto L_0x0032
            r1 = 43
            if (r0 == r1) goto L_0x002b
            if (r0 != r8) goto L_0x0086
        L_0x002b:
            int r1 = r4 + 1
            if (r1 != r6) goto L_0x0035
            java.lang.Double r0 = p001a.C0903NH.NaNobj
            goto L_0x000a
        L_0x0032:
            int r4 = r4 + 1
            goto L_0x0016
        L_0x0035:
            char r0 = r5.charAt(r1)
        L_0x0039:
            r7 = 73
            if (r0 != r7) goto L_0x005e
            int r0 = r1 + 8
            if (r0 > r6) goto L_0x005b
            java.lang.String r0 = "Infinity"
            r3 = 8
            boolean r0 = r5.regionMatches(r1, r0, r2, r3)
            if (r0 == 0) goto L_0x005b
            char r0 = r5.charAt(r4)
            if (r0 != r8) goto L_0x0058
            r0 = -4503599627370496(0xfff0000000000000, double:-Infinity)
        L_0x0053:
            java.lang.Number r0 = p001a.C0903NH.wrapNumber(r0)
            goto L_0x000a
        L_0x0058:
            r0 = 9218868437227405312(0x7ff0000000000000, double:Infinity)
            goto L_0x0053
        L_0x005b:
            java.lang.Double r0 = p001a.C0903NH.NaNobj
            goto L_0x000a
        L_0x005e:
            r0 = r3
            r2 = r3
        L_0x0060:
            if (r1 < r6) goto L_0x006b
        L_0x0062:
            java.lang.String r0 = r5.substring(r4, r1)
            java.lang.Double r0 = java.lang.Double.valueOf(r0)     // Catch:{ NumberFormatException -> 0x0082 }
            goto L_0x000a
        L_0x006b:
            char r7 = r5.charAt(r1)
            switch(r7) {
                case 43: goto L_0x0073;
                case 45: goto L_0x0073;
                case 46: goto L_0x007a;
                case 48: goto L_0x0077;
                case 49: goto L_0x0077;
                case 50: goto L_0x0077;
                case 51: goto L_0x0077;
                case 52: goto L_0x0077;
                case 53: goto L_0x0077;
                case 54: goto L_0x0077;
                case 55: goto L_0x0077;
                case 56: goto L_0x0077;
                case 57: goto L_0x0077;
                case 69: goto L_0x007e;
                case 101: goto L_0x007e;
                default: goto L_0x0072;
            }
        L_0x0072:
            goto L_0x0062
        L_0x0073:
            int r7 = r1 + -1
            if (r0 != r7) goto L_0x0062
        L_0x0077:
            int r1 = r1 + 1
            goto L_0x0060
        L_0x007a:
            if (r2 != r3) goto L_0x0062
            r2 = r1
            goto L_0x0077
        L_0x007e:
            if (r0 != r3) goto L_0x0062
            r0 = r1
            goto L_0x0077
        L_0x0082:
            r0 = move-exception
            java.lang.Double r0 = p001a.C0903NH.NaNobj
            goto L_0x000a
        L_0x0086:
            r1 = r4
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C0301Du.m2735i(java.lang.Object[]):java.lang.Object");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v0, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v1, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: char} */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0021, code lost:
        if ((r0 & -8) == 0) goto L_0x002a;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: j */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object m2737j(java.lang.Object[] r13) {
        /*
            r12 = this;
            r11 = 43
            r10 = 37
            r2 = 2
            r5 = 1
            r3 = 0
            java.lang.String r4 = p001a.C0903NH.toString(r13, r3)
            r0 = 7
            int r1 = r13.length
            if (r1 <= r5) goto L_0x002a
            r0 = r13[r5]
            double r6 = p001a.C0903NH.toNumber((java.lang.Object) r0)
            int r0 = (r6 > r6 ? 1 : (r6 == r6 ? 0 : -1))
            if (r0 != 0) goto L_0x0023
            int r0 = (int) r6
            double r8 = (double) r0
            int r1 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r1 != 0) goto L_0x0023
            r1 = r0 & -8
            if (r1 == 0) goto L_0x002a
        L_0x0023:
            java.lang.String r0 = "msg.bad.esc.mask"
            a.TJ r0 = p001a.C2909lh.m35244gE(r0)
            throw r0
        L_0x002a:
            r1 = 0
            int r7 = r4.length()
            r5 = r3
        L_0x0030:
            if (r5 != r7) goto L_0x0036
            if (r1 != 0) goto L_0x00c6
            r0 = r4
        L_0x0035:
            return r0
        L_0x0036:
            char r8 = r4.charAt(r5)
            if (r0 == 0) goto L_0x007c
            r3 = 48
            if (r8 < r3) goto L_0x0044
            r3 = 57
            if (r8 <= r3) goto L_0x0072
        L_0x0044:
            r3 = 65
            if (r8 < r3) goto L_0x004c
            r3 = 90
            if (r8 <= r3) goto L_0x0072
        L_0x004c:
            r3 = 97
            if (r8 < r3) goto L_0x0054
            r3 = 122(0x7a, float:1.71E-43)
            if (r8 <= r3) goto L_0x0072
        L_0x0054:
            r3 = 64
            if (r8 == r3) goto L_0x0072
            r3 = 42
            if (r8 == r3) goto L_0x0072
            r3 = 95
            if (r8 == r3) goto L_0x0072
            r3 = 45
            if (r8 == r3) goto L_0x0072
            r3 = 46
            if (r8 == r3) goto L_0x0072
            r3 = r0 & 4
            if (r3 == 0) goto L_0x007c
            r3 = 47
            if (r8 == r3) goto L_0x0072
            if (r8 != r11) goto L_0x007c
        L_0x0072:
            if (r1 == 0) goto L_0x0078
            char r3 = (char) r8
            r1.append(r3)
        L_0x0078:
            int r3 = r5 + 1
            r5 = r3
            goto L_0x0030
        L_0x007c:
            if (r1 != 0) goto L_0x00cc
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            int r1 = r7 + 3
            r3.<init>(r1)
            r3.append(r4)
            r3.setLength(r5)
        L_0x008b:
            r1 = 256(0x100, float:3.59E-43)
            if (r8 >= r1) goto L_0x00a7
            r1 = 32
            if (r8 != r1) goto L_0x009a
            if (r0 != r2) goto L_0x009a
            r3.append(r11)
            r1 = r3
            goto L_0x0078
        L_0x009a:
            r3.append(r10)
            r1 = r2
        L_0x009e:
            int r1 = r1 + -1
            int r1 = r1 * 4
            r6 = r1
        L_0x00a3:
            if (r6 >= 0) goto L_0x00b1
            r1 = r3
            goto L_0x0078
        L_0x00a7:
            r3.append(r10)
            r1 = 117(0x75, float:1.64E-43)
            r3.append(r1)
            r1 = 4
            goto L_0x009e
        L_0x00b1:
            int r1 = r8 >> r6
            r1 = r1 & 15
            r9 = 10
            if (r1 >= r9) goto L_0x00c3
            int r1 = r1 + 48
        L_0x00bb:
            char r1 = (char) r1
            r3.append(r1)
            int r1 = r6 + -4
            r6 = r1
            goto L_0x00a3
        L_0x00c3:
            int r1 = r1 + 55
            goto L_0x00bb
        L_0x00c6:
            java.lang.String r0 = r1.toString()
            goto L_0x0035
        L_0x00cc:
            r3 = r1
            goto L_0x008b
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C0301Du.m2737j(java.lang.Object[]):java.lang.Object");
    }

    /* renamed from: k */
    private Object m2738k(Object[] objArr) {
        int i;
        String nh = org.mozilla1.javascript.ScriptRuntime.toString(objArr, 0);
        int indexOf = nh.indexOf(37);
        if (indexOf < 0) {
            return nh;
        }
        int length = nh.length();
        char[] charArray = nh.toCharArray();
        int i2 = indexOf;
        int i3 = indexOf;
        while (i2 != length) {
            char c = charArray[i2];
            int i4 = i2 + 1;
            if (c == '%' && i4 != length) {
                if (charArray[i4] == 'u') {
                    i = i4 + 1;
                    i2 = i4 + 5;
                } else {
                    i2 = i4 + 2;
                    i = i4;
                }
                if (i2 <= length) {
                    int i5 = 0;
                    while (i != i2) {
                        i5 = Kit.xDigitToInt(charArray[i], i5);
                        i++;
                    }
                    if (i5 >= 0) {
                        c = (char) i5;
                        charArray[i3] = c;
                        i3++;
                    }
                }
            }
            i2 = i4;
            charArray[i3] = c;
            i3++;
        }
        return new String(charArray, 0, i3);
    }

    /* renamed from: a */
    private Object m2730a(Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        if (avf2.getParentScope() == null) {
            return org.mozilla1.javascript.ScriptRuntime.m7506a(lhVar, avf, (Object) avf2, objArr, "eval code", 1);
        }
        throw m2728a(lhVar, "EvalError", ScriptRuntime.getMessage1("msg.cant.call.indirect", "eval"), avf);
    }
}
