package org.mozilla1.javascript;

import java.lang.reflect.Method;

/* renamed from: a.KS */
/* compiled from: a */
public class InterfaceAdapter {
    private final Object dry;

    private InterfaceAdapter(ContextFactory iwVar, Class<?> cls) {
        this.dry = org.mozilla1.javascript.VMBridge.iEO.mo2804a(iwVar, (Class<?>[]) new Class[]{cls});
    }

    /* renamed from: a */
    static Object m6381a(org.mozilla1.javascript.Context lhVar, Class<?> cls, org.mozilla1.javascript.Callable mFVar) {
        InterfaceAdapter ks;
        boolean z = false;
        if (!cls.isInterface()) {
            throw new IllegalArgumentException();
        }
        org.mozilla1.javascript.Scriptable j = ScriptRuntime.getTopCallScope(lhVar);
        ClassCache k = ClassCache.m23644k(j);
        InterfaceAdapter ks2 = (InterfaceAdapter) k.mo14596am(cls);
        ContextFactory bwr = lhVar.bwr();
        if (ks2 == null) {
            Method[] methods = cls.getMethods();
            if (methods.length == 0) {
                throw org.mozilla1.javascript.Context.m35224a("msg.no.empty.interface.conversion", (Object) String.valueOf(mFVar), (Object) cls.getClass().getName());
            }
            Class[] parameterTypes = methods[0].getParameterTypes();
            int i = 1;
            loop0:
            while (true) {
                if (i != methods.length) {
                    Class[] parameterTypes2 = methods[i].getParameterTypes();
                    if (parameterTypes2.length != parameterTypes.length) {
                        break;
                    }
                    for (int i2 = 0; i2 != parameterTypes.length; i2++) {
                        if (parameterTypes2[i2] != parameterTypes[i2]) {
                            break loop0;
                        }
                    }
                    i++;
                } else {
                    z = true;
                    break;
                }
            }
            if (!z) {
                throw org.mozilla1.javascript.Context.m35224a("msg.no.function.interface.conversion", (Object) String.valueOf(mFVar), (Object) cls.getClass().getName());
            }
            ks = new InterfaceAdapter(bwr, cls);
            k.mo14601e(cls, ks);
        } else {
            ks = ks2;
        }
        return VMBridge.iEO.mo2805a(ks.dry, bwr, ks, mFVar, j);
    }

    /* renamed from: a */
    public Object mo3543a(ContextFactory iwVar, Object obj, org.mozilla1.javascript.Scriptable avf, Method method, Object[] objArr) {
        return iwVar.mo19793a((org.mozilla1.javascript.ContextAction) new C0727a(obj, avf, method, objArr));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Object mo3544a(org.mozilla1.javascript.Context lhVar, Object obj, org.mozilla1.javascript.Scriptable avf, Method method, Object[] objArr) {
        int length = objArr == null ? 0 : objArr.length;
        org.mozilla1.javascript.Callable mFVar = (Callable) obj;
        Object[] objArr2 = new Object[(length + 1)];
        objArr2[length] = method.getName();
        if (length != 0) {
            C1626Xo bwy = lhVar.bwy();
            for (int i = 0; i != length; i++) {
                objArr2[i] = bwy.mo6829a(lhVar, avf, objArr[i], (Class<?>) null);
            }
        }
        Object call = mFVar.call(lhVar, avf, avf, objArr2);
        Class<?> returnType = method.getReturnType();
        if (returnType == Void.TYPE) {
            return null;
        }
        return org.mozilla1.javascript.Context.jsToJava(call, returnType);
    }

    /* renamed from: a.KS$a */
    class C0727a implements ContextAction {
        private final /* synthetic */ Object[] axa;
        private final /* synthetic */ Object eyd;
        private final /* synthetic */ org.mozilla1.javascript.Scriptable eye;
        private final /* synthetic */ Method eyf;

        C0727a(Object obj, Scriptable avf, Method method, Object[] objArr) {
            this.eyd = obj;
            this.eye = avf;
            this.eyf = method;
            this.axa = objArr;
        }

        public Object run(Context lhVar) {
            return InterfaceAdapter.this.mo3544a(lhVar, this.eyd, this.eye, this.eyf, this.axa);
        }
    }
}
