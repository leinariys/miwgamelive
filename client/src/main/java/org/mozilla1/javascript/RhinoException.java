package org.mozilla1.javascript;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.Pu */
/* compiled from: a */
public abstract class RhinoException extends RuntimeException {
    Object interpreterStackInfo;
    int[] interpreterLineData;
    private int columnNumber;
    private String dOJ;
    private String eKW;
    private int lineNumber;

    RhinoException() {
        Evaluator bwC = org.mozilla1.javascript.Context.bwC();
        if (bwC != null) {
            bwC.mo5925a(this);
        }
    }

    RhinoException(String str) {
        super(str);
        Evaluator bwC = org.mozilla1.javascript.Context.bwC();
        if (bwC != null) {
            bwC.mo5925a(this);
        }
    }

    public final String getMessage() {
        String details = details();
        if (this.dOJ == null || this.lineNumber <= 0) {
            return details;
        }
        StringBuffer stringBuffer = new StringBuffer(details);
        stringBuffer.append(" (");
        if (this.dOJ != null) {
            stringBuffer.append(this.dOJ);
        }
        if (this.lineNumber > 0) {
            stringBuffer.append('#');
            stringBuffer.append(this.lineNumber);
        }
        stringBuffer.append(')');
        return stringBuffer.toString();
    }

    public String details() {
        return super.getMessage();
    }

    public final String sourceName() {
        return this.dOJ;
    }

    public final void initSourceName(String str) {
        if (str == null) {
            throw new IllegalArgumentException();
        } else if (this.dOJ != null) {
            throw new IllegalStateException();
        } else {
            this.dOJ = str;
        }
    }

    public final int lineNumber() {
        return this.lineNumber;
    }

    public final void initLineNumber(int i) {
        if (i <= 0) {
            throw new IllegalArgumentException(String.valueOf(i));
        } else if (this.lineNumber > 0) {
            throw new IllegalStateException();
        } else {
            this.lineNumber = i;
        }
    }

    public final int columnNumber() {
        return this.columnNumber;
    }

    public final void initColumnNumber(int i) {
        if (i <= 0) {
            throw new IllegalArgumentException(String.valueOf(i));
        } else if (this.columnNumber > 0) {
            throw new IllegalStateException();
        } else {
            this.columnNumber = i;
        }
    }

    public final String lineSource() {
        return this.eKW;
    }

    public final void initLineSource(String str) {
        if (str == null) {
            throw new IllegalArgumentException();
        } else if (this.eKW != null) {
            throw new IllegalStateException();
        } else {
            this.eKW = str;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo4868a(String str, int i, String str2, int i2) {
        if (i == -1) {
            i = 0;
        }
        if (str != null) {
            initSourceName(str);
        }
        if (i != 0) {
            initLineNumber(i);
        }
        if (str2 != null) {
            initLineSource(str2);
        }
        if (i2 != 0) {
            initColumnNumber(i2);
        }
    }

    private String bHn() {
        CharArrayWriter charArrayWriter = new CharArrayWriter();
        super.printStackTrace(new PrintWriter(charArrayWriter));
        String charArrayWriter2 = charArrayWriter.toString();
        Evaluator bwC = org.mozilla1.javascript.Context.bwC();
        if (bwC != null) {
            return bwC.mo5923a(this, charArrayWriter2);
        }
        return null;
    }

    public String bHo() {
        return mo4867a(new C1087a());
    }

    /* renamed from: a */
    public String mo4867a(FilenameFilter filenameFilter) {
        List arrayList;
        int i = 0;
        Evaluator bwC = Context.bwC();
        if (bwC != null) {
            arrayList = bwC.mo5927b(this);
        } else {
            arrayList = new ArrayList();
        }
        StringBuffer stringBuffer = new StringBuffer();
        String systemProperty = SecurityUtilities.getSystemProperty("line.separator");
        StackTraceElement[] stackTrace = getStackTrace();
        int i2 = 0;
        while (true) {
            int i3 = i;
            if (i2 >= stackTrace.length) {
                return stringBuffer.toString();
            }
            StackTraceElement stackTraceElement = stackTrace[i2];
            String fileName = stackTraceElement.getFileName();
            if (stackTraceElement.getLineNumber() > -1 && fileName != null && filenameFilter.accept((File) null, fileName)) {
                stringBuffer.append("\tat ");
                stringBuffer.append(stackTraceElement.getFileName());
                stringBuffer.append(':');
                stringBuffer.append(stackTraceElement.getLineNumber());
                stringBuffer.append(systemProperty);
                i = i3;
            } else if (arrayList == null || !"org.mozilla.javascript.Interpreter".equals(stackTraceElement.getClassName()) || !"interpretLoop".equals(stackTraceElement.getMethodName())) {
                i = i3;
            } else {
                i = i3 + 1;
                stringBuffer.append((String) arrayList.get(i3));
            }
            i2++;
        }
    }

    public void printStackTrace(PrintWriter printWriter) {
        if (this.interpreterStackInfo == null) {
            super.printStackTrace(printWriter);
        } else {
            printWriter.print(bHn());
        }
    }

    public void printStackTrace(PrintStream printStream) {
        if (this.interpreterStackInfo == null) {
            super.printStackTrace(printStream);
        } else {
            printStream.print(bHn());
        }
    }

    /* renamed from: a.Pu$a */
    class C1087a implements FilenameFilter {
        C1087a() {
        }

        public boolean accept(File file, String str) {
            return str.endsWith(".js");
        }
    }
}
