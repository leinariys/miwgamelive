package org.mozilla1.javascript;

import org.mozilla1.javascript.xml.XMLLib;
import org.mozilla1.javascript.xml.XMLObject;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/* renamed from: a.NH */
/* compiled from: a */
public class ScriptRuntime {
    public static final Class<?> BooleanClass = org.mozilla1.javascript.Kit.classOrNull("java.lang.Boolean");
    public static final Class<?> ByteClass = org.mozilla1.javascript.Kit.classOrNull("java.lang.Byte");
    public static final Class<?> CharacterClass = org.mozilla1.javascript.Kit.classOrNull("java.lang.Character");
    public static final Class<?> ClassClass = org.mozilla1.javascript.Kit.classOrNull("java.lang.Class");
    public static final Class<?> ContextClass = org.mozilla1.javascript.Kit.classOrNull("a.lh");
    public static final Class<?> ContextFactoryClass = org.mozilla1.javascript.Kit.classOrNull("a.iw");
    public static final Class<?> DateClass = org.mozilla1.javascript.Kit.classOrNull("java.util.Date");
    public static final Class<?> DoubleClass = org.mozilla1.javascript.Kit.classOrNull("java.lang.Double");
    public static final Class<?> FloatClass = org.mozilla1.javascript.Kit.classOrNull("java.lang.Float");
    public static final Class<?> FunctionClass = org.mozilla1.javascript.Kit.classOrNull("a.azg");
    public static final Class<?> IntegerClass = org.mozilla1.javascript.Kit.classOrNull("java.lang.Integer");
    public static final Class<?> LongClass = org.mozilla1.javascript.Kit.classOrNull("java.lang.Long");
    public static final double NaN = Double.longBitsToDouble(9221120237041090560L);
    public static final Double NaNobj = new Double(NaN);
    public static final Class<?> NumberClass = org.mozilla1.javascript.Kit.classOrNull("java.lang.Number");
    public static final Class<?> ObjectClass = org.mozilla1.javascript.Kit.classOrNull("java.lang.Object");
    public static final Class<Scriptable> ScriptableClass = Scriptable.class;
    public static final Class<?> ScriptableObjectClass = org.mozilla1.javascript.Kit.classOrNull("a.akn");
    public static final Class<?> ShortClass = org.mozilla1.javascript.Kit.classOrNull("java.lang.Short");
    public static final Class<?> StringClass = org.mozilla1.javascript.Kit.classOrNull("java.lang.String");
    public static final Object[] emptyArgs = new Object[0];
    public static final String[] emptyStrings = new String[0];
    public static final int iAA = 0;
    public static final int iAB = 1;
    public static final int iAC = 2;
    public static final int iAD = 3;
    public static final int iAE = 4;
    public static final int iAF = 5;
    public static final double negativeZero = Double.longBitsToDouble(Long.MIN_VALUE);
    private static final String[] iAw = {"RegExp", "org.mozilla.javascript.regexp.NativeRegExp", "Packages", "org.mozilla.javascript.NativeJavaTopPackage", "java", "org.mozilla.javascript.NativeJavaTopPackage", "javax", "org.mozilla.javascript.NativeJavaTopPackage", "org", "org.mozilla.javascript.NativeJavaTopPackage", "com", "org.mozilla.javascript.NativeJavaTopPackage", "edu", "org.mozilla.javascript.NativeJavaTopPackage", "net", "org.mozilla.javascript.NativeJavaTopPackage", "getClass", "org.mozilla.javascript.NativeJavaTopPackage", "JavaAdapter", "org.mozilla.javascript.JavaAdapter", "JavaImporter", "org.mozilla.javascript.ImporterTopLevel", "Continuation", "org.mozilla.javascript.continuations.Continuation", "XML", "(xml)", "XMLList", "(xml)", "Namespace", "(xml)", "QName", "(xml)"};
    private static final Object iAx = new Integer(0);
    private static final boolean iAy = true;
    private static final String iAz = "__default_namespace__";

    public ScriptRuntime() {
    }

    public static boolean isRhinoRuntimeType(Class<?> cls) {
        if (cls.isPrimitive()) {
            if (cls != Character.TYPE) {
                return true;
            }
            return false;
        } else if (cls == StringClass || cls == BooleanClass || NumberClass.isAssignableFrom(cls) || ScriptableClass.isAssignableFrom(cls)) {
            return true;
        } else {
            return false;
        }
    }

    /* renamed from: a */
    public static ScriptableObject m7484a(Context lhVar, ScriptableObject akn, boolean z) {
        if (akn == null) {
            akn = new NativeObject();
        }
        akn.associateValue(iAx, akn);
        new ClassCache().mo14597b(akn);
        org.mozilla1.javascript.BaseFunction.m20587a(akn, z);
        NativeObject.m8584a(akn, z);
        Scriptable v = ScriptableObject.m23593v(akn);
        ScriptableObject.m23594w(akn).setPrototype(v);
        if (akn.getPrototype() == null) {
            akn.setPrototype(v);
        }
        NativeError.m14617a(akn, z);
        NativeGlobal.init(lhVar, akn, z);
        NativeArray.m27402a((Scriptable) akn, z);
        if (lhVar.getOptimizationLevel() > 0) {
            NativeArray.m27424wi(200000);
        }
        NativeString.m25234a(akn, z);
        NativeBoolean.m25403a(akn, z);
        NativeNumber.m23467a(akn, z);
        NativeDate.m1765a((Scriptable) akn, z);
        NativeMath.m472a(akn, z);
        NativeWith.m26125a(akn, z);
        org.mozilla1.javascript.NativeCall.m27962a(akn, z);
        NativeScript.m29048a((Scriptable) akn, z);
        NativeIterator.m38640b(akn, z);
        boolean z2 = lhVar.hasFeature(6) && lhVar.bkM() != null;
        for (int i = 0; i != iAw.length; i += 2) {
            String str = iAw[i];
            String str2 = iAw[i + 1];
            if (z2 || !str2.equals("(xml)")) {
                if (z2 && str2.equals("(xml)")) {
                    str2 = lhVar.bkM().bjd();
                }
                new LazilyLoadedCtor(akn, str, str2, z);
            }
        }
        return akn;
    }

    /* renamed from: p */
    public static ScriptableObject m7604p(Scriptable avf) {
        return (ScriptableObject) ScriptableObject.m23581d(avf, iAx);
    }

    public static boolean isJSLineTerminator(int i) {
        if ((57296 & i) != 0) {
            return false;
        }
        if (i == 10 || i == 13 || i == 8232 || i == 8233) {
            return true;
        }
        return false;
    }

    public static Boolean wrapBoolean(boolean z) {
        return z ? Boolean.TRUE : Boolean.FALSE;
    }

    public static Integer wrapInt(int i) {
        return new Integer(i);
    }

    public static Number wrapNumber(double d) {
        if (d != d) {
            return NaNobj;
        }
        return new Double(d);
    }

    public static boolean toBoolean(Object obj) {
        Object obj2 = obj;
        while (!(obj2 instanceof Boolean)) {
            if (obj2 == null || obj2 == Undefined.instance) {
                return false;
            }
            if (obj2 instanceof String) {
                return ((String) obj2).length() != 0;
            }
            if (obj2 instanceof Number) {
                double doubleValue = ((Number) obj2).doubleValue();
                return doubleValue == doubleValue && doubleValue != NaN;
            } else if (!(obj2 instanceof Scriptable)) {
                m7534aU(obj2);
                return true;
            } else if ((obj2 instanceof ScriptableObject) && ((ScriptableObject) obj2).dwc()) {
                return false;
            } else {
                if (Context.bwA().bwE()) {
                    return true;
                }
                obj2 = ((Scriptable) obj2).getDefaultValue(BooleanClass);
                if (obj2 instanceof Scriptable) {
                    throw m7614y("msg.primitive.expected", obj2);
                }
            }
        }
        return ((Boolean) obj2).booleanValue();
    }

    public static double toNumber(Object obj) {
        Object obj2 = obj;
        while (!(obj2 instanceof Number)) {
            if (obj2 == null) {
                return NaN;
            }
            if (obj2 == Undefined.instance) {
                return NaN;
            }
            if (obj2 instanceof String) {
                return toNumber((String) obj2);
            }
            if (obj2 instanceof Boolean) {
                return ((Boolean) obj2).booleanValue() ? 1.0d : 0.0d;
            } else if (obj2 instanceof Scriptable) {
                obj2 = ((Scriptable) obj2).getDefaultValue(NumberClass);
                if (obj2 instanceof Scriptable) {
                    throw m7614y("msg.primitive.expected", obj2);
                }
            } else {
                m7534aU(obj2);
                return NaN;
            }
        }
        return ((Number) obj2).doubleValue();
    }

    public static double toNumber(Object[] objArr, int i) {
        return i < objArr.length ? toNumber(objArr[i]) : NaN;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x012d, code lost:
        r11 = r10;
     */
    /* renamed from: f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static double m7577f(java.lang.String r18, int r19, int r20) {
        /*
            r4 = 57
            r3 = 97
            r2 = 65
            int r8 = r18.length()
            r5 = 10
            r0 = r20
            if (r0 >= r5) goto L_0x0132
            int r4 = r20 + 48
            int r4 = r4 + -1
            char r4 = (char) r4
            r5 = r4
        L_0x0016:
            r4 = 10
            r0 = r20
            if (r0 <= r4) goto L_0x0026
            int r2 = r20 + 97
            int r2 = r2 + -10
            char r3 = (char) r2
            int r2 = r20 + 65
            int r2 = r2 + -10
            char r2 = (char) r2
        L_0x0026:
            r6 = 0
            r15 = r19
        L_0x002a:
            if (r15 < r8) goto L_0x0033
        L_0x002c:
            r0 = r19
            if (r0 != r15) goto L_0x0061
            double r6 = NaN
        L_0x0032:
            return r6
        L_0x0033:
            r0 = r18
            char r4 = r0.charAt(r15)
            r9 = 48
            if (r9 > r4) goto L_0x004b
            if (r4 > r5) goto L_0x004b
            int r4 = r4 + -48
        L_0x0041:
            r0 = r20
            double r10 = (double) r0
            double r6 = r6 * r10
            double r10 = (double) r4
            double r6 = r6 + r10
            int r4 = r15 + 1
            r15 = r4
            goto L_0x002a
        L_0x004b:
            r9 = 97
            if (r9 > r4) goto L_0x0056
            if (r4 >= r3) goto L_0x0056
            int r4 = r4 + -97
            int r4 = r4 + 10
            goto L_0x0041
        L_0x0056:
            r9 = 65
            if (r9 > r4) goto L_0x002c
            if (r4 >= r2) goto L_0x002c
            int r4 = r4 + -65
            int r4 = r4 + 10
            goto L_0x0041
        L_0x0061:
            r2 = 4845873199050653696(0x4340000000000000, double:9.007199254740992E15)
            int r2 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r2 < 0) goto L_0x0032
            r2 = 10
            r0 = r20
            if (r0 != r2) goto L_0x0082
            r0 = r18
            r1 = r19
            java.lang.String r2 = r0.substring(r1, r15)     // Catch:{ NumberFormatException -> 0x007e }
            java.lang.Double r2 = java.lang.Double.valueOf(r2)     // Catch:{ NumberFormatException -> 0x007e }
            double r6 = r2.doubleValue()     // Catch:{ NumberFormatException -> 0x007e }
            goto L_0x0032
        L_0x007e:
            r2 = move-exception
            double r6 = NaN
            goto L_0x0032
        L_0x0082:
            r2 = 2
            r0 = r20
            if (r0 == r2) goto L_0x009e
            r2 = 4
            r0 = r20
            if (r0 == r2) goto L_0x009e
            r2 = 8
            r0 = r20
            if (r0 == r2) goto L_0x009e
            r2 = 16
            r0 = r20
            if (r0 == r2) goto L_0x009e
            r2 = 32
            r0 = r20
            if (r0 != r2) goto L_0x0032
        L_0x009e:
            r9 = 1
            r8 = 0
            r3 = 0
            r14 = 53
            r4 = 0
            r10 = 0
            r2 = 0
            r12 = r2
            r13 = r10
            r11 = r19
        L_0x00ab:
            r2 = 1
            if (r9 != r2) goto L_0x0130
            if (r11 != r15) goto L_0x00b9
            switch(r3) {
                case 0: goto L_0x00b5;
                case 1: goto L_0x0032;
                case 2: goto L_0x0032;
                case 3: goto L_0x0116;
                case 4: goto L_0x0121;
                default: goto L_0x00b3;
            }
        L_0x00b3:
            goto L_0x0032
        L_0x00b5:
            r6 = 0
            goto L_0x0032
        L_0x00b9:
            int r10 = r11 + 1
            r0 = r18
            char r2 = r0.charAt(r11)
            r8 = 48
            if (r8 > r2) goto L_0x00dc
            r8 = 57
            if (r2 > r8) goto L_0x00dc
            int r2 = r2 + -48
        L_0x00cb:
            r8 = r2
            r9 = r20
        L_0x00ce:
            int r9 = r9 >> 1
            r2 = r8 & r9
            if (r2 == 0) goto L_0x00ea
            r2 = 1
        L_0x00d5:
            switch(r3) {
                case 0: goto L_0x00ec;
                case 1: goto L_0x00f6;
                case 2: goto L_0x0108;
                case 3: goto L_0x010e;
                case 4: goto L_0x012b;
                default: goto L_0x00d8;
            }
        L_0x00d8:
            r2 = r3
        L_0x00d9:
            r3 = r2
            r11 = r10
            goto L_0x00ab
        L_0x00dc:
            r8 = 97
            if (r8 > r2) goto L_0x00e7
            r8 = 122(0x7a, float:1.71E-43)
            if (r2 > r8) goto L_0x00e7
            int r2 = r2 + -87
            goto L_0x00cb
        L_0x00e7:
            int r2 = r2 + -55
            goto L_0x00cb
        L_0x00ea:
            r2 = 0
            goto L_0x00d5
        L_0x00ec:
            if (r2 == 0) goto L_0x012d
            int r2 = r14 + -1
            r6 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r3 = 1
            r14 = r2
            r11 = r10
            goto L_0x00ab
        L_0x00f6:
            r16 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r6 = r6 * r16
            if (r2 == 0) goto L_0x0100
            r16 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r6 = r6 + r16
        L_0x0100:
            int r14 = r14 + -1
            if (r14 != 0) goto L_0x012d
            r3 = 2
            r13 = r2
            r11 = r10
            goto L_0x00ab
        L_0x0108:
            r4 = 4611686018427387904(0x4000000000000000, double:2.0)
            r3 = 3
            r12 = r2
            r11 = r10
            goto L_0x00ab
        L_0x010e:
            if (r2 == 0) goto L_0x012b
            r2 = 4
        L_0x0111:
            r16 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r4 = r4 * r16
            goto L_0x00d9
        L_0x0116:
            r2 = r12 & r13
            if (r2 == 0) goto L_0x0129
            r2 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r2 = r2 + r6
        L_0x011d:
            double r6 = r2 * r4
            goto L_0x0032
        L_0x0121:
            if (r12 == 0) goto L_0x0126
            r2 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r6 = r6 + r2
        L_0x0126:
            double r6 = r6 * r4
            goto L_0x0032
        L_0x0129:
            r2 = r6
            goto L_0x011d
        L_0x012b:
            r2 = r3
            goto L_0x0111
        L_0x012d:
            r11 = r10
            goto L_0x00ab
        L_0x0130:
            r10 = r11
            goto L_0x00ce
        L_0x0132:
            r5 = r4
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C0903NH.m7577f(java.lang.String, int, int):double");
    }

    public static double toNumber(String str) {
        char charAt;
        char charAt2;
        char charAt3;
        int length = str.length();
        int i = 0;
        while (i != length) {
            char charAt4 = str.charAt(i);
            if (!Character.isWhitespace(charAt4)) {
                if (charAt4 == '0') {
                    if (i + 2 < length && ((charAt3 = str.charAt(i + 1)) == 'x' || charAt3 == 'X')) {
                        return m7577f(str, i + 2, 16);
                    }
                } else if ((charAt4 == '+' || charAt4 == '-') && i + 3 < length && str.charAt(i + 1) == '0' && ((charAt2 = str.charAt(i + 2)) == 'x' || charAt2 == 'X')) {
                    double f = m7577f(str, i + 3, 16);
                    if (charAt4 == '-') {
                        return -f;
                    }
                    return f;
                }
                while (true) {
                    length--;
                    charAt = str.charAt(length);
                    if (!Character.isWhitespace(charAt)) {
                        break;
                    }
                }
                if (charAt == 'y') {
                    if (charAt4 == '+' || charAt4 == '-') {
                        i++;
                    }
                    if (i + 7 != length || !str.regionMatches(i, "Infinity", 0, 8)) {
                        return NaN;
                    }
                    if (charAt4 == '-') {
                        return Double.NEGATIVE_INFINITY;
                    }
                    return Double.POSITIVE_INFINITY;
                }
                String substring = str.substring(i, length + 1);
                for (int length2 = substring.length() - 1; length2 >= 0; length2--) {
                    char charAt5 = substring.charAt(length2);
                    if (('0' > charAt5 || charAt5 > '9') && charAt5 != '.' && charAt5 != 'e' && charAt5 != 'E' && charAt5 != '+' && charAt5 != '-') {
                        return NaN;
                    }
                }
                try {
                    return Double.valueOf(substring).doubleValue();
                } catch (NumberFormatException e) {
                    return NaN;
                }
            } else {
                i++;
            }
        }
        return NaN;
    }

    public static Object[] padArguments(Object[] objArr, int i) {
        if (i < objArr.length) {
            return objArr;
        }
        Object[] objArr2 = new Object[i];
        int i2 = 0;
        while (i2 < objArr.length) {
            objArr2[i2] = objArr[i2];
            i2++;
        }
        while (i2 < i) {
            objArr2[i2] = Undefined.instance;
            i2++;
        }
        return objArr2;
    }

    public static String escapeString(String str) {
        return escapeString(str, '\"');
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: char} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String escapeString(java.lang.String r9, char r10) {
        /*
            r4 = 32
            r5 = 92
            r0 = 34
            if (r10 == r0) goto L_0x000f
            r0 = 39
            if (r10 == r0) goto L_0x000f
            p001a.C1520WN.codeBug()
        L_0x000f:
            r0 = 0
            r1 = 0
            int r6 = r9.length()
            r2 = r1
        L_0x0016:
            if (r2 != r6) goto L_0x001b
            if (r0 != 0) goto L_0x00a0
        L_0x001a:
            return r9
        L_0x001b:
            char r7 = r9.charAt(r2)
            if (r4 > r7) goto L_0x0033
            r1 = 126(0x7e, float:1.77E-43)
            if (r7 > r1) goto L_0x0033
            if (r7 == r10) goto L_0x0033
            if (r7 == r5) goto L_0x0033
            if (r0 == 0) goto L_0x002f
            char r1 = (char) r7
            r0.append(r1)
        L_0x002f:
            int r1 = r2 + 1
            r2 = r1
            goto L_0x0016
        L_0x0033:
            if (r0 != 0) goto L_0x00a6
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            int r0 = r6 + 3
            r1.<init>(r0)
            r1.append(r9)
            r1.setLength(r2)
        L_0x0042:
            r0 = -1
            switch(r7) {
                case 8: goto L_0x0051;
                case 9: goto L_0x005d;
                case 10: goto L_0x0057;
                case 11: goto L_0x0060;
                case 12: goto L_0x0054;
                case 13: goto L_0x005a;
                case 32: goto L_0x0063;
                case 92: goto L_0x0065;
                default: goto L_0x0046;
            }
        L_0x0046:
            if (r0 < 0) goto L_0x0067
            r1.append(r5)
            char r0 = (char) r0
            r1.append(r0)
            r0 = r1
            goto L_0x002f
        L_0x0051:
            r0 = 98
            goto L_0x0046
        L_0x0054:
            r0 = 102(0x66, float:1.43E-43)
            goto L_0x0046
        L_0x0057:
            r0 = 110(0x6e, float:1.54E-43)
            goto L_0x0046
        L_0x005a:
            r0 = 114(0x72, float:1.6E-43)
            goto L_0x0046
        L_0x005d:
            r0 = 116(0x74, float:1.63E-43)
            goto L_0x0046
        L_0x0060:
            r0 = 118(0x76, float:1.65E-43)
            goto L_0x0046
        L_0x0063:
            r0 = r4
            goto L_0x0046
        L_0x0065:
            r0 = r5
            goto L_0x0046
        L_0x0067:
            if (r7 != r10) goto L_0x0071
            r1.append(r5)
            r1.append(r10)
            r0 = r1
            goto L_0x002f
        L_0x0071:
            r0 = 256(0x100, float:3.59E-43)
            if (r7 >= r0) goto L_0x0084
            java.lang.String r0 = "\\x"
            r1.append(r0)
            r0 = 2
        L_0x007b:
            int r0 = r0 + -1
            int r0 = r0 * 4
            r3 = r0
        L_0x0080:
            if (r3 >= 0) goto L_0x008b
            r0 = r1
            goto L_0x002f
        L_0x0084:
            java.lang.String r0 = "\\u"
            r1.append(r0)
            r0 = 4
            goto L_0x007b
        L_0x008b:
            int r0 = r7 >> r3
            r0 = r0 & 15
            r8 = 10
            if (r0 >= r8) goto L_0x009d
            int r0 = r0 + 48
        L_0x0095:
            char r0 = (char) r0
            r1.append(r0)
            int r0 = r3 + -4
            r3 = r0
            goto L_0x0080
        L_0x009d:
            int r0 = r0 + 87
            goto L_0x0095
        L_0x00a0:
            java.lang.String r9 = r0.toString()
            goto L_0x001a
        L_0x00a6:
            r1 = r0
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C0903NH.escapeString(java.lang.String, char):java.lang.String");
    }

    /* renamed from: nj */
    static boolean m7595nj(String str) {
        int length = str.length();
        if (length == 0 || !Character.isJavaIdentifierStart(str.charAt(0))) {
            return false;
        }
        for (int i = 1; i != length; i++) {
            if (!Character.isJavaIdentifierPart(str.charAt(i))) {
                return false;
            }
        }
        if (!org.mozilla1.javascript.TokenStream.m23668m(str)) {
            return true;
        }
        return false;
    }

    public static String toString(Object obj) {
        Object obj2 = obj;
        while (obj2 != null) {
            if (obj2 == Undefined.instance) {
                return "undefined";
            }
            if (obj2 instanceof String) {
                return (String) obj2;
            }
            if (obj2 instanceof Number) {
                return numberToString(((Number) obj2).doubleValue(), 10);
            }
            if (!(obj2 instanceof Scriptable)) {
                return obj2.toString();
            }
            obj2 = ((Scriptable) obj2).getDefaultValue(StringClass);
            if (obj2 instanceof Scriptable) {
                throw m7614y("msg.primitive.expected", obj2);
            }
        }
        return "null";
    }

    /* renamed from: q */
    static String m7606q(Scriptable avf) {
        return "[object " + avf.getClassName() + ']';
    }

    public static String toString(Object[] objArr, int i) {
        return i < objArr.length ? toString(objArr[i]) : "undefined";
    }

    public static String toString(double d) {
        return numberToString(d, 10);
    }

    public static String numberToString(double d, int i) {
        if (d != d) {
            return "NaN";
        }
        if (d == Double.POSITIVE_INFINITY) {
            return "Infinity";
        }
        if (d == Double.NEGATIVE_INFINITY) {
            return "-Infinity";
        }
        if (d == NaN) {
            return "0";
        }
        if (i < 2 || i > 36) {
            throw Context.m35246q("msg.bad.radix", Integer.toString(i));
        } else if (i != 10) {
            return DToA.JS_dtobasestr(i, d);
        } else {
            StringBuffer stringBuffer = new StringBuffer();
            DToA.JS_dtostr(stringBuffer, 0, 0, d);
            return stringBuffer.toString();
        }
    }

    /* renamed from: d */
    static String m7571d(Context lhVar, Scriptable avf, Object obj) {
        if (obj == null) {
            return "null";
        }
        if (obj == Undefined.instance) {
            return "undefined";
        }
        if (obj instanceof String) {
            String escapeString = escapeString((String) obj);
            StringBuffer stringBuffer = new StringBuffer(escapeString.length() + 2);
            stringBuffer.append('\"');
            stringBuffer.append(escapeString);
            stringBuffer.append('\"');
            return stringBuffer.toString();
        } else if (obj instanceof Number) {
            double doubleValue = ((Number) obj).doubleValue();
            if (doubleValue != NaN || 1.0d / doubleValue >= NaN) {
                return toString(doubleValue);
            }
            return "-0";
        } else if (obj instanceof Boolean) {
            return toString(obj);
        } else {
            if (obj instanceof Scriptable) {
                Scriptable avf2 = (Scriptable) obj;
                if (ScriptableObject.m23589k(avf2, "toSource")) {
                    Object j = ScriptableObject.m23587j(avf2, "toSource");
                    if (j instanceof Function) {
                        return toString(((Function) j).call(lhVar, avf, avf2, emptyArgs));
                    }
                }
                return toString(obj);
            }
            m7534aU(obj);
            return obj.toString();
        }
    }

    /* renamed from: f */
    static String m7579f(Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        boolean has;
        boolean z;
        Object obj;
        if (lhVar.elm == null) {
            z = true;
            lhVar.elm = new org.mozilla1.javascript.ObjToIntMap(31);
            has = false;
        } else {
            has = lhVar.elm.has(avf2);
            z = false;
        }
        StringBuffer stringBuffer = new StringBuffer(128);
        if (z) {
            stringBuffer.append("(");
        }
        stringBuffer.append('{');
        if (!has) {
            try {
                lhVar.elm.intern(avf2);
                Object[] ids = avf2.getIds();
                for (int i = 0; i < ids.length; i++) {
                    Object obj2 = ids[i];
                    if (obj2 instanceof Integer) {
                        int intValue = ((Integer) obj2).intValue();
                        obj = avf2.get(intValue, avf2);
                        if (obj != Scriptable.NOT_FOUND) {
                            if (i > 0) {
                                stringBuffer.append(", ");
                            }
                            stringBuffer.append(intValue);
                            stringBuffer.append(':');
                            stringBuffer.append(m7571d(lhVar, avf, obj));
                        }
                    } else {
                        String str = (String) obj2;
                        Object obj3 = avf2.get(str, avf2);
                        if (obj3 != Scriptable.NOT_FOUND) {
                            if (i > 0) {
                                stringBuffer.append(", ");
                            }
                            if (m7595nj(str)) {
                                stringBuffer.append(str);
                                obj = obj3;
                            } else {
                                stringBuffer.append('\'');
                                stringBuffer.append(escapeString(str, '\''));
                                stringBuffer.append('\'');
                                obj = obj3;
                            }
                            stringBuffer.append(':');
                            stringBuffer.append(m7571d(lhVar, avf, obj));
                        }
                    }
                }
            } catch (Throwable th) {
                if (z) {
                    lhVar.elm = null;
                }
                throw th;
            }
        }
        if (z) {
            lhVar.elm = null;
        }
        stringBuffer.append('}');
        if (z) {
            stringBuffer.append(')');
        }
        return stringBuffer.toString();
    }

    /* renamed from: c */
    public static Scriptable m7554c(Scriptable avf, Object obj) {
        if (obj instanceof Scriptable) {
            return (Scriptable) obj;
        }
        return m7572e(Context.bwA(), avf, obj);
    }

    /* renamed from: a */
    public static Scriptable m7477a(Context lhVar, Object obj) {
        if (obj instanceof Scriptable) {
            return (Scriptable) obj;
        }
        if (obj == null || obj == Undefined.instance) {
            return null;
        }
        return m7572e(lhVar, getTopCallScope(lhVar), obj);
    }

    /* renamed from: b */
    public static Scriptable m7538b(Scriptable avf, Object obj, Class<?> cls) {
        if (obj instanceof Scriptable) {
            return (Scriptable) obj;
        }
        return m7572e(Context.bwA(), avf, obj);
    }

    /* renamed from: e */
    public static Scriptable m7572e(Context lhVar, Scriptable avf, Object obj) {
        String str;
        if (obj instanceof Scriptable) {
            return (Scriptable) obj;
        }
        if (obj == null) {
            throw m7600no("msg.null.to.object");
        } else if (obj == Undefined.instance) {
            throw m7600no("msg.undef.to.object");
        } else {
            if (obj instanceof String) {
                str = "String";
            } else if (obj instanceof Number) {
                str = "Number";
            } else {
                str = obj instanceof Boolean ? "Boolean" : null;
            }
            if (str != null) {
                return m7539b(lhVar, ScriptableObject.m23595x(avf), str, new Object[]{obj});
            }
            Object a = lhVar.bwy().mo6829a(lhVar, avf, obj, (Class<?>) null);
            if (a instanceof Scriptable) {
                return (Scriptable) a;
            }
            throw m7614y("msg.invalid.type", obj);
        }
    }

    /* renamed from: d */
    public static Scriptable m7566d(Context lhVar, Scriptable avf, Object obj, Class<?> cls) {
        return m7572e(lhVar, avf, obj);
    }

    /* renamed from: a */
    public static Object m7508a(Context lhVar, Object obj, Object obj2, Object[] objArr, Scriptable avf) {
        if (!(obj instanceof Function)) {
            throw notFunctionError(toString(obj));
        }
        Function azg = (Function) obj;
        Scriptable a = m7477a(lhVar, obj2);
        if (a != null) {
            return azg.call(lhVar, avf, a, objArr);
        }
        throw undefCallError(a, "function");
    }

    /* renamed from: b */
    public static Scriptable m7539b(Context lhVar, Scriptable avf, String str, Object[] objArr) {
        Scriptable x = ScriptableObject.m23595x(avf);
        Function a = m7485a(lhVar, x, str);
        if (objArr == null) {
            objArr = emptyArgs;
        }
        return a.construct(lhVar, x, objArr);
    }

    public static double toInteger(Object obj) {
        return toInteger(toNumber(obj));
    }

    public static double toInteger(double d) {
        if (d != d) {
            return NaN;
        }
        if (d == NaN || d == Double.POSITIVE_INFINITY || d == Double.NEGATIVE_INFINITY) {
            return d;
        }
        if (d > NaN) {
            return Math.floor(d);
        }
        return Math.ceil(d);
    }

    public static double toInteger(Object[] objArr, int i) {
        return i < objArr.length ? toInteger(objArr[i]) : NaN;
    }

    public static int toInt32(Object obj) {
        if (obj instanceof Integer) {
            return ((Integer) obj).intValue();
        }
        return toInt32(toNumber(obj));
    }

    public static int toInt32(Object[] objArr, int i) {
        if (i < objArr.length) {
            return toInt32(objArr[i]);
        }
        return 0;
    }

    public static int toInt32(double d) {
        int i = (int) d;
        if (((double) i) == d) {
            return i;
        }
        if (d != d || d == Double.POSITIVE_INFINITY || d == Double.NEGATIVE_INFINITY) {
            return 0;
        }
        return (int) ((long) Math.IEEEremainder(d >= NaN ? Math.floor(d) : Math.ceil(d), 4.294967296E9d));
    }

    public static long toUint32(double d) {
        long j = (long) d;
        if (((double) j) == d) {
            return j & 4294967295L;
        }
        if (d != d || d == Double.POSITIVE_INFINITY || d == Double.NEGATIVE_INFINITY) {
            return 0;
        }
        return ((long) Math.IEEEremainder(d >= NaN ? Math.floor(d) : Math.ceil(d), 4.294967296E9d)) & 4294967295L;
    }

    public static long toUint32(Object obj) {
        return toUint32(toNumber(obj));
    }

    public static char toUint16(Object obj) {
        double number = toNumber(obj);
        int i = (int) number;
        if (((double) i) == number) {
            return (char) i;
        }
        if (number != number || number == Double.POSITIVE_INFINITY || number == Double.NEGATIVE_INFINITY) {
            return 0;
        }
        return (char) ((int) Math.IEEEremainder(number >= NaN ? Math.floor(number) : Math.ceil(number), (double) C0677Jd.dij));
    }

    /* renamed from: b */
    public static Object m7544b(Object obj, Context lhVar) {
        Scriptable avf = lhVar.elk;
        if (avf == null) {
            avf = getTopCallScope(lhVar);
        }
        Object defaultXmlNamespace = currentXMLLib(lhVar).toDefaultXmlNamespace(lhVar, obj);
        if (!avf.has(iAz, avf)) {
            ScriptableObject.m23567a(avf, iAz, defaultXmlNamespace, 6);
        } else {
            avf.put(iAz, avf, defaultXmlNamespace);
        }
        return Undefined.instance;
    }

    /* renamed from: g */
    public static Object m7582g(Context lhVar) {
        Scriptable avf = lhVar.elk;
        if (avf == null) {
            avf = getTopCallScope(lhVar);
        }
        while (true) {
            Scriptable parentScope = avf.getParentScope();
            if (parentScope == null) {
                Object j = ScriptableObject.m23587j(avf, iAz);
                if (j == Scriptable.NOT_FOUND) {
                    return null;
                }
                return j;
            }
            Object obj = avf.get(iAz, avf);
            if (obj != Scriptable.NOT_FOUND) {
                return obj;
            }
            avf = parentScope;
        }
    }

    /* renamed from: e */
    public static Object m7574e(Scriptable avf, String str) {
        return ScriptableObject.m23587j(ScriptableObject.m23595x(avf), str);
    }

    /* renamed from: a */
    static Function m7485a(Context lhVar, Scriptable avf, String str) {
        Object j = ScriptableObject.m23587j(avf, str);
        if (j instanceof Function) {
            return (Function) j;
        }
        if (j == Scriptable.NOT_FOUND) {
            throw Context.m35246q("msg.ctor.not.found", str);
        }
        throw Context.m35246q("msg.not.ctor", str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0044, code lost:
        if (r2 <= r1) goto L_0x0046;
     */
    /* renamed from: nk */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static long m7596nk(java.lang.String r9) {
        /*
            r8 = 9
            r7 = -214748364(0xfffffffff3333334, float:-1.4197688E31)
            r2 = 1
            r1 = 0
            int r6 = r9.length()
            if (r6 <= 0) goto L_0x0060
            char r0 = r9.charAt(r1)
            r3 = 45
            if (r0 != r3) goto L_0x0063
            if (r6 <= r2) goto L_0x0063
            char r0 = r9.charAt(r2)
            r5 = r2
            r3 = r2
        L_0x001d:
            int r2 = r0 + -48
            if (r2 < 0) goto L_0x0060
            if (r2 > r8) goto L_0x0060
            if (r5 == 0) goto L_0x0050
            r0 = 11
        L_0x0027:
            if (r6 > r0) goto L_0x0060
            int r0 = -r2
            int r3 = r3 + 1
            if (r0 == 0) goto L_0x003a
        L_0x002e:
            if (r3 == r6) goto L_0x003a
            char r2 = r9.charAt(r3)
            int r2 = r2 + -48
            if (r2 < 0) goto L_0x003a
            if (r2 <= r8) goto L_0x0053
        L_0x003a:
            if (r3 != r6) goto L_0x0060
            if (r1 > r7) goto L_0x0046
            if (r1 != r7) goto L_0x0060
            if (r5 == 0) goto L_0x005c
            r1 = 8
        L_0x0044:
            if (r2 > r1) goto L_0x0060
        L_0x0046:
            r2 = 4294967295(0xffffffff, double:2.1219957905E-314)
            if (r5 == 0) goto L_0x005e
        L_0x004d:
            long r0 = (long) r0
            long r0 = r0 & r2
        L_0x004f:
            return r0
        L_0x0050:
            r0 = 10
            goto L_0x0027
        L_0x0053:
            int r1 = r0 * 10
            int r4 = r1 - r2
            int r3 = r3 + 1
            r1 = r0
            r0 = r4
            goto L_0x002e
        L_0x005c:
            r1 = 7
            goto L_0x0044
        L_0x005e:
            int r0 = -r0
            goto L_0x004d
        L_0x0060:
            r0 = -1
            goto L_0x004f
        L_0x0063:
            r5 = r1
            r3 = r1
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C0903NH.m7596nk(java.lang.String):long");
    }

    public static long testUint32String(String str) {
        long j = 0;
        int length = str.length();
        if (1 > length || length > 10) {
            return -1;
        }
        int charAt = str.charAt(0) - '0';
        if (charAt == 0) {
            if (length != 1) {
                j = -1;
            }
            return j;
        } else if (1 > charAt || charAt > 9) {
            return -1;
        } else {
            long j2 = (long) charAt;
            for (int i = 1; i != length; i++) {
                int charAt2 = str.charAt(i) - '0';
                if (charAt2 < 0 || charAt2 > 9) {
                    return -1;
                }
                j2 = (j2 * 10) + ((long) charAt2);
            }
            if ((j2 >>> 32) == 0) {
                return j2;
            }
            return -1;
        }
    }

    /* renamed from: nl */
    static Object m7597nl(String str) {
        long nk = m7596nk(str);
        if (nk >= 0) {
            return new Integer((int) nk);
        }
        return str;
    }

    /* renamed from: ad */
    static Object m7537ad(double d) {
        int i = (int) d;
        if (((double) i) == d) {
            return new Integer(i);
        }
        return toString(d);
    }

    /* renamed from: b */
    static String m7547b(Context lhVar, Object obj) {
        String nh;
        if (obj instanceof Number) {
            double doubleValue = ((Number) obj).doubleValue();
            int i = (int) doubleValue;
            if (((double) i) != doubleValue) {
                return toString(obj);
            }
            m7560c(lhVar, i);
            return null;
        }
        if (obj instanceof String) {
            nh = (String) obj;
        } else {
            nh = toString(obj);
        }
        long nk = m7596nk(nh);
        if (nk < 0) {
            return nh;
        }
        m7560c(lhVar, (int) nk);
        return null;
    }

    /* renamed from: a */
    public static Object m7515a(Object obj, Object obj2, Context lhVar) {
        Scriptable a = m7477a(lhVar, obj);
        if (a != null) {
            return m7493a(a, obj2, lhVar);
        }
        throw undefReadError(obj, obj2);
    }

    /* renamed from: a */
    public static Object m7493a(Scriptable avf, Object obj, Context lhVar) {
        Object j;
        if (avf instanceof org.mozilla1.javascript.xml.XMLObject) {
            return ((org.mozilla1.javascript.xml.XMLObject) avf).ecmaGet(lhVar, obj);
        }
        String b = m7547b(lhVar, obj);
        if (b == null) {
            j = ScriptableObject.m23575b(avf, m7602o(lhVar));
        } else {
            j = ScriptableObject.m23587j(avf, b);
        }
        if (j == Scriptable.NOT_FOUND) {
            return Undefined.instance;
        }
        return j;
    }

    /* renamed from: a */
    public static Object m7518a(Object obj, String str, Context lhVar) {
        Scriptable a = m7477a(lhVar, obj);
        if (a != null) {
            return m7499a(a, str, lhVar);
        }
        throw undefReadError(obj, str);
    }

    /* renamed from: a */
    public static Object m7499a(Scriptable avf, String str, Context lhVar) {
        if (avf instanceof org.mozilla1.javascript.xml.XMLObject) {
            return ((org.mozilla1.javascript.xml.XMLObject) avf).ecmaGet(lhVar, str);
        }
        Object j = ScriptableObject.m23587j(avf, str);
        if (j != Scriptable.NOT_FOUND) {
            return j;
        }
        if (lhVar.hasFeature(11)) {
            Context.reportWarning(getMessage1("msg.ref.undefined.prop", str));
        }
        return Undefined.instance;
    }

    /* renamed from: b */
    public static Object m7546b(Object obj, String str, Context lhVar) {
        Scriptable a = m7477a(lhVar, obj);
        if (a == null) {
            throw undefReadError(obj, str);
        }
        if (obj instanceof org.mozilla1.javascript.xml.XMLObject) {
            m7499a(a, str, lhVar);
        }
        Object j = ScriptableObject.m23587j(a, str);
        if (j == Scriptable.NOT_FOUND) {
            return Undefined.instance;
        }
        return j;
    }

    /* renamed from: a */
    public static Object m7511a(Object obj, double d, Context lhVar) {
        Scriptable a = m7477a(lhVar, obj);
        if (a == null) {
            throw undefReadError(obj, toString(d));
        }
        int i = (int) d;
        if (((double) i) == d) {
            return m7491a(a, i, lhVar);
        }
        return m7499a(a, toString(d), lhVar);
    }

    /* renamed from: a */
    public static Object m7491a(Scriptable avf, int i, Context lhVar) {
        if (avf instanceof org.mozilla1.javascript.xml.XMLObject) {
            return ((org.mozilla1.javascript.xml.XMLObject) avf).ecmaGet(lhVar, new Integer(i));
        }
        Object b = ScriptableObject.m23575b(avf, i);
        if (b == Scriptable.NOT_FOUND) {
            return Undefined.instance;
        }
        return b;
    }

    /* renamed from: a */
    public static Object m7517a(Object obj, Object obj2, Object obj3, Context lhVar) {
        Scriptable a = m7477a(lhVar, obj);
        if (a != null) {
            return m7496a(a, obj2, obj3, lhVar);
        }
        throw undefWriteError(obj, obj2, obj3);
    }

    /* renamed from: a */
    public static Object m7496a(Scriptable avf, Object obj, Object obj2, Context lhVar) {
        if (avf instanceof org.mozilla1.javascript.xml.XMLObject) {
            ((org.mozilla1.javascript.xml.XMLObject) avf).ecmaPut(lhVar, obj, obj2);
        } else {
            String b = m7547b(lhVar, obj);
            if (b == null) {
                ScriptableObject.m23564a(avf, m7602o(lhVar), obj2);
            } else {
                ScriptableObject.m23566a(avf, b, obj2);
            }
        }
        return obj2;
    }

    /* renamed from: a */
    public static Object m7520a(Object obj, String str, Object obj2, Context lhVar) {
        Scriptable a = m7477a(lhVar, obj);
        if (a != null) {
            return m7501a(a, str, obj2, lhVar);
        }
        throw undefWriteError(obj, str, obj2);
    }

    /* renamed from: a */
    public static Object m7501a(Scriptable avf, String str, Object obj, Context lhVar) {
        if (avf instanceof org.mozilla1.javascript.xml.XMLObject) {
            ((org.mozilla1.javascript.xml.XMLObject) avf).ecmaPut(lhVar, str, obj);
        } else {
            ScriptableObject.m23566a(avf, str, obj);
        }
        return obj;
    }

    /* renamed from: a */
    public static Object m7512a(Object obj, double d, Object obj2, Context lhVar) {
        Scriptable a = m7477a(lhVar, obj);
        if (a == null) {
            throw undefWriteError(obj, String.valueOf(d), obj2);
        }
        int i = (int) d;
        if (((double) i) == d) {
            return m7492a(a, i, obj2, lhVar);
        }
        return m7501a(a, toString(d), obj2, lhVar);
    }

    /* renamed from: a */
    public static Object m7492a(Scriptable avf, int i, Object obj, Context lhVar) {
        if (avf instanceof org.mozilla1.javascript.xml.XMLObject) {
            ((org.mozilla1.javascript.xml.XMLObject) avf).ecmaPut(lhVar, new Integer(i), obj);
        } else {
            ScriptableObject.m23564a(avf, i, obj);
        }
        return obj;
    }

    /* renamed from: b */
    public static boolean m7552b(Scriptable avf, Object obj, Context lhVar) {
        if (avf instanceof org.mozilla1.javascript.xml.XMLObject) {
            return ((org.mozilla1.javascript.xml.XMLObject) avf).ecmaDelete(lhVar, obj);
        }
        String b = m7547b(lhVar, obj);
        if (b == null) {
            return ScriptableObject.m23582d(avf, m7602o(lhVar));
        }
        return ScriptableObject.m23591l(avf, b);
    }

    /* renamed from: c */
    public static boolean m7562c(Scriptable avf, Object obj, Context lhVar) {
        if (avf instanceof org.mozilla1.javascript.xml.XMLObject) {
            return ((org.mozilla1.javascript.xml.XMLObject) avf).ecmaHas(lhVar, obj);
        }
        String b = m7547b(lhVar, obj);
        if (b == null) {
            return ScriptableObject.m23580c(avf, m7602o(lhVar));
        }
        return ScriptableObject.m23589k(avf, b);
    }

    /* renamed from: a */
    public static Object m7502a(org.mozilla1.javascript.Ref bHVar, Context lhVar) {
        return bHVar.get(lhVar);
    }

    /* renamed from: a */
    public static Object m7504a(org.mozilla1.javascript.Ref bHVar, Object obj, Context lhVar) {
        return bHVar.set(lhVar, obj);
    }

    /* renamed from: b */
    public static Object m7542b(org.mozilla1.javascript.Ref bHVar, Context lhVar) {
        return wrapBoolean(bHVar.delete(lhVar));
    }

    /* renamed from: nm */
    static boolean m7598nm(String str) {
        return str.equals("__proto__") || str.equals("__parent__");
    }

    /* renamed from: c */
    public static org.mozilla1.javascript.Ref m7556c(Object obj, String str, Context lhVar) {
        return SpecialRef.m15406a(lhVar, obj, str);
    }

    /* renamed from: b */
    public static Object m7545b(Object obj, Object obj2, Context lhVar) {
        Scriptable a = m7477a(lhVar, obj);
        if (a != null) {
            return wrapBoolean(m7552b(a, obj2, lhVar));
        }
        throw m7610u("msg.undef.prop.delete", toString(obj), obj2 == null ? "null" : obj2.toString());
    }

    /* renamed from: b */
    public static Object m7543b(Context lhVar, Scriptable avf, String str) {
        Scriptable parentScope = avf.getParentScope();
        if (parentScope != null) {
            return m7505a(lhVar, avf, parentScope, str, false);
        }
        Object c = m7558c(lhVar, avf, str);
        if (c != Scriptable.NOT_FOUND) {
            return c;
        }
        throw m7583g(avf, str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0026, code lost:
        if (r1 == null) goto L_0x0028;
     */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x007b A[LOOP:0: B:1:0x0002->B:41:0x007b, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x002e A[SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Object m7505a(Context r4, Scriptable r5, org.mozilla1.javascript.xml.XMLObject r6, java.lang.String r7, boolean r8) {
        /*
            r1 = 0
            r3 = r5
        L_0x0002:
            boolean r0 = r3 instanceof p001a.C6805atx
            if (r0 == 0) goto L_0x004a
            a.aVF r0 = r3.getPrototype()
            boolean r2 = r0 instanceof p001a.XMLObject
            if (r2 == 0) goto L_0x003f
            a.atD r0 = (p001a.XMLObject) r0
            boolean r2 = r0.ecmaHas(r4, r7)
            if (r2 == 0) goto L_0x0026
            java.lang.Object r1 = r0.ecmaGet(r4, r7)
            r6 = r0
        L_0x001b:
            if (r8 == 0) goto L_0x0073
            boolean r0 = r1 instanceof p001a.C2957mF
            if (r0 != 0) goto L_0x0070
            java.lang.RuntimeException r0 = notFunctionError(r1, r7)
            throw r0
        L_0x0026:
            if (r1 != 0) goto L_0x0076
        L_0x0028:
            a.aVF r2 = r6.getParentScope()
            if (r2 != 0) goto L_0x007b
            java.lang.Object r1 = m7558c((p001a.C2909lh) r4, (p001a.aVF) r6, (java.lang.String) r7)
            java.lang.Object r2 = p001a.aVF.NOT_FOUND
            if (r1 != r2) goto L_0x0074
            if (r0 == 0) goto L_0x003a
            if (r8 == 0) goto L_0x006a
        L_0x003a:
            java.lang.RuntimeException r0 = m7583g((p001a.aVF) r6, (java.lang.String) r7)
            throw r0
        L_0x003f:
            java.lang.Object r2 = p001a.C6327akn.m23587j(r0, r7)
            java.lang.Object r3 = p001a.aVF.NOT_FOUND
            if (r2 == r3) goto L_0x0076
            r6 = r0
            r1 = r2
            goto L_0x001b
        L_0x004a:
            boolean r0 = r3 instanceof p001a.C2058bh
            if (r0 == 0) goto L_0x005f
            java.lang.Object r2 = r3.get((java.lang.String) r7, (p001a.aVF) r3)
            java.lang.Object r0 = p001a.aVF.NOT_FOUND
            if (r2 == r0) goto L_0x0076
            if (r8 == 0) goto L_0x0078
            a.aVF r0 = p001a.C6327akn.m23595x(r6)
            r6 = r0
            r1 = r2
            goto L_0x001b
        L_0x005f:
            java.lang.Object r0 = p001a.C6327akn.m23587j(r3, r7)
            java.lang.Object r2 = p001a.aVF.NOT_FOUND
            if (r0 == r2) goto L_0x0076
            r6 = r3
            r1 = r0
            goto L_0x001b
        L_0x006a:
            java.lang.Object r0 = r0.ecmaGet(r4, r7)
        L_0x006e:
            r1 = r0
            goto L_0x001b
        L_0x0070:
            m7587i(r4, r6)
        L_0x0073:
            return r1
        L_0x0074:
            r0 = r1
            goto L_0x006e
        L_0x0076:
            r0 = r1
            goto L_0x0028
        L_0x0078:
            r6 = r5
            r1 = r2
            goto L_0x001b
        L_0x007b:
            r1 = r0
            r3 = r6
            r6 = r2
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C0903NH.m7505a(a.lh, a.aVF, a.aVF, java.lang.String, boolean):java.lang.Object");
    }

    /* renamed from: c */
    private static Object m7558c(Context lhVar, Scriptable avf, String str) {
        if (lhVar.elw) {
            avf = m7565d(lhVar.topCallScope, avf);
        }
        return ScriptableObject.m23587j(avf, str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0024, code lost:
        if (r1 == null) goto L_0x0026;
     */
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static Scriptable m7567d(Context r4, Scriptable r5, java.lang.String r6) {
        /*
            r1 = 0
            a.aVF r0 = r5.getParentScope()
            if (r0 == 0) goto L_0x0056
            r2 = r0
        L_0x0008:
            boolean r0 = r5 instanceof p001a.C6805atx
            if (r0 != 0) goto L_0x0014
        L_0x000c:
            boolean r0 = p001a.C6327akn.m23589k(r5, r6)
            if (r0 == 0) goto L_0x0047
            r0 = r5
        L_0x0013:
            return r0
        L_0x0014:
            a.aVF r0 = r5.getPrototype()
            boolean r3 = r0 instanceof p001a.XMLObject
            if (r3 == 0) goto L_0x003f
            a.atD r0 = (p001a.XMLObject) r0
            boolean r3 = r0.ecmaHas(r4, r6)
            if (r3 != 0) goto L_0x0013
            if (r1 != 0) goto L_0x0045
        L_0x0026:
            a.aVF r3 = r2.getParentScope()
            if (r3 != 0) goto L_0x0052
            r1 = r0
        L_0x002d:
            boolean r0 = r4.elw
            if (r0 == 0) goto L_0x0037
            a.aVF r0 = r4.eli
            a.aVF r2 = m7565d((p001a.aVF) r0, (p001a.aVF) r2)
        L_0x0037:
            boolean r0 = p001a.C6327akn.m23589k(r2, r6)
            if (r0 == 0) goto L_0x0050
            r0 = r2
            goto L_0x0013
        L_0x003f:
            boolean r3 = p001a.C6327akn.m23589k(r0, r6)
            if (r3 != 0) goto L_0x0013
        L_0x0045:
            r0 = r1
            goto L_0x0026
        L_0x0047:
            a.aVF r0 = r2.getParentScope()
            if (r0 == 0) goto L_0x002d
            r5 = r2
            r2 = r0
            goto L_0x000c
        L_0x0050:
            r0 = r1
            goto L_0x0013
        L_0x0052:
            r1 = r0
            r5 = r2
            r2 = r3
            goto L_0x0008
        L_0x0056:
            r2 = r5
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C0903NH.m7567d(a.lh, a.aVF, java.lang.String):a.aVF");
    }

    /* renamed from: a */
    public static Object m7494a(Scriptable avf, Object obj, Context lhVar, Scriptable avf2, String str) {
        if (avf == null) {
            if (lhVar.hasFeature(11) || lhVar.hasFeature(8)) {
                Context.reportWarning(getMessage1("msg.assn.create.strict", str));
            }
            Scriptable x = ScriptableObject.m23595x(avf2);
            if (lhVar.elw) {
                x = m7565d(lhVar.topCallScope, x);
            }
            x.put(str, x, obj);
        } else if (avf instanceof org.mozilla1.javascript.xml.XMLObject) {
            ((org.mozilla1.javascript.xml.XMLObject) avf).ecmaPut(lhVar, str, obj);
        } else {
            ScriptableObject.m23566a(avf, str, obj);
        }
        return obj;
    }

    /* renamed from: a */
    public static Object m7495a(Scriptable avf, Object obj, Context lhVar, String str) {
        if (avf instanceof org.mozilla1.javascript.xml.XMLObject) {
            ((org.mozilla1.javascript.xml.XMLObject) avf).ecmaPut(lhVar, str, obj);
        } else {
            ScriptableObject.m23578b(avf, str, obj);
        }
        return obj;
    }

    /* renamed from: a */
    public static Scriptable m7476a(Context lhVar, Scriptable avf, Scriptable avf2, boolean z) {
        Boolean bool;
        if (!ScriptableObject.m23589k(avf2, NativeIterator.hOg)) {
            return null;
        }
        Object j = ScriptableObject.m23587j(avf2, NativeIterator.hOg);
        if (!(j instanceof Callable)) {
            throw m7600no("msg.invalid.iterator");
        }
        Callable mFVar = (Callable) j;
        Object[] objArr = new Object[1];
        if (z) {
            bool = Boolean.TRUE;
        } else {
            bool = Boolean.FALSE;
        }
        objArr[0] = bool;
        Object call = mFVar.call(lhVar, avf, avf2, objArr);
        if (call instanceof Scriptable) {
            return (Scriptable) call;
        }
        throw m7600no("msg.iterator.primitive");
    }

    /* renamed from: a */
    public static Object m7514a(Object obj, Context lhVar, boolean z) {
        int i;
        if (z) {
            i = 1;
        } else {
            i = 0;
        }
        return m7513a(obj, lhVar, i);
    }

    /* renamed from: a */
    public static Object m7513a(Object obj, Context lhVar, int i) {
        C0905b bVar = new C0905b((C0905b) null);
        bVar.fnr = m7477a(lhVar, obj);
        if (bVar.fnr != null) {
            bVar.iDH = i;
            bVar.iDJ = null;
            if (!(i == 3 || i == 4 || i == 5)) {
                bVar.iDJ = m7476a(lhVar, bVar.fnr.getParentScope(), bVar.fnr, true);
            }
            if (bVar.iDJ == null) {
                m7526a(bVar);
            }
        }
        return bVar;
    }

    /* renamed from: b */
    public static void m7550b(Object obj, boolean z) {
        ((C0905b) obj).iDI = z;
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 140 */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0054, code lost:
        r0 = r5.iDE;
        r1 = r5.index;
        r5.index = r1 + 1;
        r0 = r0[r1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0060, code lost:
        if (r5.iDF == null) goto L_0x006a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0068, code lost:
        if (r5.iDF.has(r0) != false) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006c, code lost:
        if ((r0 instanceof java.lang.String) == false) goto L_0x007f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006e, code lost:
        r0 = (java.lang.String) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0078, code lost:
        if (r5.fnr.has(r0, r5.fnr) == false) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x007a, code lost:
        r5.iDG = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x007c, code lost:
        r0 = java.lang.Boolean.TRUE;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007f, code lost:
        r1 = ((java.lang.Number) r0).intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x008d, code lost:
        if (r5.fnr.has(r1, r5.fnr) == false) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0091, code lost:
        if (r5.iDI == false) goto L_0x009b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0093, code lost:
        r0 = new java.lang.Integer(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0098, code lost:
        r5.iDG = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x009b, code lost:
        r0 = java.lang.String.valueOf(r1);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Boolean enumNext(java.lang.Object r5) {
        /*
            a.NH$b r5 = (p001a.C0903NH.C0905b) r5
            a.aVF r0 = r5.iDJ
            if (r0 == 0) goto L_0x004d
            a.aVF r0 = r5.iDJ
            java.lang.String r1 = "next"
            java.lang.Object r0 = p001a.C6327akn.m23587j(r0, r1)
            boolean r1 = r0 instanceof p001a.C2957mF
            if (r1 != 0) goto L_0x0015
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
        L_0x0014:
            return r0
        L_0x0015:
            a.mF r0 = (p001a.C2957mF) r0
            a.lh r1 = p001a.C2909lh.bwA()
            a.aVF r2 = r5.iDJ     // Catch:{ dx -> 0x002e }
            a.aVF r2 = r2.getParentScope()     // Catch:{ dx -> 0x002e }
            a.aVF r3 = r5.iDJ     // Catch:{ dx -> 0x002e }
            java.lang.Object[] r4 = emptyArgs     // Catch:{ dx -> 0x002e }
            java.lang.Object r0 = r0.call(r1, r2, r3, r4)     // Catch:{ dx -> 0x002e }
            r5.iDG = r0     // Catch:{ dx -> 0x002e }
            java.lang.Boolean r0 = java.lang.Boolean.TRUE     // Catch:{ dx -> 0x002e }
            goto L_0x0014
        L_0x002e:
            r0 = move-exception
            java.lang.Object r1 = r0.getValue()
            boolean r1 = r1 instanceof NativeIterator.C3515a
            if (r1 == 0) goto L_0x003a
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            goto L_0x0014
        L_0x003a:
            throw r0
        L_0x003b:
            int r0 = r5.index
            java.lang.Object[] r1 = r5.iDE
            int r1 = r1.length
            if (r0 != r1) goto L_0x0054
            a.aVF r0 = r5.fnr
            a.aVF r0 = r0.getPrototype()
            r5.fnr = r0
            m7526a(r5)
        L_0x004d:
            a.aVF r0 = r5.fnr
            if (r0 != 0) goto L_0x003b
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            goto L_0x0014
        L_0x0054:
            java.lang.Object[] r0 = r5.iDE
            int r1 = r5.index
            int r2 = r1 + 1
            r5.index = r2
            r0 = r0[r1]
            a.uS r1 = r5.iDF
            if (r1 == 0) goto L_0x006a
            a.uS r1 = r5.iDF
            boolean r1 = r1.has(r0)
            if (r1 != 0) goto L_0x004d
        L_0x006a:
            boolean r1 = r0 instanceof java.lang.String
            if (r1 == 0) goto L_0x007f
            java.lang.String r0 = (java.lang.String) r0
            a.aVF r1 = r5.fnr
            a.aVF r2 = r5.fnr
            boolean r1 = r1.has((java.lang.String) r0, (p001a.aVF) r2)
            if (r1 == 0) goto L_0x004d
            r5.iDG = r0
        L_0x007c:
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            goto L_0x0014
        L_0x007f:
            java.lang.Number r0 = (java.lang.Number) r0
            int r1 = r0.intValue()
            a.aVF r0 = r5.fnr
            a.aVF r2 = r5.fnr
            boolean r0 = r0.has((int) r1, (p001a.aVF) r2)
            if (r0 == 0) goto L_0x004d
            boolean r0 = r5.iDI
            if (r0 == 0) goto L_0x009b
            java.lang.Integer r0 = new java.lang.Integer
            r0.<init>(r1)
        L_0x0098:
            r5.iDG = r0
            goto L_0x007c
        L_0x009b:
            java.lang.String r0 = java.lang.String.valueOf(r1)
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C0903NH.enumNext(java.lang.Object):java.lang.Boolean");
    }

    /* renamed from: c */
    public static Object m7559c(Object obj, Context lhVar) {
        C0905b bVar = (C0905b) obj;
        if (bVar.iDJ != null) {
            return bVar.iDG;
        }
        switch (bVar.iDH) {
            case 0:
            case 3:
                return bVar.iDG;
            case 1:
            case 4:
                return m7569d(obj, lhVar);
            case 2:
            case 5:
                return lhVar.mo20351b(bVar.fnr.getParentScope(), new Object[]{bVar.iDG, m7569d(obj, lhVar)});
            default:
                throw org.mozilla1.javascript.Kit.codeBug();
        }
    }

    /* renamed from: d */
    public static Object m7569d(Object obj, Context lhVar) {
        C0905b bVar = (C0905b) obj;
        String b = m7547b(lhVar, bVar.iDG);
        if (b != null) {
            return bVar.fnr.get(b, bVar.fnr);
        }
        return bVar.fnr.get(m7602o(lhVar), bVar.fnr);
    }

    /* renamed from: a */
    private static void m7526a(C0905b bVar) {
        Object[] objArr = null;
        while (bVar.fnr != null) {
            objArr = bVar.fnr.getIds();
            if (objArr.length != 0) {
                break;
            }
            bVar.fnr = bVar.fnr.getPrototype();
        }
        if (!(bVar.fnr == null || bVar.iDE == null)) {
            Object[] objArr2 = bVar.iDE;
            int length = objArr2.length;
            if (bVar.iDF == null) {
                bVar.iDF = new org.mozilla1.javascript.ObjToIntMap(length);
            }
            for (int i = 0; i != length; i++) {
                bVar.iDF.intern(objArr2[i]);
            }
        }
        bVar.iDE = objArr;
        bVar.index = 0;
    }

    /* renamed from: b */
    public static Callable m7541b(String str, Context lhVar, Scriptable avf) {
        Scriptable parentScope = avf.getParentScope();
        if (parentScope != null) {
            return (Callable) m7505a(lhVar, avf, (org.mozilla1.javascript.xml.XMLObject) parentScope, str, true);
        }
        Object c = m7558c(lhVar, avf, str);
        if (c instanceof Callable) {
            m7587i(lhVar, avf);
            return (Callable) c;
        } else if (c == Scriptable.NOT_FOUND) {
            throw m7583g(avf, str);
        } else {
            throw notFunctionError(c, str);
        }
    }

    /* renamed from: c */
    public static Callable m7557c(Object obj, Object obj2, Context lhVar) {
        Object b;
        Scriptable extraMethodSource;
        String b2 = m7547b(lhVar, obj2);
        if (b2 != null) {
            return m7568d(obj, b2, lhVar);
        }
        int o = m7602o(lhVar);
        Scriptable a = m7477a(lhVar, obj);
        if (a == null) {
            throw undefCallError(obj, String.valueOf(o));
        }
        while (true) {
            b = ScriptableObject.m23575b(a, o);
            if (b == Scriptable.NOT_FOUND && (a instanceof org.mozilla1.javascript.xml.XMLObject) && (extraMethodSource = ((org.mozilla1.javascript.xml.XMLObject) a).getExtraMethodSource(lhVar)) != null) {
                a = extraMethodSource;
            }
        }
        if (!(b instanceof Callable)) {
            throw notFunctionError(b, obj2);
        }
        m7587i(lhVar, a);
        return (Callable) b;
    }

    /* renamed from: d */
    public static Callable m7568d(Object obj, String str, Context lhVar) {
        Object j;
        Scriptable extraMethodSource;
        NoSuchMethodShim aVar;
        Scriptable a = m7477a(lhVar, obj);
        if (a == null) {
            throw undefCallError(obj, str);
        }
        while (true) {
            j = ScriptableObject.m23587j(a, str);
            if (j == Scriptable.NOT_FOUND && (a instanceof org.mozilla1.javascript.xml.XMLObject) && (extraMethodSource = ((org.mozilla1.javascript.xml.XMLObject) a).getExtraMethodSource(lhVar)) != null) {
                a = extraMethodSource;
            }
        }
        if (!(j instanceof Callable)) {
            Object j2 = ScriptableObject.m23587j(a, "__noSuchMethod__");
            if (j2 instanceof Callable) {
                aVar = new NoSuchMethodShim((Callable) j2, str);
            } else {
                throw m7523a((Object) a, j, str);
            }
        } else {
            aVar = j;
        }
        m7587i(lhVar, a);
        return (Callable) aVar;
    }

    /* renamed from: e */
    public static Callable m7573e(Object obj, Context lhVar) {
        if (!(obj instanceof Callable)) {
            throw notFunctionError(obj);
        }
        Callable mFVar = (Callable) obj;
        Scriptable avf = null;
        if (mFVar instanceof Scriptable) {
            avf = ((Scriptable) mFVar).getParentScope();
        }
        if (avf == null) {
            if (lhVar.topCallScope == null) {
                throw new IllegalStateException();
            }
            avf = lhVar.topCallScope;
        }
        if (avf.getParentScope() != null && !(avf instanceof NativeWith) && (avf instanceof org.mozilla1.javascript.NativeCall)) {
            avf = ScriptableObject.m23595x(avf);
        }
        m7587i(lhVar, avf);
        return mFVar;
    }

    /* renamed from: a */
    public static org.mozilla1.javascript.Ref m7486a(Callable mFVar, Scriptable avf, Object[] objArr, Context lhVar) {
        if (mFVar instanceof RefCallable) {
            RefCallable dn = (RefCallable) mFVar;
            org.mozilla1.javascript.Ref a = dn.mo1673a(lhVar, avf, objArr);
            if (a != null) {
                return a;
            }
            throw new IllegalStateException(String.valueOf(dn.getClass().getName()) + ".refCall() returned null");
        }
        throw m7535aY("ReferenceError", getMessage1("msg.no.ref.from.function", toString((Object) mFVar)));
    }

    /* renamed from: a */
    public static Scriptable m7479a(Object obj, Context lhVar, Scriptable avf, Object[] objArr) {
        if (obj instanceof Function) {
            return ((Function) obj).construct(lhVar, avf, objArr);
        }
        throw notFunctionError(obj);
    }

    /* renamed from: a */
    public static Object m7507a(Context lhVar, Callable mFVar, Scriptable avf, Object[] objArr, Scriptable avf2, Scriptable avf3, int i, String str, int i2) {
        if (i == 1) {
            if (NativeGlobal.m2724L(mFVar)) {
                return m7506a(lhVar, avf2, (Object) avf3, objArr, str, i2);
            }
        } else if (i != 2) {
            throw org.mozilla1.javascript.Kit.codeBug();
        } else if (NativeWith.m26126at(mFVar)) {
            throw Context.m35246q("msg.only.from.new", "With");
        }
        return mFVar.call(lhVar, avf2, avf, objArr);
    }

    /* renamed from: a */
    public static Object m7509a(Context lhVar, Object obj, Object[] objArr, Scriptable avf, int i) {
        if (i == 1) {
            if (NativeGlobal.m2724L(obj)) {
                throw m7536aZ("msg.not.ctor", "eval");
            }
        } else if (i != 2) {
            throw org.mozilla1.javascript.Kit.codeBug();
        } else if (NativeWith.m26126at(obj)) {
            return NativeWith.m26127f(lhVar, avf, objArr);
        }
        return m7479a(obj, lhVar, avf, objArr);
    }

    /* renamed from: a */
    public static Object m7522a(boolean z, Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        Scriptable avf3;
        Object[] objArr2;
        int length = objArr.length;
        Callable r = m7607r(avf2);
        Scriptable avf4 = null;
        if (length != 0) {
            avf4 = m7477a(lhVar, objArr[0]);
        }
        if (avf4 == null) {
            avf3 = getTopCallScope(lhVar);
        } else {
            avf3 = avf4;
        }
        if (z) {
            if (length <= 1) {
                objArr2 = emptyArgs;
            } else {
                objArr2 = m7563c(lhVar, objArr[1]);
            }
        } else if (length <= 1) {
            objArr2 = emptyArgs;
        } else {
            objArr2 = new Object[(length - 1)];
            System.arraycopy(objArr, 1, objArr2, 0, length - 1);
        }
        return r.call(lhVar, avf, avf3, objArr2);
    }

    /* renamed from: c */
    static Object[] m7563c(Context lhVar, Object obj) {
        if (obj == null || obj == Undefined.instance) {
            return emptyArgs;
        }
        if ((obj instanceof NativeArray) || (obj instanceof Arguments)) {
            return lhVar.mo20370g((Scriptable) obj);
        }
        throw m7600no("msg.arg.isnt.array");
    }

    /* renamed from: r */
    static Callable m7607r(Scriptable avf) {
        if (avf instanceof Callable) {
            return (Callable) avf;
        }
        Object defaultValue = avf.getDefaultValue(FunctionClass);
        if (defaultValue instanceof Callable) {
            return (Callable) defaultValue;
        }
        throw notFunctionError(defaultValue, avf);
    }

    /* renamed from: a */
    public static Object m7506a(Context lhVar, Scriptable avf, Object obj, Object[] objArr, String str, int i) {
        if (objArr.length < 1) {
            return Undefined.instance;
        }
        String str2 = objArr[0];
        if (str2 instanceof String) {
            if (str == null) {
                int[] iArr = new int[1];
                str = Context.m35242c(iArr);
                if (str != null) {
                    i = iArr[0];
                } else {
                    str = "";
                }
            }
            String a = m7524a(true, str, i);
            ErrorReporter a2 = DefaultErrorReporter.m39748a(lhVar.bwt());
            Evaluator bwC = Context.bwC();
            if (bwC == null) {
                throw new JavaScriptException("Interpreter not present", str, i);
            }
            Script a3 = lhVar.mo20328a(str2, bwC, a2, a, 1, (Object) null);
            bwC.mo5926a(a3);
            return ((Callable) a3).call(lhVar, avf, (Scriptable) obj, emptyArgs);
        } else if (lhVar.hasFeature(11) || lhVar.hasFeature(9)) {
            throw Context.m35244gE("msg.eval.nonstring.strict");
        } else {
            Context.reportWarning(getMessage0("msg.eval.nonstring"));
            return str2;
        }
    }

    public static String typeof(Object obj) {
        if (obj == null) {
            return "object";
        }
        if (obj == Undefined.instance) {
            return "undefined";
        }
        if (obj instanceof Scriptable) {
            if ((obj instanceof ScriptableObject) && ((ScriptableObject) obj).dwc()) {
                return "undefined";
            }
            if (obj instanceof org.mozilla1.javascript.xml.XMLObject) {
                return "xml";
            }
            return obj instanceof Callable ? "function" : "object";
        } else if (obj instanceof String) {
            return "string";
        } else {
            if (obj instanceof Number) {
                return "number";
            }
            if (obj instanceof Boolean) {
                return "boolean";
            }
            throw m7614y("msg.invalid.type", obj);
        }
    }

    /* renamed from: f */
    public static String m7578f(Scriptable avf, String str) {
        Context bwA = Context.bwA();
        Scriptable d = m7567d(bwA, avf, str);
        if (d == null) {
            return "undefined";
        }
        return typeof(m7499a(d, str, bwA));
    }

    /* renamed from: d */
    public static Object m7570d(Object obj, Object obj2, Context lhVar) {
        Object obj3;
        Object obj4;
        Object addValues;
        Object addValues2;
        if ((obj instanceof Number) && (obj2 instanceof Number)) {
            return wrapNumber(((Number) obj).doubleValue() + ((Number) obj2).doubleValue());
        }
        if ((obj instanceof org.mozilla1.javascript.xml.XMLObject) && (addValues2 = ((org.mozilla1.javascript.xml.XMLObject) obj).addValues(lhVar, true, obj2)) != Scriptable.NOT_FOUND) {
            return addValues2;
        }
        if ((obj2 instanceof org.mozilla1.javascript.xml.XMLObject) && (addValues = ((org.mozilla1.javascript.xml.XMLObject) obj2).addValues(lhVar, false, obj)) != Scriptable.NOT_FOUND) {
            return addValues;
        }
        if (obj instanceof Scriptable) {
            obj3 = ((Scriptable) obj).getDefaultValue((Class<?>) null);
        } else {
            obj3 = obj;
        }
        if (obj2 instanceof Scriptable) {
            obj4 = ((Scriptable) obj2).getDefaultValue((Class<?>) null);
        } else {
            obj4 = obj2;
        }
        if ((obj3 instanceof String) || (obj4 instanceof String)) {
            return toString(obj3).concat(toString(obj4));
        }
        if (!(obj3 instanceof Number) || !(obj4 instanceof Number)) {
            return wrapNumber(toNumber(obj4) + toNumber(obj3));
        }
        return wrapNumber(((Number) obj4).doubleValue() + ((Number) obj3).doubleValue());
    }

    /* renamed from: w */
    public static String m7612w(String str, Object obj) {
        return str.concat(toString(obj));
    }

    /* renamed from: k */
    public static String m7590k(Object obj, String str) {
        return toString(obj).concat(str);
    }

    /* renamed from: a */
    public static Object m7497a(Scriptable avf, String str, int i) {
        return m7500a(avf, str, Context.bwA(), i);
    }

    /* renamed from: a */
    public static Object m7500a(Scriptable avf, String str, Context lhVar, int i) {
        do {
            if (lhVar.elw && avf.getParentScope() == null) {
                avf = m7565d(lhVar.topCallScope, avf);
            }
            Scriptable avf2 = avf;
            do {
                Object obj = avf2.get(str, avf);
                if (obj != Scriptable.NOT_FOUND) {
                    return m7498a(avf2, str, avf, obj, i);
                }
                avf2 = avf2.getPrototype();
            } while (avf2 != null);
            avf = avf.getParentScope();
        } while (avf != null);
        throw m7583g(avf, str);
    }

    /* renamed from: a */
    public static Object m7519a(Object obj, String str, Context lhVar, int i) {
        Scriptable a = m7477a(lhVar, obj);
        if (a == null) {
            throw undefReadError(obj, str);
        }
        Scriptable avf = a;
        do {
            Object obj2 = avf.get(str, a);
            if (obj2 != Scriptable.NOT_FOUND) {
                return m7498a(avf, str, a, obj2, i);
            }
            avf = avf.getPrototype();
        } while (avf != null);
        a.put(str, a, (Object) NaNobj);
        return NaNobj;
    }

    /* renamed from: a */
    private static Object m7498a(Scriptable avf, String str, Scriptable avf2, Object obj, int i) {
        double number;
        Number number2;
        double d;
        boolean z = (i & 2) != 0;
        if (obj instanceof Number) {
            number = ((Number) obj).doubleValue();
            number2 = (Number) obj;
        } else {
            number = toNumber(obj);
            if (z) {
                number2 = wrapNumber(number);
            } else {
                number2 = (Number) obj;
            }
        }
        if ((i & 1) == 0) {
            d = number + 1.0d;
        } else {
            d = number - 1.0d;
        }
        Number wrapNumber = wrapNumber(d);
        avf.put(str, avf2, (Object) wrapNumber);
        if (z) {
            return number2;
        }
        return wrapNumber;
    }

    /* renamed from: a */
    public static Object m7516a(Object obj, Object obj2, Context lhVar, int i) {
        double number;
        Number number2;
        double d;
        Object a = m7515a(obj, obj2, lhVar);
        boolean z = (i & 2) != 0;
        if (a instanceof Number) {
            number = ((Number) a).doubleValue();
            number2 = (Number) a;
        } else {
            number = toNumber(a);
            if (z) {
                number2 = wrapNumber(number);
            } else {
                number2 = (Number) a;
            }
        }
        if ((i & 1) == 0) {
            d = number + 1.0d;
        } else {
            d = number - 1.0d;
        }
        Number wrapNumber = wrapNumber(d);
        m7517a(obj, obj2, (Object) wrapNumber, lhVar);
        if (z) {
            return number2;
        }
        return wrapNumber;
    }

    /* renamed from: a */
    public static Object m7503a(org.mozilla1.javascript.Ref bHVar, Context lhVar, int i) {
        double number;
        Number number2;
        double d;
        Object obj = bHVar.get(lhVar);
        boolean z = (i & 2) != 0;
        if (obj instanceof Number) {
            number = ((Number) obj).doubleValue();
            number2 = obj;
        } else {
            number = toNumber(obj);
            if (z) {
                number2 = wrapNumber(number);
            } else {
                number2 = obj;
            }
        }
        if ((i & 1) == 0) {
            d = number + 1.0d;
        } else {
            d = number - 1.0d;
        }
        Number wrapNumber = wrapNumber(d);
        bHVar.set(lhVar, wrapNumber);
        if (z) {
            return number2;
        }
        return wrapNumber;
    }

    /* renamed from: aS */
    private static Object m7532aS(Object obj) {
        if (obj instanceof Scriptable) {
            obj = ((Scriptable) obj).getDefaultValue((Class<?>) null);
            if (obj instanceof Scriptable) {
                throw m7600no("msg.bad.default.value");
            }
        }
        return obj;
    }

    /* renamed from: eq */
    public static boolean m7576eq(Object obj, Object obj2) {
        Object equivalentValues;
        Object equivalentValues2;
        Object equivalentValues3;
        Object equivalentValues4;
        Object equivalentValues5;
        double d = 1.0d;
        if (obj == null || obj == Undefined.instance) {
            if (obj2 == null || obj2 == Undefined.instance) {
                return true;
            }
            if (!(obj2 instanceof ScriptableObject) || (equivalentValues = ((ScriptableObject) obj2).equivalentValues(obj)) == Scriptable.NOT_FOUND) {
                return false;
            }
            return ((Boolean) equivalentValues).booleanValue();
        } else if (obj instanceof Number) {
            return m7551b(((Number) obj).doubleValue(), obj2);
        } else {
            if (obj instanceof String) {
                return m7613x((String) obj, obj2);
            }
            if (obj instanceof Boolean) {
                boolean booleanValue = ((Boolean) obj).booleanValue();
                if (obj2 instanceof Boolean) {
                    return booleanValue == ((Boolean) obj2).booleanValue();
                }
                if ((obj2 instanceof ScriptableObject) && (equivalentValues5 = ((ScriptableObject) obj2).equivalentValues(obj)) != Scriptable.NOT_FOUND) {
                    return ((Boolean) equivalentValues5).booleanValue();
                }
                return m7551b(booleanValue ? 1.0d : 0.0d, obj2);
            } else if (!(obj instanceof Scriptable)) {
                m7534aU(obj);
                return obj == obj2;
            } else if (obj2 instanceof Scriptable) {
                if (obj == obj2) {
                    return true;
                }
                if ((obj instanceof ScriptableObject) && (equivalentValues4 = ((ScriptableObject) obj).equivalentValues(obj2)) != Scriptable.NOT_FOUND) {
                    return ((Boolean) equivalentValues4).booleanValue();
                }
                if ((obj2 instanceof ScriptableObject) && (equivalentValues3 = ((ScriptableObject) obj2).equivalentValues(obj)) != Scriptable.NOT_FOUND) {
                    return ((Boolean) equivalentValues3).booleanValue();
                }
                if (!(obj instanceof Wrapper) || !(obj2 instanceof Wrapper)) {
                    return false;
                }
                Object unwrap = ((Wrapper) obj).unwrap();
                Object unwrap2 = ((Wrapper) obj2).unwrap();
                return unwrap == unwrap2 || (isPrimitive(unwrap) && isPrimitive(unwrap2) && m7576eq(unwrap, unwrap2));
            } else if (obj2 instanceof Boolean) {
                if ((obj instanceof ScriptableObject) && (equivalentValues2 = ((ScriptableObject) obj).equivalentValues(obj2)) != Scriptable.NOT_FOUND) {
                    return ((Boolean) equivalentValues2).booleanValue();
                }
                if (!((Boolean) obj2).booleanValue()) {
                    d = 0.0d;
                }
                return m7551b(d, obj);
            } else if (obj2 instanceof Number) {
                return m7551b(((Number) obj2).doubleValue(), obj);
            } else {
                if (obj2 instanceof String) {
                    return m7613x((String) obj2, obj);
                }
                return false;
            }
        }
    }

    private static boolean isPrimitive(Object obj) {
        return (obj instanceof Number) || (obj instanceof String) || (obj instanceof Boolean);
    }

    /* renamed from: b */
    static boolean m7551b(double d, Object obj) {
        Object equivalentValues;
        Object obj2 = obj;
        while (obj2 != null && obj2 != Undefined.instance) {
            if (obj2 instanceof Number) {
                return d == ((Number) obj2).doubleValue();
            }
            if (obj2 instanceof String) {
                return d == toNumber(obj2);
            }
            if (obj2 instanceof Boolean) {
                return d == (((Boolean) obj2).booleanValue() ? 1.0d : NaN);
            } else if (!(obj2 instanceof Scriptable)) {
                m7534aU(obj2);
                return false;
            } else if ((obj2 instanceof ScriptableObject) && (equivalentValues = ((ScriptableObject) obj2).equivalentValues(wrapNumber(d))) != Scriptable.NOT_FOUND) {
                return ((Boolean) equivalentValues).booleanValue();
            } else {
                obj2 = m7532aS(obj2);
            }
        }
        return false;
    }

    /* renamed from: x */
    private static boolean m7613x(String str, Object obj) {
        Object equivalentValues;
        Object obj2 = obj;
        while (obj2 != null && obj2 != Undefined.instance) {
            if (obj2 instanceof String) {
                return str.equals(obj2);
            }
            if (obj2 instanceof Number) {
                return toNumber(str) == ((Number) obj2).doubleValue();
            }
            if (obj2 instanceof Boolean) {
                return toNumber(str) == (((Boolean) obj2).booleanValue() ? 1.0d : NaN);
            } else if (!(obj2 instanceof Scriptable)) {
                m7534aU(obj2);
                return false;
            } else if ((obj2 instanceof ScriptableObject) && (equivalentValues = ((ScriptableObject) obj2).equivalentValues(str)) != Scriptable.NOT_FOUND) {
                return ((Boolean) equivalentValues).booleanValue();
            } else {
                obj2 = m7532aS(obj2);
            }
        }
        return false;
    }

    public static boolean shallowEq(Object obj, Object obj2) {
        if (obj == obj2) {
            if (!(obj instanceof Number)) {
                return true;
            }
            double doubleValue = ((Number) obj).doubleValue();
            if (doubleValue != doubleValue) {
                return false;
            }
            return true;
        } else if (obj == null || obj == Undefined.instance) {
            return false;
        } else {
            if (obj instanceof Number) {
                if (obj2 instanceof Number) {
                    if (((Number) obj).doubleValue() != ((Number) obj2).doubleValue()) {
                        return false;
                    }
                    return true;
                }
            } else if (obj instanceof String) {
                if (obj2 instanceof String) {
                    return obj.equals(obj2);
                }
            } else if (obj instanceof Boolean) {
                if (obj2 instanceof Boolean) {
                    return obj.equals(obj2);
                }
            } else if (!(obj instanceof Scriptable)) {
                m7534aU(obj);
                if (obj != obj2) {
                    return false;
                }
                return true;
            } else if ((obj instanceof Wrapper) && (obj2 instanceof Wrapper)) {
                if (((Wrapper) obj).unwrap() != ((Wrapper) obj2).unwrap()) {
                    return false;
                }
                return true;
            }
            return false;
        }
    }

    /* renamed from: e */
    public static boolean m7575e(Object obj, Object obj2, Context lhVar) {
        if (!(obj2 instanceof Scriptable)) {
            throw m7600no("msg.instanceof.not.object");
        } else if (!(obj instanceof Scriptable)) {
            return false;
        } else {
            return ((Scriptable) obj2).hasInstance((Scriptable) obj);
        }
    }

    /* renamed from: c */
    public static boolean m7561c(Scriptable avf, Scriptable avf2) {
        for (Scriptable prototype = avf.getPrototype(); prototype != null; prototype = prototype.getPrototype()) {
            if (prototype.equals(avf2)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: f */
    public static boolean m7581f(Object obj, Object obj2, Context lhVar) {
        if (obj2 instanceof Scriptable) {
            return m7562c((Scriptable) obj2, obj, lhVar);
        }
        throw m7600no("msg.instanceof.not.object");
    }

    public static boolean cmp_LT(Object obj, Object obj2) {
        Object obj3;
        Object obj4;
        double number;
        double number2;
        if (!(obj instanceof Number) || !(obj2 instanceof Number)) {
            if (obj instanceof Scriptable) {
                obj3 = ((Scriptable) obj).getDefaultValue(NumberClass);
            } else {
                obj3 = obj;
            }
            if (obj2 instanceof Scriptable) {
                obj4 = ((Scriptable) obj2).getDefaultValue(NumberClass);
            } else {
                obj4 = obj2;
            }
            if ((obj3 instanceof String) && (obj4 instanceof String)) {
                return ((String) obj3).compareTo((String) obj4) < 0;
            }
            number = toNumber(obj3);
            number2 = toNumber(obj4);
        } else {
            number = ((Number) obj).doubleValue();
            number2 = ((Number) obj2).doubleValue();
        }
        if (number < number2) {
            return true;
        }
        return false;
    }

    public static boolean cmp_LE(Object obj, Object obj2) {
        Object obj3;
        Object obj4;
        double number;
        double number2;
        if (!(obj instanceof Number) || !(obj2 instanceof Number)) {
            if (obj instanceof Scriptable) {
                obj3 = ((Scriptable) obj).getDefaultValue(NumberClass);
            } else {
                obj3 = obj;
            }
            if (obj2 instanceof Scriptable) {
                obj4 = ((Scriptable) obj2).getDefaultValue(NumberClass);
            } else {
                obj4 = obj2;
            }
            if ((obj3 instanceof String) && (obj4 instanceof String)) {
                return ((String) obj3).compareTo((String) obj4) <= 0;
            }
            number = toNumber(obj3);
            number2 = toNumber(obj4);
        } else {
            number = ((Number) obj).doubleValue();
            number2 = ((Number) obj2).doubleValue();
        }
        if (number <= number2) {
            return true;
        }
        return false;
    }

    /* renamed from: h */
    public static ScriptableObject m7585h(Context lhVar) {
        Class<?> classOrNull = org.mozilla1.javascript.Kit.classOrNull("org.mozilla.javascript.tools.shell.Global");
        if (classOrNull != null) {
            try {
                return (ScriptableObject) classOrNull.getConstructor(new Class[]{ContextClass}).newInstance(new Object[]{lhVar});
            } catch (Exception e) {
            }
        }
        return new ImporterTopLevel(lhVar);
    }

    /* renamed from: i */
    public static boolean m7588i(Context lhVar) {
        return lhVar.topCallScope != null;
    }

    /* renamed from: j */
    public static Scriptable getTopCallScope(Context lhVar) {
        Scriptable avf = lhVar.topCallScope;
        if (avf != null) {
            return avf;
        }
        throw new IllegalStateException();
    }

    /* renamed from: a */
    public static Object m7510a(Callable mFVar, Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        org.mozilla1.javascript.NativeCall bhVar;
        if (avf == null) {
            throw new IllegalArgumentException();
        } else if (lhVar.topCallScope != null) {
            throw new IllegalStateException();
        } else {
            lhVar.topCallScope = ScriptableObject.m23595x(avf);
            lhVar.elw = lhVar.hasFeature(7);
            try {
                if (bhVar == null) {
                    return lhVar.bwr().mo19794a(mFVar, lhVar, avf, avf2, objArr);
                }
                throw new IllegalStateException();
            } finally {
                lhVar.topCallScope = null;
                lhVar.ell = null;
                if (lhVar.elk != null) {
                    throw new IllegalStateException();
                }
            }
        }
    }

    /* renamed from: d */
    static Scriptable m7565d(Scriptable avf, Scriptable avf2) {
        if (avf == avf2) {
            return avf;
        }
        Scriptable avf3 = avf;
        do {
            avf3 = avf3.getPrototype();
            if (avf3 == avf2) {
                return avf;
            }
        } while (avf3 != null);
        return avf2;
    }

    /* renamed from: b */
    public static void m7548b(Context lhVar, int i) {
        lhVar.elI += i;
        if (lhVar.elI > lhVar.elJ) {
            lhVar.observeInstructionCount(lhVar.elI);
            lhVar.elI = 0;
        }
    }

    /* renamed from: a */
    public static void m7525a(NativeFunction ifR, Scriptable avf, Context lhVar, Scriptable avf2, boolean z) {
        if (lhVar.topCallScope == null) {
            throw new IllegalStateException();
        }
        int paramAndVarCount = ifR.getParamAndVarCount();
        if (paramAndVarCount != 0) {
            Scriptable avf3 = avf2;
            while (avf3 instanceof NativeWith) {
                avf3 = avf3.getParentScope();
            }
            while (true) {
                int i = paramAndVarCount - 1;
                if (paramAndVarCount != 0) {
                    String paramOrVarName = ifR.getParamOrVarName(i);
                    boolean kH = ifR.mo2867kH(i);
                    if (ScriptableObject.m23589k(avf2, paramOrVarName)) {
                        ScriptableObject.m23568a(avf2, paramOrVarName, kH);
                        paramAndVarCount = i;
                    } else if (z) {
                        avf3.put(paramOrVarName, avf3, Undefined.instance);
                        paramAndVarCount = i;
                    } else if (kH) {
                        ScriptableObject.m23584h(avf3, paramOrVarName);
                        paramAndVarCount = i;
                    } else {
                        ScriptableObject.m23567a(avf3, paramOrVarName, Undefined.instance, 4);
                        paramAndVarCount = i;
                    }
                } else {
                    return;
                }
            }
        }
    }

    /* renamed from: a */
    public static Scriptable m7475a(NativeFunction ifR, Scriptable avf, Object[] objArr) {
        return new org.mozilla1.javascript.NativeCall(ifR, avf, objArr);
    }

    /* renamed from: h */
    public static void m7586h(Context lhVar, Scriptable avf) {
        if (lhVar.topCallScope == null) {
            throw new IllegalStateException();
        }
        org.mozilla1.javascript.NativeCall bhVar = (org.mozilla1.javascript.NativeCall) avf;
        bhVar.f5877nw = lhVar.elk;
        lhVar.elk = bhVar;
    }

    /* renamed from: k */
    public static void m7591k(Context lhVar) {
        org.mozilla1.javascript.NativeCall bhVar = lhVar.elk;
        lhVar.elk = bhVar.f5877nw;
        bhVar.f5877nw = null;
    }

    /* renamed from: a */
    static org.mozilla1.javascript.NativeCall m7490a(Context lhVar, Function azg) {
        for (NativeCall bhVar = lhVar.elk; bhVar != null; bhVar = bhVar.f5877nw) {
            if (bhVar.f5875nu == azg) {
                return bhVar;
            }
        }
        return null;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v0, resolved type: a.TJ} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: a.TJ} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v7, resolved type: a.TJ} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v8, resolved type: a.TJ} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v26, resolved type: a.Ap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v11, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v9, resolved type: a.TJ} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static Scriptable m7480a(java.lang.Throwable r9, Scriptable r10, java.lang.String r11, Context r12, Scriptable r13) {
        /*
            boolean r0 = r9 instanceof p001a.C2317dx
            if (r0 == 0) goto L_0x0026
            r1 = 0
            r0 = r9
            a.dx r0 = (p001a.C2317dx) r0
            java.lang.Object r2 = r0.getValue()
            r0 = r1
        L_0x000d:
            a.Pd r1 = new a.Pd
            r1.<init>()
            r3 = 4
            r1.defineProperty((java.lang.String) r11, (java.lang.Object) r2, (int) r3)
            java.lang.String r3 = "__exception__"
            java.lang.Object r4 = p001a.C2909lh.m35239b((java.lang.Object) r9, (p001a.aVF) r13)
            r5 = 6
            r1.defineProperty((java.lang.String) r3, (java.lang.Object) r4, (int) r5)
            if (r0 == 0) goto L_0x0025
            r1.associateValue(r9, r2)
        L_0x0025:
            return r1
        L_0x0026:
            r6 = 1
            if (r10 == 0) goto L_0x0037
            a.Pd r10 = (org.mozilla.javascript.C1069Pd) r10
            java.lang.Object r1 = r10.getAssociatedValue(r9)
            if (r1 != 0) goto L_0x00fd
            p001a.C1520WN.codeBug()
            r0 = r6
            r2 = r1
            goto L_0x000d
        L_0x0037:
            r1 = 0
            boolean r0 = r9 instanceof p001a.C0068Ap
            if (r0 == 0) goto L_0x0094
            r0 = r9
            a.Ap r0 = (p001a.C0068Ap) r0
            java.lang.String r3 = r0.getName()
            java.lang.String r2 = r0.getErrorMessage()
            r4 = r0
        L_0x0048:
            java.lang.String r0 = r4.sourceName()
            if (r0 != 0) goto L_0x0050
            java.lang.String r0 = ""
        L_0x0050:
            int r7 = r4.lineNumber()
            if (r7 <= 0) goto L_0x00f1
            r5 = 3
            java.lang.Object[] r5 = new java.lang.Object[r5]
            r8 = 0
            r5[r8] = r2
            r2 = 1
            r5[r2] = r0
            r0 = 2
            java.lang.Integer r2 = new java.lang.Integer
            r2.<init>(r7)
            r5[r0] = r2
            r0 = r5
        L_0x0068:
            a.aVF r2 = r12.mo20332a((p001a.aVF) r13, (java.lang.String) r3, (java.lang.Object[]) r0)
            java.lang.String r0 = "name"
            p001a.C6327akn.m23566a((p001a.aVF) r2, (java.lang.String) r0, (java.lang.Object) r3)
            if (r1 == 0) goto L_0x0082
            a.Xo r0 = r12.bwy()
            r3 = 0
            java.lang.Object r0 = r0.mo6829a(r12, r13, r1, r3)
            java.lang.String r1 = "javaException"
            r3 = 5
            p001a.C6327akn.m23567a((p001a.aVF) r2, (java.lang.String) r1, (java.lang.Object) r0, (int) r3)
        L_0x0082:
            a.Xo r0 = r12.bwy()
            r1 = 0
            java.lang.Object r0 = r0.mo6829a(r12, r13, r4, r1)
            java.lang.String r1 = "rhinoException"
            r3 = 5
            p001a.C6327akn.m23567a((p001a.aVF) r2, (java.lang.String) r1, (java.lang.Object) r0, (int) r3)
            r0 = r6
            goto L_0x000d
        L_0x0094:
            boolean r0 = r9 instanceof p001a.C1049PN
            if (r0 == 0) goto L_0x00c6
            r0 = r9
            a.PN r0 = (p001a.C1049PN) r0
            java.lang.Throwable r1 = r0.getWrappedException()
            java.lang.String r3 = "JavaException"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.Class r4 = r1.getClass()
            java.lang.String r4 = r4.getName()
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r2.<init>(r4)
            java.lang.String r4 = ": "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = r1.getMessage()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r4 = r0
            goto L_0x0048
        L_0x00c6:
            boolean r0 = r9 instanceof p001a.C1319TJ
            if (r0 == 0) goto L_0x00d6
            r0 = r9
            a.TJ r0 = (p001a.C1319TJ) r0
            java.lang.String r3 = "InternalError"
            java.lang.String r2 = r0.getMessage()
            r4 = r0
            goto L_0x0048
        L_0x00d6:
            r0 = 13
            boolean r0 = r12.hasFeature(r0)
            if (r0 == 0) goto L_0x00ec
            a.PN r4 = new a.PN
            r4.<init>(r9)
            java.lang.String r3 = "JavaException"
            java.lang.String r0 = r9.toString()
            r2 = r0
            goto L_0x0048
        L_0x00ec:
            java.lang.RuntimeException r0 = p001a.C1520WN.codeBug()
            throw r0
        L_0x00f1:
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]
            r7 = 0
            r5[r7] = r2
            r2 = 1
            r5[r2] = r0
            r0 = r5
            goto L_0x0068
        L_0x00fd:
            r0 = r6
            r2 = r1
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C0903NH.m7480a(java.lang.Throwable, a.aVF, java.lang.String, a.lh, a.aVF):a.aVF");
    }

    /* renamed from: a */
    public static Scriptable m7478a(Object obj, Context lhVar, Scriptable avf) {
        Scriptable a = m7477a(lhVar, obj);
        if (a == null) {
            throw m7536aZ("msg.undef.with", toString(obj));
        } else if (a instanceof org.mozilla1.javascript.xml.XMLObject) {
            return ((org.mozilla1.javascript.xml.XMLObject) a).enterWith(avf);
        } else {
            return new NativeWith(avf, a);
        }
    }

    /* renamed from: s */
    public static Scriptable m7608s(Scriptable avf) {
        return ((NativeWith) avf).getParentScope();
    }

    /* renamed from: c */
    public static Scriptable m7555c(Object obj, Scriptable avf) {
        if (obj instanceof org.mozilla1.javascript.xml.XMLObject) {
            return ((org.mozilla1.javascript.xml.XMLObject) obj).enterDotQuery(avf);
        }
        throw m7533aT(obj);
    }

    /* renamed from: a */
    public static Object m7521a(boolean z, Scriptable avf) {
        return ((NativeWith) avf).updateDotQuery(z);
    }

    /* renamed from: t */
    public static Scriptable m7609t(Scriptable avf) {
        return ((NativeWith) avf).getParentScope();
    }

    /* renamed from: a */
    public static void m7527a(BaseFunction acp, Scriptable avf) {
        acp.setParentScope(avf);
        acp.setPrototype(ScriptableObject.m23594w(avf));
    }

    /* renamed from: a */
    public static void m7528a(ScriptableObject akn, Scriptable avf) {
        Scriptable x = ScriptableObject.m23595x(avf);
        akn.setParentScope(x);
        akn.setPrototype(ScriptableObject.m23585i(x, akn.getClassName()));
    }

    /* renamed from: a */
    public static void m7530a(Context lhVar, Scriptable avf, NativeFunction ifR, int i, boolean z) {
        if (i == 1) {
            String functionName = ifR.getFunctionName();
            if (functionName != null && functionName.length() != 0) {
                if (!z) {
                    ScriptableObject.m23567a(avf, functionName, (Object) ifR, 4);
                } else {
                    avf.put(functionName, avf, (Object) ifR);
                }
            }
        } else if (i == 3) {
            String functionName2 = ifR.getFunctionName();
            if (functionName2 != null && functionName2.length() != 0) {
                while (avf instanceof NativeWith) {
                    avf = avf.getParentScope();
                }
                avf.put(functionName2, avf, (Object) ifR);
            }
        } else {
            throw Kit.codeBug();
        }
    }

    /* renamed from: a */
    public static Scriptable m7481a(Object[] objArr, int[] iArr, Context lhVar, Scriptable avf) {
        int i;
        int length = objArr.length;
        if (iArr != null) {
            i = iArr.length;
        } else {
            i = 0;
        }
        int i2 = length + i;
        if (i2 <= 1 || i * 2 >= i2) {
            Scriptable a = lhVar.mo20332a(avf, "Array", emptyArgs);
            int i3 = 0;
            int i4 = 0;
            for (int i5 = 0; i5 != i2; i5++) {
                if (i4 == i || iArr[i4] != i5) {
                    ScriptableObject.m23564a(a, i5, objArr[i3]);
                    i3++;
                } else {
                    i4++;
                }
            }
            return a;
        }
        if (i != 0) {
            Object[] objArr2 = new Object[i2];
            int i6 = 0;
            int i7 = 0;
            for (int i8 = 0; i8 != i2; i8++) {
                if (i7 == i || iArr[i7] != i8) {
                    objArr2[i8] = objArr[i6];
                    i6++;
                } else {
                    objArr2[i8] = Scriptable.NOT_FOUND;
                    i7++;
                }
            }
            objArr = objArr2;
        }
        return lhVar.mo20332a(avf, "Array", objArr);
    }

    /* renamed from: a */
    public static Scriptable m7482a(Object[] objArr, Object[] objArr2, Context lhVar, Scriptable avf) {
        return m7483a(objArr, objArr2, new int[objArr.length], lhVar, avf);
    }

    /* renamed from: a */
    public static Scriptable m7483a(Object[] objArr, Object[] objArr2, int[] iArr, Context lhVar, Scriptable avf) {
        String str;
        Scriptable f = lhVar.mo20368f(avf);
        int length = objArr.length;
        for (int i = 0; i != length; i++) {
            String str2 = (String) objArr[i];
            int i2 = iArr[i];
            Object obj = objArr2[i];
            if (!(str2 instanceof String)) {
                ScriptableObject.m23564a(f, ((Integer) str2).intValue(), obj);
            } else if (i2 == 0) {
                ScriptableObject.m23566a(f, str2, obj);
            } else {
                if (i2 < 0) {
                    str = "__defineGetter__";
                } else {
                    str = "__defineSetter__";
                }
                Callable d = m7568d((Object) f, str, lhVar);
                m7605q(lhVar);
                d.call(lhVar, avf, f, new Object[]{str2, obj});
            }
        }
        return f;
    }

    public static boolean isArrayObject(Object obj) {
        return (obj instanceof NativeArray) || (obj instanceof Arguments);
    }

    /* renamed from: u */
    public static Object[] m7611u(Scriptable avf) {
        long f = NativeArray.m27415f(Context.bwA(), avf);
        if (f > 2147483647L) {
            throw new IllegalArgumentException();
        }
        int i = (int) f;
        if (i == 0) {
            return emptyArgs;
        }
        Object[] objArr = new Object[i];
        for (int i2 = 0; i2 < i; i2++) {
            Object b = ScriptableObject.m23575b(avf, i2);
            if (b == Scriptable.NOT_FOUND) {
                b = Undefined.instance;
            }
            objArr[i2] = b;
        }
        return objArr;
    }

    /* renamed from: b */
    static void m7549b(Context lhVar, String str) {
        int languageVersion = lhVar.getLanguageVersion();
        if (languageVersion >= 140 || languageVersion == 0) {
            String message1 = getMessage1("msg.deprec.ctor", str);
            if (languageVersion == 0) {
                Context.reportWarning(message1);
                return;
            }
            throw Context.m35245gF(message1);
        }
    }

    public static String getMessage0(String str) {
        return getMessage(str, (Object[]) null);
    }

    public static String getMessage1(String str, Object obj) {
        return getMessage(str, new Object[]{obj});
    }

    public static String getMessage2(String str, Object obj, Object obj2) {
        return getMessage(str, new Object[]{obj, obj2});
    }

    public static String getMessage3(String str, Object obj, Object obj2, Object obj3) {
        return getMessage(str, new Object[]{obj, obj2, obj3});
    }

    public static String getMessage4(String str, Object obj, Object obj2, Object obj3, Object obj4) {
        return getMessage(str, new Object[]{obj, obj2, obj3, obj4});
    }

    public static String getMessage(String str, Object[] objArr) {
        Context bwq = Context.bwq();
        try {
            return new MessageFormat(ResourceBundle.getBundle("org.mozilla.javascript.resources.Messages", bwq != null ? bwq.getLocale() : Locale.getDefault()).getString(str)).format(objArr);
        } catch (MissingResourceException e) {
            throw new RuntimeException("no message resource found for message property " + str);
        }
    }

    /* renamed from: aY */
    public static EcmaError m7535aY(String str, String str2) {
        int[] iArr = new int[1];
        return m7474a(str, str2, Context.m35242c(iArr), iArr[0], (String) null, 0);
    }

    /* renamed from: d */
    public static EcmaError m7564d(String str, String str2, int i) {
        int[] iArr = new int[1];
        String c = Context.m35242c(iArr);
        if (iArr[0] != 0) {
            iArr[0] = iArr[0] + i;
        }
        return m7474a(str, str2, c, iArr[0], (String) null, 0);
    }

    /* renamed from: a */
    public static EcmaError m7474a(String str, String str2, String str3, int i, String str4, int i2) {
        return new EcmaError(str, str2, str3, i, str4, i2);
    }

    /* renamed from: nn */
    public static EcmaError m7599nn(String str) {
        return m7535aY("TypeError", str);
    }

    /* renamed from: no */
    public static EcmaError m7600no(String str) {
        return m7599nn(getMessage0(str));
    }

    /* renamed from: aZ */
    public static EcmaError m7536aZ(String str, String str2) {
        return m7599nn(getMessage1(str, str2));
    }

    /* renamed from: u */
    public static EcmaError m7610u(String str, String str2, String str3) {
        return m7599nn(getMessage2(str, str2, str3));
    }

    /* renamed from: c */
    public static EcmaError m7553c(String str, String str2, String str3, String str4) {
        return m7599nn(getMessage3(str, str2, str3, str4));
    }

    public static RuntimeException undefReadError(Object obj, Object obj2) {
        return m7610u("msg.undef.prop.read", toString(obj), obj2 == null ? "null" : obj2.toString());
    }

    public static RuntimeException undefCallError(Object obj, Object obj2) {
        return m7610u("msg.undef.method.call", toString(obj), obj2 == null ? "null" : obj2.toString());
    }

    public static RuntimeException undefWriteError(Object obj, Object obj2, Object obj3) {
        return m7553c("msg.undef.prop.write", toString(obj), obj2 == null ? "null" : obj2.toString(), obj3 instanceof Scriptable ? obj3.toString() : toString(obj3));
    }

    /* renamed from: g */
    public static RuntimeException m7583g(Scriptable avf, String str) {
        throw m7535aY("ReferenceError", getMessage1("msg.is.not.defined", str));
    }

    public static RuntimeException notFunctionError(Object obj) {
        return notFunctionError(obj, obj);
    }

    public static RuntimeException notFunctionError(Object obj, Object obj2) {
        String obj3 = obj2 == null ? "null" : obj2.toString();
        if (obj == Scriptable.NOT_FOUND) {
            return m7536aZ("msg.function.not.found", obj3);
        }
        return m7610u("msg.isnt.function", obj3, typeof(obj));
    }

    /* renamed from: a */
    public static RuntimeException m7523a(Object obj, Object obj2, String str) {
        String nh = toString(obj);
        if (obj2 == Scriptable.NOT_FOUND) {
            return m7610u("msg.function.not.found.in", str, nh);
        }
        return m7553c("msg.isnt.function.in", str, nh, typeof(obj2));
    }

    /* renamed from: aT */
    private static RuntimeException m7533aT(Object obj) {
        throw m7536aZ("msg.isnt.xml.object", toString(obj));
    }

    /* renamed from: aU */
    private static void m7534aU(Object obj) {
        String str = "RHINO USAGE WARNING: Missed Context.javaToJS() conversion:\nRhino runtime detected object " + obj + " of class " + obj.getClass().getName() + " where it expected String, Number, Boolean or Scriptable instance. Please check your code for missing Context.javaToJS() call.";
        Context.reportWarning(str);
        System.err.println(str);
    }

    /* renamed from: l */
    public static RegExpProxy m7592l(Context lhVar) {
        return lhVar.bwD();
    }

    /* renamed from: a */
    public static void m7531a(Context lhVar, RegExpProxy aqh) {
        if (aqh == null) {
            throw new IllegalArgumentException();
        }
        lhVar.elr = aqh;
    }

    /* renamed from: m */
    public static RegExpProxy m7593m(Context lhVar) {
        RegExpProxy l = m7592l(lhVar);
        if (l != null) {
            return l;
        }
        throw Context.m35244gE("msg.no.regexp");
    }

    /* renamed from: n */
    private static org.mozilla1.javascript.xml.XMLLib currentXMLLib(Context lhVar) {
        if (lhVar.topCallScope == null) {
            throw new IllegalStateException();
        }
        org.mozilla1.javascript.xml.XMLLib am = lhVar.ell;
        if (am == null) {
            am = XMLLib.m370b(lhVar.topCallScope);
            if (am == null) {
                throw new IllegalStateException();
            }
            lhVar.ell = am;
        }
        return am;
    }

    /* renamed from: f */
    public static String m7580f(Object obj, Context lhVar) {
        return currentXMLLib(lhVar).escapeAttributeValue(obj);
    }

    /* renamed from: g */
    public static String m7584g(Object obj, Context lhVar) {
        return currentXMLLib(lhVar).escapeTextValue(obj);
    }

    /* renamed from: b */
    public static org.mozilla1.javascript.Ref m7540b(Object obj, Object obj2, Context lhVar, int i) {
        if (obj instanceof org.mozilla1.javascript.xml.XMLObject) {
            return ((org.mozilla1.javascript.xml.XMLObject) obj).memberRef(lhVar, obj2, i);
        }
        throw m7533aT(obj);
    }

    /* renamed from: a */
    public static org.mozilla1.javascript.Ref m7489a(Object obj, Object obj2, Object obj3, Context lhVar, int i) {
        if (obj instanceof org.mozilla1.javascript.xml.XMLObject) {
            return ((XMLObject) obj).memberRef(lhVar, obj2, obj3, i);
        }
        throw m7533aT(obj);
    }

    /* renamed from: a */
    public static org.mozilla1.javascript.Ref m7487a(Object obj, Context lhVar, Scriptable avf, int i) {
        return currentXMLLib(lhVar).nameRef(lhVar, obj, avf, i);
    }

    /* renamed from: a */
    public static Ref m7488a(Object obj, Object obj2, Context lhVar, Scriptable avf, int i) {
        return currentXMLLib(lhVar).nameRef(lhVar, obj, obj2, avf, i);
    }

    /* renamed from: c */
    private static void m7560c(Context lhVar, int i) {
        lhVar.elK = i;
    }

    /* renamed from: o */
    static int m7602o(Context lhVar) {
        return lhVar.elK;
    }

    /* renamed from: a */
    public static void m7529a(Context lhVar, long j) {
        if ((j >>> 32) != 0) {
            throw new IllegalArgumentException();
        }
        lhVar.elL = j;
    }

    /* renamed from: p */
    public static long m7603p(Context lhVar) {
        long j = lhVar.elL;
        if ((j >>> 32) == 0) {
            return j;
        }
        throw new IllegalStateException();
    }

    /* renamed from: i */
    private static void m7587i(Context lhVar, Scriptable avf) {
        if (lhVar.elM != null) {
            throw new IllegalStateException();
        }
        lhVar.elM = avf;
    }

    /* renamed from: q */
    public static Scriptable m7605q(Context lhVar) {
        Scriptable avf = lhVar.elM;
        lhVar.elM = null;
        return avf;
    }

    /* renamed from: a */
    static String m7524a(boolean z, String str, int i) {
        if (z) {
            return String.valueOf(str) + '#' + i + "(eval)";
        }
        return String.valueOf(str) + '#' + i + "(Function)";
    }

    /* renamed from: np */
    static boolean m7601np(String str) {
        return str.indexOf("(eval)") >= 0 || str.indexOf("(Function)") >= 0;
    }

    /* renamed from: y */
    private static RuntimeException m7614y(String str, Object obj) {
        return Context.m35246q(str, obj.getClass().getName());
    }

    /* renamed from: a.NH$a */
    private static class NoSuchMethodShim implements Callable {
        Callable dHF;
        String methodName;

        NoSuchMethodShim(Callable mFVar, String str) {
            this.dHF = mFVar;
            this.methodName = str;
        }

        public Object call(Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
            return this.dHF.call(lhVar, avf, avf2, new Object[]{this.methodName, ScriptRuntime.m7481a(objArr, (int[]) null, lhVar, avf)});
        }
    }

    /* renamed from: a.NH$b */
    /* compiled from: a */
    private static class C0905b implements Serializable {

        Scriptable fnr;
        Object[] iDE;
        ObjToIntMap iDF;
        Object iDG;
        int iDH;
        boolean iDI;
        Scriptable iDJ;
        int index;

        private C0905b() {
        }

        /* synthetic */ C0905b(C0905b bVar) {
            this();
        }
    }
}
