package org.mozilla1.javascript;


import java.io.Serializable;

/* renamed from: a.bH */
/* compiled from: a */
public abstract class Ref implements Serializable {
    public abstract Object get(Context lhVar);

    public abstract Object set(Context lhVar, Object obj);

    public boolean has(Context lhVar) {
        return true;
    }

    public boolean delete(Context lhVar) {
        return false;
    }
}
