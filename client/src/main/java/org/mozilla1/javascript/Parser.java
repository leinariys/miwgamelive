package org.mozilla1.javascript;

import java.io.IOException;
import java.io.Reader;
import java.util.Map;

/* renamed from: a.axf  reason: case insensitive filesystem */
/* compiled from: a */
public class Parser {
    static final int gPa = 65535;
    static final int gPb = 65536;
    static final int gPc = 131072;
    boolean gPe;
    ScriptOrFnNode gPl;
    Node.Scope gPm;
    /* renamed from: he */
    CompilerEnvirons f5571he;
    private ErrorReporter elq;
    private String frB;
    private String gPd;
    private org.mozilla1.javascript.TokenStream gPf;
    private int gPg;
    private int gPh;
    private IRFactory gPi;
    private int gPj;
    private org.mozilla1.javascript.Decompiler gPk;
    private int gPn;
    private Map<String, Node> gPo;
    private ObjArray gPp;
    private ObjArray gPq;
    private int gPr;

    public Parser(CompilerEnvirons aki, ErrorReporter ale) {
        this.f5571he = aki;
        this.elq = ale;
    }

    /* renamed from: x */
    private static final boolean m27154x(int i, int i2, int i3) {
        return (i & i3) != i3 && (i2 & i3) == i3;
    }

    public int cCd() {
        return this.gPf.getLineno();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public org.mozilla1.javascript.Decompiler mo16810a(CompilerEnvirons aki) {
        return new Decompiler();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: aA */
    public void mo16812aA(String str, String str2) {
        if (this.f5571he.dgs()) {
            mo16813aB(str, str2);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: aB */
    public void mo16813aB(String str, String str2) {
        String message1 = ScriptRuntime.getMessage1(str, str2);
        if (this.f5571he.dgt()) {
            this.gPh++;
            this.elq.error(message1, this.gPd, this.gPf.getLineno(), this.gPf.mo14627el(), this.gPf.getOffset());
            return;
        }
        this.elq.warning(message1, this.gPd, this.gPf.getLineno(), this.gPf.mo14627el(), this.gPf.getOffset());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: kB */
    public void mo16820kB(String str) {
        this.gPh++;
        this.elq.error(ScriptRuntime.getMessage0(str), this.gPd, this.gPf.getLineno(), this.gPf.mo14627el(), this.gPf.getOffset());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: aC */
    public void mo16814aC(String str, String str2) {
        this.gPh++;
        this.elq.error(ScriptRuntime.getMessage1(str, str2), this.gPd, this.gPf.getLineno(), this.gPf.mo14627el(), this.gPf.getOffset());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: kC */
    public RuntimeException mo16821kC(String str) {
        mo16820kB(str);
        throw new C2009a((C2009a) null);
    }

    private int peekToken() {
        int dZ;
        int i = this.gPg;
        if (i == 0) {
            i = this.gPf.mo14623dZ();
            if (i == 1) {
                do {
                    dZ = this.gPf.mo14623dZ();
                } while (dZ == 1);
                i = dZ | 65536;
            }
            this.gPg = i;
        }
        return i & gPa;
    }

    private int cCe() {
        peekToken();
        return this.gPg;
    }

    private void cCf() {
        this.gPg = 0;
    }

    private int nextToken() {
        int peekToken = peekToken();
        cCf();
        return peekToken;
    }

    private int cCg() {
        peekToken();
        int i = this.gPg;
        cCf();
        return i;
    }

    /* renamed from: vM */
    private boolean m27150vM(int i) {
        if (peekToken() != i) {
            return false;
        }
        cCf();
        return true;
    }

    private int cCh() {
        int peekToken = peekToken();
        if ((this.gPg & 65536) != 0) {
            return 1;
        }
        return peekToken;
    }

    private void cCi() {
        if ((this.gPg & gPa) != 39) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        this.gPg |= 131072;
    }

    /* renamed from: c */
    private void m27134c(int i, String str) {
        if (!m27150vM(i)) {
            mo16821kC(str);
        }
    }

    private void cCj() {
        if (!this.f5571he.isXmlAvailable()) {
            mo16821kC("msg.XML.not.available");
        }
    }

    public String getEncodedSource() {
        return this.frB;
    }

    public boolean eof() {
        return this.gPf.eof();
    }

    /* access modifiers changed from: package-private */
    public boolean cCk() {
        return this.gPj != 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: u */
    public void mo16823u(Node qlVar) {
        Node.Scope cVar = (Node.Scope) qlVar;
        if (cVar.bXA() != null) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        cVar.mo21506b(this.gPm);
        this.gPm = cVar;
    }

    /* access modifiers changed from: package-private */
    public void popScope() {
        this.gPm = this.gPm.bXA();
    }

    /* renamed from: b */
    private Node m27131b(Node qlVar, boolean z) {
        Node l = this.gPi.mo17130l(qlVar, this.gPf.getLineno());
        if (this.gPp == null) {
            this.gPp = new ObjArray();
            if (this.gPq == null) {
                this.gPq = new ObjArray();
            }
        }
        this.gPp.push(l);
        this.gPq.push(l);
        if (z) {
            mo16823u(l);
        }
        return l;
    }

    /* renamed from: hf */
    private void m27135hf(boolean z) {
        this.gPp.pop();
        this.gPq.pop();
        if (z) {
            popScope();
        }
    }

    /* renamed from: b */
    private Node m27130b(Node qlVar, int i) {
        Node d = this.gPi.mo17117d(qlVar, i);
        if (this.gPq == null) {
            this.gPq = new ObjArray();
        }
        this.gPq.push(d);
        return d;
    }

    private void cCl() {
        this.gPq.pop();
    }

    /* renamed from: c */
    public ScriptOrFnNode mo16815c(String str, String str2, int i) {
        this.gPd = str2;
        this.gPf = new org.mozilla1.javascript.TokenStream(this, (Reader) null, str, i);
        try {
            return cCm();
        } catch (IOException e) {
            throw new IllegalStateException();
        }
    }

    /* renamed from: a */
    public ScriptOrFnNode mo16809a(Reader reader, String str, int i) {
        this.gPd = str;
        this.gPf = new org.mozilla1.javascript.TokenStream(this, reader, (String) null, i);
        return cCm();
    }

    private ScriptOrFnNode cCm() {
        Node cCq;
        int i;
        this.gPk = mo16810a(this.f5571he);
        this.gPi = new IRFactory(this);
        this.gPl = this.gPi.cDG();
        this.gPm = this.gPl;
        int dte = this.gPk.dte();
        this.frB = null;
        this.gPk.addToken(135);
        this.gPg = 0;
        this.gPh = 0;
        int lineno = this.gPf.getLineno();
        Node wb = this.gPi.mo17134wb(128);
        while (true) {
            int peekToken = peekToken();
            if (peekToken <= 0) {
                break;
            }
            if (peekToken == 108) {
                cCf();
                try {
                    if (this.gPe) {
                        i = 2;
                    } else {
                        i = 1;
                    }
                    cCq = m27151vN(i);
                } catch (C2009a e) {
                }
            } else {
                cCq = cCq();
            }
            try {
                this.gPi.mo17133m(wb, cCq);
            } catch (StackOverflowError e2) {
                throw Context.m35227a(ScriptRuntime.getMessage0("msg.too.deep.parser.recursion"), this.gPd, this.gPf.getLineno(), (String) null, 0);
            }
        }
        if (this.gPh != 0) {
            throw this.elq.runtimeError(ScriptRuntime.getMessage1("msg.got.syntax.errors", String.valueOf(this.gPh)), this.gPd, lineno, (String) null, 0);
        }
        this.gPl.setSourceName(this.gPd);
        this.gPl.setBaseLineno(lineno);
        this.gPl.setEndLineno(this.gPf.getLineno());
        this.gPl.setEncodedSourceBounds(dte, this.gPk.dte());
        this.gPi.mo17105a(this.gPl, wb);
        if (this.f5571he.isGeneratingSource()) {
            this.frB = this.gPk.getEncodedSource();
        }
        this.gPk = null;
        return this.gPl;
    }

    private Node cCn() {
        Node vN;
        this.gPj++;
        Node we = this.gPi.mo17137we(this.gPf.getLineno());
        while (true) {
            try {
                switch (peekToken()) {
                    case -1:
                    case 0:
                    case 85:
                        break;
                    case 108:
                        cCf();
                        vN = m27151vN(1);
                        continue;
                    default:
                        vN = cCq();
                        continue;
                }
            } catch (C2009a e) {
            } finally {
                this.gPj--;
            }
            return we;
            this.gPi.mo17133m(we, vN);
        }
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: vN */
    private Node m27151vN(int i) {
        Node qlVar;
        String str;
        int i2;
        String str2;
        int lineno = this.gPf.getLineno();
        int Au = this.gPk.mo11279Au(i);
        Node qlVar2 = null;
        if (m27150vM(39)) {
            String string = this.gPf.getString();
            this.gPk.mo11287nw(string);
            if (!m27150vM(86)) {
                if (this.f5571he.isAllowMemberExprAsFunctionName()) {
                    Node kF = this.gPi.mo17127kF(string);
                    string = "";
                    qlVar2 = m27127a(false, kF);
                }
                m27134c(86, "msg.no.paren.parms");
                qlVar = qlVar2;
                str = string;
            } else {
                qlVar = null;
                str = string;
            }
        } else if (m27150vM(86)) {
            qlVar = null;
            str = "";
        } else {
            if (this.f5571he.isAllowMemberExprAsFunctionName()) {
                qlVar2 = m27147hr(false);
            }
            m27134c(86, "msg.no.paren.parms");
            qlVar = qlVar2;
            str = "";
        }
        if (qlVar != null) {
            i2 = 2;
        } else {
            i2 = i;
        }
        if (i2 != 2 && str.length() > 0) {
            mo16811a(108, false, str);
        }
        boolean cCk = cCk();
        FunctionNode kH = this.gPi.mo17129kH(str);
        if (cCk || this.gPn > 0) {
            kH.eMP = true;
        }
        int a = this.gPl.mo7679a(kH);
        ScriptOrFnNode aak = this.gPl;
        this.gPl = kH;
        Node.Scope cVar = this.gPm;
        this.gPm = kH;
        int i3 = this.gPn;
        this.gPn = 0;
        Map<String, Node> map = this.gPo;
        this.gPo = null;
        ObjArray acx = this.gPp;
        this.gPp = null;
        ObjArray acx2 = this.gPq;
        this.gPq = null;
        int i4 = this.gPr;
        this.gPr = 0;
        Node qlVar3 = null;
        try {
            this.gPk.addToken(86);
            if (!m27150vM(87)) {
                boolean z = true;
                do {
                    if (!z) {
                        this.gPk.addToken(88);
                    }
                    z = false;
                    int peekToken = peekToken();
                    if (peekToken == 82 || peekToken == 84) {
                        if (qlVar3 == null) {
                            qlVar3 = new Node(88);
                        }
                        String cHs = this.gPl.cHs();
                        mo16811a(86, false, cHs);
                        qlVar3.mo21461G(this.gPi.mo17119e(121, cCw(), this.gPi.mo17127kF(cHs)));
                    } else {
                        m27134c(39, "msg.no.parm");
                        String string2 = this.gPf.getString();
                        mo16811a(86, false, string2);
                        this.gPk.mo11287nw(string2);
                    }
                } while (m27150vM(88));
                m27134c(87, "msg.no.paren.after.parms");
            }
            this.gPk.addToken(87);
            m27134c(84, "msg.no.brace.body");
            this.gPk.mo11281Aw(84);
            Node cCn = cCn();
            if (qlVar3 != null) {
                cCn.mo21460F(new Node(132, qlVar3, this.gPf.getLineno()));
            }
            m27134c(85, "msg.no.brace.after.body");
            if (this.f5571he.dgs() && !cCn.cFK()) {
                if (str.length() > 0) {
                    str2 = "msg.no.return.value";
                } else {
                    str2 = "msg.anon.no.return.value";
                }
                mo16812aA(str2, str);
            }
            if (i2 == 2 && str.length() > 0 && this.gPm.mo21510iM(str) == null) {
                mo16811a(108, false, str);
            }
            this.gPk.addToken(85);
            int Av = this.gPk.mo11280Av(Au);
            if (i != 2) {
                this.gPk.addToken(1);
            }
            this.gPr = i4;
            this.gPq = acx2;
            this.gPp = acx;
            this.gPo = map;
            this.gPn = i3;
            this.gPl = aak;
            this.gPm = cVar;
            kH.setEncodedSourceBounds(Au, Av);
            kH.setSourceName(this.gPd);
            kH.setBaseLineno(lineno);
            kH.setEndLineno(this.gPf.getLineno());
            Node a2 = this.gPi.mo17096a(kH, a, cCn, i2);
            if (qlVar == null) {
                return a2;
            }
            Node d = this.gPi.mo17116d(89, qlVar, a2);
            if (i != 2) {
                return this.gPi.mo17121f(d, lineno);
            }
            return d;
        } catch (Throwable th) {
            this.gPr = i4;
            this.gPq = acx2;
            this.gPp = acx;
            this.gPo = map;
            this.gPn = i3;
            this.gPl = aak;
            this.gPm = cVar;
            throw th;
        }
    }

    /* renamed from: v */
    private Node m27149v(Node qlVar) {
        if (qlVar == null) {
            qlVar = this.gPi.mo17137we(this.gPf.getLineno());
        }
        while (true) {
            int peekToken = peekToken();
            if (peekToken <= 0 || peekToken == 85) {
                return qlVar;
            }
            this.gPi.mo17133m(qlVar, cCq());
        }
        return qlVar;
    }

    private Node cCo() {
        m27134c(86, "msg.no.paren.cond");
        this.gPk.addToken(86);
        Node hh = m27137hh(false);
        m27134c(87, "msg.no.paren.after.cond");
        this.gPk.addToken(87);
        if (hh.getProp(19) == null && (hh.getType() == 8 || hh.getType() == 35 || hh.getType() == 37)) {
            mo16812aA("msg.equal.as.assign", "");
        }
        return hh;
    }

    private Node cCp() {
        Node qlVar = null;
        if (cCh() == 39) {
            cCf();
            String string = this.gPf.getString();
            this.gPk.mo11287nw(string);
            if (this.gPo != null) {
                qlVar = this.gPo.get(string);
            }
            if (qlVar == null) {
                mo16821kC("msg.undef.label");
            }
        }
        return qlVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0024 A[LOOP_START] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private Node cCq() {
        /*
            r4 = this;
            r0 = 0
            a.ql r0 = r4.m27152w(r0)     // Catch:{ a -> 0x001d }
            if (r0 == 0) goto L_0x001e
            a.aKI r1 = r4.f5571he     // Catch:{ a -> 0x001d }
            boolean r1 = r1.dgs()     // Catch:{ a -> 0x001d }
            if (r1 == 0) goto L_0x001c
            boolean r1 = r0.cFT()     // Catch:{ a -> 0x001d }
            if (r1 != 0) goto L_0x001c
            java.lang.String r1 = "msg.no.side.effects"
            java.lang.String r2 = ""
            r4.mo16812aA(r1, r2)     // Catch:{ a -> 0x001d }
        L_0x001c:
            return r0
        L_0x001d:
            r0 = move-exception
        L_0x001e:
            a.al r0 = r4.gPf
            int r0 = r0.getLineno()
        L_0x0024:
            int r1 = r4.cCh()
            r4.cCf()
            switch(r1) {
                case -1: goto L_0x002f;
                case 0: goto L_0x002f;
                case 1: goto L_0x002f;
                case 81: goto L_0x002f;
                default: goto L_0x002e;
            }
        L_0x002e:
            goto L_0x0024
        L_0x002f:
            a.ayw r1 = r4.gPi
            a.ayw r2 = r4.gPi
            java.lang.String r3 = "error"
            a.ql r2 = r2.mo17127kF(r3)
            a.ql r0 = r1.mo17120e(r2, r0)
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6966axf.cCq():a.ql");
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00ff, code lost:
        r3 = r12.gPi.mo17134wb(128);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0107, code lost:
        r4 = peekToken();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x010d, code lost:
        if (r4 == 85) goto L_0x0119;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0111, code lost:
        if (r4 == 114) goto L_0x0119;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0115, code lost:
        if (r4 == 115) goto L_0x0119;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0117, code lost:
        if (r4 != 0) goto L_0x0143;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0143, code lost:
        r12.gPi.mo17133m(r3, cCq());
     */
    /* renamed from: w */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private Node m27152w(Node r13) {
        /*
            r12 = this;
            int r0 = r12.peekToken()
            switch(r0) {
                case -1: goto L_0x058b;
                case 4: goto L_0x0532;
                case 39: goto L_0x061a;
                case 50: goto L_0x03ef;
                case 72: goto L_0x0532;
                case 80: goto L_0x02e9;
                case 81: goto L_0x058b;
                case 84: goto L_0x0551;
                case 108: goto L_0x0598;
                case 111: goto L_0x0035;
                case 113: goto L_0x0084;
                case 115: goto L_0x05a2;
                case 116: goto L_0x014d;
                case 117: goto L_0x0184;
                case 118: goto L_0x01d5;
                case 119: goto L_0x0418;
                case 120: goto L_0x0452;
                case 121: goto L_0x04fb;
                case 122: goto L_0x049c;
                case 152: goto L_0x050a;
                case 153: goto L_0x04fb;
                case 159: goto L_0x0539;
                default: goto L_0x0007;
            }
        L_0x0007:
            a.al r0 = r12.gPf
            int r0 = r0.getLineno()
            r1 = 0
            a.ql r1 = r12.m27137hh(r1)
            a.ayw r2 = r12.gPi
            a.ql r0 = r2.mo17120e(r1, r0)
        L_0x0018:
            int r1 = r12.cCe()
            r2 = 65535(0xffff, float:9.1834E-41)
            r2 = r2 & r1
            switch(r2) {
                case -1: goto L_0x002d;
                case 0: goto L_0x002d;
                case 81: goto L_0x0696;
                case 85: goto L_0x002d;
                default: goto L_0x0023;
            }
        L_0x0023:
            r2 = 65536(0x10000, float:9.18355E-41)
            r1 = r1 & r2
            if (r1 != 0) goto L_0x002d
            java.lang.String r1 = "msg.no.semi.stmt"
            r12.mo16821kC(r1)
        L_0x002d:
            a.aRu r1 = r12.gPk
            r2 = 81
            r1.mo11281Aw(r2)
        L_0x0034:
            return r0
        L_0x0035:
            r12.cCf()
            a.aRu r0 = r12.gPk
            r1 = 111(0x6f, float:1.56E-43)
            r0.addToken(r1)
            a.al r0 = r12.gPf
            int r1 = r0.getLineno()
            a.ql r2 = r12.cCo()
            a.aRu r0 = r12.gPk
            r3 = 84
            r0.mo11281Aw(r3)
            a.ql r3 = r12.cCq()
            r0 = 0
            r4 = 112(0x70, float:1.57E-43)
            boolean r4 = r12.m27150vM(r4)
            if (r4 == 0) goto L_0x0076
            a.aRu r0 = r12.gPk
            r4 = 85
            r0.addToken(r4)
            a.aRu r0 = r12.gPk
            r4 = 112(0x70, float:1.57E-43)
            r0.addToken(r4)
            a.aRu r0 = r12.gPk
            r4 = 84
            r0.mo11281Aw(r4)
            a.ql r0 = r12.cCq()
        L_0x0076:
            a.aRu r4 = r12.gPk
            r5 = 85
            r4.mo11281Aw(r5)
            a.ayw r4 = r12.gPi
            a.ql r0 = r4.mo17112b((org.mozilla.javascript.C3366ql) r2, (org.mozilla.javascript.C3366ql) r3, (org.mozilla.javascript.C3366ql) r0, (int) r1)
            goto L_0x0034
        L_0x0084:
            r12.cCf()
            a.aRu r0 = r12.gPk
            r1 = 113(0x71, float:1.58E-43)
            r0.addToken(r1)
            a.al r0 = r12.gPf
            int r0 = r0.getLineno()
            r1 = 86
            java.lang.String r2 = "msg.no.paren.switch"
            r12.m27134c((int) r1, (java.lang.String) r2)
            a.aRu r1 = r12.gPk
            r2 = 86
            r1.addToken(r2)
            r1 = 0
            a.ql r1 = r12.m27137hh(r1)
            a.ql r0 = r12.m27130b((org.mozilla.javascript.C3366ql) r1, (int) r0)
            r1 = 87
            java.lang.String r2 = "msg.no.paren.after.switch"
            r12.m27134c((int) r1, (java.lang.String) r2)     // Catch:{ all -> 0x011f }
            a.aRu r1 = r12.gPk     // Catch:{ all -> 0x011f }
            r2 = 87
            r1.addToken(r2)     // Catch:{ all -> 0x011f }
            r1 = 84
            java.lang.String r2 = "msg.no.brace.switch"
            r12.m27134c((int) r1, (java.lang.String) r2)     // Catch:{ all -> 0x011f }
            a.aRu r1 = r12.gPk     // Catch:{ all -> 0x011f }
            r2 = 84
            r1.mo11281Aw(r2)     // Catch:{ all -> 0x011f }
            r2 = 0
        L_0x00c8:
            int r1 = r12.nextToken()     // Catch:{ all -> 0x011f }
            switch(r1) {
                case 85: goto L_0x00d4;
                case 114: goto L_0x00e5;
                case 115: goto L_0x0124;
                default: goto L_0x00cf;
            }     // Catch:{ all -> 0x011f }
        L_0x00cf:
            java.lang.String r1 = "msg.bad.switch"
            r12.mo16821kC(r1)     // Catch:{ all -> 0x011f }
        L_0x00d4:
            a.aRu r1 = r12.gPk     // Catch:{ all -> 0x011f }
            r2 = 85
            r1.mo11281Aw(r2)     // Catch:{ all -> 0x011f }
            a.ayw r1 = r12.gPi     // Catch:{ all -> 0x011f }
            r1.mo17139y(r0)     // Catch:{ all -> 0x011f }
            r12.cCl()
            goto L_0x0034
        L_0x00e5:
            a.aRu r1 = r12.gPk     // Catch:{ all -> 0x011f }
            r3 = 114(0x72, float:1.6E-43)
            r1.addToken(r3)     // Catch:{ all -> 0x011f }
            r1 = 0
            a.ql r1 = r12.m27137hh(r1)     // Catch:{ all -> 0x011f }
            r3 = 102(0x66, float:1.43E-43)
            java.lang.String r4 = "msg.no.colon.case"
            r12.m27134c((int) r3, (java.lang.String) r4)     // Catch:{ all -> 0x011f }
            a.aRu r3 = r12.gPk     // Catch:{ all -> 0x011f }
            r4 = 102(0x66, float:1.43E-43)
            r3.mo11281Aw(r4)     // Catch:{ all -> 0x011f }
        L_0x00ff:
            a.ayw r3 = r12.gPi     // Catch:{ all -> 0x011f }
            r4 = 128(0x80, float:1.794E-43)
            a.ql r3 = r3.mo17134wb(r4)     // Catch:{ all -> 0x011f }
        L_0x0107:
            int r4 = r12.peekToken()     // Catch:{ all -> 0x011f }
            r5 = 85
            if (r4 == r5) goto L_0x0119
            r5 = 114(0x72, float:1.6E-43)
            if (r4 == r5) goto L_0x0119
            r5 = 115(0x73, float:1.61E-43)
            if (r4 == r5) goto L_0x0119
            if (r4 != 0) goto L_0x0143
        L_0x0119:
            a.ayw r4 = r12.gPi     // Catch:{ all -> 0x011f }
            r4.mo17106a((org.mozilla.javascript.C3366ql) r0, (org.mozilla.javascript.C3366ql) r1, (org.mozilla.javascript.C3366ql) r3)     // Catch:{ all -> 0x011f }
            goto L_0x00c8
        L_0x011f:
            r0 = move-exception
            r12.cCl()
            throw r0
        L_0x0124:
            if (r2 == 0) goto L_0x012b
            java.lang.String r1 = "msg.double.switch.default"
            r12.mo16821kC(r1)     // Catch:{ all -> 0x011f }
        L_0x012b:
            a.aRu r1 = r12.gPk     // Catch:{ all -> 0x011f }
            r2 = 115(0x73, float:1.61E-43)
            r1.addToken(r2)     // Catch:{ all -> 0x011f }
            r2 = 1
            r1 = 0
            r3 = 102(0x66, float:1.43E-43)
            java.lang.String r4 = "msg.no.colon.case"
            r12.m27134c((int) r3, (java.lang.String) r4)     // Catch:{ all -> 0x011f }
            a.aRu r3 = r12.gPk     // Catch:{ all -> 0x011f }
            r4 = 102(0x66, float:1.43E-43)
            r3.mo11281Aw(r4)     // Catch:{ all -> 0x011f }
            goto L_0x00ff
        L_0x0143:
            a.ayw r4 = r12.gPi     // Catch:{ all -> 0x011f }
            a.ql r5 = r12.cCq()     // Catch:{ all -> 0x011f }
            r4.mo17133m((org.mozilla.javascript.C3366ql) r3, (org.mozilla.javascript.C3366ql) r5)     // Catch:{ all -> 0x011f }
            goto L_0x0107
        L_0x014d:
            r12.cCf()
            a.aRu r0 = r12.gPk
            r1 = 116(0x74, float:1.63E-43)
            r0.addToken(r1)
            r0 = 1
            a.ql r0 = r12.m27131b((org.mozilla.javascript.C3366ql) r13, (boolean) r0)
            a.ql r1 = r12.cCo()     // Catch:{ all -> 0x017e }
            a.aRu r2 = r12.gPk     // Catch:{ all -> 0x017e }
            r3 = 84
            r2.mo11281Aw(r3)     // Catch:{ all -> 0x017e }
            a.ql r2 = r12.cCq()     // Catch:{ all -> 0x017e }
            a.aRu r3 = r12.gPk     // Catch:{ all -> 0x017e }
            r4 = 85
            r3.mo11281Aw(r4)     // Catch:{ all -> 0x017e }
            a.ayw r3 = r12.gPi     // Catch:{ all -> 0x017e }
            a.ql r0 = r3.mo17111b((org.mozilla.javascript.C3366ql) r0, (org.mozilla.javascript.C3366ql) r1, (org.mozilla.javascript.C3366ql) r2)     // Catch:{ all -> 0x017e }
            r1 = 1
            r12.m27135hf(r1)
            goto L_0x0034
        L_0x017e:
            r0 = move-exception
            r1 = 1
            r12.m27135hf(r1)
            throw r0
        L_0x0184:
            r12.cCf()
            a.aRu r0 = r12.gPk
            r1 = 117(0x75, float:1.64E-43)
            r0.addToken(r1)
            a.aRu r0 = r12.gPk
            r1 = 84
            r0.mo11281Aw(r1)
            r0 = 1
            a.ql r0 = r12.m27131b((org.mozilla.javascript.C3366ql) r13, (boolean) r0)
            a.ql r1 = r12.cCq()     // Catch:{ all -> 0x01cf }
            a.aRu r2 = r12.gPk     // Catch:{ all -> 0x01cf }
            r3 = 85
            r2.addToken(r3)     // Catch:{ all -> 0x01cf }
            r2 = 116(0x74, float:1.63E-43)
            java.lang.String r3 = "msg.no.while.do"
            r12.m27134c((int) r2, (java.lang.String) r3)     // Catch:{ all -> 0x01cf }
            a.aRu r2 = r12.gPk     // Catch:{ all -> 0x01cf }
            r3 = 116(0x74, float:1.63E-43)
            r2.addToken(r3)     // Catch:{ all -> 0x01cf }
            a.ql r2 = r12.cCo()     // Catch:{ all -> 0x01cf }
            a.ayw r3 = r12.gPi     // Catch:{ all -> 0x01cf }
            a.ql r0 = r3.mo17114c((org.mozilla.javascript.C3366ql) r0, (org.mozilla.javascript.C3366ql) r1, (org.mozilla.javascript.C3366ql) r2)     // Catch:{ all -> 0x01cf }
            r1 = 1
            r12.m27135hf(r1)
            r1 = 81
            r12.m27150vM(r1)
            a.aRu r1 = r12.gPk
            r2 = 81
            r1.mo11281Aw(r2)
            goto L_0x0034
        L_0x01cf:
            r0 = move-exception
            r1 = 1
            r12.m27135hf(r1)
            throw r0
        L_0x01d5:
            r12.cCf()
            r6 = 0
            a.aRu r0 = r12.gPk
            r1 = 118(0x76, float:1.65E-43)
            r0.addToken(r1)
            r0 = 1
            a.ql r2 = r12.m27131b((org.mozilla.javascript.C3366ql) r13, (boolean) r0)
            r10 = 0
            r1 = -1
            r0 = 39
            boolean r0 = r12.m27150vM(r0)     // Catch:{ all -> 0x026f }
            if (r0 == 0) goto L_0x0209
            a.aRu r0 = r12.gPk     // Catch:{ all -> 0x026f }
            a.al r3 = r12.gPf     // Catch:{ all -> 0x026f }
            java.lang.String r3 = r3.getString()     // Catch:{ all -> 0x026f }
            r0.mo11287nw(r3)     // Catch:{ all -> 0x026f }
            a.al r0 = r12.gPf     // Catch:{ all -> 0x026f }
            java.lang.String r0 = r0.getString()     // Catch:{ all -> 0x026f }
            java.lang.String r3 = "each"
            boolean r0 = r0.equals(r3)     // Catch:{ all -> 0x026f }
            if (r0 == 0) goto L_0x0269
            r6 = 1
        L_0x0209:
            r0 = 86
            java.lang.String r3 = "msg.no.paren.for"
            r12.m27134c((int) r0, (java.lang.String) r3)     // Catch:{ all -> 0x026f }
            a.aRu r0 = r12.gPk     // Catch:{ all -> 0x026f }
            r3 = 86
            r0.addToken(r3)     // Catch:{ all -> 0x026f }
            int r0 = r12.peekToken()     // Catch:{ all -> 0x026f }
            r3 = 81
            if (r0 != r3) goto L_0x0275
            a.ayw r0 = r12.gPi     // Catch:{ all -> 0x026f }
            r3 = 127(0x7f, float:1.78E-43)
            a.ql r3 = r0.mo17134wb(r3)     // Catch:{ all -> 0x026f }
        L_0x0227:
            r0 = 52
            boolean r0 = r12.m27150vM(r0)     // Catch:{ all -> 0x026f }
            if (r0 == 0) goto L_0x0292
            a.aRu r0 = r12.gPk     // Catch:{ all -> 0x026f }
            r4 = 52
            r0.addToken(r4)     // Catch:{ all -> 0x026f }
            r0 = 0
            a.ql r4 = r12.m27137hh(r0)     // Catch:{ all -> 0x026f }
        L_0x023b:
            r0 = 87
            java.lang.String r5 = "msg.no.paren.for.ctrl"
            r12.m27134c((int) r0, (java.lang.String) r5)     // Catch:{ all -> 0x026f }
            a.aRu r0 = r12.gPk     // Catch:{ all -> 0x026f }
            r5 = 87
            r0.addToken(r5)     // Catch:{ all -> 0x026f }
            a.aRu r0 = r12.gPk     // Catch:{ all -> 0x026f }
            r5 = 84
            r0.mo11281Aw(r5)     // Catch:{ all -> 0x026f }
            a.ql r5 = r12.cCq()     // Catch:{ all -> 0x026f }
            a.aRu r0 = r12.gPk     // Catch:{ all -> 0x026f }
            r7 = 85
            r0.mo11281Aw(r7)     // Catch:{ all -> 0x026f }
            if (r10 != 0) goto L_0x02dd
            a.ayw r0 = r12.gPi     // Catch:{ all -> 0x026f }
            a.ql r0 = r0.mo17094a((int) r1, (org.mozilla.javascript.C3366ql) r2, (org.mozilla.javascript.C3366ql) r3, (org.mozilla.javascript.C3366ql) r4, (org.mozilla.javascript.C3366ql) r5, (boolean) r6)     // Catch:{ all -> 0x026f }
        L_0x0263:
            r1 = 1
            r12.m27135hf(r1)
            goto L_0x0034
        L_0x0269:
            java.lang.String r0 = "msg.no.paren.for"
            r12.mo16821kC(r0)     // Catch:{ all -> 0x026f }
            goto L_0x0209
        L_0x026f:
            r0 = move-exception
            r1 = 1
            r12.m27135hf(r1)
            throw r0
        L_0x0275:
            r3 = 121(0x79, float:1.7E-43)
            if (r0 == r3) goto L_0x027d
            r3 = 152(0x98, float:2.13E-43)
            if (r0 != r3) goto L_0x028c
        L_0x027d:
            r12.cCf()     // Catch:{ all -> 0x026f }
            a.aRu r1 = r12.gPk     // Catch:{ all -> 0x026f }
            r1.addToken(r0)     // Catch:{ all -> 0x026f }
            r1 = 1
            a.ql r3 = r12.m27132b((boolean) r1, (int) r0)     // Catch:{ all -> 0x026f }
            r1 = r0
            goto L_0x0227
        L_0x028c:
            r0 = 1
            a.ql r3 = r12.m27137hh(r0)     // Catch:{ all -> 0x026f }
            goto L_0x0227
        L_0x0292:
            r0 = 81
            java.lang.String r4 = "msg.no.semi.for"
            r12.m27134c((int) r0, (java.lang.String) r4)     // Catch:{ all -> 0x026f }
            a.aRu r0 = r12.gPk     // Catch:{ all -> 0x026f }
            r4 = 81
            r0.addToken(r4)     // Catch:{ all -> 0x026f }
            int r0 = r12.peekToken()     // Catch:{ all -> 0x026f }
            r4 = 81
            if (r0 != r4) goto L_0x02d0
            a.ayw r0 = r12.gPi     // Catch:{ all -> 0x026f }
            r4 = 127(0x7f, float:1.78E-43)
            a.ql r4 = r0.mo17134wb(r4)     // Catch:{ all -> 0x026f }
        L_0x02b0:
            r0 = 81
            java.lang.String r5 = "msg.no.semi.for.cond"
            r12.m27134c((int) r0, (java.lang.String) r5)     // Catch:{ all -> 0x026f }
            a.aRu r0 = r12.gPk     // Catch:{ all -> 0x026f }
            r5 = 81
            r0.addToken(r5)     // Catch:{ all -> 0x026f }
            int r0 = r12.peekToken()     // Catch:{ all -> 0x026f }
            r5 = 87
            if (r0 != r5) goto L_0x02d6
            a.ayw r0 = r12.gPi     // Catch:{ all -> 0x026f }
            r5 = 127(0x7f, float:1.78E-43)
            a.ql r10 = r0.mo17134wb(r5)     // Catch:{ all -> 0x026f }
            goto L_0x023b
        L_0x02d0:
            r0 = 0
            a.ql r4 = r12.m27137hh(r0)     // Catch:{ all -> 0x026f }
            goto L_0x02b0
        L_0x02d6:
            r0 = 0
            a.ql r10 = r12.m27137hh(r0)     // Catch:{ all -> 0x026f }
            goto L_0x023b
        L_0x02dd:
            a.ayw r6 = r12.gPi     // Catch:{ all -> 0x026f }
            r7 = r2
            r8 = r3
            r9 = r4
            r11 = r5
            a.ql r0 = r6.mo17101a(r7, r8, r9, r10, r11)     // Catch:{ all -> 0x026f }
            goto L_0x0263
        L_0x02e9:
            r12.cCf()
            a.al r0 = r12.gPf
            int r3 = r0.getLineno()
            r2 = 0
            a.aRu r0 = r12.gPk
            r1 = 80
            r0.addToken(r1)
            int r0 = r12.peekToken()
            r1 = 84
            if (r0 == r1) goto L_0x0307
            java.lang.String r0 = "msg.no.brace.try"
            r12.mo16821kC(r0)
        L_0x0307:
            a.aRu r0 = r12.gPk
            r1 = 84
            r0.mo11281Aw(r1)
            a.ql r4 = r12.cCq()
            a.aRu r0 = r12.gPk
            r1 = 85
            r0.mo11281Aw(r1)
            a.ayw r0 = r12.gPi
            r1 = 128(0x80, float:1.794E-43)
            a.ql r5 = r0.mo17134wb(r1)
            r0 = 0
            int r1 = r12.peekToken()
            r6 = 123(0x7b, float:1.72E-43)
            if (r1 != r6) goto L_0x03e2
        L_0x032a:
            r1 = 123(0x7b, float:1.72E-43)
            boolean r1 = r12.m27150vM(r1)
            if (r1 != 0) goto L_0x035b
        L_0x0332:
            r0 = 124(0x7c, float:1.74E-43)
            boolean r0 = r12.m27150vM(r0)
            if (r0 == 0) goto L_0x069e
            a.aRu r0 = r12.gPk
            r1 = 124(0x7c, float:1.74E-43)
            r0.addToken(r1)
            a.aRu r0 = r12.gPk
            r1 = 84
            r0.mo11281Aw(r1)
            a.ql r0 = r12.cCq()
            a.aRu r1 = r12.gPk
            r2 = 85
            r1.mo11281Aw(r2)
        L_0x0353:
            a.ayw r1 = r12.gPi
            a.ql r0 = r1.mo17100a((org.mozilla.javascript.C3366ql) r4, (org.mozilla.javascript.C3366ql) r5, (org.mozilla.javascript.C3366ql) r0, (int) r3)
            goto L_0x0034
        L_0x035b:
            if (r0 == 0) goto L_0x0362
            java.lang.String r1 = "msg.catch.unreachable"
            r12.mo16821kC(r1)
        L_0x0362:
            a.aRu r1 = r12.gPk
            r6 = 123(0x7b, float:1.72E-43)
            r1.addToken(r6)
            r1 = 86
            java.lang.String r6 = "msg.no.paren.catch"
            r12.m27134c((int) r1, (java.lang.String) r6)
            a.aRu r1 = r12.gPk
            r6 = 86
            r1.addToken(r6)
            r1 = 39
            java.lang.String r6 = "msg.bad.catchcond"
            r12.m27134c((int) r1, (java.lang.String) r6)
            a.al r1 = r12.gPf
            java.lang.String r6 = r1.getString()
            a.aRu r1 = r12.gPk
            r1.mo11287nw(r6)
            r1 = 0
            r7 = 111(0x6f, float:1.56E-43)
            boolean r7 = r12.m27150vM(r7)
            if (r7 == 0) goto L_0x03e0
            a.aRu r1 = r12.gPk
            r7 = 111(0x6f, float:1.56E-43)
            r1.addToken(r7)
            r1 = 0
            a.ql r1 = r12.m27137hh(r1)
        L_0x039e:
            r7 = 87
            java.lang.String r8 = "msg.bad.catchcond"
            r12.m27134c((int) r7, (java.lang.String) r8)
            a.aRu r7 = r12.gPk
            r8 = 87
            r7.addToken(r8)
            r7 = 84
            java.lang.String r8 = "msg.no.brace.catchblock"
            r12.m27134c((int) r7, (java.lang.String) r8)
            a.aRu r7 = r12.gPk
            r8 = 84
            r7.mo11281Aw(r8)
            a.ayw r7 = r12.gPi
            a.ayw r8 = r12.gPi
            r9 = 0
            a.ql r9 = r12.m27149v(r9)
            a.al r10 = r12.gPf
            int r10 = r10.getLineno()
            a.ql r1 = r8.mo17104a((java.lang.String) r6, (org.mozilla.javascript.C3366ql) r1, (org.mozilla.javascript.C3366ql) r9, (int) r10)
            r7.mo17133m((org.mozilla.javascript.C3366ql) r5, (org.mozilla.javascript.C3366ql) r1)
            r1 = 85
            java.lang.String r6 = "msg.no.brace.after.body"
            r12.m27134c((int) r1, (java.lang.String) r6)
            a.aRu r1 = r12.gPk
            r6 = 85
            r1.mo11281Aw(r6)
            goto L_0x032a
        L_0x03e0:
            r0 = 1
            goto L_0x039e
        L_0x03e2:
            r0 = 124(0x7c, float:1.74E-43)
            if (r1 == r0) goto L_0x0332
            r0 = 124(0x7c, float:1.74E-43)
            java.lang.String r1 = "msg.try.no.catchfinally"
            r12.m27134c((int) r0, (java.lang.String) r1)
            goto L_0x0332
        L_0x03ef:
            r12.cCf()
            int r0 = r12.cCh()
            r1 = 1
            if (r0 != r1) goto L_0x03fe
            java.lang.String r0 = "msg.bad.throw.eol"
            r12.mo16821kC(r0)
        L_0x03fe:
            a.al r0 = r12.gPf
            int r0 = r0.getLineno()
            a.aRu r1 = r12.gPk
            r2 = 50
            r1.addToken(r2)
            a.ayw r1 = r12.gPi
            r2 = 0
            a.ql r2 = r12.m27137hh(r2)
            a.ql r0 = r1.mo17123h(r2, r0)
            goto L_0x0018
        L_0x0418:
            r12.cCf()
            a.al r0 = r12.gPf
            int r1 = r0.getLineno()
            a.aRu r0 = r12.gPk
            r2 = 119(0x77, float:1.67E-43)
            r0.addToken(r2)
            a.ql r0 = r12.cCp()
            if (r0 != 0) goto L_0x044a
            a.aCx r0 = r12.gPq
            if (r0 == 0) goto L_0x043a
            a.aCx r0 = r12.gPq
            int r0 = r0.size()
            if (r0 != 0) goto L_0x0442
        L_0x043a:
            java.lang.String r0 = "msg.bad.break"
            r12.mo16821kC(r0)
            r0 = 0
            goto L_0x0034
        L_0x0442:
            a.aCx r0 = r12.gPq
            java.lang.Object r0 = r0.peek()
            a.ql r0 = (org.mozilla.javascript.C3366ql) r0
        L_0x044a:
            a.ayw r2 = r12.gPi
            a.ql r0 = r2.mo17125j(r0, r1)
            goto L_0x0018
        L_0x0452:
            r12.cCf()
            a.al r0 = r12.gPf
            int r1 = r0.getLineno()
            a.aRu r0 = r12.gPk
            r2 = 120(0x78, float:1.68E-43)
            r0.addToken(r2)
            a.ql r0 = r12.cCp()
            if (r0 != 0) goto L_0x048c
            a.aCx r0 = r12.gPp
            if (r0 == 0) goto L_0x0474
            a.aCx r0 = r12.gPp
            int r0 = r0.size()
            if (r0 != 0) goto L_0x047c
        L_0x0474:
            java.lang.String r0 = "msg.continue.outside"
            r12.mo16821kC(r0)
            r0 = 0
            goto L_0x0034
        L_0x047c:
            a.aCx r0 = r12.gPp
            java.lang.Object r0 = r0.peek()
            a.ql r0 = (org.mozilla.javascript.C3366ql) r0
        L_0x0484:
            a.ayw r2 = r12.gPi
            a.ql r0 = r2.mo17126k(r0, r1)
            goto L_0x0018
        L_0x048c:
            a.ayw r2 = r12.gPi
            a.ql r0 = r2.mo17140z(r0)
            if (r0 != 0) goto L_0x0484
            java.lang.String r0 = "msg.continue.nonloop"
            r12.mo16821kC(r0)
            r0 = 0
            goto L_0x0034
        L_0x049c:
            r12.cCf()
            a.aRu r0 = r12.gPk
            r1 = 122(0x7a, float:1.71E-43)
            r0.addToken(r1)
            a.al r0 = r12.gPf
            int r0 = r0.getLineno()
            r1 = 86
            java.lang.String r2 = "msg.no.paren.with"
            r12.m27134c((int) r1, (java.lang.String) r2)
            a.aRu r1 = r12.gPk
            r2 = 86
            r1.addToken(r2)
            r1 = 0
            a.ql r1 = r12.m27137hh(r1)
            r2 = 87
            java.lang.String r3 = "msg.no.paren.after.with"
            r12.m27134c((int) r2, (java.lang.String) r3)
            a.aRu r2 = r12.gPk
            r3 = 87
            r2.addToken(r3)
            a.aRu r2 = r12.gPk
            r3 = 84
            r2.mo11281Aw(r3)
            int r2 = r12.gPn
            int r2 = r2 + 1
            r12.gPn = r2
            a.ql r2 = r12.cCq()     // Catch:{ all -> 0x04f3 }
            int r3 = r12.gPn
            int r3 = r3 + -1
            r12.gPn = r3
            a.aRu r3 = r12.gPk
            r4 = 85
            r3.mo11281Aw(r4)
            a.ayw r3 = r12.gPi
            a.ql r0 = r3.mo17099a((org.mozilla.javascript.C3366ql) r1, (org.mozilla.javascript.C3366ql) r2, (int) r0)
            goto L_0x0034
        L_0x04f3:
            r0 = move-exception
            int r1 = r12.gPn
            int r1 = r1 + -1
            r12.gPn = r1
            throw r0
        L_0x04fb:
            r12.cCf()
            a.aRu r1 = r12.gPk
            r1.addToken(r0)
            r1 = 0
            a.ql r0 = r12.m27132b((boolean) r1, (int) r0)
            goto L_0x0018
        L_0x050a:
            r12.cCf()
            a.aRu r1 = r12.gPk
            r2 = 152(0x98, float:2.13E-43)
            r1.addToken(r2)
            int r1 = r12.peekToken()
            r2 = 86
            if (r1 != r2) goto L_0x0523
            r0 = 1
            a.ql r0 = r12.m27136hg(r0)
            goto L_0x0034
        L_0x0523:
            r1 = 0
            a.ql r0 = r12.m27132b((boolean) r1, (int) r0)
            int r1 = r12.peekToken()
            r2 = 81
            if (r1 != r2) goto L_0x0034
            goto L_0x0018
        L_0x0532:
            r1 = 0
            a.ql r0 = r12.m27148j(r0, r1)
            goto L_0x0018
        L_0x0539:
            r12.cCf()
            a.aRu r0 = r12.gPk
            r1 = 159(0x9f, float:2.23E-43)
            r0.addToken(r1)
            a.ayw r0 = r12.gPi
            a.al r1 = r12.gPf
            int r1 = r1.getLineno()
            a.ql r0 = r0.mo17135wc(r1)
            goto L_0x0018
        L_0x0551:
            r12.cCf()
            if (r13 == 0) goto L_0x055d
            a.aRu r0 = r12.gPk
            r1 = 84
            r0.addToken(r1)
        L_0x055d:
            a.ayw r0 = r12.gPi
            r1 = 128(0x80, float:1.794E-43)
            a.al r2 = r12.gPf
            int r2 = r2.getLineno()
            a.ql r0 = r0.mo17108am(r1, r2)
            r12.mo16823u(r0)
            r12.m27149v(r0)     // Catch:{ all -> 0x0586 }
            r1 = 85
            java.lang.String r2 = "msg.no.brace.block"
            r12.m27134c((int) r1, (java.lang.String) r2)     // Catch:{ all -> 0x0586 }
            if (r13 == 0) goto L_0x0581
            a.aRu r1 = r12.gPk     // Catch:{ all -> 0x0586 }
            r2 = 85
            r1.mo11281Aw(r2)     // Catch:{ all -> 0x0586 }
        L_0x0581:
            r12.popScope()
            goto L_0x0034
        L_0x0586:
            r0 = move-exception
            r12.popScope()
            throw r0
        L_0x058b:
            r12.cCf()
            a.ayw r0 = r12.gPi
            r1 = 127(0x7f, float:1.78E-43)
            a.ql r0 = r0.mo17134wb(r1)
            goto L_0x0034
        L_0x0598:
            r12.cCf()
            r0 = 3
            a.ql r0 = r12.m27151vN(r0)
            goto L_0x0034
        L_0x05a2:
            r12.cCf()
            r12.cCj()
            a.aRu r0 = r12.gPk
            r1 = 115(0x73, float:1.61E-43)
            r0.addToken(r1)
            a.al r0 = r12.gPf
            int r0 = r0.getLineno()
            r1 = 39
            boolean r1 = r12.m27150vM(r1)
            if (r1 == 0) goto L_0x05cb
            a.al r1 = r12.gPf
            java.lang.String r1 = r1.getString()
            java.lang.String r2 = "xml"
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x05d0
        L_0x05cb:
            java.lang.String r1 = "msg.bad.namespace"
            r12.mo16821kC(r1)
        L_0x05d0:
            a.aRu r1 = r12.gPk
            java.lang.String r2 = " xml"
            r1.mo11287nw(r2)
            r1 = 39
            boolean r1 = r12.m27150vM(r1)
            if (r1 == 0) goto L_0x05ed
            a.al r1 = r12.gPf
            java.lang.String r1 = r1.getString()
            java.lang.String r2 = "namespace"
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x05f2
        L_0x05ed:
            java.lang.String r1 = "msg.bad.namespace"
            r12.mo16821kC(r1)
        L_0x05f2:
            a.aRu r1 = r12.gPk
            java.lang.String r2 = " namespace"
            r1.mo11287nw(r2)
            r1 = 89
            boolean r1 = r12.m27150vM(r1)
            if (r1 != 0) goto L_0x0606
            java.lang.String r1 = "msg.bad.namespace"
            r12.mo16821kC(r1)
        L_0x0606:
            a.aRu r1 = r12.gPk
            r2 = 89
            r1.addToken(r2)
            r1 = 0
            a.ql r1 = r12.m27137hh(r1)
            a.ayw r2 = r12.gPi
            a.ql r0 = r2.mo17122g(r1, r0)
            goto L_0x0018
        L_0x061a:
            a.al r0 = r12.gPf
            int r0 = r0.getLineno()
            a.al r1 = r12.gPf
            java.lang.String r2 = r1.getString()
            r12.cCi()
            r1 = 0
            a.ql r1 = r12.m27137hh(r1)
            int r3 = r1.getType()
            r4 = 129(0x81, float:1.81E-43)
            if (r3 == r4) goto L_0x063e
            a.ayw r2 = r12.gPi
            a.ql r0 = r2.mo17120e(r1, r0)
            goto L_0x0018
        L_0x063e:
            int r0 = r12.peekToken()
            r3 = 102(0x66, float:1.43E-43)
            if (r0 == r3) goto L_0x0649
            p001a.C1520WN.codeBug()
        L_0x0649:
            r12.cCf()
            a.aRu r0 = r12.gPk
            r0.mo11287nw(r2)
            a.aRu r0 = r12.gPk
            r3 = 102(0x66, float:1.43E-43)
            r0.mo11281Aw(r3)
            java.util.Map<java.lang.String, a.ql> r0 = r12.gPo
            if (r0 != 0) goto L_0x067f
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r12.gPo = r0
        L_0x0663:
            if (r13 != 0) goto L_0x068d
            r0 = 1
            r13 = r1
        L_0x0667:
            java.util.Map<java.lang.String, a.ql> r1 = r12.gPo
            r1.put(r2, r13)
            a.ql r1 = r12.m27152w(r13)     // Catch:{ all -> 0x068f }
            java.util.Map<java.lang.String, a.ql> r3 = r12.gPo
            r3.remove(r2)
            if (r0 == 0) goto L_0x069b
            a.ayw r0 = r12.gPi
            a.ql r0 = r0.mo17131l((org.mozilla.javascript.C3366ql) r13, (org.mozilla.javascript.C3366ql) r1)
            goto L_0x0034
        L_0x067f:
            java.util.Map<java.lang.String, a.ql> r0 = r12.gPo
            boolean r0 = r0.containsKey(r2)
            if (r0 == 0) goto L_0x0663
            java.lang.String r0 = "msg.dup.label"
            r12.mo16821kC(r0)
            goto L_0x0663
        L_0x068d:
            r0 = 0
            goto L_0x0667
        L_0x068f:
            r0 = move-exception
            java.util.Map<java.lang.String, a.ql> r1 = r12.gPo
            r1.remove(r2)
            throw r0
        L_0x0696:
            r12.cCf()
            goto L_0x002d
        L_0x069b:
            r0 = r1
            goto L_0x0034
        L_0x069e:
            r0 = r2
            goto L_0x0353
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6966axf.m27152w(a.ql):a.ql");
    }

    /* renamed from: j */
    private Node m27148j(int i, boolean z) {
        Node qlVar;
        Node m;
        String str;
        if (!cCk()) {
            if (i == 4) {
                str = "msg.bad.return";
            } else {
                str = "msg.bad.yield";
            }
            mo16821kC(str);
        }
        cCf();
        this.gPk.addToken(i);
        int lineno = this.gPf.getLineno();
        switch (cCh()) {
            case -1:
            case 0:
            case 1:
            case 72:
            case 81:
            case 83:
            case 85:
            case 87:
                qlVar = null;
                break;
            default:
                qlVar = m27137hh(false);
                break;
        }
        int i2 = this.gPr;
        if (i == 4) {
            if (qlVar == null) {
                this.gPr |= 2;
            } else {
                this.gPr |= 4;
            }
            Node i3 = this.gPi.mo17124i(qlVar, lineno);
            if (m27154x(i2, this.gPr, 6)) {
                mo16812aA("msg.return.inconsistent", "");
                m = i3;
            } else {
                m = i3;
            }
        } else {
            this.gPr |= 8;
            m = this.gPi.mo17132m(qlVar, lineno);
            if (!z) {
                m = new Node(132, m, lineno);
            }
        }
        if (m27154x(i2, this.gPr, 12)) {
            String functionName = ((FunctionNode) this.gPl).getFunctionName();
            if (functionName.length() == 0) {
                mo16814aC("msg.anon.generator.returns", "");
            } else {
                mo16814aC("msg.generator.returns", functionName);
            }
        }
        return m;
    }

    /* renamed from: b */
    private Node m27132b(boolean z, int i) {
        Node qlVar;
        String str;
        Node qlVar2;
        Node al = this.gPi.mo17107al(i, this.gPf.getLineno());
        boolean z2 = true;
        do {
            int peekToken = peekToken();
            if (peekToken == 82 || peekToken == 84) {
                qlVar = cCw();
                str = null;
            } else {
                m27134c(39, "msg.bad.var");
                str = this.gPf.getString();
                if (!z2) {
                    this.gPk.addToken(88);
                }
                z2 = false;
                this.gPk.mo11287nw(str);
                mo16811a(i, z, str);
                qlVar = null;
            }
            if (m27150vM(89)) {
                this.gPk.addToken(89);
                qlVar2 = m27138hi(z);
            } else {
                qlVar2 = null;
            }
            if (qlVar == null) {
                Node kF = this.gPi.mo17127kF(str);
                if (qlVar2 != null) {
                    this.gPi.mo17133m(kF, qlVar2);
                }
                this.gPi.mo17133m(al, kF);
            } else if (qlVar2 == null) {
                if (!z) {
                    mo16821kC("msg.destruct.assign.no.init");
                }
                this.gPi.mo17133m(al, qlVar);
            } else {
                this.gPi.mo17133m(al, this.gPi.mo17119e(i, qlVar, qlVar2));
            }
        } while (m27150vM(88));
        return al;
    }

    /* renamed from: hg */
    private Node m27136hg(boolean z) {
        m27134c(86, "msg.no.paren.after.let");
        this.gPk.addToken(86);
        Node am = this.gPi.mo17108am(152, this.gPf.getLineno());
        mo16823u(am);
        try {
            this.gPi.mo17133m(am, m27132b(false, 152));
            m27134c(87, "msg.no.paren.let");
            this.gPk.addToken(87);
            if (!z || peekToken() != 84) {
                am.setType(157);
                this.gPi.mo17133m(am, m27137hh(false));
                if (z) {
                    am = this.gPi.mo17120e(am, this.gPf.getLineno());
                }
            } else {
                cCf();
                this.gPk.mo11281Aw(84);
                this.gPi.mo17133m(am, m27149v((Node) null));
                m27134c(85, "msg.no.curly.let");
                this.gPk.addToken(85);
            }
            return am;
        } finally {
            popScope();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo16811a(int i, boolean z, String str) {
        Node.C3370d dVar;
        String str2;
        boolean z2 = true;
        boolean z3 = false;
        Node.Scope iL = this.gPm.mo21509iL(str);
        if (iL != null) {
            dVar = iL.mo21510iM(str);
        } else {
            dVar = null;
        }
        if (dVar == null || !(dVar.heu == 153 || i == 153)) {
            switch (i) {
                case 86:
                    if (dVar != null) {
                        mo16813aB("msg.dup.parms", str);
                    }
                    this.gPl.mo21505a(str, new Node.C3370d(i, str));
                    break;
                case 108:
                case 121:
                case 153:
                    if (dVar != null) {
                        if (dVar.heu != 121) {
                            if (dVar.heu == 86) {
                                mo16812aA("msg.var.hides.arg", str);
                                break;
                            }
                        } else {
                            mo16812aA("msg.var.redecl", str);
                            break;
                        }
                    } else {
                        this.gPl.mo21505a(str, new Node.C3370d(i, str));
                        break;
                    }
                    break;
                case 152:
                    if (dVar != null && iL == this.gPm) {
                        if (dVar.heu != 152) {
                            z2 = false;
                        }
                        z3 = z2;
                    }
                    int type = this.gPm.getType();
                    if (!z && (type == 131 || type == 111)) {
                        mo16820kB("msg.let.decl.not.in.block");
                    }
                    this.gPm.mo21505a(str, new Node.C3370d(i, str));
                    break;
                default:
                    throw Kit.codeBug();
            }
        } else {
            z3 = true;
        }
        if (z3) {
            if (dVar.heu == 153) {
                str2 = "msg.const.redecl";
            } else if (dVar.heu == 152) {
                str2 = "msg.let.redecl";
            } else if (dVar.heu == 121) {
                str2 = "msg.var.redecl";
            } else if (dVar.heu == 108) {
                str2 = "msg.fn.redecl";
            } else {
                str2 = "msg.parm.redecl";
            }
            mo16814aC(str2, str);
        }
    }

    /* renamed from: hh */
    private Node m27137hh(boolean z) {
        Node hi = m27138hi(z);
        while (m27150vM(88)) {
            this.gPk.addToken(88);
            if (this.f5571he.dgs() && !hi.cFT()) {
                mo16812aA("msg.no.side.effects", "");
            }
            if (peekToken() == 72) {
                mo16821kC("msg.yield.parenthesized");
            }
            hi = this.gPi.mo17113c(88, hi, m27138hi(z));
        }
        return hi;
    }

    /* renamed from: hi */
    private Node m27138hi(boolean z) {
        int peekToken = peekToken();
        if (peekToken == 72) {
            cCf();
            return m27148j(peekToken, true);
        }
        Node hj = m27139hj(z);
        int peekToken2 = peekToken();
        if (89 > peekToken2 || peekToken2 > 100) {
            return hj;
        }
        cCf();
        this.gPk.addToken(peekToken2);
        return this.gPi.mo17116d(peekToken2, hj, m27138hi(z));
    }

    /* renamed from: hj */
    private Node m27139hj(boolean z) {
        Node hk = m27140hk(z);
        if (!m27150vM(101)) {
            return hk;
        }
        this.gPk.addToken(101);
        Node hi = m27138hi(false);
        m27134c(102, "msg.no.colon.cond");
        this.gPk.addToken(102);
        return this.gPi.mo17118d(hk, hi, m27138hi(z));
    }

    /* renamed from: hk */
    private Node m27140hk(boolean z) {
        Node hl = m27141hl(z);
        if (!m27150vM(103)) {
            return hl;
        }
        this.gPk.addToken(103);
        return this.gPi.mo17113c(103, hl, m27140hk(z));
    }

    /* renamed from: hl */
    private Node m27141hl(boolean z) {
        Node hm = m27142hm(z);
        if (!m27150vM(104)) {
            return hm;
        }
        this.gPk.addToken(104);
        return this.gPi.mo17113c(104, hm, m27141hl(z));
    }

    /* renamed from: hm */
    private Node m27142hm(boolean z) {
        Node hn = m27143hn(z);
        while (m27150vM(9)) {
            this.gPk.addToken(9);
            hn = this.gPi.mo17113c(9, hn, m27143hn(z));
        }
        return hn;
    }

    /* renamed from: hn */
    private Node m27143hn(boolean z) {
        Node ho = m27144ho(z);
        while (m27150vM(10)) {
            this.gPk.addToken(10);
            ho = this.gPi.mo17113c(10, ho, m27144ho(z));
        }
        return ho;
    }

    /* renamed from: ho */
    private Node m27144ho(boolean z) {
        Node hp = m27145hp(z);
        while (m27150vM(11)) {
            this.gPk.addToken(11);
            hp = this.gPi.mo17113c(11, hp, m27145hp(z));
        }
        return hp;
    }

    /* renamed from: hp */
    private Node m27145hp(boolean z) {
        int i;
        int i2;
        Node hq = m27146hq(z);
        while (true) {
            int peekToken = peekToken();
            switch (peekToken) {
                case 12:
                case 13:
                case 46:
                case 47:
                    cCf();
                    if (this.f5571he.getLanguageVersion() == 120) {
                        switch (peekToken) {
                            case 12:
                                i2 = 46;
                                i = peekToken;
                                continue;
                            case 13:
                                i2 = 47;
                                i = peekToken;
                                continue;
                            case 46:
                                i = 12;
                                i2 = peekToken;
                                continue;
                            case 47:
                                i = 13;
                                i2 = peekToken;
                                continue;
                        }
                    }
                    i2 = peekToken;
                    i = peekToken;
                    this.gPk.addToken(i);
                    hq = this.gPi.mo17113c(i2, hq, m27146hq(z));
                default:
                    return hq;
            }
        }
    }

    /* renamed from: hq */
    private Node m27146hq(boolean z) {
        Node cCr = cCr();
        while (true) {
            int peekToken = peekToken();
            switch (peekToken) {
                case 14:
                case 15:
                case 16:
                case 17:
                case 53:
                    break;
                case 52:
                    if (z) {
                        break;
                    } else {
                        continue;
                    }
            }
            cCf();
            this.gPk.addToken(peekToken);
            cCr = this.gPi.mo17113c(peekToken, cCr, cCr());
        }
        return cCr;
    }

    private Node cCr() {
        Node cCs = cCs();
        while (true) {
            int peekToken = peekToken();
            switch (peekToken) {
                case 18:
                case 19:
                case 20:
                    cCf();
                    this.gPk.addToken(peekToken);
                    cCs = this.gPi.mo17113c(peekToken, cCs, cCs());
                default:
                    return cCs;
            }
        }
    }

    private Node cCs() {
        Node cCt = cCt();
        while (true) {
            int peekToken = peekToken();
            if (peekToken != 21 && peekToken != 22) {
                return cCt;
            }
            cCf();
            this.gPk.addToken(peekToken);
            cCt = this.gPi.mo17113c(peekToken, cCt, cCt());
        }
    }

    private Node cCt() {
        Node cCu = cCu();
        while (true) {
            int peekToken = peekToken();
            switch (peekToken) {
                case 23:
                case 24:
                case 25:
                    cCf();
                    this.gPk.addToken(peekToken);
                    cCu = this.gPi.mo17113c(peekToken, cCu, cCu());
                default:
                    return cCu;
            }
        }
    }

    private Node cCu() {
        int peekToken = peekToken();
        switch (peekToken) {
            case -1:
                cCf();
                return this.gPi.mo17127kF("error");
            case 14:
                if (this.f5571he.isXmlAvailable()) {
                    cCf();
                    return m27127a(true, cCv());
                }
                break;
            case 21:
                cCf();
                this.gPk.addToken(28);
                return this.gPi.mo17093a(28, cCu());
            case 22:
                cCf();
                this.gPk.addToken(29);
                return this.gPi.mo17093a(29, cCu());
            case 26:
            case 27:
            case 32:
            case 125:
                cCf();
                this.gPk.addToken(peekToken);
                return this.gPi.mo17093a(peekToken, cCu());
            case 31:
                cCf();
                this.gPk.addToken(31);
                return this.gPi.mo17093a(31, cCu());
            case 105:
            case 106:
                cCf();
                this.gPk.addToken(peekToken);
                return this.gPi.mo17095a(peekToken, false, m27147hr(true));
        }
        Node hr = m27147hr(true);
        int cCh = cCh();
        if (cCh != 105 && cCh != 106) {
            return hr;
        }
        cCf();
        this.gPk.addToken(cCh);
        return this.gPi.mo17095a(cCh, true, hr);
    }

    private Node cCv() {
        Node c;
        Node hh;
        Node c2;
        Node a;
        int eb = this.gPf.mo14625eb();
        if (eb == 144 || eb == 147) {
            Node wb = this.gPi.mo17134wb(30);
            this.gPi.mo17133m(wb, this.gPi.mo17127kF(this.gPf.getString().trim().startsWith("<>") ? "XMLList" : "XML"));
            Node qlVar = null;
            while (true) {
                switch (eb) {
                    case 144:
                        String string = this.gPf.getString();
                        this.gPk.mo11287nw(string);
                        m27134c(84, "msg.syntax");
                        this.gPk.addToken(84);
                        if (peekToken() == 85) {
                            hh = this.gPi.mo17128kG("");
                        } else {
                            hh = m27137hh(false);
                        }
                        m27134c(85, "msg.syntax");
                        this.gPk.addToken(85);
                        if (qlVar == null) {
                            c2 = this.gPi.mo17128kG(string);
                        } else {
                            c2 = this.gPi.mo17113c(21, qlVar, this.gPi.mo17128kG(string));
                        }
                        if (this.gPf.mo14624ea()) {
                            a = this.gPi.mo17113c(21, this.gPi.mo17113c(21, this.gPi.mo17128kG("\""), this.gPi.mo17093a(74, hh)), this.gPi.mo17128kG("\""));
                        } else {
                            a = this.gPi.mo17093a(75, hh);
                        }
                        Node c3 = this.gPi.mo17113c(21, c2, a);
                        eb = this.gPf.mo14626ec();
                        qlVar = c3;
                    case 147:
                        String string2 = this.gPf.getString();
                        this.gPk.mo11287nw(string2);
                        if (qlVar == null) {
                            c = this.gPi.mo17128kG(string2);
                        } else {
                            c = this.gPi.mo17113c(21, qlVar, this.gPi.mo17128kG(string2));
                        }
                        this.gPi.mo17133m(wb, c);
                        return wb;
                    default:
                        mo16821kC("msg.syntax");
                        return null;
                }
            }
        } else {
            mo16821kC("msg.syntax");
            return null;
        }
    }

    /* renamed from: x */
    private void m27153x(Node qlVar) {
        if (!m27150vM(87)) {
            boolean z = true;
            while (true) {
                if (!z) {
                    this.gPk.addToken(88);
                }
                if (peekToken() == 72) {
                    mo16821kC("msg.yield.parenthesized");
                }
                this.gPi.mo17133m(qlVar, m27138hi(false));
                if (!m27150vM(88)) {
                    break;
                }
                z = false;
            }
            m27134c(87, "msg.no.paren.arg");
        }
        this.gPk.addToken(87);
    }

    /* renamed from: hr */
    private Node m27147hr(boolean z) {
        Node cCw;
        if (peekToken() == 30) {
            cCf();
            this.gPk.addToken(30);
            cCw = this.gPi.mo17109b(30, m27147hr(false));
            if (m27150vM(86)) {
                this.gPk.addToken(86);
                m27153x(cCw);
            }
            if (peekToken() == 84) {
                this.gPi.mo17133m(cCw, cCw());
            }
        } else {
            cCw = cCw();
        }
        return m27127a(z, cCw);
    }

    /* renamed from: a */
    private Node m27127a(boolean z, Node qlVar) {
        int i;
        while (true) {
            int peekToken = peekToken();
            switch (peekToken) {
                case 82:
                    cCf();
                    this.gPk.addToken(82);
                    qlVar = this.gPi.mo17102a(qlVar, (String) null, m27137hh(false), 0);
                    m27134c(83, "msg.no.bracket.index");
                    this.gPk.addToken(83);
                    continue;
                case 86:
                    if (!z) {
                        break;
                    } else {
                        cCf();
                        this.gPk.addToken(86);
                        qlVar = this.gPi.mo17109b(38, qlVar);
                        m27153x(qlVar);
                        continue;
                    }
                case 107:
                case 142:
                    cCf();
                    this.gPk.addToken(peekToken);
                    if (peekToken == 142) {
                        cCj();
                        i = 4;
                    } else {
                        i = 0;
                    }
                    if (this.f5571he.isXmlAvailable()) {
                        switch (nextToken()) {
                            case 23:
                                this.gPk.mo11287nw("*");
                                qlVar = m27125a(qlVar, "*", i);
                                break;
                            case 39:
                                String string = this.gPf.getString();
                                this.gPk.mo11287nw(string);
                                qlVar = m27125a(qlVar, string, i);
                                break;
                            case 50:
                                this.gPk.mo11287nw("throw");
                                qlVar = m27125a(qlVar, "throw", i);
                                break;
                            case 146:
                                this.gPk.addToken(146);
                                qlVar = m27133c(qlVar, i);
                                break;
                            default:
                                mo16821kC("msg.no.name.after.dot");
                                break;
                        }
                    } else {
                        m27134c(39, "msg.no.name.after.dot");
                        String string2 = this.gPf.getString();
                        this.gPk.mo11287nw(string2);
                        qlVar = this.gPi.mo17103a(qlVar, (String) null, string2, i);
                        continue;
                    }
                case 145:
                    cCf();
                    cCj();
                    this.gPk.addToken(145);
                    qlVar = this.gPi.mo17110b(qlVar, m27137hh(false), this.gPf.getLineno());
                    m27134c(87, "msg.no.paren");
                    this.gPk.addToken(87);
                    continue;
            }
        }
        return qlVar;
    }

    /* renamed from: c */
    private Node m27133c(Node qlVar, int i) {
        int i2 = i | 2;
        switch (nextToken()) {
            case 23:
                this.gPk.mo11287nw("*");
                return m27125a(qlVar, "*", i2);
            case 39:
                String string = this.gPf.getString();
                this.gPk.mo11287nw(string);
                return m27125a(qlVar, string, i2);
            case 82:
                this.gPk.addToken(82);
                Node a = this.gPi.mo17102a(qlVar, (String) null, m27137hh(false), i2);
                m27134c(83, "msg.no.bracket.index");
                this.gPk.addToken(83);
                return a;
            default:
                mo16821kC("msg.no.name.after.xmlAttr");
                return this.gPi.mo17103a(qlVar, (String) null, "?", i2);
        }
    }

    /* renamed from: a */
    private Node m27125a(Node qlVar, String str, int i) {
        String str2;
        String str3 = null;
        if (m27150vM(143)) {
            this.gPk.addToken(143);
            switch (nextToken()) {
                case 23:
                    this.gPk.mo11287nw("*");
                    str2 = "*";
                    str3 = str;
                    break;
                case 39:
                    str2 = this.gPf.getString();
                    this.gPk.mo11287nw(str2);
                    str3 = str;
                    break;
                case 82:
                    this.gPk.addToken(82);
                    Node a = this.gPi.mo17102a(qlVar, str, m27137hh(false), i);
                    m27134c(83, "msg.no.bracket.index");
                    this.gPk.addToken(83);
                    return a;
                default:
                    mo16821kC("msg.no.name.after.coloncolon");
                    str2 = "?";
                    str3 = str;
                    break;
            }
        } else {
            str2 = str;
        }
        return this.gPi.mo17103a(qlVar, str3, str2, i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00e7  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private Node m27126a(java.lang.String r13, Node r14) {
        /*
            r12 = this;
            r0 = 1
            r11 = 0
            r10 = 118(0x76, float:1.65E-43)
            r3 = 86
            r7 = 0
            int r1 = r12.nextToken()
            if (r1 == r10) goto L_0x0012
            java.lang.RuntimeException r0 = p001a.C1520WN.codeBug()
            throw r0
        L_0x0012:
            a.aRu r1 = r12.gPk
            java.lang.String r2 = " "
            r1.mo11287nw(r2)
            a.aRu r1 = r12.gPk
            r1.addToken(r10)
            r1 = 39
            boolean r1 = r12.m27150vM(r1)
            if (r1 == 0) goto L_0x00c3
            a.aRu r1 = r12.gPk
            a.al r2 = r12.gPf
            java.lang.String r2 = r2.getString()
            r1.mo11287nw(r2)
            a.al r1 = r12.gPf
            java.lang.String r1 = r1.getString()
            java.lang.String r2 = "each"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x00be
            r6 = r0
        L_0x0040:
            java.lang.String r1 = "msg.no.paren.for"
            r12.m27134c((int) r3, (java.lang.String) r1)
            a.aRu r1 = r12.gPk
            r1.addToken(r3)
            int r1 = r12.peekToken()
            r2 = 82
            if (r1 == r2) goto L_0x0056
            r2 = 84
            if (r1 != r2) goto L_0x00c6
        L_0x0056:
            a.aAk r1 = r12.gPl
            java.lang.String r1 = r1.cHs()
            r12.mo16811a((int) r3, (boolean) r7, (java.lang.String) r1)
            a.ayw r2 = r12.gPi
            r3 = 88
            a.ayw r4 = r12.gPi
            r5 = 89
            a.ql r8 = r12.cCw()
            a.ayw r9 = r12.gPi
            a.ql r9 = r9.mo17127kF(r1)
            a.ql r4 = r4.mo17116d((int) r5, (org.mozilla.javascript.C3366ql) r8, (org.mozilla.javascript.C3366ql) r9)
            a.ql r14 = r2.mo17113c((int) r3, (org.mozilla.javascript.C3366ql) r4, (org.mozilla.javascript.C3366ql) r14)
        L_0x0079:
            a.ayw r2 = r12.gPi
            a.ql r3 = r2.mo17127kF(r1)
            r2 = 152(0x98, float:2.13E-43)
            r12.mo16811a((int) r2, (boolean) r7, (java.lang.String) r1)
            r1 = 52
            java.lang.String r2 = "msg.in.after.for.name"
            r12.m27134c((int) r1, (java.lang.String) r2)
            a.aRu r1 = r12.gPk
            r2 = 52
            r1.addToken(r2)
            a.ql r4 = r12.m27137hh(r7)
            r1 = 87
            java.lang.String r2 = "msg.no.paren.for.ctrl"
            r12.m27134c((int) r1, (java.lang.String) r2)
            a.aRu r1 = r12.gPk
            r2 = 87
            r1.addToken(r2)
            int r2 = r12.peekToken()
            if (r2 != r10) goto L_0x00e7
            a.ql r5 = r12.m27126a((java.lang.String) r13, (org.mozilla.javascript.C3366ql) r14)
        L_0x00ae:
            a.ql r2 = r12.m27131b((org.mozilla.javascript.C3366ql) r11, (boolean) r0)
            a.ayw r0 = r12.gPi     // Catch:{ all -> 0x013c }
            r1 = 152(0x98, float:2.13E-43)
            a.ql r0 = r0.mo17094a((int) r1, (org.mozilla.javascript.C3366ql) r2, (org.mozilla.javascript.C3366ql) r3, (org.mozilla.javascript.C3366ql) r4, (org.mozilla.javascript.C3366ql) r5, (boolean) r6)     // Catch:{ all -> 0x013c }
            r12.m27135hf(r7)
        L_0x00bd:
            return r0
        L_0x00be:
            java.lang.String r1 = "msg.no.paren.for"
            r12.mo16821kC(r1)
        L_0x00c3:
            r6 = r7
            goto L_0x0040
        L_0x00c6:
            r2 = 39
            if (r1 != r2) goto L_0x00d9
            r12.cCf()
            a.al r1 = r12.gPf
            java.lang.String r1 = r1.getString()
            a.aRu r2 = r12.gPk
            r2.mo11287nw(r1)
            goto L_0x0079
        L_0x00d9:
            java.lang.String r0 = "msg.bad.var"
            r12.mo16821kC(r0)
            a.ayw r0 = r12.gPi
            r2 = 0
            a.ql r0 = r0.mo17092V(r2)
            goto L_0x00bd
        L_0x00e7:
            a.ayw r1 = r12.gPi
            r5 = 38
            a.ayw r8 = r12.gPi
            a.ayw r9 = r12.gPi
            a.ql r9 = r9.mo17127kF(r13)
            java.lang.String r10 = "push"
            a.ql r8 = r8.mo17103a((org.mozilla.javascript.C3366ql) r9, (java.lang.String) r11, (java.lang.String) r10, (int) r7)
            a.ql r5 = r1.mo17109b(r5, r8)
            r5.mo21461G(r14)
            a.ql r1 = new a.ql
            r8 = 132(0x84, float:1.85E-43)
            a.al r9 = r12.gPf
            int r9 = r9.getLineno()
            r1.<init>((int) r8, (org.mozilla.javascript.C3366ql) r5, (int) r9)
            r5 = 111(0x6f, float:1.56E-43)
            if (r2 != r5) goto L_0x012b
            r12.cCf()
            a.aRu r2 = r12.gPk
            r5 = 111(0x6f, float:1.56E-43)
            r2.addToken(r5)
            a.al r2 = r12.gPf
            int r2 = r2.getLineno()
            a.ql r5 = r12.cCo()
            a.ayw r8 = r12.gPi
            a.ql r1 = r8.mo17112b((org.mozilla.javascript.C3366ql) r5, (org.mozilla.javascript.C3366ql) r1, (org.mozilla.javascript.C3366ql) r11, (int) r2)
        L_0x012b:
            r2 = 83
            java.lang.String r5 = "msg.no.bracket.arg"
            r12.m27134c((int) r2, (java.lang.String) r5)
            a.aRu r2 = r12.gPk
            r5 = 83
            r2.addToken(r5)
            r5 = r1
            goto L_0x00ae
        L_0x013c:
            r0 = move-exception
            r12.m27135hf(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6966axf.m27126a(java.lang.String, a.ql):a.ql");
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0125 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private Node cCw() {
        /*
            r13 = this;
            r7 = 39
            r8 = 88
            r4 = 0
            r1 = 1
            r2 = 0
            int r0 = r13.cCg()
            r3 = 65535(0xffff, float:9.1834E-41)
            r3 = r3 & r0
            switch(r3) {
                case -1: goto L_0x0017;
                case 0: goto L_0x02af;
                case 24: goto L_0x0275;
                case 39: goto L_0x0213;
                case 40: goto L_0x024f;
                case 41: goto L_0x0262;
                case 42: goto L_0x029b;
                case 43: goto L_0x029b;
                case 44: goto L_0x029b;
                case 45: goto L_0x029b;
                case 82: goto L_0x001f;
                case 84: goto L_0x00fd;
                case 86: goto L_0x01e1;
                case 99: goto L_0x0275;
                case 108: goto L_0x0019;
                case 126: goto L_0x02a8;
                case 146: goto L_0x0203;
                case 152: goto L_0x01d4;
                default: goto L_0x0012;
            }
        L_0x0012:
            java.lang.String r0 = "msg.syntax"
            r13.mo16821kC(r0)
        L_0x0017:
            r0 = r4
        L_0x0018:
            return r0
        L_0x0019:
            r0 = 2
            a.ql r0 = r13.m27151vN(r0)
            goto L_0x0018
        L_0x001f:
            a.aCx r5 = new a.aCx
            r5.<init>()
            a.aRu r0 = r13.gPk
            r3 = 82
            r0.addToken(r3)
            r0 = r1
            r3 = r2
        L_0x002d:
            int r6 = r13.peekToken()
            if (r6 != r8) goto L_0x0045
            r13.cCf()
            a.aRu r6 = r13.gPk
            r6.addToken(r8)
            if (r0 != 0) goto L_0x003f
            r0 = r1
            goto L_0x002d
        L_0x003f:
            r5.add(r4)
            int r3 = r3 + 1
            goto L_0x002d
        L_0x0045:
            r7 = 83
            if (r6 != r7) goto L_0x0064
            r13.cCf()
            a.aRu r4 = r13.gPk
            r6 = 83
            r4.addToken(r6)
            int r4 = r5.size()
            if (r0 == 0) goto L_0x0062
        L_0x0059:
            int r0 = r4 + r1
            a.ayw r1 = r13.gPi
            a.ql r0 = r1.mo17098a((p001a.C5271aCx) r5, (int) r3, (int) r0)
            goto L_0x0018
        L_0x0062:
            r1 = r2
            goto L_0x0059
        L_0x0064:
            if (r3 != 0) goto L_0x00ec
            int r7 = r5.size()
            if (r7 != r1) goto L_0x00ec
            r7 = 118(0x76, float:1.65E-43)
            if (r6 != r7) goto L_0x00ec
            a.ayw r0 = r13.gPi
            r1 = 156(0x9c, float:2.19E-43)
            a.al r2 = r13.gPf
            int r2 = r2.getLineno()
            a.ql r1 = r0.mo17108am(r1, r2)
            a.aAk r0 = r13.gPl
            java.lang.String r2 = r0.cHs()
            r13.mo16823u(r1)
            r0 = 152(0x98, float:2.13E-43)
            r3 = 0
            r13.mo16811a((int) r0, (boolean) r3, (java.lang.String) r2)     // Catch:{ all -> 0x00e7 }
            r0 = 0
            java.lang.Object r0 = r5.get(r0)     // Catch:{ all -> 0x00e7 }
            a.ql r0 = (org.mozilla.javascript.C3366ql) r0     // Catch:{ all -> 0x00e7 }
            a.ayw r3 = r13.gPi     // Catch:{ all -> 0x00e7 }
            a.al r4 = r13.gPf     // Catch:{ all -> 0x00e7 }
            int r4 = r4.getLineno()     // Catch:{ all -> 0x00e7 }
            a.ql r3 = r3.mo17137we(r4)     // Catch:{ all -> 0x00e7 }
            a.ql r4 = new a.ql     // Catch:{ all -> 0x00e7 }
            r5 = 132(0x84, float:1.85E-43)
            a.ayw r6 = r13.gPi     // Catch:{ all -> 0x00e7 }
            r7 = 89
            a.ayw r8 = r13.gPi     // Catch:{ all -> 0x00e7 }
            a.ql r8 = r8.mo17127kF(r2)     // Catch:{ all -> 0x00e7 }
            a.ayw r9 = r13.gPi     // Catch:{ all -> 0x00e7 }
            r10 = 30
            a.ayw r11 = r13.gPi     // Catch:{ all -> 0x00e7 }
            java.lang.String r12 = "Array"
            a.ql r11 = r11.mo17127kF(r12)     // Catch:{ all -> 0x00e7 }
            a.ql r9 = r9.mo17109b(r10, r11)     // Catch:{ all -> 0x00e7 }
            a.ql r6 = r6.mo17116d((int) r7, (org.mozilla.javascript.C3366ql) r8, (org.mozilla.javascript.C3366ql) r9)     // Catch:{ all -> 0x00e7 }
            a.al r7 = r13.gPf     // Catch:{ all -> 0x00e7 }
            int r7 = r7.getLineno()     // Catch:{ all -> 0x00e7 }
            r4.<init>((int) r5, (org.mozilla.javascript.C3366ql) r6, (int) r7)     // Catch:{ all -> 0x00e7 }
            r3.mo21461G(r4)     // Catch:{ all -> 0x00e7 }
            a.ql r0 = r13.m27126a((java.lang.String) r2, (org.mozilla.javascript.C3366ql) r0)     // Catch:{ all -> 0x00e7 }
            r3.mo21461G(r0)     // Catch:{ all -> 0x00e7 }
            r1.mo21461G(r3)     // Catch:{ all -> 0x00e7 }
            a.ayw r0 = r13.gPi     // Catch:{ all -> 0x00e7 }
            a.ql r0 = r0.mo17127kF(r2)     // Catch:{ all -> 0x00e7 }
            r1.mo21461G(r0)     // Catch:{ all -> 0x00e7 }
            r13.popScope()
            r0 = r1
            goto L_0x0018
        L_0x00e7:
            r0 = move-exception
            r13.popScope()
            throw r0
        L_0x00ec:
            if (r0 != 0) goto L_0x00f3
            java.lang.String r0 = "msg.no.bracket.arg"
            r13.mo16821kC(r0)
        L_0x00f3:
            a.ql r0 = r13.m27138hi(r2)
            r5.add(r0)
            r0 = r2
            goto L_0x002d
        L_0x00fd:
            a.aCx r3 = new a.aCx
            r3.<init>()
            a.aRu r0 = r13.gPk
            r4 = 84
            r0.addToken(r4)
            r0 = 85
            boolean r0 = r13.m27150vM(r0)
            if (r0 != 0) goto L_0x012c
            r0 = r1
        L_0x0112:
            if (r0 != 0) goto L_0x013b
            a.aRu r4 = r13.gPk
            r4.addToken(r8)
        L_0x0119:
            int r4 = r13.peekToken()
            switch(r4) {
                case 39: goto L_0x013d;
                case 40: goto L_0x01be;
                case 41: goto L_0x013d;
                case 85: goto L_0x0125;
                default: goto L_0x0120;
            }
        L_0x0120:
            java.lang.String r0 = "msg.bad.prop"
            r13.mo16821kC(r0)
        L_0x0125:
            r0 = 85
            java.lang.String r1 = "msg.no.brace.prop"
            r13.m27134c((int) r0, (java.lang.String) r1)
        L_0x012c:
            a.aRu r0 = r13.gPk
            r1 = 85
            r0.addToken(r1)
            a.ayw r0 = r13.gPi
            a.ql r0 = r0.mo17097a(r3)
            goto L_0x0018
        L_0x013b:
            r0 = r2
            goto L_0x0119
        L_0x013d:
            r13.cCf()
            a.al r5 = r13.gPf
            java.lang.String r5 = r5.getString()
            if (r4 != r7) goto L_0x01b8
            java.lang.String r4 = "get"
            boolean r4 = r5.equals(r4)
            if (r4 == 0) goto L_0x017c
            int r4 = r13.peekToken()
            if (r4 != r7) goto L_0x017c
            a.aRu r4 = r13.gPk
            r5 = 150(0x96, float:2.1E-43)
            r4.addToken(r5)
            r13.cCf()
            a.al r4 = r13.gPf
            java.lang.String r4 = r4.getString()
            a.aRu r5 = r13.gPk
            r5.mo11287nw(r4)
            java.lang.Object r4 = p001a.C0903NH.m7597nl(r4)
            boolean r4 = r13.m27129a((p001a.C5271aCx) r3, (java.lang.Object) r4, (boolean) r1)
            if (r4 == 0) goto L_0x0125
        L_0x0175:
            boolean r4 = r13.m27150vM(r8)
            if (r4 != 0) goto L_0x0112
            goto L_0x0125
        L_0x017c:
            java.lang.String r4 = "set"
            boolean r4 = r5.equals(r4)
            if (r4 == 0) goto L_0x01ab
            int r4 = r13.peekToken()
            if (r4 != r7) goto L_0x01ab
            a.aRu r4 = r13.gPk
            r5 = 151(0x97, float:2.12E-43)
            r4.addToken(r5)
            r13.cCf()
            a.al r4 = r13.gPf
            java.lang.String r4 = r4.getString()
            a.aRu r5 = r13.gPk
            r5.mo11287nw(r4)
            java.lang.Object r4 = p001a.C0903NH.m7597nl(r4)
            boolean r4 = r13.m27129a((p001a.C5271aCx) r3, (java.lang.Object) r4, (boolean) r2)
            if (r4 != 0) goto L_0x0175
            goto L_0x0125
        L_0x01ab:
            a.aRu r4 = r13.gPk
            r4.mo11287nw(r5)
        L_0x01b0:
            java.lang.Object r4 = p001a.C0903NH.m7597nl(r5)
            r13.m27128a((p001a.C5271aCx) r3, (java.lang.Object) r4)
            goto L_0x0175
        L_0x01b8:
            a.aRu r4 = r13.gPk
            r4.mo11288nx(r5)
            goto L_0x01b0
        L_0x01be:
            r13.cCf()
            a.al r4 = r13.gPf
            double r4 = r4.mo14622dY()
            a.aRu r6 = r13.gPk
            r6.mo11283ae(r4)
            java.lang.Object r4 = p001a.C0903NH.m7537ad(r4)
            r13.m27128a((p001a.C5271aCx) r3, (java.lang.Object) r4)
            goto L_0x0175
        L_0x01d4:
            a.aRu r0 = r13.gPk
            r1 = 152(0x98, float:2.13E-43)
            r0.addToken(r1)
            a.ql r0 = r13.m27136hg(r2)
            goto L_0x0018
        L_0x01e1:
            a.aRu r0 = r13.gPk
            r1 = 86
            r0.addToken(r1)
            a.ql r0 = r13.m27137hh(r2)
            r1 = 19
            java.lang.Boolean r2 = java.lang.Boolean.TRUE
            r0.putProp(r1, r2)
            a.aRu r1 = r13.gPk
            r2 = 87
            r1.addToken(r2)
            r1 = 87
            java.lang.String r2 = "msg.no.paren"
            r13.m27134c((int) r1, (java.lang.String) r2)
            goto L_0x0018
        L_0x0203:
            r13.cCj()
            a.aRu r0 = r13.gPk
            r1 = 146(0x92, float:2.05E-43)
            r0.addToken(r1)
            a.ql r0 = r13.m27133c((org.mozilla.javascript.C3366ql) r4, (int) r2)
            goto L_0x0018
        L_0x0213:
            a.al r1 = r13.gPf
            java.lang.String r1 = r1.getString()
            r3 = 131072(0x20000, float:1.83671E-40)
            r0 = r0 & r3
            if (r0 == 0) goto L_0x0234
            int r0 = r13.peekToken()
            r3 = 102(0x66, float:1.43E-43)
            if (r0 != r3) goto L_0x0234
            a.ayw r0 = r13.gPi
            a.al r1 = r13.gPf
            int r1 = r1.getLineno()
            a.ql r0 = r0.mo17136wd(r1)
            goto L_0x0018
        L_0x0234:
            a.aRu r0 = r13.gPk
            r0.mo11287nw(r1)
            a.aKI r0 = r13.f5571he
            boolean r0 = r0.isXmlAvailable()
            if (r0 == 0) goto L_0x0247
            a.ql r0 = r13.m27125a((org.mozilla.javascript.C3366ql) r4, (java.lang.String) r1, (int) r2)
            goto L_0x0018
        L_0x0247:
            a.ayw r0 = r13.gPi
            a.ql r0 = r0.mo17127kF(r1)
            goto L_0x0018
        L_0x024f:
            a.al r0 = r13.gPf
            double r0 = r0.mo14622dY()
            a.aRu r2 = r13.gPk
            r2.mo11283ae(r0)
            a.ayw r2 = r13.gPi
            a.ql r0 = r2.mo17092V(r0)
            goto L_0x0018
        L_0x0262:
            a.al r0 = r13.gPf
            java.lang.String r0 = r0.getString()
            a.aRu r1 = r13.gPk
            r1.mo11288nx(r0)
            a.ayw r1 = r13.gPi
            a.ql r0 = r1.mo17128kG(r0)
            goto L_0x0018
        L_0x0275:
            a.al r0 = r13.gPf
            r0.mo14633t(r3)
            a.al r0 = r13.gPf
            java.lang.String r0 = r0.f4826im
            a.al r1 = r13.gPf
            r1.f4826im = r4
            a.al r1 = r13.gPf
            java.lang.String r1 = r1.getString()
            a.aRu r2 = r13.gPk
            r2.mo11284ba(r1, r0)
            a.aAk r2 = r13.gPl
            int r0 = r2.addRegexp(r1, r0)
            a.ayw r1 = r13.gPi
            a.ql r0 = r1.mo17138wf(r0)
            goto L_0x0018
        L_0x029b:
            a.aRu r0 = r13.gPk
            r0.addToken(r3)
            a.ayw r0 = r13.gPi
            a.ql r0 = r0.mo17134wb(r3)
            goto L_0x0018
        L_0x02a8:
            java.lang.String r0 = "msg.reserved.id"
            r13.mo16821kC(r0)
            goto L_0x0017
        L_0x02af:
            java.lang.String r0 = "msg.unexpected.eof"
            r13.mo16821kC(r0)
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6966axf.cCw():a.ql");
    }

    /* renamed from: a */
    private void m27128a(ObjArray acx, Object obj) {
        m27134c(102, "msg.no.colon.prop");
        this.gPk.addToken(66);
        acx.add(obj);
        acx.add(m27138hi(false));
    }

    /* renamed from: a */
    private boolean m27129a(ObjArray acx, Object obj, boolean z) {
        Node vN = m27151vN(2);
        if (vN.getType() != 108) {
            mo16821kC("msg.bad.prop");
            return false;
        }
        if (this.gPl.mo7704wM(vN.getExistingIntProp(1)).getFunctionName().length() != 0) {
            mo16821kC("msg.bad.prop");
            return false;
        }
        acx.add(obj);
        if (z) {
            acx.add(this.gPi.mo17093a(150, vN));
        } else {
            acx.add(this.gPi.mo17093a(151, vN));
        }
        return true;
    }

    /* renamed from: a.axf$a */
    private static class C2009a extends RuntimeException {
        static final long serialVersionUID = 5882582646773765630L;

        private C2009a() {
        }

        /* synthetic */ C2009a(C2009a aVar) {
            this();
        }
    }
}
