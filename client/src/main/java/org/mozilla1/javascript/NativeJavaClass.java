package org.mozilla1.javascript;

import java.lang.reflect.Array;
import java.util.Map;

/* renamed from: a.GC */
/* compiled from: a */
public class NativeJavaClass extends NativeJavaObject implements Function {
    static final String cZl = "__javaObject__";
    static final long serialVersionUID = -6460763940409461664L;
    private Map<String, org.mozilla1.javascript.FieldAndMethods> cZm;

    public NativeJavaClass() {
    }

    public NativeJavaClass(org.mozilla1.javascript.Scriptable avf, Class<?> cls) {
        this.f6800EY = avf;
        this.javaObject = cls;
        initMembers();
    }

    /* renamed from: a */
    static org.mozilla1.javascript.Scriptable m3325a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr, org.mozilla1.javascript.MemberBox amt) {
        Object[] objArr2;
        Object newInstance;
        int i = 0;
        org.mozilla1.javascript.Scriptable x = org.mozilla1.javascript.ScriptableObject.m23595x(avf);
        Class<?>[] clsArr = amt.gaM;
        if (amt.gaO) {
            Object[] objArr3 = new Object[clsArr.length];
            for (int i2 = 0; i2 < clsArr.length - 1; i2++) {
                objArr3[i2] = org.mozilla1.javascript.Context.jsToJava(objArr[i2], clsArr[i2]);
            }
            if (objArr.length != clsArr.length || (objArr[objArr.length - 1] != null && !(objArr[objArr.length - 1] instanceof NativeArray) && !(objArr[objArr.length - 1] instanceof NativeJavaArray))) {
                Class<?> componentType = clsArr[clsArr.length - 1].getComponentType();
                newInstance = Array.newInstance(componentType, (objArr.length - clsArr.length) + 1);
                while (i < Array.getLength(newInstance)) {
                    Array.set(newInstance, i, org.mozilla1.javascript.Context.jsToJava(objArr[(clsArr.length - 1) + i], componentType));
                    i++;
                }
            } else {
                newInstance = org.mozilla1.javascript.Context.jsToJava(objArr[objArr.length - 1], clsArr[clsArr.length - 1]);
            }
            objArr3[clsArr.length - 1] = newInstance;
            objArr2 = objArr3;
        } else {
            objArr2 = objArr;
            while (i < objArr2.length) {
                Object obj = objArr2[i];
                Object jsToJava = org.mozilla1.javascript.Context.jsToJava(obj, clsArr[i]);
                if (jsToJava != obj) {
                    if (objArr2 == objArr) {
                        objArr2 = (Object[]) objArr.clone();
                    }
                    objArr2[i] = jsToJava;
                }
                i++;
            }
        }
        return lhVar.bwy().mo6828a(lhVar, x, amt.newInstance(objArr2));
    }

    /* renamed from: f */
    private static Class<?> m3326f(Class<?> cls, String str) {
        String str2 = String.valueOf(cls.getName()) + '$' + str;
        ClassLoader classLoader = cls.getClassLoader();
        if (classLoader == null) {
            return org.mozilla1.javascript.Kit.classOrNull(str2);
        }
        return Kit.classOrNull(classLoader, str2);
    }

    /* access modifiers changed from: protected */
    public void initMembers() {
        Class cls = (Class) this.javaObject;
        this.f6801EZ = org.mozilla1.javascript.JavaMembers.m8430a(this.f6800EY, (Class<?>) cls, (Class<?>) cls, false);
        this.cZm = this.f6801EZ.mo4679a((org.mozilla1.javascript.Scriptable) this, (Object) cls, true);
    }

    public String getClassName() {
        return "JavaClass";
    }

    public boolean has(String str, org.mozilla1.javascript.Scriptable avf) {
        return this.f6801EZ.mo4683n(str, true) || cZl.equals(str);
    }

    public Object get(String str, org.mozilla1.javascript.Scriptable avf) {
        org.mozilla1.javascript.FieldAndMethods tr;
        if (str.equals("prototype")) {
            return null;
        }
        if (this.cZm != null && (tr = this.cZm.get(str)) != null) {
            return tr;
        }
        if (this.f6801EZ.mo4683n(str, true)) {
            return this.f6801EZ.mo4678a((org.mozilla1.javascript.Scriptable) this, str, this.javaObject, true);
        }
        if (cZl.equals(str)) {
            org.mozilla1.javascript.Context bwA = org.mozilla1.javascript.Context.bwA();
            return bwA.bwy().mo6829a(bwA, org.mozilla1.javascript.ScriptableObject.m23595x(avf), this.javaObject, org.mozilla1.javascript.ScriptRuntime.ClassClass);
        }
        Class<?> f = m3326f(getClassObject(), str);
        if (f != null) {
            NativeJavaClass gc = new NativeJavaClass(ScriptableObject.m23595x(this), f);
            gc.setParentScope(this);
            return gc;
        }
        throw this.f6801EZ.mo4682jy(str);
    }

    public void put(String str, org.mozilla1.javascript.Scriptable avf, Object obj) {
        this.f6801EZ.mo4680a(this, str, this.javaObject, obj, true);
    }

    public Object[] getIds() {
        return this.f6801EZ.mo4681dP(true);
    }

    public Class<?> getClassObject() {
        return (Class) super.unwrap();
    }

    public Object getDefaultValue(Class<?> cls) {
        if (cls == null || cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
            return toString();
        }
        if (cls == org.mozilla1.javascript.ScriptRuntime.BooleanClass) {
            return Boolean.TRUE;
        }
        if (cls == org.mozilla1.javascript.ScriptRuntime.NumberClass) {
            return ScriptRuntime.NaNobj;
        }
        return this;
    }

    public Object call(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        if (objArr.length != 1 || !(objArr[0] instanceof org.mozilla1.javascript.Scriptable)) {
            return construct(lhVar, avf, objArr);
        }
        Class<?> classObject = getClassObject();
        org.mozilla1.javascript.Scriptable avf3 = (org.mozilla1.javascript.Scriptable) objArr[0];
        do {
            if ((avf3 instanceof org.mozilla1.javascript.Wrapper) && classObject.isInstance(((org.mozilla1.javascript.Wrapper) avf3).unwrap())) {
                return avf3;
            }
            avf3 = avf3.getPrototype();
        } while (avf3 != null);
        return construct(lhVar, avf, objArr);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005c, code lost:
        if (r0 == null) goto L_0x0069;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.mozilla1.javascript.Scriptable construct(Context r8, org.mozilla1.javascript.Scriptable r9, java.lang.Object[] r10) {
        /*
            r7 = this;
            java.lang.Class r2 = r7.getClassObject()
            int r0 = r2.getModifiers()
            boolean r1 = java.lang.reflect.Modifier.isInterface(r0)
            if (r1 != 0) goto L_0x0034
            boolean r0 = java.lang.reflect.Modifier.isAbstract(r0)
            if (r0 != 0) goto L_0x0034
            a.PT r0 = r7.f6801EZ
            a.amt[] r0 = r0.gcX
            int r1 = p001a.C6186aiC.m22442a((p001a.C2909lh) r8, (p001a.C6437amt[]) r0, (java.lang.Object[]) r10)
            if (r1 >= 0) goto L_0x002d
            java.lang.String r0 = p001a.C6186aiC.m22445t(r10)
            java.lang.String r1 = "msg.no.java.ctor"
            java.lang.String r2 = r2.getName()
            a.TJ r0 = p001a.C2909lh.m35224a((java.lang.String) r1, (java.lang.Object) r2, (java.lang.Object) r0)
            throw r0
        L_0x002d:
            r0 = r0[r1]
            a.aVF r0 = m3325a(r8, r9, r10, r0)
        L_0x0033:
            return r0
        L_0x0034:
            a.aVF r3 = p001a.C6327akn.m23595x(r7)
            java.lang.String r1 = ""
            java.lang.String r0 = "JavaAdapter"
            java.lang.Object r0 = r3.get((java.lang.String) r0, (p001a.aVF) r3)     // Catch:{ Exception -> 0x0057 }
            java.lang.Object r4 = NOT_FOUND     // Catch:{ Exception -> 0x0057 }
            if (r0 == r4) goto L_0x0069
            a.azg r0 = (p001a.C7019azg) r0     // Catch:{ Exception -> 0x0057 }
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0057 }
            r5 = 0
            r4[r5] = r7     // Catch:{ Exception -> 0x0057 }
            r5 = 1
            r6 = 0
            r6 = r10[r6]     // Catch:{ Exception -> 0x0057 }
            r4[r5] = r6     // Catch:{ Exception -> 0x0057 }
            a.aVF r0 = r0.construct(r8, r3, r4)     // Catch:{ Exception -> 0x0057 }
            goto L_0x0033
        L_0x0057:
            r0 = move-exception
            java.lang.String r0 = r0.getMessage()
            if (r0 == 0) goto L_0x0069
        L_0x005e:
            java.lang.String r1 = "msg.cant.instantiate"
            java.lang.String r2 = r2.getName()
            a.TJ r0 = p001a.C2909lh.m35224a((java.lang.String) r1, (java.lang.Object) r0, (java.lang.Object) r2)
            throw r0
        L_0x0069:
            r0 = r1
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C0435GC.construct(a.lh, a.aVF, java.lang.Object[]):a.aVF");
    }

    public String toString() {
        return "[JavaClass " + getClassObject().getName() + "]";
    }

    public boolean hasInstance(Scriptable avf) {
        if (!(avf instanceof org.mozilla1.javascript.Wrapper) || (avf instanceof NativeJavaClass)) {
            return false;
        }
        return getClassObject().isInstance(((Wrapper) avf).unwrap());
    }
}
