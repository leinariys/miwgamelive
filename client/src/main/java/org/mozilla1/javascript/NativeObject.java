package org.mozilla1.javascript;

/* renamed from: a.Pd */
/* compiled from: a */
public class NativeObject extends IdScriptableObject {
    static final long serialVersionUID = -6345305608474346996L;
    private static final int cBB = 3;
    private static final int cBF = 4;
    private static final int ceN = 8;
    private static final int dPA = 12;
    private static final Object dPt = new Integer(20);
    private static final int dPu = 5;
    private static final int dPv = 6;
    private static final int dPw = 7;
    private static final int dPx = 9;
    private static final int dPy = 10;
    private static final int dPz = 11;
    /* renamed from: ns */
    private static final int f1379ns = 1;
    /* renamed from: nt */
    private static final int f1380nt = 12;
    /* renamed from: xT */
    private static final int f1381xT = 2;

    /* renamed from: a */
    static void m8584a(org.mozilla1.javascript.Scriptable avf, boolean z) {
        new NativeObject().mo15695a(12, avf, z);
    }

    public String getClassName() {
        return "Object";
    }

    public String toString() {
        return org.mozilla1.javascript.ScriptRuntime.m7606q((org.mozilla1.javascript.Scriptable) this);
    }

    /* access modifiers changed from: protected */
    public void initPrototypeId(int i) {
        String str;
        int i2 = 1;
        switch (i) {
            case 1:
                str = "constructor";
                break;
            case 2:
                i2 = 0;
                str = "toString";
                break;
            case 3:
                i2 = 0;
                str = "toLocaleString";
                break;
            case 4:
                i2 = 0;
                str = "valueOf";
                break;
            case 5:
                str = "hasOwnProperty";
                break;
            case 6:
                str = "propertyIsEnumerable";
                break;
            case 7:
                str = "isPrototypeOf";
                break;
            case 8:
                i2 = 0;
                str = "toSource";
                break;
            case 9:
                i2 = 2;
                str = "__defineGetter__";
                break;
            case 10:
                i2 = 2;
                str = "__defineSetter__";
                break;
            case 11:
                str = "__lookupGetter__";
                break;
            case 12:
                str = "__lookupSetter__";
                break;
            default:
                throw new IllegalArgumentException(String.valueOf(i));
        }
        initPrototypeMethod(dPt, i, str, i2);
    }

    public Object execIdCall(IdFunctionObject yy, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        int o;
        Object a;
        org.mozilla1.javascript.Scriptable prototype;
        Object obj;
        int o2;
        boolean z;
        boolean has;
        boolean z2 = true;
        boolean z3 = false;
        if (!yy.hasTag(dPt)) {
            return super.execIdCall(yy, lhVar, avf, avf2, objArr);
        }
        int methodId = yy.methodId();
        switch (methodId) {
            case 1:
                if (avf2 != null) {
                    return yy.construct(lhVar, avf, objArr);
                }
                if (objArr.length == 0 || objArr[0] == null || objArr[0] == org.mozilla1.javascript.Undefined.instance) {
                    return new NativeObject();
                }
                return org.mozilla1.javascript.ScriptRuntime.m7572e(lhVar, avf, objArr[0]);
            case 2:
            case 3:
                if (!lhVar.hasFeature(4)) {
                    return org.mozilla1.javascript.ScriptRuntime.m7606q(avf2);
                }
                String f = org.mozilla1.javascript.ScriptRuntime.m7579f(lhVar, avf, avf2, objArr);
                int length = f.length();
                if (length != 0 && f.charAt(0) == '(' && f.charAt(length - 1) == ')') {
                    return f.substring(1, length - 1);
                }
                return f;
            case 4:
                return avf2;
            case 5:
                if (objArr.length == 0) {
                    has = false;
                } else {
                    String b = org.mozilla1.javascript.ScriptRuntime.m7547b(lhVar, objArr[0]);
                    if (b == null) {
                        has = avf2.has(org.mozilla1.javascript.ScriptRuntime.m7602o(lhVar), avf2);
                    } else {
                        has = avf2.has(b, avf2);
                    }
                }
                return org.mozilla1.javascript.ScriptRuntime.wrapBoolean(has);
            case 6:
                if (objArr.length != 0) {
                    String b2 = org.mozilla1.javascript.ScriptRuntime.m7547b(lhVar, objArr[0]);
                    if (b2 == null) {
                        int o3 = org.mozilla1.javascript.ScriptRuntime.m7602o(lhVar);
                        z = avf2.has(o3, avf2);
                        if (z && (avf2 instanceof org.mozilla1.javascript.ScriptableObject)) {
                            if ((((org.mozilla1.javascript.ScriptableObject) avf2).getAttributes(o3) & 2) == 0) {
                                z3 = true;
                            }
                        }
                    } else {
                        z = avf2.has(b2, avf2);
                        if (z && (avf2 instanceof org.mozilla1.javascript.ScriptableObject)) {
                            if ((((org.mozilla1.javascript.ScriptableObject) avf2).getAttributes(b2) & 2) != 0) {
                                z2 = false;
                            }
                            z3 = z2;
                        }
                    }
                    z3 = z;
                }
                return org.mozilla1.javascript.ScriptRuntime.wrapBoolean(z3);
            case 7:
                if (objArr.length != 0 && (objArr[0] instanceof org.mozilla1.javascript.Scriptable)) {
                    org.mozilla1.javascript.Scriptable avf3 = (Scriptable) objArr[0];
                    while (true) {
                        avf3 = avf3.getPrototype();
                        if (avf3 == avf2) {
                            z3 = true;
                        } else if (avf3 == null) {
                        }
                    }
                }
                return org.mozilla1.javascript.ScriptRuntime.wrapBoolean(z3);
            case 8:
                return org.mozilla1.javascript.ScriptRuntime.m7579f(lhVar, avf, avf2, objArr);
            case 9:
            case 10:
                if (objArr.length < 2 || !(objArr[1] instanceof org.mozilla1.javascript.Callable)) {
                    if (objArr.length >= 2) {
                        obj = objArr[1];
                    } else {
                        obj = org.mozilla1.javascript.Undefined.instance;
                    }
                    throw org.mozilla1.javascript.ScriptRuntime.notFunctionError(obj);
                } else if (!(avf2 instanceof org.mozilla1.javascript.ScriptableObject)) {
                    throw Context.m35224a("msg.extend.scriptable", (Object) avf2.getClass().getName(), (Object) String.valueOf(objArr[0]));
                } else {
                    org.mozilla1.javascript.ScriptableObject akn = (org.mozilla1.javascript.ScriptableObject) avf2;
                    String b3 = org.mozilla1.javascript.ScriptRuntime.m7547b(lhVar, objArr[0]);
                    if (b3 != null) {
                        o2 = 0;
                    } else {
                        o2 = org.mozilla1.javascript.ScriptRuntime.m7602o(lhVar);
                    }
                    org.mozilla1.javascript.Callable mFVar = (Callable) objArr[1];
                    if (methodId != 10) {
                        z2 = false;
                    }
                    akn.mo14500a(b3, o2, mFVar, z2);
                    if (akn instanceof NativeArray) {
                        ((NativeArray) akn).mo16972hw(false);
                    }
                    return org.mozilla1.javascript.Undefined.instance;
                }
            case 11:
            case 12:
                if (objArr.length < 1 || !(avf2 instanceof org.mozilla1.javascript.ScriptableObject)) {
                    return org.mozilla1.javascript.Undefined.instance;
                }
                org.mozilla1.javascript.ScriptableObject akn2 = (org.mozilla1.javascript.ScriptableObject) avf2;
                String b4 = org.mozilla1.javascript.ScriptRuntime.m7547b(lhVar, objArr[0]);
                if (b4 != null) {
                    o = 0;
                } else {
                    o = ScriptRuntime.m7602o(lhVar);
                }
                if (methodId != 12) {
                    z2 = false;
                }
                while (true) {
                    a = akn2.mo14497a(b4, o, z2);
                    if (a == null && (prototype = akn2.getPrototype()) != null && (prototype instanceof org.mozilla1.javascript.ScriptableObject)) {
                        akn2 = (ScriptableObject) prototype;
                    }
                }
                if (a != null) {
                    return a;
                }
                return Undefined.instance;
            default:
                throw new IllegalArgumentException(String.valueOf(methodId));
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int findPrototypeId(java.lang.String r8) {
        /*
            r7 = this;
            r6 = 71
            r4 = 3
            r3 = 2
            r1 = 8
            r0 = 0
            r2 = 0
            int r5 = r8.length()
            switch(r5) {
                case 7: goto L_0x001b;
                case 8: goto L_0x001f;
                case 9: goto L_0x000f;
                case 10: goto L_0x000f;
                case 11: goto L_0x0032;
                case 12: goto L_0x000f;
                case 13: goto L_0x0036;
                case 14: goto L_0x003a;
                case 15: goto L_0x000f;
                case 16: goto L_0x004e;
                case 17: goto L_0x000f;
                case 18: goto L_0x000f;
                case 19: goto L_0x000f;
                case 20: goto L_0x0082;
                default: goto L_0x000f;
            }
        L_0x000f:
            r1 = r0
        L_0x0010:
            if (r2 == 0) goto L_0x0086
            if (r2 == r8) goto L_0x0086
            boolean r2 = r2.equals(r8)
            if (r2 != 0) goto L_0x0086
        L_0x001a:
            return r0
        L_0x001b:
            java.lang.String r2 = "valueOf"
            r1 = 4
            goto L_0x0010
        L_0x001f:
            char r4 = r8.charAt(r4)
            r5 = 111(0x6f, float:1.56E-43)
            if (r4 != r5) goto L_0x002a
            java.lang.String r2 = "toSource"
            goto L_0x0010
        L_0x002a:
            r1 = 116(0x74, float:1.63E-43)
            if (r4 != r1) goto L_0x000f
            java.lang.String r2 = "toString"
            r1 = r3
            goto L_0x0010
        L_0x0032:
            java.lang.String r2 = "constructor"
            r1 = 1
            goto L_0x0010
        L_0x0036:
            java.lang.String r2 = "isPrototypeOf"
            r1 = 7
            goto L_0x0010
        L_0x003a:
            char r1 = r8.charAt(r0)
            r3 = 104(0x68, float:1.46E-43)
            if (r1 != r3) goto L_0x0046
            java.lang.String r2 = "hasOwnProperty"
            r1 = 5
            goto L_0x0010
        L_0x0046:
            r3 = 116(0x74, float:1.63E-43)
            if (r1 != r3) goto L_0x000f
            java.lang.String r2 = "toLocaleString"
            r1 = r4
            goto L_0x0010
        L_0x004e:
            char r3 = r8.charAt(r3)
            r4 = 100
            if (r3 != r4) goto L_0x006a
            char r1 = r8.charAt(r1)
            if (r1 != r6) goto L_0x0061
            java.lang.String r2 = "__defineGetter__"
            r1 = 9
            goto L_0x0010
        L_0x0061:
            r3 = 83
            if (r1 != r3) goto L_0x000f
            java.lang.String r2 = "__defineSetter__"
            r1 = 10
            goto L_0x0010
        L_0x006a:
            r4 = 108(0x6c, float:1.51E-43)
            if (r3 != r4) goto L_0x000f
            char r1 = r8.charAt(r1)
            if (r1 != r6) goto L_0x0079
            java.lang.String r2 = "__lookupGetter__"
            r1 = 11
            goto L_0x0010
        L_0x0079:
            r3 = 83
            if (r1 != r3) goto L_0x000f
            java.lang.String r2 = "__lookupSetter__"
            r1 = 12
            goto L_0x0010
        L_0x0082:
            java.lang.String r2 = "propertyIsEnumerable"
            r1 = 6
            goto L_0x0010
        L_0x0086:
            r0 = r1
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: org.mozilla.javascript.C1069Pd.findPrototypeId(java.lang.String):int");
    }
}
