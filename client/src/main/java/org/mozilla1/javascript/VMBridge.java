package org.mozilla1.javascript;

import java.lang.reflect.Member;
import java.util.Iterator;

/* renamed from: a.aQE */
/* compiled from: a */
public abstract class VMBridge {
    public static final VMBridge iEO = dpP();

    private static VMBridge dpP() {
        VMBridge aqe;
        int i = 0;
        String[] strArr = {"org.mozilla.javascript.VMBridge_custom", "org.mozilla.javascript.jdk15.VMBridge_jdk15", "org.mozilla.javascript.jdk13.VMBridge_jdk13", "org.mozilla.javascript.jdk11.VMBridge_jdk11"};
        while (true) {
            int i2 = i;
            if (i2 == strArr.length) {
                throw new IllegalStateException("Failed to create VMBridge instance");
            }
            Class<?> classOrNull = org.mozilla1.javascript.Kit.classOrNull(strArr[i2]);
            if (classOrNull != null && (aqe = (VMBridge) Kit.m11148as(classOrNull)) != null) {
                return aqe;
            }
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: R */
    public abstract Context mo2803R(Object obj);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo2806a(Object obj, Context lhVar);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract boolean mo2807a(Member member);

    /* access modifiers changed from: protected */
    public abstract ClassLoader getCurrentThreadClassLoader();

    /* access modifiers changed from: protected */
    public abstract Object getThreadContextHelper();

    /* access modifiers changed from: protected */
    public abstract boolean tryToMakeAccessible(Object obj);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Object mo2804a(ContextFactory iwVar, Class<?>[] clsArr) {
        throw Context.m35245gF("VMBridge.getInterfaceProxyHelper is not supported");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Object mo2805a(Object obj, ContextFactory iwVar, InterfaceAdapter ks, Object obj2, Scriptable avf) {
        throw Context.m35245gF("VMBridge.newInterfaceProxy is not supported");
    }

    /* renamed from: b */
    public Iterator<?> mo10867b(Context lhVar, Scriptable avf, Object obj) {
        if (!(obj instanceof Wrapper)) {
            return null;
        }
        Object unwrap = ((Wrapper) obj).unwrap();
        if (unwrap instanceof Iterator) {
            return (Iterator) unwrap;
        }
        return null;
    }
}
