package org.mozilla1.javascript.jdk15;

import org.mozilla1.javascript.Context;
import org.mozilla1.javascript.Scriptable;
import org.mozilla1.javascript.jdk13.VMBridge_jdk13;

import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

/* renamed from: a.akY  reason: case insensitive filesystem */
/* compiled from: a */
public class VMBridge_jdk15 extends VMBridge_jdk13 {
    public VMBridge_jdk15() {
        try {
            Method.class.getMethod("isVarArgs", (Class[]) null);
        } catch (NoSuchMethodException e) {
            throw new InstantiationException(e.getMessage());
        }
    }

    /* renamed from: a */
    public boolean mo2807a(Member member) {
        if (member instanceof Method) {
            return ((Method) member).isVarArgs();
        }
        if (member instanceof Constructor) {
            return ((Constructor) member).isVarArgs();
        }
        return false;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: java.util.Iterator<?>} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Iterator<?> mo10867b(Context r4, Scriptable r5, java.lang.Object r6) {
        /*
            r3 = this;
            r0 = 0
            boolean r1 = r6 instanceof Wrapper
            if (r1 == 0) goto L_0x001c
            a.aEm r6 = (Wrapper) r6
            java.lang.Object r1 = r6.unwrap()
            boolean r2 = r1 instanceof java.util.Iterator
            if (r2 == 0) goto L_0x0012
            r0 = r1
            java.util.Iterator r0 = (java.util.Iterator) r0
        L_0x0012:
            boolean r2 = r1 instanceof java.lang.Iterable
            if (r2 == 0) goto L_0x001c
            java.lang.Iterable r1 = (java.lang.Iterable) r1
            java.util.Iterator r0 = r1.iterator()
        L_0x001c:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.mozilla.javascript.jdk15.C6312akY.mo10867b(a.lh, a.aVF, java.lang.Object):java.util.Iterator");
    }
}
