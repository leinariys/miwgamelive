package org.mozilla1.javascript;

/* renamed from: a.arK  reason: case insensitive filesystem */
/* compiled from: a */
final class NativeBoolean extends IdScriptableObject {
    static final long serialVersionUID = -3716996899943880933L;
    private static final int cBF = 4;
    private static final int ceN = 3;
    private static final Object gtp = new Integer(11);
    /* renamed from: ns */
    private static final int f5224ns = 1;
    /* renamed from: nt */
    private static final int f5225nt = 4;
    /* renamed from: xT */
    private static final int f5226xT = 2;
    private boolean gtq;

    private NativeBoolean(boolean z) {
        this.gtq = z;
    }

    /* renamed from: a */
    static void m25403a(org.mozilla1.javascript.Scriptable avf, boolean z) {
        new NativeBoolean(false).mo15695a(4, avf, z);
    }

    public String getClassName() {
        return "Boolean";
    }

    public Object getDefaultValue(Class<?> cls) {
        if (cls == org.mozilla1.javascript.ScriptRuntime.BooleanClass) {
            return org.mozilla1.javascript.ScriptRuntime.wrapBoolean(this.gtq);
        }
        return super.getDefaultValue(cls);
    }

    /* access modifiers changed from: protected */
    public void initPrototypeId(int i) {
        String str;
        int i2 = 0;
        switch (i) {
            case 1:
                i2 = 1;
                str = "constructor";
                break;
            case 2:
                str = "toString";
                break;
            case 3:
                str = "toSource";
                break;
            case 4:
                str = "valueOf";
                break;
            default:
                throw new IllegalArgumentException(String.valueOf(i));
        }
        initPrototypeMethod(gtp, i, str, i2);
    }

    public Object execIdCall(IdFunctionObject yy, Context lhVar, org.mozilla1.javascript.Scriptable avf, Scriptable avf2, Object[] objArr) {
        boolean z;
        boolean z2;
        if (!yy.hasTag(gtp)) {
            return super.execIdCall(yy, lhVar, avf, avf2, objArr);
        }
        int methodId = yy.methodId();
        if (methodId == 1) {
            if (objArr.length == 0) {
                z2 = false;
            } else {
                if (!(objArr[0] instanceof ScriptableObject) || !objArr[0].dwc()) {
                    z = org.mozilla1.javascript.ScriptRuntime.toBoolean(objArr[0]);
                } else {
                    z = true;
                }
                z2 = z;
            }
            if (avf2 == null) {
                return new NativeBoolean(z2);
            }
            return org.mozilla1.javascript.ScriptRuntime.wrapBoolean(z2);
        } else if (!(avf2 instanceof NativeBoolean)) {
            throw m25341f(yy);
        } else {
            boolean z3 = ((NativeBoolean) avf2).gtq;
            switch (methodId) {
                case 2:
                    return z3 ? "true" : "false";
                case 3:
                    return z3 ? "(new Boolean(true))" : "(new Boolean(false))";
                case 4:
                    return ScriptRuntime.wrapBoolean(z3);
                default:
                    throw new IllegalArgumentException(String.valueOf(methodId));
            }
        }
    }

    /* access modifiers changed from: protected */
    public int findPrototypeId(String str) {
        int i = 3;
        String str2 = null;
        int length = str.length();
        if (length == 7) {
            str2 = "valueOf";
            i = 4;
        } else {
            if (length == 8) {
                char charAt = str.charAt(3);
                if (charAt == 'o') {
                    str2 = "toSource";
                } else if (charAt == 't') {
                    str2 = "toString";
                    i = 2;
                }
            } else if (length == 11) {
                str2 = "constructor";
                i = 1;
            }
            i = 0;
        }
        if (str2 == null || str2 == str || str2.equals(str)) {
            return i;
        }
        return 0;
    }
}
