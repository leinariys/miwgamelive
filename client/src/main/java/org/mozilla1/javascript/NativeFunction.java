package org.mozilla1.javascript;

import org.mozilla1.javascript.debug.DebuggableScript;

/* renamed from: a.If */
/* compiled from: a */
public abstract class NativeFunction extends org.mozilla1.javascript.BaseFunction {
    /* access modifiers changed from: protected */
    public abstract int getLanguageVersion();

    /* access modifiers changed from: protected */
    public abstract int getParamAndVarCount();

    /* access modifiers changed from: protected */
    public abstract int getParamCount();

    /* access modifiers changed from: protected */
    public abstract String getParamOrVarName(int i);

    /* renamed from: b */
    public final void mo2857b(Context lhVar, Scriptable avf) {
        ScriptRuntime.m7527a((BaseFunction) this, avf);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public final String mo2858e(int i, int i2) {
        String encodedSource = getEncodedSource();
        if (encodedSource == null) {
            return super.mo2858e(i, i2);
        }
        UintMap akl = new UintMap(1);
        akl.put(1, i);
        return Decompiler.m18107a(encodedSource, i2, akl);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x000d, code lost:
        r1 = p001a.C0903NH.m7490a(p001a.C2909lh.bwA(), (p001a.C7019azg) r3);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getLength() {
        /*
            r3 = this;
            int r0 = r3.getParamCount()
            int r1 = r3.getLanguageVersion()
            r2 = 120(0x78, float:1.68E-43)
            if (r1 == r2) goto L_0x000d
        L_0x000c:
            return r0
        L_0x000d:
            a.lh r1 = p001a.C2909lh.bwA()
            a.bh r1 = p001a.C0903NH.m7490a((p001a.C2909lh) r1, (p001a.C7019azg) r3)
            if (r1 == 0) goto L_0x000c
            java.lang.Object[] r0 = r1.f5876nv
            int r0 = r0.length
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: NativeFunction.getLength():int");
    }

    public int getArity() {
        return getParamCount();
    }

    public String jsGet_name() {
        return getFunctionName();
    }

    public String getEncodedSource() {
        return null;
    }

    public DebuggableScript aVK() {
        return null;
    }

    /* renamed from: a */
    public Object mo2855a(Context lhVar, Scriptable avf, int i, Object obj, Object obj2) {
        throw new EvaluatorException("resumeGenerator() not implemented");
    }

    /* access modifiers changed from: protected */
    /* renamed from: kH */
    public boolean mo2867kH(int i) {
        return false;
    }
}
