package org.mozilla1.javascript;

/* renamed from: a.da */
/* compiled from: a */
class NativeScript extends BaseFunction {

    static final long serialVersionUID = -6795101161980121700L;
    /* renamed from: ns */
    private static final int f6547ns = 1;
    /* renamed from: nt */
    private static final int f6548nt = 4;
    /* renamed from: xS */
    private static final Object f6549xS = new Integer(21);

    /* renamed from: xT */
    private static final int f6550xT = 2;

    /* renamed from: xU */
    private static final int f6551xU = 3;

    /* renamed from: xV */
    private static final int f6552xV = 4;

    /* renamed from: xW */
    private Script f6553xW;

    private NativeScript(Script afe) {
        this.f6553xW = afe;
    }

    /* renamed from: a */
    static void m29048a(Scriptable avf, boolean z) {
        new NativeScript((Script) null).mo15695a(4, avf, z);
    }

    /* renamed from: a */
    private static NativeScript m29047a(Scriptable avf, IdFunctionObject yy) {
        if (avf instanceof NativeScript) {
            return (NativeScript) avf;
        }
        throw IdScriptableObject.m25341f(yy);
    }

    /* renamed from: a */
    private static Script m29046a(org.mozilla1.javascript.Context lhVar, String str) {
        int[] iArr = new int[1];
        String c = org.mozilla1.javascript.Context.m35242c(iArr);
        if (c == null) {
            c = "<Script object>";
            iArr[0] = 1;
        }
        return lhVar.mo20328a(str, (Evaluator) null, DefaultErrorReporter.m39748a(lhVar.bwt()), c, iArr[0], (Object) null);
    }

    public String getClassName() {
        return "Script";
    }

    public Object call(org.mozilla1.javascript.Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        if (this.f6553xW != null) {
            return this.f6553xW.mo8800a(lhVar, avf);
        }
        return Undefined.instance;
    }

    public Scriptable construct(org.mozilla1.javascript.Context lhVar, Scriptable avf, Object[] objArr) {
        throw org.mozilla1.javascript.Context.m35244gE("msg.game.script.is.not.constructor");
    }

    public int getLength() {
        return 0;
    }

    public int getArity() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public String mo2858e(int i, int i2) {
        if (this.f6553xW instanceof org.mozilla1.javascript.NativeFunction) {
            return ((NativeFunction) this.f6553xW).mo2858e(i, i2);
        }
        return super.mo2858e(i, i2);
    }

    /* access modifiers changed from: protected */
    public void initPrototypeId(int i) {
        String str;
        int i2 = 1;
        switch (i) {
            case 1:
                str = "constructor";
                break;
            case 2:
                str = "toString";
                i2 = 0;
                break;
            case 3:
                str = "compile";
                break;
            case 4:
                str = "exec";
                i2 = 0;
                break;
            default:
                throw new IllegalArgumentException(String.valueOf(i));
        }
        initPrototypeMethod(f6549xS, i, str, i2);
    }

    public Object execIdCall(IdFunctionObject yy, org.mozilla1.javascript.Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        String nh;
        if (!yy.hasTag(f6549xS)) {
            return super.execIdCall(yy, lhVar, avf, avf2, objArr);
        }
        int methodId = yy.methodId();
        switch (methodId) {
            case 1:
                if (objArr.length == 0) {
                    nh = "";
                } else {
                    nh = org.mozilla1.javascript.ScriptRuntime.toString(objArr[0]);
                }
                NativeScript daVar = new NativeScript(m29046a(lhVar, nh));
                org.mozilla1.javascript.ScriptRuntime.m7528a((ScriptableObject) daVar, avf);
                return daVar;
            case 2:
                Script afe = m29047a(avf2, yy).f6553xW;
                if (afe == null) {
                    return "";
                }
                return lhVar.mo20342a(afe, 0);
            case 3:
                NativeScript a = m29047a(avf2, yy);
                a.f6553xW = m29046a(lhVar, ScriptRuntime.toString(objArr, 0));
                return a;
            case 4:
                throw Context.m35246q("msg.cant.call.indirect", "exec");
            default:
                throw new IllegalArgumentException(String.valueOf(methodId));
        }
    }

    /* access modifiers changed from: protected */
    public int findPrototypeId(String str) {
        int i;
        String str2 = null;
        switch (str.length()) {
            case 4:
                str2 = "exec";
                i = 4;
                break;
            case 7:
                str2 = "compile";
                i = 3;
                break;
            case 8:
                str2 = "toString";
                i = 2;
                break;
            case 11:
                str2 = "constructor";
                i = 1;
                break;
            default:
                i = 0;
                break;
        }
        if (str2 == null || str2 == str || str2.equals(str)) {
            return i;
        }
        return 0;
    }
}
