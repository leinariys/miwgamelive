package org.mozilla1.javascript;


/* renamed from: a.Ap */
/* compiled from: a */
public class EcmaError extends RhinoException {
    static final long serialVersionUID = -6261226256957286699L;
    private String cfs;
    private String cft;

    EcmaError(String str, String str2, String str3, int i, String str4, int i2) {
        mo4868a(str3, i, str4, i2);
        this.cfs = str;
        this.cft = str2;
    }

    public EcmaError(org.mozilla1.javascript.Scriptable avf, String str, int i, int i2, String str2) {
        this("InternalError", ScriptRuntime.toString((Object) avf), str, i, str2, i2);
    }

    public String details() {
        return String.valueOf(this.cfs) + ": " + this.cft;
    }

    public String getName() {
        return this.cfs;
    }

    public String getErrorMessage() {
        return this.cft;
    }

    public String getSourceName() {
        return sourceName();
    }

    public int getLineNumber() {
        return lineNumber();
    }

    public int getColumnNumber() {
        return columnNumber();
    }

    public String getLineSource() {
        return lineSource();
    }

    public Scriptable auo() {
        return null;
    }
}
