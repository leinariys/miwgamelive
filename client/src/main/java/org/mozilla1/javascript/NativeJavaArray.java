package org.mozilla1.javascript;

import java.lang.reflect.Array;

/* renamed from: a.aMt  reason: case insensitive filesystem */
/* compiled from: a */
public class NativeJavaArray extends NativeJavaObject {
    static final long serialVersionUID = -924022554283675333L;
    Object array;
    Class<?> dXq;
    int length;

    public NativeJavaArray(Scriptable avf, Object obj) {
        super(avf, (Object) null, org.mozilla1.javascript.ScriptRuntime.ObjectClass);
        Class<?> cls = obj.getClass();
        if (!cls.isArray()) {
            throw new RuntimeException("Array expected");
        }
        this.array = obj;
        this.length = Array.getLength(obj);
        this.dXq = cls.getComponentType();
    }

    /* renamed from: b */
    public static NativeJavaArray m16491b(Scriptable avf, Object obj) {
        return new NativeJavaArray(avf, obj);
    }

    public String getClassName() {
        return "JavaArray";
    }

    public Object unwrap() {
        return this.array;
    }

    public boolean has(String str, Scriptable avf) {
        return str.equals("length") || super.has(str, avf);
    }

    public boolean has(int i, Scriptable avf) {
        return i >= 0 && i < this.length;
    }

    public Object get(String str, Scriptable avf) {
        if (str.equals("length")) {
            return new Integer(this.length);
        }
        Object obj = super.get(str, avf);
        if (obj != Scriptable.NOT_FOUND || ScriptableObject.m23589k(getPrototype(), str)) {
            return obj;
        }
        throw org.mozilla1.javascript.Context.m35224a("msg.java.member.not.found", (Object) this.array.getClass().getName(), (Object) str);
    }

    public Object get(int i, Scriptable avf) {
        if (i < 0 || i >= this.length) {
            return Undefined.instance;
        }
        org.mozilla1.javascript.Context bwA = org.mozilla1.javascript.Context.bwA();
        return bwA.bwy().mo6829a(bwA, this, Array.get(this.array, i), this.dXq);
    }

    public void put(String str, Scriptable avf, Object obj) {
        if (!str.equals("length")) {
            throw org.mozilla1.javascript.Context.m35246q("msg.java.array.member.not.found", str);
        }
    }

    public void put(int i, Scriptable avf, Object obj) {
        if (i < 0 || i >= this.length) {
            throw org.mozilla1.javascript.Context.m35224a("msg.java.array.index.out.of.bounds", (Object) String.valueOf(i), (Object) String.valueOf(this.length - 1));
        }
        Array.set(this.array, i, Context.jsToJava(obj, this.dXq));
    }

    public Object getDefaultValue(Class<?> cls) {
        if (cls == null || cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
            return this.array.toString();
        }
        if (cls == org.mozilla1.javascript.ScriptRuntime.BooleanClass) {
            return Boolean.TRUE;
        }
        if (cls == org.mozilla1.javascript.ScriptRuntime.NumberClass) {
            return ScriptRuntime.NaNobj;
        }
        return this;
    }

    public Object[] getIds() {
        Object[] objArr = new Object[this.length];
        int i = this.length;
        while (true) {
            i--;
            if (i < 0) {
                return objArr;
            }
            objArr[i] = new Integer(i);
        }
    }

    public boolean hasInstance(Scriptable avf) {
        if (!(avf instanceof Wrapper)) {
            return false;
        }
        return this.dXq.isInstance(((Wrapper) avf).unwrap());
    }

    public Scriptable getPrototype() {
        if (this.f6799EX == null) {
            this.f6799EX = ScriptableObject.m23585i(getParentScope(), "Array");
        }
        return this.f6799EX;
    }
}
