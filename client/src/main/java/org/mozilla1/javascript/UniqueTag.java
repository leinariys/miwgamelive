package org.mozilla1.javascript;

import java.io.Serializable;

/* renamed from: a.bd */
/* compiled from: a */
public final class UniqueTag implements Serializable {

    /* renamed from: nb */
    public static final UniqueTag f5854nb = new UniqueTag(1);
    /* renamed from: nc */
    public static final UniqueTag f5855nc = new UniqueTag(2);
    /* renamed from: nd */
    public static final UniqueTag f5856nd = new UniqueTag(3);
    static final long serialVersionUID = -4320556826714577259L;
    /* renamed from: mY */
    private static final int f5851mY = 1;
    /* renamed from: mZ */
    private static final int f5852mZ = 2;
    /* renamed from: na */
    private static final int f5853na = 3;
    /* renamed from: ne */
    private final int f5857ne;

    private UniqueTag(int i) {
        this.f5857ne = i;
    }

    public Object readResolve() {
        switch (this.f5857ne) {
            case 1:
                return f5854nb;
            case 2:
                return f5855nc;
            case 3:
                return f5856nd;
            default:
                throw new IllegalStateException(String.valueOf(this.f5857ne));
        }
    }

    public String toString() {
        String str;
        switch (this.f5857ne) {
            case 1:
                str = "NOT_FOUND";
                break;
            case 2:
                str = "NULL_VALUE";
                break;
            case 3:
                str = "DOUBLE_MARK";
                break;
            default:
                throw Kit.codeBug();
        }
        return String.valueOf(super.toString()) + ": " + str;
    }
}
