package org.mozilla1.javascript.xml;

import org.mozilla1.javascript.Ref;

/* renamed from: a.atD  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class XMLObject extends org.mozilla1.javascript.IdScriptableObject {
    public XMLObject() {
    }

    public XMLObject(org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2) {
        super(avf, avf2);
    }

    public abstract boolean ecmaDelete(org.mozilla1.javascript.Context lhVar, Object obj);

    public abstract Object ecmaGet(org.mozilla1.javascript.Context lhVar, Object obj);

    public abstract boolean ecmaHas(org.mozilla1.javascript.Context lhVar, Object obj);

    public abstract void ecmaPut(org.mozilla1.javascript.Context lhVar, Object obj, Object obj2);

    public abstract org.mozilla1.javascript.NativeWith enterDotQuery(org.mozilla1.javascript.Scriptable avf);

    public abstract org.mozilla1.javascript.NativeWith enterWith(org.mozilla1.javascript.Scriptable avf);

    public abstract org.mozilla1.javascript.Scriptable getExtraMethodSource(org.mozilla1.javascript.Context lhVar);

    public abstract org.mozilla1.javascript.Ref memberRef(org.mozilla1.javascript.Context lhVar, Object obj, int i);

    public abstract Ref memberRef(org.mozilla1.javascript.Context lhVar, Object obj, Object obj2, int i);

    public Object addValues(org.mozilla1.javascript.Context lhVar, boolean z, Object obj) {
        return org.mozilla1.javascript.Scriptable.NOT_FOUND;
    }
}
