package org.mozilla1.javascript.xml;

import org.mozilla1.javascript.ScriptRuntime;

/* renamed from: a.AM */
/* compiled from: a */
public abstract class XMLLib {
    private static final Object ckf = new Integer(25);

    /* renamed from: a */
    public static XMLLib m369a(org.mozilla1.javascript.Scriptable avf) {
        org.mozilla1.javascript.ScriptableObject p = org.mozilla1.javascript.ScriptRuntime.m7604p(avf);
        if (p == null) {
            return null;
        }
        org.mozilla1.javascript.ScriptableObject.m23587j(p, "XML");
        return (XMLLib) p.getAssociatedValue(ckf);
    }

    /* renamed from: b */
    public static XMLLib m370b(org.mozilla1.javascript.Scriptable avf) {
        XMLLib a = m369a(avf);
        if (a != null) {
            return a;
        }
        throw org.mozilla1.javascript.Context.m35245gF(org.mozilla1.javascript.ScriptRuntime.getMessage0("msg.XML.not.available"));
    }

    public abstract String escapeAttributeValue(Object obj);

    public abstract String escapeTextValue(Object obj);

    public abstract boolean isXMLName(org.mozilla1.javascript.Context lhVar, Object obj);

    public abstract org.mozilla1.javascript.Ref nameRef(org.mozilla1.javascript.Context lhVar, Object obj, org.mozilla1.javascript.Scriptable avf, int i);

    public abstract org.mozilla1.javascript.Ref nameRef(org.mozilla1.javascript.Context lhVar, Object obj, Object obj2, org.mozilla1.javascript.Scriptable avf, int i);

    public abstract Object toDefaultXmlNamespace(org.mozilla1.javascript.Context lhVar, Object obj);

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public final XMLLib mo244c(org.mozilla1.javascript.Scriptable avf) {
        org.mozilla1.javascript.ScriptableObject p = ScriptRuntime.m7604p(avf);
        if (p != null) {
            return (XMLLib) p.associateValue(ckf, this);
        }
        throw new IllegalStateException();
    }

    /* renamed from: a.AM$a */
    public static abstract class C0026a {
        /* renamed from: lp */
        public static C0026a m372lp(String str) {
            return new C0027a(str);
        }

        public abstract String bjd();

        /* renamed from: a.AM$a$a */
        static class C0027a extends C0026a {
            private final /* synthetic */ String dEr;

            C0027a(String str) {
                this.dEr = str;
            }

            public String bjd() {
                return this.dEr;
            }
        }
    }
}
