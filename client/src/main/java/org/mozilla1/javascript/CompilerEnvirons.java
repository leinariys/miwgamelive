package org.mozilla1.javascript;


import java.util.Set;

/* renamed from: a.aKI */
/* compiled from: a */
public class CompilerEnvirons {
    Set<String> elF;
    private boolean elN = false;
    private ErrorReporter errorReporter = DefaultErrorReporter.bkW;
    private boolean elu = true;
    private boolean elw = false;
    private int elx = 0;
    private int frE = 0;
    private boolean iiN = true;
    private boolean iiO = false;
    private boolean iiP = false;
    private boolean iiQ = true;
    private boolean iiR = false;
    private boolean iiS = false;

    /* renamed from: f */
    public void mo9683f(org.mozilla1.javascript.Context lhVar) {
        setErrorReporter(lhVar.bwt());
        this.frE = lhVar.getLanguageVersion();
        this.elw = lhVar.elv;
        this.iiN = !lhVar.isGeneratingDebugChanged() || lhVar.isGeneratingDebug();
        this.iiO = lhVar.hasFeature(3);
        this.iiP = lhVar.hasFeature(2);
        this.iiR = lhVar.hasFeature(11);
        this.iiS = lhVar.hasFeature(12);
        this.iiQ = lhVar.hasFeature(6);
        this.elx = lhVar.getOptimizationLevel();
        this.elu = lhVar.isGeneratingSource();
        this.elF = lhVar.elF;
        this.elN = lhVar.elN;
    }

    public final ErrorReporter bwt() {
        return this.errorReporter;
    }

    public void setErrorReporter(ErrorReporter ale) {
        if (ale == null) {
            throw new IllegalArgumentException();
        }
        this.errorReporter = ale;
    }

    public final int getLanguageVersion() {
        return this.frE;
    }

    public void setLanguageVersion(int i) {
        org.mozilla1.javascript.Context.checkLanguageVersion(i);
        this.frE = i;
    }

    public final boolean isGenerateDebugInfo() {
        return this.iiN;
    }

    public void setGenerateDebugInfo(boolean z) {
        this.iiN = z;
    }

    public final boolean isUseDynamicScope() {
        return this.elw;
    }

    public final boolean isReservedKeywordAsIdentifier() {
        return this.iiO;
    }

    public void setReservedKeywordAsIdentifier(boolean z) {
        this.iiO = z;
    }

    public final boolean isAllowMemberExprAsFunctionName() {
        return this.iiP;
    }

    public void setAllowMemberExprAsFunctionName(boolean z) {
        this.iiP = z;
    }

    public final boolean isXmlAvailable() {
        return this.iiQ;
    }

    public void setXmlAvailable(boolean z) {
        this.iiQ = z;
    }

    public final int getOptimizationLevel() {
        return this.elx;
    }

    public void setOptimizationLevel(int i) {
        Context.checkOptimizationLevel(i);
        this.elx = i;
    }

    public final boolean isGeneratingSource() {
        return this.elu;
    }

    public void setGeneratingSource(boolean z) {
        this.elu = z;
    }

    public final boolean dgs() {
        return this.iiR;
    }

    public final boolean dgt() {
        return this.iiS;
    }

    public boolean dgu() {
        return this.elN;
    }

    /* renamed from: dv */
    public void mo9682dv(boolean z) {
        this.elN = z;
    }
}
