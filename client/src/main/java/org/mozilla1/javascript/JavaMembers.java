package org.mozilla1.javascript;


import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.PT */
/* compiled from: a */
class JavaMembers {

    org.mozilla1.javascript.MemberBox[] gcX;
    /* renamed from: Fa */
    private Map<String, org.mozilla1.javascript.FieldAndMethods> f1373Fa;
    private Map<String, org.mozilla1.javascript.FieldAndMethods> cZm;
    private Class<?> gcU;
    private Map<String, Object> gcV;
    private Map<String, Object> gcW;
    private boolean gcY;

    JavaMembers(org.mozilla1.javascript.Scriptable avf, Class<?> cls) {
        this(avf, cls, false);
    }

    JavaMembers(org.mozilla1.javascript.Scriptable avf, Class<?> cls, boolean z) {
        try {
            org.mozilla1.javascript.Context bkO = ContextFactory.getGlobal().enterContext();
            ClassShutter bwx = bkO.getClassShutter();
            if (bwx == null || bwx.visibleToScripts(cls.getName())) {
                this.gcY = bkO.hasFeature(13);
                this.gcV = new HashMap();
                this.gcW = new HashMap();
                this.gcU = cls;
                m8440b(avf, z);
                return;
            }
            throw org.mozilla1.javascript.Context.m35246q("msg.access.prohibited", cls.getName());
        } finally {
            org.mozilla1.javascript.Context.exit();
        }
    }

    /* renamed from: ar */
    static String m8436ar(Class<?> cls) {
        if (!cls.isArray()) {
            return cls.getName();
        }
        int i = 0;
        do {
            i++;
            cls = cls.getComponentType();
        } while (cls.isArray());
        String name = cls.getName();
        if (i == 1) {
            return name.concat("[]");
        }
        StringBuffer stringBuffer = new StringBuffer(name.length() + ("[]".length() * i));
        stringBuffer.append(name);
        while (i != 0) {
            i--;
            stringBuffer.append("[]");
        }
        return stringBuffer.toString();
    }

    /* renamed from: b */
    static String liveConnectSignature(Class<?>[] clsArr) {
        int length = clsArr.length;
        if (length == 0) {
            return "()";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append('(');
        for (int i = 0; i != length; i++) {
            if (i != 0) {
                stringBuffer.append(',');
            }
            stringBuffer.append(m8436ar(clsArr[i]));
        }
        stringBuffer.append(')');
        return stringBuffer.toString();
    }

    /* renamed from: a */
    private static Method[] m8435a(Class<?> cls, boolean z, boolean z2) {
        HashMap hashMap = new HashMap();
        m8434a(cls, (Map<C1057a, Method>) hashMap, z, z2);
        return (Method[]) hashMap.values().toArray(new Method[hashMap.size()]);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001c, code lost:
        r6 = r6.getSuperclass();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0021, code lost:
        r3 = r2[r1];
        r4 = r3.getModifiers();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002b, code lost:
        if (java.lang.reflect.Modifier.isPublic(r4) != false) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0031, code lost:
        if (java.lang.reflect.Modifier.isProtected(r4) != false) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0033, code lost:
        if (r9 == false) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0035, code lost:
        if (r9 == false) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0037, code lost:
        r3.setAccessible(true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003b, code lost:
        r7.put(new JavaMembers.C1057a(r3), r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0043, code lost:
        r1 = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r2 = r6.getMethods();
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004d, code lost:
        if (r1 < r2.length) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004f, code lost:
        r3 = r2[r1];
        r4 = new JavaMembers.C1057a(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x005a, code lost:
        if (r7.get(r4) == null) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005c, code lost:
        r7.put(r4, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005f, code lost:
        r1 = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0062, code lost:
        r2 = r6.getMethods();
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x000b, code lost:
        if (r9 != false) goto L_0x000d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0068, code lost:
        if (r1 >= r2.length) goto L_0x0013;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x006a, code lost:
        r3 = r2[r1];
        r7.put(new JavaMembers.C1057a(r3), r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0074, code lost:
        r1 = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0078, code lost:
        p001a.C2909lh.reportWarning("Could not discover accessible methods of class " + r6.getName() + " due to lack of privileges, " + "attemping superclasses/interfaces.");
        r6 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x009a, code lost:
        r1 = r6.getInterfaces();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x009f, code lost:
        if (r0 < r1.length) goto L_0x00ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a1, code lost:
        r0 = r6.getSuperclass();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000d, code lost:
        if (r8 != false) goto L_0x0011;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a5, code lost:
        if (r0 != null) goto L_0x00a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00a7, code lost:
        m8434a((java.lang.Class<?>) r0, r7, r8, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00ac, code lost:
        m8434a((java.lang.Class<?>) r1[r0], r7, r8, r9);
        r0 = r0 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0011, code lost:
        r6 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000f, code lost:
        if (r9 == false) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (r6 != null) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
        r2 = r6.getDeclaredMethods();
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        if (r1 < r2.length) goto L_0x0021;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m8434a(java.lang.Class<?> r6, java.util.Map<JavaMembers.C1057a, java.lang.reflect.Method> r7, boolean r8, boolean r9) {
        /*
            r0 = 0
            int r1 = r6.getModifiers()
            boolean r1 = java.lang.reflect.Modifier.isPublic(r1)
            if (r1 != 0) goto L_0x000d
            if (r9 == 0) goto L_0x009a
        L_0x000d:
            if (r8 != 0) goto L_0x0011
            if (r9 == 0) goto L_0x0062
        L_0x0011:
            if (r6 != 0) goto L_0x0014
        L_0x0013:
            return
        L_0x0014:
            java.lang.reflect.Method[] r2 = r6.getDeclaredMethods()     // Catch:{ SecurityException -> 0x0046 }
            r1 = r0
        L_0x0019:
            int r3 = r2.length     // Catch:{ SecurityException -> 0x0046 }
            if (r1 < r3) goto L_0x0021
            java.lang.Class r6 = r6.getSuperclass()     // Catch:{ SecurityException -> 0x0046 }
            goto L_0x0011
        L_0x0021:
            r3 = r2[r1]     // Catch:{ SecurityException -> 0x0046 }
            int r4 = r3.getModifiers()     // Catch:{ SecurityException -> 0x0046 }
            boolean r5 = java.lang.reflect.Modifier.isPublic(r4)     // Catch:{ SecurityException -> 0x0046 }
            if (r5 != 0) goto L_0x0035
            boolean r4 = java.lang.reflect.Modifier.isProtected(r4)     // Catch:{ SecurityException -> 0x0046 }
            if (r4 != 0) goto L_0x0035
            if (r9 == 0) goto L_0x0043
        L_0x0035:
            if (r9 == 0) goto L_0x003b
            r4 = 1
            r3.setAccessible(r4)     // Catch:{ SecurityException -> 0x0046 }
        L_0x003b:
            a.PT$a r4 = new a.PT$a     // Catch:{ SecurityException -> 0x0046 }
            r4.<init>(r3)     // Catch:{ SecurityException -> 0x0046 }
            r7.put(r4, r3)     // Catch:{ SecurityException -> 0x0046 }
        L_0x0043:
            int r1 = r1 + 1
            goto L_0x0019
        L_0x0046:
            r1 = move-exception
            java.lang.reflect.Method[] r2 = r6.getMethods()     // Catch:{ SecurityException -> 0x0077 }
            r1 = r0
        L_0x004c:
            int r3 = r2.length     // Catch:{ SecurityException -> 0x0077 }
            if (r1 >= r3) goto L_0x0013
            r3 = r2[r1]     // Catch:{ SecurityException -> 0x0077 }
            a.PT$a r4 = new a.PT$a     // Catch:{ SecurityException -> 0x0077 }
            r4.<init>(r3)     // Catch:{ SecurityException -> 0x0077 }
            java.lang.Object r5 = r7.get(r4)     // Catch:{ SecurityException -> 0x0077 }
            if (r5 != 0) goto L_0x005f
            r7.put(r4, r3)     // Catch:{ SecurityException -> 0x0077 }
        L_0x005f:
            int r1 = r1 + 1
            goto L_0x004c
        L_0x0062:
            java.lang.reflect.Method[] r2 = r6.getMethods()     // Catch:{ SecurityException -> 0x0077 }
            r1 = r0
        L_0x0067:
            int r3 = r2.length     // Catch:{ SecurityException -> 0x0077 }
            if (r1 >= r3) goto L_0x0013
            r3 = r2[r1]     // Catch:{ SecurityException -> 0x0077 }
            a.PT$a r4 = new a.PT$a     // Catch:{ SecurityException -> 0x0077 }
            r4.<init>(r3)     // Catch:{ SecurityException -> 0x0077 }
            r7.put(r4, r3)     // Catch:{ SecurityException -> 0x0077 }
            int r1 = r1 + 1
            goto L_0x0067
        L_0x0077:
            r1 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Could not discover accessible methods of class "
            r1.<init>(r2)
            java.lang.String r2 = r6.getName()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " due to lack of privileges, "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "attemping superclasses/interfaces."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            p001a.C2909lh.reportWarning(r1)
        L_0x009a:
            java.lang.Class[] r1 = r6.getInterfaces()
        L_0x009e:
            int r2 = r1.length
            if (r0 < r2) goto L_0x00ac
            java.lang.Class r0 = r6.getSuperclass()
            if (r0 == 0) goto L_0x0013
            m8434a((java.lang.Class<?>) r0, (java.util.Map<JavaMembers.C1057a, java.lang.reflect.Method>) r7, (boolean) r8, (boolean) r9)
            goto L_0x0013
        L_0x00ac:
            r2 = r1[r0]
            m8434a((java.lang.Class<?>) r2, (java.util.Map<JavaMembers.C1057a, java.lang.reflect.Method>) r7, (boolean) r8, (boolean) r9)
            int r0 = r0 + 1
            goto L_0x009e
        */
        throw new UnsupportedOperationException("Method not decompiled: JavaMembers.m8434a(java.lang.Class, java.util.Map, boolean, boolean):void");
    }

    /* renamed from: a */
    private static org.mozilla1.javascript.MemberBox m8433a(org.mozilla1.javascript.MemberBox[] amtArr, boolean z) {
        org.mozilla1.javascript.MemberBox amt = null;
        int i = 0;
        while (true) {
            if (i >= amtArr.length) {
                break;
            }
            amt = amtArr[i];
            if (amt.gaM.length != 0 || (z && !amt.isStatic())) {
                i++;
            }
        }
        if (amt.cjw().getReturnType() != Void.TYPE) {
            return amt;
        }
        return null;
    }

    /* renamed from: a */
    private static org.mozilla1.javascript.MemberBox m8431a(Class<?> cls, org.mozilla1.javascript.MemberBox[] amtArr, boolean z) {
        for (int i = 1; i <= 2; i++) {
            for (org.mozilla1.javascript.MemberBox amt : amtArr) {
                if (!z || amt.isStatic()) {
                    Class<?>[] clsArr = amt.gaM;
                    if (clsArr.length != 1) {
                        continue;
                    } else if (i != 1) {
                        if (i != 2) {
                            Kit.codeBug();
                        }
                        if (clsArr[0].isAssignableFrom(cls)) {
                            return amt;
                        }
                    } else if (clsArr[0] == cls) {
                        return amt;
                    }
                }
            }
        }
        return null;
    }

    /* renamed from: b */
    private static org.mozilla1.javascript.MemberBox m8437b(org.mozilla1.javascript.MemberBox[] amtArr, boolean z) {
        for (org.mozilla1.javascript.MemberBox amt : amtArr) {
            if ((!z || amt.isStatic()) && amt.cjw().getReturnType() == Void.TYPE && amt.gaM.length == 1) {
                return amt;
            }
        }
        return null;
    }

    /* renamed from: a */
    static JavaMembers m8430a(org.mozilla1.javascript.Scriptable avf, Class<?> cls, Class<?> cls2, boolean z) {
        JavaMembers pt;
        org.mozilla1.javascript.Scriptable x = org.mozilla1.javascript.ScriptableObject.m23595x(avf);
        ClassCache k = ClassCache.m23644k(x);
        Map<Class<?>, JavaMembers> cgl = k.cgl();
        Class<?> cls3 = cls2;
        while (true) {
            pt = cgl.get(cls);
            if (pt != null) {
                break;
            }
            try {
                pt = new JavaMembers(x, cls, z);
                if (k.isCachingEnabled()) {
                    cgl.put(cls, pt);
                }
            } catch (SecurityException e) {
                SecurityException securityException = e;
                if (cls3 == null || !cls3.isInterface()) {
                    Class<? super Object> superclass = (Class<? super Object>) cls.getSuperclass();
                    if (superclass != null) {
                        cls = superclass;
                    } else if (cls.isInterface()) {
                        cls = org.mozilla1.javascript.ScriptRuntime.ObjectClass;
                    } else {
                        throw securityException;
                    }
                } else {
                    cls = cls3;
                    cls3 = null;
                }
            }
        }
        return pt;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: n */
    public boolean mo4683n(String str, boolean z) {
        if ((z ? this.gcW : this.gcV).get(str) != null) {
            return true;
        }
        return m8441o(str, z) != null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Object mo4678a(org.mozilla1.javascript.Scriptable avf, String str, Object obj, boolean z) {
        Object obj2;
        Class<?> type;
        Object obj3 = (z ? this.gcW : this.gcV).get(str);
        if (!z && obj3 == null) {
            obj3 = this.gcW.get(str);
        }
        if (obj3 == null && (obj3 = m8438b(avf, str, obj, z)) == null) {
            return org.mozilla1.javascript.Scriptable.NOT_FOUND;
        }
        if (obj3 instanceof org.mozilla1.javascript.Scriptable) {
            return obj3;
        }
        org.mozilla1.javascript.Context bwA = org.mozilla1.javascript.Context.bwA();
        try {
            if (obj3 instanceof org.mozilla1.javascript.BeanProperty) {
                org.mozilla1.javascript.BeanProperty aaa = (org.mozilla1.javascript.BeanProperty) obj3;
                if (aaa.hcz == null) {
                    return org.mozilla1.javascript.Scriptable.NOT_FOUND;
                }
                obj2 = aaa.hcz.invoke(obj, org.mozilla1.javascript.Context.emptyArgs);
                type = aaa.hcz.cjw().getReturnType();
            } else {
                Field field = (Field) obj3;
                if (z) {
                    obj = null;
                }
                obj2 = field.get(obj);
                type = field.getType();
            }
            return bwA.bwy().mo6829a(bwA, org.mozilla1.javascript.ScriptableObject.m23595x(avf), obj2, type);
        } catch (Exception e) {
            throw org.mozilla1.javascript.Context.throwAsScriptRuntimeEx(e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo4680a(org.mozilla1.javascript.Scriptable avf, String str, Object obj, Object obj2, boolean z) {
        Object obj3;
        String str2;
        Map<String, Object> map = z ? this.gcW : this.gcV;
        Object obj4 = map.get(str);
        if (!z && obj4 == null) {
            obj4 = this.gcW.get(str);
        }
        if (obj4 == null) {
            throw mo4682jy(str);
        }
        if (obj4 instanceof org.mozilla1.javascript.FieldAndMethods) {
            obj3 = ((org.mozilla1.javascript.FieldAndMethods) map.get(str)).field;
        } else {
            obj3 = obj4;
        }
        if (obj3 instanceof org.mozilla1.javascript.BeanProperty) {
            org.mozilla1.javascript.BeanProperty aaa = (org.mozilla1.javascript.BeanProperty) obj3;
            if (aaa.hcA == null) {
                throw mo4682jy(str);
            } else if (aaa.hcB == null || obj2 == null) {
                try {
                    aaa.hcA.invoke(obj, new Object[]{org.mozilla1.javascript.Context.jsToJava(obj2, aaa.hcA.gaM[0])});
                } catch (Exception e) {
                    throw org.mozilla1.javascript.Context.throwAsScriptRuntimeEx(e);
                }
            } else {
                aaa.hcB.call(org.mozilla1.javascript.Context.bwA(), org.mozilla1.javascript.ScriptableObject.m23595x(avf), avf, new Object[]{obj2});
            }
        } else if (!(obj3 instanceof Field)) {
            if (obj3 == null) {
                str2 = "msg.java.internal.private";
            } else {
                str2 = "msg.java.method.assign";
            }
            throw org.mozilla1.javascript.Context.m35246q(str2, str);
        } else {
            Field field = (Field) obj3;
            try {
                field.set(obj, org.mozilla1.javascript.Context.jsToJava(obj2, field.getType()));
            } catch (IllegalAccessException e2) {
                if ((field.getModifiers() & 16) == 0) {
                    throw org.mozilla1.javascript.Context.throwAsScriptRuntimeEx(e2);
                }
            } catch (IllegalArgumentException e3) {
                throw org.mozilla1.javascript.Context.m35225a("msg.java.internal.field.type", (Object) obj2.getClass().getName(), (Object) field, (Object) obj.getClass().getName());
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: dP */
    public Object[] mo4681dP(boolean z) {
        Map<String, Object> map = z ? this.gcW : this.gcV;
        return map.keySet().toArray(new Object[map.size()]);
    }

    /* renamed from: o */
    private org.mozilla1.javascript.MemberBox m8441o(String str, boolean z) {
        boolean z2;
        org.mozilla1.javascript.MemberBox[] amtArr;
        int indexOf = str.indexOf(40);
        if (indexOf < 0) {
            return null;
        }
        Map<String, Object> map = z ? this.gcW : this.gcV;
        org.mozilla1.javascript.MemberBox[] amtArr2 = null;
        if (!z || indexOf != 0) {
            z2 = false;
        } else {
            z2 = true;
        }
        if (z2) {
            amtArr = this.gcX;
        } else {
            String substring = str.substring(0, indexOf);
            Object obj = map.get(substring);
            if (!z && obj == null) {
                obj = this.gcW.get(substring);
            }
            if (obj instanceof org.mozilla1.javascript.NativeJavaMethod) {
                amtArr = ((org.mozilla1.javascript.NativeJavaMethod) obj).fOb;
            } else {
                amtArr = amtArr2;
            }
        }
        if (amtArr == null) {
            return null;
        }
        for (int i = 0; i < amtArr.length; i++) {
            String b = liveConnectSignature(amtArr[i].gaM);
            if (b.length() + indexOf == str.length() && str.regionMatches(indexOf, b, 0, b.length())) {
                return amtArr[i];
            }
        }
        return null;
    }

    /* renamed from: b */
    private Object m8438b(org.mozilla1.javascript.Scriptable avf, String str, Object obj, boolean z) {
        Map<String, Object> map = z ? this.gcW : this.gcV;
        org.mozilla1.javascript.MemberBox o = m8441o(str, z);
        if (o == null) {
            return null;
        }
        org.mozilla1.javascript.Scriptable w = ScriptableObject.m23594w(avf);
        if (o.cjz()) {
            C5997aeV aev = new C5997aeV(o);
            aev.setPrototype(w);
            map.put(str, aev);
            return aev;
        }
        Object obj2 = map.get(o.getName());
        if (!(obj2 instanceof org.mozilla1.javascript.NativeJavaMethod) || ((org.mozilla1.javascript.NativeJavaMethod) obj2).fOb.length <= 1) {
            return obj2;
        }
        org.mozilla1.javascript.NativeJavaMethod aic = new org.mozilla1.javascript.NativeJavaMethod(o, str);
        aic.setPrototype(w);
        map.put(str, aic);
        return aic;
    }

    /* JADX WARNING: Removed duplicated region for block: B:126:0x021c  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x0231  */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x02b0  */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m8440b(org.mozilla1.javascript.Scriptable r13, boolean r14) {
        /*
            r12 = this;
            java.lang.Class<?> r0 = r12.gcU
            boolean r1 = r12.gcY
            java.lang.reflect.Method[] r4 = m8435a((java.lang.Class<?>) r0, (boolean) r14, (boolean) r1)
            r0 = 0
            r1 = r0
        L_0x000a:
            int r0 = r4.length
            if (r1 < r0) goto L_0x002e
            r0 = 0
            r6 = r0
        L_0x000f:
            r0 = 2
            if (r6 != r0) goto L_0x0070
            java.lang.reflect.Field[] r3 = r12.ckM()
            r0 = 0
            r1 = r0
        L_0x0018:
            int r0 = r3.length
            if (r1 < r0) goto L_0x00df
            r0 = 0
            r8 = r0
        L_0x001d:
            r0 = 2
            if (r8 != r0) goto L_0x0180
            java.lang.reflect.Constructor[] r1 = r12.ckL()
            int r0 = r1.length
            a.amt[] r0 = new p001a.C6437amt[r0]
            r12.gcX = r0
            r0 = 0
        L_0x002a:
            int r2 = r1.length
            if (r0 != r2) goto L_0x029c
            return
        L_0x002e:
            r5 = r4[r1]
            int r0 = r5.getModifiers()
            boolean r0 = java.lang.reflect.Modifier.isStatic(r0)
            if (r0 == 0) goto L_0x004e
            java.util.Map<java.lang.String, java.lang.Object> r0 = r12.gcW
            r2 = r0
        L_0x003d:
            java.lang.String r6 = r5.getName()
            java.lang.Object r0 = r2.get(r6)
            if (r0 != 0) goto L_0x0052
            r2.put(r6, r5)
        L_0x004a:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x000a
        L_0x004e:
            java.util.Map<java.lang.String, java.lang.Object> r0 = r12.gcV
            r2 = r0
            goto L_0x003d
        L_0x0052:
            boolean r3 = r0 instanceof p001a.C5271aCx
            if (r3 == 0) goto L_0x005c
            a.aCx r0 = (p001a.C5271aCx) r0
        L_0x0058:
            r0.add(r5)
            goto L_0x004a
        L_0x005c:
            boolean r3 = r0 instanceof java.lang.reflect.Method
            if (r3 != 0) goto L_0x0063
            p001a.C1520WN.codeBug()
        L_0x0063:
            a.aCx r3 = new a.aCx
            r3.<init>()
            r3.add(r0)
            r2.put(r6, r3)
            r0 = r3
            goto L_0x0058
        L_0x0070:
            if (r6 != 0) goto L_0x008a
            r0 = 1
        L_0x0073:
            if (r0 == 0) goto L_0x008c
            java.util.Map<java.lang.String, java.lang.Object> r0 = r12.gcW
            r3 = r0
        L_0x0078:
            java.util.Set r0 = r3.keySet()
            java.util.Iterator r7 = r0.iterator()
        L_0x0080:
            boolean r0 = r7.hasNext()
            if (r0 != 0) goto L_0x0090
            int r0 = r6 + 1
            r6 = r0
            goto L_0x000f
        L_0x008a:
            r0 = 0
            goto L_0x0073
        L_0x008c:
            java.util.Map<java.lang.String, java.lang.Object> r0 = r12.gcV
            r3 = r0
            goto L_0x0078
        L_0x0090:
            java.lang.Object r0 = r7.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r3.get(r0)
            boolean r2 = r1 instanceof java.lang.reflect.Method
            if (r2 == 0) goto L_0x00ba
            r2 = 1
            a.amt[] r2 = new p001a.C6437amt[r2]
            r4 = 0
            a.amt r5 = new a.amt
            java.lang.reflect.Method r1 = (java.lang.reflect.Method) r1
            r5.<init>((java.lang.reflect.Method) r1)
            r2[r4] = r5
            r1 = r2
        L_0x00ac:
            a.aiC r2 = new a.aiC
            r2.<init>(r1)
            if (r13 == 0) goto L_0x00b6
            p001a.C0903NH.m7527a((p001a.C5913acp) r2, (p001a.aVF) r13)
        L_0x00b6:
            r3.put(r0, r2)
            goto L_0x0080
        L_0x00ba:
            a.aCx r1 = (p001a.C5271aCx) r1
            int r8 = r1.size()
            r2 = 2
            if (r8 >= r2) goto L_0x00c6
            p001a.C1520WN.codeBug()
        L_0x00c6:
            a.amt[] r4 = new p001a.C6437amt[r8]
            r2 = 0
            r5 = r2
        L_0x00ca:
            if (r5 != r8) goto L_0x00ce
            r1 = r4
            goto L_0x00ac
        L_0x00ce:
            java.lang.Object r2 = r1.get(r5)
            java.lang.reflect.Method r2 = (java.lang.reflect.Method) r2
            a.amt r9 = new a.amt
            r9.<init>((java.lang.reflect.Method) r2)
            r4[r5] = r9
            int r2 = r5 + 1
            r5 = r2
            goto L_0x00ca
        L_0x00df:
            r4 = r3[r1]
            java.lang.String r5 = r4.getName()
            int r0 = r4.getModifiers()
            boolean r2 = r12.gcY
            if (r2 != 0) goto L_0x00f8
            boolean r2 = java.lang.reflect.Modifier.isPublic(r0)
            if (r2 != 0) goto L_0x00f8
        L_0x00f3:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0018
        L_0x00f8:
            boolean r6 = java.lang.reflect.Modifier.isStatic(r0)     // Catch:{ SecurityException -> 0x010b }
            if (r6 == 0) goto L_0x0135
            java.util.Map<java.lang.String, java.lang.Object> r0 = r12.gcW     // Catch:{ SecurityException -> 0x010b }
            r2 = r0
        L_0x0101:
            java.lang.Object r0 = r2.get(r5)     // Catch:{ SecurityException -> 0x010b }
            if (r0 != 0) goto L_0x0139
            r2.put(r5, r4)     // Catch:{ SecurityException -> 0x010b }
            goto L_0x00f3
        L_0x010b:
            r0 = move-exception
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = "Could not access field "
            r0.<init>(r2)
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r2 = " of class "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.Class<?> r2 = r12.gcU
            java.lang.String r2 = r2.getName()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = " due to lack of privileges."
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            p001a.C2909lh.reportWarning(r0)
            goto L_0x00f3
        L_0x0135:
            java.util.Map<java.lang.String, java.lang.Object> r0 = r12.gcV     // Catch:{ SecurityException -> 0x010b }
            r2 = r0
            goto L_0x0101
        L_0x0139:
            boolean r7 = r0 instanceof p001a.C6186aiC     // Catch:{ SecurityException -> 0x010b }
            if (r7 == 0) goto L_0x0162
            a.aiC r0 = (p001a.C6186aiC) r0     // Catch:{ SecurityException -> 0x010b }
            a.TR r7 = new a.TR     // Catch:{ SecurityException -> 0x010b }
            a.amt[] r0 = r0.fOb     // Catch:{ SecurityException -> 0x010b }
            r7.<init>(r13, r0, r4)     // Catch:{ SecurityException -> 0x010b }
            if (r6 == 0) goto L_0x015c
            java.util.Map<java.lang.String, a.TR> r0 = r12.cZm     // Catch:{ SecurityException -> 0x010b }
        L_0x014a:
            if (r0 != 0) goto L_0x0155
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ SecurityException -> 0x010b }
            r0.<init>()     // Catch:{ SecurityException -> 0x010b }
            if (r6 == 0) goto L_0x015f
            r12.cZm = r0     // Catch:{ SecurityException -> 0x010b }
        L_0x0155:
            r0.put(r5, r7)     // Catch:{ SecurityException -> 0x010b }
            r2.put(r5, r7)     // Catch:{ SecurityException -> 0x010b }
            goto L_0x00f3
        L_0x015c:
            java.util.Map<java.lang.String, a.TR> r0 = r12.f1373Fa     // Catch:{ SecurityException -> 0x010b }
            goto L_0x014a
        L_0x015f:
            r12.f1373Fa = r0     // Catch:{ SecurityException -> 0x010b }
            goto L_0x0155
        L_0x0162:
            boolean r6 = r0 instanceof java.lang.reflect.Field     // Catch:{ SecurityException -> 0x010b }
            if (r6 == 0) goto L_0x017b
            java.lang.reflect.Field r0 = (java.lang.reflect.Field) r0     // Catch:{ SecurityException -> 0x010b }
            java.lang.Class r0 = r0.getDeclaringClass()     // Catch:{ SecurityException -> 0x010b }
            java.lang.Class r6 = r4.getDeclaringClass()     // Catch:{ SecurityException -> 0x010b }
            boolean r0 = r0.isAssignableFrom(r6)     // Catch:{ SecurityException -> 0x010b }
            if (r0 == 0) goto L_0x00f3
            r2.put(r5, r4)     // Catch:{ SecurityException -> 0x010b }
            goto L_0x00f3
        L_0x017b:
            p001a.C1520WN.codeBug()     // Catch:{ SecurityException -> 0x010b }
            goto L_0x00f3
        L_0x0180:
            if (r8 != 0) goto L_0x01af
            r0 = 1
            r7 = r0
        L_0x0184:
            if (r7 == 0) goto L_0x01b2
            java.util.Map<java.lang.String, java.lang.Object> r0 = r12.gcW
            r1 = r0
        L_0x0189:
            java.util.HashMap r9 = new java.util.HashMap
            r9.<init>()
            java.util.Set r0 = r1.keySet()
            java.util.Iterator r10 = r0.iterator()
        L_0x0196:
            boolean r0 = r10.hasNext()
            if (r0 != 0) goto L_0x01b6
            java.util.Set r0 = r9.keySet()
            java.util.Iterator r2 = r0.iterator()
        L_0x01a4:
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x028d
            int r0 = r8 + 1
            r8 = r0
            goto L_0x001d
        L_0x01af:
            r0 = 0
            r7 = r0
            goto L_0x0184
        L_0x01b2:
            java.util.Map<java.lang.String, java.lang.Object> r0 = r12.gcV
            r1 = r0
            goto L_0x0189
        L_0x01b6:
            java.lang.Object r0 = r10.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r2 = "get"
            boolean r2 = r0.startsWith(r2)
            java.lang.String r3 = "set"
            boolean r3 = r0.startsWith(r3)
            java.lang.String r4 = "is"
            boolean r4 = r0.startsWith(r4)
            if (r2 != 0) goto L_0x01d4
            if (r4 != 0) goto L_0x01d4
            if (r3 == 0) goto L_0x0196
        L_0x01d4:
            if (r4 == 0) goto L_0x025b
            r2 = 2
        L_0x01d7:
            java.lang.String r3 = r0.substring(r2)
            int r0 = r3.length()
            if (r0 == 0) goto L_0x0196
            r0 = 0
            char r0 = r3.charAt(r0)
            boolean r2 = java.lang.Character.isUpperCase(r0)
            if (r2 == 0) goto L_0x02b3
            int r2 = r3.length()
            r4 = 1
            if (r2 != r4) goto L_0x025e
            java.lang.String r0 = r3.toLowerCase()
            r2 = r0
        L_0x01f8:
            boolean r0 = r9.containsKey(r2)
            if (r0 != 0) goto L_0x0196
            java.lang.Object r0 = r1.get(r2)
            if (r0 == 0) goto L_0x0214
            boolean r4 = r12.gcY
            if (r4 == 0) goto L_0x0196
            java.lang.reflect.Member r0 = (java.lang.reflect.Member) r0
            int r0 = r0.getModifiers()
            boolean r0 = java.lang.reflect.Modifier.isPrivate(r0)
            if (r0 == 0) goto L_0x0196
        L_0x0214:
            java.lang.String r0 = "get"
            a.amt r0 = r12.m8432a((boolean) r7, (java.util.Map<java.lang.String, java.lang.Object>) r1, (java.lang.String) r0, (java.lang.String) r3)
            if (r0 != 0) goto L_0x02b0
            java.lang.String r0 = "is"
            a.amt r0 = r12.m8432a((boolean) r7, (java.util.Map<java.lang.String, java.lang.Object>) r1, (java.lang.String) r0, (java.lang.String) r3)
            r4 = r0
        L_0x0223:
            r6 = 0
            r5 = 0
            java.lang.String r0 = "set"
            java.lang.String r0 = r0.concat(r3)
            boolean r3 = r1.containsKey(r0)
            if (r3 == 0) goto L_0x02ad
            java.lang.Object r0 = r1.get(r0)
            boolean r3 = r0 instanceof p001a.C6186aiC
            if (r3 == 0) goto L_0x02ad
            a.aiC r0 = (p001a.C6186aiC) r0
            if (r4 == 0) goto L_0x0286
            java.lang.reflect.Method r3 = r4.cjw()
            java.lang.Class r3 = r3.getReturnType()
            a.amt[] r6 = r0.fOb
            a.amt r3 = m8431a((java.lang.Class<?>) r3, (p001a.C6437amt[]) r6, (boolean) r7)
        L_0x024b:
            a.amt[] r6 = r0.fOb
            int r6 = r6.length
            r11 = 1
            if (r6 <= r11) goto L_0x02ab
        L_0x0251:
            a.aAa r5 = new a.aAa
            r5.<init>(r4, r3, r0)
            r9.put(r2, r5)
            goto L_0x0196
        L_0x025b:
            r2 = 3
            goto L_0x01d7
        L_0x025e:
            r2 = 1
            char r2 = r3.charAt(r2)
            boolean r2 = java.lang.Character.isUpperCase(r2)
            if (r2 != 0) goto L_0x02b3
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            char r0 = java.lang.Character.toLowerCase(r0)
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r2.<init>(r0)
            r0 = 1
            java.lang.String r0 = r3.substring(r0)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r2 = r0
            goto L_0x01f8
        L_0x0286:
            a.amt[] r3 = r0.fOb
            a.amt r3 = m8437b((p001a.C6437amt[]) r3, (boolean) r7)
            goto L_0x024b
        L_0x028d:
            java.lang.Object r0 = r2.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r3 = r9.get(r0)
            r1.put(r0, r3)
            goto L_0x01a4
        L_0x029c:
            a.amt[] r2 = r12.gcX
            a.amt r3 = new a.amt
            r4 = r1[r0]
            r3.<init>((java.lang.reflect.Constructor<?>) r4)
            r2[r0] = r3
            int r0 = r0 + 1
            goto L_0x002a
        L_0x02ab:
            r0 = r5
            goto L_0x0251
        L_0x02ad:
            r0 = r5
            r3 = r6
            goto L_0x0251
        L_0x02b0:
            r4 = r0
            goto L_0x0223
        L_0x02b3:
            r2 = r3
            goto L_0x01f8
        */
        throw new UnsupportedOperationException("Method not decompiled: JavaMembers.m8440b(a.aVF, boolean):void");
    }

    private Constructor<?>[] ckL() {
        if (this.gcY && this.gcU != ScriptRuntime.ClassClass) {
            try {
                Constructor<?>[] declaredConstructors = this.gcU.getDeclaredConstructors();
                Constructor.setAccessible(declaredConstructors, true);
                return declaredConstructors;
            } catch (SecurityException e) {
                org.mozilla1.javascript.Context.reportWarning("Could not access constructor  of class " + this.gcU.getName() + " due to lack of privileges.");
            }
        }
        return this.gcU.getConstructors();
    }

    private Field[] ckM() {
        if (this.gcY) {
            try {
                ArrayList arrayList = new ArrayList();
                for (Class<? super Object> cls = (Class<? super Object>) this.gcU; cls != null; cls = cls.getSuperclass()) {
                    Field[] declaredFields = cls.getDeclaredFields();
                    for (int i = 0; i < declaredFields.length; i++) {
                        declaredFields[i].setAccessible(true);
                        arrayList.add(declaredFields[i]);
                    }
                }
                return (Field[]) arrayList.toArray(new Field[arrayList.size()]);
            } catch (SecurityException e) {
            }
        }
        return this.gcU.getFields();
    }

    /* renamed from: a */
    private org.mozilla1.javascript.MemberBox m8432a(boolean z, Map<String, Object> map, String str, String str2) {
        String concat = str.concat(str2);
        if (map.containsKey(concat)) {
            Object obj = map.get(concat);
            if (obj instanceof org.mozilla1.javascript.NativeJavaMethod) {
                return m8433a(((NativeJavaMethod) obj).fOb, z);
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Map<String, org.mozilla1.javascript.FieldAndMethods> mo4679a(Scriptable avf, Object obj, boolean z) {
        Map<String, org.mozilla1.javascript.FieldAndMethods> map = z ? this.cZm : this.f1373Fa;
        if (map == null) {
            return null;
        }
        HashMap hashMap = new HashMap(map.size());
        for (org.mozilla1.javascript.FieldAndMethods next : map.values()) {
            org.mozilla1.javascript.FieldAndMethods tr = new org.mozilla1.javascript.FieldAndMethods(avf, next.fOb, next.field);
            tr.javaObject = obj;
            hashMap.put(next.field.getName(), tr);
        }
        return hashMap;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: jy */
    public RuntimeException mo4682jy(String str) {
        return Context.m35224a("msg.java.member.not.found", (Object) this.gcU.getName(), (Object) str);
    }

    /* renamed from: a.PT$a */
    private static final class C1057a {
        private final Class<?>[] dSx;
        private final String name;

        private C1057a(String str, Class<?>[] clsArr) {
            this.name = str;
            this.dSx = clsArr;
        }

        C1057a(Method method) {
            this(method.getName(), method.getParameterTypes());
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C1057a)) {
                return false;
            }
            C1057a aVar = (C1057a) obj;
            if (!aVar.name.equals(this.name) || !Arrays.equals(this.dSx, aVar.dSx)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return this.name.hashCode() ^ this.dSx.length;
        }
    }
}
