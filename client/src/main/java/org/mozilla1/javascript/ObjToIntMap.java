package org.mozilla1.javascript;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/* renamed from: a.uS */
/* compiled from: a */
public class ObjToIntMap implements Serializable {
    /* access modifiers changed from: private */
    public static final Object fjK = new Integer(27);
    static final long serialVersionUID = -1542220580748809402L;
    private static final boolean check = false;
    private static final int fjJ = -1640531527;
    private transient Object[] bxL;
    private int fjL;
    private int fjM;
    private transient int fjN;
    private transient int[] values;

    public ObjToIntMap() {
        this(4);
    }

    public ObjToIntMap(int i) {
        if (i < 0) {
            Kit.codeBug();
        }
        int i2 = 2;
        while ((1 << i2) < (i * 4) / 3) {
            i2++;
        }
        this.fjL = i2;
    }

    /* renamed from: q */
    private static int m40032q(int i, int i2, int i3) {
        int i4 = 32 - (i3 * 2);
        if (i4 >= 0) {
            return ((i >>> i4) & i2) | 1;
        }
        return ((i2 >>> (-i4)) & i) | 1;
    }

    public boolean isEmpty() {
        return this.fjM == 0;
    }

    public int size() {
        return this.fjM;
    }

    public boolean has(Object obj) {
        if (obj == null) {
            obj = UniqueTag.f5855nc;
        }
        return m40029al(obj) >= 0;
    }

    public int get(Object obj, int i) {
        if (obj == null) {
            obj = UniqueTag.f5855nc;
        }
        int al = m40029al(obj);
        if (al >= 0) {
            return this.values[al];
        }
        return i;
    }

    public int getExisting(Object obj) {
        if (obj == null) {
            obj = UniqueTag.f5855nc;
        }
        int al = m40029al(obj);
        if (al >= 0) {
            return this.values[al];
        }
        Kit.codeBug();
        return 0;
    }

    public void put(Object obj, int i) {
        if (obj == null) {
            obj = UniqueTag.f5855nc;
        }
        this.values[m40030am(obj)] = i;
    }

    public Object intern(Object obj) {
        boolean z;
        if (obj == null) {
            z = true;
            obj = UniqueTag.f5855nc;
        } else {
            z = false;
        }
        int am = m40030am(obj);
        this.values[am] = 0;
        if (z) {
            return null;
        }
        return this.bxL[am];
    }

    public void remove(Object obj) {
        if (obj == null) {
            obj = UniqueTag.f5855nc;
        }
        int al = m40029al(obj);
        if (al >= 0) {
            this.bxL[al] = fjK;
            this.fjM--;
        }
    }

    public void clear() {
        int length = this.bxL.length;
        while (length != 0) {
            length--;
            this.bxL[length] = null;
        }
        this.fjM = 0;
        this.fjN = 0;
    }

    public Iterator newIterator() {
        return new Iterator(this);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo22385a(Iterator aVar) {
        aVar.init(this.bxL, this.values, this.fjM);
    }

    public Object[] getKeys() {
        Object[] objArr = new Object[this.fjM];
        getKeys(objArr, 0);
        return objArr;
    }

    public void getKeys(Object[] objArr, int i) {
        int i2;
        int i3 = this.fjM;
        int i4 = 0;
        int i5 = i;
        while (i3 != 0) {
            Object obj = this.bxL[i4];
            if (obj == null || obj == fjK) {
                i2 = i3;
            } else {
                if (obj == UniqueTag.f5855nc) {
                    obj = null;
                }
                objArr[i5] = obj;
                i5++;
                i2 = i3 - 1;
            }
            i4++;
            i3 = i2;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r1 = r9.hashCode();
        r2 = r1 * fjJ;
     */
    /* renamed from: al */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int m40029al(java.lang.Object r9) {
        /*
            r8 = this;
            java.lang.Object[] r0 = r8.bxL
            if (r0 == 0) goto L_0x003e
            int r1 = r9.hashCode()
            r0 = -1640531527(0xffffffff9e3779b9, float:-9.713111E-21)
            int r2 = r1 * r0
            int r0 = r8.fjL
            int r0 = 32 - r0
            int r0 = r2 >>> r0
            java.lang.Object[] r3 = r8.bxL
            r3 = r3[r0]
            if (r3 == 0) goto L_0x003e
            r4 = 1
            int r5 = r8.fjL
            int r4 = r4 << r5
            if (r3 == r9) goto L_0x002d
            int[] r5 = r8.values
            int r6 = r4 + r0
            r5 = r5[r6]
            if (r5 != r1) goto L_0x002e
            boolean r3 = r3.equals(r9)
            if (r3 == 0) goto L_0x002e
        L_0x002d:
            return r0
        L_0x002e:
            int r3 = r4 + -1
            int r5 = r8.fjL
            int r2 = m40032q(r2, r3, r5)
        L_0x0036:
            int r0 = r0 + r2
            r0 = r0 & r3
            java.lang.Object[] r5 = r8.bxL
            r5 = r5[r0]
            if (r5 != 0) goto L_0x0040
        L_0x003e:
            r0 = -1
            goto L_0x002d
        L_0x0040:
            if (r5 == r9) goto L_0x002d
            int[] r6 = r8.values
            int r7 = r4 + r0
            r6 = r6[r7]
            if (r6 != r1) goto L_0x0036
            boolean r5 = r5.equals(r9)
            if (r5 == 0) goto L_0x0036
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C3744uS.m40029al(java.lang.Object):int");
    }

    /* renamed from: b */
    private int m40031b(Object obj, int i) {
        int i2 = i * fjJ;
        int i3 = i2 >>> (32 - this.fjL);
        int i4 = 1 << this.fjL;
        if (this.bxL[i3] != null) {
            int i5 = i4 - 1;
            int q = m40032q(i2, i5, this.fjL);
            do {
                i3 = (i3 + q) & i5;
            } while (this.bxL[i3] != null);
        }
        this.bxL[i3] = obj;
        this.values[i4 + i3] = i;
        this.fjN++;
        this.fjM++;
        return i3;
    }

    private void bRh() {
        int i = 0;
        if (this.bxL == null) {
            int i2 = 1 << this.fjL;
            this.bxL = new Object[i2];
            this.values = new int[(i2 * 2)];
            return;
        }
        if (this.fjM * 2 >= this.fjN) {
            this.fjL++;
        }
        int i3 = 1 << this.fjL;
        Object[] objArr = this.bxL;
        int[] iArr = this.values;
        int length = objArr.length;
        this.bxL = new Object[i3];
        this.values = new int[(i3 * 2)];
        int i4 = this.fjM;
        this.fjM = 0;
        this.fjN = 0;
        while (i4 != 0) {
            Object obj = objArr[i];
            if (!(obj == null || obj == fjK)) {
                this.values[m40031b(obj, iArr[length + i])] = iArr[i];
                i4--;
            }
            i++;
        }
    }

    /* renamed from: am */
    private int m40030am(Object obj) {
        int i;
        int i2;
        int i3;
        int i4 = -1;
        int hashCode = obj.hashCode();
        if (this.bxL != null) {
            int i5 = hashCode * fjJ;
            i2 = i5 >>> (32 - this.fjL);
            Object obj2 = this.bxL[i2];
            if (obj2 != null) {
                int i6 = 1 << this.fjL;
                if (obj2 == obj) {
                    return i2;
                }
                if (this.values[i6 + i2] == hashCode && obj2.equals(obj)) {
                    return i2;
                }
                if (obj2 == fjK) {
                    i4 = i2;
                }
                int i7 = i6 - 1;
                int q = m40032q(i5, i7, this.fjL);
                while (true) {
                    i2 = (i2 + q) & i7;
                    Object obj3 = this.bxL[i2];
                    if (obj3 == null) {
                        i = i4;
                        break;
                    } else if (obj3 == obj) {
                        return i2;
                    } else {
                        if (this.values[i6 + i2] == hashCode && obj3.equals(obj)) {
                            return i2;
                        }
                        if (obj3 == fjK && i4 < 0) {
                            i4 = i2;
                        }
                    }
                }
            } else {
                i = -1;
            }
        } else {
            i = -1;
            i2 = -1;
        }
        if (i >= 0) {
            i3 = i;
        } else if (this.bxL == null || this.fjN * 4 >= (1 << this.fjL) * 3) {
            bRh();
            return m40031b(obj, hashCode);
        } else {
            this.fjN++;
            i3 = i2;
        }
        this.bxL[i3] = obj;
        this.values[(1 << this.fjL) + i3] = hashCode;
        this.fjM++;
        return i3;
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        int i = this.fjM;
        int i2 = 0;
        while (i != 0) {
            Object obj = this.bxL[i2];
            if (!(obj == null || obj == fjK)) {
                i--;
                objectOutputStream.writeObject(obj);
                objectOutputStream.writeInt(this.values[i2]);
            }
            i2++;
        }
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        int i = this.fjM;
        if (i != 0) {
            this.fjM = 0;
            int i2 = 1 << this.fjL;
            this.bxL = new Object[i2];
            this.values = new int[(i2 * 2)];
            for (int i3 = 0; i3 != i; i3++) {
                Object readObject = objectInputStream.readObject();
                this.values[m40031b(readObject, readObject.hashCode())] = objectInputStream.readInt();
            }
        }
    }

    /* renamed from: a.uS$a */
    public static class Iterator {
        ObjToIntMap bxJ;
        private int bxK;
        private Object[] bxL;
        private int cursor;
        private int[] values;

        Iterator(ObjToIntMap uSVar) {
            this.bxJ = uSVar;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public final void init(Object[] objArr, int[] iArr, int i) {
            this.bxL = objArr;
            this.values = iArr;
            this.cursor = -1;
            this.bxK = i;
        }

        public void start() {
            this.bxJ.mo22385a(this);
            next();
        }

        public boolean done() {
            return this.bxK < 0;
        }

        public void next() {
            if (this.bxK == -1) {
                Kit.codeBug();
            }
            if (this.bxK == 0) {
                this.bxK = -1;
                this.cursor = -1;
                return;
            }
            this.cursor++;
            while (true) {
                Object obj = this.bxL[this.cursor];
                if (obj == null || obj == ObjToIntMap.fjK) {
                    this.cursor++;
                } else {
                    this.bxK--;
                    return;
                }
            }
        }

        public Object getKey() {
            Object obj = this.bxL[this.cursor];
            if (obj == UniqueTag.f5855nc) {
                return null;
            }
            return obj;
        }

        public int getValue() {
            return this.values[this.cursor];
        }

        public void setValue(int i) {
            this.values[this.cursor] = i;
        }
    }
}
