package org.mozilla1.javascript;

import java.util.ArrayList;
import java.util.HashMap;

/* renamed from: a.YQ */
/* compiled from: a */
public class FunctionNode extends ScriptOrFnNode {
    public static final int FUNCTION_EXPRESSION = 2;
    public static final int FUNCTION_EXPRESSION_STATEMENT = 3;
    public static final int FUNCTION_STATEMENT = 1;
    int eMN;
    boolean eMO;
    boolean eMP;
    boolean eMQ;
    ArrayList<org.mozilla1.javascript.Node> eMR;
    HashMap<org.mozilla1.javascript.Node, int[]> eMS;
    String eMs;

    public FunctionNode(String str) {
        super(108);
        this.eMs = str;
    }

    public String getFunctionName() {
        return this.eMs;
    }

    public boolean requiresActivation() {
        return this.eMO;
    }

    public boolean getIgnoreDynamicScope() {
        return this.eMP;
    }

    public boolean bHQ() {
        return this.eMQ;
    }

    /* renamed from: h */
    public void mo7187h(org.mozilla1.javascript.Node qlVar) {
        if (this.eMR == null) {
            this.eMR = new ArrayList<>();
        }
        this.eMR.add(qlVar);
    }

    public ArrayList<org.mozilla1.javascript.Node> bHR() {
        return this.eMR;
    }

    public HashMap<org.mozilla1.javascript.Node, int[]> bHS() {
        return this.eMS;
    }

    /* renamed from: a */
    public void mo7180a(Node qlVar, int[] iArr) {
        if (this.eMS == null) {
            this.eMS = new HashMap<>();
        }
        this.eMS.put(qlVar, iArr);
    }

    public int getFunctionType() {
        return this.eMN;
    }
}
