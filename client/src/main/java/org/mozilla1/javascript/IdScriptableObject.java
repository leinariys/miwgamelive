package org.mozilla1.javascript;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/* renamed from: a.arA  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class IdScriptableObject extends org.mozilla1.javascript.ScriptableObject implements IdFunctionCall {
    private volatile transient PrototypeValues ims;

    public IdScriptableObject() {
    }

    public IdScriptableObject(org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2) {
        super(avf, avf2);
    }

    public static int instanceIdInfo(int i, int i2) {
        return (i << 16) | i2;
    }

    /* renamed from: f */
    public static EcmaError m25341f(org.mozilla1.javascript.IdFunctionObject yy) {
        throw ScriptRuntime.m7536aZ("msg.incompat.call", yy.getFunctionName());
    }

    /* access modifiers changed from: protected */
    public final Object defaultGet(String str) {
        return super.get(str, (org.mozilla1.javascript.Scriptable) this);
    }

    /* access modifiers changed from: protected */
    public final void defaultPut(String str, Object obj) {
        super.put(str, (org.mozilla1.javascript.Scriptable) this, obj);
    }

    public boolean has(String str, org.mozilla1.javascript.Scriptable avf) {
        int jJ;
        int findInstanceIdInfo = findInstanceIdInfo(str);
        if (findInstanceIdInfo != 0) {
            if (((findInstanceIdInfo >>> 16) & 4) == 0 && NOT_FOUND == getInstanceIdValue(findInstanceIdInfo & 65535)) {
                return false;
            }
            return true;
        } else if (this.ims == null || (jJ = this.ims.mo15713jJ(str)) == 0) {
            return super.has(str, avf);
        } else {
            return this.ims.has(jJ);
        }
    }

    public Object get(String str, org.mozilla1.javascript.Scriptable avf) {
        int jJ;
        int findInstanceIdInfo = findInstanceIdInfo(str);
        if (findInstanceIdInfo != 0) {
            return getInstanceIdValue(findInstanceIdInfo & 65535);
        }
        if (this.ims == null || (jJ = this.ims.mo15713jJ(str)) == 0) {
            return super.get(str, avf);
        }
        return this.ims.get(jJ);
    }

    public void put(String str, org.mozilla1.javascript.Scriptable avf, Object obj) {
        int jJ;
        int findInstanceIdInfo = findInstanceIdInfo(str);
        if (findInstanceIdInfo != 0) {
            if (avf == this && isSealed()) {
                throw org.mozilla1.javascript.Context.m35246q("msg.modify.sealed", str);
            } else if (((findInstanceIdInfo >>> 16) & 1) != 0) {
            } else {
                if (avf == this) {
                    setInstanceIdValue(findInstanceIdInfo & 65535, obj);
                } else {
                    avf.put(str, avf, obj);
                }
            }
        } else if (this.ims == null || (jJ = this.ims.mo15713jJ(str)) == 0) {
            super.put(str, avf, obj);
        } else if (avf != this || !isSealed()) {
            this.ims.mo15704a(jJ, avf, obj);
        } else {
            throw org.mozilla1.javascript.Context.m35246q("msg.modify.sealed", str);
        }
    }

    public void delete(String str) {
        int jJ;
        int findInstanceIdInfo = findInstanceIdInfo(str);
        if (findInstanceIdInfo == 0 || isSealed()) {
            if (this.ims == null || (jJ = this.ims.mo15713jJ(str)) == 0) {
                super.delete(str);
            } else if (!isSealed()) {
                this.ims.delete(jJ);
            }
        } else if (((findInstanceIdInfo >>> 16) & 4) == 0) {
            setInstanceIdValue(findInstanceIdInfo & 65535, NOT_FOUND);
        }
    }

    public int getAttributes(String str) {
        int jJ;
        int findInstanceIdInfo = findInstanceIdInfo(str);
        if (findInstanceIdInfo != 0) {
            return findInstanceIdInfo >>> 16;
        }
        if (this.ims == null || (jJ = this.ims.mo15713jJ(str)) == 0) {
            return super.getAttributes(str);
        }
        return this.ims.getAttributes(jJ);
    }

    public void setAttributes(String str, int i) {
        int jJ;
        org.mozilla1.javascript.ScriptableObject.m23559AT(i);
        int findInstanceIdInfo = findInstanceIdInfo(str);
        if (findInstanceIdInfo != 0) {
            if (i != (findInstanceIdInfo >>> 16)) {
                throw new RuntimeException("Change of attributes for this id is not supported");
            }
        } else if (this.ims == null || (jJ = this.ims.mo15713jJ(str)) == 0) {
            super.setAttributes(str, i);
        } else {
            this.ims.setAttributes(jJ, i);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: dP */
    public Object[] mo12353dP(boolean z) {
        Object[] objArr;
        Object[] dP = super.mo12353dP(z);
        if (this.ims != null) {
            objArr = this.ims.mo15706a(z, dP);
        } else {
            objArr = dP;
        }
        int maxInstanceId = getMaxInstanceId();
        if (maxInstanceId == 0) {
            return objArr;
        }
        Object[] objArr2 = null;
        int i = 0;
        for (int i2 = maxInstanceId; i2 != 0; i2--) {
            String instanceIdName = getInstanceIdName(i2);
            int findInstanceIdInfo = findInstanceIdInfo(instanceIdName);
            if (findInstanceIdInfo != 0) {
                int i3 = findInstanceIdInfo >>> 16;
                if (!((i3 & 4) == 0 && NOT_FOUND == getInstanceIdValue(i2)) && (z || (i3 & 2) == 0)) {
                    if (i == 0) {
                        objArr2 = new Object[i2];
                    }
                    objArr2[i] = instanceIdName;
                    i++;
                }
            }
        }
        if (i == 0) {
            return objArr;
        }
        if (objArr.length == 0 && objArr2.length == i) {
            return objArr2;
        }
        Object[] objArr3 = new Object[(objArr.length + i)];
        System.arraycopy(objArr, 0, objArr3, 0, objArr.length);
        System.arraycopy(objArr2, 0, objArr3, objArr.length, i);
        return objArr3;
    }

    /* access modifiers changed from: protected */
    public int getMaxInstanceId() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public int findInstanceIdInfo(String str) {
        return 0;
    }

    /* access modifiers changed from: protected */
    public String getInstanceIdName(int i) {
        throw new IllegalArgumentException(String.valueOf(i));
    }

    /* access modifiers changed from: protected */
    public Object getInstanceIdValue(int i) {
        throw new IllegalStateException(String.valueOf(i));
    }

    /* access modifiers changed from: protected */
    public void setInstanceIdValue(int i, Object obj) {
        throw new IllegalStateException(String.valueOf(i));
    }

    public Object execIdCall(org.mozilla1.javascript.IdFunctionObject yy, Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        throw yy.unknown();
    }

    /* renamed from: a */
    public final org.mozilla1.javascript.IdFunctionObject mo15695a(int i, org.mozilla1.javascript.Scriptable avf, boolean z) {
        if (!(avf == this || avf == null)) {
            setParentScope(avf);
            setPrototype(m23593v(avf));
        }
        activatePrototypeMap(i);
        org.mozilla1.javascript.IdFunctionObject csk = this.ims.csk();
        if (z) {
            sealObject();
        }
        mo1118a(csk);
        if (z) {
            csk.sealObject();
        }
        csk.exportAsScopeProperty();
        return csk;
    }

    public final boolean hasPrototypeMap() {
        return this.ims != null;
    }

    public final void activatePrototypeMap(int i) {
        PrototypeValues aVar = new PrototypeValues(this, i);
        synchronized (this) {
            if (this.ims != null) {
                throw new IllegalStateException();
            }
            this.ims = aVar;
        }
    }

    public final void initPrototypeMethod(Object obj, int i, String str, int i2) {
        this.ims.mo15705a(i, str, m25340a(obj, i, str, i2, org.mozilla1.javascript.ScriptableObject.m23595x(this)), 2);
    }

    /* renamed from: e */
    public final void mo15700e(org.mozilla1.javascript.IdFunctionObject yy) {
        int i = this.ims.gsu;
        if (i == 0) {
            throw new IllegalStateException();
        } else if (yy.methodId() != i) {
            throw new IllegalArgumentException();
        } else {
            if (isSealed()) {
                yy.sealObject();
            }
            this.ims.mo15705a(i, "constructor", yy, 2);
        }
    }

    public final void initPrototypeValue(int i, String str, Object obj, int i2) {
        this.ims.mo15705a(i, str, obj, i2);
    }

    /* access modifiers changed from: protected */
    public void initPrototypeId(int i) {
        throw new IllegalStateException(String.valueOf(i));
    }

    /* access modifiers changed from: protected */
    public int findPrototypeId(String str) {
        throw new IllegalStateException(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo1118a(org.mozilla1.javascript.IdFunctionObject yy) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo15696a(org.mozilla1.javascript.Scriptable avf, Object obj, int i, String str, int i2) {
        m25340a(obj, i, str, i2, org.mozilla1.javascript.ScriptableObject.m23595x(avf)).mo7320i(avf);
    }

    /* renamed from: a */
    private org.mozilla1.javascript.IdFunctionObject m25340a(Object obj, int i, String str, int i2, org.mozilla1.javascript.Scriptable avf) {
        org.mozilla1.javascript.IdFunctionObject yy = new org.mozilla1.javascript.IdFunctionObject(this, obj, i, str, i2, avf);
        if (isSealed()) {
            yy.sealObject();
        }
        return yy;
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        int readInt = objectInputStream.readInt();
        if (readInt != 0) {
            activatePrototypeMap(readInt);
        }
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        int i = 0;
        if (this.ims != null) {
            i = this.ims.csj();
        }
        objectOutputStream.writeInt(i);
    }

    /* renamed from: a.arA$a */
    private static final class PrototypeValues implements Serializable {
        static final long serialVersionUID = 3038645279153854371L;
        private static final int gsm = 0;
        private static final int gsn = 1;
        private static final int gso = 2;
        int gsu;
        private IdScriptableObject gsp;
        private int gsq;
        private volatile Object[] gsr;
        private volatile short[] gss;
        private volatile int gst = 1;
        private org.mozilla1.javascript.IdFunctionObject gsv;
        private short gsw;

        PrototypeValues(IdScriptableObject ara, int i) {
            if (ara == null) {
                throw new IllegalArgumentException();
            } else if (i < 1) {
                throw new IllegalArgumentException();
            } else {
                this.gsp = ara;
                this.gsq = i;
            }
        }

        /* access modifiers changed from: package-private */
        public final int csj() {
            return this.gsq;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public final void mo15705a(int i, String str, Object obj, int i2) {
            if (1 > i || i > this.gsq) {
                throw new IllegalArgumentException();
            } else if (str == null) {
                throw new IllegalArgumentException();
            } else if (obj == IdScriptableObject.NOT_FOUND) {
                throw new IllegalArgumentException();
            } else {
                org.mozilla1.javascript.ScriptableObject.m23559AT(i2);
                if (this.gsp.findPrototypeId(str) != i) {
                    throw new IllegalArgumentException(str);
                } else if (i != this.gsu) {
                    m25347b(i, str, obj, i2);
                } else if (!(obj instanceof org.mozilla1.javascript.IdFunctionObject)) {
                    throw new IllegalArgumentException("consructor should be initialized with IdFunctionObject");
                } else {
                    this.gsv = (org.mozilla1.javascript.IdFunctionObject) obj;
                    this.gsw = (short) i2;
                }
            }
        }

        /* renamed from: b */
        private void m25347b(int i, String str, Object obj, int i2) {
            Object[] objArr = this.gsr;
            if (objArr == null) {
                throw new IllegalStateException();
            }
            if (obj == null) {
                obj = org.mozilla1.javascript.UniqueTag.f5855nc;
            }
            int i3 = (i - 1) * 2;
            synchronized (this) {
                if (objArr[i3 + 0] == null) {
                    objArr[i3 + 0] = obj;
                    objArr[i3 + 1] = str;
                    this.gss[i - 1] = (short) i2;
                } else if (!str.equals(objArr[i3 + 1])) {
                    throw new IllegalStateException();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public final IdFunctionObject csk() {
            if (this.gsu != 0) {
                throw new IllegalStateException();
            }
            this.gsu = this.gsp.findPrototypeId("constructor");
            if (this.gsu == 0) {
                throw new IllegalStateException("No id for constructor property");
            }
            this.gsp.initPrototypeId(this.gsu);
            if (this.gsv == null) {
                throw new IllegalStateException(String.valueOf(this.gsp.getClass().getName()) + ".initPrototypeId() did not " + "initialize id=" + this.gsu);
            }
            this.gsv.mo7314d(this.gsp.getClassName(), org.mozilla1.javascript.ScriptableObject.m23595x(this.gsp));
            this.gsv.mo7318h(this.gsp);
            return this.gsv;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: jJ */
        public final int mo15713jJ(String str) {
            Object[] objArr = this.gsr;
            if (objArr == null) {
                return this.gsp.findPrototypeId(str);
            }
            int i = this.gst;
            if (str == objArr[((i - 1) * 2) + 1]) {
                return i;
            }
            int findPrototypeId = this.gsp.findPrototypeId(str);
            if (findPrototypeId == 0) {
                return findPrototypeId;
            }
            objArr[((findPrototypeId - 1) * 2) + 1] = str;
            this.gst = findPrototypeId;
            return findPrototypeId;
        }

        /* access modifiers changed from: package-private */
        public final boolean has(int i) {
            Object obj;
            Object[] objArr = this.gsr;
            if (objArr == null || (obj = objArr[((i - 1) * 2) + 0]) == null || obj != IdScriptableObject.NOT_FOUND) {
                return true;
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public final Object get(int i) {
            Object tQ = m25348tQ(i);
            if (tQ == org.mozilla1.javascript.UniqueTag.f5855nc) {
                return null;
            }
            return tQ;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public final void mo15704a(int i, Scriptable avf, Object obj) {
            if (obj == IdScriptableObject.NOT_FOUND) {
                throw new IllegalArgumentException();
            }
            m25348tQ(i);
            if ((this.gss[i - 1] & 1) != 0) {
                return;
            }
            if (avf == this.gsp) {
                if (obj == null) {
                    obj = UniqueTag.f5855nc;
                }
                int i2 = ((i - 1) * 2) + 0;
                synchronized (this) {
                    this.gsr[i2] = obj;
                }
                return;
            }
            avf.put((String) this.gsr[((i - 1) * 2) + 1], avf, obj);
        }

        /* access modifiers changed from: package-private */
        public final void delete(int i) {
            m25348tQ(i);
            if ((this.gss[i - 1] & 4) == 0) {
                int i2 = ((i - 1) * 2) + 0;
                synchronized (this) {
                    this.gsr[i2] = IdScriptableObject.NOT_FOUND;
                    this.gss[i - 1] = 0;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public final int getAttributes(int i) {
            m25348tQ(i);
            return this.gss[i - 1];
        }

        /* access modifiers changed from: package-private */
        public final void setAttributes(int i, int i2) {
            ScriptableObject.m23559AT(i2);
            m25348tQ(i);
            synchronized (this) {
                this.gss[i - 1] = (short) i2;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public final Object[] mo15706a(boolean z, Object[] objArr) {
            int i = 0;
            Object[] objArr2 = null;
            for (int i2 = 1; i2 <= this.gsq; i2++) {
                Object tQ = m25348tQ(i2);
                if ((z || (this.gss[i2 - 1] & 2) == 0) && tQ != IdScriptableObject.NOT_FOUND) {
                    String str = (String) this.gsr[((i2 - 1) * 2) + 1];
                    if (objArr2 == null) {
                        objArr2 = new Object[this.gsq];
                    }
                    objArr2[i] = str;
                    i++;
                }
            }
            if (i == 0) {
                return objArr;
            }
            if (objArr == null || objArr.length == 0) {
                if (i != objArr2.length) {
                    Object[] objArr3 = new Object[i];
                    System.arraycopy(objArr2, 0, objArr3, 0, i);
                    objArr2 = objArr3;
                }
                return objArr2;
            }
            int length = objArr.length;
            Object[] objArr4 = new Object[(length + i)];
            System.arraycopy(objArr, 0, objArr4, 0, length);
            System.arraycopy(objArr2, 0, objArr4, length, i);
            return objArr4;
        }

        /* renamed from: tQ */
        private Object m25348tQ(int i) {
            Object[] objArr = this.gsr;
            if (objArr == null) {
                synchronized (this) {
                    objArr = this.gsr;
                    if (objArr == null) {
                        objArr = new Object[(this.gsq * 2)];
                        this.gsr = objArr;
                        this.gss = new short[this.gsq];
                    }
                }
            }
            int i2 = ((i - 1) * 2) + 0;
            Object obj = objArr[i2];
            if (obj != null) {
                return obj;
            }
            if (i == this.gsu) {
                m25347b(this.gsu, "constructor", this.gsv, this.gsw);
                this.gsv = null;
            } else {
                this.gsp.initPrototypeId(i);
            }
            Object obj2 = objArr[i2];
            if (obj2 != null) {
                return obj2;
            }
            throw new IllegalStateException(String.valueOf(this.gsp.getClass().getName()) + ".initPrototypeId(int id) " + "did not initialize id=" + i);
        }
    }
}
