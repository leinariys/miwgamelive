package org.mozilla1.javascript;

/* renamed from: a.Or */
/* compiled from: a */
public class ImporterTopLevel extends IdScriptableObject {
    static final long serialVersionUID = -9095380847465315412L;
    private static final Object dKG = new Integer(8);
    private static final int dKH = 2;
    private static final int dKI = 3;
    /* renamed from: ns */
    private static final int f1339ns = 1;
    /* renamed from: nt */
    private static final int f1340nt = 3;
    private org.mozilla1.javascript.ObjArray dKJ;
    private boolean dKK;

    public ImporterTopLevel() {
        this.dKJ = new org.mozilla1.javascript.ObjArray();
    }

    public ImporterTopLevel(org.mozilla1.javascript.Context lhVar) {
        this(lhVar, false);
    }

    public ImporterTopLevel(org.mozilla1.javascript.Context lhVar, boolean z) {
        this.dKJ = new ObjArray();
        mo4533a(lhVar, z);
    }

    public static void init(org.mozilla1.javascript.Context lhVar, Scriptable avf, boolean z) {
        new ImporterTopLevel().mo15695a(3, avf, z);
    }

    public String getClassName() {
        return this.dKK ? "global" : "JavaImporter";
    }

    /* renamed from: a */
    public void mo4533a(org.mozilla1.javascript.Context lhVar, boolean z) {
        lhVar.initStandardObjects((ScriptableObject) this, z);
        this.dKK = true;
        IdFunctionObject a = mo15695a(3, this, false);
        if (z) {
            a.sealObject();
        }
        delete("constructor");
    }

    public boolean has(String str, Scriptable avf) {
        return super.has(str, avf) || m8169c(str, avf) != NOT_FOUND;
    }

    public Object get(String str, Scriptable avf) {
        Object obj = super.get(str, avf);
        if (obj != NOT_FOUND) {
            return obj;
        }
        return m8169c(str, avf);
    }

    /* renamed from: c */
    private Object m8169c(String str, Scriptable avf) {
        Object[] array;
        Object obj = NOT_FOUND;
        synchronized (this.dKJ) {
            array = this.dKJ.toArray();
        }
        Object obj2 = obj;
        for (Object obj3 : array) {
            Object a = ((NativeJavaPackage) obj3).mo23161a(str, avf, false);
            if (a != null && !(a instanceof NativeJavaPackage)) {
                if (obj2 == NOT_FOUND) {
                    obj2 = a;
                } else {
                    throw org.mozilla1.javascript.Context.m35224a("msg.ambig.import", (Object) obj2.toString(), (Object) a.toString());
                }
            }
        }
        return obj2;
    }

    /* renamed from: a */
    public void mo4532a(org.mozilla1.javascript.Context lhVar, Scriptable avf, Object[] objArr, Function azg) {
        m8171p(objArr);
    }

    /* renamed from: a */
    private Object m8165a(Scriptable avf, Object[] objArr) {
        ImporterTopLevel or = new ImporterTopLevel();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 == objArr.length) {
                or.setParentScope(avf);
                or.setPrototype(this);
                return or;
            }
            NativeJavaClass gc = objArr[i2];
            if (gc instanceof NativeJavaClass) {
                or.m8166a(gc);
            } else if (gc instanceof NativeJavaPackage) {
                or.m8167a((NativeJavaPackage) gc);
            } else {
                throw org.mozilla1.javascript.Context.m35246q("msg.not.class.not.pkg", org.mozilla1.javascript.Context.toString(gc));
            }
            i = i2 + 1;
        }
    }

    /* renamed from: o */
    private Object m8170o(Object[] objArr) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 == objArr.length) {
                return org.mozilla1.javascript.Undefined.instance;
            }
            NativeJavaClass gc = objArr[i2];
            if (!(gc instanceof NativeJavaClass)) {
                throw org.mozilla1.javascript.Context.m35246q("msg.not.class", org.mozilla1.javascript.Context.toString(gc));
            }
            m8166a(gc);
            i = i2 + 1;
        }
    }

    /* renamed from: p */
    private Object m8171p(Object[] objArr) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 == objArr.length) {
                return Undefined.instance;
            }
            NativeJavaPackage yhVar = objArr[i2];
            if (!(yhVar instanceof NativeJavaPackage)) {
                throw org.mozilla1.javascript.Context.m35246q("msg.not.pkg", org.mozilla1.javascript.Context.toString(yhVar));
            }
            m8167a(yhVar);
            i = i2 + 1;
        }
    }

    /* renamed from: a */
    private void m8167a(NativeJavaPackage yhVar) {
        if (yhVar != null) {
            synchronized (this.dKJ) {
                int i = 0;
                while (i != this.dKJ.size()) {
                    if (!yhVar.equals(this.dKJ.get(i))) {
                        i++;
                    } else {
                        return;
                    }
                }
                this.dKJ.add(yhVar);
            }
        }
    }

    /* renamed from: a */
    private void m8166a(NativeJavaClass gc) {
        String name = gc.getClassObject().getName();
        String substring = name.substring(name.lastIndexOf(46) + 1);
        Object obj = get(substring, this);
        if (obj == NOT_FOUND || obj == gc) {
            put(substring, this, gc);
            return;
        }
        throw org.mozilla1.javascript.Context.m35246q("msg.prop.defined", substring);
    }

    /* access modifiers changed from: protected */
    public void initPrototypeId(int i) {
        String str;
        int i2 = 1;
        switch (i) {
            case 1:
                i2 = 0;
                str = "constructor";
                break;
            case 2:
                str = "importClass";
                break;
            case 3:
                str = "importPackage";
                break;
            default:
                throw new IllegalArgumentException(String.valueOf(i));
        }
        initPrototypeMethod(dKG, i, str, i2);
    }

    public Object execIdCall(IdFunctionObject yy, Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        if (!yy.hasTag(dKG)) {
            return super.execIdCall(yy, lhVar, avf, avf2, objArr);
        }
        int methodId = yy.methodId();
        switch (methodId) {
            case 1:
                return m8165a(avf, objArr);
            case 2:
                return m8168b(avf2, yy).m8170o(objArr);
            case 3:
                return m8168b(avf2, yy).m8171p(objArr);
            default:
                throw new IllegalArgumentException(String.valueOf(methodId));
        }
    }

    /* renamed from: b */
    private ImporterTopLevel m8168b(Scriptable avf, IdFunctionObject yy) {
        if (this.dKK) {
            return this;
        }
        if (avf instanceof ImporterTopLevel) {
            return (ImporterTopLevel) avf;
        }
        throw m25341f(yy);
    }

    /* access modifiers changed from: protected */
    public int findPrototypeId(String str) {
        int i;
        String str2 = null;
        int length = str.length();
        if (length == 11) {
            char charAt = str.charAt(0);
            if (charAt == 'c') {
                str2 = "constructor";
                i = 1;
            } else {
                if (charAt == 'i') {
                    str2 = "importClass";
                    i = 2;
                }
                i = 0;
            }
        } else {
            if (length == 13) {
                str2 = "importPackage";
                i = 3;
            }
            i = 0;
        }
        if (str2 == null || str2 == str || str2.equals(str)) {
            return i;
        }
        return 0;
    }
}
