package org.mozilla1.javascript;


import org.mozilla1.javascript.debug.DebuggableObject;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.akn  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class ScriptableObject implements org.mozilla1.javascript.ConstProperties, org.mozilla1.javascript.Scriptable, DebuggableObject, Serializable {
    public static final int CONST = 13;
    public static final int DONTENUM = 2;
    public static final int EMPTY = 0;
    public static final int PERMANENT = 4;
    public static final int READONLY = 1;
    public static final int iPY = 8;
    private static final C1931a iQb = new C1931a((String) null, 0, 1);
    private static final int iQh = 1;
    private static final int iQi = 2;
    private static final int iQj = 3;
    private static final int iQk = 4;
    private static final int iQl = 5;

    static {
        iQb.fTj = true;
    }

    private int count;
    private org.mozilla1.javascript.Scriptable iPZ;
    private org.mozilla1.javascript.Scriptable iQa;
    private transient C1931a[] iQc;
    private transient C1931a iQd;
    private transient C1931a iQe;
    private transient C1931a iQf = iQb;
    private volatile transient Map<Object, Object> iQg;

    public ScriptableObject() {
    }

    public ScriptableObject(org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2) {
        if (avf == null) {
            throw new IllegalArgumentException();
        }
        this.iQa = avf;
        this.iPZ = avf2;
    }

    /* renamed from: AT */
    static void m23559AT(int i) {
        if ((i & -16) != 0) {
            throw new IllegalArgumentException(String.valueOf(i));
        }
    }

    /* renamed from: a */
    public static Object m23561a(org.mozilla1.javascript.Scriptable avf, Class<?> cls) {
        boolean z;
        String str;
        Object[] objArr;
        String str2;
        boolean z2;
        org.mozilla1.javascript.Context lhVar = null;
        int i = 0;
        while (i < 2) {
            if (cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
                if (i == 0) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                z = z2;
            } else {
                z = i == 1;
            }
            if (z) {
                str = "toString";
                objArr = org.mozilla1.javascript.ScriptRuntime.emptyArgs;
            } else {
                str = "valueOf";
                objArr = new Object[1];
                if (cls == null) {
                    str2 = "undefined";
                } else if (cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
                    str2 = "string";
                } else if (cls == org.mozilla1.javascript.ScriptRuntime.ScriptableClass) {
                    str2 = "object";
                } else if (cls == org.mozilla1.javascript.ScriptRuntime.FunctionClass) {
                    str2 = "function";
                } else if (cls == org.mozilla1.javascript.ScriptRuntime.BooleanClass || cls == Boolean.TYPE) {
                    str2 = "boolean";
                } else if (cls == org.mozilla1.javascript.ScriptRuntime.NumberClass || cls == org.mozilla1.javascript.ScriptRuntime.ByteClass || cls == Byte.TYPE || cls == org.mozilla1.javascript.ScriptRuntime.ShortClass || cls == Short.TYPE || cls == org.mozilla1.javascript.ScriptRuntime.IntegerClass || cls == Integer.TYPE || cls == org.mozilla1.javascript.ScriptRuntime.FloatClass || cls == Float.TYPE || cls == org.mozilla1.javascript.ScriptRuntime.DoubleClass || cls == Double.TYPE) {
                    str2 = "number";
                } else {
                    throw org.mozilla1.javascript.Context.m35246q("msg.invalid.type", cls.toString());
                }
                objArr[0] = str2;
            }
            Object j = m23587j(avf, str);
            if (j instanceof org.mozilla1.javascript.Function) {
                org.mozilla1.javascript.Function azg = (org.mozilla1.javascript.Function) j;
                if (lhVar == null) {
                    lhVar = org.mozilla1.javascript.Context.bwA();
                }
                Object call = azg.call(lhVar, azg.getParentScope(), avf, objArr);
                if (call == null) {
                    continue;
                } else {
                    if (!(!(call instanceof org.mozilla1.javascript.Scriptable) || cls == org.mozilla1.javascript.ScriptRuntime.ScriptableClass || cls == org.mozilla1.javascript.ScriptRuntime.FunctionClass)) {
                        if (z && (call instanceof Wrapper)) {
                            call = ((Wrapper) call).unwrap();
                            if (call instanceof String) {
                            }
                        }
                    }
                    return call;
                }
            }
            i++;
        }
        throw org.mozilla1.javascript.ScriptRuntime.m7536aZ("msg.default.value", cls == null ? "undefined" : cls.getName());
    }

    /* renamed from: b */
    public static <T extends org.mozilla1.javascript.Scriptable> void m23577b(org.mozilla1.javascript.Scriptable avf, Class<T> cls) {
        m23563a(avf, cls, false, false);
    }

    /* renamed from: a */
    public static <T extends org.mozilla1.javascript.Scriptable> void m23565a(org.mozilla1.javascript.Scriptable avf, Class<T> cls, boolean z) {
        m23563a(avf, cls, z, false);
    }

    /* renamed from: a */
    public static <T extends org.mozilla1.javascript.Scriptable> String m23563a(org.mozilla1.javascript.Scriptable avf, Class<T> cls, boolean z, boolean z2) {
        org.mozilla1.javascript.BaseFunction b = m23574b(avf, cls, z, z2);
        if (b == null) {
            return null;
        }
        String className = b.bOH().getClassName();
        m23567a(avf, className, (Object) b, 2);
        return className;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v0, resolved type: java.lang.reflect.Constructor[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v1, resolved type: java.lang.reflect.Constructor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: java.lang.reflect.Constructor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v13, resolved type: java.lang.reflect.Method} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v47, resolved type: a.agz} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v16, resolved type: java.lang.reflect.Constructor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v20, resolved type: java.lang.reflect.Constructor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v78, resolved type: java.lang.reflect.Method} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v82, resolved type: java.lang.reflect.Constructor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v98, resolved type: java.lang.reflect.Method} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v99, resolved type: java.lang.reflect.Method} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v100, resolved type: java.lang.reflect.Method} */
    /* JADX WARNING: type inference failed for: r2v75 */
    /* JADX WARNING: type inference failed for: r2v77 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static <T extends org.mozilla1.javascript.Scriptable> org.mozilla1.javascript.BaseFunction m23574b(org.mozilla1.javascript.Scriptable r15, java.lang.Class<T> r16, boolean r17, boolean r18) {
        /*
            java.lang.reflect.Method[] r13 = p001a.C6131agz.m22162ah(r16)
            r2 = 0
        L_0x0005:
            int r3 = r13.length
            if (r2 < r3) goto L_0x001f
            java.lang.reflect.Constructor[] r4 = r16.getConstructors()
            r3 = 0
            r2 = 0
        L_0x000e:
            int r5 = r4.length
            if (r2 < r5) goto L_0x0095
            r2 = r3
        L_0x0012:
            if (r2 != 0) goto L_0x00a6
            java.lang.String r2 = "msg.zero.arg.ctor"
            java.lang.String r3 = r16.getName()
            a.TJ r2 = p001a.C2909lh.m35246q(r2, r3)
            throw r2
        L_0x001f:
            r3 = r13[r2]
            java.lang.String r4 = r3.getName()
            java.lang.String r5 = "init"
            boolean r4 = r4.equals(r5)
            if (r4 != 0) goto L_0x0030
        L_0x002d:
            int r2 = r2 + 1
            goto L_0x0005
        L_0x0030:
            java.lang.Class[] r4 = r3.getParameterTypes()
            int r5 = r4.length
            r6 = 3
            if (r5 != r6) goto L_0x0074
            r5 = 0
            r5 = r4[r5]
            java.lang.Class<?> r6 = p001a.C0903NH.ContextClass
            if (r5 != r6) goto L_0x0074
            r5 = 1
            r5 = r4[r5]
            java.lang.Class<a.aVF> r6 = p001a.C0903NH.ScriptableClass
            if (r5 != r6) goto L_0x0074
            r5 = 2
            r5 = r4[r5]
            java.lang.Class r6 = java.lang.Boolean.TYPE
            if (r5 != r6) goto L_0x0074
            int r5 = r3.getModifiers()
            boolean r5 = java.lang.reflect.Modifier.isStatic(r5)
            if (r5 == 0) goto L_0x0074
            r2 = 3
            java.lang.Object[] r4 = new java.lang.Object[r2]
            r2 = 0
            a.lh r5 = p001a.C2909lh.bwA()
            r4[r2] = r5
            r2 = 1
            r4[r2] = r15
            r5 = 2
            if (r17 == 0) goto L_0x0071
            java.lang.Boolean r2 = java.lang.Boolean.TRUE
        L_0x0069:
            r4[r5] = r2
            r2 = 0
            r3.invoke(r2, r4)
            r12 = 0
        L_0x0070:
            return r12
        L_0x0071:
            java.lang.Boolean r2 = java.lang.Boolean.FALSE
            goto L_0x0069
        L_0x0074:
            int r5 = r4.length
            r6 = 1
            if (r5 != r6) goto L_0x002d
            r5 = 0
            r4 = r4[r5]
            java.lang.Class<a.aVF> r5 = p001a.C0903NH.ScriptableClass
            if (r4 != r5) goto L_0x002d
            int r4 = r3.getModifiers()
            boolean r4 = java.lang.reflect.Modifier.isStatic(r4)
            if (r4 == 0) goto L_0x002d
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r4 = 0
            r2[r4] = r15
            r4 = 0
            r3.invoke(r4, r2)
            r12 = 0
            goto L_0x0070
        L_0x0095:
            r5 = r4[r2]
            java.lang.Class[] r5 = r5.getParameterTypes()
            int r5 = r5.length
            if (r5 != 0) goto L_0x00a2
            r2 = r4[r2]
            goto L_0x0012
        L_0x00a2:
            int r2 = r2 + 1
            goto L_0x000e
        L_0x00a6:
            java.lang.Object[] r3 = p001a.C0903NH.emptyArgs
            java.lang.Object r2 = r2.newInstance(r3)
            r8 = r2
            a.aVF r8 = (p001a.aVF) r8
            java.lang.String r3 = r8.getClassName()
            r2 = 0
            if (r18 == 0) goto L_0x00de
            java.lang.Class r5 = r16.getSuperclass()
            java.lang.Class<a.aVF> r6 = p001a.C0903NH.ScriptableClass
            boolean r6 = r6.isAssignableFrom(r5)
            if (r6 == 0) goto L_0x00de
            int r6 = r5.getModifiers()
            boolean r6 = java.lang.reflect.Modifier.isAbstract(r6)
            if (r6 != 0) goto L_0x00de
            java.lang.Class r5 = m23573aP(r5)
            r0 = r17
            r1 = r18
            java.lang.String r5 = m23563a((p001a.aVF) r15, r5, (boolean) r0, (boolean) r1)
            if (r5 == 0) goto L_0x00de
            a.aVF r2 = m23585i(r15, r5)
        L_0x00de:
            if (r2 != 0) goto L_0x00e4
            a.aVF r2 = m23593v(r15)
        L_0x00e4:
            r8.setPrototype(r2)
            java.lang.String r2 = "jsConstructor"
            java.lang.reflect.Method r2 = p001a.C6131agz.m22161a((java.lang.reflect.Method[]) r13, (java.lang.String) r2)
            if (r2 != 0) goto L_0x0280
            int r5 = r4.length
            r6 = 1
            if (r5 != r6) goto L_0x0103
            r2 = 0
            r2 = r4[r2]
        L_0x00f6:
            if (r2 != 0) goto L_0x0123
            java.lang.String r2 = "msg.ctor.multiple.parms"
            java.lang.String r3 = r16.getName()
            a.TJ r2 = p001a.C2909lh.m35246q(r2, r3)
            throw r2
        L_0x0103:
            int r5 = r4.length
            r6 = 2
            if (r5 != r6) goto L_0x00f6
            r5 = 0
            r5 = r4[r5]
            java.lang.Class[] r5 = r5.getParameterTypes()
            int r5 = r5.length
            if (r5 != 0) goto L_0x0115
            r2 = 1
            r2 = r4[r2]
            goto L_0x00f6
        L_0x0115:
            r5 = 1
            r5 = r4[r5]
            java.lang.Class[] r5 = r5.getParameterTypes()
            int r5 = r5.length
            if (r5 != 0) goto L_0x00f6
            r2 = 0
            r2 = r4[r2]
            goto L_0x00f6
        L_0x0123:
            r9 = r2
        L_0x0124:
            a.agz r12 = new a.agz
            r12.<init>(r3, r9, r15)
            boolean r2 = r12.bWT()
            if (r2 == 0) goto L_0x013a
            java.lang.String r2 = "msg.varargs.ctor"
            java.lang.String r3 = r9.getName()
            a.TJ r2 = p001a.C2909lh.m35246q(r2, r3)
            throw r2
        L_0x013a:
            r12.mo13479b(r15, r8)
            r10 = 0
            java.util.HashSet r14 = new java.util.HashSet
            int r2 = r13.length
            r14.<init>(r2)
            r2 = 0
            r11 = r2
        L_0x0146:
            int r2 = r13.length
            if (r11 < r2) goto L_0x016b
            if (r10 == 0) goto L_0x015b
            r2 = 3
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            r2[r3] = r15
            r3 = 1
            r2[r3] = r12
            r3 = 2
            r2[r3] = r8
            r3 = 0
            r10.invoke(r3, r2)
        L_0x015b:
            if (r17 == 0) goto L_0x0070
            r12.sealObject()
            boolean r2 = r8 instanceof p001a.C6327akn
            if (r2 == 0) goto L_0x0070
            a.akn r8 = (p001a.C6327akn) r8
            r8.sealObject()
            goto L_0x0070
        L_0x016b:
            r2 = r13[r11]
            if (r2 != r9) goto L_0x0175
            r2 = r10
        L_0x0170:
            int r3 = r11 + 1
            r11 = r3
            r10 = r2
            goto L_0x0146
        L_0x0175:
            r2 = r13[r11]
            java.lang.String r3 = r2.getName()
            java.lang.String r2 = "finishInit"
            boolean r2 = r3.equals(r2)
            if (r2 == 0) goto L_0x01b1
            r2 = r13[r11]
            java.lang.Class[] r2 = r2.getParameterTypes()
            int r4 = r2.length
            r5 = 3
            if (r4 != r5) goto L_0x01b1
            r4 = 0
            r4 = r2[r4]
            java.lang.Class<a.aVF> r5 = p001a.C0903NH.ScriptableClass
            if (r4 != r5) goto L_0x01b1
            r4 = 1
            r4 = r2[r4]
            java.lang.Class<a.agz> r5 = p001a.C6131agz.class
            if (r4 != r5) goto L_0x01b1
            r4 = 2
            r2 = r2[r4]
            java.lang.Class<a.aVF> r4 = p001a.C0903NH.ScriptableClass
            if (r2 != r4) goto L_0x01b1
            r2 = r13[r11]
            int r2 = r2.getModifiers()
            boolean r2 = java.lang.reflect.Modifier.isStatic(r2)
            if (r2 == 0) goto L_0x01b1
            r2 = r13[r11]
            goto L_0x0170
        L_0x01b1:
            r2 = 36
            int r2 = r3.indexOf(r2)
            r4 = -1
            if (r2 == r4) goto L_0x01bc
            r2 = r10
            goto L_0x0170
        L_0x01bc:
            java.lang.String r2 = "jsConstructor"
            boolean r2 = r3.equals(r2)
            if (r2 == 0) goto L_0x01c6
            r2 = r10
            goto L_0x0170
        L_0x01c6:
            java.lang.String r2 = "jsFunction_"
            boolean r2 = r3.startsWith(r2)
            if (r2 == 0) goto L_0x01e5
            java.lang.String r2 = "jsFunction_"
        L_0x01d0:
            int r4 = r2.length()
            java.lang.String r4 = r3.substring(r4)
            boolean r5 = r14.contains(r4)
            if (r5 == 0) goto L_0x020d
            java.lang.String r2 = "duplicate.defineClass.name"
            a.TJ r2 = p001a.C2909lh.m35224a((java.lang.String) r2, (java.lang.Object) r3, (java.lang.Object) r4)
            throw r2
        L_0x01e5:
            java.lang.String r2 = "jsStaticFunction_"
            boolean r2 = r3.startsWith(r2)
            if (r2 == 0) goto L_0x0202
            java.lang.String r2 = "jsStaticFunction_"
            r4 = r13[r11]
            int r4 = r4.getModifiers()
            boolean r4 = java.lang.reflect.Modifier.isStatic(r4)
            if (r4 != 0) goto L_0x01d0
            java.lang.String r2 = "jsStaticFunction must be used with static method."
            a.TJ r2 = p001a.C2909lh.m35245gF(r2)
            throw r2
        L_0x0202:
            java.lang.String r2 = "jsGet_"
            boolean r2 = r3.startsWith(r2)
            if (r2 == 0) goto L_0x027b
            java.lang.String r2 = "jsGet_"
            goto L_0x01d0
        L_0x020d:
            r14.add(r4)
            int r4 = r2.length()
            java.lang.String r3 = r3.substring(r4)
            java.lang.String r4 = "jsGet_"
            if (r2 != r4) goto L_0x0255
            boolean r2 = r8 instanceof p001a.C6327akn
            if (r2 != 0) goto L_0x022f
            java.lang.String r2 = "msg.extend.scriptable"
            java.lang.Class r4 = r8.getClass()
            java.lang.String r4 = r4.toString()
            a.TJ r2 = p001a.C2909lh.m35224a((java.lang.String) r2, (java.lang.Object) r4, (java.lang.Object) r3)
            throw r2
        L_0x022f:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r4 = "jsSet_"
            r2.<init>(r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.reflect.Method r6 = p001a.C6131agz.m22161a((java.lang.reflect.Method[]) r13, (java.lang.String) r2)
            if (r6 == 0) goto L_0x0253
            r2 = 0
        L_0x0245:
            r7 = r2 | 6
            r2 = r8
            a.akn r2 = (p001a.C6327akn) r2
            r4 = 0
            r5 = r13[r11]
            r2.defineProperty(r3, r4, r5, r6, r7)
            r2 = r10
            goto L_0x0170
        L_0x0253:
            r2 = 1
            goto L_0x0245
        L_0x0255:
            a.agz r4 = new a.agz
            r5 = r13[r11]
            r4.<init>(r3, r5, r8)
            boolean r5 = r4.bWU()
            if (r5 == 0) goto L_0x026d
            java.lang.String r2 = "msg.varargs.fun"
            java.lang.String r3 = r9.getName()
            a.TJ r2 = p001a.C2909lh.m35246q(r2, r3)
            throw r2
        L_0x026d:
            java.lang.String r5 = "jsStaticFunction_"
            if (r2 != r5) goto L_0x027e
            r2 = r12
        L_0x0272:
            r5 = 2
            m23567a((p001a.aVF) r2, (java.lang.String) r3, (java.lang.Object) r4, (int) r5)
            if (r17 == 0) goto L_0x027b
            r4.sealObject()
        L_0x027b:
            r2 = r10
            goto L_0x0170
        L_0x027e:
            r2 = r8
            goto L_0x0272
        L_0x0280:
            r9 = r2
            goto L_0x0124
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6327akn.m23574b(a.aVF, java.lang.Class, boolean, boolean):a.acp");
    }

    /* renamed from: aP */
    private static <T extends org.mozilla1.javascript.Scriptable> Class<T> m23573aP(Class<?> cls) {
        if (org.mozilla1.javascript.ScriptRuntime.ScriptableClass.isAssignableFrom(cls)) {
            return cls;
        }
        return null;
    }

    /* renamed from: a */
    public static void m23567a(org.mozilla1.javascript.Scriptable avf, String str, Object obj, int i) {
        if (!(avf instanceof ScriptableObject)) {
            avf.put(str, avf, obj);
        } else {
            ((ScriptableObject) avf).defineProperty(str, obj, i);
        }
    }

    /* renamed from: h */
    public static void m23584h(org.mozilla1.javascript.Scriptable avf, String str) {
        if (avf instanceof org.mozilla1.javascript.ConstProperties) {
            ((org.mozilla1.javascript.ConstProperties) avf).defineConst(str, avf);
        } else {
            m23567a(avf, str, org.mozilla1.javascript.Undefined.instance, 13);
        }
    }

    /* renamed from: v */
    public static org.mozilla1.javascript.Scriptable m23593v(org.mozilla1.javascript.Scriptable avf) {
        return m23585i(avf, "Object");
    }

    /* renamed from: w */
    public static org.mozilla1.javascript.Scriptable m23594w(org.mozilla1.javascript.Scriptable avf) {
        return m23585i(avf, "Function");
    }

    /* renamed from: i */
    public static org.mozilla1.javascript.Scriptable m23585i(org.mozilla1.javascript.Scriptable avf, String str) {
        Object obj;
        Object j = m23587j(m23595x(avf), str);
        if (j instanceof org.mozilla1.javascript.BaseFunction) {
            obj = ((BaseFunction) j).bOI();
        } else if (!(j instanceof org.mozilla1.javascript.Scriptable)) {
            return null;
        } else {
            org.mozilla1.javascript.Scriptable avf2 = (org.mozilla1.javascript.Scriptable) j;
            obj = avf2.get("prototype", avf2);
        }
        if (obj instanceof org.mozilla1.javascript.Scriptable) {
            return (org.mozilla1.javascript.Scriptable) obj;
        }
        return null;
    }

    /* renamed from: x */
    public static org.mozilla1.javascript.Scriptable m23595x(org.mozilla1.javascript.Scriptable avf) {
        while (true) {
            org.mozilla1.javascript.Scriptable parentScope = avf.getParentScope();
            if (parentScope == null) {
                return avf;
            }
            avf = parentScope;
        }
    }

    /* renamed from: j */
    public static Object m23587j(org.mozilla1.javascript.Scriptable avf, String str) {
        Object obj;
        org.mozilla1.javascript.Scriptable avf2 = avf;
        do {
            obj = avf2.get(str, avf);
            if (obj != org.mozilla1.javascript.Scriptable.NOT_FOUND || (avf2 = avf2.getPrototype()) == null) {
                return obj;
            }
            obj = avf2.get(str, avf);
            break;
        } while ((avf2 = avf2.getPrototype()) == null);
        return obj;
    }

    /* renamed from: b */
    public static Object m23575b(org.mozilla1.javascript.Scriptable avf, int i) {
        Object obj;
        org.mozilla1.javascript.Scriptable avf2 = avf;
        do {
            obj = avf2.get(i, avf);
            if (obj != org.mozilla1.javascript.Scriptable.NOT_FOUND || (avf2 = avf2.getPrototype()) == null) {
                return obj;
            }
            obj = avf2.get(i, avf);
            break;
        } while ((avf2 = avf2.getPrototype()) == null);
        return obj;
    }

    /* renamed from: k */
    public static boolean m23589k(org.mozilla1.javascript.Scriptable avf, String str) {
        return m23592m(avf, str) != null;
    }

    /* renamed from: a */
    public static void m23568a(org.mozilla1.javascript.Scriptable avf, String str, boolean z) {
        org.mozilla1.javascript.Scriptable m = m23592m(avf, str);
        if (m != null) {
            if ((m instanceof org.mozilla1.javascript.ConstProperties) && ((org.mozilla1.javascript.ConstProperties) m).isConst(str)) {
                throw org.mozilla1.javascript.Context.m35246q("msg.const.redecl", str);
            } else if (z) {
                throw org.mozilla1.javascript.Context.m35246q("msg.var.redecl", str);
            }
        }
    }

    /* renamed from: c */
    public static boolean m23580c(org.mozilla1.javascript.Scriptable avf, int i) {
        return m23583e(avf, i) != null;
    }

    /* renamed from: a */
    public static void m23566a(org.mozilla1.javascript.Scriptable avf, String str, Object obj) {
        org.mozilla1.javascript.Scriptable m = m23592m(avf, str);
        if (m == null) {
            m = avf;
        }
        m.put(str, avf, obj);
    }

    /* renamed from: b */
    public static void m23578b(org.mozilla1.javascript.Scriptable avf, String str, Object obj) {
        org.mozilla1.javascript.Scriptable m = m23592m(avf, str);
        if (m == null) {
            m = avf;
        }
        if (m instanceof org.mozilla1.javascript.ConstProperties) {
            ((org.mozilla1.javascript.ConstProperties) m).putConst(str, avf, obj);
        }
    }

    /* renamed from: a */
    public static void m23564a(org.mozilla1.javascript.Scriptable avf, int i, Object obj) {
        org.mozilla1.javascript.Scriptable e = m23583e(avf, i);
        if (e == null) {
            e = avf;
        }
        e.put(i, avf, obj);
    }

    /* renamed from: l */
    public static boolean m23591l(org.mozilla1.javascript.Scriptable avf, String str) {
        org.mozilla1.javascript.Scriptable m = m23592m(avf, str);
        if (m == null) {
            return true;
        }
        m.delete(str);
        if (m.has(str, avf)) {
            return false;
        }
        return true;
    }

    /* renamed from: d */
    public static boolean m23582d(org.mozilla1.javascript.Scriptable avf, int i) {
        org.mozilla1.javascript.Scriptable e = m23583e(avf, i);
        if (e == null) {
            return true;
        }
        e.delete(i);
        if (e.has(i, avf)) {
            return false;
        }
        return true;
    }

    /* renamed from: y */
    public static Object[] m23596y(org.mozilla1.javascript.Scriptable avf) {
        org.mozilla1.javascript.ObjToIntMap uSVar;
        if (avf == null) {
            return org.mozilla1.javascript.ScriptRuntime.emptyArgs;
        }
        Object[] ids = avf.getIds();
        org.mozilla1.javascript.ObjToIntMap uSVar2 = null;
        while (true) {
            avf = avf.getPrototype();
            if (avf == null) {
                break;
            }
            Object[] ids2 = avf.getIds();
            if (ids2.length != 0) {
                if (uSVar2 != null) {
                    uSVar = uSVar2;
                } else if (ids.length == 0) {
                    ids = ids2;
                } else {
                    uSVar = new ObjToIntMap(ids.length + ids2.length);
                    for (int i = 0; i != ids.length; i++) {
                        uSVar.intern(ids[i]);
                    }
                    ids = null;
                }
                for (int i2 = 0; i2 != ids2.length; i2++) {
                    uSVar.intern(ids2[i2]);
                }
                uSVar2 = uSVar;
            }
        }
        if (uSVar2 != null) {
            return uSVar2.getKeys();
        }
        return ids;
    }

    /* renamed from: b */
    public static Object m23576b(org.mozilla1.javascript.Scriptable avf, String str, Object[] objArr) {
        return m23579c((org.mozilla1.javascript.Context) null, avf, str, objArr);
    }

    /* renamed from: c */
    public static Object m23579c(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, String str, Object[] objArr) {
        Object j = m23587j(avf, str);
        if (!(j instanceof org.mozilla1.javascript.Function)) {
            throw org.mozilla1.javascript.ScriptRuntime.notFunctionError(avf, str);
        }
        org.mozilla1.javascript.Function azg = (Function) j;
        org.mozilla1.javascript.Scriptable x = m23595x(avf);
        if (lhVar != null) {
            return azg.call(lhVar, x, avf, objArr);
        }
        return org.mozilla1.javascript.Context.m35234a((ContextFactory) null, (org.mozilla1.javascript.Callable) azg, x, avf, objArr);
    }

    /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP:0: B:0:0x0000->B:4:0x000b, LOOP_START, MTH_ENTER_BLOCK, PHI: r1
  PHI: (r1v1 a.aVF) = (r1v0 a.aVF), (r1v2 a.aVF) binds: [B:0:0x0000, B:4:0x000b] A[DONT_GENERATE, DONT_INLINE]] */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static org.mozilla1.javascript.Scriptable m23592m(org.mozilla1.javascript.Scriptable r1, java.lang.String r2) {
        /*
        L_0x0000:
            boolean r0 = r1.has((java.lang.String) r2, (p001a.aVF) r1)
            if (r0 == 0) goto L_0x0007
        L_0x0006:
            return r1
        L_0x0007:
            a.aVF r1 = r1.getPrototype()
            if (r1 != 0) goto L_0x0000
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6327akn.m23592m(a.aVF, java.lang.String):a.aVF");
    }

    /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP:0: B:0:0x0000->B:4:0x000b, LOOP_START, MTH_ENTER_BLOCK, PHI: r1
  PHI: (r1v1 a.aVF) = (r1v0 a.aVF), (r1v2 a.aVF) binds: [B:0:0x0000, B:4:0x000b] A[DONT_GENERATE, DONT_INLINE]] */
    /* renamed from: e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static org.mozilla1.javascript.Scriptable m23583e(org.mozilla1.javascript.Scriptable r1, int r2) {
        /*
        L_0x0000:
            boolean r0 = r1.has((int) r2, (p001a.aVF) r1)
            if (r0 == 0) goto L_0x0007
        L_0x0006:
            return r1
        L_0x0007:
            a.aVF r1 = r1.getPrototype()
            if (r1 != 0) goto L_0x0000
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6327akn.m23583e(a.aVF, int):a.aVF");
    }

    /* renamed from: d */
    public static Object m23581d(org.mozilla1.javascript.Scriptable avf, Object obj) {
        Object associatedValue;
        org.mozilla1.javascript.Scriptable x = m23595x(avf);
        do {
            if ((x instanceof ScriptableObject) && (associatedValue = ((ScriptableObject) x).getAssociatedValue(obj)) != null) {
                return associatedValue;
            }
            x = x.getPrototype();
        } while (x != null);
        return null;
    }

    /* renamed from: aK */
    private static int m23572aK(int i, int i2) {
        return (Integer.MAX_VALUE & i2) % i;
    }

    /* renamed from: a */
    private static void m23570a(C1931a[] aVarArr, C1931a[] aVarArr2, int i) {
        if (i == 0) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        int length = aVarArr2.length;
        int length2 = aVarArr.length;
        while (true) {
            length2--;
            C1931a aVar = aVarArr[length2];
            while (true) {
                if (aVar != null) {
                    int aK = m23572aK(length, aVar.fTh);
                    C1931a aVar2 = aVar.fTk;
                    m23569a(aVarArr2, aVar, aK);
                    aVar.fTk = null;
                    i--;
                    if (i != 0) {
                        aVar = aVar2;
                    } else {
                        return;
                    }
                }
            }
        }
    }

    /* renamed from: a */
    private static void m23569a(C1931a[] aVarArr, C1931a aVar, int i) {
        if (aVarArr[i] == null) {
            aVarArr[i] = aVar;
            return;
        }
        C1931a aVar2 = aVarArr[i];
        while (aVar2.fTk != null) {
            aVar2 = aVar2.fTk;
        }
        aVar2.fTk = aVar;
    }

    public abstract String getClassName();

    public boolean has(String str, org.mozilla1.javascript.Scriptable avf) {
        return m23588k(str, 0, 1) != null;
    }

    public boolean has(int i, org.mozilla1.javascript.Scriptable avf) {
        return m23588k((String) null, i, 1) != null;
    }

    public Object get(String str, org.mozilla1.javascript.Scriptable avf) {
        return m23562a(str, 0, avf);
    }

    public Object get(int i, org.mozilla1.javascript.Scriptable avf) {
        return m23562a((String) null, i, avf);
    }

    public void put(String str, org.mozilla1.javascript.Scriptable avf, Object obj) {
        if (!m23571a(str, 0, avf, obj, 0)) {
            if (avf == this) {
                throw org.mozilla1.javascript.Kit.codeBug();
            }
            avf.put(str, avf, obj);
        }
    }

    public void put(int i, org.mozilla1.javascript.Scriptable avf, Object obj) {
        if (!m23571a((String) null, i, avf, obj, 0)) {
            if (avf == this) {
                throw org.mozilla1.javascript.Kit.codeBug();
            }
            avf.put(i, avf, obj);
        }
    }

    public void delete(String str) {
        m23560C(str, 0);
        m23590l(str, 0, 3);
    }

    public void delete(int i) {
        m23560C((String) null, i);
        m23590l((String) null, i, 3);
    }

    /* renamed from: a */
    public void putConst(String str, org.mozilla1.javascript.Scriptable avf, Object obj) {
        if (!m23571a(str, 0, avf, obj, 1)) {
            if (avf == this) {
                throw org.mozilla1.javascript.Kit.codeBug();
            } else if (avf instanceof org.mozilla1.javascript.ConstProperties) {
                ((org.mozilla1.javascript.ConstProperties) avf).putConst(str, avf, obj);
            } else {
                avf.put(str, avf, obj);
            }
        }
    }

    /* renamed from: b */
    public void defineConst(String str, org.mozilla1.javascript.Scriptable avf) {
        if (!m23571a(str, 0, avf, org.mozilla1.javascript.Undefined.instance, 8)) {
            if (avf == this) {
                throw org.mozilla1.javascript.Kit.codeBug();
            } else if (avf instanceof org.mozilla1.javascript.ConstProperties) {
                ((ConstProperties) avf).defineConst(str, avf);
            }
        }
    }

    /* renamed from: em */
    public boolean isConst(String str) {
        C1931a k = m23588k(str, 0, 1);
        if (k != null && (k.cgg() & 5) == 5) {
            return true;
        }
        return false;
    }

    /* renamed from: e */
    public final int mo14509e(String str, org.mozilla1.javascript.Scriptable avf) {
        return getAttributes(str);
    }

    /* renamed from: a */
    public final int mo14496a(int i, org.mozilla1.javascript.Scriptable avf) {
        return getAttributes(i);
    }

    /* renamed from: a */
    public final void mo14501a(String str, org.mozilla1.javascript.Scriptable avf, int i) {
        setAttributes(str, i);
    }

    /* renamed from: a */
    public void mo14498a(int i, org.mozilla1.javascript.Scriptable avf, int i2) {
        setAttributes(i, i2);
    }

    public int getAttributes(String str) {
        return m23586j(str, 0, 1).cgg();
    }

    public int getAttributes(int i) {
        return m23586j((String) null, i, 1).cgg();
    }

    public void setAttributes(String str, int i) {
        m23560C(str, 0);
        m23586j(str, 0, 2).mo14521sd(i);
    }

    public void setAttributes(int i, int i2) {
        m23560C((String) null, i);
        m23586j((String) null, i, 2).mo14521sd(i2);
    }

    /* renamed from: a */
    public void mo14500a(String str, int i, Callable mFVar, boolean z) {
        if (str == null || i == 0) {
            m23560C(str, i);
            C1932b bVar = (C1932b) m23588k(str, i, 4);
            bVar.cgh();
            if (z) {
                bVar.setter = mFVar;
            } else {
                bVar.getter = mFVar;
            }
            bVar.value = org.mozilla1.javascript.Undefined.instance;
            return;
        }
        throw new IllegalArgumentException(str);
    }

    /* renamed from: a */
    public Object mo14497a(String str, int i, boolean z) {
        if (str == null || i == 0) {
            C1931a k = m23588k(str, i, 1);
            if (k == null) {
                return null;
            }
            if (!(k instanceof C1932b)) {
                return org.mozilla1.javascript.Undefined.instance;
            }
            C1932b bVar = (C1932b) k;
            Object obj = z ? bVar.setter : bVar.getter;
            if (obj == null) {
                return Undefined.instance;
            }
            return obj;
        }
        throw new IllegalArgumentException(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public boolean mo14503b(String str, int i, boolean z) {
        C1931a k = m23588k(str, i, 1);
        if (k instanceof C1932b) {
            if (z && ((C1932b) k).setter != null) {
                return true;
            }
            if (!z && ((C1932b) k).getter != null) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo14499a(String str, int i, LazilyLoadedCtor aml, int i2) {
        if (str == null || i == 0) {
            m23560C(str, i);
            C1932b bVar = (C1932b) m23588k(str, i, 4);
            bVar.mo14521sd(i2);
            bVar.getter = null;
            bVar.setter = null;
            bVar.value = aml;
            return;
        }
        throw new IllegalArgumentException(str);
    }

    public org.mozilla1.javascript.Scriptable getPrototype() {
        return this.iPZ;
    }

    public void setPrototype(org.mozilla1.javascript.Scriptable avf) {
        this.iPZ = avf;
    }

    public org.mozilla1.javascript.Scriptable getParentScope() {
        return this.iQa;
    }

    public void setParentScope(org.mozilla1.javascript.Scriptable avf) {
        this.iQa = avf;
    }

    public Object[] getIds() {
        return mo12353dP(false);
    }

    public Object[] getAllIds() {
        return mo12353dP(true);
    }

    public Object getDefaultValue(Class<?> cls) {
        return m23561a((org.mozilla1.javascript.Scriptable) this, cls);
    }

    public boolean hasInstance(org.mozilla1.javascript.Scriptable avf) {
        return org.mozilla1.javascript.ScriptRuntime.m7561c(avf, (org.mozilla1.javascript.Scriptable) this);
    }

    public boolean dwc() {
        return false;
    }

    /* access modifiers changed from: protected */
    public Object equivalentValues(Object obj) {
        return this == obj ? Boolean.TRUE : org.mozilla1.javascript.Scriptable.NOT_FOUND;
    }

    public void defineProperty(String str, Object obj, int i) {
        m23560C(str, 0);
        put(str, (org.mozilla1.javascript.Scriptable) this, obj);
        setAttributes(str, i);
    }

    public void defineProperty(String str, Class<?> cls, int i) {
        int i2;
        int length = str.length();
        if (length == 0) {
            throw new IllegalArgumentException();
        }
        char[] cArr = new char[(length + 3)];
        str.getChars(0, length, cArr, 3);
        cArr[3] = Character.toUpperCase(cArr[3]);
        cArr[0] = 'g';
        cArr[1] = 'e';
        cArr[2] = 't';
        String str2 = new String(cArr);
        cArr[0] = 's';
        String str3 = new String(cArr);
        Method[] ah = FunctionObject.m22162ah(cls);
        Method a = FunctionObject.m22161a(ah, str2);
        Method a2 = FunctionObject.m22161a(ah, str3);
        if (a2 == null) {
            i2 = i | 1;
        } else {
            i2 = i;
        }
        if (a2 == null) {
            a2 = null;
        }
        defineProperty(str, (Object) null, a, a2, i2);
    }

    public void defineProperty(String str, Object obj, Method method, Method method2, int i) {
        C6437amt amt;
        C6437amt amt2;
        boolean z;
        boolean z2;
        String str2;
        String str3 = null;
        if (method != null) {
            C6437amt amt3 = new C6437amt(method);
            if (!Modifier.isStatic(method.getModifiers())) {
                z2 = obj != null;
                amt3.gaN = obj;
            } else {
                amt3.gaN = Void.TYPE;
                z2 = true;
            }
            Class<?>[] parameterTypes = method.getParameterTypes();
            if (parameterTypes.length == 0) {
                if (z2) {
                    str2 = "msg.obj.getter.parms";
                }
                str2 = null;
            } else if (parameterTypes.length == 1) {
                Class<?> cls = parameterTypes[0];
                if (cls == org.mozilla1.javascript.ScriptRuntime.ScriptableClass || cls == org.mozilla1.javascript.ScriptRuntime.ScriptableObjectClass) {
                    if (!z2) {
                        str2 = "msg.bad.getter.parms";
                    }
                    str2 = null;
                } else {
                    str2 = "msg.bad.getter.parms";
                }
            } else {
                str2 = "msg.bad.getter.parms";
            }
            if (str2 != null) {
                throw org.mozilla1.javascript.Context.m35246q(str2, method.toString());
            }
            amt = amt3;
        } else {
            amt = null;
        }
        if (method2 == null) {
            amt2 = null;
        } else if (method2.getReturnType() != Void.TYPE) {
            throw org.mozilla1.javascript.Context.m35246q("msg.setter.return", method2.toString());
        } else {
            C6437amt amt4 = new C6437amt(method2);
            if (!Modifier.isStatic(method2.getModifiers())) {
                z = obj != null;
                amt4.gaN = obj;
            } else {
                amt4.gaN = Void.TYPE;
                z = true;
            }
            Class<?>[] parameterTypes2 = method2.getParameterTypes();
            if (parameterTypes2.length == 1) {
                if (z) {
                    str3 = "msg.setter2.expected";
                }
            } else if (parameterTypes2.length == 2) {
                Class<?> cls2 = parameterTypes2[0];
                if (cls2 != org.mozilla1.javascript.ScriptRuntime.ScriptableClass && cls2 != org.mozilla1.javascript.ScriptRuntime.ScriptableObjectClass) {
                    str3 = "msg.setter2.parms";
                } else if (!z) {
                    str3 = "msg.setter1.parms";
                }
            } else {
                str3 = "msg.setter.parms";
            }
            if (str3 != null) {
                throw org.mozilla1.javascript.Context.m35246q(str3, method2.toString());
            }
            amt2 = amt4;
        }
        C1932b bVar = (C1932b) m23588k(str, 0, 4);
        bVar.mo14521sd(i);
        bVar.getter = amt;
        bVar.setter = amt2;
    }

    public void defineFunctionProperties(String[] strArr, Class<?> cls, int i) {
        Method[] ah = FunctionObject.m22162ah(cls);
        for (String str : strArr) {
            Method a = FunctionObject.m22161a(ah, str);
            if (a == null) {
                throw org.mozilla1.javascript.Context.m35224a("msg.method.not.found", (Object) str, (Object) cls.getName());
            }
            defineProperty(str, (Object) new FunctionObject(str, a, this), i);
        }
    }

    public synchronized void sealObject() {
        if (this.count >= 0) {
            this.count ^= -1;
        }
    }

    public final boolean isSealed() {
        return this.count < 0;
    }

    /* renamed from: C */
    private void m23560C(String str, int i) {
        if (isSealed()) {
            if (str == null) {
                str = Integer.toString(i);
            }
            throw org.mozilla1.javascript.Context.m35246q("msg.modify.sealed", str);
        }
    }

    public final Object getAssociatedValue(Object obj) {
        Map<Object, Object> map = this.iQg;
        if (map == null) {
            return null;
        }
        return map.get(obj);
    }

    public final synchronized Object associateValue(Object obj, Object obj2) {
        Map map;
        if (obj2 == null) {
            throw new IllegalArgumentException();
        }
        map = this.iQg;
        if (map == null && (map = this.iQg) == null) {
            map = new HashMap();
            this.iQg = map;
        }
        return Kit.m11147a(map, obj, obj2);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v1, resolved type: a.aVF} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v2, resolved type: a.aVF} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object m23562a(java.lang.String r5, int r6, org.mozilla1.javascript.Scriptable r7) {
        /*
            r4 = this;
            r3 = 1
            a.akn$a r1 = r4.m23588k(r5, r6, r3)
            if (r1 != 0) goto L_0x000a
            java.lang.Object r0 = p001a.aVF.NOT_FOUND
        L_0x0009:
            return r0
        L_0x000a:
            boolean r0 = r1 instanceof p001a.C6327akn.C1932b
            if (r0 != 0) goto L_0x0011
            java.lang.Object r0 = r1.value
            goto L_0x0009
        L_0x0011:
            r0 = r1
            a.akn$b r0 = (p001a.C6327akn.C1932b) r0
            java.lang.Object r0 = r0.getter
            if (r0 == 0) goto L_0x0043
            boolean r1 = r0 instanceof p001a.C6437amt
            if (r1 == 0) goto L_0x0032
            a.amt r0 = (p001a.C6437amt) r0
            java.lang.Object r1 = r0.gaN
            if (r1 != 0) goto L_0x0029
            java.lang.Object[] r1 = p001a.C0903NH.emptyArgs
        L_0x0024:
            java.lang.Object r0 = r0.invoke(r7, r1)
            goto L_0x0009
        L_0x0029:
            java.lang.Object r2 = r0.gaN
            java.lang.Object[] r1 = new java.lang.Object[r3]
            r3 = 0
            r1[r3] = r7
            r7 = r2
            goto L_0x0024
        L_0x0032:
            a.azg r0 = (p001a.C7019azg) r0
            a.lh r1 = p001a.C2909lh.bwA()
            a.aVF r2 = r0.getParentScope()
            java.lang.Object[] r3 = p001a.C0903NH.emptyArgs
            java.lang.Object r0 = r0.call(r1, r2, r7, r3)
            goto L_0x0009
        L_0x0043:
            java.lang.Object r0 = r1.value
            boolean r2 = r0 instanceof p001a.C6403amL
            if (r2 == 0) goto L_0x0009
            a.amL r0 = (p001a.C6403amL) r0
            r0.init()     // Catch:{ all -> 0x0055 }
            java.lang.Object r0 = r0.getValue()
            r1.value = r0
            goto L_0x0009
        L_0x0055:
            r2 = move-exception
            java.lang.Object r0 = r0.getValue()
            r1.value = r0
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6327akn.m23562a(java.lang.String, int, a.aVF):java.lang.Object");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v1, resolved type: a.aVF} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v2, resolved type: a.aVF} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m23571a(java.lang.String r8, int r9, Scriptable r10, java.lang.Object r11, int r12) {
        /*
            r7 = this;
            r6 = 2
            r3 = 0
            r4 = 1
            if (r7 == r10) goto L_0x000d
            a.akn$a r1 = r7.m23588k(r8, r9, r4)
            if (r1 != 0) goto L_0x003b
            r0 = r3
        L_0x000c:
            return r0
        L_0x000d:
            r7.m23560C(r8, r9)
            if (r12 == 0) goto L_0x0037
            r0 = 5
            a.akn$a r0 = r7.m23588k(r8, r9, r0)
            int r1 = r0.cgg()
            r2 = r1 & 1
            if (r2 != 0) goto L_0x0026
            java.lang.String r0 = "msg.var.redecl"
            a.TJ r0 = p001a.C2909lh.m35246q(r0, r8)
            throw r0
        L_0x0026:
            r2 = r1 & 8
            if (r2 == 0) goto L_0x0035
            r0.value = r11
            r2 = 8
            if (r12 == r2) goto L_0x0035
            r1 = r1 & -9
            r0.mo14521sd(r1)
        L_0x0035:
            r0 = r4
            goto L_0x000c
        L_0x0037:
            a.akn$a r1 = r7.m23588k(r8, r9, r6)
        L_0x003b:
            int r0 = r1.cgg()
            r0 = r0 & 1
            if (r0 == 0) goto L_0x0045
            r0 = r4
            goto L_0x000c
        L_0x0045:
            boolean r0 = r1 instanceof p001a.C6327akn.C1932b
            if (r0 == 0) goto L_0x0056
            r0 = r1
            a.akn$b r0 = (p001a.C6327akn.C1932b) r0
            java.lang.Object r0 = r0.setter
            if (r0 != 0) goto L_0x005c
            r0 = r1
            a.akn$b r0 = (p001a.C6327akn.C1932b) r0
            r2 = 0
            r0.getter = r2
        L_0x0056:
            if (r7 != r10) goto L_0x009a
            r1.value = r11
            r0 = r4
            goto L_0x000c
        L_0x005c:
            a.lh r1 = p001a.C2909lh.bwA()
            boolean r2 = r0 instanceof p001a.C6437amt
            if (r2 == 0) goto L_0x008c
            a.amt r0 = (p001a.C6437amt) r0
            java.lang.Class<?>[] r2 = r0.gaM
            int r5 = r2.length
            int r5 = r5 + -1
            r2 = r2[r5]
            int r2 = p001a.C6131agz.getTypeTag(r2)
            java.lang.Object r5 = p001a.C6131agz.m22160a(r1, r10, r11, r2)
            java.lang.Object r1 = r0.gaN
            if (r1 != 0) goto L_0x0082
            java.lang.Object[] r1 = new java.lang.Object[r4]
            r1[r3] = r5
        L_0x007d:
            r0.invoke(r10, r1)
        L_0x0080:
            r0 = r4
            goto L_0x000c
        L_0x0082:
            java.lang.Object r2 = r0.gaN
            java.lang.Object[] r1 = new java.lang.Object[r6]
            r1[r3] = r10
            r1[r4] = r5
            r10 = r2
            goto L_0x007d
        L_0x008c:
            a.azg r0 = (p001a.C7019azg) r0
            a.aVF r2 = r0.getParentScope()
            java.lang.Object[] r5 = new java.lang.Object[r4]
            r5[r3] = r11
            r0.call(r1, r2, r10, r5)
            goto L_0x0080
        L_0x009a:
            r0 = r3
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6327akn.m23571a(java.lang.String, int, a.aVF, java.lang.Object, int):boolean");
    }

    /* renamed from: j */
    private C1931a m23586j(String str, int i, int i2) {
        C1931a k = m23588k(str, i, i2);
        if (k != null) {
            return k;
        }
        if (str == null) {
            str = Integer.toString(i);
        }
        throw org.mozilla1.javascript.Context.m35246q("msg.prop.not.found", str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0022, code lost:
        if ((r0 instanceof p001a.C6327akn.C1932b) != false) goto L_0x0010;
     */
    /* renamed from: k */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private ScriptableObject.C1931a m23588k(java.lang.String r3, int r4, int r5) {
        /*
            r2 = this;
            a.akn$a r0 = r2.iQf
            if (r3 == 0) goto L_0x0011
            java.lang.String r1 = r0.name
            if (r3 == r1) goto L_0x0019
        L_0x0008:
            a.akn$a r0 = r2.m23590l(r3, r4, r5)
            if (r0 == 0) goto L_0x0010
            r2.iQf = r0
        L_0x0010:
            return r0
        L_0x0011:
            java.lang.String r1 = r0.name
            if (r1 != 0) goto L_0x0008
            int r1 = r0.fTh
            if (r4 != r1) goto L_0x0008
        L_0x0019:
            boolean r1 = r0.fTj
            if (r1 != 0) goto L_0x0008
            r1 = 4
            if (r5 != r1) goto L_0x0010
            boolean r1 = r0 instanceof p001a.C6327akn.C1932b
            if (r1 != 0) goto L_0x0010
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6327akn.m23588k(java.lang.String, int, int):a.akn$a");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:159:?, code lost:
        return r0;
     */
    /* renamed from: l */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private ScriptableObject.C1931a m23590l(java.lang.String r9, int r10, int r11) {
        /*
            r8 = this;
            r7 = 4
            r6 = 5
            r4 = 1
            r0 = 0
            if (r9 == 0) goto L_0x000a
            int r10 = r9.hashCode()
        L_0x000a:
            if (r11 == r4) goto L_0x0013
            r1 = 2
            if (r11 == r1) goto L_0x0013
            if (r11 == r6) goto L_0x0013
            if (r11 != r7) goto L_0x012f
        L_0x0013:
            a.akn$a[] r1 = r8.iQc
            if (r1 != 0) goto L_0x001a
            if (r11 != r4) goto L_0x005a
        L_0x0019:
            return r0
        L_0x001a:
            int r2 = r1.length
            int r2 = m23572aK(r2, r10)
            r1 = r1[r2]
        L_0x0021:
            if (r1 != 0) goto L_0x0027
        L_0x0023:
            if (r11 != r4) goto L_0x0045
            r0 = r1
            goto L_0x0019
        L_0x0027:
            java.lang.String r2 = r1.name
            if (r2 == 0) goto L_0x003c
            if (r2 == r9) goto L_0x0023
            if (r9 == 0) goto L_0x0042
            int r3 = r1.fTh
            if (r10 != r3) goto L_0x0042
            boolean r2 = r9.equals(r2)
            if (r2 == 0) goto L_0x0042
            r1.name = r9
            goto L_0x0023
        L_0x003c:
            if (r9 != 0) goto L_0x0042
            int r2 = r1.fTh
            if (r10 == r2) goto L_0x0023
        L_0x0042:
            a.akn$a r1 = r1.fTk
            goto L_0x0021
        L_0x0045:
            r2 = 2
            if (r11 != r2) goto L_0x004c
            if (r1 == 0) goto L_0x005a
            r0 = r1
            goto L_0x0019
        L_0x004c:
            if (r11 != r7) goto L_0x0054
            boolean r2 = r1 instanceof p001a.C6327akn.C1932b
            if (r2 == 0) goto L_0x005a
            r0 = r1
            goto L_0x0019
        L_0x0054:
            if (r11 != r6) goto L_0x005a
            if (r1 == 0) goto L_0x005a
            r0 = r1
            goto L_0x0019
        L_0x005a:
            monitor-enter(r8)
            a.akn$a[] r2 = r8.iQc     // Catch:{ all -> 0x0096 }
            int r1 = r8.count     // Catch:{ all -> 0x0096 }
            if (r1 != 0) goto L_0x0099
            r0 = 5
            a.akn$a[] r2 = new p001a.C6327akn.C1931a[r0]     // Catch:{ all -> 0x0096 }
            r8.iQc = r2     // Catch:{ all -> 0x0096 }
            int r0 = r2.length     // Catch:{ all -> 0x0096 }
            int r0 = m23572aK(r0, r10)     // Catch:{ all -> 0x0096 }
            r1 = r0
        L_0x006c:
            if (r11 != r7) goto L_0x0127
            a.akn$b r0 = new a.akn$b     // Catch:{ all -> 0x0096 }
            r3 = 0
            r0.<init>(r9, r10, r3)     // Catch:{ all -> 0x0096 }
        L_0x0074:
            if (r11 != r6) goto L_0x007b
            r3 = 13
            r0.mo14521sd(r3)     // Catch:{ all -> 0x0096 }
        L_0x007b:
            int r3 = r8.count     // Catch:{ all -> 0x0096 }
            int r3 = r3 + 1
            r8.count = r3     // Catch:{ all -> 0x0096 }
            a.akn$a r3 = r8.iQe     // Catch:{ all -> 0x0096 }
            if (r3 == 0) goto L_0x0089
            a.akn$a r3 = r8.iQe     // Catch:{ all -> 0x0096 }
            r3.fTl = r0     // Catch:{ all -> 0x0096 }
        L_0x0089:
            a.akn$a r3 = r8.iQd     // Catch:{ all -> 0x0096 }
            if (r3 != 0) goto L_0x008f
            r8.iQd = r0     // Catch:{ all -> 0x0096 }
        L_0x008f:
            r8.iQe = r0     // Catch:{ all -> 0x0096 }
            m23569a((p001a.C6327akn.C1931a[]) r2, (p001a.C6327akn.C1931a) r0, (int) r1)     // Catch:{ all -> 0x0096 }
            monitor-exit(r8)     // Catch:{ all -> 0x0096 }
            goto L_0x0019
        L_0x0096:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0096 }
            throw r0
        L_0x0099:
            int r1 = r2.length     // Catch:{ all -> 0x0096 }
            int r1 = m23572aK(r1, r10)     // Catch:{ all -> 0x0096 }
            r4 = r2[r1]     // Catch:{ all -> 0x0096 }
            r3 = r4
            r5 = r4
        L_0x00a2:
            if (r3 != 0) goto L_0x00e5
        L_0x00a4:
            if (r3 == 0) goto L_0x0104
            if (r11 != r7) goto L_0x00ff
            boolean r4 = r3 instanceof p001a.C6327akn.C1932b     // Catch:{ all -> 0x0096 }
            if (r4 != 0) goto L_0x00ff
            a.akn$b r0 = new a.akn$b     // Catch:{ all -> 0x0096 }
            int r4 = r3.cgg()     // Catch:{ all -> 0x0096 }
            r0.<init>(r9, r10, r4)     // Catch:{ all -> 0x0096 }
            java.lang.Object r4 = r3.value     // Catch:{ all -> 0x0096 }
            r0.value = r4     // Catch:{ all -> 0x0096 }
            a.akn$a r4 = r3.fTk     // Catch:{ all -> 0x0096 }
            r0.fTk = r4     // Catch:{ all -> 0x0096 }
            a.akn$a r4 = r8.iQe     // Catch:{ all -> 0x0096 }
            if (r4 == 0) goto L_0x00c5
            a.akn$a r4 = r8.iQe     // Catch:{ all -> 0x0096 }
            r4.fTl = r0     // Catch:{ all -> 0x0096 }
        L_0x00c5:
            a.akn$a r4 = r8.iQd     // Catch:{ all -> 0x0096 }
            if (r4 != 0) goto L_0x00cb
            r8.iQd = r0     // Catch:{ all -> 0x0096 }
        L_0x00cb:
            r8.iQe = r0     // Catch:{ all -> 0x0096 }
            if (r5 != r3) goto L_0x00fc
            r2[r1] = r0     // Catch:{ all -> 0x0096 }
        L_0x00d1:
            r1 = 1
            r3.fTj = r1     // Catch:{ all -> 0x0096 }
            r1 = 0
            r3.value = r1     // Catch:{ all -> 0x0096 }
            r1 = 0
            r3.name = r1     // Catch:{ all -> 0x0096 }
            a.akn$a r1 = r8.iQf     // Catch:{ all -> 0x0096 }
            if (r3 != r1) goto L_0x00e2
            a.akn$a r1 = iQb     // Catch:{ all -> 0x0096 }
            r8.iQf = r1     // Catch:{ all -> 0x0096 }
        L_0x00e2:
            monitor-exit(r8)     // Catch:{ all -> 0x0096 }
            goto L_0x0019
        L_0x00e5:
            int r4 = r3.fTh     // Catch:{ all -> 0x0096 }
            if (r4 != r10) goto L_0x00f7
            java.lang.String r4 = r3.name     // Catch:{ all -> 0x0096 }
            if (r4 == r9) goto L_0x00a4
            if (r9 == 0) goto L_0x00f7
            java.lang.String r4 = r3.name     // Catch:{ all -> 0x0096 }
            boolean r4 = r9.equals(r4)     // Catch:{ all -> 0x0096 }
            if (r4 != 0) goto L_0x00a4
        L_0x00f7:
            a.akn$a r4 = r3.fTk     // Catch:{ all -> 0x0096 }
            r5 = r3
            r3 = r4
            goto L_0x00a2
        L_0x00fc:
            r5.fTk = r0     // Catch:{ all -> 0x0096 }
            goto L_0x00d1
        L_0x00ff:
            if (r11 != r6) goto L_0x0194
            monitor-exit(r8)     // Catch:{ all -> 0x0096 }
            goto L_0x0019
        L_0x0104:
            int r0 = r8.count     // Catch:{ all -> 0x0096 }
            int r0 = r0 + 1
            int r0 = r0 * 4
            int r3 = r2.length     // Catch:{ all -> 0x0096 }
            int r3 = r3 * 3
            if (r0 <= r3) goto L_0x006c
            int r0 = r2.length     // Catch:{ all -> 0x0096 }
            int r0 = r0 * 2
            int r0 = r0 + 1
            a.akn$a[] r2 = new p001a.C6327akn.C1931a[r0]     // Catch:{ all -> 0x0096 }
            a.akn$a[] r0 = r8.iQc     // Catch:{ all -> 0x0096 }
            int r1 = r8.count     // Catch:{ all -> 0x0096 }
            m23570a((p001a.C6327akn.C1931a[]) r0, (p001a.C6327akn.C1931a[]) r2, (int) r1)     // Catch:{ all -> 0x0096 }
            r8.iQc = r2     // Catch:{ all -> 0x0096 }
            int r0 = r2.length     // Catch:{ all -> 0x0096 }
            int r0 = m23572aK(r0, r10)     // Catch:{ all -> 0x0096 }
            r1 = r0
            goto L_0x006c
        L_0x0127:
            a.akn$a r0 = new a.akn$a     // Catch:{ all -> 0x0096 }
            r3 = 0
            r0.<init>(r9, r10, r3)     // Catch:{ all -> 0x0096 }
            goto L_0x0074
        L_0x012f:
            r1 = 3
            if (r11 != r1) goto L_0x018f
            monitor-enter(r8)
            a.akn$a[] r4 = r8.iQc     // Catch:{ all -> 0x0170 }
            int r1 = r8.count     // Catch:{ all -> 0x0170 }
            if (r1 == 0) goto L_0x016d
            a.akn$a[] r1 = r8.iQc     // Catch:{ all -> 0x0170 }
            int r1 = r1.length     // Catch:{ all -> 0x0170 }
            int r5 = m23572aK(r1, r10)     // Catch:{ all -> 0x0170 }
            r2 = r4[r5]     // Catch:{ all -> 0x0170 }
            r1 = r2
            r3 = r2
        L_0x0144:
            if (r1 != 0) goto L_0x0173
        L_0x0146:
            if (r1 == 0) goto L_0x016d
            int r2 = r1.cgg()     // Catch:{ all -> 0x0170 }
            r2 = r2 & 4
            if (r2 != 0) goto L_0x016d
            int r2 = r8.count     // Catch:{ all -> 0x0170 }
            int r2 = r2 + -1
            r8.count = r2     // Catch:{ all -> 0x0170 }
            if (r3 != r1) goto L_0x018a
            a.akn$a r2 = r1.fTk     // Catch:{ all -> 0x0170 }
            r4[r5] = r2     // Catch:{ all -> 0x0170 }
        L_0x015c:
            r2 = 1
            r1.fTj = r2     // Catch:{ all -> 0x0170 }
            r2 = 0
            r1.value = r2     // Catch:{ all -> 0x0170 }
            r2 = 0
            r1.name = r2     // Catch:{ all -> 0x0170 }
            a.akn$a r2 = r8.iQf     // Catch:{ all -> 0x0170 }
            if (r1 != r2) goto L_0x016d
            a.akn$a r1 = iQb     // Catch:{ all -> 0x0170 }
            r8.iQf = r1     // Catch:{ all -> 0x0170 }
        L_0x016d:
            monitor-exit(r8)     // Catch:{ all -> 0x0170 }
            goto L_0x0019
        L_0x0170:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0170 }
            throw r0
        L_0x0173:
            int r2 = r1.fTh     // Catch:{ all -> 0x0170 }
            if (r2 != r10) goto L_0x0185
            java.lang.String r2 = r1.name     // Catch:{ all -> 0x0170 }
            if (r2 == r9) goto L_0x0146
            if (r9 == 0) goto L_0x0185
            java.lang.String r2 = r1.name     // Catch:{ all -> 0x0170 }
            boolean r2 = r9.equals(r2)     // Catch:{ all -> 0x0170 }
            if (r2 != 0) goto L_0x0146
        L_0x0185:
            a.akn$a r2 = r1.fTk     // Catch:{ all -> 0x0170 }
            r3 = r1
            r1 = r2
            goto L_0x0144
        L_0x018a:
            a.akn$a r2 = r1.fTk     // Catch:{ all -> 0x0170 }
            r3.fTk = r2     // Catch:{ all -> 0x0170 }
            goto L_0x015c
        L_0x018f:
            java.lang.RuntimeException r0 = p001a.C1520WN.codeBug()
            throw r0
        L_0x0194:
            r0 = r3
            goto L_0x00e2
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6327akn.m23590l(java.lang.String, int, int):a.akn$a");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: dP */
    public Object[] mo12353dP(boolean z) {
        int i;
        Object num;
        C1931a[] aVarArr = this.iQc;
        Object[] objArr = ScriptRuntime.emptyArgs;
        if (aVarArr == null) {
            return objArr;
        }
        C1931a aVar = this.iQd;
        while (aVar != null && aVar.fTj) {
            aVar = aVar.fTl;
        }
        this.iQd = aVar;
        C1931a aVar2 = aVar;
        int i2 = 0;
        while (aVar2 != null) {
            if (z || (aVar2.cgg() & 2) == 0) {
                if (i2 == 0) {
                    objArr = new Object[aVarArr.length];
                }
                int i3 = i2 + 1;
                if (aVar2.name != null) {
                    num = aVar2.name;
                } else {
                    num = new Integer(aVar2.fTh);
                }
                objArr[i2] = num;
                i = i3;
            } else {
                i = i2;
            }
            C1931a aVar3 = aVar2.fTl;
            while (aVar3 != null && aVar3.fTj) {
                aVar3 = aVar3.fTl;
            }
            aVar2.fTl = aVar3;
            aVar2 = aVar3;
            i2 = i;
        }
        if (i2 == objArr.length) {
            return objArr;
        }
        Object[] objArr2 = new Object[i2];
        System.arraycopy(objArr, 0, objArr2, 0, i2);
        return objArr2;
    }

    private synchronized void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        int i = this.count;
        if (i < 0) {
            i ^= -1;
        }
        if (i == 0) {
            objectOutputStream.writeInt(0);
        } else {
            objectOutputStream.writeInt(this.iQc.length);
            C1931a aVar = this.iQd;
            while (aVar != null && aVar.fTj) {
                aVar = aVar.fTl;
            }
            this.iQd = aVar;
            C1931a aVar2 = aVar;
            while (aVar2 != null) {
                objectOutputStream.writeObject(aVar2);
                C1931a aVar3 = aVar2.fTl;
                while (aVar3 != null && aVar3.fTj) {
                    aVar3 = aVar3.fTl;
                }
                aVar2.fTl = aVar3;
                aVar2 = aVar3;
            }
        }
    }

    private void readObject(ObjectInputStream objectInputStream) {
        int i;
        objectInputStream.defaultReadObject();
        this.iQf = iQb;
        int readInt = objectInputStream.readInt();
        if (readInt != 0) {
            this.iQc = new C1931a[readInt];
            int i2 = this.count;
            if (i2 < 0) {
                i = i2 ^ -1;
            } else {
                i = i2;
            }
            C1931a aVar = null;
            for (int i3 = 0; i3 != i; i3++) {
                this.iQe = (C1931a) objectInputStream.readObject();
                if (i3 == 0) {
                    this.iQd = this.iQe;
                } else {
                    aVar.fTl = this.iQe;
                }
                m23569a(this.iQc, this.iQe, m23572aK(readInt, this.iQe.fTh));
                aVar = this.iQe;
            }
        }
    }

    /* renamed from: a.akn$a */
    private static class C1931a implements Serializable {
        private static final long serialVersionUID = -6090581677123995491L;
        int fTh;
        volatile transient boolean fTj;
        volatile transient C1931a fTk;
        volatile transient C1931a fTl;
        String name;
        volatile Object value;
        private volatile short fTi;

        C1931a(String str, int i, int i2) {
            this.name = str;
            this.fTh = i;
            this.fTi = (short) i2;
        }

        private void readObject(ObjectInputStream objectInputStream) {
            objectInputStream.defaultReadObject();
            if (this.name != null) {
                this.fTh = this.name.hashCode();
            }
        }

        /* access modifiers changed from: package-private */
        public final int cgg() {
            return this.fTi;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: sd */
        public final synchronized void mo14521sd(int i) {
            ScriptableObject.m23559AT(i);
            this.fTi = (short) i;
        }

        /* access modifiers changed from: package-private */
        public final void cgh() {
            String num;
            if ((this.fTi & 1) != 0) {
                if (this.name != null) {
                    num = this.name;
                } else {
                    num = Integer.toString(this.fTh);
                }
                throw Context.m35246q("msg.modify.readonly", num);
            }
        }
    }

    /* renamed from: a.akn$b */
    /* compiled from: a */
    private static final class C1932b extends C1931a {
        static final long serialVersionUID = -4900574849788797588L;
        Object getter;
        Object setter;

        C1932b(String str, int i, int i2) {
            super(str, i, i2);
        }
    }
}
