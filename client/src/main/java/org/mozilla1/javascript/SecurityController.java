package org.mozilla1.javascript;

/* renamed from: a.vz */
/* compiled from: a */
public abstract class SecurityController {
    private static SecurityController fUo;

    static SecurityController chn() {
        return fUo;
    }

    public static boolean hasGlobal() {
        return fUo != null;
    }

    /* renamed from: b */
    public static void m40443b(SecurityController vzVar) {
        if (vzVar == null) {
            throw new IllegalArgumentException();
        } else if (fUo != null) {
            throw new SecurityException("Cannot overwrite already installed global SecurityController");
        } else {
            fUo = vzVar;
        }
    }

    /* renamed from: a */
    public static GeneratedClassLoader m40442a(ClassLoader classLoader, Object obj) {
        org.mozilla1.javascript.Context bwA = org.mozilla1.javascript.Context.bwA();
        if (classLoader == null) {
            classLoader = bwA.getApplicationClassLoader();
        }
        SecurityController bwF = bwA.bwF();
        if (bwF == null) {
            return bwA.mo20335a(classLoader);
        }
        return bwF.createClassLoader(classLoader, bwF.getDynamicSecurityDomain(obj));
    }

    public static Class<?> cho() {
        SecurityController bwF = org.mozilla1.javascript.Context.bwA().bwF();
        if (bwF == null) {
            return null;
        }
        return bwF.getStaticSecurityDomainClassInternal();
    }

    public abstract GeneratedClassLoader createClassLoader(ClassLoader classLoader, Object obj);

    public abstract Object getDynamicSecurityDomain(Object obj);

    public Class<?> getStaticSecurityDomainClassInternal() {
        return null;
    }

    public Object callWithDomain(Object obj, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Callable mFVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        return mo22685a(lhVar, avf, new C3868a(mFVar, avf2, objArr), obj);
    }

    /* renamed from: a */
    public Object mo22685a(org.mozilla1.javascript.Context lhVar, Scriptable avf, Script afe, Object obj) {
        throw new IllegalStateException("callWithDomain should be overridden");
    }

    /* renamed from: a.vz$a */
    class C3868a implements Script {
        private final /* synthetic */ org.mozilla1.javascript.Callable awX;
        private final /* synthetic */ Scriptable awZ;
        private final /* synthetic */ Object[] axa;

        C3868a(Callable mFVar, Scriptable avf, Object[] objArr) {
            this.awX = mFVar;
            this.awZ = avf;
            this.axa = objArr;
        }

        /* renamed from: a */
        public Object mo8800a(Context lhVar, Scriptable avf) {
            return this.awX.call(lhVar, avf, this.awZ, this.axa);
        }
    }
}
