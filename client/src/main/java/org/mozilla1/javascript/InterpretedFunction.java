package org.mozilla1.javascript;

import org.mozilla1.javascript.debug.DebuggableScript;

/* renamed from: a.afO  reason: case insensitive filesystem */
/* compiled from: a */
final class InterpretedFunction extends NativeFunction implements Script {
    static final long serialVersionUID = 541475680333911468L;
    org.mozilla1.javascript.InterpreterData bgz;
    org.mozilla1.javascript.SecurityController elo;
    Object fvF;
    Scriptable[] fvG;

    private InterpretedFunction(org.mozilla1.javascript.InterpreterData aeq, Object obj) {
        Object obj2;
        this.bgz = aeq;
        SecurityController bwF = org.mozilla1.javascript.Context.bwA().bwF();
        if (bwF != null) {
            obj2 = bwF.getDynamicSecurityDomain(obj);
        } else if (obj != null) {
            throw new IllegalArgumentException();
        } else {
            obj2 = null;
        }
        this.elo = bwF;
        this.fvF = obj2;
    }

    private InterpretedFunction(InterpretedFunction afo, int i) {
        this.bgz = afo.bgz.frp[i];
        this.elo = afo.elo;
        this.fvF = afo.fvF;
    }

    /* renamed from: a */
    static InterpretedFunction m21537a(org.mozilla1.javascript.InterpreterData aeq, Object obj) {
        return new InterpretedFunction(aeq, obj);
    }

    /* renamed from: a */
    static InterpretedFunction m21538a(org.mozilla1.javascript.Context lhVar, Scriptable avf, org.mozilla1.javascript.InterpreterData aeq, Object obj) {
        InterpretedFunction afo = new InterpretedFunction(aeq, obj);
        afo.m21540e(lhVar, avf);
        return afo;
    }

    /* renamed from: a */
    static InterpretedFunction m21539a(org.mozilla1.javascript.Context lhVar, Scriptable avf, InterpretedFunction afo, int i) {
        InterpretedFunction afo2 = new InterpretedFunction(afo, i);
        afo2.m21540e(lhVar, avf);
        return afo2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public Scriptable[] mo13252d(org.mozilla1.javascript.Context lhVar, Scriptable avf) {
        if (this.bgz.frq == null) {
            Kit.codeBug();
        }
        RegExpProxy m = org.mozilla1.javascript.ScriptRuntime.m7593m(lhVar);
        int length = this.bgz.frq.length;
        Scriptable[] avfArr = new Scriptable[length];
        for (int i = 0; i != length; i++) {
            avfArr[i] = m.mo10824c(lhVar, avf, this.bgz.frq[i]);
        }
        return avfArr;
    }

    /* renamed from: e */
    private void m21540e(org.mozilla1.javascript.Context lhVar, Scriptable avf) {
        mo2857b(lhVar, avf);
        if (this.bgz.frq != null) {
            this.fvG = mo13252d(lhVar, avf);
        }
    }

    public String getFunctionName() {
        return this.bgz.frl == null ? "" : this.bgz.frl;
    }

    public Object call(org.mozilla1.javascript.Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        if (!org.mozilla1.javascript.ScriptRuntime.m7588i(lhVar)) {
            return org.mozilla1.javascript.ScriptRuntime.m7510a((org.mozilla1.javascript.Callable) this, lhVar, avf, avf2, objArr);
        }
        return Interpreter.m39133a(this, lhVar, avf, avf2, objArr);
    }

    /* renamed from: a */
    public Object mo8800a(org.mozilla1.javascript.Context lhVar, Scriptable avf) {
        if (!aBz()) {
            throw new IllegalStateException();
        } else if (!org.mozilla1.javascript.ScriptRuntime.m7588i(lhVar)) {
            return org.mozilla1.javascript.ScriptRuntime.m7510a((Callable) this, lhVar, avf, avf, org.mozilla1.javascript.ScriptRuntime.emptyArgs);
        } else {
            return Interpreter.m39133a(this, lhVar, avf, avf, ScriptRuntime.emptyArgs);
        }
    }

    public boolean aBz() {
        return this.bgz.eMN == 0;
    }

    public String getEncodedSource() {
        return Interpreter.m39157c(this.bgz);
    }

    public DebuggableScript aVK() {
        return this.bgz;
    }

    /* renamed from: a */
    public Object mo2855a(Context lhVar, Scriptable avf, int i, Object obj, Object obj2) {
        return Interpreter.m39134a(lhVar, avf, i, obj, obj2);
    }

    /* access modifiers changed from: protected */
    public int getLanguageVersion() {
        return this.bgz.frE;
    }

    /* access modifiers changed from: protected */
    public int getParamCount() {
        return this.bgz.frz;
    }

    /* access modifiers changed from: protected */
    public int getParamAndVarCount() {
        return this.bgz.frx.length;
    }

    /* access modifiers changed from: protected */
    public String getParamOrVarName(int i) {
        return this.bgz.frx[i];
    }

    /* access modifiers changed from: protected */
    /* renamed from: kH */
    public boolean mo2867kH(int i) {
        return this.bgz.fry[i];
    }
}
