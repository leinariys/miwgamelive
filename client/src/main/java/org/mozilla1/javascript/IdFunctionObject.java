package org.mozilla1.javascript;

/* renamed from: a.Yy */
/* compiled from: a */
public class IdFunctionObject extends BaseFunction {
    static final long serialVersionUID = -5332312783643935019L;
    private final org.mozilla1.javascript.IdFunctionCall eMn;
    private final Object eMo;
    private final int eMp;
    private int eMq;
    private boolean eMr;
    private String eMs;

    public IdFunctionObject(org.mozilla1.javascript.IdFunctionCall age, Object obj, int i, int i2) {
        if (i2 < 0) {
            throw new IllegalArgumentException();
        }
        this.eMn = age;
        this.eMo = obj;
        this.eMp = i;
        this.eMq = i2;
        if (i2 < 0) {
            throw new IllegalArgumentException();
        }
    }

    public IdFunctionObject(IdFunctionCall age, Object obj, int i, String str, int i2, org.mozilla1.javascript.Scriptable avf) {
        super(avf, (org.mozilla1.javascript.Scriptable) null);
        if (i2 < 0) {
            throw new IllegalArgumentException();
        } else if (str == null) {
            throw new IllegalArgumentException();
        } else {
            this.eMn = age;
            this.eMo = obj;
            this.eMp = i;
            this.eMq = i2;
            this.eMs = str;
        }
    }

    /* renamed from: d */
    public void mo7314d(String str, org.mozilla1.javascript.Scriptable avf) {
        if (str == null) {
            throw new IllegalArgumentException();
        } else if (avf == null) {
            throw new IllegalArgumentException();
        } else {
            this.eMs = str;
            setParentScope(avf);
        }
    }

    public final boolean hasTag(Object obj) {
        return this.eMo == obj;
    }

    public final int methodId() {
        return this.eMp;
    }

    /* renamed from: h */
    public final void mo7318h(org.mozilla1.javascript.Scriptable avf) {
        this.eMr = true;
        setImmunePrototypeProperty(avf);
    }

    /* renamed from: i */
    public final void mo7320i(org.mozilla1.javascript.Scriptable avf) {
        ScriptableObject.m23567a(avf, this.eMs, (Object) this, 2);
    }

    public void exportAsScopeProperty() {
        mo7320i(getParentScope());
    }

    public org.mozilla1.javascript.Scriptable getPrototype() {
        org.mozilla1.javascript.Scriptable prototype = super.getPrototype();
        if (prototype != null) {
            return prototype;
        }
        org.mozilla1.javascript.Scriptable w = ScriptableObject.m23594w(getParentScope());
        setPrototype(w);
        return w;
    }

    public Object call(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        return this.eMn.execIdCall(this, lhVar, avf, avf2, objArr);
    }

    /* renamed from: c */
    public org.mozilla1.javascript.Scriptable mo7313c(Context lhVar, org.mozilla1.javascript.Scriptable avf) {
        if (this.eMr) {
            return null;
        }
        throw ScriptRuntime.m7536aZ("msg.not.ctor", this.eMs);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public String mo2858e(int i, int i2) {
        boolean z;
        StringBuffer stringBuffer = new StringBuffer();
        if ((i2 & 1) != 0) {
            z = true;
        } else {
            z = false;
        }
        if (!z) {
            stringBuffer.append("function ");
            stringBuffer.append(getFunctionName());
            stringBuffer.append("() { ");
        }
        stringBuffer.append("[native code for ");
        if (this.eMn instanceof org.mozilla1.javascript.Scriptable) {
            stringBuffer.append(((Scriptable) this.eMn).getClassName());
            stringBuffer.append('.');
        }
        stringBuffer.append(getFunctionName());
        stringBuffer.append(", arity=");
        stringBuffer.append(getArity());
        stringBuffer.append(z ? "]\n" : "] }\n");
        return stringBuffer.toString();
    }

    public int getArity() {
        return this.eMq;
    }

    public int getLength() {
        return getArity();
    }

    public String getFunctionName() {
        return this.eMs == null ? "" : this.eMs;
    }

    public final RuntimeException unknown() {
        return new IllegalArgumentException("BAD FUNCTION ID=" + this.eMp + " MASTER=" + this.eMn);
    }
}
