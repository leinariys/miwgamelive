package org.mozilla1.javascript;

import java.util.Set;

/* renamed from: a.yh */
/* compiled from: a */
public class NativeJavaPackage extends org.mozilla1.javascript.ScriptableObject {
    static final long serialVersionUID = 7445054382212031523L;
    private transient ClassLoader bJF;
    private boolean bJG;
    private Set<String> bJH;
    private String packageName;

    NativeJavaPackage(boolean z, String str, ClassLoader classLoader) {
        this.bJG = false;
        this.bJH = null;
        this.packageName = str;
        this.bJF = classLoader;
        this.bJG = true;
    }

    public NativeJavaPackage(String str, ClassLoader classLoader) {
        this(false, str, classLoader);
    }

    public NativeJavaPackage(String str) {
        this(false, str, org.mozilla1.javascript.Context.bwq().getApplicationClassLoader());
    }

    public String getClassName() {
        return "JavaPackage";
    }

    public boolean has(String str, org.mozilla1.javascript.Scriptable avf) {
        return true;
    }

    public boolean has(int i, org.mozilla1.javascript.Scriptable avf) {
        return false;
    }

    public void put(String str, org.mozilla1.javascript.Scriptable avf, Object obj) {
    }

    public void put(int i, org.mozilla1.javascript.Scriptable avf, Object obj) {
        throw org.mozilla1.javascript.Context.m35244gE("msg.pkg.int");
    }

    public Object get(String str, org.mozilla1.javascript.Scriptable avf) {
        return mo23161a(str, avf, true);
    }

    public Object get(int i, org.mozilla1.javascript.Scriptable avf) {
        return NOT_FOUND;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo23162a(String str, org.mozilla1.javascript.Scriptable avf) {
        int i;
        String str2;
        NativeJavaPackage yhVar;
        int indexOf = str.indexOf(46);
        if (indexOf == -1) {
            i = str.length();
        } else {
            i = indexOf;
        }
        String substring = str.substring(0, i);
        Object obj = super.get(substring, (org.mozilla1.javascript.Scriptable) this);
        if (obj == null || !(obj instanceof NativeJavaPackage)) {
            if (this.packageName.length() == 0) {
                str2 = substring;
            } else {
                str2 = String.valueOf(this.packageName) + "." + substring;
            }
            apy();
            NativeJavaPackage yhVar2 = new NativeJavaPackage(true, str2, this.bJF);
            ScriptRuntime.m7528a((ScriptableObject) yhVar2, avf);
            super.put(substring, (org.mozilla1.javascript.Scriptable) this, (Object) yhVar2);
            yhVar = yhVar2;
        } else {
            yhVar = (NativeJavaPackage) obj;
        }
        if (i < str.length()) {
            yhVar.mo23162a(str.substring(i + 1), avf);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Failed to insert additional move for type inference */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x006b  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.Object mo23161a(java.lang.String r5, Scriptable r6, boolean r7) {
        /*
            r4 = this;
            r1 = 0
            monitor-enter(r4)
            java.lang.Object r0 = super.get((java.lang.String) r5, (p001a.aVF) r6)     // Catch:{ all -> 0x006f }
            java.lang.Object r2 = NOT_FOUND     // Catch:{ all -> 0x006f }
            if (r0 == r2) goto L_0x000c
        L_0x000a:
            monitor-exit(r4)
            return r0
        L_0x000c:
            java.util.Set<java.lang.String> r0 = r4.bJH     // Catch:{ all -> 0x006f }
            if (r0 == 0) goto L_0x001a
            java.util.Set<java.lang.String> r0 = r4.bJH     // Catch:{ all -> 0x006f }
            boolean r0 = r0.contains(r5)     // Catch:{ all -> 0x006f }
            if (r0 == 0) goto L_0x001a
            r0 = r1
            goto L_0x000a
        L_0x001a:
            java.lang.String r0 = r4.packageName     // Catch:{ all -> 0x006f }
            int r0 = r0.length()     // Catch:{ all -> 0x006f }
            if (r0 != 0) goto L_0x0072
            r2 = r5
        L_0x0023:
            a.lh r0 = p001a.C2909lh.bwA()     // Catch:{ all -> 0x006f }
            a.Ib r0 = r0.bwx()     // Catch:{ all -> 0x006f }
            if (r0 == 0) goto L_0x0033
            boolean r0 = r0.visibleToScripts(r2)     // Catch:{ all -> 0x006f }
            if (r0 == 0) goto L_0x00a3
        L_0x0033:
            r4.apy()     // Catch:{ all -> 0x006f }
            java.lang.ClassLoader r0 = r4.bJF     // Catch:{ all -> 0x006f }
            if (r0 == 0) goto L_0x008d
            java.lang.ClassLoader r0 = r4.bJF     // Catch:{ all -> 0x006f }
            java.lang.Class r0 = p001a.C1520WN.classOrNull(r0, r2)     // Catch:{ all -> 0x006f }
        L_0x0040:
            if (r0 == 0) goto L_0x00a3
            a.GC r1 = new a.GC     // Catch:{ all -> 0x006f }
            a.aVF r3 = m23595x(r4)     // Catch:{ all -> 0x006f }
            r1.<init>(r3, r0)     // Catch:{ all -> 0x006f }
            a.aVF r0 = r4.getPrototype()     // Catch:{ all -> 0x006f }
            r1.setPrototype(r0)     // Catch:{ all -> 0x006f }
            r0 = r1
        L_0x0053:
            if (r0 != 0) goto L_0x0069
            if (r7 == 0) goto L_0x0092
            r4.apy()     // Catch:{ all -> 0x006f }
            a.yh r0 = new a.yh     // Catch:{ all -> 0x006f }
            r1 = 1
            java.lang.ClassLoader r3 = r4.bJF     // Catch:{ all -> 0x006f }
            r0.<init>(r1, r2, r3)     // Catch:{ all -> 0x006f }
            a.aVF r1 = r4.getParentScope()     // Catch:{ all -> 0x006f }
            p001a.C0903NH.m7528a((p001a.C6327akn) r0, (p001a.aVF) r1)     // Catch:{ all -> 0x006f }
        L_0x0069:
            if (r0 == 0) goto L_0x000a
            super.put((java.lang.String) r5, (p001a.aVF) r6, (java.lang.Object) r0)     // Catch:{ all -> 0x006f }
            goto L_0x000a
        L_0x006f:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0072:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x006f }
            java.lang.String r2 = r4.packageName     // Catch:{ all -> 0x006f }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ all -> 0x006f }
            r0.<init>(r2)     // Catch:{ all -> 0x006f }
            r2 = 46
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x006f }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ all -> 0x006f }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x006f }
            r2 = r0
            goto L_0x0023
        L_0x008d:
            java.lang.Class r0 = p001a.C1520WN.classOrNull(r2)     // Catch:{ all -> 0x006f }
            goto L_0x0040
        L_0x0092:
            java.util.Set<java.lang.String> r1 = r4.bJH     // Catch:{ all -> 0x006f }
            if (r1 != 0) goto L_0x009d
            java.util.HashSet r1 = new java.util.HashSet     // Catch:{ all -> 0x006f }
            r1.<init>()     // Catch:{ all -> 0x006f }
            r4.bJH = r1     // Catch:{ all -> 0x006f }
        L_0x009d:
            java.util.Set<java.lang.String> r1 = r4.bJH     // Catch:{ all -> 0x006f }
            r1.add(r5)     // Catch:{ all -> 0x006f }
            goto L_0x0069
        L_0x00a3:
            r0 = r1
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C4058yh.mo23161a(java.lang.String, a.aVF, boolean):java.lang.Object");
    }

    public Object getDefaultValue(Class<?> cls) {
        return toString();
    }

    public String toString() {
        return "[JavaPackage " + this.packageName + "]";
    }

    public boolean equals(Object obj) {
        apy();
        if (!(obj instanceof NativeJavaPackage)) {
            return false;
        }
        NativeJavaPackage yhVar = (NativeJavaPackage) obj;
        if (!this.packageName.equals(yhVar.packageName) || this.bJF != yhVar.bJF) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        apy();
        return (this.bJF == null ? 0 : this.bJF.hashCode()) ^ this.packageName.hashCode();
    }

    private void apy() {
        if (this.bJF == null && this.bJG) {
            this.bJF = Context.bwq().getApplicationClassLoader();
        }
    }
}
