package org.mozilla1.javascript;

/* renamed from: a.Al */
/* compiled from: a */
final class NativeMath extends IdScriptableObject {
    static final long serialVersionUID = -8838847185801131569L;
    private static final Object ceM = new Integer(18);
    private static final int ceN = 1;
    private static final int ceO = 2;
    private static final int ceP = 3;
    private static final int ceQ = 4;
    private static final int ceR = 5;
    private static final int ceS = 6;
    private static final int ceT = 7;
    private static final int ceU = 8;
    private static final int ceV = 9;
    private static final int ceW = 10;
    private static final int ceX = 11;
    private static final int ceY = 12;
    private static final int ceZ = 13;
    private static final int cfa = 14;
    private static final int cfb = 15;
    private static final int cfc = 16;
    private static final int cfd = 17;
    private static final int cfe = 18;
    private static final int cff = 19;
    private static final int cfg = 19;
    private static final int cfh = 20;
    private static final int cfi = 21;
    private static final int cfj = 22;
    private static final int cfk = 23;
    private static final int cfl = 24;
    private static final int cfm = 25;
    private static final int cfn = 26;
    private static final int cfo = 27;
    private static final int cfp = 27;

    private NativeMath() {
    }

    /* renamed from: a */
    static void m472a(org.mozilla1.javascript.Scriptable avf, boolean z) {
        NativeMath al = new NativeMath();
        al.activatePrototypeMap(27);
        al.setPrototype(ScriptableObject.m23593v(avf));
        al.setParentScope(avf);
        if (z) {
            al.sealObject();
        }
        ScriptableObject.m23567a(avf, "Math", (Object) al, 2);
    }

    public String getClassName() {
        return "Math";
    }

    /* access modifiers changed from: protected */
    public void initPrototypeId(int i) {
        double d;
        String str;
        String str2;
        int i2 = 0;
        if (i <= 19) {
            switch (i) {
                case 1:
                    str2 = "toSource";
                    break;
                case 2:
                    i2 = 1;
                    str2 = "abs";
                    break;
                case 3:
                    i2 = 1;
                    str2 = "acos";
                    break;
                case 4:
                    i2 = 1;
                    str2 = "asin";
                    break;
                case 5:
                    i2 = 1;
                    str2 = "atan";
                    break;
                case 6:
                    i2 = 2;
                    str2 = "atan2";
                    break;
                case 7:
                    i2 = 1;
                    str2 = "ceil";
                    break;
                case 8:
                    i2 = 1;
                    str2 = "cos";
                    break;
                case 9:
                    i2 = 1;
                    str2 = "exp";
                    break;
                case 10:
                    i2 = 1;
                    str2 = "floor";
                    break;
                case 11:
                    i2 = 1;
                    str2 = "log";
                    break;
                case 12:
                    i2 = 2;
                    str2 = "max";
                    break;
                case 13:
                    i2 = 2;
                    str2 = "min";
                    break;
                case 14:
                    i2 = 2;
                    str2 = "pow";
                    break;
                case 15:
                    str2 = "random";
                    break;
                case 16:
                    i2 = 1;
                    str2 = "round";
                    break;
                case 17:
                    i2 = 1;
                    str2 = "sin";
                    break;
                case 18:
                    i2 = 1;
                    str2 = "sqrt";
                    break;
                case 19:
                    i2 = 1;
                    str2 = "tan";
                    break;
                default:
                    throw new IllegalStateException(String.valueOf(i));
            }
            initPrototypeMethod(ceM, i, str2, i2);
            return;
        }
        switch (i) {
            case 20:
                d = 2.718281828459045d;
                str = "E";
                break;
            case 21:
                d = 3.141592653589793d;
                str = "PI";
                break;
            case 22:
                d = 2.302585092994046d;
                str = "LN10";
                break;
            case 23:
                d = 0.6931471805599453d;
                str = "LN2";
                break;
            case 24:
                d = 1.4426950408889634d;
                str = "LOG2E";
                break;
            case 25:
                d = 0.4342944819032518d;
                str = "LOG10E";
                break;
            case 26:
                d = 0.7071067811865476d;
                str = "SQRT1_2";
                break;
            case 27:
                d = 1.4142135623730951d;
                str = "SQRT2";
                break;
            default:
                throw new IllegalStateException(String.valueOf(i));
        }
        initPrototypeValue(i, str, org.mozilla1.javascript.ScriptRuntime.wrapNumber(d), 7);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0127, code lost:
        if (r2 == p001a.C0903NH.NaN) goto L_0x0129;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object execIdCall(IdFunctionObject r11, Context r12, org.mozilla1.javascript.Scriptable r13, Scriptable r14, java.lang.Object[] r15) {
        /*
            r10 = this;
            r2 = 9221120237041090560(0x7ff8000000000000, double:NaN)
            r6 = 9218868437227405312(0x7ff0000000000000, double:Infinity)
            r4 = -4503599627370496(0xfff0000000000000, double:-Infinity)
            r0 = 0
            r8 = 0
            java.lang.Object r9 = ceM
            boolean r9 = r11.hasTag(r9)
            if (r9 != 0) goto L_0x0016
            java.lang.Object r0 = super.execIdCall(r11, r12, r13, r14, r15)
        L_0x0015:
            return r0
        L_0x0016:
            int r9 = r11.methodId()
            switch(r9) {
                case 1: goto L_0x0027;
                case 2: goto L_0x002a;
                case 3: goto L_0x003f;
                case 4: goto L_0x003f;
                case 5: goto L_0x0062;
                case 6: goto L_0x006b;
                case 7: goto L_0x0079;
                case 8: goto L_0x0082;
                case 9: goto L_0x0095;
                case 10: goto L_0x00a8;
                case 11: goto L_0x00b1;
                case 12: goto L_0x00c1;
                case 13: goto L_0x00c1;
                case 14: goto L_0x00eb;
                case 15: goto L_0x00fa;
                case 16: goto L_0x0100;
                case 17: goto L_0x012c;
                case 18: goto L_0x0140;
                case 19: goto L_0x014a;
                default: goto L_0x001d;
            }
        L_0x001d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = java.lang.String.valueOf(r9)
            r0.<init>(r1)
            throw r0
        L_0x0027:
            java.lang.String r0 = "Math"
            goto L_0x0015
        L_0x002a:
            double r2 = p001a.C0903NH.toNumber(r15, r8)
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r4 != 0) goto L_0x0037
        L_0x0032:
            java.lang.Number r0 = p001a.C0903NH.wrapNumber(r0)
            goto L_0x0015
        L_0x0037:
            int r0 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x003d
            double r0 = -r2
            goto L_0x0032
        L_0x003d:
            r0 = r2
            goto L_0x0032
        L_0x003f:
            double r0 = p001a.C0903NH.toNumber(r15, r8)
            int r4 = (r0 > r0 ? 1 : (r0 == r0 ? 0 : -1))
            if (r4 != 0) goto L_0x0060
            r4 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            int r4 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r4 > 0) goto L_0x0060
            r4 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r4 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r4 > 0) goto L_0x0060
            r2 = 3
            if (r9 != r2) goto L_0x005b
            double r0 = java.lang.Math.acos(r0)
            goto L_0x0032
        L_0x005b:
            double r0 = java.lang.Math.asin(r0)
            goto L_0x0032
        L_0x0060:
            r0 = r2
            goto L_0x0032
        L_0x0062:
            double r0 = p001a.C0903NH.toNumber(r15, r8)
            double r0 = java.lang.Math.atan(r0)
            goto L_0x0032
        L_0x006b:
            double r0 = p001a.C0903NH.toNumber(r15, r8)
            r2 = 1
            double r2 = p001a.C0903NH.toNumber(r15, r2)
            double r0 = java.lang.Math.atan2(r0, r2)
            goto L_0x0032
        L_0x0079:
            double r0 = p001a.C0903NH.toNumber(r15, r8)
            double r0 = java.lang.Math.ceil(r0)
            goto L_0x0032
        L_0x0082:
            double r0 = p001a.C0903NH.toNumber(r15, r8)
            int r6 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r6 == 0) goto L_0x008e
            int r4 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x0090
        L_0x008e:
            r0 = r2
            goto L_0x0032
        L_0x0090:
            double r0 = java.lang.Math.cos(r0)
            goto L_0x0032
        L_0x0095:
            double r2 = p001a.C0903NH.toNumber(r15, r8)
            int r6 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r6 != 0) goto L_0x009f
            r0 = r2
            goto L_0x0032
        L_0x009f:
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 == 0) goto L_0x0032
            double r0 = java.lang.Math.exp(r2)
            goto L_0x0032
        L_0x00a8:
            double r0 = p001a.C0903NH.toNumber(r15, r8)
            double r0 = java.lang.Math.floor(r0)
            goto L_0x0032
        L_0x00b1:
            double r4 = p001a.C0903NH.toNumber(r15, r8)
            int r0 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x00bc
        L_0x00b9:
            r0 = r2
            goto L_0x0032
        L_0x00bc:
            double r2 = java.lang.Math.log(r4)
            goto L_0x00b9
        L_0x00c1:
            r0 = 12
            if (r9 != r0) goto L_0x00ce
            r0 = r4
        L_0x00c6:
            r2 = r8
            r4 = r0
        L_0x00c8:
            int r0 = r15.length
            if (r2 != r0) goto L_0x00d0
            r0 = r4
            goto L_0x0032
        L_0x00ce:
            r0 = r6
            goto L_0x00c6
        L_0x00d0:
            r0 = r15[r2]
            double r0 = p001a.C0903NH.toNumber((java.lang.Object) r0)
            int r3 = (r0 > r0 ? 1 : (r0 == r0 ? 0 : -1))
            if (r3 != 0) goto L_0x0032
            r3 = 12
            if (r9 != r3) goto L_0x00e6
            double r4 = java.lang.Math.max(r4, r0)
        L_0x00e2:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x00c8
        L_0x00e6:
            double r4 = java.lang.Math.min(r4, r0)
            goto L_0x00e2
        L_0x00eb:
            double r0 = p001a.C0903NH.toNumber(r15, r8)
            r2 = 1
            double r2 = p001a.C0903NH.toNumber(r15, r2)
            double r0 = r10.m473c(r0, r2)
            goto L_0x0032
        L_0x00fa:
            double r0 = java.lang.Math.random()
            goto L_0x0032
        L_0x0100:
            double r2 = p001a.C0903NH.toNumber(r15, r8)
            int r8 = (r2 > r2 ? 1 : (r2 == r2 ? 0 : -1))
            if (r8 != 0) goto L_0x0129
            int r6 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r6 == 0) goto L_0x0129
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 == 0) goto L_0x0129
            long r4 = java.lang.Math.round(r2)
            r6 = 0
            int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r6 == 0) goto L_0x011d
            double r0 = (double) r4
            goto L_0x0032
        L_0x011d:
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r4 >= 0) goto L_0x0125
            double r0 = p001a.C0903NH.negativeZero
            goto L_0x0032
        L_0x0125:
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r4 != 0) goto L_0x0032
        L_0x0129:
            r0 = r2
            goto L_0x0032
        L_0x012c:
            double r0 = p001a.C0903NH.toNumber(r15, r8)
            int r6 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r6 == 0) goto L_0x0138
            int r4 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x013b
        L_0x0138:
            r0 = r2
            goto L_0x0032
        L_0x013b:
            double r2 = java.lang.Math.sin(r0)
            goto L_0x0138
        L_0x0140:
            double r0 = p001a.C0903NH.toNumber(r15, r8)
            double r0 = java.lang.Math.sqrt(r0)
            goto L_0x0032
        L_0x014a:
            double r0 = p001a.C0903NH.toNumber(r15, r8)
            double r0 = java.lang.Math.tan(r0)
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: NativeMath.execIdCall(a.Yy, a.lh, a.aVF, a.aVF, java.lang.Object[]):java.lang.Object");
    }

    /* renamed from: c */
    private double m473c(double d, double d2) {
        if (d2 != d2) {
            return d2;
        }
        if (d2 == org.mozilla1.javascript.ScriptRuntime.NaN) {
            return 1.0d;
        }
        if (d != org.mozilla1.javascript.ScriptRuntime.NaN) {
            double pow = Math.pow(d, d2);
            if (pow == pow) {
                return pow;
            }
            if (d2 == Double.POSITIVE_INFINITY) {
                if (d < -1.0d || 1.0d < d) {
                    return Double.POSITIVE_INFINITY;
                }
                if (-1.0d >= d || d >= 1.0d) {
                    return pow;
                }
                return org.mozilla1.javascript.ScriptRuntime.NaN;
            } else if (d2 == Double.NEGATIVE_INFINITY) {
                if (d < -1.0d || 1.0d < d) {
                    return org.mozilla1.javascript.ScriptRuntime.NaN;
                }
                if (-1.0d >= d || d >= 1.0d) {
                    return pow;
                }
                return Double.POSITIVE_INFINITY;
            } else if (d == Double.POSITIVE_INFINITY) {
                if (d2 > org.mozilla1.javascript.ScriptRuntime.NaN) {
                    return Double.POSITIVE_INFINITY;
                }
                return org.mozilla1.javascript.ScriptRuntime.NaN;
            } else if (d != Double.NEGATIVE_INFINITY) {
                return pow;
            } else {
                long j = (long) d2;
                if (((double) j) == d2 && (j & 1) != 0) {
                    return d2 > org.mozilla1.javascript.ScriptRuntime.NaN ? Double.NEGATIVE_INFINITY : -0.0d;
                }
                if (d2 > org.mozilla1.javascript.ScriptRuntime.NaN) {
                    return Double.POSITIVE_INFINITY;
                }
                return org.mozilla1.javascript.ScriptRuntime.NaN;
            }
        } else if (1.0d / d <= org.mozilla1.javascript.ScriptRuntime.NaN) {
            long j2 = (long) d2;
            if (((double) j2) == d2 && (j2 & 1) != 0) {
                return d2 > org.mozilla1.javascript.ScriptRuntime.NaN ? -0.0d : Double.NEGATIVE_INFINITY;
            }
            if (d2 > org.mozilla1.javascript.ScriptRuntime.NaN) {
                return org.mozilla1.javascript.ScriptRuntime.NaN;
            }
            return Double.POSITIVE_INFINITY;
        } else if (d2 > org.mozilla1.javascript.ScriptRuntime.NaN) {
            return ScriptRuntime.NaN;
        } else {
            return Double.POSITIVE_INFINITY;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int findPrototypeId(java.lang.String r8) {
        /*
            r7 = this;
            r6 = 111(0x6f, float:1.56E-43)
            r5 = 110(0x6e, float:1.54E-43)
            r1 = 0
            r0 = 2
            r3 = 1
            r2 = 0
            int r4 = r8.length()
            switch(r4) {
                case 1: goto L_0x001c;
                case 2: goto L_0x0027;
                case 3: goto L_0x003a;
                case 4: goto L_0x00f4;
                case 5: goto L_0x011e;
                case 6: goto L_0x0145;
                case 7: goto L_0x015d;
                case 8: goto L_0x0163;
                default: goto L_0x000f;
            }
        L_0x000f:
            r0 = r1
        L_0x0010:
            if (r2 == 0) goto L_0x001b
            if (r2 == r8) goto L_0x001b
            boolean r2 = r2.equals(r8)
            if (r2 != 0) goto L_0x001b
            r0 = r1
        L_0x001b:
            return r0
        L_0x001c:
            char r0 = r8.charAt(r1)
            r3 = 69
            if (r0 != r3) goto L_0x000f
            r0 = 20
            goto L_0x001b
        L_0x0027:
            char r0 = r8.charAt(r1)
            r4 = 80
            if (r0 != r4) goto L_0x000f
            char r0 = r8.charAt(r3)
            r3 = 73
            if (r0 != r3) goto L_0x000f
            r0 = 21
            goto L_0x001b
        L_0x003a:
            char r4 = r8.charAt(r1)
            switch(r4) {
                case 76: goto L_0x0043;
                case 97: goto L_0x0056;
                case 99: goto L_0x0067;
                case 101: goto L_0x0078;
                case 108: goto L_0x008b;
                case 109: goto L_0x009c;
                case 112: goto L_0x00be;
                case 115: goto L_0x00d0;
                case 116: goto L_0x00e2;
                default: goto L_0x0041;
            }
        L_0x0041:
            r0 = r1
            goto L_0x0010
        L_0x0043:
            char r0 = r8.charAt(r0)
            r4 = 50
            if (r0 != r4) goto L_0x000f
            char r0 = r8.charAt(r3)
            r3 = 78
            if (r0 != r3) goto L_0x000f
            r0 = 23
            goto L_0x001b
        L_0x0056:
            char r4 = r8.charAt(r0)
            r5 = 115(0x73, float:1.61E-43)
            if (r4 != r5) goto L_0x000f
            char r3 = r8.charAt(r3)
            r4 = 98
            if (r3 != r4) goto L_0x000f
            goto L_0x001b
        L_0x0067:
            char r0 = r8.charAt(r0)
            r4 = 115(0x73, float:1.61E-43)
            if (r0 != r4) goto L_0x000f
            char r0 = r8.charAt(r3)
            if (r0 != r6) goto L_0x000f
            r0 = 8
            goto L_0x001b
        L_0x0078:
            char r0 = r8.charAt(r0)
            r4 = 112(0x70, float:1.57E-43)
            if (r0 != r4) goto L_0x000f
            char r0 = r8.charAt(r3)
            r3 = 120(0x78, float:1.68E-43)
            if (r0 != r3) goto L_0x000f
            r0 = 9
            goto L_0x001b
        L_0x008b:
            char r0 = r8.charAt(r0)
            r4 = 103(0x67, float:1.44E-43)
            if (r0 != r4) goto L_0x000f
            char r0 = r8.charAt(r3)
            if (r0 != r6) goto L_0x000f
            r0 = 11
            goto L_0x001b
        L_0x009c:
            char r0 = r8.charAt(r0)
            if (r0 != r5) goto L_0x00ae
            char r0 = r8.charAt(r3)
            r3 = 105(0x69, float:1.47E-43)
            if (r0 != r3) goto L_0x000f
            r0 = 13
            goto L_0x001b
        L_0x00ae:
            r4 = 120(0x78, float:1.68E-43)
            if (r0 != r4) goto L_0x000f
            char r0 = r8.charAt(r3)
            r3 = 97
            if (r0 != r3) goto L_0x000f
            r0 = 12
            goto L_0x001b
        L_0x00be:
            char r0 = r8.charAt(r0)
            r4 = 119(0x77, float:1.67E-43)
            if (r0 != r4) goto L_0x000f
            char r0 = r8.charAt(r3)
            if (r0 != r6) goto L_0x000f
            r0 = 14
            goto L_0x001b
        L_0x00d0:
            char r0 = r8.charAt(r0)
            if (r0 != r5) goto L_0x000f
            char r0 = r8.charAt(r3)
            r3 = 105(0x69, float:1.47E-43)
            if (r0 != r3) goto L_0x000f
            r0 = 17
            goto L_0x001b
        L_0x00e2:
            char r0 = r8.charAt(r0)
            if (r0 != r5) goto L_0x000f
            char r0 = r8.charAt(r3)
            r3 = 97
            if (r0 != r3) goto L_0x000f
            r0 = 19
            goto L_0x001b
        L_0x00f4:
            char r0 = r8.charAt(r3)
            switch(r0) {
                case 78: goto L_0x00fe;
                case 99: goto L_0x0104;
                case 101: goto L_0x0109;
                case 113: goto L_0x010e;
                case 115: goto L_0x0114;
                case 116: goto L_0x0119;
                default: goto L_0x00fb;
            }
        L_0x00fb:
            r0 = r1
            goto L_0x0010
        L_0x00fe:
            java.lang.String r2 = "LN10"
            r0 = 22
            goto L_0x0010
        L_0x0104:
            java.lang.String r2 = "acos"
            r0 = 3
            goto L_0x0010
        L_0x0109:
            java.lang.String r2 = "ceil"
            r0 = 7
            goto L_0x0010
        L_0x010e:
            java.lang.String r2 = "sqrt"
            r0 = 18
            goto L_0x0010
        L_0x0114:
            java.lang.String r2 = "asin"
            r0 = 4
            goto L_0x0010
        L_0x0119:
            java.lang.String r2 = "atan"
            r0 = 5
            goto L_0x0010
        L_0x011e:
            char r0 = r8.charAt(r1)
            switch(r0) {
                case 76: goto L_0x0128;
                case 83: goto L_0x012e;
                case 97: goto L_0x0134;
                case 102: goto L_0x0139;
                case 114: goto L_0x013f;
                default: goto L_0x0125;
            }
        L_0x0125:
            r0 = r1
            goto L_0x0010
        L_0x0128:
            java.lang.String r2 = "LOG2E"
            r0 = 24
            goto L_0x0010
        L_0x012e:
            java.lang.String r2 = "SQRT2"
            r0 = 27
            goto L_0x0010
        L_0x0134:
            java.lang.String r2 = "atan2"
            r0 = 6
            goto L_0x0010
        L_0x0139:
            java.lang.String r2 = "floor"
            r0 = 10
            goto L_0x0010
        L_0x013f:
            java.lang.String r2 = "round"
            r0 = 16
            goto L_0x0010
        L_0x0145:
            char r0 = r8.charAt(r1)
            r3 = 76
            if (r0 != r3) goto L_0x0153
            java.lang.String r2 = "LOG10E"
            r0 = 25
            goto L_0x0010
        L_0x0153:
            r3 = 114(0x72, float:1.6E-43)
            if (r0 != r3) goto L_0x000f
            java.lang.String r2 = "random"
            r0 = 15
            goto L_0x0010
        L_0x015d:
            java.lang.String r2 = "SQRT1_2"
            r0 = 26
            goto L_0x0010
        L_0x0163:
            java.lang.String r2 = "toSource"
            r0 = r3
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: NativeMath.findPrototypeId(java.lang.String):int");
    }
}
