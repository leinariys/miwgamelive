package org.mozilla1.javascript;

import java.io.InputStream;
import java.io.Reader;
import java.lang.reflect.Method;
import java.util.Map;

/* renamed from: a.WN */
/* compiled from: a */
public class Kit {
    private static Method Throwable_initCause;

    static {
        Throwable_initCause = null;
        try {
            Class<?> classOrNull = classOrNull("java.lang.Throwable");
            Throwable_initCause = classOrNull.getMethod("initCause", new Class[]{classOrNull});
        } catch (Exception e) {
        }
    }

    public static Class<?> classOrNull(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException | IllegalArgumentException | LinkageError | SecurityException e) {
            return null;
        }
    }

    public static Class<?> classOrNull(ClassLoader classLoader, String str) {
        try {
            return classLoader.loadClass(str);
        } catch (ClassNotFoundException | IllegalArgumentException | LinkageError | SecurityException e) {
            return null;
        }
    }

    /* renamed from: as */
    static Object m11148as(Class<?> cls) {
        try {
            return cls.newInstance();
        } catch (IllegalAccessException | InstantiationException | LinkageError | SecurityException e) {
            return null;
        }
    }

    /* renamed from: b */
    static boolean m11149b(ClassLoader classLoader) {
        Class<?> cls = ScriptRuntime.ContextFactoryClass;
        if (classOrNull(classLoader, cls.getName()) != cls) {
            return false;
        }
        return true;
    }

    public static RuntimeException initCause(RuntimeException runtimeException, Throwable th) {
        if (Throwable_initCause != null) {
            try {
                Throwable_initCause.invoke(runtimeException, new Object[]{th});
            } catch (Exception e) {
            }
        }
        return runtimeException;
    }

    public static String[] semicolonSplit(String str) {
        String[] strArr = null;
        while (true) {
            int i = 0;
            int i2 = 0;
            while (true) {
                int indexOf = str.indexOf(59, i);
                if (indexOf < 0) {
                    break;
                }
                if (strArr != null) {
                    strArr[i2] = str.substring(i, indexOf);
                }
                i2++;
                i = indexOf + 1;
            }
            if (strArr != null) {
                return strArr;
            }
            if (i != str.length()) {
                throw new IllegalArgumentException();
            }
            strArr = new String[i2];
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0006, code lost:
        if (r0 >= 0) goto L_0x0008;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int xDigitToInt(int r2, int r3) {
        /*
            r0 = 57
            if (r2 > r0) goto L_0x000c
            int r0 = r2 + -48
            if (r0 < 0) goto L_0x0022
        L_0x0008:
            int r1 = r3 << 4
            r0 = r0 | r1
        L_0x000b:
            return r0
        L_0x000c:
            r0 = 70
            if (r2 > r0) goto L_0x0017
            r0 = 65
            if (r0 > r2) goto L_0x0022
            int r0 = r2 + -55
            goto L_0x0008
        L_0x0017:
            r0 = 102(0x66, float:1.43E-43)
            if (r2 > r0) goto L_0x0022
            r0 = 97
            if (r0 > r2) goto L_0x0022
            int r0 = r2 + -87
            goto L_0x0008
        L_0x0022:
            r0 = -1
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C1520WN.xDigitToInt(int, int):int");
    }

    public static Object addListener(Object obj, Object obj2) {
        if (obj2 == null) {
            throw new IllegalArgumentException();
        } else if (obj2 instanceof Object[]) {
            throw new IllegalArgumentException();
        } else if (obj == null) {
            return obj2;
        } else {
            if (!(obj instanceof Object[])) {
                return new Object[]{obj, obj2};
            }
            Object[] objArr = (Object[]) obj;
            int length = objArr.length;
            if (length < 2) {
                throw new IllegalArgumentException();
            }
            Object[] objArr2 = new Object[(length + 1)];
            System.arraycopy(objArr, 0, objArr2, 0, length);
            objArr2[length] = obj2;
            return objArr2;
        }
    }

    public static Object removeListener(Object obj, Object obj2) {
        if (obj2 == null) {
            throw new IllegalArgumentException();
        } else if (obj2 instanceof Object[]) {
            throw new IllegalArgumentException();
        } else if (obj == obj2) {
            return null;
        } else {
            if (!(obj instanceof Object[])) {
                return obj;
            }
            Object[] objArr = (Object[]) obj;
            int length = objArr.length;
            if (length < 2) {
                throw new IllegalArgumentException();
            } else if (length != 2) {
                int i = length;
                do {
                    i--;
                    if (objArr[i] == obj2) {
                        Object[] objArr2 = new Object[(length - 1)];
                        System.arraycopy(objArr, 0, objArr2, 0, i);
                        System.arraycopy(objArr, i + 1, objArr2, i, length - (i + 1));
                        return objArr2;
                    }
                } while (i != 0);
                return obj;
            } else if (objArr[1] == obj2) {
                return objArr[0];
            } else {
                if (objArr[0] == obj2) {
                    return objArr[1];
                }
                return obj;
            }
        }
    }

    public static Object getListener(Object obj, int i) {
        if (i == 0) {
            if (obj == null) {
                return null;
            }
            if (!(obj instanceof Object[])) {
                return obj;
            }
            Object[] objArr = (Object[]) obj;
            if (objArr.length >= 2) {
                return objArr[0];
            }
            throw new IllegalArgumentException();
        } else if (i != 1) {
            Object[] objArr2 = (Object[]) obj;
            int length = objArr2.length;
            if (length < 2) {
                throw new IllegalArgumentException();
            } else if (i == length) {
                return null;
            } else {
                return objArr2[i];
            }
        } else if (obj instanceof Object[]) {
            return ((Object[]) obj)[1];
        } else {
            if (obj != null) {
                return null;
            }
            throw new IllegalArgumentException();
        }
    }

    /* renamed from: a */
    static Object m11147a(Map<Object, Object> map, Object obj, Object obj2) {
        synchronized (map) {
            Object obj3 = map.get(obj);
            if (obj3 == null) {
                map.put(obj, obj2);
            } else {
                obj2 = obj3;
            }
        }
        return obj2;
    }

    public static Object makeHashKeyFromPair(Object obj, Object obj2) {
        if (obj == null) {
            throw new IllegalArgumentException();
        } else if (obj2 != null) {
            return new C1521a(obj, obj2);
        } else {
            throw new IllegalArgumentException();
        }
    }

    public static String readReader(Reader reader) {
        char[] cArr = new char[512];
        int i = 0;
        while (true) {
            int read = reader.read(cArr, i, cArr.length - i);
            if (read < 0) {
                return new String(cArr, 0, i);
            }
            i += read;
            if (i == cArr.length) {
                char[] cArr2 = new char[(cArr.length * 2)];
                System.arraycopy(cArr, 0, cArr2, 0, i);
                cArr = cArr2;
            }
        }
    }

    public static byte[] readStream(InputStream inputStream, int i) {
        if (i <= 0) {
            throw new IllegalArgumentException("Bad initialBufferCapacity: " + i);
        }
        byte[] bArr = new byte[i];
        int i2 = 0;
        while (true) {
            int read = inputStream.read(bArr, i2, bArr.length - i2);
            if (read < 0) {
                break;
            }
            i2 += read;
            if (i2 == bArr.length) {
                byte[] bArr2 = new byte[(bArr.length * 2)];
                System.arraycopy(bArr, 0, bArr2, 0, i2);
                bArr = bArr2;
            }
        }
        if (i2 == bArr.length) {
            return bArr;
        }
        byte[] bArr3 = new byte[i2];
        System.arraycopy(bArr, 0, bArr3, 0, i2);
        return bArr3;
    }

    public static RuntimeException codeBug() {
        IllegalStateException illegalStateException = new IllegalStateException("FAILED ASSERTION");
        illegalStateException.printStackTrace(System.err);
        throw illegalStateException;
    }

    /* renamed from: a.WN$a */
    private static final class C1521a {
        private Object ezs;
        private Object ezt;
        private int hash;

        C1521a(Object obj, Object obj2) {
            this.ezs = obj;
            this.ezt = obj2;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C1521a)) {
                return false;
            }
            C1521a aVar = (C1521a) obj;
            if (!this.ezs.equals(aVar.ezs) || !this.ezt.equals(aVar.ezt)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            if (this.hash == 0) {
                this.hash = this.ezs.hashCode() ^ this.ezt.hashCode();
            }
            return this.hash;
        }
    }
}
