package org.mozilla1.javascript;

import java.io.IOException;
import java.io.Reader;

/* renamed from: a.al */
/* compiled from: a */
public class TokenStream {

    /* renamed from: ik */
    private static final int f4817ik = -1;
    /* renamed from: is */
    private final int[] f4831is = new int[3];
    /* renamed from: im */
    String f4826im;
    /* renamed from: iA */
    private char[] f4818iA;
    /* renamed from: iB */
    private int f4819iB;
    /* renamed from: iC */
    private int f4820iC;
    /* renamed from: iD */
    private boolean f4821iD;
    /* renamed from: iE */
    private boolean f4822iE;
    /* renamed from: iF */
    private int f4823iF;
    /* renamed from: iG */
    private Parser f4824iG;
    /* renamed from: il */
    private boolean f4825il;
    /* renamed from: io */
    private double f4827io;
    /* renamed from: ip */
    private char[] f4828ip = new char[128];
    /* renamed from: iq */
    private int f4829iq;
    /* renamed from: ir */
    private ObjToIntMap f4830ir = new ObjToIntMap(50);
    /* renamed from: it */
    private int f4832it;

    /* renamed from: iu */
    private boolean f4833iu = false;

    /* renamed from: iv */
    private int f4834iv = 0;

    /* renamed from: iw */
    private int f4835iw;

    /* renamed from: ix */
    private int f4836ix = -1;

    /* renamed from: iy */
    private String f4837iy;

    /* renamed from: iz */
    private Reader f4838iz;
    private String string = "";

    TokenStream(Parser axf, Reader reader, String str, int i) {
        this.f4824iG = axf;
        this.f4835iw = i;
        if (reader != null) {
            if (str != null) {
                Kit.codeBug();
            }
            this.f4838iz = reader;
            this.f4818iA = new char[512];
            this.f4819iB = 0;
        } else {
            if (str == null) {
                Kit.codeBug();
            }
            this.f4837iy = str;
            this.f4819iB = str.length();
        }
        this.f4820iC = 0;
    }

    /* renamed from: m */
    static boolean m23668m(String str) {
        return m23669n(str) != 0;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:182:?, code lost:
        return 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:?, code lost:
        return r1 & 255;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x000f, code lost:
        r2 = null;
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0079, code lost:
        if (r8.charAt(1) == 'n') goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0011, code lost:
        if (r2 == null) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0013, code lost:
        if (r2 == r8) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0019, code lost:
        if (r2.equals(r8) != false) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0102, code lost:
        if (r8.charAt(1) == 'h') goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001b, code lost:
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0135, code lost:
        if (r8.charAt(1) == 'n') goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001c, code lost:
        if (r1 != 0) goto L_0x02c1;
     */
    /* renamed from: n */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int m23669n(java.lang.String r8) {
        /*
            r2 = 114(0x72, float:1.6E-43)
            r7 = 2
            r0 = 0
            r6 = 1
            r1 = 126(0x7e, float:1.77E-43)
            r3 = 0
            int r4 = r8.length()
            switch(r4) {
                case 2: goto L_0x001f;
                case 3: goto L_0x0050;
                case 4: goto L_0x00c7;
                case 5: goto L_0x0186;
                case 6: goto L_0x01f3;
                case 7: goto L_0x023f;
                case 8: goto L_0x0266;
                case 9: goto L_0x028b;
                case 10: goto L_0x02a7;
                case 11: goto L_0x000f;
                case 12: goto L_0x02bd;
                default: goto L_0x000f;
            }
        L_0x000f:
            r2 = r3
            r1 = r0
        L_0x0011:
            if (r2 == 0) goto L_0x001c
            if (r2 == r8) goto L_0x001c
            boolean r2 = r2.equals(r8)
            if (r2 != 0) goto L_0x001c
            r1 = r0
        L_0x001c:
            if (r1 != 0) goto L_0x02c1
        L_0x001e:
            return r0
        L_0x001f:
            char r1 = r8.charAt(r6)
            r2 = 102(0x66, float:1.43E-43)
            if (r1 != r2) goto L_0x0032
            char r1 = r8.charAt(r0)
            r2 = 105(0x69, float:1.47E-43)
            if (r1 != r2) goto L_0x000f
            r1 = 111(0x6f, float:1.56E-43)
            goto L_0x001c
        L_0x0032:
            r2 = 110(0x6e, float:1.54E-43)
            if (r1 != r2) goto L_0x0041
            char r1 = r8.charAt(r0)
            r2 = 105(0x69, float:1.47E-43)
            if (r1 != r2) goto L_0x000f
            r1 = 52
            goto L_0x001c
        L_0x0041:
            r2 = 111(0x6f, float:1.56E-43)
            if (r1 != r2) goto L_0x000f
            char r1 = r8.charAt(r0)
            r2 = 100
            if (r1 != r2) goto L_0x000f
            r1 = 117(0x75, float:1.64E-43)
            goto L_0x001c
        L_0x0050:
            char r4 = r8.charAt(r0)
            switch(r4) {
                case 102: goto L_0x005a;
                case 105: goto L_0x006b;
                case 108: goto L_0x007c;
                case 110: goto L_0x008f;
                case 116: goto L_0x00a3;
                case 118: goto L_0x00b5;
                default: goto L_0x0057;
            }
        L_0x0057:
            r2 = r3
            r1 = r0
            goto L_0x0011
        L_0x005a:
            char r1 = r8.charAt(r7)
            if (r1 != r2) goto L_0x000f
            char r1 = r8.charAt(r6)
            r2 = 111(0x6f, float:1.56E-43)
            if (r1 != r2) goto L_0x000f
            r1 = 118(0x76, float:1.65E-43)
            goto L_0x001c
        L_0x006b:
            char r2 = r8.charAt(r7)
            r4 = 116(0x74, float:1.63E-43)
            if (r2 != r4) goto L_0x000f
            char r2 = r8.charAt(r6)
            r4 = 110(0x6e, float:1.54E-43)
            if (r2 != r4) goto L_0x000f
            goto L_0x001c
        L_0x007c:
            char r1 = r8.charAt(r7)
            r2 = 116(0x74, float:1.63E-43)
            if (r1 != r2) goto L_0x000f
            char r1 = r8.charAt(r6)
            r2 = 101(0x65, float:1.42E-43)
            if (r1 != r2) goto L_0x000f
            r1 = 152(0x98, float:2.13E-43)
            goto L_0x001c
        L_0x008f:
            char r1 = r8.charAt(r7)
            r2 = 119(0x77, float:1.67E-43)
            if (r1 != r2) goto L_0x000f
            char r1 = r8.charAt(r6)
            r2 = 101(0x65, float:1.42E-43)
            if (r1 != r2) goto L_0x000f
            r1 = 30
            goto L_0x001c
        L_0x00a3:
            char r1 = r8.charAt(r7)
            r4 = 121(0x79, float:1.7E-43)
            if (r1 != r4) goto L_0x000f
            char r1 = r8.charAt(r6)
            if (r1 != r2) goto L_0x000f
            r1 = 80
            goto L_0x001c
        L_0x00b5:
            char r1 = r8.charAt(r7)
            if (r1 != r2) goto L_0x000f
            char r1 = r8.charAt(r6)
            r2 = 97
            if (r1 != r2) goto L_0x000f
            r1 = 121(0x79, float:1.7E-43)
            goto L_0x001c
        L_0x00c7:
            char r4 = r8.charAt(r0)
            switch(r4) {
                case 98: goto L_0x00d2;
                case 99: goto L_0x00d6;
                case 101: goto L_0x0106;
                case 103: goto L_0x0139;
                case 108: goto L_0x013d;
                case 110: goto L_0x0141;
                case 116: goto L_0x0147;
                case 118: goto L_0x017a;
                case 119: goto L_0x0180;
                default: goto L_0x00ce;
            }
        L_0x00ce:
            r2 = r3
            r1 = r0
            goto L_0x0011
        L_0x00d2:
            java.lang.String r2 = "byte"
            goto L_0x0011
        L_0x00d6:
            r4 = 3
            char r4 = r8.charAt(r4)
            r5 = 101(0x65, float:1.42E-43)
            if (r4 != r5) goto L_0x00f2
            char r1 = r8.charAt(r7)
            r4 = 115(0x73, float:1.61E-43)
            if (r1 != r4) goto L_0x000f
            char r1 = r8.charAt(r6)
            r4 = 97
            if (r1 != r4) goto L_0x000f
            r1 = r2
            goto L_0x001c
        L_0x00f2:
            if (r4 != r2) goto L_0x000f
            char r2 = r8.charAt(r7)
            r4 = 97
            if (r2 != r4) goto L_0x000f
            char r2 = r8.charAt(r6)
            r4 = 104(0x68, float:1.46E-43)
            if (r2 != r4) goto L_0x000f
            goto L_0x001c
        L_0x0106:
            r2 = 3
            char r2 = r8.charAt(r2)
            r4 = 101(0x65, float:1.42E-43)
            if (r2 != r4) goto L_0x0123
            char r1 = r8.charAt(r7)
            r2 = 115(0x73, float:1.61E-43)
            if (r1 != r2) goto L_0x000f
            char r1 = r8.charAt(r6)
            r2 = 108(0x6c, float:1.51E-43)
            if (r1 != r2) goto L_0x000f
            r1 = 112(0x70, float:1.57E-43)
            goto L_0x001c
        L_0x0123:
            r4 = 109(0x6d, float:1.53E-43)
            if (r2 != r4) goto L_0x000f
            char r2 = r8.charAt(r7)
            r4 = 117(0x75, float:1.64E-43)
            if (r2 != r4) goto L_0x000f
            char r2 = r8.charAt(r6)
            r4 = 110(0x6e, float:1.54E-43)
            if (r2 != r4) goto L_0x000f
            goto L_0x001c
        L_0x0139:
            java.lang.String r2 = "goto"
            goto L_0x0011
        L_0x013d:
            java.lang.String r2 = "long"
            goto L_0x0011
        L_0x0141:
            java.lang.String r2 = "null"
            r1 = 42
            goto L_0x0011
        L_0x0147:
            r1 = 3
            char r1 = r8.charAt(r1)
            r4 = 101(0x65, float:1.42E-43)
            if (r1 != r4) goto L_0x0162
            char r1 = r8.charAt(r7)
            r4 = 117(0x75, float:1.64E-43)
            if (r1 != r4) goto L_0x000f
            char r1 = r8.charAt(r6)
            if (r1 != r2) goto L_0x000f
            r1 = 45
            goto L_0x001c
        L_0x0162:
            r2 = 115(0x73, float:1.61E-43)
            if (r1 != r2) goto L_0x000f
            char r1 = r8.charAt(r7)
            r2 = 105(0x69, float:1.47E-43)
            if (r1 != r2) goto L_0x000f
            char r1 = r8.charAt(r6)
            r2 = 104(0x68, float:1.46E-43)
            if (r1 != r2) goto L_0x000f
            r1 = 43
            goto L_0x001c
        L_0x017a:
            java.lang.String r2 = "void"
            r1 = 125(0x7d, float:1.75E-43)
            goto L_0x0011
        L_0x0180:
            java.lang.String r2 = "with"
            r1 = 122(0x7a, float:1.71E-43)
            goto L_0x0011
        L_0x0186:
            char r2 = r8.charAt(r7)
            switch(r2) {
                case 97: goto L_0x0191;
                case 98: goto L_0x018d;
                case 99: goto L_0x018d;
                case 100: goto L_0x018d;
                case 101: goto L_0x0195;
                case 102: goto L_0x018d;
                case 103: goto L_0x018d;
                case 104: goto L_0x018d;
                case 105: goto L_0x01ad;
                case 106: goto L_0x018d;
                case 107: goto L_0x018d;
                case 108: goto L_0x01b3;
                case 109: goto L_0x018d;
                case 110: goto L_0x01b9;
                case 111: goto L_0x01cf;
                case 112: goto L_0x01e3;
                case 113: goto L_0x018d;
                case 114: goto L_0x01e7;
                case 115: goto L_0x018d;
                case 116: goto L_0x01ed;
                default: goto L_0x018d;
            }
        L_0x018d:
            r2 = r3
            r1 = r0
            goto L_0x0011
        L_0x0191:
            java.lang.String r2 = "class"
            goto L_0x0011
        L_0x0195:
            char r1 = r8.charAt(r0)
            r2 = 98
            if (r1 != r2) goto L_0x01a3
            java.lang.String r2 = "break"
            r1 = 119(0x77, float:1.67E-43)
            goto L_0x0011
        L_0x01a3:
            r2 = 121(0x79, float:1.7E-43)
            if (r1 != r2) goto L_0x000f
            java.lang.String r2 = "yield"
            r1 = 72
            goto L_0x0011
        L_0x01ad:
            java.lang.String r2 = "while"
            r1 = 116(0x74, float:1.63E-43)
            goto L_0x0011
        L_0x01b3:
            java.lang.String r2 = "false"
            r1 = 44
            goto L_0x0011
        L_0x01b9:
            char r2 = r8.charAt(r0)
            r4 = 99
            if (r2 != r4) goto L_0x01c7
            java.lang.String r2 = "const"
            r1 = 153(0x99, float:2.14E-43)
            goto L_0x0011
        L_0x01c7:
            r4 = 102(0x66, float:1.43E-43)
            if (r2 != r4) goto L_0x000f
            java.lang.String r2 = "final"
            goto L_0x0011
        L_0x01cf:
            char r2 = r8.charAt(r0)
            r4 = 102(0x66, float:1.43E-43)
            if (r2 != r4) goto L_0x01db
            java.lang.String r2 = "float"
            goto L_0x0011
        L_0x01db:
            r4 = 115(0x73, float:1.61E-43)
            if (r2 != r4) goto L_0x000f
            java.lang.String r2 = "short"
            goto L_0x0011
        L_0x01e3:
            java.lang.String r2 = "super"
            goto L_0x0011
        L_0x01e7:
            java.lang.String r2 = "throw"
            r1 = 50
            goto L_0x0011
        L_0x01ed:
            java.lang.String r2 = "catch"
            r1 = 123(0x7b, float:1.72E-43)
            goto L_0x0011
        L_0x01f3:
            char r4 = r8.charAt(r6)
            switch(r4) {
                case 97: goto L_0x01fe;
                case 101: goto L_0x0202;
                case 104: goto L_0x0217;
                case 109: goto L_0x021b;
                case 111: goto L_0x0221;
                case 116: goto L_0x0225;
                case 117: goto L_0x0229;
                case 119: goto L_0x022d;
                case 120: goto L_0x0233;
                case 121: goto L_0x0239;
                default: goto L_0x01fa;
            }
        L_0x01fa:
            r2 = r3
            r1 = r0
            goto L_0x0011
        L_0x01fe:
            java.lang.String r2 = "native"
            goto L_0x0011
        L_0x0202:
            char r1 = r8.charAt(r0)
            r4 = 100
            if (r1 != r4) goto L_0x0210
            java.lang.String r2 = "delete"
            r1 = 31
            goto L_0x0011
        L_0x0210:
            if (r1 != r2) goto L_0x000f
            java.lang.String r2 = "return"
            r1 = 4
            goto L_0x0011
        L_0x0217:
            java.lang.String r2 = "throws"
            goto L_0x0011
        L_0x021b:
            java.lang.String r2 = "import"
            r1 = 110(0x6e, float:1.54E-43)
            goto L_0x0011
        L_0x0221:
            java.lang.String r2 = "double"
            goto L_0x0011
        L_0x0225:
            java.lang.String r2 = "static"
            goto L_0x0011
        L_0x0229:
            java.lang.String r2 = "public"
            goto L_0x0011
        L_0x022d:
            java.lang.String r2 = "switch"
            r1 = 113(0x71, float:1.58E-43)
            goto L_0x0011
        L_0x0233:
            java.lang.String r2 = "export"
            r1 = 109(0x6d, float:1.53E-43)
            goto L_0x0011
        L_0x0239:
            java.lang.String r2 = "typeof"
            r1 = 32
            goto L_0x0011
        L_0x023f:
            char r2 = r8.charAt(r6)
            switch(r2) {
                case 97: goto L_0x024a;
                case 101: goto L_0x024e;
                case 105: goto L_0x0254;
                case 111: goto L_0x025a;
                case 114: goto L_0x025e;
                case 120: goto L_0x0262;
                default: goto L_0x0246;
            }
        L_0x0246:
            r2 = r3
            r1 = r0
            goto L_0x0011
        L_0x024a:
            java.lang.String r2 = "package"
            goto L_0x0011
        L_0x024e:
            java.lang.String r2 = "default"
            r1 = 115(0x73, float:1.61E-43)
            goto L_0x0011
        L_0x0254:
            java.lang.String r2 = "finally"
            r1 = 124(0x7c, float:1.74E-43)
            goto L_0x0011
        L_0x025a:
            java.lang.String r2 = "boolean"
            goto L_0x0011
        L_0x025e:
            java.lang.String r2 = "private"
            goto L_0x0011
        L_0x0262:
            java.lang.String r2 = "extends"
            goto L_0x0011
        L_0x0266:
            char r2 = r8.charAt(r0)
            switch(r2) {
                case 97: goto L_0x0271;
                case 99: goto L_0x0275;
                case 100: goto L_0x027b;
                case 102: goto L_0x0281;
                case 118: goto L_0x0287;
                default: goto L_0x026d;
            }
        L_0x026d:
            r2 = r3
            r1 = r0
            goto L_0x0011
        L_0x0271:
            java.lang.String r2 = "abstract"
            goto L_0x0011
        L_0x0275:
            java.lang.String r2 = "continue"
            r1 = 120(0x78, float:1.68E-43)
            goto L_0x0011
        L_0x027b:
            java.lang.String r2 = "debugger"
            r1 = 159(0x9f, float:2.23E-43)
            goto L_0x0011
        L_0x0281:
            java.lang.String r2 = "function"
            r1 = 108(0x6c, float:1.51E-43)
            goto L_0x0011
        L_0x0287:
            java.lang.String r2 = "volatile"
            goto L_0x0011
        L_0x028b:
            char r2 = r8.charAt(r0)
            r4 = 105(0x69, float:1.47E-43)
            if (r2 != r4) goto L_0x0297
            java.lang.String r2 = "interface"
            goto L_0x0011
        L_0x0297:
            r4 = 112(0x70, float:1.57E-43)
            if (r2 != r4) goto L_0x029f
            java.lang.String r2 = "protected"
            goto L_0x0011
        L_0x029f:
            r4 = 116(0x74, float:1.63E-43)
            if (r2 != r4) goto L_0x000f
            java.lang.String r2 = "transient"
            goto L_0x0011
        L_0x02a7:
            char r2 = r8.charAt(r6)
            r4 = 109(0x6d, float:1.53E-43)
            if (r2 != r4) goto L_0x02b3
            java.lang.String r2 = "implements"
            goto L_0x0011
        L_0x02b3:
            r1 = 110(0x6e, float:1.54E-43)
            if (r2 != r1) goto L_0x000f
            java.lang.String r2 = "instanceof"
            r1 = 53
            goto L_0x0011
        L_0x02bd:
            java.lang.String r2 = "synchronized"
            goto L_0x0011
        L_0x02c1:
            r0 = r1 & 255(0xff, float:3.57E-43)
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: TokenStream.m23669n(java.lang.String):int");
    }

    /* renamed from: q */
    private static boolean m23670q(int i) {
        if (i <= 90) {
            if (65 <= i) {
                return true;
            }
            return false;
        } else if (97 > i || i > 122) {
            return false;
        } else {
            return true;
        }
    }

    static boolean isDigit(int i) {
        return 48 <= i && i <= 57;
    }

    /* renamed from: r */
    static boolean m23671r(int i) {
        if (i <= 127) {
            if (i == 32 || i == 9 || i == 12 || i == 11) {
                return true;
            }
            return false;
        } else if (i == 160 || Character.getType((char) i) == 12) {
            return true;
        } else {
            return false;
        }
    }

    /* renamed from: s */
    private static boolean m23672s(int i) {
        return i > 127 && Character.getType((char) i) == 16;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: p */
    public String mo14632p(int i) {
        return "";
    }

    /* access modifiers changed from: package-private */
    public final int getLineno() {
        return this.f4835iw;
    }

    /* access modifiers changed from: package-private */
    public final String getString() {
        return this.string;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: dY */
    public final double mo14622dY() {
        return this.f4827io;
    }

    /* access modifiers changed from: package-private */
    public final boolean eof() {
        return this.f4833iu;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x01f6, code lost:
        if (r0 != r6) goto L_0x020a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x01f8, code lost:
        r8.string = (java.lang.String) r8.f4830ir.intern(m23663eh());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x020a, code lost:
        if (r0 == 10) goto L_0x020e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x020c, code lost:
        if (r0 != -1) goto L_0x021b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x020e, code lost:
        m23675w(r0);
        r8.f4824iG.mo16820kB("msg.unterminated.string.lit");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x021d, code lost:
        if (r0 != 92) goto L_0x025d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x021f, code lost:
        r0 = m23664ei();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0223, code lost:
        switch(r0) {
            case 10: goto L_0x02bf;
            case 98: goto L_0x0265;
            case 102: goto L_0x0268;
            case 110: goto L_0x026b;
            case 114: goto L_0x026d;
            case 116: goto L_0x0270;
            case 117: goto L_0x0276;
            case 118: goto L_0x0273;
            case 120: goto L_0x0298;
            default: goto L_0x0226;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0228, code lost:
        if (48 > r0) goto L_0x025d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x022c, code lost:
        if (r0 >= 56) goto L_0x025d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x022e, code lost:
        r0 = r0 - 48;
        r2 = m23664ei();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x0236, code lost:
        if (48 > r2) goto L_0x025a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x023a, code lost:
        if (r2 >= 56) goto L_0x025a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x023c, code lost:
        r0 = ((r0 * 8) + r2) - 48;
        r2 = m23664ei();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0247, code lost:
        if (48 > r2) goto L_0x025a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x024b, code lost:
        if (r2 >= 56) goto L_0x025a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x024f, code lost:
        if (r0 > 31) goto L_0x025a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x0251, code lost:
        r0 = ((r0 * 8) + r2) - 48;
        r2 = m23664ei();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x025a, code lost:
        m23675w(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x025d, code lost:
        m23674v(r0);
        r0 = m23664ei();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x0265, code lost:
        r0 = 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x0268, code lost:
        r0 = 12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x026b, code lost:
        r0 = 10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x026d, code lost:
        r0 = 13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x0270, code lost:
        r0 = 9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x0273, code lost:
        r0 = 11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x0276, code lost:
        r7 = r8.f4829iq;
        m23674v(117);
        r2 = 0;
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x0280, code lost:
        if (r2 != 4) goto L_0x0285;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x0282, code lost:
        r8.f4829iq = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x0285, code lost:
        r3 = m23664ei();
        r0 = p001a.C1520WN.xDigitToInt(r3, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x028d, code lost:
        if (r0 >= 0) goto L_0x0292;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x028f, code lost:
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x0292, code lost:
        m23674v(r3);
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x0298, code lost:
        r2 = m23664ei();
        r0 = p001a.C1520WN.xDigitToInt(r2, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x02a0, code lost:
        if (r0 >= 0) goto L_0x02aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x02a2, code lost:
        m23674v(120);
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x02aa, code lost:
        r3 = m23664ei();
        r0 = p001a.C1520WN.xDigitToInt(r3, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:184:0x02b2, code lost:
        if (r0 >= 0) goto L_0x025d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x02b4, code lost:
        m23674v(120);
        m23674v(r2);
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x02bf, code lost:
        r0 = m23664ei();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:249:0x03b5, code lost:
        m23677y(33);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:251:0x03c0, code lost:
        if (m23676x(60) == false) goto L_0x03d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:253:0x03c6, code lost:
        if (m23676x(61) == false) goto L_0x03cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:254:0x03c8, code lost:
        return 93;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:255:0x03cc, code lost:
        return 18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:257:0x03d4, code lost:
        if (m23676x(61) == false) goto L_0x03da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:258:0x03d6, code lost:
        return 15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:259:0x03da, code lost:
        return 14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:315:0x049e, code lost:
        r8.f4825il = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:415:?, code lost:
        return 41;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:416:?, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:420:?, code lost:
        return r0;
     */
    /* renamed from: dZ */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int mo14623dZ() {
        /*
            r8 = this;
            r1 = 10
            r3 = 1
            r5 = -1
            r7 = 61
            r4 = 0
        L_0x0007:
            int r6 = r8.m23664ei()
            if (r6 != r5) goto L_0x000e
        L_0x000d:
            return r4
        L_0x000e:
            if (r6 != r1) goto L_0x0014
            r8.f4825il = r4
            r4 = r3
            goto L_0x000d
        L_0x0014:
            boolean r0 = m23671r(r6)
            if (r0 != 0) goto L_0x0007
            r0 = 45
            if (r6 == r0) goto L_0x0020
            r8.f4825il = r3
        L_0x0020:
            r0 = 64
            if (r6 != r0) goto L_0x0027
            r4 = 146(0x92, float:2.05E-43)
            goto L_0x000d
        L_0x0027:
            r0 = 92
            if (r6 != r0) goto L_0x0055
            int r6 = r8.m23664ei()
            r0 = 117(0x75, float:1.64E-43)
            if (r6 != r0) goto L_0x004d
            r8.f4829iq = r4
            r0 = r3
            r2 = r3
        L_0x0037:
            if (r2 == 0) goto L_0x00e8
            r2 = r0
            r1 = r0
        L_0x003b:
            if (r1 == 0) goto L_0x0075
            r1 = r4
            r0 = r4
        L_0x003f:
            r6 = 4
            if (r1 != r6) goto L_0x0063
        L_0x0042:
            if (r0 >= 0) goto L_0x0070
            a.axf r0 = r8.f4824iG
            java.lang.String r1 = "msg.invalid.escape"
            r0.mo16820kB(r1)
            r4 = r5
            goto L_0x000d
        L_0x004d:
            r8.m23675w(r6)
            r6 = 92
            r0 = r4
            r2 = r4
            goto L_0x0037
        L_0x0055:
            char r0 = (char) r6
            boolean r2 = java.lang.Character.isJavaIdentifierStart(r0)
            if (r2 == 0) goto L_0x0061
            r8.f4829iq = r4
            r8.m23674v(r6)
        L_0x0061:
            r0 = r4
            goto L_0x0037
        L_0x0063:
            int r6 = r8.m23664ei()
            int r0 = p001a.C1520WN.xDigitToInt(r6, r0)
            if (r0 < 0) goto L_0x0042
            int r1 = r1 + 1
            goto L_0x003f
        L_0x0070:
            r8.m23674v(r0)
            r1 = r4
            goto L_0x003b
        L_0x0075:
            int r0 = r8.m23664ei()
            r6 = 92
            if (r0 != r6) goto L_0x0092
            int r0 = r8.m23664ei()
            r1 = 117(0x75, float:1.64E-43)
            if (r0 != r1) goto L_0x0088
            r2 = r3
            r1 = r3
            goto L_0x003b
        L_0x0088:
            a.axf r0 = r8.f4824iG
            java.lang.String r1 = "msg.illegal.character"
            r0.mo16820kB(r1)
            r4 = r5
            goto L_0x000d
        L_0x0092:
            if (r0 == r5) goto L_0x009b
            char r6 = (char) r0
            boolean r6 = java.lang.Character.isJavaIdentifierPart(r6)
            if (r6 != 0) goto L_0x00e3
        L_0x009b:
            r8.m23675w(r0)
            java.lang.String r0 = r8.m23663eh()
            if (r2 != 0) goto L_0x00d5
            int r4 = m23669n(r0)
            if (r4 == 0) goto L_0x00d5
            r1 = 152(0x98, float:2.13E-43)
            if (r4 == r1) goto L_0x00b2
            r1 = 72
            if (r4 != r1) goto L_0x00c0
        L_0x00b2:
            a.axf r1 = r8.f4824iG
            a.aKI r1 = r1.f5571he
            int r1 = r1.getLanguageVersion()
            r2 = 170(0xaa, float:2.38E-43)
            if (r1 >= r2) goto L_0x00c0
            r4 = 39
        L_0x00c0:
            r1 = 126(0x7e, float:1.77E-43)
            if (r4 != r1) goto L_0x000d
            a.axf r1 = r8.f4824iG
            a.aKI r1 = r1.f5571he
            boolean r1 = r1.isReservedKeywordAsIdentifier()
            if (r1 == 0) goto L_0x000d
            a.axf r1 = r8.f4824iG
            java.lang.String r2 = "msg.reserved.keyword"
            r1.mo16813aB(r2, r0)
        L_0x00d5:
            a.uS r1 = r8.f4830ir
            java.lang.Object r0 = r1.intern(r0)
            java.lang.String r0 = (java.lang.String) r0
            r8.string = r0
            r4 = 39
            goto L_0x000d
        L_0x00e3:
            r8.m23674v(r0)
            goto L_0x003b
        L_0x00e8:
            boolean r0 = isDigit(r6)
            if (r0 != 0) goto L_0x00fc
            r0 = 46
            if (r6 != r0) goto L_0x01e8
            int r0 = r8.peekChar()
            boolean r0 = isDigit(r0)
            if (r0 == 0) goto L_0x01e8
        L_0x00fc:
            r8.f4829iq = r4
            r0 = 48
            if (r6 != r0) goto L_0x04cb
            int r0 = r8.m23664ei()
            r2 = 120(0x78, float:1.68E-43)
            if (r0 == r2) goto L_0x010e
            r2 = 88
            if (r0 != r2) goto L_0x016c
        L_0x010e:
            r2 = 16
            int r0 = r8.m23664ei()
        L_0x0114:
            r6 = 16
            if (r2 != r6) goto L_0x04c8
        L_0x0118:
            int r6 = p001a.C1520WN.xDigitToInt(r0, r4)
            if (r6 >= 0) goto L_0x017c
            r6 = r2
        L_0x011f:
            if (r6 != r1) goto L_0x04c2
            r2 = 46
            if (r0 == r2) goto L_0x012d
            r2 = 101(0x65, float:1.42E-43)
            if (r0 == r2) goto L_0x012d
            r2 = 69
            if (r0 != r2) goto L_0x04c2
        L_0x012d:
            r2 = 46
            if (r0 != r2) goto L_0x013e
        L_0x0131:
            r8.m23674v(r0)
            int r0 = r8.m23664ei()
            boolean r2 = isDigit(r0)
            if (r2 != 0) goto L_0x0131
        L_0x013e:
            r2 = 101(0x65, float:1.42E-43)
            if (r0 == r2) goto L_0x0146
            r2 = 69
            if (r0 != r2) goto L_0x01be
        L_0x0146:
            r8.m23674v(r0)
            int r0 = r8.m23664ei()
            r2 = 43
            if (r0 == r2) goto L_0x0155
            r2 = 45
            if (r0 != r2) goto L_0x015c
        L_0x0155:
            r8.m23674v(r0)
            int r0 = r8.m23664ei()
        L_0x015c:
            boolean r2 = isDigit(r0)
            if (r2 != 0) goto L_0x01b1
            a.axf r0 = r8.f4824iG
            java.lang.String r1 = "msg.missing.exponent"
            r0.mo16820kB(r1)
            r4 = r5
            goto L_0x000d
        L_0x016c:
            boolean r2 = isDigit(r0)
            if (r2 == 0) goto L_0x0175
            r2 = 8
            goto L_0x0114
        L_0x0175:
            r2 = 48
            r8.m23674v(r2)
            r2 = r1
            goto L_0x0114
        L_0x017c:
            r8.m23674v(r0)
            int r0 = r8.m23664ei()
            goto L_0x0118
        L_0x0184:
            r0 = 8
            if (r2 != r0) goto L_0x04c5
            r0 = 56
            if (r7 < r0) goto L_0x04c5
            a.axf r2 = r8.f4824iG
            java.lang.String r6 = "msg.bad.octal.literal"
            r0 = 56
            if (r7 != r0) goto L_0x01ae
            java.lang.String r0 = "8"
        L_0x0196:
            r2.mo16813aB(r6, r0)
            r0 = r1
        L_0x019a:
            r8.m23674v(r7)
            int r7 = r8.m23664ei()
            r2 = r0
        L_0x01a2:
            r0 = 48
            if (r0 > r7) goto L_0x01aa
            r0 = 57
            if (r7 <= r0) goto L_0x0184
        L_0x01aa:
            r6 = r2
            r0 = r7
            goto L_0x011f
        L_0x01ae:
            java.lang.String r0 = "9"
            goto L_0x0196
        L_0x01b1:
            r8.m23674v(r0)
            int r0 = r8.m23664ei()
            boolean r2 = isDigit(r0)
            if (r2 != 0) goto L_0x01b1
        L_0x01be:
            r2 = r4
        L_0x01bf:
            r8.m23675w(r0)
            java.lang.String r0 = r8.m23663eh()
            if (r6 != r1) goto L_0x01e3
            if (r2 != 0) goto L_0x01e3
            java.lang.Double r0 = java.lang.Double.valueOf(r0)     // Catch:{ NumberFormatException -> 0x01d8 }
            double r0 = r0.doubleValue()     // Catch:{ NumberFormatException -> 0x01d8 }
        L_0x01d2:
            r8.f4827io = r0
            r4 = 40
            goto L_0x000d
        L_0x01d8:
            r0 = move-exception
            a.axf r0 = r8.f4824iG
            java.lang.String r1 = "msg.caught.nfe"
            r0.mo16820kB(r1)
            r4 = r5
            goto L_0x000d
        L_0x01e3:
            double r0 = p001a.C0903NH.m7577f((java.lang.String) r0, (int) r4, (int) r6)
            goto L_0x01d2
        L_0x01e8:
            r0 = 34
            if (r6 == r0) goto L_0x01f0
            r0 = 39
            if (r6 != r0) goto L_0x02c5
        L_0x01f0:
            r8.f4829iq = r4
            int r0 = r8.m23664ei()
        L_0x01f6:
            if (r0 != r6) goto L_0x020a
            java.lang.String r0 = r8.m23663eh()
            a.uS r1 = r8.f4830ir
            java.lang.Object r0 = r1.intern(r0)
            java.lang.String r0 = (java.lang.String) r0
            r8.string = r0
            r4 = 41
            goto L_0x000d
        L_0x020a:
            if (r0 == r1) goto L_0x020e
            if (r0 != r5) goto L_0x021b
        L_0x020e:
            r8.m23675w(r0)
            a.axf r0 = r8.f4824iG
            java.lang.String r1 = "msg.unterminated.string.lit"
            r0.mo16820kB(r1)
            r4 = r5
            goto L_0x000d
        L_0x021b:
            r2 = 92
            if (r0 != r2) goto L_0x025d
            int r0 = r8.m23664ei()
            switch(r0) {
                case 10: goto L_0x02bf;
                case 98: goto L_0x0265;
                case 102: goto L_0x0268;
                case 110: goto L_0x026b;
                case 114: goto L_0x026d;
                case 116: goto L_0x0270;
                case 117: goto L_0x0276;
                case 118: goto L_0x0273;
                case 120: goto L_0x0298;
                default: goto L_0x0226;
            }
        L_0x0226:
            r2 = 48
            if (r2 > r0) goto L_0x025d
            r2 = 56
            if (r0 >= r2) goto L_0x025d
            int r0 = r0 + -48
            int r2 = r8.m23664ei()
            r3 = 48
            if (r3 > r2) goto L_0x025a
            r3 = 56
            if (r2 >= r3) goto L_0x025a
            int r0 = r0 * 8
            int r0 = r0 + r2
            int r0 = r0 + -48
            int r2 = r8.m23664ei()
            r3 = 48
            if (r3 > r2) goto L_0x025a
            r3 = 56
            if (r2 >= r3) goto L_0x025a
            r3 = 31
            if (r0 > r3) goto L_0x025a
            int r0 = r0 * 8
            int r0 = r0 + r2
            int r0 = r0 + -48
            int r2 = r8.m23664ei()
        L_0x025a:
            r8.m23675w(r2)
        L_0x025d:
            r8.m23674v(r0)
            int r0 = r8.m23664ei()
            goto L_0x01f6
        L_0x0265:
            r0 = 8
            goto L_0x025d
        L_0x0268:
            r0 = 12
            goto L_0x025d
        L_0x026b:
            r0 = r1
            goto L_0x025d
        L_0x026d:
            r0 = 13
            goto L_0x025d
        L_0x0270:
            r0 = 9
            goto L_0x025d
        L_0x0273:
            r0 = 11
            goto L_0x025d
        L_0x0276:
            int r7 = r8.f4829iq
            r0 = 117(0x75, float:1.64E-43)
            r8.m23674v(r0)
            r2 = r4
            r0 = r4
        L_0x027f:
            r3 = 4
            if (r2 != r3) goto L_0x0285
            r8.f4829iq = r7
            goto L_0x025d
        L_0x0285:
            int r3 = r8.m23664ei()
            int r0 = p001a.C1520WN.xDigitToInt(r3, r0)
            if (r0 >= 0) goto L_0x0292
            r0 = r3
            goto L_0x01f6
        L_0x0292:
            r8.m23674v(r3)
            int r2 = r2 + 1
            goto L_0x027f
        L_0x0298:
            int r2 = r8.m23664ei()
            int r0 = p001a.C1520WN.xDigitToInt(r2, r4)
            if (r0 >= 0) goto L_0x02aa
            r0 = 120(0x78, float:1.68E-43)
            r8.m23674v(r0)
            r0 = r2
            goto L_0x01f6
        L_0x02aa:
            int r3 = r8.m23664ei()
            int r0 = p001a.C1520WN.xDigitToInt(r3, r0)
            if (r0 >= 0) goto L_0x025d
            r0 = 120(0x78, float:1.68E-43)
            r8.m23674v(r0)
            r8.m23674v(r2)
            r0 = r3
            goto L_0x01f6
        L_0x02bf:
            int r0 = r8.m23664ei()
            goto L_0x01f6
        L_0x02c5:
            switch(r6) {
                case 33: goto L_0x037b;
                case 37: goto L_0x046a;
                case 38: goto L_0x0349;
                case 40: goto L_0x02e6;
                case 41: goto L_0x02ea;
                case 42: goto L_0x0418;
                case 43: goto L_0x047c;
                case 44: goto L_0x02ee;
                case 45: goto L_0x0496;
                case 46: goto L_0x0306;
                case 47: goto L_0x0426;
                case 58: goto L_0x02f6;
                case 59: goto L_0x02d2;
                case 60: goto L_0x0393;
                case 61: goto L_0x0363;
                case 62: goto L_0x03de;
                case 63: goto L_0x02f2;
                case 91: goto L_0x02d6;
                case 93: goto L_0x02da;
                case 94: goto L_0x033c;
                case 123: goto L_0x02de;
                case 124: goto L_0x0322;
                case 125: goto L_0x02e2;
                case 126: goto L_0x0478;
                default: goto L_0x02c8;
            }
        L_0x02c8:
            a.axf r0 = r8.f4824iG
            java.lang.String r1 = "msg.illegal.character"
            r0.mo16820kB(r1)
            r4 = r5
            goto L_0x000d
        L_0x02d2:
            r4 = 81
            goto L_0x000d
        L_0x02d6:
            r4 = 82
            goto L_0x000d
        L_0x02da:
            r4 = 83
            goto L_0x000d
        L_0x02de:
            r4 = 84
            goto L_0x000d
        L_0x02e2:
            r4 = 85
            goto L_0x000d
        L_0x02e6:
            r4 = 86
            goto L_0x000d
        L_0x02ea:
            r4 = 87
            goto L_0x000d
        L_0x02ee:
            r4 = 88
            goto L_0x000d
        L_0x02f2:
            r4 = 101(0x65, float:1.42E-43)
            goto L_0x000d
        L_0x02f6:
            r0 = 58
            boolean r0 = r8.m23676x(r0)
            if (r0 == 0) goto L_0x0302
            r4 = 143(0x8f, float:2.0E-43)
            goto L_0x000d
        L_0x0302:
            r4 = 102(0x66, float:1.43E-43)
            goto L_0x000d
        L_0x0306:
            r0 = 46
            boolean r0 = r8.m23676x(r0)
            if (r0 == 0) goto L_0x0312
            r4 = 142(0x8e, float:1.99E-43)
            goto L_0x000d
        L_0x0312:
            r0 = 40
            boolean r0 = r8.m23676x(r0)
            if (r0 == 0) goto L_0x031e
            r4 = 145(0x91, float:2.03E-43)
            goto L_0x000d
        L_0x031e:
            r4 = 107(0x6b, float:1.5E-43)
            goto L_0x000d
        L_0x0322:
            r0 = 124(0x7c, float:1.74E-43)
            boolean r0 = r8.m23676x(r0)
            if (r0 == 0) goto L_0x032e
            r4 = 103(0x67, float:1.44E-43)
            goto L_0x000d
        L_0x032e:
            boolean r0 = r8.m23676x(r7)
            if (r0 == 0) goto L_0x0338
            r4 = 90
            goto L_0x000d
        L_0x0338:
            r4 = 9
            goto L_0x000d
        L_0x033c:
            boolean r0 = r8.m23676x(r7)
            if (r0 == 0) goto L_0x0346
            r4 = 91
            goto L_0x000d
        L_0x0346:
            r4 = r1
            goto L_0x000d
        L_0x0349:
            r0 = 38
            boolean r0 = r8.m23676x(r0)
            if (r0 == 0) goto L_0x0355
            r4 = 104(0x68, float:1.46E-43)
            goto L_0x000d
        L_0x0355:
            boolean r0 = r8.m23676x(r7)
            if (r0 == 0) goto L_0x035f
            r4 = 92
            goto L_0x000d
        L_0x035f:
            r4 = 11
            goto L_0x000d
        L_0x0363:
            boolean r0 = r8.m23676x(r7)
            if (r0 == 0) goto L_0x0377
            boolean r0 = r8.m23676x(r7)
            if (r0 == 0) goto L_0x0373
            r4 = 46
            goto L_0x000d
        L_0x0373:
            r4 = 12
            goto L_0x000d
        L_0x0377:
            r4 = 89
            goto L_0x000d
        L_0x037b:
            boolean r0 = r8.m23676x(r7)
            if (r0 == 0) goto L_0x038f
            boolean r0 = r8.m23676x(r7)
            if (r0 == 0) goto L_0x038b
            r4 = 47
            goto L_0x000d
        L_0x038b:
            r4 = 13
            goto L_0x000d
        L_0x038f:
            r4 = 26
            goto L_0x000d
        L_0x0393:
            r0 = 33
            boolean r0 = r8.m23676x(r0)
            if (r0 == 0) goto L_0x03ba
            r0 = 45
            boolean r0 = r8.m23676x(r0)
            if (r0 == 0) goto L_0x03b5
            r0 = 45
            boolean r0 = r8.m23676x(r0)
            if (r0 == 0) goto L_0x03b0
            r8.m23666ek()
            goto L_0x0007
        L_0x03b0:
            r0 = 45
            r8.m23677y(r0)
        L_0x03b5:
            r0 = 33
            r8.m23677y(r0)
        L_0x03ba:
            r0 = 60
            boolean r0 = r8.m23676x(r0)
            if (r0 == 0) goto L_0x03d0
            boolean r0 = r8.m23676x(r7)
            if (r0 == 0) goto L_0x03cc
            r4 = 93
            goto L_0x000d
        L_0x03cc:
            r4 = 18
            goto L_0x000d
        L_0x03d0:
            boolean r0 = r8.m23676x(r7)
            if (r0 == 0) goto L_0x03da
            r4 = 15
            goto L_0x000d
        L_0x03da:
            r4 = 14
            goto L_0x000d
        L_0x03de:
            r0 = 62
            boolean r0 = r8.m23676x(r0)
            if (r0 == 0) goto L_0x040a
            r0 = 62
            boolean r0 = r8.m23676x(r0)
            if (r0 == 0) goto L_0x03fc
            boolean r0 = r8.m23676x(r7)
            if (r0 == 0) goto L_0x03f8
            r4 = 95
            goto L_0x000d
        L_0x03f8:
            r4 = 20
            goto L_0x000d
        L_0x03fc:
            boolean r0 = r8.m23676x(r7)
            if (r0 == 0) goto L_0x0406
            r4 = 94
            goto L_0x000d
        L_0x0406:
            r4 = 19
            goto L_0x000d
        L_0x040a:
            boolean r0 = r8.m23676x(r7)
            if (r0 == 0) goto L_0x0414
            r4 = 17
            goto L_0x000d
        L_0x0414:
            r4 = 16
            goto L_0x000d
        L_0x0418:
            boolean r0 = r8.m23676x(r7)
            if (r0 == 0) goto L_0x0422
            r4 = 98
            goto L_0x000d
        L_0x0422:
            r4 = 23
            goto L_0x000d
        L_0x0426:
            r0 = 47
            boolean r0 = r8.m23676x(r0)
            if (r0 == 0) goto L_0x0433
            r8.m23666ek()
            goto L_0x0007
        L_0x0433:
            r0 = 42
            boolean r0 = r8.m23676x(r0)
            if (r0 == 0) goto L_0x045c
            r0 = r4
        L_0x043c:
            int r2 = r8.m23664ei()
            if (r2 != r5) goto L_0x044c
            a.axf r0 = r8.f4824iG
            java.lang.String r1 = "msg.unterminated.comment"
            r0.mo16820kB(r1)
            r4 = r5
            goto L_0x000d
        L_0x044c:
            r6 = 42
            if (r2 != r6) goto L_0x0452
            r0 = r3
            goto L_0x043c
        L_0x0452:
            r6 = 47
            if (r2 != r6) goto L_0x045a
            if (r0 == 0) goto L_0x043c
            goto L_0x0007
        L_0x045a:
            r0 = r4
            goto L_0x043c
        L_0x045c:
            boolean r0 = r8.m23676x(r7)
            if (r0 == 0) goto L_0x0466
            r4 = 99
            goto L_0x000d
        L_0x0466:
            r4 = 24
            goto L_0x000d
        L_0x046a:
            boolean r0 = r8.m23676x(r7)
            if (r0 == 0) goto L_0x0474
            r4 = 100
            goto L_0x000d
        L_0x0474:
            r4 = 25
            goto L_0x000d
        L_0x0478:
            r4 = 27
            goto L_0x000d
        L_0x047c:
            boolean r0 = r8.m23676x(r7)
            if (r0 == 0) goto L_0x0486
            r4 = 96
            goto L_0x000d
        L_0x0486:
            r0 = 43
            boolean r0 = r8.m23676x(r0)
            if (r0 == 0) goto L_0x0492
            r4 = 105(0x69, float:1.47E-43)
            goto L_0x000d
        L_0x0492:
            r4 = 21
            goto L_0x000d
        L_0x0496:
            boolean r0 = r8.m23676x(r7)
            if (r0 == 0) goto L_0x04a3
            r0 = 97
        L_0x049e:
            r8.f4825il = r3
            r4 = r0
            goto L_0x000d
        L_0x04a3:
            r0 = 45
            boolean r0 = r8.m23676x(r0)
            if (r0 == 0) goto L_0x04bf
            boolean r0 = r8.f4825il
            if (r0 != 0) goto L_0x04bc
            r0 = 62
            boolean r0 = r8.m23676x(r0)
            if (r0 == 0) goto L_0x04bc
            r8.m23666ek()
            goto L_0x0007
        L_0x04bc:
            r0 = 106(0x6a, float:1.49E-43)
            goto L_0x049e
        L_0x04bf:
            r0 = 22
            goto L_0x049e
        L_0x04c2:
            r2 = r3
            goto L_0x01bf
        L_0x04c5:
            r0 = r2
            goto L_0x019a
        L_0x04c8:
            r7 = r0
            goto L_0x01a2
        L_0x04cb:
            r2 = r1
            r0 = r6
            goto L_0x0114
        */
        throw new UnsupportedOperationException("Method not decompiled: TokenStream.mo14623dZ():int");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: t */
    public void mo14633t(int i) throws IOException {
        int ei;
        this.f4829iq = 0;
        if (i == 99) {
            m23674v(61);
        } else if (i != 24) {
            Kit.codeBug();
        }
        boolean z = false;
        while (true) {
            ei = m23664ei();
            if (ei == 47 && !z) {
                int i2 = this.f4829iq;
                while (true) {
                    if (!m23676x(103)) {
                        if (!m23676x(105)) {
                            if (!m23676x(109)) {
                                break;
                            }
                            m23674v(109);
                        } else {
                            m23674v(105);
                        }
                    } else {
                        m23674v(103);
                    }
                }
                if (m23670q(peekChar())) {
                    throw this.f4824iG.mo16821kC("msg.invalid.re.flag");
                }
                this.string = new String(this.f4828ip, 0, i2);
                this.f4826im = new String(this.f4828ip, i2, this.f4829iq - i2);
                return;
            } else if (ei == 10 || ei == -1) {
                m23675w(ei);
            } else {
                if (ei == 92) {
                    m23674v(ei);
                    ei = m23664ei();
                } else if (ei == 91) {
                    z = true;
                } else if (ei == 93) {
                    z = false;
                }
                m23674v(ei);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ea */
    public boolean mo14624ea() {
        return this.f4821iD;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: eb */
    public int mo14625eb() {
        this.f4823iF = 0;
        this.f4821iD = false;
        this.f4822iE = false;
        m23675w(60);
        return mo14626ec();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ec */
    public int mo14626ec() {
        this.f4829iq = 0;
        int ei = m23664ei();
        while (ei != -1) {
            if (!this.f4822iE) {
                switch (ei) {
                    case 60:
                        m23674v(ei);
                        switch (peekChar()) {
                            case 33:
                                m23674v(m23664ei());
                                switch (peekChar()) {
                                    case 45:
                                        m23674v(m23664ei());
                                        int ei2 = m23664ei();
                                        if (ei2 == 45) {
                                            m23674v(ei2);
                                            if (m23659ed()) {
                                                break;
                                            } else {
                                                return -1;
                                            }
                                        } else {
                                            this.f4829iq = 0;
                                            this.string = null;
                                            this.f4824iG.mo16820kB("msg.XML.bad.form");
                                            return -1;
                                        }
                                    case 91:
                                        m23674v(m23664ei());
                                        if (m23664ei() == 67 && m23664ei() == 68 && m23664ei() == 65 && m23664ei() == 84 && m23664ei() == 65 && m23664ei() == 91) {
                                            m23674v(67);
                                            m23674v(68);
                                            m23674v(65);
                                            m23674v(84);
                                            m23674v(65);
                                            m23674v(91);
                                            if (m23660ee()) {
                                                break;
                                            } else {
                                                return -1;
                                            }
                                        } else {
                                            this.f4829iq = 0;
                                            this.string = null;
                                            this.f4824iG.mo16820kB("msg.XML.bad.form");
                                            return -1;
                                        }
                                    default:
                                        if (m23661ef()) {
                                            break;
                                        } else {
                                            return -1;
                                        }
                                }
                            case 47:
                                m23674v(m23664ei());
                                if (this.f4823iF != 0) {
                                    this.f4822iE = true;
                                    this.f4823iF--;
                                    break;
                                } else {
                                    this.f4829iq = 0;
                                    this.string = null;
                                    this.f4824iG.mo16820kB("msg.XML.bad.form");
                                    return -1;
                                }
                            case 63:
                                m23674v(m23664ei());
                                if (m23662eg()) {
                                    break;
                                } else {
                                    return -1;
                                }
                            default:
                                this.f4822iE = true;
                                this.f4823iF++;
                                break;
                        }
                    case 123:
                        m23675w(ei);
                        this.string = m23663eh();
                        return 144;
                    default:
                        m23674v(ei);
                        break;
                }
            } else {
                switch (ei) {
                    case 9:
                    case 10:
                    case 13:
                    case 32:
                        m23674v(ei);
                        break;
                    case 34:
                    case 39:
                        m23674v(ei);
                        if (!m23673u(ei)) {
                            return -1;
                        }
                        break;
                    case 47:
                        m23674v(ei);
                        if (peekChar() == 62) {
                            m23674v(m23664ei());
                            this.f4822iE = false;
                            this.f4823iF--;
                            break;
                        }
                        break;
                    case 61:
                        m23674v(ei);
                        this.f4821iD = true;
                        break;
                    case 62:
                        m23674v(ei);
                        this.f4822iE = false;
                        this.f4821iD = false;
                        break;
                    case 123:
                        m23675w(ei);
                        this.string = m23663eh();
                        return 144;
                    default:
                        m23674v(ei);
                        this.f4821iD = false;
                        break;
                }
                if (!this.f4822iE && this.f4823iF == 0) {
                    this.string = m23663eh();
                    return 147;
                }
            }
            ei = m23664ei();
        }
        this.f4829iq = 0;
        this.string = null;
        this.f4824iG.mo16820kB("msg.XML.bad.form");
        return -1;
    }

    /* renamed from: u */
    private boolean m23673u(int i) {
        int ei = m23664ei();
        while (ei != -1) {
            m23674v(ei);
            if (ei == i) {
                return true;
            }
            ei = m23664ei();
        }
        this.f4829iq = 0;
        this.string = null;
        this.f4824iG.mo16820kB("msg.XML.bad.form");
        return false;
    }

    /* renamed from: ed */
    private boolean m23659ed() {
        int ei = m23664ei();
        while (ei != -1) {
            m23674v(ei);
            if (ei == 45 && peekChar() == 45) {
                ei = m23664ei();
                m23674v(ei);
                if (peekChar() == 62) {
                    m23674v(m23664ei());
                    return true;
                }
            } else {
                ei = m23664ei();
            }
        }
        this.f4829iq = 0;
        this.string = null;
        this.f4824iG.mo16820kB("msg.XML.bad.form");
        return false;
    }

    /* renamed from: ee */
    private boolean m23660ee() {
        int ei = m23664ei();
        while (ei != -1) {
            m23674v(ei);
            if (ei == 93 && peekChar() == 93) {
                ei = m23664ei();
                m23674v(ei);
                if (peekChar() == 62) {
                    m23674v(m23664ei());
                    return true;
                }
            } else {
                ei = m23664ei();
            }
        }
        this.f4829iq = 0;
        this.string = null;
        this.f4824iG.mo16820kB("msg.XML.bad.form");
        return false;
    }

    /* renamed from: ef */
    private boolean m23661ef() {
        int ei = m23664ei();
        int i = 1;
        while (ei != -1) {
            m23674v(ei);
            switch (ei) {
                case 60:
                    i++;
                    break;
                case 62:
                    i--;
                    if (i != 0) {
                        break;
                    } else {
                        return true;
                    }
            }
            ei = m23664ei();
        }
        this.f4829iq = 0;
        this.string = null;
        this.f4824iG.mo16820kB("msg.XML.bad.form");
        return false;
    }

    /* renamed from: eg */
    private boolean m23662eg() {
        int ei = m23664ei();
        while (ei != -1) {
            m23674v(ei);
            if (ei == 63 && peekChar() == 62) {
                m23674v(m23664ei());
                return true;
            }
            ei = m23664ei();
        }
        this.f4829iq = 0;
        this.string = null;
        this.f4824iG.mo16820kB("msg.XML.bad.form");
        return false;
    }

    /* renamed from: eh */
    private String m23663eh() {
        return new String(this.f4828ip, 0, this.f4829iq);
    }

    /* renamed from: v */
    private void m23674v(int i) {
        int i2 = this.f4829iq;
        if (i2 == this.f4828ip.length) {
            char[] cArr = new char[(this.f4828ip.length * 2)];
            System.arraycopy(this.f4828ip, 0, cArr, 0, i2);
            this.f4828ip = cArr;
        }
        this.f4828ip[i2] = (char) i;
        this.f4829iq = i2 + 1;
    }

    /* renamed from: w */
    private void m23675w(int i) {
        if (this.f4832it != 0 && this.f4831is[this.f4832it - 1] == 10) {
            Kit.codeBug();
        }
        int[] iArr = this.f4831is;
        int i2 = this.f4832it;
        this.f4832it = i2 + 1;
        iArr[i2] = i;
    }

    /* renamed from: x */
    private boolean m23676x(int i) throws IOException {
        int ej = m23665ej();
        if (ej == i) {
            return true;
        }
        m23677y(ej);
        return false;
    }

    private int peekChar() {
        int ei = m23664ei();
        m23675w(ei);
        return ei;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v19, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v20, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v21, resolved type: char} */
    /* JADX WARNING: CFG modification limit reached, blocks count: 139 */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0066, code lost:
        if (r0 > 127) goto L_0x0070;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0068, code lost:
        if (r0 == 10) goto L_0x006c;
        r0 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006a, code lost:
        if (r0 != 13) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006c, code lost:
        r7.f4836ix = r0;
        r0 = 10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0074, code lost:
        if (m23672s(r0) != false) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0076, code lost:
        r2 = p001a.C0903NH.isJSLineTerminator(r0);
        r0 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x007a, code lost:
        if (r2 == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x007c, code lost:
        r7.f4836ix = r0;
        r0 = 10;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: ei */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int m23664ei() {
        /*
            r7 = this;
            r6 = 13
            r5 = 1
            r2 = -1
            r1 = 10
            int r0 = r7.f4832it
            if (r0 == 0) goto L_0x002d
            int[] r0 = r7.f4831is
            int r1 = r7.f4832it
            int r1 = r1 + -1
            r7.f4832it = r1
            r0 = r0[r1]
        L_0x0014:
            return r0
        L_0x0015:
            java.lang.String r0 = r7.f4837iy
            int r3 = r7.f4820iC
            int r4 = r3 + 1
            r7.f4820iC = r4
            char r0 = r0.charAt(r3)
        L_0x0021:
            int r3 = r7.f4836ix
            if (r3 < 0) goto L_0x0064
            int r3 = r7.f4836ix
            if (r3 != r6) goto L_0x0056
            if (r0 != r1) goto L_0x0056
            r7.f4836ix = r1
        L_0x002d:
            java.lang.String r0 = r7.f4837iy
            if (r0 == 0) goto L_0x003b
            int r0 = r7.f4820iC
            int r3 = r7.f4819iB
            if (r0 != r3) goto L_0x0015
            r7.f4833iu = r5
            r0 = r2
            goto L_0x0014
        L_0x003b:
            int r0 = r7.f4820iC
            int r3 = r7.f4819iB
            if (r0 != r3) goto L_0x004b
            boolean r0 = r7.m23667em()
            if (r0 != 0) goto L_0x004b
            r7.f4833iu = r5
            r0 = r2
            goto L_0x0014
        L_0x004b:
            char[] r0 = r7.f4818iA
            int r3 = r7.f4820iC
            int r4 = r3 + 1
            r7.f4820iC = r4
            char r0 = r0[r3]
            goto L_0x0021
        L_0x0056:
            r7.f4836ix = r2
            int r3 = r7.f4820iC
            int r3 = r3 + -1
            r7.f4834iv = r3
            int r3 = r7.f4835iw
            int r3 = r3 + 1
            r7.f4835iw = r3
        L_0x0064:
            r3 = 127(0x7f, float:1.78E-43)
            if (r0 > r3) goto L_0x0070
            if (r0 == r1) goto L_0x006c
            if (r0 != r6) goto L_0x0014
        L_0x006c:
            r7.f4836ix = r0
            r0 = r1
            goto L_0x0014
        L_0x0070:
            boolean r3 = m23672s(r0)
            if (r3 != 0) goto L_0x002d
            boolean r2 = p001a.C0903NH.isJSLineTerminator(r0)
            if (r2 == 0) goto L_0x0014
            r7.f4836ix = r0
            r0 = r1
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: TokenStream.m23664ei():int");
    }

    /* renamed from: ej */
    private int m23665ej() throws IOException {
        char c;
        if (this.f4832it != 0) {
            int[] iArr = this.f4831is;
            int i = this.f4832it - 1;
            this.f4832it = i;
            return iArr[i];
        }
        do {
            if (this.f4837iy != null) {
                if (this.f4820iC == this.f4819iB) {
                    this.f4833iu = true;
                    return -1;
                }
                String str = this.f4837iy;
                int i2 = this.f4820iC;
                this.f4820iC = i2 + 1;
                c = str.charAt(i2);
            } else if (this.f4820iC != this.f4819iB || m23667em()) {
                char[] cArr = this.f4818iA;
                int i3 = this.f4820iC;
                this.f4820iC = i3 + 1;
                c = cArr[i3];
            } else {
                this.f4833iu = true;
                return -1;
            }
            if (c <= 127) {
                if (c != 10 && c != 13) {
                    return c;
                }
                this.f4836ix = c;
                return 10;
            }
        } while (m23672s(c));
        if (!ScriptRuntime.isJSLineTerminator(c)) {
            return c;
        }
        this.f4836ix = c;
        return 10;
    }

    /* renamed from: y */
    private void m23677y(int i) {
        int[] iArr = this.f4831is;
        int i2 = this.f4832it;
        this.f4832it = i2 + 1;
        iArr[i2] = i;
    }

    /*  JADX ERROR: StackOverflow in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* renamed from: ek */
    private void m23666ek() {
        /*
            r2 = this;
        L_0x0000:
            int r0 = r2.m23664ei()
            r1 = -1
            if (r0 == r1) goto L_0x000b
            r1 = 10
            if (r0 != r1) goto L_0x0000
        L_0x000b:
            r2.m23675w(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: TokenStream.m23666ek():void");
    }

    /* access modifiers changed from: package-private */
    public final int getOffset() {
        int i = this.f4820iC - this.f4834iv;
        if (this.f4836ix >= 0) {
            return i - 1;
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: el */
    public final String mo14627el() {
        if (this.f4837iy != null) {
            int i = this.f4820iC;
            if (this.f4836ix >= 0) {
                i--;
            } else {
                while (i != this.f4819iB && !ScriptRuntime.isJSLineTerminator(this.f4837iy.charAt(i))) {
                    i++;
                }
            }
            return this.f4837iy.substring(this.f4834iv, i);
        }
        int i2 = this.f4820iC - this.f4834iv;
        if (this.f4836ix >= 0) {
            i2--;
        } else {
            while (true) {
                int i3 = this.f4834iv + i2;
                if (i3 == this.f4819iB) {
                    try {
                        if (!m23667em()) {
                            break;
                        }
                        i3 = this.f4834iv + i2;
                    } catch (IOException e) {
                    }
                }
                if (ScriptRuntime.isJSLineTerminator(this.f4818iA[i3])) {
                    break;
                }
                i2++;
            }
        }
        return new String(this.f4818iA, this.f4834iv, i2);
    }

    /* renamed from: em */
    private boolean m23667em() throws IOException {
        if (this.f4837iy != null) {
            Kit.codeBug();
        }
        if (this.f4819iB == this.f4818iA.length) {
            if (this.f4834iv != 0) {
                System.arraycopy(this.f4818iA, this.f4834iv, this.f4818iA, 0, this.f4819iB - this.f4834iv);
                this.f4819iB -= this.f4834iv;
                this.f4820iC -= this.f4834iv;
                this.f4834iv = 0;
            } else {
                char[] cArr = new char[(this.f4818iA.length * 2)];
                System.arraycopy(this.f4818iA, 0, cArr, 0, this.f4819iB);
                this.f4818iA = cArr;
            }
        }
        int read = this.f4838iz.read(this.f4818iA, this.f4819iB, this.f4818iA.length - this.f4819iB);
        if (read < 0) {
            return false;
        }
        this.f4819iB += read;
        return true;
    }
}
