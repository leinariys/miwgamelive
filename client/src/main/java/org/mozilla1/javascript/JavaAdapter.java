package org.mozilla1.javascript;

import game.script.Character;
import logic.sql.C5878acG;
import org.mozilla1.classfile.C0147Bi;
import org.mozilla1.classfile.ClassFileWriter;
import p001a.C2821kb;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;


/* renamed from: a.QL */
/* compiled from: a */
public final class JavaAdapter implements IdFunctionCall {
    private static final Object cHe = new Integer(9);
    private static final int fXR = 1;

    public static void init(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, boolean z) {
        org.mozilla1.javascript.IdFunctionObject yy = new org.mozilla1.javascript.IdFunctionObject(new JavaAdapter(), cHe, 1, "JavaAdapter", 1, avf);
        yy.mo7318h((org.mozilla1.javascript.Scriptable) null);
        if (z) {
            yy.sealObject();
        }
        yy.exportAsScopeProperty();
    }

    /* renamed from: c */
    public static Object m8834c(Object obj, Class<?> cls) {
        if (obj != org.mozilla1.javascript.Undefined.instance || cls == org.mozilla1.javascript.ScriptRuntime.ObjectClass || cls == org.mozilla1.javascript.ScriptRuntime.StringClass) {
            return org.mozilla1.javascript.Context.jsToJava(obj, cls);
        }
        return null;
    }

    /* renamed from: a */
    public static org.mozilla1.javascript.Scriptable m8810a(org.mozilla1.javascript.Scriptable avf, Object obj) {
        org.mozilla1.javascript.NativeJavaObject eSVar = new NativeJavaObject(org.mozilla1.javascript.ScriptableObject.m23595x(avf), obj, (Class<?>) null, true);
        eSVar.setPrototype(avf);
        return eSVar;
    }

    /* renamed from: f */
    public static Object m8837f(Class<?> cls, Object obj) {
        return cls.getDeclaredField("self").get(obj);
    }

    /* renamed from: e */
    static Object m8836e(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr) {
        int i;
        int length = objArr.length;
        if (length == 0) {
            throw org.mozilla1.javascript.ScriptRuntime.m7600no("msg.adapter.zero.args");
        }
        Class<?> cls = null;
        Class[] clsArr = new Class[(length - 1)];
        int i2 = 0;
        int i3 = 0;
        while (i2 != length - 1) {
            org.mozilla1.javascript.NativeJavaClass gc = (org.mozilla1.javascript.NativeJavaClass) objArr[i2];
            if (!(gc instanceof NativeJavaClass)) {
                throw org.mozilla1.javascript.ScriptRuntime.m7610u("msg.not.java.class.arg", String.valueOf(i2), org.mozilla1.javascript.ScriptRuntime.toString((Object) gc));
            }
            Class<?> classObject = gc.getClassObject();
            if (classObject.isInterface()) {
                i = i3 + 1;
                clsArr[i3] = classObject;
                classObject = cls;
            } else if (cls != null) {
                throw org.mozilla1.javascript.ScriptRuntime.m7610u("msg.only.one.super", cls.getName(), classObject.getName());
            } else {
                i = i3;
            }
            i2++;
            i3 = i;
            cls = classObject;
        }
        if (cls == null) {
            cls = org.mozilla1.javascript.ScriptRuntime.ObjectClass;
        }
        Class[] clsArr2 = new Class[i3];
        System.arraycopy(clsArr, 0, clsArr2, 0, i3);
        org.mozilla1.javascript.Scriptable e = org.mozilla1.javascript.ScriptRuntime.m7572e(lhVar, avf, objArr[length - 1]);
        Class<?> a = m8811a(avf, cls, (Class<?>[]) clsArr2, e);
        try {
            return m8837f(a, a.getConstructor(new Class[]{org.mozilla1.javascript.ScriptRuntime.ContextFactoryClass, org.mozilla1.javascript.ScriptRuntime.ScriptableClass}).newInstance(new Object[]{lhVar.bwr(), e}));
        } catch (Exception e2) {
            throw org.mozilla1.javascript.Context.throwAsScriptRuntimeEx(e2);
        }
    }

    /* renamed from: b */
    public static void m8832b(Object obj, ObjectOutputStream objectOutputStream) {
        Class<?> cls = obj.getClass();
        objectOutputStream.writeObject(cls.getSuperclass().getName());
        Class[] interfaces = cls.getInterfaces();
        String[] strArr = new String[interfaces.length];
        for (int i = 0; i < interfaces.length; i++) {
            strArr[i] = interfaces[i].getName();
        }
        objectOutputStream.writeObject(strArr);
        try {
            objectOutputStream.writeObject(cls.getField("delegee").get(obj));
        } catch (IllegalAccessException | NoSuchFieldException e) {
            throw new IOException();
        }
    }

    /* renamed from: a */
    public static Object m8812a(org.mozilla1.javascript.Scriptable avf, ObjectInputStream objectInputStream) {
        org.mozilla1.javascript.ContextFactory iwVar;
        org.mozilla1.javascript.Context bwq = org.mozilla1.javascript.Context.bwq();
        if (bwq != null) {
            iwVar = bwq.bwr();
        } else {
            iwVar = null;
        }
        Class<?> cls = Class.forName((String) objectInputStream.readObject());
        String[] strArr = (String[]) objectInputStream.readObject();
        Class[] clsArr = new Class[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            clsArr[i] = Class.forName(strArr[i]);
        }
        org.mozilla1.javascript.Scriptable avf2 = (org.mozilla1.javascript.Scriptable) objectInputStream.readObject();
        try {
            return m8811a(avf, cls, (Class<?>[]) clsArr, avf2).getConstructor(new Class[]{org.mozilla1.javascript.ScriptRuntime.ContextFactoryClass, org.mozilla1.javascript.ScriptRuntime.ScriptableClass, org.mozilla1.javascript.ScriptRuntime.ScriptableClass}).newInstance(new Object[]{iwVar, avf2, avf});
        } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            throw new ClassNotFoundException("adapter");
        }
    }

    /* renamed from: l */
    private static org.mozilla1.javascript.ObjToIntMap m8838l(org.mozilla1.javascript.Scriptable avf) {
        Object[] y = org.mozilla1.javascript.ScriptableObject.m23596y(avf);
        org.mozilla1.javascript.ObjToIntMap uSVar = new org.mozilla1.javascript.ObjToIntMap(y.length);
        for (int i = 0; i != y.length; i++) {
            if (y[i] instanceof String) {
                String str = (String) y[i];
                Object j = org.mozilla1.javascript.ScriptableObject.m23587j(avf, str);
                if (j instanceof org.mozilla1.javascript.Function) {
                    int int32 = org.mozilla1.javascript.ScriptRuntime.toInt32(org.mozilla1.javascript.ScriptableObject.m23587j((org.mozilla1.javascript.Function) j, "length"));
                    if (int32 < 0) {
                        int32 = 0;
                    }
                    uSVar.put(str, int32);
                }
            }
        }
        return uSVar;
    }

    /* renamed from: a */
    private static Class<?> m8811a(org.mozilla1.javascript.Scriptable avf, Class<?> cls, Class<?>[] clsArr, org.mozilla1.javascript.Scriptable avf2) {
        org.mozilla1.javascript.ClassCache k = ClassCache.m23644k(avf);
        Map<JavaAdapterSignature, Class<?>> cgm = k.cgm();
        org.mozilla1.javascript.ObjToIntMap l = m8838l(avf2);
        JavaAdapterSignature aVar = new JavaAdapterSignature(cls, clsArr, l);
        Class<?> cls2 = cgm.get(aVar);
        if (cls2 == null) {
            String str = "adapter" + k.newClassSerialNumber();
            cls2 = m8835d(str, m8825a(l, str, cls, clsArr, (String) null));
            if (k.isCachingEnabled()) {
                cgm.put(aVar, cls2);
            }
        }
        return cls2;
    }

    /* renamed from: a */
    public static byte[] m8825a(org.mozilla1.javascript.ObjToIntMap uSVar, String str, Class<?> cls, Class<?>[] clsArr, String str2) {
        ClassFileWriter uh = new ClassFileWriter(str, cls.getName(), "<adapter>");
        uh.mo5785a("factory", "Lorg/mozilla/javascript/ContextFactory;", (short) 17);
        uh.mo5785a("delegee", "Lorg/mozilla/javascript/Scriptable;", (short) 17);
        uh.mo5785a("self", "Lorg/mozilla/javascript/Scriptable;", (short) 17);
        int length = clsArr == null ? 0 : clsArr.length;
        for (int i = 0; i < length; i++) {
            if (clsArr[i] != null) {
                uh.addInterface(clsArr[i].getName());
            }
        }
        String replace = cls.getName().replace('.', C0147Bi.cla);
        m8819a(uh, str, replace);
        m8831b(uh, str, replace);
        if (str2 != null) {
            m8820a(uh, str, replace, str2);
        }
        org.mozilla1.javascript.ObjToIntMap uSVar2 = new org.mozilla1.javascript.ObjToIntMap();
        org.mozilla1.javascript.ObjToIntMap uSVar3 = new org.mozilla1.javascript.ObjToIntMap();
        for (int i2 = 0; i2 < length; i2++) {
            Method[] methods = clsArr[i2].getMethods();
            for (Method method : methods) {
                int modifiers = method.getModifiers();
                if (!Modifier.isStatic(modifiers) && !Modifier.isFinal(modifiers)) {
                    String name = method.getName();
                    Class[] parameterTypes = method.getParameterTypes();
                    if (!uSVar.has(name)) {
                        try {
                            cls.getMethod(name, parameterTypes);
                        } catch (NoSuchMethodException e) {
                        }
                    }
                    String str3 = String.valueOf(name) + m8815a(method, (Class<?>[]) parameterTypes);
                    if (!uSVar2.has(str3)) {
                        m8822a(uh, str, name, (Class<?>[]) parameterTypes, method.getReturnType());
                        uSVar2.put(str3, 0);
                        uSVar3.put(name, 0);
                    }
                }
            }
        }
        Method[] ap = m8827ap(cls);
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= ap.length) {
                break;
            }
            Method method2 = ap[i4];
            boolean isAbstract = Modifier.isAbstract(method2.getModifiers());
            String name2 = method2.getName();
            if (isAbstract || uSVar.has(name2)) {
                Class[] parameterTypes2 = method2.getParameterTypes();
                String a = m8815a(method2, (Class<?>[]) parameterTypes2);
                String str4 = String.valueOf(name2) + a;
                if (!uSVar2.has(str4)) {
                    m8822a(uh, str, name2, (Class<?>[]) parameterTypes2, method2.getReturnType());
                    uSVar2.put(str4, 0);
                    uSVar3.put(name2, 0);
                    if (!isAbstract) {
                        m8821a(uh, str, replace, name2, a, parameterTypes2, method2.getReturnType());
                    }
                }
            }
            i3 = i4 + 1;
        }
        org.mozilla1.javascript.ObjToIntMap.Iterator aVar = new org.mozilla1.javascript.ObjToIntMap.Iterator(uSVar);
        aVar.start();
        while (!aVar.done()) {
            String str5 = (String) aVar.getKey();
            if (!uSVar3.has(str5)) {
                int value = aVar.getValue();
                Class[] clsArr2 = new Class[value];
                for (int i5 = 0; i5 < value; i5++) {
                    clsArr2[i5] = org.mozilla1.javascript.ScriptRuntime.ObjectClass;
                }
                m8822a(uh, str, str5, (Class<?>[]) clsArr2, org.mozilla1.javascript.ScriptRuntime.ObjectClass);
            }
            aVar.next();
        }
        return uh.toByteArray();
    }

    /* renamed from: ap */
    static Method[] m8827ap(Class<?> cls) {
        ArrayList arrayList = new ArrayList();
        HashSet hashSet = new HashSet();
        for (Class<? super Object> cls2 = (Class<? super Object>) cls; cls2 != null; cls2 = cls2.getSuperclass()) {
            m8824a(cls2, (ArrayList<Method>) arrayList, (HashSet<String>) hashSet);
            for (Class a : cls2.getInterfaces()) {
                m8824a((Class<?>) a, (ArrayList<Method>) arrayList, (HashSet<String>) hashSet);
            }
        }
        return (Method[]) arrayList.toArray(new Method[arrayList.size()]);
    }

    /* renamed from: a */
    private static void m8824a(Class<?> cls, ArrayList<Method> arrayList, HashSet<String> hashSet) {
        Method[] declaredMethods = cls.getDeclaredMethods();
        for (int i = 0; i < declaredMethods.length; i++) {
            String str = String.valueOf(declaredMethods[i].getName()) + m8815a(declaredMethods[i], (Class<?>[]) declaredMethods[i].getParameterTypes());
            if (!hashSet.contains(str)) {
                int modifiers = declaredMethods[i].getModifiers();
                if (!Modifier.isStatic(modifiers)) {
                    if (Modifier.isFinal(modifiers)) {
                        hashSet.add(str);
                    } else if (Modifier.isPublic(modifiers) || Modifier.isProtected(modifiers)) {
                        arrayList.add(declaredMethods[i]);
                        hashSet.add(str);
                    }
                }
            }
        }
    }

    /* renamed from: d */
    static Class<?> m8835d(String str, byte[] bArr) {
        CodeSource codeSource;
        Class<?> cho = org.mozilla1.javascript.SecurityController.cho();
        if (cho == CodeSource.class || cho == ProtectionDomain.class) {
            ProtectionDomain protectionDomain = JavaAdapter.class.getProtectionDomain();
            codeSource = protectionDomain;
            if (cho == CodeSource.class) {
                codeSource = protectionDomain == null ? null : protectionDomain.getCodeSource();
            }
        } else {
            codeSource = null;
        }
        GeneratedClassLoader a = SecurityController.m40442a((ClassLoader) null, codeSource);
        Class<?> defineClass = a.defineClass(str, bArr);
        a.linkClass(defineClass);
        return defineClass;
    }

    /* renamed from: b */
    public static org.mozilla1.javascript.Function m8829b(org.mozilla1.javascript.Scriptable avf, String str) {
        Object j = org.mozilla1.javascript.ScriptableObject.m23587j(avf, str);
        if (j == org.mozilla1.javascript.Scriptable.NOT_FOUND) {
            return null;
        }
        if (j instanceof org.mozilla1.javascript.Function) {
            return (org.mozilla1.javascript.Function) j;
        }
        throw org.mozilla1.javascript.ScriptRuntime.notFunctionError(j, str);
    }

    /* renamed from: a */
    public static Object m8813a(org.mozilla1.javascript.ContextFactory iwVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Function azg, Object[] objArr, long j) {
        if (azg == null) {
            return Undefined.instance;
        }
        if (iwVar == null) {
            iwVar = org.mozilla1.javascript.ContextFactory.getGlobal();
        }
        org.mozilla1.javascript.Scriptable parentScope = azg.getParentScope();
        if (j == 0) {
            return org.mozilla1.javascript.Context.m35234a(iwVar, (Callable) azg, parentScope, avf, objArr);
        }
        org.mozilla1.javascript.Context bwq = org.mozilla1.javascript.Context.bwq();
        if (bwq != null) {
            return m8814a(bwq, parentScope, avf, azg, objArr, j);
        }
        return iwVar.mo19793a((org.mozilla1.javascript.ContextAction) new C1111b(parentScope, avf, azg, objArr, j));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public static Object m8814a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, org.mozilla1.javascript.Function azg, Object[] objArr, long j) {
        for (int i = 0; i != objArr.length; i++) {
            if (0 != (((long) (1 << i)) & j)) {
                Object obj = objArr[i];
                if (!(obj instanceof org.mozilla1.javascript.Scriptable)) {
                    objArr[i] = lhVar.bwy().mo6829a(lhVar, avf, obj, (Class<?>) null);
                }
            }
        }
        return azg.call(lhVar, avf, avf2, objArr);
    }

    /* renamed from: c */
    public static org.mozilla1.javascript.Scriptable m8833c(org.mozilla1.javascript.Script afe) {
        return (org.mozilla1.javascript.Scriptable) ContextFactory.getGlobal().mo19793a((org.mozilla1.javascript.ContextAction) new C1112c(afe));
    }

    /* renamed from: a */
    private static void m8819a(ClassFileWriter uh, String str, String str2) {
        uh.mo5795b(C2821kb.arR, "(Lorg/mozilla/javascript/ContextFactory;Lorg/mozilla/javascript/Scriptable;)V", 1);
        uh.add(42);
        uh.mo5794b(183, str2, C2821kb.arR, "()V");
        uh.add(42);
        uh.add(43);
        uh.mo5782a(181, str, "factory", "Lorg/mozilla/javascript/ContextFactory;");
        uh.add(42);
        uh.add(44);
        uh.mo5782a(181, str, "delegee", "Lorg/mozilla/javascript/Scriptable;");
        uh.add(42);
        uh.add(44);
        uh.add(42);
        uh.mo5794b(184, "org/mozilla/javascript/JavaAdapter", "createAdapterWrapper", "(Lorg/mozilla/javascript/Scriptable;Ljava/lang/Object;)Lorg/mozilla/javascript/Scriptable;");
        uh.mo5782a(181, str, "self", "Lorg/mozilla/javascript/Scriptable;");
        uh.add(177);
        uh.stopMethod(3);
    }

    /* renamed from: b */
    private static void m8831b(ClassFileWriter uh, String str, String str2) {
        uh.mo5795b(C2821kb.arR, "(Lorg/mozilla/javascript/ContextFactory;Lorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Scriptable;)V", 1);
        uh.add(42);
        uh.mo5794b(183, str2, C2821kb.arR, "()V");
        uh.add(42);
        uh.add(43);
        uh.mo5782a(181, str, "factory", "Lorg/mozilla/javascript/ContextFactory;");
        uh.add(42);
        uh.add(44);
        uh.mo5782a(181, str, "delegee", "Lorg/mozilla/javascript/Scriptable;");
        uh.add(42);
        uh.add(45);
        uh.mo5782a(181, str, "self", "Lorg/mozilla/javascript/Scriptable;");
        uh.add(177);
        uh.stopMethod(4);
    }

    /* renamed from: a */
    private static void m8820a(ClassFileWriter uh, String str, String str2, String str3) {
        uh.mo5795b(C2821kb.arR, "()V", 1);
        uh.add(42);
        uh.mo5794b(183, str2, C2821kb.arR, "()V");
        uh.add(42);
        uh.add(1);
        uh.mo5782a(181, str, "factory", "Lorg/mozilla/javascript/ContextFactory;");
        uh.add(187, str3);
        uh.add(89);
        uh.mo5794b(183, str3, C2821kb.arR, "()V");
        uh.mo5794b(184, "org/mozilla/javascript/JavaAdapter", "runScript", "(Lorg/mozilla/javascript/Script;)Lorg/mozilla/javascript/Scriptable;");
        uh.add(76);
        uh.add(42);
        uh.add(43);
        uh.mo5782a(181, str, "delegee", "Lorg/mozilla/javascript/Scriptable;");
        uh.add(42);
        uh.add(43);
        uh.add(42);
        uh.mo5794b(184, "org/mozilla/javascript/JavaAdapter", "createAdapterWrapper", "(Lorg/mozilla/javascript/Scriptable;Ljava/lang/Object;)Lorg/mozilla/javascript/Scriptable;");
        uh.mo5782a(181, str, "self", "Lorg/mozilla/javascript/Scriptable;");
        uh.add(177);
        uh.stopMethod(2);
    }

    /* renamed from: a */
    static void m8823a(ClassFileWriter uh, Class<?>[] clsArr, int i) {
        uh.mo5814nG(i);
        uh.add(189, "java/lang/Object");
        int i2 = 1;
        for (int i3 = 0; i3 != clsArr.length; i3++) {
            uh.add(89);
            uh.mo5814nG(i3);
            i2 += m8808a(uh, i2, clsArr[i3]);
            uh.add(83);
        }
    }

    /* renamed from: a */
    private static int m8808a(ClassFileWriter uh, int i, Class<?> cls) {
        int i2 = 1;
        if (!cls.isPrimitive()) {
            uh.add(25, i);
        } else if (cls == Boolean.TYPE) {
            uh.add(187, "java/lang/Boolean");
            uh.add(89);
            uh.add(21, i);
            uh.mo5794b(183, "java/lang/Boolean", C2821kb.arR, "(Z)V");
        } else if (cls == Character.TYPE) {
            uh.add(21, i);
            uh.mo5794b(184, "java/lang/String", "valueOf", "(C)Ljava/lang/String;");
        } else {
            uh.add(187, "java/lang/Double");
            uh.add(89);
            switch (cls.getName().charAt(0)) {
                case 'b':
                case 'i':
                case 's':
                    uh.add(21, i);
                    uh.add(135);
                    break;
                case 'd':
                    uh.add(24, i);
                    i2 = 2;
                    break;
                case 'f':
                    uh.add(23, i);
                    uh.add(141);
                    break;
                case 'l':
                    uh.add(22, i);
                    uh.add(138);
                    i2 = 2;
                    break;
            }
            uh.mo5794b(183, "java/lang/Double", C2821kb.arR, "(D)V");
        }
        return i2;
    }

    /* renamed from: a */
    static void m8818a(ClassFileWriter uh, Class<?> cls, boolean z) {
        if (cls == Void.TYPE) {
            uh.add(87);
            uh.add(177);
        } else if (cls == Boolean.TYPE) {
            uh.mo5794b(184, "org/mozilla/javascript/Context", "toBoolean", "(Ljava/lang/Object;)Z");
            uh.add(172);
        } else if (cls == Character.TYPE) {
            uh.mo5794b(184, "org/mozilla/javascript/Context", "toString", "(Ljava/lang/Object;)Ljava/lang/String;");
            uh.add(3);
            uh.mo5794b(182, "java/lang/String", "charAt", "(I)C");
            uh.add(172);
        } else if (cls.isPrimitive()) {
            uh.mo5794b(184, "org/mozilla/javascript/Context", "toNumber", "(Ljava/lang/Object;)D");
            switch (cls.getName().charAt(0)) {
                case 'b':
                case 'i':
                case 's':
                    uh.add(142);
                    uh.add(172);
                    return;
                case 'd':
                    uh.add(175);
                    return;
                case 'f':
                    uh.add(144);
                    uh.add(174);
                    return;
                case 'l':
                    uh.add(143);
                    uh.add(173);
                    return;
                default:
                    throw new RuntimeException("Unexpected return type " + cls.toString());
            }
        } else {
            String name = cls.getName();
            if (z) {
                uh.mo5803gI(name);
                uh.mo5794b(184, "java/lang/Class", "forName", "(Ljava/lang/String;)Ljava/lang/Class;");
                uh.mo5794b(184, "org/mozilla/javascript/JavaAdapter", "convertResult", "(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;");
            }
            uh.add(192, name);
            uh.add(176);
        }
    }

    /* renamed from: a */
    private static void m8822a(ClassFileWriter uh, String str, String str2, Class<?>[] clsArr, Class<?> cls) {
        StringBuffer stringBuffer = new StringBuffer();
        int a = m8809a(clsArr, cls, stringBuffer);
        uh.mo5795b(str2, stringBuffer.toString(), 1);
        uh.add(42);
        uh.mo5782a(180, str, "factory", "Lorg/mozilla/javascript/ContextFactory;");
        uh.add(42);
        uh.mo5782a(180, str, "self", "Lorg/mozilla/javascript/Scriptable;");
        uh.add(42);
        uh.mo5782a(180, str, "delegee", "Lorg/mozilla/javascript/Scriptable;");
        uh.mo5804gJ(str2);
        uh.mo5794b(184, "org/mozilla/javascript/JavaAdapter", "getFunction", "(Lorg/mozilla/javascript/Scriptable;Ljava/lang/String;)Lorg/mozilla/javascript/Function;");
        m8823a(uh, clsArr, clsArr.length);
        if (clsArr.length > 64) {
            throw org.mozilla1.javascript.Context.m35244gE("JavaAdapter can not subclass methods with more then 64 arguments.");
        }
        long j = 0;
        for (int i = 0; i != clsArr.length; i++) {
            if (!clsArr[i].isPrimitive()) {
                j |= (long) (1 << i);
            }
        }
        uh.mo5802fN(j);
        uh.mo5794b(184, "org/mozilla/javascript/JavaAdapter", "callMethod", "(Lorg/mozilla/javascript/ContextFactory;Lorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Function;[Ljava/lang/Object;J)Ljava/lang/Object;");
        m8818a(uh, cls, true);
        uh.stopMethod((short) a);
    }

    /* renamed from: b */
    private static int m8828b(ClassFileWriter uh, int i, Class<?> cls) {
        if (!cls.isPrimitive()) {
            uh.mo5824nQ(i);
            return 1;
        }
        switch (cls.getName().charAt(0)) {
            case 'b':
            case 'c':
            case 'i':
            case 's':
            case 'z':
                uh.mo5820nM(i);
                return 1;
            case 'd':
                uh.mo5823nP(i);
                return 2;
            case 'f':
                uh.mo5822nO(i);
                return 1;
            case 'l':
                uh.mo5821nN(i);
                return 2;
            default:
                throw Kit.codeBug();
        }
    }

    /* renamed from: a */
    private static void m8817a(ClassFileWriter uh, Class<?> cls) {
        if (cls.isPrimitive()) {
            switch (cls.getName().charAt(0)) {
                case 'b':
                case 'c':
                case 'i':
                case 's':
                case 'z':
                    uh.add(172);
                    return;
                case 'd':
                    uh.add(175);
                    return;
                case 'f':
                    uh.add(174);
                    return;
                case 'l':
                    uh.add(173);
                    return;
                default:
                    return;
            }
        } else {
            uh.add(176);
        }
    }

    /* renamed from: a */
    private static void m8821a(ClassFileWriter uh, String str, String str2, String str3, String str4, Class<?>[] clsArr, Class<?> cls) {
        int i = 1;
        uh.mo5795b("super$" + str3, str4, 1);
        uh.add(25, 0);
        for (Class<?> b : clsArr) {
            i += m8828b(uh, i, b);
        }
        uh.mo5794b(183, str2, str3, str4);
        if (!cls.equals(Void.TYPE)) {
            m8817a(uh, cls);
        } else {
            uh.add(177);
        }
        uh.stopMethod((short) (i + 1));
    }

    /* renamed from: a */
    private static String m8815a(Method method, Class<?>[] clsArr) {
        StringBuffer stringBuffer = new StringBuffer();
        m8809a(clsArr, method.getReturnType(), stringBuffer);
        return stringBuffer.toString();
    }

    /* renamed from: a */
    static int m8809a(Class<?>[] clsArr, Class<?> cls, StringBuffer stringBuffer) {
        stringBuffer.append('(');
        int length = clsArr.length + 1;
        for (Class<?> cls2 : clsArr) {
            m8816a(stringBuffer, cls2);
            if (cls2 == Long.TYPE || cls2 == Double.TYPE) {
                length++;
            }
        }
        stringBuffer.append(')');
        m8816a(stringBuffer, cls);
        return length;
    }

    /* renamed from: a */
    private static StringBuffer m8816a(StringBuffer stringBuffer, Class<?> cls) {
        char upperCase;
        while (cls.isArray()) {
            stringBuffer.append('[');
            cls = cls.getComponentType();
        }
        if (cls.isPrimitive()) {
            if (cls == Boolean.TYPE) {
                upperCase = 'Z';
            } else if (cls == Long.TYPE) {
                upperCase = 'J';
            } else {
                upperCase = Character.toUpperCase(cls.getName().charAt(0));
            }
            stringBuffer.append(upperCase);
        } else {
            stringBuffer.append('L');
            stringBuffer.append(cls.getName().replace('.', C0147Bi.cla));
            stringBuffer.append(C5878acG.dBB);
        }
        return stringBuffer;
    }

    /* renamed from: a */
    static int[] m8826a(Class<?>[] clsArr) {
        int i = 0;
        for (int i2 = 0; i2 != clsArr.length; i2++) {
            if (!clsArr[i2].isPrimitive()) {
                i++;
            }
        }
        if (i == 0) {
            return null;
        }
        int[] iArr = new int[i];
        int i3 = 0;
        for (int i4 = 0; i4 != clsArr.length; i4++) {
            if (!clsArr[i4].isPrimitive()) {
                iArr[i3] = i4;
                i3++;
            }
        }
        return iArr;
    }

    public Object execIdCall(IdFunctionObject yy, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        if (yy.hasTag(cHe) && yy.methodId() == 1) {
            return m8836e(lhVar, avf, objArr);
        }
        throw yy.unknown();
    }

    /* renamed from: a.QL$a */
    static class JavaAdapterSignature {
        Class<?> dWO;
        Class<?>[] dWP;
        org.mozilla1.javascript.ObjToIntMap dWQ;

        JavaAdapterSignature(Class<?> cls, Class<?>[] clsArr, org.mozilla1.javascript.ObjToIntMap uSVar) {
            this.dWO = cls;
            this.dWP = clsArr;
            this.dWQ = uSVar;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof JavaAdapterSignature)) {
                return false;
            }
            JavaAdapterSignature aVar = (JavaAdapterSignature) obj;
            if (this.dWO != aVar.dWO) {
                return false;
            }
            if (this.dWP != aVar.dWP) {
                if (this.dWP.length != aVar.dWP.length) {
                    return false;
                }
                for (int i = 0; i < this.dWP.length; i++) {
                    if (this.dWP[i] != aVar.dWP[i]) {
                        return false;
                    }
                }
            }
            if (this.dWQ.size() != aVar.dWQ.size()) {
                return false;
            }
            org.mozilla1.javascript.ObjToIntMap.Iterator aVar2 = new ObjToIntMap.Iterator(this.dWQ);
            aVar2.start();
            while (!aVar2.done()) {
                int value = aVar2.getValue();
                if (value != this.dWQ.get((String) aVar2.getKey(), value + 1)) {
                    return false;
                }
                aVar2.next();
            }
            return true;
        }

        public int hashCode() {
            return this.dWO.hashCode() | (-1640531527 * (this.dWQ.size() | (this.dWP.length << 16)));
        }
    }

    /* renamed from: a.QL$b */
    /* compiled from: a */
    class C1111b implements org.mozilla1.javascript.ContextAction {
        private final /* synthetic */ org.mozilla1.javascript.Scriptable awY;
        private final /* synthetic */ org.mozilla1.javascript.Scriptable awZ;
        private final /* synthetic */ Object[] axa;
        private final /* synthetic */ org.mozilla1.javascript.Function fup;
        private final /* synthetic */ long fuq;

        C1111b(org.mozilla1.javascript.Scriptable avf, Scriptable avf2, Function azg, Object[] objArr, long j) {
            this.awY = avf;
            this.awZ = avf2;
            this.fup = azg;
            this.axa = objArr;
            this.fuq = j;
        }

        public Object run(org.mozilla1.javascript.Context lhVar) {
            return JavaAdapter.m8814a(lhVar, this.awY, this.awZ, this.fup, this.axa, this.fuq);
        }
    }

    /* renamed from: a.QL$c */
    /* compiled from: a */
    class C1112c implements ContextAction {
        private final /* synthetic */ org.mozilla1.javascript.Script fur;

        C1112c(Script afe) {
            this.fur = afe;
        }

        public Object run(Context lhVar) {
            ScriptableObject h = ScriptRuntime.m7585h(lhVar);
            this.fur.mo8800a(lhVar, h);
            return h;
        }
    }
}
