package org.mozilla1.javascript.optimizer;

import org.mozilla1.javascript.ScriptRuntime;

/* renamed from: a.bo */
/* compiled from: a */
public final class OptRuntime extends org.mozilla1.javascript.ScriptRuntime {
    public static final Double eQc = new Double(org.mozilla1.javascript.ScriptRuntime.NaN);
    public static final Double eQd = new Double(1.0d);
    public static final Double eQe = new Double(-1.0d);

    /* renamed from: a */
    public static Object m27985a(org.mozilla1.javascript.Callable mFVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf2) {
        return mFVar.call(lhVar, avf2, avf, org.mozilla1.javascript.ScriptRuntime.emptyArgs);
    }

    /* renamed from: a */
    public static Object m27986a(org.mozilla1.javascript.Callable mFVar, org.mozilla1.javascript.Scriptable avf, Object obj, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf2) {
        return mFVar.call(lhVar, avf2, avf, new Object[]{obj});
    }

    /* renamed from: a */
    public static Object m27987a(org.mozilla1.javascript.Callable mFVar, org.mozilla1.javascript.Scriptable avf, Object obj, Object obj2, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf2) {
        return mFVar.call(lhVar, avf2, avf, new Object[]{obj, obj2});
    }

    /* renamed from: a */
    public static Object m27988a(org.mozilla1.javascript.Callable mFVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf2) {
        return mFVar.call(lhVar, avf2, avf, objArr);
    }

    /* renamed from: a */
    public static Object m27993a(Object[] objArr, String str, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf) {
        return m7541b(str, lhVar, avf).call(lhVar, avf, m7605q(lhVar), objArr);
    }

    /* renamed from: a */
    public static Object m27992a(String str, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf) {
        return m7541b(str, lhVar, avf).call(lhVar, avf, m7605q(lhVar), org.mozilla1.javascript.ScriptRuntime.emptyArgs);
    }

    /* renamed from: a */
    public static Object m27991a(Object obj, String str, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf) {
        return m7568d(obj, str, lhVar).call(lhVar, avf, m7605q(lhVar), org.mozilla1.javascript.ScriptRuntime.emptyArgs);
    }

    /* renamed from: a */
    public static Object m27989a(Object obj, double d) {
        Object obj2;
        if (obj instanceof org.mozilla1.javascript.Scriptable) {
            obj2 = ((org.mozilla1.javascript.Scriptable) obj).getDefaultValue((Class<?>) null);
        } else {
            obj2 = obj;
        }
        if (!(obj2 instanceof String)) {
            return m27979J(toNumber(obj2) + d);
        }
        return ((String) obj2).concat(toString(d));
    }

    /* renamed from: a */
    public static Object m27982a(double d, Object obj) {
        Object obj2;
        if (obj instanceof org.mozilla1.javascript.Scriptable) {
            obj2 = ((org.mozilla1.javascript.Scriptable) obj).getDefaultValue((Class<?>) null);
        } else {
            obj2 = obj;
        }
        if (!(obj2 instanceof String)) {
            return m27979J(toNumber(obj2) + d);
        }
        return toString(d).concat((String) obj2);
    }

    /* renamed from: a */
    public static Object m27990a(Object obj, double d, org.mozilla1.javascript.Context lhVar, int i) {
        return org.mozilla1.javascript.ScriptRuntime.m7516a(obj, (Object) new Double(d), lhVar, i);
    }

    /* renamed from: a */
    public static Object[] m27996a(Object[] objArr, int i) {
        Object[] objArr2 = new Object[(objArr.length + i)];
        System.arraycopy(objArr, 0, objArr2, i, objArr.length);
        return objArr2;
    }

    /* renamed from: a */
    public static void m27994a(org.mozilla1.javascript.NativeFunction ifR, int i, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Context lhVar) {
        org.mozilla1.javascript.ScriptRuntime.m7530a(lhVar, avf, ifR, i, false);
    }

    /* renamed from: a */
    public static Object m27983a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Callable mFVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr, org.mozilla1.javascript.Scriptable avf2, org.mozilla1.javascript.Scriptable avf3, int i, String str, int i2) {
        return org.mozilla1.javascript.ScriptRuntime.m7507a(lhVar, mFVar, avf, objArr, avf2, avf3, i, str, i2);
    }

    /* renamed from: a */
    public static Object m27984a(org.mozilla1.javascript.Context lhVar, Object obj, Object[] objArr, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, int i) {
        return org.mozilla1.javascript.ScriptRuntime.m7509a(lhVar, obj, objArr, avf, i);
    }

    /* renamed from: J */
    public static Double m27979J(double d) {
        if (d == org.mozilla1.javascript.ScriptRuntime.NaN) {
            if (1.0d / d > ScriptRuntime.NaN) {
                return eQc;
            }
        } else if (d == 1.0d) {
            return eQd;
        } else {
            if (d == -1.0d) {
                return eQe;
            }
            if (d != d) {
                return NaNobj;
            }
        }
        return new Double(d);
    }

    /* renamed from: d */
    public static String m28000d(int[] iArr) {
        if (iArr == null) {
            return null;
        }
        int length = iArr.length;
        char[] cArr = new char[((length * 2) + 1)];
        cArr[0] = 1;
        for (int i = 0; i != length; i++) {
            int i2 = iArr[i];
            int i3 = (i * 2) + 1;
            cArr[i3] = (char) (i2 >>> 16);
            cArr[i3 + 1] = (char) i2;
        }
        return new String(cArr);
    }

    /* renamed from: s */
    private static int[] m28001s(String str, int i) {
        if (i == 0) {
            if (str == null) {
                return null;
            }
            throw new IllegalArgumentException();
        } else if (str.length() == (i * 2) + 1 || str.charAt(0) == 1) {
            int[] iArr = new int[i];
            for (int i2 = 0; i2 != i; i2++) {
                int i3 = (i2 * 2) + 1;
                iArr[i2] = str.charAt(i3 + 1) | (str.charAt(i3) << 16);
            }
            return iArr;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /* renamed from: a */
    public static org.mozilla1.javascript.Scriptable m27981a(Object[] objArr, String str, int i, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf) {
        return m7481a(objArr, m28001s(str, i), lhVar, avf);
    }

    /* renamed from: a */
    public static void m27995a(org.mozilla1.javascript.Script afe, String[] strArr) {
        org.mozilla1.javascript.ContextFactory.getGlobal().mo19793a((org.mozilla1.javascript.ContextAction) new C2068b(strArr, afe));
    }

    /* renamed from: ad */
    public static void m27997ad(Object obj) {
        throw new org.mozilla1.javascript.JavaScriptException(org.mozilla1.javascript.NativeIterator.m38643o((org.mozilla1.javascript.Scriptable) obj), "", 0);
    }

    /* renamed from: a */
    public static org.mozilla1.javascript.Scriptable m27980a(org.mozilla1.javascript.NativeFunction ifR, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, int i, int i2) {
        return new org.mozilla1.javascript.NativeGenerator(avf, ifR, new C2067a(avf2, i, i2));
    }

    /* renamed from: ae */
    public static Object[] m27998ae(Object obj) {
        C2067a aVar = (C2067a) obj;
        if (aVar.f5906og == null) {
            aVar.f5906og = new Object[aVar.maxStack];
        }
        return aVar.f5906og;
    }

    /* renamed from: af */
    public static Object[] m27999af(Object obj) {
        C2067a aVar = (C2067a) obj;
        if (aVar.f5907oh == null) {
            aVar.f5907oh = new Object[aVar.maxLocals];
        }
        return aVar.f5907oh;
    }

    /* renamed from: a.bo$a */
    public static class C2067a {
        static final String CLASS_NAME = "org/mozilla/javascript/optimizer/OptRuntime$GeneratorState";

        /* renamed from: ob */
        static final String f5900ob = "resumptionPoint";

        /* renamed from: oc */
        static final String f5901oc = "I";

        /* renamed from: oe */
        static final String f5902oe = "thisObj";

        /* renamed from: of */
        static final String f5903of = "Lorg/mozilla/javascript/Scriptable;";
        /* renamed from: oa */
        public int f5904oa;
        /* renamed from: od */
        public org.mozilla1.javascript.Scriptable f5905od;
        int maxLocals;
        int maxStack;
        /* renamed from: og */
        Object[] f5906og;

        /* renamed from: oh */
        Object[] f5907oh;

        C2067a(org.mozilla1.javascript.Scriptable avf, int i, int i2) {
            this.f5905od = avf;
            this.maxLocals = i;
            this.maxStack = i2;
        }
    }

    /* renamed from: a.bo$b */
    /* compiled from: a */
    class C2068b implements org.mozilla1.javascript.ContextAction {
        private final /* synthetic */ org.mozilla1.javascript.Script fur;
        private final /* synthetic */ String[] gCB;

        C2068b(String[] strArr, org.mozilla1.javascript.Script afe) {
            this.gCB = strArr;
            this.fur = afe;
        }

        public Object run(org.mozilla1.javascript.Context lhVar) {
            org.mozilla1.javascript.ScriptableObject h = OptRuntime.m7585h(lhVar);
            Object[] objArr = new Object[this.gCB.length];
            System.arraycopy(this.gCB, 0, objArr, 0, this.gCB.length);
            h.defineProperty("arguments", (Object) lhVar.mo20351b((org.mozilla1.javascript.Scriptable) h, objArr), 2);
            this.fur.mo8800a(lhVar, h);
            return null;
        }
    }
}
