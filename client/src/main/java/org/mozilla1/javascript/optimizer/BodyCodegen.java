package org.mozilla1.javascript.optimizer;

import org.mozilla1.classfile.ClassFileWriter;
import org.mozilla1.javascript.ScriptRuntime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: a.SN */
/* compiled from: a */
public class BodyCodegen {
    static final int eYQ = -1;
    static final int eYR = 0;
    static final int eYS = 1;
    private static final int MAX_LOCALS = 256;
    private static final int eYM = 0;
    private static final int eYN = 1;
    private static final int eYO = 2;
    private static final int eYP = 3;
    public int eYV;
    Codegen eYT;
    org.mozilla1.javascript.ScriptOrFnNode eYU;
    org.mozilla1.classfile.ClassFileWriter euH;
    /* renamed from: he */
    org.mozilla1.javascript.CompilerEnvirons f1538he;
    private boolean aKI;
    private int eYW;
    private OptFunctionNode eYX;
    private boolean eYY;
    private int[] eYZ;
    private short eZa;
    private short eZb;
    private int eZc;
    private boolean eZd;
    private short[] eZe;
    private boolean eZf;
    private int eZg;
    private int eZh;
    private short eZi;
    private short eZj;
    private short eZk;
    private short eZl;
    private short eZm;
    private short eZn;
    private short eZo;
    private short eZp;
    private short eZq;
    private short eZr;
    private short eZs;
    private boolean eZt;
    private int eZu;
    private Map<org.mozilla1.javascript.Node, C1238a> eZv;
    private int maxLocals = 0;
    private int maxStack = 0;

    BodyCodegen() {
    }

    /* renamed from: q */
    private static boolean m9438q(org.mozilla1.javascript.Node qlVar) {
        int type = qlVar.getType();
        return type == 22 || type == 25 || type == 24 || type == 23;
    }

    /* access modifiers changed from: package-private */
    public void bNn() {
        org.mozilla1.javascript.Node qlVar;
        this.eZt = Codegen.m21128d(this.eYU);
        bNq();
        if (this.eZt) {
            this.euH.mo5795b(String.valueOf(this.eYT.mo13020g(this.eYU)) + "_gen", "(" + this.eYT.f4380hk + "Lorg/mozilla/javascript/Context;" + "Lorg/mozilla/javascript/Scriptable;" + "Ljava/lang/Object;" + "Ljava/lang/Object;I)Ljava/lang/Object;", (short) 10);
        } else {
            this.euH.mo5795b(this.eYT.mo13020g(this.eYU), this.eYT.mo13021h(this.eYU), (short) 10);
        }
        bNr();
        if (this.eYX != null) {
            qlVar = this.eYU.cFF();
        } else {
            qlVar = this.eYU;
        }
        m9419i(qlVar);
        bNu();
        this.euH.stopMethod((short) (this.eZb + 1));
        if (this.eZt) {
            bNo();
        }
    }

    private void bNo() {
        this.euH.mo5795b(this.eYT.mo13020g(this.eYU), this.eYT.mo13021h(this.eYU), (short) 10);
        bNq();
        short s = this.eZa;
        this.eZa = (short) (s + 1);
        this.eZl = s;
        this.eZb = this.eZa;
        if (this.eYX != null && !this.aKI && (!this.f1538he.isUseDynamicScope() || this.eYX.eIE.getIgnoreDynamicScope())) {
            this.euH.mo5824nQ(this.eZo);
            this.euH.mo5794b(185, "org/mozilla/javascript/Scriptable", "getParentScope", "()Lorg/mozilla/javascript/Scriptable;");
            this.euH.mo5819nL(this.eZi);
        }
        this.euH.mo5824nQ(this.eZo);
        this.euH.mo5824nQ(this.eZi);
        this.euH.mo5824nQ(this.eZl);
        m9401ak("createFunctionActivation", "(Lorg/mozilla/javascript/NativeFunction;Lorg/mozilla/javascript/Scriptable;[Ljava/lang/Object;)Lorg/mozilla/javascript/Scriptable;");
        this.euH.mo5819nL(this.eZi);
        this.euH.add(187, this.eYT.f4379hj);
        this.euH.add(89);
        this.euH.mo5824nQ(this.eZi);
        this.euH.mo5824nQ(this.eZk);
        this.euH.mo5814nG(this.eYV);
        this.euH.mo5794b(183, this.eYT.f4379hj, "<init>", "(Lorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Context;I)V");
        this.euH.add(89);
        if (this.eYY) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        this.euH.add(42);
        this.euH.mo5782a(180, this.eYT.f4379hj, "_dcp", this.eYT.f4380hk);
        this.euH.mo5782a(181, this.eYT.f4379hj, "_dcp", this.eYT.f4380hk);
        bNp();
        this.euH.mo5824nQ(this.eZi);
        this.euH.mo5824nQ(this.eZn);
        this.euH.mo5813nF(this.maxLocals);
        this.euH.mo5813nF(this.maxStack);
        m9402al("createNativeGenerator", "(Lorg/mozilla/javascript/NativeFunction;Lorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Scriptable;II)Lorg/mozilla/javascript/Scriptable;");
        this.euH.add(176);
        this.euH.stopMethod((short) (this.eZb + 1));
    }

    private void bNp() {
        int functionCount = this.eYU.getFunctionCount();
        for (int i = 0; i != functionCount; i++) {
            OptFunctionNode b = OptFunctionNode.m11683b(this.eYU, i);
            if (b.eIE.getFunctionType() == 1) {
                m9390a(b, 1);
            }
        }
    }

    private void bNq() {
        boolean z;
        int paramAndVarCount;
        this.eYY = this.eYU == this.eYT.f4376hg[0];
        this.eZe = null;
        if (this.eYU.getType() == 108) {
            this.eYX = OptFunctionNode.m11684j(this.eYU);
            if (this.eYX.eIE.requiresActivation()) {
                z = false;
            } else {
                z = true;
            }
            this.eZd = z;
            if (this.eZd && (paramAndVarCount = this.eYX.eIE.getParamAndVarCount()) != 0) {
                this.eZe = new short[paramAndVarCount];
            }
            this.aKI = this.eYX.bFB();
            if (this.aKI && !this.eZd) {
                Codegen.m21129dh();
            }
        } else {
            this.eYX = null;
            this.eZd = false;
            this.aKI = false;
        }
        this.eYZ = new int[256];
        this.eZo = 0;
        this.eZk = 1;
        this.eZi = 2;
        this.eZn = 3;
        this.eZb = 4;
        this.eZa = 4;
        this.eZj = -1;
        this.eZl = -1;
        this.eZp = -1;
        this.eZq = -1;
        this.eZr = -1;
        this.eZh = -1;
        this.eZg = -1;
        this.eZs = -1;
    }

    private void bNr() {
        String str;
        short dX;
        short s;
        short s2;
        if (this.aKI) {
            int paramCount = this.eYU.getParamCount();
            if (this.eZa != 4) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            for (int i = 0; i != paramCount; i++) {
                this.eZe[i] = this.eZa;
                this.eZa = (short) (this.eZa + 3);
            }
            if (!this.eYX.bFD()) {
                this.eZf = true;
                for (int i2 = 0; i2 != paramCount; i2++) {
                    short s3 = this.eZe[i2];
                    this.euH.mo5824nQ(s3);
                    this.euH.mo5782a(178, "java/lang/Void", "TYPE", "Ljava/lang/Class;");
                    int bya = this.euH.bya();
                    this.euH.add(166, bya);
                    this.euH.mo5823nP(s3 + 1);
                    bND();
                    this.euH.mo5819nL(s3);
                    this.euH.mo5826nS(bya);
                }
            }
        }
        if (this.eYX != null && !this.aKI && (!this.f1538he.isUseDynamicScope() || this.eYX.eIE.getIgnoreDynamicScope())) {
            this.euH.mo5824nQ(this.eZo);
            this.euH.mo5794b(185, "org/mozilla/javascript/Scriptable", "getParentScope", "()Lorg/mozilla/javascript/Scriptable;");
            this.euH.mo5819nL(this.eZi);
        }
        short s4 = this.eZa;
        this.eZa = (short) (s4 + 1);
        this.eZl = s4;
        this.eZb = this.eZa;
        if (this.eZt) {
            short s5 = this.eZa;
            this.eZa = (short) (s5 + 1);
            this.eZm = s5;
            this.eZb = this.eZa;
            this.euH.mo5824nQ(this.eZn);
            short s6 = this.eZa;
            this.eZa = (short) (s6 + 1);
            this.eZs = s6;
            this.eZb = this.eZa;
            this.euH.add(192, "org/mozilla/javascript/optimizer/OptRuntime$GeneratorState");
            this.euH.add(89);
            this.euH.mo5819nL(this.eZs);
            this.euH.mo5782a(180, "org/mozilla/javascript/optimizer/OptRuntime$GeneratorState", "thisObj", "Lorg/mozilla/javascript/Scriptable;");
            this.euH.mo5819nL(this.eZn);
            if (this.eZh == -1) {
                this.eZh = this.euH.bya();
            }
            ArrayList<org.mozilla1.javascript.Node> bHR = ((org.mozilla1.javascript.FunctionNode) this.eYU).bHR();
            if (bHR != null) {
                bNs();
                this.eZu = this.euH.mo5780R(0, bHR.size() + 0);
                m9389a(-1, false, 0);
            }
        }
        if (this.eYX == null && this.eYU.getRegexpCount() != 0) {
            this.eZr = bNE();
            this.eYT.mo13016a(this.euH, this.eYU, (int) this.eZk, (int) this.eZi);
            this.euH.mo5819nL(this.eZr);
        }
        if (this.f1538he.dgu()) {
            bNA();
        }
        if (this.eZd) {
            int paramCount2 = this.eYU.getParamCount();
            if (paramCount2 > 0 && !this.aKI) {
                this.euH.mo5824nQ(this.eZl);
                this.euH.add(190);
                this.euH.mo5814nG(paramCount2);
                int bya2 = this.euH.bya();
                this.euH.add(162, bya2);
                this.euH.mo5824nQ(this.eZl);
                this.euH.mo5814nG(paramCount2);
                m9401ak("padArguments", "([Ljava/lang/Object;I)[Ljava/lang/Object;");
                this.euH.mo5819nL(this.eZl);
                this.euH.mo5826nS(bya2);
            }
            int paramCount3 = this.eYX.eIE.getParamCount();
            int paramAndVarCount = this.eYX.eIE.getParamAndVarCount();
            boolean[] cHr = this.eYX.eIE.cHr();
            int i3 = 0;
            short s7 = -1;
            while (i3 != paramAndVarCount) {
                if (i3 < paramCount3) {
                    if (!this.aKI) {
                        dX = bNE();
                        this.euH.mo5824nQ(this.eZl);
                        this.euH.mo5814nG(i3);
                        this.euH.add(50);
                        this.euH.mo5819nL(dX);
                        s = s7;
                    } else {
                        dX = -1;
                        s = s7;
                    }
                } else if (this.eYX.mo6841pe(i3)) {
                    dX = m9412dW(cHr[i3]);
                    this.euH.mo5779F(org.mozilla1.javascript.ScriptRuntime.NaN);
                    this.euH.mo5818nK(dX);
                    s = s7;
                } else {
                    dX = m9413dX(cHr[i3]);
                    if (s7 == -1) {
                        Codegen.m21135j(this.euH);
                        s7 = dX;
                    } else {
                        this.euH.mo5824nQ(s7);
                    }
                    this.euH.mo5819nL(dX);
                    s = s7;
                }
                if (dX >= 0) {
                    if (cHr[i3]) {
                        this.euH.mo5814nG(0);
                        org.mozilla1.classfile.ClassFileWriter uh = this.euH;
                        if (this.eYX.mo6841pe(i3)) {
                            s2 = 2;
                        } else {
                            s2 = 1;
                        }
                        uh.mo5815nH(s2 + dX);
                    }
                    this.eZe[i3] = dX;
                }
                if (this.f1538he.isGenerateDebugInfo()) {
                    String paramOrVarName = this.eYX.eIE.getParamOrVarName(i3);
                    String str2 = this.eYX.mo6841pe(i3) ? "D" : "Ljava/lang/Object;";
                    int byc = this.euH.byc();
                    if (dX < 0) {
                        dX = this.eZe[i3];
                    }
                    this.euH.mo5784a(paramOrVarName, str2, byc, (int) dX);
                }
                i3++;
                s7 = s;
            }
        } else if (!this.eZt) {
            if (this.eYX != null) {
                str = "activation";
                this.euH.mo5824nQ(this.eZo);
                this.euH.mo5824nQ(this.eZi);
                this.euH.mo5824nQ(this.eZl);
                m9401ak("createFunctionActivation", "(Lorg/mozilla/javascript/NativeFunction;Lorg/mozilla/javascript/Scriptable;[Ljava/lang/Object;)Lorg/mozilla/javascript/Scriptable;");
                this.euH.mo5819nL(this.eZi);
                this.euH.mo5824nQ(this.eZk);
                this.euH.mo5824nQ(this.eZi);
                m9401ak("enterActivationFunction", "(Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;)V");
            } else {
                str = "global";
                this.euH.mo5824nQ(this.eZo);
                this.euH.mo5824nQ(this.eZn);
                this.euH.mo5824nQ(this.eZk);
                this.euH.mo5824nQ(this.eZi);
                this.euH.mo5814nG(0);
                m9401ak("initScript", "(Lorg/mozilla/javascript/NativeFunction;Lorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;Z)V");
            }
            this.eZg = this.euH.bya();
            this.eZh = this.euH.bya();
            this.euH.mo5826nS(this.eZg);
            bNp();
            if (this.f1538he.isGenerateDebugInfo()) {
                this.euH.mo5784a(str, "Lorg/mozilla/javascript/Scriptable;", this.euH.byc(), (int) this.eZi);
            }
            if (this.eYX == null) {
                this.eZj = bNE();
                Codegen.m21135j(this.euH);
                this.euH.mo5819nL(this.eZj);
                int endLineno = this.eYU.getEndLineno();
                if (endLineno != -1) {
                    this.euH.mo5831p((short) endLineno);
                    return;
                }
                return;
            }
            if (this.eYX.eII) {
                this.eZp = bNE();
                this.euH.mo5782a(178, "org/mozilla/javascript/ScriptRuntime", "emptyArgs", "[Ljava/lang/Object;");
                this.euH.mo5819nL(this.eZp);
            }
            if (this.eYX.eIJ) {
                this.eZq = bNE();
                this.euH.mo5814nG(1);
                this.euH.add(189, "java/lang/Object");
                this.euH.mo5819nL(this.eZq);
            }
        }
    }

    private void bNs() {
        this.euH.mo5824nQ(this.eZs);
        this.euH.mo5782a(180, "org/mozilla/javascript/optimizer/OptRuntime$GeneratorState", "resumptionPoint", "I");
    }

    /* renamed from: pG */
    private void m9431pG(int i) {
        this.euH.mo5824nQ(this.eZs);
        this.euH.mo5813nF(i);
        this.euH.mo5782a(181, "org/mozilla/javascript/optimizer/OptRuntime$GeneratorState", "resumptionPoint", "I");
    }

    private void bNt() {
        this.euH.mo5824nQ(this.eZs);
        m9402al("getGeneratorStackState", "(Ljava/lang/Object;)[Ljava/lang/Object;");
    }

    private void bNu() {
        if (this.f1538he.dgu()) {
            bNB();
        }
        if (this.eZt) {
            HashMap<org.mozilla1.javascript.Node, int[]> bHS = ((org.mozilla1.javascript.FunctionNode) this.eYU).bHS();
            if (bHS != null) {
                ArrayList<org.mozilla1.javascript.Node> bHR = ((org.mozilla1.javascript.FunctionNode) this.eYU).bHR();
                for (int i = 0; i < bHR.size(); i++) {
                    org.mozilla1.javascript.Node qlVar = bHR.get(i);
                    int[] iArr = bHS.get(qlVar);
                    if (iArr != null) {
                        this.euH.mo5781S(this.eZu, m9421j(qlVar));
                        bNv();
                        for (int i2 = 0; i2 < iArr.length; i2++) {
                            this.euH.add(89);
                            this.euH.mo5813nF(i2);
                            this.euH.add(50);
                            this.euH.mo5819nL(iArr[i2]);
                        }
                        this.euH.add(87);
                        this.euH.add(167, m9423k(qlVar));
                    }
                }
            }
            if (this.eZv != null) {
                for (org.mozilla1.javascript.Node next : this.eZv.keySet()) {
                    if (next.getType() == 124) {
                        C1238a aVar = this.eZv.get(next);
                        this.euH.mo5783a(aVar.efS, (short) 1);
                        int R = this.euH.mo5780R(0, aVar.efR.size() - 1);
                        this.euH.mo5825nR(R);
                        int i3 = 0;
                        for (int i4 = 0; i4 < aVar.efR.size(); i4++) {
                            this.euH.mo5781S(R, i3);
                            this.euH.add(167, aVar.efR.get(i4).intValue());
                            i3++;
                        }
                    }
                }
            }
        }
        if (this.eZh != -1) {
            this.euH.mo5826nS(this.eZh);
        }
        if (this.eZd) {
            this.euH.add(176);
        } else if (this.eZt) {
            if (((org.mozilla1.javascript.FunctionNode) this.eYU).bHR() != null) {
                this.euH.mo5825nR(this.eZu);
            }
            m9431pG(-1);
            this.euH.mo5824nQ(this.eZi);
            m9402al("throwStopIteration", "(Ljava/lang/Object;)V");
            Codegen.m21135j(this.euH);
            this.euH.add(176);
        } else if (this.eYX == null) {
            this.euH.mo5824nQ(this.eZj);
            this.euH.add(176);
        } else {
            bNw();
            this.euH.add(176);
            int bya = this.euH.bya();
            this.euH.mo5827nT(bya);
            short bNE = bNE();
            this.euH.mo5819nL(bNE);
            bNw();
            this.euH.mo5824nQ(bNE);
            m9442s(bNE);
            this.euH.add(191);
            this.euH.addExceptionHandler(this.eZg, this.eZh, bya, (String) null);
        }
    }

    private void bNv() {
        this.euH.mo5824nQ(this.eZs);
        m9402al("getGeneratorLocalsState", "(Ljava/lang/Object;)[Ljava/lang/Object;");
    }

    private void bNw() {
        if (this.eYX == null || this.eZd) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        this.euH.mo5824nQ(this.eZk);
        m9401ak("exitActivationFunction", "(Lorg/mozilla/javascript/Context;)V");
    }

    /* renamed from: i */
    private void m9419i(org.mozilla1.javascript.Node qlVar) {
        int i = 0;
        m9426m(qlVar);
        int type = qlVar.getType();
        org.mozilla1.javascript.Node cFE = qlVar.cFE();
        switch (type) {
            case 2:
                m9405b(cFE, qlVar);
                this.euH.mo5824nQ(this.eZk);
                this.euH.mo5824nQ(this.eZi);
                m9401ak("enterWith", "(Ljava/lang/Object;Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;)Lorg/mozilla/javascript/Scriptable;");
                this.euH.mo5819nL(this.eZi);
                m9437q(this.eZi);
                return;
            case 3:
                this.euH.mo5824nQ(this.eZi);
                m9401ak("leaveWith", "(Lorg/mozilla/javascript/Scriptable;)Lorg/mozilla/javascript/Scriptable;");
                this.euH.mo5819nL(this.eZi);
                m9440r(this.eZi);
                return;
            case 4:
            case 64:
                if (!this.eZt) {
                    if (cFE != null) {
                        m9405b(cFE, qlVar);
                    } else if (type == 4) {
                        Codegen.m21135j(this.euH);
                    } else if (this.eZj < 0) {
                        throw Codegen.m21129dh();
                    } else {
                        this.euH.mo5824nQ(this.eZj);
                    }
                }
                if (this.f1538he.dgu()) {
                    bNB();
                }
                if (this.eZh == -1) {
                    if (!this.eZd) {
                        throw Codegen.m21129dh();
                    }
                    this.eZh = this.euH.bya();
                }
                this.euH.add(167, this.eZh);
                return;
            case 5:
            case 6:
            case 7:
            case 134:
                if (this.f1538he.dgu()) {
                    bNB();
                }
                m9391a((org.mozilla1.javascript.Node.C3367a) qlVar, type, cFE);
                return;
            case 50:
                m9405b(cFE, qlVar);
                if (this.f1538he.dgu()) {
                    bNB();
                }
                bNz();
                return;
            case 51:
                if (this.f1538he.dgu()) {
                    bNB();
                }
                this.euH.mo5824nQ(m9443t(qlVar));
                this.euH.add(191);
                return;
            case 57:
                this.euH.mo5829o((short) 0);
                int t = m9443t(qlVar);
                int existingIntProp = qlVar.getExistingIntProp(14);
                String string = cFE.getString();
                m9405b(cFE.cFG(), qlVar);
                if (existingIntProp == 0) {
                    this.euH.add(1);
                } else {
                    this.euH.mo5824nQ(t);
                }
                this.euH.mo5804gJ(string);
                this.euH.mo5824nQ(this.eZk);
                this.euH.mo5824nQ(this.eZi);
                m9401ak("newCatchScope", "(Ljava/lang/Throwable;Lorg/mozilla/javascript/Scriptable;Ljava/lang/String;Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;)Lorg/mozilla/javascript/Scriptable;");
                this.euH.mo5819nL(t);
                return;
            case 58:
            case 59:
            case 60:
                m9405b(cFE, qlVar);
                this.euH.mo5824nQ(this.eZk);
                if (type != 58) {
                    if (type == 59) {
                        i = 1;
                    } else {
                        i = 2;
                    }
                }
                this.euH.mo5814nG(i);
                m9401ak("enumInit", "(Ljava/lang/Object;Lorg/mozilla/javascript/Context;I)Ljava/lang/Object;");
                this.euH.mo5819nL(m9443t(qlVar));
                return;
            case 80:
                m9392a((org.mozilla1.javascript.Node.C3367a) qlVar, cFE);
                return;
            case 108:
                OptFunctionNode b = OptFunctionNode.m11683b(this.eYU, qlVar.getExistingIntProp(1));
                int functionType = b.eIE.getFunctionType();
                if (functionType == 3) {
                    m9390a(b, functionType);
                    return;
                } else if (functionType != 1) {
                    throw Codegen.m21129dh();
                } else {
                    return;
                }
            case 113:
                if (this.f1538he.dgu()) {
                    bNB();
                }
                m9404b((org.mozilla1.javascript.Node.C3367a) qlVar, cFE);
                return;
            case 122:
            case 127:
            case 128:
            case 129:
            case 131:
            case 135:
                if (this.f1538he.dgu()) {
                    m9432pH(1);
                }
                while (cFE != null) {
                    m9419i(cFE);
                    cFE = cFE.cFG();
                }
                return;
            case 124:
                if (this.f1538he.dgu()) {
                    bNA();
                }
                this.euH.mo5829o((short) 1);
                short bNE = bNE();
                if (this.eZt) {
                    bNx();
                }
                this.euH.mo5819nL(bNE);
                for (org.mozilla1.javascript.Node qlVar2 = cFE; qlVar2 != null; qlVar2 = qlVar2.cFG()) {
                    m9419i(qlVar2);
                }
                if (this.eZt) {
                    this.euH.mo5824nQ(bNE);
                    this.euH.add(192, "java/lang/Integer");
                    bNy();
                    C1238a aVar = this.eZv.get(qlVar);
                    aVar.efS = this.euH.bya();
                    this.euH.add(167, aVar.efS);
                } else {
                    this.euH.add(169, (int) bNE);
                }
                m9442s((short) bNE);
                return;
            case 130:
                if (this.f1538he.dgu()) {
                    bNB();
                }
                this.euH.mo5826nS(m9423k(qlVar));
                if (this.f1538he.dgu()) {
                    bNA();
                    return;
                }
                return;
            case 132:
                if (cFE.getType() == 56) {
                    m9407b(cFE, cFE.cFE(), false);
                    return;
                } else if (cFE.getType() == 155) {
                    m9410c(cFE, cFE.cFE(), false);
                    return;
                } else if (cFE.getType() == 72) {
                    m9400a(cFE, false);
                    return;
                } else {
                    m9405b(cFE, qlVar);
                    if (qlVar.getIntProp(8, -1) != -1) {
                        this.euH.add(88);
                        return;
                    } else {
                        this.euH.add(87);
                        return;
                    }
                }
            case 133:
                m9405b(cFE, qlVar);
                if (this.eZj < 0) {
                    this.eZj = bNE();
                }
                this.euH.mo5819nL(this.eZj);
                return;
            case 140:
                short bNE2 = bNE();
                if (this.eZt) {
                    this.euH.add(1);
                    this.euH.mo5819nL(bNE2);
                }
                qlVar.putIntProp(2, bNE2);
                while (cFE != null) {
                    m9419i(cFE);
                    cFE = cFE.cFG();
                }
                m9442s((short) bNE2);
                qlVar.removeProp(2);
                return;
            case 159:
                return;
            default:
                throw Codegen.m21129dh();
        }
    }

    private void bNx() {
        this.euH.mo5794b(184, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
    }

    private void bNy() {
        this.euH.mo5794b(182, "java/lang/Integer", "intValue", "()I");
    }

    private void bNz() {
        this.euH.add(187, "org/mozilla/javascript/JavaScriptException");
        this.euH.add(90);
        this.euH.add(95);
        this.euH.mo5804gJ(this.eYU.getSourceName());
        this.euH.mo5814nG(this.eZc);
        this.euH.mo5794b(183, "org/mozilla/javascript/JavaScriptException", "<init>", "(Ljava/lang/Object;Ljava/lang/String;I)V");
        this.euH.add(191);
    }

    /* renamed from: j */
    private int m9421j(org.mozilla1.javascript.Node qlVar) {
        return ((org.mozilla1.javascript.FunctionNode) this.eYU).bHR().indexOf(qlVar) + 1;
    }

    /* renamed from: b */
    private void m9405b(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        String str;
        String str2;
        int i;
        int i2;
        int type = qlVar.getType();
        org.mozilla1.javascript.Node cFE = qlVar.cFE();
        switch (type) {
            case 8:
                m9418h(qlVar, cFE);
                return;
            case 9:
            case 10:
            case 11:
            case 18:
            case 19:
            case 20:
                m9395a(qlVar, type, cFE);
                return;
            case 12:
            case 13:
            case 46:
            case 47:
                int bya = this.euH.bya();
                int bya2 = this.euH.bya();
                m9409c(qlVar, cFE, bya, bya2);
                m9386W(bya, bya2);
                return;
            case 14:
            case 15:
            case 16:
            case 17:
            case 52:
            case 53:
                int bya3 = this.euH.bya();
                int bya4 = this.euH.bya();
                m9406b(qlVar, cFE, bya3, bya4);
                m9386W(bya3, bya4);
                return;
            case 21:
                m9405b(cFE, qlVar);
                m9405b(cFE.cFG(), qlVar);
                switch (qlVar.getIntProp(8, -1)) {
                    case 0:
                        this.euH.add(99);
                        return;
                    case 1:
                        m9402al("add", "(DLjava/lang/Object;)Ljava/lang/Object;");
                        return;
                    case 2:
                        m9402al("add", "(Ljava/lang/Object;D)Ljava/lang/Object;");
                        return;
                    default:
                        if (cFE.getType() == 41) {
                            m9401ak("add", "(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;");
                            return;
                        } else if (cFE.cFG().getType() == 41) {
                            m9401ak("add", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;");
                            return;
                        } else {
                            this.euH.mo5824nQ(this.eZk);
                            m9401ak("add", "(Ljava/lang/Object;Ljava/lang/Object;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
                            return;
                        }
                }
            case 22:
                m9396a(qlVar, 103, cFE, qlVar2);
                return;
            case 23:
                m9396a(qlVar, 107, cFE, qlVar2);
                return;
            case 24:
            case 25:
                if (type == 24) {
                    i2 = 111;
                } else {
                    i2 = 115;
                }
                m9396a(qlVar, i2, cFE, qlVar2);
                return;
            case 26:
                int bya5 = this.euH.bya();
                int bya6 = this.euH.bya();
                int bya7 = this.euH.bya();
                m9398a(cFE, qlVar, bya5, bya6);
                this.euH.mo5826nS(bya5);
                this.euH.mo5782a(178, "java/lang/Boolean", "FALSE", "Ljava/lang/Boolean;");
                this.euH.add(167, bya7);
                this.euH.mo5826nS(bya6);
                this.euH.mo5782a(178, "java/lang/Boolean", "TRUE", "Ljava/lang/Boolean;");
                this.euH.mo5826nS(bya7);
                this.euH.mo5828nV(-1);
                return;
            case 27:
                m9405b(cFE, qlVar);
                m9401ak("toInt32", "(Ljava/lang/Object;)I");
                this.euH.mo5814nG(-1);
                this.euH.add(130);
                this.euH.add(135);
                bND();
                return;
            case 28:
            case 29:
                m9405b(cFE, qlVar);
                bNC();
                if (type == 29) {
                    this.euH.add(119);
                }
                bND();
                return;
            case 30:
            case 38:
                int intProp = qlVar.getIntProp(10, 0);
                if (intProp == 0) {
                    OptFunctionNode xq = (OptFunctionNode) qlVar.getProp(9);
                    if (xq != null) {
                        m9397a(qlVar, xq, type, cFE);
                        return;
                    } else if (type == 38) {
                        m9414e(qlVar, cFE);
                        return;
                    } else {
                        m9416f(qlVar, cFE);
                        return;
                    }
                } else {
                    m9394a(qlVar, type, intProp, cFE);
                    return;
                }
            case 31:
                m9405b(cFE, qlVar);
                m9405b(cFE.cFG(), qlVar);
                this.euH.mo5824nQ(this.eZk);
                m9401ak("delete", "(Ljava/lang/Object;Ljava/lang/Object;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
                return;
            case 32:
                m9405b(cFE, qlVar);
                m9401ak("typeof", "(Ljava/lang/Object;)Ljava/lang/String;");
                return;
            case 33:
            case 34:
                m9422j(qlVar, cFE);
                return;
            case 35:
            case 138:
                m9387a(type, qlVar, cFE);
                return;
            case 36:
                m9405b(cFE, qlVar);
                m9405b(cFE.cFG(), qlVar);
                this.euH.mo5824nQ(this.eZk);
                if (qlVar.getIntProp(8, -1) != -1) {
                    m9401ak("getObjectIndex", "(Ljava/lang/Object;DLorg/mozilla/javascript/Context;)Ljava/lang/Object;");
                    return;
                } else {
                    m9401ak("getObjectElem", "(Ljava/lang/Object;Ljava/lang/Object;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
                    return;
                }
            case 37:
            case 139:
                m9403b(type, qlVar, cFE);
                return;
            case 39:
                this.euH.mo5824nQ(this.eZk);
                this.euH.mo5824nQ(this.eZi);
                this.euH.mo5804gJ(qlVar.getString());
                m9401ak("name", "(Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;Ljava/lang/String;)Ljava/lang/Object;");
                return;
            case 40:
                double d = qlVar.getDouble();
                if (qlVar.getIntProp(8, -1) != -1) {
                    this.euH.mo5779F(d);
                    return;
                } else {
                    this.eYT.mo13015a(this.euH, d);
                    return;
                }
            case 41:
                this.euH.mo5804gJ(qlVar.getString());
                return;
            case 42:
                this.euH.add(1);
                return;
            case 43:
                this.euH.mo5824nQ(this.eZn);
                return;
            case 44:
                this.euH.mo5782a(178, "java/lang/Boolean", "FALSE", "Ljava/lang/Boolean;");
                return;
            case 45:
                this.euH.mo5782a(178, "java/lang/Boolean", "TRUE", "Ljava/lang/Boolean;");
                return;
            case 48:
                int existingIntProp = qlVar.getExistingIntProp(4);
                if (this.eYX == null) {
                    this.euH.mo5824nQ(this.eZr);
                } else {
                    this.euH.mo5824nQ(this.eZo);
                    this.euH.mo5782a(180, this.eYT.f4379hj, "_re", "[Ljava/lang/Object;");
                }
                this.euH.mo5814nG(existingIntProp);
                this.euH.add(50);
                return;
            case 49:
                break;
            case 54:
                this.euH.mo5824nQ(m9443t(qlVar));
                return;
            case 55:
                m9441s(qlVar);
                return;
            case 56:
                m9407b(qlVar, cFE, true);
                return;
            case 61:
            case 62:
                this.euH.mo5824nQ(m9443t(qlVar));
                if (type == 61) {
                    m9401ak("enumNext", "(Ljava/lang/Object;)Ljava/lang/Boolean;");
                    return;
                }
                this.euH.mo5824nQ(this.eZk);
                m9401ak("enumId", "(Ljava/lang/Object;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
                return;
            case 63:
                this.euH.add(42);
                return;
            case 65:
                m9408c(qlVar, cFE);
                return;
            case 66:
                m9411d(qlVar, cFE);
                return;
            case 67:
                m9405b(cFE, qlVar);
                this.euH.mo5824nQ(this.eZk);
                m9401ak("refGet", "(Lorg/mozilla/javascript/Ref;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
                return;
            case 68:
            case 141:
                m9405b(cFE, qlVar);
                org.mozilla1.javascript.Node cFG = cFE.cFG();
                if (type == 141) {
                    this.euH.add(89);
                    this.euH.mo5824nQ(this.eZk);
                    m9401ak("refGet", "(Lorg/mozilla/javascript/Ref;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
                }
                m9405b(cFG, qlVar);
                this.euH.mo5824nQ(this.eZk);
                m9401ak("refSet", "(Lorg/mozilla/javascript/Ref;Ljava/lang/Object;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
                return;
            case 69:
                m9405b(cFE, qlVar);
                this.euH.mo5824nQ(this.eZk);
                m9401ak("refDel", "(Lorg/mozilla/javascript/Ref;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
                return;
            case 70:
                m9417g(cFE, qlVar);
                m9399a(qlVar, cFE.cFG(), false);
                this.euH.mo5824nQ(this.eZk);
                m9401ak("callRef", "(Lorg/mozilla/javascript/Callable;Lorg/mozilla/javascript/Scriptable;[Ljava/lang/Object;Lorg/mozilla/javascript/Context;)Lorg/mozilla/javascript/Ref;");
                return;
            case 71:
                m9405b(cFE, qlVar);
                this.euH.mo5804gJ((String) qlVar.getProp(17));
                this.euH.mo5824nQ(this.eZk);
                m9401ak("specialRef", "(Ljava/lang/Object;Ljava/lang/String;Lorg/mozilla/javascript/Context;)Lorg/mozilla/javascript/Ref;");
                return;
            case 72:
                m9400a(qlVar, true);
                return;
            case 73:
                m9405b(cFE, qlVar);
                this.euH.mo5824nQ(this.eZk);
                m9401ak("setDefaultNamespace", "(Ljava/lang/Object;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
                return;
            case 74:
                m9405b(cFE, qlVar);
                this.euH.mo5824nQ(this.eZk);
                m9401ak("escapeAttributeValue", "(Ljava/lang/Object;Lorg/mozilla/javascript/Context;)Ljava/lang/String;");
                return;
            case 75:
                m9405b(cFE, qlVar);
                this.euH.mo5824nQ(this.eZk);
                m9401ak("escapeTextValue", "(Ljava/lang/Object;Lorg/mozilla/javascript/Context;)Ljava/lang/String;");
                return;
            case 76:
            case 77:
            case 78:
            case 79:
                int intProp2 = qlVar.getIntProp(16, 0);
                org.mozilla1.javascript.Node qlVar3 = cFE;
                do {
                    m9405b(qlVar3, qlVar);
                    qlVar3 = qlVar3.cFG();
                } while (qlVar3 != null);
                this.euH.mo5824nQ(this.eZk);
                switch (type) {
                    case 76:
                        str = "memberRef";
                        str2 = "(Ljava/lang/Object;Ljava/lang/Object;Lorg/mozilla/javascript/Context;I)Lorg/mozilla/javascript/Ref;";
                        break;
                    case 77:
                        str = "memberRef";
                        str2 = "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lorg/mozilla/javascript/Context;I)Lorg/mozilla/javascript/Ref;";
                        break;
                    case 78:
                        str = "nameRef";
                        str2 = "(Ljava/lang/Object;Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;I)Lorg/mozilla/javascript/Ref;";
                        this.euH.mo5824nQ(this.eZi);
                        break;
                    case 79:
                        str = "nameRef";
                        str2 = "(Ljava/lang/Object;Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;I)Lorg/mozilla/javascript/Ref;";
                        this.euH.mo5824nQ(this.eZi);
                        break;
                    default:
                        throw org.mozilla1.javascript.Kit.codeBug();
                }
                this.euH.mo5814nG(intProp2);
                m9401ak(str, str2);
                return;
            case 88:
                org.mozilla1.javascript.Node qlVar4 = cFE;
                for (org.mozilla1.javascript.Node cFG2 = cFE.cFG(); cFG2 != null; cFG2 = cFG2.cFG()) {
                    m9405b(qlVar4, qlVar);
                    this.euH.add(87);
                    qlVar4 = cFG2;
                }
                m9405b(qlVar4, qlVar);
                return;
            case 101:
                org.mozilla1.javascript.Node cFG3 = cFE.cFG();
                org.mozilla1.javascript.Node cFG4 = cFG3.cFG();
                m9405b(cFE, qlVar);
                m9401ak("toBoolean", "(Ljava/lang/Object;)Z");
                int bya8 = this.euH.bya();
                this.euH.add(153, bya8);
                short byd = this.euH.byd();
                m9405b(cFG3, qlVar);
                int bya9 = this.euH.bya();
                this.euH.add(167, bya9);
                this.euH.mo5783a(bya8, byd);
                m9405b(cFG4, qlVar);
                this.euH.mo5826nS(bya9);
                return;
            case 103:
            case 104:
                m9405b(cFE, qlVar);
                this.euH.add(89);
                m9401ak("toBoolean", "(Ljava/lang/Object;)Z");
                int bya10 = this.euH.bya();
                if (type == 104) {
                    this.euH.add(153, bya10);
                } else {
                    this.euH.add(154, bya10);
                }
                this.euH.add(87);
                m9405b(cFE.cFG(), qlVar);
                this.euH.mo5826nS(bya10);
                return;
            case 105:
            case 106:
                m9430p(qlVar);
                return;
            case 108:
                if (this.eYX != null || qlVar2.getType() != 135) {
                    OptFunctionNode b = OptFunctionNode.m11683b(this.eYU, qlVar.getExistingIntProp(1));
                    int functionType = b.eIE.getFunctionType();
                    if (functionType != 2) {
                        throw Codegen.m21129dh();
                    }
                    m9390a(b, functionType);
                    return;
                }
                return;
            case 125:
                m9405b(cFE, qlVar);
                this.euH.add(87);
                Codegen.m21135j(this.euH);
                return;
            case 136:
                m9429o(qlVar);
                return;
            case 137:
                return;
            case 145:
                m9424k(qlVar, cFE);
                return;
            case 148:
                if (cFE.getType() == 40) {
                    i = cFE.getIntProp(8, -1);
                } else {
                    i = -1;
                }
                if (i != -1) {
                    cFE.removeProp(8);
                    m9405b(cFE, qlVar);
                    cFE.putIntProp(8, i);
                    return;
                }
                m9405b(cFE, qlVar);
                bND();
                return;
            case 149:
                m9405b(cFE, qlVar);
                bNC();
                return;
            case 154:
                m9420i(qlVar, cFE);
                return;
            case 155:
                m9410c(qlVar, cFE, true);
                return;
            case 156:
                org.mozilla1.javascript.Node cFG5 = cFE.cFG();
                m9419i(cFE);
                m9405b(cFG5, qlVar);
                return;
            case 158:
                org.mozilla1.javascript.Node cFG6 = cFE.cFG();
                org.mozilla1.javascript.Node cFG7 = cFG6.cFG();
                m9419i(cFE);
                m9405b(cFG6.cFE(), cFG6);
                m9419i(cFG7);
                return;
            default:
                throw new RuntimeException("Unexpected node type " + type);
        }
        while (cFE != null) {
            m9405b(cFE, qlVar);
            cFE = cFE.cFG();
        }
        this.euH.mo5824nQ(this.eZk);
        this.euH.mo5824nQ(this.eZi);
        this.euH.mo5804gJ(qlVar.getString());
        m9401ak("bind", "(Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;Ljava/lang/String;)Lorg/mozilla/javascript/Scriptable;");
    }

    /* renamed from: a */
    private void m9400a(org.mozilla1.javascript.Node qlVar, boolean z) {
        short byd = this.euH.byd();
        this.maxStack = this.maxStack > byd ? this.maxStack : byd;
        if (this.euH.byd() != 0) {
            bNt();
            for (int i = 0; i < byd; i++) {
                this.euH.add(90);
                this.euH.add(95);
                this.euH.mo5813nF(i);
                this.euH.add(95);
                this.euH.add(83);
            }
            this.euH.add(87);
        }
        org.mozilla1.javascript.Node cFE = qlVar.cFE();
        if (cFE != null) {
            m9405b(cFE, qlVar);
        } else {
            Codegen.m21135j(this.euH);
        }
        int j = m9421j(qlVar);
        m9431pG(j);
        boolean n = m9427n(qlVar);
        this.euH.add(176);
        m9389a(m9423k(qlVar), n, j);
        if (byd != 0) {
            bNt();
            for (int i2 = 0; i2 < byd; i2++) {
                this.euH.add(89);
                this.euH.mo5813nF((byd - i2) - 1);
                this.euH.add(50);
                this.euH.add(95);
            }
            this.euH.add(87);
        }
        if (z) {
            this.euH.mo5824nQ(this.eZl);
        }
    }

    /* renamed from: a */
    private void m9389a(int i, boolean z, int i2) {
        int bya = this.euH.bya();
        int bya2 = this.euH.bya();
        this.euH.mo5826nS(bya);
        this.euH.mo5824nQ(this.eZl);
        bNz();
        this.euH.mo5826nS(bya2);
        this.euH.mo5824nQ(this.eZl);
        this.euH.add(192, "java/lang/Throwable");
        this.euH.add(191);
        if (i != -1) {
            this.euH.mo5826nS(i);
        }
        if (!z) {
            this.euH.mo5781S(this.eZu, i2);
        }
        this.euH.mo5820nM(this.eZm);
        this.euH.mo5813nF(2);
        this.euH.add(159, bya2);
        this.euH.mo5820nM(this.eZm);
        this.euH.mo5813nF(1);
        this.euH.add(159, bya);
    }

    /* renamed from: a */
    private void m9398a(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, int i, int i2) {
        int type = qlVar.getType();
        org.mozilla1.javascript.Node cFE = qlVar.cFE();
        switch (type) {
            case 12:
            case 13:
            case 46:
            case 47:
                m9409c(qlVar, cFE, i, i2);
                return;
            case 14:
            case 15:
            case 16:
            case 17:
            case 52:
            case 53:
                m9406b(qlVar, cFE, i, i2);
                return;
            case 26:
                m9398a(cFE, qlVar, i2, i);
                return;
            case 103:
            case 104:
                int bya = this.euH.bya();
                if (type == 104) {
                    m9398a(cFE, qlVar, bya, i2);
                } else {
                    m9398a(cFE, qlVar, i, bya);
                }
                this.euH.mo5826nS(bya);
                m9398a(cFE.cFG(), qlVar, i, i2);
                return;
            default:
                m9405b(qlVar, qlVar2);
                m9401ak("toBoolean", "(Ljava/lang/Object;)Z");
                this.euH.add(154, i);
                this.euH.add(167, i2);
                return;
        }
    }

    /* renamed from: a */
    private void m9390a(OptFunctionNode xq, int i) {
        int e = this.eYT.mo13018e((org.mozilla1.javascript.ScriptOrFnNode) xq.eIE);
        this.euH.add(187, this.eYT.f4379hj);
        this.euH.add(89);
        this.euH.mo5824nQ(this.eZi);
        this.euH.mo5824nQ(this.eZk);
        this.euH.mo5814nG(e);
        this.euH.mo5794b(183, this.eYT.f4379hj, "<init>", "(Lorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Context;I)V");
        this.euH.add(89);
        if (this.eYY) {
            this.euH.add(42);
        } else {
            this.euH.add(42);
            this.euH.mo5782a(180, this.eYT.f4379hj, "_dcp", this.eYT.f4380hk);
        }
        this.euH.mo5782a(181, this.eYT.f4379hj, "_dcp", this.eYT.f4380hk);
        int bFC = xq.bFC();
        if (bFC >= 0) {
            this.euH.add(89);
            if (this.eYY) {
                this.euH.add(42);
            } else {
                this.euH.add(42);
                this.euH.mo5782a(180, this.eYT.f4379hj, "_dcp", this.eYT.f4380hk);
            }
            this.euH.add(95);
            this.euH.mo5782a(181, this.eYT.f4379hj, Codegen.m21137m(bFC), this.eYT.f4380hk);
        }
        if (i != 2) {
            this.euH.mo5814nG(i);
            this.euH.mo5824nQ(this.eZi);
            this.euH.mo5824nQ(this.eZk);
            m9402al("initFunction", "(Lorg/mozilla/javascript/NativeFunction;ILorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Context;)V");
        }
    }

    /* renamed from: k */
    private int m9423k(org.mozilla1.javascript.Node qlVar) {
        int labelId = qlVar.labelId();
        if (labelId != -1) {
            return labelId;
        }
        int bya = this.euH.bya();
        qlVar.labelId(bya);
        return bya;
    }

    /* renamed from: a */
    private void m9391a(org.mozilla1.javascript.Node.C3367a aVar, int i, org.mozilla1.javascript.Node qlVar) {
        org.mozilla1.javascript.Node qlVar2 = aVar.aUM;
        if (i == 6 || i == 7) {
            if (qlVar == null) {
                throw Codegen.m21129dh();
            }
            int k = m9423k(qlVar2);
            int bya = this.euH.bya();
            if (i == 6) {
                m9398a(qlVar, (org.mozilla1.javascript.Node) aVar, k, bya);
            } else {
                m9398a(qlVar, (org.mozilla1.javascript.Node) aVar, bya, k);
            }
            this.euH.mo5826nS(bya);
        } else if (i != 134) {
            m9393a(qlVar2, 167);
        } else if (this.eZt) {
            m9425l(qlVar2);
        } else {
            m9393a(qlVar2, 168);
        }
    }

    /* renamed from: l */
    private void m9425l(org.mozilla1.javascript.Node qlVar) {
        C1238a aVar = this.eZv.get(qlVar);
        this.euH.mo5813nF(aVar.efR.size());
        m9393a(qlVar, 167);
        int bya = this.euH.bya();
        this.euH.mo5826nS(bya);
        aVar.efR.add(Integer.valueOf(bya));
    }

    /* renamed from: c */
    private void m9408c(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        int i = 0;
        for (org.mozilla1.javascript.Node qlVar3 = qlVar2; qlVar3 != null; qlVar3 = qlVar3.cFG()) {
            i++;
        }
        m9436pL(i);
        for (int i2 = 0; i2 != i; i2++) {
            this.euH.add(89);
            this.euH.mo5814nG(i2);
            m9405b(qlVar2, qlVar);
            this.euH.add(83);
            qlVar2 = qlVar2.cFG();
        }
        int[] iArr = (int[]) qlVar.getProp(11);
        if (iArr == null) {
            this.euH.add(1);
            this.euH.add(3);
        } else {
            this.euH.mo5804gJ(OptRuntime.m28000d(iArr));
            this.euH.mo5814nG(iArr.length);
        }
        this.euH.mo5824nQ(this.eZk);
        this.euH.mo5824nQ(this.eZi);
        m9402al("newArrayLiteral", "([Ljava/lang/Object;Ljava/lang/String;ILorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;)Lorg/mozilla/javascript/Scriptable;");
    }

    /* renamed from: d */
    private void m9411d(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        Object[] objArr = (Object[]) qlVar.getProp(12);
        int length = objArr.length;
        m9436pL(length);
        for (int i = 0; i != length; i++) {
            this.euH.add(89);
            this.euH.mo5814nG(i);
            Object obj = objArr[i];
            if (obj instanceof String) {
                this.euH.mo5804gJ((String) obj);
            } else {
                this.euH.mo5814nG(((Integer) obj).intValue());
                m9401ak("wrapInt", "(I)Ljava/lang/Integer;");
            }
            this.euH.add(83);
        }
        m9436pL(length);
        org.mozilla1.javascript.Node qlVar3 = qlVar2;
        for (int i2 = 0; i2 != length; i2++) {
            this.euH.add(89);
            this.euH.mo5814nG(i2);
            int type = qlVar3.getType();
            if (type == 150) {
                m9405b(qlVar3.cFE(), qlVar);
            } else if (type == 151) {
                m9405b(qlVar3.cFE(), qlVar);
            } else {
                m9405b(qlVar3, qlVar);
            }
            this.euH.add(83);
            qlVar3 = qlVar3.cFG();
        }
        this.euH.mo5814nG(length);
        this.euH.add(188, 10);
        for (int i3 = 0; i3 != length; i3++) {
            this.euH.add(89);
            this.euH.mo5814nG(i3);
            int type2 = qlVar2.getType();
            if (type2 == 150) {
                this.euH.add(2);
            } else if (type2 == 151) {
                this.euH.add(4);
            } else {
                this.euH.add(3);
            }
            this.euH.add(79);
            qlVar2 = qlVar2.cFG();
        }
        this.euH.mo5824nQ(this.eZk);
        this.euH.mo5824nQ(this.eZi);
        m9401ak("newObjectLiteral", "([Ljava/lang/Object;[Ljava/lang/Object;[ILorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;)Lorg/mozilla/javascript/Scriptable;");
    }

    /* renamed from: a */
    private void m9394a(org.mozilla1.javascript.Node qlVar, int i, int i2, org.mozilla1.javascript.Node qlVar2) {
        String str;
        String str2;
        this.euH.mo5824nQ(this.eZk);
        if (i == 30) {
            m9405b(qlVar2, qlVar);
        } else {
            m9417g(qlVar2, qlVar);
        }
        m9399a(qlVar, qlVar2.cFG(), false);
        if (i == 30) {
            str2 = "(Lorg/mozilla/javascript/Context;Ljava/lang/Object;[Ljava/lang/Object;Lorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Scriptable;I)Ljava/lang/Object;";
            this.euH.mo5824nQ(this.eZi);
            this.euH.mo5824nQ(this.eZn);
            this.euH.mo5814nG(i2);
            str = "newObjectSpecial";
        } else {
            str = "callSpecial";
            this.euH.mo5824nQ(this.eZi);
            this.euH.mo5824nQ(this.eZn);
            this.euH.mo5814nG(i2);
            String sourceName = this.eYU.getSourceName();
            org.mozilla1.classfile.ClassFileWriter uh = this.euH;
            if (sourceName == null) {
                sourceName = "";
            }
            uh.mo5804gJ(sourceName);
            this.euH.mo5814nG(this.eZc);
            str2 = "(Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Callable;Lorg/mozilla/javascript/Scriptable;[Ljava/lang/Object;Lorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Scriptable;ILjava/lang/String;I)Ljava/lang/Object;";
        }
        m9402al(str, str2);
    }

    /* renamed from: e */
    private void m9414e(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        String str;
        String str2;
        if (qlVar.getType() != 38) {
            throw Codegen.m21129dh();
        }
        org.mozilla1.javascript.Node cFG = qlVar2.cFG();
        int type = qlVar2.getType();
        if (cFG == null) {
            if (type == 39) {
                this.euH.mo5804gJ(qlVar2.getString());
                str = "callName0";
                str2 = "(Ljava/lang/String;Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;)Ljava/lang/Object;";
            } else if (type == 33) {
                org.mozilla1.javascript.Node cFE = qlVar2.cFE();
                m9405b(cFE, qlVar);
                this.euH.mo5804gJ(cFE.cFG().getString());
                str = "callProp0";
                str2 = "(Ljava/lang/Object;Ljava/lang/String;Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;)Ljava/lang/Object;";
            } else if (type == 34) {
                throw org.mozilla1.javascript.Kit.codeBug();
            } else {
                m9417g(qlVar2, qlVar);
                str = "call0";
                str2 = "(Lorg/mozilla/javascript/Callable;Lorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;)Ljava/lang/Object;";
            }
        } else if (type == 39) {
            String string = qlVar2.getString();
            m9399a(qlVar, cFG, false);
            this.euH.mo5804gJ(string);
            str = "callName";
            str2 = "([Ljava/lang/Object;Ljava/lang/String;Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;)Ljava/lang/Object;";
        } else {
            int i = 0;
            for (org.mozilla1.javascript.Node qlVar3 = cFG; qlVar3 != null; qlVar3 = qlVar3.cFG()) {
                i++;
            }
            m9417g(qlVar2, qlVar);
            if (i == 1) {
                m9405b(cFG, qlVar);
                str = "call1";
                str2 = "(Lorg/mozilla/javascript/Callable;Lorg/mozilla/javascript/Scriptable;Ljava/lang/Object;Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;)Ljava/lang/Object;";
            } else if (i == 2) {
                m9405b(cFG, qlVar);
                m9405b(cFG.cFG(), qlVar);
                str = "call2";
                str2 = "(Lorg/mozilla/javascript/Callable;Lorg/mozilla/javascript/Scriptable;Ljava/lang/Object;Ljava/lang/Object;Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;)Ljava/lang/Object;";
            } else {
                m9399a(qlVar, cFG, false);
                str = "callN";
                str2 = "(Lorg/mozilla/javascript/Callable;Lorg/mozilla/javascript/Scriptable;[Ljava/lang/Object;Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;)Ljava/lang/Object;";
            }
        }
        this.euH.mo5824nQ(this.eZk);
        this.euH.mo5824nQ(this.eZi);
        m9402al(str, str2);
    }

    /* renamed from: f */
    private void m9416f(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        if (qlVar.getType() != 30) {
            throw Codegen.m21129dh();
        }
        org.mozilla1.javascript.Node cFG = qlVar2.cFG();
        m9405b(qlVar2, qlVar);
        this.euH.mo5824nQ(this.eZk);
        this.euH.mo5824nQ(this.eZi);
        m9399a(qlVar, cFG, false);
        m9401ak("newObject", "(Ljava/lang/Object;Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;[Ljava/lang/Object;)Lorg/mozilla/javascript/Scriptable;");
    }

    /* renamed from: a */
    private void m9397a(org.mozilla1.javascript.Node qlVar, OptFunctionNode xq, int i, org.mozilla1.javascript.Node qlVar2) {
        String g;
        org.mozilla1.javascript.Node cFG = qlVar2.cFG();
        short s = 0;
        if (i == 30) {
            m9405b(qlVar2, qlVar);
        } else {
            m9417g(qlVar2, qlVar);
            s = bNE();
            this.euH.mo5819nL(s);
        }
        int bya = this.euH.bya();
        int bFC = xq.bFC();
        if (this.eYY) {
            this.euH.add(42);
        } else {
            this.euH.add(42);
            this.euH.mo5782a(180, this.eYT.f4379hj, "_dcp", this.eYT.f4380hk);
        }
        this.euH.mo5782a(180, this.eYT.f4379hj, Codegen.m21137m(bFC), this.eYT.f4380hk);
        this.euH.add(92);
        int bya2 = this.euH.bya();
        this.euH.add(166, bya2);
        short byd = this.euH.byd();
        this.euH.add(95);
        this.euH.add(87);
        if (this.f1538he.isUseDynamicScope()) {
            this.euH.mo5824nQ(this.eZk);
            this.euH.mo5824nQ(this.eZi);
        } else {
            this.euH.add(89);
            this.euH.mo5794b(185, "org/mozilla/javascript/Scriptable", "getParentScope", "()Lorg/mozilla/javascript/Scriptable;");
            this.euH.mo5824nQ(this.eZk);
            this.euH.add(95);
        }
        if (i == 30) {
            this.euH.add(1);
        } else {
            this.euH.mo5824nQ(s);
        }
        for (org.mozilla1.javascript.Node qlVar3 = cFG; qlVar3 != null; qlVar3 = qlVar3.cFG()) {
            int r = m9439r(qlVar3);
            if (r >= 0) {
                this.euH.mo5824nQ(r);
                this.euH.mo5823nP(r + 1);
            } else if (qlVar3.getIntProp(8, -1) == 0) {
                this.euH.mo5782a(178, "java/lang/Void", "TYPE", "Ljava/lang/Class;");
                m9405b(qlVar3, qlVar);
            } else {
                m9405b(qlVar3, qlVar);
                this.euH.mo5779F(ScriptRuntime.NaN);
            }
        }
        this.euH.mo5782a(178, "org/mozilla/javascript/ScriptRuntime", "emptyArgs", "[Ljava/lang/Object;");
        ClassFileWriter uh = this.euH;
        String str = this.eYT.f4379hj;
        if (i == 30) {
            g = this.eYT.mo13019f((org.mozilla1.javascript.ScriptOrFnNode) xq.eIE);
        } else {
            g = this.eYT.mo13020g((org.mozilla1.javascript.ScriptOrFnNode) xq.eIE);
        }
        uh.mo5794b(184, str, g, this.eYT.mo13021h((org.mozilla1.javascript.ScriptOrFnNode) xq.eIE));
        this.euH.add(167, bya);
        this.euH.mo5783a(bya2, byd);
        this.euH.add(87);
        this.euH.mo5824nQ(this.eZk);
        this.euH.mo5824nQ(this.eZi);
        if (i != 30) {
            this.euH.mo5824nQ(s);
            m9442s(s);
        }
        m9399a(qlVar, cFG, true);
        if (i == 30) {
            m9401ak("newObject", "(Ljava/lang/Object;Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;[Ljava/lang/Object;)Lorg/mozilla/javascript/Scriptable;");
        } else {
            this.euH.mo5794b(185, "org/mozilla/javascript/Callable", "call", "(Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Scriptable;[Ljava/lang/Object;)Ljava/lang/Object;");
        }
        this.euH.mo5826nS(bya);
    }

    /* renamed from: a */
    private void m9399a(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, boolean z) {
        int i = 0;
        for (org.mozilla1.javascript.Node qlVar3 = qlVar2; qlVar3 != null; qlVar3 = qlVar3.cFG()) {
            i++;
        }
        if (i != 1 || this.eZq < 0) {
            m9436pL(i);
        } else {
            this.euH.mo5824nQ(this.eZq);
        }
        for (int i2 = 0; i2 != i; i2++) {
            if (!this.eZt) {
                this.euH.add(89);
                this.euH.mo5814nG(i2);
            }
            if (!z) {
                m9405b(qlVar2, qlVar);
            } else {
                int r = m9439r(qlVar2);
                if (r >= 0) {
                    m9435pK(r);
                } else {
                    m9405b(qlVar2, qlVar);
                    if (qlVar2.getIntProp(8, -1) == 0) {
                        bND();
                    }
                }
            }
            if (this.eZt) {
                short bNE = bNE();
                this.euH.mo5819nL(bNE);
                this.euH.add(192, "[Ljava/lang/Object;");
                this.euH.add(89);
                this.euH.mo5814nG(i2);
                this.euH.mo5824nQ(bNE);
                m9442s(bNE);
            }
            this.euH.add(83);
            qlVar2 = qlVar2.cFG();
        }
    }

    /* renamed from: g */
    private void m9417g(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        int type = qlVar.getType();
        switch (qlVar.getType()) {
            case 33:
            case 36:
                org.mozilla1.javascript.Node cFE = qlVar.cFE();
                m9405b(cFE, qlVar);
                org.mozilla1.javascript.Node cFG = cFE.cFG();
                if (type != 33) {
                    if (qlVar.getIntProp(8, -1) == -1) {
                        m9405b(cFG, qlVar);
                        this.euH.mo5824nQ(this.eZk);
                        m9401ak("getElemFunctionAndThis", "(Ljava/lang/Object;Ljava/lang/Object;Lorg/mozilla/javascript/Context;)Lorg/mozilla/javascript/Callable;");
                        break;
                    } else {
                        throw Codegen.m21129dh();
                    }
                } else {
                    this.euH.mo5804gJ(cFG.getString());
                    this.euH.mo5824nQ(this.eZk);
                    m9401ak("getPropFunctionAndThis", "(Ljava/lang/Object;Ljava/lang/String;Lorg/mozilla/javascript/Context;)Lorg/mozilla/javascript/Callable;");
                    break;
                }
            case 34:
                throw org.mozilla1.javascript.Kit.codeBug();
            case 39:
                this.euH.mo5804gJ(qlVar.getString());
                this.euH.mo5824nQ(this.eZk);
                this.euH.mo5824nQ(this.eZi);
                m9401ak("getNameFunctionAndThis", "(Ljava/lang/String;Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;)Lorg/mozilla/javascript/Callable;");
                break;
            default:
                m9405b(qlVar, qlVar2);
                this.euH.mo5824nQ(this.eZk);
                m9401ak("getValueFunctionAndThis", "(Ljava/lang/Object;Lorg/mozilla/javascript/Context;)Lorg/mozilla/javascript/Callable;");
                break;
        }
        this.euH.mo5824nQ(this.eZk);
        m9401ak("lastStoredScriptable", "(Lorg/mozilla/javascript/Context;)Lorg/mozilla/javascript/Scriptable;");
    }

    /* renamed from: m */
    private void m9426m(org.mozilla1.javascript.Node qlVar) {
        this.eZc = qlVar.getLineno();
        if (this.eZc != -1) {
            this.euH.mo5831p((short) this.eZc);
        }
    }

    /* renamed from: a */
    private void m9392a(org.mozilla1.javascript.Node.C3367a aVar, org.mozilla1.javascript.Node qlVar) {
        short bNE = bNE();
        this.euH.mo5824nQ(this.eZi);
        this.euH.mo5819nL(bNE);
        int bya = this.euH.bya();
        this.euH.mo5783a(bya, (short) 0);
        org.mozilla1.javascript.Node qlVar2 = aVar.aUM;
        org.mozilla1.javascript.Node WP = aVar.mo21497WP();
        if (this.eZt && WP != null) {
            C1238a aVar2 = new C1238a();
            if (this.eZv == null) {
                this.eZv = new HashMap();
            }
            this.eZv.put(WP, aVar2);
            this.eZv.put(WP.cFG(), aVar2);
        }
        while (qlVar != null) {
            m9419i(qlVar);
            qlVar = qlVar.cFG();
        }
        int bya2 = this.euH.bya();
        this.euH.add(167, bya2);
        int t = m9443t(aVar);
        if (qlVar2 != null) {
            int labelId = qlVar2.labelId();
            m9388a(0, bNE, labelId, bya, t);
            m9388a(1, bNE, labelId, bya, t);
            m9388a(2, bNE, labelId, bya, t);
            org.mozilla1.javascript.Context bwq = org.mozilla1.javascript.Context.bwq();
            if (bwq != null && bwq.hasFeature(13)) {
                m9388a(3, bNE, labelId, bya, t);
            }
        }
        if (WP != null) {
            int bya3 = this.euH.bya();
            this.euH.mo5827nT(bya3);
            this.euH.mo5819nL(t);
            this.euH.mo5824nQ(bNE);
            this.euH.mo5819nL(this.eZi);
            int labelId2 = WP.labelId();
            if (this.eZt) {
                m9425l(WP);
            } else {
                this.euH.add(168, labelId2);
            }
            this.euH.mo5824nQ(t);
            if (this.eZt) {
                this.euH.add(192, "java/lang/Throwable");
            }
            this.euH.add(191);
            this.euH.addExceptionHandler(bya, labelId2, bya3, (String) null);
        }
        m9442s(bNE);
        this.euH.mo5826nS(bya2);
    }

    /* renamed from: a */
    private void m9388a(int i, short s, int i2, int i3, int i4) {
        String str;
        int bya = this.euH.bya();
        this.euH.mo5827nT(bya);
        this.euH.mo5819nL(i4);
        this.euH.mo5824nQ(s);
        this.euH.mo5819nL(this.eZi);
        if (i == 0) {
            str = "org/mozilla/javascript/JavaScriptException";
        } else if (i == 1) {
            str = "org/mozilla/javascript/EvaluatorException";
        } else if (i == 2) {
            str = "org/mozilla/javascript/EcmaError";
        } else if (i == 3) {
            str = "java/lang/Throwable";
        } else {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        this.euH.addExceptionHandler(i3, i2, bya, str);
        this.euH.add(167, i2);
    }

    /* renamed from: n */
    private boolean m9427n(org.mozilla1.javascript.Node qlVar) {
        int i = 0;
        for (int i2 = 0; i2 < this.eZa; i2++) {
            if (this.eYZ[i2] != 0) {
                i++;
            }
        }
        if (i == 0) {
            ((org.mozilla1.javascript.FunctionNode) this.eYU).mo7180a(qlVar, (int[]) null);
            return false;
        }
        this.maxLocals = this.maxLocals > i ? this.maxLocals : i;
        int[] iArr = new int[i];
        int i3 = 0;
        for (int i4 = 0; i4 < this.eZa; i4++) {
            if (this.eYZ[i4] != 0) {
                iArr[i3] = i4;
                i3++;
            }
        }
        ((org.mozilla1.javascript.FunctionNode) this.eYU).mo7180a(qlVar, iArr);
        bNv();
        for (int i5 = 0; i5 < i; i5++) {
            this.euH.add(89);
            this.euH.mo5813nF(i5);
            this.euH.mo5824nQ(iArr[i5]);
            this.euH.add(83);
        }
        this.euH.add(87);
        return true;
    }

    /* renamed from: b */
    private void m9404b(org.mozilla1.javascript.Node.C3367a aVar, org.mozilla1.javascript.Node qlVar) {
        m9405b(qlVar, (org.mozilla1.javascript.Node) aVar);
        short bNE = bNE();
        this.euH.mo5819nL(bNE);
        for (org.mozilla1.javascript.Node.C3367a aVar2 = (org.mozilla1.javascript.Node.C3367a) qlVar.cFG(); aVar2 != null; aVar2 = (org.mozilla1.javascript.Node.C3367a) aVar2.cFG()) {
            if (aVar2.getType() != 114) {
                throw Codegen.m21129dh();
            }
            m9405b(aVar2.cFE(), (org.mozilla1.javascript.Node) aVar2);
            this.euH.mo5824nQ(bNE);
            m9401ak("shallowEq", "(Ljava/lang/Object;Ljava/lang/Object;)Z");
            m9393a(aVar2.aUM, 154);
        }
        m9442s(bNE);
    }

    /* renamed from: o */
    private void m9429o(org.mozilla1.javascript.Node qlVar) {
        int K;
        if (!this.eZd || (K = this.eYX.eIE.mo7678K(qlVar)) < 0) {
            this.euH.mo5824nQ(this.eZi);
            this.euH.mo5804gJ(qlVar.getString());
            m9401ak("typeofName", "(Lorg/mozilla/javascript/Scriptable;Ljava/lang/String;)Ljava/lang/String;");
        } else if (this.eYX.mo6841pe(K)) {
            this.euH.mo5804gJ("number");
        } else if (m9433pI(K)) {
            short s = this.eZe[K];
            this.euH.mo5824nQ(s);
            this.euH.mo5782a(178, "java/lang/Void", "TYPE", "Ljava/lang/Class;");
            int bya = this.euH.bya();
            this.euH.add(165, bya);
            short byd = this.euH.byd();
            this.euH.mo5824nQ(s);
            m9401ak("typeof", "(Ljava/lang/Object;)Ljava/lang/String;");
            int bya2 = this.euH.bya();
            this.euH.add(167, bya2);
            this.euH.mo5783a(bya, byd);
            this.euH.mo5804gJ("number");
            this.euH.mo5826nS(bya2);
        } else {
            this.euH.mo5824nQ(this.eZe[K]);
            m9401ak("typeof", "(Ljava/lang/Object;)Ljava/lang/String;");
        }
    }

    private void bNA() {
        this.eYW = this.euH.byc();
    }

    private void bNB() {
        int byc = this.euH.byc() - this.eYW;
        if (byc != 0) {
            m9432pH(byc);
        }
    }

    /* renamed from: pH */
    private void m9432pH(int i) {
        this.euH.mo5824nQ(this.eZk);
        this.euH.mo5814nG(i);
        m9401ak("addInstructionCount", "(Lorg/mozilla/javascript/Context;I)V");
    }

    /* renamed from: p */
    private void m9430p(org.mozilla1.javascript.Node qlVar) {
        short s = 1;
        int existingIntProp = qlVar.getExistingIntProp(13);
        org.mozilla1.javascript.Node cFE = qlVar.cFE();
        switch (cFE.getType()) {
            case 33:
                org.mozilla1.javascript.Node cFE2 = cFE.cFE();
                m9405b(cFE2, qlVar);
                m9405b(cFE2.cFG(), qlVar);
                this.euH.mo5824nQ(this.eZk);
                this.euH.mo5814nG(existingIntProp);
                m9401ak("propIncrDecr", "(Ljava/lang/Object;Ljava/lang/String;Lorg/mozilla/javascript/Context;I)Ljava/lang/Object;");
                return;
            case 34:
                throw org.mozilla1.javascript.Kit.codeBug();
            case 36:
                org.mozilla1.javascript.Node cFE3 = cFE.cFE();
                m9405b(cFE3, qlVar);
                m9405b(cFE3.cFG(), qlVar);
                this.euH.mo5824nQ(this.eZk);
                this.euH.mo5814nG(existingIntProp);
                if (cFE3.cFG().getIntProp(8, -1) != -1) {
                    m9402al("elemIncrDecr", "(Ljava/lang/Object;DLorg/mozilla/javascript/Context;I)Ljava/lang/Object;");
                    return;
                } else {
                    m9401ak("elemIncrDecr", "(Ljava/lang/Object;Ljava/lang/Object;Lorg/mozilla/javascript/Context;I)Ljava/lang/Object;");
                    return;
                }
            case 39:
                this.euH.mo5824nQ(this.eZi);
                this.euH.mo5804gJ(cFE.getString());
                this.euH.mo5824nQ(this.eZk);
                this.euH.mo5814nG(existingIntProp);
                m9401ak("nameIncrDecr", "(Lorg/mozilla/javascript/Scriptable;Ljava/lang/String;Lorg/mozilla/javascript/Context;I)Ljava/lang/Object;");
                return;
            case 55:
                if (!this.eZd) {
                    org.mozilla1.javascript.Kit.codeBug();
                }
                if (qlVar.getIntProp(8, -1) != -1) {
                    boolean z = (existingIntProp & 2) != 0;
                    int g = this.eYX.mo6838g(cFE);
                    short s2 = this.eZe[g];
                    if (!m9433pI(g)) {
                        s = 0;
                    }
                    this.euH.mo5823nP(s2 + s);
                    if (z) {
                        this.euH.add(92);
                    }
                    this.euH.mo5779F(1.0d);
                    if ((existingIntProp & 1) == 0) {
                        this.euH.add(99);
                    } else {
                        this.euH.add(103);
                    }
                    if (!z) {
                        this.euH.add(92);
                    }
                    this.euH.mo5818nK(s + s2);
                    return;
                }
                if ((existingIntProp & 2) == 0) {
                    s = 0;
                }
                short s3 = this.eZe[this.eYX.mo6838g(cFE)];
                this.euH.mo5824nQ(s3);
                if (s != 0) {
                    this.euH.add(89);
                }
                bNC();
                this.euH.mo5779F(1.0d);
                if ((existingIntProp & 1) == 0) {
                    this.euH.add(99);
                } else {
                    this.euH.add(103);
                }
                bND();
                if (s == 0) {
                    this.euH.add(89);
                }
                this.euH.mo5819nL(s3);
                return;
            case 67:
                m9405b(cFE.cFE(), qlVar);
                this.euH.mo5824nQ(this.eZk);
                this.euH.mo5814nG(existingIntProp);
                m9401ak("refIncrDecr", "(Lorg/mozilla/javascript/Ref;Lorg/mozilla/javascript/Context;I)Ljava/lang/Object;");
                return;
            default:
                Codegen.m21129dh();
                return;
        }
    }

    /* renamed from: a */
    private void m9396a(org.mozilla1.javascript.Node qlVar, int i, org.mozilla1.javascript.Node qlVar2, org.mozilla1.javascript.Node qlVar3) {
        if (qlVar.getIntProp(8, -1) != -1) {
            m9405b(qlVar2, qlVar);
            m9405b(qlVar2.cFG(), qlVar);
            this.euH.add(i);
            return;
        }
        boolean q = m9438q(qlVar3);
        m9405b(qlVar2, qlVar);
        if (!m9438q(qlVar2)) {
            bNC();
        }
        m9405b(qlVar2.cFG(), qlVar);
        if (!m9438q(qlVar2.cFG())) {
            bNC();
        }
        this.euH.add(i);
        if (!q) {
            bND();
        }
    }

    /* renamed from: a */
    private void m9395a(org.mozilla1.javascript.Node qlVar, int i, org.mozilla1.javascript.Node qlVar2) {
        int intProp = qlVar.getIntProp(8, -1);
        m9405b(qlVar2, qlVar);
        if (i == 20) {
            m9401ak("toUint32", "(Ljava/lang/Object;)J");
            m9405b(qlVar2.cFG(), qlVar);
            m9401ak("toInt32", "(Ljava/lang/Object;)I");
            this.euH.mo5814nG(31);
            this.euH.add(126);
            this.euH.add(125);
            this.euH.add(138);
            bND();
            return;
        }
        if (intProp == -1) {
            m9401ak("toInt32", "(Ljava/lang/Object;)I");
            m9405b(qlVar2.cFG(), qlVar);
            m9401ak("toInt32", "(Ljava/lang/Object;)I");
        } else {
            m9401ak("toInt32", "(D)I");
            m9405b(qlVar2.cFG(), qlVar);
            m9401ak("toInt32", "(D)I");
        }
        switch (i) {
            case 9:
                this.euH.add(128);
                break;
            case 10:
                this.euH.add(130);
                break;
            case 11:
                this.euH.add(126);
                break;
            case 18:
                this.euH.add(120);
                break;
            case 19:
                this.euH.add(122);
                break;
            default:
                throw Codegen.m21129dh();
        }
        this.euH.add(135);
        if (intProp == -1) {
            bND();
        }
    }

    /* renamed from: r */
    private int m9439r(org.mozilla1.javascript.Node qlVar) {
        if (qlVar.getType() == 55 && this.aKI && !this.eZf) {
            int g = this.eYX.mo6838g(qlVar);
            if (this.eYX.mo6840pd(g)) {
                return this.eZe[g];
            }
        }
        return -1;
    }

    /* renamed from: pI */
    private boolean m9433pI(int i) {
        return this.eYX.mo6840pd(i) && this.aKI && !this.eZf;
    }

    /* renamed from: o */
    private void m9428o(int i, int i2, int i3) {
        if (i2 == -1) {
            throw Codegen.m21129dh();
        }
        switch (i) {
            case 14:
                this.euH.add(152);
                this.euH.add(155, i2);
                break;
            case 15:
                this.euH.add(152);
                this.euH.add(158, i2);
                break;
            case 16:
                this.euH.add(151);
                this.euH.add(157, i2);
                break;
            case 17:
                this.euH.add(151);
                this.euH.add(156, i2);
                break;
            default:
                throw Codegen.m21129dh();
        }
        if (i3 != -1) {
            this.euH.add(167, i3);
        }
    }

    /* renamed from: b */
    private void m9406b(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, int i, int i2) {
        if (i == -1 || i2 == -1) {
            throw Codegen.m21129dh();
        }
        int type = qlVar.getType();
        org.mozilla1.javascript.Node cFG = qlVar2.cFG();
        if (type == 53 || type == 52) {
            m9405b(qlVar2, qlVar);
            m9405b(cFG, qlVar);
            this.euH.mo5824nQ(this.eZk);
            m9401ak(type == 53 ? "instanceOf" : "in", "(Ljava/lang/Object;Ljava/lang/Object;Lorg/mozilla/javascript/Context;)Z");
            this.euH.add(154, i);
            this.euH.add(167, i2);
            return;
        }
        int intProp = qlVar.getIntProp(8, -1);
        int r = m9439r(qlVar2);
        int r2 = m9439r(cFG);
        if (intProp != -1) {
            if (intProp != 2) {
                m9405b(qlVar2, qlVar);
            } else if (r != -1) {
                m9434pJ(r);
            } else {
                m9405b(qlVar2, qlVar);
                bNC();
            }
            if (intProp != 1) {
                m9405b(cFG, qlVar);
            } else if (r2 != -1) {
                m9434pJ(r2);
            } else {
                m9405b(cFG, qlVar);
                bNC();
            }
            m9428o(type, i, i2);
            return;
        }
        if (r == -1 || r2 == -1) {
            m9405b(qlVar2, qlVar);
            m9405b(cFG, qlVar);
        } else {
            short byd = this.euH.byd();
            int bya = this.euH.bya();
            this.euH.mo5824nQ(r);
            this.euH.mo5782a(178, "java/lang/Void", "TYPE", "Ljava/lang/Class;");
            this.euH.add(166, bya);
            this.euH.mo5823nP(r + 1);
            m9434pJ(r2);
            m9428o(type, i, i2);
            if (byd != this.euH.byd()) {
                throw Codegen.m21129dh();
            }
            this.euH.mo5826nS(bya);
            int bya2 = this.euH.bya();
            this.euH.mo5824nQ(r2);
            this.euH.mo5782a(178, "java/lang/Void", "TYPE", "Ljava/lang/Class;");
            this.euH.add(166, bya2);
            this.euH.mo5824nQ(r);
            bNC();
            this.euH.mo5823nP(r2 + 1);
            m9428o(type, i, i2);
            if (byd != this.euH.byd()) {
                throw Codegen.m21129dh();
            }
            this.euH.mo5826nS(bya2);
            this.euH.mo5824nQ(r);
            this.euH.mo5824nQ(r2);
        }
        if (type == 17 || type == 16) {
            this.euH.add(95);
        }
        m9401ak((type == 14 || type == 16) ? "cmp_LT" : "cmp_LE", "(Ljava/lang/Object;Ljava/lang/Object;)Z");
        this.euH.add(154, i);
        this.euH.add(167, i2);
    }

    /* renamed from: c */
    private void m9409c(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, int i, int i2) {
        int i3;
        int i4;
        String str;
        int i5;
        if (i == -1 || i2 == -1) {
            throw Codegen.m21129dh();
        }
        short byd = this.euH.byd();
        int type = qlVar.getType();
        org.mozilla1.javascript.Node cFG = qlVar2.cFG();
        if (qlVar2.getType() == 42 || cFG.getType() == 42) {
            if (qlVar2.getType() == 42) {
                qlVar2 = cFG;
            }
            m9405b(qlVar2, qlVar);
            if (type == 46 || type == 47) {
                this.euH.add(type == 46 ? 198 : 199, i);
                i3 = i2;
            } else {
                if (type == 12) {
                    i3 = i2;
                    i4 = i;
                } else if (type != 13) {
                    throw Codegen.m21129dh();
                } else {
                    i3 = i;
                    i4 = i2;
                }
                this.euH.add(89);
                int bya = this.euH.bya();
                this.euH.add(199, bya);
                short byd2 = this.euH.byd();
                this.euH.add(87);
                this.euH.add(167, i4);
                this.euH.mo5783a(bya, byd2);
                Codegen.m21135j(this.euH);
                this.euH.add(165, i4);
            }
            this.euH.add(167, i3);
        } else {
            int r = m9439r(qlVar2);
            if (r != -1 && cFG.getType() == 148) {
                org.mozilla1.javascript.Node cFE = cFG.cFE();
                if (cFE.getType() == 40) {
                    this.euH.mo5824nQ(r);
                    this.euH.mo5782a(178, "java/lang/Void", "TYPE", "Ljava/lang/Class;");
                    int bya2 = this.euH.bya();
                    this.euH.add(166, bya2);
                    this.euH.mo5823nP(r + 1);
                    this.euH.mo5779F(cFE.getDouble());
                    this.euH.add(151);
                    if (type == 12) {
                        this.euH.add(153, i);
                    } else {
                        this.euH.add(154, i);
                    }
                    this.euH.add(167, i2);
                    this.euH.mo5826nS(bya2);
                }
            }
            m9405b(qlVar2, qlVar);
            m9405b(cFG, qlVar);
            switch (type) {
                case 12:
                    str = "eq";
                    i5 = 154;
                    break;
                case 13:
                    str = "eq";
                    i5 = 153;
                    break;
                case 46:
                    str = "shallowEq";
                    i5 = 154;
                    break;
                case 47:
                    str = "shallowEq";
                    i5 = 153;
                    break;
                default:
                    throw Codegen.m21129dh();
            }
            m9401ak(str, "(Ljava/lang/Object;Ljava/lang/Object;)Z");
            this.euH.add(i5, i);
            this.euH.add(167, i2);
        }
        if (byd != this.euH.byd()) {
            throw Codegen.m21129dh();
        }
    }

    /* renamed from: h */
    private void m9418h(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        String string = qlVar.cFE().getString();
        while (qlVar2 != null) {
            m9405b(qlVar2, qlVar);
            qlVar2 = qlVar2.cFG();
        }
        this.euH.mo5824nQ(this.eZk);
        this.euH.mo5824nQ(this.eZi);
        this.euH.mo5804gJ(string);
        m9401ak("setName", "(Lorg/mozilla/javascript/Scriptable;Ljava/lang/Object;Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;Ljava/lang/String;)Ljava/lang/Object;");
    }

    /* renamed from: i */
    private void m9420i(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        String string = qlVar.cFE().getString();
        while (qlVar2 != null) {
            m9405b(qlVar2, qlVar);
            qlVar2 = qlVar2.cFG();
        }
        this.euH.mo5824nQ(this.eZk);
        this.euH.mo5804gJ(string);
        m9401ak("setConst", "(Lorg/mozilla/javascript/Scriptable;Ljava/lang/Object;Lorg/mozilla/javascript/Context;Ljava/lang/String;)Ljava/lang/Object;");
    }

    /* renamed from: s */
    private void m9441s(org.mozilla1.javascript.Node qlVar) {
        if (!this.eZd) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        int g = this.eYX.mo6838g(qlVar);
        short s = this.eZe[g];
        if (m9433pI(g)) {
            if (qlVar.getIntProp(8, -1) != -1) {
                m9434pJ(s);
            } else {
                m9435pK(s);
            }
        } else if (this.eYX.mo6841pe(g)) {
            this.euH.mo5823nP(s);
        } else {
            this.euH.mo5824nQ(s);
        }
    }

    /* renamed from: b */
    private void m9407b(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, boolean z) {
        if (!this.eZd) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        int g = this.eYX.mo6838g(qlVar);
        m9405b(qlVar2.cFG(), qlVar);
        boolean z2 = qlVar.getIntProp(8, -1) != -1;
        short s = this.eZe[g];
        if (this.eYX.eIE.cHr()[g]) {
            if (z) {
                return;
            }
            if (z2) {
                this.euH.add(88);
            } else {
                this.euH.add(87);
            }
        } else if (!m9433pI(g)) {
            boolean pe = this.eYX.mo6841pe(g);
            if (!z2) {
                if (pe) {
                    org.mozilla1.javascript.Kit.codeBug();
                }
                this.euH.mo5819nL(s);
                if (z) {
                    this.euH.mo5824nQ(s);
                }
            } else if (pe) {
                this.euH.mo5818nK(s);
                if (z) {
                    this.euH.mo5823nP(s);
                }
            } else {
                if (z) {
                    this.euH.add(92);
                }
                bND();
                this.euH.mo5819nL(s);
            }
        } else if (z2) {
            if (z) {
                this.euH.add(92);
            }
            this.euH.mo5824nQ(s);
            this.euH.mo5782a(178, "java/lang/Void", "TYPE", "Ljava/lang/Class;");
            int bya = this.euH.bya();
            int bya2 = this.euH.bya();
            this.euH.add(165, bya);
            short byd = this.euH.byd();
            bND();
            this.euH.mo5819nL(s);
            this.euH.add(167, bya2);
            this.euH.mo5783a(bya, byd);
            this.euH.mo5818nK(s + 1);
            this.euH.mo5826nS(bya2);
        } else {
            if (z) {
                this.euH.add(89);
            }
            this.euH.mo5819nL(s);
        }
    }

    /* renamed from: c */
    private void m9410c(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, boolean z) {
        if (!this.eZd) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        int g = this.eYX.mo6838g(qlVar);
        m9405b(qlVar2.cFG(), qlVar);
        boolean z2 = qlVar.getIntProp(8, -1) != -1;
        short s = this.eZe[g];
        int bya = this.euH.bya();
        int bya2 = this.euH.bya();
        if (z2) {
            this.euH.mo5820nM(s + 2);
            this.euH.add(154, bya2);
            short byd = this.euH.byd();
            this.euH.mo5814nG(1);
            this.euH.mo5815nH(s + 2);
            this.euH.mo5818nK(s);
            if (z) {
                this.euH.mo5823nP(s);
                this.euH.mo5783a(bya2, byd);
            } else {
                this.euH.add(167, bya);
                this.euH.mo5783a(bya2, byd);
                this.euH.add(88);
            }
        } else {
            this.euH.mo5820nM(s + 1);
            this.euH.add(154, bya2);
            short byd2 = this.euH.byd();
            this.euH.mo5814nG(1);
            this.euH.mo5815nH(s + 1);
            this.euH.mo5819nL(s);
            if (z) {
                this.euH.mo5824nQ(s);
                this.euH.mo5783a(bya2, byd2);
            } else {
                this.euH.add(167, bya);
                this.euH.mo5783a(bya2, byd2);
                this.euH.add(87);
            }
        }
        this.euH.mo5826nS(bya);
    }

    /* renamed from: j */
    private void m9422j(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        m9405b(qlVar2, qlVar);
        org.mozilla1.javascript.Node cFG = qlVar2.cFG();
        m9405b(cFG, qlVar);
        if (qlVar.getType() == 34) {
            this.euH.mo5824nQ(this.eZk);
            m9401ak("getObjectPropNoWarn", "(Ljava/lang/Object;Ljava/lang/String;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
        } else if (qlVar2.getType() == 43 && cFG.getType() == 41) {
            this.euH.mo5824nQ(this.eZk);
            m9401ak("getObjectProp", "(Lorg/mozilla/javascript/Scriptable;Ljava/lang/String;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
        } else {
            this.euH.mo5824nQ(this.eZk);
            m9401ak("getObjectProp", "(Ljava/lang/Object;Ljava/lang/String;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
        }
    }

    /* renamed from: a */
    private void m9387a(int i, org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        m9405b(qlVar2, qlVar);
        org.mozilla1.javascript.Node cFG = qlVar2.cFG();
        if (i == 138) {
            this.euH.add(89);
        }
        m9405b(cFG, qlVar);
        org.mozilla1.javascript.Node cFG2 = cFG.cFG();
        if (i == 138) {
            this.euH.add(90);
            if (qlVar2.getType() == 43 && cFG.getType() == 41) {
                this.euH.mo5824nQ(this.eZk);
                m9401ak("getObjectProp", "(Lorg/mozilla/javascript/Scriptable;Ljava/lang/String;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
            } else {
                this.euH.mo5824nQ(this.eZk);
                m9401ak("getObjectProp", "(Ljava/lang/Object;Ljava/lang/String;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
            }
        }
        m9405b(cFG2, qlVar);
        this.euH.mo5824nQ(this.eZk);
        m9401ak("setObjectProp", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
    }

    /* renamed from: b */
    private void m9403b(int i, org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        m9405b(qlVar2, qlVar);
        org.mozilla1.javascript.Node cFG = qlVar2.cFG();
        if (i == 139) {
            this.euH.add(89);
        }
        m9405b(cFG, qlVar);
        org.mozilla1.javascript.Node cFG2 = cFG.cFG();
        boolean z = qlVar.getIntProp(8, -1) != -1;
        if (i == 139) {
            if (z) {
                this.euH.add(93);
                this.euH.mo5824nQ(this.eZk);
                m9402al("getObjectIndex", "(Ljava/lang/Object;DLorg/mozilla/javascript/Context;)Ljava/lang/Object;");
            } else {
                this.euH.add(90);
                this.euH.mo5824nQ(this.eZk);
                m9401ak("getObjectElem", "(Ljava/lang/Object;Ljava/lang/Object;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
            }
        }
        m9405b(cFG2, qlVar);
        this.euH.mo5824nQ(this.eZk);
        if (z) {
            m9401ak("setObjectIndex", "(Ljava/lang/Object;DLjava/lang/Object;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
        } else {
            m9401ak("setObjectElem", "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lorg/mozilla/javascript/Context;)Ljava/lang/Object;");
        }
    }

    /* renamed from: k */
    private void m9424k(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        m9426m(qlVar);
        m9405b(qlVar2, qlVar);
        this.euH.mo5824nQ(this.eZi);
        m9401ak("enterDotQuery", "(Ljava/lang/Object;Lorg/mozilla/javascript/Scriptable;)Lorg/mozilla/javascript/Scriptable;");
        this.euH.mo5819nL(this.eZi);
        this.euH.add(1);
        int bya = this.euH.bya();
        this.euH.mo5826nS(bya);
        this.euH.add(87);
        m9405b(qlVar2.cFG(), qlVar);
        m9401ak("toBoolean", "(Ljava/lang/Object;)Z");
        this.euH.mo5824nQ(this.eZi);
        m9401ak("updateDotQuery", "(ZLorg/mozilla/javascript/Scriptable;)Ljava/lang/Object;");
        this.euH.add(89);
        this.euH.add(198, bya);
        this.euH.mo5824nQ(this.eZi);
        m9401ak("leaveDotQuery", "(Lorg/mozilla/javascript/Scriptable;)Lorg/mozilla/javascript/Scriptable;");
        this.euH.mo5819nL(this.eZi);
    }

    /* renamed from: t */
    private int m9443t(org.mozilla1.javascript.Node qlVar) {
        return ((org.mozilla1.javascript.Node) qlVar.getProp(3)).getExistingIntProp(2);
    }

    /* renamed from: pJ */
    private void m9434pJ(int i) {
        this.euH.mo5824nQ(i);
        this.euH.mo5782a(178, "java/lang/Void", "TYPE", "Ljava/lang/Class;");
        int bya = this.euH.bya();
        this.euH.add(165, bya);
        short byd = this.euH.byd();
        this.euH.mo5824nQ(i);
        bNC();
        int bya2 = this.euH.bya();
        this.euH.add(167, bya2);
        this.euH.mo5783a(bya, byd);
        this.euH.mo5823nP(i + 1);
        this.euH.mo5826nS(bya2);
    }

    /* renamed from: pK */
    private void m9435pK(int i) {
        this.euH.mo5824nQ(i);
        this.euH.mo5782a(178, "java/lang/Void", "TYPE", "Ljava/lang/Class;");
        int bya = this.euH.bya();
        this.euH.add(165, bya);
        short byd = this.euH.byd();
        this.euH.mo5824nQ(i);
        int bya2 = this.euH.bya();
        this.euH.add(167, bya2);
        this.euH.mo5783a(bya, byd);
        this.euH.mo5823nP(i + 1);
        bND();
        this.euH.mo5826nS(bya2);
    }

    /* renamed from: a */
    private void m9393a(org.mozilla1.javascript.Node qlVar, int i) {
        this.euH.add(i, m9423k(qlVar));
    }

    private void bNC() {
        m9401ak("toNumber", "(Ljava/lang/Object;)D");
    }

    /* renamed from: pL */
    private void m9436pL(int i) {
        if (i != 0) {
            this.euH.mo5814nG(i);
            this.euH.add(189, "java/lang/Object");
        } else if (this.eZp >= 0) {
            this.euH.mo5824nQ(this.eZp);
        } else {
            this.euH.mo5782a(178, "org/mozilla/javascript/ScriptRuntime", "emptyArgs", "[Ljava/lang/Object;");
        }
    }

    /* renamed from: ak */
    private void m9401ak(String str, String str2) {
        this.euH.mo5794b(184, "org.mozilla.javascript.ScriptRuntime", str, str2);
    }

    /* renamed from: al */
    private void m9402al(String str, String str2) {
        this.euH.mo5794b(184, "org/mozilla/javascript/optimizer/OptRuntime", str, str2);
    }

    /* renamed from: W */
    private void m9386W(int i, int i2) {
        this.euH.mo5826nS(i2);
        int bya = this.euH.bya();
        this.euH.mo5782a(178, "java/lang/Boolean", "FALSE", "Ljava/lang/Boolean;");
        this.euH.add(167, bya);
        this.euH.mo5826nS(i);
        this.euH.mo5782a(178, "java/lang/Boolean", "TRUE", "Ljava/lang/Boolean;");
        this.euH.mo5826nS(bya);
        this.euH.mo5828nV(-1);
    }

    private void bND() {
        m9402al("wrapDouble", "(D)Ljava/lang/Double;");
    }

    /* renamed from: dW */
    private short m9412dW(boolean z) {
        short f = m9415f(2, z);
        if (f < 255) {
            this.eYZ[f] = 1;
            this.eYZ[f + 1] = 1;
            if (z) {
                this.eYZ[f + 2] = 1;
            }
            if (f == this.eZa) {
                int i = this.eZa + 2;
                while (i < 256) {
                    if (this.eYZ[i] == 0) {
                        this.eZa = (short) i;
                        if (this.eZb < this.eZa) {
                            this.eZb = this.eZa;
                        }
                    } else {
                        i++;
                    }
                }
            }
            return f;
        }
        throw org.mozilla1.javascript.Context.m35245gF("Program too complex (out of locals)");
    }

    /* renamed from: dX */
    private short m9413dX(boolean z) {
        short f = m9415f(1, z);
        if (f < 255) {
            this.eYZ[f] = 1;
            if (z) {
                this.eYZ[f + 1] = 1;
            }
            if (f == this.eZa) {
                int i = this.eZa + 2;
                while (i < 256) {
                    if (this.eYZ[i] == 0) {
                        this.eZa = (short) i;
                        if (this.eZb < this.eZa) {
                            this.eZb = this.eZa;
                        }
                    } else {
                        i++;
                    }
                }
            }
            return f;
        }
        throw org.mozilla1.javascript.Context.m35245gF("Program too complex (out of locals)");
    }

    private short bNE() {
        short s = this.eZa;
        this.eYZ[s] = 1;
        int i = this.eZa;
        while (true) {
            i++;
            if (i >= 256) {
                throw org.mozilla1.javascript.Context.m35245gF("Program too complex (out of locals)");
            } else if (this.eYZ[i] == 0) {
                this.eZa = (short) i;
                if (this.eZb < this.eZa) {
                    this.eZb = this.eZa;
                }
                return s;
            }
        }
    }

    /* renamed from: f */
    private short m9415f(int i, boolean z) {
        short s;
        if (z) {
            i++;
        }
        short s2 = this.eZa;
        while (true) {
            s = s2;
            if (s < 255) {
                int i2 = 0;
                while (i2 < i && this.eYZ[s + i2] == 0) {
                    i2++;
                }
                if (i2 >= i) {
                    break;
                }
                s2 = (short) (s + 1);
            } else {
                break;
            }
        }
        return s;
    }

    /* renamed from: q */
    private void m9437q(short s) {
        int[] iArr = this.eYZ;
        iArr[s] = iArr[s] + 1;
    }

    /* renamed from: r */
    private void m9440r(short s) {
        int[] iArr = this.eYZ;
        iArr[s] = iArr[s] - 1;
    }

    /* renamed from: s */
    private void m9442s(short s) {
        if (s < this.eZa) {
            this.eZa = s;
        }
        this.eYZ[s] = 0;
    }

    /* renamed from: a.SN$a */
    class C1238a {
        public List<Integer> efR = new ArrayList();
        public int efS = 0;

        C1238a() {
        }
    }
}
