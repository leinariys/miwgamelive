package org.mozilla1.javascript.optimizer;

import org.mozilla1.javascript.Node;
import org.mozilla1.javascript.ObjArray;
import org.mozilla1.javascript.ScriptOrFnNode;

/* renamed from: a.nH */
/* compiled from: a */
public class Optimizer {
    static final int aKF = 0;
    static final int aKG = 1;
    static final int aKH = 3;
    org.mozilla1.javascript.optimizer.OptFunctionNode aKJ;
    private boolean aKI;
    private boolean aKK;

    public Optimizer() {
    }

    /* renamed from: a */
    private static void m35942a(Node qlVar, ObjArray acx) {
        int type = qlVar.getType();
        if (type == 128 || type == 140 || type == 131 || type == 108) {
            for (Node cFE = qlVar.cFE(); cFE != null; cFE = cFE.cFG()) {
                m35942a(cFE, acx);
            }
            return;
        }
        acx.add(qlVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: i */
    public void mo20662i(ScriptOrFnNode aak) {
        int functionCount = aak.getFunctionCount();
        for (int i = 0; i != functionCount; i++) {
            m35944b(org.mozilla1.javascript.optimizer.OptFunctionNode.m11683b(aak, i));
        }
    }

    /* renamed from: b */
    private void m35944b(org.mozilla1.javascript.optimizer.OptFunctionNode xq) {
        if (!xq.eIE.requiresActivation()) {
            this.aKI = xq.bFB();
            this.aKJ = xq;
            ObjArray acx = new ObjArray();
            m35942a((Node) xq.eIE, acx);
            Node[] qlVarArr = new Node[acx.size()];
            acx.toArray(qlVarArr);
            org.mozilla1.javascript.optimizer.Block.m2865a(xq, qlVarArr);
            if (!xq.eIE.requiresActivation()) {
                this.aKK = false;
                for (Node c : qlVarArr) {
                    m35946c(c);
                }
                xq.mo6837dG(this.aKK);
            }
        }
    }

    /* renamed from: a */
    private void m35941a(Node qlVar) {
        if (this.aKI && qlVar.getType() == 55) {
            if (this.aKJ.mo6840pd(this.aKJ.mo6838g(qlVar))) {
                this.aKK = true;
            }
        }
    }

    /* renamed from: b */
    private boolean m35945b(Node qlVar) {
        if (this.aKI && qlVar.getType() == 55) {
            if (this.aKJ.mo6840pd(this.aKJ.mo6838g(qlVar))) {
                qlVar.removeProp(8);
                return true;
            }
        }
        return false;
    }

    /* renamed from: c */
    private int m35946c(Node qlVar) {
        switch (qlVar.getType()) {
            case 9:
            case 10:
            case 11:
            case 18:
            case 19:
            case 22:
            case 23:
            case 24:
            case 25:
                Node cFE = qlVar.cFE();
                Node cFG = cFE.cFG();
                int c = m35946c(cFE);
                int c2 = m35946c(cFG);
                m35941a(cFE);
                m35941a(cFG);
                if (c == 1) {
                    if (c2 == 1) {
                        qlVar.putIntProp(8, 0);
                        return 1;
                    }
                    if (!m35945b(cFG)) {
                        qlVar.mo21464J(cFG);
                        qlVar.mo21461G(new Node(149, cFG));
                        qlVar.putIntProp(8, 0);
                    }
                    return 1;
                } else if (c2 == 1) {
                    if (!m35945b(cFE)) {
                        qlVar.mo21464J(cFE);
                        qlVar.mo21460F(new Node(149, cFE));
                        qlVar.putIntProp(8, 0);
                    }
                    return 1;
                } else {
                    if (!m35945b(cFE)) {
                        qlVar.mo21464J(cFE);
                        qlVar.mo21460F(new Node(149, cFE));
                    }
                    if (!m35945b(cFG)) {
                        qlVar.mo21464J(cFG);
                        qlVar.mo21461G(new Node(149, cFG));
                    }
                    qlVar.putIntProp(8, 0);
                    return 1;
                }
            case 14:
            case 15:
            case 16:
            case 17:
                Node cFE2 = qlVar.cFE();
                Node cFG2 = cFE2.cFG();
                int c3 = m35946c(cFE2);
                int c4 = m35946c(cFG2);
                m35941a(cFE2);
                m35941a(cFG2);
                if (m35945b(cFE2)) {
                    if (m35945b(cFG2)) {
                        return 0;
                    }
                    if (c4 == 1) {
                        qlVar.putIntProp(8, 2);
                    }
                } else if (m35945b(cFG2)) {
                    if (c3 == 1) {
                        qlVar.putIntProp(8, 1);
                    }
                } else if (c3 == 1) {
                    if (c4 == 1) {
                        qlVar.putIntProp(8, 0);
                    } else {
                        qlVar.putIntProp(8, 1);
                    }
                } else if (c4 == 1) {
                    qlVar.putIntProp(8, 2);
                }
                return 0;
            case 21:
                Node cFE3 = qlVar.cFE();
                Node cFG3 = cFE3.cFG();
                int c5 = m35946c(cFE3);
                int c6 = m35946c(cFG3);
                if (m35945b(cFE3)) {
                    if (m35945b(cFG3)) {
                        return 0;
                    }
                    if (c6 == 1) {
                        qlVar.putIntProp(8, 2);
                    }
                } else if (m35945b(cFG3)) {
                    if (c5 == 1) {
                        qlVar.putIntProp(8, 1);
                    }
                } else if (c5 == 1) {
                    if (c6 == 1) {
                        qlVar.putIntProp(8, 0);
                        return 1;
                    }
                    qlVar.putIntProp(8, 1);
                } else if (c6 == 1) {
                    qlVar.putIntProp(8, 2);
                }
                return 0;
            case 36:
                Node cFE4 = qlVar.cFE();
                Node cFG4 = cFE4.cFG();
                if (m35946c(cFE4) == 1 && !m35945b(cFE4)) {
                    qlVar.mo21464J(cFE4);
                    qlVar.mo21460F(new Node(148, cFE4));
                }
                if (m35946c(cFG4) == 1 && !m35945b(cFG4)) {
                    qlVar.putIntProp(8, 2);
                }
                return 0;
            case 37:
            case 139:
                Node cFE5 = qlVar.cFE();
                Node cFG5 = cFE5.cFG();
                Node cFG6 = cFG5.cFG();
                if (m35946c(cFE5) == 1 && !m35945b(cFE5)) {
                    qlVar.mo21464J(cFE5);
                    qlVar.mo21460F(new Node(148, cFE5));
                }
                if (m35946c(cFG5) == 1) {
                    qlVar.putIntProp(8, 1);
                    m35941a(cFG5);
                }
                if (m35946c(cFG6) == 1 && !m35945b(cFG6)) {
                    qlVar.mo21464J(cFG6);
                    qlVar.mo21461G(new Node(148, cFG6));
                }
                return 0;
            case 38:
                Node cFE6 = qlVar.cFE();
                if (cFE6.getType() == 36) {
                    m35943a(cFE6, cFE6.cFE());
                } else {
                    m35946c(cFE6);
                }
                Node cFG7 = cFE6.cFG();
                if (((org.mozilla1.javascript.optimizer.OptFunctionNode) qlVar.getProp(9)) != null) {
                    for (Node qlVar2 = cFG7; qlVar2 != null; qlVar2 = qlVar2.cFG()) {
                        if (m35946c(qlVar2) == 1) {
                            m35941a(qlVar2);
                        }
                    }
                } else {
                    m35943a(qlVar, cFG7);
                }
                return 0;
            case 40:
                qlVar.putIntProp(8, 0);
                return 1;
            case 55:
                int g = this.aKJ.mo6838g(qlVar);
                if (this.aKI && this.aKJ.mo6840pd(g)) {
                    qlVar.putIntProp(8, 0);
                    return 1;
                } else if (!this.aKJ.mo6841pe(g)) {
                    return 0;
                } else {
                    qlVar.putIntProp(8, 0);
                    return 1;
                }
            case 56:
                Node cFG8 = qlVar.cFE().cFG();
                int c7 = m35946c(cFG8);
                int g2 = this.aKJ.mo6838g(qlVar);
                if (!this.aKI || !this.aKJ.mo6840pd(g2)) {
                    if (this.aKJ.mo6841pe(g2)) {
                        if (c7 != 1) {
                            qlVar.mo21464J(cFG8);
                            qlVar.mo21461G(new Node(149, cFG8));
                        }
                        qlVar.putIntProp(8, 0);
                        m35941a(cFG8);
                        return 1;
                    }
                    if (c7 == 1 && !m35945b(cFG8)) {
                        qlVar.mo21464J(cFG8);
                        qlVar.mo21461G(new Node(148, cFG8));
                    }
                    return 0;
                } else if (c7 != 1) {
                    return c7;
                } else {
                    if (!m35945b(cFG8)) {
                        qlVar.putIntProp(8, 0);
                        return 1;
                    }
                    m35941a(cFG8);
                    return 0;
                }
            case 105:
            case 106:
                Node cFE7 = qlVar.cFE();
                if (cFE7.getType() == 55) {
                    if (m35946c(cFE7) != 1) {
                        return 0;
                    }
                    qlVar.putIntProp(8, 0);
                    m35941a(cFE7);
                    return 1;
                } else if (cFE7.getType() == 36) {
                    return m35946c(cFE7);
                } else {
                    return 0;
                }
            case 132:
                if (m35946c(qlVar.cFE()) == 1) {
                    qlVar.putIntProp(8, 0);
                }
                return 0;
            default:
                m35943a(qlVar, qlVar.cFE());
                return 0;
        }
    }

    /* renamed from: a */
    private void m35943a(Node qlVar, Node qlVar2) {
        while (qlVar2 != null) {
            Node cFG = qlVar2.cFG();
            if (m35946c(qlVar2) == 1 && !m35945b(qlVar2)) {
                qlVar.mo21464J(qlVar2);
                Node qlVar3 = new Node(148, qlVar2);
                if (cFG == null) {
                    qlVar.mo21461G(qlVar3);
                } else {
                    qlVar.mo21484o(qlVar3, cFG);
                }
            }
            qlVar2 = cFG;
        }
    }
}
