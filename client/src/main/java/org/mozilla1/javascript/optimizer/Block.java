package org.mozilla1.javascript.optimizer;

import org.mozilla1.javascript.Node;
import org.mozilla1.javascript.ObjArray;
import org.mozilla1.javascript.ObjToIntMap;

import java.util.HashMap;

/* renamed from: a.EW */
/* compiled from: a */
class Block {
    static final boolean DEBUG = false;
    private static int cUB;
    private DataFlowBitSet cUA;
    private Block[] cUs;
    private Block[] cUt;
    private int cUu;
    private int cUv;
    private int cUw;
    private DataFlowBitSet cUx;
    private DataFlowBitSet cUy;
    private DataFlowBitSet cUz;

    Block(int i, int i2) {
        this.cUu = i;
        this.cUv = i2;
    }

    /* renamed from: a */
    static void m2865a(OptFunctionNode xq, Node[] qlVarArr) {
        int paramCount = xq.eIE.getParamCount();
        int paramAndVarCount = xq.eIE.getParamAndVarCount();
        int[] iArr = new int[paramAndVarCount];
        for (int i = 0; i != paramCount; i++) {
            iArr[i] = 3;
        }
        for (int i2 = paramCount; i2 != paramAndVarCount; i2++) {
            iArr[i2] = 0;
        }
        Block[] a = m2870a(qlVarArr);
        m2866a(xq, qlVarArr, a, iArr);
        m2872b(xq, qlVarArr, a, iArr);
        while (paramCount != paramAndVarCount) {
            if (iArr[paramCount] == 1) {
                xq.mo6842pf(paramCount);
            }
            paramCount++;
        }
    }

    /* renamed from: a */
    private static Block[] m2870a(Node[] qlVarArr) {
        HashMap hashMap = new HashMap();
        org.mozilla1.javascript.ObjArray acx = new ObjArray();
        int i = 0;
        for (int i2 = 0; i2 < qlVarArr.length; i2++) {
            switch (qlVarArr[i2].getType()) {
                case 5:
                case 6:
                case 7:
                    C0334a B = m2861B(i, i2);
                    if (qlVarArr[i].getType() == 130) {
                        hashMap.put(qlVarArr[i], B);
                    }
                    acx.add(B);
                    i = i2 + 1;
                    break;
                case 130:
                    if (i2 == i) {
                        break;
                    } else {
                        C0334a B2 = m2861B(i, i2 - 1);
                        if (qlVarArr[i].getType() == 130) {
                            hashMap.put(qlVarArr[i], B2);
                        }
                        acx.add(B2);
                        i = i2;
                        break;
                    }
            }
        }
        if (i != qlVarArr.length) {
            C0334a B3 = m2861B(i, qlVarArr.length - 1);
            if (qlVarArr[i].getType() == 130) {
                hashMap.put(qlVarArr[i], B3);
            }
            acx.add(B3);
        }
        for (int i3 = 0; i3 < acx.size(); i3++) {
            C0334a aVar = (C0334a) acx.get(i3);
            Node.C3367a aVar2 = (Node.C3367a) qlVarArr[aVar.gbB.cUv];
            int type = aVar2.getType();
            if (type != 5 && i3 < acx.size() - 1) {
                C0334a aVar3 = (C0334a) acx.get(i3 + 1);
                aVar.mo1877a(aVar3);
                aVar3.mo1878b(aVar);
            }
            if (type == 7 || type == 6 || type == 5) {
                Node qlVar = aVar2.aUM;
                C0334a aVar4 = (C0334a) hashMap.get(qlVar);
                qlVar.putProp(6, aVar4.gbB);
                aVar.mo1877a(aVar4);
                aVar4.mo1878b(aVar);
            }
        }
        Block[] ewArr = new Block[acx.size()];
        for (int i4 = 0; i4 < acx.size(); i4++) {
            C0334a aVar5 = (C0334a) acx.get(i4);
            Block ew = aVar5.gbB;
            ew.cUs = aVar5.cjU();
            ew.cUt = aVar5.cjV();
            ew.cUw = i4;
            ewArr[i4] = ew;
        }
        return ewArr;
    }

    /* renamed from: B */
    private static C0334a m2861B(int i, int i2) {
        C0334a aVar = new C0334a((C0334a) null);
        aVar.gbB = new Block(i, i2);
        return aVar;
    }

    /* renamed from: a */
    private static String m2863a(Block[] ewArr, Node[] qlVarArr) {
        return null;
    }

    /* renamed from: a */
    private static void m2866a(OptFunctionNode xq, Node[] qlVarArr, Block[] ewArr, int[] iArr) {
        Block[] ewArr2;
        boolean z;
        for (Block b : ewArr) {
            b.m2871b(xq, qlVarArr);
        }
        boolean[] zArr = new boolean[ewArr.length];
        boolean[] zArr2 = new boolean[ewArr.length];
        int length = ewArr.length - 1;
        zArr[length] = true;
        boolean z2 = false;
        while (true) {
            if (zArr[length] || !zArr2[length]) {
                zArr2[length] = true;
                zArr[length] = false;
                if (ewArr[length].aPe() && (ewArr2 = ewArr[length].cUt) != null) {
                    boolean z3 = z2;
                    for (Block ew : ewArr2) {
                        int i = ew.cUw;
                        zArr[i] = true;
                        if (i > length) {
                            z = true;
                        } else {
                            z = false;
                        }
                        z3 |= z;
                    }
                    z2 = z3;
                }
            }
            if (length != 0) {
                length--;
            } else if (z2) {
                length = ewArr.length - 1;
                z2 = false;
            } else {
                ewArr[0].m2867a(iArr);
                return;
            }
        }
    }

    /* renamed from: b */
    private static void m2872b(OptFunctionNode xq, Node[] qlVarArr, Block[] ewArr, int[] iArr) {
        Block[] ewArr2;
        boolean z;
        boolean[] zArr = new boolean[ewArr.length];
        boolean[] zArr2 = new boolean[ewArr.length];
        zArr[0] = true;
        boolean z2 = false;
        int i = 0;
        while (true) {
            if (zArr[i] || !zArr2[i]) {
                zArr2[i] = true;
                zArr[i] = false;
                if (ewArr[i].m2868a(xq, qlVarArr, iArr) && (ewArr2 = ewArr[i].cUs) != null) {
                    boolean z3 = z2;
                    for (Block ew : ewArr2) {
                        int i2 = ew.cUw;
                        zArr[i2] = true;
                        if (i2 < i) {
                            z = true;
                        } else {
                            z = false;
                        }
                        z3 |= z;
                    }
                    z2 = z3;
                }
            }
            if (i != ewArr.length - 1) {
                i++;
            } else if (z2) {
                z2 = false;
                i = 0;
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    private static boolean m2869a(int[] iArr, int i, int i2) {
        int i3 = iArr[i] | i2;
        iArr[i] = i3;
        return i2 != i3;
    }

    /* renamed from: a */
    private static int m2862a(OptFunctionNode xq, Node qlVar, int[] iArr) {
        switch (qlVar.getType()) {
            case 9:
            case 10:
            case 11:
            case 18:
            case 19:
            case 20:
            case 22:
            case 24:
            case 25:
            case 40:
            case 105:
            case 106:
                return 1;
            case 21:
                Node cFE = qlVar.cFE();
                return m2862a(xq, cFE.cFG(), iArr) | m2862a(xq, cFE, iArr);
            case 30:
            case 38:
            case 70:
                return 3;
            case 36:
                return 3;
            case 55:
                return iArr[xq.mo6838g(qlVar)];
            case 65:
            case 66:
                return 3;
            default:
                Node cFE2 = qlVar.cFE();
                if (cFE2 == null) {
                    return 3;
                }
                int i = 0;
                for (Node qlVar2 = cFE2; qlVar2 != null; qlVar2 = qlVar2.cFG()) {
                    i |= m2862a(xq, qlVar2, iArr);
                }
                return i;
        }
    }

    /* renamed from: b */
    private static boolean m2873b(OptFunctionNode xq, Node qlVar, int[] iArr) {
        boolean z = false;
        Node cFE = qlVar.cFE();
        switch (qlVar.getType()) {
            case 35:
            case 138:
                if (cFE.getType() == 55) {
                    m2869a(iArr, xq.mo6838g(cFE), 3);
                }
                while (cFE != null) {
                    z |= m2873b(xq, cFE, iArr);
                    cFE = cFE.cFG();
                }
                return z;
            case 56:
                return false | m2869a(iArr, xq.mo6838g(qlVar), m2862a(xq, cFE.cFG(), iArr));
            case 105:
            case 106:
                if (cFE.getType() == 55) {
                    return false | m2869a(iArr, xq.mo6838g(cFE), 1);
                }
                return false;
        }
        while (cFE != null) {
            z |= m2873b(xq, cFE, iArr);
            cFE = cFE.cFG();
        }
        return z;
    }

    /* renamed from: a */
    private void m2867a(int[] iArr) {
        for (int i = 0; i != iArr.length; i++) {
            if (this.cUx.test(i)) {
                m2869a(iArr, i, 3);
            }
        }
    }

    /* renamed from: a */
    private void m2864a(OptFunctionNode xq, Node qlVar) {
        switch (qlVar.getType()) {
            case 55:
                int g = xq.mo6838g(qlVar);
                if (!this.cUA.test(g)) {
                    this.cUz.set(g);
                    return;
                }
                return;
            case 56:
                m2864a(xq, qlVar.cFE().cFG());
                this.cUA.set(xq.mo6838g(qlVar));
                return;
            case 105:
            case 106:
                Node cFE = qlVar.cFE();
                if (cFE.getType() == 55) {
                    int g2 = xq.mo6838g(cFE);
                    if (!this.cUA.test(g2)) {
                        this.cUz.set(g2);
                    }
                    this.cUA.set(g2);
                    return;
                }
                return;
            default:
                for (Node cFE2 = qlVar.cFE(); cFE2 != null; cFE2 = cFE2.cFG()) {
                    m2864a(xq, cFE2);
                }
                return;
        }
    }

    /* renamed from: b */
    private void m2871b(OptFunctionNode xq, Node[] qlVarArr) {
        int bFE = xq.bFE();
        this.cUz = new DataFlowBitSet(bFE);
        this.cUA = new DataFlowBitSet(bFE);
        this.cUx = new DataFlowBitSet(bFE);
        this.cUy = new DataFlowBitSet(bFE);
        for (int i = this.cUu; i <= this.cUv; i++) {
            m2864a(xq, qlVarArr[i]);
        }
        this.cUA.not();
    }

    private boolean aPe() {
        this.cUy.clear();
        if (this.cUs != null) {
            for (Block ew : this.cUs) {
                this.cUy.mo22281a(ew.cUx);
            }
        }
        return this.cUx.mo22283b(this.cUy, this.cUz, this.cUA);
    }

    /* renamed from: a */
    private boolean m2868a(OptFunctionNode xq, Node[] qlVarArr, int[] iArr) {
        boolean z = false;
        for (int i = this.cUu; i <= this.cUv; i++) {
            Node qlVar = qlVarArr[i];
            if (qlVar != null) {
                z |= m2873b(xq, qlVar, iArr);
            }
        }
        return z;
    }

    /* renamed from: c */
    private void m2874c(OptFunctionNode xq) {
    }

    /* renamed from: a.EW$a */
    private static class C0334a {
        Block gbB;
        private ObjToIntMap gbA;
        private ObjToIntMap gbz;

        private C0334a() {
            this.gbz = new ObjToIntMap();
            this.gbA = new ObjToIntMap();
        }

        /* synthetic */ C0334a(C0334a aVar) {
            this();
        }

        /* renamed from: a */
        private static Block[] m2875a(ObjToIntMap uSVar) {
            Block[] ewArr = null;
            if (uSVar.isEmpty()) {
                return ewArr;
            }
            Block[] ewArr2 = new Block[uSVar.size()];
            ObjToIntMap.Iterator bRg = uSVar.newIterator();
            bRg.start();
            int i = 0;
            while (!bRg.done()) {
                ewArr2[i] = ((C0334a) bRg.getKey()).gbB;
                bRg.next();
                i++;
            }
            return ewArr2;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo1877a(C0334a aVar) {
            this.gbz.put(aVar, 0);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo1878b(C0334a aVar) {
            this.gbA.put(aVar, 0);
        }

        /* access modifiers changed from: package-private */
        public Block[] cjU() {
            return m2875a(this.gbz);
        }

        /* access modifiers changed from: package-private */
        public Block[] cjV() {
            return m2875a(this.gbA);
        }
    }
}
