package org.mozilla1.javascript.optimizer;

/* renamed from: a.tp */
/* compiled from: a */
class DataFlowBitSet {
    private int[] bmg;
    private int bmh;

    DataFlowBitSet(int i) {
        this.bmh = i;
        this.bmg = new int[((i + 31) >> 5)];
    }

    /* access modifiers changed from: package-private */
    public void set(int i) {
        if (i < 0 || i >= this.bmh) {
            m39751eD(i);
        }
        int[] iArr = this.bmg;
        int i2 = i >> 5;
        iArr[i2] = iArr[i2] | (1 << (i & 31));
    }

    /* access modifiers changed from: package-private */
    public boolean test(int i) {
        if (i < 0 || i >= this.bmh) {
            m39751eD(i);
        }
        if ((this.bmg[i >> 5] & (1 << (i & 31))) != 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void not() {
        int length = this.bmg.length;
        for (int i = 0; i < length; i++) {
            this.bmg[i] = this.bmg[i] ^ -1;
        }
    }

    /* access modifiers changed from: package-private */
    public void clear(int i) {
        if (i < 0 || i >= this.bmh) {
            m39751eD(i);
        }
        int[] iArr = this.bmg;
        int i2 = i >> 5;
        iArr[i2] = iArr[i2] & ((1 << (i & 31)) ^ -1);
    }

    /* access modifiers changed from: package-private */
    public void clear() {
        int length = this.bmg.length;
        for (int i = 0; i < length; i++) {
            this.bmg[i] = 0;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo22281a(DataFlowBitSet tpVar) {
        int length = this.bmg.length;
        for (int i = 0; i < length; i++) {
            int[] iArr = this.bmg;
            iArr[i] = iArr[i] | tpVar.bmg[i];
        }
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("DataFlowBitSet, size = ");
        stringBuffer.append(this.bmh);
        stringBuffer.append(10);
        for (int hexString : this.bmg) {
            stringBuffer.append(Integer.toHexString(hexString));
            stringBuffer.append(' ');
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo22282a(DataFlowBitSet tpVar, DataFlowBitSet tpVar2, DataFlowBitSet tpVar3) {
        boolean z;
        int length = this.bmg.length;
        boolean z2 = false;
        for (int i = 0; i < length; i++) {
            int i2 = this.bmg[i];
            this.bmg[i] = (tpVar.bmg[i] | tpVar2.bmg[i]) & tpVar3.bmg[i];
            if (i2 != this.bmg[i]) {
                z = true;
            } else {
                z = false;
            }
            z2 |= z;
        }
        return z2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public boolean mo22283b(DataFlowBitSet tpVar, DataFlowBitSet tpVar2, DataFlowBitSet tpVar3) {
        boolean z;
        int length = this.bmg.length;
        boolean z2 = false;
        for (int i = 0; i < length; i++) {
            int i2 = this.bmg[i];
            this.bmg[i] = (tpVar.bmg[i] & tpVar3.bmg[i]) | tpVar2.bmg[i];
            if (i2 != this.bmg[i]) {
                z = true;
            } else {
                z = false;
            }
            z2 |= z;
        }
        return z2;
    }

    /* renamed from: eD */
    private void m39751eD(int i) {
        throw new RuntimeException("DataFlowBitSet bad index " + i);
    }
}
