package org.mozilla1.javascript.optimizer;

import org.mozilla1.javascript.ObjToIntMap;


/* renamed from: a.aHA */
/* compiled from: a */
public class ClassCompiler {
    private String hXQ;
    private Class<?> hXR;
    private Class<?>[] hXS;

    /* renamed from: he */
    private org.mozilla1.javascript.CompilerEnvirons f2909he;

    public ClassCompiler(org.mozilla1.javascript.CompilerEnvirons aki) {
        if (aki == null) {
            throw new IllegalArgumentException();
        }
        this.f2909he = aki;
        this.hXQ = "org.mozilla.javascript.optimizer.OptRuntime";
    }

    /* renamed from: l */
    public void mo9164l(String str) {
        this.hXQ = str;
    }

    public String ddz() {
        return this.hXQ;
    }

    public org.mozilla1.javascript.CompilerEnvirons ddA() {
        return this.f2909he;
    }

    public Class<?> ddB() {
        return this.hXR;
    }

    /* renamed from: aM */
    public void mo9157aM(Class<?> cls) {
        this.hXR = cls;
    }

    public Class<?>[] ddC() {
        if (this.hXS == null) {
            return null;
        }
        return (Class[]) this.hXS.clone();
    }

    /* renamed from: d */
    public void mo9159d(Class<?>[] clsArr) {
        this.hXS = clsArr == null ? null : (Class[]) clsArr.clone();
    }

    /* access modifiers changed from: protected */
    /* renamed from: aT */
    public String mo9158aT(String str, String str2) {
        return String.valueOf(str) + str2;
    }

    /* renamed from: a */
    public Object[] mo9156a(String str, String str2, int i, String str3) {
        String aT;
        Class<?> cls;
        org.mozilla1.javascript.Parser axf = new org.mozilla1.javascript.Parser(this.f2909he, this.f2909he.bwt());
        org.mozilla1.javascript.ScriptOrFnNode c = axf.mo16815c(str, str2, i);
        String encodedSource = axf.getEncodedSource();
        Class<?> ddB = ddB();
        Class[] ddC = ddC();
        boolean z = ddC == null && ddB == null;
        if (z) {
            aT = str3;
        } else {
            aT = mo9158aT(str3, "1");
        }
        Codegen aeVar = new Codegen();
        aeVar.mo13022l(this.hXQ);
        Object a = aeVar.mo13017a(this.f2909he, aT, c, encodedSource, false);
        if (z) {
            return new Object[]{aT, a};
        }
        int functionCount = c.getFunctionCount();
        org.mozilla1.javascript.ObjToIntMap uSVar = new ObjToIntMap(functionCount);
        for (int i2 = 0; i2 != functionCount; i2++) {
            org.mozilla1.javascript.FunctionNode wM = c.mo7704wM(i2);
            String functionName = wM.getFunctionName();
            if (!(functionName == null || functionName.length() == 0)) {
                uSVar.put(functionName, wM.getParamCount());
            }
        }
        if (ddB == null) {
            cls = org.mozilla1.javascript.ScriptRuntime.ObjectClass;
        } else {
            cls = ddB;
        }
        return new Object[]{str3, org.mozilla1.javascript.JavaAdapter.m8825a(uSVar, str3, cls, (Class<?>[]) ddC, aT), aT, a};
    }
}
