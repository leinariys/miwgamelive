package org.mozilla1.javascript.optimizer;

import org.mozilla1.javascript.ObjArray;

import java.util.Map;

/* renamed from: a.wo */
/* compiled from: a */
class OptTransformer extends org.mozilla1.javascript.NodeTransformer {
    private Map<String, org.mozilla1.javascript.optimizer.OptFunctionNode> possibleDirectCalls;

    /* renamed from: hf */
    private org.mozilla1.javascript.ObjArray directCallTargets;

    OptTransformer(Map<String, org.mozilla1.javascript.optimizer.OptFunctionNode> map, ObjArray acx) {
        this.possibleDirectCalls = map;
        this.directCallTargets = acx;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo13267a(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.ScriptOrFnNode aak) {
        m40747c(qlVar, aak);
        super.mo13267a(qlVar, aak);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo13268b(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.ScriptOrFnNode aak) {
        m40747c(qlVar, aak);
        super.mo13268b(qlVar, aak);
    }

    /* renamed from: c */
    private void m40747c(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.ScriptOrFnNode aak) {
        org.mozilla1.javascript.optimizer.OptFunctionNode xq;
        if (aak.getType() == 108) {
            org.mozilla1.javascript.Node cFE = qlVar.cFE();
            int i = 0;
            org.mozilla1.javascript.Node cFG = cFE.cFG();
            while (cFG != null) {
                cFG = cFG.cFG();
                i++;
            }
            if (i == 0) {
                org.mozilla1.javascript.optimizer.OptFunctionNode.m11684j(aak).eII = true;
            }
            if (this.possibleDirectCalls != null) {
                String str = null;
                if (cFE.getType() == 39) {
                    str = cFE.getString();
                } else if (cFE.getType() == 33) {
                    str = cFE.cFE().cFG().getString();
                } else if (cFE.getType() == 34) {
                    throw org.mozilla1.javascript.Kit.codeBug();
                }
                if (str != null && (xq = this.possibleDirectCalls.get(str)) != null && i == xq.eIE.getParamCount() && !xq.eIE.requiresActivation() && i <= 32) {
                    qlVar.putProp(9, xq);
                    if (!xq.bFB()) {
                        int size = this.directCallTargets.size();
                        this.directCallTargets.add(xq);
                        xq.mo6839pc(size);
                    }
                }
            }
        }
    }
}
