package org.mozilla1.javascript.optimizer;

import org.mozilla1.classfile.ClassFileWriter;
import org.mozilla1.javascript.ScriptRuntime;

import java.util.HashMap;
import java.util.List;

/* renamed from: a.ae */
/* compiled from: a */
public class Codegen implements org.mozilla1.javascript.Evaluator {

    /* renamed from: gS */
    static final String f4362gS = "org.mozilla.javascript.optimizer.OptRuntime";
    /* renamed from: gU */
    static final String f4364gU = "_dcp";
    /* renamed from: gY */
    static final String f4368gY = "_re";
    /* renamed from: gZ */
    static final String f4369gZ = "[Ljava/lang/Object;";
    /* renamed from: ha */
    static final String f4370ha = "(Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;)V";
    /* renamed from: hb */
    static final String f4371hb = "(Lorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Context;I)V";
    /* renamed from: gT */
    private static final String f4363gT = "org.mozilla.javascript.NativeFunction";
    /* renamed from: gV */
    private static final String f4365gV = "_id";
    /* renamed from: gW */
    private static final String f4366gW = "_reInit";
    /* renamed from: gX */
    private static final String f4367gX = "(Lorg/mozilla/javascript/RegExpProxy;Lorg/mozilla/javascript/Context;)V";
    /* renamed from: hc */
    private static final Object f4372hc = new Integer(24);

    /* renamed from: hd */
    private static int f4373hd;
    /* renamed from: hg */
    org.mozilla1.javascript.ScriptOrFnNode[] f4376hg;
    /* renamed from: hj */
    String f4379hj;
    /* renamed from: hk */
    String f4380hk;
    /* renamed from: he */
    private org.mozilla1.javascript.CompilerEnvirons f4374he;
    /* renamed from: hf */
    private org.mozilla1.javascript.ObjArray f4375hf;
    /* renamed from: hh */
    private org.mozilla1.javascript.ObjToIntMap f4377hh;
    /* renamed from: hi */
    private String f4378hi = f4362gS;
    /* renamed from: hl */
    private double[] f4381hl;

    /* renamed from: hm */
    private int f4382hm;

    /* renamed from: b */
    private static void m21124b(org.mozilla1.javascript.ScriptOrFnNode aak) {
        int functionCount = aak.getFunctionCount();
        for (int i = 0; i != functionCount; i++) {
            org.mozilla1.javascript.FunctionNode wM = aak.mo7704wM(i);
            new org.mozilla1.javascript.optimizer.OptFunctionNode(wM);
            m21124b((org.mozilla1.javascript.ScriptOrFnNode) wM);
        }
    }

    /* renamed from: a */
    private static void m21120a(org.mozilla1.javascript.ScriptOrFnNode aak, org.mozilla1.javascript.ObjArray acx) {
        acx.add(aak);
        int functionCount = aak.getFunctionCount();
        for (int i = 0; i != functionCount; i++) {
            m21120a((org.mozilla1.javascript.ScriptOrFnNode) aak.mo7704wM(i), acx);
        }
    }

    /* renamed from: d */
    static boolean m21128d(org.mozilla1.javascript.ScriptOrFnNode aak) {
        return aak.getType() == 108 && ((org.mozilla1.javascript.FunctionNode) aak).bHQ();
    }

    /* renamed from: i */
    private static void m21134i(ClassFileWriter uh) {
        uh.mo5794b(184, "org/mozilla/javascript/optimizer/OptRuntime", "wrapDouble", "(D)Ljava/lang/Double;");
    }

    /* renamed from: a */
    private static String m21115a(double d) {
        if (((double) ((int) d)) == d) {
            return "Ljava/lang/Integer;";
        }
        return "Ljava/lang/Double;";
    }

    /* renamed from: j */
    static void m21135j(ClassFileWriter uh) {
        uh.mo5782a(178, "org/mozilla/javascript/Undefined", "instance", "Ljava/lang/Object;");
    }

    /* renamed from: m */
    static String m21137m(int i) {
        return "_dt" + i;
    }

    /* renamed from: dh */
    static RuntimeException m21129dh() {
        throw new RuntimeException("Bad tree in codegen");
    }

    /* renamed from: a */
    public void mo5925a(org.mozilla1.javascript.RhinoException pu) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public String mo5924a(org.mozilla1.javascript.Context lhVar, int[] iArr) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public String mo5923a(org.mozilla1.javascript.RhinoException pu, String str) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: b */
    public List<String> mo5927b(org.mozilla1.javascript.RhinoException pu) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public void mo5926a(org.mozilla1.javascript.Script afe) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public Object mo5922a(org.mozilla1.javascript.CompilerEnvirons aki, org.mozilla1.javascript.ScriptOrFnNode aak, String str, boolean z) {
        int i;
        synchronized (f4372hc) {
            i = f4373hd + 1;
            f4373hd = i;
        }
        String str2 = "org.mozilla.javascript.gen.c" + i;
        return new Object[]{str2, mo13017a(aki, str2, aak, str, z)};
    }

    /* renamed from: a */
    public org.mozilla1.javascript.Script mo5920a(Object obj, Object obj2) {
        try {
            return (org.mozilla1.javascript.Script) m21121b(obj, obj2).newInstance();
        } catch (Exception e) {
            throw new RuntimeException("Unable to instantiate compiled class:" + e.toString());
        }
    }

    /* renamed from: a */
    public org.mozilla1.javascript.Function mo5921a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object obj, Object obj2) {
        try {
            return (org.mozilla1.javascript.NativeFunction) m21121b(obj, obj2).getConstructors()[0].newInstance(new Object[]{avf, lhVar, new Integer(0)});
        } catch (Exception e) {
            throw new RuntimeException("Unable to instantiate compiled class:" + e.toString());
        }
    }

    /* renamed from: b */
    private Class<?> m21121b(Object obj, Object obj2) {
        Object[] objArr = (Object[]) obj;
        String str = (String) objArr[0];
        byte[] bArr = (byte[]) objArr[1];
        org.mozilla1.javascript.GeneratedClassLoader a = org.mozilla1.javascript.SecurityController.m40442a(getClass().getClassLoader(), obj2);
        try {
            Class<?> defineClass = a.defineClass(str, bArr);
            a.linkClass(defineClass);
            return defineClass;
        } catch (IllegalArgumentException | SecurityException e) {
            throw new RuntimeException("Malformed optimizer package " + e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public byte[] mo13017a(org.mozilla1.javascript.CompilerEnvirons aki, String str, org.mozilla1.javascript.ScriptOrFnNode aak, String str2, boolean z) {
        this.f4374he = aki;
        m21119a(aak);
        if (z) {
            aak = aak.mo7704wM(0);
        }
        m21126c(aak);
        this.f4379hj = str;
        this.f4380hk = ClassFileWriter.m10156gH(str);
        try {
            return m21136k(str2);
        } catch (ClassFileWriter.ClassFileFormatException e) {
            throw m21114a(aak, e.getMessage());
        }
    }

    /* renamed from: a */
    private RuntimeException m21114a(org.mozilla1.javascript.ScriptOrFnNode aak, String str) {
        String message1;
        if (aak instanceof org.mozilla1.javascript.FunctionNode) {
            message1 = org.mozilla1.javascript.ScriptRuntime.getMessage2("msg.while.compiling.fn", ((org.mozilla1.javascript.FunctionNode) aak).getFunctionName(), str);
        } else {
            message1 = org.mozilla1.javascript.ScriptRuntime.getMessage1("msg.while.compiling.game.script", str);
        }
        return org.mozilla1.javascript.Context.m35227a(message1, aak.getSourceName(), aak.getLineno(), (String) null, 0);
    }

    /* renamed from: a */
    private void m21119a(org.mozilla1.javascript.ScriptOrFnNode aak) {
        m21124b(aak);
        int optimizationLevel = this.f4374he.getOptimizationLevel();
        HashMap hashMap = null;
        if (optimizationLevel > 0 && aak.getType() == 135) {
            int functionCount = aak.getFunctionCount();
            for (int i = 0; i != functionCount; i++) {
                org.mozilla1.javascript.optimizer.OptFunctionNode b = org.mozilla1.javascript.optimizer.OptFunctionNode.m11683b(aak, i);
                if (b.eIE.getFunctionType() == 1) {
                    String functionName = b.eIE.getFunctionName();
                    if (functionName.length() != 0) {
                        if (hashMap == null) {
                            hashMap = new HashMap();
                        }
                        hashMap.put(functionName, b);
                    }
                }
            }
        }
        if (hashMap != null) {
            this.f4375hf = new org.mozilla1.javascript.ObjArray();
        }
        new org.mozilla1.javascript.optimizer.OptTransformer(hashMap, this.f4375hf).mo13266a(aak);
        if (optimizationLevel > 0) {
            new org.mozilla1.javascript.optimizer.Optimizer().mo20662i(aak);
        }
    }

    /* renamed from: c */
    private void m21126c(org.mozilla1.javascript.ScriptOrFnNode aak) {
        org.mozilla1.javascript.ObjArray acx = new org.mozilla1.javascript.ObjArray();
        m21120a(aak, acx);
        int size = acx.size();
        this.f4376hg = new org.mozilla1.javascript.ScriptOrFnNode[size];
        acx.toArray(this.f4376hg);
        this.f4377hh = new org.mozilla1.javascript.ObjToIntMap(size);
        for (int i = 0; i != size; i++) {
            this.f4377hh.put(this.f4376hg[i], i);
        }
    }

    /* renamed from: k */
    private byte[] m21136k(String str) {
        boolean z = true;
        boolean z2 = this.f4376hg[0].getType() == 135;
        if (this.f4376hg.length <= 1 && z2) {
            z = false;
        }
        String str2 = null;
        if (this.f4374he.isGenerateDebugInfo()) {
            str2 = this.f4376hg[0].getSourceName();
        }
        ClassFileWriter uh = new ClassFileWriter(this.f4379hj, f4363gT, str2);
        uh.mo5785a(f4365gV, "I", 2);
        uh.mo5785a(f4364gU, this.f4380hk, (short) 2);
        uh.mo5785a(f4368gY, f4369gZ, 2);
        if (z) {
            m21131f(uh);
        }
        if (z2) {
            uh.addInterface("org/mozilla/javascript/Script");
            m21130e(uh);
            m21125c(uh);
            m21127d(uh);
        }
        m21122b(uh);
        m21116a(uh);
        m21118a(uh, str);
        int length = this.f4376hg.length;
        int i = 0;
        while (i != length) {
            org.mozilla1.javascript.ScriptOrFnNode aak = this.f4376hg[i];
            org.mozilla1.javascript.optimizer.BodyCodegen sn = new org.mozilla1.javascript.optimizer.BodyCodegen();
            sn.euH = uh;
            sn.eYT = this;
            sn.f1538he = this.f4374he;
            sn.eYU = aak;
            sn.eYV = i;
            try {
                sn.bNn();
                if (aak.getType() == 108) {
                    org.mozilla1.javascript.optimizer.OptFunctionNode j = org.mozilla1.javascript.optimizer.OptFunctionNode.m11684j(aak);
                    m21123b(uh, j);
                    if (j.bFB()) {
                        m21117a(uh, j);
                    }
                }
                i++;
            } catch (ClassFileWriter.ClassFileFormatException e) {
                throw m21114a(aak, e.getMessage());
            }
        }
        if (this.f4375hf != null) {
            int size = this.f4375hf.size();
            for (int i2 = 0; i2 != size; i2++) {
                uh.mo5785a(m21137m(i2), this.f4380hk, 2);
            }
        }
        m21132g(uh);
        m21133h(uh);
        return uh.toByteArray();
    }

    /* renamed from: a */
    private void m21117a(ClassFileWriter uh, org.mozilla1.javascript.optimizer.OptFunctionNode xq) {
        uh.mo5795b(mo13019f((org.mozilla1.javascript.ScriptOrFnNode) xq.eIE), mo13021h((org.mozilla1.javascript.ScriptOrFnNode) xq.eIE), 10);
        int paramCount = xq.eIE.getParamCount();
        int i = (paramCount * 3) + 4 + 1;
        uh.mo5824nQ(0);
        uh.mo5824nQ(1);
        uh.mo5824nQ(2);
        uh.mo5794b(182, "org/mozilla/javascript/BaseFunction", "createObject", "(Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;)Lorg/mozilla/javascript/Scriptable;");
        uh.mo5819nL(i);
        uh.mo5824nQ(0);
        uh.mo5824nQ(1);
        uh.mo5824nQ(2);
        uh.mo5824nQ(i);
        for (int i2 = 0; i2 < paramCount; i2++) {
            uh.mo5824nQ((i2 * 3) + 4);
            uh.mo5823nP((i2 * 3) + 5);
        }
        uh.mo5824nQ((paramCount * 3) + 4);
        uh.mo5794b(184, this.f4379hj, mo13020g((org.mozilla1.javascript.ScriptOrFnNode) xq.eIE), mo13021h((org.mozilla1.javascript.ScriptOrFnNode) xq.eIE));
        int bya = uh.bya();
        uh.add(89);
        uh.add(193, "org/mozilla/javascript/Scriptable");
        uh.add(153, bya);
        uh.add(192, "org/mozilla/javascript/Scriptable");
        uh.add(176);
        uh.mo5826nS(bya);
        uh.mo5824nQ(i);
        uh.add(176);
        uh.stopMethod((short) (i + 1));
    }

    /* renamed from: a */
    private void m21116a(ClassFileWriter uh) {
        boolean z = false;
        for (org.mozilla1.javascript.ScriptOrFnNode d : this.f4376hg) {
            if (m21128d(d)) {
                z = true;
            }
        }
        if (z) {
            uh.mo5795b("resumeGenerator", "(Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", 17);
            uh.mo5824nQ(0);
            uh.mo5824nQ(1);
            uh.mo5824nQ(2);
            uh.mo5824nQ(4);
            uh.mo5824nQ(5);
            uh.mo5820nM(3);
            uh.bxZ();
            uh.mo5782a(180, uh.getClassName(), f4365gV, "I");
            int R = uh.mo5780R(0, this.f4376hg.length - 1);
            uh.mo5825nR(R);
            int bya = uh.bya();
            for (int i = 0; i < this.f4376hg.length; i++) {
                org.mozilla1.javascript.ScriptOrFnNode aak = this.f4376hg[i];
                uh.mo5809m(R, i, 6);
                if (m21128d(aak)) {
                    uh.mo5794b(184, this.f4379hj, String.valueOf(mo13020g(aak)) + "_gen", "(" + this.f4380hk + "Lorg/mozilla/javascript/Context;" + "Lorg/mozilla/javascript/Scriptable;" + "Ljava/lang/Object;" + "Ljava/lang/Object;I)Ljava/lang/Object;");
                    uh.add(176);
                } else {
                    uh.add(167, bya);
                }
            }
            uh.mo5826nS(bya);
            m21135j(uh);
            uh.add(176);
            uh.stopMethod(6);
        }
    }

    /* renamed from: b */
    private void m21122b(ClassFileWriter uh) {
        int i;
        int paramCount;
        uh.mo5795b("call", "(Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Scriptable;[Ljava/lang/Object;)Ljava/lang/Object;", 17);
        int bya = uh.bya();
        uh.mo5824nQ(1);
        uh.mo5794b(184, "org/mozilla/javascript/ScriptRuntime", "hasTopCall", "(Lorg/mozilla/javascript/Context;)Z");
        uh.add(154, bya);
        uh.mo5824nQ(0);
        uh.mo5824nQ(1);
        uh.mo5824nQ(2);
        uh.mo5824nQ(3);
        uh.mo5824nQ(4);
        uh.mo5794b(184, "org/mozilla/javascript/ScriptRuntime", "doTopCall", "(Lorg/mozilla/javascript/Callable;Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Scriptable;[Ljava/lang/Object;)Ljava/lang/Object;");
        uh.add(176);
        uh.mo5826nS(bya);
        uh.mo5824nQ(0);
        uh.mo5824nQ(1);
        uh.mo5824nQ(2);
        uh.mo5824nQ(3);
        uh.mo5824nQ(4);
        int length = this.f4376hg.length;
        boolean z = 2 <= length;
        if (z) {
            uh.bxZ();
            uh.mo5782a(180, uh.getClassName(), f4365gV, "I");
            i = uh.mo5780R(1, length - 1);
        } else {
            i = 0;
        }
        short s = 0;
        for (int i2 = 0; i2 != length; i2++) {
            org.mozilla1.javascript.ScriptOrFnNode aak = this.f4376hg[i2];
            if (z) {
                if (i2 == 0) {
                    uh.mo5825nR(i);
                    s = uh.byd();
                } else {
                    uh.mo5809m(i, i2 - 1, s);
                }
            }
            if (aak.getType() == 108) {
                org.mozilla1.javascript.optimizer.OptFunctionNode j = org.mozilla1.javascript.optimizer.OptFunctionNode.m11684j(aak);
                if (j.bFB() && (paramCount = j.eIE.getParamCount()) != 0) {
                    for (int i3 = 0; i3 != paramCount; i3++) {
                        uh.add(190);
                        uh.mo5814nG(i3);
                        int bya2 = uh.bya();
                        int bya3 = uh.bya();
                        uh.add(164, bya2);
                        uh.mo5824nQ(4);
                        uh.mo5814nG(i3);
                        uh.add(50);
                        uh.add(167, bya3);
                        uh.mo5826nS(bya2);
                        m21135j(uh);
                        uh.mo5826nS(bya3);
                        uh.mo5828nV(-1);
                        uh.mo5779F(org.mozilla1.javascript.ScriptRuntime.NaN);
                        uh.mo5824nQ(4);
                    }
                }
            }
            uh.mo5794b(184, this.f4379hj, mo13020g(aak), mo13021h(aak));
            uh.add(176);
        }
        uh.stopMethod(5);
    }

    /* renamed from: c */
    private void m21125c(ClassFileWriter uh) {
        uh.mo5795b("main", "([Ljava/lang/String;)V", 9);
        uh.add(187, uh.getClassName());
        uh.add(89);
        uh.mo5794b(183, uh.getClassName(), "<init>", "()V");
        uh.add(42);
        uh.mo5794b(184, this.f4378hi, "main", "(Lorg/mozilla/javascript/Script;[Ljava/lang/String;)V");
        uh.add(177);
        uh.stopMethod(1);
    }

    /* renamed from: d */
    private void m21127d(ClassFileWriter uh) {
        uh.mo5795b("exec", "(Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;)Ljava/lang/Object;", 17);
        uh.bxZ();
        uh.mo5824nQ(1);
        uh.mo5824nQ(2);
        uh.add(89);
        uh.add(1);
        uh.mo5794b(182, uh.getClassName(), "call", "(Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Scriptable;[Ljava/lang/Object;)Ljava/lang/Object;");
        uh.add(176);
        uh.stopMethod(3);
    }

    /* renamed from: e */
    private void m21130e(ClassFileWriter uh) {
        uh.mo5795b("<init>", "()V", 1);
        uh.bxZ();
        uh.mo5794b(183, f4363gT, "<init>", "()V");
        uh.bxZ();
        uh.mo5814nG(0);
        uh.mo5782a(181, uh.getClassName(), f4365gV, "I");
        uh.add(177);
        uh.stopMethod(1);
    }

    /* renamed from: f */
    private void m21131f(ClassFileWriter uh) {
        int i;
        int i2;
        short s = 0;
        uh.mo5795b("<init>", f4371hb, 1);
        uh.mo5824nQ(0);
        uh.mo5794b(183, f4363gT, "<init>", "()V");
        uh.bxZ();
        uh.mo5820nM(3);
        uh.mo5782a(181, uh.getClassName(), f4365gV, "I");
        uh.bxZ();
        uh.mo5824nQ(2);
        uh.mo5824nQ(1);
        if (this.f4376hg[0].getType() == 135) {
            i = 1;
        } else {
            i = 0;
        }
        int length = this.f4376hg.length;
        if (i == length) {
            throw m21129dh();
        }
        boolean z = 2 <= length - i;
        if (z) {
            uh.mo5820nM(3);
            i2 = uh.mo5780R(i + 1, length - 1);
        } else {
            i2 = 0;
        }
        for (int i3 = i; i3 != length; i3++) {
            if (z) {
                if (i3 == i) {
                    uh.mo5825nR(i2);
                    s = uh.byd();
                } else {
                    uh.mo5809m(i2, (i3 - 1) - i, s);
                }
            }
            uh.mo5794b(182, this.f4379hj, mo13013a(org.mozilla1.javascript.optimizer.OptFunctionNode.m11684j(this.f4376hg[i3])), f4370ha);
            uh.add(177);
        }
        uh.stopMethod(4);
    }

    /* renamed from: b */
    private void m21123b(ClassFileWriter uh, org.mozilla1.javascript.optimizer.OptFunctionNode xq) {
        uh.mo5795b(mo13013a(xq), f4370ha, 18);
        uh.bxZ();
        uh.mo5824nQ(1);
        uh.mo5824nQ(2);
        uh.mo5794b(182, "org/mozilla/javascript/NativeFunction", "initScriptFunction", f4370ha);
        if (xq.eIE.getRegexpCount() != 0) {
            uh.bxZ();
            mo13016a(uh, (org.mozilla1.javascript.ScriptOrFnNode) xq.eIE, 1, 2);
            uh.mo5782a(181, this.f4379hj, f4368gY, f4369gZ);
        }
        uh.add(177);
        uh.stopMethod(3);
    }

    /* renamed from: a */
    private void m21118a(ClassFileWriter uh, String str) {
        short s;
        int i;
        short s2;
        uh.mo5795b("getLanguageVersion", "()I", 1);
        uh.mo5814nG(this.f4374he.getLanguageVersion());
        uh.add(172);
        uh.stopMethod(1);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 != 6) {
                if (i3 != 4 || str != null) {
                    switch (i3) {
                        case 0:
                            uh.mo5795b("getFunctionName", "()Ljava/lang/String;", 1);
                            s = 1;
                            break;
                        case 1:
                            uh.mo5795b("getParamCount", "()I", 1);
                            s = 1;
                            break;
                        case 2:
                            uh.mo5795b("getParamAndVarCount", "()I", (short) 1);
                            s = 1;
                            break;
                        case 3:
                            uh.mo5795b("getParamOrVarName", "(I)Ljava/lang/String;", (short) 1);
                            s = 2;
                            break;
                        case 4:
                            uh.mo5795b("getEncodedSource", "()Ljava/lang/String;", (short) 1);
                            uh.mo5804gJ(str);
                            s = 1;
                            break;
                        case 5:
                            uh.mo5795b("getParamOrVarConst", "(I)Z", (short) 1);
                            s = 3;
                            break;
                        default:
                            throw org.mozilla1.javascript.Kit.codeBug();
                    }
                    int length = this.f4376hg.length;
                    short s3 = 0;
                    if (length > 1) {
                        uh.bxZ();
                        uh.mo5782a(180, uh.getClassName(), f4365gV, "I");
                        i = uh.mo5780R(1, length - 1);
                    } else {
                        i = 0;
                    }
                    int i4 = 0;
                    while (i4 != length) {
                        org.mozilla1.javascript.ScriptOrFnNode aak = this.f4376hg[i4];
                        if (i4 == 0) {
                            if (length > 1) {
                                uh.mo5825nR(i);
                                s2 = uh.byd();
                            }
                            s2 = s3;
                        } else {
                            uh.mo5809m(i, i4 - 1, s3);
                            s2 = s3;
                        }
                        switch (i3) {
                            case 0:
                                if (aak.getType() == 135) {
                                    uh.mo5804gJ("");
                                } else {
                                    uh.mo5804gJ(((org.mozilla1.javascript.FunctionNode) aak).getFunctionName());
                                }
                                uh.add(176);
                                break;
                            case 1:
                                uh.mo5814nG(aak.getParamCount());
                                uh.add(172);
                                break;
                            case 2:
                                uh.mo5814nG(aak.getParamAndVarCount());
                                uh.add(172);
                                break;
                            case 3:
                                int paramAndVarCount = aak.getParamAndVarCount();
                                if (paramAndVarCount != 0) {
                                    if (paramAndVarCount != 1) {
                                        uh.mo5820nM(1);
                                        int R = uh.mo5780R(1, paramAndVarCount - 1);
                                        for (int i5 = 0; i5 != paramAndVarCount; i5++) {
                                            if (uh.byd() != 0) {
                                                org.mozilla1.javascript.Kit.codeBug();
                                            }
                                            String paramOrVarName = aak.getParamOrVarName(i5);
                                            if (i5 == 0) {
                                                uh.mo5825nR(R);
                                            } else {
                                                uh.mo5809m(R, i5 - 1, 0);
                                            }
                                            uh.mo5804gJ(paramOrVarName);
                                            uh.add(176);
                                        }
                                        break;
                                    } else {
                                        uh.mo5804gJ(aak.getParamOrVarName(0));
                                        uh.add(176);
                                        break;
                                    }
                                } else {
                                    uh.add(1);
                                    uh.add(176);
                                    break;
                                }
                            case 4:
                                uh.mo5814nG(aak.getEncodedSourceStart());
                                uh.mo5814nG(aak.getEncodedSourceEnd());
                                uh.mo5794b(182, "java/lang/String", "substring", "(II)Ljava/lang/String;");
                                uh.add(176);
                                break;
                            case 5:
                                int paramAndVarCount2 = aak.getParamAndVarCount();
                                boolean[] cHr = aak.cHr();
                                if (paramAndVarCount2 != 0) {
                                    if (paramAndVarCount2 != 1) {
                                        uh.mo5820nM(1);
                                        int R2 = uh.mo5780R(1, paramAndVarCount2 - 1);
                                        for (int i6 = 0; i6 != paramAndVarCount2; i6++) {
                                            if (uh.byd() != 0) {
                                                org.mozilla1.javascript.Kit.codeBug();
                                            }
                                            if (i6 == 0) {
                                                uh.mo5825nR(R2);
                                            } else {
                                                uh.mo5809m(R2, i6 - 1, 0);
                                            }
                                            uh.mo5800dx(cHr[i6]);
                                            uh.add(172);
                                        }
                                        break;
                                    } else {
                                        uh.mo5800dx(cHr[0]);
                                        uh.add(172);
                                        break;
                                    }
                                } else {
                                    uh.add(3);
                                    uh.add(172);
                                    break;
                                }
                            default:
                                throw org.mozilla1.javascript.Kit.codeBug();
                        }
                        i4++;
                        s3 = s2;
                    }
                    uh.stopMethod(s);
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    /* renamed from: g */
    private void m21132g(ClassFileWriter uh) {
        int i = 0;
        for (int i2 = 0; i2 != this.f4376hg.length; i2++) {
            i += this.f4376hg[i2].getRegexpCount();
        }
        if (i != 0) {
            uh.mo5795b(f4366gW, f4367gX, 42);
            uh.mo5785a("_reInitDone", "Z", 10);
            uh.mo5782a(178, this.f4379hj, "_reInitDone", "Z");
            int bya = uh.bya();
            uh.add(153, bya);
            uh.add(177);
            uh.mo5826nS(bya);
            for (int i3 = 0; i3 != this.f4376hg.length; i3++) {
                org.mozilla1.javascript.ScriptOrFnNode aak = this.f4376hg[i3];
                int regexpCount = aak.getRegexpCount();
                for (int i4 = 0; i4 != regexpCount; i4++) {
                    String a = mo13014a(aak, i4);
                    String regexpString = aak.getRegexpString(i4);
                    String regexpFlags = aak.getRegexpFlags(i4);
                    uh.mo5785a(a, "Ljava/lang/Object;", 10);
                    uh.mo5824nQ(0);
                    uh.mo5824nQ(1);
                    uh.mo5804gJ(regexpString);
                    if (regexpFlags == null) {
                        uh.add(1);
                    } else {
                        uh.mo5804gJ(regexpFlags);
                    }
                    uh.mo5794b(185, "org/mozilla/javascript/RegExpProxy", "compileRegExp", "(Lorg/mozilla/javascript/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;");
                    uh.mo5782a(179, this.f4379hj, a, "Ljava/lang/Object;");
                }
            }
            uh.mo5814nG(1);
            uh.mo5782a(179, this.f4379hj, "_reInitDone", "Z");
            uh.add(177);
            uh.stopMethod(2);
        }
    }

    /* renamed from: h */
    private void m21133h(ClassFileWriter uh) {
        int i = this.f4382hm;
        if (i != 0) {
            uh.mo5795b("<clinit>", "()V", 24);
            double[] dArr = this.f4381hl;
            for (int i2 = 0; i2 != i; i2++) {
                double d = dArr[i2];
                String str = "_k" + i2;
                String a = m21115a(d);
                uh.mo5785a(str, a, 10);
                int i3 = (int) d;
                if (((double) i3) == d) {
                    uh.add(187, "java/lang/Integer");
                    uh.add(89);
                    uh.mo5814nG(i3);
                    uh.mo5794b(183, "java/lang/Integer", "<init>", "(I)V");
                } else {
                    uh.mo5779F(d);
                    m21134i(uh);
                }
                uh.mo5782a(179, this.f4379hj, str, a);
            }
            uh.add(177);
            uh.stopMethod(0);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13016a(ClassFileWriter uh, org.mozilla1.javascript.ScriptOrFnNode aak, int i, int i2) {
        int regexpCount = aak.getRegexpCount();
        if (regexpCount == 0) {
            throw m21129dh();
        }
        uh.mo5814nG(regexpCount);
        uh.add(189, "java/lang/Object");
        uh.mo5824nQ(i);
        uh.mo5794b(184, "org/mozilla/javascript/ScriptRuntime", "checkRegExpProxy", "(Lorg/mozilla/javascript/Context;)Lorg/mozilla/javascript/RegExpProxy;");
        uh.add(89);
        uh.mo5824nQ(i);
        uh.mo5794b(184, this.f4379hj, f4366gW, f4367gX);
        for (int i3 = 0; i3 != regexpCount; i3++) {
            uh.add(92);
            uh.mo5824nQ(i);
            uh.mo5824nQ(i2);
            uh.mo5782a(178, this.f4379hj, mo13014a(aak, i3), "Ljava/lang/Object;");
            uh.mo5794b(185, "org/mozilla/javascript/RegExpProxy", "wrapRegExp", "(Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;Ljava/lang/Object;)Lorg/mozilla/javascript/Scriptable;");
            uh.mo5814nG(i3);
            uh.add(95);
            uh.add(83);
        }
        uh.add(87);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13015a(ClassFileWriter uh, double d) {
        int i = 0;
        if (d == org.mozilla1.javascript.ScriptRuntime.NaN) {
            if (1.0d / d > ScriptRuntime.NaN) {
                uh.mo5782a(178, "org/mozilla/javascript/optimizer/OptRuntime", "zeroObj", "Ljava/lang/Double;");
                return;
            }
            uh.mo5779F(d);
            m21134i(uh);
        } else if (d == 1.0d) {
            uh.mo5782a(178, "org/mozilla/javascript/optimizer/OptRuntime", "oneObj", "Ljava/lang/Double;");
        } else if (d == -1.0d) {
            uh.mo5782a(178, "org/mozilla/javascript/optimizer/OptRuntime", "minusOneObj", "Ljava/lang/Double;");
        } else if (d != d) {
            uh.mo5782a(178, "org/mozilla/javascript/ScriptRuntime", "NaNobj", "Ljava/lang/Double;");
        } else if (this.f4382hm >= 2000) {
            uh.mo5779F(d);
            m21134i(uh);
        } else {
            int i2 = this.f4382hm;
            if (i2 == 0) {
                this.f4381hl = new double[64];
            } else {
                double[] dArr = this.f4381hl;
                int i3 = 0;
                while (i3 != i2 && dArr[i3] != d) {
                    i3++;
                }
                if (i2 == dArr.length) {
                    double[] dArr2 = new double[(i2 * 2)];
                    System.arraycopy(this.f4381hl, 0, dArr2, 0, i2);
                    this.f4381hl = dArr2;
                }
                i = i3;
            }
            if (i == i2) {
                this.f4381hl[i2] = d;
                this.f4382hm = i2 + 1;
            }
            uh.mo5782a(178, this.f4379hj, "_k" + i, m21115a(d));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public int mo13018e(org.mozilla1.javascript.ScriptOrFnNode aak) {
        return this.f4377hh.getExisting(aak);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: f */
    public String mo13019f(org.mozilla1.javascript.ScriptOrFnNode aak) {
        return "_n" + mo13018e(aak);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public String mo13020g(org.mozilla1.javascript.ScriptOrFnNode aak) {
        return "_c" + mo13018e(aak);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h */
    public String mo13021h(org.mozilla1.javascript.ScriptOrFnNode aak) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append('(');
        stringBuffer.append(this.f4380hk);
        stringBuffer.append("Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Scriptable;");
        if (aak.getType() == 108) {
            org.mozilla1.javascript.optimizer.OptFunctionNode j = org.mozilla1.javascript.optimizer.OptFunctionNode.m11684j(aak);
            if (j.bFB()) {
                int paramCount = j.eIE.getParamCount();
                for (int i = 0; i != paramCount; i++) {
                    stringBuffer.append("Ljava/lang/Object;D");
                }
            }
        }
        stringBuffer.append("[Ljava/lang/Object;)Ljava/lang/Object;");
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public String mo13013a(org.mozilla1.javascript.optimizer.OptFunctionNode xq) {
        return "_i" + mo13018e((org.mozilla1.javascript.ScriptOrFnNode) xq.eIE);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public String mo13014a(org.mozilla1.javascript.ScriptOrFnNode aak, int i) {
        return f4368gY + mo13018e(aak) + "_" + i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: l */
    public void mo13022l(String str) {
        this.f4378hi = str;
    }
}
