package org.mozilla1.javascript.optimizer;

import org.mozilla1.javascript.FunctionNode;
import org.mozilla1.javascript.Kit;
import org.mozilla1.javascript.Node;
import org.mozilla1.javascript.ScriptOrFnNode;

/* renamed from: a.Xq */
/* compiled from: a */
public final class OptFunctionNode {
    org.mozilla1.javascript.FunctionNode eIE;
    boolean eII;
    boolean eIJ;
    private boolean[] eIF;
    private int eIG = -1;
    private boolean eIH;

    OptFunctionNode(FunctionNode yq) {
        this.eIE = yq;
        yq.setCompilerData(this);
    }

    /* renamed from: b */
    static OptFunctionNode m11683b(ScriptOrFnNode aak, int i) {
        return (OptFunctionNode) aak.mo7704wM(i).getCompilerData();
    }

    /* renamed from: j */
    static OptFunctionNode m11684j(ScriptOrFnNode aak) {
        return (OptFunctionNode) aak.getCompilerData();
    }

    /* access modifiers changed from: package-private */
    public boolean bFB() {
        return this.eIG >= 0;
    }

    /* access modifiers changed from: package-private */
    public int bFC() {
        return this.eIG;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: pc */
    public void mo6839pc(int i) {
        if (i < 0 || this.eIG >= 0) {
            Kit.codeBug();
        }
        this.eIG = i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: dG */
    public void mo6837dG(boolean z) {
        this.eIH = z;
    }

    /* access modifiers changed from: package-private */
    public boolean bFD() {
        return this.eIH;
    }

    /* access modifiers changed from: package-private */
    public int bFE() {
        return this.eIE.getParamAndVarCount();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: pd */
    public boolean mo6840pd(int i) {
        return i < this.eIE.getParamCount();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: pe */
    public boolean mo6841pe(int i) {
        int paramCount = i - this.eIE.getParamCount();
        if (paramCount < 0 || this.eIF == null) {
            return false;
        }
        return this.eIF[paramCount];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: pf */
    public void mo6842pf(int i) {
        int paramCount = i - this.eIE.getParamCount();
        if (paramCount < 0) {
            Kit.codeBug();
        }
        if (this.eIF == null) {
            this.eIF = new boolean[(this.eIE.getParamAndVarCount() - this.eIE.getParamCount())];
        }
        this.eIF[paramCount] = true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public int mo6838g(Node qlVar) {
        Node cFE;
        int intProp = qlVar.getIntProp(7, -1);
        if (intProp == -1) {
            int type = qlVar.getType();
            if (type == 55) {
                cFE = qlVar;
            } else if (type == 56 || type == 155) {
                cFE = qlVar.cFE();
            } else {
                throw Kit.codeBug();
            }
            intProp = this.eIE.mo7678K(cFE);
            if (intProp < 0) {
                throw Kit.codeBug();
            }
            qlVar.putIntProp(7, intProp);
        }
        return intProp;
    }
}
