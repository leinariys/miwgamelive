package org.mozilla1.javascript;


import java.util.Arrays;

/* renamed from: a.ayT */
/* compiled from: a */
public class NativeArray extends IdScriptableObject {
    static final long serialVersionUID = 7331366857676127338L;
    private static final int DEFAULT_INITIAL_CAPACITY = 10;
    private static final int cBB = 3;
    private static final int ceN = 4;
    private static final int eVn = 1;
    private static final int eVp = 1;
    private static final int gWA = -12;
    private static final int gWB = -17;
    private static final int gWC = -18;
    private static final int gWD = -19;
    private static final int gWE = -20;
    private static final int gWF = -21;
    private static final double gWK = 1.5d;
    private static final int gWL = 1431655764;
    private static final Object gWe = new Integer(10);
    private static final Integer gWf = new Integer(-1);
    private static final int gWg = 5;
    private static final int gWh = 6;
    private static final int gWi = 7;
    private static final int gWj = 8;
    private static final int gWk = 9;
    private static final int gWl = 10;
    private static final int gWm = 11;
    private static final int gWn = 12;
    private static final int gWo = 17;
    private static final int gWp = 18;
    private static final int gWq = 19;
    private static final int gWr = 20;
    private static final int gWs = 21;
    private static final int gWt = -5;
    private static final int gWu = -6;
    private static final int gWv = -7;
    private static final int gWw = -8;
    private static final int gWx = -9;
    private static final int gWy = -10;
    private static final int gWz = -11;
    private static final int gpB = 15;
    private static final int gpC = 16;
    private static final int gpI = 13;
    private static final int gpJ = 14;
    private static final int gqh = -15;
    private static final int gqi = -16;
    private static final int gqo = -13;
    private static final int gqp = -14;

    /* renamed from: ns */
    private static final int f5613ns = 1;

    /* renamed from: nt */
    private static final int f5614nt = 21;
    /* renamed from: xT */
    private static final int f5615xT = 2;
    private static int gWJ = 10000;
    private long gWG;
    private Object[] gWH;
    private boolean gWI;

    public NativeArray(long j) {
        this.gWI = j <= ((long) gWJ);
        if (this.gWI) {
            int i = (int) j;
            this.gWH = new Object[(i < 10 ? 10 : i)];
            Arrays.fill(this.gWH, org.mozilla1.javascript.Scriptable.NOT_FOUND);
        }
        this.gWG = j;
    }

    public NativeArray(Object[] objArr) {
        this.gWI = true;
        this.gWH = objArr;
        this.gWG = (long) objArr.length;
    }

    /* renamed from: a */
    static void m27402a(org.mozilla1.javascript.Scriptable avf, boolean z) {
        new NativeArray(0).mo15695a(21, avf, z);
    }

    static int cEe() {
        return gWJ;
    }

    /* renamed from: wi */
    static void m27424wi(int i) {
        gWJ = i;
    }

    /* renamed from: kL */
    private static long m27421kL(String str) {
        double number = org.mozilla1.javascript.ScriptRuntime.toNumber(str);
        if (number == number) {
            long uint32 = org.mozilla1.javascript.ScriptRuntime.toUint32(number);
            if (((double) uint32) != number || uint32 == 4294967295L || !Long.toString(uint32).equals(str)) {
                return -1;
            }
            return uint32;
        }
        return -1;
    }

    /* renamed from: b */
    private static Object m27412b(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr) {
        if (objArr.length == 0) {
            return new NativeArray(0);
        }
        if (lhVar.getLanguageVersion() == 120) {
            return new NativeArray(objArr);
        }
        Number number = (Number) objArr[0];
        if (objArr.length > 1 || !(number instanceof Number)) {
            return new NativeArray(objArr);
        }
        long uint32 = org.mozilla1.javascript.ScriptRuntime.toUint32((Object) number);
        if (((double) uint32) == number.doubleValue()) {
            return new NativeArray(uint32);
        }
        throw org.mozilla1.javascript.Context.m35244gE("msg.arraylength.bad");
    }

    /* renamed from: f */
    static long m27415f(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf) {
        if (avf instanceof org.mozilla1.javascript.NativeString) {
            return (long) ((org.mozilla1.javascript.NativeString) avf).getLength();
        }
        if (avf instanceof NativeArray) {
            return ((NativeArray) avf).getLength();
        }
        return org.mozilla1.javascript.ScriptRuntime.toUint32(org.mozilla1.javascript.ScriptRuntime.m7499a(avf, "length", lhVar));
    }

    /* renamed from: a */
    private static Object m27398a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, long j) {
        return org.mozilla1.javascript.ScriptRuntime.m7501a(avf, "length", (Object) org.mozilla1.javascript.ScriptRuntime.wrapNumber((double) j), lhVar);
    }

    /* renamed from: a */
    private static void m27401a(org.mozilla1.javascript.Scriptable avf, long j) {
        int i = (int) j;
        if (((long) i) == j) {
            avf.delete(i);
        } else {
            avf.delete(Long.toString(j));
        }
    }

    /* renamed from: b */
    private static Object m27411b(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, long j) {
        if (j > 2147483647L) {
            return org.mozilla1.javascript.ScriptRuntime.m7499a(avf, Long.toString(j), lhVar);
        }
        return org.mozilla1.javascript.ScriptRuntime.m7491a(avf, (int) j, lhVar);
    }

    /* renamed from: a */
    private static void m27403a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, long j, Object obj) {
        if (j > 2147483647L) {
            org.mozilla1.javascript.ScriptRuntime.m7501a(avf, Long.toString(j), obj, lhVar);
        } else {
            org.mozilla1.javascript.ScriptRuntime.m7492a(avf, (int) j, obj, lhVar);
        }
    }

    /* renamed from: a */
    private static String m27400a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, boolean z, boolean z2) {
        String str;
        boolean has;
        boolean z3;
        long j;
        boolean z4;
        long f = m27415f(lhVar, avf2);
        StringBuffer stringBuffer = new StringBuffer(256);
        if (z) {
            stringBuffer.append('[');
            str = ", ";
        } else {
            str = ",";
        }
        boolean z5 = false;
        if (lhVar.elm == null) {
            has = false;
            lhVar.elm = new ObjToIntMap(31);
            z3 = true;
        } else {
            has = lhVar.elm.has(avf2);
            z3 = false;
        }
        if (!has) {
            try {
                lhVar.elm.put(avf2, 0);
                long j2 = 0;
                boolean z6 = false;
                while (j2 < f) {
                    if (j2 > 0) {
                        stringBuffer.append(str);
                    }
                    Object b = m27411b(lhVar, avf2, j2);
                    if (b == null || b == org.mozilla1.javascript.Undefined.instance) {
                        z4 = false;
                    } else if (z) {
                        stringBuffer.append(org.mozilla1.javascript.ScriptRuntime.m7571d(lhVar, avf, b));
                        z4 = true;
                    } else if (b instanceof String) {
                        String str2 = (String) b;
                        if (z) {
                            stringBuffer.append('\"');
                            stringBuffer.append(org.mozilla1.javascript.ScriptRuntime.escapeString(str2));
                            stringBuffer.append('\"');
                            z4 = true;
                        } else {
                            stringBuffer.append(str2);
                            z4 = true;
                        }
                    } else {
                        if (z2) {
                            b = org.mozilla1.javascript.ScriptRuntime.m7568d(b, "toLocaleString", lhVar).call(lhVar, avf, org.mozilla1.javascript.ScriptRuntime.m7605q(lhVar), org.mozilla1.javascript.ScriptRuntime.emptyArgs);
                        }
                        stringBuffer.append(org.mozilla1.javascript.ScriptRuntime.toString(b));
                        z4 = true;
                    }
                    j2++;
                    z6 = z4;
                }
                j = j2;
                z5 = z6;
            } catch (Throwable th) {
                if (z3) {
                    lhVar.elm = null;
                }
                throw th;
            }
        } else {
            j = 0;
        }
        if (z3) {
            lhVar.elm = null;
        }
        if (z) {
            if (z5 || j <= 0) {
                stringBuffer.append(']');
            } else {
                stringBuffer.append(", ]");
            }
        }
        return stringBuffer.toString();
    }

    /* renamed from: g */
    private static String m27416g(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr) {
        String str;
        Object obj;
        long f = m27415f(lhVar, avf);
        int i = (int) f;
        if (f != ((long) i)) {
            throw org.mozilla1.javascript.Context.m35246q("msg.arraylength.too.big", String.valueOf(f));
        }
        if (objArr.length < 1 || objArr[0] == org.mozilla1.javascript.Undefined.instance) {
            str = ",";
        } else {
            str = org.mozilla1.javascript.ScriptRuntime.toString(objArr[0]);
        }
        if (avf instanceof NativeArray) {
            NativeArray ayt = (NativeArray) avf;
            if (ayt.gWI) {
                StringBuffer stringBuffer = new StringBuffer();
                for (int i2 = 0; i2 < i; i2++) {
                    if (i2 != 0) {
                        stringBuffer.append(str);
                    }
                    if (!(i2 >= ayt.gWH.length || (obj = ayt.gWH[i2]) == null || obj == org.mozilla1.javascript.Undefined.instance || obj == org.mozilla1.javascript.Scriptable.NOT_FOUND)) {
                        stringBuffer.append(org.mozilla1.javascript.ScriptRuntime.toString(obj));
                    }
                }
                return stringBuffer.toString();
            }
        }
        if (i == 0) {
            return "";
        }
        String[] strArr = new String[i];
        int i3 = 0;
        for (int i4 = 0; i4 != i; i4++) {
            Object b = m27411b(lhVar, avf, (long) i4);
            if (!(b == null || b == org.mozilla1.javascript.Undefined.instance)) {
                String nh = org.mozilla1.javascript.ScriptRuntime.toString(b);
                i3 += nh.length();
                strArr[i4] = nh;
            }
        }
        StringBuffer stringBuffer2 = new StringBuffer(i3 + ((i - 1) * str.length()));
        for (int i5 = 0; i5 != i; i5++) {
            if (i5 != 0) {
                stringBuffer2.append(str);
            }
            String str2 = strArr[i5];
            if (str2 != null) {
                stringBuffer2.append(str2);
            }
        }
        return stringBuffer2.toString();
    }

    /* renamed from: h */
    private static org.mozilla1.javascript.Scriptable m27417h(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr) {
        if (avf instanceof NativeArray) {
            NativeArray ayt = (NativeArray) avf;
            if (ayt.gWI) {
                int i = 0;
                for (int i2 = ((int) ayt.gWG) - 1; i < i2; i2--) {
                    Object obj = ayt.gWH[i];
                    ayt.gWH[i] = ayt.gWH[i2];
                    ayt.gWH[i2] = obj;
                    i++;
                }
                return avf;
            }
        }
        long f = m27415f(lhVar, avf);
        long j = f / 2;
        for (long j2 = 0; j2 < j; j2++) {
            long j3 = (f - j2) - 1;
            Object b = m27411b(lhVar, avf, j2);
            m27403a(lhVar, avf, j2, m27411b(lhVar, avf, j3));
            m27403a(lhVar, avf, j3, b);
        }
        return avf;
    }

    /* renamed from: b */
    private static org.mozilla1.javascript.Scriptable m27410b(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        Object obj;
        Object[] objArr2;
        long f = m27415f(lhVar, avf2);
        if (f > 1) {
            if (objArr.length <= 0 || org.mozilla1.javascript.Undefined.instance == objArr[0]) {
                obj = null;
                objArr2 = null;
            } else {
                obj = objArr[0];
                objArr2 = new Object[2];
            }
            if (avf2 instanceof NativeArray) {
                NativeArray ayt = (NativeArray) avf2;
                if (ayt.gWI) {
                    m27407a(lhVar, avf, ayt.gWH, (int) f, obj, objArr2);
                }
            }
            if (f >= 2147483647L) {
                m27404a(lhVar, avf, avf2, f, obj, objArr2);
            } else {
                int i = (int) f;
                Object[] objArr3 = new Object[i];
                for (int i2 = 0; i2 != i; i2++) {
                    objArr3[i2] = m27411b(lhVar, avf2, (long) i2);
                }
                m27407a(lhVar, avf, objArr3, i, obj, objArr2);
                for (int i3 = 0; i3 != i; i3++) {
                    m27403a(lhVar, avf2, (long) i3, objArr3[i3]);
                }
            }
        }
        return avf2;
    }

    /* renamed from: a */
    private static boolean m27408a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object obj, Object obj2, Object obj3, Object[] objArr) {
        if (obj3 == null) {
            if (objArr != null) {
                org.mozilla1.javascript.Kit.codeBug();
            }
        } else if (objArr == null || objArr.length != 2) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        Object obj4 = org.mozilla1.javascript.Undefined.instance;
        Object obj5 = org.mozilla1.javascript.Scriptable.NOT_FOUND;
        if (obj2 == obj4 || obj2 == obj5) {
            return false;
        }
        if (obj == obj4 || obj == obj5) {
            return true;
        }
        if (obj3 != null) {
            objArr[0] = obj;
            objArr[1] = obj2;
            if (org.mozilla1.javascript.ScriptRuntime.toNumber(org.mozilla1.javascript.ScriptRuntime.m7573e(obj3, lhVar).call(lhVar, avf, org.mozilla1.javascript.ScriptRuntime.m7605q(lhVar), objArr)) <= org.mozilla1.javascript.ScriptRuntime.NaN) {
                return false;
            }
            return true;
        } else if (org.mozilla1.javascript.ScriptRuntime.toString(obj).compareTo(org.mozilla1.javascript.ScriptRuntime.toString(obj2)) <= 0) {
            return false;
        } else {
            return true;
        }
    }

    /* renamed from: a */
    private static void m27407a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr, int i, Object obj, Object[] objArr2) {
        if (i <= 1) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        int i2 = i / 2;
        while (i2 != 0) {
            i2--;
            m27406a(lhVar, avf, objArr[i2], objArr, i2, i, obj, objArr2);
        }
        while (i != 1) {
            int i3 = i - 1;
            Object obj2 = objArr[i3];
            objArr[i3] = objArr[0];
            m27406a(lhVar, avf, obj2, objArr, 0, i3, obj, objArr2);
            i = i3;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x002b A[LOOP:0: B:0:0x0000->B:11:0x002b, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0006 A[EDGE_INSN: B:14:0x0006->B:2:0x0006 ?: BREAK  , SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m27406a(org.mozilla1.javascript.Context r7, org.mozilla1.javascript.Scriptable r8, java.lang.Object r9, java.lang.Object[] r10, int r11, int r12, java.lang.Object r13, java.lang.Object[] r14) {
        /*
        L_0x0000:
            int r0 = r11 * 2
            int r6 = r0 + 1
            if (r6 < r12) goto L_0x0009
        L_0x0006:
            r10[r11] = r9
            return
        L_0x0009:
            r3 = r10[r6]
            int r0 = r6 + 1
            if (r0 >= r12) goto L_0x002f
            int r0 = r6 + 1
            r2 = r10[r0]
            r0 = r7
            r1 = r8
            r4 = r13
            r5 = r14
            boolean r0 = m27408a((p001a.C2909lh) r0, (p001a.aVF) r1, (java.lang.Object) r2, (java.lang.Object) r3, (java.lang.Object) r4, (java.lang.Object[]) r5)
            if (r0 == 0) goto L_0x002f
            int r0 = r6 + 1
            r6 = r0
        L_0x0020:
            r0 = r7
            r1 = r8
            r3 = r9
            r4 = r13
            r5 = r14
            boolean r0 = m27408a((p001a.C2909lh) r0, (p001a.aVF) r1, (java.lang.Object) r2, (java.lang.Object) r3, (java.lang.Object) r4, (java.lang.Object[]) r5)
            if (r0 == 0) goto L_0x0006
            r10[r11] = r2
            r11 = r6
            goto L_0x0000
        L_0x002f:
            r2 = r3
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.ayT.m27406a(a.lh, a.aVF, java.lang.Object, java.lang.Object[], int, int, java.lang.Object, java.lang.Object[]):void");
    }

    /* renamed from: a */
    private static void m27404a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, long j, Object obj, Object[] objArr) {
        if (j <= 1) {
            Kit.codeBug();
        }
        long j2 = j / 2;
        while (j2 != 0) {
            j2--;
            m27405a(lhVar, avf, m27411b(lhVar, avf2, j2), avf2, j2, j, obj, objArr);
        }
        while (j != 1) {
            long j3 = j - 1;
            Object b = m27411b(lhVar, avf2, j3);
            m27403a(lhVar, avf2, j3, m27411b(lhVar, avf2, 0));
            m27405a(lhVar, avf, b, avf2, 0, j3, obj, objArr);
            j = j3;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x003e A[LOOP:0: B:0:0x0000->B:11:0x003e, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x000b A[EDGE_INSN: B:14:0x000b->B:2:0x000b ?: BREAK  , SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m27405a(org.mozilla1.javascript.Context r8, org.mozilla1.javascript.Scriptable r9, java.lang.Object r10, org.mozilla1.javascript.Scriptable r11, long r12, long r14, java.lang.Object r16, java.lang.Object[] r17) {
        /*
        L_0x0000:
            r0 = 2
            long r0 = r0 * r12
            r2 = 1
            long r6 = r0 + r2
            int r0 = (r6 > r14 ? 1 : (r6 == r14 ? 0 : -1))
            if (r0 < 0) goto L_0x000f
        L_0x000b:
            m27403a((p001a.C2909lh) r8, (p001a.aVF) r11, (long) r12, (java.lang.Object) r10)
            return
        L_0x000f:
            java.lang.Object r3 = m27411b((p001a.C2909lh) r8, (p001a.aVF) r11, (long) r6)
            r0 = 1
            long r0 = r0 + r6
            int r0 = (r0 > r14 ? 1 : (r0 == r14 ? 0 : -1))
            if (r0 >= 0) goto L_0x0043
            r0 = 1
            long r0 = r0 + r6
            java.lang.Object r2 = m27411b((p001a.C2909lh) r8, (p001a.aVF) r11, (long) r0)
            r0 = r8
            r1 = r9
            r4 = r16
            r5 = r17
            boolean r0 = m27408a((p001a.C2909lh) r0, (p001a.aVF) r1, (java.lang.Object) r2, (java.lang.Object) r3, (java.lang.Object) r4, (java.lang.Object[]) r5)
            if (r0 == 0) goto L_0x0043
            r0 = 1
            long r0 = r0 + r6
            r6 = r0
        L_0x0031:
            r0 = r8
            r1 = r9
            r3 = r10
            r4 = r16
            r5 = r17
            boolean r0 = m27408a((p001a.C2909lh) r0, (p001a.aVF) r1, (java.lang.Object) r2, (java.lang.Object) r3, (java.lang.Object) r4, (java.lang.Object[]) r5)
            if (r0 == 0) goto L_0x000b
            m27403a((p001a.C2909lh) r8, (p001a.aVF) r11, (long) r12, (java.lang.Object) r2)
            r12 = r6
            goto L_0x0000
        L_0x0043:
            r2 = r3
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.ayT.m27405a(a.lh, a.aVF, java.lang.Object, a.aVF, long, long, java.lang.Object, java.lang.Object[]):void");
    }

    /* renamed from: i */
    private static Object m27418i(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr) {
        int i = 0;
        if (avf instanceof NativeArray) {
            NativeArray ayt = (NativeArray) avf;
            if (ayt.gWI && ayt.m27425wj(((int) ayt.gWG) + objArr.length)) {
                while (i < objArr.length) {
                    Object[] objArr2 = ayt.gWH;
                    long j = ayt.gWG;
                    ayt.gWG = 1 + j;
                    objArr2[(int) j] = objArr[i];
                    i++;
                }
                return org.mozilla1.javascript.ScriptRuntime.wrapNumber((double) ayt.gWG);
            }
        }
        long f = m27415f(lhVar, avf);
        while (i < objArr.length) {
            m27403a(lhVar, avf, ((long) i) + f, objArr[i]);
            i++;
        }
        Object a = m27398a(lhVar, avf, ((long) objArr.length) + f);
        if (lhVar.getLanguageVersion() != 120) {
            return a;
        }
        if (objArr.length == 0) {
            return org.mozilla1.javascript.Undefined.instance;
        }
        return objArr[objArr.length - 1];
    }

    /* renamed from: j */
    private static Object m27419j(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr) {
        Object obj;
        if (avf instanceof NativeArray) {
            NativeArray ayt = (NativeArray) avf;
            if (ayt.gWI && ayt.gWG > 0) {
                ayt.gWG--;
                Object obj2 = ayt.gWH[(int) ayt.gWG];
                ayt.gWH[(int) ayt.gWG] = NOT_FOUND;
                return obj2;
            }
        }
        long f = m27415f(lhVar, avf);
        if (f > 0) {
            f--;
            obj = m27411b(lhVar, avf, f);
        } else {
            obj = org.mozilla1.javascript.Undefined.instance;
        }
        m27398a(lhVar, avf, f);
        return obj;
    }

    /* renamed from: k */
    private static Object m27420k(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr) {
        Object obj;
        if (avf instanceof NativeArray) {
            NativeArray ayt = (NativeArray) avf;
            if (ayt.gWI && ayt.gWG > 0) {
                ayt.gWG--;
                Object obj2 = ayt.gWH[0];
                System.arraycopy(ayt.gWH, 1, ayt.gWH, 0, (int) ayt.gWG);
                ayt.gWH[(int) ayt.gWG] = NOT_FOUND;
                return obj2;
            }
        }
        long f = m27415f(lhVar, avf);
        if (f > 0) {
            f--;
            obj = m27411b(lhVar, avf, 0);
            if (f > 0) {
                for (long j = 1; j <= f; j++) {
                    m27403a(lhVar, avf, j - 1, m27411b(lhVar, avf, j));
                }
            }
        } else {
            obj = org.mozilla1.javascript.Undefined.instance;
        }
        m27398a(lhVar, avf, f);
        return obj;
    }

    /* renamed from: l */
    private static Object m27422l(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr) {
        if (avf instanceof NativeArray) {
            NativeArray ayt = (NativeArray) avf;
            if (ayt.gWI && ayt.m27425wj(((int) ayt.gWG) + objArr.length)) {
                System.arraycopy(ayt.gWH, 0, ayt.gWH, objArr.length, (int) ayt.gWG);
                for (int i = 0; i < objArr.length; i++) {
                    ayt.gWH[i] = objArr[i];
                }
                ayt.gWG += (long) objArr.length;
                return org.mozilla1.javascript.ScriptRuntime.wrapNumber((double) ayt.gWG);
            }
        }
        long f = m27415f(lhVar, avf);
        int length = objArr.length;
        if (objArr.length <= 0) {
            return org.mozilla1.javascript.ScriptRuntime.wrapNumber((double) f);
        }
        if (f > 0) {
            for (long j = f - 1; j >= 0; j--) {
                m27403a(lhVar, avf, ((long) length) + j, m27411b(lhVar, avf, j));
            }
        }
        for (int i2 = 0; i2 < objArr.length; i2++) {
            m27403a(lhVar, avf, (long) i2, objArr[i2]);
        }
        return m27398a(lhVar, avf, ((long) objArr.length) + f);
    }

    /* renamed from: c */
    private static Object m27413c(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        boolean z;
        long j;
        long j2;
        int i;
        Object obj;
        NativeArray ayt = null;
        if (avf2 instanceof NativeArray) {
            NativeArray ayt2 = (NativeArray) avf2;
            z = ayt2.gWI;
            ayt = ayt2;
        } else {
            z = false;
        }
        org.mozilla1.javascript.Scriptable x = m23595x(avf);
        int length = objArr.length;
        if (length == 0) {
            return org.mozilla1.javascript.ScriptRuntime.m7539b(lhVar, x, "Array", (Object[]) null);
        }
        long f = m27415f(lhVar, avf2);
        long a = m27396a(org.mozilla1.javascript.ScriptRuntime.toInteger(objArr[0]), f);
        int i2 = length - 1;
        if (objArr.length == 1) {
            j2 = f - a;
            i = i2;
        } else {
            double integer = org.mozilla1.javascript.ScriptRuntime.toInteger(objArr[1]);
            if (integer < org.mozilla1.javascript.ScriptRuntime.NaN) {
                j = 0;
            } else if (integer > ((double) (f - a))) {
                j = f - a;
            } else {
                j = (long) integer;
            }
            j2 = j;
            i = i2 - 1;
        }
        long j3 = a + j2;
        if (j2 != 0) {
            if (j2 == 1 && lhVar.getLanguageVersion() == 120) {
                obj = m27411b(lhVar, avf2, a);
            } else if (z) {
                int i3 = (int) (j3 - a);
                Object[] objArr2 = new Object[i3];
                System.arraycopy(ayt.gWH, (int) a, objArr2, 0, i3);
                obj = lhVar.mo20351b(x, objArr2);
            } else {
                org.mozilla1.javascript.Scriptable b = org.mozilla1.javascript.ScriptRuntime.m7539b(lhVar, x, "Array", (Object[]) null);
                for (long j4 = a; j4 != j3; j4++) {
                    m27403a(lhVar, b, j4 - a, m27411b(lhVar, avf2, j4));
                }
                obj = b;
            }
        } else if (lhVar.getLanguageVersion() == 120) {
            obj = org.mozilla1.javascript.Undefined.instance;
        } else {
            obj = org.mozilla1.javascript.ScriptRuntime.m7539b(lhVar, x, "Array", (Object[]) null);
        }
        long j5 = ((long) i) - j2;
        if (!z || f + j5 >= 2147483647L || !ayt.m27425wj((int) (f + j5))) {
            if (j5 > 0) {
                for (long j6 = f - 1; j6 >= j3; j6--) {
                    m27403a(lhVar, avf2, j6 + j5, m27411b(lhVar, avf2, j6));
                }
            } else if (j5 < 0) {
                for (long j7 = j3; j7 < f; j7++) {
                    m27403a(lhVar, avf2, j7 + j5, m27411b(lhVar, avf2, j7));
                }
            }
            int length2 = objArr.length - i;
            for (int i4 = 0; i4 < i; i4++) {
                m27403a(lhVar, avf2, ((long) i4) + a, objArr[i4 + length2]);
            }
            m27398a(lhVar, avf2, f + j5);
            return obj;
        }
        System.arraycopy(ayt.gWH, (int) j3, ayt.gWH, (int) (((long) i) + a), (int) (f - j3));
        if (i > 0) {
            System.arraycopy(objArr, 2, ayt.gWH, (int) a, i);
        }
        if (j5 < 0) {
            Arrays.fill(ayt.gWH, (int) (f + j5), (int) f, NOT_FOUND);
        }
        ayt.gWG = f + j5;
        return obj;
    }

    /* renamed from: d */
    private static org.mozilla1.javascript.Scriptable m27414d(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        long j;
        int i;
        int i2;
        org.mozilla1.javascript.Scriptable x = m23595x(avf);
        Function a = org.mozilla1.javascript.ScriptRuntime.m7485a(lhVar, x, "Array");
        org.mozilla1.javascript.Scriptable construct = a.construct(lhVar, x, org.mozilla1.javascript.ScriptRuntime.emptyArgs);
        if ((avf2 instanceof NativeArray) && (construct instanceof NativeArray)) {
            NativeArray ayt = (NativeArray) avf2;
            NativeArray ayt2 = (NativeArray) construct;
            if (ayt.gWI && ayt2.gWI) {
                boolean z = true;
                int i3 = (int) ayt.gWG;
                int i4 = 0;
                while (true) {
                    int i5 = i4;
                    if (i5 < objArr.length && z) {
                        if (org.mozilla1.javascript.ScriptRuntime.m7575e(objArr[i5], (Object) a, lhVar)) {
                            z = objArr[i5] instanceof NativeArray;
                            i3 = (int) (((long) i3) + objArr[i5].gWG);
                        } else {
                            i3++;
                        }
                        i4 = i5 + 1;
                    } else if (z && ayt2.m27425wj(i3)) {
                        System.arraycopy(ayt.gWH, 0, ayt2.gWH, 0, (int) ayt.gWG);
                        int i6 = (int) ayt.gWG;
                        i = 0;
                        while (true) {
                            i2 = i;
                            if (i2 >= objArr.length || !z) {
                                ayt2.gWG = (long) i3;
                            } else {
                                if (objArr[i2] instanceof NativeArray) {
                                    NativeArray ayt3 = (NativeArray) objArr[i2];
                                    System.arraycopy(ayt3.gWH, 0, ayt2.gWH, i6, (int) ayt3.gWG);
                                    i6 += (int) ayt3.gWG;
                                } else {
                                    ayt2.gWH[i6] = objArr[i2];
                                    i6++;
                                }
                                i = i2 + 1;
                            }
                        }
                        ayt2.gWG = (long) i3;
                        return construct;
                    }
                }
                System.arraycopy(ayt.gWH, 0, ayt2.gWH, 0, (int) ayt.gWG);
                int i62 = (int) ayt.gWG;
                i = 0;
                while (true) {
                    i2 = i;
                    if (i2 >= objArr.length) {
                        break;
                    }
                    break;
                    i = i2 + 1;
                }
                ayt2.gWG = (long) i3;
                return construct;
            }
        }
        if (org.mozilla1.javascript.ScriptRuntime.m7575e((Object) avf2, (Object) a, lhVar)) {
            long f = m27415f(lhVar, avf2);
            j = 0;
            while (j < f) {
                m27403a(lhVar, construct, j, m27411b(lhVar, avf2, j));
                j++;
            }
        } else {
            j = 1 + 0;
            m27403a(lhVar, construct, 0, (Object) avf2);
        }
        long j2 = j;
        for (int i7 = 0; i7 < objArr.length; i7++) {
            if (org.mozilla1.javascript.ScriptRuntime.m7575e(objArr[i7], (Object) a, lhVar)) {
                org.mozilla1.javascript.Scriptable avf3 = (org.mozilla1.javascript.Scriptable) objArr[i7];
                long f2 = m27415f(lhVar, avf3);
                long j3 = 0;
                while (j3 < f2) {
                    m27403a(lhVar, construct, j2, m27411b(lhVar, avf3, j3));
                    j3++;
                    j2++;
                }
            } else {
                m27403a(lhVar, construct, j2, objArr[i7]);
                j2 = 1 + j2;
            }
        }
        return construct;
    }

    /* renamed from: a */
    private static long m27396a(double d, long j) {
        if (d < org.mozilla1.javascript.ScriptRuntime.NaN) {
            if (((double) j) + d < org.mozilla1.javascript.ScriptRuntime.NaN) {
                return 0;
            }
            return (long) (((double) j) + d);
        } else if (d <= ((double) j)) {
            return (long) d;
        } else {
            return j;
        }
    }

    public String getClassName() {
        return "Array";
    }

    /* access modifiers changed from: protected */
    public int getMaxInstanceId() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public int findInstanceIdInfo(String str) {
        if (str.equals("length")) {
            return instanceIdInfo(6, 1);
        }
        return super.findInstanceIdInfo(str);
    }

    /* access modifiers changed from: protected */
    public String getInstanceIdName(int i) {
        if (i == 1) {
            return "length";
        }
        return super.getInstanceIdName(i);
    }

    /* access modifiers changed from: protected */
    public Object getInstanceIdValue(int i) {
        if (i == 1) {
            return org.mozilla1.javascript.ScriptRuntime.wrapNumber((double) this.gWG);
        }
        return super.getInstanceIdValue(i);
    }

    /* access modifiers changed from: protected */
    public void setInstanceIdValue(int i, Object obj) {
        if (i == 1) {
            m27409az(obj);
        } else {
            super.setInstanceIdValue(i, obj);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo1118a(org.mozilla1.javascript.IdFunctionObject yy) {
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gWe, (int) gWt, "join", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gWe, (int) gWu, "reverse", 1);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gWe, (int) gWv, "sort", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gWe, (int) gWw, "push", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gWe, (int) gWx, "pop", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gWe, -10, "shift", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gWe, (int) gWz, "unshift", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gWe, (int) gWA, "splice", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gWe, (int) gqo, "concat", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gWe, (int) gqp, "slice", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gWe, (int) gqh, "indexOf", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gWe, (int) gqi, "lastIndexOf", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gWe, (int) gWB, "every", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gWe, (int) gWC, "filter", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gWe, (int) gWD, "forEach", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gWe, (int) gWE, "map", 2);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, gWe, (int) gWF, "some", 2);
        super.mo1118a(yy);
    }

    /* access modifiers changed from: protected */
    public void initPrototypeId(int i) {
        String str;
        int i2 = 1;
        switch (i) {
            case 1:
                str = "constructor";
                break;
            case 2:
                str = "toString";
                i2 = 0;
                break;
            case 3:
                str = "toLocaleString";
                break;
            case 4:
                str = "toSource";
                i2 = 0;
                break;
            case 5:
                str = "join";
                break;
            case 6:
                str = "reverse";
                i2 = 0;
                break;
            case 7:
                str = "sort";
                break;
            case 8:
                str = "push";
                break;
            case 9:
                str = "pop";
                break;
            case 10:
                str = "shift";
                break;
            case 11:
                str = "unshift";
                break;
            case 12:
                str = "splice";
                break;
            case 13:
                str = "concat";
                break;
            case 14:
                str = "slice";
                break;
            case 15:
                str = "indexOf";
                break;
            case 16:
                str = "lastIndexOf";
                break;
            case 17:
                str = "every";
                break;
            case 18:
                str = "filter";
                break;
            case 19:
                str = "forEach";
                break;
            case 20:
                str = "map";
                break;
            case 21:
                str = "some";
                break;
            default:
                throw new IllegalArgumentException(String.valueOf(i));
        }
        initPrototypeMethod(gWe, i, str, i2);
    }

    public Object execIdCall(IdFunctionObject yy, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        boolean z = false;
        if (!yy.hasTag(gWe)) {
            return super.execIdCall(yy, lhVar, avf, avf2, objArr);
        }
        int methodId = yy.methodId();
        Object[] objArr2 = objArr;
        org.mozilla1.javascript.Scriptable avf3 = avf2;
        while (true) {
            switch (methodId) {
                case gWF /*-21*/:
                case gWE /*-20*/:
                case gWD /*-19*/:
                case gWC /*-18*/:
                case gWB /*-17*/:
                case gqi /*-16*/:
                case gqh /*-15*/:
                case gqp /*-14*/:
                case gqo /*-13*/:
                case gWA /*-12*/:
                case gWz /*-11*/:
                case -10:
                case gWx /*-9*/:
                case gWw /*-8*/:
                case gWv /*-7*/:
                case gWu /*-6*/:
                case gWt /*-5*/:
                    avf3 = org.mozilla1.javascript.ScriptRuntime.m7554c(avf, objArr2[0]);
                    Object[] objArr3 = new Object[(objArr2.length - 1)];
                    for (int i = 0; i < objArr3.length; i++) {
                        objArr3[i] = objArr2[i + 1];
                    }
                    methodId = -methodId;
                    objArr2 = objArr3;
                case 1:
                    if (avf3 == null) {
                        z = true;
                    }
                    if (!z) {
                        return yy.construct(lhVar, avf, objArr2);
                    }
                    return m27412b(lhVar, avf, objArr2);
                case 2:
                    return m27400a(lhVar, avf, avf3, lhVar.hasFeature(4), false);
                case 3:
                    return m27400a(lhVar, avf, avf3, false, true);
                case 4:
                    return m27400a(lhVar, avf, avf3, true, false);
                case 5:
                    return m27416g(lhVar, avf3, objArr2);
                case 6:
                    return m27417h(lhVar, avf3, objArr2);
                case 7:
                    return m27410b(lhVar, avf, avf3, objArr2);
                case 8:
                    return m27418i(lhVar, avf3, objArr2);
                case 9:
                    return m27419j(lhVar, avf3, objArr2);
                case 10:
                    return m27420k(lhVar, avf3, objArr2);
                case 11:
                    return m27422l(lhVar, avf3, objArr2);
                case 12:
                    return m27413c(lhVar, avf, avf3, objArr2);
                case 13:
                    return m27414d(lhVar, avf, avf3, objArr2);
                case 14:
                    return m27423m(lhVar, avf3, objArr2);
                case 15:
                    return m27399a(lhVar, avf3, objArr2, false);
                case 16:
                    return m27399a(lhVar, avf3, objArr2, true);
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                    return m27397a(lhVar, methodId, avf, avf3, objArr2);
                default:
                    throw new IllegalArgumentException(String.valueOf(methodId));
            }
        }
    }

    public Object get(int i, org.mozilla1.javascript.Scriptable avf) {
        if (!this.gWI && mo14503b((String) null, i, false)) {
            return super.get(i, avf);
        }
        if (this.gWH == null || i < 0 || i >= this.gWH.length) {
            return super.get(i, avf);
        }
        return this.gWH[i];
    }

    public boolean has(int i, org.mozilla1.javascript.Scriptable avf) {
        if (!this.gWI && mo14503b((String) null, i, false)) {
            return super.has(i, avf);
        }
        if (this.gWH == null || i < 0 || i >= this.gWH.length) {
            return super.has(i, avf);
        }
        if (this.gWH[i] != NOT_FOUND) {
            return true;
        }
        return false;
    }

    public void put(String str, org.mozilla1.javascript.Scriptable avf, Object obj) {
        super.put(str, avf, obj);
        if (avf == this) {
            long kL = m27421kL(str);
            if (kL >= this.gWG) {
                this.gWG = kL + 1;
                this.gWI = false;
            }
        }
    }

    /* renamed from: wj */
    private boolean m27425wj(int i) {
        if (i > this.gWH.length) {
            if (i > gWL) {
                this.gWI = false;
                return false;
            }
            Object[] objArr = new Object[Math.max(i, (int) (((double) this.gWH.length) * gWK))];
            System.arraycopy(this.gWH, 0, objArr, 0, this.gWH.length);
            Arrays.fill(objArr, this.gWH.length, objArr.length, org.mozilla1.javascript.Scriptable.NOT_FOUND);
            this.gWH = objArr;
        }
        return true;
    }

    public void put(int i, org.mozilla1.javascript.Scriptable avf, Object obj) {
        if (avf == this && !isSealed() && this.gWH != null && i >= 0 && (this.gWI || !mo14503b((String) null, i, true))) {
            if (i < this.gWH.length) {
                this.gWH[i] = obj;
                if (this.gWG <= ((long) i)) {
                    this.gWG = ((long) i) + 1;
                    return;
                }
                return;
            } else if (!this.gWI || ((double) i) >= ((double) this.gWH.length) * gWK || !m27425wj(i + 1)) {
                this.gWI = false;
            } else {
                this.gWH[i] = obj;
                this.gWG = ((long) i) + 1;
                return;
            }
        }
        super.put(i, avf, obj);
        if (avf == this && this.gWG <= ((long) i)) {
            this.gWG = ((long) i) + 1;
        }
    }

    public void delete(int i) {
        if (this.gWH == null || i < 0 || i >= this.gWH.length || isSealed() || (!this.gWI && mo14503b((String) null, i, true))) {
            super.delete(i);
        } else {
            this.gWH[i] = NOT_FOUND;
        }
    }

    public Object[] getIds() {
        int i;
        Object[] objArr;
        Object[] ids = super.getIds();
        if (this.gWH == null) {
            return ids;
        }
        int length = this.gWH.length;
        long j = this.gWG;
        if (((long) length) > j) {
            i = (int) j;
        } else {
            i = length;
        }
        if (i == 0) {
            return ids;
        }
        int length2 = ids.length;
        Object[] objArr2 = new Object[(i + length2)];
        int i2 = 0;
        for (int i3 = 0; i3 != i; i3++) {
            if (this.gWH[i3] != NOT_FOUND) {
                objArr2[i2] = new Integer(i3);
                i2++;
            }
        }
        if (i2 != i) {
            objArr = new Object[(i2 + length2)];
            System.arraycopy(objArr2, 0, objArr, 0, i2);
        } else {
            objArr = objArr2;
        }
        System.arraycopy(ids, 0, objArr, i2, length2);
        return objArr;
    }

    public Object getDefaultValue(Class<?> cls) {
        if (cls == org.mozilla1.javascript.ScriptRuntime.NumberClass && org.mozilla1.javascript.Context.bwA().getLanguageVersion() == 120) {
            return new Long(this.gWG);
        }
        return super.getDefaultValue(cls);
    }

    public long getLength() {
        return this.gWG;
    }

    public long jsGet_length() {
        return getLength();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: hw */
    public void mo16972hw(boolean z) {
        if (!z || this.gWI) {
            this.gWI = z;
            return;
        }
        throw new IllegalArgumentException();
    }

    /* renamed from: az */
    private void m27409az(Object obj) {
        int i = 0;
        double number = org.mozilla1.javascript.ScriptRuntime.toNumber(obj);
        long uint32 = org.mozilla1.javascript.ScriptRuntime.toUint32(number);
        if (((double) uint32) != number) {
            throw org.mozilla1.javascript.Context.m35244gE("msg.arraylength.bad");
        }
        if (this.gWI) {
            if (uint32 < this.gWG) {
                Arrays.fill(this.gWH, (int) uint32, this.gWH.length, NOT_FOUND);
                this.gWG = uint32;
                return;
            } else if (uint32 >= 1431655764 || ((double) uint32) >= ((double) this.gWG) * gWK || !m27425wj((int) uint32)) {
                this.gWI = false;
            } else {
                this.gWG = uint32;
                return;
            }
        }
        if (uint32 < this.gWG) {
            if (this.gWG - uint32 > 4096) {
                Object[] ids = getIds();
                while (true) {
                    int i2 = i;
                    if (i2 >= ids.length) {
                        break;
                    }
                    Object obj2 = ids[i2];
                    if (obj2 instanceof String) {
                        String str = (String) obj2;
                        if (m27421kL(str) >= uint32) {
                            delete(str);
                        }
                    } else {
                        int intValue = ((Integer) obj2).intValue();
                        if (((long) intValue) >= uint32) {
                            delete(intValue);
                        }
                    }
                    i = i2 + 1;
                }
            } else {
                for (long j = uint32; j < this.gWG; j++) {
                    m27401a((org.mozilla1.javascript.Scriptable) this, j);
                }
            }
        }
        this.gWG = uint32;
    }

    /* renamed from: m */
    private org.mozilla1.javascript.Scriptable m27423m(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr) {
        long a;
        org.mozilla1.javascript.Scriptable b = org.mozilla1.javascript.ScriptRuntime.m7539b(lhVar, m23595x(this), "Array", (Object[]) null);
        long f = m27415f(lhVar, avf);
        if (objArr.length == 0) {
            a = 0;
        } else {
            a = m27396a(org.mozilla1.javascript.ScriptRuntime.toInteger(objArr[0]), f);
            if (objArr.length != 1) {
                f = m27396a(org.mozilla1.javascript.ScriptRuntime.toInteger(objArr[1]), f);
            }
        }
        for (long j = a; j < f; j++) {
            m27403a(lhVar, b, j - a, m27411b(lhVar, avf, j));
        }
        return b;
    }

    /* renamed from: a */
    private Object m27399a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr, boolean z) {
        long int32;
        Object obj = objArr.length > 0 ? objArr[0] : Undefined.instance;
        long f = m27415f(lhVar, avf);
        if (z) {
            if (objArr.length < 2) {
                int32 = f - 1;
            } else {
                int32 = (long) org.mozilla1.javascript.ScriptRuntime.toInt32(org.mozilla1.javascript.ScriptRuntime.toNumber(objArr[1]));
                if (int32 >= f) {
                    int32 = f - 1;
                } else if (int32 < 0) {
                    int32 += f;
                }
            }
        } else if (objArr.length < 2) {
            int32 = 0;
        } else {
            int32 = (long) org.mozilla1.javascript.ScriptRuntime.toInt32(org.mozilla1.javascript.ScriptRuntime.toNumber(objArr[1]));
            if (int32 < 0) {
                int32 += f;
                if (int32 < 0) {
                    int32 = 0;
                }
            }
        }
        if (avf instanceof NativeArray) {
            NativeArray ayt = (NativeArray) avf;
            if (ayt.gWI) {
                if (z) {
                    for (int i = (int) int32; i >= 0; i--) {
                        if (ayt.gWH[i] != org.mozilla1.javascript.Scriptable.NOT_FOUND && org.mozilla1.javascript.ScriptRuntime.shallowEq(ayt.gWH[i], obj)) {
                            return new Long((long) i);
                        }
                    }
                } else {
                    for (int i2 = (int) int32; ((long) i2) < f; i2++) {
                        if (ayt.gWH[i2] != org.mozilla1.javascript.Scriptable.NOT_FOUND && org.mozilla1.javascript.ScriptRuntime.shallowEq(ayt.gWH[i2], obj)) {
                            return new Long((long) i2);
                        }
                    }
                }
                return gWf;
            }
        }
        if (z) {
            while (int32 >= 0) {
                if (org.mozilla1.javascript.ScriptRuntime.shallowEq(m27411b(lhVar, avf, int32), obj)) {
                    return new Long(int32);
                }
                int32--;
            }
        } else {
            while (int32 < f) {
                if (ScriptRuntime.shallowEq(m27411b(lhVar, avf, int32), obj)) {
                    return new Long(int32);
                }
                int32++;
            }
        }
        return gWf;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0079, code lost:
        continue;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object m27397a(Context r17, int r18, org.mozilla1.javascript.Scriptable r19, Scriptable r20, java.lang.Object[] r21) {
        /*
            r16 = this;
            r0 = r21
            int r2 = r0.length
            if (r2 <= 0) goto L_0x0017
            r2 = 0
            r2 = r21[r2]
        L_0x0008:
            if (r2 == 0) goto L_0x000e
            boolean r3 = r2 instanceof p001a.C7019azg
            if (r3 != 0) goto L_0x001a
        L_0x000e:
            java.lang.String r2 = p001a.C0903NH.toString((java.lang.Object) r2)
            java.lang.RuntimeException r2 = p001a.C0903NH.notFunctionError(r2)
            throw r2
        L_0x0017:
            java.lang.Object r2 = org.mozilla.javascript.C0321EM.instance
            goto L_0x0008
        L_0x001a:
            a.azg r2 = (p001a.C7019azg) r2
            a.aVF r4 = p001a.C6327akn.m23595x(r2)
            r0 = r21
            int r3 = r0.length
            r5 = 2
            if (r3 < r5) goto L_0x0032
            r3 = 1
            r3 = r21[r3]
            if (r3 == 0) goto L_0x0032
            r3 = 1
            r3 = r21[r3]
            java.lang.Object r5 = org.mozilla.javascript.C0321EM.instance
            if (r3 != r5) goto L_0x0055
        L_0x0032:
            r3 = r4
        L_0x0033:
            r0 = r17
            r1 = r20
            long r14 = m27415f(r0, r1)
            java.lang.String r5 = "Array"
            r6 = 0
            r0 = r17
            r1 = r19
            a.aVF r10 = p001a.C0903NH.m7539b((p001a.C2909lh) r0, (p001a.aVF) r1, (java.lang.String) r5, (java.lang.Object[]) r6)
            r6 = 0
            r8 = 0
            r12 = r8
        L_0x004b:
            int r5 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r5 < 0) goto L_0x0061
            switch(r18) {
                case 17: goto L_0x00c9;
                case 18: goto L_0x00cc;
                case 19: goto L_0x0052;
                case 20: goto L_0x00cc;
                case 21: goto L_0x00ce;
                default: goto L_0x0052;
            }
        L_0x0052:
            java.lang.Object r2 = org.mozilla.javascript.C0321EM.instance
        L_0x0054:
            return r2
        L_0x0055:
            r3 = 1
            r3 = r21[r3]
            r0 = r17
            r1 = r19
            a.aVF r3 = p001a.C0903NH.m7572e((p001a.C2909lh) r0, (p001a.aVF) r1, (java.lang.Object) r3)
            goto L_0x0033
        L_0x0061:
            r5 = 3
            java.lang.Object[] r11 = new java.lang.Object[r5]
            r8 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r5 = (r12 > r8 ? 1 : (r12 == r8 ? 0 : -1))
            if (r5 <= 0) goto L_0x007e
            java.lang.String r5 = java.lang.Long.toString(r12)
            r0 = r20
            java.lang.Object r5 = p001a.C6327akn.m23587j(r0, r5)
        L_0x0075:
            java.lang.Object r8 = p001a.aVF.NOT_FOUND
            if (r5 != r8) goto L_0x0086
        L_0x0079:
            r8 = 1
            long r8 = r8 + r12
            r12 = r8
            goto L_0x004b
        L_0x007e:
            int r5 = (int) r12
            r0 = r20
            java.lang.Object r5 = p001a.C6327akn.m23575b((p001a.aVF) r0, (int) r5)
            goto L_0x0075
        L_0x0086:
            r8 = 0
            r11[r8] = r5
            r5 = 1
            java.lang.Long r8 = new java.lang.Long
            r8.<init>(r12)
            r11[r5] = r8
            r5 = 2
            r11[r5] = r20
            r0 = r17
            java.lang.Object r5 = r2.call(r0, r4, r3, r11)
            switch(r18) {
                case 17: goto L_0x009e;
                case 18: goto L_0x00a7;
                case 19: goto L_0x0079;
                case 20: goto L_0x00ba;
                case 21: goto L_0x00c0;
                default: goto L_0x009d;
            }
        L_0x009d:
            goto L_0x0079
        L_0x009e:
            boolean r5 = p001a.C0903NH.toBoolean(r5)
            if (r5 != 0) goto L_0x0079
            java.lang.Boolean r2 = java.lang.Boolean.FALSE
            goto L_0x0054
        L_0x00a7:
            boolean r5 = p001a.C0903NH.toBoolean(r5)
            if (r5 == 0) goto L_0x0079
            r8 = 1
            long r8 = r8 + r6
            r5 = 0
            r5 = r11[r5]
            r0 = r17
            m27403a((p001a.C2909lh) r0, (p001a.aVF) r10, (long) r6, (java.lang.Object) r5)
            r6 = r8
            goto L_0x0079
        L_0x00ba:
            r0 = r17
            m27403a((p001a.C2909lh) r0, (p001a.aVF) r10, (long) r12, (java.lang.Object) r5)
            goto L_0x0079
        L_0x00c0:
            boolean r5 = p001a.C0903NH.toBoolean(r5)
            if (r5 == 0) goto L_0x0079
            java.lang.Boolean r2 = java.lang.Boolean.TRUE
            goto L_0x0054
        L_0x00c9:
            java.lang.Boolean r2 = java.lang.Boolean.TRUE
            goto L_0x0054
        L_0x00cc:
            r2 = r10
            goto L_0x0054
        L_0x00ce:
            java.lang.Boolean r2 = java.lang.Boolean.FALSE
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.ayT.m27397a(a.lh, int, a.aVF, a.aVF, java.lang.Object[]):java.lang.Object");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int findPrototypeId(java.lang.String r8) {
        /*
            r7 = this;
            r4 = 3
            r6 = 112(0x70, float:1.57E-43)
            r1 = 2
            r3 = 1
            r0 = 0
            r2 = 0
            int r5 = r8.length()
            switch(r5) {
                case 3: goto L_0x001a;
                case 4: goto L_0x0046;
                case 5: goto L_0x0061;
                case 6: goto L_0x0080;
                case 7: goto L_0x00a1;
                case 8: goto L_0x00c2;
                case 9: goto L_0x000e;
                case 10: goto L_0x000e;
                case 11: goto L_0x00d7;
                case 12: goto L_0x000e;
                case 13: goto L_0x000e;
                case 14: goto L_0x00ee;
                default: goto L_0x000e;
            }
        L_0x000e:
            r1 = r0
        L_0x000f:
            if (r2 == 0) goto L_0x00f3
            if (r2 == r8) goto L_0x00f3
            boolean r2 = r2.equals(r8)
            if (r2 != 0) goto L_0x00f3
        L_0x0019:
            return r0
        L_0x001a:
            char r4 = r8.charAt(r0)
            r5 = 109(0x6d, float:1.53E-43)
            if (r4 != r5) goto L_0x0033
            char r1 = r8.charAt(r1)
            if (r1 != r6) goto L_0x000e
            char r1 = r8.charAt(r3)
            r3 = 97
            if (r1 != r3) goto L_0x000e
            r0 = 20
            goto L_0x0019
        L_0x0033:
            if (r4 != r6) goto L_0x000e
            char r1 = r8.charAt(r1)
            if (r1 != r6) goto L_0x000e
            char r1 = r8.charAt(r3)
            r3 = 111(0x6f, float:1.56E-43)
            if (r1 != r3) goto L_0x000e
            r0 = 9
            goto L_0x0019
        L_0x0046:
            char r1 = r8.charAt(r1)
            switch(r1) {
                case 105: goto L_0x004f;
                case 109: goto L_0x0053;
                case 114: goto L_0x0058;
                case 115: goto L_0x005c;
                default: goto L_0x004d;
            }
        L_0x004d:
            r1 = r0
            goto L_0x000f
        L_0x004f:
            java.lang.String r2 = "join"
            r1 = 5
            goto L_0x000f
        L_0x0053:
            java.lang.String r2 = "some"
            r1 = 21
            goto L_0x000f
        L_0x0058:
            java.lang.String r2 = "sort"
            r1 = 7
            goto L_0x000f
        L_0x005c:
            java.lang.String r2 = "push"
            r1 = 8
            goto L_0x000f
        L_0x0061:
            char r1 = r8.charAt(r3)
            r3 = 104(0x68, float:1.46E-43)
            if (r1 != r3) goto L_0x006e
            java.lang.String r2 = "shift"
            r1 = 10
            goto L_0x000f
        L_0x006e:
            r3 = 108(0x6c, float:1.51E-43)
            if (r1 != r3) goto L_0x0077
            java.lang.String r2 = "slice"
            r1 = 14
            goto L_0x000f
        L_0x0077:
            r3 = 118(0x76, float:1.65E-43)
            if (r1 != r3) goto L_0x000e
            java.lang.String r2 = "every"
            r1 = 17
            goto L_0x000f
        L_0x0080:
            char r1 = r8.charAt(r0)
            r3 = 99
            if (r1 != r3) goto L_0x008d
            java.lang.String r2 = "concat"
            r1 = 13
            goto L_0x000f
        L_0x008d:
            r3 = 102(0x66, float:1.43E-43)
            if (r1 != r3) goto L_0x0097
            java.lang.String r2 = "filter"
            r1 = 18
            goto L_0x000f
        L_0x0097:
            r3 = 115(0x73, float:1.61E-43)
            if (r1 != r3) goto L_0x000e
            java.lang.String r2 = "splice"
            r1 = 12
            goto L_0x000f
        L_0x00a1:
            char r1 = r8.charAt(r0)
            switch(r1) {
                case 102: goto L_0x00ab;
                case 105: goto L_0x00b1;
                case 114: goto L_0x00b7;
                case 117: goto L_0x00bc;
                default: goto L_0x00a8;
            }
        L_0x00a8:
            r1 = r0
            goto L_0x000f
        L_0x00ab:
            java.lang.String r2 = "forEach"
            r1 = 19
            goto L_0x000f
        L_0x00b1:
            java.lang.String r2 = "indexOf"
            r1 = 15
            goto L_0x000f
        L_0x00b7:
            java.lang.String r2 = "reverse"
            r1 = 6
            goto L_0x000f
        L_0x00bc:
            java.lang.String r2 = "unshift"
            r1 = 11
            goto L_0x000f
        L_0x00c2:
            char r3 = r8.charAt(r4)
            r4 = 111(0x6f, float:1.56E-43)
            if (r3 != r4) goto L_0x00cf
            java.lang.String r2 = "toSource"
            r1 = 4
            goto L_0x000f
        L_0x00cf:
            r4 = 116(0x74, float:1.63E-43)
            if (r3 != r4) goto L_0x000e
            java.lang.String r2 = "toString"
            goto L_0x000f
        L_0x00d7:
            char r1 = r8.charAt(r0)
            r4 = 99
            if (r1 != r4) goto L_0x00e4
            java.lang.String r2 = "constructor"
            r1 = r3
            goto L_0x000f
        L_0x00e4:
            r3 = 108(0x6c, float:1.51E-43)
            if (r1 != r3) goto L_0x000e
            java.lang.String r2 = "lastIndexOf"
            r1 = 16
            goto L_0x000f
        L_0x00ee:
            java.lang.String r2 = "toLocaleString"
            r1 = r4
            goto L_0x000f
        L_0x00f3:
            r0 = r1
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.ayT.findPrototypeId(java.lang.String):int");
    }
}
