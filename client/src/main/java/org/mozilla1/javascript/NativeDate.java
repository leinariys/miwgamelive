package org.mozilla1.javascript;

import logic.res.KeyCode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/* renamed from: a.CU */
/* compiled from: a */
final class NativeDate extends IdScriptableObject {
    static final long serialVersionUID = -8307438915861678966L;
    private static final int cBA = 4;
    private static final int cBB = 5;
    private static final int cBC = 6;
    private static final int cBD = 7;
    private static final int cBE = 8;
    private static final int cBF = 10;
    private static final int cBG = 11;
    private static final int cBH = 12;
    private static final int cBI = 13;
    private static final int cBJ = 14;
    private static final int cBK = 15;
    private static final int cBL = 16;
    private static final int cBM = 17;
    private static final int cBN = 18;
    private static final int cBO = 19;
    private static final int cBP = 20;
    private static final int cBQ = 21;
    private static final int cBR = 22;
    private static final int cBS = 23;
    private static final int cBT = 24;
    private static final int cBU = 25;
    private static final int cBV = 26;
    private static final int cBW = 27;
    private static final int cBX = 28;
    private static final int cBY = 29;
    private static final int cBZ = 30;
    private static final Object cBh = new Integer(13);
    private static final String cBi = "Invalid Date";
    private static final double cBj = 8.64E15d;
    private static final double cBk = 24.0d;
    private static final double cBl = 60.0d;
    private static final double cBm = 60.0d;
    private static final double cBn = 1000.0d;
    private static final double cBo = 1440.0d;
    private static final double cBp = 86400.0d;
    private static final double cBq = 3600.0d;
    private static final double cBr = 8.64E7d;
    private static final double cBs = 3600000.0d;
    private static final double cBt = 60000.0d;
    private static final boolean cBu = false;
    private static final int cBv = 7;
    private static final int cBw = -3;
    private static final int cBx = -2;
    private static final int cBy = -1;
    private static final int cBz = 3;
    private static final int cCa = 31;
    private static final int cCb = 32;
    private static final int cCc = 33;
    private static final int cCd = 34;
    private static final int cCe = 35;
    private static final int cCf = 36;
    private static final int cCg = 37;
    private static final int cCh = 38;
    private static final int cCi = 39;
    private static final int cCj = 40;
    private static final int cCk = 41;
    private static final int cCl = 42;
    private static final int cCm = 43;
    private static final int cCn = 44;
    private static final int cCo = 45;
    private static final int cCp = 8;
    private static final int ceN = 9;
    /* renamed from: ns */
    private static final int f343ns = 1;
    /* renamed from: nt */
    private static final int f344nt = 45;
    /* renamed from: xT */
    private static final int f345xT = 2;
    private static TimeZone cCq = null;
    private static double cCr = 0.0d;
    private static DateFormat cCs = null;
    private static DateFormat cCt = null;
    private static DateFormat cCu = null;
    private static DateFormat cCv = null;
    private double cCw;

    private NativeDate() {
        if (cCq == null) {
            cCq = TimeZone.getDefault();
            cCr = (double) cCq.getRawOffset();
        }
    }

    /* renamed from: a */
    static void m1765a(org.mozilla1.javascript.Scriptable avf, boolean z) {
        NativeDate cu = new NativeDate();
        cu.cCw = org.mozilla1.javascript.ScriptRuntime.NaN;
        cu.mo15695a(45, avf, z);
    }

    /* renamed from: i */
    private static double m1779i(double d) {
        return Math.floor(d / cBr);
    }

    /* renamed from: j */
    private static double m1780j(double d) {
        double d2 = d % cBr;
        if (d2 < org.mozilla1.javascript.ScriptRuntime.NaN) {
            return d2 + cBr;
        }
        return d2;
    }

    /* renamed from: gC */
    private static boolean m1777gC(int i) {
        return i % 4 == 0 && (i % 100 != 0 || i % 400 == 0);
    }

    /* renamed from: k */
    private static double m1781k(double d) {
        return (((365.0d * (d - 1970.0d)) + Math.floor((d - 1969.0d) / 4.0d)) - Math.floor((d - 1901.0d) / 100.0d)) + Math.floor((d - 1601.0d) / 400.0d);
    }

    /* renamed from: l */
    private static double m1782l(double d) {
        return m1781k(d) * cBr;
    }

    /* renamed from: m */
    private static int m1783m(double d) {
        int i;
        int i2;
        int floor = ((int) Math.floor((d / cBr) / 366.0d)) + 1970;
        int floor2 = ((int) Math.floor((d / cBr) / 365.0d)) + 1970;
        if (floor2 < floor) {
            i = floor;
            i2 = floor2;
        } else {
            i = floor2;
            i2 = floor;
        }
        while (i > i2) {
            int i3 = (i + i2) / 2;
            if (m1782l((double) i3) > d) {
                i = i3 - 1;
            } else {
                i2 = i3 + 1;
                if (m1782l((double) i2) > d) {
                    return i3;
                }
            }
        }
        return i2;
    }

    /* renamed from: z */
    private static double m1796z(int i, int i2) {
        int i3;
        int i4 = i * 30;
        if (i >= 7) {
            i3 = i4 + ((i / 2) - 1);
        } else if (i >= 2) {
            i3 = i4 + (((i - 1) / 2) - 1);
        } else {
            i3 = i4 + i;
        }
        if (i >= 2 && m1777gC(i2)) {
            i3++;
        }
        return (double) i3;
    }

    /* renamed from: n */
    private static int m1784n(double d) {
        int i;
        int i2;
        int m = m1783m(d);
        int i3 = ((int) (m1779i(d) - m1781k((double) m))) - 59;
        if (i3 >= 0) {
            if (!m1777gC(m)) {
                i = i3;
            } else if (i3 == 0) {
                return 1;
            } else {
                i = i3 - 1;
            }
            int i4 = i / 30;
            switch (i4) {
                case 0:
                    return 2;
                case 1:
                    i2 = 31;
                    break;
                case 2:
                    i2 = 61;
                    break;
                case 3:
                    i2 = 92;
                    break;
                case 4:
                    i2 = 122;
                    break;
                case 5:
                    i2 = 153;
                    break;
                case 6:
                    i2 = 184;
                    break;
                case 7:
                    i2 = KeyCode.csR;
                    break;
                case 8:
                    i2 = KeyCode.ctw;
                    break;
                case 9:
                    i2 = KeyCode.RIGHT;
                    break;
                case 10:
                    return 11;
                default:
                    throw org.mozilla1.javascript.Kit.codeBug();
            }
            if (i >= i2) {
                return i4 + 2;
            }
            return i4 + 1;
        } else if (i3 < -28) {
            return 0;
        } else {
            return 1;
        }
    }

    /* renamed from: o */
    private static int m1785o(double d) {
        int i;
        int i2 = 30;
        int m = m1783m(d);
        int i3 = ((int) (m1779i(d) - m1781k((double) m))) - 59;
        if (i3 >= 0) {
            if (m1777gC(m)) {
                if (i3 == 0) {
                    return 29;
                }
                i3--;
            }
            switch (i3 / 30) {
                case 0:
                    return i3 + 1;
                case 1:
                    i = 31;
                    i2 = 31;
                    break;
                case 2:
                    i = 61;
                    break;
                case 3:
                    i = 92;
                    i2 = 31;
                    break;
                case 4:
                    i = 122;
                    break;
                case 5:
                    i = 153;
                    i2 = 31;
                    break;
                case 6:
                    i = 184;
                    i2 = 31;
                    break;
                case 7:
                    i = KeyCode.csR;
                    break;
                case 8:
                    i = KeyCode.ctw;
                    i2 = 31;
                    break;
                case 9:
                    i = KeyCode.RIGHT;
                    break;
                case 10:
                    return (i3 - 275) + 1;
                default:
                    throw org.mozilla1.javascript.Kit.codeBug();
            }
            int i4 = i3 - i;
            if (i4 < 0) {
                i4 += i2;
            }
            return i4 + 1;
        } else if (i3 < -28) {
            return i3 + 31 + 28 + 1;
        } else {
            return i3 + 28 + 1;
        }
    }

    /* renamed from: p */
    private static int m1786p(double d) {
        double i = (m1779i(d) + 4.0d) % 7.0d;
        if (i < org.mozilla1.javascript.ScriptRuntime.NaN) {
            i += 7.0d;
        }
        return (int) i;
    }

    private static double aDC() {
        return (double) System.currentTimeMillis();
    }

    /* renamed from: q */
    private static double m1787q(double d) {
        if (d < org.mozilla1.javascript.ScriptRuntime.NaN || d > 2.1459168E12d) {
            d = m1771d(m1773e((double) m1778gD(m1783m(d)), (double) m1784n(d), (double) m1785o(d)), m1780j(d));
        }
        if (cCq.inDaylightTime(new Date((long) d))) {
            return cBs;
        }
        return org.mozilla1.javascript.ScriptRuntime.NaN;
    }

    /* renamed from: gD */
    private static int m1778gD(int i) {
        int k = (((int) m1781k((double) i)) + 4) % 7;
        if (k < 0) {
            k += 7;
        }
        if (m1777gC(i)) {
            switch (k) {
                case 0:
                    return 1984;
                case 1:
                    return 1996;
                case 2:
                    return 1980;
                case 3:
                    return 1992;
                case 4:
                    return 1976;
                case 5:
                    return 1988;
                case 6:
                    return 1972;
            }
        } else {
            switch (k) {
                case 0:
                    return 1978;
                case 1:
                    return 1973;
                case 2:
                    return 1974;
                case 3:
                    return 1975;
                case 4:
                    return 1981;
                case 5:
                    return 1971;
                case 6:
                    return 1977;
            }
        }
        throw org.mozilla1.javascript.Kit.codeBug();
    }

    /* renamed from: r */
    private static double m1788r(double d) {
        return cCr + d + m1787q(d);
    }

    /* renamed from: s */
    private static double m1789s(double d) {
        return (d - cCr) - m1787q(d - cCr);
    }

    /* renamed from: t */
    private static int m1790t(double d) {
        double floor = Math.floor(d / cBs) % cBk;
        if (floor < org.mozilla1.javascript.ScriptRuntime.NaN) {
            floor += cBk;
        }
        return (int) floor;
    }

    /* renamed from: u */
    private static int m1791u(double d) {
        double floor = Math.floor(d / cBt) % 60.0d;
        if (floor < org.mozilla1.javascript.ScriptRuntime.NaN) {
            floor += 60.0d;
        }
        return (int) floor;
    }

    /* renamed from: v */
    private static int m1792v(double d) {
        double floor = Math.floor(d / cBn) % 60.0d;
        if (floor < org.mozilla1.javascript.ScriptRuntime.NaN) {
            floor += 60.0d;
        }
        return (int) floor;
    }

    /* renamed from: w */
    private static int m1793w(double d) {
        double d2 = d % cBn;
        if (d2 < org.mozilla1.javascript.ScriptRuntime.NaN) {
            d2 += cBn;
        }
        return (int) d2;
    }

    /* renamed from: a */
    private static double m1761a(double d, double d2, double d3, double d4) {
        return (((((d * 60.0d) + d2) * 60.0d) + d3) * cBn) + d4;
    }

    /* renamed from: e */
    private static double m1773e(double d, double d2, double d3) {
        double floor = d + Math.floor(d2 / 12.0d);
        double d4 = d2 % 12.0d;
        if (d4 < org.mozilla1.javascript.ScriptRuntime.NaN) {
            d4 += 12.0d;
        }
        return ((m1796z((int) d4, (int) floor) + Math.floor(m1782l(floor) / cBr)) + d3) - 1.0d;
    }

    /* renamed from: d */
    private static double m1771d(double d, double d2) {
        return (cBr * d) + d2;
    }

    /* renamed from: x */
    private static double m1794x(double d) {
        if (d != d || d == Double.POSITIVE_INFINITY || d == Double.NEGATIVE_INFINITY || Math.abs(d) > cBj) {
            return org.mozilla1.javascript.ScriptRuntime.NaN;
        }
        if (d > org.mozilla1.javascript.ScriptRuntime.NaN) {
            return Math.floor(d + org.mozilla1.javascript.ScriptRuntime.NaN);
        }
        return Math.ceil(d + org.mozilla1.javascript.ScriptRuntime.NaN);
    }

    /* renamed from: a */
    private static double m1762a(double d, double d2, double d3, double d4, double d5, double d6, double d7) {
        return m1771d(m1773e(d, d2, d3), m1761a(d4, d5, d6, d7));
    }

    /* renamed from: e */
    private static double m1774e(Object[] objArr) {
        double[] dArr = new double[7];
        for (int i = 0; i < 7; i++) {
            if (i < objArr.length) {
                double number = org.mozilla1.javascript.ScriptRuntime.toNumber(objArr[i]);
                if (number != number || Double.isInfinite(number)) {
                    return org.mozilla1.javascript.ScriptRuntime.NaN;
                }
                dArr[i] = org.mozilla1.javascript.ScriptRuntime.toInteger(objArr[i]);
            } else if (i == 2) {
                dArr[i] = 1.0d;
            } else {
                dArr[i] = 0.0d;
            }
        }
        if (dArr[0] >= org.mozilla1.javascript.ScriptRuntime.NaN && dArr[0] <= 99.0d) {
            dArr[0] = dArr[0] + 1900.0d;
        }
        return m1762a(dArr[0], dArr[1], dArr[2], dArr[3], dArr[4], dArr[5], dArr[6]);
    }

    /* renamed from: f */
    private static double m1775f(Object[] objArr) {
        return m1794x(m1774e(objArr));
    }

    /* renamed from: dT */
    private static double m1772dT(String str) {
        int i;
        char c;
        int i2;
        int i3;
        double d;
        boolean z;
        int i4;
        int i5 = -1;
        int i6 = -1;
        int i7 = -1;
        int i8 = -1;
        int i9 = -1;
        int i10 = -1;
        int i11 = 0;
        double d2 = -1.0d;
        boolean z2 = false;
        int length = str.length();
        char c2 = 0;
        while (i11 < length) {
            char charAt = str.charAt(i11);
            int i12 = i11 + 1;
            if (charAt <= ' ' || charAt == ',' || charAt == '-') {
                if (i12 < length) {
                    char charAt2 = str.charAt(i12);
                    if (charAt == '-' && '0' <= charAt2 && charAt2 <= '9') {
                        c2 = charAt;
                        i11 = i12;
                    }
                }
                i11 = i12;
            } else if (charAt == '(') {
                int i13 = 1;
                while (true) {
                    if (i12 >= length) {
                        i11 = i12;
                    } else {
                        char charAt3 = str.charAt(i12);
                        i12++;
                        if (charAt3 == '(') {
                            i13++;
                        } else if (charAt3 == ')' && i13 - 1 <= 0) {
                            i11 = i12;
                        }
                    }
                }
            } else if ('0' <= charAt && charAt <= '9') {
                int i14 = charAt - '0';
                int i15 = i12;
                while (true) {
                    if (i15 < length) {
                        c = str.charAt(i15);
                        if ('0' <= c && c <= '9') {
                            i14 = ((i14 * 10) + c) - 48;
                            i15++;
                            charAt = c;
                        }
                    } else {
                        c = charAt;
                    }
                }
                if (c2 == '+' || c2 == '-') {
                    z = true;
                    if (i14 < 24) {
                        i4 = i14 * 60;
                    } else {
                        i4 = ((i14 / 100) * 60) + (i14 % 100);
                    }
                    if (c2 == '+') {
                        i4 = -i4;
                    }
                    if (d2 != org.mozilla1.javascript.ScriptRuntime.NaN && d2 != -1.0d) {
                        return org.mozilla1.javascript.ScriptRuntime.NaN;
                    }
                    d = (double) i4;
                    i3 = i10;
                    i2 = i9;
                } else if (i14 >= 70 || (c2 == '/' && i6 >= 0 && i7 >= 0 && i5 < 0)) {
                    if (i5 >= 0) {
                        return org.mozilla1.javascript.ScriptRuntime.NaN;
                    }
                    if (c > ' ' && c != ',' && c != '/' && i15 < length) {
                        return org.mozilla1.javascript.ScriptRuntime.NaN;
                    }
                    if (i14 < 100) {
                        i14 += 1900;
                    }
                    z = z2;
                    d = d2;
                    i3 = i10;
                    i2 = i9;
                    i5 = i14;
                } else if (c == ':') {
                    if (i8 < 0) {
                        z = z2;
                        d = d2;
                        i3 = i10;
                        i2 = i9;
                        i8 = i14;
                    } else if (i9 >= 0) {
                        return org.mozilla1.javascript.ScriptRuntime.NaN;
                    } else {
                        z = z2;
                        d = d2;
                        i3 = i10;
                        i2 = i14;
                    }
                } else if (c == '/') {
                    if (i6 < 0) {
                        z = z2;
                        d = d2;
                        i3 = i10;
                        i2 = i9;
                        i6 = i14 - 1;
                    } else if (i7 >= 0) {
                        return org.mozilla1.javascript.ScriptRuntime.NaN;
                    } else {
                        z = z2;
                        d = d2;
                        i3 = i10;
                        i2 = i9;
                        i7 = i14;
                    }
                } else if (i15 < length && c != ',' && c > ' ' && c != '-') {
                    return org.mozilla1.javascript.ScriptRuntime.NaN;
                } else {
                    if (!z2 || i14 >= 60) {
                        if (i8 >= 0 && i9 < 0) {
                            z = z2;
                            d = d2;
                            i3 = i10;
                            i2 = i14;
                        } else if (i9 >= 0 && i10 < 0) {
                            z = z2;
                            d = d2;
                            i3 = i14;
                            i2 = i9;
                        } else if (i7 >= 0) {
                            return org.mozilla1.javascript.ScriptRuntime.NaN;
                        } else {
                            z = z2;
                            d = d2;
                            i3 = i10;
                            i2 = i9;
                            i7 = i14;
                        }
                    } else if (d2 < org.mozilla1.javascript.ScriptRuntime.NaN) {
                        d = d2 - ((double) i14);
                        z = z2;
                        i3 = i10;
                        i2 = i9;
                    } else {
                        d = d2 + ((double) i14);
                        z = z2;
                        i3 = i10;
                        i2 = i9;
                    }
                }
                z2 = z;
                c2 = 0;
                d2 = d;
                i11 = i15;
                i10 = i3;
                i9 = i2;
            } else if (charAt == '/' || charAt == ':' || charAt == '+' || charAt == '-') {
                c2 = charAt;
                i11 = i12;
            } else {
                int i16 = i12 - 1;
                i11 = i12;
                while (i11 < length && (('A' <= (r2 = str.charAt(i11)) && r2 <= 'Z') || ('a' <= r2 && r2 <= 'z'))) {
                    i11++;
                }
                int i17 = i11 - i16;
                if (i17 < 2) {
                    return org.mozilla1.javascript.ScriptRuntime.NaN;
                }
                int i18 = 0;
                int i19 = 0;
                while (true) {
                    int i20 = i18;
                    int indexOf = "am;pm;monday;tuesday;wednesday;thursday;friday;saturday;sunday;january;february;march;april;may;june;july;august;september;october;november;december;gmt;ut;utc;est;edt;cst;cdt;mst;mdt;pst;pdt;".indexOf(59, i19);
                    if (indexOf < 0) {
                        return org.mozilla1.javascript.ScriptRuntime.NaN;
                    }
                    if ("am;pm;monday;tuesday;wednesday;thursday;friday;saturday;sunday;january;february;march;april;may;june;july;august;september;october;november;december;gmt;ut;utc;est;edt;cst;cdt;mst;mdt;pst;pdt;".regionMatches(true, i19, str, i16, i17)) {
                        if (i20 >= 2) {
                            int i21 = i20 - 2;
                            if (i21 >= 7) {
                                int i22 = i21 - 7;
                                if (i22 >= 12) {
                                    switch (i22 - 12) {
                                        case 0:
                                            d2 = org.mozilla1.javascript.ScriptRuntime.NaN;
                                            break;
                                        case 1:
                                            d2 = org.mozilla1.javascript.ScriptRuntime.NaN;
                                            break;
                                        case 2:
                                            d2 = org.mozilla1.javascript.ScriptRuntime.NaN;
                                            break;
                                        case 3:
                                            d2 = 300.0d;
                                            break;
                                        case 4:
                                            d2 = 240.0d;
                                            break;
                                        case 5:
                                            d2 = 360.0d;
                                            break;
                                        case 6:
                                            d2 = 300.0d;
                                            break;
                                        case 7:
                                            d2 = 420.0d;
                                            break;
                                        case 8:
                                            d2 = 360.0d;
                                            break;
                                        case 9:
                                            d2 = 480.0d;
                                            break;
                                        case 10:
                                            d2 = 420.0d;
                                            break;
                                        default:
                                            org.mozilla1.javascript.Kit.codeBug();
                                            break;
                                    }
                                } else if (i6 >= 0) {
                                    return org.mozilla1.javascript.ScriptRuntime.NaN;
                                } else {
                                    i6 = i22;
                                }
                            } else {
                                continue;
                            }
                        } else if (i8 > 12 || i8 < 0) {
                            return org.mozilla1.javascript.ScriptRuntime.NaN;
                        } else {
                            if (i20 == 0) {
                                if (i8 == 12) {
                                    i8 = 0;
                                }
                            } else if (i8 != 12) {
                                i8 += 12;
                            }
                        }
                    } else {
                        i19 = indexOf + 1;
                        i18 = i20 + 1;
                    }
                }
            }
        }
        if (i5 < 0 || i6 < 0 || i7 < 0) {
            return org.mozilla1.javascript.ScriptRuntime.NaN;
        }
        if (i10 < 0) {
            i = 0;
        } else {
            i = i10;
        }
        if (i9 < 0) {
            i9 = 0;
        }
        if (i8 < 0) {
            i8 = 0;
        }
        double a = m1762a((double) i5, (double) i6, (double) i7, (double) i8, (double) i9, (double) i, org.mozilla1.javascript.ScriptRuntime.NaN);
        if (d2 == -1.0d) {
            return m1789s(a);
        }
        return a + (cBt * d2);
    }

    /* renamed from: a */
    private static String m1764a(double d, int i) {
        StringBuffer stringBuffer = new StringBuffer(60);
        double r = m1788r(d);
        if (i != 3) {
            m1770b(stringBuffer, m1786p(r));
            stringBuffer.append(' ');
            m1766a(stringBuffer, m1784n(r));
            stringBuffer.append(' ');
            m1767a(stringBuffer, m1785o(r), 2);
            stringBuffer.append(' ');
            int m = m1783m(r);
            if (m < 0) {
                stringBuffer.append('-');
                m = -m;
            }
            m1767a(stringBuffer, m, 4);
            if (i != 4) {
                stringBuffer.append(' ');
            }
        }
        if (i != 4) {
            m1767a(stringBuffer, m1790t(r), 2);
            stringBuffer.append(':');
            m1767a(stringBuffer, m1791u(r), 2);
            stringBuffer.append(':');
            m1767a(stringBuffer, m1792v(r), 2);
            int floor = (int) Math.floor((cCr + m1787q(d)) / cBt);
            int i2 = (floor % 60) + ((floor / 60) * 100);
            if (i2 > 0) {
                stringBuffer.append(" GMT+");
            } else {
                stringBuffer.append(" GMT-");
                i2 = -i2;
            }
            m1767a(stringBuffer, i2, 4);
            if (cCs == null) {
                cCs = new SimpleDateFormat("zzz");
            }
            if (d < org.mozilla1.javascript.ScriptRuntime.NaN || d > 2.1459168E12d) {
                d = m1771d(m1773e((double) m1778gD(m1783m(r)), (double) m1784n(d), (double) m1785o(d)), m1780j(d));
            }
            stringBuffer.append(" (");
            Date date = new Date((long) d);
            synchronized (cCs) {
                stringBuffer.append(cCs.format(date));
            }
            stringBuffer.append(')');
        }
        return stringBuffer.toString();
    }

    /* renamed from: g */
    private static Object m1776g(Object[] objArr) {
        double number;
        NativeDate cu = new NativeDate();
        if (objArr.length == 0) {
            cu.cCw = aDC();
            return cu;
        } else if (objArr.length == 1) {
            Object obj = objArr[0];
            if (obj instanceof org.mozilla1.javascript.Scriptable) {
                obj = ((org.mozilla1.javascript.Scriptable) obj).getDefaultValue((Class<?>) null);
            }
            if (obj instanceof String) {
                number = m1772dT((String) obj);
            } else {
                number = org.mozilla1.javascript.ScriptRuntime.toNumber(obj);
            }
            cu.cCw = m1794x(number);
            return cu;
        } else {
            double e = m1774e(objArr);
            if (!Double.isNaN(e) && !Double.isInfinite(e)) {
                e = m1794x(m1789s(e));
            }
            cu.cCw = e;
            return cu;
        }
    }

    /* renamed from: b */
    private static String m1769b(double d, int i) {
        DateFormat dateFormat;
        String format;
        switch (i) {
            case 5:
                if (cCt == null) {
                    cCt = DateFormat.getDateTimeInstance(1, 1);
                }
                dateFormat = cCt;
                break;
            case 6:
                if (cCv == null) {
                    cCv = DateFormat.getTimeInstance(1);
                }
                dateFormat = cCv;
                break;
            case 7:
                if (cCu == null) {
                    cCu = DateFormat.getDateInstance(1);
                }
                dateFormat = cCu;
                break;
            default:
                dateFormat = null;
                break;
        }
        synchronized (dateFormat) {
            format = dateFormat.format(new Date((long) d));
        }
        return format;
    }

    /* renamed from: y */
    private static String m1795y(double d) {
        StringBuffer stringBuffer = new StringBuffer(60);
        m1770b(stringBuffer, m1786p(d));
        stringBuffer.append(", ");
        m1767a(stringBuffer, m1785o(d), 2);
        stringBuffer.append(' ');
        m1766a(stringBuffer, m1784n(d));
        stringBuffer.append(' ');
        int m = m1783m(d);
        if (m < 0) {
            stringBuffer.append('-');
            m = -m;
        }
        m1767a(stringBuffer, m, 4);
        stringBuffer.append(' ');
        m1767a(stringBuffer, m1790t(d), 2);
        stringBuffer.append(':');
        m1767a(stringBuffer, m1791u(d), 2);
        stringBuffer.append(':');
        m1767a(stringBuffer, m1792v(d), 2);
        stringBuffer.append(" GMT");
        return stringBuffer.toString();
    }

    /* renamed from: a */
    private static void m1767a(StringBuffer stringBuffer, int i, int i2) {
        int i3 = 1000000000;
        if (i < 0) {
            Kit.codeBug();
        }
        int i4 = i2 - 1;
        if (i < 10) {
            i3 = 1;
        } else if (i < 1000000000) {
            i3 = 1;
            while (true) {
                int i5 = i3 * 10;
                if (i < i5) {
                    break;
                }
                i4--;
                i3 = i5;
            }
        } else {
            i4 -= 9;
        }
        while (i4 > 0) {
            stringBuffer.append('0');
            i4--;
        }
        while (i3 != 1) {
            stringBuffer.append((char) ((i / i3) + 48));
            i %= i3;
            i3 /= 10;
        }
        stringBuffer.append((char) (i + 48));
    }

    /* renamed from: a */
    private static void m1766a(StringBuffer stringBuffer, int i) {
        int i2 = i * 3;
        for (int i3 = 0; i3 != 3; i3++) {
            stringBuffer.append("JanFebMarAprMayJunJulAugSepOctNovDec".charAt(i2 + i3));
        }
    }

    /* renamed from: b */
    private static void m1770b(StringBuffer stringBuffer, int i) {
        int i2 = i * 3;
        for (int i3 = 0; i3 != 3; i3++) {
            stringBuffer.append("SunMonTueWedThuFriSat".charAt(i2 + i3));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001d, code lost:
        r8 = r0;
        r6 = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0022, code lost:
        r8 = r0;
        r6 = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0027, code lost:
        if (r14.length != 0) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0029, code lost:
        r14 = p001a.C0903NH.padArguments(r14, 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002e, code lost:
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0030, code lost:
        if (r0 >= r14.length) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0032, code lost:
        if (r0 < r6) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0034, code lost:
        if (r8 == false) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0036, code lost:
        r12 = m1788r(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003a, code lost:
        r11 = r14.length;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003d, code lost:
        if (r6 < 4) goto L_0x009c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003f, code lost:
        if (0 >= r11) goto L_0x009c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0041, code lost:
        r4 = 1;
        r0 = r10[0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0045, code lost:
        if (r6 < 3) goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0047, code lost:
        if (r4 >= r11) goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0049, code lost:
        r9 = r4 + 1;
        r2 = r10[r4];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x004e, code lost:
        if (r6 < 2) goto L_0x00aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0050, code lost:
        if (r9 >= r11) goto L_0x00aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0052, code lost:
        r7 = r9 + 1;
        r4 = r10[r9];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0057, code lost:
        if (r6 < 1) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0059, code lost:
        if (r7 >= r11) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x005b, code lost:
        r6 = r7 + 1;
        r6 = r10[r7];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x005f, code lost:
        r0 = m1771d(m1779i(r12), m1761a(r0, r2, r4, r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x006b, code lost:
        if (r8 == false) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000a, code lost:
        r10 = new double[4];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x006d, code lost:
        r0 = m1789s(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0076, code lost:
        r10[r0] = p001a.C0903NH.toNumber(r14[r0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0084, code lost:
        if (r10[r0] != r10[r0]) goto L_0x008e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x008c, code lost:
        if (java.lang.Double.isInfinite(r10[r0]) == false) goto L_0x0091;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0091, code lost:
        r10[r0] = p001a.C0903NH.toInteger(r10[r0]);
        r0 = r0 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x009c, code lost:
        r0 = (double) m1790t(r12);
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00a3, code lost:
        r2 = (double) m1791u(r12);
        r9 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000f, code lost:
        if (r12 == r12) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00aa, code lost:
        r4 = (double) m1792v(r12);
        r7 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00b1, code lost:
        r6 = (double) m1793w(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        return r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
        return m1794x(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        return p001a.C0903NH.NaN;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0013, code lost:
        r8 = r0;
        r6 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0018, code lost:
        r8 = r0;
        r6 = 2;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static double m1763a(double r12, java.lang.Object[] r14, int r15) {
        /*
            r0 = 1
            switch(r15) {
                case 31: goto L_0x0013;
                case 32: goto L_0x0012;
                case 33: goto L_0x0018;
                case 34: goto L_0x0017;
                case 35: goto L_0x001d;
                case 36: goto L_0x001c;
                case 37: goto L_0x0022;
                case 38: goto L_0x0021;
                default: goto L_0x0004;
            }
        L_0x0004:
            p001a.C1520WN.codeBug()
            r1 = 0
            r8 = r0
            r6 = r1
        L_0x000a:
            r0 = 4
            double[] r10 = new double[r0]
            int r0 = (r12 > r12 ? 1 : (r12 == r12 ? 0 : -1))
            if (r0 == 0) goto L_0x0026
        L_0x0011:
            return r12
        L_0x0012:
            r0 = 0
        L_0x0013:
            r1 = 1
            r8 = r0
            r6 = r1
            goto L_0x000a
        L_0x0017:
            r0 = 0
        L_0x0018:
            r1 = 2
            r8 = r0
            r6 = r1
            goto L_0x000a
        L_0x001c:
            r0 = 0
        L_0x001d:
            r1 = 3
            r8 = r0
            r6 = r1
            goto L_0x000a
        L_0x0021:
            r0 = 0
        L_0x0022:
            r1 = 4
            r8 = r0
            r6 = r1
            goto L_0x000a
        L_0x0026:
            int r0 = r14.length
            if (r0 != 0) goto L_0x002e
            r0 = 1
            java.lang.Object[] r14 = p001a.C0903NH.padArguments(r14, r0)
        L_0x002e:
            r0 = 0
        L_0x002f:
            int r1 = r14.length
            if (r0 >= r1) goto L_0x0034
            if (r0 < r6) goto L_0x0076
        L_0x0034:
            if (r8 == 0) goto L_0x003a
            double r12 = m1788r(r12)
        L_0x003a:
            r2 = 0
            int r11 = r14.length
            r0 = 4
            if (r6 < r0) goto L_0x009c
            if (r2 >= r11) goto L_0x009c
            r4 = 1
            r0 = r10[r2]
        L_0x0044:
            r2 = 3
            if (r6 < r2) goto L_0x00a3
            if (r4 >= r11) goto L_0x00a3
            int r9 = r4 + 1
            r2 = r10[r4]
        L_0x004d:
            r4 = 2
            if (r6 < r4) goto L_0x00aa
            if (r9 >= r11) goto L_0x00aa
            int r7 = r9 + 1
            r4 = r10[r9]
        L_0x0056:
            r9 = 1
            if (r6 < r9) goto L_0x00b1
            if (r7 >= r11) goto L_0x00b1
            int r6 = r7 + 1
            r6 = r10[r7]
        L_0x005f:
            double r0 = m1761a(r0, r2, r4, r6)
            double r2 = m1779i(r12)
            double r0 = m1771d(r2, r0)
            if (r8 == 0) goto L_0x0071
            double r0 = m1789s(r0)
        L_0x0071:
            double r12 = m1794x(r0)
            goto L_0x0011
        L_0x0076:
            r1 = r14[r0]
            double r2 = p001a.C0903NH.toNumber((java.lang.Object) r1)
            r10[r0] = r2
            r2 = r10[r0]
            r4 = r10[r0]
            int r1 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r1 != 0) goto L_0x008e
            r2 = r10[r0]
            boolean r1 = java.lang.Double.isInfinite(r2)
            if (r1 == 0) goto L_0x0091
        L_0x008e:
            double r12 = p001a.C0903NH.NaN
            goto L_0x0011
        L_0x0091:
            r2 = r10[r0]
            double r2 = p001a.C0903NH.toInteger((double) r2)
            r10[r0] = r2
            int r0 = r0 + 1
            goto L_0x002f
        L_0x009c:
            int r0 = m1790t(r12)
            double r0 = (double) r0
            r4 = r2
            goto L_0x0044
        L_0x00a3:
            int r2 = m1791u(r12)
            double r2 = (double) r2
            r9 = r4
            goto L_0x004d
        L_0x00aa:
            int r4 = m1792v(r12)
            double r4 = (double) r4
            r7 = r9
            goto L_0x0056
        L_0x00b1:
            int r6 = m1793w(r12)
            double r6 = (double) r6
            goto L_0x005f
        */
        throw new UnsupportedOperationException("Method not decompiled: NativeDate.m1763a(double, java.lang.Object[], int):double");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        if (r12 == r12) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0021, code lost:
        if (r14.length >= 3) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0027, code lost:
        r6 = r0;
        r4 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002c, code lost:
        r6 = r0;
        r4 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0031, code lost:
        r6 = r0;
        r4 = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0035, code lost:
        r8[r0] = p001a.C0903NH.toNumber(r14[r0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0043, code lost:
        if (r8[r0] != r8[r0]) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004b, code lost:
        if (java.lang.Double.isInfinite(r8[r0]) == false) goto L_0x0050;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0050, code lost:
        r8[r0] = p001a.C0903NH.toInteger(r8[r0]);
        r0 = r0 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005b, code lost:
        r12 = p001a.C0903NH.NaN;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005d, code lost:
        r9 = r14.length;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0060, code lost:
        if (r4 < 3) goto L_0x0097;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0062, code lost:
        if (0 >= r9) goto L_0x0097;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0064, code lost:
        r7 = 1;
        r0 = r8[0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0068, code lost:
        if (r4 < 2) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x006a, code lost:
        if (r7 >= r9) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x006c, code lost:
        r5 = r7 + 1;
        r2 = r8[r7];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0071, code lost:
        if (r4 < 1) goto L_0x00a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0073, code lost:
        if (r5 >= r9) goto L_0x00a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0075, code lost:
        r4 = r5 + 1;
        r4 = r8[r5];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000a, code lost:
        r8 = new double[3];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0079, code lost:
        r0 = m1771d(m1773e(r0, r2, r4), m1780j(r12));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0085, code lost:
        if (r6 == false) goto L_0x008b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0087, code lost:
        r0 = m1789s(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0090, code lost:
        if (r6 == false) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0092, code lost:
        r12 = m1788r(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0097, code lost:
        r0 = (double) m1783m(r12);
        r7 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x009e, code lost:
        r2 = (double) m1784n(r12);
        r5 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00a5, code lost:
        r4 = (double) m1785o(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000e, code lost:
        if (r14.length != 0) goto L_0x0015;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        return p001a.C0903NH.NaN;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return p001a.C0903NH.NaN;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        return m1794x(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0010, code lost:
        r14 = p001a.C0903NH.padArguments(r14, 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0015, code lost:
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        if (r0 >= r14.length) goto L_0x001b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
        if (r0 < r4) goto L_0x0035;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static double m1768b(double r12, java.lang.Object[] r14, int r15) {
        /*
            r0 = 1
            switch(r15) {
                case 39: goto L_0x0027;
                case 40: goto L_0x0026;
                case 41: goto L_0x002c;
                case 42: goto L_0x002b;
                case 43: goto L_0x0031;
                case 44: goto L_0x0030;
                default: goto L_0x0004;
            }
        L_0x0004:
            p001a.C1520WN.codeBug()
            r1 = 0
            r6 = r0
            r4 = r1
        L_0x000a:
            r0 = 3
            double[] r8 = new double[r0]
            int r0 = r14.length
            if (r0 != 0) goto L_0x0015
            r0 = 1
            java.lang.Object[] r14 = p001a.C0903NH.padArguments(r14, r0)
        L_0x0015:
            r0 = 0
        L_0x0016:
            int r1 = r14.length
            if (r0 >= r1) goto L_0x001b
            if (r0 < r4) goto L_0x0035
        L_0x001b:
            int r0 = (r12 > r12 ? 1 : (r12 == r12 ? 0 : -1))
            if (r0 == 0) goto L_0x0090
            int r0 = r14.length
            r1 = 3
            if (r0 >= r1) goto L_0x005b
            double r0 = p001a.C0903NH.NaN
        L_0x0025:
            return r0
        L_0x0026:
            r0 = 0
        L_0x0027:
            r1 = 1
            r6 = r0
            r4 = r1
            goto L_0x000a
        L_0x002b:
            r0 = 0
        L_0x002c:
            r1 = 2
            r6 = r0
            r4 = r1
            goto L_0x000a
        L_0x0030:
            r0 = 0
        L_0x0031:
            r1 = 3
            r6 = r0
            r4 = r1
            goto L_0x000a
        L_0x0035:
            r1 = r14[r0]
            double r2 = p001a.C0903NH.toNumber((java.lang.Object) r1)
            r8[r0] = r2
            r2 = r8[r0]
            r10 = r8[r0]
            int r1 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1))
            if (r1 != 0) goto L_0x004d
            r2 = r8[r0]
            boolean r1 = java.lang.Double.isInfinite(r2)
            if (r1 == 0) goto L_0x0050
        L_0x004d:
            double r0 = p001a.C0903NH.NaN
            goto L_0x0025
        L_0x0050:
            r2 = r8[r0]
            double r2 = p001a.C0903NH.toInteger((double) r2)
            r8[r0] = r2
            int r0 = r0 + 1
            goto L_0x0016
        L_0x005b:
            r12 = 0
        L_0x005d:
            r2 = 0
            int r9 = r14.length
            r0 = 3
            if (r4 < r0) goto L_0x0097
            if (r2 >= r9) goto L_0x0097
            r7 = 1
            r0 = r8[r2]
        L_0x0067:
            r2 = 2
            if (r4 < r2) goto L_0x009e
            if (r7 >= r9) goto L_0x009e
            int r5 = r7 + 1
            r2 = r8[r7]
        L_0x0070:
            r7 = 1
            if (r4 < r7) goto L_0x00a5
            if (r5 >= r9) goto L_0x00a5
            int r4 = r5 + 1
            r4 = r8[r5]
        L_0x0079:
            double r0 = m1773e(r0, r2, r4)
            double r2 = m1780j(r12)
            double r0 = m1771d(r0, r2)
            if (r6 == 0) goto L_0x008b
            double r0 = m1789s(r0)
        L_0x008b:
            double r0 = m1794x(r0)
            goto L_0x0025
        L_0x0090:
            if (r6 == 0) goto L_0x005d
            double r12 = m1788r(r12)
            goto L_0x005d
        L_0x0097:
            int r0 = m1783m(r12)
            double r0 = (double) r0
            r7 = r2
            goto L_0x0067
        L_0x009e:
            int r2 = m1784n(r12)
            double r2 = (double) r2
            r5 = r7
            goto L_0x0070
        L_0x00a5:
            int r4 = m1785o(r12)
            double r4 = (double) r4
            goto L_0x0079
        */
        throw new UnsupportedOperationException("Method not decompiled: NativeDate.m1768b(double, java.lang.Object[], int):double");
    }

    public String getClassName() {
        return "Date";
    }

    public Object getDefaultValue(Class<?> cls) {
        if (cls == null) {
            cls = org.mozilla1.javascript.ScriptRuntime.StringClass;
        }
        return super.getDefaultValue(cls);
    }

    /* access modifiers changed from: package-private */
    public double aDB() {
        return this.cCw;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo1118a(org.mozilla1.javascript.IdFunctionObject yy) {
        mo15696a((org.mozilla1.javascript.Scriptable) yy, cBh, (int) cBw, "now", 0);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, cBh, -2, "parse", 1);
        mo15696a((org.mozilla1.javascript.Scriptable) yy, cBh, -1, "UTC", 1);
        super.mo1118a(yy);
    }

    /* access modifiers changed from: protected */
    public void initPrototypeId(int i) {
        String str;
        int i2 = 1;
        switch (i) {
            case 1:
                str = "constructor";
                break;
            case 2:
                i2 = 0;
                str = "toString";
                break;
            case 3:
                i2 = 0;
                str = "toTimeString";
                break;
            case 4:
                i2 = 0;
                str = "toDateString";
                break;
            case 5:
                i2 = 0;
                str = "toLocaleString";
                break;
            case 6:
                i2 = 0;
                str = "toLocaleTimeString";
                break;
            case 7:
                i2 = 0;
                str = "toLocaleDateString";
                break;
            case 8:
                i2 = 0;
                str = "toUTCString";
                break;
            case 9:
                i2 = 0;
                str = "toSource";
                break;
            case 10:
                i2 = 0;
                str = "valueOf";
                break;
            case 11:
                i2 = 0;
                str = "getTime";
                break;
            case 12:
                i2 = 0;
                str = "getYear";
                break;
            case 13:
                i2 = 0;
                str = "getFullYear";
                break;
            case 14:
                i2 = 0;
                str = "getUTCFullYear";
                break;
            case 15:
                i2 = 0;
                str = "getMonth";
                break;
            case 16:
                i2 = 0;
                str = "getUTCMonth";
                break;
            case 17:
                i2 = 0;
                str = "getDate";
                break;
            case 18:
                i2 = 0;
                str = "getUTCDate";
                break;
            case 19:
                i2 = 0;
                str = "getDay";
                break;
            case 20:
                i2 = 0;
                str = "getUTCDay";
                break;
            case 21:
                i2 = 0;
                str = "getHours";
                break;
            case 22:
                i2 = 0;
                str = "getUTCHours";
                break;
            case 23:
                i2 = 0;
                str = "getMinutes";
                break;
            case 24:
                i2 = 0;
                str = "getUTCMinutes";
                break;
            case 25:
                i2 = 0;
                str = "getSeconds";
                break;
            case 26:
                i2 = 0;
                str = "getUTCSeconds";
                break;
            case 27:
                i2 = 0;
                str = "getMilliseconds";
                break;
            case 28:
                i2 = 0;
                str = "getUTCMilliseconds";
                break;
            case 29:
                i2 = 0;
                str = "getTimezoneOffset";
                break;
            case 30:
                str = "setTime";
                break;
            case 31:
                str = "setMilliseconds";
                break;
            case 32:
                str = "setUTCMilliseconds";
                break;
            case 33:
                i2 = 2;
                str = "setSeconds";
                break;
            case 34:
                i2 = 2;
                str = "setUTCSeconds";
                break;
            case 35:
                i2 = 3;
                str = "setMinutes";
                break;
            case 36:
                i2 = 3;
                str = "setUTCMinutes";
                break;
            case 37:
                i2 = 4;
                str = "setHours";
                break;
            case 38:
                i2 = 4;
                str = "setUTCHours";
                break;
            case 39:
                str = "setDate";
                break;
            case 40:
                str = "setUTCDate";
                break;
            case 41:
                i2 = 2;
                str = "setMonth";
                break;
            case 42:
                i2 = 2;
                str = "setUTCMonth";
                break;
            case 43:
                i2 = 3;
                str = "setFullYear";
                break;
            case 44:
                i2 = 3;
                str = "setUTCFullYear";
                break;
            case 45:
                str = "setYear";
                break;
            default:
                throw new IllegalArgumentException(String.valueOf(i));
        }
        initPrototypeMethod(cBh, i, str, i2);
    }

    public Object execIdCall(IdFunctionObject yy, Context lhVar, org.mozilla1.javascript.Scriptable avf, Scriptable avf2, Object[] objArr) {
        double d;
        double r;
        double d2;
        if (!yy.hasTag(cBh)) {
            return super.execIdCall(yy, lhVar, avf, avf2, objArr);
        }
        int methodId = yy.methodId();
        switch (methodId) {
            case cBw /*-3*/:
                return org.mozilla1.javascript.ScriptRuntime.wrapNumber(aDC());
            case -2:
                return org.mozilla1.javascript.ScriptRuntime.wrapNumber(m1772dT(org.mozilla1.javascript.ScriptRuntime.toString(objArr, 0)));
            case -1:
                return org.mozilla1.javascript.ScriptRuntime.wrapNumber(m1775f(objArr));
            case 1:
                if (avf2 != null) {
                    return m1764a(aDC(), 2);
                }
                return m1776g(objArr);
            default:
                if (!(avf2 instanceof NativeDate)) {
                    throw m25341f(yy);
                }
                NativeDate cu = (NativeDate) avf2;
                double d3 = cu.cCw;
                switch (methodId) {
                    case 2:
                    case 3:
                    case 4:
                        if (d3 == d3) {
                            return m1764a(d3, methodId);
                        }
                        return cBi;
                    case 5:
                    case 6:
                    case 7:
                        if (d3 == d3) {
                            return m1769b(d3, methodId);
                        }
                        return cBi;
                    case 8:
                        if (d3 == d3) {
                            return m1795y(d3);
                        }
                        return cBi;
                    case 9:
                        return "(new Date(" + org.mozilla1.javascript.ScriptRuntime.toString(d3) + "))";
                    case 10:
                    case 11:
                        return org.mozilla1.javascript.ScriptRuntime.wrapNumber(d3);
                    case 12:
                    case 13:
                    case 14:
                        if (d3 == d3) {
                            if (methodId != 14) {
                                d3 = m1788r(d3);
                            }
                            d3 = (double) m1783m(d3);
                            if (methodId == 12) {
                                if (!lhVar.hasFeature(1)) {
                                    d3 -= 1900.0d;
                                } else if (1900.0d <= d3 && d3 < 2000.0d) {
                                    d3 -= 1900.0d;
                                }
                            }
                        }
                        return org.mozilla1.javascript.ScriptRuntime.wrapNumber(d3);
                    case 15:
                    case 16:
                        if (d3 == d3) {
                            if (methodId == 15) {
                                d3 = m1788r(d3);
                            }
                            d3 = (double) m1784n(d3);
                        }
                        return org.mozilla1.javascript.ScriptRuntime.wrapNumber(d3);
                    case 17:
                    case 18:
                        if (d3 == d3) {
                            if (methodId == 17) {
                                d3 = m1788r(d3);
                            }
                            d3 = (double) m1785o(d3);
                        }
                        return org.mozilla1.javascript.ScriptRuntime.wrapNumber(d3);
                    case 19:
                    case 20:
                        if (d3 == d3) {
                            if (methodId == 19) {
                                d3 = m1788r(d3);
                            }
                            d3 = (double) m1786p(d3);
                        }
                        return org.mozilla1.javascript.ScriptRuntime.wrapNumber(d3);
                    case 21:
                    case 22:
                        if (d3 == d3) {
                            if (methodId == 21) {
                                d3 = m1788r(d3);
                            }
                            d3 = (double) m1790t(d3);
                        }
                        return org.mozilla1.javascript.ScriptRuntime.wrapNumber(d3);
                    case 23:
                    case 24:
                        if (d3 == d3) {
                            if (methodId == 23) {
                                d3 = m1788r(d3);
                            }
                            d3 = (double) m1791u(d3);
                        }
                        return org.mozilla1.javascript.ScriptRuntime.wrapNumber(d3);
                    case 25:
                    case 26:
                        if (d3 == d3) {
                            if (methodId == 25) {
                                d3 = m1788r(d3);
                            }
                            d3 = (double) m1792v(d3);
                        }
                        return org.mozilla1.javascript.ScriptRuntime.wrapNumber(d3);
                    case 27:
                    case 28:
                        if (d3 == d3) {
                            if (methodId == 27) {
                                d3 = m1788r(d3);
                            }
                            d3 = (double) m1793w(d3);
                        }
                        return org.mozilla1.javascript.ScriptRuntime.wrapNumber(d3);
                    case 29:
                        if (d3 == d3) {
                            d3 = (d3 - m1788r(d3)) / cBt;
                        }
                        return org.mozilla1.javascript.ScriptRuntime.wrapNumber(d3);
                    case 30:
                        double x = m1794x(org.mozilla1.javascript.ScriptRuntime.toNumber(objArr, 0));
                        cu.cCw = x;
                        return org.mozilla1.javascript.ScriptRuntime.wrapNumber(x);
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                        double a = m1763a(d3, objArr, methodId);
                        cu.cCw = a;
                        return org.mozilla1.javascript.ScriptRuntime.wrapNumber(a);
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                        double b = m1768b(d3, objArr, methodId);
                        cu.cCw = b;
                        return org.mozilla1.javascript.ScriptRuntime.wrapNumber(b);
                    case 45:
                        double number = org.mozilla1.javascript.ScriptRuntime.toNumber(objArr, 0);
                        if (number != number || Double.isInfinite(number)) {
                            d = org.mozilla1.javascript.ScriptRuntime.NaN;
                        } else {
                            if (d3 != d3) {
                                r = 0.0d;
                            } else {
                                r = m1788r(d3);
                            }
                            if (number < org.mozilla1.javascript.ScriptRuntime.NaN || number > 99.0d) {
                                d2 = number;
                            } else {
                                d2 = number + 1900.0d;
                            }
                            d = m1794x(m1789s(m1771d(m1773e(d2, (double) m1784n(r), (double) m1785o(r)), m1780j(r))));
                        }
                        cu.cCw = d;
                        return ScriptRuntime.wrapNumber(d);
                    default:
                        throw new IllegalArgumentException(String.valueOf(methodId));
                }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int findPrototypeId(java.lang.String r8) {
        /*
            r7 = this;
            r6 = 116(0x74, float:1.63E-43)
            r1 = 3
            r5 = 115(0x73, float:1.61E-43)
            r4 = 103(0x67, float:1.44E-43)
            r0 = 0
            r2 = 0
            int r3 = r8.length()
            switch(r3) {
                case 6: goto L_0x001c;
                case 7: goto L_0x0021;
                case 8: goto L_0x0065;
                case 9: goto L_0x009d;
                case 10: goto L_0x00a3;
                case 11: goto L_0x00ef;
                case 12: goto L_0x0156;
                case 13: goto L_0x016c;
                case 14: goto L_0x01a6;
                case 15: goto L_0x01c1;
                case 16: goto L_0x0010;
                case 17: goto L_0x01d5;
                case 18: goto L_0x01db;
                default: goto L_0x0010;
            }
        L_0x0010:
            r1 = r0
        L_0x0011:
            if (r2 == 0) goto L_0x0209
            if (r2 == r8) goto L_0x0209
            boolean r2 = r2.equals(r8)
            if (r2 != 0) goto L_0x0209
        L_0x001b:
            return r0
        L_0x001c:
            java.lang.String r2 = "getDay"
            r1 = 19
            goto L_0x0011
        L_0x0021:
            char r1 = r8.charAt(r1)
            switch(r1) {
                case 68: goto L_0x002a;
                case 84: goto L_0x003c;
                case 89: goto L_0x004e;
                case 117: goto L_0x0060;
                default: goto L_0x0028;
            }
        L_0x0028:
            r1 = r0
            goto L_0x0011
        L_0x002a:
            char r1 = r8.charAt(r0)
            if (r1 != r4) goto L_0x0035
            java.lang.String r2 = "getDate"
            r1 = 17
            goto L_0x0011
        L_0x0035:
            if (r1 != r5) goto L_0x0010
            java.lang.String r2 = "setDate"
            r1 = 39
            goto L_0x0011
        L_0x003c:
            char r1 = r8.charAt(r0)
            if (r1 != r4) goto L_0x0047
            java.lang.String r2 = "getTime"
            r1 = 11
            goto L_0x0011
        L_0x0047:
            if (r1 != r5) goto L_0x0010
            java.lang.String r2 = "setTime"
            r1 = 30
            goto L_0x0011
        L_0x004e:
            char r1 = r8.charAt(r0)
            if (r1 != r4) goto L_0x0059
            java.lang.String r2 = "getYear"
            r1 = 12
            goto L_0x0011
        L_0x0059:
            if (r1 != r5) goto L_0x0010
            java.lang.String r2 = "setYear"
            r1 = 45
            goto L_0x0011
        L_0x0060:
            java.lang.String r2 = "valueOf"
            r1 = 10
            goto L_0x0011
        L_0x0065:
            char r1 = r8.charAt(r1)
            switch(r1) {
                case 72: goto L_0x006e;
                case 77: goto L_0x0080;
                case 111: goto L_0x0092;
                case 116: goto L_0x0098;
                default: goto L_0x006c;
            }
        L_0x006c:
            r1 = r0
            goto L_0x0011
        L_0x006e:
            char r1 = r8.charAt(r0)
            if (r1 != r4) goto L_0x0079
            java.lang.String r2 = "getHours"
            r1 = 21
            goto L_0x0011
        L_0x0079:
            if (r1 != r5) goto L_0x0010
            java.lang.String r2 = "setHours"
            r1 = 37
            goto L_0x0011
        L_0x0080:
            char r1 = r8.charAt(r0)
            if (r1 != r4) goto L_0x008b
            java.lang.String r2 = "getMonth"
            r1 = 15
            goto L_0x0011
        L_0x008b:
            if (r1 != r5) goto L_0x0010
            java.lang.String r2 = "setMonth"
            r1 = 41
            goto L_0x0011
        L_0x0092:
            java.lang.String r2 = "toSource"
            r1 = 9
            goto L_0x0011
        L_0x0098:
            java.lang.String r2 = "toString"
            r1 = 2
            goto L_0x0011
        L_0x009d:
            java.lang.String r2 = "getUTCDay"
            r1 = 20
            goto L_0x0011
        L_0x00a3:
            char r1 = r8.charAt(r1)
            r3 = 77
            if (r1 != r3) goto L_0x00bf
            char r1 = r8.charAt(r0)
            if (r1 != r4) goto L_0x00b7
            java.lang.String r2 = "getMinutes"
            r1 = 23
            goto L_0x0011
        L_0x00b7:
            if (r1 != r5) goto L_0x0010
            java.lang.String r2 = "setMinutes"
            r1 = 35
            goto L_0x0011
        L_0x00bf:
            r3 = 83
            if (r1 != r3) goto L_0x00d7
            char r1 = r8.charAt(r0)
            if (r1 != r4) goto L_0x00cf
            java.lang.String r2 = "getSeconds"
            r1 = 25
            goto L_0x0011
        L_0x00cf:
            if (r1 != r5) goto L_0x0010
            java.lang.String r2 = "setSeconds"
            r1 = 33
            goto L_0x0011
        L_0x00d7:
            r3 = 85
            if (r1 != r3) goto L_0x0010
            char r1 = r8.charAt(r0)
            if (r1 != r4) goto L_0x00e7
            java.lang.String r2 = "getUTCDate"
            r1 = 18
            goto L_0x0011
        L_0x00e7:
            if (r1 != r5) goto L_0x0010
            java.lang.String r2 = "setUTCDate"
            r1 = 40
            goto L_0x0011
        L_0x00ef:
            char r1 = r8.charAt(r1)
            switch(r1) {
                case 70: goto L_0x00f9;
                case 77: goto L_0x010d;
                case 84: goto L_0x0113;
                case 85: goto L_0x0119;
                case 115: goto L_0x0151;
                default: goto L_0x00f6;
            }
        L_0x00f6:
            r1 = r0
            goto L_0x0011
        L_0x00f9:
            char r1 = r8.charAt(r0)
            if (r1 != r4) goto L_0x0105
            java.lang.String r2 = "getFullYear"
            r1 = 13
            goto L_0x0011
        L_0x0105:
            if (r1 != r5) goto L_0x0010
            java.lang.String r2 = "setFullYear"
            r1 = 43
            goto L_0x0011
        L_0x010d:
            java.lang.String r2 = "toGMTString"
            r1 = 8
            goto L_0x0011
        L_0x0113:
            java.lang.String r2 = "toUTCString"
            r1 = 8
            goto L_0x0011
        L_0x0119:
            char r1 = r8.charAt(r0)
            if (r1 != r4) goto L_0x0137
            r1 = 9
            char r1 = r8.charAt(r1)
            r3 = 114(0x72, float:1.6E-43)
            if (r1 != r3) goto L_0x012f
            java.lang.String r2 = "getUTCHours"
            r1 = 22
            goto L_0x0011
        L_0x012f:
            if (r1 != r6) goto L_0x0010
            java.lang.String r2 = "getUTCMonth"
            r1 = 16
            goto L_0x0011
        L_0x0137:
            if (r1 != r5) goto L_0x0010
            r1 = 9
            char r1 = r8.charAt(r1)
            r3 = 114(0x72, float:1.6E-43)
            if (r1 != r3) goto L_0x0149
            java.lang.String r2 = "setUTCHours"
            r1 = 38
            goto L_0x0011
        L_0x0149:
            if (r1 != r6) goto L_0x0010
            java.lang.String r2 = "setUTCMonth"
            r1 = 42
            goto L_0x0011
        L_0x0151:
            java.lang.String r2 = "constructor"
            r1 = 1
            goto L_0x0011
        L_0x0156:
            r3 = 2
            char r3 = r8.charAt(r3)
            r4 = 68
            if (r3 != r4) goto L_0x0164
            java.lang.String r2 = "toDateString"
            r1 = 4
            goto L_0x0011
        L_0x0164:
            r4 = 84
            if (r3 != r4) goto L_0x0010
            java.lang.String r2 = "toTimeString"
            goto L_0x0011
        L_0x016c:
            char r1 = r8.charAt(r0)
            if (r1 != r4) goto L_0x018b
            r1 = 6
            char r1 = r8.charAt(r1)
            r3 = 77
            if (r1 != r3) goto L_0x0181
            java.lang.String r2 = "getUTCMinutes"
            r1 = 24
            goto L_0x0011
        L_0x0181:
            r3 = 83
            if (r1 != r3) goto L_0x0010
            java.lang.String r2 = "getUTCSeconds"
            r1 = 26
            goto L_0x0011
        L_0x018b:
            if (r1 != r5) goto L_0x0010
            r1 = 6
            char r1 = r8.charAt(r1)
            r3 = 77
            if (r1 != r3) goto L_0x019c
            java.lang.String r2 = "setUTCMinutes"
            r1 = 36
            goto L_0x0011
        L_0x019c:
            r3 = 83
            if (r1 != r3) goto L_0x0010
            java.lang.String r2 = "setUTCSeconds"
            r1 = 34
            goto L_0x0011
        L_0x01a6:
            char r1 = r8.charAt(r0)
            if (r1 != r4) goto L_0x01b2
            java.lang.String r2 = "getUTCFullYear"
            r1 = 14
            goto L_0x0011
        L_0x01b2:
            if (r1 != r5) goto L_0x01ba
            java.lang.String r2 = "setUTCFullYear"
            r1 = 44
            goto L_0x0011
        L_0x01ba:
            if (r1 != r6) goto L_0x0010
            java.lang.String r2 = "toLocaleString"
            r1 = 5
            goto L_0x0011
        L_0x01c1:
            char r1 = r8.charAt(r0)
            if (r1 != r4) goto L_0x01cd
            java.lang.String r2 = "getMilliseconds"
            r1 = 27
            goto L_0x0011
        L_0x01cd:
            if (r1 != r5) goto L_0x0010
            java.lang.String r2 = "setMilliseconds"
            r1 = 31
            goto L_0x0011
        L_0x01d5:
            java.lang.String r2 = "getTimezoneOffset"
            r1 = 29
            goto L_0x0011
        L_0x01db:
            char r1 = r8.charAt(r0)
            if (r1 != r4) goto L_0x01e7
            java.lang.String r2 = "getUTCMilliseconds"
            r1 = 28
            goto L_0x0011
        L_0x01e7:
            if (r1 != r5) goto L_0x01ef
            java.lang.String r2 = "setUTCMilliseconds"
            r1 = 32
            goto L_0x0011
        L_0x01ef:
            if (r1 != r6) goto L_0x0010
            r1 = 8
            char r1 = r8.charAt(r1)
            r3 = 68
            if (r1 != r3) goto L_0x0200
            java.lang.String r2 = "toLocaleDateString"
            r1 = 7
            goto L_0x0011
        L_0x0200:
            r3 = 84
            if (r1 != r3) goto L_0x0010
            java.lang.String r2 = "toLocaleTimeString"
            r1 = 6
            goto L_0x0011
        L_0x0209:
            r0 = r1
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: NativeDate.findPrototypeId(java.lang.String):int");
    }
}
