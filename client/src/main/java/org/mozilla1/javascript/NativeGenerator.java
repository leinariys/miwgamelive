package org.mozilla1.javascript;

/* renamed from: a.mt */
/* compiled from: a */
public final class NativeGenerator extends IdScriptableObject {
    public static final int iJR = 0;
    public static final int iJS = 1;
    public static final int iJT = 2;
    private static final int hOh = 2;
    private static final int hOi = 5;
    private static final Object iJQ = new Integer(2);
    private static final int iJU = 1;
    private static final int iJV = 3;
    private static final int iJW = 4;

    /* renamed from: nt */
    private static final int f8667nt = 5;
    private static final long serialVersionUID = 1645892441041347273L;
    private boolean dtt;
    private String eKW;
    private Object iJX;
    private boolean iJY = true;
    private int lineNumber;

    /* renamed from: nu */
    private org.mozilla1.javascript.NativeFunction f8668nu;

    private NativeGenerator() {
    }

    public NativeGenerator(Scriptable avf, NativeFunction ifR, Object obj) {
        this.f8668nu = ifR;
        this.iJX = obj;
        Scriptable x = ScriptableObject.m23595x(avf);
        setParentScope(x);
        setPrototype((NativeGenerator) ScriptableObject.m23581d(x, iJQ));
    }

    /* renamed from: c */
    static NativeGenerator m35874c(ScriptableObject akn, boolean z) {
        NativeGenerator mtVar = new NativeGenerator();
        if (akn != null) {
            mtVar.setParentScope(akn);
            mtVar.setPrototype(ScriptableObject.m23593v(akn));
        }
        mtVar.activatePrototypeMap(5);
        if (z) {
            mtVar.sealObject();
        }
        if (akn != null) {
            akn.associateValue(iJQ, mtVar);
        }
        return mtVar;
    }

    public String getClassName() {
        return "Generator";
    }

    public void finalize() {
        ContextFactory bkJ;
        if (this.iJX != null) {
            org.mozilla1.javascript.Context bwq = org.mozilla1.javascript.Context.bwq();
            if (bwq != null) {
                bkJ = bwq.bwr();
            } else {
                bkJ = ContextFactory.getGlobal();
            }
            bkJ.mo19793a((org.mozilla1.javascript.ContextAction) new CloseGeneratorAction(this));
        }
    }

    /* access modifiers changed from: protected */
    public void initPrototypeId(int i) {
        String str;
        int i2 = 1;
        switch (i) {
            case 1:
                str = "close";
                break;
            case 2:
                str = "next";
                break;
            case 3:
                str = "send";
                i2 = 0;
                break;
            case 4:
                str = "throw";
                i2 = 0;
                break;
            case 5:
                str = NativeIterator.hOg;
                break;
            default:
                throw new IllegalArgumentException(String.valueOf(i));
        }
        initPrototypeMethod(iJQ, i, str, i2);
    }

    public Object execIdCall(IdFunctionObject yy, org.mozilla1.javascript.Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        if (!yy.hasTag(iJQ)) {
            return super.execIdCall(yy, lhVar, avf, avf2, objArr);
        }
        int methodId = yy.methodId();
        if (!(avf2 instanceof NativeGenerator)) {
            throw m25341f(yy);
        }
        NativeGenerator mtVar = (NativeGenerator) avf2;
        switch (methodId) {
            case 1:
                return mtVar.m35872a(lhVar, avf, 2, new C3007a());
            case 2:
                mtVar.iJY = false;
                return mtVar.m35872a(lhVar, avf, 0, org.mozilla1.javascript.Undefined.instance);
            case 3:
                Object obj = objArr.length > 0 ? objArr[0] : org.mozilla1.javascript.Undefined.instance;
                if (!mtVar.iJY || obj.equals(org.mozilla1.javascript.Undefined.instance)) {
                    return mtVar.m35872a(lhVar, avf, 0, obj);
                }
                throw org.mozilla1.javascript.ScriptRuntime.m7600no("msg.send.newborn");
            case 4:
                return mtVar.m35872a(lhVar, avf, 1, objArr.length > 0 ? objArr[0] : org.mozilla1.javascript.Undefined.instance);
            case 5:
                return avf2;
            default:
                throw new IllegalArgumentException(String.valueOf(methodId));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Object m35872a(org.mozilla1.javascript.Context lhVar, Scriptable avf, int i, Object obj) {
        if (this.iJX != null) {
            try {
                synchronized (this) {
                    if (this.dtt) {
                        throw org.mozilla1.javascript.ScriptRuntime.m7600no("msg.already.exec.gen");
                    }
                    this.dtt = true;
                }
                Object a = this.f8668nu.mo2855a(lhVar, avf, i, this.iJX, obj);
                synchronized (this) {
                    this.dtt = false;
                }
                if (i != 2) {
                    return a;
                }
                this.iJX = null;
                return a;
            } catch (C3007a e) {
                try {
                    Object obj2 = org.mozilla1.javascript.Undefined.instance;
                    synchronized (this) {
                        this.dtt = false;
                        if (i != 2) {
                            return obj2;
                        }
                        this.iJX = null;
                        return obj2;
                    }
                } catch (Throwable th) {
                    synchronized (this) {
                        this.dtt = false;
                        if (i == 2) {
                            this.iJX = null;
                        }
                        throw th;
                    }
                }
            } catch (RhinoException e2) {
                this.lineNumber = e2.lineNumber();
                this.eKW = e2.lineSource();
                this.iJX = null;
                throw e2;
            }
        } else if (i == 2) {
            return Undefined.instance;
        } else {
            if (i != 1) {
                obj = NativeIterator.m38643o(avf);
            }
            throw new JavaScriptException(obj, this.eKW, this.lineNumber);
        }
    }

    /* access modifiers changed from: protected */
    public int findPrototypeId(String str) {
        int i = 4;
        String str2 = null;
        int length = str.length();
        if (length == 4) {
            char charAt = str.charAt(0);
            if (charAt == 'n') {
                str2 = "next";
                i = 2;
            } else {
                if (charAt == 's') {
                    str2 = "send";
                    i = 3;
                }
                i = 0;
            }
        } else if (length == 5) {
            char charAt2 = str.charAt(0);
            if (charAt2 == 'c') {
                str2 = "close";
                i = 1;
            } else {
                if (charAt2 == 't') {
                    str2 = "throw";
                }
                i = 0;
            }
        } else {
            if (length == 12) {
                str2 = NativeIterator.hOg;
                i = 5;
            }
            i = 0;
        }
        if (str2 == null || str2 == str || str2.equals(str)) {
            return i;
        }
        return 0;
    }

    /* renamed from: a.mt$a */
    public static class C3007a extends RuntimeException {
        private static final long serialVersionUID = 2561315658662379681L;
    }

    /* renamed from: a.mt$b */
    /* compiled from: a */
    private static class CloseGeneratorAction implements ContextAction {
        private NativeGenerator cnN;

        CloseGeneratorAction(NativeGenerator mtVar) {
            this.cnN = mtVar;
        }

        public Object run(org.mozilla1.javascript.Context lhVar) {
            return ScriptRuntime.m7510a((org.mozilla1.javascript.Callable) new C3009a(), lhVar, ScriptableObject.m23595x(this.cnN), (Scriptable) this.cnN, (Object[]) null);
        }

        /* renamed from: a.mt$b$a */
        class C3009a implements Callable {
            C3009a() {
            }

            public Object call(Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
                return ((NativeGenerator) avf2).m35872a(lhVar, avf, 2, new C3007a());
            }
        }
    }
}
