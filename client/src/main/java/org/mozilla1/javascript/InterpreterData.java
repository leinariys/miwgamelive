package org.mozilla1.javascript;

import org.mozilla1.javascript.debug.DebuggableScript;

import java.io.Serializable;

/* renamed from: a.aeQ  reason: case insensitive filesystem */
/* compiled from: a */
final class InterpreterData implements org.mozilla1.javascript.debug.DebuggableScript, Serializable {
    static final int fri = 1024;
    static final int frj = 64;
    static final int frk = 64;
    static final long serialVersionUID = 5067677351589230234L;
    int eMN;
    boolean eMO;
    boolean elw;
    int frA;
    String frB;
    int frC;
    int frD;
    int frE;
    boolean frF;
    Object[] frG;
    UintMap frH;
    int frI = -1;
    InterpreterData frJ;
    boolean frK;
    String frl;
    String frm;
    String[] frn;
    double[] fro;
    InterpreterData[] frp;
    Object[] frq;
    byte[] frr;
    int[] frs;
    int frt;
    int fru;
    int frv;
    int frw;
    String[] frx;
    boolean[] fry;
    int frz;

    InterpreterData(int i, String str, String str2) {
        this.frE = i;
        this.frm = str;
        this.frB = str2;
        init();
    }

    InterpreterData(InterpreterData aeq) {
        this.frJ = aeq;
        this.frE = aeq.frE;
        this.frm = aeq.frm;
        this.frB = aeq.frB;
        init();
    }

    private void init() {
        this.frr = new byte[1024];
        this.frn = new String[64];
    }

    public boolean isTopLevel() {
        return this.frF;
    }

    public boolean isFunction() {
        return this.eMN != 0;
    }

    public String getFunctionName() {
        return this.frl;
    }

    public int getParamCount() {
        return this.frz;
    }

    public int getParamAndVarCount() {
        return this.frx.length;
    }

    public String getParamOrVarName(int i) {
        return this.frx[i];
    }

    /* renamed from: kH */
    public boolean mo13073kH(int i) {
        return this.fry[i];
    }

    public String getSourceName() {
        return this.frm;
    }

    public boolean isGeneratedScript() {
        return ScriptRuntime.m7601np(this.frm);
    }

    public int[] getLineNumbers() {
        return Interpreter.m39156b(this);
    }

    public int getFunctionCount() {
        if (this.frp == null) {
            return 0;
        }
        return this.frp.length;
    }

    /* renamed from: gW */
    public org.mozilla1.javascript.debug.DebuggableScript mo1978gW(int i) {
        return this.frp[i];
    }

    public DebuggableScript aNR() {
        return this.frJ;
    }
}
