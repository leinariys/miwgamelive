package org.mozilla1.javascript;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/* renamed from: a.akl  reason: case insensitive filesystem */
/* compiled from: a */
public class UintMap implements Serializable {
    static final long serialVersionUID = 4242698212885848444L;
    private static final int EMPTY = -1;
    private static final boolean check = false;
    private static final int fTf = -2;
    private static final int fjJ = -1640531527;
    private transient int fTg;
    private int fjL;
    private int fjM;
    private transient int fjN;
    private transient int[] keys;
    private transient Object[] values;

    public UintMap() {
        this(4);
    }

    public UintMap(int i) {
        if (i < 0) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        int i2 = 2;
        while ((1 << i2) < (i * 4) / 3) {
            i2++;
        }
        this.fjL = i2;
    }

    /* renamed from: q */
    private static int m23536q(int i, int i2, int i3) {
        int i4 = 32 - (i3 * 2);
        if (i4 >= 0) {
            return ((i >>> i4) & i2) | 1;
        }
        return ((i2 >>> (-i4)) & i) | 1;
    }

    public boolean isEmpty() {
        return this.fjM == 0;
    }

    public int size() {
        return this.fjM;
    }

    public boolean has(int i) {
        if (i < 0) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        return m23537sb(i) >= 0;
    }

    public Object getObject(int i) {
        int sb;
        if (i < 0) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        if (this.values == null || (sb = m23537sb(i)) < 0) {
            return null;
        }
        return this.values[sb];
    }

    public int getInt(int i, int i2) {
        if (i < 0) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        int sb = m23537sb(i);
        if (sb < 0) {
            return i2;
        }
        if (this.fTg != 0) {
            return this.keys[sb + this.fTg];
        }
        return 0;
    }

    public int getExistingInt(int i) {
        if (i < 0) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        int sb = m23537sb(i);
        if (sb < 0) {
            org.mozilla1.javascript.Kit.codeBug();
            return 0;
        } else if (this.fTg != 0) {
            return this.keys[sb + this.fTg];
        } else {
            return 0;
        }
    }

    public void put(int i, Object obj) {
        if (i < 0) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        int h = m23535h(i, false);
        if (this.values == null) {
            this.values = new Object[(1 << this.fjL)];
        }
        this.values[h] = obj;
    }

    public void put(int i, int i2) {
        if (i < 0) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        int h = m23535h(i, true);
        if (this.fTg == 0) {
            int i3 = 1 << this.fjL;
            if (this.keys.length != i3 * 2) {
                int[] iArr = new int[(i3 * 2)];
                System.arraycopy(this.keys, 0, iArr, 0, i3);
                this.keys = iArr;
            }
            this.fTg = i3;
        }
        this.keys[h + this.fTg] = i2;
    }

    public void remove(int i) {
        if (i < 0) {
            Kit.codeBug();
        }
        int sb = m23537sb(i);
        if (sb >= 0) {
            this.keys[sb] = -2;
            this.fjM--;
            if (this.values != null) {
                this.values[sb] = null;
            }
            if (this.fTg != 0) {
                this.keys[sb + this.fTg] = 0;
            }
        }
    }

    public void clear() {
        int i = 1 << this.fjL;
        if (this.keys != null) {
            for (int i2 = 0; i2 != i; i2++) {
                this.keys[i2] = -1;
            }
            if (this.values != null) {
                for (int i3 = 0; i3 != i; i3++) {
                    this.values[i3] = null;
                }
            }
        }
        this.fTg = 0;
        this.fjM = 0;
        this.fjN = 0;
    }

    public int[] getKeys() {
        int[] iArr = this.keys;
        int i = this.fjM;
        int[] iArr2 = new int[i];
        int i2 = 0;
        while (i != 0) {
            int i3 = iArr[i2];
            if (!(i3 == -1 || i3 == -2)) {
                i--;
                iArr2[i] = i3;
            }
            i2++;
        }
        return iArr2;
    }

    /* renamed from: sb */
    private int m23537sb(int i) {
        int i2;
        int[] iArr = this.keys;
        if (iArr != null) {
            int i3 = i * fjJ;
            int i4 = i3 >>> (32 - this.fjL);
            int i5 = iArr[i4];
            if (i5 == i) {
                return i4;
            }
            if (i5 != -1) {
                int i6 = (1 << this.fjL) - 1;
                int q = m23536q(i3, i6, this.fjL);
                do {
                    i4 = (i4 + q) & i6;
                    i2 = iArr[i4];
                    if (i2 == i) {
                        return i4;
                    }
                } while (i2 != -1);
            }
        }
        return -1;
    }

    /* renamed from: sc */
    private int m23538sc(int i) {
        int[] iArr = this.keys;
        int i2 = i * fjJ;
        int i3 = i2 >>> (32 - this.fjL);
        if (iArr[i3] != -1) {
            int i4 = (1 << this.fjL) - 1;
            int q = m23536q(i2, i4, this.fjL);
            do {
                i3 = (i3 + q) & i4;
            } while (iArr[i3] != -1);
        }
        iArr[i3] = i;
        this.fjN++;
        this.fjM++;
        return i3;
    }

    /* renamed from: fd */
    private void m23534fd(boolean z) {
        int i = 0;
        if (this.keys != null && this.fjM * 2 >= this.fjN) {
            this.fjL++;
        }
        int i2 = 1 << this.fjL;
        int[] iArr = this.keys;
        int i3 = this.fTg;
        if (i3 != 0 || z) {
            this.fTg = i2;
            this.keys = new int[(i2 * 2)];
        } else {
            this.keys = new int[i2];
        }
        for (int i4 = 0; i4 != i2; i4++) {
            this.keys[i4] = -1;
        }
        Object[] objArr = this.values;
        if (objArr != null) {
            this.values = new Object[i2];
        }
        int i5 = this.fjM;
        this.fjN = 0;
        if (i5 != 0) {
            this.fjM = 0;
            while (i5 != 0) {
                int i6 = iArr[i];
                if (!(i6 == -1 || i6 == -2)) {
                    int sc = m23538sc(i6);
                    if (objArr != null) {
                        this.values[sc] = objArr[i];
                    }
                    if (i3 != 0) {
                        this.keys[sc + this.fTg] = iArr[i3 + i];
                    }
                    i5--;
                }
                i++;
            }
        }
    }

    /* renamed from: h */
    private int m23535h(int i, boolean z) {
        int i2;
        int i3;
        int i4;
        int[] iArr = this.keys;
        if (iArr != null) {
            int i5 = i * fjJ;
            i3 = i5 >>> (32 - this.fjL);
            int i6 = iArr[i3];
            if (i6 == i) {
                return i3;
            }
            if (i6 != -1) {
                if (i6 == -2) {
                    i2 = i3;
                } else {
                    i2 = -1;
                }
                int i7 = (1 << this.fjL) - 1;
                int q = m23536q(i5, i7, this.fjL);
                do {
                    i3 = (i3 + q) & i7;
                    i4 = iArr[i3];
                    if (i4 == i) {
                        return i3;
                    }
                    if (i4 == -2 && i2 < 0) {
                        i2 = i3;
                        continue;
                    }
                } while (i4 != -1);
            } else {
                i2 = -1;
            }
        } else {
            i2 = -1;
            i3 = -1;
        }
        if (i2 < 0) {
            if (iArr == null || this.fjN * 4 >= (1 << this.fjL) * 3) {
                m23534fd(z);
                int[] iArr2 = this.keys;
                return m23538sc(i);
            }
            this.fjN++;
            i2 = i3;
        }
        iArr[i2] = i;
        this.fjM++;
        return i2;
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        int i;
        boolean z = true;
        objectOutputStream.defaultWriteObject();
        int i2 = this.fjM;
        if (i2 != 0) {
            boolean z2 = this.fTg != 0;
            if (this.values == null) {
                z = false;
            }
            objectOutputStream.writeBoolean(z2);
            objectOutputStream.writeBoolean(z);
            int i3 = 0;
            while (i2 != 0) {
                int i4 = this.keys[i3];
                if (i4 == -1 || i4 == -2) {
                    i = i2;
                } else {
                    i = i2 - 1;
                    objectOutputStream.writeInt(i4);
                    if (z2) {
                        objectOutputStream.writeInt(this.keys[this.fTg + i3]);
                    }
                    if (z) {
                        objectOutputStream.writeObject(this.values[i3]);
                    }
                }
                i3++;
                i2 = i;
            }
        }
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        int i = this.fjM;
        if (i != 0) {
            this.fjM = 0;
            boolean readBoolean = objectInputStream.readBoolean();
            boolean readBoolean2 = objectInputStream.readBoolean();
            int i2 = 1 << this.fjL;
            if (readBoolean) {
                this.keys = new int[(i2 * 2)];
                this.fTg = i2;
            } else {
                this.keys = new int[i2];
            }
            for (int i3 = 0; i3 != i2; i3++) {
                this.keys[i3] = -1;
            }
            if (readBoolean2) {
                this.values = new Object[i2];
            }
            for (int i4 = 0; i4 != i; i4++) {
                int sc = m23538sc(objectInputStream.readInt());
                if (readBoolean) {
                    this.keys[this.fTg + sc] = objectInputStream.readInt();
                }
                if (readBoolean2) {
                    this.values[sc] = objectInputStream.readObject();
                }
            }
        }
    }
}
