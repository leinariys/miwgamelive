package org.mozilla1.javascript;

/* renamed from: a.akX  reason: case insensitive filesystem */
/* compiled from: a */
final class NativeNumber extends IdScriptableObject {
    static final long serialVersionUID = 3504516769741512101L;
    private static final int cBB = 3;
    private static final int cBF = 5;
    private static final int ceN = 4;
    private static final Object fUY = new Integer(19);
    private static final int fUZ = 100;
    private static final int fVa = 6;
    private static final int fVb = 7;
    private static final int fVc = 8;
    /* renamed from: ns */
    private static final int f4789ns = 1;
    /* renamed from: nt */
    private static final int f4790nt = 8;
    /* renamed from: xT */
    private static final int f4791xT = 2;
    private double doubleValue;

    private NativeNumber(double d) {
        this.doubleValue = d;
    }

    /* renamed from: a */
    static void m23467a(org.mozilla1.javascript.Scriptable avf, boolean z) {
        new NativeNumber(org.mozilla1.javascript.ScriptRuntime.NaN).mo15695a(8, avf, z);
    }

    /* renamed from: a */
    private static String m23466a(double d, Object[] objArr, int i, int i2, int i3, int i4) {
        int i5 = 0;
        if (objArr.length != 0) {
            int int32 = org.mozilla1.javascript.ScriptRuntime.toInt32(objArr[0]);
            if (int32 < i3 || int32 > 100) {
                throw org.mozilla1.javascript.ScriptRuntime.m7535aY("RangeError", org.mozilla1.javascript.ScriptRuntime.getMessage1("msg.bad.precision", org.mozilla1.javascript.ScriptRuntime.toString(objArr[0])));
            }
            i5 = int32;
            i = i2;
        }
        StringBuffer stringBuffer = new StringBuffer();
        DToA.JS_dtostr(stringBuffer, i, i5 + i4, d);
        return stringBuffer.toString();
    }

    public String getClassName() {
        return "Number";
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo1118a(IdFunctionObject yy) {
        yy.defineProperty("NaN", (Object) org.mozilla1.javascript.ScriptRuntime.NaNobj, 7);
        yy.defineProperty("POSITIVE_INFINITY", (Object) org.mozilla1.javascript.ScriptRuntime.wrapNumber(Double.POSITIVE_INFINITY), 7);
        yy.defineProperty("NEGATIVE_INFINITY", (Object) org.mozilla1.javascript.ScriptRuntime.wrapNumber(Double.NEGATIVE_INFINITY), 7);
        yy.defineProperty("MAX_VALUE", (Object) org.mozilla1.javascript.ScriptRuntime.wrapNumber(Double.MAX_VALUE), 7);
        yy.defineProperty("MIN_VALUE", (Object) org.mozilla1.javascript.ScriptRuntime.wrapNumber(Double.MIN_VALUE), 7);
        super.mo1118a(yy);
    }

    /* access modifiers changed from: protected */
    public void initPrototypeId(int i) {
        String str;
        int i2 = 1;
        switch (i) {
            case 1:
                str = "constructor";
                break;
            case 2:
                str = "toString";
                break;
            case 3:
                str = "toLocaleString";
                break;
            case 4:
                str = "toSource";
                i2 = 0;
                break;
            case 5:
                str = "valueOf";
                i2 = 0;
                break;
            case 6:
                str = "toFixed";
                break;
            case 7:
                str = "toExponential";
                break;
            case 8:
                str = "toPrecision";
                break;
            default:
                throw new IllegalArgumentException(String.valueOf(i));
        }
        initPrototypeMethod(fUY, i, str, i2);
    }

    public Object execIdCall(IdFunctionObject yy, Context lhVar, org.mozilla1.javascript.Scriptable avf, Scriptable avf2, Object[] objArr) {
        if (!yy.hasTag(fUY)) {
            return super.execIdCall(yy, lhVar, avf, avf2, objArr);
        }
        int methodId = yy.methodId();
        if (methodId == 1) {
            double number = objArr.length >= 1 ? org.mozilla1.javascript.ScriptRuntime.toNumber(objArr[0]) : 0.0d;
            if (avf2 == null) {
                return new NativeNumber(number);
            }
            return org.mozilla1.javascript.ScriptRuntime.wrapNumber(number);
        } else if (!(avf2 instanceof NativeNumber)) {
            throw m25341f(yy);
        } else {
            double d = ((NativeNumber) avf2).doubleValue;
            switch (methodId) {
                case 2:
                case 3:
                    return org.mozilla1.javascript.ScriptRuntime.numberToString(d, objArr.length == 0 ? 10 : org.mozilla1.javascript.ScriptRuntime.toInt32(objArr[0]));
                case 4:
                    return "(new Number(" + org.mozilla1.javascript.ScriptRuntime.toString(d) + "))";
                case 5:
                    return org.mozilla1.javascript.ScriptRuntime.wrapNumber(d);
                case 6:
                    return m23466a(d, objArr, 2, 2, -20, 0);
                case 7:
                    return m23466a(d, objArr, 1, 3, 0, 1);
                case 8:
                    return m23466a(d, objArr, 0, 4, 1, 0);
                default:
                    throw new IllegalArgumentException(String.valueOf(methodId));
            }
        }
    }

    public String toString() {
        return ScriptRuntime.numberToString(this.doubleValue, 10);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int findPrototypeId(java.lang.String r6) {
        /*
            r5 = this;
            r1 = 3
            r4 = 116(0x74, float:1.63E-43)
            r0 = 0
            r2 = 0
            int r3 = r6.length()
            switch(r3) {
                case 7: goto L_0x0018;
                case 8: goto L_0x002a;
                case 9: goto L_0x000c;
                case 10: goto L_0x000c;
                case 11: goto L_0x003c;
                case 12: goto L_0x000c;
                case 13: goto L_0x004f;
                case 14: goto L_0x0053;
                default: goto L_0x000c;
            }
        L_0x000c:
            r1 = r0
        L_0x000d:
            if (r2 == 0) goto L_0x0056
            if (r2 == r6) goto L_0x0056
            boolean r2 = r2.equals(r6)
            if (r2 != 0) goto L_0x0056
        L_0x0017:
            return r0
        L_0x0018:
            char r1 = r6.charAt(r0)
            if (r1 != r4) goto L_0x0022
            java.lang.String r2 = "toFixed"
            r1 = 6
            goto L_0x000d
        L_0x0022:
            r3 = 118(0x76, float:1.65E-43)
            if (r1 != r3) goto L_0x000c
            java.lang.String r2 = "valueOf"
            r1 = 5
            goto L_0x000d
        L_0x002a:
            char r1 = r6.charAt(r1)
            r3 = 111(0x6f, float:1.56E-43)
            if (r1 != r3) goto L_0x0036
            java.lang.String r2 = "toSource"
            r1 = 4
            goto L_0x000d
        L_0x0036:
            if (r1 != r4) goto L_0x000c
            java.lang.String r2 = "toString"
            r1 = 2
            goto L_0x000d
        L_0x003c:
            char r1 = r6.charAt(r0)
            r3 = 99
            if (r1 != r3) goto L_0x0048
            java.lang.String r2 = "constructor"
            r1 = 1
            goto L_0x000d
        L_0x0048:
            if (r1 != r4) goto L_0x000c
            java.lang.String r2 = "toPrecision"
            r1 = 8
            goto L_0x000d
        L_0x004f:
            java.lang.String r2 = "toExponential"
            r1 = 7
            goto L_0x000d
        L_0x0053:
            java.lang.String r2 = "toLocaleString"
            goto L_0x000d
        L_0x0056:
            r0 = r1
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6311akX.findPrototypeId(java.lang.String):int");
    }
}
