package org.mozilla1.javascript;

/* renamed from: a.aax  reason: case insensitive filesystem */
/* compiled from: a */
final class Arguments extends IdScriptableObject {
    static final long serialVersionUID = 4275508002492040609L;
    private static final int eVm = 1;
    private static final int eVn = 2;
    private static final int eVo = 3;
    private static final int eVp = 3;
    private Object[] args;
    private Object eVq;
    private Object eVr;
    private Object eVs = new Integer(this.args.length);
    private org.mozilla1.javascript.NativeCall eVt;

    public Arguments(org.mozilla1.javascript.NativeCall bhVar) {
        this.eVt = bhVar;
        org.mozilla1.javascript.Scriptable parentScope = bhVar.getParentScope();
        setParentScope(parentScope);
        setPrototype(ScriptableObject.m23593v(parentScope));
        this.args = bhVar.f5876nv;
        org.mozilla1.javascript.NativeFunction ifR = bhVar.f5875nu;
        this.eVr = ifR;
        int languageVersion = ifR.getLanguageVersion();
        if (languageVersion > 130 || languageVersion == 0) {
            this.eVq = NOT_FOUND;
        } else {
            this.eVq = null;
        }
    }

    public String getClassName() {
        return "Object";
    }

    public boolean has(int i, org.mozilla1.javascript.Scriptable avf) {
        if (i < 0 || i >= this.args.length || this.args[i] == NOT_FOUND) {
            return super.has(i, avf);
        }
        return true;
    }

    public Object get(int i, org.mozilla1.javascript.Scriptable avf) {
        Object obj;
        if (i < 0 || i >= this.args.length || (obj = this.args[i]) == NOT_FOUND) {
            return super.get(i, avf);
        }
        if (!m19845ps(i)) {
            return obj;
        }
        Object obj2 = this.eVt.get(this.eVt.f5875nu.getParamOrVarName(i), this.eVt);
        if (obj2 != NOT_FOUND) {
            return obj2;
        }
        org.mozilla1.javascript.Kit.codeBug();
        return obj2;
    }

    /* renamed from: ps */
    private boolean m19845ps(int i) {
        NativeFunction ifR = this.eVt.f5875nu;
        int paramCount = ifR.getParamCount();
        if (i >= paramCount) {
            return false;
        }
        if (i < paramCount - 1) {
            String paramOrVarName = ifR.getParamOrVarName(i);
            for (int i2 = i + 1; i2 < paramCount; i2++) {
                if (paramOrVarName.equals(ifR.getParamOrVarName(i2))) {
                    return false;
                }
            }
        }
        return true;
    }

    public void put(int i, Scriptable avf, Object obj) {
        if (i >= 0 && i < this.args.length && this.args[i] != NOT_FOUND) {
            if (m19845ps(i)) {
                this.eVt.put(this.eVt.f5875nu.getParamOrVarName(i), this.eVt, obj);
                return;
            }
            synchronized (this) {
                if (this.args[i] != NOT_FOUND) {
                    if (this.args == this.eVt.f5876nv) {
                        this.args = (Object[]) this.args.clone();
                    }
                    this.args[i] = obj;
                    return;
                }
            }
        }
        super.put(i, avf, obj);
    }

    public void delete(int i) {
        if (i >= 0 && i < this.args.length) {
            synchronized (this) {
                if (this.args[i] != NOT_FOUND) {
                    if (this.args == this.eVt.f5876nv) {
                        this.args = (Object[]) this.args.clone();
                    }
                    this.args[i] = NOT_FOUND;
                    return;
                }
            }
        }
        super.delete(i);
    }

    /* access modifiers changed from: protected */
    public int getMaxInstanceId() {
        return 3;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0037  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int findInstanceIdInfo(java.lang.String r6) {
        /*
            r5 = this;
            r3 = 2
            r0 = 0
            r2 = 0
            int r1 = r6.length()
            r4 = 6
            if (r1 != r4) goto L_0x0047
            r1 = 5
            char r1 = r6.charAt(r1)
            r4 = 101(0x65, float:1.42E-43)
            if (r1 != r4) goto L_0x0027
            java.lang.String r2 = "callee"
            r1 = 1
        L_0x0016:
            if (r2 == 0) goto L_0x0045
            if (r2 == r6) goto L_0x0045
            boolean r2 = r2.equals(r6)
            if (r2 != 0) goto L_0x0045
        L_0x0020:
            if (r0 != 0) goto L_0x0037
            int r0 = super.findInstanceIdInfo(r6)
        L_0x0026:
            return r0
        L_0x0027:
            r4 = 104(0x68, float:1.46E-43)
            if (r1 != r4) goto L_0x002f
            java.lang.String r2 = "length"
            r1 = r3
            goto L_0x0016
        L_0x002f:
            r4 = 114(0x72, float:1.6E-43)
            if (r1 != r4) goto L_0x0047
            java.lang.String r2 = "caller"
            r1 = 3
            goto L_0x0016
        L_0x0037:
            switch(r0) {
                case 1: goto L_0x0040;
                case 2: goto L_0x0040;
                case 3: goto L_0x0040;
                default: goto L_0x003a;
            }
        L_0x003a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        L_0x0040:
            int r0 = instanceIdInfo(r3, r0)
            goto L_0x0026
        L_0x0045:
            r0 = r1
            goto L_0x0020
        L_0x0047:
            r1 = r0
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C5817aax.findInstanceIdInfo(java.lang.String):int");
    }

    /* access modifiers changed from: protected */
    public String getInstanceIdName(int i) {
        switch (i) {
            case 1:
                return "callee";
            case 2:
                return "length";
            case 3:
                return "caller";
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public Object getInstanceIdValue(int i) {
        NativeCall bhVar;
        switch (i) {
            case 1:
                return this.eVr;
            case 2:
                return this.eVs;
            case 3:
                Object obj = this.eVq;
                if (obj == org.mozilla1.javascript.UniqueTag.f5855nc) {
                    return null;
                }
                if (obj != null || (bhVar = this.eVt.f5877nw) == null) {
                    return obj;
                }
                return bhVar.get("arguments", bhVar);
            default:
                return super.getInstanceIdValue(i);
        }
    }

    /* access modifiers changed from: protected */
    public void setInstanceIdValue(int i, Object obj) {
        switch (i) {
            case 1:
                this.eVr = obj;
                return;
            case 2:
                this.eVs = obj;
                return;
            case 3:
                if (obj == null) {
                    obj = UniqueTag.f5855nc;
                }
                this.eVq = obj;
                return;
            default:
                super.setInstanceIdValue(i, obj);
                return;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: dP */
    public Object[] mo12353dP(boolean z) {
        int intValue;
        Object[] dP = super.mo12353dP(z);
        if (z && this.args.length != 0) {
            int length = this.args.length;
            boolean[] zArr = null;
            for (int i = 0; i != dP.length; i++) {
                Object obj = dP[i];
                if ((obj instanceof Integer) && (intValue = ((Integer) obj).intValue()) >= 0 && intValue < this.args.length) {
                    if (zArr == null) {
                        zArr = new boolean[this.args.length];
                    }
                    if (!zArr[intValue]) {
                        zArr[intValue] = true;
                        length--;
                    }
                }
            }
            if (length != 0) {
                Object[] objArr = new Object[(dP.length + length)];
                System.arraycopy(dP, 0, objArr, length, dP.length);
                int i2 = 0;
                for (int i3 = 0; i3 != this.args.length; i3++) {
                    if (zArr == null || !zArr[i3]) {
                        objArr[i2] = new Integer(i3);
                        i2++;
                    }
                }
                if (i2 != length) {
                    Kit.codeBug();
                }
                return objArr;
            }
        }
        return dP;
    }
}
