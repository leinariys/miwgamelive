package org.mozilla1.javascript;

import logic.thred.LogPrinter;
import org.mozilla1.javascript.debug.DebugFrame;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.sq */
/* compiled from: a */
public class Interpreter implements Evaluator {
    private static final int epS = 32;
    private static final int epV = 40;
    private static final int htA = -25;
    private static final int htB = -26;
    private static final int htC = -27;
    private static final int htD = -28;
    private static final int htE = -29;
    private static final int htF = -30;
    private static final int htG = -31;
    private static final int htH = -32;
    private static final int htI = -33;
    private static final int htJ = -34;
    private static final int htK = -35;
    private static final int htL = -36;
    private static final int htM = -37;
    private static final int htN = -38;
    private static final int htO = -39;
    private static final int htP = -40;
    private static final int htQ = -41;
    private static final int htR = -42;
    private static final int htS = -43;
    private static final int htT = -44;
    private static final int htU = -45;
    private static final int htV = -46;
    private static final int htW = -47;
    private static final int htX = -48;
    private static final int htY = -49;
    private static final int htZ = -50;
    private static final int htc = -1;
    private static final int htd = -2;
    private static final int hte = -3;
    private static final int htf = -4;
    private static final int htg = -5;
    private static final int hth = -6;
    private static final int hti = -7;
    private static final int htj = -8;
    private static final int htk = -9;
    private static final int htl = -10;
    private static final int htm = -11;
    private static final int htn = -12;
    private static final int hto = -13;
    private static final int htp = -14;
    private static final int htq = -15;
    private static final int htr = -16;
    private static final int hts = -17;
    private static final int htt = -18;
    private static final int htu = -19;
    private static final int htv = -20;
    private static final int htw = -21;
    private static final int htx = -22;
    private static final int hty = -23;
    private static final int htz = -24;
    private static final int huA = 1;
    private static final int huB = 2;
    private static final int huC = 3;
    private static final int huD = 4;
    private static final int huE = 5;
    private static final int huF = 6;
    private static final int huG = 1;
    private static final int hua = -51;
    private static final int huc = -52;
    private static final int hud = -53;
    private static final int hue = -54;
    private static final int huf = -55;
    private static final int hug = -56;
    private static final int huh = -57;
    private static final int hui = -58;
    private static final int huj = -59;
    private static final int huk = -60;
    private static final int hul = -61;
    private static final int hum = -62;
    private static final int hun = -63;
    private static final int huo = -64;
    private static final int hup = -64;
    private static final int huz = 0;
    private org.mozilla1.javascript.ScriptOrFnNode eYU;
    private int eZc;
    private int epC;
    private int[] epT;
    private int epU;
    private long[] epW;
    private int epX;

    /* renamed from: he */
    private org.mozilla1.javascript.CompilerEnvirons f9155he;
    private boolean huq;
    private boolean hur;
    private org.mozilla1.javascript.InterpreterData hus;
    private int hut;
    private int huu;
    private int huv;
    private org.mozilla1.javascript.ObjToIntMap huw = new org.mozilla1.javascript.ObjToIntMap(20);
    private int hux;
    private org.mozilla1.javascript.ObjArray huy = new ObjArray();

    /* renamed from: a */
    private static C3604a m39131a(C3604a aVar) {
        aVar.bgx = true;
        C3604a abD = aVar.abD();
        aVar.bgx = false;
        abD.bgv = null;
        abD.bgw = 0;
        return abD;
    }

    /* renamed from: wS */
    private static String m39170wS(int i) {
        if (m39173wV(i)) {
            return String.valueOf(i);
        }
        throw new IllegalArgumentException(String.valueOf(i));
    }

    /* renamed from: wT */
    private static boolean m39171wT(int i) {
        return -64 <= i && i <= -1;
    }

    /* renamed from: wU */
    private static boolean m39172wU(int i) {
        return 2 <= i && i <= 79;
    }

    /* renamed from: wV */
    private static boolean m39173wV(int i) {
        return m39171wT(i) || m39172wU(i);
    }

    private static int getShort(byte[] bArr, int i) {
        return (bArr[i] << 8) | (bArr[i + 1] & 255);
    }

    /* renamed from: d */
    private static int m39159d(byte[] bArr, int i) {
        return ((bArr[i] & 255) << 8) | (bArr[i + 1] & 255);
    }

    private static int getInt(byte[] bArr, int i) {
        return (bArr[i] << 24) | ((bArr[i + 1] & 255) << LogPrinter.eqN) | ((bArr[i + 2] & 255) << 8) | (bArr[i + 3] & 255);
    }

    /* renamed from: a */
    private static int m39127a(C3604a aVar, boolean z) {
        int i = -1;
        int[] iArr = aVar.bgz.frs;
        if (iArr != null) {
            int i2 = aVar.f9158pc - 1;
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            while (i3 != iArr.length) {
                int i6 = iArr[i3 + 0];
                int i7 = iArr[i3 + 1];
                if (i6 > i2) {
                    i7 = i4;
                } else if (i2 >= i7) {
                    i7 = i4;
                } else if (!z || iArr[i3 + 3] == 1) {
                    if (i >= 0) {
                        if (i4 < i7) {
                            i7 = i4;
                        } else {
                            if (i5 > i6) {
                                org.mozilla1.javascript.Kit.codeBug();
                            }
                            if (i4 == i7) {
                                org.mozilla1.javascript.Kit.codeBug();
                            }
                        }
                    }
                    i5 = i6;
                    i = i3;
                } else {
                    i7 = i4;
                }
                i3 += 6;
                i4 = i7;
            }
        }
        return i;
    }

    /* renamed from: a */
    private static void m39139a(org.mozilla1.javascript.InterpreterData aeq) {
    }

    /* renamed from: xg */
    private static int m39184xg(int i) {
        switch (i) {
            case hun /*-63*/:
            case hum /*-62*/:
            case hue /*-54*/:
            case htV /*-46*/:
            case htO /*-39*/:
            case htC /*-27*/:
            case htB /*-26*/:
            case hty /*-23*/:
            case hth /*-6*/:
            case 5:
            case 6:
            case 7:
            case 50:
            case 72:
                return 3;
            case hul /*-61*/:
            case htY /*-49*/:
            case htX /*-48*/:
                return 2;
            case htW /*-47*/:
                return 5;
            case htU /*-45*/:
                return 2;
            case htP /*-40*/:
                return 5;
            case htN /*-38*/:
                return 2;
            case htD /*-28*/:
                return 5;
            case htw /*-21*/:
                return 5;
            case htm /*-11*/:
            case -10:
            case htk /*-9*/:
            case htj /*-8*/:
            case hti /*-7*/:
                return 2;
            case 57:
                return 2;
            default:
                if (m39173wV(i)) {
                    return 1;
                }
                throw org.mozilla1.javascript.Kit.codeBug();
        }
    }

    /* renamed from: b */
    static int[] m39156b(org.mozilla1.javascript.InterpreterData aeq) {
        org.mozilla1.javascript.UintMap akl = new org.mozilla1.javascript.UintMap();
        byte[] bArr = aeq.frr;
        int length = bArr.length;
        int i = 0;
        while (i != length) {
            byte b = bArr[i];
            int xg = m39184xg(b);
            if (b == htB) {
                if (xg != 3) {
                    org.mozilla1.javascript.Kit.codeBug();
                }
                akl.put(m39159d(bArr, i + 1), 0);
            }
            i += xg;
        }
        return akl.getKeys();
    }

    /* renamed from: c */
    static String m39157c(org.mozilla1.javascript.InterpreterData aeq) {
        if (aeq.frB == null) {
            return null;
        }
        return aeq.frB.substring(aeq.frC, aeq.frD);
    }

    /* renamed from: b */
    private static void m39153b(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.InterpretedFunction afo, int i) {
        org.mozilla1.javascript.InterpretedFunction a = org.mozilla1.javascript.InterpretedFunction.m21539a(lhVar, avf, afo, i);
        org.mozilla1.javascript.ScriptRuntime.m7530a(lhVar, avf, (org.mozilla1.javascript.NativeFunction) a, a.bgz.eMN, afo.bgz.frK);
    }

    /* renamed from: a */
    static Object m39133a(org.mozilla1.javascript.InterpretedFunction afo, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        if (!org.mozilla1.javascript.ScriptRuntime.m7588i(lhVar)) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        if (lhVar.eln != afo.fvF) {
            Object obj = lhVar.eln;
            lhVar.eln = afo.fvF;
            try {
                return afo.elo.callWithDomain(afo.fvF, lhVar, afo, avf, avf2, objArr);
            } finally {
                lhVar.eln = obj;
            }
        } else {
            C3604a aVar = new C3604a((C3604a) null);
            m39140a(lhVar, avf, avf2, objArr, (double[]) null, 0, objArr.length, afo, (C3604a) null, aVar);
            aVar.bgI = lhVar.elj;
            lhVar.elj = false;
            return m39136a(lhVar, aVar, (Object) null);
        }
    }

    /* renamed from: a */
    public static Object m39134a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, int i, Object obj, Object obj2) {
        C3604a aVar = (C3604a) obj;
        C3606c cVar = new C3606c(i, obj2);
        if (i == 2) {
            try {
                return m39136a(lhVar, aVar, (Object) cVar);
            } catch (RuntimeException e) {
                if (e == obj2) {
                    return org.mozilla1.javascript.Undefined.instance;
                }
                throw e;
            }
        } else {
            Object a = m39136a(lhVar, aVar, (Object) cVar);
            if (cVar.gqP == null) {
                return a;
            }
            throw cVar.gqP;
        }
    }

    /* renamed from: a */
    public static Object m39132a(NativeContinuation ahs, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr) {
        Object obj;
        if (!org.mozilla1.javascript.ScriptRuntime.m7588i(lhVar)) {
            return org.mozilla1.javascript.ScriptRuntime.m7510a((Callable) ahs, lhVar, avf, (org.mozilla1.javascript.Scriptable) null, objArr);
        }
        if (objArr.length == 0) {
            obj = org.mozilla1.javascript.Undefined.instance;
        } else {
            obj = objArr[0];
        }
        if (((C3604a) ahs.getImplementation()) == null) {
            return obj;
        }
        C3605b bVar = new C3605b(ahs, (C3604a) null);
        bVar.bgK = obj;
        return m39136a(lhVar, (C3604a) null, (Object) bVar);
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x01d0, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01d1, code lost:
        r24 = m39127a(r13, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01d5, code lost:
        if (r24 >= 0) goto L_0x01d7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x01d7, code lost:
        r20 = r8;
        r19 = r10;
        r12 = r22;
        r18 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00f3, code lost:
        r4.printStackTrace(java.lang.System.err);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00fd, code lost:
        throw new java.lang.IllegalStateException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:419:0x0b10, code lost:
        if (r23.bgH != false) goto L_0x0b3d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:421:0x0b16, code lost:
        if ((r32[r24] & 1) != 0) goto L_0x0b27;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:423:0x0b26, code lost:
        throw p001a.C2909lh.m35246q("msg.var.redecl", r23.bgz.frx[r24]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:425:0x0b2b, code lost:
        if ((r32[r24] & 8) == 0) goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:426:0x0b2d, code lost:
        r30[r24] = r28[r6];
        r32[r24] = r32[r24] & htk;
        r31[r24] = r29[r6];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:427:0x0b3d, code lost:
        r4 = r28[r6];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:428:0x0b41, code lost:
        if (r4 != r26) goto L_0x11dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:429:0x0b43, code lost:
        r7 = p001a.C0903NH.wrapNumber(r29[r6]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:430:0x0b4a, code lost:
        r5 = r23.bgz.frx[r24];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:434:0x0b58, code lost:
        if ((r23.f9156TR instanceof p001a.C0316EH) == false) goto L_0x0b6b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:435:0x0b5a, code lost:
        ((p001a.C0316EH) r23.f9156TR).mo1827a(r5, r23.f9156TR, r7);
        r22 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:437:0x0b6f, code lost:
        throw p001a.C1520WN.codeBug();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:438:0x0b70, code lost:
        r4 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:439:0x0b71, code lost:
        r6 = r20;
        r10 = r19;
        r11 = r5;
        r13 = r23;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:444:0x0b8a, code lost:
        if (r23.bgH != false) goto L_0x0b9c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:446:0x0b90, code lost:
        if ((r32[r24] & 1) != 0) goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:447:0x0b92, code lost:
        r30[r24] = r28[r6];
        r31[r24] = r29[r6];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:448:0x0b9c, code lost:
        r4 = r28[r6];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:449:0x0ba0, code lost:
        if (r4 != r26) goto L_0x0ba8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:450:0x0ba2, code lost:
        r4 = p001a.C0903NH.wrapNumber(r29[r6]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:451:0x0ba8, code lost:
        r5 = r23.bgz.frx[r24];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:454:?, code lost:
        r23.f9156TR.put(r5, r23.f9156TR, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:455:0x0bbb, code lost:
        r22 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:459:0x0bcb, code lost:
        r6 = r6 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:460:0x0bd1, code lost:
        if (r23.bgH != false) goto L_0x0bdf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:461:0x0bd3, code lost:
        r28[r6] = r30[r4];
        r29[r6] = r31[r4];
        r24 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:462:0x0bdf, code lost:
        r5 = r23.bgz.frx[r4];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:465:?, code lost:
        r28[r6] = r23.f9156TR.get(r5, r23.f9156TR);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:466:0x0bf5, code lost:
        r24 = r4;
        r22 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0145, code lost:
        m39154b(r36, r23, (java.lang.Object) null);
        r5 = r23.bgK;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        r6 = r23.bgL;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0159, code lost:
        if (r23.bgv == null) goto L_0x1205;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x015b, code lost:
        r8 = r23.bgv;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:635:0x10a4, code lost:
        r8 = r6;
        r22 = r11;
        r5 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:637:0x10ac, code lost:
        if ((r5 instanceof p001a.C2317dx) != false) goto L_0x10ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:638:0x10ae, code lost:
        r7 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:640:0x10b4, code lost:
        if ((r5 instanceof p001a.C0068Ap) != false) goto L_0x10b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:641:0x10b6, code lost:
        r7 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:643:0x10bc, code lost:
        if ((r5 instanceof p001a.C1319TJ) != false) goto L_0x10be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:644:0x10be, code lost:
        r7 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:646:0x10c4, code lost:
        if ((r5 instanceof java.lang.RuntimeException) != false) goto L_0x10c6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:648:0x10ce, code lost:
        if (r36.hasFeature(13) != false) goto L_0x10d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:649:0x10d0, code lost:
        r4 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0161, code lost:
        if (r8.bgx == false) goto L_0x0167;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:650:0x10d1, code lost:
        r7 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:651:0x10d4, code lost:
        r4 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:653:0x10d8, code lost:
        if ((r5 instanceof java.lang.Error) != false) goto L_0x10da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:655:0x10e2, code lost:
        if (r36.hasFeature(13) != false) goto L_0x10e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:656:0x10e4, code lost:
        r4 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:657:0x10e5, code lost:
        r7 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:658:0x10e8, code lost:
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0163, code lost:
        r8 = r8.abD();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:660:0x10ec, code lost:
        if ((r5 instanceof Interpreter.C3605b) != false) goto L_0x10ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:661:0x10ee, code lost:
        r7 = 1;
        r6 = (Interpreter.C3605b) r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:663:0x10fd, code lost:
        if (r36.hasFeature(13) != false) goto L_0x10ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:664:0x10ff, code lost:
        r4 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:665:0x1100, code lost:
        r7 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:666:0x1103, code lost:
        r4 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:667:0x1105, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:668:0x1106, code lost:
        r7 = 1;
        r5 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:669:0x110a, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0167, code lost:
        m39144a(r8, r5, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:670:0x110b, code lost:
        r7 = 0;
        r6 = null;
        r5 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:671:0x1110, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:672:0x1111, code lost:
        r6 = null;
        r7 = 0;
        r11 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:673:0x1116, code lost:
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:674:0x1119, code lost:
        m39154b(r36, r13, r11);
        r13 = r13.bgv;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:675:0x1120, code lost:
        if (r13 == null) goto L_0x1122;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:676:0x1122, code lost:
        if (r6 != null) goto L_0x1124;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:678:0x1126, code lost:
        if (r6.cuZ != null) goto L_0x1128;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:679:0x1128, code lost:
        p001a.C1520WN.codeBug();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x016a, code lost:
        r19 = null;
        r20 = r6;
        r12 = r22;
        r13 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:681:0x112d, code lost:
        if (r6.cuY != null) goto L_0x112f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:682:0x112f, code lost:
        r24 = -1;
        r20 = r8;
        r19 = r10;
        r12 = r22;
        r18 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:683:0x113b, code lost:
        if (r6 == null) goto L_0x01cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:686:0x1141, code lost:
        r24 = -1;
        r20 = r8;
        r19 = r10;
        r12 = r22;
        r18 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:687:0x114d, code lost:
        r5 = r6.bgK;
        r6 = r6.bgL;
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:704:0x118f, code lost:
        r4 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:705:0x1190, code lost:
        r10 = r5;
        r11 = r22;
        r13 = r23;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:706:0x1197, code lost:
        r4 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:707:0x1198, code lost:
        r10 = r5;
        r11 = r22;
        r13 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:714:0x11b9, code lost:
        r4 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:715:0x11ba, code lost:
        r6 = r20;
        r10 = r5;
        r11 = r22;
        r13 = r23;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:716:0x11c3, code lost:
        r6 = r8;
        r5 = r10;
        r4 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0177, code lost:
        if (r23.bgx != false) goto L_0x0184;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:723:0x11dc, code lost:
        r7 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:732:0x1202, code lost:
        r11 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:733:0x1205, code lost:
        r4 = r18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0184, code lost:
        r5 = m39137a(r23, r6, r17, (int) r35);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0190, code lost:
        if (r5 == p001a.aVF.NOT_FOUND) goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0192, code lost:
        r8 = r20;
        r10 = r19;
        r13 = r23;
        r5 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0198, code lost:
        if (r5 == null) goto L_0x019a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x019a, code lost:
        p001a.C1520WN.codeBug();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x019d, code lost:
        r6 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x019e, code lost:
        if (r17 == null) goto L_0x10aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:829:0x00ab, code lost:
        r24 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:830:0x00ab, code lost:
        r24 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:831:0x00ab, code lost:
        r24 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:832:0x00ab, code lost:
        r24 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:833:0x00ab, code lost:
        r24 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:834:0x00ab, code lost:
        r24 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01ad, code lost:
        r7 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01af, code lost:
        if (r16 != false) goto L_0x01b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        m39141a(r36, r13, 100);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:898:?, code lost:
        return m39135a(r36, r23, r6, r17);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01ba, code lost:
        if (r13.bgG == null) goto L_0x1202;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:?, code lost:
        r13.bgG.onExceptionThrown(r36, (java.lang.RuntimeException) r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01ca, code lost:
        r11 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01cb, code lost:
        if (r7 != 0) goto L_0x01cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01ce, code lost:
        if (r7 != 2) goto L_0x01d0;
     */
    /* JADX WARNING: Failed to insert additional move for type inference */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0244 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x02b1 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x02e7 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x0333 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x034f A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x0366 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x037d A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x039b A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:194:0x03af A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:196:0x03bc A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:197:0x03c7 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x03c9 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:199:0x03d7 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:205:0x03fc A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:213:0x042d A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:214:0x0434 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:215:0x0447 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:216:0x0457 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:217:0x0477 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:220:0x04a9 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:221:0x04b8 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:229:0x04dc A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:230:0x04fa A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:234:0x050d A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:235:0x051a A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:242:0x0539 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:247:0x054c A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x055e A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:252:0x0581 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:256:0x05a0 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:263:0x05c5 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:267:0x05dd A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:271:0x05f5 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:278:0x061c A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:282:0x0644 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:290:0x066f A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:301:0x06ab A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:308:0x06e0 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:309:0x06ee A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:313:0x070b A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:314:0x0719 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:315:0x0737 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:316:0x0749 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:317:0x0754 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:318:0x076e A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:322:0x078e A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:329:0x07bd A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:333:0x07db A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:347:0x086f A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:386:0x09b1 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:406:0x0a58 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:410:0x0a6c A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:411:0x0a7c A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:412:0x0a82 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:413:0x0a9f A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:414:0x0abc A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:415:0x0acc A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:416:0x0ade A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:417:0x0b00 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0112  */
    /* JADX WARNING: Removed duplicated region for block: B:440:0x0b7a  */
    /* JADX WARNING: Removed duplicated region for block: B:456:0x0bbf  */
    /* JADX WARNING: Removed duplicated region for block: B:467:0x0bfb  */
    /* JADX WARNING: Removed duplicated region for block: B:485:0x0c52 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:486:0x0c5c A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:487:0x0c66 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:488:0x0c6d A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:489:0x0c77 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:490:0x0c81 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:491:0x0c89 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:492:0x0c91 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:493:0x0c97 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:497:0x0cb5 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:498:0x0cc3 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:507:0x0d06 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:518:0x0d36 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:523:0x0d55 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:527:0x0d6d A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:534:0x0d94 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:544:0x0dca A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:548:0x0de6 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:555:0x0e11 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:556:0x0e21 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:557:0x0e2f A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:558:0x0e45 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:559:0x0e56 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:560:0x0e62 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:561:0x0e78 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:565:0x0e97 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:566:0x0eb4 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:567:0x0ed1 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:575:0x0f18 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:579:0x0f34 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:583:0x0f62 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:587:0x0f78 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:590:0x0f88 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:596:0x0fa9 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:600:0x0fd6 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:601:0x0fda A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:602:0x0fde A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:603:0x0fe2 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:604:0x0fe6 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:605:0x0fea A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:606:0x0fee A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:607:0x1004 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:608:0x101a A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:609:0x1030 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:610:0x1035 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:611:0x103a A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:612:0x103f A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:613:0x1044 A[Catch:{ Throwable -> 0x00e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:618:0x105c  */
    /* JADX WARNING: Removed duplicated region for block: B:625:0x1076  */
    /* JADX WARNING: Removed duplicated region for block: B:632:0x1090  */
    /* JADX WARNING: Removed duplicated region for block: B:635:0x10a4  */
    /* JADX WARNING: Removed duplicated region for block: B:692:0x1162  */
    /* JADX WARNING: Removed duplicated region for block: B:694:0x1170  */
    /* JADX WARNING: Removed duplicated region for block: B:698:0x1177  */
    /* JADX WARNING: Removed duplicated region for block: B:701:0x1185  */
    /* JADX WARNING: Removed duplicated region for block: B:717:0x11c7  */
    /* JADX WARNING: Removed duplicated region for block: B:731:0x11fe  */
    /* JADX WARNING: Removed duplicated region for block: B:735:0x00ba A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:736:0x00f3 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:755:0x0145 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:756:0x01e1 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:757:0x0209 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:758:0x0234 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:759:0x0491 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:760:0x04a1 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:761:0x0f98 A[SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Object m39136a(org.mozilla1.javascript.Context r36, Interpreter.C3604a r37, java.lang.Object r38) {
        /*
            a.bd r26 = p001a.C2054bd.f5856nd
            java.lang.Object r27 = org.mozilla.javascript.C0321EM.instance
            r0 = r36
            int r4 = r0.elJ
            if (r4 == 0) goto L_0x00fe
            r4 = 1
            r16 = r4
        L_0x000d:
            r22 = 0
            r24 = -1
            r0 = r36
            java.lang.Object r4 = r0.elG
            if (r4 == 0) goto L_0x0031
            r0 = r36
            a.aCx r4 = r0.elH
            if (r4 != 0) goto L_0x0026
            a.aCx r4 = new a.aCx
            r4.<init>()
            r0 = r36
            r0.elH = r4
        L_0x0026:
            r0 = r36
            a.aCx r4 = r0.elH
            r0 = r36
            java.lang.Object r5 = r0.elG
            r4.push(r5)
        L_0x0031:
            r4 = 0
            if (r38 == 0) goto L_0x010c
            r0 = r38
            boolean r5 = r0 instanceof Interpreter.C3606c
            if (r5 == 0) goto L_0x0103
            a.sq$c r38 = (Interpreter.C3606c) r38
            java.lang.Object[] r4 = p001a.C0903NH.emptyArgs
            r5 = 1
            r0 = r36
            r1 = r37
            m39142a((p001a.C2909lh) r0, (Interpreter.C3604a) r1, (java.lang.Object[]) r4, (boolean) r5)
            r4 = 0
            r17 = r38
            r5 = r4
        L_0x004a:
            r19 = 0
            r20 = 0
            r12 = r22
            r18 = r5
            r13 = r37
        L_0x0054:
            if (r18 == 0) goto L_0x0112
            r0 = r36
            r1 = r18
            r2 = r24
            r3 = r16
            a.sq$a r5 = m39130a((p001a.C2909lh) r0, (java.lang.Object) r1, (Interpreter.C3604a) r13, (int) r2, (boolean) r3)     // Catch:{ Throwable -> 0x119e }
            java.lang.Object r0 = r5.bgQ     // Catch:{ Throwable -> 0x11a6 }
            r18 = r0
            r4 = 0
            r5.bgQ = r4     // Catch:{ Throwable -> 0x11a6 }
            r23 = r5
        L_0x006b:
            r0 = r23
            java.lang.Object[] r0 = r0.bgA     // Catch:{ Throwable -> 0x11af }
            r28 = r0
            r0 = r23
            double[] r0 = r0.bgC     // Catch:{ Throwable -> 0x11af }
            r29 = r0
            r0 = r23
            a.sq$a r4 = r0.bgD     // Catch:{ Throwable -> 0x11af }
            java.lang.Object[] r0 = r4.bgA     // Catch:{ Throwable -> 0x11af }
            r30 = r0
            r0 = r23
            a.sq$a r4 = r0.bgD     // Catch:{ Throwable -> 0x11af }
            double[] r0 = r4.bgC     // Catch:{ Throwable -> 0x11af }
            r31 = r0
            r0 = r23
            a.sq$a r4 = r0.bgD     // Catch:{ Throwable -> 0x11af }
            int[] r0 = r4.bgB     // Catch:{ Throwable -> 0x11af }
            r32 = r0
            r0 = r23
            a.aeQ r4 = r0.bgz     // Catch:{ Throwable -> 0x11af }
            byte[] r0 = r4.frr     // Catch:{ Throwable -> 0x11af }
            r33 = r0
            r0 = r23
            a.aeQ r4 = r0.bgz     // Catch:{ Throwable -> 0x11af }
            java.lang.String[] r0 = r4.frn     // Catch:{ Throwable -> 0x11af }
            r34 = r0
            r0 = r23
            int r6 = r0.bgO     // Catch:{ Throwable -> 0x11af }
            r0 = r23
            r1 = r36
            r1.elG = r0     // Catch:{ Throwable -> 0x11af }
            r22 = r12
        L_0x00ab:
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r5 = r4 + 1
            r0 = r23
            r0.f9158pc = r5     // Catch:{ Throwable -> 0x00e8 }
            byte r35 = r33[r4]     // Catch:{ Throwable -> 0x00e8 }
            switch(r35) {
                case -64: goto L_0x0f98;
                case -63: goto L_0x01e1;
                case -62: goto L_0x011f;
                case -61: goto L_0x0b00;
                case -60: goto L_0x00ba;
                case -59: goto L_0x0581;
                case -58: goto L_0x0eb4;
                case -57: goto L_0x0e97;
                case -56: goto L_0x0749;
                case -55: goto L_0x086f;
                case -54: goto L_0x0f34;
                case -53: goto L_0x0f18;
                case -52: goto L_0x0c5c;
                case -51: goto L_0x0c52;
                case -50: goto L_0x0c91;
                case -49: goto L_0x0b7a;
                case -48: goto L_0x0bbf;
                case -47: goto L_0x1076;
                case -46: goto L_0x105c;
                case -45: goto L_0x1044;
                case -44: goto L_0x103f;
                case -43: goto L_0x103a;
                case -42: goto L_0x1035;
                case -41: goto L_0x1030;
                case -40: goto L_0x101a;
                case -39: goto L_0x1004;
                case -38: goto L_0x0fee;
                case -37: goto L_0x0fea;
                case -36: goto L_0x0fe6;
                case -35: goto L_0x0fe2;
                case -34: goto L_0x0fde;
                case -33: goto L_0x0fda;
                case -32: goto L_0x0fd6;
                case -31: goto L_0x0ed1;
                case -30: goto L_0x0e78;
                case -29: goto L_0x0e62;
                case -28: goto L_0x0a9f;
                case -27: goto L_0x0a82;
                case -26: goto L_0x0fa9;
                case -25: goto L_0x03fc;
                case -24: goto L_0x03d7;
                case -23: goto L_0x03c9;
                case -22: goto L_0x04a1;
                case -21: goto L_0x07db;
                case -20: goto L_0x0e45;
                case -19: goto L_0x0e2f;
                case -18: goto L_0x07bd;
                case -17: goto L_0x078e;
                case -16: goto L_0x076e;
                case -15: goto L_0x0754;
                case -14: goto L_0x0a6c;
                case -13: goto L_0x0e21;
                case -12: goto L_0x0e11;
                case -11: goto L_0x0719;
                case -10: goto L_0x06ab;
                case -9: goto L_0x061c;
                case -8: goto L_0x0ade;
                case -7: goto L_0x0bfb;
                case -6: goto L_0x037d;
                case -5: goto L_0x0434;
                case -4: goto L_0x042d;
                case -3: goto L_0x0477;
                case -2: goto L_0x0457;
                case -1: goto L_0x0447;
                case 0: goto L_0x00ba;
                case 1: goto L_0x00ba;
                case 2: goto L_0x0c97;
                case 3: goto L_0x0cb5;
                case 4: goto L_0x0491;
                case 5: goto L_0x03c7;
                case 6: goto L_0x0366;
                case 7: goto L_0x034f;
                case 8: goto L_0x055e;
                case 9: goto L_0x04b8;
                case 10: goto L_0x04b8;
                case 11: goto L_0x04b8;
                case 12: goto L_0x02e7;
                case 13: goto L_0x02e7;
                case 14: goto L_0x0244;
                case 15: goto L_0x0244;
                case 16: goto L_0x0244;
                case 17: goto L_0x0244;
                case 18: goto L_0x04b8;
                case 19: goto L_0x04b8;
                case 20: goto L_0x04dc;
                case 21: goto L_0x050d;
                case 22: goto L_0x051a;
                case 23: goto L_0x051a;
                case 24: goto L_0x051a;
                case 25: goto L_0x051a;
                case 26: goto L_0x0539;
                case 27: goto L_0x04a9;
                case 28: goto L_0x04fa;
                case 29: goto L_0x04fa;
                case 30: goto L_0x09b1;
                case 31: goto L_0x05a0;
                case 32: goto L_0x0a58;
                case 33: goto L_0x05dd;
                case 34: goto L_0x05c5;
                case 35: goto L_0x05f5;
                case 36: goto L_0x0644;
                case 37: goto L_0x066f;
                case 38: goto L_0x086f;
                case 39: goto L_0x0acc;
                case 40: goto L_0x0abc;
                case 41: goto L_0x0a7c;
                case 42: goto L_0x0c66;
                case 43: goto L_0x0c6d;
                case 44: goto L_0x0c81;
                case 45: goto L_0x0c89;
                case 46: goto L_0x0333;
                case 47: goto L_0x0333;
                case 48: goto L_0x0e56;
                case 49: goto L_0x054c;
                case 50: goto L_0x0209;
                case 51: goto L_0x0234;
                case 52: goto L_0x02b1;
                case 53: goto L_0x02b1;
                case 54: goto L_0x0737;
                case 55: goto L_0x11fe;
                case 56: goto L_0x0b86;
                case 57: goto L_0x0cc3;
                case 58: goto L_0x0d06;
                case 59: goto L_0x0d06;
                case 60: goto L_0x0d06;
                case 61: goto L_0x0d36;
                case 62: goto L_0x0d36;
                case 63: goto L_0x0c77;
                case 64: goto L_0x0145;
                case 65: goto L_0x0ed1;
                case 66: goto L_0x0ed1;
                case 67: goto L_0x06e0;
                case 68: goto L_0x06ee;
                case 69: goto L_0x070b;
                case 70: goto L_0x086f;
                case 71: goto L_0x0d55;
                case 72: goto L_0x0173;
                case 73: goto L_0x0f62;
                case 74: goto L_0x0f78;
                case 75: goto L_0x0f88;
                case 76: goto L_0x0d6d;
                case 77: goto L_0x0d94;
                case 78: goto L_0x0dca;
                case 79: goto L_0x0de6;
                case 80: goto L_0x00ba;
                case 81: goto L_0x00ba;
                case 82: goto L_0x00ba;
                case 83: goto L_0x00ba;
                case 84: goto L_0x00ba;
                case 85: goto L_0x00ba;
                case 86: goto L_0x00ba;
                case 87: goto L_0x00ba;
                case 88: goto L_0x00ba;
                case 89: goto L_0x00ba;
                case 90: goto L_0x00ba;
                case 91: goto L_0x00ba;
                case 92: goto L_0x00ba;
                case 93: goto L_0x00ba;
                case 94: goto L_0x00ba;
                case 95: goto L_0x00ba;
                case 96: goto L_0x00ba;
                case 97: goto L_0x00ba;
                case 98: goto L_0x00ba;
                case 99: goto L_0x00ba;
                case 100: goto L_0x00ba;
                case 101: goto L_0x00ba;
                case 102: goto L_0x00ba;
                case 103: goto L_0x00ba;
                case 104: goto L_0x00ba;
                case 105: goto L_0x00ba;
                case 106: goto L_0x00ba;
                case 107: goto L_0x00ba;
                case 108: goto L_0x00ba;
                case 109: goto L_0x00ba;
                case 110: goto L_0x00ba;
                case 111: goto L_0x00ba;
                case 112: goto L_0x00ba;
                case 113: goto L_0x00ba;
                case 114: goto L_0x00ba;
                case 115: goto L_0x00ba;
                case 116: goto L_0x00ba;
                case 117: goto L_0x00ba;
                case 118: goto L_0x00ba;
                case 119: goto L_0x00ba;
                case 120: goto L_0x00ba;
                case 121: goto L_0x00ba;
                case 122: goto L_0x00ba;
                case 123: goto L_0x00ba;
                case 124: goto L_0x00ba;
                case 125: goto L_0x00ba;
                case 126: goto L_0x00ba;
                case 127: goto L_0x00ba;
                case 128: goto L_0x00ba;
                case 129: goto L_0x00ba;
                case 130: goto L_0x00ba;
                case 131: goto L_0x00ba;
                case 132: goto L_0x00ba;
                case 133: goto L_0x00ba;
                case 134: goto L_0x00ba;
                case 135: goto L_0x00ba;
                case 136: goto L_0x00ba;
                case 137: goto L_0x00ba;
                case 138: goto L_0x00ba;
                case 139: goto L_0x00ba;
                case 140: goto L_0x00ba;
                case 141: goto L_0x00ba;
                case 142: goto L_0x00ba;
                case 143: goto L_0x00ba;
                case 144: goto L_0x00ba;
                case 145: goto L_0x00ba;
                case 146: goto L_0x00ba;
                case 147: goto L_0x00ba;
                case 148: goto L_0x00ba;
                case 149: goto L_0x00ba;
                case 150: goto L_0x00ba;
                case 151: goto L_0x00ba;
                case 152: goto L_0x00ba;
                case 153: goto L_0x00ba;
                case 154: goto L_0x00ba;
                case 155: goto L_0x0b0c;
                default: goto L_0x00ba;
            }     // Catch:{ Throwable -> 0x00e8 }
        L_0x00ba:
            r0 = r23
            a.aeQ r4 = r0.bgz     // Catch:{ Throwable -> 0x00e8 }
            m39139a((InterpreterData) r4)     // Catch:{ Throwable -> 0x00e8 }
            java.lang.RuntimeException r4 = new java.lang.RuntimeException     // Catch:{ Throwable -> 0x00e8 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00e8 }
            java.lang.String r6 = "Unknown icode : "
            r5.<init>(r6)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r35
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Throwable -> 0x00e8 }
            java.lang.String r6 = " @ pc : "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r6 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r6 = r6 + -1
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Throwable -> 0x00e8 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x00e8 }
            r4.<init>(r5)     // Catch:{ Throwable -> 0x00e8 }
            throw r4     // Catch:{ Throwable -> 0x00e8 }
        L_0x00e8:
            r4 = move-exception
            r6 = r20
            r10 = r19
            r11 = r22
            r13 = r23
        L_0x00f1:
            if (r18 == 0) goto L_0x10a4
            java.io.PrintStream r5 = java.lang.System.err
            r4.printStackTrace(r5)
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException
            r4.<init>()
            throw r4
        L_0x00fe:
            r4 = 0
            r16 = r4
            goto L_0x000d
        L_0x0103:
            r0 = r38
            boolean r5 = r0 instanceof Interpreter.C3605b
            if (r5 != 0) goto L_0x010c
            p001a.C1520WN.codeBug()
        L_0x010c:
            r17 = r4
            r5 = r38
            goto L_0x004a
        L_0x0112:
            if (r17 != 0) goto L_0x011b
            boolean r4 = r13.bgx     // Catch:{ Throwable -> 0x119e }
            if (r4 == 0) goto L_0x011b
            p001a.C1520WN.codeBug()     // Catch:{ Throwable -> 0x119e }
        L_0x011b:
            r23 = r13
            goto L_0x006b
        L_0x011f:
            r0 = r23
            boolean r4 = r0.bgx     // Catch:{ Throwable -> 0x00e8 }
            if (r4 != 0) goto L_0x0173
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + -1
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x00e8 }
            a.sq$a r4 = m39131a((Interpreter.C3604a) r23)     // Catch:{ Throwable -> 0x00e8 }
            r5 = 1
            r4.bgx = r5     // Catch:{ Throwable -> 0x00e8 }
            a.mt r5 = new a.mt     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aVF r6 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            a.afO r7 = r4.bgy     // Catch:{ Throwable -> 0x00e8 }
            r5.<init>(r6, r7, r4)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            r0.bgK = r5     // Catch:{ Throwable -> 0x00e8 }
        L_0x0145:
            r4 = 0
            r0 = r36
            r1 = r23
            m39154b(r0, r1, r4)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            java.lang.Object r5 = r0.bgK     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            double r6 = r0.bgL     // Catch:{ Throwable -> 0x11b9 }
            r0 = r23
            a.sq$a r4 = r0.bgv     // Catch:{ Throwable -> 0x118f }
            if (r4 == 0) goto L_0x1205
            r0 = r23
            a.sq$a r8 = r0.bgv     // Catch:{ Throwable -> 0x118f }
            boolean r4 = r8.bgx     // Catch:{ Throwable -> 0x1197 }
            if (r4 == 0) goto L_0x0167
            a.sq$a r8 = r8.abD()     // Catch:{ Throwable -> 0x1197 }
        L_0x0167:
            m39144a((Interpreter.C3604a) r8, (java.lang.Object) r5, (double) r6)     // Catch:{ Throwable -> 0x1197 }
            r19 = 0
            r20 = r6
            r12 = r22
            r13 = r8
            goto L_0x0054
        L_0x0173:
            r0 = r23
            boolean r4 = r0.bgx     // Catch:{ Throwable -> 0x00e8 }
            if (r4 != 0) goto L_0x0184
            r0 = r36
            r1 = r23
            r2 = r17
            java.lang.Object r5 = m39135a((p001a.C2909lh) r0, (Interpreter.C3604a) r1, (int) r6, (Interpreter.C3606c) r2)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0183:
            return r5
        L_0x0184:
            r0 = r23
            r1 = r17
            r2 = r35
            java.lang.Object r5 = m39137a((Interpreter.C3604a) r0, (int) r6, (Interpreter.C3606c) r1, (int) r2)     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Object r4 = p001a.aVF.NOT_FOUND     // Catch:{ Throwable -> 0x00e8 }
            if (r5 == r4) goto L_0x00ab
            r8 = r20
            r10 = r19
            r13 = r23
        L_0x0198:
            if (r5 != 0) goto L_0x019d
            p001a.C1520WN.codeBug()
        L_0x019d:
            r6 = 0
            if (r17 == 0) goto L_0x10aa
            r0 = r17
            int r4 = r0.operation
            r7 = 2
            if (r4 != r7) goto L_0x10aa
            r0 = r17
            java.lang.Object r4 = r0.value
            if (r5 != r4) goto L_0x10aa
            r4 = 1
            r7 = r4
        L_0x01af:
            if (r16 == 0) goto L_0x01b8
            r4 = 100
            r0 = r36
            m39141a((p001a.C2909lh) r0, (Interpreter.C3604a) r13, (int) r4)     // Catch:{ RuntimeException -> 0x1105, Error -> 0x110a }
        L_0x01b8:
            a.Tp r4 = r13.bgG
            if (r4 == 0) goto L_0x1202
            boolean r4 = r5 instanceof java.lang.RuntimeException
            if (r4 == 0) goto L_0x1202
            r4 = r5
            java.lang.RuntimeException r4 = (java.lang.RuntimeException) r4
            a.Tp r11 = r13.bgG     // Catch:{ Throwable -> 0x1110 }
            r0 = r36
            r11.onExceptionThrown(r0, r4)     // Catch:{ Throwable -> 0x1110 }
            r11 = r5
        L_0x01cb:
            if (r7 == 0) goto L_0x1119
            r4 = 2
            if (r7 == r4) goto L_0x1116
            r4 = 1
        L_0x01d1:
            int r24 = m39127a((Interpreter.C3604a) r13, (boolean) r4)
            if (r24 < 0) goto L_0x1119
            r20 = r8
            r19 = r10
            r12 = r22
            r18 = r11
            goto L_0x0054
        L_0x01e1:
            r4 = 1
            r0 = r23
            r0.bgx = r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            r0 = r33
            int r4 = m39159d(r0, r4)     // Catch:{ Throwable -> 0x00e8 }
            a.dx r5 = new a.dx     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aVF r6 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Object r6 = NativeIterator.m38643o(r6)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aeQ r7 = r0.bgz     // Catch:{ Throwable -> 0x00e8 }
            java.lang.String r7 = r7.frm     // Catch:{ Throwable -> 0x00e8 }
            r5.<init>(r6, r7, r4)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r17
            r0.gqP = r5     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x0145
        L_0x0209:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x0215
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0215:
            int r5 = r6 + -1
            r0 = r23
            int r5 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            r0 = r33
            int r6 = m39159d(r0, r5)     // Catch:{ Throwable -> 0x00e8 }
            a.dx r5 = new a.dx     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aeQ r7 = r0.bgz     // Catch:{ Throwable -> 0x00e8 }
            java.lang.String r7 = r7.frm     // Catch:{ Throwable -> 0x00e8 }
            r5.<init>(r4, r7, r6)     // Catch:{ Throwable -> 0x00e8 }
            r8 = r20
            r10 = r19
            r13 = r23
            goto L_0x0198
        L_0x0234:
            r0 = r23
            int r4 = r0.bgE     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + r24
            r5 = r28[r4]     // Catch:{ Throwable -> 0x00e8 }
            r8 = r20
            r10 = r19
            r13 = r23
            goto L_0x0198
        L_0x0244:
            int r8 = r6 + -1
            int r4 = r8 + 1
            r4 = r28[r4]     // Catch:{ Throwable -> 0x00e8 }
            r5 = r28[r8]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x0262
            int r4 = r8 + 1
            r6 = r29[r4]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            double r4 = m39152b(r0, r8)     // Catch:{ Throwable -> 0x00e8 }
        L_0x025a:
            switch(r35) {
                case 14: goto L_0x028d;
                case 15: goto L_0x027d;
                case 16: goto L_0x0285;
                case 17: goto L_0x026d;
                default: goto L_0x025d;
            }     // Catch:{ Throwable -> 0x00e8 }
        L_0x025d:
            java.lang.RuntimeException r4 = p001a.C1520WN.codeBug()     // Catch:{ Throwable -> 0x00e8 }
            throw r4     // Catch:{ Throwable -> 0x00e8 }
        L_0x0262:
            r0 = r26
            if (r5 != r0) goto L_0x0295
            double r6 = p001a.C0903NH.toNumber((java.lang.Object) r4)     // Catch:{ Throwable -> 0x00e8 }
            r4 = r29[r8]     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x025a
        L_0x026d:
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 < 0) goto L_0x027b
            r4 = 1
        L_0x0272:
            java.lang.Boolean r4 = p001a.C0903NH.wrapBoolean(r4)     // Catch:{ Throwable -> 0x00e8 }
            r28[r8] = r4     // Catch:{ Throwable -> 0x00e8 }
            r6 = r8
            goto L_0x00ab
        L_0x027b:
            r4 = 0
            goto L_0x0272
        L_0x027d:
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 > 0) goto L_0x0283
            r4 = 1
            goto L_0x0272
        L_0x0283:
            r4 = 0
            goto L_0x0272
        L_0x0285:
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 <= 0) goto L_0x028b
            r4 = 1
            goto L_0x0272
        L_0x028b:
            r4 = 0
            goto L_0x0272
        L_0x028d:
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 >= 0) goto L_0x0293
            r4 = 1
            goto L_0x0272
        L_0x0293:
            r4 = 0
            goto L_0x0272
        L_0x0295:
            switch(r35) {
                case 14: goto L_0x02ac;
                case 15: goto L_0x02a2;
                case 16: goto L_0x02a7;
                case 17: goto L_0x029d;
                default: goto L_0x0298;
            }     // Catch:{ Throwable -> 0x00e8 }
        L_0x0298:
            java.lang.RuntimeException r4 = p001a.C1520WN.codeBug()     // Catch:{ Throwable -> 0x00e8 }
            throw r4     // Catch:{ Throwable -> 0x00e8 }
        L_0x029d:
            boolean r4 = p001a.C0903NH.cmp_LE(r4, r5)     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x0272
        L_0x02a2:
            boolean r4 = p001a.C0903NH.cmp_LE(r5, r4)     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x0272
        L_0x02a7:
            boolean r4 = p001a.C0903NH.cmp_LT(r4, r5)     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x0272
        L_0x02ac:
            boolean r4 = p001a.C0903NH.cmp_LT(r5, r4)     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x0272
        L_0x02b1:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x11fb
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4
        L_0x02be:
            int r6 = r6 + -1
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x02cc
            r8 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r8)     // Catch:{ Throwable -> 0x00e8 }
        L_0x02cc:
            r7 = 52
            r0 = r35
            if (r0 != r7) goto L_0x02e0
            r0 = r36
            boolean r4 = p001a.C0903NH.m7581f((java.lang.Object) r4, (java.lang.Object) r5, (p001a.C2909lh) r0)     // Catch:{ Throwable -> 0x00e8 }
        L_0x02d8:
            java.lang.Boolean r4 = p001a.C0903NH.wrapBoolean(r4)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x02e0:
            r0 = r36
            boolean r4 = p001a.C0903NH.m7575e((java.lang.Object) r4, (java.lang.Object) r5, (p001a.C2909lh) r0)     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x02d8
        L_0x02e7:
            int r6 = r6 + -1
            int r4 = r6 + 1
            r4 = r28[r4]     // Catch:{ Throwable -> 0x00e8 }
            r5 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x031f
            r0 = r26
            if (r5 != r0) goto L_0x0315
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            int r7 = r6 + 1
            r8 = r29[r7]     // Catch:{ Throwable -> 0x00e8 }
            int r4 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r4 != 0) goto L_0x0313
            r4 = 1
        L_0x0302:
            r5 = r4
        L_0x0303:
            r4 = 13
            r0 = r35
            if (r0 != r4) goto L_0x0331
            r4 = 1
        L_0x030a:
            r4 = r4 ^ r5
            java.lang.Boolean r4 = p001a.C0903NH.wrapBoolean(r4)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0313:
            r4 = 0
            goto L_0x0302
        L_0x0315:
            int r4 = r6 + 1
            r8 = r29[r4]     // Catch:{ Throwable -> 0x00e8 }
            boolean r4 = p001a.C0903NH.m7551b((double) r8, (java.lang.Object) r5)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4
            goto L_0x0303
        L_0x031f:
            r0 = r26
            if (r5 != r0) goto L_0x032b
            r8 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            boolean r4 = p001a.C0903NH.m7551b((double) r8, (java.lang.Object) r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4
            goto L_0x0303
        L_0x032b:
            boolean r4 = p001a.C0903NH.m7576eq(r5, r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4
            goto L_0x0303
        L_0x0331:
            r4 = 0
            goto L_0x030a
        L_0x0333:
            int r6 = r6 + -1
            r0 = r28
            r1 = r29
            boolean r5 = m39146a((java.lang.Object[]) r0, (double[]) r1, (int) r6)     // Catch:{ Throwable -> 0x00e8 }
            r4 = 47
            r0 = r35
            if (r0 != r4) goto L_0x034d
            r4 = 1
        L_0x0344:
            r4 = r4 ^ r5
            java.lang.Boolean r4 = p001a.C0903NH.wrapBoolean(r4)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x034d:
            r4 = 0
            goto L_0x0344
        L_0x034f:
            int r4 = r6 + -1
            r0 = r23
            boolean r5 = m39158c(r0, r6)     // Catch:{ Throwable -> 0x00e8 }
            if (r5 == 0) goto L_0x0399
            r0 = r23
            int r5 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r5 = r5 + 2
            r0 = r23
            r0.f9158pc = r5     // Catch:{ Throwable -> 0x00e8 }
            r6 = r4
            goto L_0x00ab
        L_0x0366:
            int r4 = r6 + -1
            r0 = r23
            boolean r5 = m39158c(r0, r6)     // Catch:{ Throwable -> 0x00e8 }
            if (r5 != 0) goto L_0x0399
            r0 = r23
            int r5 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r5 = r5 + 2
            r0 = r23
            r0.f9158pc = r5     // Catch:{ Throwable -> 0x00e8 }
            r6 = r4
            goto L_0x00ab
        L_0x037d:
            int r5 = r6 + -1
            r0 = r23
            boolean r4 = m39158c(r0, r6)     // Catch:{ Throwable -> 0x00e8 }
            if (r4 != 0) goto L_0x0394
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 2
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x00e8 }
            r6 = r5
            goto L_0x00ab
        L_0x0394:
            int r4 = r5 + -1
            r6 = 0
            r28[r5] = r6     // Catch:{ Throwable -> 0x00e8 }
        L_0x0399:
            if (r16 == 0) goto L_0x03a3
            r5 = 2
            r0 = r36
            r1 = r23
            m39141a((p001a.C2909lh) r0, (Interpreter.C3604a) r1, (int) r5)     // Catch:{ Throwable -> 0x00e8 }
        L_0x03a3:
            r0 = r23
            int r5 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            r0 = r33
            int r5 = getShort(r0, r5)     // Catch:{ Throwable -> 0x00e8 }
            if (r5 == 0) goto L_0x1090
            r0 = r23
            int r6 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r5 = r5 + -1
            int r5 = r5 + r6
            r0 = r23
            r0.f9158pc = r5     // Catch:{ Throwable -> 0x00e8 }
        L_0x03ba:
            if (r16 == 0) goto L_0x11c7
            r0 = r23
            int r5 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            r0.bgM = r5     // Catch:{ Throwable -> 0x00e8 }
            r6 = r4
            goto L_0x00ab
        L_0x03c7:
            r4 = r6
            goto L_0x0399
        L_0x03c9:
            int r4 = r6 + 1
            r28[r4] = r26     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r5 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r5 = r5 + 2
            double r6 = (double) r5     // Catch:{ Throwable -> 0x00e8 }
            r29[r4] = r6     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x0399
        L_0x03d7:
            r0 = r23
            int r4 = r0.bgF     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 1
            if (r6 != r4) goto L_0x03f1
            r0 = r23
            int r4 = r0.bgE     // Catch:{ Throwable -> 0x00e8 }
            int r24 = r24 + r4
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r28[r24] = r4     // Catch:{ Throwable -> 0x00e8 }
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            r29[r24] = r4     // Catch:{ Throwable -> 0x00e8 }
            int r6 = r6 + -1
            goto L_0x00ab
        L_0x03f1:
            r0 = r23
            int r4 = r0.bgF     // Catch:{ Throwable -> 0x00e8 }
            if (r6 == r4) goto L_0x00ab
            p001a.C1520WN.codeBug()     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x03fc:
            if (r16 == 0) goto L_0x0406
            r4 = 0
            r0 = r36
            r1 = r23
            m39141a((p001a.C2909lh) r0, (Interpreter.C3604a) r1, (int) r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0406:
            r0 = r23
            int r4 = r0.bgE     // Catch:{ Throwable -> 0x00e8 }
            int r24 = r24 + r4
            r5 = r28[r24]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r5 == r0) goto L_0x041a
            r8 = r20
            r10 = r19
            r13 = r23
            goto L_0x0198
        L_0x041a:
            r4 = r29[r24]     // Catch:{ Throwable -> 0x00e8 }
            int r4 = (int) r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x00e8 }
            if (r16 == 0) goto L_0x00ab
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            r0.bgM = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x042d:
            r4 = 0
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            int r6 = r6 + -1
            goto L_0x00ab
        L_0x0434:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            r0.bgK = r4     // Catch:{ Throwable -> 0x00e8 }
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            r0.bgL = r4     // Catch:{ Throwable -> 0x00e8 }
            r4 = 0
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            int r6 = r6 + -1
            goto L_0x00ab
        L_0x0447:
            int r4 = r6 + 1
            r5 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r28[r4] = r5     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r6 + 1
            r8 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            r29[r4] = r8     // Catch:{ Throwable -> 0x00e8 }
            int r6 = r6 + 1
            goto L_0x00ab
        L_0x0457:
            int r4 = r6 + 1
            int r5 = r6 + -1
            r5 = r28[r5]     // Catch:{ Throwable -> 0x00e8 }
            r28[r4] = r5     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r6 + 1
            int r5 = r6 + -1
            r8 = r29[r5]     // Catch:{ Throwable -> 0x00e8 }
            r29[r4] = r8     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r6 + 2
            r5 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r28[r4] = r5     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r6 + 2
            r8 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            r29[r4] = r8     // Catch:{ Throwable -> 0x00e8 }
            int r6 = r6 + 2
            goto L_0x00ab
        L_0x0477:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            int r5 = r6 + -1
            r5 = r28[r5]     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r5     // Catch:{ Throwable -> 0x00e8 }
            int r5 = r6 + -1
            r28[r5] = r4     // Catch:{ Throwable -> 0x00e8 }
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            int r7 = r6 + -1
            r8 = r29[r7]     // Catch:{ Throwable -> 0x00e8 }
            r29[r6] = r8     // Catch:{ Throwable -> 0x00e8 }
            int r7 = r6 + -1
            r29[r7] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0491:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            r0.bgK = r4     // Catch:{ Throwable -> 0x00e8 }
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            r0.bgL = r4     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r6 + -1
            goto L_0x0145
        L_0x04a1:
            r0 = r27
            r1 = r23
            r1.bgK = r0     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x0145
        L_0x04a9:
            r0 = r23
            int r4 = m39126a((Interpreter.C3604a) r0, (int) r6)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r26     // Catch:{ Throwable -> 0x00e8 }
            r4 = r4 ^ -1
            double r4 = (double) r4     // Catch:{ Throwable -> 0x00e8 }
            r29[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x04b8:
            int r4 = r6 + -1
            r0 = r23
            int r4 = m39126a((Interpreter.C3604a) r0, (int) r4)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r5 = m39126a((Interpreter.C3604a) r0, (int) r6)     // Catch:{ Throwable -> 0x00e8 }
            int r6 = r6 + -1
            r28[r6] = r26     // Catch:{ Throwable -> 0x00e8 }
            switch(r35) {
                case 9: goto L_0x04d4;
                case 10: goto L_0x04d6;
                case 11: goto L_0x04d2;
                case 12: goto L_0x04cd;
                case 13: goto L_0x04cd;
                case 14: goto L_0x04cd;
                case 15: goto L_0x04cd;
                case 16: goto L_0x04cd;
                case 17: goto L_0x04cd;
                case 18: goto L_0x04d8;
                case 19: goto L_0x04da;
                default: goto L_0x04cd;
            }     // Catch:{ Throwable -> 0x00e8 }
        L_0x04cd:
            double r4 = (double) r4     // Catch:{ Throwable -> 0x00e8 }
            r29[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x04d2:
            r4 = r4 & r5
            goto L_0x04cd
        L_0x04d4:
            r4 = r4 | r5
            goto L_0x04cd
        L_0x04d6:
            r4 = r4 ^ r5
            goto L_0x04cd
        L_0x04d8:
            int r4 = r4 << r5
            goto L_0x04cd
        L_0x04da:
            int r4 = r4 >> r5
            goto L_0x04cd
        L_0x04dc:
            int r4 = r6 + -1
            r0 = r23
            double r4 = m39152b(r0, r4)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r7 = m39126a((Interpreter.C3604a) r0, (int) r6)     // Catch:{ Throwable -> 0x00e8 }
            r7 = r7 & 31
            int r6 = r6 + -1
            r28[r6] = r26     // Catch:{ Throwable -> 0x00e8 }
            long r4 = p001a.C0903NH.toUint32((double) r4)     // Catch:{ Throwable -> 0x00e8 }
            long r4 = r4 >>> r7
            double r4 = (double) r4     // Catch:{ Throwable -> 0x00e8 }
            r29[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x04fa:
            r0 = r23
            double r4 = m39152b(r0, r6)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r26     // Catch:{ Throwable -> 0x00e8 }
            r7 = 29
            r0 = r35
            if (r0 != r7) goto L_0x0509
            double r4 = -r4
        L_0x0509:
            r29[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x050d:
            int r6 = r6 + -1
            r0 = r28
            r1 = r29
            r2 = r36
            m39145a((java.lang.Object[]) r0, (double[]) r1, (int) r6, (p001a.C2909lh) r2)     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x051a:
            r0 = r23
            double r8 = m39152b(r0, r6)     // Catch:{ Throwable -> 0x00e8 }
            int r6 = r6 + -1
            r0 = r23
            double r4 = m39152b(r0, r6)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r26     // Catch:{ Throwable -> 0x00e8 }
            switch(r35) {
                case 22: goto L_0x0531;
                case 23: goto L_0x0533;
                case 24: goto L_0x0535;
                case 25: goto L_0x0537;
                default: goto L_0x052d;
            }     // Catch:{ Throwable -> 0x00e8 }
        L_0x052d:
            r29[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0531:
            double r4 = r4 - r8
            goto L_0x052d
        L_0x0533:
            double r4 = r4 * r8
            goto L_0x052d
        L_0x0535:
            double r4 = r4 / r8
            goto L_0x052d
        L_0x0537:
            double r4 = r4 % r8
            goto L_0x052d
        L_0x0539:
            r0 = r23
            boolean r4 = m39158c(r0, r6)     // Catch:{ Throwable -> 0x00e8 }
            if (r4 == 0) goto L_0x054a
            r4 = 0
        L_0x0542:
            java.lang.Boolean r4 = p001a.C0903NH.wrapBoolean(r4)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x054a:
            r4 = 1
            goto L_0x0542
        L_0x054c:
            int r6 = r6 + 1
            r0 = r23
            a.aVF r4 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            r1 = r22
            a.aVF r4 = p001a.C0903NH.m7567d((p001a.C2909lh) r0, (p001a.aVF) r4, (java.lang.String) r1)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x055e:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x11f8
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4
        L_0x056b:
            int r6 = r6 + -1
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            a.aVF r4 = (p001a.aVF) r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aVF r7 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            r1 = r22
            java.lang.Object r4 = p001a.C0903NH.m7494a((p001a.aVF) r4, (java.lang.Object) r5, (p001a.C2909lh) r0, (p001a.aVF) r7, (java.lang.String) r1)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0581:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x11f5
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4
        L_0x058e:
            int r6 = r6 + -1
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            a.aVF r4 = (p001a.aVF) r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            r1 = r22
            java.lang.Object r4 = p001a.C0903NH.m7495a((p001a.aVF) r4, (java.lang.Object) r5, (p001a.C2909lh) r0, (java.lang.String) r1)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x05a0:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x11f2
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4
        L_0x05ad:
            int r6 = r6 + -1
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x05bb
            r8 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r8)     // Catch:{ Throwable -> 0x00e8 }
        L_0x05bb:
            r0 = r36
            java.lang.Object r4 = p001a.C0903NH.m7545b((java.lang.Object) r4, (java.lang.Object) r5, (p001a.C2909lh) r0)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x05c5:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x05d1
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x05d1:
            r0 = r22
            r1 = r36
            java.lang.Object r4 = p001a.C0903NH.m7546b((java.lang.Object) r4, (java.lang.String) r0, (p001a.C2909lh) r1)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x05dd:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x05e9
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x05e9:
            r0 = r22
            r1 = r36
            java.lang.Object r4 = p001a.C0903NH.m7518a((java.lang.Object) r4, (java.lang.String) r0, (p001a.C2909lh) r1)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x05f5:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x11ef
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4
        L_0x0602:
            int r6 = r6 + -1
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x0610
            r8 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r8)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0610:
            r0 = r22
            r1 = r36
            java.lang.Object r4 = p001a.C0903NH.m7520a((java.lang.Object) r4, (java.lang.String) r0, (java.lang.Object) r5, (p001a.C2909lh) r1)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x061c:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x0628
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0628:
            r0 = r23
            int r5 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            byte r5 = r33[r5]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r22
            r1 = r36
            java.lang.Object r4 = p001a.C0903NH.m7519a((java.lang.Object) r4, (java.lang.String) r0, (p001a.C2909lh) r1, (int) r5)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 1
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0644:
            int r6 = r6 + -1
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x0652
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0652:
            int r5 = r6 + 1
            r5 = r28[r5]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r5 == r0) goto L_0x0664
            r0 = r36
            java.lang.Object r4 = p001a.C0903NH.m7515a((java.lang.Object) r4, (java.lang.Object) r5, (p001a.C2909lh) r0)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0660:
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0664:
            int r5 = r6 + 1
            r8 = r29[r5]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            java.lang.Object r4 = p001a.C0903NH.m7511a((java.lang.Object) r4, (double) r8, (p001a.C2909lh) r0)     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x0660
        L_0x066f:
            int r6 = r6 + -2
            int r4 = r6 + 2
            r4 = r28[r4]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x11ec
            int r4 = r6 + 2
            r4 = r29[r4]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4
        L_0x0682:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x068e
            r8 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r8)     // Catch:{ Throwable -> 0x00e8 }
        L_0x068e:
            int r7 = r6 + 1
            r7 = r28[r7]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r7 == r0) goto L_0x06a0
            r0 = r36
            java.lang.Object r4 = p001a.C0903NH.m7517a((java.lang.Object) r4, (java.lang.Object) r7, (java.lang.Object) r5, (p001a.C2909lh) r0)     // Catch:{ Throwable -> 0x00e8 }
        L_0x069c:
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x06a0:
            int r7 = r6 + 1
            r8 = r29[r7]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            java.lang.Object r4 = p001a.C0903NH.m7512a((java.lang.Object) r4, (double) r8, (java.lang.Object) r5, (p001a.C2909lh) r0)     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x069c
        L_0x06ab:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x11e9
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4
        L_0x06b8:
            int r6 = r6 + -1
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x06c6
            r8 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r8)     // Catch:{ Throwable -> 0x00e8 }
        L_0x06c6:
            r0 = r23
            int r7 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            byte r7 = r33[r7]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            java.lang.Object r4 = p001a.C0903NH.m7516a((java.lang.Object) r4, (java.lang.Object) r5, (p001a.C2909lh) r0, (int) r7)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 1
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x06e0:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            a.bH r4 = (Ref) r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            java.lang.Object r4 = p001a.C0903NH.m7502a((Ref) r4, (p001a.C2909lh) r0)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x06ee:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x11e6
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4
        L_0x06fb:
            int r6 = r6 + -1
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            a.bH r4 = (Ref) r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            java.lang.Object r4 = p001a.C0903NH.m7504a((Ref) r4, (java.lang.Object) r5, (p001a.C2909lh) r0)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x070b:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            a.bH r4 = (Ref) r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            java.lang.Object r4 = p001a.C0903NH.m7542b((Ref) r4, (p001a.C2909lh) r0)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0719:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            a.bH r4 = (Ref) r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r5 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            byte r5 = r33[r5]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            java.lang.Object r4 = p001a.C0903NH.m7503a((Ref) r4, (p001a.C2909lh) r0, (int) r5)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 1
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0737:
            int r6 = r6 + 1
            r0 = r23
            int r4 = r0.bgE     // Catch:{ Throwable -> 0x00e8 }
            int r24 = r24 + r4
            r4 = r28[r24]     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            r4 = r29[r24]     // Catch:{ Throwable -> 0x00e8 }
            r29[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0749:
            r0 = r23
            int r4 = r0.bgE     // Catch:{ Throwable -> 0x00e8 }
            int r24 = r24 + r4
            r4 = 0
            r28[r24] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0754:
            int r4 = r6 + 1
            r0 = r23
            a.aVF r5 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r22
            r1 = r36
            a.mF r5 = p001a.C0903NH.m7541b((java.lang.String) r0, (p001a.C2909lh) r1, (p001a.aVF) r5)     // Catch:{ Throwable -> 0x00e8 }
            r28[r4] = r5     // Catch:{ Throwable -> 0x00e8 }
            int r6 = r4 + 1
            a.aVF r4 = p001a.C0903NH.m7605q((p001a.C2909lh) r36)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x076e:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x077a
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x077a:
            r0 = r22
            r1 = r36
            a.mF r4 = p001a.C0903NH.m7568d((java.lang.Object) r4, (java.lang.String) r0, (p001a.C2909lh) r1)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            int r6 = r6 + 1
            a.aVF r4 = p001a.C0903NH.m7605q((p001a.C2909lh) r36)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x078e:
            int r4 = r6 + -1
            r4 = r28[r4]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x11e3
            int r4 = r6 + -1
            r4 = r29[r4]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4
        L_0x079f:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x07ab
            r8 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r8)     // Catch:{ Throwable -> 0x00e8 }
        L_0x07ab:
            int r7 = r6 + -1
            r0 = r36
            a.mF r4 = p001a.C0903NH.m7557c((java.lang.Object) r5, (java.lang.Object) r4, (p001a.C2909lh) r0)     // Catch:{ Throwable -> 0x00e8 }
            r28[r7] = r4     // Catch:{ Throwable -> 0x00e8 }
            a.aVF r4 = p001a.C0903NH.m7605q((p001a.C2909lh) r36)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x07bd:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x07c9
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x07c9:
            r0 = r36
            a.mF r4 = p001a.C0903NH.m7573e((java.lang.Object) r4, (p001a.C2909lh) r0)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            int r6 = r6 + 1
            a.aVF r4 = p001a.C0903NH.m7605q((p001a.C2909lh) r36)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x07db:
            if (r16 == 0) goto L_0x07e7
            r0 = r36
            int r4 = r0.elI     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 100
            r0 = r36
            r0.elI = r4     // Catch:{ Throwable -> 0x00e8 }
        L_0x07e7:
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            byte r4 = r33[r4]     // Catch:{ Throwable -> 0x00e8 }
            r10 = r4 & 255(0xff, float:3.57E-43)
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 1
            byte r4 = r33[r4]     // Catch:{ Throwable -> 0x00e8 }
            if (r4 == 0) goto L_0x083b
            r4 = 1
        L_0x07fa:
            r0 = r23
            int r5 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r5 = r5 + 2
            r0 = r33
            int r12 = m39159d(r0, r5)     // Catch:{ Throwable -> 0x00e8 }
            if (r4 == 0) goto L_0x083d
            int r4 = r6 - r24
            r5 = r28[r4]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r5 != r0) goto L_0x0816
            r6 = r29[r4]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r5 = p001a.C0903NH.wrapNumber(r6)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0816:
            int r6 = r4 + 1
            r0 = r28
            r1 = r29
            r2 = r24
            java.lang.Object[] r6 = m39147a((java.lang.Object[]) r0, (double[]) r1, (int) r6, (int) r2)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aVF r7 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            java.lang.Object r5 = p001a.C0903NH.m7509a((p001a.C2909lh) r0, (java.lang.Object) r5, (java.lang.Object[]) r6, (p001a.aVF) r7, (int) r10)     // Catch:{ Throwable -> 0x00e8 }
            r28[r4] = r5     // Catch:{ Throwable -> 0x00e8 }
        L_0x082e:
            r0 = r23
            int r5 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r5 = r5 + 4
            r0 = r23
            r0.f9158pc = r5     // Catch:{ Throwable -> 0x00e8 }
            r6 = r4
            goto L_0x00ab
        L_0x083b:
            r4 = 0
            goto L_0x07fa
        L_0x083d:
            int r4 = r24 + 1
            int r13 = r6 - r4
            int r4 = r13 + 1
            r6 = r28[r4]     // Catch:{ Throwable -> 0x00e8 }
            a.aVF r6 = (p001a.aVF) r6     // Catch:{ Throwable -> 0x00e8 }
            r5 = r28[r13]     // Catch:{ Throwable -> 0x00e8 }
            a.mF r5 = (p001a.C2957mF) r5     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r13 + 2
            r0 = r28
            r1 = r29
            r2 = r24
            java.lang.Object[] r7 = m39147a((java.lang.Object[]) r0, (double[]) r1, (int) r4, (int) r2)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aVF r8 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aVF r9 = r0.f9157od     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aeQ r4 = r0.bgz     // Catch:{ Throwable -> 0x00e8 }
            java.lang.String r11 = r4.frm     // Catch:{ Throwable -> 0x00e8 }
            r4 = r36
            java.lang.Object r4 = p001a.C0903NH.m7507a(r4, r5, r6, r7, r8, r9, r10, r11, r12)     // Catch:{ Throwable -> 0x00e8 }
            r28[r13] = r4     // Catch:{ Throwable -> 0x00e8 }
            r4 = r13
            goto L_0x082e
        L_0x086f:
            if (r16 == 0) goto L_0x087b
            r0 = r36
            int r4 = r0.elI     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 100
            r0 = r36
            r0.elI = r4     // Catch:{ Throwable -> 0x00e8 }
        L_0x087b:
            int r4 = r24 + 1
            int r25 = r6 - r4
            r4 = r28[r25]     // Catch:{ Throwable -> 0x00e8 }
            a.mF r4 = (p001a.C2957mF) r4     // Catch:{ Throwable -> 0x00e8 }
            int r5 = r25 + 1
            r6 = r28[r5]     // Catch:{ Throwable -> 0x00e8 }
            a.aVF r6 = (p001a.aVF) r6     // Catch:{ Throwable -> 0x00e8 }
            r5 = 70
            r0 = r35
            if (r0 != r5) goto L_0x08a7
            int r5 = r25 + 2
            r0 = r28
            r1 = r29
            r2 = r24
            java.lang.Object[] r5 = m39147a((java.lang.Object[]) r0, (double[]) r1, (int) r5, (int) r2)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            a.bH r4 = p001a.C0903NH.m7486a((p001a.C2957mF) r4, (p001a.aVF) r6, (java.lang.Object[]) r5, (p001a.C2909lh) r0)     // Catch:{ Throwable -> 0x00e8 }
            r28[r25] = r4     // Catch:{ Throwable -> 0x00e8 }
            r6 = r25
            goto L_0x00ab
        L_0x08a7:
            r0 = r23
            a.aVF r5 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            boolean r7 = r0.bgH     // Catch:{ Throwable -> 0x00e8 }
            if (r7 == 0) goto L_0x08b9
            r0 = r23
            a.aVF r5 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            a.aVF r5 = p001a.C6327akn.m23595x(r5)     // Catch:{ Throwable -> 0x00e8 }
        L_0x08b9:
            boolean r7 = r4 instanceof InterpretedFunction     // Catch:{ Throwable -> 0x00e8 }
            if (r7 == 0) goto L_0x0906
            r0 = r4
            a.afO r0 = (InterpretedFunction) r0     // Catch:{ Throwable -> 0x00e8 }
            r11 = r0
            r0 = r23
            a.afO r7 = r0.bgy     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Object r7 = r7.fvF     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Object r8 = r11.fvF     // Catch:{ Throwable -> 0x00e8 }
            if (r7 != r8) goto L_0x0906
            a.sq$a r13 = new a.sq$a     // Catch:{ Throwable -> 0x00e8 }
            r4 = 0
            r13.<init>(r4)     // Catch:{ Throwable -> 0x00e8 }
            r4 = -55
            r0 = r35
            if (r0 != r4) goto L_0x11df
            r0 = r23
            a.sq$a r12 = r0.bgv     // Catch:{ Throwable -> 0x00e8 }
            r4 = 0
            r0 = r36
            r1 = r23
            m39154b(r0, r1, r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x08e3:
            int r9 = r25 + 2
            r4 = r36
            r7 = r28
            r8 = r29
            r10 = r24
            m39140a((p001a.C2909lh) r4, (p001a.aVF) r5, (p001a.aVF) r6, (java.lang.Object[]) r7, (double[]) r8, (int) r9, (int) r10, (InterpretedFunction) r11, (Interpreter.C3604a) r12, (Interpreter.C3604a) r13)     // Catch:{ Throwable -> 0x00e8 }
            r4 = -55
            r0 = r35
            if (r0 == r4) goto L_0x0902
            r0 = r25
            r1 = r23
            r1.bgO = r0     // Catch:{ Throwable -> 0x00e8 }
            r0 = r35
            r1 = r23
            r1.bgP = r0     // Catch:{ Throwable -> 0x00e8 }
        L_0x0902:
            r12 = r22
            goto L_0x0054
        L_0x0906:
            boolean r7 = r4 instanceof org.mozilla.javascript.NativeContinuation     // Catch:{ Throwable -> 0x00e8 }
            if (r7 == 0) goto L_0x092e
            a.sq$b r5 = new a.sq$b     // Catch:{ Throwable -> 0x00e8 }
            a.aHs r4 = (org.mozilla.javascript.NativeContinuation) r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            r5.<init>(r4, r0)     // Catch:{ Throwable -> 0x00e8 }
            if (r24 != 0) goto L_0x0921
            r0 = r27
            r5.bgK = r0     // Catch:{ Throwable -> 0x00e8 }
        L_0x0919:
            r8 = r20
            r10 = r19
            r13 = r23
            goto L_0x0198
        L_0x0921:
            int r4 = r25 + 2
            r4 = r28[r4]     // Catch:{ Throwable -> 0x00e8 }
            r5.bgK = r4     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r25 + 2
            r6 = r29[r4]     // Catch:{ Throwable -> 0x00e8 }
            r5.bgL = r6     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x0919
        L_0x092e:
            boolean r7 = r4 instanceof p001a.C1697Yy     // Catch:{ Throwable -> 0x00e8 }
            if (r7 == 0) goto L_0x0982
            r0 = r4
            a.Yy r0 = (p001a.C1697Yy) r0     // Catch:{ Throwable -> 0x00e8 }
            r14 = r0
            boolean r7 = org.mozilla.javascript.NativeContinuation.m15303d(r14)     // Catch:{ Throwable -> 0x00e8 }
            if (r7 == 0) goto L_0x0951
            r0 = r23
            java.lang.Object[] r4 = r0.bgA     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.sq$a r5 = r0.bgv     // Catch:{ Throwable -> 0x00e8 }
            r6 = 0
            r0 = r36
            a.aHs r5 = m39128a((p001a.C2909lh) r0, (Interpreter.C3604a) r5, (boolean) r6)     // Catch:{ Throwable -> 0x00e8 }
            r4[r25] = r5     // Catch:{ Throwable -> 0x00e8 }
            r6 = r25
            goto L_0x00ab
        L_0x0951:
            boolean r7 = p001a.C5913acp.m20591c(r14)     // Catch:{ Throwable -> 0x00e8 }
            if (r7 == 0) goto L_0x0982
            a.mF r15 = p001a.C0903NH.m7607r(r6)     // Catch:{ Throwable -> 0x00e8 }
            boolean r7 = r15 instanceof InterpretedFunction     // Catch:{ Throwable -> 0x00e8 }
            if (r7 == 0) goto L_0x0982
            a.afO r15 = (InterpretedFunction) r15     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.afO r7 = r0.bgy     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Object r7 = r7.fvF     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Object r8 = r15.fvF     // Catch:{ Throwable -> 0x00e8 }
            if (r7 != r8) goto L_0x0982
            r6 = r36
            r7 = r23
            r8 = r24
            r9 = r28
            r10 = r29
            r11 = r25
            r12 = r35
            r13 = r5
            a.sq$a r13 = m39129a((p001a.C2909lh) r6, (Interpreter.C3604a) r7, (int) r8, (java.lang.Object[]) r9, (double[]) r10, (int) r11, (int) r12, (p001a.aVF) r13, (p001a.C1697Yy) r14, (p001a.C6042afO) r15)     // Catch:{ Throwable -> 0x00e8 }
            r12 = r22
            goto L_0x0054
        L_0x0982:
            r0 = r23
            r1 = r36
            r1.elG = r0     // Catch:{ Throwable -> 0x00e8 }
            r0 = r35
            r1 = r23
            r1.bgP = r0     // Catch:{ Throwable -> 0x00e8 }
            r0 = r25
            r1 = r23
            r1.bgO = r0     // Catch:{ Throwable -> 0x00e8 }
            int r7 = r25 + 2
            r0 = r28
            r1 = r29
            r2 = r24
            java.lang.Object[] r7 = m39147a((java.lang.Object[]) r0, (double[]) r1, (int) r7, (int) r2)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            java.lang.Object r4 = r4.call(r0, r5, r6, r7)     // Catch:{ Throwable -> 0x00e8 }
            r28[r25] = r4     // Catch:{ Throwable -> 0x00e8 }
            r4 = 0
            r0 = r36
            r0.elG = r4     // Catch:{ Throwable -> 0x00e8 }
            r6 = r25
            goto L_0x00ab
        L_0x09b1:
            if (r16 == 0) goto L_0x09bd
            r0 = r36
            int r4 = r0.elI     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 100
            r0 = r36
            r0.elI = r4     // Catch:{ Throwable -> 0x00e8 }
        L_0x09bd:
            int r14 = r6 - r24
            r4 = r28[r14]     // Catch:{ Throwable -> 0x00e8 }
            boolean r5 = r4 instanceof p001a.C6042afO     // Catch:{ Throwable -> 0x00e8 }
            if (r5 == 0) goto L_0x0a06
            r0 = r4
            a.afO r0 = (p001a.C6042afO) r0     // Catch:{ Throwable -> 0x00e8 }
            r11 = r0
            r0 = r23
            a.afO r5 = r0.bgy     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Object r5 = r5.fvF     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Object r6 = r11.fvF     // Catch:{ Throwable -> 0x00e8 }
            if (r5 != r6) goto L_0x0a06
            r0 = r23
            a.aVF r4 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            a.aVF r6 = r11.mo7313c((p001a.C2909lh) r0, (p001a.aVF) r4)     // Catch:{ Throwable -> 0x00e8 }
            a.sq$a r13 = new a.sq$a     // Catch:{ Throwable -> 0x00e8 }
            r4 = 0
            r13.<init>(r4)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aVF r5 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            int r9 = r14 + 1
            r4 = r36
            r7 = r28
            r8 = r29
            r10 = r24
            r12 = r23
            m39140a((p001a.C2909lh) r4, (p001a.aVF) r5, (p001a.aVF) r6, (java.lang.Object[]) r7, (double[]) r8, (int) r9, (int) r10, (p001a.C6042afO) r11, (Interpreter.C3604a) r12, (Interpreter.C3604a) r13)     // Catch:{ Throwable -> 0x00e8 }
            r28[r14] = r6     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            r0.bgO = r14     // Catch:{ Throwable -> 0x00e8 }
            r0 = r35
            r1 = r23
            r1.bgP = r0     // Catch:{ Throwable -> 0x00e8 }
            r12 = r22
            goto L_0x0054
        L_0x0a06:
            boolean r5 = r4 instanceof p001a.C7019azg     // Catch:{ Throwable -> 0x00e8 }
            if (r5 != 0) goto L_0x0a19
            r0 = r26
            if (r4 != r0) goto L_0x0a14
            r4 = r29[r14]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0a14:
            java.lang.RuntimeException r4 = p001a.C0903NH.notFunctionError(r4)     // Catch:{ Throwable -> 0x00e8 }
            throw r4     // Catch:{ Throwable -> 0x00e8 }
        L_0x0a19:
            a.azg r4 = (p001a.C7019azg) r4     // Catch:{ Throwable -> 0x00e8 }
            boolean r5 = r4 instanceof p001a.C1697Yy     // Catch:{ Throwable -> 0x00e8 }
            if (r5 == 0) goto L_0x0a3d
            r0 = r4
            a.Yy r0 = (p001a.C1697Yy) r0     // Catch:{ Throwable -> 0x00e8 }
            r5 = r0
            boolean r5 = org.mozilla.javascript.NativeContinuation.m15303d(r5)     // Catch:{ Throwable -> 0x00e8 }
            if (r5 == 0) goto L_0x0a3d
            r0 = r23
            java.lang.Object[] r4 = r0.bgA     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.sq$a r5 = r0.bgv     // Catch:{ Throwable -> 0x00e8 }
            r6 = 0
            r0 = r36
            a.aHs r5 = m39128a((p001a.C2909lh) r0, (Interpreter.C3604a) r5, (boolean) r6)     // Catch:{ Throwable -> 0x00e8 }
            r4[r14] = r5     // Catch:{ Throwable -> 0x00e8 }
            r6 = r14
            goto L_0x00ab
        L_0x0a3d:
            int r5 = r14 + 1
            r0 = r28
            r1 = r29
            r2 = r24
            java.lang.Object[] r5 = m39147a((java.lang.Object[]) r0, (double[]) r1, (int) r5, (int) r2)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aVF r6 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            a.aVF r4 = r4.construct(r0, r6, r5)     // Catch:{ Throwable -> 0x00e8 }
            r28[r14] = r4     // Catch:{ Throwable -> 0x00e8 }
            r6 = r14
            goto L_0x00ab
        L_0x0a58:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x0a64
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0a64:
            java.lang.String r4 = p001a.C0903NH.typeof(r4)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0a6c:
            int r6 = r6 + 1
            r0 = r23
            a.aVF r4 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r22
            java.lang.String r4 = p001a.C0903NH.m7578f((p001a.aVF) r4, (java.lang.String) r0)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0a7c:
            int r6 = r6 + 1
            r28[r6] = r22     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0a82:
            int r6 = r6 + 1
            r28[r6] = r26     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            r0 = r33
            int r4 = getShort(r0, r4)     // Catch:{ Throwable -> 0x00e8 }
            double r4 = (double) r4     // Catch:{ Throwable -> 0x00e8 }
            r29[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 2
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0a9f:
            int r6 = r6 + 1
            r28[r6] = r26     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            r0 = r33
            int r4 = getInt(r0, r4)     // Catch:{ Throwable -> 0x00e8 }
            double r4 = (double) r4     // Catch:{ Throwable -> 0x00e8 }
            r29[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 4
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0abc:
            int r6 = r6 + 1
            r28[r6] = r26     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aeQ r4 = r0.bgz     // Catch:{ Throwable -> 0x00e8 }
            double[] r4 = r4.fro     // Catch:{ Throwable -> 0x00e8 }
            r4 = r4[r24]     // Catch:{ Throwable -> 0x00e8 }
            r29[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0acc:
            int r6 = r6 + 1
            r0 = r23
            a.aVF r4 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            r1 = r22
            java.lang.Object r4 = p001a.C0903NH.m7543b((p001a.C2909lh) r0, (p001a.aVF) r4, (java.lang.String) r1)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0ade:
            int r6 = r6 + 1
            r0 = r23
            a.aVF r4 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r5 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            byte r5 = r33[r5]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r22
            r1 = r36
            java.lang.Object r4 = p001a.C0903NH.m7500a((p001a.aVF) r4, (java.lang.String) r0, (p001a.C2909lh) r1, (int) r5)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 1
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0b00:
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r5 = r4 + 1
            r0 = r23
            r0.f9158pc = r5     // Catch:{ Throwable -> 0x00e8 }
            byte r24 = r33[r4]     // Catch:{ Throwable -> 0x00e8 }
        L_0x0b0c:
            r0 = r23
            boolean r4 = r0.bgH     // Catch:{ Throwable -> 0x00e8 }
            if (r4 != 0) goto L_0x0b3d
            r4 = r32[r24]     // Catch:{ Throwable -> 0x00e8 }
            r4 = r4 & 1
            if (r4 != 0) goto L_0x0b27
            java.lang.String r4 = "msg.var.redecl"
            r0 = r23
            a.aeQ r5 = r0.bgz     // Catch:{ Throwable -> 0x00e8 }
            java.lang.String[] r5 = r5.frx     // Catch:{ Throwable -> 0x00e8 }
            r5 = r5[r24]     // Catch:{ Throwable -> 0x00e8 }
            a.TJ r4 = p001a.C2909lh.m35246q(r4, r5)     // Catch:{ Throwable -> 0x00e8 }
            throw r4     // Catch:{ Throwable -> 0x00e8 }
        L_0x0b27:
            r4 = r32[r24]     // Catch:{ Throwable -> 0x00e8 }
            r4 = r4 & 8
            if (r4 == 0) goto L_0x00ab
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r30[r24] = r4     // Catch:{ Throwable -> 0x00e8 }
            r4 = r32[r24]     // Catch:{ Throwable -> 0x00e8 }
            r4 = r4 & -9
            r32[r24] = r4     // Catch:{ Throwable -> 0x00e8 }
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            r31[r24] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0b3d:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x11dc
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
            r7 = r4
        L_0x0b4a:
            r0 = r23
            a.aeQ r4 = r0.bgz     // Catch:{ Throwable -> 0x00e8 }
            java.lang.String[] r4 = r4.frx     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4[r24]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aVF r4 = r0.f9156TR     // Catch:{ Throwable -> 0x0b70 }
            boolean r4 = r4 instanceof p001a.C0316EH     // Catch:{ Throwable -> 0x0b70 }
            if (r4 == 0) goto L_0x0b6b
            r0 = r23
            a.aVF r4 = r0.f9156TR     // Catch:{ Throwable -> 0x0b70 }
            a.EH r4 = (p001a.C0316EH) r4     // Catch:{ Throwable -> 0x0b70 }
            r0 = r23
            a.aVF r8 = r0.f9156TR     // Catch:{ Throwable -> 0x0b70 }
            r4.mo1827a(r5, r8, r7)     // Catch:{ Throwable -> 0x0b70 }
            r22 = r5
            goto L_0x00ab
        L_0x0b6b:
            java.lang.RuntimeException r4 = p001a.C1520WN.codeBug()     // Catch:{ Throwable -> 0x0b70 }
            throw r4     // Catch:{ Throwable -> 0x0b70 }
        L_0x0b70:
            r4 = move-exception
            r6 = r20
            r10 = r19
            r11 = r5
            r13 = r23
            goto L_0x00f1
        L_0x0b7a:
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r5 = r4 + 1
            r0 = r23
            r0.f9158pc = r5     // Catch:{ Throwable -> 0x00e8 }
            byte r24 = r33[r4]     // Catch:{ Throwable -> 0x00e8 }
        L_0x0b86:
            r0 = r23
            boolean r4 = r0.bgH     // Catch:{ Throwable -> 0x00e8 }
            if (r4 != 0) goto L_0x0b9c
            r4 = r32[r24]     // Catch:{ Throwable -> 0x00e8 }
            r4 = r4 & 1
            if (r4 != 0) goto L_0x00ab
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r30[r24] = r4     // Catch:{ Throwable -> 0x00e8 }
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            r31[r24] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0b9c:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x0ba8
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0ba8:
            r0 = r23
            a.aeQ r5 = r0.bgz     // Catch:{ Throwable -> 0x00e8 }
            java.lang.String[] r5 = r5.frx     // Catch:{ Throwable -> 0x00e8 }
            r5 = r5[r24]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aVF r7 = r0.f9156TR     // Catch:{ Throwable -> 0x0b70 }
            r0 = r23
            a.aVF r8 = r0.f9156TR     // Catch:{ Throwable -> 0x0b70 }
            r7.put((java.lang.String) r5, (p001a.aVF) r8, (java.lang.Object) r4)     // Catch:{ Throwable -> 0x0b70 }
            r22 = r5
            goto L_0x00ab
        L_0x0bbf:
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r5 = r4 + 1
            r0 = r23
            r0.f9158pc = r5     // Catch:{ Throwable -> 0x00e8 }
            byte r4 = r33[r4]     // Catch:{ Throwable -> 0x00e8 }
        L_0x0bcb:
            int r6 = r6 + 1
            r0 = r23
            boolean r5 = r0.bgH     // Catch:{ Throwable -> 0x00e8 }
            if (r5 != 0) goto L_0x0bdf
            r5 = r30[r4]     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r5     // Catch:{ Throwable -> 0x00e8 }
            r8 = r31[r4]     // Catch:{ Throwable -> 0x00e8 }
            r29[r6] = r8     // Catch:{ Throwable -> 0x00e8 }
            r24 = r4
            goto L_0x00ab
        L_0x0bdf:
            r0 = r23
            a.aeQ r5 = r0.bgz     // Catch:{ Throwable -> 0x00e8 }
            java.lang.String[] r5 = r5.frx     // Catch:{ Throwable -> 0x00e8 }
            r5 = r5[r4]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aVF r7 = r0.f9156TR     // Catch:{ Throwable -> 0x0b70 }
            r0 = r23
            a.aVF r8 = r0.f9156TR     // Catch:{ Throwable -> 0x0b70 }
            java.lang.Object r7 = r7.get((java.lang.String) r5, (p001a.aVF) r8)     // Catch:{ Throwable -> 0x0b70 }
            r28[r6] = r7     // Catch:{ Throwable -> 0x0b70 }
            r24 = r4
            r22 = r5
            goto L_0x00ab
        L_0x0bfb:
            int r8 = r6 + 1
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            byte r9 = r33[r4]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            boolean r4 = r0.bgH     // Catch:{ Throwable -> 0x00e8 }
            if (r4 != 0) goto L_0x0c3d
            r28[r8] = r26     // Catch:{ Throwable -> 0x00e8 }
            r4 = r30[r24]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x0c2f
            r6 = r31[r24]     // Catch:{ Throwable -> 0x00e8 }
        L_0x0c13:
            r4 = r9 & 1
            if (r4 != 0) goto L_0x0c36
            r4 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r4 = r4 + r6
        L_0x0c1a:
            r31[r24] = r4     // Catch:{ Throwable -> 0x00e8 }
            r9 = r9 & 2
            if (r9 != 0) goto L_0x0c3b
        L_0x0c20:
            r29[r8] = r4     // Catch:{ Throwable -> 0x00e8 }
        L_0x0c22:
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 1
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x00e8 }
            r6 = r8
            goto L_0x00ab
        L_0x0c2f:
            double r6 = p001a.C0903NH.toNumber((java.lang.Object) r4)     // Catch:{ Throwable -> 0x00e8 }
            r30[r24] = r26     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x0c13
        L_0x0c36:
            r4 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r4 = r6 - r4
            goto L_0x0c1a
        L_0x0c3b:
            r4 = r6
            goto L_0x0c20
        L_0x0c3d:
            r0 = r23
            a.aeQ r4 = r0.bgz     // Catch:{ Throwable -> 0x00e8 }
            java.lang.String[] r4 = r4.frx     // Catch:{ Throwable -> 0x00e8 }
            r4 = r4[r24]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aVF r5 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            java.lang.Object r4 = p001a.C0903NH.m7500a((p001a.aVF) r5, (java.lang.String) r4, (p001a.C2909lh) r0, (int) r9)     // Catch:{ Throwable -> 0x00e8 }
            r28[r8] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x0c22
        L_0x0c52:
            int r6 = r6 + 1
            r28[r6] = r26     // Catch:{ Throwable -> 0x00e8 }
            r4 = 0
            r29[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0c5c:
            int r6 = r6 + 1
            r28[r6] = r26     // Catch:{ Throwable -> 0x00e8 }
            r4 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r29[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0c66:
            int r6 = r6 + 1
            r4 = 0
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0c6d:
            int r6 = r6 + 1
            r0 = r23
            a.aVF r4 = r0.f9157od     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0c77:
            int r6 = r6 + 1
            r0 = r23
            a.afO r4 = r0.bgy     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0c81:
            int r6 = r6 + 1
            java.lang.Boolean r4 = java.lang.Boolean.FALSE     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0c89:
            int r6 = r6 + 1
            java.lang.Boolean r4 = java.lang.Boolean.TRUE     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0c91:
            int r6 = r6 + 1
            r28[r6] = r27     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0c97:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x0ca3
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0ca3:
            int r6 = r6 + -1
            r0 = r23
            a.aVF r5 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            a.aVF r4 = p001a.C0903NH.m7478a((java.lang.Object) r4, (p001a.C2909lh) r0, (p001a.aVF) r5)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            r0.f9156TR = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0cb5:
            r0 = r23
            a.aVF r4 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            a.aVF r4 = p001a.C0903NH.m7608s(r4)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            r0.f9156TR = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0cc3:
            int r6 = r6 + -1
            r0 = r23
            int r4 = r0.bgE     // Catch:{ Throwable -> 0x00e8 }
            int r24 = r24 + r4
            r0 = r23
            a.aeQ r4 = r0.bgz     // Catch:{ Throwable -> 0x00e8 }
            byte[] r4 = r4.frr     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r5 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            byte r4 = r4[r5]     // Catch:{ Throwable -> 0x00e8 }
            if (r4 == 0) goto L_0x0cfe
            r4 = 1
            r5 = r4
        L_0x0cdb:
            int r4 = r6 + 1
            r4 = r28[r4]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Throwable r4 = (java.lang.Throwable) r4     // Catch:{ Throwable -> 0x00e8 }
            if (r5 != 0) goto L_0x0d01
            r5 = 0
        L_0x0ce4:
            r0 = r23
            a.aVF r7 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r22
            r1 = r36
            a.aVF r4 = p001a.C0903NH.m7480a((java.lang.Throwable) r4, (p001a.aVF) r5, (java.lang.String) r0, (p001a.C2909lh) r1, (p001a.aVF) r7)     // Catch:{ Throwable -> 0x00e8 }
            r28[r24] = r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 1
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0cfe:
            r4 = 0
            r5 = r4
            goto L_0x0cdb
        L_0x0d01:
            r5 = r28[r24]     // Catch:{ Throwable -> 0x00e8 }
            a.aVF r5 = (p001a.aVF) r5     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x0ce4
        L_0x0d06:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x11d9
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4
        L_0x0d13:
            int r6 = r6 + -1
            r0 = r23
            int r4 = r0.bgE     // Catch:{ Throwable -> 0x00e8 }
            int r24 = r24 + r4
            r4 = 58
            r0 = r35
            if (r0 != r4) goto L_0x0d2c
            r4 = 0
        L_0x0d22:
            r0 = r36
            java.lang.Object r4 = p001a.C0903NH.m7513a((java.lang.Object) r5, (p001a.C2909lh) r0, (int) r4)     // Catch:{ Throwable -> 0x00e8 }
            r28[r24] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0d2c:
            r4 = 59
            r0 = r35
            if (r0 != r4) goto L_0x0d34
            r4 = 1
            goto L_0x0d22
        L_0x0d34:
            r4 = 2
            goto L_0x0d22
        L_0x0d36:
            r0 = r23
            int r4 = r0.bgE     // Catch:{ Throwable -> 0x00e8 }
            int r24 = r24 + r4
            r4 = r28[r24]     // Catch:{ Throwable -> 0x00e8 }
            int r6 = r6 + 1
            r5 = 61
            r0 = r35
            if (r0 != r5) goto L_0x0d4e
            java.lang.Boolean r4 = p001a.C0903NH.enumNext(r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0d4a:
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0d4e:
            r0 = r36
            java.lang.Object r4 = p001a.C0903NH.m7559c((java.lang.Object) r4, (p001a.C2909lh) r0)     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x0d4a
        L_0x0d55:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x0d61
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0d61:
            r0 = r22
            r1 = r36
            a.bH r4 = p001a.C0903NH.m7556c((java.lang.Object) r4, (java.lang.String) r0, (p001a.C2909lh) r1)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0d6d:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x11d6
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4
        L_0x0d7a:
            int r6 = r6 + -1
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x0d88
            r8 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r8)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0d88:
            r0 = r36
            r1 = r24
            a.bH r4 = p001a.C0903NH.m7540b((java.lang.Object) r4, (java.lang.Object) r5, (p001a.C2909lh) r0, (int) r1)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0d94:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x11d3
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
            r7 = r4
        L_0x0da1:
            int r6 = r6 + -1
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x11d0
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4
        L_0x0db0:
            int r6 = r6 + -1
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x0dbe
            r8 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r8)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0dbe:
            r0 = r36
            r1 = r24
            a.bH r4 = p001a.C0903NH.m7489a((java.lang.Object) r4, (java.lang.Object) r5, (java.lang.Object) r7, (p001a.C2909lh) r0, (int) r1)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0dca:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x0dd6
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0dd6:
            r0 = r23
            a.aVF r5 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            r1 = r24
            a.bH r4 = p001a.C0903NH.m7487a((java.lang.Object) r4, (p001a.C2909lh) r0, (p001a.aVF) r5, (int) r1)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0de6:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x11cd
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4
        L_0x0df3:
            int r6 = r6 + -1
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x0e01
            r8 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r8)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0e01:
            r0 = r23
            a.aVF r7 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            r1 = r24
            a.bH r4 = p001a.C0903NH.m7488a((java.lang.Object) r4, (java.lang.Object) r5, (p001a.C2909lh) r0, (p001a.aVF) r7, (int) r1)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0e11:
            r0 = r23
            int r4 = r0.bgE     // Catch:{ Throwable -> 0x00e8 }
            int r24 = r24 + r4
            r4 = r28[r24]     // Catch:{ Throwable -> 0x00e8 }
            a.aVF r4 = (p001a.aVF) r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            r0.f9156TR = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0e21:
            r0 = r23
            int r4 = r0.bgE     // Catch:{ Throwable -> 0x00e8 }
            int r24 = r24 + r4
            r0 = r23
            a.aVF r4 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r28[r24] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0e2f:
            int r6 = r6 + 1
            r0 = r23
            a.aVF r4 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.afO r5 = r0.bgy     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            r1 = r24
            a.afO r4 = p001a.C6042afO.m21539a((p001a.C2909lh) r0, (p001a.aVF) r4, (p001a.C6042afO) r5, (int) r1)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0e45:
            r0 = r23
            a.aVF r4 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.afO r5 = r0.bgy     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            r1 = r24
            m39153b(r0, r4, r5, r1)     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0e56:
            int r6 = r6 + 1
            r0 = r23
            a.aVF[] r4 = r0.bgJ     // Catch:{ Throwable -> 0x00e8 }
            r4 = r4[r24]     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0e62:
            int r4 = r6 + 1
            r0 = r24
            int[] r5 = new int[r0]     // Catch:{ Throwable -> 0x00e8 }
            r28[r4] = r5     // Catch:{ Throwable -> 0x00e8 }
            int r6 = r4 + 1
            r0 = r24
            java.lang.Object[] r4 = new java.lang.Object[r0]     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            r4 = 0
            r29[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0e78:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x11ca
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r4
        L_0x0e85:
            int r6 = r6 + -1
            r8 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            int r7 = (int) r8     // Catch:{ Throwable -> 0x00e8 }
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Object[] r4 = (java.lang.Object[]) r4     // Catch:{ Throwable -> 0x00e8 }
            r4[r7] = r5     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r7 + 1
            double r4 = (double) r4     // Catch:{ Throwable -> 0x00e8 }
            r29[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0e97:
            r5 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            int r6 = r6 + -1
            r8 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            int r7 = (int) r8     // Catch:{ Throwable -> 0x00e8 }
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Object[] r4 = (java.lang.Object[]) r4     // Catch:{ Throwable -> 0x00e8 }
            r4[r7] = r5     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r6 + -1
            r4 = r28[r4]     // Catch:{ Throwable -> 0x00e8 }
            int[] r4 = (int[]) r4     // Catch:{ Throwable -> 0x00e8 }
            r5 = -1
            r4[r7] = r5     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r7 + 1
            double r4 = (double) r4     // Catch:{ Throwable -> 0x00e8 }
            r29[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0eb4:
            r5 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            int r6 = r6 + -1
            r8 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            int r7 = (int) r8     // Catch:{ Throwable -> 0x00e8 }
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Object[] r4 = (java.lang.Object[]) r4     // Catch:{ Throwable -> 0x00e8 }
            r4[r7] = r5     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r6 + -1
            r4 = r28[r4]     // Catch:{ Throwable -> 0x00e8 }
            int[] r4 = (int[]) r4     // Catch:{ Throwable -> 0x00e8 }
            r5 = 1
            r4[r7] = r5     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r7 + 1
            double r4 = (double) r4     // Catch:{ Throwable -> 0x00e8 }
            r29[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0ed1:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Object[] r4 = (java.lang.Object[]) r4     // Catch:{ Throwable -> 0x00e8 }
            int r7 = r6 + -1
            r5 = r28[r7]     // Catch:{ Throwable -> 0x00e8 }
            int[] r5 = (int[]) r5     // Catch:{ Throwable -> 0x00e8 }
            r6 = 66
            r0 = r35
            if (r0 != r6) goto L_0x0efa
            r0 = r23
            a.aeQ r6 = r0.bgz     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Object[] r6 = r6.frG     // Catch:{ Throwable -> 0x00e8 }
            r6 = r6[r24]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Object[] r6 = (java.lang.Object[]) r6     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aVF r8 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            a.aVF r4 = p001a.C0903NH.m7483a((java.lang.Object[]) r6, (java.lang.Object[]) r4, (int[]) r5, (p001a.C2909lh) r0, (p001a.aVF) r8)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0ef5:
            r28[r7] = r4     // Catch:{ Throwable -> 0x00e8 }
            r6 = r7
            goto L_0x00ab
        L_0x0efa:
            r5 = 0
            int[] r5 = (int[]) r5     // Catch:{ Throwable -> 0x00e8 }
            r6 = -31
            r0 = r35
            if (r0 != r6) goto L_0x0f0d
            r0 = r23
            a.aeQ r5 = r0.bgz     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Object[] r5 = r5.frG     // Catch:{ Throwable -> 0x00e8 }
            r5 = r5[r24]     // Catch:{ Throwable -> 0x00e8 }
            int[] r5 = (int[]) r5     // Catch:{ Throwable -> 0x00e8 }
        L_0x0f0d:
            r0 = r23
            a.aVF r6 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            a.aVF r4 = p001a.C0903NH.m7481a((java.lang.Object[]) r4, (int[]) r5, (p001a.C2909lh) r0, (p001a.aVF) r6)     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x0ef5
        L_0x0f18:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x0f24
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0f24:
            int r6 = r6 + -1
            r0 = r23
            a.aVF r5 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            a.aVF r4 = p001a.C0903NH.m7555c((java.lang.Object) r4, (p001a.aVF) r5)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            r0.f9156TR = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0f34:
            r0 = r23
            boolean r4 = m39158c(r0, r6)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aVF r5 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Object r4 = p001a.C0903NH.m7521a((boolean) r4, (p001a.aVF) r5)     // Catch:{ Throwable -> 0x00e8 }
            if (r4 == 0) goto L_0x0f5e
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.aVF r4 = r0.f9156TR     // Catch:{ Throwable -> 0x00e8 }
            a.aVF r4 = p001a.C0903NH.m7609t(r4)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            r0.f9156TR = r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 2
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0f5e:
            int r4 = r6 + -1
            goto L_0x0399
        L_0x0f62:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 != r0) goto L_0x0f6e
            r4 = r29[r6]     // Catch:{ Throwable -> 0x00e8 }
            java.lang.Number r4 = p001a.C0903NH.wrapNumber(r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0f6e:
            r0 = r36
            java.lang.Object r4 = p001a.C0903NH.m7544b((java.lang.Object) r4, (p001a.C2909lh) r0)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0f78:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 == r0) goto L_0x00ab
            r0 = r36
            java.lang.String r4 = p001a.C0903NH.m7580f((java.lang.Object) r4, (p001a.C2909lh) r0)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0f88:
            r4 = r28[r6]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r26
            if (r4 == r0) goto L_0x00ab
            r0 = r36
            java.lang.String r4 = p001a.C0903NH.m7584g((java.lang.Object) r4, (p001a.C2909lh) r0)     // Catch:{ Throwable -> 0x00e8 }
            r28[r6] = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0f98:
            r0 = r23
            a.Tp r4 = r0.bgG     // Catch:{ Throwable -> 0x00e8 }
            if (r4 == 0) goto L_0x0145
            r0 = r23
            a.Tp r4 = r0.bgG     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            r4.onDebuggerStatement(r0)     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x0145
        L_0x0fa9:
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            r0.bgN = r4     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.Tp r4 = r0.bgG     // Catch:{ Throwable -> 0x00e8 }
            if (r4 == 0) goto L_0x0fca
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            r0 = r33
            int r4 = m39159d(r0, r4)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            a.Tp r5 = r0.bgG     // Catch:{ Throwable -> 0x00e8 }
            r0 = r36
            r5.onLineChange(r0, r4)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0fca:
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 2
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x0fd6:
            r24 = 0
            goto L_0x00ab
        L_0x0fda:
            r24 = 1
            goto L_0x00ab
        L_0x0fde:
            r24 = 2
            goto L_0x00ab
        L_0x0fe2:
            r24 = 3
            goto L_0x00ab
        L_0x0fe6:
            r24 = 4
            goto L_0x00ab
        L_0x0fea:
            r24 = 5
            goto L_0x00ab
        L_0x0fee:
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            byte r4 = r33[r4]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r4 & 255(0xff, float:3.57E-43)
            r24 = r0
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 1
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x1004:
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            r0 = r33
            int r24 = m39159d(r0, r4)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 2
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x101a:
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            r0 = r33
            int r24 = getInt(r0, r4)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r4 = r4 + 4
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x1030:
            r4 = 0
            r22 = r34[r4]     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x1035:
            r4 = 1
            r22 = r34[r4]     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x103a:
            r4 = 2
            r22 = r34[r4]     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x103f:
            r4 = 3
            r22 = r34[r4]     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x00ab
        L_0x1044:
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            byte r4 = r33[r4]     // Catch:{ Throwable -> 0x00e8 }
            r4 = r4 & 255(0xff, float:3.57E-43)
            r5 = r34[r4]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x0b70 }
            int r4 = r4 + 1
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x0b70 }
            r22 = r5
            goto L_0x00ab
        L_0x105c:
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            r0 = r33
            int r4 = m39159d(r0, r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r34[r4]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x0b70 }
            int r4 = r4 + 2
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x0b70 }
            r22 = r5
            goto L_0x00ab
        L_0x1076:
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            r0 = r33
            int r4 = getInt(r0, r4)     // Catch:{ Throwable -> 0x00e8 }
            r5 = r34[r4]     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r4 = r0.f9158pc     // Catch:{ Throwable -> 0x0b70 }
            int r4 = r4 + 4
            r0 = r23
            r0.f9158pc = r4     // Catch:{ Throwable -> 0x0b70 }
            r22 = r5
            goto L_0x00ab
        L_0x1090:
            r0 = r23
            a.aeQ r5 = r0.bgz     // Catch:{ Throwable -> 0x00e8 }
            a.akl r5 = r5.frH     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            int r6 = r0.f9158pc     // Catch:{ Throwable -> 0x00e8 }
            int r5 = r5.getExistingInt(r6)     // Catch:{ Throwable -> 0x00e8 }
            r0 = r23
            r0.f9158pc = r5     // Catch:{ Throwable -> 0x00e8 }
            goto L_0x03ba
        L_0x10a4:
            r8 = r6
            r22 = r11
            r5 = r4
            goto L_0x0198
        L_0x10aa:
            boolean r4 = r5 instanceof p001a.C2317dx
            if (r4 == 0) goto L_0x10b2
            r4 = 2
            r7 = r4
            goto L_0x01af
        L_0x10b2:
            boolean r4 = r5 instanceof p001a.C0068Ap
            if (r4 == 0) goto L_0x10ba
            r4 = 2
            r7 = r4
            goto L_0x01af
        L_0x10ba:
            boolean r4 = r5 instanceof p001a.C1319TJ
            if (r4 == 0) goto L_0x10c2
            r4 = 2
            r7 = r4
            goto L_0x01af
        L_0x10c2:
            boolean r4 = r5 instanceof java.lang.RuntimeException
            if (r4 == 0) goto L_0x10d6
            r4 = 13
            r0 = r36
            boolean r4 = r0.hasFeature(r4)
            if (r4 == 0) goto L_0x10d4
            r4 = 2
        L_0x10d1:
            r7 = r4
            goto L_0x01af
        L_0x10d4:
            r4 = 1
            goto L_0x10d1
        L_0x10d6:
            boolean r4 = r5 instanceof java.lang.Error
            if (r4 == 0) goto L_0x10ea
            r4 = 13
            r0 = r36
            boolean r4 = r0.hasFeature(r4)
            if (r4 == 0) goto L_0x10e8
            r4 = 2
        L_0x10e5:
            r7 = r4
            goto L_0x01af
        L_0x10e8:
            r4 = 0
            goto L_0x10e5
        L_0x10ea:
            boolean r4 = r5 instanceof Interpreter.C3605b
            if (r4 == 0) goto L_0x10f5
            r7 = 1
            r4 = r5
            a.sq$b r4 = (Interpreter.C3605b) r4
            r6 = r4
            goto L_0x01af
        L_0x10f5:
            r4 = 13
            r0 = r36
            boolean r4 = r0.hasFeature(r4)
            if (r4 == 0) goto L_0x1103
            r4 = 2
        L_0x1100:
            r7 = r4
            goto L_0x01af
        L_0x1103:
            r4 = 1
            goto L_0x1100
        L_0x1105:
            r5 = move-exception
            r4 = 1
            r7 = r4
            goto L_0x01b8
        L_0x110a:
            r5 = move-exception
            r4 = 0
            r7 = 0
            r6 = r4
            goto L_0x01b8
        L_0x1110:
            r4 = move-exception
            r6 = 0
            r7 = 0
            r11 = r4
            goto L_0x01cb
        L_0x1116:
            r4 = 0
            goto L_0x01d1
        L_0x1119:
            r0 = r36
            m39154b(r0, r13, r11)
            a.sq$a r13 = r13.bgv
            if (r13 != 0) goto L_0x113b
            if (r6 == 0) goto L_0x11c3
            a.sq$a r4 = r6.cuZ
            if (r4 == 0) goto L_0x112b
            p001a.C1520WN.codeBug()
        L_0x112b:
            a.sq$a r4 = r6.cuY
            if (r4 == 0) goto L_0x114d
            r24 = -1
            r20 = r8
            r19 = r10
            r12 = r22
            r18 = r11
            goto L_0x0054
        L_0x113b:
            if (r6 == 0) goto L_0x01cb
            a.sq$a r4 = r6.cuZ
            if (r4 != r13) goto L_0x01cb
            r24 = -1
            r20 = r8
            r19 = r10
            r12 = r22
            r18 = r11
            goto L_0x0054
        L_0x114d:
            java.lang.Object r5 = r6.bgK
            double r6 = r6.bgL
            r4 = 0
        L_0x1152:
            r0 = r36
            a.aCx r8 = r0.elH
            if (r8 == 0) goto L_0x1177
            r0 = r36
            a.aCx r8 = r0.elH
            int r8 = r8.size()
            if (r8 == 0) goto L_0x1177
            r0 = r36
            a.aCx r8 = r0.elH
            java.lang.Object r8 = r8.pop()
            r0 = r36
            r0.elG = r8
        L_0x116e:
            if (r4 == 0) goto L_0x1185
            boolean r5 = r4 instanceof java.lang.RuntimeException
            if (r5 == 0) goto L_0x1182
            java.lang.RuntimeException r4 = (java.lang.RuntimeException) r4
            throw r4
        L_0x1177:
            r8 = 0
            r0 = r36
            r0.elG = r8
            r8 = 0
            r0 = r36
            r0.elH = r8
            goto L_0x116e
        L_0x1182:
            java.lang.Error r4 = (java.lang.Error) r4
            throw r4
        L_0x1185:
            r0 = r26
            if (r5 != r0) goto L_0x0183
            java.lang.Number r5 = p001a.C0903NH.wrapNumber(r6)
            goto L_0x0183
        L_0x118f:
            r4 = move-exception
            r10 = r5
            r11 = r22
            r13 = r23
            goto L_0x00f1
        L_0x1197:
            r4 = move-exception
            r10 = r5
            r11 = r22
            r13 = r8
            goto L_0x00f1
        L_0x119e:
            r4 = move-exception
            r6 = r20
            r10 = r19
            r11 = r12
            goto L_0x00f1
        L_0x11a6:
            r4 = move-exception
            r6 = r20
            r10 = r19
            r11 = r12
            r13 = r5
            goto L_0x00f1
        L_0x11af:
            r4 = move-exception
            r6 = r20
            r10 = r19
            r11 = r12
            r13 = r23
            goto L_0x00f1
        L_0x11b9:
            r4 = move-exception
            r6 = r20
            r10 = r5
            r11 = r22
            r13 = r23
            goto L_0x00f1
        L_0x11c3:
            r6 = r8
            r5 = r10
            r4 = r11
            goto L_0x1152
        L_0x11c7:
            r6 = r4
            goto L_0x00ab
        L_0x11ca:
            r5 = r4
            goto L_0x0e85
        L_0x11cd:
            r5 = r4
            goto L_0x0df3
        L_0x11d0:
            r5 = r4
            goto L_0x0db0
        L_0x11d3:
            r7 = r4
            goto L_0x0da1
        L_0x11d6:
            r5 = r4
            goto L_0x0d7a
        L_0x11d9:
            r5 = r4
            goto L_0x0d13
        L_0x11dc:
            r7 = r4
            goto L_0x0b4a
        L_0x11df:
            r12 = r23
            goto L_0x08e3
        L_0x11e3:
            r5 = r4
            goto L_0x079f
        L_0x11e6:
            r5 = r4
            goto L_0x06fb
        L_0x11e9:
            r5 = r4
            goto L_0x06b8
        L_0x11ec:
            r5 = r4
            goto L_0x0682
        L_0x11ef:
            r5 = r4
            goto L_0x0602
        L_0x11f2:
            r5 = r4
            goto L_0x05ad
        L_0x11f5:
            r5 = r4
            goto L_0x058e
        L_0x11f8:
            r5 = r4
            goto L_0x056b
        L_0x11fb:
            r5 = r4
            goto L_0x02be
        L_0x11fe:
            r4 = r24
            goto L_0x0bcb
        L_0x1202:
            r11 = r5
            goto L_0x01cb
        L_0x1205:
            r4 = r18
            goto L_0x1152
        */
        throw new UnsupportedOperationException("Method not decompiled: Interpreter.m39136a(a.lh, a.sq$a, java.lang.Object):java.lang.Object");
    }

    /* renamed from: a */
    private static boolean m39146a(Object[] objArr, double[] dArr, int i) {
        double doubleValue;
        double d;
        Number number = (Number) objArr[i + 1];
        Number number2 = (Number) objArr[i];
        org.mozilla1.javascript.UniqueTag bdVar = org.mozilla1.javascript.UniqueTag.f5856nd;
        if (number == bdVar) {
            double d2 = dArr[i + 1];
            if (number2 == bdVar) {
                d = dArr[i];
                doubleValue = d2;
            } else if (!(number2 instanceof Number)) {
                return false;
            } else {
                d = number2.doubleValue();
                doubleValue = d2;
            }
        } else if (number2 != bdVar) {
            return org.mozilla1.javascript.ScriptRuntime.shallowEq(number2, number);
        } else {
            double d3 = dArr[i];
            if (number == bdVar) {
                doubleValue = dArr[i + 1];
                d = d3;
            } else if (!(number instanceof Number)) {
                return false;
            } else {
                doubleValue = number.doubleValue();
                d = d3;
            }
        }
        if (d == doubleValue) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    private static C3604a m39130a(org.mozilla1.javascript.Context lhVar, Object obj, C3604a aVar, int i, boolean z) {
        int i2;
        if (i >= 0) {
            if (aVar.bgx) {
                aVar = aVar.abD();
            }
            int[] iArr = aVar.bgz.frs;
            aVar.f9158pc = iArr[i + 2];
            if (z) {
                aVar.bgM = aVar.f9158pc;
            }
            aVar.bgO = aVar.bgF;
            int i3 = aVar.bgE + iArr[i + 5];
            int i4 = aVar.bgE + iArr[i + 4];
            aVar.f9156TR = (org.mozilla1.javascript.Scriptable) aVar.bgA[i3];
            aVar.bgA[i4] = obj;
        } else {
            C3605b bVar = (C3605b) obj;
            if (bVar.cuZ != aVar) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            if (bVar.cuY == null) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            int i5 = bVar.cuY.bgw + 1;
            if (bVar.cuZ != null) {
                i2 = i5 - bVar.cuZ.bgw;
            } else {
                i2 = i5;
            }
            C3604a[] aVarArr = null;
            C3604a aVar2 = bVar.cuY;
            int i6 = 0;
            int i7 = 0;
            while (i6 != i2) {
                if (!aVar2.bgx) {
                    org.mozilla1.javascript.Kit.codeBug();
                }
                if (m39155b(aVar2)) {
                    if (aVarArr == null) {
                        aVarArr = new C3604a[(i2 - i6)];
                    }
                    aVarArr[i7] = aVar2;
                    i7++;
                }
                i6++;
                aVar2 = aVar2.bgv;
            }
            while (i7 != 0) {
                i7--;
                m39142a(lhVar, aVarArr[i7], org.mozilla1.javascript.ScriptRuntime.emptyArgs, true);
            }
            aVar = bVar.cuY.abD();
            m39144a(aVar, bVar.bgK, bVar.bgL);
        }
        aVar.bgQ = null;
        return aVar;
    }

    /* renamed from: a */
    private static Object m39135a(org.mozilla1.javascript.Context lhVar, C3604a aVar, int i, C3606c cVar) {
        if (cVar.operation == 2) {
            throw org.mozilla1.javascript.ScriptRuntime.m7600no("msg.yield.closing");
        }
        aVar.bgx = true;
        aVar.bgK = aVar.bgA[i];
        aVar.bgL = aVar.bgC[i];
        aVar.bgO = i;
        aVar.f9158pc--;
        org.mozilla1.javascript.ScriptRuntime.m7591k(lhVar);
        if (aVar.bgK != org.mozilla1.javascript.UniqueTag.f5856nd) {
            return aVar.bgK;
        }
        return org.mozilla1.javascript.ScriptRuntime.wrapNumber(aVar.bgL);
    }

    /* renamed from: a */
    private static Object m39137a(C3604a aVar, int i, C3606c cVar, int i2) {
        aVar.bgx = false;
        int d = m39159d(aVar.bgz.frr, aVar.f9158pc);
        aVar.f9158pc += 2;
        if (cVar.operation == 1) {
            return new JavaScriptException(cVar.value, aVar.bgz.frm, d);
        }
        if (cVar.operation == 2) {
            return cVar.value;
        }
        if (cVar.operation != 0) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        if (i2 == 72) {
            aVar.bgA[i] = cVar.value;
        }
        return org.mozilla1.javascript.Scriptable.NOT_FOUND;
    }

    /* renamed from: a */
    private static C3604a m39129a(org.mozilla1.javascript.Context lhVar, C3604a aVar, int i, Object[] objArr, double[] dArr, int i2, int i3, org.mozilla1.javascript.Scriptable avf, IdFunctionObject yy, org.mozilla1.javascript.InterpretedFunction afo) {
        org.mozilla1.javascript.Scriptable avf2;
        C3604a aVar2;
        Object[] c;
        if (i != 0) {
            avf2 = org.mozilla1.javascript.ScriptRuntime.m7477a(lhVar, objArr[i2 + 2]);
        } else {
            avf2 = null;
        }
        if (avf2 == null) {
            avf2 = org.mozilla1.javascript.ScriptRuntime.getTopCallScope(lhVar);
        }
        if (i3 == huf) {
            m39154b(lhVar, aVar, (Object) null);
            aVar2 = aVar.bgv;
        } else {
            aVar.bgO = i2;
            aVar.bgP = i3;
            aVar2 = aVar;
        }
        C3604a aVar3 = new C3604a((C3604a) null);
        if (BaseFunction.m20589b(yy)) {
            if (i < 2) {
                c = org.mozilla1.javascript.ScriptRuntime.emptyArgs;
            } else {
                c = org.mozilla1.javascript.ScriptRuntime.m7563c(lhVar, objArr[i2 + 3]);
            }
            m39140a(lhVar, avf, avf2, c, (double[]) null, 0, c.length, afo, aVar2, aVar3);
        } else {
            for (int i4 = 1; i4 < i; i4++) {
                objArr[i2 + 1 + i4] = objArr[i2 + 2 + i4];
                dArr[i2 + 1 + i4] = dArr[i2 + 2 + i4];
            }
            m39140a(lhVar, avf, avf2, objArr, dArr, i2 + 2, i < 2 ? 0 : i - 1, afo, aVar2, aVar3);
        }
        return aVar3;
    }

    /* renamed from: a */
    private static void m39140a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr, double[] dArr, int i, int i2, org.mozilla1.javascript.InterpretedFunction afo, C3604a aVar, C3604a aVar2) {
        DebugFrame tp;
        boolean z;
        boolean z2;
        Object[] objArr2;
        int[] iArr;
        double[] dArr2;
        org.mozilla1.javascript.InterpreterData aeq = afo.bgz;
        boolean z3 = aeq.eMO;
        if (lhVar.elA != null) {
            DebugFrame frame = lhVar.elA.getFrame(lhVar, aeq);
            if (frame != null) {
                tp = frame;
                z = true;
            } else {
                tp = frame;
                z = z3;
            }
        } else {
            tp = null;
            z = z3;
        }
        if (z) {
            if (dArr != null) {
                objArr = m39147a(objArr, dArr, i, i2);
            }
            i = 0;
            dArr = null;
        }
        if (aeq.eMN != 0) {
            if (!aeq.elw) {
                avf = afo.getParentScope();
            }
            if (z) {
                avf = org.mozilla1.javascript.ScriptRuntime.m7475a((org.mozilla1.javascript.NativeFunction) afo, avf, objArr);
            }
        } else {
            org.mozilla1.javascript.ScriptRuntime.m7525a((NativeFunction) afo, avf2, lhVar, avf, afo.bgz.frK);
        }
        if (aeq.frp != null) {
            if (aeq.eMN != 0 && !aeq.eMO) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            for (int i3 = 0; i3 < aeq.frp.length; i3++) {
                if (aeq.frp[i3].eMN == 1) {
                    m39153b(lhVar, avf, afo, i3);
                }
            }
        }
        org.mozilla1.javascript.Scriptable[] avfArr = null;
        if (aeq.frq != null) {
            if (aeq.eMN != 0) {
                avfArr = afo.fvG;
            } else {
                avfArr = afo.mo13252d(lhVar, avf);
            }
        }
        int i4 = (aeq.frt + aeq.fru) - 1;
        int i5 = aeq.frw;
        if (i5 != aeq.frv + i4 + 1) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        if (aVar2.bgA == null || i5 > aVar2.bgA.length) {
            z2 = false;
            objArr2 = new Object[i5];
            iArr = new int[i5];
            dArr2 = new double[i5];
        } else {
            z2 = true;
            objArr2 = aVar2.bgA;
            iArr = aVar2.bgB;
            dArr2 = aVar2.bgC;
        }
        int paramAndVarCount = aeq.getParamAndVarCount();
        for (int i6 = 0; i6 < paramAndVarCount; i6++) {
            if (aeq.mo13073kH(i6)) {
                iArr[i6] = 13;
            }
        }
        int i7 = aeq.frz;
        if (i7 <= i2) {
            i2 = i7;
        }
        aVar2.bgv = aVar;
        aVar2.bgw = aVar == null ? 0 : aVar.bgw + 1;
        if (aVar2.bgw > lhVar.bww()) {
            throw org.mozilla1.javascript.Context.m35245gF("Exceeded maximum stack depth");
        }
        aVar2.bgx = false;
        aVar2.bgy = afo;
        aVar2.bgz = aeq;
        aVar2.bgA = objArr2;
        aVar2.bgB = iArr;
        aVar2.bgC = dArr2;
        aVar2.bgD = aVar2;
        aVar2.bgE = aeq.frt;
        aVar2.bgF = i4;
        aVar2.bgG = tp;
        aVar2.bgH = z;
        aVar2.f9157od = avf2;
        aVar2.bgJ = avfArr;
        aVar2.bgK = org.mozilla1.javascript.Undefined.instance;
        aVar2.f9158pc = 0;
        aVar2.bgM = 0;
        aVar2.bgN = aeq.frI;
        aVar2.f9156TR = avf;
        aVar2.bgO = i4;
        aVar2.bgP = 0;
        System.arraycopy(objArr, i, objArr2, 0, i2);
        if (dArr != null) {
            System.arraycopy(dArr, i, dArr2, 0, i2);
        }
        while (i2 != aeq.frt) {
            objArr2[i2] = org.mozilla1.javascript.Undefined.instance;
            i2++;
        }
        if (z2) {
            for (int i8 = i4 + 1; i8 != objArr2.length; i8++) {
                objArr2[i8] = null;
            }
        }
        m39142a(lhVar, aVar2, objArr, false);
    }

    /* renamed from: b */
    private static boolean m39155b(C3604a aVar) {
        return aVar.bgG != null || aVar.bgz.eMO;
    }

    /* renamed from: a */
    private static void m39142a(org.mozilla1.javascript.Context lhVar, C3604a aVar, Object[] objArr, boolean z) {
        boolean z2 = aVar.bgz.eMO;
        boolean z3 = aVar.bgG != null;
        if (z2 || z3) {
            org.mozilla1.javascript.Scriptable avf = aVar.f9156TR;
            if (avf == null) {
                org.mozilla1.javascript.Kit.codeBug();
            } else if (z) {
                while (true) {
                    if (!(avf instanceof NativeWith)) {
                        break;
                    }
                    avf = avf.getParentScope();
                    if (avf == null || (aVar.bgv != null && aVar.bgv.f9156TR == avf)) {
                        org.mozilla1.javascript.Kit.codeBug();
                    }
                }
                org.mozilla1.javascript.Kit.codeBug();
            }
            if (z3) {
                aVar.bgG.onEnter(lhVar, avf, aVar.f9157od, objArr);
            }
            if (z2) {
                org.mozilla1.javascript.ScriptRuntime.m7586h(lhVar, avf);
            }
        }
    }

    /* renamed from: b */
    private static void m39154b(org.mozilla1.javascript.Context lhVar, C3604a aVar, Object obj) {
        Object obj2;
        double d;
        if (aVar.bgz.eMO) {
            org.mozilla1.javascript.ScriptRuntime.m7591k(lhVar);
        }
        if (aVar.bgG != null) {
            try {
                if (obj instanceof Throwable) {
                    aVar.bgG.onExit(lhVar, true, obj);
                    return;
                }
                C3605b bVar = (C3605b) obj;
                if (bVar == null) {
                    obj2 = aVar.bgK;
                } else {
                    obj2 = bVar.bgK;
                }
                if (obj2 == org.mozilla1.javascript.UniqueTag.f5856nd) {
                    if (bVar == null) {
                        d = aVar.bgL;
                    } else {
                        d = bVar.bgL;
                    }
                    obj2 = org.mozilla1.javascript.ScriptRuntime.wrapNumber(d);
                }
                aVar.bgG.onExit(lhVar, false, obj2);
            } catch (Throwable th) {
                System.err.println("RHINO USAGE WARNING: onExit terminated with exception");
                th.printStackTrace(System.err);
            }
        }
    }

    /* renamed from: a */
    private static void m39144a(C3604a aVar, Object obj, double d) {
        if (aVar.bgP == 38) {
            aVar.bgA[aVar.bgO] = obj;
            aVar.bgC[aVar.bgO] = d;
        } else if (aVar.bgP != 30) {
            org.mozilla1.javascript.Kit.codeBug();
        } else if (obj instanceof org.mozilla1.javascript.Scriptable) {
            aVar.bgA[aVar.bgO] = obj;
        }
        aVar.bgP = 0;
    }

    /* renamed from: e */
    public static NativeContinuation m39160e(org.mozilla1.javascript.Context lhVar) {
        if (lhVar.elG != null && (lhVar.elG instanceof C3604a)) {
            return m39128a(lhVar, (C3604a) lhVar.elG, true);
        }
        throw new IllegalStateException("Interpreter frames not found");
    }

    /* renamed from: a */
    private static NativeContinuation m39128a(org.mozilla1.javascript.Context lhVar, C3604a aVar, boolean z) {
        NativeContinuation ahs = new NativeContinuation();
        org.mozilla1.javascript.ScriptRuntime.m7528a((ScriptableObject) ahs, org.mozilla1.javascript.ScriptRuntime.getTopCallScope(lhVar));
        C3604a aVar2 = aVar;
        C3604a aVar3 = aVar;
        while (aVar3 != null && !aVar3.bgx) {
            aVar3.bgx = true;
            int i = aVar3.bgO;
            while (true) {
                i++;
                if (i == aVar3.bgA.length) {
                    break;
                }
                aVar3.bgA[i] = null;
                aVar3.bgB[i] = 0;
            }
            if (aVar3.bgP == 38) {
                aVar3.bgA[aVar3.bgO] = null;
            } else if (aVar3.bgP != 30) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            aVar2 = aVar3;
            aVar3 = aVar3.bgv;
        }
        while (aVar2.bgv != null) {
            aVar2 = aVar2.bgv;
        }
        if (!z || aVar2.bgI) {
            ahs.initImplementation(aVar);
            return ahs;
        }
        throw new IllegalStateException("Cannot capture continuation from JavaScript code not called directly by executeScriptWithContinuations or callFunctionWithContinuations");
    }

    /* renamed from: a */
    private static int m39126a(C3604a aVar, int i) {
        double number;
        Object obj = aVar.bgA[i];
        if (obj == org.mozilla1.javascript.UniqueTag.f5856nd) {
            number = aVar.bgC[i];
        } else {
            number = org.mozilla1.javascript.ScriptRuntime.toNumber(obj);
        }
        return org.mozilla1.javascript.ScriptRuntime.toInt32(number);
    }

    /* renamed from: b */
    private static double m39152b(C3604a aVar, int i) {
        Object obj = aVar.bgA[i];
        if (obj != org.mozilla1.javascript.UniqueTag.f5856nd) {
            return org.mozilla1.javascript.ScriptRuntime.toNumber(obj);
        }
        return aVar.bgC[i];
    }

    /* renamed from: c */
    private static boolean m39158c(C3604a aVar, int i) {
        Object obj = aVar.bgA[i];
        if (obj == Boolean.TRUE) {
            return true;
        }
        if (obj == Boolean.FALSE) {
            return false;
        }
        if (obj == org.mozilla1.javascript.UniqueTag.f5856nd) {
            double d = aVar.bgC[i];
            return d == d && d != org.mozilla1.javascript.ScriptRuntime.NaN;
        } else if (obj == null || obj == Undefined.instance) {
            return false;
        } else {
            if (obj instanceof Number) {
                double doubleValue = ((Number) obj).doubleValue();
                return doubleValue == doubleValue && doubleValue != org.mozilla1.javascript.ScriptRuntime.NaN;
            } else if (obj instanceof Boolean) {
                return ((Boolean) obj).booleanValue();
            } else {
                return org.mozilla1.javascript.ScriptRuntime.toBoolean(obj);
            }
        }
    }

    /* renamed from: a */
    private static void m39145a(Object[] objArr, double[] dArr, int i, org.mozilla1.javascript.Context lhVar) {
        double d;
        boolean z;
        Number number;
        String str;
        Number number2 = (Number) objArr[i + 1];
        String str2 = (String) objArr[i];
        if (number2 == org.mozilla1.javascript.UniqueTag.f5856nd) {
            d = dArr[i + 1];
            if (str2 == org.mozilla1.javascript.UniqueTag.f5856nd) {
                dArr[i] = dArr[i] + d;
                return;
            }
            z = true;
        } else if (str2 == org.mozilla1.javascript.UniqueTag.f5856nd) {
            d = dArr[i];
            z = false;
            str2 = number2;
        } else if ((str2 instanceof org.mozilla1.javascript.Scriptable) || (number2 instanceof org.mozilla1.javascript.Scriptable)) {
            objArr[i] = org.mozilla1.javascript.ScriptRuntime.m7570d((Object) str2, (Object) number2, lhVar);
            return;
        } else if (str2 instanceof String) {
            objArr[i] = str2.concat(org.mozilla1.javascript.ScriptRuntime.toString((Object) number2));
            return;
        } else if (number2 instanceof String) {
            objArr[i] = org.mozilla1.javascript.ScriptRuntime.toString((Object) str2).concat(number2);
            return;
        } else {
            double doubleValue = str2 instanceof Number ? ((Number) str2).doubleValue() : org.mozilla1.javascript.ScriptRuntime.toNumber((Object) str2);
            double doubleValue2 = number2 instanceof Number ? number2.doubleValue() : org.mozilla1.javascript.ScriptRuntime.toNumber((Object) number2);
            objArr[i] = org.mozilla1.javascript.UniqueTag.f5856nd;
            dArr[i] = doubleValue2 + doubleValue;
            return;
        }
        if (str2 instanceof org.mozilla1.javascript.Scriptable) {
            Number wrapNumber = org.mozilla1.javascript.ScriptRuntime.wrapNumber(d);
            if (!z) {
                number = wrapNumber;
                str = str2;
            } else {
                number = str2;
                str = wrapNumber;
            }
            objArr[i] = org.mozilla1.javascript.ScriptRuntime.m7570d((Object) number, (Object) str, lhVar);
        } else if (str2 instanceof String) {
            String str3 = str2;
            String nh = org.mozilla1.javascript.ScriptRuntime.toString(d);
            if (z) {
                objArr[i] = str3.concat(nh);
            } else {
                objArr[i] = nh.concat(str3);
            }
        } else {
            double doubleValue3 = str2 instanceof Number ? ((Number) str2).doubleValue() : org.mozilla1.javascript.ScriptRuntime.toNumber((Object) str2);
            objArr[i] = org.mozilla1.javascript.UniqueTag.f5856nd;
            dArr[i] = doubleValue3 + d;
        }
    }

    /* renamed from: a */
    private static Object[] m39147a(Object[] objArr, double[] dArr, int i, int i2) {
        if (i2 == 0) {
            return org.mozilla1.javascript.ScriptRuntime.emptyArgs;
        }
        Object[] objArr2 = new Object[i2];
        for (int i3 = 0; i3 != i2; i3++) {
            Number number = objArr[i];
            if (number == UniqueTag.f5856nd) {
                number = org.mozilla1.javascript.ScriptRuntime.wrapNumber(dArr[i]);
            }
            objArr2[i3] = number;
            i++;
        }
        return objArr2;
    }

    /* renamed from: a */
    private static void m39141a(org.mozilla1.javascript.Context lhVar, C3604a aVar, int i) {
        lhVar.elI += (aVar.f9158pc - aVar.bgM) + i;
        if (lhVar.elI > lhVar.elJ) {
            lhVar.observeInstructionCount(lhVar.elI);
            lhVar.elI = 0;
        }
    }

    /* renamed from: a */
    public Object mo5922a(CompilerEnvirons aki, ScriptOrFnNode aak, String str, boolean z) {
        this.f9155he = aki;
        new NodeTransformer().mo13266a(aak);
        if (z) {
            aak = aak.mo7704wM(0);
        }
        this.eYU = aak;
        this.hus = new org.mozilla1.javascript.InterpreterData(aki.getLanguageVersion(), this.eYU.getSourceName(), str);
        this.hus.frF = true;
        if (z) {
            cPN();
        } else {
            m39120L(this.eYU);
        }
        return this.hus;
    }

    /* renamed from: a */
    public Script mo5920a(Object obj, Object obj2) {
        if (obj != this.hus) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        return org.mozilla1.javascript.InterpretedFunction.m21537a(this.hus, obj2);
    }

    /* renamed from: a */
    public void mo5926a(Script afe) {
        ((org.mozilla1.javascript.InterpretedFunction) afe).bgz.frK = true;
    }

    /* renamed from: a */
    public Function mo5921a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object obj, Object obj2) {
        if (obj != this.hus) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        return org.mozilla1.javascript.InterpretedFunction.m21538a(lhVar, avf, this.hus, obj2);
    }

    private void cPN() {
        this.huq = true;
        org.mozilla1.javascript.FunctionNode yq = (org.mozilla1.javascript.FunctionNode) this.eYU;
        this.hus.eMN = yq.getFunctionType();
        this.hus.eMO = yq.requiresActivation();
        this.hus.frl = yq.getFunctionName();
        if (!yq.getIgnoreDynamicScope() && this.f9155he.isUseDynamicScope()) {
            this.hus.elw = true;
        }
        if (yq.bHQ()) {
            m39175wX(hum);
            m39177wZ(yq.getBaseLineno() & 65535);
        }
        m39120L(yq.cFF());
    }

    /* renamed from: L */
    private void m39120L(org.mozilla1.javascript.Node qlVar) {
        cPO();
        cPP();
        m39166n(qlVar, 0);
        byb();
        if (this.hus.eMN == 0) {
            addToken(64);
        }
        if (this.hus.frr.length != this.hut) {
            byte[] bArr = new byte[this.hut];
            System.arraycopy(this.hus.frr, 0, bArr, 0, this.hut);
            this.hus.frr = bArr;
        }
        if (this.huw.size() == 0) {
            this.hus.frn = null;
        } else {
            this.hus.frn = new String[this.huw.size()];
            ObjToIntMap.Iterator bRg = this.huw.newIterator();
            bRg.start();
            while (!bRg.done()) {
                String str = (String) bRg.getKey();
                int value = bRg.getValue();
                if (this.hus.frn[value] != null) {
                    org.mozilla1.javascript.Kit.codeBug();
                }
                this.hus.frn[value] = str;
                bRg.next();
            }
        }
        if (this.huv == 0) {
            this.hus.fro = null;
        } else if (this.hus.fro.length != this.huv) {
            double[] dArr = new double[this.huv];
            System.arraycopy(this.hus.fro, 0, dArr, 0, this.huv);
            this.hus.fro = dArr;
        }
        if (!(this.epC == 0 || this.hus.frs.length == this.epC)) {
            int[] iArr = new int[this.epC];
            System.arraycopy(this.hus.frs, 0, iArr, 0, this.epC);
            this.hus.frs = iArr;
        }
        this.hus.frt = this.eYU.getParamAndVarCount();
        this.hus.frw = this.hus.frt + this.hus.fru + this.hus.frv;
        this.hus.frx = this.eYU.getParamAndVarNames();
        this.hus.fry = this.eYU.cHr();
        this.hus.frz = this.eYU.getParamCount();
        this.hus.frC = this.eYU.getEncodedSourceStart();
        this.hus.frD = this.eYU.getEncodedSourceEnd();
        if (this.huy.size() != 0) {
            this.hus.frG = this.huy.toArray();
        }
    }

    private void cPO() {
        int functionCount = this.eYU.getFunctionCount();
        if (functionCount != 0) {
            org.mozilla1.javascript.InterpreterData[] aeqArr = new org.mozilla1.javascript.InterpreterData[functionCount];
            for (int i = 0; i != functionCount; i++) {
                FunctionNode wM = this.eYU.mo7704wM(i);
                Interpreter sqVar = new Interpreter();
                sqVar.f9155he = this.f9155he;
                sqVar.eYU = wM;
                sqVar.hus = new org.mozilla1.javascript.InterpreterData(this.hus);
                sqVar.cPN();
                aeqArr[i] = sqVar.hus;
            }
            this.hus.frp = aeqArr;
        }
    }

    private void cPP() {
        int regexpCount = this.eYU.getRegexpCount();
        if (regexpCount != 0) {
            org.mozilla1.javascript.Context bwA = org.mozilla1.javascript.Context.bwA();
            RegExpProxy m = org.mozilla1.javascript.ScriptRuntime.m7593m(bwA);
            Object[] objArr = new Object[regexpCount];
            for (int i = 0; i != regexpCount; i++) {
                objArr[i] = m.mo10823b(bwA, this.eYU.getRegexpString(i), this.eYU.getRegexpFlags(i));
            }
            this.hus.frq = objArr;
        }
    }

    /* renamed from: m */
    private void m39165m(org.mozilla1.javascript.Node qlVar) {
        int lineno = qlVar.getLineno();
        if (lineno != this.eZc && lineno >= 0) {
            if (this.hus.frI < 0) {
                this.hus.frI = lineno;
            }
            this.eZc = lineno;
            m39175wX(htB);
            m39177wZ(lineno & 65535);
        }
    }

    /* renamed from: M */
    private RuntimeException m39121M(org.mozilla1.javascript.Node qlVar) {
        throw new RuntimeException(qlVar.toString());
    }

    /* renamed from: n */
    private void m39166n(org.mozilla1.javascript.Node qlVar, int i) {
        int i2 = htg;
        int i3 = 0;
        int type = qlVar.getType();
        org.mozilla1.javascript.Node cFE = qlVar.cFE();
        switch (type) {
            case hum /*-62*/:
                break;
            case 2:
                m39167o(cFE, 0);
                addToken(2);
                m39182xe(-1);
                break;
            case 3:
                addToken(3);
                break;
            case 4:
                m39165m(qlVar);
                if (qlVar.getIntProp(20, 0) == 0) {
                    if (cFE == null) {
                        m39175wX(htx);
                        break;
                    } else {
                        m39167o(cFE, 1);
                        addToken(4);
                        m39182xe(-1);
                        break;
                    }
                } else {
                    m39175wX(hun);
                    m39177wZ(this.eZc & 65535);
                    break;
                }
            case 5:
                m39143a(((org.mozilla1.javascript.Node.C3367a) qlVar).aUM, type);
                break;
            case 6:
            case 7:
                org.mozilla1.javascript.Node qlVar2 = ((org.mozilla1.javascript.Node.C3367a) qlVar).aUM;
                m39167o(cFE, 0);
                m39143a(qlVar2, type);
                m39182xe(-1);
                break;
            case 50:
                m39165m(qlVar);
                m39167o(cFE, 0);
                addToken(50);
                m39177wZ(this.eZc & 65535);
                m39182xe(-1);
                break;
            case 51:
                m39165m(qlVar);
                m39151ax(51, m39123O(qlVar));
                break;
            case 57:
                int O = m39123O(qlVar);
                int existingIntProp = qlVar.getExistingIntProp(14);
                String string = cFE.getString();
                m39167o(cFE.cFG(), 0);
                m39164lx(string);
                m39180xc(O);
                addToken(57);
                if (existingIntProp != 0) {
                    i3 = 1;
                }
                m39176wY(i3);
                m39182xe(-1);
                break;
            case 58:
            case 59:
            case 60:
                m39167o(cFE, 0);
                m39151ax(type, m39123O(qlVar));
                m39182xe(-1);
                break;
            case 64:
                m39165m(qlVar);
                addToken(64);
                break;
            case 80:
                org.mozilla1.javascript.Node.C3367a aVar = (org.mozilla1.javascript.Node.C3367a) qlVar;
                int O2 = m39123O(aVar);
                int cPQ = cPQ();
                m39151ax(hto, cPQ);
                int i4 = this.hut;
                boolean z = this.hur;
                this.hur = true;
                for (org.mozilla1.javascript.Node qlVar3 = cFE; qlVar3 != null; qlVar3 = qlVar3.cFG()) {
                    m39166n(qlVar3, i);
                }
                this.hur = z;
                org.mozilla1.javascript.Node qlVar4 = aVar.aUM;
                if (qlVar4 != null) {
                    int i5 = this.epT[m39163k(qlVar4)];
                    m39138a(i4, i5, i5, false, O2, cPQ);
                }
                org.mozilla1.javascript.Node WP = aVar.mo21497WP();
                if (WP != null) {
                    int i6 = this.epT[m39163k(WP)];
                    m39138a(i4, i6, i6, true, O2, cPQ);
                }
                m39151ax(hug, cPQ);
                m39183xf(cPQ);
                break;
            case 108:
                int existingIntProp2 = qlVar.getExistingIntProp(1);
                int functionType = this.eYU.mo7704wM(existingIntProp2).getFunctionType();
                if (functionType == 3) {
                    m39151ax(htv, existingIntProp2);
                } else if (functionType != 1) {
                    throw org.mozilla1.javascript.Kit.codeBug();
                }
                if (!this.huq) {
                    m39151ax(htu, existingIntProp2);
                    m39182xe(1);
                    m39175wX(htg);
                    m39182xe(-1);
                    break;
                }
                break;
            case 113:
                m39165m(qlVar);
                m39167o(cFE, 0);
                for (org.mozilla1.javascript.Node.C3367a aVar2 = (org.mozilla1.javascript.Node.C3367a) cFE.cFG(); aVar2 != null; aVar2 = (org.mozilla1.javascript.Node.C3367a) aVar2.cFG()) {
                    if (aVar2.getType() != 114) {
                        throw m39121M(aVar2);
                    }
                    org.mozilla1.javascript.Node cFE2 = aVar2.cFE();
                    m39175wX(-1);
                    m39182xe(1);
                    m39167o(cFE2, 0);
                    addToken(46);
                    m39182xe(-1);
                    m39143a(aVar2.aUM, (int) hth);
                    m39182xe(-1);
                }
                m39175wX(htf);
                m39182xe(-1);
                break;
            case 122:
            case 127:
            case 128:
            case 129:
            case 131:
                m39165m(qlVar);
                break;
            case 124:
                m39182xe(1);
                int O3 = m39123O(qlVar);
                m39151ax(htz, O3);
                m39182xe(-1);
                while (cFE != null) {
                    m39166n(cFE, i);
                    cFE = cFE.cFG();
                }
                m39151ax(htA, O3);
                break;
            case 130:
                m39124P(qlVar);
                break;
            case 132:
            case 133:
                m39165m(qlVar);
                m39167o(cFE, 0);
                if (type == 132) {
                    i2 = htf;
                }
                m39175wX(i2);
                m39182xe(-1);
                break;
            case 134:
                m39143a(((org.mozilla1.javascript.Node.C3367a) qlVar).aUM, (int) hty);
                break;
            case 135:
                break;
            case 140:
                int cPQ2 = cPQ();
                qlVar.putIntProp(2, cPQ2);
                m39165m(qlVar);
                while (cFE != null) {
                    m39166n(cFE, i);
                    cFE = cFE.cFG();
                }
                m39151ax(hug, cPQ2);
                m39183xf(cPQ2);
                break;
            case 159:
                m39175wX(-64);
                break;
            default:
                throw m39121M(qlVar);
        }
        while (cFE != null) {
            m39166n(cFE, i);
            cFE = cFE.cFG();
        }
        if (this.huu != i) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
    }

    /* renamed from: o */
    private void m39167o(org.mozilla1.javascript.Node qlVar, int i) {
        int i2;
        int i3;
        int i4 = 0;
        int type = qlVar.getType();
        org.mozilla1.javascript.Node cFE = qlVar.cFE();
        int i5 = this.huu;
        switch (type) {
            case 8:
                String string = cFE.getString();
                m39167o(cFE, 0);
                m39167o(cFE.cFG(), 0);
                m39162f(8, string);
                m39182xe(-1);
                break;
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 31:
            case 36:
            case 46:
            case 47:
            case 52:
            case 53:
                m39167o(cFE, 0);
                m39167o(cFE.cFG(), 0);
                addToken(type);
                m39182xe(-1);
                break;
            case 26:
            case 27:
            case 28:
            case 29:
            case 32:
            case 125:
                m39167o(cFE, 0);
                if (type != 125) {
                    addToken(type);
                    break;
                } else {
                    m39175wX(htf);
                    m39175wX(htZ);
                    break;
                }
            case 30:
            case 38:
            case 70:
                if (type == 30) {
                    m39167o(cFE, 0);
                } else {
                    m39122N(cFE);
                }
                int i6 = 0;
                while (true) {
                    cFE = cFE.cFG();
                    if (cFE == null) {
                        int intProp = qlVar.getIntProp(10, 0);
                        if (intProp != 0) {
                            m39151ax(htw, i6);
                            m39176wY(intProp);
                            if (type == 30) {
                                i4 = 1;
                            }
                            m39176wY(i4);
                            m39177wZ(this.eZc & 65535);
                        } else {
                            if (type != 38 || (i & 1) == 0 || this.f9155he.isGenerateDebugInfo() || this.hur) {
                                i3 = type;
                            } else {
                                i3 = huf;
                            }
                            m39151ax(i3, i6);
                            type = i3;
                        }
                        if (type == 30) {
                            m39182xe(-i6);
                        } else {
                            m39182xe(-1 - i6);
                        }
                        if (i6 > this.hus.frA) {
                            this.hus.frA = i6;
                            break;
                        }
                    } else {
                        m39167o(cFE, 0);
                        i6++;
                    }
                }
                break;
            case 33:
            case 34:
                m39167o(cFE, 0);
                m39162f(type, cFE.cFG().getString());
                break;
            case 35:
            case 138:
                m39167o(cFE, 0);
                org.mozilla1.javascript.Node cFG = cFE.cFG();
                String string2 = cFG.getString();
                org.mozilla1.javascript.Node cFG2 = cFG.cFG();
                if (type == 138) {
                    m39175wX(-1);
                    m39182xe(1);
                    m39162f(33, string2);
                    m39182xe(-1);
                }
                m39167o(cFG2, 0);
                m39162f(35, string2);
                m39182xe(-1);
                break;
            case 37:
            case 139:
                m39167o(cFE, 0);
                org.mozilla1.javascript.Node cFG3 = cFE.cFG();
                m39167o(cFG3, 0);
                org.mozilla1.javascript.Node cFG4 = cFG3.cFG();
                if (type == 139) {
                    m39175wX(-2);
                    m39182xe(2);
                    addToken(36);
                    m39182xe(-1);
                    m39182xe(-1);
                }
                m39167o(cFG4, 0);
                addToken(37);
                m39182xe(-2);
                break;
            case 39:
            case 41:
            case 49:
                m39162f(type, qlVar.getString());
                m39182xe(1);
                break;
            case 40:
                double d = qlVar.getDouble();
                int i7 = (int) d;
                if (((double) i7) != d) {
                    m39151ax(40, m39125X(d));
                } else if (i7 == 0) {
                    m39175wX(hua);
                    if (1.0d / d < ScriptRuntime.NaN) {
                        addToken(29);
                    }
                } else if (i7 == 1) {
                    m39175wX(huc);
                } else if (((short) i7) == i7) {
                    m39175wX(htC);
                    m39177wZ(65535 & i7);
                } else {
                    m39175wX(htD);
                    m39178xa(i7);
                }
                m39182xe(1);
                break;
            case 42:
            case 43:
            case 44:
            case 45:
            case 63:
                addToken(type);
                m39182xe(1);
                break;
            case 48:
                m39151ax(48, qlVar.getExistingIntProp(4));
                m39182xe(1);
                break;
            case 54:
                m39151ax(54, m39123O(qlVar));
                m39182xe(1);
                break;
            case 55:
                if (this.hus.eMO) {
                    org.mozilla1.javascript.Kit.codeBug();
                }
                m39150aw(55, this.eYU.mo7678K(qlVar));
                m39182xe(1);
                break;
            case 56:
                if (this.hus.eMO) {
                    org.mozilla1.javascript.Kit.codeBug();
                }
                int K = this.eYU.mo7678K(cFE);
                m39167o(cFE.cFG(), 0);
                m39150aw(56, K);
                break;
            case 61:
            case 62:
                m39151ax(type, m39123O(qlVar));
                m39182xe(1);
                break;
            case 65:
            case 66:
                m39169t(qlVar, cFE);
                break;
            case 67:
            case 69:
                m39167o(cFE, 0);
                addToken(type);
                break;
            case 68:
            case 141:
                m39167o(cFE, 0);
                org.mozilla1.javascript.Node cFG5 = cFE.cFG();
                if (type == 141) {
                    m39175wX(-1);
                    m39182xe(1);
                    addToken(67);
                    m39182xe(-1);
                }
                m39167o(cFG5, 0);
                addToken(68);
                m39182xe(-1);
                break;
            case 71:
                m39167o(cFE, 0);
                m39162f(type, (String) qlVar.getProp(17));
                break;
            case 72:
                if (cFE != null) {
                    m39167o(cFE, 0);
                } else {
                    m39175wX(htZ);
                    m39182xe(1);
                }
                addToken(72);
                m39177wZ(qlVar.getLineno() & 65535);
                break;
            case 73:
            case 74:
            case 75:
                m39167o(cFE, 0);
                addToken(type);
                break;
            case 76:
            case 77:
            case 78:
            case 79:
                int intProp2 = qlVar.getIntProp(16, 0);
                int i8 = 0;
                do {
                    m39167o(cFE, 0);
                    i8++;
                    cFE = cFE.cFG();
                } while (cFE != null);
                m39151ax(type, intProp2);
                m39182xe(1 - i8);
                break;
            case 88:
                org.mozilla1.javascript.Node cFF = qlVar.cFF();
                while (cFE != cFF) {
                    m39167o(cFE, 0);
                    m39175wX(htf);
                    m39182xe(-1);
                    cFE = cFE.cFG();
                }
                m39167o(cFE, i & 1);
                break;
            case 101:
                org.mozilla1.javascript.Node cFG6 = cFE.cFG();
                org.mozilla1.javascript.Node cFG7 = cFG6.cFG();
                m39167o(cFE, 0);
                int i9 = this.hut;
                m39179xb(7);
                m39182xe(-1);
                m39167o(cFG6, i & 1);
                int i10 = this.hut;
                m39179xb(5);
                m39174wW(i9);
                this.huu = i5;
                m39167o(cFG7, i & 1);
                m39174wW(i10);
                break;
            case 103:
            case 104:
                m39167o(cFE, 0);
                m39175wX(-1);
                m39182xe(1);
                int i11 = this.hut;
                m39179xb(type == 104 ? 7 : 6);
                m39182xe(-1);
                m39175wX(htf);
                m39182xe(-1);
                m39167o(cFE.cFG(), i & 1);
                m39174wW(i11);
                break;
            case 105:
            case 106:
                m39168s(qlVar, cFE);
                break;
            case 108:
                int existingIntProp = qlVar.getExistingIntProp(1);
                if (this.eYU.mo7704wM(existingIntProp).getFunctionType() == 2) {
                    m39151ax(htu, existingIntProp);
                    m39182xe(1);
                    break;
                } else {
                    throw org.mozilla1.javascript.Kit.codeBug();
                }
            case 136:
                if (!this.huq || this.hus.eMO) {
                    i2 = -1;
                } else {
                    i2 = this.eYU.mo7678K(qlVar);
                }
                if (i2 != -1) {
                    m39150aw(55, i2);
                    m39182xe(1);
                    addToken(32);
                    break;
                } else {
                    m39162f(htp, qlVar.getString());
                    m39182xe(1);
                    break;
                }
                break;
            case 137:
                m39182xe(1);
                break;
            case 145:
                m39165m(qlVar);
                m39167o(cFE, 0);
                m39175wX(hud);
                m39182xe(-1);
                int i12 = this.hut;
                m39167o(cFE.cFG(), 0);
                m39148au(hue, i12);
                break;
            case 154:
                String string3 = cFE.getString();
                m39167o(cFE, 0);
                m39167o(cFE.cFG(), 0);
                m39162f(huj, string3);
                m39182xe(-1);
                break;
            case 155:
                if (this.hus.eMO) {
                    org.mozilla1.javascript.Kit.codeBug();
                }
                int K2 = this.eYU.mo7678K(cFE);
                m39167o(cFE.cFG(), 0);
                m39150aw(155, K2);
                break;
            case 156:
                m39161e(qlVar, cFE, cFE.cFG());
                break;
            case 158:
                org.mozilla1.javascript.Node cFE2 = qlVar.cFE();
                org.mozilla1.javascript.Node cFG8 = cFE2.cFG();
                m39167o(cFE2.cFE(), 0);
                addToken(2);
                m39182xe(-1);
                m39167o(cFG8.cFE(), 0);
                addToken(3);
                break;
            default:
                throw m39121M(qlVar);
        }
        if (i5 + 1 != this.huu) {
            org.mozilla1.javascript.Kit.codeBug();
        }
    }

    /* renamed from: N */
    private void m39122N(org.mozilla1.javascript.Node qlVar) {
        int type = qlVar.getType();
        switch (type) {
            case 33:
            case 36:
                org.mozilla1.javascript.Node cFE = qlVar.cFE();
                m39167o(cFE, 0);
                org.mozilla1.javascript.Node cFG = cFE.cFG();
                if (type == 33) {
                    m39162f(htr, cFG.getString());
                    m39182xe(1);
                    return;
                }
                m39167o(cFG, 0);
                m39175wX(hts);
                return;
            case 39:
                m39162f(htq, qlVar.getString());
                m39182xe(2);
                return;
            default:
                m39167o(qlVar, 0);
                m39175wX(htt);
                m39182xe(1);
                return;
        }
    }

    /* renamed from: s */
    private void m39168s(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        int existingIntProp = qlVar.getExistingIntProp(13);
        switch (qlVar2.getType()) {
            case 33:
                org.mozilla1.javascript.Node cFE = qlVar2.cFE();
                m39167o(cFE, 0);
                m39162f(htk, cFE.cFG().getString());
                m39176wY(existingIntProp);
                return;
            case 36:
                org.mozilla1.javascript.Node cFE2 = qlVar2.cFE();
                m39167o(cFE2, 0);
                m39167o(cFE2.cFG(), 0);
                m39175wX(-10);
                m39176wY(existingIntProp);
                m39182xe(-1);
                return;
            case 39:
                m39162f(htj, qlVar2.getString());
                m39176wY(existingIntProp);
                m39182xe(1);
                return;
            case 55:
                if (this.hus.eMO) {
                    org.mozilla1.javascript.Kit.codeBug();
                }
                m39150aw(hti, this.eYU.mo7678K(qlVar2));
                m39176wY(existingIntProp);
                m39182xe(1);
                return;
            case 67:
                m39167o(qlVar2.cFE(), 0);
                m39175wX(htm);
                m39176wY(existingIntProp);
                return;
            default:
                throw m39121M(qlVar);
        }
    }

    /* renamed from: t */
    private void m39169t(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2) {
        int length;
        int type = qlVar.getType();
        Object[] objArr = null;
        if (type == 65) {
            length = 0;
            for (org.mozilla1.javascript.Node qlVar3 = qlVar2; qlVar3 != null; qlVar3 = qlVar3.cFG()) {
                length++;
            }
        } else if (type == 66) {
            objArr = (Object[]) qlVar.getProp(12);
            length = objArr.length;
        } else {
            throw m39121M(qlVar);
        }
        m39151ax(htE, length);
        m39182xe(2);
        while (qlVar2 != null) {
            int type2 = qlVar2.getType();
            if (type2 == 150) {
                m39167o(qlVar2.cFE(), 0);
                m39175wX(huh);
            } else if (type2 == 151) {
                m39167o(qlVar2.cFE(), 0);
                m39175wX(hui);
            } else {
                m39167o(qlVar2, 0);
                m39175wX(htF);
            }
            m39182xe(-1);
            qlVar2 = qlVar2.cFG();
        }
        if (type == 65) {
            int[] iArr = (int[]) qlVar.getProp(11);
            if (iArr == null) {
                addToken(65);
            } else {
                int size = this.huy.size();
                this.huy.add(iArr);
                m39151ax(htG, size);
            }
        } else {
            int size2 = this.huy.size();
            this.huy.add(objArr);
            m39151ax(66, size2);
        }
        m39182xe(-1);
    }

    /* renamed from: e */
    private void m39161e(org.mozilla1.javascript.Node qlVar, org.mozilla1.javascript.Node qlVar2, org.mozilla1.javascript.Node qlVar3) {
        m39166n(qlVar2, this.huu);
        m39167o(qlVar3, 0);
    }

    /* renamed from: O */
    private int m39123O(org.mozilla1.javascript.Node qlVar) {
        return ((org.mozilla1.javascript.Node) qlVar.getProp(3)).getExistingIntProp(2);
    }

    /* renamed from: k */
    private int m39163k(org.mozilla1.javascript.Node qlVar) {
        int labelId = qlVar.labelId();
        if (labelId == -1) {
            labelId = this.epU;
            if (this.epT == null || labelId == this.epT.length) {
                if (this.epT == null) {
                    this.epT = new int[32];
                } else {
                    int[] iArr = new int[(this.epT.length * 2)];
                    System.arraycopy(this.epT, 0, iArr, 0, labelId);
                    this.epT = iArr;
                }
            }
            this.epU = labelId + 1;
            this.epT[labelId] = -1;
            qlVar.labelId(labelId);
        }
        return labelId;
    }

    /* renamed from: P */
    private void m39124P(org.mozilla1.javascript.Node qlVar) {
        int k = m39163k(qlVar);
        if (this.epT[k] != -1) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        this.epT[k] = this.hut;
    }

    /* renamed from: a */
    private void m39143a(Node qlVar, int i) {
        int k = m39163k(qlVar);
        if (k >= this.epU) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        int i2 = this.epT[k];
        if (i2 != -1) {
            m39148au(i, i2);
            return;
        }
        int i3 = this.hut;
        m39179xb(i);
        int i4 = this.epX;
        if (this.epW == null || i4 == this.epW.length) {
            if (this.epW == null) {
                this.epW = new long[40];
            } else {
                long[] jArr = new long[(this.epW.length * 2)];
                System.arraycopy(this.epW, 0, jArr, 0, i4);
                this.epW = jArr;
            }
        }
        this.epX = i4 + 1;
        this.epW[i4] = ((long) i3) | (((long) k) << 32);
    }

    private void byb() {
        for (int i = 0; i < this.epX; i++) {
            long j = this.epW[i];
            int i2 = (int) j;
            int i3 = this.epT[(int) (j >> 32)];
            if (i3 == -1) {
                throw org.mozilla1.javascript.Kit.codeBug();
            }
            m39149av(i2, i3);
        }
        this.epX = 0;
    }

    /* renamed from: au */
    private void m39148au(int i, int i2) {
        int i3 = this.hut;
        if (i3 <= i2) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        m39179xb(i);
        m39149av(i3, i2);
    }

    /* renamed from: wW */
    private void m39174wW(int i) {
        if (this.hut < i + 3) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        m39149av(i, this.hut);
    }

    /* renamed from: av */
    private void m39149av(int i, int i2) {
        int i3 = i2 - i;
        if (i3 < 0 || i3 > 2) {
            int i4 = i + 1;
            if (i3 != ((short) i3)) {
                if (this.hus.frH == null) {
                    this.hus.frH = new UintMap();
                }
                this.hus.frH.put(i4, i2);
                i3 = 0;
            }
            byte[] bArr = this.hus.frr;
            bArr[i4] = (byte) (i3 >> 8);
            bArr[i4 + 1] = (byte) i3;
            return;
        }
        throw org.mozilla1.javascript.Kit.codeBug();
    }

    private void addToken(int i) {
        if (!m39172wU(i)) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        m39176wY(i);
    }

    /* renamed from: wX */
    private void m39175wX(int i) {
        if (!m39171wT(i)) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        m39176wY(i & 255);
    }

    /* renamed from: wY */
    private void m39176wY(int i) {
        if ((i & -256) != 0) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        byte[] bArr = this.hus.frr;
        int i2 = this.hut;
        if (i2 == bArr.length) {
            bArr = m39181xd(1);
        }
        bArr[i2] = (byte) i;
        this.hut = i2 + 1;
    }

    /* renamed from: wZ */
    private void m39177wZ(int i) {
        if ((-65536 & i) != 0) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        byte[] bArr = this.hus.frr;
        int i2 = this.hut;
        if (i2 + 2 > bArr.length) {
            bArr = m39181xd(2);
        }
        bArr[i2] = (byte) (i >>> 8);
        bArr[i2 + 1] = (byte) i;
        this.hut = i2 + 2;
    }

    /* renamed from: xa */
    private void m39178xa(int i) {
        byte[] bArr = this.hus.frr;
        int i2 = this.hut;
        if (i2 + 4 > bArr.length) {
            bArr = m39181xd(4);
        }
        bArr[i2] = (byte) (i >>> 24);
        bArr[i2 + 1] = (byte) (i >>> 16);
        bArr[i2 + 2] = (byte) (i >>> 8);
        bArr[i2 + 3] = (byte) i;
        this.hut = i2 + 4;
    }

    /* renamed from: X */
    private int m39125X(double d) {
        int i = this.huv;
        if (i == 0) {
            this.hus.fro = new double[64];
        } else if (this.hus.fro.length == i) {
            double[] dArr = new double[(i * 2)];
            System.arraycopy(this.hus.fro, 0, dArr, 0, i);
            this.hus.fro = dArr;
        }
        this.hus.fro[i] = d;
        this.huv = i + 1;
        return i;
    }

    /* renamed from: xb */
    private void m39179xb(int i) {
        byte[] bArr = this.hus.frr;
        int i2 = this.hut;
        if (i2 + 3 > bArr.length) {
            bArr = m39181xd(3);
        }
        bArr[i2] = (byte) i;
        this.hut = i2 + 1 + 2;
    }

    /* renamed from: aw */
    private void m39150aw(int i, int i2) {
        switch (i) {
            case hti /*-7*/:
                break;
            case 55:
            case 56:
                if (i2 < 128) {
                    m39175wX(i == 55 ? htX : htY);
                    m39176wY(i2);
                    return;
                }
                break;
            case 155:
                if (i2 < 128) {
                    m39175wX(hul);
                    m39176wY(i2);
                    return;
                }
                m39151ax(huk, i2);
                return;
            default:
                throw org.mozilla1.javascript.Kit.codeBug();
        }
        m39151ax(i, i2);
    }

    /* renamed from: f */
    private void m39162f(int i, String str) {
        m39164lx(str);
        if (m39171wT(i)) {
            m39175wX(i);
        } else {
            addToken(i);
        }
    }

    /* renamed from: ax */
    private void m39151ax(int i, int i2) {
        m39180xc(i2);
        if (m39171wT(i)) {
            m39175wX(i);
        } else {
            addToken(i);
        }
    }

    /* renamed from: lx */
    private void m39164lx(String str) {
        int i = this.huw.get(str, -1);
        if (i == -1) {
            i = this.huw.size();
            this.huw.put(str, i);
        }
        if (i < 4) {
            m39175wX(-41 - i);
        } else if (i <= 255) {
            m39175wX(htU);
            m39176wY(i);
        } else if (i <= 65535) {
            m39175wX(htV);
            m39177wZ(i);
        } else {
            m39175wX(htW);
            m39178xa(i);
        }
    }

    /* renamed from: xc */
    private void m39180xc(int i) {
        if (i < 0) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        if (i < 6) {
            m39175wX(-32 - i);
        } else if (i <= 255) {
            m39175wX(htN);
            m39176wY(i);
        } else if (i <= 65535) {
            m39175wX(htO);
            m39177wZ(i);
        } else {
            m39175wX(htP);
            m39178xa(i);
        }
    }

    /* renamed from: a */
    private void m39138a(int i, int i2, int i3, boolean z, int i4, int i5) {
        int i6 = 0;
        int i7 = this.epC;
        int[] iArr = this.hus.frs;
        if (iArr == null) {
            if (i7 != 0) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            iArr = new int[12];
            this.hus.frs = iArr;
        } else if (iArr.length == i7) {
            iArr = new int[(iArr.length * 2)];
            System.arraycopy(this.hus.frs, 0, iArr, 0, i7);
            this.hus.frs = iArr;
        }
        iArr[i7 + 0] = i;
        iArr[i7 + 1] = i2;
        iArr[i7 + 2] = i3;
        int i8 = i7 + 3;
        if (z) {
            i6 = 1;
        }
        iArr[i8] = i6;
        iArr[i7 + 4] = i4;
        iArr[i7 + 5] = i5;
        this.epC = i7 + 6;
    }

    /* renamed from: xd */
    private byte[] m39181xd(int i) {
        int length = this.hus.frr.length;
        int i2 = this.hut;
        if (i2 + i <= length) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        int i3 = length * 2;
        if (i2 + i > i3) {
            i3 = i2 + i;
        }
        byte[] bArr = new byte[i3];
        System.arraycopy(this.hus.frr, 0, bArr, 0, i2);
        this.hus.frr = bArr;
        return bArr;
    }

    /* renamed from: xe */
    private void m39182xe(int i) {
        if (i <= 0) {
            this.huu += i;
            return;
        }
        int i2 = this.huu + i;
        if (i2 > this.hus.frv) {
            this.hus.frv = i2;
        }
        this.huu = i2;
    }

    private int cPQ() {
        int i = this.hux;
        this.hux++;
        if (this.hux > this.hus.fru) {
            this.hus.fru = this.hux;
        }
        return i;
    }

    /* renamed from: xf */
    private void m39183xf(int i) {
        this.hux--;
        if (i != this.hux) {
            org.mozilla1.javascript.Kit.codeBug();
        }
    }

    /* renamed from: a */
    public void mo5925a(RhinoException pu) {
        C3604a[] aVarArr;
        int i;
        int i2 = 0;
        org.mozilla1.javascript.Context bwq = org.mozilla1.javascript.Context.bwq();
        if (bwq == null || bwq.elG == null) {
            pu.eKX = null;
            pu.eKY = null;
            return;
        }
        if (bwq.elH == null || bwq.elH.size() == 0) {
            aVarArr = new C3604a[1];
        } else {
            int size = bwq.elH.size();
            if (bwq.elH.peek() == bwq.elG) {
                size--;
            }
            C3604a[] aVarArr2 = new C3604a[(size + 1)];
            bwq.elH.toArray(aVarArr2);
            aVarArr = aVarArr2;
        }
        aVarArr[aVarArr.length - 1] = (C3604a) bwq.elG;
        int i3 = 0;
        while (true) {
            i = i2;
            if (i3 == aVarArr.length) {
                break;
            }
            i2 = aVarArr[i3].bgw + 1 + i;
            i3++;
        }
        int[] iArr = new int[i];
        int length = aVarArr.length;
        int i4 = i;
        while (length != 0) {
            int i5 = length - 1;
            for (C3604a aVar = aVarArr[i5]; aVar != null; aVar = aVar.bgv) {
                i4--;
                iArr[i4] = aVar.bgN;
            }
            length = i5;
        }
        if (i4 != 0) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        pu.eKX = aVarArr;
        pu.eKY = iArr;
    }

    /* renamed from: a */
    public String mo5924a(Context lhVar, int[] iArr) {
        C3604a aVar = (C3604a) lhVar.elG;
        org.mozilla1.javascript.InterpreterData aeq = aVar.bgz;
        if (aVar.bgN >= 0) {
            iArr[0] = m39159d(aeq.frr, aVar.bgN);
        } else {
            iArr[0] = 0;
        }
        return aeq.frm;
    }

    /* renamed from: a */
    public String mo5923a(RhinoException pu, String str) {
        StringBuffer stringBuffer = new StringBuffer(str.length() + 1000);
        String systemProperty = SecurityUtilities.getSystemProperty("line.separator");
        C3604a[] aVarArr = (C3604a[]) pu.eKX;
        int[] iArr = pu.eKY;
        int length = aVarArr.length;
        int length2 = iArr.length;
        int i = 0;
        while (length != 0) {
            length--;
            int indexOf = str.indexOf("org.mozilla.javascript.Interpreter.interpretLoop", i);
            if (indexOf < 0) {
                break;
            }
            int length3 = indexOf + "org.mozilla.javascript.Interpreter.interpretLoop".length();
            while (length3 != str.length() && (r9 = str.charAt(length3)) != 10 && r9 != 13) {
                length3++;
            }
            stringBuffer.append(str.substring(i, length3));
            for (C3604a aVar = aVarArr[length]; aVar != null; aVar = aVar.bgv) {
                if (length2 == 0) {
                    org.mozilla1.javascript.Kit.codeBug();
                }
                length2--;
                org.mozilla1.javascript.InterpreterData aeq = aVar.bgz;
                stringBuffer.append(systemProperty);
                stringBuffer.append("\tat game.script");
                if (!(aeq.frl == null || aeq.frl.length() == 0)) {
                    stringBuffer.append('.');
                    stringBuffer.append(aeq.frl);
                }
                stringBuffer.append('(');
                stringBuffer.append(aeq.frm);
                int i2 = iArr[length2];
                if (i2 >= 0) {
                    stringBuffer.append(':');
                    stringBuffer.append(m39159d(aeq.frr, i2));
                }
                stringBuffer.append(')');
            }
            i = length3;
        }
        stringBuffer.append(str.substring(i));
        return stringBuffer.toString();
    }

    /* renamed from: b */
    public List<String> mo5927b(RhinoException pu) {
        if (pu.eKX == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        String systemProperty = SecurityUtilities.getSystemProperty("line.separator");
        C3604a[] aVarArr = (C3604a[]) pu.eKX;
        int[] iArr = pu.eKY;
        int length = aVarArr.length;
        int length2 = iArr.length;
        while (length != 0) {
            int i = length - 1;
            StringBuilder sb = new StringBuilder();
            for (C3604a aVar = aVarArr[i]; aVar != null; aVar = aVar.bgv) {
                if (length2 == 0) {
                    org.mozilla1.javascript.Kit.codeBug();
                }
                length2--;
                org.mozilla1.javascript.InterpreterData aeq = aVar.bgz;
                sb.append("\tat ");
                sb.append(aeq.frm);
                int i2 = iArr[length2];
                if (i2 >= 0) {
                    sb.append(':');
                    sb.append(m39159d(aeq.frr, i2));
                }
                if (!(aeq.frl == null || aeq.frl.length() == 0)) {
                    sb.append(" (");
                    sb.append(aeq.frl);
                    sb.append(')');
                }
                sb.append(systemProperty);
            }
            arrayList.add(sb.toString());
            length = i;
        }
        return arrayList;
    }

    /* renamed from: a.sq$a */
    private static class C3604a implements Serializable, Cloneable {
        static final long serialVersionUID = -2843792508994958978L;

        /* renamed from: TR */
        org.mozilla1.javascript.Scriptable f9156TR;
        Object[] bgA;
        int[] bgB;
        double[] bgC;
        C3604a bgD;
        int bgE;
        int bgF;
        DebugFrame bgG;
        boolean bgH;
        boolean bgI;
        org.mozilla1.javascript.Scriptable[] bgJ;
        Object bgK;
        double bgL;
        int bgM;
        int bgN;
        int bgO;
        int bgP;
        Object bgQ;
        C3604a bgv;
        int bgw;
        boolean bgx;
        org.mozilla1.javascript.InterpretedFunction bgy;
        org.mozilla1.javascript.InterpreterData bgz;

        /* renamed from: od */
        Scriptable f9157od;

        /* renamed from: pc */
        int f9158pc;

        private C3604a() {
        }

        /* synthetic */ C3604a(C3604a aVar) {
            this();
        }

        /* access modifiers changed from: package-private */
        public C3604a abD() {
            if (!this.bgx) {
                org.mozilla1.javascript.Kit.codeBug();
            }
            try {
                C3604a aVar = (C3604a) clone();
                aVar.bgA = (Object[]) this.bgA.clone();
                aVar.bgB = (int[]) this.bgB.clone();
                aVar.bgC = (double[]) this.bgC.clone();
                aVar.bgx = false;
                return aVar;
            } catch (CloneNotSupportedException e) {
                throw new IllegalStateException();
            }
        }
    }

    /* renamed from: a.sq$b */
    /* compiled from: a */
    private static final class C3605b implements Serializable {
        static final long serialVersionUID = 7687739156004308247L;
        Object bgK;
        double bgL;
        C3604a cuY;
        C3604a cuZ;

        C3605b(NativeContinuation ahs, C3604a aVar) {
            C3604a aVar2;
            C3604a aVar3;
            this.cuY = (C3604a) ahs.getImplementation();
            if (this.cuY == null || aVar == null) {
                this.cuZ = null;
                return;
            }
            C3604a aVar4 = this.cuY;
            int i = aVar4.bgw - aVar.bgw;
            if (i != 0) {
                if (i < 0) {
                    aVar2 = this.cuY;
                    i = -i;
                    aVar4 = aVar;
                } else {
                    aVar2 = aVar;
                }
                while (true) {
                    aVar3 = aVar4.bgv;
                    i--;
                    if (i == 0) {
                        break;
                    }
                    aVar4 = aVar3;
                }
                if (aVar3.bgw != aVar2.bgw) {
                    org.mozilla1.javascript.Kit.codeBug();
                    aVar4 = aVar3;
                } else {
                    aVar4 = aVar3;
                }
            } else {
                aVar2 = aVar;
            }
            while (aVar4 != aVar2 && aVar4 != null) {
                C3604a aVar5 = aVar4.bgv;
                aVar2 = aVar2.bgv;
                aVar4 = aVar5;
            }
            this.cuZ = aVar4;
            if (this.cuZ != null && !this.cuZ.bgx) {
                Kit.codeBug();
            }
        }
    }

    /* renamed from: a.sq$c */
    /* compiled from: a */
    static class C3606c {
        RuntimeException gqP;
        int operation;
        Object value;

        C3606c(int i, Object obj) {
            this.operation = i;
            this.value = obj;
        }
    }
}
