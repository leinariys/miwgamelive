package org.mozilla1.javascript;


/* renamed from: a.azg  reason: case insensitive filesystem */
/* compiled from: a */
public interface Function extends Scriptable, Callable {
    Object call(org.mozilla1.javascript.Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr);

    Scriptable construct(Context lhVar, Scriptable avf, Object[] objArr);
}
