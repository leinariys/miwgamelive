package org.mozilla1.javascript;

import java.util.ArrayList;

/* renamed from: a.aAk  reason: case insensitive filesystem */
/* compiled from: a */
public class ScriptOrFnNode extends Node.Scope {
    private String dOJ;
    private int frC;
    private int frD;
    private int hcT = -1;
    private ObjArray hcU;
    private ObjArray hcV;
    private ArrayList<Node.C3370d> hcW = new ArrayList<>(4);
    private int hcX = 0;
    private String[] hcY;
    private boolean[] hcZ;
    private Object hda;
    private int hdb = 0;

    public ScriptOrFnNode(int i) {
        super(i);
        mo21506b((Scope) null);
    }

    public final String getSourceName() {
        return this.dOJ;
    }

    public final void setSourceName(String str) {
        this.dOJ = str;
    }

    public final int getEncodedSourceStart() {
        return this.frC;
    }

    public final int getEncodedSourceEnd() {
        return this.frD;
    }

    public final void setEncodedSourceBounds(int i, int i2) {
        this.frC = i;
        this.frD = i2;
    }

    public final int getBaseLineno() {
        return this.f8955iw;
    }

    public final void setBaseLineno(int i) {
        if (i < 0 || this.f8955iw >= 0) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        this.f8955iw = i;
    }

    public final int getEndLineno() {
        return this.hcT;
    }

    public final void setEndLineno(int i) {
        if (i < 0 || this.hcT >= 0) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        this.hcT = i;
    }

    public final int getFunctionCount() {
        if (this.hcU == null) {
            return 0;
        }
        return this.hcU.size();
    }

    /* renamed from: wM */
    public final FunctionNode mo7704wM(int i) {
        return (FunctionNode) this.hcU.get(i);
    }

    /* renamed from: a */
    public final int mo7679a(FunctionNode yq) {
        if (yq == null) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        if (this.hcU == null) {
            this.hcU = new ObjArray();
        }
        this.hcU.add(yq);
        return this.hcU.size() - 1;
    }

    public final int getRegexpCount() {
        if (this.hcV == null) {
            return 0;
        }
        return this.hcV.size() / 2;
    }

    public final String getRegexpString(int i) {
        return (String) this.hcV.get(i * 2);
    }

    public final String getRegexpFlags(int i) {
        return (String) this.hcV.get((i * 2) + 1);
    }

    public final int addRegexp(String str, String str2) {
        if (str == null) {
            org.mozilla1.javascript.Kit.codeBug();
        }
        if (this.hcV == null) {
            this.hcV = new ObjArray();
        }
        this.hcV.add(str);
        this.hcV.add(str2);
        return (this.hcV.size() / 2) - 1;
    }

    /* renamed from: K */
    public int mo7678K(Node qlVar) {
        Node.C3370d iM;
        if (this.hcY == null) {
            throw org.mozilla1.javascript.Kit.codeBug();
        }
        Scope cFI = qlVar.cFI();
        if (cFI == null) {
            iM = null;
        } else {
            iM = cFI.mo21510iM(qlVar.getString());
        }
        if (iM == null) {
            return -1;
        }
        return iM.index;
    }

    public final String getParamOrVarName(int i) {
        if (this.hcY != null) {
            return this.hcY[i];
        }
        throw org.mozilla1.javascript.Kit.codeBug();
    }

    public final int getParamCount() {
        return this.hcX;
    }

    public final int getParamAndVarCount() {
        if (this.hcY != null) {
            return this.hcW.size();
        }
        throw org.mozilla1.javascript.Kit.codeBug();
    }

    public final String[] getParamAndVarNames() {
        if (this.hcY != null) {
            return this.hcY;
        }
        throw org.mozilla1.javascript.Kit.codeBug();
    }

    public final boolean[] cHr() {
        if (this.hcY != null) {
            return this.hcZ;
        }
        throw org.mozilla1.javascript.Kit.codeBug();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo7680a(Node.C3370d dVar) {
        if (this.hcY != null) {
            throw Kit.codeBug();
        }
        if (dVar.heu == 86) {
            this.hcX++;
        }
        this.hcW.add(dVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: hC */
    public void mo7698hC(boolean z) {
        boolean z2;
        if (!z) {
            ArrayList<Node.C3370d> arrayList = new ArrayList<>();
            if (this.fFc != null) {
                for (int i = 0; i < this.hcW.size(); i++) {
                    Node.C3370d dVar = this.hcW.get(i);
                    if (dVar.hev == this) {
                        arrayList.add(dVar);
                    }
                }
            }
            this.hcW = arrayList;
        }
        this.hcY = new String[this.hcW.size()];
        this.hcZ = new boolean[this.hcW.size()];
        for (int i2 = 0; i2 < this.hcW.size(); i2++) {
            Node.C3370d dVar2 = this.hcW.get(i2);
            this.hcY[i2] = dVar2.name;
            boolean[] zArr = this.hcZ;
            if (dVar2.heu == 153) {
                z2 = true;
            } else {
                z2 = false;
            }
            zArr[i2] = z2;
            dVar2.index = i2;
        }
    }

    public final Object getCompilerData() {
        return this.hda;
    }

    public final void setCompilerData(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException();
        } else if (this.hda != null) {
            throw new IllegalStateException();
        } else {
            this.hda = obj;
        }
    }

    public String cHs() {
        StringBuilder sb = new StringBuilder("$");
        int i = this.hdb;
        this.hdb = i + 1;
        return sb.append(i).toString();
    }
}
