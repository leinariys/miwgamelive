package org.mozilla1.javascript;

/* renamed from: a.aFc  reason: case insensitive filesystem */
/* compiled from: a */
final class NativeError extends IdScriptableObject {
    static final long serialVersionUID = -5338413581437645187L;
    private static final int ceN = 3;
    private static final Object hIG = new Integer(14);
    /* renamed from: ns */
    private static final int f2836ns = 1;
    /* renamed from: nt */
    private static final int f2837nt = 3;
    /* renamed from: xT */
    private static final int f2838xT = 2;

    NativeError() {
    }

    /* renamed from: a */
    static void m14617a(org.mozilla1.javascript.Scriptable avf, boolean z) {
        NativeError afc = new NativeError();
        org.mozilla1.javascript.ScriptableObject.m23566a((org.mozilla1.javascript.Scriptable) afc, "name", (Object) "Error");
        org.mozilla1.javascript.ScriptableObject.m23566a((org.mozilla1.javascript.Scriptable) afc, "message", (Object) "");
        org.mozilla1.javascript.ScriptableObject.m23566a((org.mozilla1.javascript.Scriptable) afc, "fileName", (Object) "");
        org.mozilla1.javascript.ScriptableObject.m23566a((org.mozilla1.javascript.Scriptable) afc, "lineNumber", (Object) new Integer(0));
        afc.mo15695a(3, avf, z);
    }

    /* renamed from: a */
    static NativeError m14615a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.IdFunctionObject yy, Object[] objArr) {
        NativeError afc = new NativeError();
        afc.setPrototype((org.mozilla1.javascript.Scriptable) yy.get("prototype", yy));
        afc.setParentScope(avf);
        int length = objArr.length;
        if (length >= 1) {
            org.mozilla1.javascript.ScriptableObject.m23566a((org.mozilla1.javascript.Scriptable) afc, "message", (Object) org.mozilla1.javascript.ScriptRuntime.toString(objArr[0]));
            if (length >= 2) {
                org.mozilla1.javascript.ScriptableObject.m23566a((org.mozilla1.javascript.Scriptable) afc, "fileName", objArr[1]);
                if (length >= 3) {
                    org.mozilla1.javascript.ScriptableObject.m23566a((org.mozilla1.javascript.Scriptable) afc, "lineNumber", (Object) new Integer(org.mozilla1.javascript.ScriptRuntime.toInt32(objArr[2])));
                }
            }
        }
        if (length < 3 && lhVar.hasFeature(10)) {
            int[] iArr = new int[1];
            String c = org.mozilla1.javascript.Context.m35242c(iArr);
            org.mozilla1.javascript.ScriptableObject.m23566a((org.mozilla1.javascript.Scriptable) afc, "lineNumber", (Object) new Integer(iArr[0]));
            if (length < 2) {
                org.mozilla1.javascript.ScriptableObject.m23566a((org.mozilla1.javascript.Scriptable) afc, "fileName", (Object) c);
            }
        }
        return afc;
    }

    /* renamed from: n */
    private static String m14619n(org.mozilla1.javascript.Scriptable avf) {
        return String.valueOf(m14618c(avf, "name")) + ": " + m14618c(avf, "message");
    }

    /* renamed from: a */
    private static String m14616a(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2) {
        Object obj;
        Object obj2;
        int int32;
        Object j = org.mozilla1.javascript.ScriptableObject.m23587j(avf2, "name");
        Object j2 = org.mozilla1.javascript.ScriptableObject.m23587j(avf2, "message");
        Object j3 = org.mozilla1.javascript.ScriptableObject.m23587j(avf2, "fileName");
        Object j4 = org.mozilla1.javascript.ScriptableObject.m23587j(avf2, "lineNumber");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("(new ");
        if (j == NOT_FOUND) {
            j = Undefined.instance;
        }
        stringBuffer.append(org.mozilla1.javascript.ScriptRuntime.toString(j));
        stringBuffer.append("(");
        if (!(j2 == NOT_FOUND && j3 == NOT_FOUND && j4 == NOT_FOUND)) {
            if (j2 == NOT_FOUND) {
                obj = "";
            } else {
                obj = j2;
            }
            stringBuffer.append(org.mozilla1.javascript.ScriptRuntime.m7571d(lhVar, avf, obj));
            if (!(j3 == NOT_FOUND && j4 == NOT_FOUND)) {
                stringBuffer.append(", ");
                if (j3 == NOT_FOUND) {
                    obj2 = "";
                } else {
                    obj2 = j3;
                }
                stringBuffer.append(org.mozilla1.javascript.ScriptRuntime.m7571d(lhVar, avf, obj2));
                if (!(j4 == NOT_FOUND || (int32 = org.mozilla1.javascript.ScriptRuntime.toInt32(j4)) == 0)) {
                    stringBuffer.append(", ");
                    stringBuffer.append(org.mozilla1.javascript.ScriptRuntime.toString((double) int32));
                }
            }
        }
        stringBuffer.append("))");
        return stringBuffer.toString();
    }

    /* renamed from: c */
    private static String m14618c(org.mozilla1.javascript.Scriptable avf, String str) {
        Object j = ScriptableObject.m23587j(avf, str);
        if (j == NOT_FOUND) {
            return "";
        }
        return ScriptRuntime.toString(j);
    }

    public String getClassName() {
        return "Error";
    }

    public String toString() {
        return m14619n(this);
    }

    /* access modifiers changed from: protected */
    public void initPrototypeId(int i) {
        String str;
        int i2 = 0;
        switch (i) {
            case 1:
                i2 = 1;
                str = "constructor";
                break;
            case 2:
                str = "toString";
                break;
            case 3:
                str = "toSource";
                break;
            default:
                throw new IllegalArgumentException(String.valueOf(i));
        }
        initPrototypeMethod(hIG, i, str, i2);
    }

    public Object execIdCall(IdFunctionObject yy, Context lhVar, org.mozilla1.javascript.Scriptable avf, Scriptable avf2, Object[] objArr) {
        if (!yy.hasTag(hIG)) {
            return super.execIdCall(yy, lhVar, avf, avf2, objArr);
        }
        int methodId = yy.methodId();
        switch (methodId) {
            case 1:
                return m14615a(lhVar, avf, yy, objArr);
            case 2:
                return m14619n(avf2);
            case 3:
                return m14616a(lhVar, avf, avf2);
            default:
                throw new IllegalArgumentException(String.valueOf(methodId));
        }
    }

    /* access modifiers changed from: protected */
    public int findPrototypeId(String str) {
        int i = 3;
        String str2 = null;
        int length = str.length();
        if (length == 8) {
            char charAt = str.charAt(3);
            if (charAt == 'o') {
                str2 = "toSource";
            } else {
                if (charAt == 't') {
                    str2 = "toString";
                    i = 2;
                }
                i = 0;
            }
        } else {
            if (length == 11) {
                str2 = "constructor";
                i = 1;
            }
            i = 0;
        }
        if (str2 == null || str2 == str || str2.equals(str)) {
            return i;
        }
        return 0;
    }
}
