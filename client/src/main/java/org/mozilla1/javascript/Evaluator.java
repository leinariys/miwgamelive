package org.mozilla1.javascript;


import java.util.List;

/* renamed from: a.Uw */
/* compiled from: a */
public interface Evaluator {
    /* renamed from: a */
    org.mozilla1.javascript.Script mo5920a(Object obj, Object obj2);

    /* renamed from: a */
    Function mo5921a(org.mozilla1.javascript.Context lhVar, Scriptable avf, Object obj, Object obj2);

    /* renamed from: a */
    Object mo5922a(CompilerEnvirons aki, ScriptOrFnNode aak, String str, boolean z);

    /* renamed from: a */
    String mo5923a(org.mozilla1.javascript.RhinoException pu, String str);

    /* renamed from: a */
    String mo5924a(Context lhVar, int[] iArr);

    /* renamed from: a */
    void mo5925a(org.mozilla1.javascript.RhinoException pu);

    /* renamed from: a */
    void mo5926a(Script afe);

    /* renamed from: b */
    List<String> mo5927b(RhinoException pu);
}
