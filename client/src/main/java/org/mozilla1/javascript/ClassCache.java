package org.mozilla1.javascript;

import java.util.HashMap;
import java.util.Map;

/* renamed from: a.aks  reason: case insensitive filesystem */
/* compiled from: a */
public class ClassCache {
    private static final Object fTm = new Integer(1);
    private volatile boolean cachingIsEnabled = true;
    private HashMap<Class<?>, org.mozilla1.javascript.JavaMembers> fTo = new HashMap<>();
    private HashMap<Class<?>, org.mozilla1.javascript.JavaMembers> fTp = new HashMap<>();
    private HashMap<JavaAdapter.JavaAdapterSignature, Class<?>> fTq = new HashMap<>();
    private HashMap<Class<?>, Object> interfaceAdapterCache;
    private int fTs;

    /* renamed from: k */
    public static ClassCache m23644k(Scriptable avf) {
        ClassCache aks = (ClassCache) org.mozilla1.javascript.ScriptableObject.m23581d(avf, fTm);
        if (aks != null) {
            return aks;
        }
        throw new RuntimeException("Can't find top level scope for ClassCache.get");
    }

    /* renamed from: b */
    public boolean mo14597b(ScriptableObject akn) {
        if (akn.getParentScope() != null) {
            throw new IllegalArgumentException();
        } else if (this == akn.associateValue(fTm, this)) {
            return true;
        } else {
            return false;
        }
    }

    public synchronized void clearCaches() {
        this.fTo.clear();
        this.fTp.clear();
        this.fTq.clear();
        this.interfaceAdapterCache = null;
    }

    public final boolean isCachingEnabled() {
        return this.cachingIsEnabled;
    }

    public synchronized void setCachingEnabled(boolean z) {
        if (z != this.cachingIsEnabled) {
            if (!z) {
                clearCaches();
            }
            this.cachingIsEnabled = z;
        }
    }

    /* access modifiers changed from: package-private */
    public Map<Class<?>, org.mozilla1.javascript.JavaMembers> cgl() {
        return this.fTo;
    }

    /* access modifiers changed from: package-private */
    public Map<JavaAdapter.JavaAdapterSignature, Class<?>> cgm() {
        return this.fTq;
    }

    public boolean isInvokerOptimizationEnabled() {
        return false;
    }

    public synchronized void setInvokerOptimizationEnabled(boolean z) {
    }

    public final synchronized int newClassSerialNumber() {
        int i;
        i = this.fTs + 1;
        this.fTs = i;
        return i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: am */
    public Object mo14596am(Class<?> cls) {
        if (this.interfaceAdapterCache == null) {
            return null;
        }
        return this.interfaceAdapterCache.get(cls);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public synchronized void mo14601e(Class<?> cls, Object obj) {
        if (this.cachingIsEnabled) {
            if (this.interfaceAdapterCache == null) {
                this.interfaceAdapterCache = new HashMap<>();
            }
            this.interfaceAdapterCache.put(cls, obj);
        }
    }
}
