package org.mozilla1.javascript;


/* renamed from: a.aeV  reason: case insensitive filesystem */
/* compiled from: a */
public class NativeJavaConstructor extends BaseFunction {
    static final long serialVersionUID = -8149253217482668463L;
    org.mozilla1.javascript.MemberBox fsv;

    public NativeJavaConstructor(org.mozilla1.javascript.MemberBox amt) {
        this.fsv = amt;
    }

    public Object call(Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        return NativeJavaClass.m3325a(lhVar, avf, objArr, this.fsv);
    }

    public String getFunctionName() {
        return "<init>".concat(JavaMembers.liveConnectSignature(this.fsv.gaM));
    }

    public String toString() {
        return "[JavaConstructor " + this.fsv.getName() + "]";
    }
}
