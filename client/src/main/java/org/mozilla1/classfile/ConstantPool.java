package org.mozilla1.classfile;

import logic.res.KeyCode;
import org.mozilla1.javascript.ObjToIntMap;
import org.mozilla1.javascript.UintMap;

/* renamed from: a.VF */
/* compiled from: a */
final class ConstantPool {
    private static final byte CONSTANT_Class = 7;
    private static final byte CONSTANT_Double = 6;
    private static final byte CONSTANT_Fieldref = 9;
    private static final byte CONSTANT_Float = 4;
    private static final byte CONSTANT_Integer = 3;
    private static final byte CONSTANT_InterfaceMethodref = 11;
    private static final byte CONSTANT_Long = 5;
    private static final byte CONSTANT_Methodref = 10;
    private static final byte CONSTANT_NameAndType = 12;
    private static final byte CONSTANT_String = 8;
    private static final byte CONSTANT_Utf8 = 1;
    private static final int euG = 256;
    private static final int euI = 65535;
    private org.mozilla1.classfile.ClassFileWriter euH;
    private UintMap euJ = new UintMap();
    private ObjToIntMap euK = new ObjToIntMap();
    private ObjToIntMap euL = new ObjToIntMap();
    private ObjToIntMap euM = new ObjToIntMap();
    private ObjToIntMap euN = new ObjToIntMap();
    private int euO;
    private int euP;
    private byte[] euQ;

    ConstantPool(org.mozilla1.classfile.ClassFileWriter uh) {
        this.euH = uh;
        this.euP = 1;
        this.euQ = new byte[256];
        this.euO = 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public int mo5974b(byte[] bArr, int i) {
        int a = org.mozilla1.classfile.ClassFileWriter.m10152a((int) (short) this.euP, bArr, i);
        System.arraycopy(this.euQ, 0, bArr, a, this.euO);
        return a + this.euO;
    }

    /* access modifiers changed from: package-private */
    public int blR() {
        return this.euO + 2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: oL */
    public int mo5987oL(int i) {
        ensure(5);
        byte[] bArr = this.euQ;
        int i2 = this.euO;
        this.euO = i2 + 1;
        bArr[i2] = 3;
        this.euO = org.mozilla1.classfile.ClassFileWriter.m10154b(i, this.euQ, this.euO);
        int i3 = this.euP;
        this.euP = i3 + 1;
        return (short) i3;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: fR */
    public int mo5978fR(long j) {
        ensure(9);
        byte[] bArr = this.euQ;
        int i = this.euO;
        this.euO = i + 1;
        bArr[i] = 5;
        this.euO = org.mozilla1.classfile.ClassFileWriter.m10153a(j, this.euQ, this.euO);
        int i2 = this.euP;
        this.euP += 2;
        return i2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: iz */
    public int mo5984iz(float f) {
        ensure(5);
        byte[] bArr = this.euQ;
        int i = this.euO;
        this.euO = i + 1;
        bArr[i] = 4;
        this.euO = org.mozilla1.classfile.ClassFileWriter.m10154b(Float.floatToIntBits(f), this.euQ, this.euO);
        int i2 = this.euP;
        this.euP = i2 + 1;
        return i2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: G */
    public int mo5973G(double d) {
        ensure(9);
        byte[] bArr = this.euQ;
        int i = this.euO;
        this.euO = i + 1;
        bArr[i] = 6;
        this.euO = org.mozilla1.classfile.ClassFileWriter.m10153a(Double.doubleToLongBits(d), this.euQ, this.euO);
        int i2 = this.euP;
        this.euP += 2;
        return i2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: hh */
    public int mo5979hh(String str) {
        short hj = mo5981hj(str) & euI;
        int i = this.euJ.getInt(hj, -1);
        if (i != -1) {
            return i;
        }
        int i2 = this.euP;
        this.euP = i2 + 1;
        ensure(3);
        byte[] bArr = this.euQ;
        int i3 = this.euO;
        this.euO = i3 + 1;
        bArr[i3] = 8;
        this.euO = org.mozilla1.classfile.ClassFileWriter.m10152a((int) hj, this.euQ, this.euO);
        this.euJ.put((int) hj, i2);
        return i2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: hi */
    public boolean mo5980hi(String str) {
        int length = str.length();
        if (length * 3 <= euI) {
            return true;
        }
        if (length > euI) {
            return false;
        }
        if (length != mo5976d(str, 0, length)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public int mo5976d(String str, int i, int i2) {
        int i3 = euI;
        if ((i2 - i) * 3 <= euI) {
            return i2;
        }
        for (int i4 = i; i4 != i2; i4++) {
            char charAt = str.charAt(i4);
            if (charAt != 0 && charAt <= 127) {
                i3--;
            } else if (charAt < 2047) {
                i3 -= 2;
            } else {
                i3 -= 3;
            }
            if (i3 < 0) {
                return i4;
            }
        }
        return i2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: hj */
    public short mo5981hj(String str) {
        int i;
        int i2;
        boolean z = true;
        int i3 = this.euK.get(str, -1);
        if (i3 == -1) {
            int length = str.length();
            if (length > euI) {
                i = i3;
            } else {
                ensure((length * 3) + 3);
                int i4 = this.euO;
                this.euQ[i4] = 1;
                char[] od = this.euH.getCharBuffer(length);
                str.getChars(0, length, od, 0);
                int i5 = 0;
                int i6 = i4 + 1 + 2;
                while (i5 != length) {
                    char c = od[i5];
                    if (c != 0 && c <= 127) {
                        i2 = i6 + 1;
                        this.euQ[i6] = (byte) c;
                    } else if (c > 2047) {
                        int i7 = i6 + 1;
                        this.euQ[i6] = (byte) ((c >> 12) | KeyCode.ctb);
                        int i8 = i7 + 1;
                        this.euQ[i7] = (byte) (((c >> 6) & 63) | 128);
                        i2 = i8 + 1;
                        this.euQ[i8] = (byte) ((c & '?') | 128);
                    } else {
                        int i9 = i6 + 1;
                        this.euQ[i6] = (byte) ((c >> 6) | 192);
                        i2 = i9 + 1;
                        this.euQ[i9] = (byte) ((c & '?') | 128);
                    }
                    i5++;
                    i6 = i2;
                }
                int i10 = i6 - ((this.euO + 1) + 2);
                if (i10 > euI) {
                    i = i3;
                } else {
                    this.euQ[this.euO + 1] = (byte) (i10 >>> 8);
                    this.euQ[this.euO + 2] = (byte) i10;
                    this.euO = i6;
                    int i11 = this.euP;
                    this.euP = i11 + 1;
                    this.euK.put(str, i11);
                    z = false;
                    i = i11;
                }
            }
            if (z) {
                throw new IllegalArgumentException("Too big string");
            }
        } else {
            i = i3;
        }
        return (short) i;
    }

    /* renamed from: N */
    private short m10467N(String str, String str2) {
        short hj = mo5981hj(str);
        short hj2 = mo5981hj(str2);
        ensure(5);
        byte[] bArr = this.euQ;
        int i = this.euO;
        this.euO = i + 1;
        bArr[i] = 12;
        this.euO = org.mozilla1.classfile.ClassFileWriter.m10152a((int) hj, this.euQ, this.euO);
        this.euO = org.mozilla1.classfile.ClassFileWriter.m10152a((int) hj2, this.euQ, this.euO);
        int i2 = this.euP;
        this.euP = i2 + 1;
        return (short) i2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: hk */
    public short mo5982hk(String str) {
        String str2;
        int i = this.euN.get(str, -1);
        if (i == -1) {
            if (str.indexOf(46) > 0) {
                str2 = org.mozilla1.classfile.ClassFileWriter.m10155gG(str);
                i = this.euN.get(str2, -1);
                if (i != -1) {
                    this.euN.put(str, i);
                }
            } else {
                str2 = str;
            }
            if (i == -1) {
                short hj = mo5981hj(str2);
                ensure(3);
                byte[] bArr = this.euQ;
                int i2 = this.euO;
                this.euO = i2 + 1;
                bArr[i2] = 7;
                this.euO = org.mozilla1.classfile.ClassFileWriter.m10152a((int) hj, this.euQ, this.euO);
                i = this.euP;
                this.euP = i + 1;
                this.euN.put(str2, i);
                if (str != str2) {
                    this.euN.put(str, i);
                }
            }
        }
        return (short) i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: i */
    public short mo5983i(String str, String str2, String str3) {
        org.mozilla1.classfile.FieldOrMethodRef xz = new org.mozilla1.classfile.FieldOrMethodRef(str, str2, str3);
        int i = this.euL.get(xz, -1);
        if (i == -1) {
            short N = m10467N(str2, str3);
            short hk = mo5982hk(str);
            ensure(5);
            byte[] bArr = this.euQ;
            int i2 = this.euO;
            this.euO = i2 + 1;
            bArr[i2] = 9;
            this.euO = org.mozilla1.classfile.ClassFileWriter.m10152a((int) hk, this.euQ, this.euO);
            this.euO = org.mozilla1.classfile.ClassFileWriter.m10152a((int) N, this.euQ, this.euO);
            i = this.euP;
            this.euP = i + 1;
            this.euL.put(xz, i);
        }
        return (short) i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: j */
    public short mo5985j(String str, String str2, String str3) {
        org.mozilla1.classfile.FieldOrMethodRef xz = new org.mozilla1.classfile.FieldOrMethodRef(str, str2, str3);
        int i = this.euM.get(xz, -1);
        if (i == -1) {
            short N = m10467N(str2, str3);
            short hk = mo5982hk(str);
            ensure(5);
            byte[] bArr = this.euQ;
            int i2 = this.euO;
            this.euO = i2 + 1;
            bArr[i2] = 10;
            this.euO = org.mozilla1.classfile.ClassFileWriter.m10152a((int) hk, this.euQ, this.euO);
            this.euO = org.mozilla1.classfile.ClassFileWriter.m10152a((int) N, this.euQ, this.euO);
            i = this.euP;
            this.euP = i + 1;
            this.euM.put(xz, i);
        }
        return (short) i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: k */
    public short mo5986k(String str, String str2, String str3) {
        short N = m10467N(str2, str3);
        short hk = mo5982hk(str);
        ensure(5);
        byte[] bArr = this.euQ;
        int i = this.euO;
        this.euO = i + 1;
        bArr[i] = 11;
        this.euO = org.mozilla1.classfile.ClassFileWriter.m10152a((int) hk, this.euQ, this.euO);
        this.euO = ClassFileWriter.m10152a((int) N, this.euQ, this.euO);
        int i2 = this.euP;
        this.euP = i2 + 1;
        return (short) i2;
    }

    /* access modifiers changed from: package-private */
    public void ensure(int i) {
        if (this.euO + i > this.euQ.length) {
            int length = this.euQ.length * 2;
            if (this.euO + i > length) {
                length = this.euO + i;
            }
            byte[] bArr = new byte[length];
            System.arraycopy(this.euQ, 0, bArr, 0, this.euO);
            this.euQ = bArr;
        }
    }
}
