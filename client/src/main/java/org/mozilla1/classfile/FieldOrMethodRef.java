package org.mozilla1.classfile;

/* renamed from: a.XZ */
/* compiled from: a */
public final class FieldOrMethodRef {
    private String className;
    private int hashCode = -1;
    private String name;
    private String type;

    FieldOrMethodRef(String str, String str2, String str3) {
        this.className = str;
        this.name = str2;
        this.type = str3;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof FieldOrMethodRef)) {
            return false;
        }
        FieldOrMethodRef xz = (FieldOrMethodRef) obj;
        if (!this.className.equals(xz.className) || !this.name.equals(xz.name) || !this.type.equals(xz.type)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this.hashCode == -1) {
            this.hashCode = (this.className.hashCode() ^ this.name.hashCode()) ^ this.type.hashCode();
        }
        return this.hashCode;
    }
}
