package org.mozilla1.classfile;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.geometry.aLH;
import game.network.message.externalizable.C6950awp;
import game.network.message.externalizable.aUP;
import gnu.trove.THashSet;
import logic.aaa.C1088Pv;
import logic.aaa.C2235dB;
import logic.thred.C1362Tq;
import logic.thred.C6339akz;
import logic.thred.C6615aqP;
import logic.thred.LogPrinter;
import p001a.*;

import javax.vecmath.Tuple3d;
import javax.vecmath.Tuple3f;
import java.util.*;

/* renamed from: a.aiI  reason: case insensitive filesystem */
/* compiled from: a */
public class C6192aiI implements C1355Tk {
    public static final int fOg = 1;
    public static final int fOh = 2;
    public static final int fOi = 4;
    public static final int fOj = 16;
    public static final int fOk = 32;
    public static final int fOl = 64;
    public static final int fOm = Integer.MIN_VALUE;
    /* access modifiers changed from: private */
    public static final LogPrinter logger = LogPrinter.m10275K(C6192aiI.class);
    /* access modifiers changed from: private */
    public CurrentTimeMilli cVg;
    /* access modifiers changed from: private */
    public C3876wB fOq = new C3859vu();
    int fOo = 10;
    private long buF;
    private long fOn = 0;
    private C3055nX<C0819Lq, C0461GN> fOp = new aEY(8000.0d, 8000.0d, 8000.0d);
    private C3286py fOr = new C6020aes();
    private ArrayList<C1362Tq> fOs = new ArrayList<>();
    private THashSet<C3102np> fOt = new THashSet<>();
    private long lastUpdate;

    /* renamed from: ox */
    private C1037PD f4603ox = new C6944awj();

    public C6192aiI() {
        this.fOr.mo1226a(this.f4603ox);
        mo5729a(SingletonCurrentTimeMilliImpl.CURRENT_TIME_MILLI);
    }

    /* renamed from: a */
    public void mo5726a(C1037PD pd) {
        this.f4603ox = pd;
        this.fOr.mo1226a(pd);
    }

    public C1037PD bvg() {
        return this.f4603ox;
    }

    /* renamed from: c */
    public int mo5736c(C6615aqP aqp, C6615aqP aqp2) {
        return this.f4603ox.mo4617b(aqp, aqp2);
    }

    /* renamed from: a */
    public void mo5729a(CurrentTimeMilli ahw) {
        this.cVg = ahw;
        this.buF = ahw.currentTimeMillis();
    }

    /* renamed from: a */
    public aNW mo5725a(long j, C2235dB dBVar, int i) {
        C0819Lq lq = new C0819Lq(this, dBVar, i);
        if ((i & 8) != 0) {
            if (!(dBVar instanceof C6615aqP)) {
                throw new IllegalArgumentException("Not collidable");
            }
            C6615aqP aqp = (C6615aqP) dBVar;
            lq.aabb = new aLH();
            aqp.mo2437IL().getBoundingBox().mo23419a(aqp.getOrientation(), aqp.getPosition(), lq.aabb);
            lq.dut = this.fOr.mo1225a(lq.aabb, aqp);
        }
        if ((i & 2) == 0 || (dBVar instanceof C6243ajH)) {
            if (j <= 0) {
                j = this.buF;
            }
            lq.buF = j;
            int i2 = i & 2;
            if ((i & 1) != 0) {
                if ((i & 4) != 0) {
                    i2 |= 4;
                } else {
                    i2 |= 1;
                }
            }
            lq.mo3830a(this.fOp.mo8631a(lq, i2));
            return lq;
        }
        throw new IllegalArgumentException("Not steppable");
    }

    /* renamed from: a */
    public C3387qy<C0461GN> mo13314a(C0461GN gn) {
        return this.fOp.mo8632a(gn);
    }

    /* renamed from: gZ */
    public void mo5739gZ() {
        mo5738fI(this.cVg.currentTimeMillis());
    }

    /* renamed from: fI */
    public void mo5738fI(long j) {
        ccv();
        long j2 = j - this.buF;
        if (j2 <= 0) {
            this.buF = j;
            return;
        }
        long j3 = this.buF;
        m22460hw(j - j3);
        if (j2 > 2000) {
            long j4 = j2 - 2000;
            if (j4 > 33) {
                logger.warn("Lost " + j4 + " milliseconds. " + ((((float) j4) * 100.0f) / ((float) j2)) + "% loss");
            }
        }
        while (j3 < j) {
            long hw = m22460hw(j - j3);
            j3 += hw;
            m22457a(j3, ((float) hw) * 0.001f, true);
            this.buF = j3;
        }
        if (this.buF < j) {
            m22457a(j, ((float) (j - this.buF)) * 0.001f, true);
            this.buF = j;
        }
        this.fOp.mo8641gZ();
    }

    /* renamed from: hw */
    private long m22460hw(long j) {
        if (j <= 10) {
            return j;
        }
        if (j <= 1000) {
            return 10;
        }
        if (j <= 1500) {
            return 16;
        }
        if (j <= 2000) {
            return 33;
        }
        if (j <= 3000) {
            return 50;
        }
        if (j <= 5000) {
            return 100;
        }
        if (j <= 10000) {
            return 500;
        }
        if (j <= 60000) {
            return 1000;
        }
        return j - 1000;
    }

    /* renamed from: a */
    private void m22457a(long j, float f, boolean z) {
        C6268ajg a;
        C1362Tq ccQ;
        C1362Tq ccP;
        C1362Tq ccQ2;
        C1362Tq ccP2;
        C1362Tq ccQ3;
        C1362Tq ccP3;
        this.fOp.mo8636a(new C1902a(j));
        this.fOr.mo1231gZ();
        for (C3102np next : this.fOr.mo1222VA()) {
            try {
                C6615aqP dhC = next.mo14894Qg().dhC();
                C6615aqP dhC2 = next.mo14895Qh().dhC();
                int b = this.f4603ox.mo4617b(dhC, dhC2);
                int b2 = this.f4603ox.mo4617b(dhC2, dhC);
                if (b != 0 || b2 != 0) {
                    if (((b | b2) & 99) != 0) {
                        this.fOt.add(next);
                    }
                    if ((b & 4) != 0) {
                        dhC.mo2543a(new C4106zZ(dhC2));
                    }
                    if ((b2 & 4) != 0) {
                        dhC2.mo2543a(new C4106zZ(dhC));
                    }
                }
            } catch (Exception e) {
                logger.error("Error processing collision: ", e);
            }
        }
        if (f < 0.05f && z) {
            Iterator it = this.fOt.iterator();
            while (it.hasNext()) {
                C3102np npVar = (C3102np) it.next();
                try {
                    if (!((!npVar.mo14894Qg().cip() && !npVar.mo14895Qh().cip()) || ((C0501Gx) npVar.mo14894Qg().dhC()).aRh() == null || ((C0501Gx) npVar.mo14895Qh().dhC()).aRh() == null)) {
                        boolean z2 = j - ((C0501Gx) npVar.mo14894Qg().dhC()).aRh().bfx() < 50;
                        boolean z3 = j - ((C0501Gx) npVar.mo14895Qh().dhC()).aRh().bfx() < 50;
                        if ((z2 || z3) && (a = this.f4603ox.mo4614a(npVar)) != null) {
                            C6615aqP UV = a.mo7942UV();
                            C6615aqP UW = a.mo7943UW();
                            int b3 = this.f4603ox.mo4617b(UV, UW);
                            int b4 = this.f4603ox.mo4617b(UW, UV);
                            if (a.mo7941Ef()) {
                                if (((b3 | b4) & 1) != 0) {
                                    mo13660a(a, (b3 & 1) != 0, (b4 & 1) != 0);
                                }
                                if (!((b3 & 2) == 0 || (ccP3 = a.ccP()) == null)) {
                                    this.fOs.add(ccP3);
                                }
                                if (!((b4 & 2) == 0 || (ccQ3 = a.ccQ()) == null)) {
                                    this.fOs.add(ccQ3);
                                }
                                if (!a.ccS()) {
                                    if (!((b3 & 32) == 0 || (ccP2 = a.ccP()) == null)) {
                                        this.fOs.add(new C6134ahC(ccP2, C1088Pv.ENTER));
                                    }
                                    if (!((b4 & 32) == 0 || (ccQ2 = a.ccQ()) == null)) {
                                        this.fOs.add(new C6134ahC(ccQ2, C1088Pv.ENTER));
                                    }
                                }
                            } else if (a.ccS()) {
                                if (!((b3 & 64) == 0 || (ccP = a.ccP()) == null)) {
                                    this.fOs.add(new C6134ahC(ccP, C1088Pv.EXIT));
                                }
                                if (!((b4 & 64) == 0 || (ccQ = a.ccQ()) == null)) {
                                    this.fOs.add(new C6134ahC(ccQ, C1088Pv.EXIT));
                                }
                            }
                        }
                    }
                } catch (Exception e2) {
                    it.remove();
                    logger.error("Error processing collision: ", e2);
                }
            }
        }
        for (C3102np next2 : this.fOr.mo1224Vz()) {
            try {
                this.fOt.remove(next2);
                C6615aqP dhC3 = next2.mo14894Qg().dhC();
                C6615aqP dhC4 = next2.mo14895Qh().dhC();
                int b5 = this.f4603ox.mo4617b(dhC3, dhC4);
                int b6 = this.f4603ox.mo4617b(dhC4, dhC3);
                if ((b5 & 16) != 0) {
                    dhC3.mo2543a(new C2752jW(dhC4));
                }
                if ((b6 & 16) != 0) {
                    dhC4.mo2543a(new C2752jW(dhC3));
                }
            } catch (Error e3) {
                logger.error("Error processing collision: ", e3);
            } catch (Exception e4) {
                logger.error("Error processing collision: ", e4);
            }
        }
        Iterator<C1362Tq> it2 = this.fOs.iterator();
        while (it2.hasNext()) {
            C1362Tq next3 = it2.next();
            try {
                next3.mo5750Jj().mo2543a(next3);
            } catch (Error e5) {
                logger.error("Error processing collision: ", e5);
            } catch (Exception e6) {
                logger.error("Error processing collision: ", e6);
            }
        }
        this.fOs.clear();
        this.buF = j;
    }

    public void ccv() {
    }

    /* renamed from: a */
    public void mo5727a(Vec3d ajr, float f, C1355Tk.C1356a aVar) {
        this.fOr.mo1227a(ajr, f, new C1903b(aVar));
    }

    public long bvf() {
        return this.buF;
    }

    /* renamed from: s */
    public void mo13317s(Collection<C6950awp> collection) {
        ccv();
        for (C6950awp next : collection) {
            try {
                next.mo12575a(this);
            } catch (Exception e) {
                logger.error("Error arriving data " + next, e);
            }
        }
    }

    public List<C6272ajk> bWW() {
        C5979aeD aed = new C5979aeD();
        HashMap hashMap = new HashMap();
        this.fOp.mo8634a(100000.0f, new C1904c(new THashSet(), new THashSet(), aed));
        long currentTimeMillis = bvh().currentTimeMillis();
        if (this.fOn == 0 || currentTimeMillis - this.fOn > 2500) {
            this.fOn = currentTimeMillis;
            this.fOp.mo8636a(new C1905d(hashMap));
            for (Map.Entry entry : hashMap.entrySet()) {
                aed.mo13023a((C6625aqZ) entry.getKey(), (C6950awp) new C6037afJ((List) entry.getValue()));
            }
        }
        Map<C6625aqZ, List<C6950awp>> map = aed.getMap();
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry next : map.entrySet()) {
            arrayList.add(new C6099agT(new aUP((C6625aqZ) next.getKey()), (Collection) next.getValue()));
        }
        return arrayList;
    }

    /* renamed from: a */
    public void mo13315a(C3876wB wBVar) {
        this.fOq = wBVar;
    }

    /* renamed from: f */
    private void m22459f(Vec3f vec3f, float f) {
        float lengthSquared = vec3f.lengthSquared();
        if (lengthSquared > f * f) {
            float sqrt = f / ((float) Math.sqrt((double) lengthSquared));
            vec3f.x *= sqrt;
            vec3f.y *= sqrt;
            vec3f.z = sqrt * vec3f.z;
        }
    }

    public C3055nX<C0819Lq, C0461GN> bvi() {
        return this.fOp;
    }

    public CurrentTimeMilli bvh() {
        return this.cVg;
    }

    /* renamed from: a */
    public void mo5728a(Vec3d ajr, Vec3f vec3f, float f, C1355Tk.C1356a aVar, List<C6615aqP> list) {
        this.fOr.mo1228a(ajr, ajr.mo9531q(vec3f.mo23510mS(f)), new C1906e(aVar), list);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo13660a(C6268ajg ajg, boolean z, boolean z2) {
        C0501Gx gx = (C0501Gx) ajg.mo7942UV();
        C0501Gx gx2 = (C0501Gx) ajg.mo7943UW();
        boolean z3 = z && !gx.isStatic();
        boolean z4 = z2 && !gx2.isStatic();
        if (!z3 && !z4) {
            return;
        }
        if (!this.f4603ox.mo1932a(gx, gx2)) {
            logger.warn("Trying to collide: " + gx + " " + gx2);
            return;
        }
        C0763Kt bcE = C0763Kt.bcE();
        bcE.bcH().push();
        try {
            C6339akz akz = ajg.ccT().get(0);
            Vec3d cgq = akz.cgq();
            Vec3d cgp = akz.cgp();
            Vec3f c = ((Vec3f) bcE.bcH().get()).mo23480c((Tuple3d) cgq, (Tuple3d) gx.aRe());
            Vec3f c2 = ((Vec3f) bcE.bcH().get()).mo23480c((Tuple3d) cgp, (Tuple3d) gx2.aRe());
            Vec3f ac = bcE.bcH().mo4458ac(gx.mo2446R(c));
            Vec3f ac2 = bcE.bcH().mo4458ac(gx2.mo2446R(c2));
            Vec3f c3 = ((Vec3f) bcE.bcH().get()).mo23481c((Tuple3f) ac, (Tuple3f) ac2);
            Vec3f vec3f = (Vec3f) bcE.bcH().get();
            Vec3f vec3f2 = (Vec3f) bcE.bcH().get();
            vec3f.set(akz.fTL);
            if (((double) vec3f.lengthSquared()) < 1.0E-4d) {
                if (((double) ac.lengthSquared()) > 1.0E-4d) {
                    vec3f.set(ac);
                } else if (((double) ac2.lengthSquared()) > 1.0E-4d) {
                    vec3f.set(-ac2.x, ac2.y, -ac2.z);
                } else {
                    vec3f.set(1.0f, 1.0f, 1.0f);
                }
                vec3f.normalize();
            }
            vec3f2.set(vec3f);
            if (vec3f.length() >= 0.0f) {
                vec3f.negate();
            }
            vec3f.normalize();
            float max = Math.max(0.0f, vec3f.dot(ac));
            float min = Math.min(0.0f, vec3f2.dot(ac2));
            Vec3f a = ((Vec3f) bcE.bcH().get()).mo23471a(-max, vec3f, ac);
            Vec3f a2 = ((Vec3f) bcE.bcH().get()).mo23471a(-min, vec3f2, ac2);
            float volume = gx.mo2437IL().getVolume() + 1.0E-6f;
            float volume2 = gx2.mo2437IL().getVolume() + 1.0E-6f;
            float max2 = (float) Math.max((double) (volume + volume2), 0.1d);
            if (z3) {
                a.scaleAdd((((volume - volume2) * max) + ((2.0f * volume2) * min)) / max2, vec3f, a);
                a.length();
                m22459f(a, 200.0f);
                if (a.isValid()) {
                    gx.mo2472kQ().blq().set(a);
                }
                Vec3f dfT = bcE.bcH().mo4458ac(c3).dfP().dfT();
                dfT.cross(c, dfT);
                Vec3f ac3 = bcE.bcH().mo4458ac(dfT);
                gx.aRa().transform(ac3);
                if (ac3.isValid()) {
                    gx.aRd().scaleAdd(gx.aRg(), ac3, gx.aRd());
                }
                gx.mo2440IR();
            }
            if (z4) {
                a2.scaleAdd((((2.0f * volume) * max) + ((volume2 - volume) * min)) / max2, vec3f2, a2);
                a2.length();
                m22459f(a2, 200.0f);
                if (a2.isValid()) {
                    gx2.mo2472kQ().blq().set(a2);
                }
                Vec3f dfP = bcE.bcH().mo4458ac(c3).dfP();
                dfP.cross(c2, dfP);
                Vec3f ac4 = bcE.bcH().mo4458ac(dfP);
                gx2.aRa().transform(ac4);
                if (ac4.isValid()) {
                    gx2.aRd().scaleAdd(gx2.aRg(), ac4, gx2.aRd());
                }
                gx2.mo2440IR();
            }
        } finally {
            bcE.bcH().pop();
        }
    }

    /* renamed from: bZ */
    public void mo5730bZ(long j) {
        this.lastUpdate = j;
    }

    /* renamed from: IO */
    public long mo5724IO() {
        return this.lastUpdate;
    }

    public void dispose() {
        this.fOp.dispose();
        this.fOp = null;
        this.cVg = null;
        this.fOq = null;
        this.fOr.dispose();
        this.fOr = null;
        this.f4603ox = null;
        this.fOs.clear();
        this.fOs = null;
        this.fOt.clear();
        this.fOt = null;
    }

    public long bvj() {
        return this.buF;
    }

    /* renamed from: a.aiI$a */
    class C1902a implements C3055nX.C3057b<C0819Lq, C0461GN> {
        private final /* synthetic */ long hWl;

        C1902a(long j) {
            this.hWl = j;
        }

        /* renamed from: a */
        public void mo1933a(C3055nX.C3056a<C0819Lq, C0461GN> aVar) {
            C1696Yx yx = (C1696Yx) aVar.mo11189B((Object) null);
            if (yx == null) {
                yx = new C1696Yx();
                aVar.mo11196e((Object) null, yx);
            }
            long j = 10;
            if (aVar.mo11190IN() > 1000000.0f) {
                j = 20;
                if (aVar.mo11190IN() > 1.0E8f) {
                    j = 50;
                    if (aVar.mo11190IN() > 4.0E8f) {
                        j = 100;
                        if (aVar.mo11190IN() > 2.5E9f) {
                            j = 400;
                            if (aVar.mo11190IN() > 1.0E10f) {
                                j = 1000;
                                if (aVar.mo11190IN() > 4.0E10f) {
                                    j = 5000;
                                    if (aVar.mo11190IN() > 1.0E12f) {
                                        j = 6000;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (this.hWl - yx.eMk >= j) {
                yx.eMk = this.hWl;
                for (C0463GP<C0819Lq> kP : aVar.mo11195cy(1)) {
                    C0819Lq lq = (C0819Lq) kP.mo2340kP();
                    if (this.hWl > lq.dup) {
                        try {
                            float f = ((float) (this.hWl - lq.buF)) * 0.001f * 1.0f;
                            if (f > 0.0f) {
                                lq.buF = this.hWl;
                                ((C6243ajH) lq.duo).step(f);
                            }
                        } catch (Exception e) {
                            C6192aiI.logger.error(e);
                            lq.dispose();
                        }
                        lq.dup = ((long) lq.duq) + lq.buF;
                    }
                }
            }
        }
    }

    /* renamed from: a.aiI$b */
    /* compiled from: a */
    class C1903b implements C6178ahu {
        private final /* synthetic */ C1355Tk.C1356a hWm;

        C1903b(C1355Tk.C1356a aVar) {
            this.hWm = aVar;
        }

        /* renamed from: a */
        public boolean mo13625a(aRR arr) {
            return this.hWm.mo1934c(arr.dhC());
        }
    }

    /* renamed from: a.aiI$c */
    /* compiled from: a */
    class C1904c implements C3055nX.C3057b<C0819Lq, C0461GN> {
        private final /* synthetic */ Set hWt;
        private final /* synthetic */ Set hWu;
        private final /* synthetic */ C5979aeD hWv;

        C1904c(Set set, Set set2, C5979aeD aed) {
            this.hWt = set;
            this.hWu = set2;
            this.hWv = aed;
        }

        /* renamed from: a */
        public void mo1933a(C3055nX.C3056a<C0819Lq, C0461GN> aVar) {
            C1696Yx yx;
            C1696Yx yx2 = (C1696Yx) aVar.mo11189B((Object) null);
            if (yx2 == null) {
                C1696Yx yx3 = new C1696Yx();
                aVar.mo11196e((Object) null, yx3);
                yx = yx3;
            } else {
                yx = yx2;
            }
            long currentTimeMillis = C6192aiI.this.cVg.currentTimeMillis();
            C3876wB.C3877a a = C6192aiI.this.fOq.mo22671a((float) Math.sqrt((double) aVar.mo11190IN()), currentTimeMillis, yx.eMl, yx.eMm);
            if (a != null) {
                List<C0463GP<C0819Lq>> cy = aVar.mo11195cy(2);
                List<C0463GP<C0819Lq>> cy2 = aVar.mo11195cy(0);
                if ((cy.size() != 0 && a.bcP() != 0) || cy2.size() != 0) {
                    this.hWt.clear();
                    aVar.mo11193a(a.bcR(), this.hWt);
                    yx.eMl = currentTimeMillis;
                    if (a.bcQ()) {
                        yx.eMm = currentTimeMillis;
                    }
                    if (cy2.size() > 0) {
                        m22477a(a.bcP(), currentTimeMillis, cy2, this.hWt);
                    }
                    if (cy.size() > 0 && a.bcP() != 0) {
                        m22477a(a.bcP(), currentTimeMillis, cy, this.hWt);
                    }
                }
            }
        }

        /* renamed from: a */
        private void m22477a(int i, long j, List<C0463GP<C0819Lq>> list, Collection<C6625aqZ> collection) {
            long currentTimeMillis = C6192aiI.this.cVg.currentTimeMillis();
            for (C0463GP next : list) {
                C0819Lq lq = (C0819Lq) next.mo2340kP();
                if (lq.bfw().isReady() && (lq.flags & 1) != 0) {
                    if ((lq.flags & 4) != 0) {
                        this.hWu.clear();
                        for (C6625aqZ next2 : collection) {
                            C0819Lq.C0821b bVar = lq.bfz().get(next2);
                            if (currentTimeMillis - bVar.hQp > 30000) {
                                this.hWu.add(next2);
                            } else if (bVar.hQp < next2.crP()) {
                                this.hWu.add(next2);
                            }
                            bVar.hQp = currentTimeMillis;
                        }
                        if (this.hWu.size() > 0) {
                            ((aKR) lq.duo).mo2462b(i, ((C0819Lq) next.mo2340kP()).buF, this.hWu, this.hWv);
                        }
                    } else {
                        ((aKR) lq.duo).mo2462b(i, ((C0819Lq) next.mo2340kP()).buF, collection, this.hWv);
                    }
                }
            }
        }
    }

    /* renamed from: a.aiI$d */
    /* compiled from: a */
    class C1905d implements C3055nX.C3057b<C0819Lq, C0461GN> {
        private final /* synthetic */ Map hWw;

        C1905d(Map map) {
            this.hWw = map;
        }

        /* renamed from: a */
        public void mo1933a(C3055nX.C3056a<C0819Lq, C0461GN> aVar) {
            HashSet<C6625aqZ> hashSet = new HashSet<>();
            aVar.mo11193a(1.0E10f, hashSet);
            for (C6625aqZ aqz : hashSet) {
                if (!this.hWw.containsKey(aqz)) {
                    this.hWw.put(aqz, new ArrayList());
                }
                ((List) this.hWw.get(aqz)).add(aVar);
            }
        }
    }

    /* renamed from: a.aiI$e */
    /* compiled from: a */
    class C1906e implements C6178ahu {
        private final /* synthetic */ C1355Tk.C1356a hWm;

        C1906e(C1355Tk.C1356a aVar) {
            this.hWm = aVar;
        }

        /* renamed from: a */
        public boolean mo13625a(aRR arr) {
            return this.hWm.mo1934c(arr.dhC());
        }
    }
}
