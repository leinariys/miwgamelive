package org.mozilla1.classfile;

import org.mozilla1.javascript.ObjArray;

import java.io.OutputStream;

/* renamed from: a.UH */
/* compiled from: a */
public class ClassFileWriter {
    public static final short ACC_ABSTRACT = 1024;
    public static final short ACC_FINAL = 16;
    public static final short ACC_NATIVE = 256;
    public static final short ACC_PRIVATE = 2;
    public static final short ACC_PROTECTED = 4;
    public static final short ACC_PUBLIC = 1;
    public static final short ACC_STATIC = 8;
    public static final short ACC_SYNCHRONIZED = 32;
    public static final short ACC_TRANSIENT = 128;
    public static final short ACC_VOLATILE = 64;
    private static final int epS = 32;
    private static final int epV = 40;
    private static final int epu = 16;
    private static final int epv = 4;
    private static final long epw = -3819410108756852691L;
    private static final boolean epx = false;
    private static final boolean epy = false;
    private static final boolean epz = false;
    private short itsFlags;
    private String epA;
    private org.mozilla1.classfile.ExceptionTableEntry[] itsExceptionTable;
    private int epC;
    private int[] epD;
    private int epE;
    private byte[] epF = new byte[256];
    private int epG;
    private ConstantPool epH;
    private org.mozilla1.classfile.ClassFileMethod epI;
    private short itsStackTop;
    private short itsMaxStack;
    private short epL;
    private ObjArray itsMethods = new ObjArray();
    private ObjArray itsFields = new ObjArray();
    private ObjArray itsInterfaces = new ObjArray();
    private short epP;
    private short epQ;
    private short epR;
    private int[] epT;
    private int epU;
    private long[] itsFixupTable;
    private int itsFixupTableTop;
    private ObjArray itsVarDescriptors;
    private char[] tmpCharBuffer = new char[64];

    public ClassFileWriter(String str, String str2, String str3) {
        this.epA = str;
        this.epH = new ConstantPool(this);
        this.epP = this.epH.mo5982hk(str);
        this.epQ = this.epH.mo5982hk(str2);
        if (str3 != null) {
            this.epR = this.epH.mo5981hj(str3);
        }
        this.itsFlags = 1;
    }

    /* renamed from: gG */
    static String m10155gG(String str) {
        return str.replace('.', C0147Bi.cla);
    }

    /* renamed from: gH */
    public static String m10156gH(String str) {
        int length = str.length();
        int i = length + 1;
        char[] cArr = new char[(i + 1)];
        cArr[0] = 'L';
        cArr[i] = ';';
        str.getChars(0, length, cArr, 1);
        for (int i2 = 1; i2 != i; i2++) {
            if (cArr[i2] == '.') {
                cArr[i2] = C0147Bi.cla;
            }
        }
        return new String(cArr, 0, i + 1);
    }

    /* renamed from: a */
    static int m10153a(long j, byte[] bArr, int i) {
        return m10154b((int) j, bArr, m10154b((int) (j >>> 32), bArr, i));
    }

    /* renamed from: nZ */
    private static void m10163nZ(int i) {
        String str;
        if (i < 0) {
            str = "Stack underflow: " + i;
        } else {
            str = "Too big stack: " + i;
        }
        throw new IllegalStateException(str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0044, code lost:
        r0 = r0 - 1;
        r3 = r3 + 1;
        r1 = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0068, code lost:
        r0 = r0 - 1;
        r3 = r3 + 1;
        r1 = r1 + 1;
        r5 = r8.indexOf(59, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0076, code lost:
        if ((r1 + 1) > r5) goto L_0x007a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0078, code lost:
        if (r5 < r6) goto L_0x007c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x007a, code lost:
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x007c, code lost:
        r1 = r5 + 1;
     */
    /* renamed from: gL */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int m10157gL(java.lang.String r8) {
        /*
            r4 = 1
            r2 = 0
            int r0 = r8.length()
            r1 = 41
            int r6 = r8.lastIndexOf(r1)
            r1 = 3
            if (r1 > r0) goto L_0x0084
            char r1 = r8.charAt(r2)
            r3 = 40
            if (r1 != r3) goto L_0x0084
            if (r4 > r6) goto L_0x0084
            int r1 = r6 + 1
            if (r1 >= r0) goto L_0x0084
            r3 = r2
            r0 = r2
            r1 = r4
        L_0x0020:
            if (r1 != r6) goto L_0x0039
            r1 = r4
        L_0x0023:
            if (r1 == 0) goto L_0x0084
            int r4 = r6 + 1
            char r4 = r8.charAt(r4)
            switch(r4) {
                case 66: goto L_0x0081;
                case 67: goto L_0x0081;
                case 68: goto L_0x007f;
                case 69: goto L_0x002e;
                case 70: goto L_0x0081;
                case 71: goto L_0x002e;
                case 72: goto L_0x002e;
                case 73: goto L_0x0081;
                case 74: goto L_0x007f;
                case 75: goto L_0x002e;
                case 76: goto L_0x0081;
                case 77: goto L_0x002e;
                case 78: goto L_0x002e;
                case 79: goto L_0x002e;
                case 80: goto L_0x002e;
                case 81: goto L_0x002e;
                case 82: goto L_0x002e;
                case 83: goto L_0x0081;
                case 84: goto L_0x002e;
                case 85: goto L_0x002e;
                case 86: goto L_0x002f;
                case 87: goto L_0x002e;
                case 88: goto L_0x002e;
                case 89: goto L_0x002e;
                case 90: goto L_0x0081;
                case 91: goto L_0x0081;
                default: goto L_0x002e;
            }
        L_0x002e:
            r1 = r2
        L_0x002f:
            if (r1 == 0) goto L_0x0084
            int r1 = r3 << 16
            r2 = 65535(0xffff, float:9.1834E-41)
            r0 = r0 & r2
            r0 = r0 | r1
            return r0
        L_0x0039:
            char r5 = r8.charAt(r1)
            switch(r5) {
                case 66: goto L_0x0044;
                case 67: goto L_0x0044;
                case 68: goto L_0x0042;
                case 70: goto L_0x0044;
                case 73: goto L_0x0044;
                case 74: goto L_0x0042;
                case 76: goto L_0x0068;
                case 83: goto L_0x0044;
                case 90: goto L_0x0044;
                case 91: goto L_0x004b;
                default: goto L_0x0040;
            }
        L_0x0040:
            r1 = r2
            goto L_0x0023
        L_0x0042:
            int r0 = r0 + -1
        L_0x0044:
            int r0 = r0 + -1
            int r3 = r3 + 1
            int r1 = r1 + 1
            goto L_0x0020
        L_0x004b:
            int r1 = r1 + 1
            char r5 = r8.charAt(r1)
        L_0x0051:
            r7 = 91
            if (r5 == r7) goto L_0x005a
            switch(r5) {
                case 66: goto L_0x0061;
                case 67: goto L_0x0061;
                case 68: goto L_0x0061;
                case 70: goto L_0x0061;
                case 73: goto L_0x0061;
                case 74: goto L_0x0061;
                case 76: goto L_0x0068;
                case 83: goto L_0x0061;
                case 90: goto L_0x0061;
                default: goto L_0x0058;
            }
        L_0x0058:
            r1 = r2
            goto L_0x0023
        L_0x005a:
            int r1 = r1 + 1
            char r5 = r8.charAt(r1)
            goto L_0x0051
        L_0x0061:
            int r0 = r0 + -1
            int r3 = r3 + 1
            int r1 = r1 + 1
            goto L_0x0020
        L_0x0068:
            int r0 = r0 + -1
            int r3 = r3 + 1
            int r1 = r1 + 1
            r5 = 59
            int r5 = r8.indexOf(r5, r1)
            int r1 = r1 + 1
            if (r1 > r5) goto L_0x007a
            if (r5 < r6) goto L_0x007c
        L_0x007a:
            r1 = r2
            goto L_0x0023
        L_0x007c:
            int r1 = r5 + 1
            goto L_0x0020
        L_0x007f:
            int r0 = r0 + 1
        L_0x0081:
            int r0 = r0 + 1
            goto L_0x002f
        L_0x0084:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Bad parameter signature: "
            r1.<init>(r2)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: C1383UH.m10157gL(java.lang.String):int");
    }

    /* renamed from: a */
    static int m10152a(int i, byte[] bArr, int i2) {
        bArr[i2 + 0] = (byte) (i >>> 8);
        bArr[i2 + 1] = (byte) i;
        return i2 + 2;
    }

    /* renamed from: b */
    static int m10154b(int i, byte[] bArr, int i2) {
        bArr[i2 + 0] = (byte) (i >>> 24);
        bArr[i2 + 1] = (byte) (i >>> 16);
        bArr[i2 + 2] = (byte) (i >>> 8);
        bArr[i2 + 3] = (byte) i;
        return i2 + 4;
    }

    /* renamed from: oa */
    static int m10164oa(int i) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
            case 68:
            case 69:
            case 70:
            case 71:
            case 72:
            case 73:
            case 74:
            case 75:
            case 76:
            case 77:
            case 78:
            case 79:
            case 80:
            case 81:
            case 82:
            case 83:
            case 84:
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
            case 92:
            case 93:
            case 94:
            case 95:
            case 96:
            case 97:
            case 98:
            case 99:
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case 108:
            case 109:
            case 110:
            case 111:
            case 112:
            case 113:
            case 114:
            case 115:
            case 116:
            case 117:
            case 118:
            case 119:
            case 120:
            case 121:
            case 122:
            case 123:
            case 124:
            case 125:
            case 126:
            case 127:
            case 128:
            case 129:
            case 130:
            case 131:
            case 133:
            case 134:
            case 135:
            case 136:
            case 137:
            case 138:
            case 139:
            case 140:
            case 141:
            case 142:
            case 143:
            case 144:
            case 145:
            case 146:
            case 147:
            case 148:
            case 149:
            case 150:
            case 151:
            case 152:
            case 172:
            case 173:
            case 174:
            case 175:
            case 176:
            case 177:
            case 190:
            case 191:
            case 194:
            case 195:
            case 196:
            case 202:
            case 254:
            case 255:
                return 0;
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 153:
            case 154:
            case 155:
            case 156:
            case 157:
            case 158:
            case 159:
            case 160:
            case 161:
            case 162:
            case 163:
            case 164:
            case 165:
            case 166:
            case 167:
            case 168:
            case 169:
            case 178:
            case 179:
            case 180:
            case 181:
            case 182:
            case 183:
            case 184:
            case 185:
            case 187:
            case 188:
            case 189:
            case 192:
            case 193:
            case 198:
            case 199:
            case 200:
            case 201:
                return 1;
            case 132:
            case 197:
                return 2;
            case 170:
            case 171:
                return -1;
            default:
                throw new IllegalArgumentException("Bad opcode: " + i);
        }
    }

    /* renamed from: ob */
    static int m10165ob(int i) {
        switch (i) {
            case 0:
            case 47:
            case 49:
            case 95:
            case 116:
            case 117:
            case 118:
            case 119:
            case 132:
            case 134:
            case 138:
            case 139:
            case 143:
            case 145:
            case 146:
            case 147:
            case 167:
            case 169:
            case 177:
            case 178:
            case 179:
            case 184:
            case 188:
            case 189:
            case 190:
            case 192:
            case 193:
            case 196:
            case 200:
            case 202:
            case 254:
            case 255:
                return 0;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 11:
            case 12:
            case 13:
            case 16:
            case 17:
            case 18:
            case 19:
            case 21:
            case 23:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 34:
            case 35:
            case 36:
            case 37:
            case 42:
            case 43:
            case 44:
            case 45:
            case 89:
            case 90:
            case 91:
            case 133:
            case 135:
            case 140:
            case 141:
            case 168:
            case 187:
            case 197:
            case 201:
                return 1;
            case 9:
            case 10:
            case 14:
            case 15:
            case 20:
            case 22:
            case 24:
            case 30:
            case 31:
            case 32:
            case 33:
            case 38:
            case 39:
            case 40:
            case 41:
            case 92:
            case 93:
            case 94:
                return 2;
            case 46:
            case 48:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 56:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 67:
            case 68:
            case 69:
            case 70:
            case 75:
            case 76:
            case 77:
            case 78:
            case 87:
            case 96:
            case 98:
            case 100:
            case 102:
            case 104:
            case 106:
            case 108:
            case 110:
            case 112:
            case 114:
            case 120:
            case 121:
            case 122:
            case 123:
            case 124:
            case 125:
            case 126:
            case 128:
            case 130:
            case 136:
            case 137:
            case 142:
            case 144:
            case 149:
            case 150:
            case 153:
            case 154:
            case 155:
            case 156:
            case 157:
            case 158:
            case 170:
            case 171:
            case 172:
            case 174:
            case 176:
            case 180:
            case 181:
            case 182:
            case 183:
            case 185:
            case 191:
            case 194:
            case 195:
            case 198:
            case 199:
                return -1;
            case 55:
            case 57:
            case 63:
            case 64:
            case 65:
            case 66:
            case 71:
            case 72:
            case 73:
            case 74:
            case 88:
            case 97:
            case 99:
            case 101:
            case 103:
            case 105:
            case 107:
            case 109:
            case 111:
            case 113:
            case 115:
            case 127:
            case 129:
            case 131:
            case 159:
            case 160:
            case 161:
            case 162:
            case 163:
            case 164:
            case 165:
            case 166:
            case 173:
            case 175:
                return -2;
            case 79:
            case 81:
            case 83:
            case 84:
            case 85:
            case 86:
            case 148:
            case 151:
            case 152:
                return -3;
            case 80:
            case 82:
                return -4;
            default:
                throw new IllegalArgumentException("Bad opcode: " + i);
        }
    }

    /* renamed from: oc */
    private static String m10166oc(int i) {
        return "";
    }

    public final String getClassName() {
        return this.epA;
    }

    public void addInterface(String str) {
        this.itsInterfaces.add(new Short(this.epH.mo5982hk(str)));
    }

    /* renamed from: m */
    public void mo5810m(short s) {
        this.itsFlags = s;
    }

    /* renamed from: a */
    public void mo5785a(String str, String str2, short s) {
        this.itsFields.add(new org.mozilla1.classfile.ClassFileField(this.epH.mo5981hj(str), this.epH.mo5981hj(str2), s));
    }

    /* renamed from: a */
    public void mo5787a(String str, String str2, short s, int i) {
        org.mozilla1.classfile.ClassFileField ob = new org.mozilla1.classfile.ClassFileField(this.epH.mo5981hj(str), this.epH.mo5981hj(str2), s);
        ob.mo4324a(this.epH.mo5981hj("ConstantValue"), 0, 0, this.epH.mo5987oL(i));
        this.itsFields.add(ob);
    }

    /* renamed from: a */
    public void mo5788a(String str, String str2, short s, long j) {
        org.mozilla1.classfile.ClassFileField ob = new org.mozilla1.classfile.ClassFileField(this.epH.mo5981hj(str), this.epH.mo5981hj(str2), s);
        ob.mo4324a(this.epH.mo5981hj("ConstantValue"), 0, 2, this.epH.mo5978fR(j));
        this.itsFields.add(ob);
    }

    /* renamed from: a */
    public void mo5786a(String str, String str2, short s, double d) {
        org.mozilla1.classfile.ClassFileField ob = new org.mozilla1.classfile.ClassFileField(this.epH.mo5981hj(str), this.epH.mo5981hj(str2), s);
        ob.mo4324a(this.epH.mo5981hj("ConstantValue"), 0, 2, this.epH.mo5973G(d));
        this.itsFields.add(ob);
    }

    /* renamed from: a */
    public void mo5784a(String str, String str2, int i, int i2) {
        int[] iArr = {this.epH.mo5981hj(str), this.epH.mo5981hj(str2), i, i2};
        if (this.itsVarDescriptors == null) {
            this.itsVarDescriptors = new ObjArray();
        }
        this.itsVarDescriptors.add(iArr);
    }

    /* renamed from: b */
    public void mo5795b(String str, String str2, short s) {
        this.epI = new org.mozilla1.classfile.ClassFileMethod(this.epH.mo5981hj(str), this.epH.mo5981hj(str2), s);
        this.itsMethods.add(this.epI);
    }

    /* renamed from: n */
    public void stopMethod(short s) {
        int i;
        int i2;
        int a;
        int i3;
        if (this.epI == null) {
            throw new IllegalStateException("No method to stop");
        }
        byb();
        this.epL = s;
        if (this.epD != null) {
            i = (this.epE * 4) + 8;
        } else {
            i = 0;
        }
        if (this.itsVarDescriptors != null) {
            i2 = (this.itsVarDescriptors.size() * 10) + 8;
        } else {
            i2 = 0;
        }
        int i4 = i + this.epG + 14 + 2 + (this.epC * 8) + 2 + i2;
        if (i4 > 65536) {
            throw new ClassFileFormatException("generated bytecode for method exceeds 64K limit.");
        }
        byte[] bArr = new byte[i4];
        int b = m10154b(this.epG, bArr, m10152a((int) this.epL, bArr, m10152a((int) this.itsMaxStack, bArr, m10154b(i4 - 6, bArr, m10152a((int) this.epH.mo5981hj("Code"), bArr, 0)))));
        System.arraycopy(this.epF, 0, bArr, b, this.epG);
        int i5 = b + this.epG;
        if (this.epC > 0) {
            a = m10152a(this.epC, bArr, i5);
            int i6 = 0;
            while (i6 < this.epC) {
                org.mozilla1.classfile.ExceptionTableEntry zEVar = this.itsExceptionTable[i6];
                short nU = (short) m10159nU(zEVar.itsStartLabel);
                short nU2 = (short) m10159nU(zEVar.itsEndLabel);
                short nU3 = (short) m10159nU(zEVar.itsHandlerLabel);
                short s2 = zEVar.itsCatchType;
                if (nU == -1) {
                    throw new IllegalStateException("start label not defined");
                } else if (nU2 == -1) {
                    throw new IllegalStateException("end label not defined");
                } else if (nU3 == -1) {
                    throw new IllegalStateException("handler label not defined");
                } else {
                    a = m10152a((int) s2, bArr, m10152a((int) nU3, bArr, m10152a((int) nU2, bArr, m10152a((int) nU, bArr, a))));
                    i6++;
                }
            }
        } else {
            a = m10152a(0, bArr, i5);
        }
        if (this.epD != null) {
            i3 = 1;
        } else {
            i3 = 0;
        }
        if (this.itsVarDescriptors != null) {
            i3++;
        }
        int a2 = m10152a(i3, bArr, a);
        if (this.epD != null) {
            a2 = m10152a(this.epE, bArr, m10154b((this.epE * 4) + 2, bArr, m10152a((int) this.epH.mo5981hj("LineNumberTable"), bArr, a2)));
            for (int i7 = 0; i7 < this.epE; i7++) {
                a2 = m10154b(this.epD[i7], bArr, a2);
            }
        }
        if (this.itsVarDescriptors != null) {
            int a3 = m10152a((int) this.epH.mo5981hj("LocalVariableTable"), bArr, a2);
            int size = this.itsVarDescriptors.size();
            int a4 = m10152a(size, bArr, m10154b((size * 10) + 2, bArr, a3));
            for (int i8 = 0; i8 < size; i8++) {
                int[] iArr = (int[]) this.itsVarDescriptors.get(i8);
                int i9 = iArr[0];
                int i10 = iArr[1];
                int i11 = iArr[2];
                a4 = m10152a(iArr[3], bArr, m10152a(i10, bArr, m10152a(i9, bArr, m10152a(this.epG - i11, bArr, m10152a(i11, bArr, a4)))));
            }
        }
        this.epI.mo14883h(bArr);
        this.itsExceptionTable = null;
        this.epC = 0;
        this.epE = 0;
        this.epG = 0;
        this.epI = null;
        this.itsMaxStack = 0;
        this.itsStackTop = 0;
        this.epU = 0;
        this.itsFixupTableTop = 0;
        this.itsVarDescriptors = null;
    }

    public void add(int i) {
        if (m10164oa(i) != 0) {
            throw new IllegalArgumentException("Unexpected operands");
        }
        int ob = this.itsStackTop + m10165ob(i);
        if (ob < 0 || 32767 < ob) {
            m10163nZ(ob);
        }
        m10160nW(i);
        this.itsStackTop = (short) ob;
        if (ob > this.itsMaxStack) {
            this.itsMaxStack = (short) ob;
        }
    }

    public void add(int i, int i2) {
        int ob = this.itsStackTop + m10165ob(i);
        if (ob < 0 || 32767 < ob) {
            m10163nZ(ob);
        }
        switch (i) {
            case 16:
                if (((byte) i2) == i2) {
                    m10160nW(i);
                    m10160nW((byte) i2);
                    break;
                } else {
                    throw new IllegalArgumentException("out of range byte");
                }
            case 17:
                if (((short) i2) == i2) {
                    m10160nW(i);
                    m10161nX(i2);
                    break;
                } else {
                    throw new IllegalArgumentException("out of range short");
                }
            case 18:
            case 19:
            case 20:
                if (i2 >= 0 && i2 < 65536) {
                    if (i2 < 256 && i != 19 && i != 20) {
                        m10160nW(i);
                        m10160nW(i2);
                        break;
                    } else {
                        if (i == 18) {
                            m10160nW(19);
                        } else {
                            m10160nW(i);
                        }
                        m10161nX(i2);
                        break;
                    }
                } else {
                    throw new IllegalArgumentException("out of range index");
                }
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 169:
                if (i2 >= 0 && i2 < 65536) {
                    if (i2 < 256) {
                        m10160nW(i);
                        m10160nW(i2);
                        break;
                    } else {
                        m10160nW(196);
                        m10160nW(i);
                        m10161nX(i2);
                        break;
                    }
                } else {
                    throw new ClassFileFormatException("out of range variable");
                }
            case 153:
            case 154:
            case 155:
            case 156:
            case 157:
            case 158:
            case 159:
            case 160:
            case 161:
            case 162:
            case 163:
            case 164:
            case 165:
            case 166:
            case 167:
            case 168:
            case 198:
            case 199:
                if ((i2 & Integer.MIN_VALUE) == Integer.MIN_VALUE || (i2 >= 0 && i2 <= 65535)) {
                    int i3 = this.epG;
                    m10160nW(i);
                    if ((i2 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
                        int nU = m10159nU(i2);
                        if (nU == -1) {
                            m10151T(i2, i3 + 1);
                            m10161nX(0);
                            break;
                        } else {
                            m10161nX(nU - i3);
                            break;
                        }
                    } else {
                        m10161nX(i2);
                        break;
                    }
                } else {
                    throw new IllegalArgumentException("Bad label for branch");
                }
            case 180:
            case 181:
                if (i2 >= 0 && i2 < 65536) {
                    m10160nW(i);
                    m10161nX(i2);
                    break;
                } else {
                    throw new IllegalArgumentException("out of range field");
                }
            case 188:
                if (i2 >= 0 && i2 < 256) {
                    m10160nW(i);
                    m10160nW(i2);
                    break;
                } else {
                    throw new IllegalArgumentException("out of range index");
                }
            default:
                throw new IllegalArgumentException("Unexpected opcode for 1 operand");
        }
        this.itsStackTop = (short) ob;
        if (ob > this.itsMaxStack) {
            this.itsMaxStack = (short) ob;
        }
    }

    /* renamed from: nF */
    public void mo5813nF(int i) {
        switch (i) {
            case 0:
                add(3);
                return;
            case 1:
                add(4);
                return;
            case 2:
                add(5);
                return;
            case 3:
                add(6);
                return;
            case 4:
                add(7);
                return;
            case 5:
                add(8);
                return;
            default:
                add(18, this.epH.mo5987oL(i));
                return;
        }
    }

    /* renamed from: fM */
    public void mo5801fM(long j) {
        add(20, this.epH.mo5978fR(j));
    }

    /* renamed from: ic */
    public void mo5807ic(float f) {
        add(18, this.epH.mo5984iz(f));
    }

    /* renamed from: E */
    public void mo5778E(double d) {
        add(20, this.epH.mo5973G(d));
    }

    /* renamed from: gI */
    public void mo5803gI(String str) {
        add(18, this.epH.mo5979hh(str));
    }

    /* renamed from: k */
    public void mo5808k(int i, int i2, int i3) {
        int ob = this.itsStackTop + m10165ob(i);
        if (ob < 0 || 32767 < ob) {
            m10163nZ(ob);
        }
        if (i == 132) {
            if (i2 < 0 || i2 >= 65536) {
                throw new ClassFileFormatException("out of range variable");
            } else if (i3 < 0 || i3 >= 65536) {
                throw new ClassFileFormatException("out of range increment");
            } else if (i2 > 255 || i3 < -128 || i3 > 127) {
                m10160nW(196);
                m10160nW(132);
                m10161nX(i2);
                m10161nX(i3);
            } else {
                m10160nW(196);
                m10160nW(132);
                m10160nW(i2);
                m10160nW(i3);
            }
        } else if (i != 197) {
            throw new IllegalArgumentException("Unexpected opcode for 2 operands");
        } else if (i2 < 0 || i2 >= 65536) {
            throw new IllegalArgumentException("out of range index");
        } else if (i3 < 0 || i3 >= 256) {
            throw new IllegalArgumentException("out of range dimensions");
        } else {
            m10160nW(197);
            m10161nX(i2);
            m10160nW(i3);
        }
        this.itsStackTop = (short) ob;
        if (ob > this.itsMaxStack) {
            this.itsMaxStack = (short) ob;
        }
    }

    public void add(int i, String str) {
        int ob = this.itsStackTop + m10165ob(i);
        if (ob < 0 || 32767 < ob) {
            m10163nZ(ob);
        }
        switch (i) {
            case 187:
            case 189:
            case 192:
            case 193:
                short hk = this.epH.mo5982hk(str);
                m10160nW(i);
                m10161nX(hk);
                this.itsStackTop = (short) ob;
                if (ob > this.itsMaxStack) {
                    this.itsMaxStack = (short) ob;
                    return;
                }
                return;
            default:
                throw new IllegalArgumentException("bad opcode for class reference");
        }
    }

    /* renamed from: a */
    public void mo5782a(int i, String str, String str2, String str3) {
        int i2;
        int ob = m10165ob(i) + this.itsStackTop;
        char charAt = str3.charAt(0);
        int i3 = (charAt == 'J' || charAt == 'D') ? 2 : 1;
        switch (i) {
            case 178:
            case 180:
                i2 = i3 + ob;
                break;
            case 179:
            case 181:
                i2 = ob - i3;
                break;
            default:
                throw new IllegalArgumentException("bad opcode for field reference");
        }
        if (i2 < 0 || 32767 < i2) {
            m10163nZ(i2);
        }
        short i4 = this.epH.mo5983i(str, str2, str3);
        m10160nW(i);
        m10161nX(i4);
        this.itsStackTop = (short) i2;
        if (i2 > this.itsMaxStack) {
            this.itsMaxStack = (short) i2;
        }
    }

    /* renamed from: b */
    public void mo5794b(int i, String str, String str2, String str3) {
        int gL = m10157gL(str3);
        int i2 = gL >>> 16;
        int ob = ((short) gL) + this.itsStackTop + m10165ob(i);
        if (ob < 0 || 32767 < ob) {
            m10163nZ(ob);
        }
        switch (i) {
            case 182:
            case 183:
            case 184:
            case 185:
                m10160nW(i);
                if (i == 185) {
                    m10161nX(this.epH.mo5986k(str, str2, str3));
                    m10160nW(i2 + 1);
                    m10160nW(0);
                } else {
                    m10161nX(this.epH.mo5985j(str, str2, str3));
                }
                this.itsStackTop = (short) ob;
                if (ob > this.itsMaxStack) {
                    this.itsMaxStack = (short) ob;
                    return;
                }
                return;
            default:
                throw new IllegalArgumentException("bad opcode for method reference");
        }
    }

    /* renamed from: nG */
    public void mo5814nG(int i) {
        if (((byte) i) == i) {
            if (i == -1) {
                add(2);
            } else if (i < 0 || i > 5) {
                add(16, (int) (byte) i);
            } else {
                add((byte) (i + 3));
            }
        } else if (((short) i) == i) {
            add(17, (int) (short) i);
        } else {
            mo5813nF(i);
        }
    }

    /* renamed from: dx */
    public void mo5800dx(boolean z) {
        add(z ? 4 : 3);
    }

    /* renamed from: fN */
    public void mo5802fN(long j) {
        int i = (int) j;
        if (((long) i) == j) {
            mo5814nG(i);
            add(133);
            return;
        }
        mo5801fM(j);
    }

    /* renamed from: F */
    public void mo5779F(double d) {
        if (d == C0903NH.NaN) {
            add(14);
            if (1.0d / d < C0903NH.NaN) {
                add(119);
            }
        } else if (d == 1.0d || d == -1.0d) {
            add(15);
            if (d < C0903NH.NaN) {
                add(119);
            }
        } else {
            mo5778E(d);
        }
    }

    /* renamed from: gJ */
    public void mo5804gJ(String str) {
        int i = 0;
        int length = str.length();
        int d = this.epH.mo5976d(str, 0, length);
        if (d == length) {
            mo5803gI(str);
            return;
        }
        add(187, "java/lang/StringBuffer");
        add(89);
        mo5814nG(length);
        mo5794b(183, "java/lang/StringBuffer", C2821kb.arR, "(I)V");
        while (true) {
            add(89);
            mo5803gI(str.substring(i, d));
            mo5794b(182, "java/lang/StringBuffer", "append", "(Ljava/lang/String;)Ljava/lang/StringBuffer;");
            add(87);
            if (d == length) {
                mo5794b(182, "java/lang/StringBuffer", "toString", "()Ljava/lang/String;");
                return;
            }
            i = d;
            d = this.epH.mo5976d(str, d, length);
        }
    }

    /* renamed from: gK */
    public boolean mo5805gK(String str) {
        return this.epH.mo5980hi(str);
    }

    /* renamed from: nH */
    public void mo5815nH(int i) {
        m10158l(59, 54, i);
    }

    /* renamed from: nI */
    public void mo5816nI(int i) {
        m10158l(63, 55, i);
    }

    /* renamed from: nJ */
    public void mo5817nJ(int i) {
        m10158l(67, 56, i);
    }

    /* renamed from: nK */
    public void mo5818nK(int i) {
        m10158l(71, 57, i);
    }

    /* renamed from: nL */
    public void mo5819nL(int i) {
        m10158l(75, 58, i);
    }

    /* renamed from: nM */
    public void mo5820nM(int i) {
        m10158l(26, 21, i);
    }

    /* renamed from: nN */
    public void mo5821nN(int i) {
        m10158l(30, 22, i);
    }

    /* renamed from: nO */
    public void mo5822nO(int i) {
        m10158l(34, 23, i);
    }

    /* renamed from: nP */
    public void mo5823nP(int i) {
        m10158l(38, 24, i);
    }

    /* renamed from: nQ */
    public void mo5824nQ(int i) {
        m10158l(42, 25, i);
    }

    public void bxZ() {
        add(42);
    }

    /* renamed from: l */
    private void m10158l(int i, int i2, int i3) {
        switch (i3) {
            case 0:
                add(i);
                return;
            case 1:
                add(i + 1);
                return;
            case 2:
                add(i + 2);
                return;
            case 3:
                add(i + 3);
                return;
            default:
                add(i2, i3);
                return;
        }
    }

    /* renamed from: R */
    public int mo5780R(int i, int i2) {
        if (i > i2) {
            throw new ClassFileFormatException("Bad bounds: " + i + ' ' + i2);
        }
        int ob = this.itsStackTop + m10165ob(170);
        if (ob < 0 || 32767 < ob) {
            m10163nZ(ob);
        }
        int i3 = (this.epG ^ -1) & 3;
        int nY = m10162nY((((i2 - i) + 1 + 3) * 4) + i3 + 1);
        int i4 = nY + 1;
        this.epF[nY] = -86;
        int i5 = i3;
        while (i5 != 0) {
            this.epF[i4] = 0;
            i5--;
            i4++;
        }
        m10154b(i2, this.epF, m10154b(i, this.epF, i4 + 4));
        this.itsStackTop = (short) ob;
        if (ob > this.itsMaxStack) {
            this.itsMaxStack = (short) ob;
        }
        return nY;
    }

    /* renamed from: nR */
    public final void mo5825nR(int i) {
        mo5811n(i, -1, this.epG);
    }

    /* renamed from: S */
    public final void mo5781S(int i, int i2) {
        mo5811n(i, i2, this.epG);
    }

    /* renamed from: m */
    public final void mo5809m(int i, int i2, int i3) {
        if (i3 < 0 || i3 > this.itsMaxStack) {
            throw new IllegalArgumentException("Bad stack index: " + i3);
        }
        this.itsStackTop = (short) i3;
        mo5811n(i, i2, this.epG);
    }

    /* renamed from: n */
    public void mo5811n(int i, int i2, int i3) {
        int i4;
        if (i3 < 0 || i3 > this.epG) {
            throw new IllegalArgumentException("Bad jump target: " + i3);
        } else if (i2 < -1) {
            throw new IllegalArgumentException("Bad case index: " + i2);
        } else {
            int i5 = (i ^ -1) & 3;
            if (i2 < 0) {
                i4 = i + 1 + i5;
            } else {
                i4 = i + 1 + i5 + ((i2 + 3) * 4);
            }
            if (i < 0 || i > ((this.epG - 16) - i5) - 1) {
                throw new IllegalArgumentException(String.valueOf(i) + " is outside a possible range of tableswitch" + " in already generated code");
            } else if ((this.epF[i] & 255) != 170) {
                throw new IllegalArgumentException(String.valueOf(i) + " is not offset of tableswitch statement");
            } else if (i4 < 0 || i4 + 4 > this.epG) {
                throw new ClassFileFormatException("Too big case index: " + i2);
            } else {
                m10154b(i3 - i, this.epF, i4);
            }
        }
    }

    public int bya() {
        int i = this.epU;
        if (this.epT == null || i == this.epT.length) {
            if (this.epT == null) {
                this.epT = new int[32];
            } else {
                int[] iArr = new int[(this.epT.length * 2)];
                System.arraycopy(this.epT, 0, iArr, 0, i);
                this.epT = iArr;
            }
        }
        this.epU = i + 1;
        this.epT[i] = -1;
        return i | Integer.MIN_VALUE;
    }

    /* renamed from: nS */
    public void mo5826nS(int i) {
        if (i >= 0) {
            throw new IllegalArgumentException("Bad label, no biscuit");
        }
        int i2 = Integer.MAX_VALUE & i;
        if (i2 > this.epU) {
            throw new IllegalArgumentException("Bad label");
        } else if (this.epT[i2] != -1) {
            throw new IllegalStateException("Can only mark label once");
        } else {
            this.epT[i2] = this.epG;
        }
    }

    /* renamed from: a */
    public void mo5783a(int i, short s) {
        mo5826nS(i);
        this.itsStackTop = s;
    }

    /* renamed from: nT */
    public void mo5827nT(int i) {
        this.itsStackTop = 1;
        mo5826nS(i);
    }

    /* renamed from: nU */
    private int m10159nU(int i) {
        if (i >= 0) {
            throw new IllegalArgumentException("Bad label, no biscuit");
        }
        int i2 = Integer.MAX_VALUE & i;
        if (i2 < this.epU) {
            return this.epT[i2];
        }
        throw new IllegalArgumentException("Bad label");
    }

    /* renamed from: T */
    private void m10151T(int i, int i2) {
        if (i >= 0) {
            throw new IllegalArgumentException("Bad label, no biscuit");
        }
        int i3 = Integer.MAX_VALUE & i;
        if (i3 >= this.epU) {
            throw new IllegalArgumentException("Bad label");
        }
        int i4 = this.itsFixupTableTop;
        if (this.itsFixupTable == null || i4 == this.itsFixupTable.length) {
            if (this.itsFixupTable == null) {
                this.itsFixupTable = new long[40];
            } else {
                long[] jArr = new long[(this.itsFixupTable.length * 2)];
                System.arraycopy(this.itsFixupTable, 0, jArr, 0, i4);
                this.itsFixupTable = jArr;
            }
        }
        this.itsFixupTableTop = i4 + 1;
        this.itsFixupTable[i4] = (((long) i3) << 32) | ((long) i2);
    }

    private void byb() {
        byte[] bArr = this.epF;
        for (int i = 0; i < this.itsFixupTableTop; i++) {
            long j = this.itsFixupTable[i];
            int i2 = (int) j;
            int i3 = this.epT[(int) (j >> 32)];
            if (i3 == -1) {
                throw new RuntimeException();
            }
            int i4 = i3 - (i2 - 1);
            if (((short) i4) != i4) {
                throw new ClassFileFormatException("Program too complex: too big jump offset");
            }
            bArr[i2] = (byte) (i4 >> 8);
            bArr[i2 + 1] = (byte) i4;
        }
        this.itsFixupTableTop = 0;
    }

    public int byc() {
        return this.epG;
    }

    public short byd() {
        return this.itsStackTop;
    }

    /* renamed from: o */
    public void mo5829o(short s) {
        this.itsStackTop = s;
    }

    /* renamed from: nV */
    public void mo5828nV(int i) {
        int i2 = this.itsStackTop + i;
        if (i2 < 0 || 32767 < i2) {
            m10163nZ(i2);
        }
        this.itsStackTop = (short) i2;
        if (i2 > this.itsMaxStack) {
            this.itsMaxStack = (short) i2;
        }
    }

    /* renamed from: nW */
    private void m10160nW(int i) {
        this.epF[m10162nY(1)] = (byte) i;
    }

    /* renamed from: nX */
    private void m10161nX(int i) {
        m10152a(i, this.epF, m10162nY(2));
    }

    /* renamed from: nY */
    private int m10162nY(int i) {
        if (this.epI == null) {
            throw new IllegalArgumentException("No method to add to");
        }
        int i2 = this.epG;
        int i3 = i2 + i;
        if (i3 > this.epF.length) {
            int length = this.epF.length * 2;
            if (i3 > length) {
                length = i3;
            }
            byte[] bArr = new byte[length];
            System.arraycopy(this.epF, 0, bArr, 0, i2);
            this.epF = bArr;
        }
        this.epG = i3;
        return i2;
    }

    public void addExceptionHandler(int i, int i2, int i3, String str) {
        short hk;
        if ((i & Integer.MIN_VALUE) != Integer.MIN_VALUE) {
            throw new IllegalArgumentException("Bad startLabel");
        } else if ((i2 & Integer.MIN_VALUE) != Integer.MIN_VALUE) {
            throw new IllegalArgumentException("Bad endLabel");
        } else if ((i3 & Integer.MIN_VALUE) != Integer.MIN_VALUE) {
            throw new IllegalArgumentException("Bad handlerLabel");
        } else {
            if (str == null) {
                hk = 0;
            } else {
                hk = this.epH.mo5982hk(str);
            }
            org.mozilla1.classfile.ExceptionTableEntry zEVar = new org.mozilla1.classfile.ExceptionTableEntry(i, i2, i3, hk);
            int i4 = this.epC;
            if (i4 == 0) {
                this.itsExceptionTable = new org.mozilla1.classfile.ExceptionTableEntry[4];
            } else if (i4 == this.itsExceptionTable.length) {
                org.mozilla1.classfile.ExceptionTableEntry[] zEVarArr = new org.mozilla1.classfile.ExceptionTableEntry[(i4 * 2)];
                System.arraycopy(this.itsExceptionTable, 0, zEVarArr, 0, i4);
                this.itsExceptionTable = zEVarArr;
            }
            this.itsExceptionTable[i4] = zEVar;
            this.epC = i4 + 1;
        }
    }

    /* renamed from: p */
    public void mo5831p(short s) {
        if (this.epI == null) {
            throw new IllegalArgumentException("No method to stop");
        }
        int i = this.epE;
        if (i == 0) {
            this.epD = new int[16];
        } else if (i == this.epD.length) {
            int[] iArr = new int[(i * 2)];
            System.arraycopy(this.epD, 0, iArr, 0, i);
            this.epD = iArr;
        }
        this.epD[i] = (this.epG << 16) + s;
        this.epE = i + 1;
    }

    public void write(OutputStream outputStream) {
        outputStream.write(toByteArray());
    }

    private int blR() {
        int i;
        int i2 = 0;
        if (this.epR != 0) {
            this.epH.mo5981hj("SourceFile");
        }
        int blR = this.epH.blR() + 8 + 2 + 2 + 2 + 2 + (this.itsInterfaces.size() * 2) + 2;
        for (int i3 = 0; i3 < this.itsFields.size(); i3++) {
            blR += ((org.mozilla1.classfile.ClassFileField) this.itsFields.get(i3)).blR();
        }
        int i4 = blR + 2;
        while (true) {
            i = i4;
            if (i2 >= this.itsMethods.size()) {
                break;
            }
            i4 = ((org.mozilla1.classfile.ClassFileMethod) this.itsMethods.get(i2)).blR() + i;
            i2++;
        }
        if (this.epR != 0) {
            return i + 2 + 2 + 4 + 2;
        }
        return i + 2;
    }

    public byte[] toByteArray() {
        short s;
        int a;
        int blR = blR();
        byte[] bArr = new byte[blR];
        if (this.epR != 0) {
            s = this.epH.mo5981hj("SourceFile");
        } else {
            s = 0;
        }
        int a2 = m10152a(this.itsInterfaces.size(), bArr, m10152a((int) this.epQ, bArr, m10152a((int) this.epP, bArr, m10152a((int) this.itsFlags, bArr, this.epH.mo5974b(bArr, m10153a((long) epw, bArr, 0))))));
        for (int i = 0; i < this.itsInterfaces.size(); i++) {
            a2 = m10152a((int) ((Short) this.itsInterfaces.get(i)).shortValue(), bArr, a2);
        }
        int a3 = m10152a(this.itsFields.size(), bArr, a2);
        for (int i2 = 0; i2 < this.itsFields.size(); i2++) {
            a3 = ((org.mozilla1.classfile.ClassFileField) this.itsFields.get(i2)).mo4325b(bArr, a3);
        }
        int a4 = m10152a(this.itsMethods.size(), bArr, a3);
        for (int i3 = 0; i3 < this.itsMethods.size(); i3++) {
            a4 = ((org.mozilla1.classfile.ClassFileMethod) this.itsMethods.get(i3)).mo14881b(bArr, a4);
        }
        if (this.epR != 0) {
            a = m10152a((int) this.epR, bArr, m10154b(2, bArr, m10152a((int) s, bArr, m10152a(1, bArr, a4))));
        } else {
            a = m10152a(0, bArr, a4);
        }
        if (a == blR) {
            return bArr;
        }
        throw new RuntimeException();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: od */
    public final char[] getCharBuffer(int i) {
        if (i > this.tmpCharBuffer.length) {
            int length = this.tmpCharBuffer.length * 2;
            if (i <= length) {
                i = length;
            }
            this.tmpCharBuffer = new char[i];
        }
        return this.tmpCharBuffer;
    }

    /* renamed from: a.UH$a */
    public static class ClassFileFormatException extends RuntimeException {
        private static final long serialVersionUID = 1263998431033790599L;

        ClassFileFormatException(String str) {
            super(str);
        }
    }
}
