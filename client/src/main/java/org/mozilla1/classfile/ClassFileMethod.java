package org.mozilla1.classfile;

/* renamed from: a.aml  reason: case insensitive filesystem */
/* compiled from: a */
final class ClassFileMethod {
    private short dMk;
    private short dMl;
    private short dMm;
    private byte[] fZW;

    ClassFileMethod(short s, short s2, short s3) {
        this.dMk = s;
        this.dMl = s2;
        this.dMm = s3;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h */
    public void mo14883h(byte[] bArr) {
        this.fZW = bArr;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public int mo14881b(byte[] bArr, int i) {
        int a = ClassFileWriter.m10152a(1, bArr, ClassFileWriter.m10152a((int) this.dMl, bArr, ClassFileWriter.m10152a((int) this.dMk, bArr, ClassFileWriter.m10152a((int) this.dMm, bArr, i))));
        System.arraycopy(this.fZW, 0, bArr, a, this.fZW.length);
        return a + this.fZW.length;
    }

    /* access modifiers changed from: package-private */
    public int blR() {
        return this.fZW.length + 8;
    }
}
