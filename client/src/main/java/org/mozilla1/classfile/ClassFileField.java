package org.mozilla1.classfile;

/* renamed from: a.OB */
/* compiled from: a */
public final class ClassFileField {
    private short dMk;
    private short dMl;
    private short dMm;
    private boolean dMn = false;
    private short dMo;
    private short dMp;
    private short dMq;
    private int dMr;

    public ClassFileField(short s, short s2, short s3) {
        this.dMk = s;
        this.dMl = s2;
        this.dMm = s3;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo4324a(short s, short s2, short s3, int i) {
        this.dMn = true;
        this.dMo = s;
        this.dMp = s2;
        this.dMq = s3;
        this.dMr = i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public int mo4325b(byte[] bArr, int i) {
        int a = ClassFileWriter.m10152a((int) this.dMl, bArr, ClassFileWriter.m10152a((int) this.dMk, bArr, ClassFileWriter.m10152a((int) this.dMm, bArr, i)));
        if (!this.dMn) {
            return ClassFileWriter.m10152a(0, bArr, a);
        }
        return ClassFileWriter.m10152a(this.dMr, bArr, ClassFileWriter.m10152a((int) this.dMq, bArr, ClassFileWriter.m10152a((int) this.dMp, bArr, ClassFileWriter.m10152a((int) this.dMo, bArr, ClassFileWriter.m10152a(1, bArr, a)))));
    }

    /* access modifiers changed from: package-private */
    public int blR() {
        if (!this.dMn) {
            return 8;
        }
        return 16;
    }
}
