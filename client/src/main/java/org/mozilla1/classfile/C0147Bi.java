package org.mozilla1.classfile;


import java.io.File;
import java.util.*;

/* renamed from: a.Bi */
/* compiled from: a */
public class C0147Bi implements Iterable<C0147Bi.C0149b> {
    public static final String SEPARATOR = "/";
    public static final char cla = '/';
    private String clb;

    private C0147Bi(String str) {
        this.clb = str;
    }

    /* renamed from: dA */
    public static C0147Bi m1293dA(String str) {
        return new C0147Bi(str);
    }

    /* renamed from: d */
    public static String m1292d(String... strArr) {
        char c;
        StringBuilder sb = new StringBuilder();
        for (String str : strArr) {
            if (str == null) {
                throw new IllegalArgumentException("null String found");
            }
            int length = str.length();
            if (length != 0) {
                char charAt = str.charAt(0);
                if (sb.length() > 0) {
                    c = sb.charAt(sb.length() - 1);
                } else {
                    c = 0;
                }
                if (charAt == '\\' || charAt == '/') {
                    sb.setLength(0);
                    sb.append(str);
                } else if (length > 2 && str.charAt(1) == ':' && ((str.charAt(2) == '\\' || str.charAt(2) == '/') && charAt >= 'A' && charAt <= 'z' && (charAt <= 'Z' || charAt >= 'a'))) {
                    sb.setLength(0);
                    sb.append(str);
                } else if (c == '\\' || c == '/') {
                    sb.append(str);
                } else {
                    if (sb.length() > 0) {
                        sb.append(SEPARATOR);
                    }
                    sb.append(str);
                }
            }
        }
        return sb.toString();
    }

    /* renamed from: dB */
    public static String m1294dB(String str) {
        return split(str)[1];
    }

    /* renamed from: dC */
    public static String m1295dC(String str) {
        return split(str)[0];
    }

    /* renamed from: dD */
    public static String[] m1296dD(String str) {
        int i;
        int lastIndexOf = str.lastIndexOf(47);
        if (lastIndexOf == -1) {
            lastIndexOf = str.lastIndexOf(92);
        }
        if (lastIndexOf == -1) {
            lastIndexOf = 0;
        }
        int lastIndexOf2 = str.lastIndexOf(46);
        if (lastIndexOf2 < lastIndexOf) {
            i = str.length();
        } else {
            i = lastIndexOf2;
        }
        return new String[]{str.substring(0, i), str.substring(i, str.length())};
    }

    public static String[] split(String str) {
        int i;
        int lastIndexOf = str.lastIndexOf(47);
        if (lastIndexOf == -1) {
            i = str.lastIndexOf(92);
        } else {
            i = lastIndexOf;
        }
        if (i == -1) {
            return new String[]{"", str};
        }
        return new String[]{str.substring(0, i), str.substring(i, str.length())};
    }

    /* renamed from: dE */
    private static String m1297dE(String str) {
        return str.replace('\\', cla);
    }

    /* renamed from: a */
    private static void m1291a(Stack<String> stack, String str) {
        if (str.equals("..")) {
            if (stack.size() > 0) {
                String pop = stack.pop();
                if (pop.equals("..")) {
                    stack.push("..");
                    stack.push("..");
                } else if (pop.length() == 2) {
                    char charAt = pop.charAt(0);
                    if (pop.charAt(1) != ':') {
                        return;
                    }
                    if (charAt >= 'A' && charAt <= 'Z') {
                        stack.push(pop);
                    } else if (charAt >= 'a' && charAt <= 'z') {
                        stack.push(pop);
                    }
                }
            } else {
                stack.push("..");
            }
        } else if (str.length() > 0 && !str.equals(".")) {
            stack.push(str);
        }
    }

    /* renamed from: dF */
    public static String m1298dF(String str) {
        int i;
        int i2 = 1;
        if (str.length() == 0) {
            return ".";
        }
        String dE = m1297dE(str);
        StringBuilder sb = new StringBuilder();
        Stack stack = new Stack();
        if (dE.charAt(0) == '/') {
            sb.append(cla);
            if (dE.length() > 1 && dE.charAt(1) == '/') {
                i2 = 2;
                sb.append(cla);
            }
        } else {
            i2 = 0;
        }
        int length = dE.length();
        int i3 = i2;
        int i4 = i2;
        while (i4 < length) {
            if (dE.charAt(i4) == '/') {
                m1291a(stack, dE.substring(i3, i4));
                i = i4 + 1;
            } else {
                i = i3;
            }
            i4++;
            i3 = i;
        }
        m1291a(stack, dE.substring(i3, i4));
        int size = stack.size();
        for (int i5 = 0; i5 < size; i5++) {
            if (i5 > 0) {
                sb.append(cla);
            }
            sb.append((String) stack.elementAt(i5));
        }
        if (sb.length() == 0) {
            return ".";
        }
        return sb.toString();
    }

    public static boolean exists(String str) {
        return new File(str).exists();
    }

    public Iterator<C0149b> iterator() {
        return new C0148a(this.clb);
    }

    /* renamed from: a.Bi$b */
    /* compiled from: a */
    public class C0149b {
        /* access modifiers changed from: private */
        public List<String> hxE;
        /* access modifiers changed from: private */
        public List<String> hxF;
        /* access modifiers changed from: private */
        public String path;

        /* synthetic */ C0149b(C0147Bi bi, String str, C0149b bVar) {
            this(str);
        }

        private C0149b(String str) {
            this.path = str;
        }

        public String getPath() {
            return this.path;
        }

        public String[] cRM() {
            return (String[]) this.hxE.toArray();
        }

        public String[] cRN() {
            return (String[]) this.hxF.toArray();
        }

        public List<String> cRO() {
            return this.hxE;
        }

        public List<String> cRP() {
            return this.hxF;
        }
    }

    /* renamed from: a.Bi$a */
    private class C0148a implements Iterator<C0149b> {
        private C0149b dbA = null;
        private Stack<C0149b> stack = new Stack<>();

        public C0148a(String str) {
            this.stack.push(m1299ew(str));
        }

        /* renamed from: ew */
        private C0149b m1299ew(String str) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            String[] list = new File(str).list();
            if (list == null) {
                throw new IllegalArgumentException();
            }
            for (String str2 : list) {
                File file = new File(str, str2);
                if (file.isDirectory()) {
                    arrayList.add(str2);
                } else if (file.isFile()) {
                    arrayList2.add(str2);
                }
            }
            C0149b bVar = new C0149b(C0147Bi.this, str, (C0149b) null);
            bVar.hxE = arrayList;
            bVar.hxF = arrayList2;
            return bVar;
        }

        public boolean hasNext() {
            if (!this.stack.isEmpty() || (this.dbA != null && this.dbA.hxE.size() != 0)) {
                return true;
            }
            return false;
        }

        /* renamed from: aRC */
        public C0149b next() {
            if (this.dbA != null) {
                for (String str : this.dbA.hxE) {
                    if (str != null) {
                        this.stack.push(m1299ew(C0147Bi.m1292d(this.dbA.path, str)));
                    }
                }
                try {
                    this.dbA = this.stack.pop();
                    return this.dbA;
                } catch (EmptyStackException e) {
                    throw new NoSuchElementException();
                }
            } else if (this.stack.isEmpty()) {
                throw new NoSuchElementException();
            } else {
                this.dbA = this.stack.pop();
                return this.dbA;
            }
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
