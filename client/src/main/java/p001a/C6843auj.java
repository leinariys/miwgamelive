package p001a;

import game.engine.SocketMessage;

import java.io.DataOutput;
import java.io.FilterOutputStream;
import java.io.OutputStream;

/* renamed from: a.auj  reason: case insensitive filesystem */
/* compiled from: a */
public final class C6843auj extends FilterOutputStream implements DataOutput {
    private SocketMessage gCN;

    public C6843auj(OutputStream outputStream) {
        super(outputStream);
    }

    public final void write(int i) {
        this.out.write(i);
    }

    public final void write(byte[] bArr, int i, int i2) {
        this.out.write(bArr, i, i2);
    }

    public final void flush() {
        this.out.flush();
    }

    public final void writeBoolean(boolean z) {
        this.out.write(z ? 1 : 0);
    }

    public final void writeByte(int i) {
        this.out.write(i);
    }

    public final void writeShort(int i) {
        this.out.write((i >> 8) & 255);
        this.out.write((i >> 0) & 255);
    }

    public final void writeChar(int i) {
        this.out.write((i >> 8) & 255);
        this.out.write(i & 255);
    }

    public final void writeInt(int i) {
        this.out.write((i >> 24) & 255);
        this.out.write((i >> 16) & 255);
        this.out.write((i >> 8) & 255);
        this.out.write(i & 255);
    }

    public final void writeLong(long j) {
        if (this.gCN == null) {
            this.gCN = new SocketMessage(8);
        }
        byte[] chG = this.gCN.getMessage();
        chG[0] = (byte) ((int) (j >> 56));
        chG[1] = (byte) ((int) (j >> 48));
        chG[2] = (byte) ((int) (j >> 40));
        chG[3] = (byte) ((int) (j >> 32));
        chG[4] = (byte) ((int) (j >> 24));
        chG[5] = (byte) ((int) (j >> 16));
        chG[6] = (byte) ((int) (j >> 8));
        chG[7] = (byte) ((int) j);
        this.out.write(chG, 0, 8);
    }

    public final void writeFloat(float f) {
        writeInt(Float.floatToIntBits(f));
    }

    public final void writeDouble(double d) {
        writeLong(Double.doubleToLongBits(d));
    }

    public final void writeBytes(String str) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            this.out.write((byte) str.charAt(i));
        }
    }

    public final void writeChars(String str) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            this.out.write((charAt >> 8) & 255);
            this.out.write(charAt & 255);
        }
    }

    public final void writeUTF(String str) {
        if (this.gCN == null) {
            this.gCN = new SocketMessage(Math.max(str.length() * 2, 8));
        }
        this.gCN.reset();
        this.gCN.writeUTF(str);
        this.gCN.writeTo(this.out);
    }
}
