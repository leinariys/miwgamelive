package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.amv  reason: case insensitive filesystem */
/* compiled from: a */
public class C6439amv {

    public final Vec3f gaQ = new Vec3f();
    public final Vec3f gaR = new Vec3f();
    public final Vec3f gaS = new Vec3f();
    public final C0763Kt stack = C0763Kt.bcE();
    /* renamed from: Tx */
    public float f4933Tx;
    public float dMe;
    public float damping;

    public C6439amv() {
        this.gaQ.set(0.0f, 0.0f, 0.0f);
        this.gaR.set(0.0f, 0.0f, 0.0f);
        this.gaS.set(0.0f, 0.0f, 0.0f);
        this.f4933Tx = 0.7f;
        this.damping = 1.0f;
        this.dMe = 0.5f;
    }

    public C6439amv(C6439amv amv) {
        this.gaQ.set(amv.gaQ);
        this.gaR.set(amv.gaR);
        this.gaS.set(amv.gaS);
        this.f4933Tx = amv.f4933Tx;
        this.damping = amv.damping;
        this.dMe = amv.dMe;
    }

    /* renamed from: dg */
    public boolean mo14912dg(int i) {
        return C0647JL.m5594b(this.gaR, i) >= C0647JL.m5594b(this.gaQ, i);
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public float mo14911a(float f, float f2, C6238ajC ajc, Vec3f vec3f, C6238ajC ajc2, Vec3f vec3f2, int i, Vec3f vec3f3) {
        float f3;
        this.stack.bcH().push();
        try {
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            vec3f5.sub(vec3f, ajc.ces());
            Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
            vec3f6.sub(vec3f2, ajc2.ces());
            Vec3f ac = this.stack.bcH().mo4458ac(ajc.mo13897R(vec3f5));
            Vec3f ac2 = this.stack.bcH().mo4458ac(ajc2.mo13897R(vec3f6));
            Vec3f vec3f7 = (Vec3f) this.stack.bcH().get();
            vec3f7.sub(ac, ac2);
            float dot = vec3f3.dot(vec3f7);
            vec3f4.sub(vec3f, vec3f2);
            float f4 = -vec3f4.dot(vec3f3);
            float f5 = -1.0E30f;
            float b = C0647JL.m5594b(this.gaQ, i);
            float b2 = C0647JL.m5594b(this.gaR, i);
            if (b >= b2) {
                f3 = 1.0E30f;
            } else if (f4 > b2) {
                f4 -= b2;
                f5 = 0.0f;
                f3 = 1.0E30f;
            } else if (f4 < b) {
                f4 -= b;
                f3 = 0.0f;
            } else {
                this.stack.bcH().pop();
                return 0.0f;
            }
            float b3 = C0647JL.m5594b(this.gaS, i);
            float f6 = (this.f4933Tx * (((f4 * this.dMe) / f) - (dot * this.damping)) * f2) + b3;
            Vec3f vec3f8 = this.gaS;
            if (f6 > f3) {
                f6 = 0.0f;
            } else if (f6 < f5) {
                f6 = 0.0f;
            }
            C0647JL.m5590a(vec3f8, i, f6);
            float b4 = C0647JL.m5594b(this.gaS, i) - b3;
            Vec3f vec3f9 = (Vec3f) this.stack.bcH().get();
            vec3f9.scale(b4, vec3f3);
            ajc.mo13937m(vec3f9, vec3f5);
            vec3f4.negate(vec3f9);
            ajc2.mo13937m(vec3f4, vec3f6);
            this.stack.bcH().pop();
            return b4;
        } catch (Throwable th) {
            this.stack.bcH().pop();
            throw th;
        }
    }
}
