package p001a;

/* renamed from: a.agH  reason: case insensitive filesystem */
/* compiled from: a */
public enum C6087agH implements C5990aeO {
    AVATAR_CREATION_MUSIC("avatar_creation"),
    INSIDE_STATION("inside_station"),
    LOGIN_SCREEN("login_screen"),
    STATION_AMBIANCE("habitat_commercial_ambience", true);

    public static final String fyR = "data/audio/ambiences/ambiences.pro";
    public static final String fyS = "data/audio/music/playlist.pro";
    private String file;
    private String handle;

    private C6087agH(String str) {
        this.handle = str;
        this.file = fyS;
    }

    private C6087agH(String str, boolean z) {
        this.handle = str;
        if (z) {
            this.file = fyR;
        } else {
            this.file = fyS;
        }
    }

    public String getHandle() {
        return this.handle;
    }

    public String getFile() {
        return this.file;
    }
}
