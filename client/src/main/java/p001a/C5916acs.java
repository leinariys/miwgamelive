package p001a;

import game.engine.IEngineGame;
import game.network.message.externalizable.C6018aeq;

/* renamed from: a.acs  reason: case insensitive filesystem */
/* compiled from: a */
public class C5916acs {
    private static ThreadLocal<IEngineGame> threadLocal;
    private static IEngineGame rootAndRenderPath = null;
    private static boolean fbF = false;

    public static IEngineGame getSingolton() {
        if (threadLocal == null) {
            return rootAndRenderPath;
        }
        return threadLocal.get();
    }

    /* renamed from: eb */
    public static void m20630eb(boolean z) {
        fbF = z;
    }

    /* renamed from: a */
    public static void setRootAndRenderPath(IEngineGame rootAndRenderPath) {
        if ((rootAndRenderPath instanceof EngineGameServer) || !fbF) {
            C5916acs.rootAndRenderPath = rootAndRenderPath;
            return;
        }
        if (threadLocal == null) {
            threadLocal = new ThreadLocal<>();
        }
        threadLocal.set(rootAndRenderPath);
        C5916acs.rootAndRenderPath = rootAndRenderPath;
    }

    @Deprecated
    /* renamed from: am */
    public static void m20629am(String str, String str2) {
        getSingolton().getEventManager().mo13975h(new C6018aeq(str, str2));
    }
}
