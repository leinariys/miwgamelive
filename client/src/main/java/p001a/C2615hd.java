package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.hd */
/* compiled from: a */
public class C2615hd {

    public final C0763Kt stack;
    /* renamed from: TA */
    public boolean f7977TA;
    /* renamed from: TB */
    public float f7978TB;
    /* renamed from: TC */
    public int f7979TC;
    /* renamed from: TD */
    public float f7980TD;
    /* renamed from: Ts */
    public float f7981Ts;
    /* renamed from: Tt */
    public float f7982Tt;
    /* renamed from: Tu */
    public float f7983Tu;
    /* renamed from: Tv */
    public float f7984Tv;
    /* renamed from: Tw */
    public float f7985Tw;
    /* renamed from: Tx */
    public float f7986Tx;
    /* renamed from: Ty */
    public float f7987Ty;
    /* renamed from: Tz */
    public float f7988Tz;
    public float damping;

    public C2615hd() {
        this.stack = C0763Kt.bcE();
        this.f7980TD = 0.0f;
        this.f7983Tu = 0.0f;
        this.f7984Tv = 0.1f;
        this.f7985Tw = 300.0f;
        this.f7981Ts = -3.4028235E38f;
        this.f7982Tt = Float.MAX_VALUE;
        this.f7987Ty = 0.5f;
        this.f7988Tz = 0.0f;
        this.damping = 1.0f;
        this.f7986Tx = 0.5f;
        this.f7979TC = 0;
        this.f7978TB = 0.0f;
        this.f7977TA = false;
    }

    public C2615hd(C2615hd hdVar) {
        this.stack = C0763Kt.bcE();
        this.f7983Tu = hdVar.f7983Tu;
        this.f7984Tv = hdVar.f7984Tv;
        this.f7986Tx = hdVar.f7986Tx;
        this.f7981Ts = hdVar.f7981Ts;
        this.f7982Tt = hdVar.f7982Tt;
        this.f7987Ty = hdVar.f7987Ty;
        this.f7988Tz = hdVar.f7988Tz;
        this.f7979TC = hdVar.f7979TC;
        this.f7978TB = hdVar.f7978TB;
        this.f7977TA = hdVar.f7977TA;
    }

    /* renamed from: xW */
    public boolean mo19290xW() {
        if (this.f7981Ts >= this.f7982Tt) {
            return false;
        }
        return true;
    }

    /* renamed from: xX */
    public boolean mo19291xX() {
        if (this.f7979TC != 0 || this.f7977TA) {
            return true;
        }
        return false;
    }

    /* renamed from: ar */
    public int mo19289ar(float f) {
        if (this.f7981Ts > this.f7982Tt) {
            this.f7979TC = 0;
            return 0;
        } else if (f < this.f7981Ts) {
            this.f7979TC = 1;
            this.f7978TB = f - this.f7981Ts;
            return 1;
        } else if (f > this.f7982Tt) {
            this.f7979TC = 2;
            this.f7978TB = f - this.f7982Tt;
            return 2;
        } else {
            this.f7979TC = 0;
            return 0;
        }
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public float mo19288a(float f, Vec3f vec3f, float f2, C6238ajC ajc, C6238ajC ajc2) {
        float f3 = 0.0f;
        if (!mo19291xX()) {
            return 0.0f;
        }
        this.stack.bcH().push();
        try {
            float f4 = this.f7983Tu;
            float f5 = this.f7984Tv;
            if (this.f7979TC != 0) {
                f4 = ((-this.f7987Ty) * this.f7978TB) / f;
                f5 = this.f7985Tw;
            }
            float f6 = f5 * f;
            Vec3f ac = this.stack.bcH().mo4458ac(ajc.getAngularVelocity());
            if (ajc2 != null) {
                ac.sub(ajc2.getAngularVelocity());
            }
            float dot = vec3f.dot(ac);
            float f7 = (f4 - (dot * this.damping)) * this.f7986Tx;
            if (f7 >= 1.1920929E-7f || f7 <= -1.1920929E-7f) {
                float f8 = f7 * (1.0f + this.f7988Tz) * f2;
                if (f8 <= 0.0f) {
                    if (f8 < (-f6)) {
                        f8 = -f6;
                    }
                    f6 = f8;
                } else if (f8 <= f6) {
                    f6 = f8;
                }
                float f9 = this.f7980TD;
                float f10 = f6 + f9;
                if (f10 <= 1.0E30f && f10 >= -1.0E30f) {
                    f3 = f10;
                }
                this.f7980TD = f3;
                float f11 = this.f7980TD - f9;
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                vec3f2.scale(f11, vec3f);
                ajc.mo13896Q(vec3f2);
                if (ajc2 != null) {
                    vec3f2.negate();
                    ajc2.mo13896Q(vec3f2);
                }
                this.stack.bcH().pop();
                return f11;
            }
            this.stack.bcH().pop();
            return 0.0f;
        } catch (Throwable th) {
            this.stack.bcH().pop();
            throw th;
        }
    }
}
