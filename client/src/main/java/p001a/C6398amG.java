package p001a;

import logic.data.mbean.C0677Jd;

/* renamed from: a.amG  reason: case insensitive filesystem */
/* compiled from: a */
class C6398amG extends C6393amB.C1941c {
    final /* synthetic */ C6393amB gbd;

    C6398amG(C6393amB amb) {
        this.gbd = amb;
    }

    /* renamed from: a */
    public int compare(C0677Jd jd, C0677Jd jd2) {
        long aXC = jd.aXC();
        long aXC2 = jd2.aXC();
        if (aXC < aXC2) {
            return -1;
        }
        if (aXC > aXC2) {
            return 1;
        }
        return mo14765b(jd, jd2);
    }
}
