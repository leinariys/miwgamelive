package p001a;

import game.script.citizenship.CitizenImprovementType;
import game.script.citizenship.CitizenshipPack;
import logic.swing.C3940wz;
import logic.swing.C5378aHa;
import logic.ui.Panel;
import logic.ui.item.ListJ;
import logic.ui.item.aUR;
import taikodom.addon.IAddonProperties;
import taikodom.addon.tooltip.TooltipAddon;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.Ho */
/* compiled from: a */
public class C0564Ho extends C3940wz {

    /* renamed from: DU */
    public static final Object f691DU = "DATA";
    /* renamed from: kj */
    public IAddonProperties f693kj;
    /* access modifiers changed from: private */
    /* renamed from: DV */
    private Panel f692DV;

    public C0564Ho(IAddonProperties vWVar) {
        this.f693kj = vWVar;
    }

    /* renamed from: b */
    public Component mo2675b(Component component) {
        if (this.f692DV == null) {
            dDz();
        }
        if (component instanceof ListJ) {
            ListJ aan = (ListJ) component;
            int locationToIndex = aan.locationToIndex(aan.cHt().getPoint());
            if (aan.getModel().getElementAt(locationToIndex) instanceof CitizenshipPack) {
                C3987xk xkVar = C3987xk.bFK;
                CitizenshipPack acu = (CitizenshipPack) aan.getModel().getElementAt(locationToIndex);
                if (acu != null) {
                    this.f692DV.mo4917cf("cost").setText(String.valueOf(acu.mo12738rZ()));
                    this.f692DV.mo4917cf("expires").setText(String.valueOf(xkVar.mo22980dY(acu.bOT())));
                    this.f692DV.mo4917cf("name").setText(acu.bOV().mo12809rP().get());
                    ListJ cd = this.f692DV.mo4915cd("improvs");
                    cd.setModel(new C2302dn(acu.bOV().bRL()));
                    cd.setCellRenderer(new C0565a());
                }
            }
        }
        return this.f692DV;
    }

    private void dDz() {
        this.f692DV = (Panel) this.f693kj.bHv().mo16789b(TooltipAddon.class, "citizenImprovementTooltip.xml");
    }

    /* renamed from: a.Ho$a */
    class C0565a extends C5517aMj {
        C0565a() {
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            CitizenImprovementType kt = (CitizenImprovementType) obj;
            Panel dAx = aur.dAx();
            JLabel cf = dAx.mo4917cf("imp");
            cf.setText(kt.mo3547rP().get());
            if (kt.mo3548sK() != null) {
                cf.setIcon(new ImageIcon(C0564Ho.this.f693kj.bHv().adz().getImage(String.valueOf(C5378aHa.ICONOGRAPHY.getPath()) + kt.mo3548sK().getHandle())));
            }
            return dAx;
        }
    }
}
