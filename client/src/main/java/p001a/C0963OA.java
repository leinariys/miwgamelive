package p001a;

/* renamed from: a.OA */
/* compiled from: a */
public class C0963OA extends aOG {
    public static final C5961adl dMj = new C5961adl("JoystickButtonEvent");
    private boolean aCJ;
    private int button;

    public C0963OA(int i, boolean z) {
        this.button = i;
        this.aCJ = z;
    }

    public final int getButton() {
        return this.button;
    }

    public final boolean getState() {
        return this.aCJ;
    }

    public final boolean isPressed() {
        return this.aCJ;
    }

    /* renamed from: Fp */
    public C5961adl mo2005Fp() {
        return dMj;
    }
}
