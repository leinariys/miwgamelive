package p001a;

import logic.baa.C0694Ju;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: a.nM */
/* compiled from: a */
public class C3041nM implements Runnable {
    /* access modifiers changed from: private */
    public static final Log logger = LogPrinter.m10275K(C3041nM.class);
    /* access modifiers changed from: private */
    public C0694Ju fNC;
    private CurrentTimeMilli cVg;
    private List<C6920awI> dkr = new ArrayList();
    private Executor executor;
    private AtomicInteger fNB = new AtomicInteger();

    public C3041nM(Executor executor2, CurrentTimeMilli ahw, C0694Ju ju) {
        this.executor = executor2;
        this.cVg = ahw;
        this.fNC = ju;
    }

    public void run() {
        while (true) {
            try {
                Thread.sleep(80);
                ccr();
            } catch (InterruptedException e) {
                return;
            } catch (RuntimeException e2) {
                logger.error("Runtime exception caught at TaskletTicker. Preventing thread from crashing", e2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void ccr() {
        long currentTimeMillis = this.cVg.currentTimeMillis();
        int incrementAndGet = this.fNB.incrementAndGet();
        this.fNC.mo3270b(this.dkr, incrementAndGet, currentTimeMillis);
        int size = this.dkr.size();
        for (int i = 0; i < size; i++) {
            C3042a aVar = new C3042a(this.dkr.get(i), currentTimeMillis, incrementAndGet);
            if (incrementAndGet == 1) {
                aVar.run();
            } else {
                this.executor.execute(aVar);
            }
        }
        this.dkr.clear();
    }

    /* renamed from: a.nM$a */
    class C3042a implements Runnable {
        private final /* synthetic */ C6920awI aLv;
        private final /* synthetic */ long aLw;
        private final /* synthetic */ int aLx;

        C3042a(C6920awI awi, long j, int i) {
            this.aLv = awi;
            this.aLw = j;
            this.aLx = i;
        }

        public void run() {
            try {
                this.aLv.mo4703h(this.aLw, this.aLx);
            } catch (Exception e) {
                C3041nM.logger.error("An error happened during TaskletTicker " + this.aLv.toString() + " execution", e);
            }
            C3041nM.this.fNC.mo3273j(this.aLv);
        }
    }
}
