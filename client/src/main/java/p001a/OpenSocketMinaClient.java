package p001a;

import game.network.Transport;
import game.network.channel.client.ClientConnect;
import game.network.exception.*;
import game.network.message.TransportCommand;
import game.network.message.TransportResponse;
import game.network.message.externalizable.AuthorizationExt;
import game.network.message.externalizable.ConnectStatusExt;
import game.network.message.externalizable.VersionExt;
import logic.res.html.C7025azm;
import logic.res.html.MessageContainer;
import logic.thred.PoolThread;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: a.cY */
/* compiled from: a */
public class OpenSocketMinaClient extends OpenSocketMina {

    /* renamed from: xH */
    private AtomicLong countWriteMessage = new AtomicLong(0);

    /* renamed from: xI */
    private long clientTimeAuthorization;

    /* renamed from: xJ */
    private Map<Integer, C2181a> f6151xJ = new ConcurrentHashMap();

    public OpenSocketMinaClient(String host, int port, String userName, String password, boolean isForcingNewUser, String ckientVersion) throws Exception {
        super(ClientConnect.Attitude.CHILD, new NioSocketConnector(), ckientVersion);
        NioSocketConnector iZ = m28379iZ();
        Object obj = new Object();
        C7025azm dgN = MessageContainer.dgN();
        iZ.setHandler(new C2182b(dgN, userName, obj, password, isForcingNewUser));
        ConnectFuture connect = iZ.connect(new InetSocketAddress(host, port));
        connect.awaitUninterruptibly();
        if (!connect.isConnected()) {
            iZ.dispose();
            throw new ServerLoginException();
        }
        Object pop = dgN.pop();
        if (pop instanceof VersionExt) {
            throw new InvalidHandshakeException();
        }
        if (pop instanceof ConnectStatusExt) {
            switch (((ConnectStatusExt) pop).dqe().ordinal()) {
                case 0:
                    break;
                case 1:
                    throw new InvalidUsernamePasswordException();
                case 2:
                    throw new ClientAlreadyConnectedException();
                case 3:
                    throw new ServerLoginException();
                case 4:
                    throw new UserAccountNotActivatedException();
                case 5:
                    throw new UserBannedException();
                case 6:
                    throw new InvalidHandshakeException();
                case 7:
                    throw new HasNotCreditsException();
                case 8:
                    throw new UserDeletedException();
                default:
                    throw new RuntimeException(String.valueOf(getClass().getName()) + " protocol error!");
            }
        }
        this.clientTimeAuthorization = System.currentTimeMillis();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo17617a(IoSession ioSession, Object obj) {
        this.countWriteMessage.getAndIncrement();
        ioSession.write(obj);
    }

    /* renamed from: a */
    public void sendTransportMessageInSeparateThread(ClientConnect bVar, TransportCommand transportCommand) {
        this.countWriteMessage.getAndIncrement();
        super.sendTransportMessageInSeparateThread(bVar, transportCommand);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo17616a(C5772aaE aae, Transport xuVar) {
        C2181a remove = this.f6151xJ.remove(Integer.valueOf(xuVar.getThreadId()));
        if (remove != null) {
            synchronized (remove) {
                remove.aMf = xuVar;
                remove.notify();
            }
            return;
        }
        mo19614HV().addToPoolExecuterCommand(aae, (TransportCommand) xuVar);
    }

    /* renamed from: iX */
    public boolean isRunningThreadSelectableChannel() {
        return false;
    }

    /* renamed from: iY */
    public boolean isListenerPort() {
        return false;
    }

    /* renamed from: iZ */
    private NioSocketConnector m28379iZ() {
        return getService();
    }

    /* renamed from: b */
    public TransportResponse sendTransportMessageAndWaitForResponse(ClientConnect bVar, TransportCommand transportCommand) {
        C2181a aVar = new C2181a();
        this.f6151xJ.put(Integer.valueOf(transportCommand.getThreadId()), aVar);
        sendTransportMessageInSeparateThread(bVar, transportCommand);
        synchronized (aVar) {
            if (aVar.aMf == null) {
                PoolThread.wait(aVar);
            }
        }
        return (TransportResponse) aVar.aMf;
    }

    /* renamed from: ja */
    public long metricClientTimeAuthorization() {
        return this.clientTimeAuthorization;
    }

    /* renamed from: jb */
    public long metricCountWriteMessage() {
        return this.countWriteMessage.get();
    }

    /* renamed from: jc */
    public long metricCountReadMessage() {
        return getIoSession().getReadMessages();
    }

    /* renamed from: jd */
    public long metricAllOriginalByteLength() {
        return getIoSession().getWrittenBytes();
    }

    /* renamed from: je */
    public long metricByteLength() {
        return getIoSession().getReadBytes();
    }

    /* renamed from: jf */
    public long metricReadByte() {
        return getIoSession().getReadBytes();
    }

    /* renamed from: jg */
    public long metricWriteByte() {
        return getIoSession().getWrittenBytes();
    }

    /* renamed from: jh */
    public InetAddress getIpRemoteHost() {
        return null;
    }

    /* renamed from: a.cY$a */
    static final class C2181a {
        Transport aMf;

        C2181a() {
        }
    }

    /* renamed from: a.cY$b */
    /* compiled from: a */
    class C2182b extends IoHandlerAdapter {

        /* renamed from: AD */
        private static /* synthetic */ int[] f6152AD;
        private final /* synthetic */ String gEl;
        private final /* synthetic */ String gye;
        private final /* synthetic */ boolean gyf;
        private final /* synthetic */ C7025azm hCN;
        private final /* synthetic */ Object hCO;

        C2182b(C7025azm azm, String str, Object obj, String str2, boolean z) {
            this.hCN = azm;
            this.gEl = str;
            this.hCO = obj;
            this.gye = str2;
            this.gyf = z;
        }

        /* renamed from: kj */
        static /* synthetic */ int[] m28395kj() {
            int[] iArr = f6152AD;
            if (iArr == null) {
                iArr = new int[OpenSocketMina.C2680a.values().length];
                try {
                    iArr[OpenSocketMina.C2680a.CLOSED.ordinal()] = 3;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[OpenSocketMina.C2680a.HANDSHAKING.ordinal()] = 1;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[OpenSocketMina.C2680a.UP.ordinal()] = 2;
                } catch (NoSuchFieldError e3) {
                }
                f6152AD = iArr;
            }
            return iArr;
        }

        public void exceptionCaught(IoSession ioSession, Throwable th) {
            th.printStackTrace();
        }

        public void sessionOpened(IoSession ioSession) {
            C5772aaE aae = new C5772aaE(ioSession);
            aae.mo12141a(OpenSocketMina.C2680a.HANDSHAKING);
            ioSession.setAttribute(OpenSocketMinaClient.anW, aae);
            OpenSocketMinaClient.this.mo19617b(ioSession);
        }

        public void messageReceived(IoSession ioSession, Object obj) {
            C5772aaE a = OpenSocketMinaClient.m33375a(ioSession);
            switch (m28395kj()[a.bLH().ordinal()]) {
                case 1:
                    if (obj instanceof VersionExt) {
                        VersionExt aua = (VersionExt) obj;
                        if (!aua.getVersion().equals(OpenSocketMinaClient.this.getVersionString())) {
                            this.hCN.push(aua);
                            ioSession.close();
                            return;
                        } else if (this.gEl == null) {
                            a.mo12141a(OpenSocketMina.C2680a.UP);
                            this.hCN.push(this.hCO);
                            return;
                        } else {
                            OpenSocketMinaClient.this.mo17617a(ioSession, (Object) new AuthorizationExt(this.gEl, this.gye, this.gyf));
                            return;
                        }
                    } else if (obj instanceof ConnectStatusExt) {
                        ConnectStatusExt aqp = (ConnectStatusExt) obj;
                        if (!aqp.dqf()) {
                            this.hCN.push(aqp);
                            ioSession.close();
                            return;
                        }
                        a.mo12141a(OpenSocketMina.C2680a.UP);
                        this.hCN.push(this.hCO);
                        return;
                    } else {
                        return;
                    }
                case 2:
                    OpenSocketMinaClient.this.mo17616a(a, (Transport) obj);
                    return;
                default:
                    return;
            }
        }

        public void sessionClosed(IoSession ioSession) {
            ((C5772aaE) ioSession.removeAttribute(OpenSocketMinaClient.anW)).mo12141a(OpenSocketMina.C2680a.CLOSED);
        }
    }
}
