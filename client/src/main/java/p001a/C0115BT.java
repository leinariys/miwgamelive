package p001a;

import game.script.corporation.Corporation;
import game.script.player.Player;
import game.script.space.Node;
import logic.sql.C5878acG;
import logic.thred.LogPrinter;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: a.BT */
/* compiled from: a */
public class C0115BT {
    static LogPrinter log = LogPrinter.m10275K(C0115BT.class);
    private static volatile C0115BT ecx = null;
    private static BlockingQueue<C0117b> ecy = new LinkedBlockingQueue();

    private C0115BT() {
        C0116a aVar = new C0116a("Chat log thread");
        aVar.setDaemon(true);
        aVar.start();
    }

    /* renamed from: gy */
    private static void m1221gy(String str) {
        ecy.add(new C0117b(str));
    }

    /* renamed from: a */
    public static void m1220a(Node rPVar, Player aku, String str) {
        if (validate()) {
            m1221gy("[LOCAL: " + rPVar.mo21665ke().get() + "] '" + aku.getName() + "': " + str);
        }
    }

    /* renamed from: a */
    public static void m1217a(Corporation aPVar, Player aku, String str) {
        if (validate()) {
            m1221gy("[CORP: " + aPVar.getName() + "] '" + aku.getName() + "': " + str);
        }
    }

    /* renamed from: i */
    public static void m1222i(Player aku, String str) {
        if (validate()) {
            m1221gy("[GLOBAL] '" + aku.getName() + "': " + str);
        }
    }

    /* renamed from: a */
    public static void m1219a(Player aku, Player aku2, String str) {
        if (validate()) {
            m1221gy("[PRIVATE] '" + aku.getName() + "' to '" + aku2.getName() + "': " + str);
        }
    }

    /* renamed from: a */
    public static void m1218a(Player aku, Party gt, String str) {
        if (validate()) {
            m1221gy("[PARTY] '" + aku.getName() + "': " + str);
        }
    }

    private static boolean validate() {
        if (ecx != null) {
            return true;
        }
        synchronized (C0115BT.class) {
            if (ecx == null) {
                ecx = new C0115BT();
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void bsA() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(C5878acG.feC);
        PrintWriter printWriter = null;
        while (true) {
            try {
                C0117b take = ecy.take();
                String str = take.message;
                if (printWriter == null) {
                    printWriter = LogPrinter.createLogFile("chat", "log");
                }
                printWriter.println(String.valueOf(simpleDateFormat.format(new Date(take.duT))) + " " + str);
            } catch (Exception e) {
                log.warn("Problems in ChatLogger, chat conversations will no longer be logged", e);
                return;
            }
        }
    }

    /* renamed from: a.BT$b */
    /* compiled from: a */
    public static class C0117b {
        /* access modifiers changed from: private */
        public long duT = System.currentTimeMillis();
        String message;

        public C0117b(String str) {
            this.message = str;
        }
    }

    /* renamed from: a.BT$a */
    class C0116a extends Thread {
        C0116a(String str) {
            super(str);
        }

        public void run() {
            C0115BT.this.bsA();
        }
    }
}
