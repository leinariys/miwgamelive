package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import logic.bbb.C4029yK;

/* renamed from: a.aOe  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5564aOe {
    /* renamed from: IL */
    C4029yK mo10648IL();

    /* renamed from: an */
    Vec3f mo10649an(Vec3d ajr);

    /* renamed from: bB */
    Vec3d mo10650bB(Vec3f vec3f);

    /* renamed from: bp */
    Vec3f mo10651bp(Vec3f vec3f);

    /* renamed from: br */
    Vec3f mo10652br(Vec3f vec3f);

    Quat4fWrap getOrientation();

    Vec3d getPosition();
}
