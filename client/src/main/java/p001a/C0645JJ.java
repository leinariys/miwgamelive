package p001a;

/* renamed from: a.JJ */
/* compiled from: a */
public interface C0645JJ {
    public static final String RES_SERVER_CONFIG = "res/server/config/";
    public static final String TAIKODOM_SERVER_PORT = "taikodom.server.port";
    public static final String TAIKODOM_SERVER_DEFAULTS_PROPERTIES = "taikodom.server.defaults.properties";
    public static final String TAIKODOM_SERVER_CUSTOM_PROPERTIES = "taikodom.server.custom.properties";
    public static final String ETC_TAIKODOM = "/etc/taikodom/";
}
