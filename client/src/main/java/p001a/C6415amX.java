package p001a;

import logic.aaa.C5893acV;

import javax.swing.table.AbstractTableModel;

/* renamed from: a.amX  reason: case insensitive filesystem */
/* compiled from: a */
public class C6415amX extends AbstractTableModel {

    private C0306Dz gcb;

    public C6415amX(C0306Dz dz) {
        this.gcb = dz;
    }

    public int getColumnCount() {
        return 3;
    }

    public Class<?> getColumnClass(int i) {
        return C2570gx.class;
    }

    public String getColumnName(int i) {
        switch (i) {
            case 0:
                return "";
            case 1:
                return this.gcb.mo1787JW().translate("Name");
            case 2:
                return this.gcb.mo1787JW().translate("Distance");
            default:
                return null;
        }
    }

    public int getRowCount() {
        return this.gcb.ddt();
    }

    public Object getValueAt(int i, int i2) {
        return this.gcb.mo1802yj(i);
    }

    /* renamed from: sE */
    public void mo14841sE(int i) {
        if (i >= 0 && getRowCount() > 0) {
            this.gcb.mo1787JW().aVU().mo13975h(new C5893acV(this.gcb.mo1801yi(i)));
            this.gcb.mo1787JW().aVU().mo13975h(new C6811auD(C3667tV.MOUSE_CLICK_SCROLL));
        }
    }

    public void fireTableDataChanged() {
        this.gcb.dds();
        this.gcb.ddw();
        if (this.gcb.ddx()) {
            C6415amX.super.fireTableDataChanged();
        }
        this.gcb.updateSize();
    }
}
