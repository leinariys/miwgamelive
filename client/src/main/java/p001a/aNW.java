package p001a;

import game.geometry.Vec3d;
import logic.aaa.C2235dB;

/* renamed from: a.aNW */
/* compiled from: a */
public interface aNW {
    /* renamed from: F */
    float mo3829F(Vec3d ajr);

    /* renamed from: b */
    float mo3831b(aNW anw);

    C2235dB bfw();

    long bfx();

    void dispose();

    int getFlags();

    /* renamed from: kO */
    void mo3841kO();

    /* renamed from: lz */
    void mo3842lz(int i);
}
