package p001a;

import game.script.pda.DatabaseCategory;
import logic.baa.C4033yO;
import taikodom.addon.pda.DataBase;

/* renamed from: a.ahg  reason: case insensitive filesystem */
/* compiled from: a */
class C6164ahg extends C5519aMl {
    final /* synthetic */ DataBase cDE;

    public C6164ahg(DataBase dataBase) {
        this.cDE = dataBase;
    }

    /* renamed from: a */
    public boolean mo10165a(Object obj) {
        if (obj instanceof C4033yO) {
            C4033yO yOVar = (C4033yO) obj;
            if (!this.cDE.giw.isSelected() || this.cDE.f10208P.dyl().dBQ().get(yOVar).booleanValue()) {
                return yOVar.mo195rP().get().toLowerCase().contains(this.cDE.searchField.getText().toLowerCase());
            }
            return false;
        } else if (!(obj instanceof DatabaseCategory)) {
            return true;
        } else {
            if (this.cDE.giw.isSelected()) {
                return false;
            }
            return ((DatabaseCategory) obj).mo195rP().get().toLowerCase().contains(this.cDE.searchField.getText().toLowerCase());
        }
    }

    /* renamed from: z */
    public boolean mo10166z() {
        return true;
    }
}
