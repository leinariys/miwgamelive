package p001a;

import logic.bbb.C2820ka;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.nP */
/* compiled from: a */
public class C3046nP extends C2956mE {
    /* access modifiers changed from: protected */
    /* renamed from: e */
    public Object mo20457e(ObjectInput objectInput) {
        return new C2820ka((String) C2562gp.f7767PX.inputReadObject(objectInput));
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo20456c(ObjectOutput objectOutput, Object obj) {
        C2562gp.f7767PX.serializeData(objectOutput, ((C2820ka) obj).getFilename());
    }
}
