package p001a;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.ObjectStreamClass;
import java.util.Collection;
import java.util.Map;

/* renamed from: a.arE  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6656arE {
    /* renamed from: B */
    void mo12005B(Map<String, String> map);

    /* renamed from: a */
    int mo12007a(DataInput dataInput);

    /* renamed from: a */
    void mo12008a(DataOutput dataOutput, int i);

    /* renamed from: at */
    void mo12009at(Class<?> cls);

    Collection<Class<?>> getClasses();

    int getMagicNumber(Class<?> cls);

    ObjectStreamClass getObjectStreamClassFromMagicNumber(int i);

    /* renamed from: jK */
    String mo12014jK(String str);

    /* renamed from: jL */
    String mo12015jL(String str);

    /* renamed from: jM */
    void mo12016jM(String str);

    /* renamed from: tS */
    Class<?> mo12017tS(int i);
}
