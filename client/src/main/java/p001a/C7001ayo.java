package p001a;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: a.ayo  reason: case insensitive filesystem */
/* compiled from: a */
public class C7001ayo implements aLA {
    public C6217aih gTl = null;
    public Set<C6217aih> gTm = new HashSet();
    public ReentrantLock gTn = new ReentrantLock();

    /* renamed from: a */
    public C3596sj mo9816a(C6217aih aih) {
        return new C2015a(aih);
    }

    /* renamed from: a.ayo$a */
    class C2015a implements C3596sj {
        private final /* synthetic */ C6217aih hKP;

        C2015a(C6217aih aih) {
            this.hKP = aih;
        }

        /* renamed from: dn */
        public void mo17084dn() {
            C7001ayo.this.gTn.lock();
            try {
                if (C7001ayo.this.gTl != null) {
                    C7001ayo.this.gTl.mo13777dk();
                    C7001ayo.this.gTl = null;
                }
                C7001ayo.this.gTm.add(this.hKP);
            } finally {
                C7001ayo.this.gTn.unlock();
            }
        }

        /* renamed from: dm */
        public void mo17083dm() {
            C7001ayo.this.gTn.lock();
            try {
                if (C7001ayo.this.gTl != null) {
                    C7001ayo.this.gTl.mo13777dk();
                } else {
                    for (C6217aih dl : C7001ayo.this.gTm) {
                        dl.mo13778dl();
                    }
                    C7001ayo.this.gTm.clear();
                }
                C7001ayo.this.gTl = this.hKP;
            } finally {
                C7001ayo.this.gTn.unlock();
            }
        }

        public void unregister() {
        }
    }
}
