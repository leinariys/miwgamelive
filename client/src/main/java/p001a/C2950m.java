package p001a;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.m */
/* compiled from: a */
public @interface C2950m {
    long timeout() default -1;
}
