package p001a;

import logic.swing.C3940wz;
import logic.ui.item.aOA;
import taikodom.addon.IAddonProperties;
import taikodom.addon.hud.lag.LagAddon;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.Lw */
/* compiled from: a */
public class C0827Lw extends C3940wz {
    JPanel dve = new JPanel();
    private aOA dvd = new aOA();
    /* renamed from: kj */
    private IAddonProperties f1061kj;

    public C0827Lw(IAddonProperties vWVar) {
        this.f1061kj = vWVar;
        this.dve.add(this.dvd);
    }

    /* renamed from: b */
    public Component mo2675b(Component component) {
        this.dvd.setText(((LagAddon) this.f1061kj.mo11830U(LagAddon.class)).aks());
        if (this.dvd.getText() == null || this.dvd.getText().isEmpty()) {
            return null;
        }
        return this.dve;
    }
}
