package p001a;

import logic.res.html.DataClassSerializer;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.iy */
/* compiled from: a */
public class C2717iy extends DataClassSerializer {
    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) {
        if (!objectInput.readBoolean()) {
            return null;
        }
        Externalizable Bw = mo19815Bw();
        Bw.readExternal(objectInput);
        return Bw;
    }

    /* access modifiers changed from: protected */
    /* renamed from: Bw */
    public Externalizable mo19815Bw() {
        try {
            return (Externalizable) this.clazz.newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException(e2);
        }
    }

    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) {
        if (obj == null) {
            objectOutput.writeBoolean(false);
            return;
        }
        objectOutput.writeBoolean(true);
        ((Externalizable) obj).writeExternal(objectOutput);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo3590a(Class<?> cls, Class<?>[] clsArr) {
        super.mo3590a(cls, clsArr);
        if (getClass() != C2717iy.class) {
            return;
        }
        if ((cls.getModifiers() & 16) == 0 || cls.isInterface()) {
            throw new IllegalArgumentException("Not a true final class!");
        }
    }
}
