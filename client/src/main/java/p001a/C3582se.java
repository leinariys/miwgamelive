package p001a;

import game.engine.DataGameEvent;
import game.network.ProxyTyp;
import game.network.WhoAmI;
import game.network.channel.client.ClientConnect;
import game.network.exception.C1728ZX;
import game.network.exception.C3578sa;
import game.network.exception.CreatListenerChannelException;
import game.network.message.Blocking;
import game.network.message.C5581aOv;
import game.network.message.C6215aif;
import game.network.message.externalizable.*;
import logic.baa.*;
import logic.bbb.aDR;
import logic.data.mbean.C0677Jd;
import logic.data.mbean.C5439aJj;
import logic.data.mbean.C6064afk;
import logic.res.code.C5663aRz;
import logic.res.html.*;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import taikodom.addon.C6144ahM;
import util.Syst;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/* renamed from: a.se */
/* compiled from: a */
public abstract class C3582se implements C5545aNl, C6663arL {
    private static final Log logger = LogPrinter.setClass(C3582se.class);
    private static ConcurrentHashMap<Long, List<Throwable>> hCq = new ConcurrentHashMap<>();
    public DataGameEvent aGA;
    public boolean disposed;
    public C6064afk hCm;
    volatile C0413Fm.C0415b hCo;
    ThreadLocal<Boolean> hCr = new ThreadLocal<>();
    private BuildThreadServerInfraCacheCleanup.C2200a dqS;
    private C1616Xf hCl;
    private ProxyTyp hCn;
    private long hCp = -1;
    /* renamed from: qT */
    private ObjectId f9145qT;

    public static boolean bxL() {
        return aWq.dDA() != null;
    }

    /* renamed from: o */
    public static String m38988o(C1616Xf xf) {
        if (xf != null) {
            return String.valueOf(xf.getClass().getName()) + ":" + xf.bFf().getObjectId().getId();
        }
        return "null";
    }

    /* renamed from: a */
    public static <T extends aDJ> T m38985a(T t, C6144ahM<?> ahm) {
        try {
            C3582se seVar = (C3582se) ((C1616Xf) t).bFf();
            T P = (T) seVar.mo6866PM().mo3336P(t.getClass());
            P.mo6754b(new C3589e(seVar, seVar, ahm));
            return (T) P;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    /* renamed from: a */
    public abstract C6302akO mo5593a(ClientConnect bVar, C5703aTn atn);

    /* renamed from: a */
    public abstract void mo5599a(C5663aRz arz, Object obj);

    /* renamed from: a */
    public abstract void mo5600a(Class<? extends C4062yl> cls, C0495Gr gr);

    /* renamed from: b */
    public abstract Object mo5602b(C5663aRz arz);

    /* renamed from: dq */
    public abstract C6064afk mo5608dq();

    public long cVe() {
        return this.hCp;
    }

    /* renamed from: kv */
    public void mo22001kv(long j) {
        this.hCp = j;
    }

    /* renamed from: i */
    public int mo6893i(C2491fm fmVar) {
        if (this.aGA.bGN() != null) {
            return 2;
        }
        return m38987j(fmVar);
    }

    /* renamed from: j */
    private int m38987j(C2491fm fmVar) {
        if (bGZ()) {
            int pA = fmVar.mo4755pA();
            if (pA == 0) {
                fmVar.mo4770pP();
                return 0;
            }
            aWq dDA = aWq.dDA();
            if (dDA != null) {
                if (dDA.dDE()) {
                    dDA.jgd++;
                    if (fmVar.mo7391hm()) {
                        dDA.enr++;
                    }
                } else {
                    dDA.enx++;
                    if (fmVar.mo7391hm()) {
                        dDA.enr++;
                        dDA.mo12085k(fmVar);
                    }
                }
            } else if (!this.aGA.bGo()) {
                return 2;
            } else {
                if (fmVar.mo4793py()) {
                    return 2;
                }
            }
            return pA;
        }
        if (this.hCm.mo3202hF() && !fmVar.mo4766pL()) {
            if (mo6901yn().mo11T().mo15898ho() || this.disposed) {
                return 0;
            }
            if (!mo5609ds()) {
                this.disposed = true;
                return 0;
            }
        }
        if (bGY()) {
            return fmVar.mo4779pY();
        }
        return fmVar.mo4794pz();
    }

    /* renamed from: a */
    public Object mo5594a(C0495Gr gr, C6144ahM<Object> ahm) {
        if (!bGZ()) {
            return null;
        }
        throw new IllegalStateException("Invalid async call at server side: " + gr);
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object mo5606d(p001a.C0495Gr r4) {
        /*
            r3 = this;
            a.Jz r0 = r3.aGA
            a.sz r1 = r0.bGN()
            if (r1 != 0) goto L_0x0015
            a.Jz r0 = r3.mo6866PM()
            a.Uy r0 = r0.bGR()
            java.lang.Object r0 = r0.mo5929a((p001a.C3582se) r3, (p001a.C0495Gr) r4)
        L_0x0014:
            return r0
        L_0x0015:
            a.fm r0 = r4.aQK()
            int r0 = r3.m38987j(r0)
            if (r0 != 0) goto L_0x002f
            a.fm r0 = r4.aQK()
            r1.mo22053b(r3, r0)
            a.fm r0 = r4.aQK()
            java.lang.Object r0 = r0.mo4768pN()
            goto L_0x0014
        L_0x002f:
            a.fm r2 = r4.aQK()
            a.Xz r2 = r1.mo22050a((logic.res.html.C6663arL) r3, (logic.res.html.C2491fm) r2)
            switch(r0) {
                case 1: goto L_0x0044;
                case 2: goto L_0x004d;
                case 3: goto L_0x005a;
                default: goto L_0x003a;
            }
        L_0x003a:
            r0 = 0
        L_0x003b:
            r1.mo22051a(r2)     // Catch:{ Exception -> 0x003f }
            goto L_0x0014
        L_0x003f:
            r0 = move-exception
            r1.mo22052a((logic.res.html.C1639Xz) r2, (java.lang.Exception) r0)
            throw r0
        L_0x0044:
            a.Xf r0 = r3.mo6901yn()     // Catch:{ Exception -> 0x003f }
            java.lang.Object r0 = r0.mo14a((p001a.C0495Gr) r4)     // Catch:{ Exception -> 0x003f }
            goto L_0x003b
        L_0x004d:
            a.Jz r0 = r3.mo6866PM()     // Catch:{ Exception -> 0x003f }
            a.Uy r0 = r0.bGR()     // Catch:{ Exception -> 0x003f }
            java.lang.Object r0 = r0.mo5929a((p001a.C3582se) r3, (p001a.C0495Gr) r4)     // Catch:{ Exception -> 0x003f }
            goto L_0x003b
        L_0x005a:
            a.Jz r0 = r3.mo6866PM()     // Catch:{ Exception -> 0x003f }
            a.Uy r0 = r0.bGR()     // Catch:{ Exception -> 0x003f }
            r0.mo5929a((p001a.C3582se) r3, (p001a.C0495Gr) r4)     // Catch:{ Exception -> 0x003f }
            a.Xf r0 = r3.mo6901yn()     // Catch:{ Exception -> 0x003f }
            java.lang.Object r0 = r0.mo14a((p001a.C0495Gr) r4)     // Catch:{ Exception -> 0x003f }
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C3582se.mo5606d(a.Gr):java.lang.Object");
    }

    /* renamed from: v */
    public C6064afk mo22004v(aDR adr) {
        if (this.hCm.mo3202hF() && !this.hCm.mo3194du()) {
            mo5609ds();
        }
        return this.hCm;
    }

    public void cVf() {
        this.disposed = true;
    }

    /* renamed from: dr */
    public void mo10527dr() {
        this.hCm.mo3191cS(true);
    }

    public void cVg() {
        if (this.disposed) {
            throw new RuntimeException("Trying to dispose a disposed object");
        }
        this.disposed = true;
        this.aGA.mo3434e(this);
    }

    public boolean isDisposed() {
        return this.disposed;
    }

    /* renamed from: du */
    public boolean mo10528du() {
        return this.hCm.mo3194du();
    }

    public final boolean cVh() {
        return isDisposed();
    }

    public final boolean cVi() {
        return mo10528du();
    }

    /* renamed from: hE */
    public ClientConnect mo6892hE() {
        return this.aGA.mo3447hE();
    }

    /* renamed from: hC */
    public ObjectId getObjectId() {
        return this.f9145qT;
    }

    public C6064afk cVj() {
        return this.hCm;
    }

    /* renamed from: a */
    public C6302akO mo21964a(C5581aOv aov) {
        if (bGZ()) {
            throw new SecurityException();
        }
        try {
            this.hCr.set(true);
            mo6901yn().mo14a(((C3962xP) aov).dvF());
            return null;
        } finally {
            this.hCr.set((Object) null);
        }
    }

    /* renamed from: c */
    public void mo5605c(ClientConnect bVar, C5581aOv aov) {
    }

    /* renamed from: c */
    public void mo21982c(C6064afk afk) {
        if (hasListeners() || this.aGA.bGL()) {
            boolean hF = this.hCm.mo3202hF();
            C6064afk afk2 = this.hCm;
            mo21994d(afk);
            if (bGw()) {
                mo21998f(new C3591g(hF, (C5439aJj) null, afk2, afk));
            } else {
                mo21972a(hF, (C5439aJj) null, afk2, afk);
            }
        } else {
            mo21994d(afk);
        }
        this.dqS.mo17664A(false);
    }

    /* renamed from: b */
    public C6302akO mo5601b(C5581aOv aov) {
        if (this.hCm.mo3202hF()) {
            logger.debug("Received update for a hollow object: " + getObjectId().getId() + " - " + mo6901yn().getClass().getName());
        } else {
            C0677Jd Wo = ((C3213pH) aov).mo21129Wo();
            boolean hF = this.hCm.mo3202hF();
            Wo.mo3188c((C0677Jd) this.hCm);
            C6064afk afk = this.hCm;
            Wo.mo3146A(false);
            this.hCm = Wo;
            bFj();
            if (bGw()) {
                mo21998f(new C3590f(hF, afk, Wo));
            } else {
                mo21972a(hF, (C5439aJj) null, afk, Wo);
            }
        }
        return null;
    }

    /* renamed from: a */
    private boolean m38986a(C1619Xi<C1616Xf> xi) {
        C6147ahP[] bFh = mo6901yn().bFh();
        if (bFh != null && bFh.length > 0) {
            C6663arL bGI = mo6866PM().bGI();
            if (xi == null) {
                xi = new C1619Xi<>();
            }
            if (bGI != null) {
                xi.mo6768Z(bGI.mo6901yn());
            }
            xi.mo6776h(mo6892hE());
            for (C6147ahP a : bFh) {
                if (!a.mo13516a(mo6901yn(), xi)) {
                    return false;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo21973a(C1619Xi<C1616Xf> xi, C0495Gr gr) {
        if (gr.aQL().isUseClientOnly()) {
            return false;
        }
        xi.mo6769a(WhoAmI.SERVER);
        xi.mo6775c(gr);
        if (!m38986a(xi)) {
            return false;
        }
        C6147ahP[] hi = gr.aQK().mo7387hi();
        if (hi != null) {
            C6663arL bGI = mo6866PM().bGI();
            if (bGI != null) {
                xi.mo6768Z(bGI.mo6901yn());
            }
            xi.mo6776h(mo6892hE());
            for (C6147ahP a : hi) {
                if (!a.mo13516a(mo6901yn(), xi)) {
                    return false;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Object[] mo21974a(C6064afk afk, C6064afk afk2, C5663aRz arz) {
        Collection v = MessageContainer.m16003v((Collection) afk2.mo3199g(arz));
        v.removeAll((Collection) afk.mo3199g(arz));
        return v.toArray();
    }

    /* renamed from: a */
    public void mo21972a(boolean z, C5439aJj ajj, C6064afk afk, C6064afk afk2) {
        if (!z) {
            this.aGA.mo3354a(this, ajj);
            if (hasListeners()) {
                for (C5663aRz arz : this.hCm.mo309c()) {
                    if (((C0677Jd) afk2).mo3219s(arz) && !z) {
                        if (arz.isCollection()) {
                            mo5597a(arz, afk, afk2);
                        } else {
                            mo5611j(arz, afk2.mo3199g(arz));
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean hasListeners() {
        return false;
    }

    /* renamed from: d */
    public void mo21994d(C6064afk afk) {
        if (this.hCm.aBt() > afk.aBt()) {
            afk.mo3196eU(this.hCm.aBt());
        }
        afk.mo3146A(false);
        this.hCm = afk;
    }

    /* renamed from: a */
    public void mo13206a(C6064afk afk) {
        this.hCm = afk;
    }

    /* renamed from: ds */
    public boolean mo5609ds() {
        if (cVk() == ProxyTyp.AUTHORITY) {
            throw new RuntimeException("State is hollow on authority side. somethings wrong. " + mo6901yn().getClass() + " " + getClass());
        }
        try {
            C6064afk ayZ = ((C6197aiN) mo21980c((C5581aOv) new C3345qX(mo6901yn(), true))).ayZ();
            if (ayZ == null) {
                throw new C3578sa("Not allowed to access this object state");
            }
            C6064afk afk = this.hCm;
            mo21994d(ayZ);
            mo21972a(true, (C5439aJj) null, afk, ayZ);
            if (this.hCm.mo3202hF()) {
                throw new RuntimeException("Fields stay hollow after a RetrieveDataCommand");
            }
            logger.warn("[OPTIMIZE]: pushing " + mo6901yn().getClass() + " " + getObjectId().getId());
            return true;
        } catch (C1728ZX e) {
            if (e.aQW() == C0500Gw.cYS) {
                return false;
            }
            throw new C3578sa("Not allowed to access this object state", e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo21969a(ClientConnect bVar, Blocking[] kUVarArr) {
        if (kUVarArr != null) {
            mo6866PM().bGx();
            try {
                for (Blocking d : kUVarArr) {
                    mo6866PM().bGU().mo15556d(bVar, d);
                }
                mo6866PM().bGy();
            } catch (RuntimeException e) {
                throw e;
            } catch (Error e2) {
                throw e2;
            } catch (Throwable th) {
                mo6866PM().bGy();
                throw th;
            }
        }
    }

    public ProxyTyp cVk() {
        return this.hCn;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo21970a(Blocking kUVar) {
        this.aGA.bGU().mo15549b(mo6892hE(), kUVar);
    }

    public final Class<? extends C1616Xf> bFU() {
        return mo6901yn().getClass();
    }

    /* renamed from: a */
    public final void mo21971a(Class<? extends C1616Xf> cls, C1616Xf xf) {
        this.hCl = xf;
        this.hCl.mo6754b(this);
        this.hCm = (C6064afk) mo6901yn().mo13W();
    }

    /* renamed from: a */
    public void mo21967a(ProxyTyp bh) {
        this.hCn = bh;
    }

    /* renamed from: n */
    public final C1616Xf mo6894i(C1616Xf xf) {
        if (bHa()) {
            if (xf.isUseClientOnly()) {
                //Не следует использовать классы ClientOnly на сервере
                throw new IllegalAccessError("Should not use ClientOnly classes at server");
            }
        } else if (xf.isUseServerOnly()) {
            //Не следует использовать / иметь классы ServerOnly на сервере
            throw new IllegalAccessError("Should not use/have ServerOnly classes at server");
        }
        this.aGA.mo3453k(xf);
        return xf;
    }

    /* renamed from: yn */
    public final C1616Xf mo6901yn() {
        return this.hCl;
    }

    /* renamed from: c */
    public final void mo21981c(DataGameEvent jz) {
        this.aGA = jz;
    }

    /* renamed from: d */
    public final void mo21995d(ObjectId apo) {
        this.f9145qT = apo;
    }

    /* renamed from: iY */
    public final void mo22000iY(boolean z) {
        this.hCm.mo3146A(z);
    }

    public void bFR() {
        this.aGA.mo3437f(this);
        cVf();
    }

    public final boolean bFT() {
        return this.hCm.mo3202hF();
    }

    /* renamed from: e */
    public final void mo21997e(C6064afk afk) {
        this.hCm = afk;
    }

    /* renamed from: PM */
    public final DataGameEvent mo6866PM() {
        return this.aGA;
    }

    public final Date bxM() {
        return new Date(currentTimeMillis());
    }

    public final long currentTimeMillis() {
        return mo6866PM().currentTimeMillis();
    }

    public final Date bxN() {
        aWq dDA = aWq.dDA();
        if (dDA != null) {
            return dDA.bxM();
        }
        //Вы можете вызвать этот метод только из транзакции.
        throw new RuntimeException("You can only call this method from within a transaction.");
    }

    /* access modifiers changed from: protected */
    public void cVl() {
        if (!bGX()) {
            throw new UnsupportedOperationException("You can only use this method at client side");
        }
    }

    /* renamed from: a */
    public void mo5598a(C5663aRz arz, C6200aiQ aiq) {
        cVl();
    }

    /* renamed from: b */
    public void mo5604b(C5663aRz arz, C6200aiQ aiq) {
        cVl();
    }

    /* renamed from: j */
    public void mo5611j(C5663aRz arz, Object obj) {
    }

    /* renamed from: a */
    public void mo5596a(C5663aRz arz, C5473aKr akr) {
        cVl();
    }

    /* renamed from: b */
    public void mo5603b(C5663aRz arz, C5473aKr akr) {
        cVl();
    }

    /* renamed from: a */
    public void mo5597a(C5663aRz arz, C6064afk afk, C6064afk afk2) {
    }

    /* renamed from: w */
    public void mo10534w(aDR adr) {
        aWq dDA = aWq.dDA();
        if (dDA != null && adr != null) {
            dDA.mo12046D(adr);
        }
    }

    public void cVm() {
        aWq dDA = aWq.dDA();
        if (dDA != null) {
            dDA.dDT();
        }
    }

    /* renamed from: w */
    public final Object mo10533w(C5663aRz arz) {
        return mo5602b(arz);
    }

    /* renamed from: g */
    public final void mo10529g(C5663aRz arz, Object obj) {
        mo5599a(arz, obj);
    }

    /* renamed from: c */
    public final C5663aRz[] mo21983c() {
        return mo6901yn().mo25c();
    }

    public void aMr() {
    }

    /* renamed from: c */
    public C6302akO mo21980c(C5581aOv aov) {
        return this.aGA.bGU().mo15551c(mo6892hE(), aov);
    }

    public final boolean bFS() {
        return cVk() == ProxyTyp.AUTHORITY;
    }

    public final Object bFV() {
        return mo6901yn();
    }

    public final void bFj() {
        mo6901yn().bFj();
    }

    /* renamed from: b */
    public final void mo21976b(C0722KO ko) {
        synchronized (this.hCm) {
            ko.bdh().mo3188c((C0677Jd) this.hCm);
            this.hCm = ko.bdh();
        }
    }

    /* renamed from: dt */
    public void mo13226dt() {
    }

    public String toString() {
        return String.valueOf(Syst.getClassName(getClass())) + "[game.script=" + Syst.getClassName(mo6901yn().getClass()) + ",oid=" + getObjectId().getId() + "]" + "@" + Integer.toHexString(hashCode());
    }

    /* renamed from: dv */
    public GameScriptClassesImpl mo6888dv() {
        return new GameScriptClassesImpl(this);
    }

    public C6663arL aMp() {
        return null;
    }

    public long cVn() {
        return this.aGA.bGG();
    }

    public C6064afk cVo() {
        return this.hCm;
    }

    public void cVp() {
        mo6866PM().ayN();
        aWq dDA = aWq.dDA();
        dDA.mo12087kM(true);
        dDA.readOnly = true;
    }

    public final String bFY() {
        return String.valueOf(mo6901yn().getClass().getName()) + ":" + getObjectId().getId();
    }

    /* access modifiers changed from: package-private */
    public boolean bGw() {
        return mo6866PM().bGw();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: f */
    public void mo21998f(Runnable runnable) {
        mo6866PM().mo3438f(runnable);
    }

    /* renamed from: a */
    public void mo21968a(C6215aif aif) {
    }

    public C6215aif cVq() {
        return null;
    }

    public aDR bGF() {
        return mo6866PM().bGF();
    }

    /* renamed from: M */
    public C1616Xf mo6865M(Class<?> cls) {
        return this.aGA.mo3340T(cls);
    }

    public boolean bGX() {
        return this.aGA.bGX();
    }

    public boolean bGY() {
        return this.aGA.bGY();
    }

    public boolean bGZ() {
        return this.aGA.bGZ();
    }

    public boolean bHa() {
        return this.aGA.bHa();
    }

    public boolean bHb() {
        return this.aGA.bHb();
    }

    /* renamed from: aG */
    public C1867ad<? extends aDJ> mo10512aG(Class<? extends aDJ> cls) {
        throw new CreatListenerChannelException();
    }

    /* renamed from: n */
    public aDJ mo10532n(Class cls) {
        return (aDJ) mo6866PM().mo3338R(cls).mo6901yn();
    }

    @C0401Fa
    /* renamed from: ms */
    public void mo10530ms(float f) {
        if (isDisposed()) {
            logger.info("Calling schedule on disposed object " + bFY());
        } else if (mo10528du()) {
            logger.info("Calling schedule on deleted object " + bFY());
        } else {
            if (this.hCo == null) {
                synchronized (this) {
                    if (this.hCo == null) {
                        this.hCo = mo6866PM().bGS().mo2190a((C0413Fm.C0414a) new C3588d());
                    }
                }
            }
            mo6866PM().bGS().mo2191a(this.hCo, (long) (1000.0f * f), TimeUnit.MILLISECONDS, -1, TimeUnit.MILLISECONDS);
        }
    }

    @C0401Fa
    /* renamed from: a */
    public void mo21966a(float f, C2742jN jNVar) {
        mo6866PM().bGS().mo2191a(mo6866PM().bGS().mo2190a((C0413Fm.C0414a) new C3587c(f, jNVar)), ((long) f) * 1000, TimeUnit.MILLISECONDS, -1, TimeUnit.MILLISECONDS);
    }

    /* renamed from: V */
    public void mo21963V(float f) {
        if (isDisposed()) {
            logger.info("Script is disposed " + bFY());
        } else if (mo10528du()) {
            logger.info("Script is deleted " + bFY());
        } else {
            mo6901yn().mo4709V(f);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo21965a(float f, int i, Callable<Object> callable) {
        C0413Fm.C0415b a = mo6866PM().bGS().mo2190a((C0413Fm.C0414a) new C3586b(callable, i));
        if (i == 1) {
            mo6866PM().bGS().mo2191a(a, (long) (f * 1000.0f), TimeUnit.MILLISECONDS, 0, TimeUnit.MILLISECONDS);
        } else {
            mo6866PM().bGS().mo2191a(a, (long) (f * 1000.0f), TimeUnit.MILLISECONDS, (long) (1000.0f * f), TimeUnit.MILLISECONDS);
        }
    }

    /* renamed from: mt */
    public <T extends aDJ> T mo10531mt(float f) {
        return mo21999i(f, 1);
    }

    /* renamed from: i */
    public <T extends aDJ> T mo21999i(float f, int i) {
        try {
            T P = mo6866PM().mo3336P(mo6901yn().getClass());
            P.mo6754b(new C3583a(f, i));
            return (aDJ) P;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public long cVr() {
        return mo6866PM().getSynchronizerTime().currentTimeMillis();
    }

    public void bFZ() {
    }

    public void cVs() {
        this.hCm.mo3191cS(true);
    }

    public void delete() {
        C3582se g = this.aGA.mo3440g(this);
        this.hCm = g.hCm;
        mo6901yn().mo6754b(g);
        this.hCl = g.hCl;
    }

    /* renamed from: b */
    public void mo21977b(BuildThreadServerInfraCacheCleanup.C2200a aVar) {
        this.dqS = aVar;
    }

    /* renamed from: a.se$g */
    /* compiled from: a */
    class C3591g implements Runnable {
        private final /* synthetic */ boolean bgc;
        private final /* synthetic */ C6064afk bgd;
        private final /* synthetic */ C6064afk bge;
        private final /* synthetic */ C5439aJj bgg;

        C3591g(boolean z, C5439aJj ajj, C6064afk afk, C6064afk afk2) {
            this.bgc = z;
            this.bgg = ajj;
            this.bgd = afk;
            this.bge = afk2;
        }

        public void run() {
            C3582se.this.mo21972a(this.bgc, this.bgg, this.bgd, this.bge);
        }
    }

    /* renamed from: a.se$f */
    /* compiled from: a */
    class C3590f implements Runnable {
        private final /* synthetic */ boolean bgc;
        private final /* synthetic */ C6064afk bgd;
        private final /* synthetic */ C6064afk bge;

        C3590f(boolean z, C6064afk afk, C6064afk afk2) {
            this.bgc = z;
            this.bgd = afk;
            this.bge = afk2;
        }

        public void run() {
            C3582se.this.mo21972a(this.bgc, (C5439aJj) null, this.bgd, this.bge);
        }
    }

    /* renamed from: a.se$e */
    /* compiled from: a */
    static class C3589e extends C1637Xx {
        private final /* synthetic */ C3582se bfZ;
        private final /* synthetic */ C6144ahM bga;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C3589e(C3582se seVar, C3582se seVar2, C6144ahM ahm) {
            super(seVar);
            this.bfZ = seVar2;
            this.bga = ahm;
        }

        /* renamed from: d */
        public Object mo5606d(C0495Gr gr) {
            return this.bfZ.mo5594a(gr, (C6144ahM<Object>) this.bga);
        }
    }

    /* renamed from: a.se$d */
    /* compiled from: a */
    class C3588d implements C0413Fm.C0414a {
        C3588d() {
        }

        /* renamed from: a */
        public void mo2197a(C0413Fm.C0415b bVar) {
            C3582se.this.mo21963V(((float) (bVar.dnx() - bVar.dny())) * 0.001f);
        }
    }

    /* renamed from: a.se$c */
    /* compiled from: a */
    class C3587c implements C0413Fm.C0414a {
        private final /* synthetic */ C2742jN bfY;
        private final /* synthetic */ float bfz;

        C3587c(float f, C2742jN jNVar) {
            this.bfz = f;
            this.bfY = jNVar;
        }

        /* renamed from: a */
        public void mo2197a(C0413Fm.C0415b bVar) {
            float dnx = ((float) (bVar.dnx() - bVar.dny())) * 0.001f;
            if (dnx - this.bfz <= 0.1f || dnx / this.bfz <= 0.5f) {
            }
            long nanoTime = System.nanoTime();
            this.bfY.run();
            long nanoTime2 = System.nanoTime() - nanoTime;
        }
    }

    /* renamed from: a.se$b */
    /* compiled from: a */
    class C3586b implements C0413Fm.C0414a {
        private final /* synthetic */ int bfA;
        private final /* synthetic */ Callable bfC;
        private int bfB = 0;

        C3586b(Callable callable, int i) {
            this.bfC = callable;
            this.bfA = i;
        }

        /* renamed from: a */
        public void mo2197a(C0413Fm.C0415b bVar) {
            try {
                Object call = this.bfC.call();
                int i = this.bfB + 1;
                this.bfB = i;
                if (i > this.bfA || (call != null && (call instanceof Boolean) && !((Boolean) call).booleanValue())) {
                    bVar.cancel();
                }
            } catch (Exception e) {
                if (bVar != null) {
                    bVar.cancel();
                }
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a.se$a */
    class C3583a extends C1637Xx {
        private final /* synthetic */ int bfA;
        private final /* synthetic */ float bfz;

        C3583a(float f, int i) {
            this.bfz = f;
            this.bfA = i;
        }

        /* renamed from: d */
        public Object mo5606d(C0495Gr gr) {
            C3584a aVar = new C3584a(gr, this.bfz, this.bfA);
            aWq dDA = aWq.dDA();
            if (dDA == null || dDA.jfH || dDA.readOnly) {
                aVar.run();
            } else {
                dDA.mo12083i(aVar);
            }
            return gr.aQK().mo4768pN();
        }

        /* renamed from: a.se$a$a */
        class C3584a implements Runnable {
            private final /* synthetic */ int bfA;
            private final /* synthetic */ float bfz;
            private final /* synthetic */ C0495Gr dgP;

            C3584a(C0495Gr gr, float f, int i) {
                this.dgP = gr;
                this.bfz = f;
                this.bfA = i;
            }

            public void run() {
                ((aCE) this.dgP).mo7998m(C3582se.this.mo6901yn());
                C3582se.this.mo21965a(this.bfz, this.bfA, (Callable<Object>) new C3585a(this.dgP));
            }

            /* renamed from: a.se$a$a$a */
            class C3585a implements Callable<Object> {
                private final /* synthetic */ C0495Gr dgP;

                C3585a(C0495Gr gr) {
                    this.dgP = gr;
                }

                public Object call() {
                    if (C3583a.this.isDisposed()) {
                        return null;
                    }
                    switch (C3582se.this.mo6893i(this.dgP.aQK())) {
                        case 1:
                            try {
                                return this.dgP.aQL().mo14a(this.dgP);
                            } catch (RuntimeException e) {
                                throw e;
                            } catch (Throwable th) {
                                throw new RuntimeException(th);
                            }
                        case 2:
                            return C3582se.this.mo5606d(this.dgP);
                        case 3:
                            C3582se.this.mo5606d(this.dgP);
                            return this.dgP.aQL().mo14a(this.dgP);
                        default:
                            return null;
                    }
                }
            }
        }
    }
}
