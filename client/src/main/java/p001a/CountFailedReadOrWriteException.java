package p001a;

/* renamed from: a.adk  reason: case insensitive filesystem */
/* compiled from: a */
public class CountFailedReadOrWriteException extends RuntimeException {


    public CountFailedReadOrWriteException(String str) {
        super(str);
    }

    public CountFailedReadOrWriteException(String str, Throwable th) {
        super(str, th);
    }

    public CountFailedReadOrWriteException(Throwable th) {
        super(th);
    }

    public CountFailedReadOrWriteException() {
    }
}
