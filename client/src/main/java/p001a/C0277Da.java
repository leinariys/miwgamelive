package p001a;

import game.script.mail.MailMessage;
import logic.ui.item.EditorPane;
import logic.ui.item.GBox;
import logic.ui.item.InternalFrame;
import logic.ui.item.TextField;
import taikodom.addon.mail.MailAddon;

import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

/* renamed from: a.Da */
/* compiled from: a */
public class C0277Da {
    private static /* synthetic */ int[] hvW;
    private MailAddon hvP;
    private GBox hvQ;
    private logic.ui.item.TextField hvR;
    private TextField hvS;
    private EditorPane hvT;
    private C0279b hvU;
    private MailMessage hvV;

    /* renamed from: nM */
    private InternalFrame f422nM;

    public C0277Da(MailAddon mailAddon, InternalFrame nxVar) {
        this(mailAddon, nxVar, C0279b.NEW, (MailMessage) null);
    }

    public C0277Da(MailAddon mailAddon, InternalFrame nxVar, C0279b bVar, MailMessage asf) {
        this.hvP = mailAddon;
        this.f422nM = nxVar;
        this.hvU = bVar;
        this.hvV = asf;
        init();
    }

    static /* synthetic */ int[] cQe() {
        int[] iArr = hvW;
        if (iArr == null) {
            iArr = new int[C0279b.values().length];
            try {
                iArr[C0279b.FORWARD.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C0279b.NEW.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C0279b.REPLY.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[C0279b.REPLYALL.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            hvW = iArr;
        }
        return iArr;
    }

    private void init() {
        this.f422nM.setVisible(true);
        this.f422nM.pack();
        this.hvQ = this.f422nM.mo4915cd("form");
        this.hvQ.mo4917cf("subject").setText("Subject:");
        this.hvR = this.f422nM.mo4915cd("subjectText");
        this.hvQ.mo4917cf("to").setText("To:");
        this.hvS = this.f422nM.mo4915cd("toText");
        this.hvT = this.f422nM.mo4915cd("body");
        this.hvT.setCaretColor(Color.WHITE);
        cQc();
        cPW();
        cPX();
        this.f422nM.mo4915cd("sendMessageButton").addActionListener(new C0278a());
    }

    private void cPW() {
        switch (cQe()[this.hvU.ordinal()]) {
            case 1:
                this.f422nM.setTitle("New Message");
                return;
            case 2:
            case 3:
                this.f422nM.setTitle("Re: " + this.hvV.getSubject());
                return;
            case 4:
                this.f422nM.setTitle("Fwd: " + this.hvV.getSubject());
                return;
            default:
                return;
        }
    }

    private void cPX() {
        if (this.hvU != C0279b.NEW) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("<br><br>");
            stringBuffer.append(this.hvV.ctR());
            stringBuffer.append(" ");
            stringBuffer.append(this.hvV.mo15962KJ().getName());
            stringBuffer.append(":<br>");
            stringBuffer.append(this.hvV.ctL());
            this.hvT.setText(stringBuffer.toString());
        }
        switch (cQe()[this.hvU.ordinal()]) {
            case 1:
                cPY();
                return;
            case 2:
                cQa();
                return;
            case 3:
                cQb();
                return;
            case 4:
                cPZ();
                return;
            default:
                return;
        }
    }

    private void cPY() {
        this.hvR.requestFocus();
    }

    private void cPZ() {
        this.hvR.setText("Fwd: " + this.hvV.getSubject());
        this.hvT.setCaretPosition(1);
        this.hvT.requestFocus();
    }

    private void cQa() {
        this.hvS.setText(this.hvV.mo15962KJ().getName());
        this.hvR.setText("Re: " + this.hvV.getSubject());
        this.hvT.setCaretPosition(1);
        this.hvT.requestFocus();
    }

    private void cQb() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.hvV.mo15962KJ().getName());
        int size = this.hvV.ctO().size();
        for (int i = 0; i < size; i++) {
            String name = this.hvV.ctO().get(i).getName();
            if (!name.equals(this.hvP.mo24252JW().getPlayer().getName())) {
                if (i == 0) {
                    sb.append(", ");
                }
                sb.append(name);
                if (i < size - 1) {
                    sb.append(", ");
                }
            }
        }
        this.hvS.setText(sb.toString());
        this.hvR.setText("Re: " + this.hvV.getSubject());
        this.hvT.setCaretPosition(1);
        this.hvT.requestFocus();
    }

    private void cQc() {
        String str = null;
        try {
            InputStream f = this.hvP.mo24252JW().mo11846f(getClass(), "message.html");
            StringBuilder sb = new StringBuilder();
            while (f.available() > 0) {
                sb.append((char) f.read());
            }
            str = sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (str != null) {
            HTMLEditorKit hTMLEditorKit = new HTMLEditorKit();
            HTMLDocument createDefaultDocument = hTMLEditorKit.createDefaultDocument();
            try {
                hTMLEditorKit.read(new StringReader(str), createDefaultDocument, 0);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            this.hvT.setDocument(createDefaultDocument);
        }
    }

    private String cQd() {
        String str;
        HTMLDocument document = this.hvT.getDocument();
        HTMLDocument document2 = document.getElement("body").getDocument();
        try {
            str = document2.getText(0, document2.getLength());
        } catch (BadLocationException e) {
            e.printStackTrace();
            str = "";
        }
        try {
            document.setInnerHTML(document.getElement("body"), str.replaceFirst("\\n", "").replaceAll("\\n", "<br>\n"));
        } catch (BadLocationException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        return this.hvT.getText();
    }

    /* access modifiers changed from: private */
    public void sendMessage() {
        if (this.hvP.mo24252JW().getPlayer().dwU().mo21455s(this.hvS.getText(), this.hvR.getText(), cQd())) {
            this.f422nM.destroy();
            this.f422nM = null;
        }
    }

    /* renamed from: a.Da$b */
    /* compiled from: a */
    public enum C0279b {
        NEW,
        REPLY,
        REPLYALL,
        FORWARD
    }

    /* renamed from: a.Da$a */
    class C0278a implements ActionListener {
        C0278a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C0277Da.this.sendMessage();
        }
    }
}
