package p001a;

import game.network.message.externalizable.C3328qK;
import logic.res.html.DataClassSerializer;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.NB */
/* compiled from: a */
public class C0897NB extends DataClassSerializer {
    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) {
        if (!objectInput.readBoolean()) {
            return null;
        }
        C3328qK qKVar = new C3328qK();
        qKVar.readExternal(objectInput);
        return qKVar;
    }

    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) {
        if (obj == null) {
            objectOutput.writeBoolean(false);
            return;
        }
        objectOutput.writeBoolean(true);
        ((C3328qK) obj).writeExternal(objectOutput);
    }
}
