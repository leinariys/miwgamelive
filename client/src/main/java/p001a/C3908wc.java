package p001a;

import game.network.message.serializable.C0035AS;
import logic.res.html.DataClassSerializer;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.wc */
/* compiled from: a */
public class C3908wc extends DataClassSerializer {
    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) {
        if (!objectInput.readBoolean()) {
            return null;
        }
        C0035AS as = new C0035AS();
        as.readExternal(objectInput);
        return as;
    }

    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) {
        if (obj == null) {
            objectOutput.writeBoolean(false);
            return;
        }
        objectOutput.writeBoolean(true);
        ((C0035AS) obj).writeExternal(objectOutput);
    }
}
