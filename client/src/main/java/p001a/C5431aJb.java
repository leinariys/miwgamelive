package p001a;

import javax.swing.*;
import javax.swing.event.ListDataListener;

/* renamed from: a.aJb  reason: case insensitive filesystem */
/* compiled from: a */
public class C5431aJb implements ComboBoxModel {
    private int aVX;

    public Object getSelectedItem() {
        return C0314EF.values()[this.aVX];
    }

    public void setSelectedItem(Object obj) {
        for (int i = 0; i < C0314EF.values().length; i++) {
            if (C0314EF.values()[i] == obj) {
                this.aVX = i;
                return;
            }
        }
    }

    public void addListDataListener(ListDataListener listDataListener) {
    }

    public Object getElementAt(int i) {
        return C0314EF.values()[i];
    }

    public int getSize() {
        return C0314EF.values().length;
    }

    public void removeListDataListener(ListDataListener listDataListener) {
    }
}
