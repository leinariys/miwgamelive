package p001a;

import game.network.aQD;
import org.apache.commons.jxpath.JXPathContext;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: a.FD */
/* compiled from: a */
public class C0378FD extends aQD {
    /* renamed from: a */
    public String mo2073a(Iterator<?> it, List<String> list, Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        sb.append("\n<table border='1' width='98%'>\n<thead>\n\t<tr>");
        for (int i = 0; i < list.size(); i++) {
            sb.append("<td><b>").append(map.get(list.get(i))).append("</b></td>");
        }
        sb.append("</tr>\n</thead>\n<tbody>");
        while (it.hasNext()) {
            sb.append("\n\t<tr>");
            JXPathContext newContext = JXPathContext.newContext(it.next());
            newContext.setFunctions(this.iEM);
            for (int i2 = 0; i2 < list.size(); i2++) {
                List selectNodes = newContext.selectNodes(list.get(i2));
                sb.append("<td>\n<pre>");
                String str = "";
                for (Object next : selectNodes) {
                    if (next == null) {
                        next = "";
                    }
                    sb.append(str).append(next.toString());
                    str = "\n";
                }
                sb.append("</pre>\n</td>");
            }
            sb.append("</tr>");
        }
        sb.append("\n</tbody>\n</table>");
        return sb.toString();
    }
}
