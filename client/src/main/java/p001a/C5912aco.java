package p001a;

import logic.swing.C0454GJ;
import logic.swing.C2740jL;
import logic.swing.aDX;

import javax.swing.*;
import java.util.Vector;

/* renamed from: a.aco  reason: case insensitive filesystem */
/* compiled from: a */
abstract class C5912aco {
    public C0454GJ fbg;
    private int count;
    private boolean efB = false;
    private C1865a fbf;
    private Vector<aDX> fbh = new Vector<>();

    public C5912aco() {
    }

    /* access modifiers changed from: protected */
    public void bOC() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.fbh.size()) {
                this.fbh.clear();
                return;
            } else {
                this.fbh.get(i2).kill();
                i = i2 + 1;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo12692a(JComponent jComponent, String str, C2740jL jLVar, C0454GJ gj) {
        this.fbh.add(new aDX(jComponent, str, jLVar, gj, true));
    }

    /* renamed from: a */
    public void mo12691a(C1865a aVar) {
        this.fbf = aVar;
    }

    /* access modifiers changed from: protected */
    public void bOD() {
        if (this.fbg != null) {
            this.fbg.mo9a((JComponent) null);
        }
    }

    /* renamed from: a */
    public void mo12690a(C0454GJ gj) {
        this.efB = true;
        this.fbh.clear();
        this.fbg = gj;
        this.count = 0;
    }

    public void stop() {
        mo12691a((C1865a) null);
        this.efB = false;
        bOC();
    }

    /* access modifiers changed from: protected */
    public synchronized void bOE() {
        this.count++;
    }

    /* access modifiers changed from: protected */
    public synchronized void bOF() {
        this.count--;
    }

    /* access modifiers changed from: protected */
    public synchronized int getCount() {
        return this.count;
    }

    public void bOG() {
        bOF();
        if (this.count <= 0 && this.fbf != null) {
            this.fbf.mo4gZ();
            this.count = 0;
        }
    }

    public boolean isAnimating() {
        return this.efB;
    }

    /* renamed from: du */
    public void mo12698du(boolean z) {
        this.efB = z;
    }

    /* renamed from: a.aco$a */
    public interface C1865a {
        /* renamed from: gZ */
        void mo4gZ();
    }
}
