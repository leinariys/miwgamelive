package p001a;

import gnu.trove.THashMap;
import org.mozilla1.classfile.C0147Bi;
import org.objectweb.asm.tree.AnnotationNode;
import org.objectweb.asm.tree.FieldNode;
import util.Syst;

import java.util.Map;

/* renamed from: a.Fd */
/* compiled from: a */
public class C0404Fd implements FieldVisitor, Opcodes {
    final String cUR;
    final String cUS;
    final String cUT;
    final String cUU;
    final String cUW;
    final String desc;
    final String name;
    final String signature;
    final Object value;
    private final FieldNode cUN;
    private final C6416amY cUO;
    String cUQ;
    Type cUV;
    int index;
    private boolean cUP;
    private boolean cUX;
    private Map<String, AnnotationNode> cUY;

    public C0404Fd(C6416amY amy, int i, String str, String str2, String str3, Object obj) {
        this.cUO = amy;
        this.signature = str3;
        this.name = str;
        this.desc = str2;
        this.value = obj;
        if ((i & 128) != 0) {
            this.cUP = false;
        } else if ((i & 8) == 0) {
            this.cUP = true;
        } else if ((i & 16) != 0 || !amy.ckp()) {
            this.cUP = false;
        } else {
            this.cUP = true;
            this.cUX = true;
        }
        this.cUN = new FieldNode(i, str, str2, str3, obj);
        this.cUR = C2821kb.arZ + str;
        this.cUS = "()" + str2;
        this.cUT = C2821kb.asa + str;
        this.cUU = "(" + str2 + ")V";
        this.cUW = Syst.getMd5(String.valueOf(amy.name.replace(C0147Bi.cla, '.')) + "#" + str);
    }

    public AnnotationVisitor visitAnnotation(String str, boolean z) {
        AnnotationNode annotationNode = new AnnotationNode(str);
        C0925NZ nz = new C0925NZ(this.cUN.visitAnnotation(str, z), annotationNode);
        if (this.cUY == null) {
            this.cUY = new THashMap();
        }
        this.cUY.put(str, annotationNode);
        if (C2821kb.aqz.getDescriptor().equals(str)) {
            this.cUP = false;
        }
        return nz;
    }

    public void visitAttribute(Attribute attribute) {
        this.cUN.visitAttribute(attribute);
    }

    public void visitEnd() {
        if (!this.cUP) {
            this.cUN.visitEnd();
            this.cUN.accept(this.cUO.ckg());
            return;
        }
        this.cUO.mo14842a(this);
        AnnotationVisitor visitAnnotation = this.cUN.visitAnnotation(C2821kb.aqu.getDescriptor(), true);
        visitAnnotation.visit("hash", this.cUW);
        visitAnnotation.visit("id", Integer.valueOf(this.index));
        visitAnnotation.visitEnd();
        this.cUN.access = (this.cUN.access & -17) | 8;
        this.cUN.visitEnd();
        this.cUN.accept(this.cUO.ckg());
        ClassVisitor ckg = this.cUO.ckg();
        this.cUV = Type.getType(this.desc);
        this.cUQ = C2821kb.arP + this.name;
        ckg.visitField(25, this.cUQ, "Ltaikodom/infra/IField;", (String) null, (Object) null).visitEnd();
        MethodVisitor visitMethod = ckg.visitMethod(2, this.cUR, this.cUS, (String) null, (String[]) null);
        visitMethod.visitCode();
        if (!this.cUX) {
            this.cUO.mo14851e(visitMethod);
            C2821kb.arg.mo20041a(visitMethod, 185);
            visitMethod.visitFieldInsn(178, this.cUO.name, this.cUQ, "Ltaikodom/infra/IField;");
            switch (this.cUV.getSort()) {
                case 1:
                    C2821kb.ars.mo20041a(visitMethod, 185);
                    break;
                case 2:
                    C2821kb.aru.mo20041a(visitMethod, 185);
                    break;
                case 3:
                    C2821kb.arr.mo20041a(visitMethod, 185);
                    break;
                case 4:
                    C2821kb.art.mo20041a(visitMethod, 185);
                    break;
                case 5:
                    C2821kb.arq.mo20041a(visitMethod, 185);
                    break;
                case 6:
                    C2821kb.arw.mo20041a(visitMethod, 185);
                    break;
                case 7:
                    C2821kb.arv.mo20041a(visitMethod, 185);
                    break;
                case 8:
                    C2821kb.arx.mo20041a(visitMethod, 185);
                    break;
                default:
                    C2821kb.ary.mo20041a(visitMethod, 185);
                    if (C1551Wl.m11312a(this.cUV) == null) {
                        if (this.cUV.getSort() != 9) {
                            visitMethod.visitTypeInsn(192, this.cUV.getInternalName());
                            break;
                        } else {
                            visitMethod.visitTypeInsn(192, this.cUV.getDescriptor());
                            break;
                        }
                    }
                    break;
            }
            visitMethod.visitInsn(this.cUV.getOpcode(172));
            visitMethod.visitMaxs(4, 1);
        } else {
            visitMethod.visitVarInsn(25, 0);
            C2821kb.aqW.mo20041a(visitMethod, 182);
            visitMethod.visitTypeInsn(192, this.cUO.cko());
            visitMethod.visitMethodInsn(182, this.cUO.cko(), "get" + Syst.m15908t(this.name), this.cUS);
            visitMethod.visitInsn(this.cUV.getOpcode(172));
            visitMethod.visitMaxs(2, 1);
        }
        visitMethod.visitEnd();
        MethodVisitor visitMethod2 = ckg.visitMethod(2, this.cUT, this.cUU, (String) null, (String[]) null);
        visitMethod2.visitCode();
        if (!this.cUX) {
            this.cUO.mo14851e(visitMethod2);
            C2821kb.arg.mo20041a(visitMethod2, 185);
            visitMethod2.visitFieldInsn(178, this.cUO.name, this.cUQ, "Ltaikodom/infra/IField;");
            visitMethod2.visitVarInsn(this.cUV.getOpcode(21), 1);
            switch (this.cUV.getSort()) {
                case 1:
                    C2821kb.arj.mo20041a(visitMethod2, 185);
                    break;
                case 2:
                    C2821kb.arl.mo20041a(visitMethod2, 185);
                    break;
                case 3:
                    C2821kb.ari.mo20041a(visitMethod2, 185);
                    break;
                case 4:
                    C2821kb.ark.mo20041a(visitMethod2, 185);
                    break;
                case 5:
                    C2821kb.arh.mo20041a(visitMethod2, 185);
                    break;
                case 6:
                    C2821kb.arn.mo20041a(visitMethod2, 185);
                    break;
                case 7:
                    C2821kb.arm.mo20041a(visitMethod2, 185);
                    break;
                case 8:
                    C2821kb.aro.mo20041a(visitMethod2, 185);
                    break;
                default:
                    C2821kb.arp.mo20041a(visitMethod2, 185);
                    break;
            }
            visitMethod2.visitInsn(177);
            visitMethod2.visitMaxs(6, 2);
        } else {
            visitMethod2.visitTypeInsn(187, C2821kb.arL.getInternalName());
            visitMethod2.visitInsn(89);
            C2821kb.arM.mo5379a(visitMethod2);
            visitMethod2.visitInsn(191);
            visitMethod2.visitMaxs(3, 2);
        }
        visitMethod2.visitEnd();
    }
}
