package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.Dg */
/* compiled from: a */
public abstract class C0285Dg extends C2225d {
    public C0285Dg(C1701ZB zb, C3737uO uOVar, C6760atE ate) {
        super(zb, uOVar, ate);
    }

    /* renamed from: K */
    public abstract void mo1647K(Vec3f vec3f);

    /* renamed from: a */
    public abstract int mo1649a(float f, int i, float f2);

    /* renamed from: a */
    public abstract void mo1653a(C6238ajC ajc);

    /* renamed from: a */
    public abstract void mo1654a(C2849lA lAVar);

    public abstract void aDR();

    public abstract C2849lA aDS();

    public abstract aVG aDU();

    public abstract void aDV();

    /* renamed from: b */
    public abstract void mo1662b(C6238ajC ajc);

    /* renamed from: eB */
    public final int mo1663eB(float f) {
        return mo1649a(f, 1, 0.016666668f);
    }

    /* renamed from: a */
    public final int mo1648a(float f, int i) {
        return mo1649a(f, i, 0.016666668f);
    }

    /* renamed from: a */
    public final void mo1650a(C1083Pr pr) {
        mo1651a(pr, false);
    }

    /* renamed from: a */
    public void mo1651a(C1083Pr pr, boolean z) {
    }

    /* renamed from: b */
    public void mo1660b(C1083Pr pr) {
    }

    /* renamed from: a */
    public void mo1652a(C1725ZV zv) {
    }

    /* renamed from: b */
    public void mo1661b(C1725ZV zv) {
    }

    public int aDT() {
        return 0;
    }

    /* renamed from: gF */
    public C1083Pr mo1664gF(int i) {
        return null;
    }
}
