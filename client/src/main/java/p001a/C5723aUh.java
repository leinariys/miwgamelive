package p001a;

import logic.bbb.C4029yK;

import java.util.HashMap;
import java.util.Map;

/* renamed from: a.aUh  reason: case insensitive filesystem */
/* compiled from: a */
public class C5723aUh implements aOT<C1466Va> {
    private Map<String, C4029yK> iWF = new HashMap();

    /* renamed from: a */
    public void mo4838c(C1466Va va) {
        synchronized (this.iWF) {
            va.mo6159b(this.iWF.get(va.fileName));
        }
    }

    /* renamed from: b */
    public void mo4836b(C1466Va va) {
        synchronized (this.iWF) {
            if (!this.iWF.containsKey(va.fileName)) {
                this.iWF.put(va.fileName, C2606hU.m32703b(va.fileName, va.enL));
            }
        }
    }
}
