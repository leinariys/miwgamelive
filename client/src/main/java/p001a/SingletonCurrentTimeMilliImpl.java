package p001a;

/* renamed from: a.atM  reason: case insensitive filesystem */
/* compiled from: a */
public class SingletonCurrentTimeMilliImpl {
    public static final CurrentTimeMilli CURRENT_TIME_MILLI = new CurrentTimeMilliImpl();

    public static long currentTimeMillis() {
        return CURRENT_TIME_MILLI.currentTimeMillis();
    }
}
