package p001a;

import gnu.trove.THashMap;

import java.util.ArrayList;
import java.util.Map;

/* renamed from: a.aMq  reason: case insensitive filesystem */
/* compiled from: a */
public class C5524aMq implements C5598aPm {
    public Map<Class<?>, ArrayList<?>> ipr;
    public int ips;

    public C5524aMq() {
        this(128);
    }

    public C5524aMq(int i) {
        this.ipr = new THashMap();
        this.ips = i;
    }

    public <T> void put(Class<T> cls, Object obj) {
        ArrayList arrayList = this.ipr.get(cls);
        if (arrayList == null) {
            arrayList = new ArrayList();
            this.ipr.put(cls, arrayList);
        }
        if (arrayList.size() < this.ips) {
            arrayList.add(obj);
        }
    }

    public int getMaximumCacheSize() {
        return this.ips;
    }

    public void setMaximumCacheSize(int i) {
        this.ips = i;
    }

    /* renamed from: aN */
    public <T> Object mo10176aN(Class<T> cls) {
        ArrayList arrayList = this.ipr.get(cls);
        if (arrayList == null) {
            return null;
        }
        int size = arrayList.size();
        if (size == 0) {
            return null;
        }
        return arrayList.remove(size - 1);
    }

    public boolean isEmpty() {
        return this.ipr.isEmpty();
    }

    /* renamed from: aO */
    public <T> int mo10177aO(Class<T> cls) {
        ArrayList arrayList = this.ipr.get(cls);
        if (arrayList == null) {
            return 0;
        }
        return arrayList.size();
    }
}
