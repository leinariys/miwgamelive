package p001a;

import logic.res.html.C7025azm;
import logic.res.html.axI;
import logic.thred.C2316dw;

import java.util.Collection;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: a.aWm */
/* compiled from: a */
public class aWm<T> implements C7025azm<T> {
    private final LinkedBlockingQueue<Object> iMY;
    private final int jfA;
    private final Object jfz = new Object();
    private boolean disposed;

    public aWm(int i) {
        this.jfA = i;
        this.iMY = new LinkedBlockingQueue<>(i == -1 ? Integer.MAX_VALUE : i);
    }

    public boolean isEmpty() {
        return this.iMY.isEmpty();
    }

    public int size() {
        return this.iMY.size();
    }

    public int capacity() {
        return this.jfA;
    }

    public boolean isFull() {
        return this.iMY.remainingCapacity() == 0;
    }

    public void push(T t) {
        if (this.disposed) {
            throw new axI();
        }
        try {
            this.iMY.put(t);
        } catch (InterruptedException e) {
            throw new C2316dw("wait interrupted", e);
        }
    }

    public T pop() {
        if (this.disposed) {
            throw new axI();
        }
        try {
            T take = this.iMY.take();
            if (take == this.jfz) {
                return null;
            }
            return take;
        } catch (InterruptedException e) {
            throw new C2316dw("wait interrupted", e);
        }
    }

    /* renamed from: t */
    public boolean mo12044t(Collection<T> collection) {
        if (this.disposed) {
            throw new axI();
        }
        this.iMY.remove(this.jfz);
        collection.addAll(this.iMY);
        this.iMY.clear();
        this.iMY.add(this.jfz);
        return true;
    }

    public void dispose() {
        int i = 1000;
        this.disposed = true;
        if (this.iMY.remainingCapacity() <= 1000) {
            i = this.iMY.remainingCapacity();
        }
        int i2 = 0;
        while (i2 < i) {
            try {
                this.iMY.put(this.jfz);
                i2++;
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    public boolean isDisposed() {
        return this.disposed;
    }
}
