package p001a;

import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;
import java.util.HashMap;
import java.util.Map;
/**
 * URL-адрес приложения для Java Virtual Machine.
 * URLStreamHandlerFactory - Этот интерфейс определяет фабрику для обработчиков протоколов потока URL.
 */
/* renamed from: a.hP */
/* compiled from: a */
public class C2600hP implements URLStreamHandlerFactory {

    /**
     * Ссылка на фабрику
     */
    /* renamed from: VR */
    private static C2600hP f7899VR;

    /**
     * Таблица хеш значений
     */
    public Map<String, URLStreamHandler> map = new HashMap();

    /* renamed from: ze */
    public static C2600hP m32602ze() {
        if (f7899VR == null) {
            f7899VR = new C2600hP();
        }
        return f7899VR;
    }

    /**
     * Создает новый экземпляр URLStreamHandler с указанным протоколом.
     *
     * @param str протокол («ftp», «http», «nntp» и т. д.).
     * @return URLStreamHandler для конкретного протокола.
     */
    public URLStreamHandler createURLStreamHandler(String str) {
        return this.map.get(str.toLowerCase());
    }

    /**
     * Связывает указанное значение с указанным ключом на этой карте.
     *
     * @param str              ключ, с которым должно быть связано указанное значение
     * @param uRLStreamHandler значение, которое должно быть связано с указанным ключом
     */
    /* renamed from: a */
    public void mo19253a(String str, URLStreamHandler uRLStreamHandler) {
        this.map.put(str.toLowerCase(), uRLStreamHandler);
    }
}
