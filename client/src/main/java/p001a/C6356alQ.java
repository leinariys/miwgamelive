package p001a;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.alQ  reason: case insensitive filesystem */
/* compiled from: a */
public class C6356alQ<T> {

    private final BoundingBox dVu;
    private final float volume;
    /* renamed from: bV */
    List<C1934a<T>> f4851bV = new ArrayList();
    BoundingBox[] dVs = new BoundingBox[8];
    C6356alQ<T>[] fYd = new C6356alQ[8];
    private float fYe = 1000.0f;

    public C6356alQ(BoundingBox boundingBox) {
        this.dVu = boundingBox;
        Vec3f dqQ = boundingBox.dqQ();
        Vec3f dqP = boundingBox.dqP();
        Vec3f bny = boundingBox.bny();
        this.volume = boundingBox.dio() * boundingBox.dip() * boundingBox.diq();
        this.dVs[0] = new BoundingBox(dqP, bny);
        this.dVs[1] = new BoundingBox(bny, dqQ);
        this.dVs[2] = new BoundingBox(new Vec3f(dqP.x, dqP.y, bny.z), new Vec3f(bny.x, bny.y, dqQ.z));
        this.dVs[3] = new BoundingBox(new Vec3f(dqP.x, bny.y, dqP.z), new Vec3f(bny.x, dqQ.y, bny.z));
        this.dVs[4] = new BoundingBox(new Vec3f(dqP.x, bny.y, bny.z), new Vec3f(bny.x, dqQ.y, dqQ.z));
        this.dVs[5] = new BoundingBox(new Vec3f(bny.x, dqP.y, dqP.z), new Vec3f(dqQ.x, bny.y, bny.z));
        this.dVs[6] = new BoundingBox(new Vec3f(bny.x, dqP.y, bny.z), new Vec3f(dqQ.x, bny.y, dqQ.z));
        this.dVs[7] = new BoundingBox(new Vec3f(bny.x, bny.y, dqP.z), new Vec3f(dqQ.x, dqQ.y, bny.z));
    }

    /* renamed from: b */
    public void mo14662b(BoundingBox boundingBox, T t) {
        if (this.dVu.mo23423a(boundingBox)) {
            if (boundingBox.dio() * boundingBox.dip() * boundingBox.diq() >= this.volume || this.volume <= this.fYe) {
                this.f4851bV.add(new C1934a(boundingBox, t));
                return;
            }
            for (int i = 0; i < 8; i++) {
                if (this.dVs[i].mo23423a(boundingBox)) {
                    if (this.fYd[i] == null) {
                        this.fYd[i] = new C6356alQ<>(this.dVs[i]);
                    }
                    this.fYd[i].mo14662b(boundingBox, t);
                }
            }
        }
    }

    /* renamed from: b */
    public boolean mo14664b(BoundingBox boundingBox) {
        if (!this.dVu.mo23423a(boundingBox)) {
            return false;
        }
        if (this.f4851bV.size() > 0) {
            for (C1934a<T> a : this.f4851bV) {
                if (a.bJB.mo23423a(boundingBox)) {
                    return true;
                }
            }
        }
        for (int i = 0; i < 8; i++) {
            if (this.fYd[i] != null && this.fYd[i].mo14664b(boundingBox)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: a */
    public <QT extends T> void mo14661a(C3026nA<QT> nAVar) {
        if (nAVar.mo5332a(this.dVu)) {
            for (int i = 0; i < 8; i++) {
                if (this.fYd[i] != null) {
                    this.fYd[i].mo14661a(nAVar);
                }
            }
            if (this.f4851bV.size() > 0) {
                int size = this.f4851bV.size();
                while (true) {
                    int i2 = size - 1;
                    if (i2 >= 0) {
                        C1934a aVar = this.f4851bV.get(i2);
                        nAVar.mo5334a(aVar.bJB, aVar.obj);
                        size = i2;
                    } else {
                        return;
                    }
                }
            }
        }
    }

    /* renamed from: b */
    public <QT extends T> boolean mo14663b(C3026nA<QT> nAVar) {
        if (!nAVar.mo5332a(this.dVu)) {
            return false;
        }
        if (this.f4851bV.size() > 0) {
            for (C1934a next : this.f4851bV) {
                if (nAVar.mo5334a(next.bJB, next.obj)) {
                    return true;
                }
            }
        }
        for (int i = 0; i < 8; i++) {
            if (this.fYd[i] != null && this.fYd[i].mo14663b(nAVar)) {
                return true;
            }
        }
        return false;
    }

    public float cil() {
        return this.fYe;
    }

    /* renamed from: jR */
    public void mo14666jR(float f) {
        this.fYe = f;
    }

    /* renamed from: a.alQ$a */
    static class C1934a<Q> {
        /* access modifiers changed from: private */
        public final BoundingBox bJB;
        /* access modifiers changed from: private */
        public final Q obj;

        public C1934a(BoundingBox boundingBox, Q q) {
            this.bJB = boundingBox;
            this.obj = q;
        }
    }
}
