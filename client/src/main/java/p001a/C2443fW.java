package p001a;

import java.util.HashMap;
import java.util.Map;

/* renamed from: a.fW */
/* compiled from: a */
public class C2443fW {

    /* renamed from: OA */
    public static final int f7375OA = 2;

    /* renamed from: OB */
    public static final int f7376OB = 3;

    /* renamed from: OC */
    public static final String f7377OC = "UNDEFINED";
    /* renamed from: Oy */
    public static final int f7380Oy = 0;
    /* renamed from: Oz */
    public static final int f7381Oz = 1;
    /* renamed from: Ow */
    public static Map<Integer, String> f7378Ow = new HashMap();
    /* renamed from: Ox */
    public static Map<String, Integer> f7379Ox = new HashMap();

    static {
        f7378Ow.put(0, "JOY_AXIS_X");
        f7378Ow.put(1, "JOY_AXIS_Y");
        f7378Ow.put(2, "JOY_AXIS_Z");
        f7378Ow.put(3, "JOY_AXIS_W");
        f7379Ox.put("JOY_AXIS_X", 0);
        f7379Ox.put("JOY_AXIS_Y", 1);
        f7379Ox.put("JOY_AXIS_Z", 2);
        f7379Ox.put("JOY_AXIS_W", 3);
    }

    /* renamed from: ao */
    public static int m31093ao(String str) {
        Integer num = f7379Ox.get(str);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    /* renamed from: bf */
    public static String m31094bf(int i) {
        String str = f7378Ow.get(Integer.valueOf(i));
        return str != null ? str : "UNDEFINED";
    }
}
