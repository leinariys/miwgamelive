package p001a;

import java.util.HashMap;
import java.util.Map;

/* renamed from: a.xA */
/* compiled from: a */
public class C3942xA {

    public static final int bGK = 0;
    public static final int bGL = 1;
    public static final int bGM = 2;
    public static final int bGN = 3;
    public static final int bGO = 4;
    public static final int bGP = 5;
    public static final int bGQ = 6;
    public static final int bGR = 7;
    public static final int bGS = 8;
    public static final int bGT = 9;
    public static final int bGU = 10;
    public static final int bGV = 11;
    public static final int bGW = 10;
    public static final int bGX = 11;
    public static final int bGY = 12;
    public static final int bGZ = 13;
    public static final int bHa = 14;
    /* renamed from: Ow */
    public static Map<Integer, String> f9511Ow = new HashMap();
    /* renamed from: Ox */
    public static Map<String, Integer> f9512Ox = new HashMap();

    static {
        f9511Ow.put(0, "JOY_BTN_0");
        f9511Ow.put(1, "JOY_BTN_1");
        f9511Ow.put(2, "JOY_BTN_2");
        f9511Ow.put(3, "JOY_BTN_3");
        f9511Ow.put(4, "JOY_BTN_4");
        f9511Ow.put(5, "JOY_BTN_5");
        f9511Ow.put(6, "JOY_BTN_6");
        f9511Ow.put(7, "JOY_BTN_7");
        f9511Ow.put(8, "JOY_BTN_8");
        f9511Ow.put(9, "JOY_BTN_9");
        f9511Ow.put(10, "JOY_BTN_10");
        f9511Ow.put(11, "JOY_BTN_11");
        f9512Ox.put("JOY_BTN_0", 0);
        f9512Ox.put("JOY_BTN_1", 1);
        f9512Ox.put("JOY_BTN_2", 2);
        f9512Ox.put("JOY_BTN_3", 3);
        f9512Ox.put("JOY_BTN_4", 4);
        f9512Ox.put("JOY_BTN_5", 5);
        f9512Ox.put("JOY_BTN_6", 6);
        f9512Ox.put("JOY_BTN_7", 7);
        f9512Ox.put("JOY_BTN_8", 8);
        f9512Ox.put("JOY_BTN_9", 9);
        f9512Ox.put("JOY_BTN_10", 10);
        f9512Ox.put("JOY_BTN_11", 11);
    }

    /* renamed from: ao */
    public static int m40807ao(String str) {
        Integer num = f9512Ox.get(str);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    /* renamed from: bf */
    public static String m40808bf(int i) {
        String str = f9511Ow.get(Integer.valueOf(i));
        return str != null ? str : "UNDEFINED";
    }
}
