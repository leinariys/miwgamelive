package p001a;

import game.network.message.externalizable.C0939Nn;
import logic.WrapRunnable;
import logic.aaa.C2682iV;
import logic.swing.C5378aHa;
import logic.ui.ComponentManager;
import logic.ui.item.*;
import taikodom.addon.IAddonProperties;
import taikodom.addon.conversioncampaign.ConversionCampaignAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

/* renamed from: a.e */
/* compiled from: a */
public class PopUpWindowCreateUser {
    private static final String aof = "createUser.xml";

    /* renamed from: kj */
    public final IAddonProperties f6673kj;
    private final String aoh;
    private final String aoq;
    /* renamed from: qk */
    public JLabel f6674qk;
    private InternalFrame aog;
    private PasswordField aoi;
    private PasswordField aoj;
    private TextField aok;
    private JSpinner aol;
    private JSpinner aom;
    private JSpinner aon;
    private TextField aoo;
    private JCheckBox aop;
    private HashMap<String, TextArea> aor;
    private JRadioButton aos;
    private JRadioButton aot;
    private C3997xt aou;
    private WrapRunnable aov = new C2321a(this, (C2321a) null);
    private JButton aow;
    private JButton aox;
    /* access modifiers changed from: private */
    private JButton aoy;
    /* access modifiers changed from: private */
    private JCheckBox aoz;

    public PopUpWindowCreateUser(IAddonProperties vWVar, String str, String str2) {
        this.f6673kj = vWVar;
        this.aoh = str;
        this.aoq = str2;
        this.aog = vWVar.bHv().mo16792bN(aof);
        m29388oS();
        this.aog.pack();
        this.aog.setModal(true);
        this.aog.center();
    }

    /* renamed from: oS */
    private void m29388oS() {
        this.aoo = this.aog.mo4920ci("username");
        this.aoi = this.aog.mo4915cd("password");
        this.aoj = this.aog.mo4915cd("confirmPassword");
        this.aok = this.aog.mo4920ci("email");
        this.aol = this.aog.mo4915cd("day");
        this.aom = this.aog.mo4915cd("month");
        this.aon = this.aog.mo4915cd("year");
        this.aop = this.aog.mo4915cd("newsletter");
        this.aoz = this.aog.mo4915cd("eula");
        this.aos = this.aog.mo4915cd("male");
        this.aot = this.aog.mo4915cd("female");
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(this.aos);
        buttonGroup.add(this.aot);
        this.aoy = this.aog.mo4913cb("eulaButton");
        this.aoy.addActionListener(new C2322b());
        this.aow = this.aog.mo4913cb("acceptButton");
        this.aow.addActionListener(new C2323c());
        this.aox = this.aog.mo4913cb("denyButton");
        this.aox.addActionListener(new C2324d());
        m29375HZ();
        this.aou = new C3997xt(this.f6673kj.bHv().adz().getImage(String.valueOf(C5378aHa.LOGIN.getPath()) + "loading_icon"));
        this.f6674qk = this.aog.mo4917cf("loadingImage");
        this.f6674qk.setIcon(this.aou);
    }

    /* renamed from: HZ */
    private void m29375HZ() {
        this.aor = new HashMap<>();
        this.aor.put("name", this.aog.mo4915cd("nameError"));
        this.aor.put("email", this.aog.mo4915cd("emailError"));
        this.aor.put("password", this.aog.mo4915cd("passwordError"));
        this.aor.put("birthdate", this.aog.mo4915cd("birthdateError"));
        this.aor.put("gender", this.aog.mo4915cd("genderError"));
    }

    /* renamed from: xK */
    public void showPopUpWindowCreateUser() {
        this.aog.setVisible(true);
        this.aoo.requestFocusInWindow();
    }

    /* renamed from: Ia */
    public void hidePopUpWindowCreateUser() {
        this.aog.setVisible(false);
    }

    /* renamed from: Ib */
    public void mo17937Ib() {
        C2325e eVar = new C2325e("Help page");
        eVar.setDaemon(true);
        eVar.start();
    }

    /* renamed from: Ic */
    public void mo17938Ic() {
        String Ii = checkFieldEmpty();
        if (Ii != null) {
            visibilityMesageFormsUserRegistr(C0939Nn.C0940a.ERROR, Ii);
            return;
        }
        String resultCreateUser = m29379Ig();
        if (resultCreateUser.length() == 0) {
            visibilityMesageFormsUserRegistr(C0939Nn.C0940a.ERROR, this.f6673kj.translate("site-server-offline"));
        } else if (resultCreateUser.equalsIgnoreCase("OK")) {
            visibilityMesageCreateUserSuccess();
        } else {
            visibilityMesageOther(resultCreateUser);
        }
    }

    /* renamed from: Id */
    private String m29376Id() {
        return this.aol.getValue() + "-" + this.aom.getValue() + "-" + this.aon.getValue();
    }

    /* renamed from: aH */
    private void visibilityMesageOther(String str) {
        int indexOf;
        m29377Ie();
        try {
            StringTokenizer stringTokenizer = new StringTokenizer(str, "||");
            while (stringTokenizer.hasMoreTokens()) {
                String nextToken = stringTokenizer.nextToken();
                if (!(nextToken.length() == 0 || (indexOf = nextToken.indexOf(":")) == -1)) {
                    String substring = nextToken.substring(0, indexOf);
                    String substring2 = nextToken.substring(indexOf + 1, nextToken.length());
                    if (this.aor.containsKey(substring)) {
                        TextArea ae = this.aor.get(substring);
                        ae.append(String.valueOf(substring2) + "\n");
                        ComponentManager.getCssHolder(ae).setAttribute("class", "error");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* renamed from: Ie */
    private void m29377Ie() {
        for (TextArea next : this.aor.values()) {
            next.setText("");
            ComponentManager.getCssHolder(next).setAttribute("class", "tip");
        }
    }

    /* renamed from: If */
    private void visibilityMesageCreateUserSuccess() {
        hidePopUpWindowCreateUser();
        this.f6673kj.aVU().mo13975h(new C0592IN(ConversionCampaignAddon.f9842yq));
        visibilityMesageFormsUserRegistr(C0939Nn.C0940a.INFO, this.f6673kj.translate("user-created-msg"));
    }

    /* renamed from: Ig */
    private String m29379Ig() {
        showLoadingBar();
        String resultCreateUser = C1453VN.requestPostArgument(this.aoq, new Object[]{"locale", this.aoh, "external_agent", "true", "name", this.aoo.getText(), "email", this.aok.getText(), "password", new String(this.aoi.getPassword()), "birthdate", m29376Id(), "gender", m29380Ih(), "newsletter", Boolean.valueOf(this.aop.isSelected())});
        hideLoadingBar();
        return resultCreateUser;
    }

    /* renamed from: Ih */
    private String m29380Ih() {
        if (this.aot.isSelected()) {
            return "female";
        }
        return "male";
    }

    /* renamed from: Ii */
    private String checkFieldEmpty() {
        StackJ stack = new StackJ();
        if (this.aoo.getText().isEmpty()) {
            stack.push(this.f6673kj.translate("Login"));
        }
        if (this.aok.getText().isEmpty()) {
            stack.push(this.f6673kj.translate("Email"));
        }
        if (this.aoi.getPassword().length == 0) {
            stack.push(this.f6673kj.translate("Password"));
        }
        if (this.aoj.getPassword().length == 0) {
            stack.push(this.f6673kj.translate("Confirmation"));
        }
        if (!this.aos.isSelected() && !this.aot.isSelected()) {
            stack.push(this.f6673kj.translate("Gender"));
        }
        if (stack.size() > 0) {
            String translate = this.f6673kj.translate("error-following-fields");
            Iterator it = stack.iterator();
            while (true) {
                String str = translate;
                if (!it.hasNext()) {
                    return str;
                }
                translate = String.valueOf(str) + "\n - " + ((String) it.next());
            }
        } else if (!Arrays.equals(this.aoi.getPassword(), this.aoj.getPassword())) {
            return this.f6673kj.translate("error-pass-not-equal");
        } else {
            if (!this.aoz.isSelected()) {
                return this.f6673kj.translate("error-eula-not-accepted");
            }
            return null;
        }
    }

    /* renamed from: Ij */
    private void showLoadingBar() {
        this.aow.setEnabled(false);
        this.aox.setEnabled(false);
        this.f6674qk.setVisible(true);
        this.aou.start();
        this.f6673kj.alf().addTask("UpdateMarketLoadingTask", this.aov, 10);
    }

    /* renamed from: Ik */
    private void hideLoadingBar() {
        this.aow.setEnabled(true);
        this.aox.setEnabled(true);
        this.f6673kj.alf().mo7963a(this.aov);
        this.aou.stop();
        this.f6674qk.setVisible(false);
    }

    /* renamed from: b */
    private void visibilityMesageFormsUserRegistr(C0939Nn.C0940a aVar, String str) {
        ((MessageDialogAddon) this.f6673kj.mo11830U(MessageDialogAddon.class)).mo24306a(aVar, str, this.f6673kj.translate("OK"));
    }

    /* renamed from: a.e$a */
    private class C2321a extends WrapRunnable {
        private C2321a() {
        }

        /* synthetic */ C2321a(PopUpWindowCreateUser eVar, C2321a aVar) {
            this();
        }

        public void run() {
            PopUpWindowCreateUser.this.f6674qk.repaint();
        }
    }

    /* renamed from: a.e$b */
    /* compiled from: a */
    class C2322b implements ActionListener {
        C2322b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            PopUpWindowCreateUser.this.f6673kj.aVU().mo13975h(new C2682iV());
        }
    }

    /* renamed from: a.e$c */
    /* compiled from: a */
    class C2323c implements ActionListener {
        C2323c() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            PopUpWindowCreateUser.this.mo17937Ib();
        }
    }

    /* renamed from: a.e$d */
    /* compiled from: a */
    class C2324d implements ActionListener {
        C2324d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            PopUpWindowCreateUser.this.hidePopUpWindowCreateUser();
        }
    }

    /* renamed from: a.e$e */
    /* compiled from: a */
    class C2325e extends Thread {
        C2325e(String str) {
            super(str);
        }

        public void run() {
            PopUpWindowCreateUser.this.mo17938Ic();
        }
    }
}
