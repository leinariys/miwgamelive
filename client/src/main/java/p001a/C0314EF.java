package p001a;

/* renamed from: a.EF */
/* compiled from: a */
public enum C0314EF {
    STORAGE("Storage"),
    CARGO_HOLD("Cargo Hold"),
    HANGAR("Hangar");

    private String name;

    private C0314EF(String str) {
        this.name = str;
    }

    public String getName() {
        return this.name;
    }

    public String toString() {
        return this.name;
    }
}
