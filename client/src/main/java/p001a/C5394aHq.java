package p001a;

import game.network.message.serializable.C1370Tw;

/* renamed from: a.aHq  reason: case insensitive filesystem */
/* compiled from: a */
public class C5394aHq extends C1370Tw {

    private final Class<?> eQl;
    private final Object hWY;

    public C5394aHq(Object obj, Class<?> cls) {
        this.hWY = obj;
        this.eQl = cls;
    }

    public Object getId() {
        return this.hWY;
    }

    public Class<?> bJj() {
        return this.eQl;
    }
}
