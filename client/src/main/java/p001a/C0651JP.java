package p001a;

import game.network.message.serializable.C2991mi;

/* renamed from: a.JP */
/* compiled from: a */
public enum C0651JP {
    ADD(C2991mi.C2992a.VALUE, false),
    ADD_AT(C2991mi.C2992a.INDEXED_VALUE, false),
    ADD_COLLECTION(C2991mi.C2992a.VALUE, true),
    ADD_COLLECTION_AT(C2991mi.C2992a.INDEXED_VALUE, true),
    REMOVE(C2991mi.C2992a.VALUE, false),
    REMOVE_AT(C2991mi.C2992a.INDEXED_VALUE, false),
    REMOVE_COLLECTION(C2991mi.C2992a.VALUE, true),
    SET(C2991mi.C2992a.INDEXED_VALUE, false),
    CLEAR(C2991mi.C2992a.VOID, false);

    private final C2991mi.C2992a dlR;
    private final boolean dlS;

    private C0651JP(C2991mi.C2992a aVar, boolean z) {
        this.dlR = aVar;
        this.dlS = z;
    }

    public C2991mi.C2992a aZB() {
        return this.dlR;
    }

    public boolean isCollection() {
        return this.dlS;
    }
}
