package p001a;

import game.network.manager.C6546aoy;

@C6546aoy
/* renamed from: a.add  reason: case insensitive filesystem */
/* compiled from: a */
public class C5953add extends Exception {


    public C5953add() {
    }

    public C5953add(String str) {
        super(str);
    }

    public C5953add(Throwable th) {
        super(th);
    }

    public C5953add(String str, Throwable th) {
        super(str, th);
    }
}
