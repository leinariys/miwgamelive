package p001a;

import game.io.IReadExternal;
import game.io.IWriteExternal;
import game.network.exception.CreatListenerChannelException;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aNm  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C5546aNm {


    /* renamed from: Ul */
    public transient C5663aRz f3436Ul;
    transient C5546aNm irv;

    /* renamed from: K */
    public abstract Object mo880K(Object obj);

    /* renamed from: a */
    public abstract C5546aNm mo881a(C5546aNm anm);

    /* renamed from: a */
    public abstract void mo882a(C1616Xf xf, IWriteExternal att);

    /* renamed from: a */
    public abstract void mo883a(C1616Xf xf, ObjectOutput objectOutput);

    /* renamed from: b */
    public abstract void mo885b(C1616Xf xf, IReadExternal vm);

    /* renamed from: b */
    public abstract void mo886b(C1616Xf xf, ObjectInput objectInput);

    public abstract int getType();

    /* renamed from: c */
    public void mo9792c(C5663aRz arz) {
        this.f3436Ul = arz;
    }

    public C5546aNm dgf() {
        throw new CreatListenerChannelException();
    }
}
