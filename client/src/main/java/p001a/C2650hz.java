package p001a;

import game.network.message.serializable.C5512aMe;
import logic.baa.C2961mJ;

import java.io.*;
import java.util.*;
import java.util.zip.GZIPInputStream;

/* renamed from: a.hz */
/* compiled from: a */
public class C2650hz {

    /* renamed from: UK */
    private long f8088UK;

    /* renamed from: UL */
    private long f8089UL;

    /* renamed from: a */
    public Map<Long, C5512aMe> mo19438a(C0390FP fp, String str, String str2, String str3) throws IOException {
        C0233Cv cv = new C0233Cv();
        C0233Cv cv2 = new C0233Cv();
        C0233Cv cv3 = new C0233Cv();
        cv.mo1173a(fp.ate());
        cv2.mo1173a(fp.ate());
        cv3.mo1173a(fp.ate());
        cv2.mo1183b((InputStream) new GZIPInputStream(new BufferedInputStream(new FileInputStream(new File(str2)))));
        cv3.mo1183b((InputStream) new GZIPInputStream(new BufferedInputStream(new FileInputStream(new File(str)))));
        cv.mo1183b((InputStream) new GZIPInputStream(new BufferedInputStream(new FileInputStream(new File(str3)))));
        this.f8089UL = Math.max(cv.aBt(), cv2.aBt());
        cv2.aBs();
        cv.aBs();
        Set<Long> keySet = cv3.aBu().keySet();
        long a = m33029a(cv2.aBu().keySet());
        this.f8088UK = Math.max(m33029a(keySet), Math.max(m33029a(cv.aBu().keySet()), a));
        HashMap hashMap = new HashMap();
        TreeMap treeMap = new TreeMap();
        TreeMap treeMap2 = new TreeMap();
        HashMap hashMap2 = new HashMap();
        TreeMap treeMap3 = new TreeMap();
        TreeMap treeMap4 = new TreeMap();
        for (Map.Entry<Long, C5512aMe> value : cv.aBu().entrySet()) {
            C5512aMe ame = (C5512aMe) value.getValue();
            try {
                if (C2961mJ.class.isAssignableFrom(ame.bUE().getClazz()) && ame.mo10091iU("handle")) {
                    if (((String) ame.get("handle")) != null) {
                        hashMap2.put(Long.valueOf(ame.mo10072Ej()), ame);
                        m33032b(treeMap3, ame.bUE().getClassName());
                    } else {
                        m33032b(treeMap4, ame.bUE().getClassName());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        m33031a((Map<String, Long>) treeMap4, "Live Null Handle Template Objects: ");
        System.out.println("");
        m33031a((Map<String, Long>) treeMap3, "Live Template Objects: ");
        System.out.println("");
        HashMap hashMap3 = new HashMap();
        TreeMap treeMap5 = new TreeMap();
        TreeMap treeMap6 = new TreeMap();
        for (Map.Entry<Long, C5512aMe> value2 : cv2.aBu().entrySet()) {
            C5512aMe ame2 = (C5512aMe) value2.getValue();
            try {
                if (C2961mJ.class.isAssignableFrom(ame2.bUE().getClazz()) && ame2.mo10091iU("handle")) {
                    if (((String) ame2.get("handle")) != null) {
                        hashMap3.put(Long.valueOf(ame2.mo10072Ej()), ame2);
                        m33032b(treeMap5, ame2.bUE().getClassName());
                    } else {
                        m33032b(treeMap6, ame2.bUE().getClassName());
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        m33031a((Map<String, Long>) treeMap6, "Head Null Handle Template Objects: ");
        System.out.println("");
        m33031a((Map<String, Long>) treeMap5, "Head Template Objects: ");
        System.out.println("");
        for (Map.Entry next : cv.aBu().entrySet()) {
            C5512aMe ame3 = (C5512aMe) next.getValue();
            if (hashMap2.keySet().contains(next.getKey())) {
                if (!keySet.contains(next.getKey())) {
                    long j = this.f8088UK + 1;
                    this.f8088UK = j;
                    ame3.mo10093lh(j);
                }
            } else if (!keySet.contains(next.getKey())) {
                long j2 = this.f8088UK + 1;
                this.f8088UK = j2;
                ame3.mo10093lh(j2);
                m33032b(treeMap, ame3.bUE().getClassName());
            } else {
                m33032b(treeMap2, ame3.bUE().getClassName());
            }
            hashMap.put(Long.valueOf(ame3.mo10072Ej()), ame3);
        }
        cv.mo1184j(hashMap);
        m33031a((Map<String, Long>) treeMap2, "Matched Instances: ");
        System.out.println("");
        m33031a((Map<String, Long>) treeMap, "Unmatched Instances: ");
        System.out.println("");
        TreeMap treeMap7 = new TreeMap();
        for (Long next2 : keySet) {
            C5512aMe ame4 = cv2.aBu().get(next2);
            C5512aMe ame5 = cv.aBu().get(next2);
            if ((ame5 == null || !hashMap2.keySet().contains(Long.valueOf(ame5.mo10072Ej()))) && ame4 == null && ame5 != null) {
                m33032b(treeMap7, cv3.aBu().get(next2).bUE().getClassName());
            }
        }
        m33031a((Map<String, Long>) treeMap7, "Deleted Instances (not in HEAD but in LIVE): ");
        HashMap hashMap4 = new HashMap();
        for (Map.Entry<Long, C5512aMe> value3 : cv2.aBu().entrySet()) {
            C5512aMe ame6 = (C5512aMe) value3.getValue();
            if (hashMap2.keySet().contains(Long.valueOf(ame6.mo10072Ej()))) {
                ame6.mo10081c(cv3.aBu().get(Long.valueOf(ame6.mo10072Ej())));
                ame6.mo10083d(ame6);
                ame6.mo10088e(m33030a(cv.aBu().values(), (String) ame6.get("handle")));
                hashMap4.put(Long.valueOf(ame6.mo10072Ej()), ame6);
            } else {
                ame6.mo10081c(cv3.aBu().get(Long.valueOf(ame6.mo10072Ej())));
                ame6.mo10083d(ame6);
                ame6.mo10088e((C5512aMe) hashMap.get(Long.valueOf(ame6.mo10072Ej())));
                hashMap4.put(Long.valueOf(ame6.mo10072Ej()), ame6);
            }
        }
        for (Map.Entry value4 : hashMap.entrySet()) {
            C5512aMe ame7 = (C5512aMe) value4.getValue();
            if (!hashMap2.keySet().contains(Long.valueOf(ame7.mo10072Ej())) && !hashMap4.containsKey(Long.valueOf(ame7.mo10072Ej()))) {
                ame7.mo10081c(cv3.aBu().get(Long.valueOf(ame7.mo10072Ej())));
                ame7.mo10083d(cv2.aBu().get(Long.valueOf(ame7.mo10072Ej())));
                ame7.mo10088e(ame7);
                hashMap4.put(Long.valueOf(ame7.mo10072Ej()), ame7);
            }
        }
        System.out.println("------> mergedScripts.size()=" + hashMap4.size());
        for (C5512aMe ame8 : hashMap4.values()) {
            if (ame8.bav().equals("taikodom.game.script.player.Player")) {
                System.out.println("---> " + ame8.mo10072Ej());
            }
        }
        return hashMap4;
    }

    /* renamed from: a */
    private C5512aMe m33030a(Collection<C5512aMe> collection, String str) {
        if (str == null) {
            return null;
        }
        for (C5512aMe next : collection) {
            try {
                if (C2961mJ.class.isAssignableFrom(next.bUE().getClazz()) && next.mo10091iU("handle") && str.equals(next.mo10094nf("handle"))) {
                    return next;
                }
            } catch (Exception e) {
            }
        }
        return null;
    }

    /* renamed from: a */
    private void m33031a(Map<String, Long> map, String str) {
        System.out.println(str);
        long j = 0;
        for (Map.Entry next : map.entrySet()) {
            j += ((Long) next.getValue()).longValue();
            System.out.printf(" %,8d %s\n", new Object[]{next.getValue(), next.getKey()});
        }
        System.out.printf(" %,8d %s\n", new Object[]{Long.valueOf(j), "total"});
    }

    /* renamed from: b */
    private void m33032b(Map<String, Long> map, String str) {
        Long l = map.get(str);
        if (l == null) {
            map.put(str, 1L);
        } else {
            map.put(str, Long.valueOf(l.longValue() + 1));
        }
    }

    /* renamed from: a */
    private long m33029a(Set<Long> set) {
        long j = 0;
        for (Long next : set) {
            if (next.longValue() > j) {
                j = next.longValue();
            }
        }
        return j;
    }

    /* renamed from: yx */
    public long mo19439yx() {
        return this.f8088UK;
    }

    /* renamed from: yy */
    public long mo19440yy() {
        return this.f8089UL;
    }
}
