package p001a;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/* renamed from: a.IQ */
/* compiled from: a */
public abstract class C0596IQ extends MouseAdapter {
    /* access modifiers changed from: protected */
    /* renamed from: H */
    public abstract void mo2812H(int i, int i2);

    public void mousePressed(MouseEvent mouseEvent) {
        if (mouseEvent.isPopupTrigger()) {
            mo2812H(mouseEvent.getX(), mouseEvent.getY());
        }
    }

    public void mouseReleased(MouseEvent mouseEvent) {
        if (mouseEvent.isPopupTrigger()) {
            mo2812H(mouseEvent.getX(), mouseEvent.getY());
        }
    }
}
