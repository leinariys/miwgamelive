package p001a;

import game.network.message.serializable.C6400amI;
import game.script.Actor;
import game.script.missile.Missile;
import game.script.simulation.Space;
import logic.bbb.C4029yK;

/* renamed from: a.anq  reason: case insensitive filesystem */
/* compiled from: a */
public class C6486anq extends C1714ZM {
    private C0832M gds = new C0832M();

    public C6486anq(Space ea, Actor cr, C4029yK yKVar) {
        super(ea, cr, yKVar);
        Missile aed = (Missile) cr;
        this.gds.mo3863o(aed.mo962VF());
        this.gds.mo3864p(aed.mo963VH());
        this.gds.mo3865q(aed.mo1092rb());
        this.gds.mo3866r(aed.mo1091ra());
        mo2450a((C3735uM) this.gds);
    }

    /* renamed from: a */
    public boolean mo2545a(C0520HN hn) {
        if (clw().cLw().aiD() == hn) {
            return false;
        }
        if (!(hn instanceof C6486anq)) {
            return super.mo2545a(hn);
        }
        if (clw().cLw() != ((C6486anq) hn).clw().cLw()) {
            return true;
        }
        return false;
    }

    public Missile clw() {
        return (Missile) super.aSp();
    }

    public void step(float f) {
        Actor agB = ((Missile) aSp()).agB();
        if (!(agB == null || agB.aiD() == null)) {
            this.gds.mo3860a((C6400amI) agB.aiD().mo2472kQ());
        }
        super.step(f);
    }
}
