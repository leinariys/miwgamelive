package p001a;

import game.network.message.serializable.C1370Tw;

/* renamed from: a.ZL */
/* compiled from: a */
public class C1713ZL extends C1370Tw {

    private final Object content;
    private final Class<?> eQl;

    public C1713ZL(Class<?> cls, Object obj) {
        this.eQl = cls;
        this.content = obj;
    }

    public Object getContent() {
        return this.content;
    }

    public Class<?> bJj() {
        return this.eQl;
    }
}
