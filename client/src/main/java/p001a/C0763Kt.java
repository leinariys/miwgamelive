package p001a;

import logic.thred.LogPrinter;

/* renamed from: a.Kt */
/* compiled from: a */
public class C0763Kt {
    private static final ThreadLocal<C0763Kt> dpR = new C5606aPu();
    private static final LogPrinter logger = LogPrinter.m10275K(C0763Kt.class);
    private final C0988OY dpT;
    private final C1465VZ dpU;
    private final C0215Cg dpV;
    private final C6609aqJ dpW;
    private final C6996ayj dpX;
    private final C4048yb dpY;
    private final C1826aT dpZ;
    private final C0223Co<float[]> dqa;
    public Thread thread;
    private int dpS;

    private C0763Kt() {
        this.dpT = new C0988OY();
        this.dpU = new C1465VZ();
        this.dpV = new C0215Cg();
        this.dpW = new C6609aqJ();
        this.dpX = new C6996ayj();
        this.dpY = new C4048yb();
        this.dpZ = new C1826aT();
        this.dqa = new C0223Co<>(Float.TYPE);
    }

    /* synthetic */ C0763Kt(C0763Kt kt) {
        this();
    }

    public static C0763Kt bcE() {
        return dpR.get();
    }

    private C0763Kt bcC() {
        Thread currentThread = Thread.currentThread();
        if (this.thread == currentThread) {
            return this;
        }
        if (this.dpS < 10) {
            if (logger.isDebugEnabled()) {
                logger.debug("Using wrong stack for this thread (" + currentThread + ") should be (" + this.thread + ") ", new Throwable());
            }
            this.dpS++;
        }
        return bcE();
    }

    public C0763Kt bcD() {
        return this.thread == Thread.currentThread() ? this : bcE();
    }

    public void bcF() {
        bcH().push();
        bcJ().push();
        bcK().push();
        bcL().push();
    }

    public void bcG() {
        bcH().pop();
        bcJ().pop();
        bcK().pop();
        bcL().pop();
    }

    public C0988OY bcH() {
        return bcC().dpT;
    }

    public C1465VZ bcI() {
        return bcC().dpU;
    }

    public C0215Cg bcJ() {
        return bcC().dpV;
    }

    public C6609aqJ bcK() {
        return bcC().dpW;
    }

    public C6996ayj bcL() {
        return bcC().dpX;
    }

    public C4048yb bcM() {
        return bcC().dpY;
    }

    public C1826aT bcN() {
        return bcC().dpZ;
    }

    public C0223Co<float[]> bcO() {
        return bcC().dqa;
    }
}
