package p001a;

import gnu.trove.THashSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/* renamed from: a.aqU  reason: case insensitive filesystem */
/* compiled from: a */
public class C6620aqU<T> implements Iterable<T> {
    Collection<T> gqZ;
    ArrayList<T> gra = new ArrayList<>();
    THashSet<T> grb = new THashSet<>();
    ArrayList<T> removes = new ArrayList<>();

    public C6620aqU(Collection<T> collection) {
        this.gqZ = collection;
    }

    public void add(T t) {
        this.gra.add(t);
    }

    public void remove(T t) {
        this.removes.add(t);
    }

    public Set<T> crD() {
        return this.grb;
    }

    /* renamed from: ar */
    public void mo15585ar(T t) {
        this.grb.add(t);
    }

    public void crE() {
        this.gqZ.removeAll(this.removes);
        this.removes.clear();
        this.gqZ.addAll(this.gra);
        this.gra.clear();
    }

    public Iterator<T> iterator() {
        return this.gqZ.iterator();
    }
}
