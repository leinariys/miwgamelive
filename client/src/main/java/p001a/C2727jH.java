package p001a;

import gnu.trove.TLongArrayList;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: a.jH */
/* compiled from: a */
public class C2727jH {
    ThreadLocal<C2731d> iLN = new C3214pI(this);
    Map<String, C2728a> iLO = new ConcurrentHashMap();

    public void start(String str) {
        long nanoTime = System.nanoTime();
        C2731d dVar = this.iLN.get();
        C2728a aVar = this.iLO.get(str);
        if (aVar == null) {
            aVar = new C2728a(str);
            this.iLO.put(str, aVar);
        }
        dVar.glO.add(aVar);
        TLongArrayList tLongArrayList = dVar.glN;
        tLongArrayList.add(nanoTime);
        tLongArrayList.add(0);
    }

    public void end(String str) {
        C2731d dVar = this.iLN.get();
        TLongArrayList tLongArrayList = dVar.glN;
        long remove = tLongArrayList.remove(tLongArrayList.size() - 1);
        long remove2 = tLongArrayList.remove(tLongArrayList.size() - 1);
        ArrayList<C2728a> arrayList = dVar.glO;
        C2728a remove3 = arrayList.remove(arrayList.size() - 1);
        long nanoTime = System.nanoTime() - remove2;
        long j = nanoTime - remove;
        if (!str.equals(remove3.name)) {
            throw new IllegalArgumentException("Fix profiling please: " + str + "<>" + remove3.name);
        }
        remove3.aoc.addAndGet(j);
        remove3.aob.incrementAndGet();
        if (tLongArrayList.size() > 0) {
            int size = tLongArrayList.size() - 1;
            tLongArrayList.set(size, tLongArrayList.get(size) + nanoTime);
        }
    }

    public void dtZ() {
        long j;
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.iLO.values());
        Collections.sort(arrayList, new C2729b());
        System.out.printf("%60s %20s %20s %10s %8s\n", new Object[]{"measure", "avg time (ns)", "acc time (ns)", "call count", "%"});
        long j2 = 0;
        Iterator it = arrayList.iterator();
        while (true) {
            j = j2;
            if (!it.hasNext()) {
                break;
            }
            j2 = ((C2728a) it.next()).aoc.get() + j;
        }
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            C2728a aVar = (C2728a) it2.next();
            System.out.printf("%60s %,20d %,20d %,10d %8.2f\n", new Object[]{aVar.name, Long.valueOf(Math.round(((double) aVar.aoc.get()) / ((double) aVar.aob.get()))), Long.valueOf(aVar.aoc.get()), Long.valueOf(aVar.aob.get()), Double.valueOf((100.0d * ((double) aVar.aoc.get())) / ((double) j))});
        }
        System.out.printf("%60s %20s %,20d %10s %8.2f\n", new Object[]{"total", "", Long.valueOf(j), "", Float.valueOf(100.0f)});
    }

    /* renamed from: a.jH$c */
    /* compiled from: a */
    public static class C2730c {
        C2728a dnq;
        long start;
    }

    /* renamed from: a.jH$d */
    /* compiled from: a */
    public static class C2731d {
        TLongArrayList glN = new TLongArrayList();
        ArrayList<C2728a> glO = new ArrayList<>();
    }

    /* renamed from: a.jH$a */
    public static class C2728a {
        AtomicLong aob = new AtomicLong();
        AtomicLong aoc = new AtomicLong();
        String name;

        public C2728a(String str) {
            this.name = str;
        }
    }

    /* renamed from: a.jH$b */
    /* compiled from: a */
    class C2729b implements Comparator<C2728a> {
        C2729b() {
        }

        /* renamed from: a */
        public int compare(C2728a aVar, C2728a aVar2) {
            long j = aVar.aoc.get() - aVar2.aoc.get();
            return -(j < 0 ? -1 : j > 0 ? 1 : aVar.name.compareTo(aVar2.name));
        }
    }
}
