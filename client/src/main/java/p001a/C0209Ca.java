package p001a;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.Ca */
/* compiled from: a */
public final class C0209Ca {
    public static final C0209Ca cuO = new C0209Ca("GrannyNoSkeletonLOD", grannyJNI.GrannyNoSkeletonLOD_get());
    public static final C0209Ca cuP = new C0209Ca("GrannyEstimatedLOD", grannyJNI.GrannyEstimatedLOD_get());
    public static final C0209Ca cuQ = new C0209Ca("GrannyMeasuredLOD", grannyJNI.GrannyMeasuredLOD_get());
    private static C0209Ca[] cuR = {cuO, cuP, cuQ};

    /* renamed from: pF */
    private static int f349pF = 0;

    /* renamed from: pG */
    private final int f350pG;

    /* renamed from: pH */
    private final String f351pH;

    private C0209Ca(String str) {
        this.f351pH = str;
        int i = f349pF;
        f349pF = i + 1;
        this.f350pG = i;
    }

    private C0209Ca(String str, int i) {
        this.f351pH = str;
        this.f350pG = i;
        f349pF = i + 1;
    }

    private C0209Ca(String str, C0209Ca ca) {
        this.f351pH = str;
        this.f350pG = ca.f350pG;
        f349pF = this.f350pG + 1;
    }

    /* renamed from: gz */
    public static C0209Ca m1810gz(int i) {
        if (i < cuR.length && i >= 0 && cuR[i].f350pG == i) {
            return cuR[i];
        }
        for (int i2 = 0; i2 < cuR.length; i2++) {
            if (cuR[i2].f350pG == i) {
                return cuR[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C0209Ca.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f350pG;
    }

    public String toString() {
        return this.f351pH;
    }
}
