package p001a;

/* renamed from: a.aPm  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5598aPm {
    /* renamed from: aN */
    <T> Object mo10176aN(Class<T> cls);

    /* renamed from: aO */
    <T> int mo10177aO(Class<T> cls);

    int getMaximumCacheSize();

    void setMaximumCacheSize(int i);

    boolean isEmpty();

    <T> void put(Class<T> cls, Object obj);
}
