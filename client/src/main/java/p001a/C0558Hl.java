package p001a;

import logic.res.LoaderTrail;

import java.util.Comparator;

/* renamed from: a.Hl */
/* compiled from: a */
public class C0558Hl implements Comparator<aGU> {
    final /* synthetic */ LoaderTrail daN;

    public C0558Hl(LoaderTrail glVar) {
        this.daN = glVar;
    }

    /* renamed from: a */
    public int compare(aGU agu, aGU agu2) {
        return agu2.priority - agu.priority;
    }
}
