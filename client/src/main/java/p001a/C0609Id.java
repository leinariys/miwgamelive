package p001a;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Vector4fWrap;
import logic.res.scene.C1891ah;
import logic.res.scene.C2036bO;
import logic.res.scene.aBN;

import javax.vecmath.Tuple3f;

/* renamed from: a.Id */
/* compiled from: a */
public final class C0609Id {
    private C0609Id() {
    }

    /* renamed from: c */
    public static <T extends C2036bO> T m5426c(C2036bO bOVar, Class<T> cls) {
        T[] Ix = bOVar.mo15350Ix();
        if (Ix.length == 0) {
            return null;
        }
        T t = Ix[0];
        if (cls.isInstance(t)) {
            return t;
        }
        return m5426c(t, cls);
    }

    /* renamed from: a */
    public static BoundingBox m5425a(C2036bO bOVar, Matrix4fWrap ajk) {
        if (bOVar instanceof aBN) {
            return m5424a((aBN) bOVar, ajk);
        }
        throw new IllegalArgumentException("Invalid object type: " + bOVar.getClass().getName());
    }

    /* renamed from: a */
    public static BoundingBox m5424a(aBN abn, Matrix4fWrap ajk) {
        C1891ah lc = abn.mo7883lc();
        if (lc == null) {
            throw new IllegalArgumentException("Object does not have a mesh: " + abn.getName());
        } else if (lc.mo13487dC() == 0) {
            throw new IllegalArgumentException("Object mesh does not have primitives: " + abn.getName());
        } else {
            BoundingBox boundingBox = new BoundingBox();
            for (int i = 0; ((long) i) < lc.mo13487dC(); i++) {
                boundingBox.addPoint(ajk.mo14004c((Tuple3f) lc.mo13484d((long) i)));
            }
            return boundingBox;
        }
    }

    /* renamed from: f */
    public static Matrix4fWrap m5427f(C2036bO bOVar) {
        if (bOVar instanceof aBN) {
            return m5423a((aBN) bOVar);
        }
        throw new IllegalArgumentException("Invalid object type: " + bOVar.getClass().getName());
    }

    /* renamed from: a */
    public static Matrix4fWrap m5423a(aBN abn) {
        C1891ah lc = abn.mo7883lc();
        if (lc == null) {
            throw new IllegalArgumentException("Object does not have a mesh: " + abn.getName());
        } else if (lc.mo13487dC() == 0) {
            throw new IllegalArgumentException("Object mesh does not have primitives: " + abn.getName());
        } else {
            Vec3f vec3f = new Vec3f();
            Vec3f vec3f2 = new Vec3f();
            Vec3f vec3f3 = new Vec3f();
            for (int i = 0; ((long) i) < lc.mo13487dC(); i++) {
                vec3f.mo23515p((Tuple3f) lc.mo13484d((long) i));
                vec3f2.mo23515p((Tuple3f) lc.mo13490e((long) i));
                Vector4fWrap f = lc.mo13491f((long) i);
                vec3f3.mo23467D(f.x, f.y, f.z);
            }
            vec3f.scale(1.0f / ((float) lc.mo13487dC()));
            vec3f2.scale(1.0f / ((float) lc.mo13487dC()));
            vec3f2.dfP();
            vec3f3.scale(1.0f / ((float) lc.mo13487dC()));
            vec3f3.dfP();
            Vec3f b = vec3f3.mo23476b(vec3f2);
            Matrix4fWrap ajk = new Matrix4fWrap();
            ajk.setX(vec3f3.x, vec3f3.y, vec3f3.z);
            ajk.setY(vec3f2.x, vec3f2.y, vec3f2.z);
            ajk.setZ(b.x, b.y, b.z);
            ajk.setTranslation(vec3f);
            if (ajk.mo14042jK(1.0E-5f)) {
                return ajk;
            }
            throw new IllegalArgumentException("Object transform is not orthonormal: " + abn.getName());
        }
    }
}
