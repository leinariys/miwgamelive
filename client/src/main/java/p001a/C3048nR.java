package p001a;

import java.io.*;
import java.util.UUID;

/* renamed from: a.nR */
/* compiled from: a */
public class C3048nR {
    /* renamed from: aT */
    public static String m36075aT(String str) {
        return m36077b(new File(str));
    }

    /* renamed from: b */
    public static String m36077b(File file) {
        try {
            return new BufferedReader(new InputStreamReader(new FileInputStream(file))).readLine();
        } catch (Exception e) {
            return m36078c(file);
        }
    }

    /* renamed from: aU */
    public static String m36076aU(String str) {
        return m36078c(new File(str));
    }

    /* renamed from: c */
    public static String m36078c(File file) {
        String RI = m36074RI();
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
        bufferedWriter.write(RI);
        bufferedWriter.close();
        return RI;
    }

    /* renamed from: RI */
    public static String m36074RI() {
        return UUID.randomUUID().toString();
    }
}
