package p001a;

import game.engine.C0507HC;
import game.engine.C3658tN;
import game.engine.DataGameEvent;
import game.engine.IEngineGame;
import game.script.PlayerController;
import game.script.Taikodom;
import game.script.TaikodomObject;
import game.script.player.Player;
import game.script.player.User;
import logic.C6245ajJ;
import logic.render.GUIModule;
import logic.render.IEngineGraphics;
import logic.res.ConfigManager;
import logic.res.ILoaderTrail;
import logic.res.code.SoftTimer;
import taikodom.render.loader.provider.FilePath;

import javax.swing.text.ViewFactory;

/* renamed from: a.aGf  reason: case insensitive filesystem */
/* compiled from: a */
public class EngineGameServer implements IEngineGame {
    private SoftTimer dej;
    private FilePath rootDirectory;
    private C3658tN hOu;

    /* renamed from: ng */
    private FilePath renderRootpath;

    public EngineGameServer(FilePath rootDirectory, FilePath renderRootpath) {
        this.rootDirectory = rootDirectory;
        this.renderRootpath = renderRootpath;
    }

    public ConfigManager getConfigManager() {
        throw new IllegalAccessError("Not for server");
    }

    public DataGameEvent bhc() {
        throw new IllegalAccessError("Not for server");
    }

    public GUIModule getGuiModule() {
        throw new IllegalAccessError("Not for server");
    }

    /* renamed from: dL */
    public Player mo4089dL() {
        throw new IllegalAccessError("Not for server");
    }

    public PlayerController alb() {
        throw new IllegalAccessError("Not for server");
    }

    public IEngineGraphics getEngineGraphics() {
        return null;
    }

    public Taikodom getTaikodom() {
        throw new IllegalAccessError("Not for server");
    }

    public User getUser() {
        throw new IllegalAccessError("Not for server");
    }

    public ViewFactory getViewFactory() {
        throw new IllegalAccessError("Not for server");
    }

    /* renamed from: a */
    public void mo4066a(SoftTimer abg) {
        throw new UnsupportedOperationException("This method should not be called serverside");
    }

    public SoftTimer alf() {
        throw new UnsupportedOperationException("This method should not be called serverside");
    }

    public void cleanUp() {
        throw new IllegalAccessError("Not for server");
    }

    /* renamed from: a */
    public void mo9090a(aPP app) {
        throw new IllegalAccessError("Not for server");
    }

    /* renamed from: a */
    public void mo9089a(TaikodomObject fc) {
        throw new IllegalAccessError("Not for server");
    }

    /* renamed from: b */
    public void mo9092b(aPP app) {
        throw new IllegalAccessError("Not for server");
    }

    public void exit() {
    }

    public void bha() {
    }

    public boolean isVerbose() {
        return false;
    }

    public void bgY() {
    }

    public void bgZ() {
    }

    public void cZP() {
    }

    public void disconnect() {
    }

    public void cZQ() {
    }

    public void cZR() {
    }

    public void cZS() {
    }

    public ILoaderTrail getLoaderTrail() {
        return null;
    }

    /* renamed from: a */
    public void mo4065a(ILoaderTrail qu) {
    }

    /* renamed from: eK */
    public boolean mo9098eK(String str) {
        return false;
    }

    /* renamed from: y */
    public <T> T mo9100y(Class<T> cls) {
        return null;
    }

    public C6245ajJ getEventManager() {
        throw new IllegalAccessError("Illegal at server");
    }

    public C3658tN bgX() {
        return this.hOu;
    }

    /* renamed from: a */
    public void mo9091a(C3658tN tNVar) {
        this.hOu = tNVar;
    }

    public void initAllAddonWaitPlayer() {
    }

    public boolean bhf() {
        return false;
    }

    public boolean bhg() {
        throw new IllegalAccessError("Not for server");
    }

    public C0507HC bhh() {
        throw new IllegalAccessError("Illegal at server");
    }

    public FilePath getRootPathRender() {
        return this.renderRootpath;
    }

    public FilePath getRootPath() {
        return this.rootDirectory;
    }

    /* renamed from: e */
    public void mo4091e(Runnable runnable) {
        throw new IllegalAccessError("Illegal at server");
    }
}
