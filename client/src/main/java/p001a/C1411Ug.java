package p001a;

import logic.res.KeyCode;
import sdljava.event.SDLMod;

import java.util.HashMap;
import java.util.Map;

/* renamed from: a.Ug */
/* compiled from: a */
public class C1411Ug {
    private static final Map<Integer, Integer> emY = new HashMap();
    private static final Map<Integer, Integer> emZ = new HashMap();

    static {
        m10324Q(8, 8);
        m10324Q(9, 9);
        m10324Q(12, 12);
        m10324Q(13, 13);
        m10324Q(19, 19);
        m10324Q(27, 27);
        m10324Q(32, 32);
        m10324Q(33, 33);
        m10324Q(34, 34);
        m10324Q(35, 35);
        m10324Q(36, 36);
        m10324Q(38, 38);
        m10324Q(39, 39);
        m10324Q(40, 40);
        m10324Q(41, 41);
        m10324Q(42, 42);
        m10324Q(43, 43);
        m10324Q(44, 44);
        m10324Q(45, 45);
        m10324Q(46, 46);
        m10324Q(47, 47);
        m10324Q(48, 48);
        m10324Q(49, 49);
        m10324Q(50, 50);
        m10324Q(51, 51);
        m10324Q(52, 52);
        m10324Q(53, 53);
        m10324Q(54, 54);
        m10324Q(55, 55);
        m10324Q(56, 56);
        m10324Q(57, 57);
        m10324Q(58, 58);
        m10324Q(59, 59);
        m10324Q(60, 60);
        m10324Q(61, 61);
        m10324Q(62, 62);
        m10324Q(63, 63);
        m10324Q(64, 64);
        m10324Q(91, 91);
        m10324Q(92, 92);
        m10324Q(93, 93);
        m10324Q(94, 94);
        m10324Q(95, 95);
        m10324Q(96, 96);
        m10324Q(97, 97);
        m10324Q(98, 98);
        m10324Q(99, 99);
        m10324Q(100, 100);
        m10324Q(101, 101);
        m10324Q(102, 102);
        m10324Q(103, 103);
        m10324Q(104, 104);
        m10324Q(105, 105);
        m10324Q(106, 106);
        m10324Q(107, 107);
        m10324Q(108, 108);
        m10324Q(109, 109);
        m10324Q(110, 110);
        m10324Q(111, 111);
        m10324Q(112, 112);
        m10324Q(113, 113);
        m10324Q(114, 114);
        m10324Q(115, 115);
        m10324Q(116, 116);
        m10324Q(117, 117);
        m10324Q(118, 118);
        m10324Q(119, 119);
        m10324Q(120, 120);
        m10324Q(121, 121);
        m10324Q(122, 122);
        m10324Q(127, 127);
        m10324Q(160, 160);
        m10324Q(161, 161);
        m10324Q(162, 162);
        m10324Q(163, 163);
        m10324Q(164, 164);
        m10324Q(165, 165);
        m10324Q(166, 166);
        m10324Q(167, 167);
        m10324Q(168, 168);
        m10324Q(169, 169);
        m10324Q(170, 170);
        m10324Q(171, 171);
        m10324Q(172, 172);
        m10324Q(173, 173);
        m10324Q(174, 174);
        m10324Q(175, 175);
        m10324Q(176, 176);
        m10324Q(177, 177);
        m10324Q(178, 178);
        m10324Q(179, 179);
        m10324Q(180, 180);
        m10324Q(181, 181);
        m10324Q(182, 182);
        m10324Q(183, 183);
        m10324Q(184, 184);
        m10324Q(185, 185);
        m10324Q(KeyCode.cso, KeyCode.cso);
        m10324Q(187, 187);
        m10324Q(188, 188);
        m10324Q(189, 189);
        m10324Q(190, 190);
        m10324Q(191, 191);
        m10324Q(192, 192);
        m10324Q(193, 193);
        m10324Q(194, 194);
        m10324Q(195, 195);
        m10324Q(196, 196);
        m10324Q(197, 197);
        m10324Q(198, 198);
        m10324Q(199, 199);
        m10324Q(200, 200);
        m10324Q(201, 201);
        m10324Q(202, 202);
        m10324Q(KeyCode.csG, KeyCode.csG);
        m10324Q(KeyCode.csH, KeyCode.csH);
        m10324Q(KeyCode.csI, KeyCode.csI);
        m10324Q(KeyCode.csJ, KeyCode.csJ);
        m10324Q(KeyCode.csK, KeyCode.csK);
        m10324Q(KeyCode.csL, KeyCode.csL);
        m10324Q(KeyCode.csM, KeyCode.csM);
        m10324Q(KeyCode.csN, KeyCode.csN);
        m10324Q(KeyCode.csO, KeyCode.csO);
        m10324Q(KeyCode.csP, KeyCode.csP);
        m10324Q(KeyCode.csQ, KeyCode.csQ);
        m10324Q(KeyCode.csR, KeyCode.csR);
        m10324Q(KeyCode.csS, KeyCode.csS);
        m10324Q(KeyCode.csT, KeyCode.csT);
        m10324Q(KeyCode.csU, KeyCode.csU);
        m10324Q(KeyCode.csV, KeyCode.csV);
        m10324Q(KeyCode.csW, KeyCode.csW);
        m10324Q(KeyCode.csX, KeyCode.csX);
        m10324Q(KeyCode.csY, KeyCode.csY);
        m10324Q(KeyCode.csZ, KeyCode.csZ);
        m10324Q(KeyCode.cta, KeyCode.cta);
        m10324Q(KeyCode.ctb, KeyCode.ctb);
        m10324Q(KeyCode.ctc, KeyCode.ctc);
        m10324Q(KeyCode.ctd, KeyCode.ctd);
        m10324Q(KeyCode.cte, KeyCode.cte);
        m10324Q(KeyCode.ctf, KeyCode.ctf);
        m10324Q(KeyCode.ctg, KeyCode.ctg);
        m10324Q(KeyCode.cth, KeyCode.cth);
        m10324Q(KeyCode.cti, KeyCode.cti);
        m10324Q(KeyCode.ctj, KeyCode.ctj);
        m10324Q(KeyCode.ctk, KeyCode.ctk);
        m10324Q(KeyCode.ctl, KeyCode.ctl);
        m10324Q(KeyCode.ctm, KeyCode.ctm);
        m10324Q(KeyCode.ctn, KeyCode.ctn);
        m10324Q(KeyCode.cto, KeyCode.cto);
        m10324Q(KeyCode.ctp, KeyCode.ctp);
        m10324Q(KeyCode.ctq, KeyCode.ctq);
        m10324Q(240, 240);
        m10324Q(KeyCode.cts, KeyCode.cts);
        m10324Q(242, 242);
        m10324Q(243, 243);
        m10324Q(244, 244);
        m10324Q(KeyCode.ctw, KeyCode.ctw);
        m10324Q(KeyCode.ctx, KeyCode.ctx);
        m10324Q(KeyCode.cty, KeyCode.cty);
        m10324Q(KeyCode.ctz, KeyCode.ctz);
        m10324Q(KeyCode.ctA, KeyCode.ctA);
        m10324Q(250, 250);
        m10324Q(KeyCode.ctC, KeyCode.ctC);
        m10324Q(KeyCode.ctD, KeyCode.ctD);
        m10324Q(KeyCode.ctE, KeyCode.ctE);
        m10324Q(254, 254);
        m10324Q(255, 255);
        m10324Q(256, 256);
        m10324Q(KeyCode.ctI, KeyCode.ctI);
        m10324Q(KeyCode.ctJ, KeyCode.ctJ);
        m10324Q(KeyCode.ctK, KeyCode.ctK);
        m10324Q(KeyCode.ctL, KeyCode.ctL);
        m10324Q(KeyCode.ctM, KeyCode.ctM);
        m10324Q(KeyCode.ctN, KeyCode.ctN);
        m10324Q(KeyCode.ctO, KeyCode.ctO);
        m10324Q(KeyCode.ctP, KeyCode.ctP);
        m10324Q(KeyCode.ctQ, KeyCode.ctQ);
        m10324Q(KeyCode.ctR, KeyCode.ctR);
        m10324Q(KeyCode.ctS, KeyCode.ctS);
        m10324Q(KeyCode.ctT, KeyCode.ctT);
        m10324Q(KeyCode.ctU, KeyCode.ctU);
        m10324Q(KeyCode.ctV, KeyCode.ctV);
        m10324Q(KeyCode.ctW, KeyCode.ctW);
        m10324Q(KeyCode.ctX, KeyCode.ctX);
        m10324Q(KeyCode.f209UP, KeyCode.f209UP);
        m10324Q(KeyCode.DOWN, KeyCode.DOWN);
        m10324Q(KeyCode.RIGHT, KeyCode.RIGHT);
        m10324Q(KeyCode.LEFT, KeyCode.LEFT);
        m10324Q(KeyCode.INSERT, KeyCode.INSERT);
        m10324Q(KeyCode.HOME, KeyCode.HOME);
        m10324Q(KeyCode.END, KeyCode.END);
        m10324Q(KeyCode.ctY, KeyCode.ctY);
        m10324Q(KeyCode.ctZ, KeyCode.ctZ);
        m10324Q(KeyCode.f200F1, KeyCode.f200F1);
        m10324Q(KeyCode.f201F2, KeyCode.f201F2);
        m10324Q(KeyCode.f202F3, KeyCode.f202F3);
        m10324Q(KeyCode.f203F4, KeyCode.f203F4);
        m10324Q(KeyCode.f204F5, KeyCode.f204F5);
        m10324Q(KeyCode.f205F6, KeyCode.f205F6);
        m10324Q(KeyCode.f206F7, KeyCode.f206F7);
        m10324Q(KeyCode.f207F8, KeyCode.f207F8);
        m10324Q(KeyCode.f208F9, KeyCode.f208F9);
        m10324Q(KeyCode.F10, KeyCode.F10);
        m10324Q(KeyCode.F11, KeyCode.F11);
        m10324Q(KeyCode.F12, KeyCode.F12);
        m10324Q(KeyCode.F13, KeyCode.F13);
        m10324Q(KeyCode.F14, KeyCode.F14);
        m10324Q(KeyCode.F15, KeyCode.F15);
        m10324Q(KeyCode.cua, KeyCode.cua);
        m10324Q(KeyCode.cub, KeyCode.cub);
        m10324Q(KeyCode.cuc, KeyCode.cuc);
        m10324Q(KeyCode.RSHIFT, KeyCode.RSHIFT);
        m10324Q(KeyCode.LSHIFT, KeyCode.LSHIFT);
        m10324Q(KeyCode.cud, KeyCode.cud);
        m10324Q(KeyCode.cue, KeyCode.cue);
        m10324Q(KeyCode.cuf, KeyCode.cuf);
        m10324Q(KeyCode.cug, KeyCode.cug);
        m10324Q(KeyCode.cuh, KeyCode.cuh);
        m10324Q(KeyCode.cui, KeyCode.cui);
        m10324Q(KeyCode.cuj, KeyCode.cuj);
        m10324Q(KeyCode.cuk, KeyCode.cuk);
        m10324Q(KeyCode.cul, KeyCode.cul);
        m10324Q(KeyCode.cum, KeyCode.cum);
        m10324Q(KeyCode.HELP, KeyCode.HELP);
        m10324Q(KeyCode.cun, KeyCode.cun);
        m10324Q(KeyCode.cuo, KeyCode.cuo);
        m10324Q(KeyCode.BREAK, KeyCode.BREAK);
        m10324Q(KeyCode.MENU, KeyCode.MENU);
        m10324Q(KeyCode.cup, KeyCode.cup);
        m10324Q(KeyCode.cuq, KeyCode.cuq);
    }

    /* renamed from: Q */
    private static void m10324Q(int i, int i2) {
        emY.put(Integer.valueOf(i2), Integer.valueOf(i));
        emZ.put(Integer.valueOf(i), Integer.valueOf(i2));
    }

    /* renamed from: nz */
    public static int m10327nz(int i) {
        Integer num = emY.get(Integer.valueOf(i));
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    /* renamed from: nA */
    public static int m10326nA(int i) {
        Integer num = emZ.get(Integer.valueOf(i));
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    /* renamed from: a */
    public static int m10325a(SDLMod sDLMod) {
        int i = 0;
        if (sDLMod.leftAlt()) {
            i = 256;
        }
        if (sDLMod.rightAlt()) {
            i |= 512;
        }
        if (sDLMod.leftCtrl()) {
            i |= 64;
        }
        if (sDLMod.rightCtrl()) {
            i |= 128;
        }
        if (sDLMod.leftShift()) {
            i |= 1;
        }
        if (sDLMod.rightShift()) {
            return i | 2;
        }
        return i;
    }
}
