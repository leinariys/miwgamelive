package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;
import game.geometry.Quat4fWrap;
import logic.abc.C0802Lc;

import javax.vecmath.Quat4f;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.ajC  reason: case insensitive filesystem */
/* compiled from: a */
public class C6238ajC extends ayY {
    /* renamed from: jR */
    static final /* synthetic */ boolean f4683jR;
    private static final float fRt = 1.5707964f;
    private static int dxB = 0;

    static {
        boolean z;
        if (!C6238ajC.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        f4683jR = z;
    }

    private final Vec3f angularVelocity;
    private final Matrix3fWrap cZf;
    private final Vec3f fRu;
    private final Vec3f fRv;
    private final Vec3f fRw;
    private final Vec3f fRx;
    private final List<C1083Pr> fRz;
    private final Vec3f fdB;
    public int fRA;
    public int fRB;
    public int fRC;
    private float cZe;
    private float cZg;
    private C6863avD fRy;
    private float fgF;
    private float fgG;
    private float fgH;
    private float fgI;
    private boolean fgJ;
    private float fgK;
    private float fgL;
    private float fgM;
    private float fgN;

    public C6238ajC(C5897acZ acz) {
        this.cZf = new Matrix3fWrap();
        this.fRu = new Vec3f();
        this.angularVelocity = new Vec3f();
        this.fdB = new Vec3f();
        this.fRv = new Vec3f();
        this.fRw = new Vec3f();
        this.fRx = new Vec3f();
        this.fRz = new ArrayList();
        m22754a(acz);
    }

    public C6238ajC(float f, C6863avD avd, C0802Lc lc) {
        this(f, avd, lc, C0128Bb.dMU);
    }

    public C6238ajC(float f, C6863avD avd, C0802Lc lc, Vec3f vec3f) {
        this.cZf = new Matrix3fWrap();
        this.fRu = new Vec3f();
        this.angularVelocity = new Vec3f();
        this.fdB = new Vec3f();
        this.fRv = new Vec3f();
        this.fRw = new Vec3f();
        this.fRx = new Vec3f();
        this.fRz = new ArrayList();
        m22754a(new C5897acZ(f, avd, lc, vec3f));
    }

    /* renamed from: c */
    public static C6238ajC m22755c(ayY ayy) {
        return (C6238ajC) ayy.cFe();
    }

    /* renamed from: a */
    private void m22754a(C5897acZ acz) {
        this.fRu.set(0.0f, 0.0f, 0.0f);
        this.angularVelocity.set(0.0f, 0.0f, 0.0f);
        this.cZg = 1.0f;
        this.fdB.set(0.0f, 0.0f, 0.0f);
        this.fRw.set(0.0f, 0.0f, 0.0f);
        this.fRx.set(0.0f, 0.0f, 0.0f);
        this.fgF = 0.0f;
        this.fgG = 0.5f;
        this.fgH = acz.fgH;
        this.fgI = acz.fgI;
        this.fRy = acz.fgB;
        this.fRA = 0;
        this.fRB = 0;
        this.fgJ = acz.fgJ;
        this.fgL = acz.fgL;
        this.fgM = acz.fgM;
        this.fgN = acz.fgN;
        if (this.fRy != null) {
            this.fRy.mo2960e(this.deL);
        } else {
            this.deL.mo22947a(acz.fgC);
        }
        this.gXJ.mo22947a(this.deL);
        this.gXK.set(0.0f, 0.0f, 0.0f);
        this.gXL.set(0.0f, 0.0f, 0.0f);
        this.dMd = acz.dMd;
        this.dMe = acz.dMe;
        this.fgD = acz.fgD;
        int i = dxB;
        dxB = i + 1;
        this.fRC = i;
        this.gXT = this;
        mo13909b(acz.mass, acz.fgE);
        mo13940t(acz.fgF, acz.fgG);
        cer();
    }

    public void destroy() {
        if (!f4683jR && this.fRz.size() != 0) {
            throw new AssertionError();
        }
    }

    /* renamed from: h */
    public void mo13930h(C3978xf xfVar) {
        mo13931i(xfVar);
    }

    /* renamed from: a */
    public void mo13898a(float f, C3978xf xfVar) {
        C0503Gz.m3561a(this.deL, this.fRu, this.angularVelocity, f, xfVar);
    }

    /* renamed from: iX */
    public void mo13932iX(float f) {
        if (f != 0.0f) {
            if (cey() != null) {
                cey().mo2960e(this.deL);
            }
            C0503Gz.m3559a(this.gXJ, this.deL, f, this.fRu, this.angularVelocity);
            this.gXK.set(this.fRu);
            this.gXL.set(this.angularVelocity);
            this.gXJ.mo22947a(this.deL);
        }
    }

    public void bPy() {
        if (!cEX()) {
            mo13907ay(this.fdB);
        }
    }

    /* renamed from: K */
    public void mo13893K(Vec3f vec3f) {
        if (this.cZe != 0.0f) {
            this.fdB.scale(1.0f / this.cZe, vec3f);
        }
    }

    public Vec3f cep() {
        return this.fdB;
    }

    /* renamed from: t */
    public void mo13940t(float f, float f2) {
        this.fgF = C3696tt.m39765c(f, 0.0f, 1.0f);
        this.fgG = C3696tt.m39765c(f2, 0.0f, 1.0f);
    }

    /* renamed from: jE */
    public void mo13933jE(float f) {
        this.stack.bcH().push();
        try {
            this.fRu.scale(C3696tt.m39765c(1.0f - (this.fgF * f), 0.0f, 1.0f));
            this.angularVelocity.scale(C3696tt.m39765c(1.0f - (this.fgG * f), 0.0f, 1.0f));
            if (this.fgJ) {
                if (this.angularVelocity.lengthSquared() < this.fgM && this.fRu.lengthSquared() < this.fgL) {
                    this.angularVelocity.scale(this.fgK);
                    this.fRu.scale(this.fgK);
                }
                float length = this.fRu.length();
                if (length < this.fgF) {
                    if (length > 0.005f) {
                        Vec3f ac = this.stack.bcH().mo4458ac(this.fRu);
                        ac.normalize();
                        ac.scale(0.005f);
                        this.fRu.sub(ac);
                    } else {
                        this.fRu.set(0.0f, 0.0f, 0.0f);
                    }
                }
                float length2 = this.angularVelocity.length();
                if (length2 < this.fgG) {
                    if (length2 > 0.005f) {
                        Vec3f ac2 = this.stack.bcH().mo4458ac(this.angularVelocity);
                        ac2.normalize();
                        ac2.scale(0.005f);
                        this.angularVelocity.sub(ac2);
                    } else {
                        this.angularVelocity.set(0.0f, 0.0f, 0.0f);
                    }
                }
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: b */
    public void mo13909b(float f, Vec3f vec3f) {
        float f2;
        float f3;
        float f4 = 0.0f;
        if (f == 0.0f) {
            this.gXN |= 1;
            this.cZe = 0.0f;
        } else {
            this.gXN &= -2;
            this.cZe = 1.0f / f;
        }
        Vec3f vec3f2 = this.fRv;
        if (vec3f.x != 0.0f) {
            f2 = 1.0f / vec3f.x;
        } else {
            f2 = 0.0f;
        }
        if (vec3f.y != 0.0f) {
            f3 = 1.0f / vec3f.y;
        } else {
            f3 = 0.0f;
        }
        if (vec3f.z != 0.0f) {
            f4 = 1.0f / vec3f.z;
        }
        vec3f2.set(f2, f3, f4);
    }

    public float aRf() {
        return this.cZe;
    }

    public Matrix3fWrap aRa() {
        return this.cZf;
    }

    /* renamed from: jF */
    public void mo13934jF(float f) {
        if (!cEX()) {
            this.stack.bcH().push();
            try {
                this.fRu.scaleAdd(this.cZe * f, this.fRw, this.fRu);
                Vec3f ac = this.stack.bcH().mo4458ac(this.fRx);
                this.cZf.transform(ac);
                this.angularVelocity.scaleAdd(f, ac, this.angularVelocity);
                float length = this.angularVelocity.length();
                if (length * f > 1.5707964f) {
                    this.angularVelocity.scale((1.5707964f / f) / length);
                }
            } finally {
                this.stack.bcH().pop();
            }
        }
    }

    /* renamed from: i */
    public void mo13931i(C3978xf xfVar) {
        if (cEX()) {
            this.gXJ.mo22947a(this.deL);
        } else {
            this.gXJ.mo22947a(xfVar);
        }
        this.gXK.set(cev());
        this.gXL.set(getAngularVelocity());
        this.deL.mo22947a(xfVar);
        cer();
    }

    /* renamed from: ay */
    public void mo13907ay(Vec3f vec3f) {
        this.fRw.add(vec3f);
    }

    public Vec3f ceq() {
        return this.fRv;
    }

    /* renamed from: az */
    public void mo13908az(Vec3f vec3f) {
        this.fRv.set(vec3f);
    }

    /* renamed from: u */
    public void mo13941u(float f, float f2) {
        this.fgH = f;
        this.fgI = f2;
    }

    /* renamed from: aA */
    public void mo13900aA(Vec3f vec3f) {
        this.fRx.add(vec3f);
    }

    /* renamed from: y */
    public void mo13942y(Vec3f vec3f, Vec3f vec3f2) {
        this.stack.bcH().push();
        try {
            mo13907ay(vec3f);
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            vec3f3.cross(vec3f2, vec3f);
            mo13900aA(vec3f3);
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: P */
    public void mo13895P(Vec3f vec3f) {
        this.fRu.scaleAdd(this.cZe, vec3f, this.fRu);
    }

    /* renamed from: Q */
    public void mo13896Q(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f ac = this.stack.bcH().mo4458ac(vec3f);
            this.cZf.transform(ac);
            this.angularVelocity.add(ac);
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: m */
    public void mo13937m(Vec3f vec3f, Vec3f vec3f2) {
        this.stack.bcH().push();
        try {
            if (this.cZe != 0.0f) {
                mo13895P(vec3f);
                if (this.cZg != 0.0f) {
                    Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                    vec3f3.cross(vec3f2, vec3f);
                    vec3f3.scale(this.cZg);
                    mo13896Q(vec3f3);
                }
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: b */
    public void mo13911b(Vec3f vec3f, Vec3f vec3f2, float f) {
        if (this.cZe != 0.0f) {
            this.fRu.scaleAdd(f, vec3f, this.fRu);
            if (this.cZg != 0.0f) {
                this.angularVelocity.scaleAdd(this.cZg * f, vec3f2, this.angularVelocity);
            }
        }
    }

    public void aDV() {
        this.fRw.set(0.0f, 0.0f, 0.0f);
        this.fRx.set(0.0f, 0.0f, 0.0f);
    }

    public void cer() {
        this.stack.bcK().push();
        try {
            Matrix3fWrap ajd = (Matrix3fWrap) this.stack.bcK().get();
            C3427rS.m38369a(ajd, this.deL.bFF, this.fRv);
            Matrix3fWrap g = this.stack.bcK().mo15565g(this.deL.bFF);
            g.transpose();
            this.cZf.mul(ajd, g);
        } finally {
            this.stack.bcK().pop();
        }
    }

    public Vec3f ces() {
        return this.deL.bFG;
    }

    public Quat4f cet() {
        this.stack.bcN().push();
        try {
            Quat4fWrap aoy = (Quat4fWrap) this.stack.bcN().get();
            C3427rS.m38375b(this.deL.bFF, (Quat4f) aoy);
            return (Quat4f) this.stack.bcN().mo15197aq(aoy);
        } finally {
            this.stack.bcN().pop();
        }
    }

    public C3978xf ceu() {
        return this.deL;
    }

    public Vec3f cev() {
        return this.fRu;
    }

    public Vec3f getAngularVelocity() {
        return this.angularVelocity;
    }

    public void setAngularVelocity(Vec3f vec3f) {
        if (f4683jR || this.gXN != 1) {
            this.angularVelocity.set(vec3f);
            return;
        }
        throw new AssertionError();
    }

    /* renamed from: O */
    public void mo13894O(Vec3f vec3f) {
        if (f4683jR || this.gXN != 1) {
            this.fRu.set(vec3f);
            return;
        }
        throw new AssertionError();
    }

    /* renamed from: R */
    public Vec3f mo13897R(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            vec3f2.cross(this.angularVelocity, vec3f);
            vec3f2.add(this.fRu);
            return (Vec3f) this.stack.bcH().mo15197aq(vec3f2);
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: aB */
    public void mo13901aB(Vec3f vec3f) {
        this.deL.bFG.add(vec3f);
    }

    /* renamed from: z */
    public void mo13943z(Vec3f vec3f, Vec3f vec3f2) {
        cEZ().getAabb(this.deL, vec3f, vec3f2);
    }

    /* renamed from: A */
    public float mo13892A(Vec3f vec3f, Vec3f vec3f2) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            vec3f3.sub(vec3f, ces());
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            vec3f4.cross(vec3f3, vec3f2);
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            C3427rS.m38372a(vec3f5, vec3f4, aRa());
            Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
            vec3f6.cross(vec3f5, vec3f3);
            float dot = this.cZe + vec3f2.dot(vec3f6);
            return dot;
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: aC */
    public float mo13902aC(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            C3427rS.m38372a(vec3f2, vec3f, aRa());
            return vec3f.dot(vec3f2);
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: jG */
    public void mo13935jG(float f) {
        if (cFa() != 2 && cFa() != 4) {
            if (cev().lengthSquared() >= this.fgH * this.fgH || getAngularVelocity().lengthSquared() >= this.fgI * this.fgI) {
                this.gXR = 0.0f;
                mo17050wv(0);
                return;
            }
            this.gXR += f;
        }
    }

    public boolean cew() {
        if (cFa() == 4 || C0128Bb.dMG || C0128Bb.dMF == 0.0f) {
            return false;
        }
        if (cFa() == 2 || cFa() == 3) {
            return true;
        }
        if (this.gXR > C0128Bb.dMF) {
            return true;
        }
        return false;
    }

    public C0783LL cex() {
        return this.gXM;
    }

    /* renamed from: b */
    public void mo13910b(C0783LL ll) {
        this.gXM = ll;
    }

    public C6863avD cey() {
        return this.fRy;
    }

    /* renamed from: a */
    public void mo13899a(C6863avD avd) {
        this.fRy = avd;
        if (this.fRy != null) {
            avd.mo2960e(this.deL);
        }
    }

    /* renamed from: jH */
    public void mo13936jH(float f) {
        this.cZg = f;
    }

    public float aRg() {
        return this.cZg;
    }

    public boolean cez() {
        return cex() != null;
    }

    /* renamed from: d */
    public boolean mo13925d(ayY ayy) {
        C6238ajC c = m22755c(ayy);
        if (c == null) {
            return true;
        }
        for (int i = 0; i < this.fRz.size(); i++) {
            C1083Pr pr = this.fRz.get(i);
            if (pr.bnn() == c || pr.bno() == c) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: e */
    public void mo13927e(C1083Pr pr) {
        if (this.fRz.indexOf(pr) == -1) {
            this.fRz.add(pr);
        }
        this.gXW = true;
    }

    /* renamed from: f */
    public void mo13928f(C1083Pr pr) {
        this.fRz.remove(pr);
        this.gXW = this.fRz.size() > 0;
    }

    /* renamed from: rA */
    public C1083Pr mo13938rA(int i) {
        return this.fRz.get(i);
    }

    public int ceA() {
        return this.fRz.size();
    }
}
