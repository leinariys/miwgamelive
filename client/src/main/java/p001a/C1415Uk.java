package p001a;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/* renamed from: a.Uk */
/* compiled from: a */
public class C1415Uk {
    private static final Comparator<C1416a> iAW = new aJU();
    private final List<C1416a> elements = new ArrayList();

    public void doe() {
        int size = this.elements.size();
        for (int i = 0; i < size; i++) {
            this.elements.get(i).f1797id = mo5909zZ(i);
            this.elements.get(i).ena = i;
        }
        C3696tt.m39764a(this.elements, iAW);
    }

    public void reset(int i) {
        mo5908zY(i);
        for (int i2 = 0; i2 < i; i2++) {
            this.elements.get(i2).f1797id = i2;
            this.elements.get(i2).ena = 1;
        }
    }

    public int dof() {
        return this.elements.size();
    }

    /* renamed from: zW */
    public boolean mo5906zW(int i) {
        return i == this.elements.get(i).f1797id;
    }

    /* renamed from: zX */
    public C1416a mo5907zX(int i) {
        return this.elements.get(i);
    }

    /* renamed from: zY */
    public void mo5908zY(int i) {
        C3696tt.m39762a(this.elements, i, C1416a.class);
    }

    public void free() {
        this.elements.clear();
    }

    /* renamed from: aG */
    public int mo5900aG(int i, int i2) {
        return mo5909zZ(i) == mo5909zZ(i2) ? 1 : 0;
    }

    /* renamed from: aH */
    public void mo5901aH(int i, int i2) {
        int zZ = mo5909zZ(i);
        int zZ2 = mo5909zZ(i2);
        if (zZ != zZ2) {
            this.elements.get(zZ).f1797id = zZ2;
            C1416a aVar = this.elements.get(zZ2);
            aVar.ena = this.elements.get(zZ).ena + aVar.ena;
        }
    }

    /* renamed from: zZ */
    public int mo5909zZ(int i) {
        while (i != this.elements.get(i).f1797id) {
            this.elements.get(i).f1797id = this.elements.get(this.elements.get(i).f1797id).f1797id;
            i = this.elements.get(i).f1797id;
        }
        return i;
    }

    /* renamed from: a.Uk$a */
    public static class C1416a {
        public int ena;

        /* renamed from: id */
        public int f1797id;
    }
}
