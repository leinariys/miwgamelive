package p001a;

import game.network.message.C6215aif;
import logic.thred.LogPrinter;

import java.io.RandomAccessFile;

/* renamed from: a.aAH */
/* compiled from: a */
public class aAH extends aLV {
    private long dtn = 997732322341L;
    private long dto = 999323423421L;
    private long dtp = 999323423241L;
    private RandomAccessFile evF;
    private byte[] hfj = new byte[4];

    public aAH(String str) {
        this.evF = new RandomAccessFile(str, "r");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: K */
    public int mo7625K(int i, int i2) {
        return (((int) ((this.dtn * (this.dto + ((long) i))) + this.dtp)) ^ i2) & 255;
    }

    /* renamed from: a */
    public boolean mo7626a(int i, int i2, byte[] bArr, int i3, int i4) {
        this.evF.seek((long) i);
        this.evF.read(bArr, i3, i4);
        for (int i5 = 0; i5 < i4; i5++) {
            bArr[i3 + i5] = (byte) mo7625K(i2 + i5, bArr[i3 + i5]);
        }
        return true;
    }

    public int getInt(int i) {
        mo7626a(i, 0, this.hfj, 0, 4);
        return ((this.hfj[0] & 255) << C6215aif.idH) | ((this.hfj[1] & 255) << LogPrinter.eqN) | ((this.hfj[2] & 255) << 8) | (this.hfj[3] & 255);
    }

    public int cIE() {
        return (int) this.evF.length();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        try {
            this.evF.close();
        } catch (Exception e) {
        }
        super.finalize();
    }
}
