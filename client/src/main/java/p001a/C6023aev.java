package p001a;

import game.engine.DataGameEvent;
import game.network.message.serializable.C1695Yw;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

/* renamed from: a.aev  reason: case insensitive filesystem */
/* compiled from: a */
public class C6023aev extends C1395US {
    private FileOutputStream fou = null;
    private ObjectOutputStream fov = null;

    public C6023aev(String str, DataGameEvent jz) {
        this.fou = new FileOutputStream(str, true);
        this.fov = new aAD(jz, new C2883lR(new BufferedOutputStream(this.fou)));
    }

    /* renamed from: a */
    public void mo5874a(C1596XM xm) {
        this.fou.write((String.valueOf(xm.getText().replaceAll("=", ": ").replaceAll("[\\r\\n]$|[\\r]", "")) + "\n\n").getBytes());
        this.fou.flush();
    }

    /* renamed from: b */
    public void mo5875b(C1695Yw yw) {
        this.fov.writeObject(yw.bHC());
        this.fov.writeObject(yw.bHD());
        this.fov.writeObject(yw.bHG());
        this.fov.flush();
    }

    public void close() {
        if (this.fov != null) {
            this.fov.close();
            this.fov = null;
        }
        if (this.fou != null) {
            this.fou.close();
            this.fou = null;
        }
    }
}
