package p001a;

import game.network.manager.C6546aoy;

@C6546aoy
/* renamed from: a.PQ */
/* compiled from: a */
public class C1052PQ extends C5953add {

    private C1053a eox;

    public C1052PQ(C1053a aVar) {
        this.eox = aVar;
    }

    public C1053a bxI() {
        return this.eox;
    }

    /* renamed from: a.PQ$a */
    public enum C1053a {
        INVALID_NAME,
        NAME_IN_USE,
        NO_ENOUGH_BALANCE,
        CORPORATION_ALREADY_CREATED,
        NO_ENOUGH_MEMBERS,
        NO_INVITATION_STATUS,
        NAME_RESERVED
    }
}
