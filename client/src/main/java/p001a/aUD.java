package p001a;

import logic.res.scene.C2036bO;
import logic.res.scene.C6791atj;

/* renamed from: a.aUD */
/* compiled from: a */
public class aUD implements C1740Zi<C1693Yu> {
    private String name;

    public aUD(String str) {
        this.name = str;
    }

    /* renamed from: a */
    public static C2036bO m18503a(String str, C2036bO bOVar) {
        return new aUD(str).mo7458j(bOVar).bHA();
    }

    /* renamed from: a */
    public static C2036bO m18502a(String str, C6791atj atj) {
        return new aUD(str).mo7457c(atj).bHA();
    }

    /* renamed from: n */
    public C1693Yu mo7458j(C2036bO bOVar) {
        C1693Yu yu = new C1693Yu();
        if (mo7456a(bOVar, yu)) {
            yu.mo7296i((C2036bO) null);
            yu.mo7295h((C2036bO) null);
        }
        return yu;
    }

    /* renamed from: d */
    public C1693Yu mo7457c(C6791atj atj) {
        C1693Yu yu = new C1693Yu();
        C2036bO[] Ix = atj.mo15350Ix();
        int length = Ix.length;
        int i = 0;
        while (true) {
            if (i < length) {
                C2036bO bOVar = Ix[i];
                yu.mo7296i((C2036bO) null);
                yu.mo7295h((C2036bO) null);
                if (!mo7456a(bOVar, yu)) {
                    break;
                }
                i++;
            } else {
                yu.mo7296i((C2036bO) null);
                yu.mo7295h((C2036bO) null);
                break;
            }
        }
        return yu;
    }

    /* renamed from: a */
    public boolean mo7456a(C2036bO bOVar, C1693Yu yu) {
        if (this.name.equals(bOVar.getName())) {
            yu.mo7295h(bOVar);
            return false;
        }
        for (C2036bO bOVar2 : bOVar.mo15350Ix()) {
            yu.mo7296i(bOVar);
            if (!mo7456a(bOVar2, yu)) {
                return false;
            }
        }
        return true;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }
}
