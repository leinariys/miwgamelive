package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;
import game.geometry.Quat4fWrap;

import javax.vecmath.Quat4f;

/* renamed from: a.Gz */
/* compiled from: a */
public class C0503Gz {
    public static final float cZi = 0.70710677f;
    public static final float cZj = 0.7853982f;

    /* renamed from: fp */
    public static float m3563fp(float f) {
        return 1.0f / ((float) Math.sqrt((double) f));
    }

    /* renamed from: e */
    public static void m3562e(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        if (Math.abs(vec3f.z) > 0.70710677f) {
            float f = (vec3f.y * vec3f.y) + (vec3f.z * vec3f.z);
            float fp = m3563fp(f);
            vec3f2.set(0.0f, (-vec3f.z) * fp, vec3f.y * fp);
            vec3f3.set(f * fp, (-vec3f.x) * vec3f2.z, vec3f.x * vec3f2.y);
            return;
        }
        float f2 = (vec3f.x * vec3f.x) + (vec3f.y * vec3f.y);
        float fp2 = m3563fp(f2);
        vec3f2.set((-vec3f.y) * fp2, vec3f.x * fp2, 0.0f);
        vec3f3.set((-vec3f.z) * vec3f2.y, vec3f.z * vec3f2.x, f2 * fp2);
    }

    /* renamed from: a */
    public static void m3561a(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2, float f, C3978xf xfVar2) {
        C0763Kt bcE = C0763Kt.bcE();
        bcE.bcH().push();
        bcE.bcN().push();
        try {
            xfVar2.bFG.scaleAdd(f, vec3f, xfVar.bFG);
            Vec3f vec3f3 = (Vec3f) bcE.bcH().get();
            float length = vec3f2.length();
            if (length * f > 0.7853982f) {
                length = 0.7853982f / f;
            }
            if (length < 0.001f) {
                vec3f3.scale((0.5f * f) - (((((f * f) * f) * 0.020833334f) * length) * length), vec3f2);
            } else {
                vec3f3.scale(((float) Math.sin((double) ((0.5f * length) * f))) / length, vec3f2);
            }
            Quat4fWrap a = bcE.bcN().mo11472a(vec3f3.x, vec3f3.y, vec3f3.z, (float) Math.cos((double) (length * f * 0.5f)));
            Quat4fWrap b = bcE.bcN().mo11474b(xfVar.anz());
            Quat4f quat4f = (Quat4f) bcE.bcN().get();
            quat4f.mul(a, b);
            quat4f.normalize();
            xfVar2.setRotation(quat4f);
        } finally {
            bcE.bcH().pop();
            bcE.bcN().pop();
        }
    }

    /* renamed from: a */
    public static void m3559a(C3978xf xfVar, C3978xf xfVar2, float f, Vec3f vec3f, Vec3f vec3f2) {
        C0763Kt bcE = C0763Kt.bcE();
        bcE.bcH().push();
        try {
            vec3f.sub(xfVar2.bFG, xfVar.bFG);
            vec3f.scale(1.0f / f);
            Vec3f vec3f3 = (Vec3f) bcE.bcH().get();
            float[] fArr = new float[1];
            m3560a(xfVar, xfVar2, vec3f3, fArr);
            vec3f2.scale(fArr[0] / f, vec3f3);
        } finally {
            bcE.bcH().pop();
        }
    }

    /* renamed from: a */
    public static void m3560a(C3978xf xfVar, C3978xf xfVar2, Vec3f vec3f, float[] fArr) {
        C0763Kt bcE = C0763Kt.bcE();
        bcE.bcK().push();
        bcE.bcN().push();
        try {
            Matrix3fWrap ajd = (Matrix3fWrap) bcE.bcK().get();
            ajd.set(xfVar.bFF);
            C3427rS.m38378c(ajd);
            Matrix3fWrap ajd2 = (Matrix3fWrap) bcE.bcK().get();
            ajd2.mul(xfVar2.bFF, ajd);
            Quat4f quat4f = (Quat4f) bcE.bcN().get();
            C3427rS.m38375b(ajd2, quat4f);
            quat4f.normalize();
            fArr[0] = C1777aC.m13118a(quat4f);
            vec3f.set(quat4f.x, quat4f.y, quat4f.z);
            float lengthSquared = vec3f.lengthSquared();
            if (lengthSquared < 1.4210855E-14f) {
                vec3f.set(1.0f, 0.0f, 0.0f);
            } else {
                vec3f.scale(1.0f / ((float) Math.sqrt((double) lengthSquared)));
            }
        } finally {
            bcE.bcK().pop();
            bcE.bcN().pop();
        }
    }
}
