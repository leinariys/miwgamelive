package p001a;

import logic.res.html.DataClassSerializer;

import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* renamed from: a.aJl  reason: case insensitive filesystem */
/* compiled from: a */
public class C5441aJl extends DataClassSerializer {
    public static C5441aJl ifE = new C5441aJl();

    /* renamed from: a */
    public Object mo9622a(ObjectInput objectInput, DataClassSerializer kd) {
        return mo9623a(objectInput, kd, (List) new ArrayList());
    }

    /* renamed from: a */
    public Object mo9623a(ObjectInput objectInput, DataClassSerializer kd, List list) {
        int readInt = objectInput.readInt();
        for (int i = 0; i < readInt; i++) {
            list.add(kd.inputReadObject(objectInput));
        }
        return list;
    }

    /* renamed from: a */
    public void mo9624a(ObjectOutput objectOutput, Collection collection, DataClassSerializer kd) {
        int size = collection.size();
        objectOutput.writeInt(size);
        for (Object a : collection) {
            size--;
            kd.serializeData(objectOutput, a);
        }
        if (size != 0) {
            throw new IllegalStateException();
        }
    }
}
