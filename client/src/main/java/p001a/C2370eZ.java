package p001a;

import game.network.manager.C6546aoy;

@C6546aoy
/* renamed from: a.eZ */
/* compiled from: a */
public class C2370eZ extends Exception {


    /* renamed from: Jb */
    private C2371a f6997Jb;

    public C2370eZ(C2371a aVar) {
        this.f6997Jb = aVar;
    }

    public C2370eZ() {
    }

    /* renamed from: pa */
    public C2371a mo18132pa() {
        return this.f6997Jb;
    }

    /* renamed from: a */
    public void mo18131a(C2371a aVar) {
        this.f6997Jb = aVar;
    }

    /* renamed from: a.eZ$a */
    public enum C2371a {
        NO_FOUNDS,
        INVALID_LOCATION,
        INVALID_ITEM,
        INVALID_PARAMETERS,
        UNABLE_TO_TRANSFER
    }
}
