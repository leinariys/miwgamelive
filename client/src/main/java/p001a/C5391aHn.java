package p001a;

import util.C3969xW;

import java.util.Set;

/* renamed from: a.aHn  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C5391aHn<T> extends C3969xW<T> implements Set<T> {


    public boolean add(T t) {
        int size = size() - 1;
        int i = 0;
        while (true) {
            if (i <= size) {
                int i2 = ((size - i) >> 1) + i;
                int compare = compare(t, get(i2));
                if (compare <= 0) {
                    if (compare == 0) {
                        break;
                    }
                    size = i2 - 1;
                } else {
                    i = i2 + 1;
                }
            } else {
                add(i, t);
                break;
            }
        }
        return true;
    }
}
