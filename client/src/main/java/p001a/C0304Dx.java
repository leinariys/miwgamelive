package p001a;

/* renamed from: a.Dx */
/* compiled from: a */
class C0304Dx extends ayA {
    private final int cHD = C1298TD.hFl;
    private final int cHE = 1000;
    private final long cHF = 100;
    /* access modifiers changed from: private */
    public boolean cHG = false;
    /* access modifiers changed from: private */
    public boolean cHH = true;
    private Thread cHI = new C5310aEk(this);

    public C0304Dx(C1285Sv sv) {
        super(sv);
        restart();
        this.cHI.start();
    }

    /* renamed from: cz */
    public synchronized void mo1786cz(boolean z) {
        this.cHH = z;
    }

    /* access modifiers changed from: protected */
    public void aGu() {
        switch (this.state) {
            case 0:
                mo16928jh(5000);
                this.state++;
                return;
            case 1:
                mo1786cz(true);
                long nanoTime = System.nanoTime();
                while (true) {
                    if (!this.cHG) {
                        if (((double) System.nanoTime()) > ((double) nanoTime) + 1.0E9d) {
                            log("Request timeout");
                            mo1786cz(false);
                        } else {
                            try {
                                Thread.sleep(10);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                int round = (int) Math.round(((double) (System.nanoTime() - nanoTime)) / 1000000.0d);
                if (((long) round) > 100) {
                    log("Request exceeded maximum request time, delay was: " + (((long) round) - 100) + " ms");
                }
                log("Send response time: " + round + " ms");
                this.state = 0;
                return;
            default:
                return;
        }
    }
}
