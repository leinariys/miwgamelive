package p001a;

import com.hoplon.geometry.Vec3f;
import logic.abc.C5804aak;

/* renamed from: a.gt */
/* compiled from: a */
public class C2566gt extends C3808vG {

    /* renamed from: Qm */
    public static final C6548apA f7785Qm = new C6635aqj();

    /* renamed from: Qk */
    private boolean f7786Qk;

    /* renamed from: Ql */
    private aGW f7787Ql;

    public C2566gt(aGW agw, C1034PA pa, ayY ayy, ayY ayy2) {
        super(pa);
        this.f7787Ql = agw;
        if (this.f7787Ql == null) {
            this.f7787Ql = this.f9400Uy.mo594g(ayy, ayy2);
            this.f7786Qk = true;
        }
    }

    public C2566gt(C1034PA pa) {
        super(pa);
    }

    public void destroy() {
        if (this.f7786Qk && this.f7787Ql != null) {
            this.f9400Uy.mo583a(this.f7787Ql);
        }
    }

    /* renamed from: a */
    public void mo8931a(ayY ayy, ayY ayy2, C5408aIe aie, C5362aGk agk) {
        if (this.f7787Ql != null) {
            this.stack.bcH().push();
            try {
                agk.mo9117e(this.f7787Ql);
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                vec3f.sub(ayy.cFf().bFG, ayy2.cFf().bFG);
                float length = vec3f.length();
                float radius = ((C5804aak) ayy.cEZ()).getRadius();
                float radius2 = ((C5804aak) ayy2.cEZ()).getRadius();
                this.f7787Ql.dcC();
                if (length <= radius + radius2) {
                    float f = length - (radius + radius2);
                    Vec3f h = this.stack.bcH().mo4460h(1.0f, 0.0f, 0.0f);
                    if (length > 1.1920929E-7f) {
                        h.scale(1.0f / length, vec3f);
                    }
                    Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                    vec3f2.scale(radius, h);
                    ((Vec3f) this.stack.bcH().get()).sub(ayy.cFf().bFG, vec3f2);
                    Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                    vec3f2.scale(radius2, h);
                    vec3f3.add(ayy2.cFf().bFG, vec3f2);
                    agk.mo5217c(h, vec3f3, f);
                    this.stack.bcH().pop();
                }
            } finally {
                this.stack.bcH().pop();
            }
        }
    }

    /* renamed from: b */
    public float mo8932b(ayY ayy, ayY ayy2, C5408aIe aie, C5362aGk agk) {
        return 1.0f;
    }
}
