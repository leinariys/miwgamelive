package p001a;

/* renamed from: a.Je */
/* compiled from: a */
public class C0678Je implements C3223pR {
    public final int diz;

    /* renamed from: w */
    public final int f804w;

    /* renamed from: x */
    public final int f805x;

    /* renamed from: y */
    public final int f806y;

    public C0678Je(int i, int i2, int i3, int i4) {
        this.f805x = i;
        this.f806y = i2;
        this.f804w = i3;
        this.diz = i4;
    }

    public boolean inside(int i, int i2) {
        int i3 = this.f804w;
        int i4 = this.diz;
        if ((i3 | i4) < 0) {
            return false;
        }
        int i5 = this.f805x;
        int i6 = this.f806y;
        if (i < i5 || i2 < i6) {
            return false;
        }
        int i7 = i3 + i5;
        int i8 = i4 + i6;
        if (i7 >= i5 && i7 <= i) {
            return false;
        }
        if (i8 < i6 || i8 > i2) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public boolean mo3226a(C0678Je je) {
        int i = this.f804w;
        int i2 = this.diz;
        int i3 = je.f804w;
        int i4 = je.diz;
        if (i3 <= 0 || i4 <= 0 || i <= 0 || i2 <= 0) {
            return false;
        }
        int i5 = this.f805x;
        int i6 = this.f806y;
        int i7 = je.f805x;
        int i8 = je.f806y;
        int i9 = i3 + i7;
        int i10 = i4 + i8;
        int i11 = i + i5;
        int i12 = i2 + i6;
        if (i9 >= i7 && i9 <= i5) {
            return false;
        }
        if (i10 >= i8 && i10 <= i6) {
            return false;
        }
        if (i11 >= i5 && i11 <= i7) {
            return false;
        }
        if (i12 < i6 || i12 > i8) {
            return true;
        }
        return false;
    }

    /* renamed from: WD */
    public final C0678Je mo3225WD() {
        return this;
    }
}
