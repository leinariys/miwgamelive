package p001a;

import game.script.item.Item;
import game.script.item.ItemType;
import logic.baa.aDJ;
import logic.ui.Panel;
import taikodom.addon.IAddonProperties;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.mT */
/* compiled from: a */
abstract class C2974mT {
    public final Panel aGi;
    public final List<Class<? extends aDJ>> aGj = new ArrayList();

    public C2974mT(Class<? extends aDJ> cls, Panel aco) {
        this.aGi = aco;
        this.aGj.add(cls);
    }

    public C2974mT(Class<? extends aDJ>[] clsArr, Panel aco) {
        this.aGi = aco;
        for (Class<? extends aDJ> add : clsArr) {
            this.aGj.add(add);
        }
    }

    /* renamed from: a */
    public abstract void mo1169a(IAddonProperties vWVar, ItemType jCVar, Item auq);

    /* renamed from: g */
    public void mo20557g(Class<? extends aDJ> cls) {
        this.aGj.add(cls);
    }

    /* renamed from: Pz */
    public void mo20556Pz() {
        this.aGi.setVisible(false);
    }

    /* renamed from: h */
    public boolean mo20558h(Class<? extends aDJ> cls) {
        for (Class<? extends aDJ> isAssignableFrom : this.aGj) {
            if (isAssignableFrom.isAssignableFrom(cls)) {
                return true;
            }
        }
        return false;
    }
}
