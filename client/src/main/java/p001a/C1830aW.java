package p001a;

import game.network.message.Blocking;

import java.util.List;

/* renamed from: a.aW */
/* compiled from: a */
public class C1830aW {

    /* renamed from: lS */
    private final List<Blocking> f3999lS;

    /* renamed from: lT */
    private final Object f4000lT;

    public C1830aW(List<Blocking> list, Object obj) {
        this.f3999lS = list;
        this.f4000lT = obj;
    }

    /* renamed from: fB */
    public List<Blocking> mo12003fB() {
        return this.f3999lS;
    }

    /* renamed from: fC */
    public Object mo12004fC() {
        return this.f4000lT;
    }
}
