package p001a;

import com.hoplon.geometry.Color;

/* renamed from: a.aTO */
/* compiled from: a */
public class aTO {
    /* renamed from: Y */
    public static String m18351Y(Color color) {
        if (color == null) {
            return null;
        }
        return String.valueOf(String.valueOf(color.getRed())) + ";" + String.valueOf(color.getGreen()) + ";" + String.valueOf(color.getBlue()) + ";" + String.valueOf(color.getAlpha());
    }

    /* renamed from: nG */
    public static Color m18352nG(String str) {
        int indexOf;
        int indexOf2;
        int indexOf3;
        if (str == null || "null".equals(str) || "".equals(str) || (indexOf = str.indexOf(";")) == -1 || (indexOf2 = str.indexOf(";", indexOf + 1)) == -1 || (indexOf3 = str.indexOf(";", indexOf2 + 1)) == -1) {
            return null;
        }
        return new Color(Float.parseFloat(str.substring(0, indexOf)), Float.parseFloat(str.substring(indexOf + 1, indexOf2)), Float.parseFloat(str.substring(indexOf2 + 1, indexOf3)), Float.parseFloat(str.substring(indexOf3 + 1)));
    }
}
