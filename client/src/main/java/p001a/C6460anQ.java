package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;

/* renamed from: a.anQ  reason: case insensitive filesystem */
/* compiled from: a */
public class C6460anQ {

    /* renamed from: jR */
    static final /* synthetic */ boolean f4953jR = (!C6460anQ.class.desiredAssertionStatus());
    public final Vec3f gfg = new Vec3f();
    public final Vec3f gfh = new Vec3f();
    public final Vec3f gfi = new Vec3f();
    public final Vec3f gfj = new Vec3f();
    public final Vec3f gfk = new Vec3f();
    public final C0763Kt stack = C0763Kt.bcE();
    public float gfl;

    /* renamed from: a */
    public void mo14967a(Matrix3fWrap ajd, Matrix3fWrap ajd2, Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4, float f, Vec3f vec3f5, float f2) {
        this.gfg.set(vec3f3);
        this.gfh.cross(vec3f, this.gfg);
        ajd.transform(this.gfh);
        this.gfi.set(this.gfg);
        this.gfi.negate();
        this.gfi.cross(vec3f2, this.gfi);
        ajd2.transform(this.gfi);
        C0647JL.m5603g(this.gfj, vec3f4, this.gfh);
        C0647JL.m5603g(this.gfk, vec3f5, this.gfi);
        this.gfl = this.gfj.dot(this.gfh) + f + f2 + this.gfk.dot(this.gfi);
        if (!f4953jR && this.gfl <= 0.0f) {
            throw new AssertionError();
        }
    }

    /* renamed from: a */
    public void mo14969a(Vec3f vec3f, Matrix3fWrap ajd, Matrix3fWrap ajd2, Vec3f vec3f2, Vec3f vec3f3) {
        this.gfg.set(0.0f, 0.0f, 0.0f);
        this.gfh.set(vec3f);
        ajd.transform(this.gfh);
        this.gfi.set(vec3f);
        this.gfi.negate();
        ajd2.transform(this.gfi);
        C0647JL.m5603g(this.gfj, vec3f2, this.gfh);
        C0647JL.m5603g(this.gfk, vec3f3, this.gfi);
        this.gfl = this.gfj.dot(this.gfh) + this.gfk.dot(this.gfi);
        if (!f4953jR && this.gfl <= 0.0f) {
            throw new AssertionError();
        }
    }

    /* renamed from: h */
    public void mo14971h(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
        this.gfg.set(0.0f, 0.0f, 0.0f);
        this.gfh.set(vec3f);
        this.gfi.set(vec3f2);
        this.gfi.negate();
        C0647JL.m5603g(this.gfj, vec3f3, this.gfh);
        C0647JL.m5603g(this.gfk, vec3f4, this.gfi);
        this.gfl = this.gfj.dot(this.gfh) + this.gfk.dot(this.gfi);
        if (!f4953jR && this.gfl <= 0.0f) {
            throw new AssertionError();
        }
    }

    /* renamed from: a */
    public void mo14968a(Matrix3fWrap ajd, Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4, float f) {
        this.gfg.set(vec3f3);
        this.gfh.cross(vec3f, vec3f3);
        ajd.transform(this.gfh);
        this.gfi.set(vec3f3);
        this.gfi.negate();
        this.gfi.cross(vec3f2, this.gfi);
        ajd.transform(this.gfi);
        C0647JL.m5603g(this.gfj, vec3f4, this.gfh);
        this.gfk.set(0.0f, 0.0f, 0.0f);
        this.gfl = this.gfj.dot(this.gfh) + f;
        if (!f4953jR && this.gfl <= 0.0f) {
            throw new AssertionError();
        }
    }

    public float clX() {
        return this.gfl;
    }

    /* renamed from: a */
    public float mo14965a(C6460anQ anq, float f) {
        return (this.gfg.dot(anq.gfg) * f) + this.gfj.dot(anq.gfh);
    }

    /* renamed from: a */
    public float mo14966a(C6460anQ anq, float f, float f2) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            C0647JL.m5603g(vec3f, this.gfg, anq.gfg);
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            C0647JL.m5603g(vec3f2, this.gfj, anq.gfh);
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            C0647JL.m5603g(vec3f3, this.gfk, anq.gfi);
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            vec3f4.scale(f, vec3f);
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            vec3f5.scale(f2, vec3f);
            Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
            C0647JL.m5592a(vec3f6, vec3f2, vec3f3, vec3f4, vec3f5);
            return vec3f6.z + vec3f6.x + vec3f6.y;
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: i */
    public float mo14972i(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            vec3f5.sub(vec3f, vec3f3);
            Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
            C0647JL.m5603g(vec3f6, vec3f2, this.gfh);
            Vec3f vec3f7 = (Vec3f) this.stack.bcH().get();
            C0647JL.m5603g(vec3f7, vec3f4, this.gfi);
            C0647JL.m5603g(vec3f5, vec3f5, this.gfg);
            vec3f6.add(vec3f7);
            vec3f6.add(vec3f5);
            float f = vec3f6.x + vec3f6.y + vec3f6.z + 1.1920929E-7f;
            return f;
        } finally {
            this.stack.bcH().pop();
        }
    }
}
