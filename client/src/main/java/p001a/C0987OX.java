package p001a;

/* renamed from: a.OX */
/* compiled from: a */
public class C0987OX {
    /* renamed from: a */
    public static String m8085a(C1649YI yi) {
        if (yi == null) {
            return null;
        }
        return yi.toString();
    }

    /* renamed from: gh */
    public static C1649YI m8086gh(String str) {
        if (str == null || "null".equals(str) || "".equals(str)) {
            return null;
        }
        if (str.contains("%")) {
            return new C1649YI(Float.parseFloat(str.substring(0, str.length() - 1)), C1649YI.C1650a.PERCENT);
        }
        return new C1649YI(Float.parseFloat(str), C1649YI.C1650a.ABSOLUTE);
    }
}
