package p001a;

/* renamed from: a.VC */
/* compiled from: a */
public abstract class C1440VC<T> {
    private final long euD;
    public T euB;
    private long euC = 0;
    private boolean valid = false;

    public C1440VC(long j) {
        this.euD = j;
    }

    /* access modifiers changed from: protected */
    public abstract void bBw();

    public void invalidate() {
        this.valid = false;
    }

    /* renamed from: fQ */
    public void mo5955fQ(long j) {
        if (!this.valid && j - this.euC > this.euD) {
            this.valid = true;
            this.euC = j;
            bBw();
        }
    }

    public void reset() {
        this.euB = null;
        this.valid = false;
        this.euC = 0;
    }
}
