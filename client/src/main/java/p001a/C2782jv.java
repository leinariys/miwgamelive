package p001a;

import game.script.npc.NPCType;

/* renamed from: a.jv */
/* compiled from: a */
public class C2782jv {
    private float akn;

    /* renamed from: nC */
    private NPCType f8346nC;

    public C2782jv(NPCType aed, float f) {
        this.f8346nC = aed;
        this.akn = f;
    }

    /* renamed from: Fs */
    public NPCType mo19997Fs() {
        return this.f8346nC;
    }

    /* renamed from: a */
    public void mo19999a(NPCType aed) {
        this.f8346nC = aed;
    }

    /* renamed from: Ft */
    public float mo19998Ft() {
        return this.akn;
    }

    /* renamed from: aM */
    public void mo20000aM(float f) {
        this.akn = f;
    }
}
