package p001a;

import logic.WrapRunnable;
import logic.res.code.SoftTimer;

@Deprecated
/* renamed from: a.afT  reason: case insensitive filesystem */
/* compiled from: a */
public class C6047afT {
    public static final String aGx = "TimerModule";
    private SoftTimer fvI = new SoftTimer(true);

    public boolean step(float f) {
        this.fvI.mo7965jB((long) (1000.0f * f));
        return false;
    }

    /* renamed from: a */
    public void mo13258a(WrapRunnable aen) {
        this.fvI.mo7963a(aen);
    }

    /* renamed from: a */
    public WrapRunnable mo13256a(String str, WrapRunnable aen, long j) {
        return this.fvI.mo7960a(str, aen, j);
    }

    /* renamed from: a */
    public WrapRunnable mo13257a(String str, WrapRunnable aen, long j, boolean z) {
        return this.fvI.mo7961a(str, aen, j, z);
    }

    /* renamed from: b */
    public WrapRunnable mo13260b(String str, WrapRunnable aen, long j) {
        return this.fvI.addTask(str, aen, j);
    }

    public SoftTimer alf() {
        return this.fvI;
    }
}
