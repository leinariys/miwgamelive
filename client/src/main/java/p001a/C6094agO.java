package p001a;

import game.network.WhoAmI;
import game.network.channel.NetworkChannel;
import taikodom.render.loader.provider.FilePath;

/* renamed from: a.agO  reason: case insensitive filesystem */
/* compiled from: a */
public class C6094agO implements C0390FP {
    private FilePath dDk;
    private WhoAmI whoAmI;
    private FilePath eKA;
    private Object eKa;
    private Class<?> eKv;
    private NetworkChannel fEJ;
    private C6280ajs fEK;
    private boolean fEL;
    private boolean fEM;
    private boolean fEN;
    private boolean fEO = true;
    private boolean fEP = true;
    private boolean fEQ = true;
    private boolean fER;
    private boolean fES;
    private boolean fET = true;
    private C6965axe fEU;
    private WraperDbFile fEV;

    /* renamed from: er */
    public void mo13368er(boolean z) {
        this.fEQ = z;
    }

    public boolean aQc() {
        return this.fEP;
    }

    /* renamed from: es */
    public void mo13369es(boolean z) {
        this.fEP = z;
    }

    public NetworkChannel aPT() {
        return this.fEJ;
    }

    /* renamed from: c */
    public void setServerSocket(NetworkChannel ahg) {
        this.fEJ = ahg;
    }

    public WhoAmI getWhoAmI() {
        return this.whoAmI;
    }

    /* renamed from: b */
    public void setWhoAmI(WhoAmI lt) {
        this.whoAmI = lt;
    }

    public Class<?> aPV() {
        return this.eKv;
    }

    /* renamed from: ai */
    public void setClassObject(Class<?> cls) {
        this.eKv = cls;
    }

    public C6280ajs ate() {
        return this.fEK;
    }

    /* renamed from: b */
    public void mo13364b(C6280ajs ajs) {
        this.fEK = ajs;
    }

    public Object aPW() {
        return this.eKa;
    }

    /* renamed from: ac */
    public void setEngineGame(Object obj) {
        this.eKa = obj;
    }

    public boolean aPX() {
        return this.fEL;
    }

    /* renamed from: et */
    public void mo13370et(boolean z) {
        this.fEL = z;
    }

    public boolean aPY() {
        return this.fEM;
    }

    public boolean aQa() {
        return this.fEO;
    }

    /* renamed from: eu */
    public void mo13371eu(boolean z) {
        this.fEO = z;
    }

    /* renamed from: ev */
    public void mo13372ev(boolean z) {
        this.fEM = z;
    }

    public boolean aPZ() {
        return this.fEN;
    }

    /* renamed from: ew */
    public void mo13373ew(boolean z) {
        this.fEN = z;
    }

    public boolean atf() {
        return this.fER;
    }

    /* renamed from: ex */
    public void mo13374ex(boolean z) {
        this.fER = z;
    }

    public boolean aQd() {
        return this.fEQ;
    }

    public FilePath aQe() {
        return this.eKA;
    }

    /* renamed from: f */
    public void setResursData(FilePath ain) {
        this.eKA = ain;
    }

    /* renamed from: ey */
    public void mo13375ey(boolean z) {
        this.fES = z;
    }

    public boolean aQb() {
        return this.fES;
    }

    public boolean aQf() {
        return this.fET;
    }

    /* renamed from: ez */
    public void mo13376ez(boolean z) {
        this.fET = z;
    }

    public FilePath bXr() {
        return this.dDk;
    }

    /* renamed from: g */
    public void setResursComponent(FilePath ain) {
        this.dDk = ain;
    }

    public C6965axe aQg() {
        return this.fEU;
    }

    /* renamed from: a */
    public void mo13360a(C6965axe axe) {
        this.fEU = axe;
    }

    /* renamed from: b */
    public void setWraperDbFile(WraperDbFile aqc) {
        this.fEV = aqc;
    }

    public WraperDbFile aQh() {
        return this.fEV;
    }
}
