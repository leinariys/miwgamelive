package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.geometry.Vector2fWrap;
import game.script.Actor;
import game.script.Character;
import game.script.faction.Faction;
import game.script.missile.Missile;
import game.script.npc.NPC;
import game.script.player.Player;
import game.script.ship.Outpost;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.ship.categories.*;
import game.script.space.Asteroid;
import game.script.space.Gate;
import logic.aaa.C5783aaP;
import logic.render.IEngineGraphics;
import logic.ui.C1276Sr;
import logic.ui.C2698il;
import taikodom.addon.IAddonProperties;
import taikodom.render.RenderView;
import taikodom.render.SceneView;
import taikodom.render.camera.Camera;
import taikodom.render.enums.BillboardAlignment;
import taikodom.render.enums.BlendType;
import taikodom.render.enums.GeometryType;
import taikodom.render.helpers.RenderTask;
import taikodom.render.scene.RBillboard;
import taikodom.render.scene.RExpandableMesh;
import taikodom.render.scene.Scene;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.ChannelInfo;
import taikodom.render.textures.Material;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: a.W */
/* compiled from: a */
public class C1494W {

    /* renamed from: mc */
    private static int f1929mc = 5;
    /* renamed from: lU */
    public Ship f1931lU;
    /* access modifiers changed from: private */
    /* renamed from: lV */
    public IEngineGraphics f1932lV;
    /* access modifiers changed from: private */
    /* renamed from: lW */
    public Scene f1933lW;
    /* access modifiers changed from: private */
    /* renamed from: lX */
    public Camera f1934lX;
    /* access modifiers changed from: private */
    /* renamed from: lY */
    public RExpandableMesh f1935lY;
    /* access modifiers changed from: private */
    /* renamed from: lZ */
    public RBillboard f1936lZ;
    /* access modifiers changed from: private */
    /* renamed from: mb */
    public boolean f1938mb;
    /* renamed from: md */
    public C1497c[] f1939md = new C1497c[f1929mc];
    /* access modifiers changed from: private */
    /* renamed from: me */
    public int f1940me = 0;
    /* access modifiers changed from: private */
    /* renamed from: mf */
    public C1495a f1941mf = new C1495a(this, (C1495a) null);
    /* access modifiers changed from: private */
    /* renamed from: mg */
    public Map<Actor, RBillboard> f1942mg = new HashMap();
    /* access modifiers changed from: private */
    /* renamed from: ml */
    public RBillboard f1947ml;
    /* access modifiers changed from: private */
    /* renamed from: mm */
    public RBillboard f1948mm;
    /* renamed from: mn */
    public Actor f1949mn;
    /* renamed from: mq */
    public Material f1952mq;
    /* renamed from: mr */
    public Material f1953mr;
    /* renamed from: mi */
    C6296akI.C1925a f1944mi = new aDK(this);
    /* access modifiers changed from: private */
    /* renamed from: kj */
    private IAddonProperties f1930kj;
    /* access modifiers changed from: private */
    /* renamed from: ma */
    private boolean f1937ma = false;
    /* access modifiers changed from: private */
    /* renamed from: mh */
    private boolean f1943mh = false;
    /* renamed from: mj */
    private Material f1945mj;
    /* renamed from: mk */
    private Shader f1946mk;
    /* access modifiers changed from: private */
    /* renamed from: mo */
    private C2698il f1950mo;
    /* access modifiers changed from: private */
    /* renamed from: mp */
    private RBillboard f1951mp;
    private SceneView sceneView;

    public C1494W(IAddonProperties vWVar) {
        this.f1930kj = vWVar;
        this.f1932lV = this.f1930kj.ale();
        this.f1930kj.mo2317P(this);
        for (int i = 0; i < f1929mc; i++) {
            this.f1939md[i] = new C1497c(this, (C1497c) null);
        }
        m10975fM();
        mo6468fS();
    }

    /* renamed from: a */
    public void mo6461a(C5783aaP.C1841a aVar) {
        if (aVar == C5783aaP.C1841a.DOCKED) {
            this.f1943mh = false;
            m10977fO();
        } else if (aVar == C5783aaP.C1841a.JUMPING) {
            this.f1943mh = false;
            m10977fO();
        } else {
            this.f1943mh = true;
            m10978fP();
        }
    }

    /* renamed from: b */
    public void mo6462b(Actor cr) {
        m10974f(cr);
    }

    /* renamed from: c */
    public void mo6464c(Actor cr) {
        m10972e(cr);
    }

    /* renamed from: r */
    public void mo6473r(boolean z) {
        if (z) {
            this.f1938mb = this.f1937ma;
        } else {
            this.f1937ma = this.f1938mb;
            this.f1938mb = false;
        }
        this.f1950mo.setVisible(this.f1938mb);
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public C1496b m10968d(Actor cr) {
        if (cr instanceof Ship) {
            Character agj = ((Ship) cr).agj();
            if (!(agj instanceof NPC)) {
                Player aku = (Player) agj;
                if (aku == null) {
                    return C1496b.ALLIED;
                }
                if (this.f1930kj.getPlayer().mo12639T(aku) == Faction.C1624a.ENEMY) {
                    return C1496b.ENEMY;
                }
                if (!aku.dxk() || !aku.dxi().aQS().contains(this.f1930kj.getPlayer())) {
                    return C1496b.ENEMY;
                }
                return C1496b.ALLIED;
            } else if (this.f1930kj.getPlayer().mo12639T(agj) == Faction.C1624a.ENEMY) {
                return C1496b.ENEMY;
            } else {
                return C1496b.ALLIED;
            }
        } else if (!(cr instanceof Missile)) {
            return C1496b.NEUTRAL;
        } else {
            Pawn cLw = ((Missile) cr).cLw();
            if (!(cLw instanceof Ship) || !((Ship) cLw).agj().azN().equals(this.f1930kj.getPlayer().azN())) {
                return C1496b.ENEMY;
            }
            return C1496b.ALLIED;
        }
    }

    /* renamed from: c */
    public Actor mo6463c(int i, int i2) {
        int aes = this.f1932lV.aes() / 2;
        int aet = this.f1932lV.aet() / 2;
        Vector2fWrap aka = new Vector2fWrap();
        Vec3f vec3f = new Vec3f();
        Vec3f vec3f2 = new Vec3f();
        for (Map.Entry next : this.f1942mg.entrySet()) {
            Actor cr = (Actor) next.getKey();
            if (!(cr == this.f1931lU || cr.cHg() == null)) {
                ((RBillboard) next.getValue()).getTransform().getTranslation(vec3f);
                this.f1934lX.getTransform().mo17345b(vec3f, vec3f2);
                aka.x = vec3f2.x + ((float) aes);
                aka.y = vec3f2.y + ((float) aet);
                if (Math.abs((aka.x - 2.0f) - ((float) i)) <= 8.0f && Math.abs((aka.y + 2.0f) - ((float) i2)) <= 8.0f) {
                    return cr;
                }
            }
        }
        return null;
    }

    /* renamed from: e */
    private void m10972e(Actor cr) {
        RBillboard remove = this.f1942mg.remove(cr);
        if (remove != null) {
            this.f1933lW.removeChild(remove);
        }
    }

    /* renamed from: f */
    private void m10974f(Actor cr) {
        RBillboard rBillboard = new RBillboard();
        rBillboard.setRender(false);
        rBillboard.setMaterial(this.f1945mj);
        rBillboard.setAlignment(BillboardAlignment.USER_ALIGNED);
        rBillboard.setSize(new Vector2fWrap(15.0f, 15.0f));
        this.f1932lV.mo3007a((RenderTask) new C1500f(rBillboard, cr));
        this.f1942mg.put(cr, rBillboard);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m10964a(RBillboard rBillboard, Actor cr) {
        Material material;
        if (cr instanceof Station) {
            material = (Material) this.f1932lV.aej().instantiateAsset("data/gui/imageset/radar/radar_station.png/material");
        } else if (cr instanceof Gate) {
            material = (Material) this.f1932lV.aej().instantiateAsset("data/gui/imageset/radar/radar_portal.png/material");
        } else if (cr instanceof Ship) {
            material = (Material) this.f1932lV.aej().instantiateAsset("data/gui/imageset/radar/radar_ship.png/material");
        } else if (cr instanceof Missile) {
            material = (Material) this.f1932lV.aej().instantiateAsset("data/gui/imageset/radar/radar_missile.png/material");
        } else {
            material = (Material) this.f1932lV.aej().instantiateAsset("data/gui/imageset/radar/radar_neutral.png/material");
        }
        if (material != null) {
            material.setShader(this.f1946mk);
            rBillboard.setMaterial(material);
            return;
        }
        this.f1930kj.getLog().error("Unable to find a material for radar billboard to actor " + cr);
    }

    /* renamed from: fM */
    private void m10975fM() {
        int aes = this.f1932lV.aes();
        int aet = this.f1932lV.aet();
        this.sceneView = new SceneView();
        this.sceneView.disable();
        this.sceneView.setViewport(0, 0, aes, aet);
        this.sceneView.setClearColorBuffer(false);
        this.sceneView.setClearDepthBuffer(true);
        this.sceneView.setClearStencilBuffer(true);
        this.f1933lW = this.sceneView.getScene();
        this.f1934lX = this.sceneView.getCamera();
        this.f1934lX.setOrthogonal(true);
        this.f1934lX.setOrthoWindow((float) ((-aes) / 2), (float) (aes / 2), (float) ((-aet) / 2), (float) (aet / 2));
        this.f1934lX.setAspect(((float) aes) / ((float) aet));
        C1501g gVar = new C1501g();
        C1498d dVar = new C1498d(aes, aet);
        this.f1932lV.mo3007a((RenderTask) gVar);
        this.f1932lV.mo3007a((RenderTask) dVar);
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m10970d(int i, int i2) {
        this.f1945mj = new Material();
        ShaderPass shaderPass = new ShaderPass();
        shaderPass.setDepthMask(false);
        shaderPass.setBlendEnabled(true);
        shaderPass.setSrcBlend(BlendType.SRC_ALPHA);
        shaderPass.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
        shaderPass.setColorArrayEnabled(true);
        shaderPass.setDepthTestEnabled(false);
        ShaderPass shaderPass2 = new ShaderPass();
        shaderPass2.setBlendEnabled(true);
        shaderPass2.setSrcBlend(BlendType.ONE);
        shaderPass2.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
        shaderPass2.setDepthMask(false);
        shaderPass2.setDepthTestEnabled(false);
        shaderPass2.setCullFaceEnabled(false);
        ChannelInfo channelInfo = new ChannelInfo();
        channelInfo.setTexChannel(8);
        channelInfo.setTexCoord(0);
        channelInfo.setChannelMapping(0);
        shaderPass2.setChannelTextureSet(0, channelInfo);
        Shader shader = new Shader();
        shader.addPass(shaderPass);
        Material material = new Material();
        material.setShader(shader);
        this.f1946mk = new Shader();
        this.f1946mk.addPass(shaderPass2);
        this.f1945mj.setShader(shader);
        this.f1935lY = new RExpandableMesh();
        this.f1933lW.addChild(this.f1935lY);
        this.f1935lY.getMesh().setGeometryType(GeometryType.LINES);
        this.f1935lY.setMaterial(material);
        this.f1936lZ = new RBillboard();
        this.f1952mq = (Material) this.f1932lV.aej().instantiateAsset("data/gui/imageset/radar/radar_selection.png/material");
        this.f1952mq.setShader(this.f1946mk);
        this.f1936lZ.setMaterial(this.f1952mq);
        this.f1933lW.addChild(this.f1936lZ);
        this.f1936lZ.setRender(false);
        this.f1936lZ.setSize(this.f1952mq.getDiffuseTexture().getTransformedWidth(), this.f1952mq.getDiffuseTexture().getTransformedHeight());
        this.f1936lZ.setAlignment(BillboardAlignment.USER_ALIGNED);
        this.f1951mp = new RBillboard();
        this.f1953mr = (Material) this.f1932lV.aej().instantiateAsset("data/gui/imageset/radar/radar_selection_oor.png/material");
        this.f1953mr.setShader(this.f1946mk);
        this.f1951mp.setMaterial(this.f1953mr);
        this.f1933lW.addChild(this.f1951mp);
        this.f1951mp.setRender(false);
        this.f1951mp.setSize(this.f1953mr.getDiffuseTexture().getTransformedWidth(), this.f1953mr.getDiffuseTexture().getTransformedHeight());
        this.f1951mp.setAlignment(BillboardAlignment.USER_ALIGNED);
    }

    /* access modifiers changed from: private */
    /* renamed from: fN */
    public void m10976fN() {
        Material material = (Material) this.f1932lV.mo3047bT("data/gui/imageset/radar/radar_frame.png/material");
        Material material2 = (Material) this.f1932lV.mo3047bT("data/gui/imageset/radar/radar_frame_shader.png/material");
        material.setShader(this.f1946mk);
        material2.setShader(this.f1946mk);
        this.f1947ml = new RBillboard();
        this.f1947ml.setAlignment(BillboardAlignment.USER_ALIGNED);
        this.f1947ml.setSize(new Vector2fWrap(316.0f, 316.0f));
        this.f1947ml.setMaterial(material);
        this.f1947ml.setRenderPriority(90);
        this.f1948mm = new RBillboard();
        this.f1948mm.setAlignment(BillboardAlignment.USER_ALIGNED);
        this.f1948mm.setSize(new Vector2fWrap(316.0f, 316.0f));
        this.f1948mm.setMaterial(material2);
        this.f1948mm.setRenderPriority(100);
        this.f1933lW.addChild(this.f1947ml);
        this.f1933lW.addChild(this.f1948mm);
    }

    /* renamed from: fO */
    private void m10977fO() {
        this.f1932lV.mo3044b(this.f1944mi);
        this.f1931lU = null;
        this.f1933lW.removeAllChildren();
        m10976fN();
        this.f1933lW.addChild(this.f1935lY);
        this.f1933lW.addChild(this.f1936lZ);
        this.f1942mg.clear();
        this.f1938mb = false;
        if (this.f1950mo != null) {
            this.f1950mo.setVisible(false);
        }
        this.f1932lV.mo3046b(this.sceneView);
    }

    /* renamed from: fP */
    private void m10978fP() {
        Taikodom ala = this.f1930kj.ala();
        if (ala != null) {
            this.f1938mb = false;
            this.f1937ma = false;
            if (this.f1950mo != null) {
                this.f1950mo.setVisible(false);
            }
            this.f1931lU = ala.aPC().bQx();
            this.f1932lV.mo3004a(this.f1944mi);
            this.f1941mf.mo6477v(this.f1939md[this.f1940me].getRadius());
            this.f1932lV.mo3005a(this.sceneView);
            this.sceneView.getScene().setName("radar scene");
            this.sceneView.disable();
        }
    }

    /* renamed from: fQ */
    public boolean mo6466fQ() {
        if (this.f1940me < 1) {
            return true;
        }
        m10959N(this.f1940me - 1);
        return false;
    }

    /* renamed from: fR */
    public void mo6467fR() {
        if (this.f1940me < f1929mc - 1) {
            m10959N(this.f1940me + 1);
        }
    }

    public void hide() {
        if (this.f1943mh) {
            this.f1938mb = false;
            this.f1950mo.setVisible(false);
            this.sceneView.disable();
        }
    }

    public void show() {
        if (this.f1943mh) {
            this.f1938mb = true;
            this.f1937ma = true;
            this.f1950mo.setVisible(true);
            this.sceneView.enable();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: N */
    public void m10959N(int i) {
        int i2 = this.f1940me;
        this.f1940me = i;
        this.f1941mf.mo6476a(this.f1939md[this.f1940me].getRadius(), 0.1f);
        if (this.f1950mo != null) {
            try {
                this.f1950mo.mo4917cf("radar-range").setText(String.valueOf((int) (this.f1939md[this.f1940me].getRadius() / 1000.0f)) + "Km");
                this.f1950mo.mo4913cb("radar-button-" + (i + 1)).setSelected(true);
                this.f1950mo.mo4913cb("radar-button-" + (i2 + 1)).setSelected(false);
            } catch (Exception e) {
                System.out.println("Wrong names for the components into hudRadar.xml");
            }
        }
    }

    /* renamed from: fS */
    public void mo6468fS() {
        this.f1939md[0].setRadius(1200.0f);
        this.f1939md[0].mo6481an(Station.class);
        this.f1939md[0].mo6481an(Asteroid.class);
        this.f1939md[0].mo6481an(Outpost.class);
        this.f1939md[0].mo6481an(Missile.class);
        this.f1939md[0].mo6481an(Gate.class);
        this.f1939md[0].mo6481an(Bomber.class);
        this.f1939md[0].mo6481an(Fighter.class);
        this.f1939md[0].mo6481an(Freighter.class);
        this.f1939md[0].mo6481an(Explorer.class);
        this.f1939md[0].mo6481an(BattleCruiser.class);
        this.f1939md[0].mo6481an(Ship.class);
        this.f1939md[0].mo6480a(C1496b.ALLIED);
        this.f1939md[0].mo6480a(C1496b.ENEMY);
        this.f1939md[0].mo6480a(C1496b.NEUTRAL);
        this.f1939md[1].setRadius(4500.0f);
        this.f1939md[1].mo6481an(Station.class);
        this.f1939md[1].mo6481an(Asteroid.class);
        this.f1939md[1].mo6481an(Outpost.class);
        this.f1939md[1].mo6481an(Missile.class);
        this.f1939md[1].mo6481an(Gate.class);
        this.f1939md[1].mo6481an(Bomber.class);
        this.f1939md[1].mo6481an(Fighter.class);
        this.f1939md[1].mo6481an(Freighter.class);
        this.f1939md[1].mo6481an(Explorer.class);
        this.f1939md[1].mo6481an(BattleCruiser.class);
        this.f1939md[1].mo6481an(Ship.class);
        this.f1939md[1].mo6480a(C1496b.ALLIED);
        this.f1939md[1].mo6480a(C1496b.ENEMY);
        this.f1939md[1].mo6480a(C1496b.NEUTRAL);
        this.f1939md[2].setRadius(12000.0f);
        this.f1939md[2].mo6481an(Station.class);
        this.f1939md[2].mo6481an(Asteroid.class);
        this.f1939md[2].mo6481an(Outpost.class);
        this.f1939md[2].mo6481an(Gate.class);
        this.f1939md[2].mo6481an(Missile.class);
        this.f1939md[2].mo6481an(Bomber.class);
        this.f1939md[2].mo6481an(Fighter.class);
        this.f1939md[2].mo6481an(Freighter.class);
        this.f1939md[2].mo6481an(Explorer.class);
        this.f1939md[2].mo6481an(BattleCruiser.class);
        this.f1939md[2].mo6481an(Ship.class);
        this.f1939md[2].mo6480a(C1496b.ALLIED);
        this.f1939md[2].mo6480a(C1496b.ENEMY);
        this.f1939md[2].mo6480a(C1496b.NEUTRAL);
        this.f1939md[3].setRadius(30000.0f);
        this.f1939md[3].mo6481an(Station.class);
        this.f1939md[3].mo6481an(Outpost.class);
        this.f1939md[3].mo6481an(Missile.class);
        this.f1939md[3].mo6481an(Gate.class);
        this.f1939md[3].mo6481an(Bomber.class);
        this.f1939md[3].mo6481an(Fighter.class);
        this.f1939md[3].mo6481an(Freighter.class);
        this.f1939md[3].mo6481an(Explorer.class);
        this.f1939md[3].mo6481an(BattleCruiser.class);
        this.f1939md[3].mo6481an(Ship.class);
        this.f1939md[3].mo6480a(C1496b.ALLIED);
        this.f1939md[3].mo6480a(C1496b.ENEMY);
        this.f1939md[3].mo6480a(C1496b.NEUTRAL);
        this.f1939md[4].setRadius(80000.0f);
        this.f1939md[4].mo6481an(Station.class);
        this.f1939md[4].mo6481an(Outpost.class);
        this.f1939md[4].mo6481an(Gate.class);
        this.f1939md[4].mo6481an(Bomber.class);
        this.f1939md[4].mo6481an(BattleCruiser.class);
        this.f1939md[4].mo6480a(C1496b.ALLIED);
        this.f1939md[4].mo6480a(C1496b.ENEMY);
        this.f1939md[4].mo6480a(C1496b.NEUTRAL);
    }

    public boolean isVisible() {
        return this.f1938mb;
    }

    public void destroy() {
        m10977fO();
    }

    /* renamed from: g */
    public void mo6470g(Actor cr) {
        this.f1949mn = cr;
        this.f1936lZ.setRender(false);
    }

    /* renamed from: a */
    public void mo6460a(C1276Sr sr) {
        this.f1950mo = (C2698il) sr;
        this.f1950mo.mo4913cb("radar-button-1").addActionListener(new C1499e());
        this.f1950mo.mo4913cb("radar-button-2").addActionListener(new C1502h());
        this.f1950mo.mo4913cb("radar-button-3").addActionListener(new C1503i());
        this.f1950mo.mo4913cb("radar-button-4").addActionListener(new C1504j());
        this.f1950mo.mo4913cb("radar-button-5").addActionListener(new C1505k());
        this.f1950mo.mo4917cf("radar-range").setText(String.valueOf((int) (this.f1939md[this.f1940me].getRadius() / 1000.0f)) + "Km");
        this.f1950mo.setVisible(this.f1938mb);
        this.f1950mo.setLocation((this.f1930kj.ale().aes() / 2) - (this.f1950mo.getHeight() / 2), (((this.f1930kj.ale().aet() / 2) - 158) - this.f1950mo.getWidth()) + 8);
    }

    /* renamed from: fT */
    public boolean mo6469fT() {
        return this.f1943mh;
    }

    /* renamed from: s */
    public void mo6474s(boolean z) {
        this.f1943mh = z;
    }

    /* renamed from: a.W$b */
    /* compiled from: a */
    enum C1496b {
        ALLIED,
        NEUTRAL,
        ENEMY,
        NOT_VISIBLE
    }

    /* renamed from: a.W$c */
    /* compiled from: a */
    private class C1497c {
        private Set<Class> fWQ;
        private Set<C1496b> fWR;
        private float radius;

        private C1497c() {
            this.fWQ = new HashSet();
            this.fWR = new HashSet();
        }

        /* synthetic */ C1497c(C1494W w, C1497c cVar) {
            this();
        }

        /* renamed from: a */
        public void mo6480a(C1496b bVar) {
            this.fWR.add(bVar);
        }

        /* renamed from: b */
        public void mo6483b(C1496b bVar) {
            this.fWR.remove(bVar);
        }

        /* renamed from: an */
        public void mo6481an(Class cls) {
            this.fWQ.add(cls);
        }

        /* renamed from: ao */
        public void mo6482ao(Class cls) {
            this.fWQ.remove(cls);
        }

        public float getRadius() {
            return this.radius;
        }

        public void setRadius(float f) {
            this.radius = f;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C1496b mo6479a(Actor cr, Vec3d ajr, Vec3d ajr2, float f) {
            if (!this.fWQ.contains(cr.getClass()) || Vec3d.m15654o(ajr, ajr2) >= ((double) f)) {
                return C1496b.NOT_VISIBLE;
            }
            return C1494W.this.m10968d(cr);
        }
    }

    /* renamed from: a.W$a */
    private class C1495a {
        float currentTime;

        /* renamed from: gr */
        float f1954gr;

        /* renamed from: gs */
        float f1955gs;

        /* renamed from: gt */
        float f1956gt;

        private C1495a() {
            this.f1956gt = 1.0f;
        }

        /* synthetic */ C1495a(C1494W w, C1495a aVar) {
            this();
        }

        /* renamed from: v */
        public void mo6477v(float f) {
            this.f1955gs = f;
            this.f1954gr = f;
            this.f1956gt = 1.0f;
            this.currentTime = 0.0f;
        }

        /* renamed from: a */
        public void mo6476a(float f, float f2) {
            this.f1955gs = mo6478w(0.0f);
            this.currentTime = 0.0f;
            this.f1956gt = f2;
            this.f1954gr = f;
        }

        /* renamed from: w */
        public float mo6478w(float f) {
            this.currentTime += f;
            float f2 = this.currentTime / this.f1956gt;
            if (f2 > 1.0f) {
                return this.f1954gr;
            }
            if (f2 < 0.0f) {
                return this.f1955gs;
            }
            return (f2 * (this.f1954gr - this.f1955gs)) + this.f1955gs;
        }
    }

    /* renamed from: a.W$f */
    /* compiled from: a */
    class C1500f implements RenderTask {
        public RBillboard hCR;

        /* renamed from: pI */
        public Actor f1962pI;

        C1500f(RBillboard rBillboard, Actor cr) {
            this.hCR = rBillboard;
            this.f1962pI = cr;
        }

        public void run(RenderView renderView) {
            C1494W.this.m10964a(this.hCR, this.f1962pI);
            Material material = this.hCR.getMaterial();
            this.hCR.setSize((float) ((int) Math.abs(material.getDiffuseTexture().getTransformedWidth() * material.getXScaling())), (float) ((int) Math.abs(material.getYScaling() * material.getDiffuseTexture().getTransformedHeight())));
            C1494W.this.f1933lW.addChild(this.hCR);
        }
    }

    /* renamed from: a.W$g */
    /* compiled from: a */
    class C1501g implements RenderTask {
        C1501g() {
        }

        public void run(RenderView renderView) {
            try {
                C1494W.this.f1932lV.aej().loadFile("data/gui/imageset/radar/radar_frame.png");
                C1494W.this.f1932lV.aej().loadFile("data/gui/imageset/radar/radar_neutral.png");
                C1494W.this.f1932lV.aej().loadFile("data/gui/imageset/radar/radar_station.png");
                C1494W.this.f1932lV.aej().loadFile("data/gui/imageset/radar/radar_portal.png");
                C1494W.this.f1932lV.aej().loadFile("data/gui/imageset/radar/radar_missile.png");
                C1494W.this.f1932lV.aej().loadFile("data/gui/imageset/radar/radar_ship.png");
                C1494W.this.f1932lV.aej().loadFile("data/gui/imageset/radar/radar_selection.png");
                C1494W.this.f1932lV.aej().loadFile("data/gui/imageset/radar/radar_out_of_range.png");
                C1494W.this.f1932lV.aej().loadFile("data/gui/imageset/radar/radar_selection_oor.png");
                C1494W.this.f1932lV.aej().loadFile("data/gui/imageset/radar/radar_frame_shader.png");
                C1494W.this.f1932lV.aej().loadMissing();
            } catch (IOException e) {
                System.out.println("Unable to load radar images!!!");
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a.W$d */
    /* compiled from: a */
    class C1498d implements RenderTask {
        private final /* synthetic */ int hCP;
        private final /* synthetic */ int hCQ;

        C1498d(int i, int i2) {
            this.hCP = i;
            this.hCQ = i2;
        }

        public void run(RenderView renderView) {
            C1494W.this.m10970d(this.hCP, this.hCQ);
            C1494W.this.m10976fN();
        }
    }

    /* renamed from: a.W$e */
    /* compiled from: a */
    class C1499e implements ActionListener {
        C1499e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C1494W.this.m10959N(0);
        }
    }

    /* renamed from: a.W$h */
    /* compiled from: a */
    class C1502h implements ActionListener {
        C1502h() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C1494W.this.m10959N(1);
        }
    }

    /* renamed from: a.W$i */
    /* compiled from: a */
    class C1503i implements ActionListener {
        C1503i() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C1494W.this.m10959N(2);
        }
    }

    /* renamed from: a.W$j */
    /* compiled from: a */
    class C1504j implements ActionListener {
        C1504j() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C1494W.this.m10959N(3);
        }
    }

    /* renamed from: a.W$k */
    /* compiled from: a */
    class C1505k implements ActionListener {
        C1505k() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C1494W.this.m10959N(4);
        }
    }
}
