package p001a;

import game.engine.DataGameEvent;
import game.network.channel.Connect;
import game.network.channel.client.ClientConnect;
import game.network.channel.client.ClientConnectImpl;
import game.network.channel.client.HandlerClientState;
import game.network.exception.ChechUserException;
import game.script.Taikodom;
import game.script.login.UserConnection;
import game.script.player.User;
import logic.baa.C1616Xf;
import logic.bbb.aDR;
import logic.thred.LogPrinter;

import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: a.xp */
/* compiled from: a */
public class WraperManagerGameEvent implements Connect {
    static LogPrinter log = LogPrinter.m10275K(WraperManagerGameEvent.class);
    private final DataGameEvent aGA;
    private final Taikodom bFZ;
    ConcurrentHashMap<ClientConnect, User> bGa = new ConcurrentHashMap<>();
    Map<ClientConnect, User> bGb = Collections.synchronizedMap(new WeakHashMap());
    ConcurrentHashMap<UserConnection, BlockingQueue<Object>> bGc = new ConcurrentHashMap<>();

    public WraperManagerGameEvent(DataGameEvent jz, Taikodom dn) {
        this.aGA = jz;
        this.bFZ = dn;
    }


    public void chechUser(ClientConnect paramb, String userName, String pass, boolean isForcingNewUser) throws ChechUserException {
        User localaDk = getUser(userName, pass);
        if (localaDk == null) {
            if (C5877acF.RUNNING_CLIENT_BOT) {
                localaDk = this.bFZ.getUserBot(paramb, userName, pass);
            } else {
                logger.error("Invalid user " + userName + " from " + paramb);
                throw new ChechUserException(Connect.Status.INVALID_USERNAME_PASSWORD);
            }
        }
        Connect.Status locala = this.bFZ.mo1419b(localaDk, userName, pass);
        if (locala != Connect.Status.OK) {
            throw new ChechUserException(locala);
        }
        int count = this.bFZ.mo1500f(localaDk);
        if (count >= 0) {
            throw new ChechUserException(Connect.Status.SERVER_FULL, count);
        }
        long l = System.currentTimeMillis();
        /* apK*/
        UserConnection localapK1 = localaDk.cSV();
        int j = 0;
        if ((localapK1 != null) && (localapK1.bFs() != null) && (localapK1.bFs().bgt() != null)) {
            Object localObject = (HandlerClientState) ((ClientConnectImpl) localapK1.bFs().bgt()).getHandlerClientState();
            if (
                (localObject != null)
                    && (((HandlerClientState) localObject).getCurrentStepLoging() == HandlerClientState.StepLoging.UP)
                    && (l - ((HandlerClientState) localObject).getLastTimeBufferReader() < 60000L)
            ) {
                j = 1;
            } else if (localObject != null) {
                try {
                    ((HandlerClientState) localObject).getSelectionKey().channel().close();
                } catch (Exception localException1) {
                    logger.error(localException1);
                }
                try {
                    ((HandlerClientState) localObject).stepDisconnected();
                } catch (Exception localException2) {
                    logger.error(localException2);
                }
            }
        }
        if (j != 0) {
            if (isForcingNewUser) {
                this.aGA.bGU().authorizationChannelClose(localapK1.bFs().bgt());
            } else {
                logger.info("user " + userName + " [connected from " + localapK1.bFs().bgt() + "] trying to reconnect from " + paramb);

                throw new ChechUserException(Connect.Status.USER_ALREADY_LOGGED_IN);
            }
        }
        Object localObject = (User) this.bGa.putIfAbsent(paramb, localaDk);
        this.bGb.put(paramb, localaDk);
        if (localObject != null) {
            logger.info("user " + userName + " concurrent userName detected.");

            throw new ChechUserException(Connect.Status.USER_ALREADY_LOGGED_IN);
        }
        aDR localaDR = this.aGA.bGJ().mo6442f(paramb);

        logger.info("user " + userName + " connected from " + localaDR.bgt());

        UserConnection localapK2 = localaDk.mo8486u(localaDR);

        this.aGA.mo3341a(localaDR, (C1616Xf) localapK2, localaDk.cST());
    }

    /* access modifiers changed from: protected */
    /* renamed from: u */
    public User getUser(String login, String pass) {
        return this.bFZ.mo1497eg(login);
    }

    /* renamed from: d */
    public void userDisconnected(ClientConnect bVar) {
        UserConnection cSV;
        BlockingQueue blockingQueue;
        User remove = this.bGa.remove(bVar);
        User adk = this.bGb.get(bVar);
        if (remove != null || adk == null) {
            adk = remove;
        } else {
            log.info("User already disconnected? " + adk + " " + bVar + "  ");
        }
        if (adk == null) {
            log.info("unknown address " + bVar + " disconnected");
            cSV = null;
        } else {
            cSV = adk.cSV();
            if (cSV == null) {
                log.warn("User '" + adk.getUsername() + "', oid " + adk.cWm() + " has no current connection");
            } else if (cSV.bFs() == null || cSV.bFs().bgt() == bVar) {
                log.info("user " + adk + " disconnected from " + bVar);
                this.bFZ.mo1519h(adk);
                adk.cSZ();
            } else {
                log.info("User is already bound to a new connection: " + adk + " current: " + cSV.bFs().bgt() + " old:" + bVar + "  ");
                this.bFZ.aJu().mo7842b(adk, bVar);
            }
        }
        this.aGA.mo3449i(bVar);
        if (cSV != null && (blockingQueue = this.bGc.get(cSV)) != null) {
            try {
                blockingQueue.put(new Object());
            } catch (InterruptedException e) {
            }
        }
    }
}
