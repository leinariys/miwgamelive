package p001a;

import java.io.Flushable;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

/* renamed from: a.lR */
/* compiled from: a */
public class C2883lR extends DeflaterOutputStream implements Flushable {
    private static final byte[] aAC = new byte[0];

    public C2883lR(OutputStream outputStream, Deflater deflater, int i) {
        super(outputStream, deflater, i);
    }

    public C2883lR(OutputStream outputStream, Deflater deflater) {
        super(outputStream, deflater);
    }

    public C2883lR(OutputStream outputStream) {
        this(outputStream, -1, 0, 4096);
    }

    public C2883lR(OutputStream outputStream, int i, int i2, int i3) {
        this(outputStream, new Deflater(i, true), i3);
    }

    public void flush() {
        if (!this.def.finished()) {
            this.def.setInput(aAC, 0, 0);
            this.def.setLevel(0);
            deflate();
            this.def.setLevel(-1);
            deflate();
            super.flush();
        }
    }

    /* access modifiers changed from: protected */
    public void deflate() {
        while (true) {
            try {
                int deflate = this.def.deflate(this.buf, 0, this.buf.length);
                if (deflate > 0) {
                    this.out.write(this.buf, 0, deflate);
                } else {
                    return;
                }
            } catch (NullPointerException e) {
                throw new IOException("deflater was ended");
            }
        }
    }
}
