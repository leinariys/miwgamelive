package p001a;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.vL */
/* compiled from: a */
public abstract class C3813vL extends C5342aFq {
    public boolean bzP;

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public abstract void mo7919d(ObjectOutput objectOutput, Object obj);

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public abstract Object mo7920f(ObjectInput objectInput);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo3590a(Class<?> cls, Class<?>[] clsArr) {
        super.mo3590a(cls, clsArr);
        this.bzP = !cls.isPrimitive();
    }

    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) {
        if (this.bzP) {
            if (obj == null) {
                objectOutput.writeBoolean(false);
                return;
            }
            objectOutput.writeBoolean(true);
        }
        mo7919d(objectOutput, obj);
    }

    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) {
        if (!this.bzP || objectInput.readBoolean()) {
            return mo7920f(objectInput);
        }
        return null;
    }
}
