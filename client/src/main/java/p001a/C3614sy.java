package p001a;

/* renamed from: a.sy */
/* compiled from: a */
public class C3614sy {
    /* renamed from: A */
    public static String m39218A(C3892wO wOVar) {
        if (wOVar == null) {
            return null;
        }
        return wOVar.toString();
    }

    /* renamed from: br */
    public static C3892wO m39219br(String str) {
        if (str == null || "null".equals(str) || "".equals(str)) {
            return null;
        }
        if (str.contains("%")) {
            return new C3892wO(Float.parseFloat(str.substring(0, str.length() - 1)), C3892wO.C3893a.PERCENT);
        }
        return new C3892wO(Float.parseFloat(str), C3892wO.C3893a.ABSOLUTE);
    }
}
