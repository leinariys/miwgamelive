package p001a;

import logic.baa.C1616Xf;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.ata  reason: case insensitive filesystem */
/* compiled from: a */
public class C6782ata extends C3813vL {
    private static final Short gxP = 0;

    /* renamed from: d */
    public void mo7919d(ObjectOutput objectOutput, Object obj) {
        objectOutput.writeShort(((Short) obj).shortValue());
    }

    /* renamed from: f */
    public Object mo7920f(ObjectInput objectInput) {
        return Short.valueOf(objectInput.readShort());
    }

    /* renamed from: b */
    public Object mo3591b(C1616Xf xf) {
        return gxP;
    }
}
