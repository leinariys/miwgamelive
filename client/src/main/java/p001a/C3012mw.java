package p001a;

import game.network.message.ByteMessageReader;
import game.network.message.TransportCommand;
import game.network.message.TransportResponse;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.AttributeKey;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

import java.io.Externalizable;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Iterator;
import java.util.Map;

/* renamed from: a.mw */
/* compiled from: a */
public final class C3012mw extends CumulativeProtocolDecoder {
    private static final Object dVo = new AttributeKey(C3012mw.class, "context");

    /* access modifiers changed from: protected */
    public boolean doDecode(IoSession ioSession, IoBuffer ioBuffer, ProtocolDecoderOutput protocolDecoderOutput) {
        C3013a aVar;
        Class cls;
        Object transportResponse;
        C3013a aVar2 = (C3013a) ioSession.getAttribute(dVo);
        if (aVar2 == null) {
            C3013a aVar3 = new C3013a();
            ioSession.setAttribute(dVo, aVar3);
            aVar = aVar3;
        } else {
            aVar = aVar2;
        }
        if (aVar.aDi == -1) {
            if (ioBuffer.remaining() < 4) {
                return false;
            }
            aVar.aDi = ioBuffer.getInt();
            if (ioBuffer.remaining() < aVar.aDi) {
                return true;
            }
        } else if (ioBuffer.remaining() < aVar.aDi) {
            return false;
        }
        if (OpenSocketMina.m33375a(ioSession).bLH() == OpenSocketMina.C2680a.UP) {
            byte[] bArr = new byte[aVar.aDi];
            ioBuffer.get(bArr, 0, aVar.aDi);
            ByteMessageReader amf = new ByteMessageReader(bArr, 0, aVar.aDi);
            switch (amf.readBits(2)) {
                case 0:
                    transportResponse = new TransportCommand(amf);
                    break;
                case 1:
                    transportResponse = new TransportResponse(amf);
                    break;
                default:
                    throw new IllegalArgumentException();
            }
            protocolDecoderOutput.write(transportResponse);
        } else {
            int i = ioBuffer.getInt();
            Iterator<Map.Entry<Class<?>, Integer>> it = OpenSocketMina.classes.entrySet().iterator();
            while (true) {
                if (it.hasNext()) {
                    Map.Entry next = it.next();
                    if (i == ((Integer) next.getValue()).intValue()) {
                        cls = (Class) next.getKey();
                        break;
                    }
                } else {
                    cls = null;
                    break;
                }
            }
            if (cls == null) {
                throw new IOException("unknown class id " + i);
            }
            Externalizable externalizable = (Externalizable) cls.newInstance();
            externalizable.readExternal(new C6765atJ(ioBuffer, aVar.aDj));
            protocolDecoderOutput.write(externalizable);
        }
        aVar.aDi = -1;
        return true;
    }

    /* renamed from: a.mw$a */
    static final class C3013a {
        /* access modifiers changed from: private */
        public int aDi = -1;
        /* access modifiers changed from: private */
        public CharsetDecoder aDj = Charset.forName("UTF-8").newDecoder();

        C3013a() {
        }
    }
}
