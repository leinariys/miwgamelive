package p001a;

import game.geometry.Quat4fWrap;
import logic.baa.C1616Xf;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aIL */
/* compiled from: a */
public class aIL extends C5342aFq {
    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) {
        if (obj == null) {
            objectOutput.writeBoolean(false);
            return;
        }
        objectOutput.writeBoolean(true);
        Quat4fWrap aoy = (Quat4fWrap) obj;
        objectOutput.writeFloat(aoy.x);
        objectOutput.writeFloat(aoy.y);
        objectOutput.writeFloat(aoy.z);
        objectOutput.writeFloat(aoy.w);
    }

    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) {
        if (!objectInput.readBoolean()) {
            return null;
        }
        return new Quat4fWrap(objectInput.readFloat(), objectInput.readFloat(), objectInput.readFloat(), objectInput.readFloat());
    }

    /* renamed from: b */
    public Object mo3591b(C1616Xf xf) {
        return null;
    }
}
