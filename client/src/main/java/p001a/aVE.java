package p001a;

import game.script.Character;
import game.script.avatar.AvatarSector;
import game.script.avatar.cloth.Cloth;
import game.script.resource.Asset;
import logic.res.sound.C0907NJ;
import taikodom.render.scene.RModel;

import javax.swing.*;
import java.util.Collection;

/* renamed from: a.aVE */
/* compiled from: a */
public interface aVE {
    Collection<AvatarSector> aFI();

    float aOD();

    ImageIcon aOk();

    C5207aAl aOm();

    RModel aOo();

    Cloth aOs();

    Collection<Cloth> aOu();

    Cloth aOw();

    String aOy();

    Character agj();

    /* renamed from: ak */
    void mo2026ak(Asset tCVar);

    /* renamed from: b */
    void mo2027b(C0907NJ nj);

    /* renamed from: b */
    void mo2028b(C1009Os.C1019i iVar);

    /* renamed from: cJ */
    Asset mo2030cJ();

    /* renamed from: d */
    void mo2031d(ImageIcon imageIcon);

    float getHeight();

    /* renamed from: qV */
    void mo2034qV();

    /* renamed from: r */
    void mo2035r(Cloth wj);

    /* renamed from: t */
    void mo2036t(Cloth wj);
}
