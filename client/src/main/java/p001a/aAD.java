package p001a;

import game.DataNetworkInformation;
import game.engine.DataGameEvent;
import logic.baa.C1616Xf;

import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.io.OutputStream;
import java.io.Serializable;

/* renamed from: a.aAD */
/* compiled from: a */
public class aAD extends ObjectOutputStream implements C6744aso {
    static final boolean heM = true;
    public final DataGameEvent aGA;
    private boolean aGB;
    private int aGC;
    private boolean aVN;
    private boolean cWu;

    public aAD(DataGameEvent jz, OutputStream outputStream) {
        super(outputStream);
        this.aGB = false;
        this.cWu = false;
        this.aGC = 255;
        this.aVN = false;
        this.aGA = jz;
        enableReplaceObject(true);
    }

    public aAD(DataGameEvent jz, OutputStream outputStream, boolean z, boolean z2, int i, boolean z3) {
        this(jz, outputStream);
        this.aGB = z;
        this.cWu = z2;
        this.aGC = i;
        this.aVN = z3;
    }

    /* renamed from: Xc */
    public boolean mo2092Xc() {
        return this.aVN;
    }

    /* renamed from: PN */
    public int mo2091PN() {
        return this.aGC;
    }

    /* access modifiers changed from: protected */
    public void writeStreamHeader() {
    }

    /* access modifiers changed from: protected */
    public Object replaceObject(Object obj) {
        if (obj instanceof DataNetworkInformation) {
            return this.aGA.mo3346a((DataNetworkInformation) obj);
        }
        return super.replaceObject(obj);
    }

    /* renamed from: f */
    public void mo2095f(C1616Xf xf) {
        if (xf == null) {
            this.aGA.bxy().mo12008a(this, -1);
            return;
        }
        Class<?> cls = xf.getClass();
        int magicNumber = this.aGA.bxy().getMagicNumber(cls);
        if (magicNumber == -1) {
            throw new IllegalStateException("No magic number for game.script " + cls);
        }
        this.aGA.bxy().mo12008a(this, magicNumber);
        writeLong(xf.bFf().getObjectId().getId());
    }

    /* access modifiers changed from: protected */
    public void writeClassDescriptor(ObjectStreamClass objectStreamClass) {
        Class<?> forClass = objectStreamClass.forClass();
        C6656arE bxy = this.aGA.bxy();
        int magicNumber = bxy.getMagicNumber(forClass);
        if (magicNumber == -1) {
            if (this.cWu) {
                String name = forClass.getName();
                if (this.aGB) {
                    name = bxy.mo12015jL(name);
                }
                if (Serializable.class.isAssignableFrom(forClass) && name != null) {
                    bxy.mo12008a(this, -2);
                    writeUTF(name);
                    return;
                }
            }
            bxy.mo12008a(this, -1);
            super.writeClassDescriptor(objectStreamClass);
            return;
        }
        bxy.mo12008a(this, magicNumber);
    }

    /* renamed from: PM */
    public DataGameEvent mo2090PM() {
        return this.aGA;
    }
}
