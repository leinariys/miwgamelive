package p001a;

import java.util.List;
import java.util.concurrent.Executor;

/* renamed from: a.mV */
/* compiled from: a */
public interface C2976mV {
    public static final String aGx = "EventManager";

    /* renamed from: a */
    void mo8719a(C2776jq jqVar);

    /* renamed from: a */
    void mo8720a(C3028nC nCVar, C5961adl adl);

    /* renamed from: a */
    void mo8721a(C3028nC nCVar, List<C5961adl> list);

    /* renamed from: a */
    boolean mo8722a(float f, Executor executor);

    /* renamed from: b */
    void mo8723b(C2776jq jqVar);

    /* renamed from: b */
    void mo8724b(C3028nC nCVar, C5961adl adl);

    /* renamed from: b */
    void mo8725b(C3028nC nCVar, List<C5961adl> list);

    boolean step(float f);
}
