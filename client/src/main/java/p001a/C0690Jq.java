package p001a;

import logic.ui.C2698il;
import taikodom.addon.IAddonProperties;

/* renamed from: a.Jq */
/* compiled from: a */
public abstract class C0690Jq {
    public C2698il djv;

    /* renamed from: kj */
    public IAddonProperties f880kj;

    public C0690Jq(IAddonProperties vWVar, C2698il ilVar) {
        this.f880kj = vWVar;
        this.djv = ilVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: Fa */
    public abstract void mo525Fa();

    /* access modifiers changed from: package-private */
    public abstract void refresh();

    /* access modifiers changed from: package-private */
    public abstract void removeListeners();
}
