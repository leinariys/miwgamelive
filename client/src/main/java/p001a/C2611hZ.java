package p001a;

/* renamed from: a.hZ */
/* compiled from: a */
public enum C2611hZ {
    LEVEL_1,
    LEVEL_2,
    LEVEL_3,
    LEVEL_4,
    LEVEL_5;

    /* renamed from: Ac */
    public static C2611hZ m32750Ac() {
        if (values().length != 0) {
            return values()[0];
        }
        throw new IllegalStateException("OutpostLevel has no values");
    }

    /* renamed from: Aa */
    public C2611hZ mo19280Aa() {
        return mo19281Ab() ? this : values()[ordinal() + 1];
    }

    /* renamed from: Ab */
    public boolean mo19281Ab() {
        return ordinal() == values().length + -1;
    }
}
