package p001a;

import java.util.AbstractList;
import java.util.Queue;

/* renamed from: a.aKd  reason: case insensitive filesystem */
/* compiled from: a */
public class C5459aKd<T> extends AbstractList<T> implements Queue<T> {
    private int capacity;
    private int head = 0;
    private T[] queue;
    private int tail = 0;

    public C5459aKd(int i) {
        this.capacity = i + 1;
        this.queue = new Object[(i + 1)];
    }

    public int capacity() {
        return this.capacity - 1;
    }

    public boolean add(T t) {
        int i = (this.tail + 1) % this.capacity;
        if (i == this.head) {
            throw new IndexOutOfBoundsException("Queue full");
        }
        this.queue[this.tail] = t;
        this.tail = i;
        this.modCount++;
        return true;
    }

    public T remove(int i) {
        if (i != 0) {
            throw new IllegalArgumentException("Can only remove head of queue");
        } else if (this.head == this.tail) {
            throw new IndexOutOfBoundsException("Queue empty");
        } else {
            T t = this.queue[this.head];
            this.queue[this.head] = null;
            this.head = (this.head + 1) % this.capacity;
            this.modCount++;
            return t;
        }
    }

    public T get(int i) {
        int size = size();
        if (i < 0 || i >= size) {
            throw new IndexOutOfBoundsException("Index " + i + ", queue size " + size);
        }
        return this.queue[(this.head + i) % this.capacity];
    }

    public int size() {
        int i = this.tail - this.head;
        if (i < 0) {
            return i + this.capacity;
        }
        return i;
    }

    public T element() {
        if (this.head != this.tail) {
            return this.queue[this.head];
        }
        throw new IndexOutOfBoundsException("Queue empty");
    }

    public boolean offer(T t) {
        if ((this.tail + 1) % this.capacity == this.head) {
            return false;
        }
        return add(t);
    }

    public T peek() {
        if (size() == 0) {
            return null;
        }
        return element();
    }

    public T poll() {
        if (size() > 0) {
            return remove();
        }
        return null;
    }

    public T remove() {
        if (this.head == this.tail) {
            throw new IndexOutOfBoundsException("Queue empty");
        }
        T t = this.queue[this.head];
        this.queue[this.head] = null;
        this.head = (this.head + 1) % this.capacity;
        this.modCount++;
        return t;
    }
}
