package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import logic.bbb.C4029yK;

import javax.vecmath.Tuple3f;

/* renamed from: a.aaM  reason: case insensitive filesystem */
/* compiled from: a */
public class C5780aaM {
    private final Vec3f aAq = new Vec3f();
    private final Vec3f aAr = new Vec3f();
    private final Matrix4fWrap eXa = new Matrix4fWrap();
    private final Matrix4fWrap eXb = new Matrix4fWrap();
    private C4029yK aAo;
    private C4029yK aAp;

    public C5780aaM() {
    }

    public C5780aaM(C4029yK yKVar, C4029yK yKVar2, Matrix4fWrap ajk) {
        mo12175a(yKVar, yKVar2);
        mo12182f(ajk);
    }

    public Matrix4fWrap bML() {
        return this.eXa;
    }

    /* renamed from: a */
    public void mo12176a(Vec3f vec3f, C1422Up up) {
        this.aAo.getFarthest(vec3f, up.bxH());
        this.eXb.mo13987a((Tuple3f) vec3f, (Tuple3f) this.aAq);
        this.aAq.negate();
        this.aAp.getFarthest(this.aAq, this.aAr);
        this.eXa.mo14002b((Tuple3f) this.aAr, (Tuple3f) this.aAq);
        up.bxH().mo23513o(this.aAq);
        up.setDirection(vec3f);
    }

    public void getFarthest(Vec3f vec3f, Vec3f vec3f2) {
        this.aAo.getFarthest(vec3f, vec3f2);
        this.eXb.mo13987a((Tuple3f) vec3f, (Tuple3f) this.aAq);
        this.aAq.negate();
        this.aAp.getFarthest(this.aAq, this.aAr);
        this.eXa.mo14002b((Tuple3f) this.aAr, (Tuple3f) this.aAq);
        vec3f2.mo23513o(this.aAq);
    }

    /* renamed from: b */
    public void mo12177b(Vec3f vec3f, C1422Up up) {
        this.aAo.getFarthest(vec3f, up.bxH());
        up.setDirection(vec3f);
    }

    /* renamed from: u */
    public void mo12184u(Vec3f vec3f, Vec3f vec3f2) {
        this.aAo.getFarthest(vec3f, vec3f2);
    }

    /* renamed from: c */
    public void mo12180c(Vec3f vec3f, C1422Up up) {
        this.eXb.mo13987a((Tuple3f) vec3f, (Tuple3f) this.aAq);
        this.aAq.negate();
        this.aAp.getFarthest(this.aAq, this.aAr);
        this.eXa.mo14002b((Tuple3f) this.aAr, (Tuple3f) this.aAq);
        up.bxH().mo23513o(this.aAq);
        up.setDirection(vec3f);
    }

    /* renamed from: v */
    public void mo12185v(Vec3f vec3f, Vec3f vec3f2) {
        this.eXb.mo13987a((Tuple3f) vec3f, (Tuple3f) this.aAq);
        this.aAq.negate();
        this.aAp.getFarthest(this.aAq, this.aAr);
        this.eXa.mo14002b((Tuple3f) this.aAr, (Tuple3f) this.aAq);
        vec3f2.mo23513o(this.aAq);
    }

    /* renamed from: d */
    public void mo12181d(Vec3f vec3f, C1422Up up) {
        mo12176a(vec3f, up);
    }

    /* renamed from: w */
    public void mo12186w(Vec3f vec3f, Vec3f vec3f2) {
        getFarthest(vec3f, vec3f2);
    }

    public Matrix4fWrap bMM() {
        return this.eXb;
    }

    /* renamed from: a */
    public void mo12175a(C4029yK yKVar, C4029yK yKVar2) {
        this.aAo = yKVar;
        this.aAp = yKVar2;
    }

    /* renamed from: f */
    public void mo12182f(Matrix4fWrap ajk) {
        this.eXb.set(ajk);
        this.eXa.mo14043k(ajk);
    }
}
