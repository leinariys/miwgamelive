package p001a;

import game.network.manager.aHN;
import logic.res.html.DataClassSerializer;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aLo  reason: case insensitive filesystem */
/* compiled from: a */
public class C5496aLo extends DataClassSerializer {
    public static C5496aLo ilg = new C5496aLo();

    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) {
        if (!(objectInput instanceof aHN)) {
            return objectInput.readObject();
        }
        C6656arE bxy = ((aHN) objectInput).mo9184PM().bxy();
        int a = bxy.mo12007a(objectInput);
        if (a == -1) {
            return Class.forName(bxy.mo12014jK(objectInput.readUTF()));
        }
        if (a == -2) {
            return null;
        }
        return bxy.mo12017tS(a);
    }

    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) {
        if (objectOutput instanceof C6744aso) {
            C6656arE bxy = ((C6744aso) objectOutput).mo2090PM().bxy();
            if (obj == null) {
                bxy.mo12008a(objectOutput, -2);
                return;
            }
            if (!(obj instanceof Class)) {
                System.err.println("value: [" + obj + " : " + obj.getClass() + "] is not a java.lang.Class.");
            }
            Class cls = (Class) obj;
            int magicNumber = bxy.getMagicNumber(cls);
            bxy.mo12008a(objectOutput, magicNumber);
            if (magicNumber == -1) {
                addClassTMakingSpecialSerializer("Consider precaching ", cls);
                objectOutput.writeUTF(bxy.mo12015jL(cls.getName()));
                return;
            }
            return;
        }
        objectOutput.writeObject(obj);
    }
}
