package p001a;

import logic.data.mbean.C0677Jd;

/* renamed from: a.amD  reason: case insensitive filesystem */
/* compiled from: a */
class C6395amD extends C6393amB.C1941c {
    final /* synthetic */ C6393amB gbd;

    C6395amD(C6393amB amb) {
        this.gbd = amb;
    }

    /* renamed from: a */
    public int compare(C0677Jd jd, C0677Jd jd2) {
        long aXB = jd.aXB();
        long aXB2 = jd2.aXB();
        if (aXB < aXB2) {
            return -1;
        }
        if (aXB > aXB2) {
            return 1;
        }
        return mo14765b(jd, jd2);
    }
}
