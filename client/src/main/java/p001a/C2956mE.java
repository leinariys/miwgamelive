package p001a;

import logic.res.html.DataClassSerializer;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.mE */
/* compiled from: a */
public class C2956mE extends DataClassSerializer {
    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) {
        if (!objectInput.readBoolean()) {
            return null;
        }
        return mo20457e(objectInput);
    }

    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) {
        if (obj == null) {
            objectOutput.writeBoolean(false);
            return;
        }
        objectOutput.writeBoolean(true);
        mo20456c(objectOutput, obj);
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo20456c(ObjectOutput objectOutput, Object obj) {
        super.serializeData(objectOutput, obj);
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public Object mo20457e(ObjectInput objectInput) {
        return super.inputReadObject(objectInput);
    }
}
