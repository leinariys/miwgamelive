package p001a;

import logic.swing.C0454GJ;
import logic.ui.Panel;
import logic.ui.item.TaskPaneContainer;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

/* renamed from: a.jf */
/* compiled from: a */
final class C2762jf {
    private static final String ahA = "[0..255] dur 50";
    private static final String ahB = "[255..0] dur 50";
    private static final String ahC = " regular_in";
    private static final String ahD = " regular_in";
    private static final String ahx = "50";
    private static final String ahy = "50";
    private static final String ahz = "50";
    /* access modifiers changed from: private */
    public final TaskPaneContainer ahE;
    /* access modifiers changed from: private */
    public final Panel ahK;
    /* access modifiers changed from: private */
    public Vector<Integer> ahF = new Vector<>();
    /* access modifiers changed from: private */
    public C5912aco ahG = new C2764b();
    /* access modifiers changed from: private */
    public C5912aco ahH = new C2763a(this, (C2763a) null);
    /* access modifiers changed from: private */
    public JComponent ahI;
    /* access modifiers changed from: private */
    public int ahJ = 0;

    public C2762jf(TaskPaneContainer xxVar, Panel aco) {
        this.ahE = xxVar;
        this.ahK = aco;
    }

    /* renamed from: a */
    public void mo19983a(boolean z, C0454GJ gj) {
        if (z) {
            this.ahG.mo12690a(gj);
        } else {
            this.ahH.mo12690a(gj);
        }
    }

    /* renamed from: EP */
    public JComponent mo19982EP() {
        if (this.ahI == null) {
            this.ahI = this.ahE.mo23023EP();
        }
        return this.ahI;
    }

    /* access modifiers changed from: private */
    /* renamed from: EQ */
    public void m34092EQ() {
        this.ahF.clear();
        int i = 0;
        for (Component component : this.ahE.buw()) {
            if (!component.isVisible()) {
                this.ahF.add(-1);
            } else {
                this.ahF.add(Integer.valueOf(i));
                component.setLocation(0, i);
                i += component.getHeight();
            }
        }
    }

    /* renamed from: a.jf$b */
    /* compiled from: a */
    public final class C2764b extends C5912aco {
        /* access modifiers changed from: private */
        public C5912aco.C1865a glY = new C3677td(this);
        /* access modifiers changed from: private */
        public C5912aco.C1865a glZ = new C3675tc(this);
        /* access modifiers changed from: private */
        public C5912aco.C1865a gqM = new C3679tf(this);
        private C5912aco.C1865a glX = new C0004A(this);

        public C2764b() {
        }

        /* renamed from: a */
        public void mo12690a(C0454GJ gj) {
            mo12698du(true);
            C2762jf.this.ahK.setVisible(true);
            if (C2762jf.this.ahH.isAnimating()) {
                C2762jf.this.ahH.stop();
            } else {
                C2762jf.this.m34092EQ();
            }
            super.mo12690a(gj);
            bOD();
            this.glX.mo4gZ();
        }
    }

    /* renamed from: a.jf$a */
    private final class C2763a extends C5912aco {
        /* access modifiers changed from: private */
        public C5912aco.C1865a glY;
        /* access modifiers changed from: private */
        public C5912aco.C1865a gma;
        C0454GJ atu;
        C5912aco.C1865a glZ;
        private C5912aco.C1865a glX;

        private C2763a() {
            this.atu = new C0926Na(this);
            this.glX = new C0865MX(this);
            this.glY = new C0866MY(this);
            this.glZ = new C2174cT(this);
            this.gma = new C0861MV(this);
        }

        /* synthetic */ C2763a(C2762jf jfVar, C2763a aVar) {
            this();
        }

        /* renamed from: a */
        public void mo12690a(C0454GJ gj) {
            mo12698du(true);
            C2762jf.this.ahK.setVisible(true);
            if (C2762jf.this.ahG.isAnimating()) {
                C2762jf.this.ahG.stop();
            } else if (C2762jf.this.ahF.size() != C2762jf.this.ahE.bux()) {
                C2762jf.this.m34092EQ();
            }
            super.mo12690a(gj);
            this.glX.mo4gZ();
        }
    }
}
