package p001a;

import com.steadystate.css.dom.CSSValueImpl;
import com.steadystate.css.parser.LexicalUnitImpl;
import logic.res.css.ColorCss;
import org.w3c.css.sac.LexicalUnit;

/* renamed from: a.aRv  reason: case insensitive filesystem */
/* compiled from: a */
public class C5659aRv extends ColorCss {
    public C5659aRv(C6923awL awl) {
        mo11489a(new CSSValueImpl(LexicalUnitImpl.perseValueColor((LexicalUnit) null, (float) awl.getRed()), true));
        mo11490b(new CSSValueImpl(LexicalUnitImpl.perseValueColor((LexicalUnit) null, (float) awl.getGreen()), true));
        mo11492d(new CSSValueImpl(LexicalUnitImpl.perseValueColor((LexicalUnit) null, (float) awl.getBlue()), true));
        mo11492d(new CSSValueImpl(LexicalUnitImpl.perseValueColor((LexicalUnit) null, (float) awl.getAlpha()), true));
    }
}
