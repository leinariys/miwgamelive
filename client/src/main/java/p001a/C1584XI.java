package p001a;

import taikodom.infra.script.I18NString;

/* renamed from: a.XI */
/* compiled from: a */
public enum C1584XI {
    ENGLISH(I18NString.DEFAULT_LOCATION),
    PORTUGUES("pt");

    private String eJz;

    private C1584XI(String str) {
        this.eJz = str;
    }

    public String getCode() {
        return this.eJz;
    }
}
