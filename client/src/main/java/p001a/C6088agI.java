package p001a;

import com.hoplon.geometry.Vec3l;
import logic.baa.C1616Xf;
import logic.res.html.DataClassSerializer;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.agI  reason: case insensitive filesystem */
/* compiled from: a */
public class C6088agI extends DataClassSerializer {
    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) {
        if (obj == null) {
            objectOutput.writeBoolean(false);
            return;
        }
        objectOutput.writeBoolean(true);
        Vec3l vec3l = (Vec3l) obj;
        objectOutput.writeLong(vec3l.x);
        objectOutput.writeLong(vec3l.y);
        objectOutput.writeLong(vec3l.z);
    }

    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) {
        if (!objectInput.readBoolean()) {
            return null;
        }
        return new Vec3l(objectInput.readLong(), objectInput.readLong(), objectInput.readLong());
    }

    /* renamed from: z */
    public Object mo3598z(Object obj) {
        Vec3l vec3l = (Vec3l) obj;
        return new Vec3l(vec3l.x, vec3l.y, vec3l.z);
    }

    /* renamed from: yl */
    public boolean mo3597yl() {
        return true;
    }

    /* renamed from: b */
    public Object mo3591b(C1616Xf xf) {
        return null;
    }
}
