package p001a;

import javax.swing.*;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;

/* renamed from: a.aQe  reason: case insensitive filesystem */
/* compiled from: a */
public class C5616aQe {
    /* renamed from: a */
    public static int m17577a(JTable jTable, int i) {
        int rowCount = jTable.getRowCount();
        int i2 = 0;
        int i3 = 0;
        while (i2 < rowCount) {
            int i4 = jTable.getCellRenderer(i2, i).getTableCellRendererComponent(jTable, jTable.getValueAt(i2, i), false, false, i2, i).getPreferredSize().width;
            if (i4 <= i3) {
                i4 = i3;
            }
            i2++;
            i3 = i4;
        }
        return i3;
    }

    /* renamed from: a */
    public static void m17578a(JTable jTable, Insets insets, boolean z, boolean z2) {
        int columnCount = jTable.getColumnCount();
        TableColumnModel columnModel = jTable.getColumnModel();
        int i = insets == null ? 0 : insets.left + insets.right;
        for (int i2 = 0; i2 < columnCount; i2++) {
            int a = m17577a(jTable, i2) + i;
            TableColumn column = columnModel.getColumn(i2);
            column.setPreferredWidth(a);
            if (z) {
                column.setMinWidth(a);
            }
            if (z2) {
                column.setMaxWidth(a);
            }
        }
    }

    public static void sort(int[] iArr) {
        int length = iArr.length;
        if (length > 1) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < length - 1) {
                    for (int i3 = i2 + 1; i3 < length; i3++) {
                        if (iArr[i3] < iArr[i2]) {
                            int i4 = iArr[i2];
                            iArr[i2] = iArr[i3];
                            iArr[i3] = i4;
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
