package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;
import logic.aaa.C6026aey;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.qc */
/* compiled from: a */
public class C3354qc extends C2849lA {
    /* renamed from: jR */
    static final /* synthetic */ boolean f8944jR = (!C3354qc.class.desiredAssertionStatus());
    private static final int aUd = C6866avG.MAX_CONTACT_SOLVER_TYPES.ordinal();
    private static final int aUe = 16384;
    /* access modifiers changed from: private */
    public static int aUg = 0;
    private static C3355a[] aUf = new C3355a[16384];

    static {
        for (int i = 0; i < aUf.length; i++) {
            aUf[i] = new C3355a((C3355a) null);
        }
    }

    public final C0709KF[][] aUp = ((C0709KF[][]) Array.newInstance(C0709KF.class, new int[]{aUd, aUd}));
    public final C0709KF[][] aUq = ((C0709KF[][]) Array.newInstance(C0709KF.class, new int[]{aUd, aUd}));
    private final C1123QW<C6383alr> aUh = C0762Ks.m6640D(C6383alr.class);
    private final C1123QW<C1025Ox> aUi = C0762Ks.m6640D(C1025Ox.class);
    private final C1123QW<C6460anQ> aUj = C0762Ks.m6640D(C6460anQ.class);
    private final List<C6383alr> aUk = new ArrayList();
    private final List<C1025Ox> aUl = new ArrayList();
    private final List<C1025Ox> aUm = new ArrayList();
    private final C5283aDj aUn = new C5283aDj();
    private final C5283aDj aUo = new C5283aDj();
    public int aUr = 9;
    public long aUs = 0;

    public C3354qc() {
        C0128Bb.dMC = new C3356b();
        for (int i = 0; i < aUd; i++) {
            for (int i2 = 0; i2 < aUd; i2++) {
                this.aUp[i][i2] = C2196ce.f6197qn;
                this.aUq[i][i2] = C2196ce.f6198qo;
            }
        }
    }

    /* renamed from: WG */
    public long mo21408WG() {
        this.aUs = ((1664525 * this.aUs) + 1013904223) & -1;
        return this.aUs;
    }

    /* renamed from: dQ */
    public int mo21422dQ(int i) {
        long j = (long) i;
        long WG = mo21408WG();
        if (j <= 65536) {
            WG ^= WG >>> 16;
            if (j <= 256) {
                WG ^= WG >>> 8;
                if (j <= 16) {
                    WG ^= WG >>> 4;
                    if (j <= 4) {
                        WG ^= WG >>> 2;
                        if (j <= 2) {
                            WG ^= WG >>> 1;
                        }
                    }
                }
            }
        }
        return (int) Math.abs(WG % j);
    }

    /* renamed from: a */
    private void m37731a(C6383alr alr, ayY ayy) {
        C6238ajC c = C6238ajC.m22755c(ayy);
        if (c != null) {
            alr.angularVelocity.set(c.getAngularVelocity());
            alr.fWZ.set(ayy.cFf().bFG);
            alr.dMd = ayy.cFd();
            alr.fWX = c.aRf();
            alr.fRu.set(c.cev());
            alr.fWY = c;
            alr.cZg = c.aRg();
            return;
        }
        alr.angularVelocity.set(0.0f, 0.0f, 0.0f);
        alr.fWZ.set(ayy.cFf().bFG);
        alr.dMd = ayy.cFd();
        alr.fWX = 0.0f;
        alr.fRu.set(0.0f, 0.0f, 0.0f);
        alr.fWY = null;
        alr.cZg = 1.0f;
    }

    /* renamed from: f */
    private float m37733f(float f, float f2) {
        return (-f) * f2;
    }

    /* renamed from: a */
    private float m37729a(C6383alr alr, C6383alr alr2, C1025Ox ox, C5718aUc auc) {
        this.stack.bcH().push();
        try {
            float dot = (ox.dLV.dot(alr.fRu) + ox.dLU.dot(alr.angularVelocity)) - (ox.dLV.dot(alr2.fRu) + ox.dLW.dot(alr2.angularVelocity));
            float f = ox.dMg;
            float f2 = ox.dMe - dot;
            float f3 = f * ox.dMf;
            float f4 = ox.dMf * f2;
            float f5 = f3 + f4;
            float f6 = ox.dMa;
            float f7 = f5 + f6;
            if (0.0f > f7) {
                f7 = 0.0f;
            }
            ox.dMa = f7;
            float f8 = ox.dLZ + f4;
            if (0.0f > f8) {
                f8 = 0.0f;
            }
            ox.dLZ = f8;
            float f9 = ox.dMa - f6;
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            if (alr.fWX != 0.0f) {
                vec3f.scale(alr.fWX, ox.dLV);
                alr.mo14741b(vec3f, ox.dLX, f9);
            }
            if (alr2.fWX != 0.0f) {
                vec3f.scale(alr2.fWX, ox.dLV);
                alr2.mo14741b(vec3f, ox.dLY, -f9);
            }
            return f9;
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a */
    private float m37730a(C6383alr alr, C6383alr alr2, C1025Ox ox, C5718aUc auc, float f) {
        this.stack.bcH().push();
        try {
            float f2 = ox.dMd * f;
            if (f > 0.0f) {
                float f3 = (-((ox.dLV.dot(alr.fRu) + ox.dLU.dot(alr.angularVelocity)) - (ox.dLV.dot(alr2.fRu) + ox.dLW.dot(alr2.angularVelocity)))) * ox.dMf;
                float f4 = ox.dMa;
                ox.dMa = f3 + f4;
                if (f2 < ox.dMa) {
                    ox.dMa = f2;
                } else if (ox.dMa < (-f2)) {
                    ox.dMa = -f2;
                }
                float f5 = ox.dMa - f4;
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                if (alr.fWX != 0.0f) {
                    vec3f.scale(alr.fWX, ox.dLV);
                    alr.mo14741b(vec3f, ox.dLX, f5);
                }
                if (alr2.fWX != 0.0f) {
                    vec3f.scale(alr2.fWX, ox.dLV);
                    alr2.mo14741b(vec3f, ox.dLY, -f5);
                }
            }
            return 0.0f;
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo21415a(Vec3f vec3f, int i, int i2, int i3, C6995ayi ayi, Vec3f vec3f2, Vec3f vec3f3, ayY ayy, ayY ayy2, float f) {
        float f2;
        this.stack.bcH().push();
        try {
            C6238ajC c = C6238ajC.m22755c(ayy);
            C6238ajC c2 = C6238ajC.m22755c(ayy2);
            C1025Ox ox = this.aUi.get();
            this.aUm.add(ox);
            ox.dLV.set(vec3f);
            ox.dMb = i;
            ox.dMc = i2;
            ox.dMh = C6226aiq.SOLVER_FRICTION_1D;
            ox.dMi = i3;
            ox.dMd = ayi.gSK;
            ox.dMa = 0.0f;
            ox.dLZ = 0.0f;
            ox.dMg = 0.0f;
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            vec3f4.cross(vec3f2, ox.dLV);
            ox.dLU.set(vec3f4);
            if (c != null) {
                ox.dLX.set(vec3f4);
                c.aRa().transform(ox.dLX);
            } else {
                ox.dLX.set(0.0f, 0.0f, 0.0f);
            }
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            vec3f5.cross(vec3f3, ox.dLV);
            ox.dLW.set(vec3f5);
            if (c2 != null) {
                ox.dLY.set(vec3f5);
                c2.aRa().transform(ox.dLY);
            } else {
                ox.dLY.set(0.0f, 0.0f, 0.0f);
            }
            Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
            float f3 = 0.0f;
            if (c != null) {
                vec3f6.cross(ox.dLX, vec3f2);
                f3 = c.aRf() + vec3f.dot(vec3f6);
            }
            if (c2 != null) {
                vec3f6.cross(ox.dLY, vec3f3);
                f2 = vec3f.dot(vec3f6) + c2.aRf();
            } else {
                f2 = 0.0f;
            }
            ox.dMf = f / (f2 + f3);
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a */
    public float mo21412a(List<ayY> list, int i, List<aGW> list2, int i2, int i3, List<C1083Pr> list3, int i4, int i5, C5718aUc auc, C1072Pg pg) {
        float f;
        Vec3f h;
        C0128Bb.m1253gf("solveGroupCacheFriendlySetup");
        this.stack.bcH().push();
        if (i5 + i3 != 0) {
            try {
                Vec3f h2 = this.stack.bcH().mo4460h(0.0f, 1.0f, 0.0f);
                for (int i6 = 0; i6 < i3; i6++) {
                    aGW agw = list2.get(i2 + i6);
                    ayY ayy = (ayY) agw.dcy();
                    ayY ayy2 = (ayY) agw.dcz();
                    int i7 = -1;
                    int i8 = -1;
                    if (agw.dcA() != 0) {
                        if (ayy.cFk() < 0) {
                            i7 = this.aUk.size();
                            C6383alr alr = this.aUh.get();
                            this.aUk.add(alr);
                            m37731a(alr, ayy);
                        } else if (ayy.cFl() >= 0) {
                            i7 = ayy.cFl();
                        } else {
                            i7 = this.aUk.size();
                            C6383alr alr2 = this.aUh.get();
                            this.aUk.add(alr2);
                            m37731a(alr2, ayy);
                            ayy.mo17053wy(i7);
                        }
                        if (ayy2.cFk() < 0) {
                            i8 = this.aUk.size();
                            C6383alr alr3 = this.aUh.get();
                            this.aUk.add(alr3);
                            m37731a(alr3, ayy2);
                        } else if (ayy2.cFl() >= 0) {
                            i8 = ayy2.cFl();
                        } else {
                            i8 = this.aUk.size();
                            C6383alr alr4 = this.aUh.get();
                            this.aUk.add(alr4);
                            m37731a(alr4, ayy2);
                            ayy2.mo17053wy(i8);
                        }
                    }
                    Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                    Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                    for (int i9 = 0; i9 < agw.dcA(); i9++) {
                        C6995ayi yd = agw.mo9019yd(i9);
                        if (pg != null) {
                            pg.mo4807a(yd.gSH, yd.fTL, yd.getDistance(), yd.getLifeTime(), h2);
                        }
                        if (yd.getDistance() <= 0.0f) {
                            Vec3f cDp = yd.cDp();
                            Vec3f cDq = yd.cDq();
                            vec3f.sub(cDp, ayy.cFf().bFG);
                            vec3f2.sub(cDq, ayy2.cFf().bFG);
                            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                            int size = this.aUl.size();
                            C1025Ox ox = this.aUi.get();
                            this.aUl.add(ox);
                            C6238ajC c = C6238ajC.m22755c(ayy);
                            C6238ajC c2 = C6238ajC.m22755c(ayy2);
                            ox.dMb = i7;
                            ox.dMc = i8;
                            ox.dMh = C6226aiq.SOLVER_CONTACT_1D;
                            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                            vec3f4.cross(vec3f, yd.fTL);
                            if (c != null) {
                                ox.dLX.set(vec3f4);
                                c.aRa().transform(ox.dLX);
                            } else {
                                ox.dLX.set(0.0f, 0.0f, 0.0f);
                            }
                            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                            vec3f5.cross(vec3f2, yd.fTL);
                            if (c2 != null) {
                                ox.dLY.set(vec3f5);
                                c2.aRa().transform(ox.dLY);
                            } else {
                                ox.dLY.set(0.0f, 0.0f, 0.0f);
                            }
                            Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
                            float f2 = 0.0f;
                            if (c != null) {
                                vec3f6.cross(ox.dLX, vec3f);
                                f2 = c.aRf() + yd.fTL.dot(vec3f6);
                            }
                            if (c2 != null) {
                                vec3f6.cross(ox.dLY, vec3f2);
                                f = yd.fTL.dot(vec3f6) + c2.aRf();
                            } else {
                                f = 0.0f;
                            }
                            ox.dMf = 1.0f / (f + f2);
                            ox.dLV.set(yd.fTL);
                            ox.dLU.cross(vec3f, yd.fTL);
                            ox.dLW.cross(vec3f2, yd.fTL);
                            Vec3f ac = c != null ? this.stack.bcH().mo4458ac(c.mo13897R(vec3f)) : this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f);
                            if (c2 != null) {
                                h = this.stack.bcH().mo4458ac(c2.mo13897R(vec3f2));
                            } else {
                                h = this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f);
                            }
                            vec3f3.sub(ac, h);
                            float dot = yd.fTL.dot(vec3f3);
                            ox.dMg = yd.getDistance();
                            ox.dMd = yd.gSK;
                            ox.dMe = m37733f(dot, yd.gSL);
                            if (ox.dMe <= 0.0f) {
                                ox.dMe = 0.0f;
                            }
                            float f3 = (-ox.dMg) / auc.aGM;
                            ox.dMg *= -(auc.iWs / auc.aGM);
                            if (ox.dMe > f3) {
                                ox.dMg = 0.0f;
                            }
                            ox.dMa = 0.0f;
                            ox.dLZ = 0.0f;
                            Vec3f vec3f7 = (Vec3f) this.stack.bcH().get();
                            vec3f7.scale(dot, yd.fTL);
                            vec3f7.sub(vec3f3, vec3f7);
                            float lengthSquared = vec3f7.lengthSquared();
                            if (lengthSquared > 1.1920929E-7f) {
                                vec3f7.scale(1.0f / ((float) Math.sqrt((double) lengthSquared)));
                                mo21415a(vec3f7, i7, i8, size, yd, vec3f, vec3f2, ayy, ayy2, 1.0f);
                                Vec3f vec3f8 = (Vec3f) this.stack.bcH().get();
                                vec3f8.cross(vec3f7, yd.fTL);
                                vec3f8.normalize();
                                mo21415a(vec3f8, i7, i8, size, yd, vec3f, vec3f2, ayy, ayy2, 1.0f);
                            } else {
                                Vec3f vec3f9 = (Vec3f) this.stack.bcH().get();
                                C0503Gz.m3562e(yd.fTL, vec3f7, vec3f9);
                                mo21415a(vec3f7, i7, i8, size, yd, vec3f, vec3f2, ayy, ayy2, 1.0f);
                                mo21415a(vec3f9, i7, i8, size, yd, vec3f, vec3f2, ayy, ayy2, 1.0f);
                            }
                        }
                    }
                }
                for (int i10 = 0; i10 < i5; i10++) {
                    list3.get(i4 + i10).mo4840PU();
                }
                int size2 = this.aUl.size();
                int size3 = this.aUm.size();
                C3696tt.m39760a(this.aUn, size2, 0);
                C3696tt.m39760a(this.aUo, size3, 0);
                for (int i11 = 0; i11 < size2; i11++) {
                    this.aUn.set(i11, i11);
                }
                for (int i12 = 0; i12 < size3; i12++) {
                    this.aUo.set(i12, i12);
                }
            } catch (Throwable th) {
                this.stack.bcH().pop();
                C0128Bb.blS();
                throw th;
            }
        }
        this.stack.bcH().pop();
        C0128Bb.blS();
        return 0.0f;
    }

    /* renamed from: b */
    public float mo21417b(List<ayY> list, int i, List<aGW> list2, int i2, int i3, List<C1083Pr> list3, int i4, int i5, C5718aUc auc, C1072Pg pg) {
        C0128Bb.m1253gf("solveGroupCacheFriendlyIterations");
        try {
            int size = this.aUl.size();
            int size2 = this.aUm.size();
            for (int i6 = 0; i6 < auc.iWp; i6++) {
                if ((this.aUr & 1) != 0 && (i6 & 7) == 0) {
                    for (int i7 = 0; i7 < size; i7++) {
                        int i8 = this.aUn.get(i7);
                        int dQ = mo21422dQ(i7 + 1);
                        this.aUn.set(i7, this.aUn.get(dQ));
                        this.aUn.set(dQ, i8);
                    }
                    for (int i9 = 0; i9 < size2; i9++) {
                        int i10 = this.aUo.get(i9);
                        int dQ2 = mo21422dQ(i9 + 1);
                        this.aUo.set(i9, this.aUo.get(dQ2));
                        this.aUo.set(dQ2, i10);
                    }
                }
                for (int i11 = 0; i11 < i5; i11++) {
                    C0128Bb.m1253gf("solveConstraint");
                    C1083Pr pr = list3.get(i4 + i11);
                    if (pr.bnn().cFk() >= 0 && pr.bnn().cFl() >= 0) {
                        this.aUk.get(pr.bnn().cFl()).chQ();
                    }
                    if (pr.bno().cFk() >= 0 && pr.bno().cFl() >= 0) {
                        this.aUk.get(pr.bno().cFl()).chQ();
                    }
                    pr.mo4847cv(auc.aGM);
                    if (pr.bnn().cFk() >= 0 && pr.bnn().cFl() >= 0) {
                        this.aUk.get(pr.bnn().cFl()).chR();
                    }
                    if (pr.bno().cFk() >= 0 && pr.bno().cFl() >= 0) {
                        this.aUk.get(pr.bno().cFl()).chR();
                    }
                    C0128Bb.blS();
                }
                C0128Bb.m1253gf("resolveSingleCollisionCombinedCacheFriendly");
                int size3 = this.aUl.size();
                for (int i12 = 0; i12 < size3; i12++) {
                    C1025Ox ox = this.aUl.get(this.aUn.get(i12));
                    m37729a(this.aUk.get(ox.dMb), this.aUk.get(ox.dMc), ox, auc);
                }
                C0128Bb.blS();
                C0128Bb.m1253gf("resolveSingleFrictionCacheFriendly");
                int size4 = this.aUm.size();
                for (int i13 = 0; i13 < size4; i13++) {
                    C1025Ox ox2 = this.aUm.get(this.aUo.get(i13));
                    m37730a(this.aUk.get(ox2.dMb), this.aUk.get(ox2.dMc), ox2, auc, this.aUl.get(ox2.dMi).dMa);
                }
                C0128Bb.blS();
            }
            C0128Bb.blS();
            return 0.0f;
        } catch (Throwable th) {
            throw th;
        } finally {
        }
    }

    /* renamed from: c */
    public float mo21420c(List<ayY> list, int i, List<aGW> list2, int i2, int i3, List<C1083Pr> list3, int i4, int i5, C5718aUc auc, C1072Pg pg) {
        mo21412a(list, i, list2, i2, i3, list3, i4, i5, auc, pg);
        mo21417b(list, i, list2, i2, i3, list3, i4, i5, auc, pg);
        for (int i6 = 0; i6 < this.aUk.size(); i6++) {
            C6383alr alr = this.aUk.get(i6);
            alr.chQ();
            this.aUh.release(alr);
        }
        this.aUk.clear();
        for (int i7 = 0; i7 < this.aUl.size(); i7++) {
            this.aUi.release(this.aUl.get(i7));
        }
        this.aUl.clear();
        for (int i8 = 0; i8 < this.aUm.size(); i8++) {
            this.aUi.release(this.aUm.get(i8));
        }
        this.aUm.clear();
        return 0.0f;
    }

    /* renamed from: a */
    public float mo20161a(List<ayY> list, int i, List<aGW> list2, int i2, int i3, List<C1083Pr> list3, int i4, int i5, C5718aUc auc, C1072Pg pg, C1701ZB zb) {
        C0128Bb.m1253gf("solveGroup");
        try {
            if ((mo21409WH() & 8) == 0) {
                C5718aUc auc2 = new C5718aUc(auc);
                int i6 = auc.iWp;
                int i7 = 0;
                short s = 0;
                while (s < i3) {
                    mo21414a(list2.get(i2 + s), auc2, pg);
                    int i8 = i7;
                    for (short s2 = 0; s2 < list2.get(i2 + s).dcA(); s2 = (short) (s2 + 1)) {
                        aUf[i8].eZU = s;
                        aUf[i8].eZV = s2;
                        i8++;
                    }
                    s = (short) (s + 1);
                    i7 = i8;
                }
                for (int i9 = 0; i9 < i5; i9++) {
                    list3.get(i4 + i9).mo4840PU();
                }
                for (int i10 = 0; i10 < i6; i10++) {
                    if ((this.aUr & 1) != 0 && (i10 & 7) == 0) {
                        for (int i11 = 0; i11 < i7; i11++) {
                            C3355a aVar = aUf[i11];
                            int dQ = mo21422dQ(i11 + 1);
                            aUf[i11] = aUf[dQ];
                            aUf[dQ] = aVar;
                        }
                    }
                    for (int i12 = 0; i12 < i5; i12++) {
                        list3.get(i4 + i12).mo4847cv(auc2.aGM);
                    }
                    for (int i13 = 0; i13 < i7; i13++) {
                        aGW agw = list2.get(aUf[i13].eZU + i2);
                        mo21416b((C6238ajC) agw.dcy(), (C6238ajC) agw.dcz(), agw.mo9019yd(aUf[i13].eZV), auc2, i10, pg);
                    }
                    for (int i14 = 0; i14 < i7; i14++) {
                        aGW agw2 = list2.get(aUf[i14].eZU + i2);
                        mo21419c((C6238ajC) agw2.dcy(), (C6238ajC) agw2.dcz(), agw2.mo9019yd(aUf[i14].eZV), auc2, i10, pg);
                    }
                }
                C0128Bb.blS();
                return 0.0f;
            } else if (!f8944jR && list == null) {
                throw new AssertionError();
            } else if (f8944jR || i != 0) {
                return mo21420c(list, i, list2, i2, i3, list3, i4, i5, auc, pg);
            } else {
                throw new AssertionError();
            }
        } finally {
            C0128Bb.blS();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo21414a(aGW agw, C5718aUc auc, C1072Pg pg) {
        C6026aey aey;
        this.stack.bcF();
        try {
            C6238ajC ajc = (C6238ajC) agw.dcy();
            C6238ajC ajc2 = (C6238ajC) agw.dcz();
            int dcA = agw.dcA();
            C0128Bb.dMH += dcA;
            this.stack.bcH().mo4460h(0.0f, 1.0f, 0.0f);
            for (int i = 0; i < dcA; i++) {
                C6995ayi yd = agw.mo9019yd(i);
                if (yd.getDistance() <= 0.0f) {
                    Vec3f cDp = yd.cDp();
                    Vec3f cDq = yd.cDq();
                    Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                    vec3f.sub(cDp, ajc.ces());
                    Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                    vec3f2.sub(cDq, ajc2.ces());
                    Matrix3fWrap g = this.stack.bcK().mo15565g(ajc.ceu().bFF);
                    g.transpose();
                    Matrix3fWrap g2 = this.stack.bcK().mo15565g(ajc2.ceu().bFF);
                    g2.transpose();
                    C6460anQ anq = this.aUj.get();
                    anq.mo14967a(g, g2, vec3f, vec3f2, yd.fTL, ajc.ceq(), ajc.aRf(), ajc2.ceq(), ajc2.aRf());
                    float clX = anq.clX();
                    this.aUj.release(anq);
                    C6026aey aey2 = (C6026aey) yd.gSM;
                    if (aey2 != null) {
                        aey2.fpC++;
                        if (aey2.fpC != yd.getLifeTime()) {
                            aey2.reset();
                            aey2.fpC = yd.getLifeTime();
                            aey = aey2;
                        } else {
                            aey = aey2;
                        }
                    } else {
                        C6026aey aey3 = new C6026aey();
                        aUg++;
                        yd.gSM = aey3;
                        aey3.fpC = yd.getLifeTime();
                        aey = aey3;
                    }
                    if (f8944jR || aey != null) {
                        aey.dMf = 1.0f / clX;
                        aey.fpK = this.aUq[ajc.fRB][ajc2.fRB];
                        aey.fpJ = this.aUp[ajc.fRA][ajc2.fRA];
                        Vec3f ac = this.stack.bcH().mo4458ac(ajc.mo13897R(vec3f));
                        Vec3f ac2 = this.stack.bcH().mo4458ac(ajc2.mo13897R(vec3f2));
                        Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                        vec3f3.sub(ac, ac2);
                        float dot = yd.fTL.dot(vec3f3);
                        float f = yd.gSL;
                        aey.dMg = yd.getDistance();
                        aey.dMd = yd.gSK;
                        aey.dMe = m37733f(dot, f);
                        if (aey.dMe <= 0.0f) {
                            aey.dMe = 0.0f;
                        }
                        if (aey.dMe > (-aey.dMg) / auc.aGM) {
                            aey.dMg = 0.0f;
                        }
                        float f2 = auc.damping;
                        if ((this.aUr & 4) != 0) {
                            aey.dMa *= f2;
                        } else {
                            aey.dMa = 0.0f;
                        }
                        aey.fpx = aey.dMa;
                        C0503Gz.m3562e(yd.fTL, aey.fpD, aey.fpE);
                        aey.fpy = 0.0f;
                        aey.fpz = 0.0f;
                        aey.fpA = f2 / (ajc.mo13892A(cDp, aey.fpD) + ajc2.mo13892A(cDq, aey.fpD));
                        aey.fpB = f2 / (ajc.mo13892A(cDp, aey.fpE) + ajc2.mo13892A(cDq, aey.fpE));
                        Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                        vec3f4.scale(aey.dMa, yd.fTL);
                        Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                        vec3f5.cross(vec3f, yd.fTL);
                        aey.dLX.set(vec3f5);
                        ajc.aRa().transform(aey.dLX);
                        Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
                        vec3f6.cross(vec3f2, yd.fTL);
                        aey.dLY.set(vec3f6);
                        ajc2.aRa().transform(aey.dLY);
                        Vec3f vec3f7 = (Vec3f) this.stack.bcH().get();
                        vec3f7.cross(vec3f, aey.fpD);
                        aey.fpF.set(vec3f7);
                        ajc.aRa().transform(aey.fpF);
                        Vec3f vec3f8 = (Vec3f) this.stack.bcH().get();
                        vec3f8.cross(vec3f, aey.fpE);
                        aey.fpH.set(vec3f8);
                        ajc.aRa().transform(aey.fpH);
                        Vec3f vec3f9 = (Vec3f) this.stack.bcH().get();
                        vec3f9.cross(vec3f2, aey.fpD);
                        aey.fpG.set(vec3f9);
                        ajc2.aRa().transform(aey.fpG);
                        Vec3f vec3f10 = (Vec3f) this.stack.bcH().get();
                        vec3f10.cross(vec3f2, aey.fpE);
                        aey.fpI.set(vec3f10);
                        ajc2.aRa().transform(aey.fpI);
                        ajc.mo13937m(vec3f4, vec3f);
                        Vec3f ac3 = this.stack.bcH().mo4458ac(vec3f4);
                        ac3.negate();
                        ajc2.mo13937m(ac3, vec3f2);
                    } else {
                        throw new AssertionError();
                    }
                }
            }
        } finally {
            this.stack.bcG();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003a, code lost:
        if (0.0f < r0) goto L_0x003c;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public float mo21411a(p001a.C6238ajC r8, p001a.C6238ajC r9, p001a.C6995ayi r10, p001a.C5718aUc r11, int r12, p001a.C1072Pg r13) {
        /*
            r7 = this;
            r6 = 0
            a.Kt r0 = r7.stack
            a.OY r0 = r0.bcH()
            r0.push()
            a.Kt r0 = r7.stack     // Catch:{ all -> 0x0046 }
            a.OY r0 = r0.bcH()     // Catch:{ all -> 0x0046 }
            r1 = 0
            r2 = 1065353216(0x3f800000, float:1.0)
            r3 = 0
            com.hoplon.geometry.Vec3f r5 = r0.mo4460h(r1, r2, r3)     // Catch:{ all -> 0x0046 }
            float r0 = r10.getDistance()     // Catch:{ all -> 0x0046 }
            int r0 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r0 > 0) goto L_0x0051
            if (r12 != 0) goto L_0x0034
            if (r13 == 0) goto L_0x0034
            com.hoplon.geometry.Vec3f r1 = r10.gSH     // Catch:{ all -> 0x0046 }
            com.hoplon.geometry.Vec3f r2 = r10.fTL     // Catch:{ all -> 0x0046 }
            float r3 = r10.getDistance()     // Catch:{ all -> 0x0046 }
            int r4 = r10.getLifeTime()     // Catch:{ all -> 0x0046 }
            r0 = r13
            r0.mo4807a(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0046 }
        L_0x0034:
            float r0 = p001a.C2196ce.m28563c(r8, r9, r10, r11)     // Catch:{ all -> 0x0046 }
            int r1 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r1 >= 0) goto L_0x0051
        L_0x003c:
            a.Kt r1 = r7.stack
            a.OY r1 = r1.bcH()
            r1.pop()
            return r0
        L_0x0046:
            r0 = move-exception
            a.Kt r1 = r7.stack
            a.OY r1 = r1.bcH()
            r1.pop()
            throw r0
        L_0x0051:
            r0 = r6
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C3354qc.mo21411a(a.ajC, a.ajC, a.ayi, a.aUc, int, a.Pg):float");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0040, code lost:
        if (0.0f < r0) goto L_0x0042;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public float mo21416b(p001a.C6238ajC r8, p001a.C6238ajC r9, p001a.C6995ayi r10, p001a.C5718aUc r11, int r12, p001a.C1072Pg r13) {
        /*
            r7 = this;
            r6 = 0
            a.Kt r0 = r7.stack
            a.OY r0 = r0.bcH()
            r0.push()
            a.Kt r0 = r7.stack     // Catch:{ all -> 0x004c }
            a.OY r0 = r0.bcH()     // Catch:{ all -> 0x004c }
            r1 = 0
            r2 = 1065353216(0x3f800000, float:1.0)
            r3 = 0
            com.hoplon.geometry.Vec3f r5 = r0.mo4460h(r1, r2, r3)     // Catch:{ all -> 0x004c }
            float r0 = r10.getDistance()     // Catch:{ all -> 0x004c }
            int r0 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r0 > 0) goto L_0x0057
            if (r12 != 0) goto L_0x0034
            if (r13 == 0) goto L_0x0034
            com.hoplon.geometry.Vec3f r1 = r10.gSH     // Catch:{ all -> 0x004c }
            com.hoplon.geometry.Vec3f r2 = r10.fTL     // Catch:{ all -> 0x004c }
            float r3 = r10.getDistance()     // Catch:{ all -> 0x004c }
            int r4 = r10.getLifeTime()     // Catch:{ all -> 0x004c }
            r0 = r13
            r0.mo4807a(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x004c }
        L_0x0034:
            java.lang.Object r0 = r10.gSM     // Catch:{ all -> 0x004c }
            a.aey r0 = (logic.aaa.C6026aey) r0     // Catch:{ all -> 0x004c }
            a.KF r0 = r0.fpJ     // Catch:{ all -> 0x004c }
            float r0 = r0.mo3478e(r8, r9, r10, r11)     // Catch:{ all -> 0x004c }
            int r1 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r1 >= 0) goto L_0x0057
        L_0x0042:
            a.Kt r1 = r7.stack
            a.OY r1 = r1.bcH()
            r1.pop()
            return r0
        L_0x004c:
            r0 = move-exception
            a.Kt r1 = r7.stack
            a.OY r1 = r1.bcH()
            r1.pop()
            throw r0
        L_0x0057:
            r0 = r6
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C3354qc.mo21416b(a.ajC, a.ajC, a.ayi, a.aUc, int, a.Pg):float");
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public float mo21419c(C6238ajC ajc, C6238ajC ajc2, C6995ayi ayi, C5718aUc auc, int i, C1072Pg pg) {
        this.stack.bcH().push();
        try {
            this.stack.bcH().mo4460h(0.0f, 1.0f, 0.0f);
            if (ayi.getDistance() <= 0.0f) {
                ((C6026aey) ayi.gSM).fpK.mo3478e(ajc, ajc2, ayi, auc);
            }
            return 0.0f;
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void reset() {
        this.aUs = 0;
    }

    /* renamed from: a */
    public void mo21413a(C0709KF kf, int i, int i2) {
        this.aUp[i][i2] = kf;
    }

    /* renamed from: b */
    public void mo21418b(C0709KF kf, int i, int i2) {
        this.aUq[i][i2] = kf;
    }

    /* renamed from: WH */
    public int mo21409WH() {
        return this.aUr;
    }

    /* renamed from: dR */
    public void mo21423dR(int i) {
        this.aUr = i;
    }

    /* renamed from: cK */
    public void mo21421cK(long j) {
        this.aUs = j;
    }

    /* renamed from: WI */
    public long mo21410WI() {
        return this.aUs;
    }

    /* renamed from: a.qc$a */
    private static class C3355a {
        public int eZU;
        public int eZV;

        private C3355a() {
        }

        /* synthetic */ C3355a(C3355a aVar) {
            this();
        }
    }

    /* renamed from: a.qc$b */
    /* compiled from: a */
    class C3356b implements C5799aaf {
        C3356b() {
        }

        /* renamed from: ag */
        public boolean mo12228ag(Object obj) {
            if (C3354qc.f8944jR || obj != null) {
                C6026aey aey = (C6026aey) obj;
                C3354qc.aUg = C3354qc.aUg - 1;
                return true;
            }
            throw new AssertionError();
        }
    }
}
