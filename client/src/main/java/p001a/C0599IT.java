package p001a;

import java.util.HashMap;
import java.util.Map;

/* renamed from: a.IT */
/* compiled from: a */
public class C0599IT {
    private Map<String, String> dht = new HashMap();

    public String aXg() {
        return this.dht.get("classtype");
    }

    public String getName() {
        return this.dht.get("name");
    }

    public void setName(String str) {
        this.dht.put("name", str);
    }

    public void setProperty(String str, String str2) {
        this.dht.put(str, str2);
    }

    public String getProperty(String str) {
        return this.dht.get(str);
    }

    public Map<String, String> getProperties() {
        return this.dht;
    }

    public String toString() {
        return String.valueOf(getName()) + ":" + aXg();
    }
}
