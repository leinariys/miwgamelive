package p001a;

import org.apache.mina.core.buffer.IoBuffer;

import java.io.ObjectOutput;
import java.nio.charset.CharsetEncoder;

/* renamed from: a.aOi  reason: case insensitive filesystem */
/* compiled from: a */
public final class C5568aOi implements ObjectOutput {
    public static final int iyz = 1;
    private final IoBuffer gAO;
    private CharsetEncoder encoder;

    public C5568aOi(IoBuffer ioBuffer, CharsetEncoder charsetEncoder) {
        this.gAO = ioBuffer;
        this.encoder = charsetEncoder;
    }

    public void close() {
    }

    public void flush() {
    }

    public void write(int i) {
        this.gAO.put((byte) i);
    }

    public void write(byte[] bArr) {
        this.gAO.put(bArr);
    }

    public void write(byte[] bArr, int i, int i2) {
        this.gAO.put(bArr, i, i2);
    }

    public void writeObject(Object obj) {
        throw new UnsupportedOperationException();
    }

    public void writeBoolean(boolean z) {
        this.gAO.put((byte) (z ? 1 : 0));
    }

    public void writeByte(int i) {
        this.gAO.put((byte) i);
    }

    public void writeBytes(String str) {
        this.gAO.putString(str, this.encoder);
        this.gAO.put((byte) 0);
    }

    public void writeChar(int i) {
        this.gAO.putChar((char) i);
    }

    public void writeChars(String str) {
        this.gAO.putString(str, this.encoder);
        this.gAO.put((byte) 0);
    }

    public void writeDouble(double d) {
        this.gAO.putDouble(d);
    }

    public void writeFloat(float f) {
        this.gAO.putFloat(f);
    }

    public void writeInt(int i) {
        this.gAO.putInt(i);
    }

    public void writeLong(long j) {
        this.gAO.putLong(j);
    }

    public void writeShort(int i) {
        this.gAO.putShort((short) i);
    }

    public void writeUTF(String str) {
        this.gAO.putString(str, this.encoder);
        this.gAO.put((byte) 0);
    }
}
