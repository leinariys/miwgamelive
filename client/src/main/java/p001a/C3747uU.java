package p001a;

import game.script.item.ItemType;
import game.script.item.ItemTypeCategory;

/* renamed from: a.uU */
/* compiled from: a */
class C3747uU extends C5519aMl {
    final /* synthetic */ C3740uR bxI;

    C3747uU(C3740uR uRVar) {
        this.bxI = uRVar;
    }

    /* renamed from: a */
    public boolean mo10165a(Object obj) {
        String str = "";
        if (obj instanceof ItemType) {
            str = ((ItemType) obj).mo19891ke().get();
        } else if (obj instanceof ItemTypeCategory) {
            str = ((ItemTypeCategory) obj).mo12246ke().get();
        }
        return str.toLowerCase().contains(this.bxI.ejz.toLowerCase());
    }

    /* renamed from: z */
    public boolean mo10166z() {
        return this.bxI.ejz != null;
    }
}
