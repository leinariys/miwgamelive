package p001a;

/* renamed from: a.aUc  reason: case insensitive filesystem */
/* compiled from: a */
public class C5718aUc {
    public float aGM;
    public float dMd = 0.3f;
    public float dMe = 0.0f;
    public float damping = 1.0f;
    public float feX = 0.6f;
    public int iWp = 10;
    public float iWq = 20.0f;
    public float iWr = 1.3f;
    public float iWs = 0.4f;

    public C5718aUc() {
    }

    public C5718aUc(C5718aUc auc) {
        this.feX = auc.feX;
        this.damping = auc.damping;
        this.dMd = auc.dMd;
        this.aGM = auc.aGM;
        this.dMe = auc.dMe;
        this.iWp = auc.iWp;
        this.iWq = auc.iWq;
        this.iWr = auc.iWr;
        this.iWs = auc.iWs;
    }
}
