package p001a;

import javax.swing.table.TableRowSorter;
import java.util.Comparator;

/* renamed from: a.alc  reason: case insensitive filesystem */
/* compiled from: a */
public class C6368alc extends TableRowSorter<C6415amX> {
    /* access modifiers changed from: private */
    public Comparator<String> fVu = new C5197aAb(this);
    private Comparator<C2570gx> fVt = new azZ(this);

    public C6368alc(C6415amX amx) {
        super(amx);
    }

    /* access modifiers changed from: private */
    /* renamed from: ae */
    public int m23789ae(int i, int i2) {
        if (i > i2) {
            return 1;
        }
        if (i < i2) {
            return -1;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public boolean useToString(int i) {
        if (i == 0) {
            return false;
        }
        return C6368alc.super.useToString(i);
    }

    public Comparator<?> getComparator(int i) {
        switch (i) {
            case 0:
                return this.fVt;
            case 1:
                return C6368alc.super.getComparator(i);
            case 2:
                return this.fVu;
            default:
                return C6368alc.super.getComparator(i);
        }
    }
}
