package p001a;

import logic.baa.C1616Xf;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.gp */
/* compiled from: a */
public class C2562gp extends C5342aFq {

    /* renamed from: PX */
    public static final C2562gp f7767PX = new C2562gp();

    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) {
        if (obj != null) {
            objectOutput.writeBoolean(true);
            objectOutput.writeUTF((String) obj);
            return;
        }
        objectOutput.writeBoolean(false);
    }

    /* renamed from: b */
    public void mo19110b(ObjectOutput objectOutput, Object obj) {
        objectOutput.writeUTF((String) obj);
    }

    /* renamed from: a */
    public Object mo19109a(ObjectInput objectInput) {
        return objectInput.readUTF();
    }

    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) {
        if (objectInput.readBoolean()) {
            return objectInput.readUTF();
        }
        return null;
    }

    /* renamed from: b */
    public Object mo3591b(C1616Xf xf) {
        return null;
    }
}
