package p001a;

import java.util.Map;

/* renamed from: a.Ks */
/* compiled from: a */
public class C0762Ks {
    private static ThreadLocal<Map> dpR = new C3528sA();

    private C0762Ks() {
    }

    /* renamed from: D */
    public static <T> C1123QW<T> m6640D(Class<T> cls) {
        Map map = dpR.get();
        C1123QW<T> qw = (C1123QW) map.get(cls);
        if (qw != null) {
            return qw;
        }
        C1123QW<T> qw2 = new C1123QW<>(cls);
        map.put(cls, qw2);
        return qw2;
    }
}
