package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.OY */
/* compiled from: a */
public class C0988OY extends C6518aoW<Vec3f> {
    /* renamed from: h */
    public Vec3f mo4460h(float f, float f2, float f3) {
        Vec3f vec3f = (Vec3f) get();
        vec3f.set(f, f2, f3);
        return vec3f;
    }

    /* renamed from: ac */
    public Vec3f mo4458ac(Vec3f vec3f) {
        Vec3f vec3f2 = (Vec3f) get();
        vec3f2.set(vec3f);
        return vec3f2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: bnc */
    public Vec3f create() {
        return new Vec3f();
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public void copy(Vec3f vec3f, Vec3f vec3f2) {
        vec3f.set(vec3f2);
    }
}
