package p001a;

import game.geometry.Vec3d;
import logic.aaa.C2235dB;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: a.dT */
/* compiled from: a */
public class C2268dT<E extends C2235dB, B extends C0461GN> implements C3055nX<E, B> {
    /* access modifiers changed from: private */
    public Map<Object, Object> cTp = new HashMap();
    /* access modifiers changed from: private */
    public List<List<C0463GP<E>>> cTq = new ArrayList();
    /* access modifiers changed from: private */
    public List<C2268dT<E, B>.IEyeHandleImplementation<B>> cTr = new ArrayList();
    private C3055nX.C3056a<E, B> cTs = new C5648aRk(this);

    public C2268dT(int i, int i2, int i3) {
        for (int i4 = 0; i4 < 32; i4++) {
            this.cTq.add(new ArrayList());
        }
    }

    /* renamed from: a */
    public void mo8634a(float f, C3055nX.C3057b<E, B> bVar) {
        bVar.mo1933a(this.cTs);
    }

    /* renamed from: a */
    public C0463GP<E> mo8631a(E e, int i) {
        C2270b bVar = new C2270b(e);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= 32) {
                return bVar;
            }
            if ((i & i3) != 0) {
                this.cTq.get(i3).add(bVar);
            }
            i2 = i3 + 1;
        }
    }

    /* renamed from: a */
    public C3387qy<B> mo8632a(B b) {
        C2269a aVar = new C2269a(this, b, (C2269a) null);
        this.cTr.add(aVar);
        return aVar;
    }

    /* renamed from: gZ */
    public void mo8641gZ() {
    }

    /* renamed from: a */
    public void mo8636a(C3055nX.C3057b<E, B> bVar) {
    }

    /* renamed from: p */
    public C3055nX.C3056a<E, B> mo8644p(Vec3d ajr) {
        return null;
    }

    public void dispose() {
    }

    /* renamed from: a.dT$a */
    private final class C2269a<X extends C0461GN> implements C3387qy<X> {
        /* access modifiers changed from: private */

        /* renamed from: AR */
        public final X f6525AR;

        /* synthetic */ C2269a(C2268dT dTVar, C0461GN gn, C2269a aVar) {
            this(gn);
        }

        private C2269a(X x) {
            this.f6525AR = x;
        }

        public void dispose() {
            C2268dT.this.cTr.remove(this);
        }

        /* renamed from: kN */
        public X mo2340kP() {
            return this.f6525AR;
        }

        /* renamed from: kO */
        public void mo2339kO() {
        }
    }

    /* renamed from: a.dT$b */
    /* compiled from: a */
    class C2270b implements C0463GP<E> {
        private final /* synthetic */ C2235dB iGY;

        C2270b(C2235dB dBVar) {
            this.iGY = dBVar;
        }

        public void dispose() {
            for (int i = 0; i < 32; i++) {
                C2268dT.this.cTq.remove(this);
            }
        }

        /* renamed from: kP */
        public E mo2340kP() {
            return this.iGY;
        }

        /* renamed from: kO */
        public void mo2339kO() {
        }
    }
}
