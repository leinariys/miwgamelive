package p001a;

import java.util.Comparator;

/* renamed from: a.sn */
/* compiled from: a */
public class C3600sn<T> extends C5391aHn<T> {

    private final Comparator<T> comparator;

    public C3600sn() {
        this.comparator = null;
    }

    public C3600sn(Comparator<T> comparator2) {
        this.comparator = comparator2;
    }

    /* access modifiers changed from: protected */
    public int compare(T t, T t2) {
        if (this.comparator == null) {
            return ((Comparable) t).compareTo(t2);
        }
        return this.comparator.compare(t, t2);
    }
}
