package p001a;

/* renamed from: a.asA  reason: case insensitive filesystem */
/* compiled from: a */
public enum C6704asA {
    HIRE,
    FIRE(true),
    PROMOTE(true),
    DEMOTE(true),
    EDIT_CORP_DESCRIPTION,
    POST_MESSAGE_OF_THE_DAY,
    EDIT_ROLE_NAMES,
    EDIT_ROLE_PERMISSIONS,
    EDIT_ROLE_FUNCTION_DESCRIPTION,
    TAKE_MONEY,
    ACCESS_CORP_STORAGE,
    REMOVE_FROM_CORP_STORAGE,
    OUTPOST_PAY_UPKEEP,
    OUTPOST_LEVEL_UP;

    private boolean gvu;

    private C6704asA(boolean z) {
        this.gvu = z;
    }

    public boolean cuB() {
        return this.gvu;
    }
}
