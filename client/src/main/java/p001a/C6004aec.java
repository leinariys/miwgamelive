package p001a;

import org.mozilla1.javascript.Callable;
import org.mozilla1.javascript.Context;
import org.mozilla1.javascript.Scriptable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.lang.reflect.UndeclaredThrowableException;
import java.security.*;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: a.aec  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C6004aec {
    /* access modifiers changed from: private */
    public static final byte[] gtP = ctt();
    private static final Map<CodeSource, Map<ClassLoader, SoftReference<C6004aec>>> gtQ = new WeakHashMap();

    /* renamed from: a */
    static Object m21236a(CodeSource codeSource, Callable mFVar, Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        WeakHashMap weakHashMap;
        C6004aec aec;
        C6004aec aec2;
        ClassLoader classLoader = (ClassLoader) AccessController.doPrivileged(new C1873c(Thread.currentThread()));
        synchronized (gtQ) {
            Map map = gtQ.get(codeSource);
            if (map == null) {
                WeakHashMap weakHashMap2 = new WeakHashMap();
                gtQ.put(codeSource, weakHashMap2);
                weakHashMap = weakHashMap2;
            } else {
                weakHashMap = map;
            }
        }
        synchronized (weakHashMap) {
            SoftReference softReference = (SoftReference) weakHashMap.get(classLoader);
            if (softReference != null) {
                aec = (C6004aec) softReference.get();
            } else {
                aec = null;
            }
            if (aec == null) {
                try {
                    C6004aec aec3 = (C6004aec) AccessController.doPrivileged(new C1871a(classLoader, codeSource));
                    weakHashMap.put(classLoader, new SoftReference(aec3));
                    aec2 = aec3;
                } catch (PrivilegedActionException e) {
                    throw new UndeclaredThrowableException(e.getCause());
                }
            } else {
                aec2 = aec;
            }
        }
        return aec2.mo13096b(mFVar, lhVar, avf, avf2, objArr);
    }

    private static byte[] ctt() {
        return (byte[]) AccessController.doPrivileged(new C1872b());
    }

    /* access modifiers changed from: private */
    public static byte[] dBl() {
        InputStream openStream;
        try {
            openStream = C6004aec.class.getResource("SecureCallerImpl.clazz").openStream();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (true) {
                int read = openStream.read();
                if (read == -1) {
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    openStream.close();
                    return byteArray;
                }
                byteArrayOutputStream.write(read);
            }
        } catch (IOException e) {
            throw new UndeclaredThrowableException(e);
        } catch (Throwable th) {
            openStream.close();
            throw th;
        }
    }

    /* renamed from: b */
    public abstract Object mo13096b(Callable mFVar, Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr);

    /* renamed from: a.aec$d */
    /* compiled from: a */
    private static class C1874d extends SecureClassLoader {
        C1874d(ClassLoader classLoader) {
            super(classLoader);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public Class<?> mo13100a(String str, byte[] bArr, CodeSource codeSource) {
            Class<?> defineClass = defineClass(str, bArr, 0, bArr.length, codeSource);
            resolveClass(defineClass);
            return defineClass;
        }
    }

    /* renamed from: a.aec$c */
    /* compiled from: a */
    class C1873c implements PrivilegedAction<Object> {
        private final /* synthetic */ Thread fni;

        C1873c(Thread thread) {
            this.fni = thread;
        }

        public Object run() {
            return this.fni.getContextClassLoader();
        }
    }

    /* renamed from: a.aec$a */
    class C1871a implements PrivilegedExceptionAction<Object> {
        private final /* synthetic */ ClassLoader efW;
        private final /* synthetic */ CodeSource efX;

        C1871a(ClassLoader classLoader, CodeSource codeSource) {
            this.efW = classLoader;
            this.efX = codeSource;
        }

        public Object run() {
            ClassLoader classLoader;
            Class<?> cls = getClass();
            if (this.efW.loadClass(cls.getName()) != cls) {
                classLoader = cls.getClassLoader();
            } else {
                classLoader = this.efW;
            }
            return new C1874d(classLoader).mo13100a(String.valueOf(C6004aec.class.getName()) + "Impl", C6004aec.gtP, this.efX).newInstance();
        }
    }

    /* renamed from: a.aec$b */
    /* compiled from: a */
    class C1872b implements PrivilegedAction<Object> {
        C1872b() {
        }

        public Object run() {
            return C6004aec.dBl();
        }
    }
}
