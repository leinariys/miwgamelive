package p001a;

import game.engine.DataGameEvent;
import game.network.message.serializable.C1695Yw;
import game.network.message.serializable.C5613aQb;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.List;

/* renamed from: a.auC  reason: case insensitive filesystem */
/* compiled from: a */
public class C6810auC extends C6420amc {
    private FileInputStream gEe = null;
    private ObjectInputStream gEf = null;
    private C1596XM gEg;

    public C6810auC(String str, DataGameEvent jz) {
        this.gEe = new FileInputStream(str);
        this.gEf = new C3086na(jz, new C5238aBq(this.gEe));
    }

    public C1596XM cis() {
        int i = 0;
        int i2 = 0;
        byte[] bArr = new byte[1024];
        while (true) {
            int read = this.gEe.read();
            if (read == 10 && i2 == 10) {
                String str = new String(bArr, 0, i);
                this.gEg = new C1596XM();
                this.gEg.setText(str);
                return this.gEg;
            }
            if (bArr.length == i) {
                byte[] bArr2 = new byte[(bArr.length * 2)];
                System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
                bArr = bArr2;
            }
            bArr[i] = (byte) read;
            i++;
            i2 = read;
        }
    }

    public C1695Yw cit() {
        List list = (List) this.gEf.readObject();
        C1695Yw yw = new C1695Yw();
        yw.mo7311w((List) this.gEf.readObject());
        yw.mo7310v((List) this.gEf.readObject());
        yw.mo7300a((C5613aQb) this.gEf.readObject());
        return yw;
    }

    public void close() {
        this.gEf.close();
        this.gEe.close();
    }
}
