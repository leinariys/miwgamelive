package p001a;

import com.hoplon.geometry.Color;
import logic.res.scene.*;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.qI */
/* compiled from: a */
public class C3319qI {
    private List<aBN> aWi;
    private List<aBN> aWj = new ArrayList();
    private C2036bO aWk;
    private C6000aeY aWl;

    public C3319qI(C2036bO bOVar, C6000aeY aey) {
        this.aWl = aey;
        this.aWk = bOVar;
        this.aWi = C3879wD.m40491a(bOVar, aBN.class);
        m37471Xz();
    }

    /* renamed from: Xz */
    private void m37471Xz() {
        for (aBN dy : this.aWi) {
            aBN abn = (aBN) dy.mo764dy();
            abn.mo20809PR();
            this.aWj.add(abn);
            this.aWl.mo13079a(abn);
        }
    }

    /* renamed from: c */
    public void mo21311c(Color color) {
        for (aBN primitiveColor : this.aWj) {
            primitiveColor.setPrimitiveColor(color);
        }
    }

    /* renamed from: a */
    public void mo21307a(C1710ZI zi, int i) {
        for (aBN YU : this.aWj) {
            YU.mo16837YU().mo20686a(i, zi);
        }
    }

    /* renamed from: a */
    public void mo21308a(C3038nJ nJVar) {
        for (aBN b : this.aWj) {
            b.mo9377b(nJVar);
        }
    }

    /* renamed from: c */
    public void mo21310c(C3674tb tbVar) {
        for (aBN next : this.aWj) {
            if (next.mo16837YU() != null) {
                next.mo16837YU().mo20690a(tbVar);
            } else {
                next.mo758a(tbVar);
            }
        }
    }

    /* renamed from: c */
    public void mo21309c(C2724jE jEVar) {
        for (aBN next : this.aWj) {
            next.mo16837YU().mo20689a(jEVar);
            if (next.mo16837YU().mo20699eo() != null) {
                next.mo16837YU().mo20690a(next.mo16837YU().mo20699eo());
            }
        }
    }

    /* renamed from: XA */
    public void mo21305XA() {
        this.aWj.clear();
        this.aWi.clear();
    }

    /* renamed from: XB */
    public void mo21306XB() {
        for (aBN c : this.aWj) {
            this.aWl.mo13081c(c);
        }
        mo21305XA();
    }
}
