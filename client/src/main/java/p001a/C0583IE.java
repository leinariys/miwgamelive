package p001a;

import game.script.space.Node;

import javax.swing.tree.DefaultMutableTreeNode;

/* renamed from: a.IE */
/* compiled from: a */
public class C0583IE extends DefaultMutableTreeNode {
    public C0583IE(Node rPVar) {
        super(rPVar);
    }

    public String toString() {
        if (getUserObject() instanceof Node) {
            return ((Node) getUserObject()).mo21665ke().get();
        }
        return C0583IE.super.toString();
    }
}
