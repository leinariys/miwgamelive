package p001a;

import taikodom.addon.IAddonProperties;
import taikodom.addon.neo.equipment.C0258DL;

import javax.swing.*;

/* renamed from: a.qd */
/* compiled from: a */
public abstract class C3357qd extends C0258DL {
    private C0539Ha aUt;

    public C3357qd(IAddonProperties vWVar) {
        super(vWVar);
        this.bgR.mo17429aw(true);
    }

    /* access modifiers changed from: protected */
    public TransferHandler getTransferHandler() {
        if (this.aUt == null) {
            this.aUt = new C0539Ha();
        }
        this.aUt.mo2646b(this.f395kj.getPlayer().bQx().mo18253C(mo1299qc()));
        return this.aUt;
    }
}
