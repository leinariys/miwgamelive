package p001a;

import java.util.Comparator;

/* renamed from: a.aeS  reason: case insensitive filesystem */
/* compiled from: a */
class C5994aeS implements Comparator {
    C5994aeS() {
    }

    public int compare(Object obj, Object obj2) {
        int length = obj instanceof C0223Co.C0224a ? ((C0223Co.C0224a) obj).value : ((Object[]) obj).length;
        int length2 = obj2 instanceof C0223Co.C0224a ? ((C0223Co.C0224a) obj2).value : ((Object[]) obj2).length;
        if (length > length2) {
            return 1;
        }
        return length < length2 ? -1 : 0;
    }
}
