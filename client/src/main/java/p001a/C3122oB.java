package p001a;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.oB */
/* compiled from: a */
public @interface C3122oB {

    /* renamed from: Uq */
    C3123a mo20937Uq();

    /* renamed from: Ur */
    long mo20938Ur() default 5000;

    /* renamed from: a.oB$a */
    public enum C3123a {
        NOT_REPLICATED,
        TIME_BASED,
        IMMEDIATE
    }
}
