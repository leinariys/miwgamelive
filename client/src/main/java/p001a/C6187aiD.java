package p001a;

import logic.res.code.C1625Xn;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import taikodom.game.main.ClientMain;

import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: a.aiD  reason: case insensitive filesystem */
/* compiled from: a */
public class C6187aiD implements Executor {
    /* access modifiers changed from: private */
    public static final Log logger = LogPrinter.setClass(C6187aiD.class);
    /* access modifiers changed from: private */
    public final ClientMain gQb;
    LinkedBlockingQueue<Runnable> gQc = new LinkedBlockingQueue<>();

    public C6187aiD(ClientMain clientMain) {
        this.gQb = clientMain;
    }

    public void execute(Runnable runnable) {
        try {
            this.gQc.put(new C1896a(runnable));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (RuntimeException e2) {
            e2.printStackTrace();
            throw e2;
        }
    }

    public void cCM() {
        while (true) {
            Runnable poll = this.gQc.poll();
            if (poll != null) {
                this.gQb.dTr.submit(new C1897b(poll));
            } else {
                return;
            }
        }
    }

    /* renamed from: a.aiD$a */
    class C1896a implements Runnable {
        private final /* synthetic */ Runnable bsv;

        C1896a(Runnable runnable) {
            this.bsv = runnable;
        }

        public void run() {
            synchronized (C6187aiD.this.gQb.engineGraphics.aee().getTreeLock()) {
                C1625Xn cYc = C6187aiD.this.gQb.dTs.cYc();
                try {
                    cYc.brK();
                    this.bsv.run();
                    cYc.brL();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    cYc.brL();
                } catch (Throwable th) {
                    cYc.brL();
                    throw th;
                }
            }
        }
    }

    /* renamed from: a.aiD$b */
    /* compiled from: a */
    class C1897b implements Runnable {
        private final /* synthetic */ Runnable fOd;

        C1897b(Runnable runnable) {
            this.fOd = runnable;
        }

        public void run() {
            try {
                this.fOd.run();
            } catch (Throwable th) {
                C6187aiD.logger.warn("Exception while executing a task", th);
            }
        }
    }
}
