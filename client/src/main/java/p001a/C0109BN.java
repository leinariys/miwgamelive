package p001a;

import org.mozilla1.javascript.ScriptRuntime;

/* renamed from: a.BN */
/* compiled from: a */
class C0109BN extends C3311qA {
    public C0109BN(boolean z, C6365alZ alz) {
        super(z, ScriptRuntime.NaN, alz);
    }

    /* renamed from: a */
    public boolean mo728a(C6365alZ alz) {
        C6365alZ alz2 = this.aVW;
        if (alz2.fYn.aVX <= alz.fYq.aVX && alz.fYn.aVX <= alz2.fYq.aVX && alz2.fYl.aVX <= alz.fYo.aVX && alz.fYl.aVX <= alz2.fYo.aVX && alz2 != alz) {
            return true;
        }
        return false;
    }
}
