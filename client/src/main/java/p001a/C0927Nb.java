package p001a;

import logic.res.KeyCode;

import java.util.HashMap;
import java.util.Map;

/* renamed from: a.Nb */
/* compiled from: a */
public class C0927Nb {
    private static final Map<Integer, Integer> dCX = new HashMap();
    private static final Map<Integer, Integer> dCY = new HashMap();

    static {
        m7691L(8, 8);
        m7691L(9, 9);
        m7691L(12, 12);
        m7691L(10, 13);
        m7691L(19, 19);
        m7691L(27, 27);
        m7691L(32, 32);
        m7691L(517, 33);
        m7691L(152, 34);
        m7691L(515, 36);
        m7691L(150, 38);
        m7691L(KeyCode.csZ, 39);
        m7691L(519, 40);
        m7691L(522, 41);
        m7691L(151, 42);
        m7691L(521, 43);
        m7691L(44, 44);
        m7691L(45, 45);
        m7691L(46, 46);
        m7691L(47, 47);
        m7691L(48, 48);
        m7691L(49, 49);
        m7691L(50, 50);
        m7691L(51, 51);
        m7691L(52, 52);
        m7691L(53, 53);
        m7691L(54, 54);
        m7691L(55, 55);
        m7691L(56, 56);
        m7691L(57, 57);
        m7691L(513, 58);
        m7691L(59, 59);
        m7691L(153, 60);
        m7691L(61, 61);
        m7691L(160, 62);
        m7691L(512, 64);
        m7691L(91, 91);
        m7691L(92, 92);
        m7691L(93, 93);
        m7691L(523, 95);
        m7691L(192, 96);
        m7691L(65, 97);
        m7691L(66, 98);
        m7691L(67, 99);
        m7691L(68, 100);
        m7691L(69, 101);
        m7691L(70, 102);
        m7691L(71, 103);
        m7691L(72, 104);
        m7691L(73, 105);
        m7691L(74, 106);
        m7691L(75, 107);
        m7691L(76, 108);
        m7691L(77, 109);
        m7691L(78, 110);
        m7691L(79, 111);
        m7691L(80, 112);
        m7691L(81, 113);
        m7691L(82, 114);
        m7691L(83, 115);
        m7691L(84, 116);
        m7691L(85, 117);
        m7691L(86, 118);
        m7691L(87, 119);
        m7691L(88, 120);
        m7691L(89, 121);
        m7691L(90, 122);
        m7691L(127, 127);
        m7691L(96, 256);
        m7691L(97, KeyCode.ctI);
        m7691L(98, KeyCode.ctJ);
        m7691L(99, KeyCode.ctK);
        m7691L(100, KeyCode.ctL);
        m7691L(101, KeyCode.ctM);
        m7691L(102, KeyCode.ctN);
        m7691L(103, KeyCode.ctO);
        m7691L(104, KeyCode.ctP);
        m7691L(105, KeyCode.ctQ);
        m7691L(46, KeyCode.ctR);
        m7691L(111, KeyCode.ctS);
        m7691L(106, KeyCode.ctT);
        m7691L(109, KeyCode.ctU);
        m7691L(107, KeyCode.ctV);
        m7691L(38, KeyCode.f209UP);
        m7691L(40, KeyCode.DOWN);
        m7691L(39, KeyCode.RIGHT);
        m7691L(37, KeyCode.LEFT);
        m7691L(155, KeyCode.INSERT);
        m7691L(36, KeyCode.HOME);
        m7691L(35, KeyCode.END);
        m7691L(33, KeyCode.ctY);
        m7691L(34, KeyCode.ctZ);
        m7691L(112, KeyCode.f200F1);
        m7691L(113, KeyCode.f201F2);
        m7691L(114, KeyCode.f202F3);
        m7691L(115, KeyCode.f203F4);
        m7691L(116, KeyCode.f204F5);
        m7691L(117, KeyCode.f205F6);
        m7691L(118, KeyCode.f206F7);
        m7691L(119, KeyCode.f207F8);
        m7691L(120, KeyCode.f208F9);
        m7691L(121, KeyCode.F10);
        m7691L(122, KeyCode.F11);
        m7691L(123, KeyCode.F12);
        m7691L(61440, KeyCode.F13);
        m7691L(61441, KeyCode.F14);
        m7691L(61442, KeyCode.F15);
        m7691L(144, KeyCode.cua);
        m7691L(20, KeyCode.cub);
        m7691L(145, KeyCode.cuc);
        m7691L(16, KeyCode.LSHIFT);
        m7691L(17, KeyCode.cue);
        m7691L(18, KeyCode.cug);
        m7691L(157, KeyCode.cui);
        m7691L(31, KeyCode.cul);
        m7691L(65312, KeyCode.cum);
        m7691L(156, KeyCode.HELP);
        m7691L(154, KeyCode.cun);
        m7691L(19, KeyCode.BREAK);
        m7691L(525, KeyCode.MENU);
        m7691L(516, KeyCode.cuq);
    }

    /* renamed from: L */
    private static void m7691L(int i, int i2) {
        dCX.put(Integer.valueOf(i), Integer.valueOf(i2));
        dCY.put(Integer.valueOf(i2), Integer.valueOf(i));
    }

    /* renamed from: lP */
    public static int m7693lP(int i) {
        Integer num = dCX.get(Integer.valueOf(i));
        return num == null ? i : num.intValue();
    }

    /* renamed from: cb */
    public static int m7692cb(int i) {
        Integer num = dCY.get(Integer.valueOf(i));
        return num == null ? i : num.intValue();
    }

    /* renamed from: lQ */
    public static int m7694lQ(int i) {
        int i2 = 0;
        if ((i & 512) != 0) {
            i2 = 256;
        }
        if ((i & 128) != 0) {
            i2 |= 64;
        }
        if ((i & 64) != 0) {
            i2 |= 1;
        }
        if ((i & 32) != 0) {
            return i2 | 32;
        }
        return i2;
    }

    /* renamed from: lR */
    public static int m7695lR(int i) {
        int i2 = 0;
        if ((i & 256) != 0) {
            i2 = 512;
        }
        if ((i & 64) != 0) {
            i2 |= 128;
        }
        if ((i & 1) != 0) {
            i2 |= 64;
        }
        if ((i & 32) != 0) {
            return i2 | 32;
        }
        return i2;
    }
}
