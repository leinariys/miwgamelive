package p001a;

import game.network.message.externalizable.C3131oI;
import game.script.Actor;
import game.script.citizenship.Citizenship;
import game.script.citizenship.CitizenshipControl;
import game.script.player.Player;
import game.script.progression.CharacterEvolution;
import game.script.ship.Outpost;
import game.script.ship.OutpostType;
import game.script.ship.Station;
import logic.aaa.C1274Sq;
import logic.baa.C5473aKr;
import logic.baa.C6200aiQ;
import logic.data.link.C1092Px;
import logic.data.link.C3581sd;
import logic.data.link.C6360alU;
import logic.data.link.aEX;
import logic.res.code.C5663aRz;
import logic.swing.C5378aHa;
import logic.ui.Panel;
import logic.ui.item.C0567Hq;
import logic.ui.item.Picture;
import taikodom.addon.IAddonProperties;
import taikodom.addon.neo.sidebar.SideBarManagerAddon;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.tB */
/* compiled from: a */
public class C3629tB extends Panel implements C0567Hq {
    private static final long serialVersionUID = 8097829273038930068L;
    /* renamed from: kj */
    public final IAddonProperties f9208kj;
    /* access modifiers changed from: private */
    private final boolean aTI;
    /* renamed from: P */
    public Player f9207P;
    /* access modifiers changed from: private */
    public C6200aiQ<Outpost> bCB;
    /* access modifiers changed from: private */
    public C3428rT<C3131oI> bCD;
    /* renamed from: DV */
    private Panel f9206DV;
    private C5473aKr<CitizenshipControl, Object> bCA;
    private C3428rT<C1274Sq> bCC;
    private JLabel bCr;
    private JLabel bCs;
    private JLabel bCt;
    private JLabel bCu;
    private Picture bCv;
    private Picture bCw;
    private C6200aiQ<Player> bCx;
    private C6200aiQ<Player> bCy;
    /* access modifiers changed from: private */
    private C6200aiQ<CharacterEvolution> bCz;

    public C3629tB(IAddonProperties vWVar, boolean z) {
        super(new GridLayout());
        this.f9208kj = vWVar;
        this.aTI = z;
        this.f9207P = vWVar.getPlayer();
        this.f9206DV = (Panel) vWVar.bHv().mo16794bQ("toppanel.xml");
        addImpl(this.f9206DV, (Object) null, -1);
        m39460iM();
        if (z) {
            amf();
        } else {
            amg();
        }
        m39447Fa();
    }

    /* renamed from: iM */
    private void m39460iM() {
        this.bCr = mo4917cf("label1");
        this.bCs = mo4917cf("label2");
        this.bCt = mo4917cf("label3");
        this.bCu = mo4917cf("label4");
        this.bCv = mo4915cd("left-icon");
        this.bCw = mo4915cd("right-icon");
    }

    /* renamed from: Fa */
    private void m39447Fa() {
        if (this.aTI) {
            this.bCx = new C3631b();
            this.f9207P.mo8320a(C1092Px.dQN, (C6200aiQ<?>) this.bCx);
            this.bCy = new C3630a();
            this.f9207P.mo8320a(C1092Px.anV, (C6200aiQ<?>) this.bCy);
            this.bCz = new C3635f();
            this.f9207P.mo12659ly().mo20722Rp().mo8320a(aEX.bgt, (C6200aiQ<?>) this.bCz);
            this.bCA = new C3634e();
            this.f9207P.cXk().mo8319a(C3581sd.f9143Vr, (C5473aKr<?, ?>) this.bCA);
            if (!SideBarManagerAddon.m44918iL() && this.f9207P.mo12637Rn() == null) {
                this.bCD = new C3633d();
                this.f9208kj.aVU().mo13965a(C3131oI.class, this.bCD);
                return;
            }
            return;
        }
        this.bCB = new C3632c();
        this.bCC = new C3636g();
        this.f9208kj.aVU().mo13965a(C1274Sq.class, this.bCC);
    }

    private void amf() {
        ami();
        amh();
        amj();
        amk();
        aml();
        amm();
    }

    /* access modifiers changed from: private */
    public void amg() {
        this.bCr.setVisible(false);
        this.bCu.setVisible(false);
        this.bCv.setVisible(false);
        amn();
        amo();
        amp();
    }

    /* access modifiers changed from: private */
    public void amh() {
        if (this.f9207P.isUnnamed()) {
            this.bCs.setText(this.f9208kj.translate("<Tutorial Pending>"));
        } else {
            this.bCs.setText(this.f9207P.getName());
        }
    }

    private void ami() {
        this.bCr.setVisible(false);
    }

    /* access modifiers changed from: private */
    public void amj() {
        if (this.f9207P.mo12637Rn() != null) {
            this.bCt.setText(this.f9207P.mo12637Rn().mo21959rP().get());
            this.bCt.setVisible(true);
            return;
        }
        this.bCt.setVisible(false);
    }

    /* access modifiers changed from: private */
    public void amk() {
        this.bCu.setText(String.valueOf(this.f9208kj.translate("Rank")) + " " + this.f9207P.mo12658lt());
    }

    /* access modifiers changed from: private */
    public void aml() {
        if (this.f9207P.bYd() != null) {
            this.bCw.mo16824a(C5378aHa.CORP_IMAGES, this.f9207P.bYd().mo10706Qu().aOQ());
            this.bCw.setVisible(true);
            return;
        }
        this.bCw.setVisible(false);
    }

    /* access modifiers changed from: private */
    public void amm() {
        if (this.f9207P.cXk().bqx().size() > 0) {
            Citizenship vm = (Citizenship) this.f9207P.cXk().bqx().get(0);
            this.bCv.mo16824a(C5378aHa.CITIZENSHIP, vm.bJB().mo12811sK().getHandle());
            this.bCv.setVisible(true);
            this.bCv.setToolTipText(C5956adg.format(this.f9208kj.translate("Expiration in {0} {1}"), m39453dn(vm.getExpiration()), m39454do(vm.getExpiration())));
            return;
        }
        this.bCv.setVisible(false);
    }

    private void amn() {
        String str;
        JLabel jLabel = this.bCs;
        if (this.f9207P.bQB()) {
            str = this.f9207P.bhE().getName();
        } else {
            str = this.f9207P.bQx().agH().mo19891ke().get();
        }
        jLabel.setText(str);
    }

    private void amo() {
        String str;
        Actor bhE = this.f9207P.bhE();
        if (bhE instanceof Station) {
            if (((Station) bhE).azP() instanceof OutpostType) {
                aHK beD = ((OutpostType) ((Station) bhE).azP()).beD();
                str = this.f9208kj.translate("Outpost");
                if (beD == aHK.HABITAT) {
                    str = String.valueOf(str) + ": " + this.f9208kj.translate("Habitat");
                } else if (beD == aHK.INDUSTRY) {
                    str = String.valueOf(str) + ": " + this.f9208kj.translate("Industry");
                } else if (beD == aHK.MILITARY) {
                    str = String.valueOf(str) + ": " + this.f9208kj.translate("Military");
                }
            } else {
                str = this.f9208kj.translate("Station");
            }
        } else if (this.f9207P.bQx() != null) {
            str = this.f9207P.bQx().agH().mo19866HC().mo12246ke().get();
        } else {
            str = "";
        }
        this.bCt.setText(str);
    }

    /* access modifiers changed from: private */
    public void amp() {
        Actor bhE = this.f9207P.bhE();
        if (!(bhE instanceof Outpost) || ((Outpost) bhE).bYd() == null) {
            this.bCw.setVisible(false);
            return;
        }
        this.bCw.mo16824a(C5378aHa.CORP_IMAGES, ((Outpost) bhE).bYd().mo10706Qu().aOQ());
        this.bCw.setVisible(true);
    }

    /* renamed from: dn */
    private String m39453dn(long j) {
        long j2 = j / 3600000;
        long j3 = (j - (3600000 * j2)) / 60000;
        long j4 = (j2 - (j2 % 24)) / 24;
        if (j4 > 0) {
            return String.valueOf(j4);
        }
        return String.format("%02d:%02d", new Object[]{Long.valueOf(j2), Long.valueOf(j3)});
    }

    /* renamed from: do */
    private String m39454do(long j) {
        long j2 = j / 3600000;
        long j3 = j - (3600000 * j2);
        if ((j2 - (j2 % 24)) / 24 <= 0) {
            return this.f9208kj.translate("hour(s)");
        }
        return this.f9208kj.translate("days");
    }

    public boolean isPinned() {
        return false;
    }

    /* renamed from: aA */
    public boolean mo878aA() {
        return false;
    }

    public void clear() {
        if (this.aTI) {
            this.f9207P.mo8326b(C1092Px.dQN, (C6200aiQ<?>) this.bCx);
            this.f9207P.mo12659ly().mo20722Rp().mo8326b(aEX.bgt, (C6200aiQ<?>) this.bCz);
            this.f9207P.cXk().mo8325b(C3581sd.f9143Vr, (C5473aKr<?, ?>) this.bCA);
            return;
        }
        this.f9208kj.aVU().mo13968b(C1274Sq.class, this.bCC);
        if (this.f9207P.bhE() instanceof Outpost) {
            ((Outpost) this.f9207P.bhE()).mo8326b(C6360alU.dQN, (C6200aiQ<?>) this.bCB);
        }
    }

    /* renamed from: a.tB$b */
    /* compiled from: a */
    class C3631b implements C6200aiQ<Player> {
        C3631b() {
        }

        /* renamed from: a */
        public void mo1143a(Player aku, C5663aRz arz, Object obj) {
            C3629tB.this.aml();
        }
    }

    /* renamed from: a.tB$a */
    class C3630a implements C6200aiQ<Player> {
        C3630a() {
        }

        /* renamed from: a */
        public void mo1143a(Player aku, C5663aRz arz, Object obj) {
            C3629tB.this.amh();
        }
    }

    /* renamed from: a.tB$f */
    /* compiled from: a */
    class C3635f implements C6200aiQ<CharacterEvolution> {
        C3635f() {
        }

        /* renamed from: a */
        public void mo1143a(CharacterEvolution eqVar, C5663aRz arz, Object obj) {
            C3629tB.this.amk();
        }
    }

    /* renamed from: a.tB$e */
    /* compiled from: a */
    class C3634e implements C5473aKr<CitizenshipControl, Object> {
        C3634e() {
        }

        /* renamed from: a */
        public void mo1102b(CitizenshipControl qo, C5663aRz arz, Object[] objArr) {
            C3629tB.this.amm();
        }

        /* renamed from: b */
        public void mo1101a(CitizenshipControl qo, C5663aRz arz, Object[] objArr) {
            C3629tB.this.amm();
        }
    }

    /* renamed from: a.tB$d */
    /* compiled from: a */
    class C3633d extends C3428rT<C3131oI> {
        C3633d() {
        }

        /* renamed from: b */
        public void mo321b(C3131oI oIVar) {
            if (oIVar.mo20956Us() == C3131oI.C3132a.CREATED_AVATAR) {
                C3629tB.this.amj();
                C3629tB.this.f9208kj.aVU().mo13964a(C3629tB.this.bCD);
                C3629tB.this.bCD = null;
            }
        }
    }

    /* renamed from: a.tB$c */
    /* compiled from: a */
    class C3632c implements C6200aiQ<Outpost> {
        C3632c() {
        }

        /* renamed from: a */
        public void mo1143a(Outpost qZVar, C5663aRz arz, Object obj) {
            C3629tB.this.amp();
        }
    }

    /* renamed from: a.tB$g */
    /* compiled from: a */
    class C3636g extends C3428rT<C1274Sq> {
        private Outpost baT = null;

        C3636g() {
        }

        /* renamed from: a */
        public void mo321b(C1274Sq sq) {
            if (sq.fMR == C1274Sq.C1275a.DOCK) {
                if (C3629tB.this.f9207P.bhE() instanceof Outpost) {
                    this.baT = (Outpost) C3629tB.this.f9207P.bhE();
                    this.baT.mo8320a(C6360alU.dQN, (C6200aiQ<?>) C3629tB.this.bCB);
                }
            } else if (sq.fMR == C1274Sq.C1275a.UNDOCK && this.baT != null) {
                this.baT.mo8326b(C6360alU.dQN, (C6200aiQ<?>) C3629tB.this.bCB);
                this.baT = null;
            }
            C3629tB.this.amg();
        }
    }
}
