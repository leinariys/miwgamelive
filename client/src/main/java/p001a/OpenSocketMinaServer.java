package p001a;

import game.network.channel.Connect;
import game.network.channel.client.ClientConnect;
import game.network.message.TransportCommand;
import game.network.message.externalizable.VersionExt;
import logic.thred.LogPrinter;
import org.apache.mina.core.future.IoFutureListener;
import org.apache.mina.core.future.WriteFuture;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: a.dO */
/* compiled from: a */
public class OpenSocketMinaServer extends OpenSocketMina {
    static LogPrinter logger = LogPrinter.m10275K(OpenSocketMinaServer.class);
    private final int port;
    public Map<Integer, Boolean> gep = new ConcurrentHashMap();

    public OpenSocketMinaServer(int i, String str) {
        super(ClientConnect.Attitude.PARENT, new NioSocketAcceptor(), str);
        this.port = i;
    }

    /* renamed from: iX */
    public boolean isRunningThreadSelectableChannel() {
        return true;
    }

    /* renamed from: a */
    public void creatListenerChannel(Connect omVar) {
        getService().setHandler(new C2259a(this, omVar, (C2259a) null));
        try {
            InetSocketAddress inetSocketAddress = new InetSocketAddress(this.port);
            this.gep.put(Integer.valueOf(this.port), true);
            clC().bind(inetSocketAddress);
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("server is listening at port " + this.port);
    }

    private NioSocketAcceptor clC() {
        return getService();
    }

    /* renamed from: a */
    public void buildListenerChannel(int listenerPort, int[] iArr, int[] iArr2, boolean z) {
        InetSocketAddress inetSocketAddress = new InetSocketAddress(listenerPort);
        this.gep.put(Integer.valueOf(listenerPort), Boolean.valueOf(z));
        clC().bind(inetSocketAddress);
        logger.info("server is listening at port " + listenerPort);
    }

    /* renamed from: iY */
    public boolean isListenerPort() {
        return true;
    }

    /* renamed from: jh */
    public InetAddress getIpRemoteHost() {
        return null;
    }

    /* renamed from: a.dO$a */
    private final class C2259a extends IoHandlerAdapter {

        /* renamed from: AD */
        private static /* synthetic */ int[] f6501AD;

        /* renamed from: AB */
        private final Connect f6502AB;

        /* synthetic */ C2259a(OpenSocketMinaServer dOVar, Connect omVar, C2259a aVar) {
            this(omVar);
        }

        private C2259a(Connect omVar) {
            this.f6502AB = omVar;
        }

        /* renamed from: kj */
        static /* synthetic */ int[] m28833kj() {
            int[] iArr = f6501AD;
            if (iArr == null) {
                iArr = new int[OpenSocketMina.C2680a.values().length];
                try {
                    iArr[OpenSocketMina.C2680a.CLOSED.ordinal()] = 3;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[OpenSocketMina.C2680a.HANDSHAKING.ordinal()] = 1;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[OpenSocketMina.C2680a.UP.ordinal()] = 2;
                } catch (NoSuchFieldError e3) {
                }
                f6501AD = iArr;
            }
            return iArr;
        }

        public void exceptionCaught(IoSession ioSession, Throwable th) {
            th.printStackTrace();
        }

        public void sessionOpened(IoSession ioSession) {
            C5772aaE aae = new C5772aaE(ioSession);
            aae.mo12141a(OpenSocketMina.C2680a.HANDSHAKING);
            ioSession.setAttribute(OpenSocketMinaServer.anW, aae);
            WriteFuture write = ioSession.write(new VersionExt(OpenSocketMinaServer.this.getVersionString()));
            if (!OpenSocketMinaServer.this.gep.get(Integer.valueOf(((InetSocketAddress) ioSession.getLocalAddress()).getPort())).booleanValue()) {
                write.addListener(new C2260a(aae));
            }
        }

        public void messageReceived(IoSession ioSession, Object obj) {
            C5772aaE a = OpenSocketMinaServer.m33375a(ioSession);
            switch (m28833kj()[a.bLH().ordinal()]) {
                case 2:
                    OpenSocketMinaServer.this.mo19614HV().addToPoolExecuterCommand(a, (TransportCommand) obj);
                    return;
                default:
                    return;
            }
        }

        public void sessionClosed(IoSession ioSession) {
            C5772aaE aae = (C5772aaE) ioSession.removeAttribute(OpenSocketMinaServer.anW);
            this.f6502AB.userDisconnected(aae);
            aae.mo12141a(OpenSocketMina.C2680a.CLOSED);
        }

        /* renamed from: a.dO$a$a */
        class C2260a implements IoFutureListener<WriteFuture> {
            private final /* synthetic */ C5772aaE gTJ;

            C2260a(C5772aaE aae) {
                this.gTJ = aae;
            }

            public void operationComplete(WriteFuture writeFuture) {
                this.gTJ.mo12141a(OpenSocketMina.C2680a.UP);
            }
        }
    }
}
