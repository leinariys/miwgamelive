package p001a;

import game.io.IReadExternal;
import game.io.IWriteExternal;
import game.network.message.externalizable.C1291Sy;
import game.network.message.serializable.C1556Wo;
import logic.baa.C1616Xf;
import logic.data.mbean.C0677Jd;
import logic.res.code.C2759jd;
import logic.res.html.DataClassSerializer;

import java.lang.reflect.Field;

/* renamed from: a.arT  reason: case insensitive filesystem */
/* compiled from: a */
public class C6671arT extends C2759jd {

    public DataClassSerializer gtN;
    public DataClassSerializer gtO;

    public C6671arT(Class<?> cls, Field field, String str, Class<?>[] clsArr) {
        super(cls, field, str, clsArr);
    }

    public DataClassSerializer csE() {
        if (this.gtN == null) {
            csF();
        }
        return this.gtN;
    }

    private void csF() {
        DataClassSerializer[] Eu = mo11298Eu();
        if (Eu == null || Eu.length != 2) {
            this.gtN = DataClassSerializer.dnL;
            this.gtO = DataClassSerializer.dnL;
            return;
        }
        this.gtN = Eu[0];
        this.gtO = Eu[1];
    }

    public DataClassSerializer csG() {
        if (this.gtO == null) {
            csF();
        }
        return this.gtO;
    }

    /* renamed from: z */
    public Object mo2181z(Object obj) {
        if (obj == null) {
            return null;
        }
        return ((C1556Wo) obj).clone();
    }

    /* renamed from: yl */
    public boolean mo2180yl() {
        return true;
    }

    /* renamed from: b */
    public Object mo2188b(C1616Xf xf) {
        return new C1291Sy(xf, this);
    }

    /* renamed from: d */
    public Object mo11307d(Object obj, Object obj2) {
        C1556Wo wo = (C1556Wo) obj;
        C1556Wo wo2 = (C1556Wo) obj2;
        return (wo == null || !wo.equals(wo2)) ? wo2 : obj;
    }

    /* renamed from: c */
    public C5546aNm mo11306c(Object obj, Object obj2) {
        return null;
    }

    /* renamed from: a */
    public Object mo2185a(C0677Jd jd, Object obj) {
        return super.mo2185a(jd, obj);
    }

    /* renamed from: Ek */
    public boolean mo2179Ek() {
        return false;
    }

    /* renamed from: a */
    public Object mo2186a(C1616Xf xf, IReadExternal vm) {
        boolean gR = vm.mo6354gR("null");
        String property = System.getProperty("FIX_COLLECTIONS");
        if (property != null && property.equalsIgnoreCase("TRUE")) {
            return super.mo2186a(xf, vm);
        }
        if (gR) {
            return vm.mo6366hd("unknow");
        }
        C1291Sy sy = new C1291Sy(xf, this);
        sy.readExternal(vm);
        return sy;
    }

    /* renamed from: a */
    public void mo2187a(C1616Xf xf, IWriteExternal att, Object obj) {
        boolean z = obj == null;
        att.writeBoolean("null", z);
        if (z) {
            att.mo16273g("unknow", obj);
        } else {
            ((C1291Sy) obj).writeExternal(att);
        }
    }
}
