package p001a;

import java.io.Serializable;

/* renamed from: a.aHb  reason: case insensitive filesystem */
/* compiled from: a */
public final class C5379aHb implements Serializable, Cloneable {

    private long eKo;
    private float hUC;
    private float hUD;
    private long hUE;

    C5379aHb() {
    }

    /* renamed from: kR */
    public float mo9243kR(long j) {
        if (j > this.hUE) {
            return this.hUD;
        }
        if (j < this.eKo) {
            return this.hUC;
        }
        return this.hUC + ((((float) (j - this.eKo)) * (this.hUD - this.hUC)) / ((float) (this.hUE - this.eKo)));
    }

    public Object clone() {
        return this;
    }
}
