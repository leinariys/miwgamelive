package p001a;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

/* renamed from: a.rC */
/* compiled from: a */
public class TimeServer {
    private ServerThread serverThread;
    private boolean daemon = true;
    private SocketAddress socketAddress;
    private DatagramChannel datagramChannel;
    private ByteBuffer buffer;

    public TimeServer(int listenerPort) {
        this.socketAddress = new InetSocketAddress((InetAddress) null, listenerPort);
    }

    public static void main(String[] strArr) throws IOException {
        TimeServer rCVar = new TimeServer(11333);
        rCVar.setDaemon(false);
        rCVar.start();
    }

    public boolean isDaemon() {
        return this.daemon;
    }

    public void setDaemon(boolean z) {
        this.daemon = z;
    }

    public void start() throws IOException {
        this.datagramChannel = DatagramChannel.open();
        this.datagramChannel.socket().bind(this.socketAddress);
        this.buffer = ByteBuffer.allocateDirect(64);
        this.serverThread = new ServerThread();
        this.serverThread.setDaemon(this.daemon);
        this.serverThread.start();
    }

    /* access modifiers changed from: private */
    /* renamed from: Ys */
    public void run() {
        try {
            this.buffer.clear();
            SocketAddress receive = this.datagramChannel.receive(this.buffer);
            if (this.buffer.position() == 16) {
                this.buffer.putLong(getCurrentTimeMillis());
                this.buffer.putLong(getNanoTime());
                this.buffer.flip();
                this.datagramChannel.send(this.buffer, receive);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: Yt */
    public long getNanoTime() {
        return System.nanoTime();
    }

    /* access modifiers changed from: protected */
    /* renamed from: Yu */
    public long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }

    /* renamed from: a.rC$a */
    public class ServerThread extends Thread {
        public ServerThread() {
            super("Time server");
        }

        public void run() {
            while (!Thread.interrupted()) {
                TimeServer.this.run();
            }
        }
    }
}
