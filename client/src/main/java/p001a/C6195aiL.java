package p001a;

/* renamed from: a.aiL  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6195aiL {
    String bau();

    String bav();

    long ccC();

    boolean getBoolean(String str);

    byte getByte(String str);

    double getDouble(String str);

    float getFloat(String str);

    int getInt(String str);

    long getLong(String str);

    <T> T getObject(String str);

    short getShort(String str);

    /* renamed from: iU */
    boolean mo13684iU(String str);

    /* renamed from: iV */
    char mo13685iV(String str);
}
