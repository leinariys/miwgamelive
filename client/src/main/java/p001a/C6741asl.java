package p001a;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.asl  reason: case insensitive filesystem */
/* compiled from: a */
public @interface C6741asl {

    C1987a cub() default C1987a.AT_ALL;

    /* renamed from: a.asl$a */
    public enum C1987a {
        AT_ALL,
        ON_TOPLEVEL,
        ON_CHILDREN
    }
}
