package p001a;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.pc */
/* compiled from: a */
public @interface C3248pc {

    boolean aYQ() default false;

    C3250b aYR() default C3250b.NONE;

    String aYS() default "";

    C3249a aYT() default C3249a.PLAIN;

    String displayName() default "";

    /* renamed from: a.pc$a */
    public enum C3249a {
        PLAIN,
        MISSION_SCRIPT
    }

    /* renamed from: a.pc$b */
    /* compiled from: a */
    public enum C3250b {
        NONE,
        ACTION,
        GETTER,
        SETTER,
        ALTERNATE_SETTER,
        REMOVER,
        ADDER,
        ALTERNATE_ADDER,
        ITERATOR
    }
}
