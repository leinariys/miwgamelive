package p001a;

import com.hoplon.geometry.Vec3f;
import game.script.Actor;
import game.script.ship.Ship;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.asZ  reason: case insensitive filesystem */
/* compiled from: a */
public class C6729asZ extends C4031yM {
    private boolean fbN;
    private Vec3f gxN;
    private Vec3f gxO;

    public C6729asZ() {
    }

    public C6729asZ(int i, Actor cr, C1003Om om, long j) {
        super(i, cr, om, j);
        if (this.gxN == null) {
            this.gxN = new Vec3f();
        }
        if (this.gxO == null) {
            this.gxO = new Vec3f();
        }
        if (cr instanceof Ship) {
            this.gxN.set(((Ship) cr).aip());
            this.gxO.set(((Ship) cr).aip());
            this.fbN = ((Ship) cr).ahO().aXW();
            return;
        }
        this.gxN.set(0.0f, 0.0f, -1.0f);
        this.gxO.set(0.0f, 0.0f, -1.0f);
    }

    /* renamed from: a */
    public void mo12575a(C1355Tk tk) {
        C0520HN cLd;
        super.mo12575a(tk);
        if (mo23123ha() != null && ((C1298TD) mo23123ha().bFf()) != null && (cLd = mo23123ha().cLd()) != null && (cLd instanceof C6901avp)) {
            if (!mo23123ha().bGX() || mo23123ha() != mo23123ha().ala().aLW().ald().mo4089dL().bhE()) {
                C6901avp avp = (C6901avp) cLd;
                avp.mo16631aX(this.gxN);
                avp.mo16629E(this.gxO);
                if (avp.aXW() != this.fbN) {
                    avp.mo16635ec(this.fbN);
                    if (avp.aQX() != null && (avp.aQX() instanceof C5917act)) {
                        C5917act act = (C5917act) avp.aQX();
                        if (act.aXW() != this.fbN) {
                            act.mo12722ec(this.fbN);
                        }
                    }
                }
            }
        }
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        this.gxN = new Vec3f(objectInput.readFloat(), objectInput.readFloat(), objectInput.readFloat());
        this.gxO = new Vec3f(objectInput.readFloat(), objectInput.readFloat(), objectInput.readFloat());
        this.fbN = objectInput.readBoolean();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        objectOutput.writeFloat(this.gxN.x);
        objectOutput.writeFloat(this.gxN.y);
        objectOutput.writeFloat(this.gxN.z);
        objectOutput.writeFloat(this.gxO.x);
        objectOutput.writeFloat(this.gxO.y);
        objectOutput.writeFloat(this.gxO.z);
        objectOutput.writeBoolean(this.fbN);
    }

    public String toString() {
        return "ShipPacketData[" + super.toString() + " aim: " + this.gxN + " desired: " + this.gxO + "]";
    }

    public boolean aXW() {
        return this.fbN;
    }

    /* renamed from: ec */
    public void mo15933ec(boolean z) {
        this.fbN = z;
    }
}
