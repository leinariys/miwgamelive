package p001a;

import com.hoplon.geometry.Vec3f;

import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;
import java.util.Iterator;

/* renamed from: a.qt */
/* compiled from: a */
public class C3381qt implements Iterable<Vec3f> {
    private static final float eta = 1.0E-30f;
    private final Vec3f aAq = new Vec3f();
    private final Vec3f aAr = new Vec3f();
    private final Vec3f etb = new Vec3f();
    private final Vec3f etc = new Vec3f();
    private final Vec3f etd = new Vec3f();
    private final Vec3f ete = new Vec3f();
    private final Vec3f etf = new Vec3f();
    private final Vec3f etg = new Vec3f();
    private final Vec3f eth = new Vec3f();
    private final Vec3f eti = new Vec3f();
    private final Vec3f etj = new Vec3f();
    private final Vec3f etk = new Vec3f();
    private final Vec3f etl = new Vec3f();
    private int count = 0;

    /* renamed from: ah */
    public void mo21521ah(Vec3f vec3f) {
        this.etb.set(vec3f);
        this.count = 1;
    }

    /* renamed from: t */
    public void mo21531t(Vec3f vec3f, Vec3f vec3f2) {
        float f = vec3f.x;
        float f2 = vec3f.y;
        float f3 = vec3f.z;
        float f4 = vec3f2.x;
        float f5 = vec3f2.y;
        float f6 = vec3f2.z;
        this.etb.set(f, f2, f3);
        this.etc.set(f4, f5, f6);
        this.count = 2;
    }

    /* renamed from: l */
    public void mo21528l(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        float f = vec3f.x;
        float f2 = vec3f.y;
        float f3 = vec3f.z;
        float f4 = vec3f2.x;
        float f5 = vec3f2.y;
        float f6 = vec3f2.z;
        float f7 = vec3f3.x;
        float f8 = vec3f3.y;
        float f9 = vec3f3.z;
        this.etb.set(f, f2, f3);
        this.etc.set(f4, f5, f6);
        this.etd.set(f7, f8, f9);
        this.count = 3;
    }

    /* renamed from: g */
    public void mo21525g(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
        float f = vec3f.x;
        float f2 = vec3f.y;
        float f3 = vec3f.z;
        float f4 = vec3f2.x;
        float f5 = vec3f2.y;
        float f6 = vec3f2.z;
        float f7 = vec3f3.x;
        float f8 = vec3f3.y;
        float f9 = vec3f3.z;
        float f10 = vec3f4.x;
        float f11 = vec3f4.y;
        float f12 = vec3f4.z;
        this.etb.set(f, f2, f3);
        this.etc.set(f4, f5, f6);
        this.etd.set(f7, f8, f9);
        this.ete.set(f10, f11, f12);
        this.count = 4;
    }

    public int getSize() {
        return this.count;
    }

    public Vec3f bBi() {
        return this.etf;
    }

    public void reset() {
        this.count = 0;
        this.etf.set(1.0f, 0.0f, 0.0f);
    }

    /* renamed from: oJ */
    public Vec3f mo21529oJ(int i) {
        switch (i) {
            case 0:
                return this.etb;
            case 1:
                return this.etc;
            case 2:
                return this.etd;
            case 3:
                return this.ete;
            default:
                return null;
        }
    }

    /* renamed from: bBj */
    public C3382a iterator() {
        return new C3382a(this);
    }

    /* renamed from: ai */
    public boolean mo21522ai(Vec3f vec3f) {
        switch (this.count) {
            case 0:
                return m37876aj(vec3f);
            case 1:
                return m37877ak(vec3f);
            case 2:
                return m37878al(vec3f);
            case 3:
                return m37879am(vec3f);
            default:
                return false;
        }
    }

    /* renamed from: aj */
    private boolean m37876aj(Vec3f vec3f) {
        mo21521ah(vec3f);
        this.etf.set(-vec3f.x, -vec3f.y, -vec3f.z);
        return true;
    }

    /* renamed from: ak */
    private boolean m37877ak(Vec3f vec3f) {
        Vec3f vec3f2 = this.etb;
        Vec3f c = this.aAq.mo23481c((Tuple3f) vec3f2, (Tuple3f) vec3f);
        this.aAr.set(-vec3f.x, -vec3f.y, -vec3f.z);
        Vec3f vec3f3 = this.aAr;
        if (c.dot(vec3f3) > 0.0f) {
            mo21531t(vec3f, vec3f2);
            m37880m(c, vec3f3, this.etf);
        } else {
            mo21521ah(vec3f);
        }
        return true;
    }

    /* renamed from: al */
    private boolean m37878al(Vec3f vec3f) {
        Vec3f vec3f2 = this.etb;
        Vec3f vec3f3 = this.etc;
        Vec3f c = this.aAq.mo23481c((Tuple3f) vec3f2, (Tuple3f) vec3f);
        Vec3f c2 = this.aAr.mo23481c((Tuple3f) vec3f3, (Tuple3f) vec3f);
        Vec3f a = this.etg.mo23470a(-1.0f, (Tuple3f) vec3f);
        Vec3f b = this.eth.mo23477b((Vector3f) c, (Vector3f) c2);
        if (m37881n(b, c2, a)) {
            if (c2.dot(a) > 0.0f) {
                mo21531t(vec3f, vec3f3);
                m37880m(c2, a, this.etf);
            } else if (c.dot(a) > 0.0f) {
                mo21531t(vec3f, vec3f2);
                m37880m(c, a, this.etf);
            } else {
                mo21521ah(vec3f);
                this.etf.set(a);
            }
            return true;
        }
        if (m37881n(c, b, a)) {
            if (c.dot(a) > 0.0f) {
                mo21531t(vec3f, vec3f2);
                m37880m(c, a, this.etf);
                return true;
            }
            mo21521ah(vec3f);
            this.etf.set(a);
        }
        if (b.dot(a) > 0.0f) {
            mo21528l(vec3f, vec3f2, vec3f3);
            this.etf.set(b);
        } else {
            mo21528l(vec3f, vec3f3, vec3f2);
            this.etf.mo23470a(-1.0f, (Tuple3f) b);
        }
        return true;
    }

    /* renamed from: m */
    private void m37880m(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        vec3f3.mo23477b((Vector3f) vec3f, (Vector3f) vec3f2).mo23489d((Vector3f) vec3f);
        if (vec3f3.lengthSquared() < eta) {
            vec3f3.set(vec3f.y, vec3f.z, vec3f.x);
        }
    }

    /* renamed from: n */
    private boolean m37881n(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        return this.etl.mo23477b((Vector3f) vec3f, (Vector3f) vec3f2).dot(vec3f3) > 0.0f;
    }

    /* renamed from: am */
    private boolean m37879am(Vec3f vec3f) {
        Vec3f vec3f2 = this.etb;
        Vec3f vec3f3 = this.etc;
        Vec3f vec3f4 = this.etd;
        this.aAq.set(vec3f2);
        this.aAq.mo23513o(vec3f);
        Vec3f vec3f5 = this.aAq;
        this.aAr.set(vec3f3);
        this.aAr.mo23513o(vec3f);
        Vec3f vec3f6 = this.aAr;
        this.etg.set(vec3f4);
        this.etg.mo23513o(vec3f);
        Vec3f vec3f7 = this.etg;
        Vec3f b = this.eth.mo23477b((Vector3f) vec3f5, (Vector3f) vec3f6);
        Vec3f b2 = this.eti.mo23477b((Vector3f) vec3f6, (Vector3f) vec3f7);
        Vec3f b3 = this.etj.mo23477b((Vector3f) vec3f7, (Vector3f) vec3f5);
        Vec3f z = this.etk.mo23525z(-vec3f.x, -vec3f.y, -vec3f.z);
        boolean z2 = false;
        if (b.dot(z) > 0.0f) {
            z2 = true;
        }
        if (b2.dot(z) > 0.0f) {
            z2 |= true;
        }
        if (b3.dot(z) > 0.0f) {
            z2 |= true;
        }
        switch (z2) {
            case false:
                mo21525g(vec3f2, vec3f3, vec3f4, vec3f);
                return false;
            case true:
                mo21531t(vec3f2, vec3f3);
                return m37878al(vec3f);
            case true:
                mo21531t(vec3f3, vec3f4);
                return m37878al(vec3f);
            case true:
                if (m37881n(b, vec3f6, z)) {
                    if (m37881n(vec3f6, b2, z)) {
                        if (vec3f6.dot(z) > 0.0f) {
                            mo21531t(vec3f, vec3f3);
                        } else {
                            mo21521ah(vec3f);
                        }
                    } else if (m37881n(b2, vec3f7, z)) {
                        mo21531t(vec3f, vec3f4);
                    } else {
                        mo21528l(vec3f, vec3f3, vec3f4);
                    }
                } else if (!m37881n(vec3f5, b, z)) {
                    mo21528l(vec3f, vec3f2, vec3f3);
                } else if (vec3f5.dot(z) > 0.0f) {
                    mo21531t(vec3f, vec3f2);
                } else {
                    mo21521ah(vec3f);
                }
                return true;
            case true:
                mo21531t(vec3f4, vec3f2);
                return m37878al(vec3f);
            case true:
                if (!m37881n(b3, vec3f5, z)) {
                    if (m37881n(vec3f7, b3, z)) {
                        if (vec3f7.dot(z) <= 0.0f) {
                            mo21521ah(vec3f);
                            break;
                        } else {
                            mo21531t(vec3f, vec3f4);
                            break;
                        }
                    } else {
                        mo21528l(vec3f, vec3f4, vec3f2);
                        break;
                    }
                } else if (!m37881n(vec3f5, b, z)) {
                    if (!m37881n(b, vec3f6, z)) {
                        mo21528l(vec3f, vec3f2, vec3f3);
                        break;
                    } else {
                        mo21531t(vec3f, vec3f3);
                        break;
                    }
                } else if (vec3f5.dot(z) <= 0.0f) {
                    mo21521ah(vec3f);
                    break;
                } else {
                    mo21531t(vec3f, vec3f2);
                    break;
                }
            case true:
                if (!m37881n(b2, vec3f7, z)) {
                    if (m37881n(vec3f6, b2, z)) {
                        if (vec3f6.dot(z) <= 0.0f) {
                            mo21521ah(vec3f);
                            break;
                        } else {
                            mo21531t(vec3f, vec3f3);
                            break;
                        }
                    } else {
                        mo21528l(vec3f, vec3f3, vec3f4);
                        break;
                    }
                } else if (!m37881n(vec3f7, b3, z)) {
                    if (!m37881n(b3, vec3f5, z)) {
                        mo21528l(vec3f, vec3f4, vec3f2);
                        break;
                    } else {
                        mo21531t(vec3f, vec3f2);
                        break;
                    }
                } else if (vec3f7.dot(z) <= 0.0f) {
                    mo21521ah(vec3f);
                    break;
                } else {
                    mo21531t(vec3f, vec3f4);
                    break;
                }
            case true:
                if (vec3f5.dot(z) <= 0.0f) {
                    if (vec3f6.dot(z) <= 0.0f) {
                        if (vec3f7.dot(z) <= 0.0f) {
                            mo21521ah(vec3f);
                            break;
                        } else {
                            mo21531t(vec3f, vec3f4);
                            break;
                        }
                    } else {
                        mo21531t(vec3f, vec3f3);
                        break;
                    }
                } else {
                    mo21531t(vec3f, vec3f2);
                    break;
                }
        }
        return true;
    }

    public String toString() {
        String str = "Simplex: " + getSize();
        if (getSize() <= 0) {
            return str;
        }
        String str2 = String.valueOf(str) + " [" + this.etb;
        if (getSize() > 1) {
            str2 = String.valueOf(str2) + " " + this.etc;
            if (getSize() > 2) {
                str2 = String.valueOf(str2) + " " + this.etd;
                if (getSize() > 3) {
                    str2 = String.valueOf(str2) + " " + this.ete;
                }
            }
        }
        return String.valueOf(str2) + "]";
    }

    /* renamed from: a.qt$a */
    public class C3382a implements Iterator<Vec3f> {
        private C3381qt aVP;

        /* renamed from: i */
        private int f8960i = -1;

        public C3382a(C3381qt qtVar) {
            this.aVP = qtVar;
        }

        public boolean hasNext() {
            return this.f8960i < 4;
        }

        /* renamed from: Xe */
        public Vec3f next() {
            this.f8960i++;
            return this.aVP.mo21529oJ(this.f8960i);
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
