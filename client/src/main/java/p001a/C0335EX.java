package p001a;

import game.script.progression.ProgressionCell;
import game.script.progression.ProgressionCellType;

/* renamed from: a.EX */
/* compiled from: a */
public class C0335EX {
    private final ProgressionCell cUC;
    private final ProgressionCellType cUD;

    public C0335EX(ProgressionCellType kgVar) {
        this.cUD = kgVar;
        this.cUC = null;
    }

    public C0335EX(ProgressionCell jyVar) {
        this.cUC = jyVar;
        this.cUD = jyVar.mo20004FP();
    }

    /* renamed from: FP */
    public ProgressionCellType mo1881FP() {
        return this.cUD;
    }

    public ProgressionCell aPf() {
        return this.cUC;
    }
}
