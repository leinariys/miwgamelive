package p001a;

/* renamed from: a.HV */
/* compiled from: a */
public class C0534HV {
    private long swigCPtr;

    public C0534HV(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C0534HV() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m5202a(C0534HV hv) {
        if (hv == null) {
            return 0;
        }
        return hv.swigCPtr;
    }
}
