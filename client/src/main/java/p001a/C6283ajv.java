package p001a;

/* renamed from: a.ajv  reason: case insensitive filesystem */
/* compiled from: a */
public enum C6283ajv {
    ADMIN(true, true),
    GMMASTER(false, true),
    GM(false, true),
    LEVELDESIGNER(false, true),
    TESTMASTER(true, false),
    TESTER(true, false),
    AUTOMATIC(false, false),
    PLAYER(false, false);

    private boolean fRi;
    private boolean fRj;

    private C6283ajv(boolean z, boolean z2) {
        this.fRi = z;
        this.fRj = z2;
    }

    public boolean cen() {
        return this.fRi;
    }

    public boolean ceo() {
        return this.fRj;
    }
}
