package p001a;

import gnu.trove.TLongObjectHashMap;
import gnu.trove.TObjectProcedure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: a.nc */
/* compiled from: a */
public class C3088nc<T> {
    private final TLongObjectHashMap<List<T>> aGE = new TLongObjectHashMap<>();

    /* renamed from: ct */
    public List<T> mo20813ct(long j) {
        List<T> list = (List) this.aGE.get(j);
        if (list == null) {
            return Collections.EMPTY_LIST;
        }
        return list;
    }

    /* renamed from: a */
    public void mo20811a(long j, T t) {
        List list = (List) this.aGE.get(j);
        if (list == null) {
            list = new ArrayList();
            this.aGE.put(j, list);
        }
        list.add(t);
    }

    /* renamed from: b */
    public void mo20812b(long j, T t) {
        List list = (List) this.aGE.get(j);
        list.remove(t);
        if (list.isEmpty()) {
            this.aGE.remove(j);
        }
    }

    public boolean forEachValue(TObjectProcedure<List<T>> tObjectProcedure) {
        return this.aGE.forEachValue(tObjectProcedure);
    }
}
