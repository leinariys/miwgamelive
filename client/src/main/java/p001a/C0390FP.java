package p001a;

import game.network.WhoAmI;
import game.network.channel.NetworkChannel;
import taikodom.render.loader.provider.FilePath;

/* renamed from: a.FP */
/* compiled from: a */
public interface C0390FP {
    NetworkChannel aPT();

    WhoAmI getWhoAmI();

    Class<?> aPV();

    Object aPW();

    boolean aPX();

    boolean aPY();

    boolean aPZ();

    boolean aQa();

    boolean aQb();

    boolean aQc();

    boolean aQd();

    FilePath aQe();

    boolean aQf();

    C6965axe aQg();

    WraperDbFile aQh();

    C6280ajs ate();

    boolean atf();
}
