package p001a;

import org.objectweb.asm.AnnotationVisitor;

/* renamed from: a.NZ */
/* compiled from: a */
public class C0925NZ implements AnnotationVisitor {
    private AnnotationVisitor dIL;
    private AnnotationVisitor dIM;

    public C0925NZ(AnnotationVisitor annotationVisitor, AnnotationVisitor annotationVisitor2) {
        this.dIL = annotationVisitor;
        this.dIM = annotationVisitor2;
    }

    public void visit(String str, Object obj) {
        this.dIL.visit(str, obj);
        this.dIM.visit(str, obj);
    }

    public AnnotationVisitor visitAnnotation(String str, String str2) {
        return new C0925NZ(this.dIL.visitAnnotation(str, str2), this.dIM.visitAnnotation(str, str2));
    }

    public AnnotationVisitor visitArray(String str) {
        return new C0925NZ(this.dIL.visitArray(str), this.dIM.visitArray(str));
    }

    public void visitEnd() {
        this.dIL.visitEnd();
        this.dIM.visitEnd();
    }

    public void visitEnum(String str, String str2, String str3) {
        this.dIL.visitEnum(str, str2, str3);
        this.dIM.visitEnum(str, str2, str3);
    }
}
