package p001a;

import java.util.Comparator;

/* renamed from: a.aoX  reason: case insensitive filesystem */
/* compiled from: a */
class C6519aoX implements Comparator<C1083Pr> {
    C6519aoX() {
    }

    /* renamed from: a */
    public int compare(C1083Pr pr, C1083Pr pr2) {
        return C2636hr.m32977d(pr) < C2636hr.m32977d(pr2) ? -1 : 1;
    }
}
