package p001a;

import logic.swing.C0454GJ;
import logic.ui.item.C2830kk;

import javax.swing.*;

/* renamed from: a.tc */
/* compiled from: a */
class C3675tc implements C5912aco.C1865a {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ C2762jf.C2764b bkm;

    C3675tc(C2762jf.C2764b bVar) {
        this.bkm = bVar;
    }

    /* renamed from: gZ */
    public void mo4gZ() {
        this.bkm.mo12691a((C5912aco.C1865a) null);
        if (C2762jf.this.mo19982EP() != null) {
            this.bkm.mo12692a(C2762jf.this.ahI, "[255..0] dur 50", C2830kk.asS, new C3676a());
            return;
        }
        C2762jf.this.ahK.setVisible(false);
        this.bkm.stop();
    }

    /* renamed from: a.tc$a */
    class C3676a implements C0454GJ {
        C3676a() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            C2762jf.this.ahK.setVisible(false);
            C3675tc.this.bkm.stop();
        }
    }
}
