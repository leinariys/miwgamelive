package p001a;

import game.script.Actor;
import game.script.corporation.Corporation;
import game.script.item.Item;
import game.script.ship.Outpost;
import game.script.ship.OutpostLevelInfo;
import logic.baa.C6200aiQ;
import logic.baa.aDJ;
import logic.data.link.C3161oY;
import logic.res.code.C5663aRz;
import logic.ui.C2698il;
import logic.ui.Panel;
import logic.ui.item.aUR;
import taikodom.addon.IAddonProperties;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/* renamed from: a.ajQ  reason: case insensitive filesystem */
/* compiled from: a */
public class C6252ajQ extends C5884acM {
    /* access modifiers changed from: private */
    public JList grd = this.djv.mo4915cd("upkeepList");
    /* access modifiers changed from: private */
    public Map<OutpostLevelInfo.OutpostUpkeepItem, Item> gre = new HashMap();
    /* access modifiers changed from: private */
    public C6200aiQ<aDJ> grf = new C1919b();

    public C6252ajQ(IAddonProperties vWVar, C2698il ilVar) {
        super(vWVar, ilVar);
        this.grd.setModel(new DefaultListModel());
        this.grd.setCellRenderer(new C1920c(this, (C1920c) null));
        this.grd.setTransferHandler(new C1921d(this, (C1921d) null));
        this.djv.mo4913cb("upkeepBtn").addActionListener(new C1918a());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo12600a(Outpost qZVar, boolean z) {
        String translate;
        Corporation bYd = this.f4217kj.getPlayer().bYd();
        if (bYd == null || !bYd.mo10707Qy().contains(qZVar)) {
            this.djv.setEnabled(false);
            return;
        }
        this.djv.setEnabled(true);
        if (!bYd.mo10718b(this.f4217kj.getPlayer(), C6704asA.OUTPOST_PAY_UPKEEP)) {
            this.djv.setEnabled(false);
        } else {
            this.djv.setEnabled(true);
        }
        if (qZVar.bqX()) {
            this.djv.setEnabled(false);
            crF();
        } else {
            this.djv.setEnabled(true);
        }
        this.grd.getModel().clear();
        for (OutpostLevelInfo.OutpostUpkeepItem addElement : qZVar.bYt()) {
            this.grd.getModel().addElement(addElement);
        }
        this.djv.mo4917cf("periodCount").setText(C5956adg.format(this.f4217kj.translate("{0} days"), Integer.valueOf(qZVar.bqZ())));
        this.djv.mo4917cf("upkeepCost").setText(String.valueOf(qZVar.bYr()));
        if (!qZVar.isEnabled()) {
            translate = this.f4217kj.translate("Outpost will be lost in:");
        } else if (qZVar.bqX()) {
            translate = this.f4217kj.translate("Next upkeep in:");
        } else {
            translate = this.f4217kj.translate("Upkeep period finishes in:");
        }
        this.djv.mo4917cf("periodMsg").setText(translate);
        this.djv.validate();
    }

    /* access modifiers changed from: package-private */
    public void destroy() {
        crF();
    }

    /* access modifiers changed from: private */
    public void crF() {
        for (Item b : new HashSet(this.gre.values())) {
            b.mo8326b(C3161oY.aQI, (C6200aiQ<?>) this.grf);
        }
        this.gre.clear();
    }

    /* access modifiers changed from: private */
    public void crG() {
        this.djv.validate();
    }

    /* renamed from: a.ajQ$b */
    /* compiled from: a */
    class C1919b implements C6200aiQ<aDJ> {
        C1919b() {
        }

        /* renamed from: a */
        public void mo1143a(aDJ adj, C5663aRz arz, Object obj) {
            for (Map.Entry entry : new HashMap(C6252ajQ.this.gre).entrySet()) {
                if (entry.getValue() == adj) {
                    C6252ajQ.this.gre.remove(entry.getKey());
                    C6252ajQ.this.crG();
                    return;
                }
            }
        }
    }

    /* renamed from: a.ajQ$a */
    class C1918a implements ActionListener {
        C1918a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            Actor bhE = C6252ajQ.this.f4217kj.getPlayer().bhE();
            if (bhE instanceof Outpost) {
                try {
                    ((Outpost) bhE).mo21393d((Item[]) C6252ajQ.this.gre.values().toArray(new Item[0]));
                    C6252ajQ.this.crF();
                } catch (Outpost.C3350c e) {
                    C6252ajQ.this.mo12599a(e);
                }
            }
        }
    }

    /* renamed from: a.ajQ$c */
    /* compiled from: a */
    private class C1920c extends C5517aMj {
        private C1920c() {
        }

        /* synthetic */ C1920c(C6252ajQ ajq, C1920c cVar) {
            this();
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            OutpostLevelInfo.OutpostUpkeepItem aVar = (OutpostLevelInfo.OutpostUpkeepItem) obj;
            Panel dAx = aur.dAx();
            dAx.mo4915cd("pic").setIcon(new C2539gY(aVar.mo463eP()));
            C6252ajQ.this.gre.containsKey(aVar);
            return dAx;
        }
    }

    /* renamed from: a.ajQ$d */
    /* compiled from: a */
    private class C1921d extends TransferHandler {
        private static final long serialVersionUID = -6418112749113351347L;

        private C1921d() {
        }

        /* synthetic */ C1921d(C6252ajQ ajq, C1921d dVar) {
            this();
        }

        public boolean canImport(TransferHandler.TransferSupport transferSupport) {
            return true;
        }

        public boolean importData(TransferHandler.TransferSupport transferSupport) {
            if (!transferSupport.isDrop()) {
                return false;
            }
            if (!transferSupport.isDataFlavorSupported(C2125cH.f6026vP)) {
                return false;
            }
            if (transferSupport.getComponent() != C6252ajQ.this.grd) {
                return false;
            }
            int index = C6252ajQ.this.grd.getDropLocation().getIndex();
            if (index < 0) {
                return false;
            }
            OutpostLevelInfo.OutpostUpkeepItem aVar = (OutpostLevelInfo.OutpostUpkeepItem) C6252ajQ.this.grd.getModel().getElementAt(index);
            try {
                Collection collection = (Collection) transferSupport.getTransferable().getTransferData(C2125cH.f6026vP);
                if (collection.isEmpty() || collection.size() > 1) {
                    return false;
                }
                aDJ adj = (aDJ) collection.iterator().next();
                if (!(adj instanceof Item)) {
                    return false;
                }
                Item auq = (Item) adj;
                if (auq.bAP() != aVar.mo463eP()) {
                    return false;
                }
                if (auq.mo302Iq() != aVar.getAmount()) {
                    return false;
                }
                Item auq2 = (Item) C6252ajQ.this.gre.get(aVar);
                if (auq2 == auq) {
                    return false;
                }
                if (auq2 != null) {
                    auq2.mo8326b(C3161oY.aQI, (C6200aiQ<?>) C6252ajQ.this.grf);
                }
                C6252ajQ.this.gre.put(aVar, auq);
                auq.mo8320a(C3161oY.aQI, (C6200aiQ<?>) C6252ajQ.this.grf);
                return true;
            } catch (UnsupportedFlavorException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }
}
