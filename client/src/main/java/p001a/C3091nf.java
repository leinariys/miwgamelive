package p001a;

import logic.aaa.C5783aaP;
import taikodom.addon.hud.HudShipAimAddon;

/* renamed from: a.nf */
/* compiled from: a */
class C3091nf extends C6124ags<C5783aaP> {
    final /* synthetic */ HudShipAimAddon aGF;

    public C3091nf(HudShipAimAddon hudShipAimAddon) {
        this.aGF = hudShipAimAddon;
    }

    /* renamed from: b */
    public boolean updateInfo(C5783aaP aap) {
        if (aap.bMO() == C5783aaP.C1841a.DOCKED) {
            this.aGF.m43782fO();
            return false;
        } else if (aap.bMO() == C5783aaP.C1841a.JUMPING) {
            this.aGF.m43782fO();
            return false;
        } else {
            this.aGF.m43783fP();
            return false;
        }
    }
}
