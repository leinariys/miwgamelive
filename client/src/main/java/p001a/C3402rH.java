package p001a;

import game.network.manager.C6546aoy;
import taikodom.infra.script.I18NString;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

@C6546aoy
/* renamed from: a.rH */
/* compiled from: a */
public class C3402rH extends C5953add {
    private C3403a bbX;
    private I18NString bbY;

    public C3402rH(C3403a aVar) {
        this.bbX = aVar;
    }

    public C3402rH(C3403a aVar, I18NString i18NString) {
        this.bbX = aVar;
        this.bbY = i18NString;
    }

    public C3402rH() {
    }

    /* renamed from: YJ */
    public C3403a mo21573YJ() {
        return this.bbX;
    }

    /* renamed from: a */
    public void mo21575a(C3403a aVar) {
        this.bbX = aVar;
    }

    /* renamed from: YK */
    public I18NString mo21574YK() {
        return this.bbY;
    }

    private void readObject(ObjectInputStream objectInputStream) {
        this.bbX = (C3403a) objectInputStream.readObject();
        this.bbY = (I18NString) objectInputStream.readObject();
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeObject(this.bbX);
        objectOutputStream.writeObject(this.bbY);
    }

    /* renamed from: a.rH$a */
    public enum C3403a {
        NO_FOUNDS,
        MINIMAL_AMOUNT_NOT_REACHED,
        NOT_ENOUGH_AMOUNT_ON_ORDER,
        AGENT_ON_DIFERENT_STATION,
        NO_BALANCE,
        UNABLE_TO_TRANSFER,
        DIFERENT_TYPE,
        ITEM_BOUNDED,
        NO_STORAGE,
        INVALID_CALL_PLAYER,
        HANGAR_EMPTY,
        INVALID_LOCATION,
        TRANSACTON_ALREADY_CANCELED,
        ITEM_ADDITION,
        EQUIPPED_SHIP
    }
}
