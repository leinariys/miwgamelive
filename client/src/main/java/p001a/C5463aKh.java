package p001a;

import game.script.avatar.cloth.Cloth;

import java.util.Comparator;

/* renamed from: a.aKh  reason: case insensitive filesystem */
/* compiled from: a */
class C5463aKh implements Comparator<Cloth> {
    C5463aKh() {
    }

    /* renamed from: a */
    public int compare(Cloth wj, Cloth wj2) {
        if (wj.getPriority() > wj2.getPriority()) {
            return 1;
        }
        if (wj.getPriority() < wj2.getPriority()) {
            return -1;
        }
        return 0;
    }
}
