package p001a;

import game.network.message.externalizable.C0939Nn;
import game.script.corporation.Corporation;
import game.script.corporation.CorporationRoleInfo;
import game.script.player.Player;
import logic.swing.C5378aHa;
import logic.ui.C2698il;
import taikodom.addon.IAddonProperties;
import taikodom.addon.messagedialog.MessageDialogAddon;

/* renamed from: a.Ov */
/* compiled from: a */
public abstract class C1023Ov {

    /* renamed from: P */
    public Player f1342P;
    public Corporation dLu;

    /* renamed from: kj */
    public IAddonProperties f1343kj;

    public C1023Ov(IAddonProperties vWVar, Player aku, Corporation aPVar) {
        this.f1343kj = vWVar;
        this.f1342P = aku;
        this.dLu = aPVar;
    }

    /* renamed from: Fa */
    public abstract void mo4555Fa();

    public abstract void blJ();

    public abstract void refresh();

    public abstract void removeListeners();

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public final void mo4562c(C2698il ilVar) {
        ilVar.mo4915cd("corpLogo").mo16824a(C5378aHa.CORP_IMAGES, this.dLu.mo10706Qu().aOO());
        ilVar.mo4917cf("corpName").setText(this.dLu.getName());
        ilVar.mo4917cf("playerNameAndRole").setText(String.valueOf(this.f1342P.getName()) + " - " + this.dLu.mo10695I(this.f1342P).getRoleName());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final String mo4556a(Corporation aPVar, C5741aUz auz) {
        CorporationRoleInfo b = aPVar.mo10711b(auz);
        if (b.mo12098sK() == null) {
            return null;
        }
        return b.mo12098sK().getHandle();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void mo4557a(aEP aep, String str) {
        ((MessageDialogAddon) this.f1343kj.mo11830U(MessageDialogAddon.class)).mo24307a(str, aep, this.f1343kj.translate("Cancel"), this.f1343kj.translate("Confirm"));
    }

    /* access modifiers changed from: protected */
    public final void blK() {
        this.f1343kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, this.f1343kj.translate("You don't have permission to perform this action."), new Object[0]));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void mo4558a(aEP aep, String str, String str2) {
        ((MessageDialogAddon) this.f1343kj.mo11830U(MessageDialogAddon.class)).mo24307a(str, aep, this.f1343kj.translate("Cancel"), this.f1343kj.translate("Confirm")).mo4915cd("input").setFormat(str2);
    }

    /* access modifiers changed from: protected */
    public void blL() {
    }
}
