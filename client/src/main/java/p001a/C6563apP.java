package p001a;

import game.script.ship.Ship;

/* renamed from: a.apP  reason: case insensitive filesystem */
/* compiled from: a */
public class C6563apP extends ayA {
    boolean eQv = false;
    long gnG = 0;
    private long gnH = C1285Sv.btL();

    public C6563apP(C1285Sv sv) {
        super(sv);
    }

    /* access modifiers changed from: protected */
    public void aGu() {
        m24886Z(mo5495al());
    }

    /* renamed from: Z */
    private void m24886Z(Ship fAVar) {
        if (System.currentTimeMillis() - this.gnG >= this.gnH) {
            this.gnH = C1285Sv.btL();
            if (this.eQv) {
                log("Activate weapon");
                cDL();
                mo16919a("Fire", C1285Sv.C1287b.Fire);
            } else {
                log("Deactivate weapon");
                bai();
                mo16920b("NotFire", C1285Sv.C1287b.Fire);
            }
            this.eQv = !this.eQv;
            this.gnG = System.currentTimeMillis();
        }
    }

    public void finish() {
        this.eQv = false;
        bai();
        mo16920b("NotFire", C1285Sv.C1287b.Fire);
        super.finish();
    }
}
