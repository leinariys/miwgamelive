package p001a;

import logic.res.XmlNode;

import java.util.HashMap;

/* renamed from: a.Ng */
/* compiled from: a */
public class C0932Ng extends C3772uq {
    public C0932Ng(String str) {
        super(str);
    }

    /* renamed from: a */
    public Object mo1200a(C6146ahO aho, XmlNode agy) {
        HashMap hashMap = new HashMap();
        for (XmlNode next : agy.getListChildrenTag()) {
            if ("item".equals(next.getTagName())) {
                hashMap.put(aho.mo13513f(next.findNodeChildTagDepth("key")), aho.mo13513f(next.findNodeChildTagDepth("value")));
            }
        }
        return hashMap;
    }
}
