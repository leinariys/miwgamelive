package p001a;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.ye */
/* compiled from: a */
public class C4054ye<T> {

    private final BoundingBox dVu;
    /* renamed from: bV */
    List<C4055a<T>> f9591bV;
    BoundingBox[] dVs;
    C4054ye<T>[] dVt;
    private int dVv;
    private int dVw;
    private boolean leaf;
    private int level;

    public C4054ye(BoundingBox boundingBox, int i) {
        this.dVs = new BoundingBox[8];
        this.dVt = new C4054ye[8];
        this.f9591bV = new ArrayList();
        this.leaf = true;
        this.dVv = 15;
        this.dVw = 2;
        this.level = i;
        this.dVu = boundingBox;
        Vec3f dqQ = boundingBox.dqQ();
        Vec3f dqP = boundingBox.dqP();
        Vec3f bny = boundingBox.bny();
        this.dVs[0] = new BoundingBox(dqP, bny);
        this.dVs[1] = new BoundingBox(bny, dqQ);
        this.dVs[2] = new BoundingBox(new Vec3f(dqP.x, dqP.y, bny.z), new Vec3f(bny.x, bny.y, dqQ.z));
        this.dVs[3] = new BoundingBox(new Vec3f(dqP.x, bny.y, dqP.z), new Vec3f(bny.x, dqQ.y, bny.z));
        this.dVs[4] = new BoundingBox(new Vec3f(dqP.x, bny.y, bny.z), new Vec3f(bny.x, dqQ.y, dqQ.z));
        this.dVs[5] = new BoundingBox(new Vec3f(bny.x, dqP.y, dqP.z), new Vec3f(dqQ.x, bny.y, bny.z));
        this.dVs[6] = new BoundingBox(new Vec3f(bny.x, dqP.y, bny.z), new Vec3f(dqQ.x, bny.y, dqQ.z));
        this.dVs[7] = new BoundingBox(new Vec3f(bny.x, bny.y, dqP.z), new Vec3f(dqQ.x, dqQ.y, bny.z));
    }

    public C4054ye(BoundingBox boundingBox) {
        this(boundingBox, 0);
    }

    /* renamed from: b */
    public void mo23157b(BoundingBox boundingBox, T t) {
        int i = 0;
        if (this.dVu.mo23423a(boundingBox)) {
            if (!this.leaf || (this.f9591bV.size() >= this.dVv && this.level != this.dVw)) {
                this.leaf = false;
                while (true) {
                    int i2 = i;
                    if (i2 >= 8) {
                        break;
                    }
                    if (this.dVs[i2].mo23423a(boundingBox)) {
                        if (this.dVt[i2] == null) {
                            this.dVt[i2] = new C4054ye<>(this.dVs[i2]);
                        }
                        this.dVt[i2].mo23157b(boundingBox, t);
                    }
                    for (C4055a next : this.f9591bV) {
                        if (this.dVs[i2].mo23423a(next.bJB)) {
                            if (this.dVt[i2] == null) {
                                this.dVt[i2] = new C4054ye<>(this.dVs[i2], this.level + 1);
                            }
                            this.dVt[i2].mo23157b(next.bJB, next.obj);
                        }
                    }
                    i = i2 + 1;
                }
                if (this.f9591bV.size() == this.dVv || this.f9591bV.size() == 0) {
                    this.f9591bV.clear();
                    return;
                }
                throw new IllegalStateException("Should never clear if its different from 0 or " + this.dVv);
            }
            this.f9591bV.add(new C4055a(boundingBox, t));
        }
    }

    /* renamed from: b */
    public boolean mo23159b(BoundingBox boundingBox) {
        if (!this.dVu.mo23423a(boundingBox)) {
            return false;
        }
        if (this.leaf && this.f9591bV.size() > 0) {
            for (C4055a<T> a : this.f9591bV) {
                if (a.bJB.mo23423a(boundingBox)) {
                    return true;
                }
            }
        }
        for (int i = 0; i < 8; i++) {
            if (this.dVt[i] != null && this.dVt[i].mo23159b(boundingBox)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: a */
    public <QT extends T> void mo23156a(C3026nA<QT> nAVar) {
        if (!nAVar.mo5332a(this.dVu)) {
            return;
        }
        if (this.leaf) {
            int size = this.f9591bV.size();
            while (true) {
                int i = size - 1;
                if (i >= 0) {
                    C4055a aVar = this.f9591bV.get(i);
                    nAVar.mo5334a(aVar.bJB, aVar.obj);
                    size = i;
                } else {
                    return;
                }
            }
        } else {
            for (int i2 = 0; i2 < 8; i2++) {
                if (this.dVt[i2] != null) {
                    this.dVt[i2].mo23156a(nAVar);
                }
            }
        }
    }

    /* renamed from: b */
    public <QT extends T> boolean mo23158b(C3026nA<QT> nAVar) {
        if (!nAVar.mo5332a(this.dVu)) {
            return false;
        }
        if (this.f9591bV.size() > 0) {
            for (C4055a next : this.f9591bV) {
                if (nAVar.mo5334a(next.bJB, next.obj)) {
                    return true;
                }
            }
        }
        for (int i = 0; i < 8; i++) {
            if (this.dVt[i] != null && this.dVt[i].mo23158b(nAVar)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: t */
    public void mo23160t(List<T> list) {
        for (C4055a<T> b : this.f9591bV) {
            list.add(b.obj);
        }
        for (int i = 0; i < 8; i++) {
            if (this.dVt[i] != null) {
                this.dVt[i].mo23160t(list);
            }
        }
    }

    /* renamed from: a.ye$a */
    static class C4055a<Q> {
        /* access modifiers changed from: private */
        public final BoundingBox bJB;
        /* access modifiers changed from: private */
        public final Q obj;

        public C4055a(BoundingBox boundingBox, Q q) {
            this.bJB = boundingBox;
            this.obj = q;
        }
    }
}
