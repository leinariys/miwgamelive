package p001a;

import logic.baa.C0468GU;
import logic.ui.item.ListJ;

import java.text.MessageFormat;
import java.util.*;

/* renamed from: a.aKD */
/* compiled from: a */
public class aKD {
    private Map<C6117agl, ListJ<String>> iiM = new HashMap();

    aKD() {
    }

    public boolean isEmpty() {
        return this.iiM.isEmpty();
    }

    /* renamed from: jJ */
    public int mo9669jJ(boolean z) {
        int i = 0;
        for (Map.Entry next : this.iiM.entrySet()) {
            if (!z || next.getKey() != C6117agl.I18N) {
                i = ((ListJ) next.getValue()).size() + i;
            }
        }
        return i;
    }

    /* renamed from: jK */
    public ListJ<String> mo9670jK(boolean z) {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry next : this.iiM.entrySet()) {
            if (!z || next.getKey() != C6117agl.I18N) {
                arrayList.addAll((Collection) next.getValue());
            }
        }
        return arrayList;
    }

    /* renamed from: a */
    private void m15926a(C6117agl agl, String str) {
        ListJ list = this.iiM.get(agl);
        if (list == null) {
            list = new ArrayList();
            this.iiM.put(agl, list);
        }
        list.add(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public ListJ<String> mo9662a(C6117agl agl) {
        if (this.iiM.containsKey(agl)) {
            return this.iiM.get(agl);
        }
        return Collections.emptyList();
    }

    /* renamed from: j */
    public void mo9668j(C0468GU gu) {
        m15926a(C6117agl.INVALID_HANDLE, "Some content of class " + gu.getClass().getSimpleName() + " has null handle: '" + gu + "'");
    }

    /* renamed from: k */
    public void mo9671k(C0468GU gu) {
        m15926a(C6117agl.INVALID_HANDLE, "Some content of class '" + gu.getClass().getSimpleName() + "' has a default generic handle. Set a specific handle for it ASAP!!!!");
    }

    /* renamed from: l */
    public void mo9672l(C0468GU gu) {
        m15926a(C6117agl.INVALID_HANDLE, "The handle [" + gu.getHandle() + "] for content of class '" + gu.getClass().getSimpleName() + "' is not unique");
    }

    /* renamed from: a */
    public void mo9664a(C0468GU gu, String str) {
        m15926a(C6117agl.I18N, MessageFormat.format("{0} [{1}]: {2}", new Object[]{gu.getClass().getSimpleName(), gu.getHandle(), str}));
    }

    /* renamed from: b */
    public void mo9665b(C0468GU gu, String str) {
        m15926a(C6117agl.INVALID_FIELD_VALUE, MessageFormat.format("{0} [{1}] has {2}", new Object[]{gu.getClass().getSimpleName(), gu.getHandle(), str}));
    }

    /* renamed from: a */
    public void mo9663a(C0468GU gu, C0468GU gu2, String str) {
        m15926a(C6117agl.UNKNOWN_TAIKODOM_CONTENT, MessageFormat.format("{0} [{1}] has a {2} unrelated to Taikodom: [{3}]", new Object[]{gu.getClass().getSimpleName(), gu.getHandle(), str, gu2.getHandle()}));
    }

    /* renamed from: c */
    public void mo9666c(C0468GU gu, String str) {
        m15926a(C6117agl.BONE, MessageFormat.format("{0} [{1}] has no bone {2}", new Object[]{gu.getClass().getSimpleName(), gu.getHandle(), str}));
    }

    /* renamed from: mZ */
    public void mo9674mZ(String str) {
        m15926a(C6117agl.INVALID_FIELD_VALUE, MessageFormat.format("Taikodom Defaults has {0}", new Object[]{str}));
    }

    /* renamed from: m */
    public void mo9673m(C0468GU gu) {
        m15926a(C6117agl.INVALID_FIELD_VALUE, MessageFormat.format("Taikodom Defaults has {0} unknown to Taikodom: [{1}]", new Object[]{gu.getClass().getSigners(), gu.getHandle()}));
    }
}
