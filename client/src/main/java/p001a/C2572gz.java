package p001a;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.gz */
/* compiled from: a */
public final class C2572gz {

    /* renamed from: QN */
    public static final C2572gz f7822QN = new C2572gz("GrannyBinkEncodeAlpha", grannyJNI.GrannyBinkEncodeAlpha_get());

    /* renamed from: QO */
    public static final C2572gz f7823QO = new C2572gz("GrannyBinkUseScaledRGBInsteadOfYUV", grannyJNI.GrannyBinkUseScaledRGBInsteadOfYUV_get());

    /* renamed from: QP */
    public static final C2572gz f7824QP = new C2572gz("GrannyBinkUseBink1", grannyJNI.GrannyBinkUseBink1_get());

    /* renamed from: QQ */
    private static C2572gz[] f7825QQ = {f7822QN, f7823QO, f7824QP};

    /* renamed from: pF */
    private static int f7826pF = 0;

    /* renamed from: pG */
    private final int f7827pG;

    /* renamed from: pH */
    private final String f7828pH;

    private C2572gz(String str) {
        this.f7828pH = str;
        int i = f7826pF;
        f7826pF = i + 1;
        this.f7827pG = i;
    }

    private C2572gz(String str, int i) {
        this.f7828pH = str;
        this.f7827pG = i;
        f7826pF = i + 1;
    }

    private C2572gz(String str, C2572gz gzVar) {
        this.f7828pH = str;
        this.f7827pG = gzVar.f7827pG;
        f7826pF = this.f7827pG + 1;
    }

    /* renamed from: bk */
    public static C2572gz m32466bk(int i) {
        if (i < f7825QQ.length && i >= 0 && f7825QQ[i].f7827pG == i) {
            return f7825QQ[i];
        }
        for (int i2 = 0; i2 < f7825QQ.length; i2++) {
            if (f7825QQ[i2].f7827pG == i) {
                return f7825QQ[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C2572gz.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f7827pG;
    }

    public String toString() {
        return this.f7828pH;
    }
}
