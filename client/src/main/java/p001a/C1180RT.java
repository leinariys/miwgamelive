package p001a;

import game.geometry.Vec3d;
import game.script.map3d.ZoomLevel;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.addon.map3d.Map3DAddon;
import taikodom.render.camera.Camera;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* renamed from: a.RT */
/* compiled from: a */
public class C1180RT {
    public static final float ebQ = 10.0f;
    private Camera camera;
    private Vec3d distance = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, 1.0d);
    private Map3DAddon drA;
    private int ebR;
    private List<ZoomLevel> ebS;

    public C1180RT(Camera camera2, Map3DAddon map3DAddon) {
        this.camera = camera2;
        this.drA = map3DAddon;
        this.ebS = map3DAddon.mo24290JW().ala().aJe().mo19021xz().bsn();
        Collections.sort(this.ebS, new C1181a());
        bsj();
        bso();
    }

    private void bsj() {
        this.ebR = bsn().size() / 2;
    }

    /* renamed from: nf */
    public void mo5211nf(int i) {
        this.ebR = i;
    }

    public boolean bsk() {
        if (this.ebR == bsn().size()) {
            return false;
        }
        this.ebR++;
        bso();
        return true;
    }

    public boolean bsl() {
        if (this.ebR == 1) {
            return false;
        }
        this.ebR--;
        bso();
        return true;
    }

    public ZoomLevel bsm() {
        return this.ebS.get(this.ebR);
    }

    private List<ZoomLevel> bsn() {
        return this.ebS;
    }

    public Camera getCamera() {
        return this.camera;
    }

    private void bso() {
        float f = 1.0f;
        if (bsn().size() > 0) {
            f = bsn().get(this.ebR - 1).getZoom();
        }
        float f2 = f * ebQ;
        Vec3d ajr = new Vec3d();
        ajr.mo9484aA(this.distance);
        ajr.normalize();
        ajr.scale((double) f2);
        System.out.println(new StringBuilder().append(ajr).toString());
        this.camera.setPosition(ajr);
    }

    /* renamed from: a.RT$a */
    class C1181a implements Comparator<ZoomLevel> {
        C1181a() {
        }

        /* renamed from: a */
        public int compare(ZoomLevel dRVar, ZoomLevel dRVar2) {
            int ordinal = dRVar.mo17807kp().ordinal();
            int ordinal2 = dRVar2.mo17807kp().ordinal();
            if (ordinal > ordinal2) {
                return 1;
            }
            if (ordinal > ordinal2) {
                return -1;
            }
            if (dRVar.getZoom() > dRVar.getZoom()) {
                return 1;
            }
            if (dRVar.getZoom() > dRVar.getZoom()) {
                return -1;
            }
            return 0;
        }
    }
}
