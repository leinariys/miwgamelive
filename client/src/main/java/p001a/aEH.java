package p001a;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Vec3d;
import game.geometry.Vector2fWrap;
import game.geometry.aLH;

/* renamed from: a.aEH */
/* compiled from: a */
public class aEH {
    private final C1791a<Vector2fWrap> hHF = new C5836abQ(this);
    private final C1791a<Vec3f> hHG = new C5900acc(this);
    private final C1791a<Matrix4fWrap> hHH = new C5899acb(this);
    private final C1791a<BoundingBox> hHI = new C5845abZ(this);
    private final C1791a<Vec3d> hHJ = new C5843abX(this);
    private final C1791a<aLH> hHK = new C5842abW(this);

    /* renamed from: mp */
    public Vector2fWrap mo8557mp(String str) {
        return this.hHF.mo8563O(str);
    }

    /* renamed from: mq */
    public Vec3f mo8558mq(String str) {
        return this.hHG.mo8563O(str);
    }

    /* renamed from: mr */
    public Matrix4fWrap mo8559mr(String str) {
        return this.hHH.mo8563O(str);
    }

    /* renamed from: ms */
    public BoundingBox mo8560ms(String str) {
        return this.hHI.mo8563O(str);
    }

    /* renamed from: mt */
    public Vec3d mo8561mt(String str) {
        return this.hHJ.mo8563O(str);
    }

    /* renamed from: mu */
    public aLH mo8562mu(String str) {
        return this.hHK.mo8563O(str);
    }

    /* renamed from: a.aEH$a */
    private static abstract class C1791a<T> {
        private C1791a() {
        }

        /* synthetic */ C1791a(C1791a aVar) {
            this();
        }

        /* renamed from: O */
        public abstract T mo8563O(String str);
    }
}
