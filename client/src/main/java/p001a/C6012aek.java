package p001a;

import org.mozilla1.javascript.ScriptRuntime;

/* renamed from: a.aek  reason: case insensitive filesystem */
/* compiled from: a */
public class C6012aek implements org.mozilla1.javascript.Function {
    public org.mozilla1.javascript.Scriptable fnr = null;

    public C6012aek() {
    }

    public C6012aek(org.mozilla1.javascript.Scriptable avf) {
        this.fnr = avf;
    }

    /* access modifiers changed from: protected */
    public C6012aek bUe() {
        try {
            return (C6012aek) getClass().newInstance();
        } catch (Exception e) {
            throw org.mozilla1.javascript.Context.throwAsScriptRuntimeEx(e);
        }
    }

    public org.mozilla1.javascript.Scriptable bUf() {
        return this.fnr;
    }

    /* renamed from: j */
    public void mo13138j(org.mozilla1.javascript.Scriptable avf) {
        this.fnr = avf;
    }

    public String getClassName() {
        return this.fnr.getClassName();
    }

    public Object get(String str, org.mozilla1.javascript.Scriptable avf) {
        return this.fnr.get(str, avf);
    }

    public Object get(int i, org.mozilla1.javascript.Scriptable avf) {
        return this.fnr.get(i, avf);
    }

    public boolean has(String str, org.mozilla1.javascript.Scriptable avf) {
        return this.fnr.has(str, avf);
    }

    public boolean has(int i, org.mozilla1.javascript.Scriptable avf) {
        return this.fnr.has(i, avf);
    }

    public void put(String str, org.mozilla1.javascript.Scriptable avf, Object obj) {
        this.fnr.put(str, avf, obj);
    }

    public void put(int i, org.mozilla1.javascript.Scriptable avf, Object obj) {
        this.fnr.put(i, avf, obj);
    }

    public void delete(String str) {
        this.fnr.delete(str);
    }

    public void delete(int i) {
        this.fnr.delete(i);
    }

    public org.mozilla1.javascript.Scriptable getPrototype() {
        return this.fnr.getPrototype();
    }

    public void setPrototype(org.mozilla1.javascript.Scriptable avf) {
        this.fnr.setPrototype(avf);
    }

    public org.mozilla1.javascript.Scriptable getParentScope() {
        return this.fnr.getParentScope();
    }

    public void setParentScope(org.mozilla1.javascript.Scriptable avf) {
        this.fnr.setParentScope(avf);
    }

    public Object[] getIds() {
        return this.fnr.getIds();
    }

    public Object getDefaultValue(Class<?> cls) {
        if (cls == null || cls == org.mozilla1.javascript.ScriptRuntime.ScriptableClass || cls == org.mozilla1.javascript.ScriptRuntime.FunctionClass) {
            return this;
        }
        return this.fnr.getDefaultValue(cls);
    }

    public boolean hasInstance(org.mozilla1.javascript.Scriptable avf) {
        return this.fnr.hasInstance(avf);
    }

    public Object call(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        return ((org.mozilla1.javascript.Function) this.fnr).call(lhVar, avf, avf2, objArr);
    }

    public org.mozilla1.javascript.Scriptable construct(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, Object[] objArr) {
        org.mozilla1.javascript.Scriptable e;
        if (this.fnr != null) {
            return ((org.mozilla1.javascript.Function) this.fnr).construct(lhVar, avf, objArr);
        }
        C6012aek bUe = bUe();
        if (objArr.length == 0) {
            e = new org.mozilla1.javascript.NativeObject();
        } else {
            e = ScriptRuntime.m7572e(lhVar, avf, objArr[0]);
        }
        bUe.mo13138j(e);
        return bUe;
    }
}
