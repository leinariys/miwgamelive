package p001a;

/* renamed from: a.abY  reason: case insensitive filesystem */
/* compiled from: a */
public class C5844abY {
    private long swigCPtr;

    public C5844abY(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5844abY() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m20014a(C5844abY aby) {
        if (aby == null) {
            return 0;
        }
        return aby.swigCPtr;
    }
}
