package p001a;

import taikodom.render.textures.DDSLoader;

import java.io.FilterOutputStream;
import java.io.OutputStream;

/* renamed from: a.Cb */
/* compiled from: a */
public final class C0210Cb extends FilterOutputStream {
    private final byte[] buf;
    private int count;

    public C0210Cb(OutputStream outputStream) {
        this(outputStream, DDSLoader.DDSCAPS2_CUBEMAP_NEGATIVEY);
    }

    public C0210Cb(OutputStream outputStream, int i) {
        super(outputStream);
        if (i <= 0) {
            throw new IllegalArgumentException("Invalid buffer size");
        }
        this.buf = new byte[i];
    }

    public final void write(int i) {
        if (this.count >= this.buf.length) {
            aBj();
        }
        byte[] bArr = this.buf;
        int i2 = this.count;
        this.count = i2 + 1;
        bArr[i2] = (byte) i;
    }

    public final void write(byte[] bArr, int i, int i2) {
        if (i2 >= this.buf.length) {
            aBj();
            this.out.write(bArr, i, i2);
            return;
        }
        if (i2 > this.buf.length - this.count) {
            aBj();
        }
        System.arraycopy(bArr, i, this.buf, this.count, i2);
        this.count += i2;
    }

    public final void flush() {
        aBj();
        this.out.flush();
    }

    private final void aBj() {
        if (this.count > 0) {
            this.out.write(this.buf, 0, this.count);
            this.count = 0;
        }
    }
}
