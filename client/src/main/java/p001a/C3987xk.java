package p001a;

import java.text.DecimalFormat;

/* renamed from: a.xk */
/* compiled from: a */
public class C3987xk {
    private static final String bFJ = "#.##";
    public static final C3987xk bFK = new C3987xk(bFJ);
    private DecimalFormat bvp;
    private String pattern;

    private C3987xk() {
        this.pattern = bFJ;
        this.bvp = new DecimalFormat(bFJ);
    }

    private C3987xk(String str) {
        this.pattern = str;
        this.bvp = new DecimalFormat(str);
    }

    /* renamed from: cy */
    private String m41115cy(String str) {
        if (str.endsWith(String.valueOf(this.bvp.getDecimalFormatSymbols().getDecimalSeparator()) + "0")) {
            return str.substring(0, str.length() - 2);
        }
        return str;
    }

    /* renamed from: dY */
    public String mo22980dY(float f) {
        if ("#".equals(this.pattern)) {
            f = (float) Math.ceil((double) f);
        }
        return m41115cy(this.bvp.format((double) f));
    }

    public String format(long j) {
        return m41115cy(this.bvp.format(j));
    }
}
