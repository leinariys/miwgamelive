package p001a;

import game.geometry.Vector2fWrap;

/* renamed from: a.qp */
/* compiled from: a */
public class C3376qp {
    public final float height;
    public final float width;

    /* renamed from: x */
    public final float f8958x;

    /* renamed from: y */
    public final float f8959y;

    public C3376qp(float f, float f2, float f3, float f4) {
        this.height = f3;
        this.width = f4;
        this.f8958x = f;
        this.f8959y = f2;
    }

    /* renamed from: a */
    public C3377a mo21511a(C3376qp qpVar) {
        C3376qp qpVar2;
        C3376qp qpVar3;
        if (this.width * this.height > qpVar.width * qpVar.height) {
            qpVar2 = qpVar;
            qpVar3 = this;
        } else {
            qpVar2 = this;
            qpVar3 = qpVar;
        }
        boolean g = qpVar3.mo21514g(qpVar2.f8958x, qpVar2.f8959y);
        boolean g2 = qpVar3.mo21514g(qpVar2.f8958x + qpVar2.width, qpVar2.f8959y);
        boolean g3 = qpVar3.mo21514g(qpVar2.f8958x, qpVar2.f8959y + qpVar2.height);
        boolean g4 = qpVar3.mo21514g(qpVar2.f8958x + qpVar2.width, qpVar2.height + qpVar2.f8959y);
        if (!g && !g2 && !g3 && !g4) {
            return C3377a.NO_INTERSECTION;
        }
        if (!g || !g2 || !g3 || !g4) {
            return C3377a.PARTIAL_INTERSECTION;
        }
        return C3377a.TOTAL_INTERSECTION;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public boolean mo21514g(float f, float f2) {
        if (f < this.f8958x || f > this.f8958x + this.width || f2 < this.f8959y || f2 > this.f8959y + this.height) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public C3376qp mo21513b(C3376qp qpVar) {
        float max = Math.max(qpVar.f8958x, this.f8958x);
        float max2 = Math.max(qpVar.f8959y, this.f8959y);
        return new C3376qp(max, max2, Math.min(qpVar.f8958x + qpVar.width, this.f8958x + this.width) - max, Math.min(qpVar.f8959y + qpVar.height, this.f8959y + this.height) - max2);
    }

    /* renamed from: h */
    public boolean mo21515h(float f, float f2) {
        return f >= Math.min(this.f8958x, this.f8958x + this.width) && f2 >= Math.min(this.f8959y, this.f8959y + this.height) && f <= Math.max(this.f8958x, this.f8958x + this.width) && f2 <= Math.max(this.f8959y, this.f8959y + this.height);
    }

    /* renamed from: a */
    public boolean mo21512a(Vector2fWrap aka) {
        return mo21515h(aka.x, aka.y);
    }

    public String toString() {
        return "x: " + this.f8958x + ", y: " + this.f8959y + ", width: " + this.width + ", height: " + this.height;
    }

    /* renamed from: a.qp$a */
    public enum C3377a {
        NO_INTERSECTION,
        TOTAL_INTERSECTION,
        PARTIAL_INTERSECTION
    }
}
