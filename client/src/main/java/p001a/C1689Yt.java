package p001a;

import game.network.message.C1408Ue;
import game.network.message.C3767um;
import game.network.message.C6972axl;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: a.Yt */
/* compiled from: a */
public class C1689Yt implements ReadWriteLock {
    /* access modifiers changed from: private */
    public C1408Ue iJl;
    ReentrantLock dga = new ReentrantLock(true);
    Lock iJj = new C1690a(this, (C1690a) null);
    Lock iJk = new C1691b(this, (C1691b) null);
    Condition iJm = this.dga.newCondition();
    Condition iJn = this.dga.newCondition();
    Condition iJo = this.dga.newCondition();
    boolean iJp = false;
    boolean iJq = false;
    int iJr = 0;
    boolean iJs = false;
    Condition iJt = this.dga.newCondition();
    Condition iJu = this.dga.newCondition();
    boolean iJv = false;
    boolean iJw = false;
    boolean iJx = false;
    boolean iJy = false;

    public C1689Yt(C3767um umVar) {
        this.iJl = umVar.mo279a(new C1692c());
    }

    public Lock readLock() {
        return this.iJj;
    }

    public Lock writeLock() {
        return this.iJk;
    }

    public boolean dtg() {
        return this.iJq;
    }

    public boolean dth() {
        return this.iJw;
    }

    public int dti() {
        return this.iJr;
    }

    public boolean dtj() {
        return this.iJx;
    }

    /* renamed from: a.Yt$c */
    /* compiled from: a */
    class C1692c implements C6972axl {
        C1692c() {
        }

        public void cbt() {
            C1689Yt.this.dga.lock();
            try {
                C1689Yt.this.iJp = false;
                C1689Yt.this.iJq = true;
                C1689Yt.this.iJm.signalAll();
            } finally {
                C1689Yt.this.dga.unlock();
            }
        }

        public void cbw() {
            C1689Yt.this.dga.lock();
            try {
                C1689Yt.this.iJv = false;
                C1689Yt.this.iJw = true;
                C1689Yt.this.iJt.signal();
            } finally {
                C1689Yt.this.dga.unlock();
            }
        }

        public void cbu() {
            C1689Yt.this.dga.lock();
            try {
                C1689Yt.this.iJs = true;
                while (C1689Yt.this.iJr > 0) {
                    C1689Yt.this.iJn.await();
                }
                C1689Yt.this.iJq = false;
                C1689Yt.this.iJs = false;
                C1689Yt.this.iJl.bxm();
                C1689Yt.this.iJo.signalAll();
                C1689Yt.this.dga.unlock();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } catch (Throwable th) {
                C1689Yt.this.dga.unlock();
                throw th;
            }
        }

        public void cbv() {
            C1689Yt.this.dga.lock();
            try {
                if (C1689Yt.this.iJx) {
                    C1689Yt.this.iJy = true;
                    C1689Yt.this.iJt.await();
                    C1689Yt.this.iJy = false;
                }
                C1689Yt.this.iJw = false;
                C1689Yt.this.iJl.bxn();
                C1689Yt.this.iJu.signalAll();
                C1689Yt.this.dga.unlock();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } catch (Throwable th) {
                C1689Yt.this.dga.unlock();
                throw th;
            }
        }
    }

    /* renamed from: a.Yt$a */
    private final class C1690a implements Lock {
        private C1690a() {
        }

        /* synthetic */ C1690a(C1689Yt yt, C1690a aVar) {
            this();
        }

        public void lock() {
            C1689Yt.this.dga.lock();
            while (true) {
                try {
                    if (!C1689Yt.this.iJq) {
                        if (!C1689Yt.this.iJp) {
                            C1689Yt.this.iJp = true;
                            C1689Yt.this.iJl.bxk();
                        }
                        C1689Yt.this.iJm.await();
                    }
                    C1689Yt.this.iJr++;
                    if (C1689Yt.this.iJs) {
                        C1689Yt.this.iJo.await();
                    } else {
                        C1689Yt.this.dga.unlock();
                        return;
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (Throwable th) {
                    C1689Yt.this.dga.unlock();
                    throw th;
                }
            }
        }

        public void unlock() {
            C1689Yt.this.dga.lock();
            try {
                C1689Yt yt = C1689Yt.this;
                yt.iJr--;
                C1689Yt.this.iJn.signal();
            } finally {
                C1689Yt.this.dga.unlock();
            }
        }

        public void lockInterruptibly() {
            throw new UnsupportedOperationException();
        }

        public Condition newCondition() {
            throw new UnsupportedOperationException();
        }

        /* JADX INFO: finally extract failed */
        public boolean tryLock() {
            C1689Yt.this.dga.lock();
            try {
                if (!C1689Yt.this.iJq) {
                    C1689Yt.this.dga.unlock();
                    return false;
                }
                lock();
                C1689Yt.this.dga.unlock();
                return true;
            } catch (Throwable th) {
                C1689Yt.this.dga.unlock();
                throw th;
            }
        }

        public boolean tryLock(long j, TimeUnit timeUnit) {
            throw new UnsupportedOperationException();
        }
    }

    /* renamed from: a.Yt$b */
    /* compiled from: a */
    private final class C1691b implements Lock {
        private C1691b() {
        }

        /* synthetic */ C1691b(C1689Yt yt, C1691b bVar) {
            this();
        }

        public void lock() {
            C1689Yt.this.dga.lock();
            while (true) {
                try {
                    if (!C1689Yt.this.iJw) {
                        if (!C1689Yt.this.iJv) {
                            C1689Yt.this.iJv = true;
                            C1689Yt.this.iJl.bxl();
                        }
                        C1689Yt.this.iJt.await();
                    }
                    if (!C1689Yt.this.iJy) {
                        break;
                    }
                    C1689Yt.this.iJu.await();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (Throwable th) {
                    C1689Yt.this.dga.unlock();
                    throw th;
                }
            }
            if (C1689Yt.this.iJx) {
                C1689Yt.this.iJt.await();
            }
            C1689Yt.this.iJx = true;
            C1689Yt.this.dga.unlock();
        }

        public void unlock() {
            C1689Yt.this.dga.lock();
            try {
                C1689Yt.this.iJx = false;
                C1689Yt.this.iJt.signal();
            } finally {
                C1689Yt.this.dga.unlock();
            }
        }

        public void lockInterruptibly() {
            throw new UnsupportedOperationException();
        }

        public Condition newCondition() {
            throw new UnsupportedOperationException();
        }

        /* JADX INFO: finally extract failed */
        public boolean tryLock() {
            C1689Yt.this.dga.lock();
            try {
                if (!C1689Yt.this.iJw || C1689Yt.this.iJx) {
                    C1689Yt.this.dga.unlock();
                    return false;
                }
                lock();
                C1689Yt.this.dga.unlock();
                return true;
            } catch (Throwable th) {
                C1689Yt.this.dga.unlock();
                throw th;
            }
        }

        public boolean tryLock(long j, TimeUnit timeUnit) {
            throw new UnsupportedOperationException();
        }
    }
}
