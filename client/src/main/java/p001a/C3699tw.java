package p001a;

import logic.res.ConfigManagerSection;
import sdljava.SDLException;
import sdljava.SDLMain;
import sdljava.event.*;
import sdljava.joystick.SDLJoystick;
import sdljava.x.swig.SDLPressedState;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.tw */
/* compiled from: a */
public class C3699tw implements C0879Mk {
    public float bmQ;
    public float bmR;
    private C2976mV bmS;
    private int bmT;
    private int bmU;

    /* renamed from: a */
    public void mo4056a(ConfigManagerSection yr, float f, float f2, C2976mV mVVar) {
        this.bmQ = f;
        this.bmR = f2;
        this.bmS = mVVar;
        try {
            SDLMain.initSubSystem(512);
            SDLEvent.enableUNICODE(1);
        } catch (SDLException e) {
            throw new IOException("Unable to start joystick subsystem");
        }
    }

    public Map<Integer, String> adL() {
        HashMap hashMap = new HashMap();
        int numJoysticks = SDLJoystick.numJoysticks();
        for (int i = 0; i < numJoysticks; i++) {
            hashMap.put(Integer.valueOf(i), SDLJoystick.joystickName(i));
        }
        return hashMap;
    }

    /* renamed from: eG */
    public C6641aqp mo4060eG(int i) {
        if (SDLJoystick.numJoysticks() < i) {
            return null;
        }
        try {
            return new C6641aqp(SDLJoystick.joystickOpen(i));
        } catch (SDLException e) {
            return null;
        }
    }

    public boolean step(float f) {
        boolean z;
        boolean z2;
        boolean z3;
        while (true) {
            try {
                SDLKeyboardEvent pollEvent = SDLEvent.pollEvent();
                if (pollEvent == null) {
                    return false;
                }
                switch (pollEvent.getType()) {
                    case 2:
                    case 3:
                        SDLKeyboardEvent sDLKeyboardEvent = pollEvent;
                        C2976mV mVVar = this.bmS;
                        int nA = C1411Ug.m10326nA(sDLKeyboardEvent.getSym());
                        if (sDLKeyboardEvent.getState() == SDLPressedState.PRESSED) {
                            z3 = true;
                        } else {
                            z3 = false;
                        }
                        mVVar.mo8719a(new C2989mg(nA, z3, sDLKeyboardEvent.getUnicode(), C1411Ug.m10325a(sDLKeyboardEvent.getMod()), true));
                        break;
                    case 4:
                        SDLMouseMotionEvent sDLMouseMotionEvent = (SDLMouseMotionEvent) pollEvent;
                        this.bmT = sDLMouseMotionEvent.getX();
                        this.bmU = sDLMouseMotionEvent.getY();
                        this.bmS.mo8719a(new aIU(((float) sDLMouseMotionEvent.getX()) / this.bmQ, ((float) sDLMouseMotionEvent.getY()) / this.bmR, ((float) sDLMouseMotionEvent.getXrel()) / this.bmQ, ((float) sDLMouseMotionEvent.getYrel()) / this.bmR, sDLMouseMotionEvent.getX(), sDLMouseMotionEvent.getY(), sDLMouseMotionEvent.getXrel(), sDLMouseMotionEvent.getYrel(), C1411Ug.m10325a(SDLMouseMotionEvent.getModState())));
                        break;
                    case 5:
                    case 6:
                        SDLMouseButtonEvent sDLMouseButtonEvent = pollEvent;
                        C2976mV mVVar2 = this.bmS;
                        int eC = C3545sQ.m38734eC(sDLMouseButtonEvent.getButton());
                        if (sDLMouseButtonEvent.getState() == SDLPressedState.PRESSED) {
                            z2 = true;
                        } else {
                            z2 = false;
                        }
                        mVVar2.mo8719a(new C3010mu(eC, z2, ((float) sDLMouseButtonEvent.getX()) / this.bmQ, ((float) sDLMouseButtonEvent.getY()) / this.bmR, sDLMouseButtonEvent.getX(), sDLMouseButtonEvent.getY(), C1411Ug.m10325a(SDLMouseButtonEvent.getModState())));
                        break;
                    case 7:
                        SDLJoyAxisEvent sDLJoyAxisEvent = (SDLJoyAxisEvent) pollEvent;
                        this.bmS.mo8719a(new C6009aeh(sDLJoyAxisEvent.getAxis(), (((float) sDLJoyAxisEvent.getValue()) + 0.5f) / 32767.5f));
                        break;
                    case 9:
                        SDLJoyHatEvent sDLJoyHatEvent = (SDLJoyHatEvent) pollEvent;
                        this.bmS.mo8719a(new C0366Et(sDLJoyHatEvent.getSwigEvent().getValue(), sDLJoyHatEvent.getSwigEvent().getValue() != 0));
                        break;
                    case 10:
                    case 11:
                        SDLJoyButtonEvent sDLJoyButtonEvent = (SDLJoyButtonEvent) pollEvent;
                        C2976mV mVVar3 = this.bmS;
                        int button = sDLJoyButtonEvent.getButton();
                        if (sDLJoyButtonEvent.getState() == SDLPressedState.PRESSED) {
                            z = true;
                        } else {
                            z = false;
                        }
                        mVVar3.mo8719a(new C0963OA(button, z));
                        break;
                    case 12:
                        return true;
                }
            } catch (SDLException e) {
                e.printStackTrace();
                return true;
            }
        }
    }

    public int adM() {
        return this.bmT;
    }

    public int adN() {
        return this.bmU;
    }
}
