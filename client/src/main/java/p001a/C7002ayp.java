package p001a;

import game.script.player.Player;
import logic.C3642tH;

/* renamed from: a.ayp  reason: case insensitive filesystem */
/* compiled from: a */
class C7002ayp extends ayA {
    private final long gTo = 300000;
    private final int gTp = 10000;
    private final int gTq = 20;
    private final String gTr = "012345678901234567890123456789012345678901234567890123456789";
    private int gTs = 0;
    private int gTt = 0;

    public C7002ayp(C1285Sv sv) {
        super(sv);
        restart();
    }

    private long cDv() {
        return 300000 + ((long) ((int) (300000.0d * Math.random())));
    }

    private long cDw() {
        return (long) (((int) (10000.0d * Math.random())) + 10000);
    }

    /* access modifiers changed from: protected */
    public void aGu() {
        switch (this.state) {
            case 0:
                mo16920b("Wait", C1285Sv.C1287b.Chat);
                mo16928jh(cDv());
                this.gTt = 20;
                this.state++;
                return;
            case 1:
                mo16919a("SentGlob", C1285Sv.C1287b.Chat);
                Player dL = mo5503dL();
                int i = this.gTs;
                this.gTs = i + 1;
                dL.mo14435nF(String.valueOf(i) + ": " + "012345678901234567890123456789012345678901234567890123456789");
                this.state++;
                return;
            case 2:
                mo16928jh(cDw());
                mo16919a("SentP" + this.gTt, C1285Sv.C1287b.Chat);
                Player dL2 = mo5503dL();
                String str = C3642tH.hak[(int) (Math.random() * ((double) C3642tH.hak.length))];
                int i2 = this.gTs;
                this.gTs = i2 + 1;
                dL2.mo14349ax(str, String.valueOf(i2) + ": " + "012345678901234567890123456789012345678901234567890123456789");
                this.gTt--;
                if (this.gTt <= 0) {
                    this.state = 0;
                    return;
                }
                return;
            default:
                return;
        }
    }
}
