package p001a;

import game.script.damage.DamageType;
import game.script.ship.Hull;
import game.script.ship.Shield;
import game.script.ship.Ship;
import game.script.ship.ShipStructure;
import logic.aaa.C1654YM;
import logic.ui.C2698il;
import logic.ui.item.Progress;
import taikodom.addon.IAddonProperties;

import javax.swing.*;
import java.util.Iterator;
import java.util.Map;

/* renamed from: a.Nx */
/* compiled from: a */
public class C0956Nx extends C7023azk {
    /* access modifiers changed from: private */

    /* renamed from: IU */
    public C3987xk f1277IU = C3987xk.bFK;
    private JLabel bvo;
    private JLabel eYg;
    private JLabel gSN;
    private JLabel gSO;
    private JLabel gSP;
    private C3428rT<C1654YM> gSQ;

    public C0956Nx(IAddonProperties vWVar, C2698il ilVar) {
        super(vWVar, ilVar);
        cDs();
        cDt();
        initComponents();
        m7792iG();
        m7788Fa();
    }

    /* renamed from: Fa */
    private void m7788Fa() {
        bUA().aVU().mo13965a(C1654YM.class, this.gSQ);
    }

    public void removeListeners() {
        bUA().aVU().mo13968b(C1654YM.class, this.gSQ);
    }

    /* renamed from: iG */
    private void m7792iG() {
        this.gSQ = new C0959c();
    }

    private void initComponents() {
        this.bvo = cFB().mo4917cf("velocity");
        this.gSN = cFB().mo4917cf("acceleration");
        this.eYg = cFB().mo4917cf("maneuver");
        this.gSO = cFB().mo4917cf("regeneration");
        this.gSP = cFB().mo4917cf("cruise");
    }

    /* renamed from: M */
    public void mo4307M(Ship fAVar) {
        super.mo4307M(fAVar);
        refresh();
    }

    /* access modifiers changed from: private */
    public void refresh() {
        if (cFC() != null) {
            m7789a(cFC().agH().mo4217ra(), cFC().mo1091ra(), this.bvo);
            m7789a(cFC().agH().mo4218rb(), cFC().mo1092rb(), this.gSN);
            m7789a(cFC().agH().mo4153VH(), cFC().mo963VH(), this.eYg);
            m7789a(cFC().mo8288zv().mo19075Tp(), cFC().mo8288zv().mo19090ia(), this.gSO);
            m7789a(cFC().agH().bkd().mo4142ra(), cFC().afL().mo18835ra(), this.gSP);
            cDr();
        }
    }

    private void cDr() {
        float f;
        float f2;
        Iterator<Map.Entry<DamageType, C1649YI>> it = cFC().agH().mo4155VT().ana().entrySet().iterator();
        while (true) {
            if (it.hasNext()) {
                Map.Entry next = it.next();
                if (((DamageType) next.getKey()).getHandle().equals("dmg_non_hull")) {
                    float value = ((C1649YI) next.getValue()).getValue();
                    f = cFC().mo8287zt().mo19927b((DamageType) next.getKey());
                    f2 = value;
                    break;
                }
            } else {
                f = 0.0f;
                f2 = 0.0f;
                break;
            }
        }
        m7789a(f2, f, cFB().mo4917cf("armor"));
    }

    /* renamed from: a */
    private void m7789a(float f, float f2, JLabel jLabel) {
        String dY = this.f1277IU.mo22980dY(f);
        if (f2 > f) {
            dY = String.valueOf(dY) + " + " + this.f1277IU.mo22980dY(f2 - f);
        }
        jLabel.setText(dY);
    }

    private void cDs() {
        Progress cg = cFB().mo4918cg("shieldProgress");
        cg.setToolTipText(bUA().translate("Shield"));
        cg.mo17403a(new C0958b());
    }

    private void cDt() {
        Progress cg = cFB().mo4918cg("hullProgress");
        cg.setToolTipText(bUA().translate("Hull"));
        cg.mo17403a(new C0957a());
    }

    /* renamed from: a.Nx$c */
    /* compiled from: a */
    class C0959c extends C3428rT<C1654YM> {
        C0959c() {
        }

        /* renamed from: a */
        public void mo321b(C1654YM ym) {
            C0956Nx.this.refresh();
        }
    }

    /* renamed from: a.Nx$b */
    /* compiled from: a */
    class C0958b implements Progress.C2065a {
        C0958b() {
        }

        public float getMaximum() {
            if (C0956Nx.this.cFC() == null) {
                return 0.0f;
            }
            return C0956Nx.this.cFC().mo8288zv().mo19089hh();
        }

        public float getMinimum() {
            return 0.0f;
        }

        public String getText() {
            if (C0956Nx.this.cFC() == null) {
                return null;
            }
            Shield zv = ((ShipStructure) C0956Nx.this.cFC().agt().get(0)).mo13246zv();
            float Tx = zv.mo19079Tx();
            float hh = zv.mo19089hh();
            float max = Math.max(Tx, 0.0f);
            if (hh < 0.0f) {
                hh = max;
            }
            return String.valueOf(C0956Nx.this.f1277IU.mo22980dY(max)) + " / " + C0956Nx.this.f1277IU.mo22980dY(hh);
        }

        public float getValue() {
            if (C0956Nx.this.cFC() == null) {
                return 0.0f;
            }
            return C0956Nx.this.cFC().mo8288zv().mo19079Tx();
        }
    }

    /* renamed from: a.Nx$a */
    class C0957a implements Progress.C2065a {
        C0957a() {
        }

        public float getMaximum() {
            if (C0956Nx.this.cFC() == null) {
                return 0.0f;
            }
            return C0956Nx.this.cFC().mo8287zt().mo19934hh();
        }

        public float getMinimum() {
            return 0.0f;
        }

        public String getText() {
            if (C0956Nx.this.cFC() == null) {
                return null;
            }
            Hull zt = C0956Nx.this.cFC().mo8287zt();
            float Tx = zt.mo19923Tx();
            float hh = zt.mo19934hh();
            float max = Math.max(Tx, 0.0f);
            if (hh < 0.0f) {
                hh = max;
            }
            return String.valueOf(C0956Nx.this.f1277IU.mo22980dY(max)) + " / " + C0956Nx.this.f1277IU.mo22980dY(hh);
        }

        public float getValue() {
            if (C0956Nx.this.cFC() == null) {
                return 0.0f;
            }
            return C0956Nx.this.cFC().mo8287zt().mo19923Tx();
        }
    }
}
