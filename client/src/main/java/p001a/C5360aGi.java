package p001a;

import com.hoplon.geometry.Vec3f;
import logic.bbb.C3165ob;

/* renamed from: a.aGi  reason: case insensitive filesystem */
/* compiled from: a */
public class C5360aGi implements C6752asw {
    /* renamed from: a */
    public boolean mo9106a(C6331akr akr, C3165ob obVar, C3165ob obVar2, C3978xf xfVar, C3978xf xfVar2, Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        C1568X.C1572b bVar = new C1568X.C1572b();
        if (!C1568X.m11405a(obVar, xfVar, obVar2, xfVar2, bVar)) {
            return false;
        }
        vec3f2.set(bVar.f2088Oi[0]);
        vec3f3.set(bVar.f2088Oi[1]);
        return true;
    }
}
