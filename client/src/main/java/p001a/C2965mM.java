package p001a;

import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.win32.StdCallLibrary;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.event.WindowListener;

/* renamed from: a.mM */
/* compiled from: a */
public class C2965mM extends Frame implements WindowFocusListener, WindowListener {
    private static final long serialVersionUID = 7681330913013644565L;
    private boolean iyL = false;
    private Component iyM = this;
    private C2966a.C2967a iyN = dnq();
    private Rectangle iyO;

    public C2965mM(String str) {
        super(str);
        addWindowFocusListener(this);
        addWindowListener(this);
    }

    private C2966a.C2967a dnq() {
        C2966a.C2967a aVar = new C2966a.C2967a();
        C2966a.aFG.mo20526b(aVar);
        return aVar;
    }

    /* renamed from: jT */
    public void mo20512jT(boolean z) {
        this.iyL = z;
        dnr();
    }

    /* renamed from: e */
    public void mo20511e(int i, int i2, int i3, int i4) {
        this.iyO = new Rectangle(i, i2, i3, i4);
        dnr();
    }

    /* renamed from: r */
    public void mo20513r(Component component) {
        this.iyM = component;
        dnr();
    }

    private void dnr() {
        if (this.iyL) {
            dnt();
        } else {
            dns();
        }
    }

    private void dns() {
        m35678c(this.iyN);
    }

    private void dnt() {
        Rectangle bounds;
        if (this.iyO != null) {
            bounds = this.iyO;
        } else {
            bounds = this.iyM.getBounds();
        }
        m35679f(bounds.x, bounds.y, bounds.width, bounds.height);
    }

    /* renamed from: f */
    private void m35679f(int i, int i2, int i3, int i4) {
        C2966a.C2967a aVar = new C2966a.C2967a();
        aVar.hQs = new NativeLong((long) i2);
        aVar.hQr = new NativeLong((long) i);
        aVar.hQu = new NativeLong((long) (i2 + i4));
        aVar.hQt = new NativeLong((long) (i + i3));
        m35678c(aVar);
    }

    /* renamed from: c */
    private void m35678c(C2966a.C2967a aVar) {
        C2966a.aFG.mo20525a(aVar);
    }

    public void windowActivated(WindowEvent windowEvent) {
        System.out.println("windowActivated");
        dns();
    }

    public void windowClosed(WindowEvent windowEvent) {
        System.out.println("windowClosed");
        dns();
    }

    public void windowClosing(WindowEvent windowEvent) {
        System.out.println("windowClosing");
        dns();
    }

    public void windowDeactivated(WindowEvent windowEvent) {
        System.out.println("windowDeactivated");
        dns();
    }

    public void windowDeiconified(WindowEvent windowEvent) {
        System.out.println("windowDeiconified");
        dnr();
    }

    public void windowIconified(WindowEvent windowEvent) {
        System.out.println("windowIconified");
        dns();
    }

    public void windowOpened(WindowEvent windowEvent) {
        System.out.println("windowOpened");
        dnr();
    }

    public void windowGainedFocus(WindowEvent windowEvent) {
        System.out.println("windowGainedFocus");
        dnr();
    }

    public void windowLostFocus(WindowEvent windowEvent) {
        System.out.println("windowLostFocus");
        dns();
    }

    /* renamed from: a.mM$a */
    public interface C2966a extends StdCallLibrary {
        public static final C2966a aFG = ((C2966a) Native.loadLibrary("user32", C2966a.class));

        /* renamed from: Pg */
        Pointer mo20523Pg();

        /* renamed from: a */
        Pointer mo20524a(Pointer pointer);

        /* renamed from: a */
        boolean mo20525a(C2967a aVar);

        /* renamed from: b */
        boolean mo20526b(C2967a aVar);

        /* renamed from: a.mM$a$a */
        public static class C2967a extends Structure {
            public NativeLong hQr;
            public NativeLong hQs;
            public NativeLong hQt;
            public NativeLong hQu;
        }
    }
}
