package p001a;

import logic.abc.C0802Lc;

/* renamed from: a.Pr */
/* compiled from: a */
public abstract class C1083Pr {
    private static final C6238ajC dQi = new C6238ajC(0.0f, (C6863avD) null, (C0802Lc) null);
    public final C0763Kt stack;
    public float dMa;
    public C6238ajC dQm;
    public C6238ajC dQn;
    private int dQj;
    private int dQk;
    private aVP dQl;

    public C1083Pr(aVP avp) {
        this(avp, dQi, dQi);
    }

    public C1083Pr(aVP avp, C6238ajC ajc) {
        this(avp, ajc, dQi);
    }

    public C1083Pr(aVP avp, C6238ajC ajc, C6238ajC ajc2) {
        this.stack = C0763Kt.bcE();
        this.dQj = -1;
        this.dQk = -1;
        this.dMa = 0.0f;
        this.dQl = avp;
        this.dQm = ajc;
        this.dQn = ajc2;
        dQi.mo13909b(0.0f, C0128Bb.dMU);
    }

    /* renamed from: PU */
    public abstract void mo4840PU();

    /* renamed from: cv */
    public abstract void mo4847cv(float f);

    public C6238ajC bnn() {
        return this.dQm;
    }

    public C6238ajC bno() {
        return this.dQn;
    }

    public int bnp() {
        return this.dQj;
    }

    /* renamed from: mB */
    public void mo4848mB(int i) {
        this.dQj = i;
    }

    public int bnq() {
        return this.dQk;
    }

    /* renamed from: mC */
    public void mo4849mC(int i) {
        this.dQk = i;
    }

    public float bnr() {
        return this.dMa;
    }

    public aVP bns() {
        return this.dQl;
    }
}
