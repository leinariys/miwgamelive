package p001a;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.wO */
/* compiled from: a */
public class C3892wO implements Externalizable, Cloneable {
    private C3893a gkS;
    private float value;

    public C3892wO() {
        this.value = 0.0f;
        this.gkS = C3893a.ABSOLUTE;
    }

    public C3892wO(float f, C3893a aVar) {
        this.gkS = aVar;
        this.value = f;
    }

    /* renamed from: v */
    public float mo22753v(float f, float f2) {
        if (this.gkS == C3893a.PERCENT) {
            float f3 = ((this.value * f) / 100.0f) + f2;
            if (f3 < 1.0E-5f) {
                return 0.0f;
            }
            return f3;
        }
        float f4 = this.value + f2;
        if (f4 > 0.0f) {
            return f4;
        }
        return 0.0f;
    }

    public float getValue() {
        return this.value;
    }

    public C3893a coZ() {
        return this.gkS;
    }

    public String toString() {
        if (this.gkS == C3893a.PERCENT) {
            return String.valueOf(this.value) + "%";
        }
        return Float.toString(this.value);
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeFloat(this.value);
        objectOutput.writeByte(this.gkS.ordinal());
    }

    public void readExternal(ObjectInput objectInput) {
        this.value = objectInput.readFloat();
        this.gkS = C3893a.values()[objectInput.read()];
    }

    /* renamed from: ag */
    public void mo22744ag(C3892wO wOVar) {
        if (wOVar.gkS == this.gkS) {
            this.value += wOVar.value;
            return;
        }
        throw new IllegalArgumentException("Trying to add a buff of type " + wOVar.gkS + " and " + this + " has buff of type " + this.gkS);
    }

    /* renamed from: ah */
    public C3892wO mo22745ah(C3892wO wOVar) {
        C3892wO wOVar2 = new C3892wO(wOVar.getValue(), wOVar.coZ());
        if (wOVar.gkS == this.gkS) {
            wOVar2.mo22744ag(this);
            return wOVar2;
        }
        throw new IllegalArgumentException("Trying to add a buff of type " + wOVar.gkS + " and " + this + " has buff of type " + this.gkS);
    }

    public Object clone() {
        return new C3892wO(this.value, this.gkS);
    }

    public C3892wO cpa() {
        return new C3892wO(this.value, this.gkS);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C3892wO)) {
            return super.equals(obj);
        }
        C3892wO wOVar = (C3892wO) obj;
        return this.gkS == wOVar.gkS && this.value == wOVar.value;
    }

    /* renamed from: a.wO$a */
    public enum C3893a {
        PERCENT,
        ABSOLUTE
    }
}
