package p001a;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.tP */
/* compiled from: a */
public final class C3660tP {
    public static final C3660tP btA = new C3660tP("GrannyBoundScaleShearCurveIsGeneral", grannyJNI.GrannyBoundScaleShearCurveIsGeneral_get());
    public static final C3660tP btB = new C3660tP("GrannyBoundScaleShearCurveFlagMask", grannyJNI.GrannyBoundScaleShearCurveFlagMask_get());
    public static final C3660tP btn = new C3660tP("GrannyBoundPositionCurveIsIdentity", grannyJNI.GrannyBoundPositionCurveIsIdentity_get());
    public static final C3660tP bto = new C3660tP("GrannyBoundPositionCurveIsConstant", grannyJNI.GrannyBoundPositionCurveIsConstant_get());
    public static final C3660tP btp = new C3660tP("GrannyBoundPositionCurveIsKeyframed", grannyJNI.GrannyBoundPositionCurveIsKeyframed_get());
    public static final C3660tP btq = new C3660tP("GrannyBoundPositionCurveIsGeneral", grannyJNI.GrannyBoundPositionCurveIsGeneral_get());
    public static final C3660tP btr = new C3660tP("GrannyBoundPositionCurveFlagMask", grannyJNI.GrannyBoundPositionCurveFlagMask_get());
    public static final C3660tP bts = new C3660tP("GrannyBoundOrientationCurveIsIdentity", grannyJNI.GrannyBoundOrientationCurveIsIdentity_get());
    public static final C3660tP btt = new C3660tP("GrannyBoundOrientationCurveIsConstant", grannyJNI.GrannyBoundOrientationCurveIsConstant_get());
    public static final C3660tP btu = new C3660tP("GrannyBoundOrientationCurveIsKeyframed", grannyJNI.GrannyBoundOrientationCurveIsKeyframed_get());
    public static final C3660tP btv = new C3660tP("GrannyBoundOrientationCurveIsGeneral", grannyJNI.GrannyBoundOrientationCurveIsGeneral_get());
    public static final C3660tP btw = new C3660tP("GrannyBoundOrientationCurveFlagMask", grannyJNI.GrannyBoundOrientationCurveFlagMask_get());
    public static final C3660tP btx = new C3660tP("GrannyBoundScaleShearCurveIsIdentity", grannyJNI.GrannyBoundScaleShearCurveIsIdentity_get());
    public static final C3660tP bty = new C3660tP("GrannyBoundScaleShearCurveIsConstant", grannyJNI.GrannyBoundScaleShearCurveIsConstant_get());
    public static final C3660tP btz = new C3660tP("GrannyBoundScaleShearCurveIsKeyframed", grannyJNI.GrannyBoundScaleShearCurveIsKeyframed_get());
    private static C3660tP[] btC = {btn, bto, btp, btq, btr, bts, btt, btu, btv, btw, btx, bty, btz, btA, btB};
    /* renamed from: pF */
    private static int f9237pF = 0;

    /* renamed from: pG */
    private final int f9238pG;

    /* renamed from: pH */
    private final String f9239pH;

    private C3660tP(String str) {
        this.f9239pH = str;
        int i = f9237pF;
        f9237pF = i + 1;
        this.f9238pG = i;
    }

    private C3660tP(String str, int i) {
        this.f9239pH = str;
        this.f9238pG = i;
        f9237pF = i + 1;
    }

    private C3660tP(String str, C3660tP tPVar) {
        this.f9239pH = str;
        this.f9238pG = tPVar.f9238pG;
        f9237pF = this.f9238pG + 1;
    }

    /* renamed from: eN */
    public static C3660tP m39590eN(int i) {
        if (i < btC.length && i >= 0 && btC[i].f9238pG == i) {
            return btC[i];
        }
        for (int i2 = 0; i2 < btC.length; i2++) {
            if (btC[i2].f9238pG == i) {
                return btC[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C3660tP.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f9238pG;
    }

    public String toString() {
        return this.f9239pH;
    }
}
