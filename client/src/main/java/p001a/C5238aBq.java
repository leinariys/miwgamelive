package p001a;

import java.io.InputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

/* renamed from: a.aBq  reason: case insensitive filesystem */
/* compiled from: a */
public class C5238aBq extends InflaterInputStream {
    public C5238aBq(InputStream inputStream, Inflater inflater, int i) {
        super(inputStream, inflater, i);
    }

    public C5238aBq(InputStream inputStream, Inflater inflater) {
        super(inputStream, inflater);
    }

    public C5238aBq(InputStream inputStream) {
        super(inputStream, new Inflater(true), 4096);
    }
}
