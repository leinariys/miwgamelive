package p001a;

import taikodom.render.helpers.RenderVisitor;
import taikodom.render.scene.SceneObject;
import taikodom.render.scene.SoundObject;

/* renamed from: a.pk */
/* compiled from: a */
class C3263pk implements RenderVisitor {
    C3263pk() {
    }

    public boolean visit(SceneObject sceneObject) {
        if (!(sceneObject instanceof SoundObject)) {
            return true;
        }
        ((SoundObject) sceneObject).setGain(C5916acs.getSingolton().getEngineGraphics().aed() * 0.95f);
        return true;
    }
}
