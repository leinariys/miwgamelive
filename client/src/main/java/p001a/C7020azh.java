package p001a;

import org.junit.Assert;
import org.junit.Before;

import java.io.File;
import java.io.FileFilter;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.Set;

/* renamed from: a.azh  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C7020azh extends Assert {
    private static final String gYv = ".class";
    public static final FileFilter gYw = new C2031bJ(gYv);
    public Set<String> gYz;
    private Set<Class> gYA;
    private Class<?>[] gYB;
    private ClassLoader gYx;
    private File gYy;

    /* access modifiers changed from: protected */
    /* renamed from: H */
    public abstract void mo4489H(Class<?> cls);

    /* access modifiers changed from: protected */
    public abstract Class<?>[] cFv();

    /* access modifiers changed from: protected */
    public abstract String getBase();

    @Before
    public void init() {
        this.gYx = new URLClassLoader(new URL[]{new URL("file:" + File.separator + getBase())});
        this.gYz = new HashSet();
        this.gYy = new File(getBase());
        this.gYA = new HashSet();
        this.gYB = cFv();
        cFw();
    }

    private final void cFw() {
        m27734n(this.gYy);
    }

    /* renamed from: n */
    private final void m27734n(File file) {
        for (File file2 : file.listFiles(gYw)) {
            if (file2.isDirectory()) {
                m27734n(file2);
            } else {
                m27735o(file2);
            }
        }
    }

    /* renamed from: o */
    private void m27735o(File file) {
        String absolutePath = file.getAbsolutePath();
        if (!absolutePath.startsWith(this.gYy.getAbsolutePath())) {
            fail();
        }
        String substring = absolutePath.substring(this.gYy.getAbsolutePath().length() + 1);
        Class<?> loadClass = this.gYx.loadClass(substring.replaceAll("\\" + File.separator, ".").substring(0, substring.indexOf(gYv)));
        if (this.gYB.length > 0) {
            for (Class<?> isAssignableFrom : this.gYB) {
                if (isAssignableFrom.isAssignableFrom(loadClass)) {
                    mo4489H(loadClass);
                    this.gYA.add(loadClass);
                }
            }
            return;
        }
        mo4489H(loadClass);
        this.gYA.add(loadClass);
    }

    /* access modifiers changed from: protected */
    public final void cFx() {
        System.out.println("Test finished. Classes inspected: " + this.gYA.size());
        if (!this.gYz.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append("\n\n******* Failures (" + this.gYz.size() + ") *******\n\n");
            for (String valueOf : this.gYz) {
                sb.append(String.valueOf(valueOf) + "\n");
            }
            System.out.println(sb.toString());
            fail(sb.toString());
            return;
        }
        System.out.println("No failures!");
    }
}
