package p001a;

import game.script.item.ItemType;
import game.script.item.ItemTypeCategory;

/* renamed from: a.alj  reason: case insensitive filesystem */
/* compiled from: a */
class C6375alj extends C5519aMl {

    /* renamed from: ou */
    final /* synthetic */ C2092bw f4878ou;

    C6375alj(C2092bw bwVar) {
        this.f4878ou = bwVar;
    }

    /* renamed from: a */
    public boolean mo10165a(Object obj) {
        String str = "";
        if (obj instanceof ItemType) {
            str = ((ItemType) obj).mo19891ke().get();
        } else if (obj instanceof ItemTypeCategory) {
            str = ((ItemTypeCategory) obj).mo12246ke().get();
        }
        return str.toLowerCase().contains(this.f4878ou.ejz.toLowerCase());
    }

    /* renamed from: z */
    public boolean mo10166z() {
        return this.f4878ou.ejz != null;
    }
}
