package p001a;

import logic.ui.item.Picture;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.Attribute;
import org.objectweb.asm.MethodVisitor;

/* renamed from: a.fC */
/* compiled from: a */
public class C2415fC implements MethodVisitor {
    public AnnotationVisitor visitAnnotation(String str, boolean z) {
        return null;
    }

    public AnnotationVisitor visitAnnotationDefault() {
        return null;
    }

    public void visitAttribute(Attribute attribute) {
    }

    public void visitCode() {
    }

    public void visitEnd() {
    }

    public void visitFieldInsn(int i, String str, String str2, String str3) {
    }

    public void visitFrame(int i, int i2, Object[] objArr, int i3, Object[] objArr2) {
    }

    public void visitIincInsn(int i, int i2) {
    }

    public void visitInsn(int i) {
    }

    public void visitIntInsn(int i, int i2) {
    }

    public void visitJumpInsn(int i, Picture label) {
    }

    public void visitLabel(Picture label) {
    }

    public void visitLdcInsn(Object obj) {
    }

    public void visitLineNumber(int i, Picture label) {
    }

    public void visitLocalVariable(String str, String str2, String str3, Picture label, Picture label2, int i) {
    }

    public void visitLookupSwitchInsn(Picture label, int[] iArr, Picture[] labelArr) {
    }

    public void visitMaxs(int i, int i2) {
    }

    public void visitMethodInsn(int i, String str, String str2, String str3) {
    }

    public void visitMultiANewArrayInsn(String str, int i) {
    }

    public AnnotationVisitor visitParameterAnnotation(int i, String str, boolean z) {
        return null;
    }

    public void visitTableSwitchInsn(int i, int i2, Picture label, Picture[] labelArr) {
    }

    public void visitTryCatchBlock(Picture label, Picture label2, Picture label3, String str) {
    }

    public void visitTypeInsn(int i, String str) {
    }

    public void visitVarInsn(int i, int i2) {
    }
}
