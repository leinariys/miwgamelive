package p001a;

import game.script.item.ItemType;
import game.script.item.ItemTypeCategory;
import taikodom.addon.hoplons.C3291q;

/* renamed from: a.r */
/* compiled from: a */
class C3390r extends C5519aMl {

    /* renamed from: aL */
    final /* synthetic */ C3291q f8972aL;

    C3390r(C3291q qVar) {
        this.f8972aL = qVar;
    }

    /* renamed from: a */
    public boolean mo10165a(Object obj) {
        String str = "";
        if (obj instanceof ItemType) {
            str = ((ItemType) obj).mo19891ke().get();
        } else if (obj instanceof ItemTypeCategory) {
            str = ((ItemTypeCategory) obj).mo12246ke().get();
        }
        return str.toLowerCase().contains(this.f8972aL.ejz.toLowerCase());
    }

    /* renamed from: z */
    public boolean mo10166z() {
        return this.f8972aL.ejz != null;
    }
}
