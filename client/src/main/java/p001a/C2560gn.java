package p001a;

import game.script.player.Player;

import java.awt.*;

/* renamed from: a.gn */
/* compiled from: a */
public class C2560gn {
    /* renamed from: a */
    public static String m32373a(C4045yZ yZVar, Player aku) {
        return Integer.toHexString(m32374b(yZVar, aku).getRGB()).substring(2);
    }

    /* renamed from: b */
    public static Color m32374b(C4045yZ yZVar, Player aku) {
        int lt = yZVar.mo710lt() - aku.mo12658lt();
        Color color = Color.WHITE;
        if (lt >= -2 && lt <= 2) {
            return Color.YELLOW;
        }
        if (lt >= -5 && lt <= -3) {
            return Color.GREEN;
        }
        if (lt >= 3 && lt <= 5) {
            return Color.ORANGE;
        }
        if (lt <= -6) {
            return Color.GRAY;
        }
        if (lt >= 6) {
            return Color.RED;
        }
        return color;
    }
}
