package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.Ik */
/* compiled from: a */
public class C0616Ik {
    public final C0617a deK = new C0617a();
    public final C3978xf deL = new C3978xf();
    public final Vec3f deM = new Vec3f();
    public final Vec3f deN = new Vec3f();
    public final Vec3f deO = new Vec3f();
    public final C0763Kt stack = C0763Kt.bcE();
    public float deP;
    public float deQ;
    public float deR;
    public float deS;
    public float deT;
    public float deU;
    public float deV;
    public float deW;
    public float deX;
    public float deY;
    public float deZ;
    public float dfa;
    public float dfb;
    public boolean dfc;
    public Object dfd;
    public float dfe;
    public float dff;
    public float dfg;
    public float dfh;

    public C0616Ik(C0996Og og) {
        this.deP = og.dJl;
        this.deQ = og.deQ;
        this.deR = og.dJm;
        this.deS = og.deS;
        this.deT = og.deT;
        this.deU = og.deU;
        this.deM.set(og.dJk);
        this.deN.set(og.deN);
        this.deO.set(og.deO);
        this.deV = og.deV;
        this.deW = 0.0f;
        this.dfa = 0.0f;
        this.deX = 0.0f;
        this.deY = 0.0f;
        this.dfb = 0.0f;
        this.deZ = 0.1f;
        this.dfc = og.dfc;
    }

    public float aVZ() {
        return this.deP;
    }

    /* renamed from: a */
    public void mo2872a(C6238ajC ajc, C0617a aVar) {
        this.stack.bcH().push();
        try {
            if (aVar.hPI) {
                float dot = aVar.hPC.dot(aVar.hPG);
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                vec3f2.sub(aVar.hPD, ajc.ces());
                vec3f.set(ajc.mo13897R(vec3f2));
                float dot2 = aVar.hPC.dot(vec3f);
                if (dot >= -0.1f) {
                    this.dff = 0.0f;
                    this.dfe = 10.0f;
                } else {
                    float f = -1.0f / dot;
                    this.dff = dot2 * f;
                    this.dfe = f;
                }
            } else {
                aVar.hPE = aVZ();
                this.dff = 0.0f;
                aVar.hPC.negate(aVar.hPG);
                this.dfe = 1.0f;
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a.Ik$a */
    public static class C0617a {
        public final Vec3f hPC = new Vec3f();
        public final Vec3f hPD = new Vec3f();
        public final Vec3f hPF = new Vec3f();
        public final Vec3f hPG = new Vec3f();
        public final Vec3f hPH = new Vec3f();
        public float hPE;
        public boolean hPI;
        public Object hPJ;
    }
}
