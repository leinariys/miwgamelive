package p001a;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: a.BE */
/* compiled from: a */
public class C0093BE extends C1701ZB {
    /* renamed from: jR */
    static final /* synthetic */ boolean f127jR = (!C0093BE.class.desiredAssertionStatus());
    private static final int iPJ = C3655tK.MAX_BROADPHASE_COLLISION_TYPES.ordinal();
    private static int iPR = 0;
    public final C1123QW<aGW> iPI = C0762Ks.m6640D(aGW.class);
    private final List<aGW> iPK = new ArrayList();
    private final C6548apA[][] iPP = ((C6548apA[][]) Array.newInstance(C6548apA.class, new int[]{iPJ, iPJ}));
    private int count = 0;
    private boolean iPL = true;
    private boolean iPM = false;
    private C5362aGk iPN;
    private C6433amp iPO;
    private C6760atE iPQ;
    private C0094a iPS = new C0094a((C0094a) null);

    public C0093BE(C6760atE ate) {
        this.iPQ = ate;
        mo584a((C6433amp) new C0095b((C0095b) null));
        for (int i = 0; i < iPJ; i++) {
            int i2 = 0;
            while (i2 < iPJ) {
                this.iPP[i][i2] = ate.mo13454a(C3655tK.m39582eM(i), C3655tK.m39582eM(i2));
                if (f127jR || this.iPP[i][i2] != null) {
                    i2++;
                } else {
                    throw new AssertionError();
                }
            }
        }
    }

    /* renamed from: a */
    public void mo582a(int i, int i2, C6548apA apa) {
        this.iPP[i][i2] = apa;
    }

    public C6433amp dvU() {
        return this.iPO;
    }

    /* renamed from: a */
    public void mo584a(C6433amp amp) {
        this.iPO = amp;
    }

    public C6760atE dvV() {
        return this.iPQ;
    }

    /* renamed from: a */
    public void mo585a(C6760atE ate) {
        this.iPQ = ate;
    }

    /* renamed from: a */
    public C3808vG mo581a(ayY ayy, ayY ayy2, aGW agw) {
        C1034PA pa = new C1034PA();
        pa.czU = this;
        pa.dRf = agw;
        return this.iPP[ayy.cEZ().arV().ordinal()][ayy2.cEZ().arV().ordinal()].mo8207a(pa, ayy, ayy2);
    }

    /* renamed from: g */
    public aGW mo594g(Object obj, Object obj2) {
        iPR++;
        aGW agw = this.iPI.get();
        agw.mo9008a((ayY) obj, (ayY) obj2, 0);
        agw.hUa = this.iPK.size();
        this.iPK.add(agw);
        return agw;
    }

    /* renamed from: a */
    public void mo583a(aGW agw) {
        iPR--;
        mo587b(agw);
        int i = agw.hUa;
        if (f127jR || i < this.iPK.size()) {
            Collections.swap(this.iPK, i, this.iPK.size() - 1);
            this.iPK.get(i).hUa = i;
            this.iPK.remove(this.iPK.size() - 1);
            this.iPI.release(agw);
            return;
        }
        throw new AssertionError();
    }

    /* renamed from: b */
    public void mo587b(aGW agw) {
        agw.dcC();
    }

    /* renamed from: b */
    public boolean mo588b(ayY ayy, ayY ayy2) {
        if (!f127jR && ayy == null) {
            throw new AssertionError();
        } else if (f127jR || ayy2 != null) {
            if (!this.iPM && ((ayy.cEV() || ayy.cEW()) && (ayy2.cEV() || ayy2.cEW()))) {
                this.iPM = true;
                System.err.println("warning CollisionDispatcher.needsCollision: static-static collision!");
            }
            if ((ayy.isActive() || ayy2.isActive()) && ayy.mo17039e(ayy2)) {
                return true;
            }
            return false;
        } else {
            throw new AssertionError();
        }
    }

    /* renamed from: c */
    public boolean mo591c(ayY ayy, ayY ayy2) {
        boolean z;
        if (!ayy.cEY() || !ayy2.cEY()) {
            z = false;
        } else {
            z = true;
        }
        return z && (!ayy.cEX() || !ayy2.cEX());
    }

    /* renamed from: a */
    public void mo586a(C3180oo ooVar, C5408aIe aie, C1701ZB zb) {
        this.iPS.mo596a(aie, this);
        ooVar.mo21063a((C6133ahB) this.iPS, zb);
    }

    public int bJa() {
        return this.iPK.size();
    }

    /* renamed from: po */
    public aGW mo595po(int i) {
        return this.iPK.get(i);
    }

    public List<aGW> bJb() {
        return this.iPK;
    }

    /* renamed from: a.BE$a */
    private static class C0094a implements C6133ahB {
        private C5408aIe cnP;
        private C0093BE cnQ;

        private C0094a() {
        }

        /* synthetic */ C0094a(C0094a aVar) {
            this();
        }

        /* renamed from: a */
        public void mo596a(C5408aIe aie, C0093BE be) {
            this.cnP = aie;
            this.cnQ = be;
        }

        /* renamed from: a */
        public boolean mo597a(aUJ auj) {
            this.cnQ.dvU().mo598a(auj, this.cnQ, this.cnP);
            return false;
        }
    }

    /* renamed from: a.BE$b */
    /* compiled from: a */
    private static class C0095b implements C6433amp {
        private final C5362aGk eWZ;

        private C0095b() {
            this.eWZ = new C5362aGk();
        }

        /* synthetic */ C0095b(C0095b bVar) {
            this();
        }

        /* renamed from: a */
        public void mo598a(aUJ auj, C0093BE be, C5408aIe aie) {
            ayY ayy = (ayY) auj.iXz.dxy;
            ayY ayy2 = (ayY) auj.iXA.dxy;
            if (be.mo588b(ayy, ayy2)) {
                if (auj.iXB == null) {
                    auj.iXB = be.mo7323a(ayy, ayy2);
                }
                if (auj.iXB != null) {
                    this.eWZ.mo9114d(ayy, ayy2);
                    if (aie.iaL == C5647aRj.DISPATCH_DISCRETE) {
                        auj.iXB.mo8931a(ayy, ayy2, aie, this.eWZ);
                        return;
                    }
                    float b = auj.iXB.mo8932b(ayy, ayy2, aie, this.eWZ);
                    if (aie.iaM > b) {
                        aie.iaM = b;
                    }
                }
            }
        }
    }
}
