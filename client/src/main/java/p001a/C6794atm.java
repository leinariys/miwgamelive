package p001a;

import java.util.Arrays;

/* renamed from: a.atm  reason: case insensitive filesystem */
/* compiled from: a */
public final class C6794atm {
    private C3311qA[] gzo;
    private int size;

    public C6794atm(int i) {
        this.gzo = new C3311qA[i];
    }

    public C6794atm() {
        this(128);
    }

    public void ensureCapacity(int i) {
        int length = this.gzo.length;
        if (i > length) {
            int i2 = ((length * 3) / 2) + 1;
            if (i2 >= i) {
                i = i2;
            }
            this.gzo = m26054a(this.gzo, i);
        }
    }

    public int size() {
        return this.size;
    }

    /* renamed from: uA */
    public C3311qA mo16250uA(int i) {
        return this.gzo[i];
    }

    /* renamed from: a */
    public void mo16242a(int i, C3311qA qAVar) {
        this.gzo[i] = qAVar;
    }

    /* renamed from: a */
    public void mo16243a(C3311qA qAVar) {
        ensureCapacity(this.size + 1);
        C3311qA[] qAVarArr = this.gzo;
        int i = this.size;
        this.size = i + 1;
        qAVarArr[i] = qAVar;
    }

    /* renamed from: b */
    public void mo16244b(int i, C3311qA qAVar) {
        if (i > this.size || i < 0) {
            throw new IndexOutOfBoundsException("Index: " + i + ", Size: " + this.size);
        }
        ensureCapacity(this.size + 1);
        System.arraycopy(this.gzo, i, this.gzo, i + 1, this.size - i);
        this.gzo[i] = qAVar;
        this.size++;
    }

    public void remove(int i) {
        int i2 = (this.size - i) - 1;
        if (i2 > 0) {
            System.arraycopy(this.gzo, i + 1, this.gzo, i, i2);
        }
        C3311qA[] qAVarArr = this.gzo;
        int i3 = this.size - 1;
        this.size = i3;
        qAVarArr[i3] = null;
    }

    /* renamed from: a */
    private C3311qA[] m26054a(C3311qA[] qAVarArr, int i) {
        C3311qA[] qAVarArr2 = new C3311qA[i];
        int min = Math.min(qAVarArr.length, i);
        for (int i2 = 0; i2 < min; i2++) {
            qAVarArr2[i2] = qAVarArr[i2];
        }
        while (min < i) {
            qAVarArr2[min] = null;
            min++;
        }
        return qAVarArr2;
    }

    public String toString() {
        if (size() == 0) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (int i = 0; i < this.size; i++) {
            sb.append(mo16250uA(i));
            if (i < this.size - 1) {
                sb.append(", ");
            }
        }
        return sb.append(']').toString();
    }

    public void clear() {
        if (this.size > 0) {
            Arrays.fill(this.gzo, 0, this.size, (Object) null);
            this.size = 0;
        }
    }
}
