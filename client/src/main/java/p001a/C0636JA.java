package p001a;

import game.network.message.serializable.C2991mi;
import logic.res.html.MessageContainer;

import java.util.Collection;
import java.util.List;

/* renamed from: a.JA */
/* compiled from: a */
public abstract class C0636JA<T> implements Collection<T> {

    public boolean bFM;
    public Collection<T> dkv;

    public C0636JA(Collection<T> collection) {
        this.dkv = collection;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo2919a(C2991mi<T> miVar);

    public abstract List<C2991mi<T>> anB();

    /* access modifiers changed from: protected */
    public abstract void anC();

    public boolean add(T t) {
        anC();
        if (!this.dkv.add(t)) {
            return false;
        }
        mo2919a(new C6630aqe(C0651JP.ADD, t));
        return true;
    }

    public boolean remove(Object obj) {
        anC();
        if (!this.dkv.remove(obj)) {
            return false;
        }
        mo2919a(new C6630aqe(C0651JP.REMOVE, obj));
        return true;
    }

    public boolean contains(Object obj) {
        return this.dkv.contains(obj);
    }

    /* access modifiers changed from: package-private */
    public int anD() {
        List anB = anB();
        if (anB == null) {
            return 0;
        }
        return anB.size();
    }

    public int size() {
        return this.dkv.size();
    }

    public boolean addAll(Collection<? extends T> collection) {
        anC();
        this.dkv.addAll(collection);
        mo2919a(new C6630aqe(C0651JP.ADD_COLLECTION, collection));
        return collection.size() > 0;
    }

    public void clear() {
        anC();
        this.dkv.clear();
        mo2919a(new C6786ate(C0651JP.CLEAR));
    }

    public boolean containsAll(Collection<?> collection) {
        return this.dkv.containsAll(collection);
    }

    public boolean isEmpty() {
        return this.dkv.isEmpty();
    }

    public Object[] toArray() {
        return this.dkv.toArray();
    }

    public <T> T[] toArray(T[] tArr) {
        return this.dkv.toArray(tArr);
    }

    public boolean removeAll(Collection<?> collection) {
        anC();
        List dgK = MessageContainer.init();
        for (Object next : collection) {
            if (this.dkv.remove(next)) {
                dgK.add(next);
            }
        }
        if (dgK.size() <= 0) {
            return false;
        }
        mo2919a(new C6630aqe(C0651JP.REMOVE_COLLECTION, dgK));
        return true;
    }

    public boolean retainAll(Collection<?> collection) {
        anC();
        List<VALUE> w = MessageContainer.m16004w(this.dkv);
        boolean retainAll = this.dkv.retainAll(collection);
        w.removeAll(this.dkv);
        if (w.size() > 0) {
            mo2919a(new C6630aqe(C0651JP.REMOVE_COLLECTION, w));
        }
        return retainAll;
    }
}
