package p001a;

import game.script.item.Item;
import game.script.newmarket.BuyOrderInfo;
import game.script.newmarket.ListContainer;
import game.script.newmarket.SellOrderInfo;
import game.script.player.Player;
import game.script.player.PlayerAssistant;
import game.script.ship.Station;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.stationmarket.C0041AY;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.KM */
/* compiled from: a */
public class C0716KM {

    /* access modifiers changed from: private */
    public C0041AY dqL;
    /* access modifiers changed from: private */
    public List<BuyOrderInfo> dqM = new ArrayList();
    /* access modifiers changed from: private */
    public List<SellOrderInfo> dqN = new ArrayList();
    /* renamed from: iH */
    public Station f938iH = null;
    /* renamed from: P */
    private Player f937P;
    /* access modifiers changed from: private */
    public PlayerAssistant dqK = this.f937P.dyl();
    /* access modifiers changed from: private */
    private C2495fo apa;
    /* renamed from: kj */
    private IAddonProperties f939kj;

    public C0716KM(IAddonProperties vWVar, C2495fo foVar) {
        this.f939kj = vWVar;
        this.apa = foVar;
        this.f937P = vWVar.getPlayer();
        if (this.f937P.bQB() && (this.f937P.bhE() instanceof Station)) {
            this.f938iH = (Station) this.f937P.bhE();
        }
        this.dqL = new C0041AY(vWVar, this, foVar);
    }

    public void dispose() {
    }

    public boolean bdd() {
        if (!this.f937P.bQB() || !(this.f937P.bhE() instanceof Station)) {
            this.f939kj.getLog().error("Player should be in station while this controller is active.");
            return false;
        }
        ((PlayerAssistant) C3582se.m38985a(this.dqK, (C6144ahM<?>) new C0720d())).dBC();
        ((PlayerAssistant) C3582se.m38985a(this.dqK, (C6144ahM<?>) new C0719c())).dBE();
        return true;
    }

    /* renamed from: a */
    public void mo3500a(BuyOrderInfo sfVar, Item auq) {
        try {
            ((PlayerAssistant) C3582se.m38985a(this.dqK, (C6144ahM<?>) new C0718b(sfVar, auq))).mo11966c(sfVar, auq);
        } catch (C3402rH e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    public void mo3501a(SellOrderInfo xTVar, int i, C0314EF ef) {
        try {
            ((PlayerAssistant) C3582se.m38985a(this.dqK, (C6144ahM<?>) new C0717a(xTVar, i))).mo11967c(xTVar, i, ef);
        } catch (C3402rH e) {
            e.printStackTrace();
        }
    }

    public List<BuyOrderInfo> bde() {
        return this.dqM;
    }

    public List<SellOrderInfo> bdf() {
        return this.dqN;
    }

    /* renamed from: a.KM$d */
    /* compiled from: a */
    class C0720d implements C6144ahM<Boolean> {
        C0720d() {
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            if (bool.booleanValue()) {
                C0716KM.this.dqM.clear();
                for (BuyOrderInfo add : ((ListContainer) C0716KM.this.dqK.dBI().get(C0716KM.this.f938iH)).getList()) {
                    C0716KM.this.dqM.add(add);
                }
            }
            C0716KM.this.dqL.bjh();
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }
    }

    /* renamed from: a.KM$c */
    /* compiled from: a */
    class C0719c implements C6144ahM<Boolean> {
        C0719c() {
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            if (bool.booleanValue()) {
                C0716KM.this.dqN.clear();
                for (SellOrderInfo add : ((ListContainer) C0716KM.this.dqK.dBK().get(C0716KM.this.f938iH)).getList()) {
                    C0716KM.this.dqN.add(add);
                }
            }
            C0716KM.this.dqL.bji();
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }
    }

    /* renamed from: a.KM$b */
    /* compiled from: a */
    class C0718b implements C6144ahM<Boolean> {

        /* renamed from: IX */
        private final /* synthetic */ BuyOrderInfo f942IX;

        /* renamed from: IY */
        private final /* synthetic */ Item f943IY;

        C0718b(BuyOrderInfo sfVar, Item auq) {
            this.f942IX = sfVar;
            this.f943IY = auq;
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            this.f942IX.mo8613K(this.f942IX.dBu() + this.f943IY.mo302Iq());
            if (this.f942IX.mo8627ff() <= 0) {
                C0716KM.this.dqM.remove(this.f942IX);
            }
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            th.printStackTrace();
        }
    }

    /* renamed from: a.KM$a */
    class C0717a implements C6144ahM<Boolean> {

        /* renamed from: IZ */
        private final /* synthetic */ SellOrderInfo f940IZ;

        /* renamed from: Ja */
        private final /* synthetic */ int f941Ja;

        C0717a(SellOrderInfo xTVar, int i) {
            this.f940IZ = xTVar;
            this.f941Ja = i;
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            this.f940IZ.mo8613K(this.f940IZ.dBu() + this.f941Ja);
            if (this.f940IZ.mo8627ff() <= 0) {
                C0716KM.this.dqN.remove(this.f940IZ);
            }
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            th.printStackTrace();
        }
    }
}
