package p001a;

import game.geometry.Vec3d;

/* renamed from: a.VZ */
/* compiled from: a */
public class C1465VZ extends C6518aoW<Vec3d> {
    /* renamed from: l */
    public Vec3d mo6157l(float f, float f2, float f3) {
        Vec3d ajr = (Vec3d) get();
        ajr.set((double) f, (double) f2, (double) f3);
        return ajr;
    }

    /* renamed from: N */
    public Vec3d mo6154N(Vec3d ajr) {
        Vec3d ajr2 = (Vec3d) get();
        ajr2.mo9484aA(ajr);
        return ajr2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: bDn */
    public Vec3d create() {
        return new Vec3d();
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void copy(Vec3d ajr, Vec3d ajr2) {
        ajr.mo9484aA(ajr2);
    }
}
