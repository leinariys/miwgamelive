package p001a;

/* renamed from: a.jN */
/* compiled from: a */
public abstract class C2742jN implements Runnable {
    private boolean api;
    private boolean apj;

    public void cancel() {
        this.api = true;
    }

    /* access modifiers changed from: package-private */
    public boolean isCanceled() {
        return this.api;
    }

    public void reset() {
        this.apj = true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: Iw */
    public boolean mo19913Iw() {
        return this.apj;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: am */
    public void mo19914am(boolean z) {
        this.apj = z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: an */
    public void mo19915an(boolean z) {
        this.api = z;
    }
}
