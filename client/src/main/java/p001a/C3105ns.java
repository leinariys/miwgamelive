package p001a;

import game.script.ship.Station;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.ns */
/* compiled from: a */
public class C3105ns extends JLabel implements ListCellRenderer {


    public C3105ns() {
        setOpaque(true);
        setHorizontalAlignment(2);
        setVerticalAlignment(0);
    }

    /* renamed from: i */
    public static String m36511i(Station bf) {
        return String.valueOf(bf.azW().mo21665ke().get()) + " - " + bf.getName();
    }

    /* renamed from: a */
    public static int m36510a(Station bf, Station bf2) {
        return m36511i(bf).compareTo(m36511i(bf2));
    }

    public Component getListCellRendererComponent(JList jList, Object obj, int i, boolean z, boolean z2) {
        if (obj instanceof Station) {
            setText(m36511i((Station) obj));
        } else if (obj instanceof String) {
            setText((String) obj);
        } else {
            setText("ERROR - NEITHER STATION NOR STRING");
        }
        return this;
    }
}
