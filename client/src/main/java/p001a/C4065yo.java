package p001a;

import game.network.aQD;
import org.apache.commons.jxpath.JXPathContext;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: a.yo */
/* compiled from: a */
public class C4065yo extends aQD {
    /* renamed from: a */
    public String mo2073a(Iterator<?> it, List<String> list, Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        String str = "";
        Iterator<String> it2 = list.iterator();
        while (true) {
            String str2 = str;
            if (!it2.hasNext()) {
                break;
            }
            sb.append(str2);
            sb.append("\"").append(map.get(it2.next())).append("\"");
            str = ";";
        }
        sb.append("\n");
        while (it.hasNext()) {
            JXPathContext newContext = JXPathContext.newContext(it.next());
            newContext.setFunctions(this.iEM);
            String str3 = "";
            Iterator<String> it3 = list.iterator();
            while (true) {
                String str4 = str3;
                if (!it3.hasNext()) {
                    break;
                }
                sb.append(str4);
                sb.append("\"");
                String str5 = "";
                for (Object append : newContext.selectNodes(it3.next())) {
                    sb.append(str5).append(append);
                    str5 = "\n";
                }
                sb.append("\"");
                str3 = ";";
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
