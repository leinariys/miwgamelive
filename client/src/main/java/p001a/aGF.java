package p001a;

import game.engine.DataGameEvent;
import game.network.message.serializable.C1695Yw;
import logic.thred.LogPrinter;
import taikodom.render.loader.provider.FilePath;

import java.io.IOException;
import java.util.zip.GZIPInputStream;

/* renamed from: a.aGF */
/* compiled from: a */
public class aGF implements WraperDbFile {
    private static final LogPrinter log = LogPrinter.m10275K(aGF.class);
    public WraperDbFileImpl hQa = new WraperDbFileImpl();
    private FilePath eKA;
    private C1412Uh guy;

    public boolean bOc() {
        return this.hQa.bOc();
    }

    public void flush() {
        this.hQa.flush();
    }

    public void init() {
        this.hQa.init();
    }

    public boolean bOd() {
        FilePath ain;
        GZIPInputStream openInputStream;
        FilePath aC = this.eKA.concat("environment-snapshot.xml.gz");
        if (!aC.exists()) {
            ain = aC.mo2249BB().concat(aC.getName().replaceAll("[.]gz$", ""));
        } else {
            ain = aC;
        }
        if (!ain.exists()) {
            return this.hQa.bOd();
        }
        long currentTimeMillis = System.currentTimeMillis();
        log.info("LOADING OLD FASHIONED XML REPOSITORY DATA...");
        log.info("Reading " + ain.getName() + "...");
        if (ain.getName().toLowerCase().endsWith(".gz")) {
            openInputStream = new GZIPInputStream(ain.openInputStream());
        } else {
            openInputStream = ain.openInputStream();
        }
        try {
            new C0233Cv((DataGameEvent) this.guy, openInputStream);
            if (openInputStream != null) {
                try {
                    openInputStream.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            log.info(String.format("done. (took %.2fs)", new Object[]{Double.valueOf(((double) (System.currentTimeMillis() - currentTimeMillis)) / 1000.0d)}));
            System.gc();
            System.gc();
            ain.mo2261d(ain.mo2249BB().concat(String.valueOf(ain.getName()) + ".old.delete-later.bak"));
            this.hQa.bOc();
            return true;
        } catch (Exception e2) {
            throw new CountFailedReadOrWriteException((Throwable) e2);
        } catch (Throwable th) {
            if (openInputStream != null) {
                try {
                    openInputStream.close();
                } catch (IOException e3) {
                    throw new RuntimeException(e3);
                }
            }
            throw th;
        }
    }

    /* renamed from: h */
    public void mo5236h(C3582se seVar) {
        this.hQa.mo5236h(seVar);
    }

    public void bOe() {
        this.hQa.bOe();
    }

    public void bOf() {
        this.hQa.bOf();
    }

    /* renamed from: i */
    public void mo5238i(C3582se seVar) {
        this.hQa.mo5238i(seVar);
    }

    /* renamed from: c */
    public void mo5226c(C1695Yw yw) {
        this.hQa.mo5226c(yw);
    }

    /* renamed from: a */
    public void mo5219a(C1412Uh uh) {
        this.guy = uh;
        this.hQa.mo5219a(uh);
    }

    /* renamed from: f */
    public void mo5234f(FilePath ain) {
        this.eKA = ain;
        this.hQa.mo5234f(ain);
    }

    public void start() {
        this.hQa.start();
    }

    /* renamed from: HY */
    public void mo5218HY() {
        this.hQa.mo5218HY();
    }
}
