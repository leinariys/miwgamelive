package p001a;

/* renamed from: a.aRj  reason: case insensitive filesystem */
/* compiled from: a */
public enum C5647aRj {
    DISPATCH_DISCRETE(1),
    DISPATCH_CONTINUOUS(2);

    private int value;

    private C5647aRj(int i) {
        this.value = i;
    }

    public int getValue() {
        return this.value;
    }
}
