package p001a;

/* renamed from: a.FZ */
/* compiled from: a */
public abstract class C0400FZ<T> {
    private C3385qw cXi;

    /* access modifiers changed from: protected */
    /* renamed from: N */
    public abstract boolean mo2164N(Object obj);

    public abstract boolean evaluate(T t);

    /* renamed from: O */
    public void mo2165O(Object obj) {
        if (this.cXi != null && mo2164N(obj)) {
            this.cXi.mo21537Xg();
        }
    }

    /* renamed from: a */
    public void mo2166a(C3385qw qwVar) {
        this.cXi = qwVar;
    }

    public void aQx() {
        this.cXi = null;
    }
}
