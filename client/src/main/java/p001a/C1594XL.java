package p001a;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.XL */
/* compiled from: a */
public @interface C1594XL {

    C1595a bGi() default C1595a.Delayed;

    /* renamed from: a.XL$a */
    public enum C1595a {
        Delayed,
        Immediate
    }
}
