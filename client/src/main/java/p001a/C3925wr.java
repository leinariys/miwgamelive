package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Quat4fWrap;
import game.network.message.externalizable.C6853aut;
import logic.res.XmlNode;
import taikodom.addon.neo.preferences.GUIPrefAddon;
import taikodom.render.loader.provider.FilePath;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.wr */
/* compiled from: a */
public class C3925wr {
    /* renamed from: cw */
    public static List<C6853aut> m40752cw(String str) {
        ArrayList arrayList = new ArrayList();
        FilePath aC = C2606hU.getRootPathResource().concat("res/bone/" + (String.valueOf(str) + ".bone"));
        try {
            InputStream openInputStream = aC.openInputStream();
            for (XmlNode next : new LoaderFileXML().loadFileXml(openInputStream, "UTF-8").mo9057q("Bone", (String) null, (String) null)) {
                String attribute = next.findNodeChildTagDepth("Name").getAttribute("value");
                Vec3f vec3f = new Vec3f();
                XmlNode mD = next.findNodeChildTagDepth("Position");
                vec3f.x = Float.parseFloat(mD.getAttribute(GUIPrefAddon.C4817c.X));
                vec3f.y = Float.parseFloat(mD.getAttribute(GUIPrefAddon.C4817c.Y));
                vec3f.z = Float.parseFloat(mD.getAttribute("z"));
                XmlNode mD2 = next.findNodeChildTagDepth("Orientation");
                Quat4fWrap aoy = new Quat4fWrap(Float.parseFloat(mD2.getAttribute(GUIPrefAddon.C4817c.X)), Float.parseFloat(mD2.getAttribute(GUIPrefAddon.C4817c.Y)), Float.parseFloat(mD2.getAttribute("z")), Float.parseFloat(mD2.getAttribute(GUIPrefAddon.C4817c.WIDTH)));
                Matrix4fWrap ajk = new Matrix4fWrap();
                aoy.mo15237e(ajk);
                ajk.setTranslation(vec3f);
                arrayList.add(new C6853aut(attribute, ajk));
            }
            openInputStream.close();
            return arrayList;
        } catch (IOException e) {
            throw new C1547Wh(aC.getName());
        }
    }
}
