package p001a;

import game.network.aQD;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.Pointer;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: a.aQs  reason: case insensitive filesystem */
/* compiled from: a */
public class C5630aQs extends aQD {
    /* renamed from: a */
    public String mo2073a(Iterator<?> it, List<String> list, Map<String, String> map) {
        Object value;
        boolean z;
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\"?>\n<?mso-application progid='Excel.Sheet'?>\n<Workbook xmlns='urn:schemas-microsoft-com:office:spreadsheet' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet' xmlns:html='http://www.w3.org/TR/REC-html40'>");
        sb.append("\n<Worksheet ss:Name='Data'><Table>");
        sb.append("\n\t<Row>");
        for (String str : list) {
            sb.append("\n\t\t<Cell><Data ss:Type='String'>").append(map.get(str)).append("</Data></Cell>");
        }
        sb.append("\n\t</Row>");
        while (it.hasNext()) {
            sb.append("\n\t<Row>");
            JXPathContext newContext = JXPathContext.newContext(it.next());
            newContext.setFunctions(this.iEM);
            for (String next : list) {
                JXPathContext jXPathContext = null;
                try {
                    Pointer createPath = newContext.createPath(next);
                    jXPathContext = newContext.getRelativeContext(createPath);
                    value = createPath.getValue();
                    z = value instanceof Number;
                } catch (RuntimeException e) {
                    value = newContext.getValue(next);
                    z = false;
                }
                sb.append("\n\t\t<Cell><Data ss:Type='").append(z ? "Number" : "String").append("'>").append(value).append("</Data>");
                if (z) {
                    sb.append("\n\t\t<Comment><ss:Data>");
                    sb.append(jXPathContext.getParentContext().getValue("oid")).append(".");
                    sb.append(jXPathContext.getNamespaceContextPointer().asPath().substring(1));
                    sb.append(" ").append(value).append("</ss:Data></Comment>");
                }
                sb.append("</Cell>");
            }
            sb.append("\n\t</Row>");
        }
        sb.append("</Table></Worksheet>");
        return sb.toString();
    }
}
