package p001a;

import game.network.exception.CreatListenerChannelException;

import java.io.DataInput;
import java.io.DataInputStream;

/* renamed from: a.Ua */
/* compiled from: a */
public class C1403Ua implements DataInput {
    public static final int[] emM = new int[32];

    static {
        for (int i = 0; i < 32; i++) {
            emM[i] = 0;
        }
    }

    int emJ;
    int emK = 0;
    DataInput emL;

    /* renamed from: a */
    public int mo5878a(int i) {
        int i2;
        if (i <= this.emK) {
            int i3 = this.emJ;
            int[] iArr = emM;
            int i4 = this.emK - i;
            this.emK = i4;
            return i3 >> iArr[i4];
        }
        if (this.emK > 0) {
            i2 = this.emJ & emM[this.emK];
            this.emK = 0;
            i -= this.emK;
        } else {
            i2 = 0;
        }
        while (i > 8) {
            i2 = (i2 << 8) | (this.emL.readUnsignedByte() & 255);
            i -= 8;
        }
        if (i <= 0) {
            return i2;
        }
        this.emJ = this.emL.readUnsignedByte();
        int i5 = this.emJ;
        int[] iArr2 = emM;
        int i6 = 8 - i;
        this.emK = i6;
        return (i2 << i) | (i5 >> iArr2[i6]);
    }

    public boolean readBoolean() {
        return mo5878a(1) != 0;
    }

    public char readChar() {
        return (char) mo5878a(16);
    }

    public double readDouble() {
        return Double.longBitsToDouble(readLong());
    }

    public float readFloat() {
        return Float.intBitsToFloat(mo5878a(32));
    }

    public void readFully(byte[] bArr) {
        for (int i = 0; i < bArr.length; i++) {
            bArr[i] = readByte();
        }
    }

    public void readFully(byte[] bArr, int i, int i2) {
        while (true) {
            i2--;
            if (i2 >= 0) {
                bArr[i] = readByte();
                i++;
            } else {
                return;
            }
        }
    }

    public int readInt() {
        return mo5878a(32);
    }

    @Deprecated
    public String readLine() {
        throw new CreatListenerChannelException();
    }

    public long readLong() {
        return (((long) mo5878a(32)) << 32) | ((long) mo5878a(32));
    }

    public short readShort() {
        return (short) mo5878a(32);
    }

    public String readUTF() {
        return DataInputStream.readUTF(this);
    }

    public int readUnsignedByte() {
        return mo5878a(8);
    }

    public int readUnsignedShort() {
        return mo5878a(16);
    }

    public int skipBytes(int i) {
        int i2 = i * 8;
        if (i2 < this.emK) {
            this.emK -= i2;
        } else {
            int i3 = i2 - this.emK;
            skipBytes(i3 / 8);
            int i4 = i3 & 7;
            if (i4 > 0) {
                this.emJ = this.emL.readUnsignedByte();
                this.emK = 8 - i4;
            }
        }
        return i;
    }

    public byte readByte() {
        return (byte) mo5878a(8);
    }
}
