package p001a;

import logic.res.ILoaderTrail;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import taikodom.render.loader.FileSceneLoader;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.loader.provider.FilePath;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/* renamed from: a.awQ  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C6928awQ implements ILoaderTrail {
    private static final Log log = LogFactory.getLog(C6928awQ.class);
    public List<C5964ado> listeners = new ArrayList();
    private FileSceneLoader gNT;
    private String gNU = Locale.getDefault().getLanguage().toLowerCase();
    private FilePath rootpath;

    public C6928awQ(FilePath ain, FileSceneLoader fileSceneLoader) {
        this.gNT = fileSceneLoader;
        this.rootpath = ain;
        mo16690bV("data/dummy_data.pro");
    }

    private SceneLoader cBk() {
        return this.gNT;
    }

    private String cBl() {
        return this.gNU;
    }

    /* renamed from: kw */
    private boolean m26871kw(String str) {
        return this.rootpath.concat(str).exists();
    }

    /* access modifiers changed from: protected */
    /* renamed from: bV */
    public void mo16690bV(String str) {
        if (str == null) {
            log.info("trying to loadStockDefinitions from null file!");
        } else if (cBk().getStockFile(str) == null) {
            if (cBl() != null) {
                String replaceAll = str.replaceAll("([.][^.])$", "_" + cBl() + "$1");
                if (m26871kw(replaceAll)) {
                    str = replaceAll;
                }
            }
            if (cBk().getStockFile(str) != null) {
                return;
            }
            if (!m26871kw(str)) {
                throw new aOX(String.valueOf(this.rootpath.concat(str).getPath()) + " not found");
            }
            m26872kx(str);
        }
    }

    /* renamed from: kx */
    private void m26872kx(String str) {
        FilePath aC = this.rootpath.concat(str);
        try {
            this.gNT.loadFile(str);
            log.info("loaded file " + str);
            while (this.gNT.getMissingIncludes().size() > 0) {
                mo16690bV(this.gNT.getMissingIncludes().iterator().next());
            }
        } catch (IOException e) {
            throw new RuntimeException("Problems loading " + aC, e);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: bT */
    public RenderAsset mo16689bT(String str) {
        if (str == null) {
            System.err.println("null object name");
            return null;
        }
        if (cBl() != null) {
            String str2 = String.valueOf(str) + "_" + cBl();
            if (cBk().getStockEntry(str2) != null) {
                str = str2;
            }
        }
        return cBk().instantiateAsset(str);
    }

    /* renamed from: c */
    public void mo16691c(C5964ado ado) {
        this.listeners.add(ado);
    }

    /* renamed from: d */
    public void mo16692d(C5964ado ado) {
        this.listeners.remove(ado);
    }
}
