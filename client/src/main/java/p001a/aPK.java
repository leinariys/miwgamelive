package p001a;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aPK */
/* compiled from: a */
public final class aPK {
    public static final aPK iCa = new aPK("GrannyFirstGRNUserTag", grannyJNI.GrannyFirstGRNUserTag_get());
    public static final aPK iCb = new aPK("GrannyLastGRNUserTag", grannyJNI.GrannyLastGRNUserTag_get());
    public static final aPK iCc = new aPK("GrannyFirstGRNStandardTag", grannyJNI.GrannyFirstGRNStandardTag_get());
    public static final aPK iCd = new aPK("GrannyLastGRNStandardTag", grannyJNI.GrannyLastGRNStandardTag_get());
    private static aPK[] iCe = {iCa, iCb, iCc, iCd};

    /* renamed from: pF */
    private static int f3515pF = 0;

    /* renamed from: pG */
    private final int f3516pG;

    /* renamed from: pH */
    private final String f3517pH;

    private aPK(String str) {
        this.f3517pH = str;
        int i = f3515pF;
        f3515pF = i + 1;
        this.f3516pG = i;
    }

    private aPK(String str, int i) {
        this.f3517pH = str;
        this.f3516pG = i;
        f3515pF = i + 1;
    }

    private aPK(String str, aPK apk) {
        this.f3517pH = str;
        this.f3516pG = apk.f3516pG;
        f3515pF = this.f3516pG + 1;
    }

    /* renamed from: Ac */
    public static aPK m17160Ac(int i) {
        if (i < iCe.length && i >= 0 && iCe[i].f3516pG == i) {
            return iCe[i];
        }
        for (int i2 = 0; i2 < iCe.length; i2++) {
            if (iCe[i2].f3516pG == i) {
                return iCe[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + aPK.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f3516pG;
    }

    public String toString() {
        return this.f3517pH;
    }
}
