package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.akr  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6331akr {
    /* renamed from: D */
    void mo1844D(Vec3f vec3f, Vec3f vec3f2);

    /* renamed from: a */
    int mo1845a(Vec3f[] vec3fArr, Vec3f[] vec3fArr2, Vec3f[] vec3fArr3);

    /* renamed from: aK */
    boolean mo1849aK(Vec3f vec3f);

    /* renamed from: aL */
    boolean mo1850aL(Vec3f vec3f);

    /* renamed from: aM */
    void mo1851aM(Vec3f vec3f);

    float cgi();

    boolean cgj();

    boolean cgk();

    int numVertices();

    /* renamed from: p */
    void mo1858p(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3);

    void reset();
}
