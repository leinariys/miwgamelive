package p001a;

import game.network.message.externalizable.C5966adq;
import game.script.item.*;
import logic.baa.aDJ;
import logic.ui.item.ListJ;
import taikodom.addon.C6144ahM;

import javax.swing.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;
import java.util.Collection;

/* renamed from: a.Ha */
/* compiled from: a */
public class C0539Ha extends TransferHandler {

    private ItemLocation bLj;

    public ItemLocation apL() {
        return this.bLj;
    }

    /* renamed from: b */
    public void mo2646b(ItemLocation aag) {
        this.bLj = aag;
    }

    public boolean canImport(TransferHandler.TransferSupport transferSupport) {
        if (!transferSupport.isDataFlavorSupported(DataFlavor.stringFlavor)) {
            return false;
        }
        if (transferSupport.getDropLocation().getIndex() == -1) {
            return false;
        }
        return true;
    }

    public boolean importData(TransferHandler.TransferSupport transferSupport) {
        boolean z;
        if (this.bLj == null) {
            return false;
        }
        if (!transferSupport.isDrop()) {
            return false;
        }
        if (!transferSupport.isDataFlavorSupported(C2125cH.f6026vP)) {
            return false;
        }
        try {
            for (aDJ adj : (Collection) transferSupport.getTransferable().getTransferData(C2125cH.f6026vP)) {
                if (adj instanceof Item) {
                    Item auq = (Item) adj;
                    if (auq.bNh() != this.bLj) {
                        ListJ component = transferSupport.getComponent();
                        Object elementAt = component.getModel().getElementAt(component.locationToIndex(transferSupport.getDropLocation().getDropPoint()));
                        if (elementAt instanceof ProjectileWeapon) {
                            ProjectileWeapon sg = (ProjectileWeapon) elementAt;
                            if (auq instanceof ClipBox) {
                                if (sg != null) {
                                    component.getParent().getParent().getParent().mo4911Kk();
                                    ((ProjectileWeapon) C3582se.m38985a(sg, (C6144ahM<?>) new C0543b(component, sg, auq))).mo5362b((ClipBox) auq);
                                }
                            } else if ((auq instanceof Clip) && sg != null) {
                                component.getParent().getParent().getParent().mo4911Kk();
                                ((ProjectileWeapon) C3582se.m38985a(sg, (C6144ahM<?>) new C0540a(component, sg, auq))).mo5368e((Clip) auq);
                            }
                        }
                    }
                }
                if (adj instanceof ItemType) {
                    if (!C5916acs.getSingolton().getTaikodom().aLS().ads()) {
                        z = false;
                    } else {
                        z = true;
                    }
                    if (z) {
                        this.bLj.mo2695u((ItemType) adj);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public int getSourceActions(JComponent jComponent) {
        return 2;
    }

    /* access modifiers changed from: protected */
    public Transferable createTransferable(JComponent jComponent) {
        try {
            Object[] selectedValues = ((JList) jComponent).getSelectedValues();
            ArrayList arrayList = new ArrayList(selectedValues.length);
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= selectedValues.length) {
                    return new C5569aOj((Item[]) arrayList.toArray(new Item[arrayList.size()]));
                }
                arrayList.add(((C2459fd.C2479f) selectedValues[i2]).bHV());
                i = i2 + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void exportDone(JComponent jComponent, Transferable transferable, int i) {
    }

    /* renamed from: a.Ha$b */
    /* compiled from: a */
    class C0543b implements C6144ahM<Item> {
        private final /* synthetic */ Item cQM;
        private final /* synthetic */ ListJ fRm;
        private final /* synthetic */ ProjectileWeapon fRn;

        C0543b(ListJ aan, ProjectileWeapon sg, Item auq) {
            this.fRm = aan;
            this.fRn = sg;
            this.cQM = auq;
        }

        /* renamed from: o */
        public void mo1931n(Item auq) {
            SwingUtilities.invokeLater(new C0544a(this.fRm, this.fRn, this.cQM));
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            SwingUtilities.invokeLater(new C0545b(this.fRm));
            th.printStackTrace();
        }

        /* renamed from: a.Ha$b$a */
        class C0544a implements Runnable {
            private final /* synthetic */ Item cQM;
            private final /* synthetic */ ListJ fRm;
            private final /* synthetic */ ProjectileWeapon fRn;

            C0544a(ListJ aan, ProjectileWeapon sg, Item auq) {
                this.fRm = aan;
                this.fRn = sg;
                this.cQM = auq;
            }

            public void run() {
                this.fRm.getParent().getParent().getParent().mo4912Kl();
                this.fRn.ald().getEventManager().mo13975h(new C5966adq(this.fRn, this.cQM));
            }
        }

        /* renamed from: a.Ha$b$b */
        /* compiled from: a */
        class C0545b implements Runnable {
            private final /* synthetic */ ListJ fRm;

            C0545b(ListJ aan) {
                this.fRm = aan;
            }

            public void run() {
                this.fRm.getParent().getParent().getParent().mo4912Kl();
            }
        }
    }

    /* renamed from: a.Ha$a */
    class C0540a implements C6144ahM<Item> {
        private final /* synthetic */ Item cQM;
        private final /* synthetic */ ListJ fRm;
        private final /* synthetic */ ProjectileWeapon fRn;

        C0540a(ListJ aan, ProjectileWeapon sg, Item auq) {
            this.fRm = aan;
            this.fRn = sg;
            this.cQM = auq;
        }

        /* renamed from: o */
        public void mo1931n(Item auq) {
            SwingUtilities.invokeLater(new C0541a(this.fRm, this.fRn, this.cQM));
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            SwingUtilities.invokeLater(new C0542b(this.fRm));
            th.printStackTrace();
        }

        /* renamed from: a.Ha$a$a */
        class C0541a implements Runnable {
            private final /* synthetic */ Item cQM;
            private final /* synthetic */ ListJ fRm;
            private final /* synthetic */ ProjectileWeapon fRn;

            C0541a(ListJ aan, ProjectileWeapon sg, Item auq) {
                this.fRm = aan;
                this.fRn = sg;
                this.cQM = auq;
            }

            public void run() {
                this.fRm.getParent().getParent().getParent().mo4912Kl();
                this.fRn.ald().getEventManager().mo13975h(new C5966adq(this.fRn, this.cQM));
            }
        }

        /* renamed from: a.Ha$a$b */
        /* compiled from: a */
        class C0542b implements Runnable {
            private final /* synthetic */ ListJ fRm;

            C0542b(ListJ aan) {
                this.fRm = aan;
            }

            public void run() {
                this.fRm.getParent().getParent().getParent().mo4912Kl();
            }
        }
    }
}
