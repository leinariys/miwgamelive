package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.Ox */
/* compiled from: a */
public class C1025Ox {
    public final Vec3f dLU = new Vec3f();
    public final Vec3f dLV = new Vec3f();
    public final Vec3f dLW = new Vec3f();
    public final Vec3f dLX = new Vec3f();
    public final Vec3f dLY = new Vec3f();
    public float dLZ;
    public float dMa;
    public int dMb;
    public int dMc;
    public float dMd;
    public float dMe;
    public float dMf;
    public float dMg;
    public C6226aiq dMh;
    public int dMi;
}
