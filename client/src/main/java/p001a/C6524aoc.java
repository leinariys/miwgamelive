package p001a;

/* renamed from: a.aoc  reason: case insensitive filesystem */
/* compiled from: a */
public class C6524aoc {
    private final int eMp;
    private final String methodName;
    private final String methodSignature;

    public C6524aoc(int i, String str, String str2) {
        this.eMp = i;
        this.methodName = str;
        this.methodSignature = str2;
    }

    public int cmf() {
        return this.eMp;
    }

    public String getMethodName() {
        return this.methodName;
    }

    public String cmg() {
        return this.methodSignature;
    }
}
