package p001a;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.C6753asx;
import util.Maths;

import java.io.Externalizable;

/* renamed from: a.act  reason: case insensitive filesystem */
/* compiled from: a */
public class C5917act implements C3735uM {

    /* renamed from: dP */
    private static final float f4278dP = 0.017453292f;

    /* renamed from: dQ */
    final Vec3f f4279dQ = new Vec3f(1000.0f, 1000.0f, 1000.0f);
    /* renamed from: dS */
    final Vec3f f4281dS = new Vec3f(1000.0f, 1000.0f, 1000.0f);
    /* renamed from: dY */
    final Vec3f f4283dY = new Vec3f(0.0f, 0.0f, -1.0f);
    final Vec3f fbJ = new Vec3f(0.0f, 0.0f, 0.0f);
    final Vec3f fbK = new Vec3f(0.0f, 0.0f, 0.0f);
    /* renamed from: dR */
    float f4280dR = 1000.0f;
    /* renamed from: dT */
    float f4282dT = 1000.0f;
    float fbL = 10.0f;
    float fbM = 1.5f;
    private boolean fbN;
    private C0763Kt stack = C0763Kt.bcE();

    /* renamed from: p */
    public void mo12729p(float f) {
        this.f4279dQ.set(f, f, f);
    }

    /* renamed from: ae */
    public void mo12709ae(Vec3f vec3f) {
        this.f4279dQ.set(vec3f);
    }

    /* renamed from: o */
    public void mo12728o(float f) {
        this.f4280dR = f;
    }

    /* renamed from: as */
    public void mo12712as(Vec3f vec3f) {
        this.fbJ.set(vec3f);
    }

    /* renamed from: at */
    public void mo12713at(Vec3f vec3f) {
        this.fbK.set(vec3f);
        if (vec3f.lengthSquared() > 0.0f) {
            this.f4283dY.set(vec3f);
            this.f4283dY.normalize();
            if (!this.f4283dY.anA()) {
                this.f4283dY.set(0.0f, 0.0f, 0.0f);
                return;
            }
            return;
        }
        this.f4283dY.set(0.0f, 0.0f, 0.0f);
    }

    /* renamed from: q */
    public void mo12731q(float f) {
        this.f4282dT = f;
    }

    /* renamed from: r */
    public void mo12732r(float f) {
        this.f4281dS.set(f, f, f);
    }

    /* renamed from: au */
    public void mo12714au(Vec3f vec3f) {
        this.f4281dS.set(vec3f);
    }

    public float bOL() {
        return this.f4280dR;
    }

    /* renamed from: pR */
    public void mo12730pR(int i) {
        this.f4280dR = (float) i;
    }

    public Vec3f bnO() {
        return this.f4279dQ;
    }

    public Vec3f bOM() {
        return this.fbJ;
    }

    public Vec3f bON() {
        return this.fbK;
    }

    public float bOO() {
        return this.f4282dT;
    }

    public Vec3f bOP() {
        return this.f4281dS;
    }

    public float agd() {
        return this.fbL;
    }

    /* renamed from: iT */
    public void mo12723iT(float f) {
        this.fbL = f;
    }

    public float agf() {
        return this.fbM;
    }

    /* renamed from: iU */
    public void mo12724iU(float f) {
        this.fbM = f;
    }

    /* renamed from: b */
    public float mo12715b(int i, float f, float f2) {
        return m20631a(f, f2, this.fbK.get(i), this.f4282dT, this.f4281dS.get(i));
    }

    private final float max(float f, float f2) {
        return f < f2 ? f2 : f;
    }

    /* access modifiers changed from: package-private */
    public final float min(float f, float f2) {
        return f < f2 ? f : f2;
    }

    /* renamed from: a */
    private float m20631a(float f, float f2, float f3, float f4, float f5) {
        float clamp = Maths.clamp(-f5, f5, f3);
        if (f2 > f5 || f2 < (-f5)) {
            float f6 = f4 * f;
            float clamp2 = Maths.clamp(-f6, f6, clamp - f2) / f;
            float f7 = 10.0f * f6;
            float clamp3 = Maths.clamp(-f7, f7, (Math.signum(f2) * f5) - f2) / f;
            return Math.abs(clamp3) > Math.abs(clamp2) ? clamp3 : clamp2;
        }
        float f8 = f4 * f;
        return Maths.clamp(-f8, f8, clamp - f2) / f;
    }

    /* renamed from: a */
    public void mo3859a(float f, C1003Om om) {
        float f2;
        float f3;
        float f4;
        float f5;
        float f6;
        float f7;
        float f8;
        float f9;
        Vec3f bln = om.bln();
        Vec3f blm = om.blm();
        Vec3f vec3f = this.fbJ;
        Vec3f blp = om.blp();
        Vec3f blo = om.blo();
        float f10 = vec3f.x;
        float f11 = vec3f.y;
        float f12 = vec3f.z;
        if (this.fbM >= 0.0f || (this.fbM < 0.0f && !this.fbN)) {
            f2 = blm.z;
        } else {
            f2 = -blm.length();
        }
        if (!(vec3f.x == 0.0f && vec3f.y == 0.0f && vec3f.z == 0.0f) && (f2 == 0.0f || f2 > (-this.f4281dS.z) * 0.25f)) {
            float abs = (Math.abs(f2 / this.f4281dS.z) + 0.55f) * bnO().x;
            float clamp = Maths.clamp(-abs, abs, vec3f.x);
            float clamp2 = Maths.clamp(-abs, abs, vec3f.y);
            f3 = Maths.clamp(-abs, abs, vec3f.z);
            f4 = clamp2;
            f10 = clamp;
        } else {
            f3 = f12;
            f4 = f11;
        }
        blp.x = m20631a(f, bln.x, f10 * 0.017453292f, this.f4280dR * 0.017453292f, this.f4279dQ.x * 0.017453292f);
        blp.y = m20631a(f, bln.y, f4 * 0.017453292f, this.f4280dR * 0.017453292f, this.f4279dQ.y * 0.017453292f);
        blp.z = m20631a(f, bln.z, f3 * 0.017453292f, this.f4280dR * 0.017453292f, this.f4279dQ.z * 0.017453292f);
        Vec3f vec3f2 = this.fbK;
        float f13 = vec3f2.x;
        float f14 = vec3f2.y;
        float f15 = vec3f2.z;
        Vec3f bOP = bOP();
        float f16 = this.f4282dT;
        if (!this.fbN) {
            float f17 = (bln.x * bln.x) + (bln.y * bln.y);
            if (((double) f17) > 1.0E-6d) {
                float clamp3 = Maths.clamp(0.5f, 1.0f, 1.5f - (((float) Math.sqrt((double) f17)) / (((this.f4279dQ.x + this.f4279dQ.y) * 0.5f) * 0.017453292f)));
                Vec3f vec3f3 = this.f4283dY;
                if (f15 < 0.0f) {
                    f13 = max(f13, bOP.x * clamp3 * vec3f3.x);
                    f14 = max(f14, bOP.y * clamp3 * vec3f3.y);
                    f15 = max(f15, clamp3 * bOP.z * vec3f3.z);
                }
            }
            float f18 = this.fbL;
            if (f15 > 0.0f) {
                f16 = 0.5f * f16;
            }
            if (this.fbN) {
                f5 = this.fbM;
                f6 = f16;
                f7 = f15;
                f8 = f14;
                f9 = f13;
            } else {
                f5 = f18;
                f6 = f16;
                f7 = f15;
                f8 = f14;
                f9 = f13;
            }
        } else {
            float f19 = 0.0f;
            if (this.f4281dS.length() != 0.0f) {
                f19 = (this.fbK.length() / this.f4281dS.length()) * this.f4282dT;
            }
            float f20 = this.f4283dY.x * f19;
            float f21 = this.f4283dY.y * f19;
            float f22 = f19 * this.f4283dY.z;
            float clamp4 = Maths.clamp(-this.f4281dS.x, this.f4281dS.x, (f20 * f) + blm.x);
            float clamp5 = Maths.clamp(-this.f4281dS.y, this.f4281dS.y, (f21 * f) + blm.y);
            float clamp6 = Maths.clamp(-this.f4281dS.z, this.f4281dS.z, (f22 * f) + blm.z);
            float f23 = (clamp4 * clamp4) + (clamp5 * clamp5) + (clamp6 * clamp6);
            float f24 = this.f4281dS.z * this.f4281dS.z;
            if (f23 > f24) {
                float sqrt = (float) Math.sqrt((double) (f24 / f23));
                clamp4 *= sqrt;
                clamp5 *= sqrt;
                clamp6 *= sqrt;
            }
            f5 = 1.0f;
            f6 = f16;
            f7 = clamp6;
            f8 = clamp5;
            f9 = clamp4;
        }
        blo.x = m20631a(f, blm.x, f9, f6 * f5, bOP.x);
        blo.y = m20631a(f, blm.y, f8, f6 * f5, bOP.y);
        blo.z = m20631a(f, blm.z, f7, f6, bOP.z);
        bOQ();
        om.mo4490IQ().orientation.transform(blo, om.blr());
        om.mo4490IQ().orientation.transform(blp, om.bls());
    }

    /* renamed from: a */
    public void mo3861a(Externalizable externalizable) {
        C6753asx asx = (C6753asx) externalizable;
        this.fbN = asx.fbN;
        this.f4279dQ.set(asx.f5293dQ);
        this.f4280dR = asx.f5294dR;
        this.fbJ.set(asx.fbJ);
        this.fbK.set(asx.fbK);
        this.f4282dT = asx.f5296dT;
        this.f4281dS.set(asx.f5295dS);
        this.f4283dY.set(asx.f5297dY);
        bOQ();
    }

    private void bOQ() {
        if (!this.f4279dQ.anA()) {
            this.f4279dQ.set(0.0f, 0.0f, 0.0f);
        }
        if (Float.isInfinite(this.f4280dR) || Float.isNaN(this.f4280dR)) {
            this.f4280dR = 0.0f;
        }
        if (!this.fbJ.anA()) {
            this.fbJ.set(0.0f, 0.0f, 0.0f);
        }
        if (!this.fbK.anA()) {
            this.fbK.set(0.0f, 0.0f, 0.0f);
        }
        if (Float.isInfinite(this.f4282dT) || Float.isNaN(this.f4282dT)) {
            this.f4282dT = 0.0f;
        }
        if (!this.f4281dS.anA()) {
            this.f4281dS.set(0.0f, 0.0f, 0.0f);
        }
        if (!this.f4283dY.anA()) {
            this.f4283dY.set(0.0f, 0.0f, 0.0f);
        }
    }

    /* renamed from: j */
    public Externalizable mo3862j(int i) {
        return new C6753asx(this);
    }

    /* renamed from: m */
    public void mo12725m(float f, float f2, float f3) {
        this.fbK.set(f, f2, f3);
    }

    /* renamed from: n */
    public void mo12727n(float f, float f2, float f3) {
        this.fbJ.set(f, f2, f3);
    }

    public boolean aXW() {
        return this.fbN;
    }

    /* renamed from: ec */
    public void mo12722ec(boolean z) {
        this.fbN = z;
    }
}
