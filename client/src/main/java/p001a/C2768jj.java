package p001a;

import logic.aaa.C5476aKu;

import javax.swing.*;

/* renamed from: a.jj */
/* compiled from: a */
public class C2768jj extends JMenuItem {

    private C6622aqW aiA;
    private C6274ajm aiB;

    public C2768jj(C5476aKu aku) {
        super(aku.getTitle());
        this.aiA = aku.mo9808ER();
        this.aiB = aku.djd();
    }

    public void execute() {
        if (this.aiB != null) {
            this.aiA.mo315a(this.aiB.fQq, this.aiB.object);
        }
    }

    /* renamed from: ER */
    public C6622aqW mo19984ER() {
        return this.aiA;
    }

    /* renamed from: a */
    public boolean mo19985a(C5476aKu aku) {
        if (this.aiB == null || aku == null) {
            return false;
        }
        return this.aiB.mo14175a(aku.djd());
    }
}
