package p001a;

import taikodom.render.graphics2d.C0559Hm;
import util.Syst;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/* renamed from: a.aSj  reason: case insensitive filesystem */
/* compiled from: a */
public class C5673aSj {
    /* renamed from: a */
    public static String m18227a(String str, String str2, File file) {
        Semaphore semaphore = new Semaphore(1);
        String[] strArr = new String[1];
        File file2 = new File(file, String.valueOf(Syst.getMd5(m18230bd(str, str2))) + ".cache");
        try {
            semaphore.acquire(1);
            C1825a aVar = new C1825a(strArr, str, str2, semaphore);
            aVar.setDaemon(true);
            aVar.start();
            if (!semaphore.tryAcquire(5, TimeUnit.SECONDS) || strArr[0] == null) {
                return C0559Hm.m5244a(file2, Charset.forName("utf-8"));
            }
            try {
                file.mkdirs();
                C0559Hm.m5257b(file2, strArr[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return strArr[0];
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* renamed from: S */
    public static String m18226S(String str, String str2) {
        String str3;
        Exception exc;
        if (str.startsWith("http://")) {
            try {
                URLConnection openConnection = new URL(m18230bd(str, str2)).openConnection();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(openConnection.getInputStream()));
                StringBuffer stringBuffer = new StringBuffer();
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    stringBuffer.append(readLine);
                }
                bufferedReader.close();
                String stringBuffer2 = stringBuffer.toString();
                try {
                    switch (((HttpURLConnection) openConnection).getResponseCode()) {
                        case 404:
                        case 503:
                            return null;
                        default:
                            str3 = stringBuffer2;
                            break;
                    }
                } catch (Exception e) {
                    exc = e;
                    str3 = stringBuffer2;
                }
            } catch (Exception e2) {
                exc = e2;
                str3 = null;
            }
        } else {
            str3 = null;
        }
        return str3;
        exc.printStackTrace();
        return str3;
    }

    /* renamed from: bd */
    private static String m18230bd(String str, String str2) {
        new StringBuffer();
        if (str2 == null || str2.length() <= 0) {
            return str;
        }
        return String.valueOf(str) + "?" + str2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0081  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void m18229a(java.io.Reader r5, java.net.URL r6, java.io.Writer r7) {
        /*
            r2 = 0
            java.net.URLConnection r0 = r6.openConnection()     // Catch:{ IOException -> 0x00a8 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x00a8 }
            java.lang.String r1 = "POST"
            r0.setRequestMethod(r1)     // Catch:{ ProtocolException -> 0x0053 }
            r1 = 1
            r0.setDoOutput(r1)     // Catch:{ IOException -> 0x005c, all -> 0x0095 }
            r1 = 1
            r0.setDoInput(r1)     // Catch:{ IOException -> 0x005c, all -> 0x0095 }
            r1 = 0
            r0.setUseCaches(r1)     // Catch:{ IOException -> 0x005c, all -> 0x0095 }
            r1 = 0
            r0.setAllowUserInteraction(r1)     // Catch:{ IOException -> 0x005c, all -> 0x0095 }
            java.lang.String r1 = "Content-type"
            java.lang.String r2 = "text/xml; charset=UTF-8"
            r0.setRequestProperty(r1, r2)     // Catch:{ IOException -> 0x005c, all -> 0x0095 }
            java.io.OutputStream r2 = r0.getOutputStream()     // Catch:{ IOException -> 0x005c, all -> 0x0095 }
            java.io.OutputStreamWriter r1 = new java.io.OutputStreamWriter     // Catch:{ IOException -> 0x0085 }
            java.lang.String r3 = "UTF-8"
            r1.<init>(r2, r3)     // Catch:{ IOException -> 0x0085 }
            m18228a(r5, r1)     // Catch:{ IOException -> 0x0085 }
            r1.close()     // Catch:{ IOException -> 0x0085 }
            if (r2 == 0) goto L_0x0039
            r2.close()     // Catch:{ IOException -> 0x005c, all -> 0x0095 }
        L_0x0039:
            java.io.InputStream r2 = r0.getInputStream()     // Catch:{ IOException -> 0x005c, all -> 0x0095 }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0098 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x0098 }
            m18228a(r1, r7)     // Catch:{ IOException -> 0x0098 }
            r1.close()     // Catch:{ IOException -> 0x0098 }
            if (r2 == 0) goto L_0x004d
            r2.close()     // Catch:{ IOException -> 0x005c, all -> 0x0095 }
        L_0x004d:
            if (r0 == 0) goto L_0x0052
            r0.disconnect()
        L_0x0052:
            return
        L_0x0053:
            r1 = move-exception
            java.lang.Exception r2 = new java.lang.Exception     // Catch:{ IOException -> 0x005c, all -> 0x0095 }
            java.lang.String r3 = "Shouldn't happen: HttpURLConnection doesn't support POST??"
            r2.<init>(r3, r1)     // Catch:{ IOException -> 0x005c, all -> 0x0095 }
            throw r2     // Catch:{ IOException -> 0x005c, all -> 0x0095 }
        L_0x005c:
            r1 = move-exception
            r2 = r0
        L_0x005e:
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ all -> 0x007d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x007d }
            java.lang.String r4 = "Connection error (is server running at "
            r3.<init>(r4)     // Catch:{ all -> 0x007d }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ all -> 0x007d }
            java.lang.String r4 = " ?): "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x007d }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x007d }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x007d }
            r0.<init>(r1)     // Catch:{ all -> 0x007d }
            throw r0     // Catch:{ all -> 0x007d }
        L_0x007d:
            r0 = move-exception
            r1 = r0
        L_0x007f:
            if (r2 == 0) goto L_0x0084
            r2.disconnect()
        L_0x0084:
            throw r1
        L_0x0085:
            r1 = move-exception
            java.lang.Exception r3 = new java.lang.Exception     // Catch:{ all -> 0x008e }
            java.lang.String r4 = "IOException while posting data"
            r3.<init>(r4, r1)     // Catch:{ all -> 0x008e }
            throw r3     // Catch:{ all -> 0x008e }
        L_0x008e:
            r1 = move-exception
            if (r2 == 0) goto L_0x0094
            r2.close()     // Catch:{ IOException -> 0x005c, all -> 0x0095 }
        L_0x0094:
            throw r1     // Catch:{ IOException -> 0x005c, all -> 0x0095 }
        L_0x0095:
            r1 = move-exception
            r2 = r0
            goto L_0x007f
        L_0x0098:
            r1 = move-exception
            java.lang.Exception r3 = new java.lang.Exception     // Catch:{ all -> 0x00a1 }
            java.lang.String r4 = "IOException while reading response"
            r3.<init>(r4, r1)     // Catch:{ all -> 0x00a1 }
            throw r3     // Catch:{ all -> 0x00a1 }
        L_0x00a1:
            r1 = move-exception
            if (r2 == 0) goto L_0x00a7
            r2.close()     // Catch:{ IOException -> 0x005c, all -> 0x0095 }
        L_0x00a7:
            throw r1     // Catch:{ IOException -> 0x005c, all -> 0x0095 }
        L_0x00a8:
            r0 = move-exception
            r1 = r0
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C5673aSj.m18229a(java.io.Reader, java.net.URL, java.io.Writer):void");
    }

    /* renamed from: a */
    private static void m18228a(Reader reader, Writer writer) {
        char[] cArr = new char[1024];
        while (true) {
            int read = reader.read(cArr);
            if (read < 0) {
                writer.flush();
                return;
            }
            writer.write(cArr, 0, read);
        }
    }

    /* renamed from: a.aSj$a */
    class C1825a extends Thread {
        private final /* synthetic */ String[] iMt;
        private final /* synthetic */ String iMu;
        private final /* synthetic */ String iMv;
        private final /* synthetic */ Semaphore iMw;

        C1825a(String[] strArr, String str, String str2, Semaphore semaphore) {
            this.iMt = strArr;
            this.iMu = str;
            this.iMv = str2;
            this.iMw = semaphore;
        }

        public void run() {
            this.iMt[0] = C5673aSj.m18226S(this.iMu, this.iMv);
            this.iMw.release();
        }
    }
}
