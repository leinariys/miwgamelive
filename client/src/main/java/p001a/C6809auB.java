package p001a;

/* renamed from: a.auB  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6809auB {

    /* renamed from: YE */
    float mo16294YE();

    /* renamed from: YG */
    float mo16295YG();

    void abort();

    C1996a acZ();

    void activate();

    String agJ();

    float agz();

    void ahv();

    boolean isDisposed();

    /* renamed from: jt */
    float mo16301jt();

    /* renamed from: a.auB$a */
    public enum C1996a {
        DEACTIVE,
        WARMUP,
        COOLDOWN,
        ACTIVE
    }
}
