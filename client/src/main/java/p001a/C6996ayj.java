package p001a;

import game.geometry.Matrix4fWrap;

/* renamed from: a.ayj  reason: case insensitive filesystem */
/* compiled from: a */
public class C6996ayj extends C6518aoW<Matrix4fWrap> {
    /* renamed from: o */
    public Matrix4fWrap mo17082o(Matrix4fWrap ajk) {
        Matrix4fWrap ajk2 = (Matrix4fWrap) get();
        ajk2.set(ajk);
        return ajk2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: cDu */
    public Matrix4fWrap create() {
        return new Matrix4fWrap();
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public void copy(Matrix4fWrap ajk, Matrix4fWrap ajk2) {
        ajk.set(ajk2);
    }
}
