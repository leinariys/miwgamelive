package p001a;

/* renamed from: a.ayF */
/* compiled from: a */
public final class ayF {
    public static final int T_BOOLEAN = 0;
    public static final int T_BYTE = 1;
    public static final int T_CHAR = 3;
    public static final int T_CLASS = 144;
    public static final int T_DOUBLE = 128;
    public static final int T_FLOAT = 6;
    public static final int T_LONG = 5;
    public static final int T_SHORT = 2;
    public static final int gUA = 129;
    public static final int gUB = 130;
    public static final int gUC = 131;
    public static final int gUD = 133;
    public static final int gUE = 134;
    public static final int gUF = 135;
    public static final int gUG = 145;
    public static final int gUH = 146;
    public static final int gUI = 147;
    public static final int gUJ = 160;
    public static final int gUK = 161;
    public static final int gUL = 162;
    public static final int gUM = 163;
    public static final int gUN = 164;
    public static final int gUO = 165;
    public static final int gUP = 166;
    public static final int gUQ = 169;
    public static final int gUR = 170;
    public static final int gUS = 171;
    public static final int gUT = 177;
    public static final int gUU = 178;
    public static final int gUV = 179;
    public static final int gUW = 180;
    public static final int gUX = 192;
    public static final int gUY = 193;
    public static final int gUZ = 194;
    public static final int gUy = 4;
    public static final int gUz = 7;
    public static final int gVa = 195;
    public static final int gVb = 196;
    public static final int gVc = 196;
    public static final int gVd = 240;
    public static final int gVe = 242;
    public static final int gVf = 243;
    public static final int gVg = 244;
    public static final String[] names = new String[256];

    static {
        names[0] = "boolean";
        names[1] = "byte";
        names[4] = "int";
        names[5] = "long";
        names[2] = "short";
        names[3] = "char";
        names[6] = "float";
        names[128] = "double";
        names[134] = "space";
        names[135] = "space";
        names[161] = "byte-array";
        names[171] = "bytes";
        names[165] = "long-array";
        names[7] = "bit-array";
        names[131] = "utf";
        names[133] = "enum";
        names[145] = "asymmetric-externalizable";
        names[147] = "serializable";
        names[146] = "externalizable";
        names[135] = "space-end";
        names[144] = "class";
        names[129] = "null";
        names[130] = "reference";
        names[240] = "extended";
        for (int i = 0; i < names.length; i++) {
            if (names[i] == null) {
                names[i] = "T_UNDEF_" + Integer.toHexString(i).toUpperCase();
            }
        }
    }
}
