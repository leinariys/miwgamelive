package p001a;

import game.network.ProxyTyp;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.aFt  reason: case insensitive filesystem */
/* compiled from: a */
public @interface C5345aFt {
    ProxyTyp[] cYL();
}
