package p001a;

import game.script.Actor;
import game.script.cloning.CloningDefaults;
import game.script.hangar.Bay;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.space.Gate;
import logic.baa.C6200aiQ;
import logic.data.link.C1092Px;
import logic.data.link.C3981xi;
import logic.res.code.C5663aRz;
import logic.res.code.DataGameEventImpl;
import org.mozilla1.javascript.JavaScriptException;

import java.util.*;

/* renamed from: a.aio  reason: case insensitive filesystem */
/* compiled from: a */
public class C6224aio {

    private final DataGameEventImpl gLk;
    /* access modifiers changed from: private */
    public String iSF;
    /* renamed from: P */
    private Player f4666P;
    private Map<String, Runnable> erj = new HashMap();
    private Ship gYK;

    public C6224aio(DataGameEventImpl arVar, Player aku) {
        this.gLk = arVar;
        this.f4666P = aku;
        aku.mo8320a(C1092Px.gjx, (C6200aiQ<?>) new C1915b(aku));
    }

    public void dwn() {
        Actor bhE = this.f4666P.bhE();
        if ((bhE instanceof Station) && this.f4666P.bQx() == null) {
            log("player docked at: " + ((Station) bhE).getName());
            Iterator it = this.f4666P.mo14346aD((Station) bhE).mo20056KU().iterator();
            while (true) {
                if (it.hasNext()) {
                    Bay ajm = (Bay) it.next();
                    if (ajm.mo9625al() != null) {
                        this.f4666P.mo12634P(ajm.mo9625al());
                        log("ship found: " + ajm.mo9625al().agH().mo19891ke());
                        break;
                    }
                } else {
                    break;
                }
            }
            if (this.f4666P.bQx() == null) {
                log("player has no ship");
            }
        }
    }

    /* access modifiers changed from: protected */
    public void log(String str) {
    }

    public String getName() {
        return this.f4666P.getName();
    }

    public boolean bQB() {
        return this.f4666P.bQB();
    }

    public void ajM() {
        PlayerController dxc = this.f4666P.dxc();
        if (bQB()) {
            dxc.cOp();
        }
        dxc.cOi();
        this.gYK = this.f4666P.bQx();
    }

    /* renamed from: nE */
    public void mo13812nE(String str) {
        try {
            this.gLk.ala().aMa().mo11179k(this.f4666P, str);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public Collection<String> drh() {
        HashSet hashSet = new HashSet();
        for (Gate rF : this.gLk.ala().aMa().drh()) {
            Gate rF2 = rF.mo18386rF();
            if (rF2 != null) {
                hashSet.add(rF2.getName());
            }
        }
        return hashSet;
    }

    /* renamed from: af */
    public void mo13792af(double d) {
        this.f4666P.dxc().mo22148lB((float) d);
    }

    public void setPitch(float f) {
        this.f4666P.dxc().mo22149lD(f);
    }

    public void setYaw(float f) {
        this.f4666P.dxc().mo22151lH(f);
    }

    public void setRoll(float f) {
        this.f4666P.dxc().mo22150lF(f);
    }

    /* renamed from: nF */
    public void mo13813nF(String str) {
    }

    /* renamed from: iV */
    public void mo13808iV(boolean z) {
        Ship bQx = this.f4666P.bQx();
        if (bQx.mo18325k(C3904wY.CANNON)) {
            bQx.mo18250A(C3904wY.CANNON);
        } else {
            bQx.mo18331y(C3904wY.CANNON);
        }
    }

    public boolean cUB() {
        return this.f4666P.bQx().mo18325k(C3904wY.CANNON);
    }

    public void dwo() {
    }

    /* renamed from: a */
    public void mo13791a(String str, Runnable runnable) {
        if (this.erj.put(str, runnable) == null && "died".equals(str)) {
            this.f4666P.bQx().mo8320a(C3981xi.dae, (C6200aiQ<?>) new C1914a(str));
        }
    }

    /* renamed from: gO */
    public Runnable mo13805gO(String str) {
        return this.erj.get(str);
    }

    /* renamed from: gP */
    public void mo13806gP(String str) {
        this.iSF = str;
        throw new JavaScriptException("_x_cont: nop", "<java>", 0);
    }

    public Runnable dwp() {
        if (this.iSF == null) {
            return null;
        }
        Runnable gO = mo13805gO(this.iSF);
        this.iSF = null;
        return gO;
    }

    public boolean dwq() {
        return this.iSF != null;
    }

    public void dwr() {
        this.f4666P.dxc().cNY();
    }

    public boolean isDead() {
        dwn();
        return this.f4666P.bQx() == null || this.f4666P.bQx().isDead();
    }

    public boolean isAlive() {
        return !isDead();
    }

    public void dws() {
        Taikodom ala = this.f4666P.ala();
        CloningDefaults xd = ala.aJe().mo19010xd();
        ala.mo1436b(this.f4666P, xd.bTc(), xd.bTg());
    }

    public void dwt() {
        Taikodom ala = this.f4666P.ala();
        aQF aqf = new aQF(ala.aJe().mo19019xv(), this.f4666P, this.gYK);
        aqf.dqa();
        ala.aJA().mo11895b(this.f4666P, aqf.dpY());
    }

    public boolean dwu() {
        dwn();
        return bQB() && this.f4666P.bQx() != null;
    }

    /* renamed from: a.aio$b */
    /* compiled from: a */
    class C1915b implements C6200aiQ<Player> {
        private final /* synthetic */ Player agw;

        C1915b(Player aku) {
            this.agw = aku;
        }

        /* renamed from: a */
        public void mo1143a(Player aku, C5663aRz arz, Object obj) {
            if ((obj instanceof Station) && this.agw.bQx() == null) {
                C6224aio.this.log("docked at: " + ((Station) obj).getName());
                Iterator it = this.agw.mo14346aD((Station) obj).mo20056KU().iterator();
                while (true) {
                    if (it.hasNext()) {
                        Bay ajm = (Bay) it.next();
                        if (ajm.mo9625al() != null) {
                            this.agw.mo12634P(ajm.mo9625al());
                            C6224aio.this.log("ship found: " + ajm.mo9625al().agH().mo19891ke());
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (this.agw.bQx() == null) {
                    C6224aio.this.log("player has no ship");
                }
            }
        }
    }

    /* renamed from: a.aio$a */
    class C1914a implements C6200aiQ<Ship> {
        private final /* synthetic */ String fMZ;

        C1914a(String str) {
            this.fMZ = str;
        }

        /* renamed from: a */
        public void mo1143a(Ship fAVar, C5663aRz arz, Object obj) {
            if (((Boolean) obj).booleanValue()) {
                C6224aio.this.iSF = this.fMZ;
            }
        }
    }
}
