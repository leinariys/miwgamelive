package p001a;

import game.script.item.ItemLocation;
import logic.ui.C2698il;
import logic.ui.item.Progress;
import org.mozilla1.classfile.C0147Bi;
import taikodom.addon.IAddonProperties;
import taikodom.addon.components.TItemPanel;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/* renamed from: a.agk  reason: case insensitive filesystem */
/* compiled from: a */
public class C6116agk extends C0690Jq {

    /* access modifiers changed from: private */
    public TItemPanel aZK;
    /* renamed from: IU */
    private C3987xk f4530IU = C3987xk.bFK;
    private ItemLocation bLj;
    private Progress hJQ;

    public C6116agk(IAddonProperties vWVar, C2698il ilVar) {
        super(vWVar, ilVar);
        this.djv = ilVar;
        this.hJQ = ilVar.mo4918cg("capacity");
        ilVar.mo4915cd("search-field").addKeyListener(new C1889a(ilVar));
        this.aZK = new TItemPanel();
        ilVar.mo4915cd("itemsPanel").add(this.aZK);
        ilVar.mo4917cf("search-label").setText(String.valueOf(vWVar.translate("Search")) + ": ");
    }

    public void refresh() {
        cZj();
    }

    /* renamed from: Fa */
    public void mo525Fa() {
    }

    /* access modifiers changed from: package-private */
    public void removeListeners() {
    }

    /* access modifiers changed from: protected */
    public void cZj() {
        this.hJQ.mo17398E(this.bLj.mo2696wE());
        this.hJQ.setValue(this.bLj.aRB());
        this.hJQ.setString(String.valueOf(this.f4530IU.mo22980dY(this.bLj.aRB())) + C0147Bi.SEPARATOR + this.f4530IU.mo22980dY(this.bLj.mo2696wE()));
    }

    /* renamed from: b */
    public void mo13457b(ItemLocation aag) {
        this.bLj = aag;
        this.aZK.mo23647b(this.bLj);
    }

    /* renamed from: a.agk$a */
    class C1889a extends KeyAdapter {
        private final /* synthetic */ C2698il fxI;

        C1889a(C2698il ilVar) {
            this.fxI = ilVar;
        }

        public void keyReleased(KeyEvent keyEvent) {
            if (C6116agk.this.aZK != null) {
                C6116agk.this.aZK.bCU().mo2165O(this.fxI.mo4920ci("search-field").getText());
            }
        }
    }
}
