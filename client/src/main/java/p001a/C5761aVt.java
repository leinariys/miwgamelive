package p001a;

import java.io.Serializable;

/* renamed from: a.aVt  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C5761aVt implements Serializable, Cloneable {
    public abstract float cuK();

    /* renamed from: a */
    public int mo11955a(C5761aVt avt) {
        if (cuK() == avt.cuK()) {
            return 0;
        }
        if (cuK() > avt.cuK()) {
            return -1;
        }
        return 1;
    }
}
