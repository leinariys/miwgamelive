package p001a;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.qk */
/* compiled from: a */
public final class C3365qk {
    public static final C3365qk aUJ = new C3365qk("GrannyRenormalizeNormals", grannyJNI.GrannyRenormalizeNormals_get());
    public static final C3365qk aUK = new C3365qk("GrannyReorderTriangleIndices", grannyJNI.GrannyReorderTriangleIndices_get());
    private static C3365qk[] aUL = {aUJ, aUK};

    /* renamed from: pF */
    private static int f8952pF = 0;

    /* renamed from: pG */
    private final int f8953pG;

    /* renamed from: pH */
    private final String f8954pH;

    private C3365qk(String str) {
        this.f8954pH = str;
        int i = f8952pF;
        f8952pF = i + 1;
        this.f8953pG = i;
    }

    private C3365qk(String str, int i) {
        this.f8954pH = str;
        this.f8953pG = i;
        f8952pF = i + 1;
    }

    private C3365qk(String str, C3365qk qkVar) {
        this.f8954pH = str;
        this.f8953pG = qkVar.f8953pG;
        f8952pF = this.f8953pG + 1;
    }

    /* renamed from: dV */
    public static C3365qk m37822dV(int i) {
        if (i < aUL.length && i >= 0 && aUL[i].f8953pG == i) {
            return aUL[i];
        }
        for (int i2 = 0; i2 < aUL.length; i2++) {
            if (aUL[i2].f8953pG == i) {
                return aUL[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C3365qk.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f8953pG;
    }

    public String toString() {
        return this.f8954pH;
    }
}
