package p001a;

import logic.res.code.C6494any;
import logic.res.html.MessageContainer;
import logic.thred.LogPrinter;
import util.Syst;

import java.lang.reflect.Field;
import java.util.List;

/* renamed from: a.Jm */
/* compiled from: a */
public abstract class C0686Jm implements C0478Gd, C6280ajs {
    private static final LogPrinter logger = LogPrinter.m10275K(C0686Jm.class);
    public List<String> djo = new C5595aPj();
    int djs = 0;
    private List<Class<?>> djn = new C5595aPj();
    private boolean djp;
    private C6781atZ djq;
    private boolean djr;
    private String signature;

    public C0686Jm(C6781atZ atz) {
        this.djq = atz;
        this.djr = true;
    }

    /* renamed from: C */
    public static C6494any m6009C(Class<?> cls) {
        try {
            Field declaredField = cls.getDeclaredField("___iScriptClass");
            declaredField.setAccessible(true);
            return (C6494any) declaredField.get((Object) null);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e2) {
            e2.printStackTrace();
        } catch (IllegalArgumentException e3) {
            e3.printStackTrace();
        } catch (IllegalAccessException e4) {
            e4.printStackTrace();
        }
        System.err.println("No IScriptClass found for " + cls);
        return null;
    }

    /* renamed from: A */
    public List<Class<?>> mo3257A(Class<?> cls) {
        aiL();
        List<Class<?>> dgK = MessageContainer.init();
        for (Class next : this.djn) {
            if (next.getSuperclass() == cls || m6010d(next.getInterfaces(), cls)) {
                dgK.add(next);
            }
        }
        return dgK;
    }

    /* renamed from: d */
    private boolean m6010d(Object[] objArr, Object obj) {
        for (Object obj2 : objArr) {
            if (obj2 == obj) {
                return true;
            }
        }
        return false;
    }

    public synchronized void aiL() {
        Class<?> cls;
        if (!this.djp) {
            int i = 0;
            while (i < this.djo.size()) {
                Class<?> cls2 = i < this.djn.size() ? this.djn.get(i) : null;
                if (cls2 == null) {
                    try {
                        cls = Class.forName(this.djo.get(i));
                    } catch (ClassNotFoundException e) {
                        logger.error(e);
                        cls = cls2;
                    } catch (Error e2) {
                        Error error = e2;
                        System.err.println("Problems reading class " + this.djo.get(i) + ":");
                        error.printStackTrace();
                        throw error;
                    }
                    this.djn.set(i, cls);
                }
                if (this.djq != null) {
                    this.djq.mo16097ag(i, this.djo.size());
                }
                i++;
            }
            this.djp = true;
        }
    }

    public String getSignature() {
        aiL();
        if (this.signature != null) {
            return this.signature;
        }
        StringBuilder sb = new StringBuilder();
        for (Class<?> annotation : this.djn) {
            sb.append(((C6050afW) annotation.getAnnotation(C6050afW.class)).aul());
        }
        String w = Syst.getMd5(sb.toString());
        this.signature = w;
        return w;
    }

    public List<Class<?>> aXU() {
        aiL();
        return this.djn;
    }

    public List<String> aXV() {
        return this.djo;
    }

    /* renamed from: es */
    public void mo2380es(String str) {
        if (this.djo.contains(str)) {
            this.djs++;
            if (this.djq != null) {
                this.djq.mo16097ag(this.djs, this.djo.size());
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: eD */
    public void mo3262eD(String str) {
        this.djo.add(str);
    }

    /* renamed from: B */
    public C6494any mo3258B(Class<?> cls) {
        return m6009C(cls);
    }
}
