package p001a;

import logic.thred.C6339akz;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.BS */
/* compiled from: a */
public class C0114BS extends AbstractList<C6339akz> {
    List<C6339akz> aWo = new ArrayList();
    private int size;

    /* renamed from: gy */
    public C6339akz get(int i) {
        if (i < this.size) {
            return this.aWo.get(i);
        }
        throw new ArrayIndexOutOfBoundsException();
    }

    public C6339akz aBa() {
        int i = this.size;
        this.size++;
        if (this.size > this.aWo.size()) {
            this.aWo.add(new C6339akz());
        }
        return this.aWo.get(i);
    }

    public void clear() {
        this.size = 0;
    }

    public int size() {
        return this.size;
    }
}
