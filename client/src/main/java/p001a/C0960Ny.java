package p001a;

import javax.swing.*;
import javax.swing.tree.TreePath;

/* renamed from: a.Ny */
/* compiled from: a */
public abstract class C0960Ny extends C0596IQ {
    private final JTree tree;

    public C0960Ny(JTree jTree) {
        this.tree = jTree;
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public abstract JPopupMenu mo4315c(TreePath treePath);

    /* access modifiers changed from: protected */
    /* renamed from: H */
    public void mo2812H(int i, int i2) {
        TreePath pathForLocation = this.tree.getPathForLocation(i, i2);
        m7796a(pathForLocation);
        JPopupMenu b = m7797b(pathForLocation);
        if (b != null) {
            b.show(this.tree, i, i2);
        }
    }

    /* renamed from: a */
    private void m7796a(TreePath treePath) {
        boolean z = false;
        TreePath[] selectionPaths = this.tree.getSelectionPaths();
        if (treePath != null && selectionPaths != null) {
            int i = 0;
            while (true) {
                if (i >= selectionPaths.length) {
                    break;
                } else if (treePath.equals(selectionPaths[i])) {
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
        }
        if (z) {
            return;
        }
        if (treePath != null) {
            this.tree.setSelectionPath(treePath);
        } else {
            this.tree.clearSelection();
        }
    }

    /* renamed from: b */
    private JPopupMenu m7797b(TreePath treePath) {
        JPopupMenu bje;
        if (treePath != null) {
            bje = mo4315c(treePath);
        } else {
            bje = bje();
        }
        if (bje == null || bje.getComponentCount() < 0) {
            return null;
        }
        bje.invalidate();
        bje.pack();
        return bje;
    }

    /* access modifiers changed from: protected */
    public JPopupMenu bje() {
        return null;
    }
}
