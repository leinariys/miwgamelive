package p001a;

import logic.swing.C3940wz;
import logic.ui.Panel;
import taikodom.addon.IAddonProperties;
import taikodom.addon.tooltip.TooltipAddon;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.KR */
/* compiled from: a */
public class C0725KR extends C3940wz {

    /* renamed from: DV */
    private Panel f958DV;
    private JLabel drv;
    private JTextArea drw;
    private JLabel drx;

    /* renamed from: kj */
    private IAddonProperties f959kj;

    public C0725KR(IAddonProperties vWVar) {
        this.f959kj = vWVar;
        this.f958DV = (Panel) vWVar.bHv().mo16789b(TooltipAddon.class, "windowTooltip.xml");
        m6379iM();
    }

    /* renamed from: iM */
    private void m6379iM() {
        this.drv = this.f958DV.mo4917cf("windowName");
        this.drw = this.f958DV.mo4915cd("windowDescription");
        this.drx = this.f958DV.mo4917cf("shortcut");
    }

    /* renamed from: b */
    public Component mo2675b(Component component) {
        Object obj;
        String str = null;
        if (component instanceof JToggleButton) {
            obj = ((JToggleButton) component).getClientProperty("command");
        } else if (component instanceof JLabel) {
            obj = ((JLabel) component).getClientProperty("command");
        } else {
            obj = null;
        }
        if (!(obj instanceof C6622aqW)) {
            return null;
        }
        C6622aqW aqw = (C6622aqW) obj;
        this.drv.setText(aqw.crH());
        String crK = aqw.crK();
        if (crK != null) {
            this.drw.setVisible(true);
            this.drw.setText(crK);
        } else {
            this.drw.setVisible(false);
        }
        String crL = aqw.crL();
        if (crL != null && !crL.isEmpty()) {
            str = this.f959kj.mo2324af(crL);
        }
        if (str == null || str.isEmpty()) {
            this.drx.setVisible(false);
        } else {
            this.drx.setVisible(true);
            this.drx.setText("<" + str.replace('_', '+') + ">");
        }
        this.f958DV.pack();
        return this.f958DV;
    }
}
