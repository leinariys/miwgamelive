package p001a;

import game.script.Taikodom;
import game.script.item.EnergyWeapon;
import game.script.item.EnergyWeaponType;
import game.script.item.Weapon;
import game.script.item.WeaponType;
import game.script.main.bot.BotScript;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.ship.ShipType;
import game.script.ship.Station;
import logic.EngineGame;

import java.util.Iterator;

/* renamed from: a.Sv */
/* compiled from: a */
public class C1285Sv extends ayA {
    private static final long eek = 45000;
    private static final long eel = 120000;
    private static final long eem = 120000;
    private static final long een = 500;
    final C1277Ss eet;
    final ayA[] eeu;
    /* renamed from: P */
    private final Player f1578P;
    /* renamed from: vS */
    private final EngineGame f1581vS;
    ayA eeo;
    C2675iR eep = new C2675iR(this);
    C6563apP eeq = new C6563apP(this);
    ayA eer = new C5250aCc(this);
    C7002ayp ees = new C7002ayp(this);
    String eev;
    /* renamed from: iH */
    Station f1579iH;
    /* renamed from: nz */
    BotScript f1580nz;
    private Ship coW;
    private EnergyWeapon eew;
    private boolean initialized = false;

    public C1285Sv(EngineGame aou, Player aku, C1288c cVar, BotScript ati, String str) {
        super((C1285Sv) null);
        this.eet = new C1277Ss(this, str);
        this.eeu = new ayA[]{this.eep, this.eeq, this.eer, this.ees, this.eet};
        this.f1581vS = aou;
        this.f1578P = aku;
        this.f1580nz = ati;
        ati.mo16132p(aku);
        if (cVar == C1288c.MINER) {
            this.eeo = new C6697art(this);
        } else if (cVar == C1288c.PVP) {
            this.eeo = new C5603aPr(this);
        } else if (cVar == C1288c.MISSION) {
            this.eeo = new C0881Mm(this);
        }
        restart();
        this.eeo.restart();
    }

    public static long btK() {
        return 120000 + ((long) ((int) (120000.0d * Math.random())));
    }

    public static long btL() {
        return een + ((long) ((int) (500.0d * Math.random())));
    }

    public static long btM() {
        return eek + ((long) ((int) (45000.0d * Math.random())));
    }

    public static long btN() {
        return 120000 + ((long) ((int) (120000.0d * Math.random())));
    }

    public void aGu() {
        if (!this.initialized) {
            btD();
            this.initialized = true;
        }
        this.eeo.mo16926gZ();
        for (ayA gZ : this.eeu) {
            gZ.mo16926gZ();
        }
    }

    private void btD() {
        if (this.f1579iH == null) {
            this.f1579iH = (Station) this.f1578P.bhE();
        }
        if (this.f1579iH == null) {
            throw new IllegalStateException("Station should not be null.");
        }
        this.f1580nz.mo16127h(this.f1579iH);
        this.eev = this.f1580nz.mo16109ap(this.f1579iH);
        btE();
        C6749ast.cun().mo15995a(this);
    }

    public void btE() {
        ShipType ng;
        log("Creating a new ship!");
        Iterator<ShipType> it = this.f1581vS.getTaikodom().aKC().iterator();
        while (true) {
            if (it.hasNext()) {
                ng = it.next();
                if (ng.getHandle().indexOf("bullf") >= 0) {
                    break;
                }
            } else {
                ng = null;
                break;
            }
        }
        if (ng == null) {
            throw new IllegalStateException("Could not find bullfrog");
        }
        Ship y = this.f1580nz.mo16134y(ng);
        this.coW = y;
        this.f1578P.mo12634P(y);
        this.eew = m9628K(this.coW);
    }

    /* renamed from: dL */
    public Player mo5503dL() {
        return this.f1578P;
    }

    public EnergyWeapon btF() {
        return this.eew;
    }

    /* renamed from: al */
    public Ship mo5495al() {
        return this.coW;
    }

    /* renamed from: eT */
    public Station mo5504eT() {
        return this.f1579iH;
    }

    public C2675iR btG() {
        return this.eep;
    }

    public C6563apP btH() {
        return this.eeq;
    }

    public ayA btI() {
        return this.eer;
    }

    public BotScript btJ() {
        return this.f1580nz;
    }

    public Taikodom ala() {
        return this.f1581vS.getTaikodom();
    }

    /* renamed from: K */
    private EnergyWeapon m9628K(Ship fAVar) {
        EnergyWeapon yGVar = null;
        Iterator<WeaponType> it = this.f1581vS.getTaikodom().mo1545uZ().iterator();
        while (true) {
            if (it.hasNext()) {
                WeaponType next = it.next();
                if ((next instanceof EnergyWeaponType) && next.mo19891ke().get().contains("asic")) {
                    yGVar = btJ().mo16133v(next);
                    break;
                }
            } else {
                break;
            }
        }
        if (yGVar == null) {
            throw new RuntimeException("no 'asic' energy weapons available.");
        }
        try {
            btJ().mo16114b(fAVar, (Weapon) yGVar);
        } catch (C2293dh e) {
            e.printStackTrace();
        }
        return yGVar;
    }

    /* renamed from: P */
    private void m9629P(Station bf) {
    }

    /* renamed from: a.Sv$c */
    /* compiled from: a */
    public enum C1288c {
        MINER,
        PVP,
        MISSION
    }

    /* renamed from: a.Sv$b */
    /* compiled from: a */
    public enum C1287b {
        None(0),
        Chat(1),
        Fire(2);

        public final int modifier;

        private C1287b(int i) {
            this.modifier = i;
        }
    }

    /* renamed from: a.Sv$a */
    public enum C1286a {
        LongFlight(3),
        ShortFlight(6),
        Docked(1),
        Minning(4),
        PVPFight(4),
        NPCFightMoving(12),
        NPCFightStopped(4),
        Waiting(1);

        public final int ffE;

        private C1286a(int i) {
            this.ffE = i;
        }
    }
}
