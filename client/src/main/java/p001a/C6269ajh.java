package p001a;

import org.mozilla1.javascript.NativeJavaPackage;

/* renamed from: a.ajh  reason: case insensitive filesystem */
/* compiled from: a */
public class C6269ajh extends org.mozilla1.javascript.NativeJavaPackage implements org.mozilla1.javascript.IdFunctionCall, org.mozilla1.javascript.Function {
    static final long serialVersionUID = -1455787259477709999L;
    private static final Object cHe = new Integer(17);
    private static final String fOX = "java.lang;java.lang.reflect;java.io;java.math;java.net;java.util;java.util.zip;java.text;java.text.resources;java.applet;javax.swing;";
    private static final int fOY = 1;

    C6269ajh(ClassLoader classLoader) {
        super(true, "", classLoader);
    }

    public static void init(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, boolean z) {
        C6269ajh ajh = new C6269ajh(lhVar.getApplicationClassLoader());
        ajh.setPrototype(m23593v(avf));
        ajh.setParentScope(avf);
        String[] semicolonSplit = org.mozilla1.javascript.Kit.semicolonSplit(fOX);
        for (int i = 0; i != semicolonSplit.length; i++) {
            ajh.mo23162a(semicolonSplit[i], avf);
        }
        org.mozilla1.javascript.IdFunctionObject yy = new org.mozilla1.javascript.IdFunctionObject(ajh, cHe, 1, "getClass", 1, avf);
        String[] strArr = {"java", "javax", "org", "com", "edu", "net"};
        org.mozilla1.javascript.NativeJavaPackage[] yhVarArr = new org.mozilla1.javascript.NativeJavaPackage[strArr.length];
        for (int i2 = 0; i2 < strArr.length; i2++) {
            yhVarArr[i2] = (NativeJavaPackage) ajh.get(strArr[i2], (org.mozilla1.javascript.Scriptable) ajh);
        }
        org.mozilla1.javascript.ScriptableObject akn = (org.mozilla1.javascript.ScriptableObject) avf;
        if (z) {
            yy.sealObject();
        }
        yy.exportAsScopeProperty();
        akn.defineProperty("Packages", (Object) ajh, 2);
        for (int i3 = 0; i3 < strArr.length; i3++) {
            akn.defineProperty(strArr[i3], (Object) yhVarArr[i3], 2);
        }
    }

    public Object call(org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        return construct(lhVar, avf, objArr);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.mozilla1.javascript.Scriptable construct(org.mozilla1.javascript.Context r5, org.mozilla1.javascript.Scriptable r6, java.lang.Object[] r7) {
        /*
            r4 = this;
            r1 = 0
            int r0 = r7.length
            if (r0 == 0) goto L_0x002a
            r0 = 0
            r0 = r7[r0]
            boolean r2 = r0 instanceof Wrapper
            if (r2 == 0) goto L_0x0011
            a.aEm r0 = (Wrapper) r0
            java.lang.Object r0 = r0.unwrap()
        L_0x0011:
            boolean r2 = r0 instanceof java.lang.ClassLoader
            if (r2 == 0) goto L_0x002a
            java.lang.ClassLoader r0 = (java.lang.ClassLoader) r0
            r2 = r0
        L_0x0018:
            if (r2 != 0) goto L_0x0021
            java.lang.String r0 = "msg.not.classloader"
            p001a.C2909lh.m35244gE(r0)
            r0 = r1
        L_0x0020:
            return r0
        L_0x0021:
            a.yh r0 = new a.yh
            r1 = 1
            java.lang.String r3 = ""
            r0.<init>(r1, r3, r2)
            goto L_0x0020
        L_0x002a:
            r2 = r1
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6269ajh.construct(a.lh, a.aVF, java.lang.Object[]):a.aVF");
    }

    public Object execIdCall(org.mozilla1.javascript.IdFunctionObject yy, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        if (yy.hasTag(cHe) && yy.methodId() == 1) {
            return m23025d(lhVar, avf, objArr);
        }
        throw yy.unknown();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v1, resolved type: a.ajh} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v13, resolved type: a.aVF} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: a.ajh} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.mozilla1.javascript.Scriptable m23025d(org.mozilla1.javascript.Context r6, org.mozilla1.javascript.Scriptable r7, java.lang.Object[] r8) {
        /*
            r5 = this;
            r4 = -1
            r1 = 0
            int r0 = r8.length
            if (r0 <= 0) goto L_0x002f
            r0 = r8[r1]
            boolean r0 = r0 instanceof Wrapper
            if (r0 == 0) goto L_0x002f
            r0 = r8[r1]
            a.aEm r0 = (Wrapper) r0
            java.lang.Object r0 = r0.unwrap()
            java.lang.Class r0 = r0.getClass()
            java.lang.String r2 = r0.getName()
        L_0x001b:
            r0 = 46
            int r3 = r2.indexOf(r0, r1)
            if (r3 != r4) goto L_0x0036
            java.lang.String r0 = r2.substring(r1)
        L_0x0027:
            java.lang.Object r0 = r5.get((java.lang.String) r0, (p001a.aVF) r5)
            boolean r1 = r0 instanceof p001a.aVF
            if (r1 != 0) goto L_0x003b
        L_0x002f:
            java.lang.String r0 = "msg.not.java.obj"
            a.TJ r0 = p001a.C2909lh.m35244gE(r0)
            throw r0
        L_0x0036:
            java.lang.String r0 = r2.substring(r1, r3)
            goto L_0x0027
        L_0x003b:
            a.aVF r0 = (p001a.aVF) r0
            if (r3 != r4) goto L_0x0040
            return r0
        L_0x0040:
            int r1 = r3 + 1
            r5 = r0
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6269ajh.m23025d(a.lh, a.aVF, java.lang.Object[]):a.aVF");
    }
}
