package p001a;

import game.script.mission.Mission;
import taikodom.infra.script.I18NString;

import java.util.List;

/* renamed from: a.yZ */
/* compiled from: a */
public interface C4045yZ {

    I18NString aqH();

    I18NString aqI();

    boolean aqJ();

    float aqK();

    long aqL();

    List<ItemTypeTableItem> aqM();

    List<ItemTypeTableItem> aqN();

    String aqO();

    String aqP();

    C4046a aqQ();

    int aqR();

    boolean aqS();

    String getHandle();

    /* renamed from: iS */
    Mission mo1716iS();

    /* renamed from: lZ */
    boolean mo709lZ();

    /* renamed from: lt */
    int mo710lt();

    void push();

    /* renamed from: rP */
    I18NString mo711rP();

    /* renamed from: vW */
    I18NString mo713vW();

    /* renamed from: a.yZ$a */
    public enum C4046a {
        COMBAT,
        TRADE,
        NEUTRAL
    }
}
