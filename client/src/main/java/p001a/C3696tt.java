package p001a;

import java.util.Comparator;
import java.util.List;

/* renamed from: a.tt */
/* compiled from: a */
public class C3696tt {
    /* renamed from: i */
    public static int m39767i(List<?> list) {
        return m39766eF(list.size());
    }

    /* renamed from: eF */
    public static int m39766eF(int i) {
        int i2 = 2;
        while (i2 < i) {
            i2 <<= 1;
        }
        return i2;
    }

    /* renamed from: a */
    public static <T> void m39763a(List<T> list, int i, T t) {
        while (list.size() <= i) {
            list.add(t);
        }
    }

    /* renamed from: a */
    public static void m39760a(C5283aDj adj, int i, int i2) {
        while (adj.size() < i) {
            adj.add(i2);
        }
        while (adj.size() > i) {
            adj.remove(adj.size() - 1);
        }
    }

    /* renamed from: a */
    public static void m39759a(C1220S s, int i, float f) {
        while (s.size() < i) {
            s.add(f);
        }
        while (s.size() > i) {
            s.remove(s.size() - 1);
        }
    }

    /* renamed from: a */
    public static <T> void m39762a(List<T> list, int i, Class<T> cls) {
        while (list.size() < i) {
            try {
                list.add(cls != null ? cls.newInstance() : null);
            } catch (IllegalAccessException e) {
                throw new IllegalStateException(e);
            } catch (InstantiationException e2) {
                throw new IllegalStateException(e2);
            }
        }
        while (list.size() > i) {
            list.remove(list.size() - 1);
        }
    }

    /* renamed from: a */
    public static <T> int m39758a(T[] tArr, T t) {
        for (int i = 0; i < tArr.length; i++) {
            if (tArr[i] == t) {
                return i;
            }
        }
        return -1;
    }

    /* renamed from: c */
    public static float m39765c(float f, float f2, float f3) {
        if (f < f2) {
            return f2;
        }
        return f3 < f ? f3 : f;
    }

    /* renamed from: a */
    private static <T> void m39761a(List<T> list, int i, int i2, Comparator<T> comparator) {
        T t = list.get(i - 1);
        while (i <= i2 / 2) {
            int i3 = i * 2;
            if (i3 < i2 && comparator.compare(list.get(i3 - 1), list.get(i3)) < 0) {
                i3++;
            }
            if (comparator.compare(t, list.get(i3 - 1)) >= 0) {
                break;
            }
            list.set(i - 1, list.get(i3 - 1));
            i = i3;
        }
        list.set(i - 1, t);
    }

    /* renamed from: a */
    public static <T> void m39764a(List<T> list, Comparator<T> comparator) {
        int size = list.size();
        for (int i = size / 2; i > 0; i--) {
            m39761a(list, i, size, comparator);
        }
        while (size >= 1) {
            swap(list, 0, size - 1);
            size--;
            m39761a(list, 1, size, comparator);
        }
    }

    private static <T> void swap(List<T> list, int i, int i2) {
        T t = list.get(i);
        list.set(i, list.get(i2));
        list.set(i2, t);
    }
}
