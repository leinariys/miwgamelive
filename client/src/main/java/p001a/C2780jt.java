package p001a;

import taikodom.infra.script.I18NString;

/* renamed from: a.jt */
/* compiled from: a */
public interface C2780jt {
    String getHandle();

    String getInfo();

    boolean isHidden();

    /* renamed from: vW */
    I18NString mo4247vW();
}
