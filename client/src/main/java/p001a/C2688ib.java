package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.script.ship.Ship;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.camera.Camera;

/* renamed from: a.ib */
/* compiled from: a */
public class C2688ib extends C6258ajW {
    private static final Log logger = LogPrinter.setClass(C2688ib.class);
    /* renamed from: WN */
    private final Vec3d f8206WN = new Vec3d(ScriptRuntime.NaN, 8.0d, 20.0d);
    /* renamed from: WO */
    private final Vec3d f8207WO = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, 50.0d);
    /* renamed from: WP */
    private final Vec3d f8208WP = new Vec3d(ScriptRuntime.NaN, 5.0d, -20.0d);
    /* renamed from: WL */
    private float f8204WL = 0.7f;
    /* renamed from: WM */
    private float f8205WM = 0.25f;
    /* renamed from: WQ */
    private float f8209WQ = 800.0f;

    /* renamed from: WR */
    private boolean f8210WR = false;

    /* renamed from: lU */
    private Ship f8211lU;
    private C0763Kt stack = C0763Kt.bcE();

    public C2688ib(Camera camera) {
        super(camera);
    }

    /* renamed from: a */
    public void mo3859a(float f, C1003Om om) {
        this.stack = this.stack.bcD();
        this.stack.bcI().push();
        this.stack.bcH().push();
        try {
            if (this.f8211lU != null) {
                if (this.f8211lU.ait() != null && this.f8211lU.ait().lengthSquared() > ScriptRuntime.NaN) {
                    this.f8206WN.mo9484aA(this.f8211lU.ait());
                }
                if (this.f8211lU.aix() != null && this.f8211lU.aix().lengthSquared() > ScriptRuntime.NaN) {
                    this.f8207WO.mo9484aA(this.f8211lU.aix());
                }
                if (this.f8211lU.aiv() != null && this.f8211lU.aiv().lengthSquared() > ScriptRuntime.NaN) {
                    this.f8208WP.mo9484aA(this.f8211lU.aiv());
                }
                this.f8204WL = this.f8211lU.mo18252Ag();
                this.f8205WM = this.f8211lU.mo18251Ae();
            }
            Vec3d ajr = (Vec3d) this.stack.bcI().get();
            Vec3d ajr2 = (Vec3d) this.stack.bcI().get();
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            Vec3d ajr3 = (Vec3d) this.stack.bcI().get();
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            ((Vec3f) this.stack.bcH().get()).set(this.fSF.mo2472kQ().blq());
            ajr3.mo9497bG(this.fSF.mo2472kQ().bln());
            float length = this.fSF.mo2472kQ().blm().length() / this.fSB;
            float length2 = (float) (((double) this.fSF.mo2472kQ().bln().length()) - (((double) (Math.abs(this.fSF.mo2472kQ().bln().z) * 180.0f)) / (3.141592653589793d * ((double) this.fSC))));
            Vec3d ajr4 = (Vec3d) this.stack.bcI().get();
            ajr4.set(ScriptRuntime.NaN, ScriptRuntime.NaN, (double) (-this.f8209WQ));
            ajr2.scaleAdd((double) (1.0f - length2), this.f8208WP, ajr4);
            ajr2.y *= (double) this.f8204WL;
            this.fSF.mo2472kQ().mo4490IQ().mo17331a(ajr2);
            om.mo4490IQ().getZ(vec3f2);
            om.mo4490IQ().getY(vec3f3);
            vec3f4.set(0.0f, 0.0f, -1.0f);
            this.fSF.mo2472kQ().bll().mo15204F(vec3f4, vec3f4);
            ajr2.sub(om.mo4490IQ().position);
            ajr2.negate();
            vec3f.set(ajr2);
            vec3f.normalize();
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            this.fSF.mo2472kQ().mo4490IQ().getY(vec3f5);
            float dot = vec3f5.dot(vec3f3);
            Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
            if (dot < 0.0f) {
                if (dot == -1.0f) {
                    vec3f6.set(vec3f2);
                } else {
                    vec3f6.cross(vec3f3, vec3f5);
                }
                vec3f6.normalize();
                vec3f6.scale(1.0f - dot);
            } else {
                vec3f6.cross(vec3f3, vec3f5);
            }
            vec3f6.scale((Math.max(vec3f5.dot(vec3f2), 0.0f) * 10.0f) + 1.0f);
            om.blt().cross(vec3f2, vec3f);
            om.blt().scale(10.0f);
            om.blt().add(vec3f6);
            Vec3d ajr5 = (Vec3d) this.stack.bcI().get();
            ajr5.cross(this.f8207WO, ajr3);
            ajr5.scale(this.f8207WO.z / ((double) this.fSC));
            if (ajr5.y > ScriptRuntime.NaN) {
                ajr5.y *= (double) this.f8205WM;
            }
            float f2 = 1.0f;
            if (this.f8210WR) {
                f2 = 2.0f;
            }
            ajr5.z += this.f8206WN.z * ((double) f2);
            ajr5.y += this.f8206WN.y * ((double) f2);
            ajr5.x = (((double) f2) * this.f8206WN.x) + ajr5.x;
            ajr5.y *= (double) this.f8204WL;
            this.fSF.mo2472kQ().mo4490IQ().mo17331a(ajr5);
            ajr.sub(ajr5, om.mo4490IQ().position);
            ajr.scale((double) (this.fSB * 0.05f * (1.0f + (0.1f * length))));
            om.blq().set(ajr);
            if (this.camera != null) {
                this.camera.setFovY((((this.fSD * (1.0f - length)) + (this.fSE * length)) * 0.1f) + (this.camera.getFovY() * 0.9f));
            }
            float determinant = this.camera.getTransform().orientation.determinant();
            if (Float.isNaN(determinant) || Float.isInfinite(determinant)) {
                logger.warn("Camera controller problem. Reseting the camera.");
                om.blt().set(0.0f, 0.0f, 0.0f);
                om.blq().set(0.0f, 0.0f, 0.0f);
                om.mo4490IQ().mo17344b(this.fSF.mo2439IQ());
            }
            double length3 = this.camera.getTransform().position.length();
            if (Double.isNaN(length3) || Double.isInfinite(length3)) {
                logger.warn("Camera controller problem. Reseting the camera.");
                om.mo4490IQ().mo17344b(this.fSF.mo2439IQ());
            }
        } finally {
            this.stack.bcI().pop();
            this.stack.bcH().pop();
        }
    }

    /* renamed from: Ad */
    public float mo19641Ad() {
        return this.f8209WQ;
    }

    /* renamed from: aD */
    public void mo19646aD(float f) {
        this.f8209WQ = f;
    }

    /* renamed from: Ae */
    public float mo19642Ae() {
        return this.f8205WM;
    }

    /* renamed from: aE */
    public void mo19647aE(float f) {
        this.f8205WM = f;
    }

    /* renamed from: i */
    public void mo19650i(Vec3d ajr) {
        this.f8206WN.mo9484aA(ajr);
    }

    /* renamed from: j */
    public void mo19651j(Vec3d ajr) {
        this.f8208WP.mo9484aA(ajr);
    }

    /* renamed from: k */
    public void mo19652k(Vec3d ajr) {
        this.f8207WO.mo9484aA(ajr);
    }

    /* renamed from: Af */
    public Ship mo19643Af() {
        return this.f8211lU;
    }

    /* renamed from: f */
    public void mo19649f(Ship fAVar) {
        this.f8211lU = fAVar;
    }

    /* renamed from: Ag */
    public float mo19644Ag() {
        return this.f8204WL;
    }

    /* renamed from: aF */
    public void mo19648aF(float f) {
        this.f8204WL = f;
    }

    /* renamed from: W */
    public void mo19645W(boolean z) {
        this.f8210WR = z;
    }
}
