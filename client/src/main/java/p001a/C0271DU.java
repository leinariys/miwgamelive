package p001a;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Deprecated
@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.DU */
/* compiled from: a */
public @interface C0271DU {
    C6293akF aNs();

    String aNt();

    C1371Tx aNu();

    String aNv();

    boolean aNw();

    boolean aNx();

    boolean aNy();

    String aNz() default "";
}
