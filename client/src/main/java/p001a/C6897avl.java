package p001a;

import logic.res.html.DataClassSerializer;

import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Date;

/* renamed from: a.avl  reason: case insensitive filesystem */
/* compiled from: a */
public class C6897avl extends DataClassSerializer {
    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) {
        if (objectInput.readBoolean()) {
            return new Date(objectInput.readLong());
        }
        return null;
    }

    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) {
        if (obj == null) {
            objectOutput.writeBoolean(false);
            return;
        }
        objectOutput.writeBoolean(true);
        objectOutput.writeLong(((Date) obj).getTime());
    }
}
