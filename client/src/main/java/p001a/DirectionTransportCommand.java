package p001a;

import game.network.channel.client.ClientConnect;
import game.network.message.TransportCommand;

/* renamed from: a.aCh  reason: case insensitive filesystem */
/* compiled from: a */
public class DirectionTransportCommand {
    TransportCommand command;
    ClientConnect client;

    /* renamed from: h */
    public void setDirectionTransportCommand(ClientConnect bVar, TransportCommand transportCommand) {
        this.client = bVar;
        this.command = transportCommand;
    }

    public void setNullClientConnect() {
        this.client = null;
    }
}
