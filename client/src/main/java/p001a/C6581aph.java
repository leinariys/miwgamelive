package p001a;

import game.network.message.externalizable.ObjectId;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* renamed from: a.aph  reason: case insensitive filesystem */
/* compiled from: a */
public class C6581aph implements C2913lj {
    private ConcurrentMap<ObjectId, C3582se> exE = new ConcurrentHashMap();

    /* renamed from: a */
    public void mo15460a(C3582se seVar) {
        if (this.exE.putIfAbsent(seVar.getObjectId(), seVar) != null) {
            throw new RuntimeException("Given OID is already in use: " + seVar.getObjectId());
        }
    }

    /* renamed from: a */
    public C3582se mo15459a(ObjectId apo) {
        return (C3582se) this.exE.get(apo);
    }

    /* renamed from: b */
    public boolean mo15462b(ObjectId apo) {
        return this.exE.containsKey(apo);
    }

    /* renamed from: b */
    public void mo15461b(C3582se seVar) {
        this.exE.remove(seVar.getObjectId());
    }

    public int size() {
        return this.exE.size();
    }

    public Iterator<C3582se> iterator() {
        return this.exE.values().iterator();
    }

    /* renamed from: im */
    public boolean mo15465im(long j) {
        return false;
    }

    public void dispose() {
    }

    /* renamed from: c */
    public boolean mo15463c(C3582se seVar) {
        if (this.exE.putIfAbsent(seVar.getObjectId(), seVar) != null) {
            return false;
        }
        return true;
    }
}
