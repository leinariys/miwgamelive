package p001a;

import javax.swing.*;
import java.util.List;

/* renamed from: a.dn */
/* compiled from: a */
public class C2302dn<T> extends AbstractListModel {

    /* renamed from: yZ */
    private List<T> f6621yZ;

    public C2302dn(List<T> list) {
        this.f6621yZ = list;
    }

    public Object getElementAt(int i) {
        return this.f6621yZ.get(i);
    }

    public int getSize() {
        if (this.f6621yZ == null) {
            return 0;
        }
        return this.f6621yZ.size();
    }
}
