package p001a;

import logic.data.mbean.C0677Jd;

/* renamed from: a.vM */
/* compiled from: a */
public class C3814vM {
    private static final boolean bzQ = true;
    private static final boolean bzR = false;
    C5397aHt bzW;
    C5397aHt bzX;
    C5397aHt bzY;
    int size;
    private int bzS = C0677Jd.dij;
    private C5397aHt bzT;
    private int bzU;
    private C5524aMq bzV = new C5524aMq(1024);
    private aWq bzZ;

    public C3814vM() {
    }

    public C3814vM(aWq awq) {
        this.bzZ = awq;
    }

    /* renamed from: e */
    private static boolean m40187e(C5397aHt aht) {
        if (aht == null) {
            return true;
        }
        return aht.hXm;
    }

    /* renamed from: f */
    private static C5397aHt m40188f(C5397aHt aht) {
        if (aht == null) {
            return null;
        }
        return aht.hXi;
    }

    /* renamed from: a */
    private static void m40183a(C5397aHt aht, boolean z) {
        if (aht != null) {
            aht.hXm = z;
        }
    }

    /* renamed from: g */
    private static C5397aHt m40189g(C5397aHt aht) {
        if (aht == null) {
            return null;
        }
        return aht.hXj;
    }

    /* renamed from: h */
    private static C5397aHt m40190h(C5397aHt aht) {
        if (aht == null) {
            return null;
        }
        return aht.hXk;
    }

    public void clear() {
        int i = this.bzS;
        int i2 = this.bzU;
        if (i2 < i) {
            C5397aHt aht = this.bzT;
            C5397aHt aht2 = this.bzX;
            C5397aHt aht3 = aht;
            while (i2 < i && aht2 != null) {
                C5397aHt aht4 = aht2.hXl;
                if (aht2.hXn) {
                    aht2.hXh.reset();
                    this.bzV.put(aht2.ehD.mo6901yn().getClass(), aht2.hXh);
                }
                aht2.reset();
                aht2.hXl = aht3;
                i2++;
                aht3 = aht2;
                aht2 = aht4;
            }
            if (aht2 != null) {
                while (aht2 != null) {
                    if (aht2.hXn) {
                        aht2.hXh.reset();
                        this.bzV.put(aht2.ehD.mo6901yn().getClass(), aht2.hXh);
                    }
                    aht2.reset();
                    aht2 = aht2.hXl;
                }
            }
            this.bzU = i2;
            this.bzT = aht3;
        } else {
            for (C5397aHt aht5 = this.bzX; aht5 != null; aht5 = aht5.hXl) {
                aht5.reset();
            }
        }
        this.bzY = null;
        this.bzX = null;
        this.bzW = null;
        this.size = 0;
    }

    /* renamed from: a */
    public void mo22566a(C5397aHt aht) {
        C5397aHt aht2;
        char c;
        long j = aht.agR;
        C5397aHt aht3 = this.bzW;
        if (aht3 == null) {
            this.bzW = aht;
            this.bzY = aht;
            this.bzX = aht;
            this.size = 1;
            return;
        }
        while (true) {
            if (j < aht3.agR) {
                aht2 = aht3.hXj;
                c = 65535;
            } else {
                aht2 = aht3.hXk;
                c = 1;
            }
            if (aht2 == null) {
                break;
            }
            aht3 = aht2;
        }
        aht.hXi = aht3;
        if (c < 0) {
            aht3.hXj = aht;
        } else {
            aht3.hXk = aht;
        }
        m40184b(aht);
        if (this.bzY != null) {
            this.bzY.hXl = aht;
            this.bzY = aht;
        } else {
            this.bzY = aht;
            this.bzX = aht;
            aht.hXl = null;
        }
        this.size++;
    }

    public void akX() {
        C5397aHt aht;
        C5397aHt aht2 = this.bzW;
        while (aht2.hXj != null) {
            aht2 = aht2.hXj;
        }
        this.bzX = aht2;
        int i = 0;
        C5397aHt aht3 = aht2;
        for (C5397aHt aht4 = aht2; aht4 != null; aht4 = aht) {
            if (aht4.hXk != null) {
                aht = aht4.hXk;
                while (aht.hXj != null) {
                    aht = aht.hXj;
                }
            } else {
                C5397aHt aht5 = aht4.hXi;
                C5397aHt aht6 = aht4;
                while (aht != null && aht6 == aht.hXk) {
                    aht6 = aht;
                    aht5 = aht.hXi;
                }
            }
            aht4.hXl = aht;
            i++;
            aht3 = aht4;
        }
        this.bzY = aht3;
        this.bzY.hXl = null;
    }

    /* renamed from: a */
    public C5397aHt mo22565a(long j, C1875af afVar) {
        C5397aHt aht = this.bzW;
        C5397aHt aht2 = aht;
        C5397aHt aht3 = aht;
        while (aht2 != null) {
            long j2 = aht2.agR;
            if (j < j2) {
                aht3 = aht2;
                aht2 = aht2.hXj;
            } else if (j <= j2) {
                return aht2;
            } else {
                aht3 = aht2;
                aht2 = aht2.hXk;
            }
        }
        if (afVar != null && afVar.mo10528du()) {
            return null;
        }
        C5397aHt b = mo22570b(j, afVar);
        if (aht3 == null) {
            this.bzW = b;
            this.bzY = b;
            this.bzX = b;
        } else {
            if (j < aht3.agR) {
                aht3.hXj = b;
            } else {
                aht3.hXk = b;
            }
            b.hXi = aht3;
            this.bzY.hXl = b;
            this.bzY = b;
            m40184b(b);
        }
        this.size++;
        return b;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public C5397aHt mo22570b(long j, C1875af afVar) {
        C5397aHt aht;
        if (afVar == null) {
            throw new C6411amT();
        }
        if (this.bzU > 0) {
            C5397aHt aht2 = this.bzT;
            C5397aHt aht3 = aht2.hXl;
            this.bzT = aht3;
            if (aht3 == null) {
                this.bzU = 0;
                aht = aht2;
            } else {
                this.bzU--;
                aht = aht2;
            }
        } else {
            aht = new C5397aHt();
        }
        C0677Jd jd = (C0677Jd) this.bzV.mo10176aN(afVar.mo6901yn().getClass());
        if (jd == null) {
            jd = (C0677Jd) afVar.mo6901yn().mo13W();
        } else {
            jd.mo3200g(afVar.mo6901yn());
        }
        jd.mo3155a(this.bzZ, (C3582se) afVar);
        aht.mo9270a(this.bzZ, afVar, jd);
        return aht;
    }

    /* renamed from: b */
    private void m40184b(C5397aHt aht) {
        aht.hXm = false;
        C5397aHt aht2 = aht;
        while (aht2 != null && aht2 != this.bzW && !aht2.hXi.hXm) {
            if (m40188f(aht2) == m40189g(m40188f(m40188f(aht2)))) {
                C5397aHt h = m40190h(m40188f(m40188f(aht2)));
                if (!m40187e(h)) {
                    m40183a(m40188f(aht2), true);
                    m40183a(h, true);
                    m40183a(m40188f(m40188f(aht2)), false);
                    aht2 = m40188f(m40188f(aht2));
                } else {
                    if (aht2 == m40190h(m40188f(aht2))) {
                        aht2 = m40188f(aht2);
                        m40185c(aht2);
                    }
                    m40183a(m40188f(aht2), true);
                    m40183a(m40188f(m40188f(aht2)), false);
                    m40186d(m40188f(m40188f(aht2)));
                }
            } else {
                C5397aHt g = m40189g(m40188f(m40188f(aht2)));
                if (!m40187e(g)) {
                    m40183a(m40188f(aht2), true);
                    m40183a(g, true);
                    m40183a(m40188f(m40188f(aht2)), false);
                    aht2 = m40188f(m40188f(aht2));
                } else {
                    if (aht2 == m40189g(m40188f(aht2))) {
                        aht2 = m40188f(aht2);
                        m40186d(aht2);
                    }
                    m40183a(m40188f(aht2), true);
                    m40183a(m40188f(m40188f(aht2)), false);
                    m40185c(m40188f(m40188f(aht2)));
                }
            }
        }
        this.bzW.hXm = true;
    }

    /* renamed from: c */
    private void m40185c(C5397aHt aht) {
        if (aht != null) {
            C5397aHt aht2 = aht.hXk;
            aht.hXk = aht2.hXj;
            if (aht2.hXj != null) {
                aht2.hXj.hXi = aht;
            }
            aht2.hXi = aht.hXi;
            if (aht.hXi == null) {
                this.bzW = aht2;
            } else if (aht.hXi.hXj == aht) {
                aht.hXi.hXj = aht2;
            } else {
                aht.hXi.hXk = aht2;
            }
            aht2.hXj = aht;
            aht.hXi = aht2;
        }
    }

    /* renamed from: d */
    private void m40186d(C5397aHt aht) {
        if (aht != null) {
            C5397aHt aht2 = aht.hXj;
            aht.hXj = aht2.hXk;
            if (aht2.hXk != null) {
                aht2.hXk.hXi = aht;
            }
            aht2.hXi = aht.hXi;
            if (aht.hXi == null) {
                this.bzW = aht2;
            } else if (aht.hXi.hXk == aht) {
                aht.hXi.hXk = aht2;
            } else {
                aht.hXi.hXj = aht2;
            }
            aht2.hXk = aht;
            aht.hXi = aht2;
        }
    }

    public C5397aHt akY() {
        return this.bzX;
    }

    public C5397aHt akZ() {
        return this.bzY;
    }

    public int size() {
        return this.size;
    }
}
