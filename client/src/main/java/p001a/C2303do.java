package p001a;

import taikodom.render.graphics2d.C0559Hm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;

/* renamed from: a.do */
/* compiled from: a */
public class C2303do {

    /* renamed from: a */
    public C2305b mo17918a(File file, C2305b bVar) {
        m29301a(bVar, file.getAbsolutePath(), file);
        return bVar;
    }

    /* renamed from: a */
    private void m29301a(C2305b bVar, String str, File file) {
        try {
            for (File b : C0559Hm.m5256b(file, (FilenameFilter) new C2304a())) {
                m29302b(bVar, str, b);
            }
        } catch (FileNotFoundException e) {
        }
    }

    /* renamed from: b */
    private void m29302b(C2305b bVar, String str, File file) {
        String replace = file.getAbsolutePath().substring(str.length() + 1).replace('\\', '.');
        try {
            bVar.mo17920ag(Class.forName(replace.substring(0, replace.length() - 6), false, getClass().getClassLoader()));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /* renamed from: a.do$b */
    /* compiled from: a */
    public interface C2305b {
        /* renamed from: ag */
        void mo17920ag(Class<?> cls);
    }

    /* renamed from: a.do$a */
    class C2304a implements FilenameFilter {
        C2304a() {
        }

        public boolean accept(File file, String str) {
            return str.endsWith(".class");
        }
    }
}
