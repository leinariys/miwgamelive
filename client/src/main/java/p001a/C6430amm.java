package p001a;

import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: a.amm  reason: case insensitive filesystem */
/* compiled from: a */
public class C6430amm<K, V> extends LinkedHashMap<K, V> {

    private final int fZX;
    private long fZY = 0;
    private long fZZ = 0;

    public C6430amm(int i) {
        super(i, 0.75f, true);
        this.fZX = i;
    }

    /* access modifiers changed from: protected */
    public boolean removeEldestEntry(Map.Entry<K, V> entry) {
        return size() > this.fZX;
    }

    public V get(Object obj) {
        V v = super.get(obj);
        this.fZY++;
        if (v != null) {
            this.fZZ++;
        }
        return v;
    }

    public int ciL() {
        return this.fZX;
    }

    public long ciM() {
        return this.fZY;
    }

    public long ciN() {
        return this.fZZ;
    }
}
