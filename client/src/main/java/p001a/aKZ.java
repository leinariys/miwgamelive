package p001a;

import java.util.IdentityHashMap;
import java.util.Map;

/* renamed from: a.aKZ */
/* compiled from: a */
public class aKZ {

    /* renamed from: Xa */
    Map<Object, String[]> f3250Xa;
    Map<String, Object> ijV;
    C2045bV ijW;

    public aKZ(int i) {
        this.ijV = new IdentityHashMap();
        this.f3250Xa = new IdentityHashMap();
        this.ijW = new C2045bV((long) i);
    }

    public aKZ() {
        this.ijV = new IdentityHashMap();
        this.f3250Xa = new IdentityHashMap();
        this.ijW = new C2045bV(1);
    }

    /* renamed from: j */
    public String mo9753j(Object obj, String str) {
        String aL = mo9747aL(obj);
        if (aL != null) {
            return aL;
        }
        String next = this.ijW.next();
        this.f3250Xa.put(obj, new String[]{next, str});
        this.ijV.put(next, obj);
        return next;
    }

    /* renamed from: aL */
    public String mo9747aL(Object obj) {
        String[] strArr = this.f3250Xa.get(obj);
        if (strArr == null || strArr.length != 2) {
            return null;
        }
        return strArr[0];
    }

    /* renamed from: aM */
    public String mo9748aM(Object obj) {
        String[] strArr = this.f3250Xa.get(obj);
        if (strArr == null || strArr.length != 2) {
            return null;
        }
        return strArr[1];
    }

    /* renamed from: aN */
    public boolean mo9749aN(Object obj) {
        return this.f3250Xa.containsKey(obj);
    }

    /* renamed from: aO */
    public Object mo9750aO(Object obj) {
        String[] remove = this.f3250Xa.remove(obj);
        if (remove != null && remove.length > 0) {
            this.ijV.remove(remove[0]);
        }
        return remove;
    }

    public int dgR() {
        return this.f3250Xa.size();
    }

    public void clear() {
        this.f3250Xa.clear();
        this.ijV.clear();
    }

    /* renamed from: nb */
    public Object mo9754nb(String str) {
        return this.ijV.get(str);
    }
}
