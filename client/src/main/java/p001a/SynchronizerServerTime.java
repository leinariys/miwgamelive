package p001a;

import org.mozilla1.javascript.ScriptRuntime;

/* renamed from: a.Hr */
/* compiled from: a */
public class SynchronizerServerTime implements CurrentTimeMilli {
    private final CurrentTimeMilli cVg;
    private double dbw;
    private long offset;
    private boolean stopped;

    public SynchronizerServerTime(CurrentTimeMilli ahw) {
        this.cVg = ahw;
    }

    public SynchronizerServerTime() {
        this(SingletonCurrentTimeMilliImpl.CURRENT_TIME_MILLI);
    }

    public long currentTimeMillis() {
        if (this.stopped) {
            return this.offset;
        }
        if (this.dbw == ScriptRuntime.NaN || this.dbw == 1.0d) {
            return this.cVg.currentTimeMillis() + this.offset;
        }
        return ((long) (this.dbw * ((double) this.cVg.currentTimeMillis()))) + this.offset;
    }

    public long getOffset() {
        return this.offset;
    }

    public void setOffset(long j) {
        this.offset = j;
    }

    /* renamed from: eO */
    public void mo2677eO(long j) {
        this.offset += j;
    }

    /* renamed from: eP */
    public void mo2678eP(long j) {
        this.offset = j - currentTimeMillis();
    }

    public boolean isStopped() {
        return this.stopped;
    }

    /* renamed from: cN */
    public void mo2676cN(boolean z) {
        this.stopped = z;
    }

    public float getFactor() {
        return (float) this.dbw;
    }

    public void setFactor(float f) {
        this.dbw = (double) f;
    }

    /* renamed from: z */
    public void mo2684z(double d) {
        this.dbw = d;
    }
}
