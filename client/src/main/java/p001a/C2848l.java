package p001a;

/* renamed from: a.l */
/* compiled from: a */
public class C2848l {
    String name;
    String prefix;

    public C2848l(String str, String str2) {
        this.prefix = str;
        this.name = str2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String toString() {
        return String.valueOf(this.prefix) + " " + this.name;
    }
}
