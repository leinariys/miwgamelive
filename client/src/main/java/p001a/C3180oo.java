package p001a;

/* renamed from: a.oo */
/* compiled from: a */
public class C3180oo {
    /* access modifiers changed from: private */
    public final C1123QW<aUJ> aOk = C0762Ks.m6640D(aUJ.class);
    private final C1026Oy.C1028b<aUJ, aUJ> aOl = C1026Oy.diz();
    private C6892avg aOm;
    private C3181a aOn = new C3181a(this, (C3181a) null);

    /* renamed from: a */
    public aUJ mo21059a(C0783LL ll, C0783LL ll2) {
        C0128Bb.dMS++;
        if (!mo21066b(ll, ll2)) {
            return null;
        }
        aUJ auj = this.aOk.get();
        auj.mo11609g(ll, ll2);
        aUJ auj2 = this.aOl.get(auj);
        if (auj2 != null) {
            this.aOk.release(auj);
            return auj2;
        }
        this.aOl.put(auj, auj);
        return auj;
    }

    /* renamed from: a */
    public Object mo21060a(C0783LL ll, C0783LL ll2, C1701ZB zb) {
        C0128Bb.dMR++;
        aUJ auj = this.aOk.get();
        auj.mo11609g(ll, ll2);
        aUJ remove = this.aOl.remove(auj);
        this.aOk.release(auj);
        if (remove == null) {
            return null;
        }
        mo21062a(remove, zb);
        this.aOk.release(remove);
        return remove.iXC;
    }

    /* renamed from: b */
    public boolean mo21066b(C0783LL ll, C0783LL ll2) {
        boolean z;
        if (this.aOm != null) {
            return this.aOm.mo16580f(ll, ll2);
        }
        if ((ll.dxz & ll2.dxA) != 0) {
            z = true;
        } else {
            z = false;
        }
        if (!z || (ll2.dxz & ll.dxA) == 0) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public void mo21063a(C6133ahB ahb, C1701ZB zb) {
        this.aOn.gsR = ahb;
        this.aOn.f8784Uy = zb;
        this.aOl.mo4580b(this.aOn);
    }

    /* renamed from: a */
    public void mo21061a(C0783LL ll, C1701ZB zb) {
        mo21063a((C6133ahB) new C3183c(ll), zb);
    }

    /* renamed from: b */
    public void mo21065b(C0783LL ll, C1701ZB zb) {
        mo21063a((C6133ahB) new C3182b(ll, this, zb), zb);
    }

    /* renamed from: SX */
    public C1026Oy.C1028b<aUJ, aUJ> mo21056SX() {
        return this.aOl;
    }

    /* renamed from: a */
    public void mo21062a(aUJ auj, C1701ZB zb) {
        if (auj.iXB != null) {
            auj.iXB.destroy();
            auj.iXB = null;
        }
    }

    /* renamed from: c */
    public aUJ mo21067c(C0783LL ll, C0783LL ll2) {
        C0128Bb.dMT++;
        aUJ auj = this.aOk.get();
        auj.mo11609g(ll, ll2);
        aUJ auj2 = this.aOl.get(auj);
        this.aOk.release(auj);
        return auj2;
    }

    public int getCount() {
        return this.aOl.size();
    }

    /* renamed from: SY */
    public C6892avg mo21057SY() {
        return this.aOm;
    }

    /* renamed from: a */
    public void mo21064a(C6892avg avg) {
        this.aOm = avg;
    }

    /* renamed from: SZ */
    public int mo21058SZ() {
        return this.aOl.size();
    }

    /* renamed from: a.oo$c */
    /* compiled from: a */
    private static class C3183c implements C6133ahB {
        private C0783LL iWI;

        public C3183c(C0783LL ll) {
            this.iWI = ll;
        }

        /* renamed from: a */
        public boolean mo597a(aUJ auj) {
            return auj.iXz == this.iWI || auj.iXA == this.iWI;
        }
    }

    /* renamed from: a.oo$b */
    /* compiled from: a */
    private static class C3182b implements C6133ahB {

        /* renamed from: Uy */
        private C1701ZB f8785Uy;
        private C3180oo hdS;
        private C0783LL hfr;

        public C3182b(C0783LL ll, C3180oo ooVar, C1701ZB zb) {
            this.hfr = ll;
            this.hdS = ooVar;
            this.f8785Uy = zb;
        }

        /* renamed from: a */
        public boolean mo597a(aUJ auj) {
            if (auj.iXz != this.hfr && auj.iXA != this.hfr) {
                return false;
            }
            this.hdS.mo21062a(auj, this.f8785Uy);
            return false;
        }
    }

    /* renamed from: a.oo$a */
    private class C3181a implements C1026Oy.C1027a<aUJ> {

        /* renamed from: Uy */
        public C1701ZB f8784Uy;
        public C6133ahB gsR;

        private C3181a() {
        }

        /* synthetic */ C3181a(C3180oo ooVar, C3181a aVar) {
            this();
        }

        /* renamed from: b */
        public boolean execute(aUJ auj) {
            if (!this.gsR.mo597a(auj)) {
                return true;
            }
            C3180oo.this.mo21062a(auj, this.f8784Uy);
            C0128Bb.dMR++;
            C0128Bb.dMQ--;
            C3180oo.this.aOk.release(auj);
            return false;
        }
    }
}
