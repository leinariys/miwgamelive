package p001a;

import game.network.aQD;
import org.apache.commons.jxpath.JXPathContext;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: a.auQ  reason: case insensitive filesystem */
/* compiled from: a */
public class C6824auQ extends aQD {
    /* renamed from: a */
    public String mo2073a(Iterator<?> it, List<String> list, Map<String, String> map) {
        boolean z;
        StringBuilder sb = new StringBuilder();
        sb.append("\n<table border='1' width='98%'>\n<thead>\n\t<tr>");
        for (int i = 0; i < list.size(); i++) {
            sb.append("<td><b>").append(map.get(list.get(i))).append("</b></td>");
        }
        sb.append("</tr>\n</thead>\n<tbody>");
        while (it.hasNext()) {
            sb.append("\n\t<tr>");
            JXPathContext newContext = JXPathContext.newContext(it.next());
            newContext.setFunctions(this.iEM);
            for (int i2 = 0; i2 < list.size(); i2++) {
                List selectNodes = newContext.selectNodes(list.get(i2));
                sb.append("<td>\n<pre>");
                String str = "";
                if (i2 == list.size() - 1) {
                    z = true;
                } else {
                    z = false;
                }
                for (Object next : selectNodes) {
                    if (next == null) {
                        next = "";
                    }
                    if (z) {
                        sb.append(str).append("<a href='javascript:call(").append(next.toString()).append(")'>").append(next.toString()).append("</a>");
                    } else {
                        sb.append(str).append(next.toString());
                    }
                    str = "\n";
                }
                sb.append("</pre>\n</td>");
            }
            sb.append("</tr>");
        }
        sb.append("\n</tbody>\n</table>");
        return sb.toString();
    }
}
