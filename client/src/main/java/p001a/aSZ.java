package p001a;

import game.network.message.externalizable.C6869avJ;
import logic.ui.item.TaskPane;

/* renamed from: a.aSZ */
/* compiled from: a */
public class aSZ extends C6869avJ {
    private final int cQJ;
    private final aGC hax;
    private final TaskPane iOf;
    private final int order;

    public aSZ() {
        this.iOf = null;
        this.hax = null;
        this.order = 0;
        this.cQJ = 0;
    }

    public aSZ(TaskPane bpVar, aGC agc, int i, int i2) {
        this.iOf = bpVar;
        this.hax = agc;
        this.order = i;
        this.cQJ = i2;
    }

    public TaskPane aMO() {
        return this.iOf;
    }

    /* renamed from: qd */
    public aGC mo11389qd() {
        return this.hax;
    }

    public int getOrder() {
        return this.order;
    }

    public int aMP() {
        return this.cQJ;
    }
}
