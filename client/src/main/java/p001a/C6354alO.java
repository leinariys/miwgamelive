package p001a;

import game.network.manager.C6546aoy;

@C6546aoy
/* renamed from: a.alO  reason: case insensitive filesystem */
/* compiled from: a */
public class C6354alO extends Exception {
    public C6354alO() {
    }

    public C6354alO(String str, Throwable th) {
        super(str, th);
    }

    public C6354alO(String str) {
        super(str);
    }

    public C6354alO(Throwable th) {
        super(th);
    }
}
