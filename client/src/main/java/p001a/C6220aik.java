package p001a;

import com.steadystate.css.parser.CSSOMParser;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.Node;
import org.w3c.dom.css.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/* renamed from: a.aik  reason: case insensitive filesystem */
/* compiled from: a */
public class C6220aik {
    private CSSOMParser fMV;
    private InputSource fMW;

    public C6220aik(Reader reader, Node node, String str, String str2, String str3) {
        this.fMV = null;
        this.fMW = null;
        this.fMV = CSSOMParser.getInstance();
        this.fMW = new InputSource(reader);
    }

    public C6220aik(InputStream inputStream, Node node, String str, String str2, String str3) {
        this((Reader) new InputStreamReader(inputStream), node, str, str2, str3);
    }

    public C6220aik(Reader reader) {
        this(reader, (Node) null, (String) null, (String) null, (String) null);
    }

    public C6220aik(InputStream inputStream) {
        this(inputStream, (Node) null, (String) null, (String) null, (String) null);
    }

    public CSSStyleSheet cbx() {
        try {
            return this.fMV.parseStyleSheet(this.fMW);
        } catch (IOException e) {
            return null;
        }
    }

    public CSSRuleList cby() {
        return null;
    }

    public CSSCharsetRule cbz() {
        return null;
    }

    public CSSUnknownRule cbA() {
        return null;
    }

    public CSSImportRule cbB() {
        return null;
    }

    public CSSMediaRule cbC() {
        return null;
    }

    public CSSPageRule cbD() {
        return null;
    }

    public CSSFontFaceRule cbE() {
        return null;
    }

    public CSSStyleRule cbF() {
        return null;
    }

    public CSSStyleDeclaration cbG() {
        try {
            return this.fMV.parseStyleDeclaration(this.fMW);
        } catch (IOException e) {
            return null;
        }
    }

    public CSSValue cbH() {
        try {
            return this.fMV.parsePropertyValue(this.fMW);
        } catch (IOException e) {
            return null;
        }
    }
}
