package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Vec3d;
import logic.bbb.*;
import logic.thred.C3253pf;
import logic.thred.C6339akz;
import logic.thred.C6615aqP;

import javax.vecmath.Matrix3f;
import java.lang.reflect.Array;

/* renamed from: a.awj  reason: case insensitive filesystem */
/* compiled from: a */
public class C6944awj implements C1037PD {
    private boolean[][] gLO = ((boolean[][]) Array.newInstance(Boolean.TYPE, new int[]{256, 256}));
    private int[][] gLP = ((int[][]) Array.newInstance(Integer.TYPE, new int[]{256, 256}));
    private aVJ[][] gLQ;
    private int gLR = 32;
    private C0763Kt stack = C0763Kt.bcE();

    public C6944awj() {
        C2051bb bbVar = new C2051bb(new C0327ES(), new C5360aGi());
        C1688Ys ys = new C1688Ys();
        this.gLQ = (aVJ[][]) Array.newInstance(aVJ.class, new int[]{this.gLR, this.gLR});
        this.gLQ[0][0] = bbVar;
        this.gLQ[0][1] = bbVar;
        this.gLQ[0][3] = bbVar;
        this.gLQ[0][6] = new aTH();
        this.gLQ[0][8] = bbVar;
        this.gLQ[0][4] = ys;
        this.gLQ[1][1] = bbVar;
        this.gLQ[1][6] = new C5871abz();
        this.gLQ[1][8] = bbVar;
        this.gLQ[1][4] = ys;
        this.gLQ[3][3] = bbVar;
        this.gLQ[8][8] = bbVar;
        this.gLQ[8][0] = bbVar;
        this.gLQ[8][1] = bbVar;
        this.gLQ[8][6] = bbVar;
        this.gLQ[8][4] = ys;
        this.gLQ[4][0] = ys;
        this.gLQ[4][1] = ys;
        this.gLQ[4][8] = ys;
        this.gLQ[4][6] = ys;
        for (int i = 0; i < this.gLR; i++) {
            for (int i2 = 0; i2 < this.gLR; i2++) {
                if (this.gLQ[i][i2] != null && this.gLQ[i2][i] == null) {
                    this.gLQ[i2][i] = new C3768un(this.gLQ[i][i2]);
                }
            }
        }
        for (int i3 = 0; i3 < this.gLR; i3++) {
            this.gLQ[7][i3] = new C2105by(this);
            this.gLQ[i3][7] = new C2105by(this);
        }
        for (int i4 = 0; i4 < 256; i4++) {
            for (int i5 = 0; i5 < 256; i5++) {
                this.gLO[i4][i5] = true;
            }
        }
        for (int i6 = 0; i6 < 256; i6++) {
            for (int i7 = 0; i7 < 256; i7++) {
                this.gLP[i6][i7] = Integer.MIN_VALUE;
            }
        }
        mo16723w(0, 0, 1);
        mo16723w(0, 1, 1);
        mo16723w(1, 1, 1);
    }

    /* renamed from: b */
    public void mo4618b(int i, int i2, boolean z) {
        boolean[] zArr = this.gLO[i];
        this.gLO[i2][i] = z;
        zArr[i2] = z;
    }

    /* renamed from: v */
    public void mo16722v(int i, int i2, int i3) {
        this.gLP[i][i2] = i3;
    }

    /* renamed from: w */
    public void mo16723w(int i, int i2, int i3) {
        int[] iArr = this.gLP[i];
        this.gLP[i2][i] = i3;
        iArr[i2] = i3;
    }

    /* renamed from: a */
    public C6268ajg mo4614a(C3102np npVar) {
        C5223aBb abb;
        this.stack = this.stack.bcD();
        this.stack.bcH().push();
        try {
            C6615aqP dhC = npVar.mo14894Qg().dhC();
            C6615aqP dhC2 = npVar.mo14895Qh().dhC();
            if (npVar.mo14896Qi() instanceof C5223aBb) {
                C5223aBb abb2 = (C5223aBb) npVar.mo14896Qi();
                abb2.mo7954jL(abb2.mo7941Ef());
                abb2.reset();
                abb = abb2;
            } else {
                C5223aBb abb3 = new C5223aBb(dhC, dhC2);
                npVar.mo14893C(abb3);
                abb3.mo7954jL(false);
                abb = abb3;
            }
            ((Vec3f) this.stack.bcH().get()).set(abb.mo7940Ee());
            abb.mo7940Ee().mo9484aA(dhC.getPosition());
            mo4615a(dhC, dhC2, abb);
            for (C6339akz next : abb.ccT()) {
                next.fTK.add(dhC.getPosition());
                abb.mo7942UV().mo2439IQ().mo17333a(next.fTK, next.fTI);
                abb.mo7943UW().mo2439IQ().mo17333a(next.fTK, next.fTJ);
            }
            return abb;
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a */
    public boolean mo1932a(C6615aqP aqp, C6615aqP aqp2) {
        if (aqp == aqp2 || !this.gLO[aqp.mo2438IM() & 255][aqp2.mo2438IM() & 255]) {
            return false;
        }
        if (!aqp.isStatic() || !aqp2.isStatic()) {
            return true;
        }
        return false;
    }

    /* renamed from: ai */
    public boolean mo16720ai(int i, int i2) {
        return this.gLO[i][i2];
    }

    /* renamed from: b */
    public int mo4617b(C6615aqP aqp, C6615aqP aqp2) {
        return this.gLP[aqp.mo2438IM() & 255][aqp2.mo2438IM() & 255];
    }

    /* renamed from: aj */
    public int mo16721aj(int i, int i2) {
        return this.gLP[i & 255][i2 & 255];
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public void mo4615a(C6615aqP aqp, C6615aqP aqp2, C2678iT iTVar) {
        Vec3d position = aqp.getPosition();
        Vec3d position2 = aqp2.getPosition();
        this.stack = this.stack.bcD();
        C3253pf.m37268a(aqp);
        C3253pf.m37269b(aqp2);
        this.stack.bcF();
        try {
            Matrix4fWrap ajk = (Matrix4fWrap) this.stack.bcL().get();
            Matrix4fWrap ajk2 = (Matrix4fWrap) this.stack.bcL().get();
            ajk.mo13985a((Matrix3f) aqp.mo2439IQ().orientation);
            ajk2.mo13985a((Matrix3f) aqp2.mo2439IQ().orientation);
            Vec3d Ee = iTVar.mo7940Ee();
            ajk.mo14049p((float) (position.x - Ee.x), (float) (position.y - Ee.y), (float) (position.z - Ee.z));
            ajk2.mo14049p((float) (position2.x - Ee.x), (float) (position2.y - Ee.y), (float) (position2.z - Ee.z));
            mo4616a(ajk, aqp.mo2437IL(), ajk2, aqp2.mo2437IL(), iTVar);
            this.stack.bcG();
            C3253pf.m37268a((C6615aqP) null);
            C3253pf.m37269b((C6615aqP) null);
        } catch (Throwable th) {
            this.stack.bcG();
            throw th;
        }
    }

    /* renamed from: a */
    public boolean mo4616a(Matrix4fWrap ajk, C4029yK yKVar, Matrix4fWrap ajk2, C4029yK yKVar2, C2678iT iTVar) {
        aVJ avj = this.gLQ[yKVar.getShapeType() & 255][yKVar2.getShapeType() & 255];
        if (avj != null) {
            return avj.mo7270a(ajk, yKVar, ajk2, yKVar2, iTVar);
        }
        System.out.println("[ERROR] Can´t find an algorithm to collide " + yKVar + " and " + yKVar2);
        return false;
    }
}
