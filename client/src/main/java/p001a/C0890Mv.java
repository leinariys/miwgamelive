package p001a;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.Mv */
/* compiled from: a */
public final class C0890Mv {
    public static final C0890Mv dBk = new C0890Mv("GrannyNoDOFs", grannyJNI.GrannyNoDOFs_get());
    public static final C0890Mv dBl = new C0890Mv("GrannyXTranslation", grannyJNI.GrannyXTranslation_get());
    public static final C0890Mv dBm = new C0890Mv("GrannyYTranslation", grannyJNI.GrannyYTranslation_get());
    public static final C0890Mv dBn = new C0890Mv("GrannyZTranslation", grannyJNI.GrannyZTranslation_get());
    public static final C0890Mv dBo = new C0890Mv("GrannyXRotation", grannyJNI.GrannyXRotation_get());
    public static final C0890Mv dBp = new C0890Mv("GrannyYRotation", grannyJNI.GrannyYRotation_get());
    public static final C0890Mv dBq = new C0890Mv("GrannyZRotation", grannyJNI.GrannyZRotation_get());
    public static final C0890Mv dBr = new C0890Mv("GrannyXScaleShear", grannyJNI.GrannyXScaleShear_get());
    public static final C0890Mv dBs = new C0890Mv("GrannyYScaleShear", grannyJNI.GrannyYScaleShear_get());
    public static final C0890Mv dBt = new C0890Mv("GrannyZScaleShear", grannyJNI.GrannyZScaleShear_get());
    public static final C0890Mv dBu = new C0890Mv("GrannyTranslationDOFs", grannyJNI.GrannyTranslationDOFs_get());
    public static final C0890Mv dBv = new C0890Mv("GrannyRotationDOFs", grannyJNI.GrannyRotationDOFs_get());
    public static final C0890Mv dBw = new C0890Mv("GrannyScaleShearDOFs", grannyJNI.GrannyScaleShearDOFs_get());
    public static final C0890Mv dBx = new C0890Mv("GrannyAllDOFs", grannyJNI.GrannyAllDOFs_get());
    private static C0890Mv[] dBy = {dBk, dBl, dBm, dBn, dBo, dBp, dBq, dBr, dBs, dBt, dBu, dBv, dBw, dBx};

    /* renamed from: pF */
    private static int f1165pF = 0;

    /* renamed from: pG */
    private final int f1166pG;

    /* renamed from: pH */
    private final String f1167pH;

    private C0890Mv(String str) {
        this.f1167pH = str;
        int i = f1165pF;
        f1165pF = i + 1;
        this.f1166pG = i;
    }

    private C0890Mv(String str, int i) {
        this.f1167pH = str;
        this.f1166pG = i;
        f1165pF = i + 1;
    }

    private C0890Mv(String str, C0890Mv mv) {
        this.f1167pH = str;
        this.f1166pG = mv.f1166pG;
        f1165pF = this.f1166pG + 1;
    }

    /* renamed from: lM */
    public static C0890Mv m7187lM(int i) {
        if (i < dBy.length && i >= 0 && dBy[i].f1166pG == i) {
            return dBy[i];
        }
        for (int i2 = 0; i2 < dBy.length; i2++) {
            if (dBy[i2].f1166pG == i) {
                return dBy[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C0890Mv.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f1166pG;
    }

    public String toString() {
        return this.f1167pH;
    }
}
