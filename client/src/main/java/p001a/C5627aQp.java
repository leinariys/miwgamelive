package p001a;

/* renamed from: a.aQp  reason: case insensitive filesystem */
/* compiled from: a */
public class C5627aQp {
    private long swigCPtr;

    public C5627aQp(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5627aQp() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m17703a(C5627aQp aqp) {
        if (aqp == null) {
            return 0;
        }
        return aqp.swigCPtr;
    }
}
