package p001a;

import game.network.message.WriterBitsData;
import game.network.message.serializable.C2483fh;

/* renamed from: a.aAu  reason: case insensitive filesystem */
/* compiled from: a */
public class C5216aAu extends C2483fh {

    public StringBuilder hdV = new StringBuilder();

    public C5216aAu(WriterBitsData oUVar) {
        super(oUVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aJ */
    public void mo7783aJ(int i) {
        super.mo7783aJ(i);
        if (this.hdV.length() <= 65535) {
            if (this.hdV.length() > 0) {
                this.hdV.append("\n");
            }
            this.hdV.append(ayF.names[i]);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ad */
    public void mo7784ad(String str) {
        super.mo7784ad(str);
        if (this.hdV.length() <= 65535) {
            this.hdV.append(' ');
            this.hdV.append('\'').append(str).append('\'');
        }
    }

    public String toString() {
        return this.hdV.toString();
    }
}
