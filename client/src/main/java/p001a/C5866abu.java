package p001a;

import logic.ui.item.EditorPane;
import taikodom.addon.neo.preferences.GUIPrefAddon;

import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.CSS;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.StyleSheet;
import java.awt.*;
import java.util.*;

/* renamed from: a.abu  reason: case insensitive filesystem */
/* compiled from: a */
public class C5866abu {
    static final String KEY = "chat";
    static final String eYA = "local";
    static final String eYB = "station";
    static final String eYC = "party";
    static final String eYD = "corp";
    static final String eYE = "pvt";
    static final String eYF = "sys";
    static final String eYG = "actions";
    static final String eYH = "global";
    static final String eYm = "</style></head>\n<body id='body'>\n";
    static final String eYn = "font-size";
    static final String eYo = "global.font-color";
    static final String eYp = "local.font-color";
    static final String eYq = "station.font-color";
    static final String eYr = "party.font-color";
    static final String eYs = "corp.font-color";
    static final String eYt = "private.font-color";
    static final String eYu = "system.font-color";
    static final String eYv = "local.visible";
    static final String eYw = "station.visible";
    static final String eYx = "party.visible";
    static final String eYy = "corp.visible";
    static final String eYz = "private.visible";
    static final String footer = "</body>\n</html>";
    static final String header = "<html>\n<head>\n<style>";
    ArrayList<C1858b> eYI = new ArrayList<>();
    ArrayList<String> eYJ = new ArrayList<>();
    private boolean eYK;
    private HTMLDocument eYL;

    public C5866abu(EditorPane afp) {
        afp.setStyleSheet((StyleSheet) null);
        this.eYL = afp.getDocument();
        afp.setText(m20103ie(eYH));
        this.eYK = true;
    }

    /* renamed from: a */
    private void m20102a(String str, Color color) {
        Style style = this.eYL.getStyle(str);
        Enumeration attributeNames = style.getAttributeNames();
        while (true) {
            if (attributeNames.hasMoreElements()) {
                Object nextElement = attributeNames.nextElement();
                if ((nextElement instanceof CSS.Attribute) && "color".equals(((CSS.Attribute) nextElement).toString())) {
                    style.removeAttribute(nextElement);
                    break;
                }
            } else {
                break;
            }
        }
        style.addAttribute(StyleConstants.Foreground, color);
    }

    /* renamed from: t */
    public void mo12541t(String str, int i) {
        m20102a("." + str, new Color(i));
    }

    /* renamed from: u */
    private void m20104u(String str, int i) {
        Style style = this.eYL.getStyle(str);
        Enumeration attributeNames = style.getAttributeNames();
        while (true) {
            if (attributeNames.hasMoreElements()) {
                Object nextElement = attributeNames.nextElement();
                if ((nextElement instanceof CSS.Attribute) && eYn.equals(((CSS.Attribute) nextElement).toString())) {
                    style.removeAttribute(nextElement);
                    break;
                }
            } else {
                break;
            }
        }
        style.addAttribute(StyleConstants.Size, Integer.valueOf(i));
    }

    /* renamed from: pE */
    public void mo12539pE(int i) {
        m20104u("body", i);
    }

    private Map<String, String> bNj() {
        String[] strArr = {eYn, eYo, eYp, eYq, eYr, eYs, eYt, eYu};
        String[] b = GUIPrefAddon.m44896b(KEY, strArr, new String[]{"11", "ffffff", "ffffff", "ffa046", "14d741", "00c8ff", "ffbe00", "ff0000"});
        HashMap hashMap = new HashMap(strArr.length);
        for (int i = 0; i < strArr.length; i++) {
            hashMap.put(strArr[i], b[i]);
        }
        return hashMap;
    }

    /* renamed from: ai */
    public void mo12530ai(String str, String str2) {
        this.eYI.add(new C1858b(str, str2));
    }

    public int bNk() {
        return this.eYI.size();
    }

    /* renamed from: pF */
    public void mo12540pF(int i) {
        while (this.eYI.size() > i) {
            this.eYI.remove(0);
        }
    }

    /* renamed from: aj */
    public void mo12531aj(String str, String str2) {
        GUIPrefAddon.m44897e(KEY, str, str2);
    }

    /* renamed from: ib */
    public void mo12536ib(String str) {
        this.eYJ.add(str);
    }

    /* renamed from: ic */
    public void mo12537ic(String str) {
        this.eYJ.remove(str);
    }

    public String bNl() {
        return mo12538id((String) null);
    }

    /* renamed from: id */
    public String mo12538id(String str) {
        String replaceFirst;
        if (str != null && str.equals(eYH)) {
            str = null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(header);
        sb.append(eYm);
        Iterator<C1858b> it = this.eYI.iterator();
        while (it.hasNext()) {
            C1858b next = it.next();
            String str2 = next.gxY;
            if (this.eYK) {
                replaceFirst = next.message;
            } else {
                replaceFirst = next.message.replaceFirst("<img width='48' height='48' src='res://.*' border='0'/>", "");
            }
            if (str == null) {
                if (!this.eYJ.contains(str2) && !str2.equals(eYG)) {
                    sb.append(replaceFirst);
                }
            } else if (str.equals(str2) || str2.equals(eYF) || str2.equals(eYE)) {
                sb.append(replaceFirst);
            }
        }
        sb.append(footer);
        return sb.toString();
    }

    /* renamed from: ie */
    private String m20103ie(String str) {
        if (str == null || str.equals(eYH)) {
        }
        StringBuilder sb = new StringBuilder();
        sb.append(header);
        Map<String, String> bNj = bNj();
        sb.append("body {font-family:Tahoma; font-size:" + bNj.get(eYn) + "pt; width:100%; vertical-align:top; horizontal-align:left; color:#" + bNj.get(eYo) + ";}");
        sb.append("div {width: 100%; border:2px;}");
        sb.append("a {text-decoration: none; color:#ffa055; font-weight:bold;}");
        sb.append("img {vertical-align:top; horizontal-align:center;}");
        sb.append(".local {color:#" + bNj.get(eYp) + ";}");
        sb.append(".party {color:#" + bNj.get(eYr) + ";}");
        sb.append(".corp {color:#" + bNj.get(eYs) + ";}");
        sb.append(".pvt {color:#" + bNj.get(eYt) + ";}");
        sb.append(".station {color:#" + bNj.get(eYq) + ";}");
        sb.append(".sys {color:#" + bNj.get(eYu) + "; text-wight:bold;}");
        sb.append(eYm);
        sb.append(footer);
        return sb.toString();
    }

    /* renamed from: dV */
    public void mo12535dV(boolean z) {
        this.eYK = z;
    }

    public boolean bNm() {
        return this.eYK;
    }

    /* renamed from: a.abu$b */
    /* compiled from: a */
    class C1858b {
        String gxY;
        String message;

        public C1858b(String str, String str2) {
            this.gxY = str;
            this.message = str2;
        }
    }

    /* renamed from: a.abu$a */
    class C1857a {
        ArrayList<String> iaW = new ArrayList<>();
        String name;

        public C1857a(String str, String... strArr) {
            this.name = str;
            for (String add : strArr) {
                this.iaW.add(add);
            }
        }
    }
}
