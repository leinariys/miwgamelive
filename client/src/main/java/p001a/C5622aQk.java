package p001a;

import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

/* renamed from: a.aQk  reason: case insensitive filesystem */
/* compiled from: a */
public class C5622aQk {
    private SceneObject iEq;
    private Scene iEr;

    public C5622aQk(Scene scene) {
        this.iEr = scene;
    }

    public SceneObject dpJ() {
        return this.iEq;
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public void mo11067r(SceneObject sceneObject) {
        if (this.iEq != null) {
            sceneObject.setTransform(this.iEq.getTransform());
            dispose();
        }
        this.iEq = sceneObject;
        this.iEr.addChild(this.iEq);
    }

    public void dispose() {
        this.iEr.removeChild(this.iEq);
        this.iEq.dispose();
        this.iEq = null;
    }
}
