package p001a;

import game.network.manager.C6546aoy;
import game.script.player.Player;

@C6546aoy
/* renamed from: a.MO */
/* compiled from: a */
public class C0849MO extends Exception {

    private final C0850a dCn;
    /* renamed from: P */
    private transient Player f1100P = null;

    public C0849MO(C0850a aVar, Object... objArr) {
        super(aVar.mo3923E(objArr));
        this.dCn = aVar;
    }

    /* renamed from: dL */
    public Player mo3922dL() {
        return this.f1100P;
    }

    public C0850a bhF() {
        return this.dCn;
    }

    /* renamed from: a.MO$a */
    public enum C0850a {
        NAME_IS_NULL("The player name is null!"),
        TOO_LONG("Name ({0}) is too long!"),
        TOO_SHORT("Name ({0}) is too short!"),
        INVALID_CHARACTER("Name ({0}) has an invalid character ({1})!"),
        TOO_MANY_SPACES("Name ({0}) has too many words!"),
        FORBIDDEN("Name ({0}) is forbidden!"),
        RESERVED("Name ({0}) is reserved to another person!"),
        BELONGS_TO_USER("Name ({0}) already taken by another person!"),
        BELONGS_TO_NPC("Name ({0}) already taken by a NPC!"),
        BELONGS_TO_CORP("Name ({0}) already taken by a Corporation!"),
        BELONGS_TO_THING("Name ({0}) already taken by something!"),
        ALREADY_NAMED("You already have a name!");

        private final String message;

        private C0850a(String str) {
            this.message = str;
        }

        /* renamed from: E */
        public String mo3923E(Object... objArr) {
            return C5956adg.format(this.message, objArr);
        }
    }
}
