package p001a;

import game.network.message.C6215aif;
import logic.thred.LogPrinter;
import org.mozilla1.javascript.ScriptRuntime;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

/* renamed from: a.jT */
/* compiled from: a */
public class C2749jT {
    /* renamed from: a */
    public static BufferedImage m34024a(BufferedImage bufferedImage, BufferedImage bufferedImage2) {
        return new AffineTransformOp(AffineTransform.getScaleInstance(((double) bufferedImage2.getWidth()) / ((double) bufferedImage.getWidth()), ((double) bufferedImage2.getHeight()) / ((double) bufferedImage.getHeight())), 1).filter(bufferedImage, bufferedImage2);
    }

    /* renamed from: a */
    public static BufferedImage m34023a(BufferedImage bufferedImage, int i, int i2) {
        return new AffineTransformOp(AffineTransform.getScaleInstance(((double) i) / ((double) bufferedImage.getWidth()), ((double) i2) / ((double) bufferedImage.getHeight())), 2).filter(bufferedImage, (BufferedImage) null);
    }

    /* renamed from: b */
    public static BufferedImage m34026b(BufferedImage bufferedImage, BufferedImage bufferedImage2) {
        AffineTransform scaleInstance = AffineTransform.getScaleInstance(1.0d, -1.0d);
        scaleInstance.translate(ScriptRuntime.NaN, (double) (-bufferedImage.getHeight()));
        return new AffineTransformOp(scaleInstance, 1).filter(bufferedImage, bufferedImage2);
    }

    /* renamed from: d */
    public static BufferedImage m34027d(Component component) {
        return new Robot().createScreenCapture(new Rectangle(component.getLocationOnScreen(), component.getSize()));
    }

    /* renamed from: a */
    public static BufferedImage m34025a(ByteBuffer byteBuffer, int i, int i2) {
        int[] iArr = new int[(i * i2)];
        int i3 = 0;
        for (int i4 = 0; i4 < iArr.length; i4++) {
            int i5 = i3 + 1;
            byte b = byteBuffer.get(i3);
            int i6 = i5 + 1;
            byte b2 = byteBuffer.get(i5);
            int i7 = i6 + 1;
            byte b3 = byteBuffer.get(i6);
            i3 = i7 + 1;
            iArr[i4] = ((b2 & 255) << 8) | ((b & 255) << LogPrinter.eqN) | ((byteBuffer.get(i7) & 255) << C6215aif.idH) | (b3 & 255);
        }
        BufferedImage bufferedImage = new BufferedImage(i, i2, 2);
        bufferedImage.setRGB(0, 0, i, i2, iArr, 0, i);
        return bufferedImage;
    }
}
