package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;

/* renamed from: a.awE  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6916awE {
    /* renamed from: aY */
    void mo16663aY(Vec3f vec3f);

    /* renamed from: aZ */
    void mo16664aZ(Vec3f vec3f);

    /* renamed from: ad */
    void mo16665ad(Vec3d ajr);

    /* renamed from: ba */
    void mo16666ba(Vec3f vec3f);

    /* renamed from: bb */
    void mo16667bb(Vec3f vec3f);

    /* renamed from: bc */
    void mo16668bc(Vec3f vec3f);

    /* renamed from: g */
    void mo16669g(Quat4fWrap aoy);

    /* renamed from: iZ */
    void mo16670iZ(long j);

    /* renamed from: j */
    void mo16671j(double d, double d2, double d3);

    /* renamed from: k */
    void mo16672k(double d, double d2, double d3);

    /* renamed from: q */
    void mo16673q(float f, float f2, float f3);

    /* renamed from: r */
    void mo16674r(float f, float f2, float f3);

    /* renamed from: s */
    void mo16675s(float f, float f2, float f3);

    /* renamed from: t */
    void mo16676t(float f, float f2, float f3);

    /* renamed from: w */
    void mo16677w(float f, float f2, float f3, float f4);
}
