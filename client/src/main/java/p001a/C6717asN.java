package p001a;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/* renamed from: a.asN  reason: case insensitive filesystem */
/* compiled from: a */
public class C6717asN extends JLabel implements TableCellRenderer {
    public Component getTableCellRendererComponent(JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
        if (obj != null && (obj instanceof C0847MM)) {
            setText(((C0847MM) obj).getName());
        }
        return this;
    }
}
