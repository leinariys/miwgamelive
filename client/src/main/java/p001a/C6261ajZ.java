package p001a;

import logic.baa.C1616Xf;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: a.ajZ  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C6261ajZ implements C5285aDl {
    public List<Class<? extends C1616Xf>> fSH = new C5595aPj();
    public Map<Class<? extends C1616Xf>, Integer> fSI = new HashMap();
    public Map<Class<? extends C1616Xf>, Class<?>> fSJ = new HashMap();
    public List<Class<?>> list = new C5595aPj();

    public C6261ajZ() {
        init();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.list.size()) {
                if (this.fSH.get(i2) != null) {
                    this.fSJ.put(this.fSH.get(i2), this.list.get(i2));
                    this.fSI.put(this.fSH.get(i2), Integer.valueOf(i2));
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void init();

    /* renamed from: rL */
    public C1616Xf mo8490rL(int i) {
        Class cls = this.list.get(i);
        if (cls == null) {
            return null;
        }
        try {
            return (C1616Xf) cls.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    /* renamed from: ak */
    public <T extends C1616Xf> T mo8488ak(Class<T> cls) {
        Class cls2 = this.fSJ.get(cls);
        if (cls2 == null) {
            return null;
        }
        try {
            return (C1616Xf) cls2.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    /* renamed from: al */
    public int mo8489al(Class<? extends C1616Xf> cls) {
        Integer num = this.fSI.get(cls);
        if (num == null) {
            return -1;
        }
        return num.intValue();
    }

    /* renamed from: rM */
    public Class<? extends C1616Xf> mo14153rM(int i) {
        return this.fSH.get(i);
    }

    public List<Class<? extends C1616Xf>> cfP() {
        return Collections.unmodifiableList(this.fSH);
    }
}
