package p001a;

import game.script.Actor;
import game.script.ai.npc.AIController;
import game.script.simulation.Space;
import logic.bbb.C4029yK;

/* renamed from: a.ZM */
/* compiled from: a */
public class C1714ZM extends C0520HN {
    public float eQm;

    public C1714ZM(Space ea, Actor cr, C4029yK yKVar) {
        super(ea, cr, yKVar);
    }

    public void step(float f) {
        super.step(f);
        mo4860hf(f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: hf */
    public void mo4860hf(float f) {
        Pawn bJk = bJk();
        if (bJk != null && !bJk.isDisposed() && bJk.bae() && bJk.bGY()) {
            Controller hb = bJk.mo2998hb();
            if (hb instanceof AIController) {
                this.eQm += f;
                if (this.eQm > 0.6f) {
                    ((AIController) hb).step(this.eQm);
                    this.eQm = 0.0f;
                }
            }
        }
    }

    private Pawn bJk() {
        return (Pawn) mo2568ha();
    }
}
