package p001a;

import game.network.message.serializable.C2991mi;
import logic.baa.C1616Xf;

/* renamed from: a.ajY  reason: case insensitive filesystem */
/* compiled from: a */
public class C6260ajY<T> implements C2991mi<T> {

    private final C0651JP cUJ;
    private final int index;
    private final Object value;

    public C6260ajY(C0651JP jp, int i, Object obj) {
        this.cUJ = jp;
        this.index = i;
        this.value = obj;
    }

    public C0651JP aPk() {
        return this.cUJ;
    }

    public int index() {
        return this.index;
    }

    public Object value() {
        return this.value;
    }

    public String toString() {
        return String.valueOf(getClass().getSimpleName()) + " " + this.index + " " + this.cUJ + " " + (this.value instanceof C1616Xf ? String.valueOf(this.value.getClass().getName()) + ":" + ((C1616Xf) this.value).bFf().getObjectId().getId() : this.value);
    }
}
