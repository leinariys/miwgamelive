package p001a;

import com.sun.opengl.impl.Debug;
import com.sun.opengl.impl.GLContextImpl;
import com.sun.opengl.impl.GLDrawableHelper;

import javax.media.opengl.*;
import java.awt.*;
import java.beans.Beans;
import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedAction;

/* renamed from: a.yC */
/* compiled from: a */
public class C4014yC extends Frame implements GLAutoDrawable {
    private static final boolean DEBUG = Debug.debug("GLCanvas");
    /* access modifiers changed from: private */
    public static Method disableBackgroundEraseMethod;
    private static boolean disableBackgroundEraseInitialized;
    /* access modifiers changed from: private */
    public GLContextImpl context;
    /* access modifiers changed from: private */
    public GLDrawable drawable;
    /* access modifiers changed from: private */
    public GLDrawableHelper drawableHelper;
    /* access modifiers changed from: private */
    public C4020f gdQ;
    /* access modifiers changed from: private */
    public C4019e gdR;
    /* access modifiers changed from: private */
    public C4017c gdS;
    /* access modifiers changed from: private */
    public boolean sendReshape;
    private boolean autoSwapBufferMode;
    private GraphicsConfiguration chosen;
    private C4021g gdT;
    private C4016b gdU;
    private C4018d gdV;
    private GLCapabilitiesChooser glCapChooser;
    private GLCapabilities glCaps;

    public C4014yC() {
        this((GLCapabilities) null);
    }

    public C4014yC(GLCapabilities gLCapabilities) {
        this(gLCapabilities, (GLCapabilitiesChooser) null, (GLContext) null, (GraphicsDevice) null);
    }

    public C4014yC(GLCapabilities gLCapabilities, GLCapabilitiesChooser gLCapabilitiesChooser, GLContext gLContext, GraphicsDevice graphicsDevice) {
        this.drawableHelper = new GLDrawableHelper();
        this.autoSwapBufferMode = true;
        this.sendReshape = false;
        this.gdQ = new C4020f();
        this.gdR = new C4019e();
        this.gdS = new C4017c();
        this.gdT = new C4021g();
        this.gdU = new C4016b();
        this.gdV = new C4018d();
        this.chosen = chooseGraphicsConfiguration(gLCapabilities, gLCapabilitiesChooser, graphicsDevice);
        if (this.chosen != null) {
            this.glCapChooser = gLCapabilitiesChooser;
            this.glCaps = gLCapabilities;
        }
        if (!Beans.isDesignTime()) {
            this.drawable = GLDrawableFactory.getFactory().getGLDrawable(this, gLCapabilities, gLCapabilitiesChooser);
            this.context = this.drawable.createContext(gLContext);
            this.context.setSynchronized(true);
        }
    }

    private static GraphicsConfiguration chooseGraphicsConfiguration(GLCapabilities gLCapabilities, GLCapabilitiesChooser gLCapabilitiesChooser, GraphicsDevice graphicsDevice) {
        if (Beans.isDesignTime()) {
            return null;
        }
        AWTGraphicsConfiguration chooseGraphicsConfiguration = GLDrawableFactory.getFactory().chooseGraphicsConfiguration(gLCapabilities, gLCapabilitiesChooser, new AWTGraphicsDevice(graphicsDevice));
        if (chooseGraphicsConfiguration == null) {
            return null;
        }
        return chooseGraphicsConfiguration.getGraphicsConfiguration();
    }

    public GraphicsConfiguration getGraphicsConfiguration() {
        GraphicsConfiguration chooseGraphicsConfiguration;
        GraphicsConfiguration graphicsConfiguration = C4014yC.super.getGraphicsConfiguration();
        if (graphicsConfiguration != null && this.chosen != null && !this.chosen.equals(graphicsConfiguration)) {
            if (!this.chosen.getDevice().getIDstring().equals(graphicsConfiguration.getDevice().getIDstring()) && (chooseGraphicsConfiguration = chooseGraphicsConfiguration(this.glCaps, this.glCapChooser, graphicsConfiguration.getDevice())) != null) {
                this.chosen = chooseGraphicsConfiguration;
            }
            return this.chosen;
        } else if (graphicsConfiguration == null) {
            return this.chosen;
        } else {
            return graphicsConfiguration;
        }
    }

    public GLContext createContext(GLContext gLContext) {
        return this.drawable.createContext(gLContext);
    }

    public void setRealized(boolean z) {
    }

    public void display() {
        maybeDoSingleThreadedWorkaround(this.gdT, this.gdR);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0023, code lost:
        r0 = getClass().getName();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void paint(java.awt.Graphics r11) {
        /*
            r10 = this;
            r2 = 0
            r8 = 4611686018427387904(0x4000000000000000, double:2.0)
            boolean r0 = java.beans.Beans.isDesignTime()
            if (r0 == 0) goto L_0x005e
            java.awt.Color r0 = java.awt.Color.BLACK
            r11.setColor(r0)
            int r0 = r10.getWidth()
            int r1 = r10.getHeight()
            r11.fillRect(r2, r2, r0, r1)
            java.awt.FontMetrics r1 = r11.getFontMetrics()
            java.lang.String r0 = r10.getName()
            if (r0 != 0) goto L_0x0039
            java.lang.Class r0 = r10.getClass()
            java.lang.String r0 = r0.getName()
            r2 = 46
            int r2 = r0.lastIndexOf(r2)
            if (r2 < 0) goto L_0x0039
            int r2 = r2 + 1
            java.lang.String r0 = r0.substring(r2)
        L_0x0039:
            java.awt.geom.Rectangle2D r1 = r1.getStringBounds(r0, r11)
            java.awt.Color r2 = java.awt.Color.WHITE
            r11.setColor(r2)
            int r2 = r10.getWidth()
            double r2 = (double) r2
            double r4 = r1.getWidth()
            double r2 = r2 - r4
            double r2 = r2 / r8
            int r2 = (int) r2
            int r3 = r10.getHeight()
            double r4 = (double) r3
            double r6 = r1.getHeight()
            double r4 = r4 + r6
            double r4 = r4 / r8
            int r1 = (int) r4
            r11.drawString(r0, r2, r1)
        L_0x005d:
            return
        L_0x005e:
            r10.display()
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C4014yC.paint(java.awt.Graphics):void");
    }

    public void paintComponents(Graphics graphics) {
    }

    public void paintAll(Graphics graphics) {
    }

    public void addNotify() {
        C4014yC.super.addNotify();
        if (!Beans.isDesignTime()) {
            disableBackgroundErase();
            this.drawable.setRealized(true);
        }
        if (DEBUG) {
            System.err.println("GLCanvas.addNotify()");
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:16:0x0040=Splitter:B:16:0x0040, B:23:0x005b=Splitter:B:23:0x005b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void removeNotify() {
        /*
            r3 = this;
            r2 = 0
            boolean r0 = java.beans.Beans.isDesignTime()
            if (r0 == 0) goto L_0x000b
            p001a.C4014yC.super.removeNotify()
        L_0x000a:
            return
        L_0x000b:
            boolean r0 = javax.media.opengl.Threading.isSingleThreaded()     // Catch:{ all -> 0x0046 }
            if (r0 == 0) goto L_0x005b
            boolean r0 = javax.media.opengl.Threading.isOpenGLThread()     // Catch:{ all -> 0x0046 }
            if (r0 != 0) goto L_0x005b
            boolean r0 = javax.media.opengl.Threading.isAWTMode()     // Catch:{ all -> 0x0046 }
            if (r0 == 0) goto L_0x0040
            java.lang.Object r0 = r3.getTreeLock()     // Catch:{ all -> 0x0046 }
            boolean r0 = java.lang.Thread.holdsLock(r0)     // Catch:{ all -> 0x0046 }
            if (r0 == 0) goto L_0x0040
            a.yC$d r0 = r3.gdV     // Catch:{ all -> 0x0046 }
            r0.run()     // Catch:{ all -> 0x0046 }
        L_0x002c:
            javax.media.opengl.GLDrawable r0 = r3.drawable
            r0.setRealized(r2)
            p001a.C4014yC.super.removeNotify()
            boolean r0 = DEBUG
            if (r0 == 0) goto L_0x000a
            java.io.PrintStream r0 = java.lang.System.err
            java.lang.String r1 = "GLCanvas.removeNotify()"
            r0.println(r1)
            goto L_0x000a
        L_0x0040:
            a.yC$d r0 = r3.gdV     // Catch:{ all -> 0x0046 }
            javax.media.opengl.Threading.invokeOnOpenGLThread(r0)     // Catch:{ all -> 0x0046 }
            goto L_0x002c
        L_0x0046:
            r0 = move-exception
            javax.media.opengl.GLDrawable r1 = r3.drawable
            r1.setRealized(r2)
            p001a.C4014yC.super.removeNotify()
            boolean r1 = DEBUG
            if (r1 == 0) goto L_0x005a
            java.io.PrintStream r1 = java.lang.System.err
            java.lang.String r2 = "GLCanvas.removeNotify()"
            r1.println(r2)
        L_0x005a:
            throw r0
        L_0x005b:
            a.yC$d r0 = r3.gdV     // Catch:{ all -> 0x0046 }
            r0.run()     // Catch:{ all -> 0x0046 }
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C4014yC.removeNotify():void");
    }

    public void reshape(int i, int i2, int i3, int i4) {
        C4014yC.super.reshape(i, i2, i3, i4);
        this.sendReshape = true;
    }

    public void update(Graphics graphics) {
        paint(graphics);
    }

    public void addGLEventListener(GLEventListener gLEventListener) {
        this.drawableHelper.addGLEventListener(gLEventListener);
    }

    public void removeGLEventListener(GLEventListener gLEventListener) {
        this.drawableHelper.removeGLEventListener(gLEventListener);
    }

    public GLContext getContext() {
        return this.context;
    }

    public GL getGL() {
        if (Beans.isDesignTime()) {
            return null;
        }
        return getContext().getGL();
    }

    public void setGL(GL gl) {
        if (!Beans.isDesignTime()) {
            getContext().setGL(gl);
        }
    }

    public boolean getAutoSwapBufferMode() {
        return this.drawableHelper.getAutoSwapBufferMode();
    }

    public void setAutoSwapBufferMode(boolean z) {
        this.drawableHelper.setAutoSwapBufferMode(z);
    }

    public void swapBuffers() {
        maybeDoSingleThreadedWorkaround(this.gdU, this.gdS);
    }

    public GLCapabilities getChosenGLCapabilities() {
        if (this.drawable == null) {
            return null;
        }
        return this.drawable.getChosenGLCapabilities();
    }

    private void maybeDoSingleThreadedWorkaround(Runnable runnable, Runnable runnable2) {
        if (!Threading.isSingleThreaded() || Threading.isOpenGLThread()) {
            this.drawableHelper.invokeGL(this.drawable, this.context, runnable2, this.gdQ);
        } else {
            Threading.invokeOnOpenGLThread(runnable);
        }
    }

    private void disableBackgroundErase() {
        if (!disableBackgroundEraseInitialized) {
            try {
                AccessController.doPrivileged(new C4015a());
            } catch (Exception e) {
            }
            disableBackgroundEraseInitialized = true;
        }
        if (disableBackgroundEraseMethod != null) {
            try {
                disableBackgroundEraseMethod.invoke(getToolkit(), new Object[]{this});
            } catch (Exception e2) {
            }
        }
    }

    /* renamed from: a.yC$f */
    /* compiled from: a */
    class C4020f implements Runnable {
        C4020f() {
        }

        public void run() {
            C4014yC.this.drawableHelper.init(C4014yC.this);
        }
    }

    /* renamed from: a.yC$e */
    /* compiled from: a */
    class C4019e implements Runnable {
        C4019e() {
        }

        public void run() {
            if (C4014yC.this.sendReshape) {
                int width = C4014yC.this.getWidth();
                int height = C4014yC.this.getHeight();
                C4014yC.this.getGL().glViewport(0, 0, width, height);
                C4014yC.this.drawableHelper.reshape(C4014yC.this, 0, 0, width, height);
                C4014yC.this.sendReshape = false;
            }
            C4014yC.this.drawableHelper.display(C4014yC.this);
        }
    }

    /* renamed from: a.yC$c */
    /* compiled from: a */
    class C4017c implements Runnable {
        C4017c() {
        }

        public void run() {
            C4014yC.this.drawable.swapBuffers();
        }
    }

    /* renamed from: a.yC$g */
    /* compiled from: a */
    class C4021g implements Runnable {
        C4021g() {
        }

        public void run() {
            C4014yC.this.drawableHelper.invokeGL(C4014yC.this.drawable, C4014yC.this.context, C4014yC.this.gdR, C4014yC.this.gdQ);
        }
    }

    /* renamed from: a.yC$b */
    /* compiled from: a */
    class C4016b implements Runnable {
        C4016b() {
        }

        public void run() {
            C4014yC.this.drawableHelper.invokeGL(C4014yC.this.drawable, C4014yC.this.context, C4014yC.this.gdS, C4014yC.this.gdQ);
        }
    }

    /* renamed from: a.yC$d */
    /* compiled from: a */
    class C4018d implements Runnable {
        C4018d() {
        }

        public void run() {
            if (GLContext.getCurrent() == C4014yC.this.context) {
                C4014yC.this.context.release();
            }
            C4014yC.this.context.destroy();
        }
    }

    /* renamed from: a.yC$a */
    class C4015a implements PrivilegedAction {
        C4015a() {
        }

        public Object run() {
            try {
                C4014yC.disableBackgroundEraseMethod = C4014yC.this.getToolkit().getClass().getDeclaredMethod("disableBackgroundErase", new Class[]{Canvas.class});
                C4014yC.disableBackgroundEraseMethod.setAccessible(true);
                return null;
            } catch (Exception e) {
                return null;
            }
        }
    }
}
