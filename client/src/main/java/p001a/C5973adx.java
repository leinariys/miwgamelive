package p001a;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

/* renamed from: a.adx  reason: case insensitive filesystem */
/* compiled from: a */
public class C5973adx {
    private static Random random = new Random();
    URLConnection evH;
    Map evI;
    String evJ;

    /* renamed from: os */
    OutputStream f4361os;

    public C5973adx(URLConnection uRLConnection) {
        this.f4361os = null;
        this.evI = new HashMap();
        this.evJ = "---------------------------" + bCQ() + bCQ() + bCQ();
        this.evH = uRLConnection;
        uRLConnection.setDoOutput(true);
        uRLConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + this.evJ);
    }

    public C5973adx(URL url) {
        this(url.openConnection());
    }

    public C5973adx(String str) {
        this(new URL(str));
    }

    public static String bCQ() {
        return Long.toString(random.nextLong(), 36);
    }

    /* renamed from: a */
    private static void m21086a(InputStream inputStream, OutputStream outputStream) {
        int i = 0;
        byte[] bArr = new byte[500000];
        synchronized (inputStream) {
            while (true) {
                int read = inputStream.read(bArr, 0, bArr.length);
                if (read >= 0) {
                    outputStream.write(bArr, 0, read);
                    i += read;
                }
            }
        }
        outputStream.flush();
        byte[] bArr2 = null;
    }

    /* renamed from: a */
    public static InputStream m21082a(URL url, Map map) {
        return new C5973adx(url).mo12985t(map);
    }

    /* renamed from: a */
    public static InputStream m21084a(URL url, Object[] objArr) {
        return new C5973adx(url).mo12978q(objArr);
    }

    /* renamed from: a */
    public static InputStream m21083a(URL url, Map map, Map map2) {
        return new C5973adx(url).mo12970a(map, map2);
    }

    /* renamed from: a */
    public static InputStream m21085a(URL url, String[] strArr, Object[] objArr) {
        return new C5973adx(url).mo12971a(strArr, objArr);
    }

    /* renamed from: a */
    public static InputStream m21078a(URL url, String str, Object obj) {
        return new C5973adx(url).mo12980s(str, obj);
    }

    /* renamed from: a */
    public static InputStream m21079a(URL url, String str, Object obj, String str2, Object obj2) {
        return new C5973adx(url).mo12967a(str, obj, str2, obj2);
    }

    /* renamed from: a */
    public static InputStream m21080a(URL url, String str, Object obj, String str2, Object obj2, String str3, Object obj3) {
        return new C5973adx(url).mo12968a(str, obj, str2, obj2, str3, obj3);
    }

    /* renamed from: a */
    public static InputStream m21081a(URL url, String str, Object obj, String str2, Object obj2, String str3, Object obj3, String str4, Object obj4) {
        return new C5973adx(url).mo12969a(str, obj, str2, obj2, str3, obj3, str4, obj4);
    }

    /* access modifiers changed from: protected */
    public void connect() {
        if (this.f4361os == null) {
            this.f4361os = this.evH.getOutputStream();
        }
    }

    /* access modifiers changed from: protected */
    public void write(char c) {
        connect();
        this.f4361os.write(c);
    }

    /* access modifiers changed from: protected */
    public void write(String str) {
        connect();
        this.f4361os.write(str.getBytes());
    }

    /* access modifiers changed from: protected */
    public void bCP() {
        connect();
        write("\r\n");
    }

    /* access modifiers changed from: protected */
    public void writeln(String str) {
        connect();
        write(str);
        bCP();
    }

    private void bCR() {
        write("--");
        write(this.evJ);
    }

    private void bCS() {
        StringBuffer stringBuffer = new StringBuffer();
        Iterator it = this.evI.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            stringBuffer.append(String.valueOf(entry.getKey().toString()) + "=" + entry.getValue());
            if (it.hasNext()) {
                stringBuffer.append("; ");
            }
        }
        if (stringBuffer.length() > 0) {
            this.evH.setRequestProperty("Cookie", stringBuffer.toString());
        }
    }

    /* renamed from: R */
    public void mo12966R(String str, String str2) {
        this.evI.put(str, str2);
    }

    /* renamed from: r */
    public void mo12979r(Map map) {
        if (map != null) {
            this.evI.putAll(map);
        }
    }

    /* renamed from: i */
    public void mo12977i(String[] strArr) {
        if (strArr != null) {
            for (int i = 0; i < strArr.length - 1; i += 2) {
                mo12966R(strArr[i], strArr[i + 1]);
            }
        }
    }

    /* renamed from: hq */
    private void m21087hq(String str) {
        bCP();
        write("Content-Disposition: form-data; name=\"");
        write(str);
        write('\"');
    }

    public void setParameter(String str, String str2) {
        bCR();
        m21087hq(str);
        bCP();
        bCP();
        writeln(str2);
    }

    /* renamed from: a */
    public void mo12973a(String str, String str2, InputStream inputStream) {
        bCR();
        m21087hq(str);
        write("; filename=\"");
        write(str2);
        write('\"');
        bCP();
        write("Content-Type: ");
        String guessContentTypeFromName = URLConnection.guessContentTypeFromName(str2);
        if (guessContentTypeFromName == null) {
            guessContentTypeFromName = "application/octet-stream";
        }
        writeln(guessContentTypeFromName);
        bCP();
        m21086a(inputStream, this.f4361os);
        bCP();
    }

    /* renamed from: a */
    public void mo12972a(String str, File file) {
        mo12973a(str, file.getPath(), (InputStream) new FileInputStream(file));
    }

    public void setParameter(String str, Object obj) {
        if (obj instanceof File) {
            mo12972a(str, (File) obj);
        } else {
            setParameter(str, obj.toString());
        }
    }

    /* renamed from: s */
    public void mo12981s(Map map) {
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                setParameter(entry.getKey().toString(), entry.getValue());
            }
        }
    }

    public void setParameters(Object[] objArr) {
        if (objArr != null) {
            for (int i = 0; i < objArr.length - 1; i += 2) {
                setParameter(objArr[i].toString(), objArr[i + 1]);
            }
        }
    }

    public InputStream bCT() {
        bCR();
        writeln("--");
        this.f4361os.close();
        return this.evH.getInputStream();
    }

    /* renamed from: t */
    public InputStream mo12985t(Map map) {
        mo12981s(map);
        return bCT();
    }

    /* renamed from: q */
    public InputStream mo12978q(Object[] objArr) {
        setParameters(objArr);
        return bCT();
    }

    /* renamed from: a */
    public InputStream mo12970a(Map map, Map map2) {
        mo12979r(map);
        mo12981s(map2);
        return bCT();
    }

    /* renamed from: a */
    public InputStream mo12971a(String[] strArr, Object[] objArr) {
        mo12977i(strArr);
        setParameters(objArr);
        return bCT();
    }

    /* renamed from: s */
    public InputStream mo12980s(String str, Object obj) {
        setParameter(str, obj);
        return bCT();
    }

    /* renamed from: a */
    public InputStream mo12967a(String str, Object obj, String str2, Object obj2) {
        setParameter(str, obj);
        return mo12980s(str2, obj2);
    }

    /* renamed from: a */
    public InputStream mo12968a(String str, Object obj, String str2, Object obj2, String str3, Object obj3) {
        setParameter(str, obj);
        return mo12967a(str2, obj2, str3, obj3);
    }

    /* renamed from: a */
    public InputStream mo12969a(String str, Object obj, String str2, Object obj2, String str3, Object obj3, String str4, Object obj4) {
        setParameter(str, obj);
        return mo12968a(str2, obj2, str3, obj3, str4, obj4);
    }
}
