package p001a;

/* renamed from: a.axp  reason: case insensitive filesystem */
/* compiled from: a */
public enum C6976axp {
    _default(C0175CB.class),
    _collection(C5465aKj.class);

    private final Class<? extends C5546aNm> clazz;

    private C6976axp(Class<? extends C5546aNm> cls) {
        this.clazz = cls;
    }

    /* renamed from: vT */
    public static C5546aNm m27241vT(int i) {
        try {
            return (C5546aNm) values()[i].clazz.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
