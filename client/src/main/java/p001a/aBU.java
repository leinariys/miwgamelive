package p001a;

import logic.baa.C1616Xf;
import logic.res.html.DataClassSerializer;
import logic.thred.LogPrinter;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aBU */
/* compiled from: a */
public class aBU extends C3813vL {
    private static final Long hjS = 0L;
    static LogPrinter logger = LogPrinter.m10275K(DataClassSerializer.class);

    /* renamed from: d */
    public void mo7919d(ObjectOutput objectOutput, Object obj) {
        if (obj == null) {
            logger.warn("Long value is null! writing zero", new Exception("stack trace"));
            objectOutput.writeLong(0);
        }
        objectOutput.writeLong(((Long) obj).longValue());
    }

    /* renamed from: f */
    public Object mo7920f(ObjectInput objectInput) {
        return Long.valueOf(objectInput.readLong());
    }

    /* renamed from: b */
    public Object mo3591b(C1616Xf xf) {
        return hjS;
    }
}
