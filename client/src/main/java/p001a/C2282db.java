package p001a;

import game.network.channel.*;
import game.network.channel.client.ClientConnect;
import game.network.channel.client.ClientConnectBuilder;
import game.network.message.ByteMessage;
import logic.res.html.MessageContainer;
import logic.thred.LogPrinter;
import logic.thred.ThreadWrapper;
import util.Syst;

import java.io.EOFException;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: a.db */
/* compiled from: a */
public class C2282db implements MessageProcessing, ProcesTransportMessage, NetworkChannel {
    static LogPrinter log = LogPrinter.m10275K(C2282db.class);
    /* renamed from: yP */
    private final ClientConnect f6554yP;
    /* access modifiers changed from: private */
    public BlockingQueue<ByteMessage> aDy;
    ByteMessage aDz;
    private List<NetworkChannel> fnj;

    public C2282db(ClientConnect.Attitude aVar) {
        this.fnj = MessageContainer.init();
        this.aDy = new LinkedBlockingQueue();
        this.aDz = new ByteMessage((ClientConnect) null, (byte[]) null);
        this.f6554yP = ClientConnectBuilder.build(aVar, (InetAddress) null);
    }

    public C2282db() {
        this.fnj = MessageContainer.init();
        this.aDy = new LinkedBlockingQueue();
        this.aDz = new ByteMessage((ClientConnect) null, (byte[]) null);
        this.f6554yP = ClientConnectBuilder.build(ClientConnect.Attitude.BROTHER, (InetAddress) null);
    }

    /* renamed from: b */
    public void mo17868b(NetworkChannel ahg) {
        this.fnj.add(ahg);
        C2283a aVar = new C2283a("Multiplex Reader of " + ahg, ahg);
        aVar.setDaemon(true);
        aVar.start();
    }

    /* renamed from: jH */
    public ClientConnect getClientConnect() {
        return this.f6554yP;
    }

    /* renamed from: jI */
    public MessageProcessing getMessageProcessing() {
        return this;
    }

    /* renamed from: jJ */
    public ProcesTransportMessage getProcessSendMessage() {
        return this;
    }

    /* renamed from: iX */
    public boolean isRunningThreadSelectableChannel() {
        return false;
    }

    public void stopListening() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: Nz */
    public boolean mo17867Nz() {
        return true;
    }

    public void close() {
        for (NetworkChannel close : this.fnj) {
            try {
                close.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.aDy.add(this.aDz);
    }

    /* renamed from: jG */
    public ByteMessage getTransportMessage() {
        try {
            ByteMessage take = this.aDy.take();
            log.info(Thread.currentThread() + " take from: " + take.getFromClientConnect() + " " + Syst.toHexString(take.getMessage()));
            if (take != this.aDz) {
                return take;
            }
            throw new EOFException();
        } catch (InterruptedException e) {
            throw new ServerSocketSnapshot.C2954a(e);
        }
    }

    /* renamed from: b */
    public void sendTransportMessage(byte[] bArr, int i, int i2) {
        for (NetworkChannel next : this.fnj) {
            try {
                if (next.getClientConnect().getAttitude() != ClientConnect.Attitude.CHILD) {
                    log.info(Thread.currentThread() + " write broadcast from: " + next.getClientConnect() + " " + Syst.toHexString(bArr) + " " + next.getClass());
                    next.getProcessSendMessage().sendTransportMessage(bArr, i, i2);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public void sendTransportMessage(ClientConnect bVar, byte[] bArr, int i, int i2) {
        for (NetworkChannel next : this.fnj) {
            try {
                log.info(Thread.currentThread() + " write to: " + bVar + " " + Syst.toHexString(bArr));
                next.getProcessSendMessage().sendTransportMessage(bVar, bArr, i, i2);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isUp() {
        return true;
    }

    /* renamed from: a */
    public void creatListenerChannel(Connect omVar) {
    }

    /* renamed from: a */
    public Collection<ClientConnect> getClientConnects(ClientConnect.Attitude aVar) {
        HashSet hashSet = new HashSet();
        for (NetworkChannel a : this.fnj) {
            hashSet.addAll(a.getClientConnects(aVar));
        }
        hashSet.remove(getClientConnect());
        return hashSet;
    }

    /* renamed from: iY */
    public boolean isListenerPort() {
        return true;
    }

    /* renamed from: jK */
    public ClientConnect getClientConnectLocal() {
        return getClientConnect();
    }

    /* renamed from: c */
    public void authorizationChannelClose(ClientConnect bVar) {
    }

    /* renamed from: a */
    public void mo13494a(C5931adH adh) {
    }

    /* renamed from: a */
    public void buildListenerChannel(int listenerPort, int[] iArr, int[] iArr2, boolean z) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: af */
    public void removeChannelAndClose(int i) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: ja */
    public long metricClientTimeAuthorization() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jf */
    public long metricReadByte() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jg */
    public long metricWriteByte() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jc */
    public long metricCountReadMessage() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jb */
    public long metricCountWriteMessage() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: je */
    public long metricByteLength() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jd */
    public long metricAllOriginalByteLength() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jh */
    public InetAddress getIpRemoteHost() {
        return null;
    }

    /* renamed from: a.db$a */
    class C2283a extends ThreadWrapper {

        /* renamed from: ya */
        private final /* synthetic */ NetworkChannel f6556ya;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C2283a(String str, NetworkChannel ahg) {
            super(str);
            this.f6556ya = ahg;
        }

        public void run() {
            while (true) {
                try {
                    C2282db.this.aDy.add(this.f6556ya.getMessageProcessing().getTransportMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                } catch (ClassNotFoundException e2) {
                    e2.printStackTrace();
                    return;
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                    return;
                }
            }
        }
    }
}
