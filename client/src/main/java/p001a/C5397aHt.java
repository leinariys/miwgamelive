package p001a;

import logic.data.mbean.C0677Jd;
import logic.thred.LogPrinter;

/* renamed from: a.aHt  reason: case insensitive filesystem */
/* compiled from: a */
public class C5397aHt {
    private static LogPrinter logger = LogPrinter.m10275K(C5397aHt.class);
    public boolean hXn;
    long agR;
    C1875af ehD;
    C0677Jd hXh;
    C5397aHt hXi;
    C5397aHt hXj;
    C5397aHt hXk;
    C5397aHt hXl;
    boolean hXm;
    private byte gYL = -1;
    private aWq hXf;
    private boolean hXg;

    public C5397aHt(long j) {
        this.agR = j;
    }

    public C5397aHt() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo9270a(aWq awq, C1875af afVar, C0677Jd jd) {
        this.hXf = awq;
        this.ehD = afVar;
        this.hXh = jd;
        this.agR = afVar.getObjectId().getId();
        this.hXl = null;
    }

    public boolean isDirty() {
        return (this.hXh != null && this.hXh.aXm()) || isCreated();
    }

    public void lock() {
        if (isDirty()) {
            this.hXf.jgf++;
            this.ehD.mo13222dm();
            this.gYL = 1;
            return;
        }
        this.hXf.jge++;
        this.ehD.mo13223dn();
        this.gYL = 0;
    }

    public void unlock() {
        switch (this.gYL) {
            case 0:
                this.ehD.mo13221dl();
                return;
            case 1:
                this.ehD.mo13220dk();
                return;
            default:
                return;
        }
    }

    /* renamed from: cR */
    public void mo9271cR(boolean z) {
        this.hXg = z;
        this.hXh.mo3190cR(z);
    }

    public boolean isCreated() {
        return this.hXg;
    }

    public void reset() {
        this.hXn = false;
        this.hXg = false;
        this.hXh = null;
        this.ehD = null;
        this.hXi = null;
        this.hXj = null;
        this.hXk = null;
        this.hXl = null;
        this.gYL = -1;
        this.agR = -1;
        this.hXm = false;
    }

    public long ddk() {
        return this.agR;
    }

    public C5397aHt ddl() {
        return this.hXl;
    }

    /* renamed from: cS */
    public void mo9272cS(boolean z) {
        this.hXh.mo3191cS(true);
    }
}
