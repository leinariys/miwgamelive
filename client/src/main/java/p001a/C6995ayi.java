package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.ayi  reason: case insensitive filesystem */
/* compiled from: a */
public class C6995ayi {
    public final Vec3f fTI = new Vec3f();
    public final Vec3f fTJ = new Vec3f();
    public final Vec3f fTL = new Vec3f();
    public final Vec3f gSH = new Vec3f();
    public final Vec3f gSI = new Vec3f();
    public float gSJ;
    public float gSK;
    public float gSL;
    public Object gSM;
    public int lifeTime;

    public C6995ayi() {
    }

    public C6995ayi(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, float f) {
        mo17074b(vec3f, vec3f2, vec3f3, f);
    }

    /* renamed from: b */
    public void mo17074b(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, float f) {
        this.fTI.set(vec3f);
        this.fTJ.set(vec3f2);
        this.fTL.set(vec3f3);
        this.gSJ = f;
    }

    public float getDistance() {
        return this.gSJ;
    }

    public void setDistance(float f) {
        this.gSJ = f;
    }

    public int getLifeTime() {
        return this.lifeTime;
    }

    /* renamed from: a */
    public void mo17073a(C6995ayi ayi) {
        this.fTI.set(ayi.fTI);
        this.fTJ.set(ayi.fTJ);
        this.gSI.set(ayi.gSI);
        this.gSH.set(ayi.gSH);
        this.fTL.set(ayi.fTL);
        this.gSJ = ayi.gSJ;
        this.gSK = ayi.gSK;
        this.gSL = ayi.gSL;
        this.gSM = ayi.gSM;
        this.lifeTime = ayi.lifeTime;
    }

    public Vec3f cDp() {
        return this.gSI;
    }

    public Vec3f cDq() {
        return this.gSH;
    }
}
