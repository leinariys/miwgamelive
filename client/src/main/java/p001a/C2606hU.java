package p001a;

import logic.bbb.C4029yK;
import logic.res.FileControl;
import logic.res.XmlNode;
import taikodom.render.loader.provider.FilePath;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/* renamed from: a.hU */
/* compiled from: a */
public class C2606hU {

    private static final Map<String, C4029yK> cache = Collections.synchronizedMap(new HashMap());
    /* renamed from: WA */
    private static FilePath rootPathResource = new FileControl(System.getProperty("physics.resources.rootpath", "."));

    /* renamed from: zK */
    public static FilePath getRootPathResource() {
        return rootPathResource;
    }

    /* renamed from: c */
    public static void setRootPathResource(FilePath ain) {
        rootPathResource = ain;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x00aa A[SYNTHETIC, Splitter:B:22:0x00aa] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static C4029yK m32703b(java.lang.String r13, boolean r14) {

        String r3 = r13.replace(".pro", ".msh").replace("data/models", "res/shapes");
        String r4 = r3 + "/" + r14;
        C4029yK r0 = cache.get(r4);

        // Если объект есть в кэше, возвращаем его
        if (r0 != null) {
            return r0;
        }

        try {
            // Загружаем из файла
            InputStream stream = f7948WA.aC(r3).openInputStream();

            // Неизвестно что делает aGY(XmlNode), нужно больше информации
            XmlNode data = new aXX().a(stream, "UTF-8");

            stream.close();

            String shapeType = r14 ? "concave" : "convex";
            List<XmlNode> shapeData = data.q("shape", "type", shapeType);

            // Проверяем количество форм в полученных данных
            if (shapeData.size() == 0) {
                // Если не находим форму, указанную в shapeType, ищем сферическую форму
                shapeData = data.q("shape", "type", "spherical");
                XmlNode shape = shapeData.get(0);

                // Ищем аттрибут радиуса сферы
                float radius = Float.parseFloat(shape.mD("sphere").getAttribute("radius"));

                // Создаем экземпляр alI с найденным радиусом и возвращаем его
                return new alI(radius);
            }
        } catch (IOException e) {
            throw new RuntimeException("PHYSICS RESOURCE " + r3 + " NOT FOUND.", e);
        }

        // Обработка исключений
        // ...

        return r0;


        /*
            r12 = 12
            r11 = 1
            r2 = 0
            r1 = 0
            java.lang.String r0 = ".pro"
            java.lang.String r3 = ".msh"
            java.lang.String r0 = r13.replace(r0, r3)
            java.lang.String r3 = "data/models"
            java.lang.String r4 = "res/shapes"
            java.lang.String r3 = r0.replace(r3, r4)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r3)
            r0.<init>(r4)
            java.lang.String r4 = "/"
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.StringBuilder r0 = r0.append(r14)
            java.lang.String r4 = r0.toString()
            java.util.Map<java.lang.String, a.yK> r0 = cache
            java.lang.Object r0 = r0.get(r4)
            a.yK r0 = (logic.bbb.C4029yK) r0
            if (r0 == 0) goto L_0x0037
        L_0x0036:
            return r0
        L_0x0037:
            a.ain r0 = f7948WA     // Catch:{ IOException -> 0x0089, all -> 0x01e3 }
            a.ain r0 = r0.mo2259aC(r3)     // Catch:{ IOException -> 0x0089, all -> 0x01e3 }
            java.io.InputStream r0 = r0.openInputStream()     // Catch:{ IOException -> 0x0089, all -> 0x01e3 }
            a.axx r5 = new a.axx     // Catch:{ IOException -> 0x01e7 }
            r5.<init>()     // Catch:{ IOException -> 0x01e7 }
            java.lang.String r6 = "UTF-8"
            a.aGY r5 = r5.mo16863a((java.io.InputStream) r0, (java.lang.String) r6)     // Catch:{ IOException -> 0x01e7 }
            if (r0 == 0) goto L_0x0051
            r0.close()     // Catch:{ Exception -> 0x01e0 }
        L_0x0051:
            if (r14 == 0) goto L_0x00ae
            java.lang.String r0 = "concave"
        L_0x0055:
            java.lang.String r6 = "shape"
            java.lang.String r7 = "type"
            java.util.List r0 = r5.mo9057q(r6, r7, r0)
            int r6 = r0.size()
            if (r6 != 0) goto L_0x00b1
            java.lang.String r0 = "shape"
            java.lang.String r2 = "type"
            java.lang.String r3 = "spherical"
            java.util.List r0 = r5.mo9057q(r0, r2, r3)
            java.lang.Object r0 = r0.get(r1)
            a.aGY r0 = (p001a.aGY) r0
            java.lang.String r1 = "sphere"
            a.aGY r0 = r0.mo9049mD(r1)
            java.lang.String r1 = "radius"
            java.lang.String r0 = r0.getAttribute(r1)
            float r1 = java.lang.Float.parseFloat(r0)
            a.alI r0 = new a.alI
            r0.<init>(r1)
            goto L_0x0036
        L_0x0089:
            r0 = move-exception
            r0 = r2
        L_0x008b:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x00a6 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a6 }
            java.lang.String r4 = "PHYSICS RESOURCE "
            r2.<init>(r4)     // Catch:{ all -> 0x00a6 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x00a6 }
            java.lang.String r3 = " NOT FOUND."
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x00a6 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00a6 }
            r1.<init>(r2)     // Catch:{ all -> 0x00a6 }
            throw r1     // Catch:{ all -> 0x00a6 }
        L_0x00a6:
            r1 = move-exception
            r2 = r0
        L_0x00a8:
            if (r2 == 0) goto L_0x00ad
            r2.close()     // Catch:{ Exception -> 0x01dd }
        L_0x00ad:
            throw r1
        L_0x00ae:
            java.lang.String r0 = "convex"
            goto L_0x0055
        L_0x00b1:
            int r5 = r0.size()
            if (r5 <= r11) goto L_0x00d2
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "PHYSICS RESOUCE "
            r1.<init>(r2)
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r2 = " CONTAINS MORE THAN ONE SHAPE FOR THE SAME ASSET."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00d2:
            if (r14 != 0) goto L_0x0133
            java.lang.Object r0 = r0.get(r1)
            a.aGY r0 = (p001a.aGY) r0
            java.lang.String r3 = "point"
            java.util.List r2 = r0.mo9057q(r3, r2, r2)
            int r0 = r2.size()
            com.hoplon.geometry.Vec3f[] r3 = new com.hoplon.geometry.Vec3f[r0]
        L_0x00e6:
            int r0 = r2.size()
            if (r1 < r0) goto L_0x00f8
            a.SC r0 = new a.SC
            r0.<init>(r3)
            java.util.Map<java.lang.String, a.yK> r1 = cache
            r1.put(r4, r0)
            goto L_0x0036
        L_0x00f8:
            com.hoplon.geometry.Vec3f r5 = new com.hoplon.geometry.Vec3f
            java.lang.Object r0 = r2.get(r1)
            a.aGY r0 = (p001a.aGY) r0
            java.lang.String r6 = "x"
            java.lang.String r0 = r0.getAttribute(r6)
            float r6 = java.lang.Float.parseFloat(r0)
            java.lang.Object r0 = r2.get(r1)
            a.aGY r0 = (p001a.aGY) r0
            java.lang.String r7 = "y"
            java.lang.String r0 = r0.getAttribute(r7)
            float r7 = java.lang.Float.parseFloat(r0)
            java.lang.Object r0 = r2.get(r1)
            a.aGY r0 = (p001a.aGY) r0
            java.lang.String r8 = "z"
            java.lang.String r0 = r0.getAttribute(r8)
            float r0 = java.lang.Float.parseFloat(r0)
            r5.<init>(r6, r7, r0)
            r3[r1] = r5
            int r0 = r1 + 1
            r1 = r0
            goto L_0x00e6
        L_0x0133:
            java.lang.Object r0 = r0.get(r1)
            a.aGY r0 = (p001a.aGY) r0
            java.lang.String r3 = "mesh"
            java.util.List r0 = r0.mo9057q(r3, r2, r2)
            a.aRF r2 = new a.aRF
            r2.<init>()
            java.util.Iterator r3 = r0.iterator()
        L_0x0148:
            boolean r0 = r3.hasNext()
            if (r0 != 0) goto L_0x015a
            a.eh r0 = new a.eh
            r0.<init>(r2, r11)
            java.util.Map<java.lang.String, a.yK> r1 = cache
            r1.put(r4, r0)
            goto L_0x0036
        L_0x015a:
            java.lang.Object r0 = r3.next()
            a.aGY r0 = (p001a.aGY) r0
            java.lang.String r5 = "indexes"
            a.aGY r5 = r0.mo9049mD(r5)
            java.lang.String r6 = "vertices"
            a.aGY r0 = r0.mo9049mD(r6)
            java.lang.String r6 = "value"
            java.lang.String r5 = r5.getAttribute(r6)
            java.lang.String r6 = "value"
            java.lang.String r6 = r0.getAttribute(r6)
            java.util.StringTokenizer r7 = new java.util.StringTokenizer
            java.lang.String r0 = " "
            r7.<init>(r5, r0)
            int r5 = r7.countTokens()
            int r0 = r5 * 4
            java.nio.ByteBuffer r8 = java.nio.ByteBuffer.allocate(r0)
            r0 = r1
        L_0x018a:
            boolean r9 = r7.hasMoreTokens()
            if (r9 != 0) goto L_0x01c1
            java.util.StringTokenizer r7 = new java.util.StringTokenizer
            java.lang.String r0 = " "
            r7.<init>(r6, r0)
            int r6 = r7.countTokens()
            int r0 = r6 * 4
            java.nio.ByteBuffer r9 = java.nio.ByteBuffer.allocate(r0)
            r0 = r1
        L_0x01a2:
            boolean r10 = r7.hasMoreTokens()
            if (r10 != 0) goto L_0x01cf
            a.wW r0 = new a.wW
            r0.<init>()
            int r5 = r5 / 3
            r0.bFf = r5
            r0.bFg = r8
            r0.bFh = r12
            int r5 = r6 / 3
            r0.numVertices = r5
            r0.bFi = r9
            r0.bFj = r12
            r2.mo11092a(r0)
            goto L_0x0148
        L_0x01c1:
            int r0 = r0 + 1
            java.lang.String r9 = r7.nextToken()
            int r9 = java.lang.Integer.parseInt(r9)
            r8.putInt(r9)
            goto L_0x018a
        L_0x01cf:
            int r0 = r0 + 1
            java.lang.String r10 = r7.nextToken()
            float r10 = java.lang.Float.parseFloat(r10)
            r9.putFloat(r10)
            goto L_0x01a2
        L_0x01dd:
            r0 = move-exception
            goto L_0x00ad
        L_0x01e0:
            r0 = move-exception
            goto L_0x0051
        L_0x01e3:
            r0 = move-exception
            r1 = r0
            goto L_0x00a8
        L_0x01e7:
            r1 = move-exception
            goto L_0x008b
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C2606hU.m32703b(java.lang.String, boolean):a.yK");
    }
}
