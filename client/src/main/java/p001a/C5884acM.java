package p001a;

import game.network.message.externalizable.C0939Nn;
import game.script.ship.Outpost;
import logic.ui.C2698il;
import taikodom.addon.IAddonProperties;

/* renamed from: a.acM  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C5884acM {
    private static /* synthetic */ int[] feZ;
    public C2698il djv;

    /* renamed from: kj */
    public IAddonProperties f4217kj;

    C5884acM(IAddonProperties vWVar, C2698il ilVar) {
        this.f4217kj = vWVar;
        this.djv = ilVar;
    }

    static /* synthetic */ int[] bPY() {
        int[] iArr = feZ;
        if (iArr == null) {
            iArr = new int[Outpost.C3348a.values().length];
            try {
                iArr[Outpost.C3348a.MONEY_TRANSFER.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Outpost.C3348a.NO_UPKEEP_ITEM.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            feZ = iArr;
        }
        return iArr;
    }

    /* renamed from: a */
    public abstract void mo12600a(Outpost qZVar, boolean z);

    public abstract void destroy();

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo12599a(Outpost.C3350c cVar) {
        String format;
        switch (bPY()[cVar.cuS().ordinal()]) {
            case 1:
                format = this.f4217kj.translate("Your corporation has no enough money");
                break;
            case 2:
                format = C5956adg.format(this.f4217kj.translate("Required upkeep item '{0}' with amount '{1}' was not found"), cVar.cuT().mo463eP().mo19891ke().get(), Integer.valueOf(cVar.cuT().getAmount()));
                break;
            default:
                throw new IllegalArgumentException("Invalid code");
        }
        this.f4217kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, format, new Object[0]));
    }
}
