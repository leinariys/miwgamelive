package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.Po */
/* compiled from: a */
public class C1080Po {
    public final Vec3f dPX = new Vec3f();
    public final Vec3f dPY = new Vec3f();
    public int dPZ;
    public int dQa;
    public int dQb;

    /* renamed from: a */
    public void mo4839a(C1080Po po) {
        this.dPX.set(po.dPX);
        this.dPY.set(po.dPY);
        this.dPZ = po.dPZ;
        this.dQa = po.dQa;
        this.dQb = po.dQb;
    }
}
