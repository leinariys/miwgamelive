package p001a;

import game.engine.FloatBit;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aCz  reason: case insensitive filesystem */
/* compiled from: a */
public class C5273aCz extends FloatBit {
    public static final C5273aCz hvo = new C5273aCz();
    private static final int gbL = -126;
    private static final float hvp = Float.MIN_NORMAL;
    private static final int hvq = 127;

    /* renamed from: Ka */
    public int mo8271Ka() {
        return 127;
    }

    /* renamed from: Kb */
    public int mo8272Kb() {
        return -126;
    }

    /* renamed from: Kc */
    public int mo8273Kc() {
        return 32;
    }

    /* renamed from: aT */
    public int mo8282aT(float f) {
        return Float.floatToRawIntBits(f);
    }

    /* renamed from: cF */
    public float mo8283cF(int i) {
        return Float.intBitsToFloat(i);
    }

    /* renamed from: a */
    public void mo8281a(ObjectOutput objectOutput, float f) {
        objectOutput.writeFloat(f);
    }

    /* renamed from: d */
    public float mo8284d(ObjectInput objectInput) {
        return objectInput.readFloat();
    }

    /* renamed from: Kd */
    public int mo8274Kd() {
        return 23;
    }

    /* renamed from: Ke */
    public int mo8275Ke() {
        return 8;
    }

    /* renamed from: Kf */
    public float mo8276Kf() {
        return Float.MAX_VALUE;
    }

    /* renamed from: Kg */
    public float mo8277Kg() {
        return hvp;
    }

    /* renamed from: Kh */
    public float mo8278Kh() {
        return 0.0f;
    }

    /* renamed from: Ki */
    public boolean mo8279Ki() {
        return true;
    }

    /* renamed from: Kj */
    public boolean mo8280Kj() {
        return true;
    }
}
