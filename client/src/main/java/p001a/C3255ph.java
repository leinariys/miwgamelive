package p001a;


import java.util.*;

/* renamed from: a.ph */
/* compiled from: a */
public class C3255ph {
    public static final int gSS = 10;
    public static final int gST = 2;
    public static final float gSU = 0.05f;

    /* renamed from: H */
    public static <T extends C2355eP> List<T> m37270H(Map<T, ? extends C5761aVt> map) {
        Object obj;
        if (map == null || map.containsValue((Object) null)) {
            return null;
        }
        ArrayList<C2355eP> arrayList = new ArrayList<>();
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(map.keySet());
        Collections.sort(arrayList2, new C3256a(map));
        ArrayList arrayList3 = new ArrayList();
        for (int i = 0; i < 2; i++) {
            arrayList3.add((C2355eP) arrayList2.get(i));
            arrayList.add((C2355eP) arrayList2.get(i));
        }
        Iterator it = arrayList3.iterator();
        while (it.hasNext()) {
            C2355eP ePVar = (C2355eP) it.next();
            if (it.hasNext()) {
                obj = it.next();
            } else {
                obj = arrayList.get(0);
            }
            C2355eP[] b = ePVar.mo18004b((C2355eP) obj);
            arrayList.add(b[0]);
            arrayList.add(b[1]);
        }
        while (arrayList.size() != 10) {
            C2355eP ePVar2 = (C2355eP) arrayList2.get(arrayList2.size() - 1);
            for (int i2 = 0; i2 < 50; i2++) {
                ePVar2.mo18006mu();
            }
            try {
                arrayList.add((C2355eP) ePVar2.clone());
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
        for (C2355eP ePVar3 : arrayList) {
            if (Math.random() < 0.05000000074505806d) {
                ePVar3.mo18006mu();
            }
        }
        return arrayList;
    }

    /* renamed from: a.ph$a */
    class C3256a implements Comparator<T> {
        private final /* synthetic */ Map aRy;

        C3256a(Map map) {
            this.aRy = map;
        }

        /* renamed from: a */
        public int compare(C2355eP ePVar, C2355eP ePVar2) {
            return ((C5761aVt) this.aRy.get(ePVar)).mo11955a((C5761aVt) this.aRy.get(ePVar2));
        }
    }
}
