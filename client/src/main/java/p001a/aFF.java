package p001a;

/* renamed from: a.aFF */
/* compiled from: a */
public class aFF<T> extends C6518aoW<T> {
    private Class<T> dXq;

    public aFF(Class<T> cls) {
        super(false);
        this.dXq = cls;
    }

    /* access modifiers changed from: protected */
    public T create() {
        try {
            return this.dXq.newInstance();
        } catch (InstantiationException e) {
            throw new IllegalStateException(e);
        } catch (IllegalAccessException e2) {
            throw new IllegalStateException(e2);
        }
    }

    /* access modifiers changed from: protected */
    public void copy(T t, T t2) {
        throw new UnsupportedOperationException();
    }
}
