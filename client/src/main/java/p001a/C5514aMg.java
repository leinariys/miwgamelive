package p001a;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

/* renamed from: a.aMg  reason: case insensitive filesystem */
/* compiled from: a */
public class C5514aMg extends C5445aJp {
    long start = -1;
    private ThreadMXBean cVI;

    public void start() {
        this.start = ako();
    }

    public void end() {
        set(Math.round(((double) (ako() - this.start)) / 1000000.0d));
    }

    private long ako() {
        return diY().getCurrentThreadCpuTime();
    }

    public ThreadMXBean diY() {
        if (this.cVI == null) {
            this.cVI = ManagementFactory.getThreadMXBean();
        }
        return this.cVI;
    }
}
