package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;

import javax.vecmath.Quat4f;

/* renamed from: a.aC */
/* compiled from: a */
public class C1777aC {

    /* renamed from: jR */
    static final /* synthetic */ boolean f2455jR = (!C1777aC.class.desiredAssertionStatus());

    /* renamed from: a */
    public static float m13118a(Quat4f quat4f) {
        return 2.0f * ((float) Math.acos((double) quat4f.w));
    }

    /* renamed from: a */
    public static void m13121a(Quat4f quat4f, Vec3f vec3f, float f) {
        float length = vec3f.length();
        if (f2455jR || length != 0.0f) {
            float sin = ((float) Math.sin((double) (f * 0.5f))) / length;
            quat4f.set(vec3f.x * sin, vec3f.y * sin, sin * vec3f.z, (float) Math.cos((double) (f * 0.5f)));
            return;
        }
        throw new AssertionError();
    }

    /* renamed from: a */
    public static Quat4f m13119a(Vec3f vec3f, Vec3f vec3f2) {
        Quat4f quat4f;
        C0763Kt bcE = C0763Kt.bcE();
        bcE.bcH().push();
        bcE.bcN().push();
        try {
            Vec3f vec3f3 = (Vec3f) bcE.bcH().get();
            vec3f3.cross(vec3f, vec3f2);
            float dot = vec3f.dot(vec3f2);
            if (((double) dot) < -0.9999998807907104d) {
                quat4f = (Quat4f) bcE.bcN().mo15197aq(bcE.bcN().mo11472a(0.0f, 1.0f, 0.0f, 0.0f));
            } else {
                float sqrt = (float) Math.sqrt((double) ((dot + 1.0f) * 2.0f));
                float f = 1.0f / sqrt;
                quat4f = (Quat4f) bcE.bcN().mo15197aq(bcE.bcN().mo11472a(vec3f3.x * f, vec3f3.y * f, vec3f3.z * f, sqrt * 0.5f));
                bcE.bcH().pop();
                bcE.bcN().pop();
            }
            return quat4f;
        } finally {
            bcE.bcH().pop();
            bcE.bcN().pop();
        }
    }

    /* renamed from: a */
    public static void m13120a(Quat4f quat4f, Vec3f vec3f) {
        quat4f.set(((quat4f.w * vec3f.x) + (quat4f.y * vec3f.z)) - (quat4f.z * vec3f.y), ((quat4f.w * vec3f.y) + (quat4f.z * vec3f.x)) - (quat4f.x * vec3f.z), ((quat4f.w * vec3f.z) + (quat4f.x * vec3f.y)) - (quat4f.y * vec3f.x), (((-quat4f.x) * vec3f.x) - (quat4f.y * vec3f.y)) - (quat4f.z * vec3f.z));
    }

    /* renamed from: b */
    public static Vec3f m13122b(Quat4f quat4f, Vec3f vec3f) {
        C0763Kt bcE = C0763Kt.bcE();
        bcE.bcH().push();
        bcE.bcN().push();
        try {
            Quat4fWrap b = bcE.bcN().mo11474b(quat4f);
            m13120a((Quat4f) b, vec3f);
            Quat4f quat4f2 = (Quat4f) bcE.bcN().get();
            quat4f2.inverse(quat4f);
            b.mul(quat4f2);
            return (Vec3f) bcE.bcH().mo15197aq(bcE.bcH().mo4460h(b.x, b.y, b.z));
        } finally {
            bcE.bcH().pop();
            bcE.bcN().pop();
        }
    }
}
