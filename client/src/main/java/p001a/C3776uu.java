package p001a;

import java.util.HashMap;

/* renamed from: a.uu */
/* compiled from: a */
public class C3776uu {
    HashMap<Integer, C3777a> bvW = new HashMap<>();
    private boolean bvX = true;

    public static void main(String[] strArr) {
        C3776uu uuVar = new C3776uu();
        StringBuilder sb = new StringBuilder();
        sb.append(uuVar.mo22465c("method", "sttt", "tran. time", 123456789L, "avg tran. time", Float.valueOf(5.1234568E7f), "perc of up time", Float.valueOf(2.342134E7f), "exec. count", 10)).append("\n");
        sb.append(uuVar.mo22465c("method", "sttt", "tran. time", 10L, "avg tran. time", Float.valueOf(5.0f), "perc of up time", Float.valueOf(0.1f), "exec. count", 10));
        System.out.println(sb.toString());
    }

    /* renamed from: c */
    public String mo22465c(Object... objArr) {
        StringBuilder sb = this.bvX ? new StringBuilder() : null;
        StringBuilder sb2 = new StringBuilder();
        for (int i = 0; i < objArr.length - 1; i += 2) {
            String str = objArr[i];
            String str2 = objArr[i + 1];
            C3777a aVar = this.bvW.get(Integer.valueOf(i));
            if (aVar == null) {
                C3777a aVar2 = new C3777a();
                aVar2.title = str;
                aVar2.width = str.length();
                if (str2 instanceof Integer) {
                    aVar2.width = Math.max(9, aVar2.width);
                    aVar2.format = "%," + aVar2.width + "d ";
                } else if (str2 instanceof String) {
                    aVar2.width = Math.max(80, Math.max(str2.length(), aVar2.width));
                    aVar2.format = "%" + aVar2.width + "s ";
                } else if (str2 instanceof Long) {
                    aVar2.width = Math.max(20, aVar2.width);
                    aVar2.format = "%," + aVar2.width + "d ";
                } else if (str2 instanceof Float) {
                    aVar2.width = Math.max(20, aVar2.width);
                    aVar2.format = "%," + aVar2.width + "f ";
                }
                aVar2.fSK = "%" + aVar2.width + "s ";
                this.bvW.put(Integer.valueOf(i), aVar2);
                sb.append(String.format(aVar2.fSK, new Object[]{str}));
                aVar = aVar2;
            }
            sb2.append(String.format(aVar.format, new Object[]{str2}));
        }
        if (!this.bvX) {
            return sb2.toString();
        }
        this.bvX = false;
        return String.valueOf(sb.toString()) + "\n" + sb2.toString();
    }

    /* renamed from: a.uu$a */
    static class C3777a {
        public int width;
        String fSK;
        String format;
        String title;

        C3777a() {
        }
    }
}
