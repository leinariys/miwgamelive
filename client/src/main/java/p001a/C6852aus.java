package p001a;

/* renamed from: a.aus  reason: case insensitive filesystem */
/* compiled from: a */
public final class C6852aus {
    public static final C6852aus gDE = new C6852aus("GrannySeekStart");
    public static final C6852aus gDF = new C6852aus("GrannySeekEnd");
    public static final C6852aus gDG = new C6852aus("GrannySeekCurrent");
    private static C6852aus[] gDH = {gDE, gDF, gDG};

    /* renamed from: pF */
    private static int f5429pF = 0;

    /* renamed from: pG */
    private final int f5430pG;

    /* renamed from: pH */
    private final String f5431pH;

    private C6852aus(String str) {
        this.f5431pH = str;
        int i = f5429pF;
        f5429pF = i + 1;
        this.f5430pG = i;
    }

    private C6852aus(String str, int i) {
        this.f5431pH = str;
        this.f5430pG = i;
        f5429pF = i + 1;
    }

    private C6852aus(String str, C6852aus aus) {
        this.f5431pH = str;
        this.f5430pG = aus.f5430pG;
        f5429pF = this.f5430pG + 1;
    }

    /* renamed from: uI */
    public static C6852aus m26465uI(int i) {
        if (i < gDH.length && i >= 0 && gDH[i].f5430pG == i) {
            return gDH[i];
        }
        for (int i2 = 0; i2 < gDH.length; i2++) {
            if (gDH[i2].f5430pG == i) {
                return gDH[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C6852aus.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f5430pG;
    }

    public String toString() {
        return this.f5431pH;
    }
}
