package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.acc  reason: case insensitive filesystem */
/* compiled from: a */
class C5900acc extends aEH.C1791a<Vec3f> {
    final /* synthetic */ aEH eZT;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C5900acc(aEH aeh) {
        super((aEH.C1791a) null);
        this.eZT = aeh;
    }

    /* renamed from: eP */
    public Vec3f mo8563O(String str) {
        String[] split = str.split("[ \t\r\n]+");
        return new Vec3f(Float.parseFloat(split[0]), Float.parseFloat(split[1]), Float.parseFloat(split[2]));
    }
}
