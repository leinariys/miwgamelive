package p001a;

/* renamed from: a.ann  reason: case insensitive filesystem */
/* compiled from: a */
public enum C6483ann {
    UNDEFINED,
    YAW_LEFT,
    YAW_RIGHT,
    PITCH_UP,
    PITCH_DOWN,
    ROLL_CW,
    ROLL_CCW,
    ACCELERATE,
    DEACCELERATE,
    YAW,
    PITCH,
    ROLL,
    THROTTLE;

    /* renamed from: jA */
    public static C6483ann m24336jA(String str) {
        try {
            return valueOf(str);
        } catch (IllegalArgumentException e) {
            return UNDEFINED;
        }
    }
}
