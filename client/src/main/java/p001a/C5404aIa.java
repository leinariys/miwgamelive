package p001a;

import java.io.PrintStream;
import java.io.PrintWriter;

/* renamed from: a.aIa  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5404aIa {
    /* renamed from: N */
    int mo6731N(Class cls);

    /* renamed from: a */
    int mo6732a(Class cls, int i);

    /* renamed from: bg */
    String mo6733bg(int i);

    /* renamed from: bh */
    Throwable mo6734bh(int i);

    /* renamed from: c */
    void mo6735c(PrintWriter printWriter);

    Throwable getCause();

    String getMessage();

    String[] getMessages();

    void printStackTrace(PrintStream printStream);

    void printStackTrace(PrintWriter printWriter);

    /* renamed from: vn */
    int mo6742vn();

    /* renamed from: vo */
    Throwable[] mo6743vo();
}
