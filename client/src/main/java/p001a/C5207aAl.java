package p001a;

import game.script.avatar.AvatarSector;
import game.script.avatar.ColorWrapper;
import game.script.avatar.TextureGrid;
import game.script.avatar.body.BodyPiece;
import game.script.avatar.body.TextureScheme;
import game.script.resource.Asset;

import java.awt.geom.Rectangle2D;
import java.util.Collection;

/* renamed from: a.aAl  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5207aAl {
    Collection<AvatarSector> aFI();

    AvatarSector aFO();

    AvatarSector aFQ();

    Rectangle2D.Float aGk();

    float aOD();

    Asset bdT();

    Collection<BodyPiece> bdU();

    AvatarSector bdV();

    Asset bdW();

    ColorWrapper bdX();

    float bdY();

    TextureGrid bdZ();

    Asset bea();

    Asset beb();

    float bec();

    TextureScheme bed();

    boolean bee();

    TextureGrid bef();

    TextureGrid beg();

    TextureGrid beh();

    /* renamed from: cJ */
    Asset mo7725cJ();

    float getHeight();

    String getPrefix();

    /* renamed from: n */
    BodyPiece mo7728n(AvatarSector nw);
}
