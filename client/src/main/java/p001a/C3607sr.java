package p001a;

import game.network.message.externalizable.C2631hn;
import game.network.message.externalizable.C3328qK;
import game.network.message.serializable.C3988xl;
import logic.baa.C1616Xf;
import logic.res.code.C2759jd;
import logic.res.html.C0029AO;
import logic.res.html.DataClassSerializer;
import logic.res.html.MessageContainer;

import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.lang.reflect.Field;
import java.util.List;

/* renamed from: a.sr */
/* compiled from: a */
public class C3607sr extends C2759jd {

    private DataClassSerializer ahd;

    public C3607sr(Class<?> cls, Field field, String str, Class<?>[] clsArr) {
        super(cls, field, str, clsArr);
    }

    /* renamed from: EE */
    public DataClassSerializer mo19962EE() {
        if (this.ahd == null) {
            DataClassSerializer[] Eu = mo11298Eu();
            if (Eu == null || Eu.length <= 0) {
                this.ahd = C5726aUk.m18699b(Object.class, (Class<?>[]) null);
            } else {
                this.ahd = Eu[0];
            }
        }
        return this.ahd;
    }

    /* renamed from: yl */
    public boolean mo2180yl() {
        return true;
    }

    /* renamed from: Et */
    public C5546aNm mo19968Et() {
        C5465aKj akj = new C5465aKj();
        akj.mo9792c(this);
        return akj;
    }

    /* renamed from: c */
    public C5546aNm mo11306c(Object obj, Object obj2) {
        if (obj2 instanceof C3988xl) {
            List anB = ((C3988xl) obj2).anB();
            if (anB == null || anB.size() <= 0) {
                return null;
            }
            return new C5465aKj(anB);
        }
        C2631hn hnVar = (C2631hn) obj2;
        List dgK = MessageContainer.init();
        dgK.add(new C6786ate(C0651JP.CLEAR));
        if (hnVar.getCollection().size() > 0) {
            dgK.add(new C6630aqe(C0651JP.ADD_COLLECTION, hnVar.getCollection()));
        }
        return new C5465aKj(dgK);
    }

    /* renamed from: z */
    public Object mo2181z(Object obj) {
        return ((C2631hn) obj).clone();
    }

    /* renamed from: b */
    public Object mo2188b(C1616Xf xf) {
        return new C3328qK(xf, this);
    }

    /* renamed from: d */
    public Object mo11307d(Object obj, Object obj2) {
        return ((C3988xl) obj2).anE();
    }

    /* renamed from: Ek */
    public boolean mo2179Ek() {
        return false;
    }

    /* renamed from: c */
    public C0029AO mo2189c(C1616Xf xf) {
        return null;
    }

    /* renamed from: a */
    public void mo11305a(C1616Xf xf, ObjectOutput objectOutput, Object obj) {
        if (obj != null) {
            objectOutput.writeBoolean(true);
            ((C2631hn) obj).mo284a(objectOutput);
            return;
        }
        objectOutput.writeBoolean(false);
    }

    /* renamed from: a */
    public Object mo11304a(C1616Xf xf, ObjectInput objectInput) {
        if (!objectInput.readBoolean()) {
            return null;
        }
        C2631hn hnVar = (C2631hn) mo2189c(xf);
        hnVar.mo286c(objectInput);
        return hnVar;
    }
}
