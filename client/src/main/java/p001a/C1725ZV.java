package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;
import logic.abc.C0802Lc;

import javax.vecmath.Quat4f;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.ZV */
/* compiled from: a */
public class C1725ZV extends C1083Pr {
    /* renamed from: jR */
    static final /* synthetic */ boolean f2229jR = (!C1725ZV.class.desiredAssertionStatus());
    private static final float gBG = 1.0f;
    private static C6238ajC gBF = new C6238ajC(0.0f, (C6863avD) null, (C0802Lc) null);
    public List<C0616Ik> gBT = new ArrayList();
    public List<Vec3f> gBH = new ArrayList();
    public List<Vec3f> gBI = new ArrayList();
    public C1220S gBJ = new C1220S();
    public C1220S gBK = new C1220S();
    private float damping;
    private float feX;
    private C1450VL gBL;
    private float gBM = 0.0f;
    private float gBN;
    private float gBO;
    private C6238ajC gBP;
    private int gBQ = 0;
    private int gBR = 2;
    private int gBS = 1;

    public C1725ZV(aIS ais, C6238ajC ajc, C1450VL vl) {
        super(aVP.VEHICLE_CONSTRAINT_TYPE);
        this.gBL = vl;
        this.gBP = ajc;
        m12069a(ais);
    }

    /* renamed from: a */
    private void m12069a(aIS ais) {
        this.gBO = 0.0f;
        this.gBN = 0.0f;
    }

    /* renamed from: a */
    public C0616Ik mo7405a(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, float f, float f2, aIS ais, boolean z) {
        C0996Og og = new C0996Og();
        og.dJk.set(vec3f);
        og.deN.set(vec3f2);
        og.deO.set(vec3f3);
        og.dJl = f;
        og.dJm = f2;
        og.deS = ais.deS;
        og.deT = ais.idr;
        og.deU = ais.ids;
        og.deV = ais.deV;
        og.dfc = z;
        og.deQ = ais.deQ;
        this.gBT.add(new C0616Ik(og));
        C0616Ik ik = this.gBT.get(cwA() - 1);
        mo7407a(ik, false);
        mo7421i(cwA() - 1, false);
        return ik;
    }

    /* renamed from: uD */
    public C3978xf mo7427uD(int i) {
        if (f2229jR || i < cwA()) {
            return this.gBT.get(i).deL;
        }
        throw new AssertionError();
    }

    /* renamed from: uE */
    public void mo7428uE(int i) {
        mo7421i(i, true);
    }

    /* renamed from: i */
    public void mo7421i(int i, boolean z) {
        this.stack.bcH().push();
        this.stack.bcN().push();
        this.stack.bcK().push();
        try {
            C0616Ik ik = this.gBT.get(i);
            mo7407a(ik, z);
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            vec3f.negate(ik.deK.hPG);
            Vec3f vec3f2 = ik.deK.hPH;
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            vec3f3.cross(vec3f, vec3f2);
            vec3f3.normalize();
            float f = ik.deW;
            Quat4f quat4f = (Quat4f) this.stack.bcN().get();
            C1777aC.m13121a(quat4f, vec3f, f);
            Matrix3fWrap ajd = (Matrix3fWrap) this.stack.bcK().get();
            C3427rS.m38370a(ajd, quat4f);
            Quat4f quat4f2 = (Quat4f) this.stack.bcN().get();
            C1777aC.m13121a(quat4f2, vec3f2, -ik.deX);
            Matrix3fWrap ajd2 = (Matrix3fWrap) this.stack.bcK().get();
            C3427rS.m38370a(ajd2, quat4f2);
            Matrix3fWrap ajd3 = (Matrix3fWrap) this.stack.bcK().get();
            ajd3.setRow(0, vec3f2.x, vec3f3.x, vec3f.x);
            ajd3.setRow(1, vec3f2.y, vec3f3.y, vec3f.y);
            ajd3.setRow(2, vec3f2.z, vec3f3.z, vec3f.z);
            Matrix3fWrap ajd4 = ik.deL.bFF;
            ajd4.mul(ajd, ajd2);
            ajd4.mul(ajd3);
            ik.deL.bFG.scaleAdd(ik.deK.hPE, ik.deK.hPG, ik.deK.hPF);
        } finally {
            this.stack.bcH().pop();
            this.stack.bcN().pop();
            this.stack.bcK().pop();
        }
    }

    public void cwy() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.gBT.size()) {
                C0616Ik ik = this.gBT.get(i2);
                ik.deK.hPE = ik.aVZ();
                ik.dff = 0.0f;
                ik.deK.hPC.negate(ik.deK.hPG);
                ik.dfe = 1.0f;
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    public void mo7406a(C0616Ik ik) {
        mo7407a(ik, true);
    }

    /* renamed from: a */
    public void mo7407a(C0616Ik ik, boolean z) {
        this.stack.bcJ().push();
        try {
            ik.deK.hPI = false;
            C3978xf d = this.stack.bcJ().mo1157d(cwz());
            if (z && cwB().cey() != null) {
                cwB().cey().mo2960e(d);
            }
            ik.deK.hPF.set(ik.deM);
            d.mo22946G(ik.deK.hPF);
            ik.deK.hPG.set(ik.deN);
            d.bFF.transform(ik.deK.hPG);
            ik.deK.hPH.set(ik.deO);
            d.bFF.transform(ik.deK.hPH);
        } finally {
            this.stack.bcJ().pop();
        }
    }

    /* renamed from: b */
    public float mo7409b(C0616Ik ik) {
        float f;
        this.stack.bcH().push();
        try {
            mo7407a(ik, false);
            float aVZ = ik.deR + ik.aVZ();
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            vec3f.scale(aVZ, ik.deK.hPG);
            Vec3f vec3f2 = ik.deK.hPF;
            ik.deK.hPD.add(vec3f2, vec3f);
            Vec3f vec3f3 = ik.deK.hPD;
            C0403Fc fc = new C0403Fc();
            if (f2229jR || this.gBL != null) {
                Object a = this.gBL.mo6056a(vec3f2, vec3f3, fc);
                ik.deK.hPJ = null;
                if (a != null) {
                    float f2 = fc.cUM;
                    float f3 = fc.cUM * aVZ;
                    ik.deK.hPC.set(fc.cUL);
                    ik.deK.hPI = true;
                    ik.deK.hPJ = gBF;
                    ik.deK.hPE = (f2 * aVZ) - ik.deR;
                    float aVZ2 = ik.aVZ() - (ik.deQ * 0.01f);
                    float aVZ3 = ik.aVZ() + (ik.deQ * 0.01f);
                    if (ik.deK.hPE < aVZ2) {
                        ik.deK.hPE = aVZ2;
                    }
                    if (ik.deK.hPE > aVZ3) {
                        ik.deK.hPE = aVZ3;
                    }
                    ik.deK.hPD.set(fc.cUK);
                    float dot = ik.deK.hPC.dot(ik.deK.hPG);
                    Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                    Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                    vec3f5.sub(ik.deK.hPD, cwB().ces());
                    vec3f4.set(cwB().mo13897R(vec3f5));
                    float dot2 = ik.deK.hPC.dot(vec3f4);
                    if (dot >= -0.1f) {
                        ik.dff = 0.0f;
                        ik.dfe = 10.0f;
                        f = f3;
                    } else {
                        float f4 = -1.0f / dot;
                        ik.dff = dot2 * f4;
                        ik.dfe = f4;
                        f = f3;
                    }
                } else {
                    ik.deK.hPE = ik.aVZ();
                    ik.dff = 0.0f;
                    ik.deK.hPC.negate(ik.deK.hPG);
                    ik.dfe = 1.0f;
                    f = -1.0f;
                }
                return f;
            }
            throw new AssertionError();
        } finally {
            this.stack.bcH().pop();
        }
    }

    public C3978xf cwz() {
        return cwB().ceu();
    }

    /* renamed from: kL */
    public void mo7422kL(float f) {
        this.stack.bcH().push();
        this.stack.bcJ().push();
        int i = 0;
        while (i < cwA()) {
            try {
                mo7421i(i, false);
                i++;
            } finally {
                this.stack.bcH().pop();
                this.stack.bcJ().pop();
            }
        }
        this.gBO = 3.6f * cwB().cev().length();
        C3978xf d = this.stack.bcJ().mo1157d(cwz());
        if (this.stack.bcH().mo4460h(d.bFF.getElement(0, this.gBS), d.bFF.getElement(1, this.gBS), d.bFF.getElement(2, this.gBS)).dot(cwB().cev()) < 0.0f) {
            this.gBO *= -1.0f;
        }
        for (int i2 = 0; i2 < this.gBT.size(); i2++) {
            mo7409b(this.gBT.get(i2));
        }
        mo7423kM(f);
        for (int i3 = 0; i3 < this.gBT.size(); i3++) {
            C0616Ik ik = this.gBT.get(i3);
            float f2 = ik.dfg;
            if (f2 > 6000.0f) {
                f2 = 6000.0f;
            }
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            vec3f.scale(f2 * f, ik.deK.hPC);
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            vec3f2.sub(ik.deK.hPD, cwB().ces());
            cwB().mo13937m(vec3f, vec3f2);
        }
        mo7424kN(f);
        for (int i4 = 0; i4 < this.gBT.size(); i4++) {
            C0616Ik ik2 = this.gBT.get(i4);
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            vec3f3.sub(ik2.deK.hPF, cwB().ces());
            Vec3f ac = this.stack.bcH().mo4458ac(cwB().mo13897R(vec3f3));
            if (ik2.deK.hPI) {
                C3978xf d2 = this.stack.bcJ().mo1157d(cwz());
                Vec3f h = this.stack.bcH().mo4460h(d2.bFF.getElement(0, this.gBS), d2.bFF.getElement(1, this.gBS), d2.bFF.getElement(2, this.gBS));
                float dot = h.dot(ik2.deK.hPC);
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                vec3f4.scale(dot, ik2.deK.hPC);
                h.sub(vec3f4);
                ik2.deY = (h.dot(ac) * f) / ik2.deR;
                ik2.deX += ik2.deY;
            } else {
                ik2.deX += ik2.deY;
            }
            ik2.deY *= 0.99f;
        }
    }

    /* renamed from: f */
    public void mo7418f(float f, int i) {
        if (f2229jR || (i >= 0 && i < cwA())) {
            mo7430uG(i).deW = f;
            return;
        }
        throw new AssertionError();
    }

    /* renamed from: uF */
    public float mo7429uF(int i) {
        return mo7430uG(i).deW;
    }

    /* renamed from: g */
    public void mo7419g(float f, int i) {
        if (f2229jR || (i >= 0 && i < cwA())) {
            mo7430uG(i).dfa = f;
            return;
        }
        throw new AssertionError();
    }

    /* renamed from: uG */
    public C0616Ik mo7430uG(int i) {
        if (f2229jR || (i >= 0 && i < cwA())) {
            return this.gBT.get(i);
        }
        throw new AssertionError();
    }

    /* renamed from: h */
    public void mo7420h(float f, int i) {
        if (f2229jR || (i >= 0 && i < cwA())) {
            mo7430uG(i).dfb = f;
            return;
        }
        throw new AssertionError();
    }

    /* renamed from: kM */
    public void mo7423kM(float f) {
        float f2;
        float aRf = 1.0f / this.gBP.aRf();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < cwA()) {
                C0616Ik ik = this.gBT.get(i2);
                if (ik.deK.hPI) {
                    float aVZ = ik.dfe * (ik.aVZ() - ik.deK.hPE) * ik.deS;
                    float f3 = ik.dff;
                    if (f3 < 0.0f) {
                        f2 = ik.deT;
                    } else {
                        f2 = ik.deU;
                    }
                    ik.dfg = (aVZ - (f2 * f3)) * aRf;
                    if (ik.dfg < 0.0f) {
                        ik.dfg = 0.0f;
                    }
                } else {
                    ik.dfg = 0.0f;
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    private float m12068a(C1726a aVar) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f = aVar.eSb;
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            vec3f2.sub(vec3f, aVar.eRZ.ces());
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            vec3f3.sub(vec3f, aVar.eSa.ces());
            float f = aVar.eSd;
            Vec3f ac = this.stack.bcH().mo4458ac(aVar.eRZ.mo13897R(vec3f2));
            Vec3f ac2 = this.stack.bcH().mo4458ac(aVar.eSa.mo13897R(vec3f3));
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            vec3f4.sub(ac, ac2);
            return Math.max(Math.min((-aVar.eSc.dot(vec3f4)) * aVar.dMf, f), -f);
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: kN */
    public void mo7424kN(float f) {
        float f2;
        boolean z;
        int i;
        this.stack.bcH().push();
        this.stack.bcK().push();
        try {
            int cwA = cwA();
            if (cwA != 0) {
                C3696tt.m39762a(this.gBH, cwA, Vec3f.class);
                C3696tt.m39762a(this.gBI, cwA, Vec3f.class);
                C3696tt.m39759a(this.gBJ, cwA, 0.0f);
                C3696tt.m39759a(this.gBK, cwA, 0.0f);
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                int i2 = 0;
                int i3 = 0;
                while (i3 < cwA()) {
                    if (((C6238ajC) this.gBT.get(i3).deK.hPJ) != null) {
                        i = i2 + 1;
                    } else {
                        i = i2;
                    }
                    this.gBK.set(i3, 0.0f);
                    this.gBJ.set(i3, 0.0f);
                    i3++;
                    i2 = i;
                }
                for (int i4 = 0; i4 < cwA(); i4++) {
                    C0616Ik ik = this.gBT.get(i4);
                    C6238ajC ajc = (C6238ajC) ik.deK.hPJ;
                    if (ajc != null) {
                        Matrix3fWrap g = this.stack.bcK().mo15565g(mo7427uD(i4).bFF);
                        this.gBI.get(i4).set(g.getElement(0, this.gBQ), g.getElement(1, this.gBQ), g.getElement(2, this.gBQ));
                        Vec3f vec3f2 = ik.deK.hPC;
                        vec3f.scale(this.gBI.get(i4).dot(vec3f2), vec3f2);
                        this.gBI.get(i4).sub(vec3f);
                        this.gBI.get(i4).normalize();
                        this.gBH.get(i4).cross(vec3f2, this.gBI.get(i4));
                        this.gBH.get(i4).normalize();
                        float[] qW = this.stack.bcO().mo1166qW(1);
                        C2196ce.m28561a(this.gBP, ik.deK.hPD, ajc, ik.deK.hPD, 0.0f, this.gBI.get(i4), qW, f);
                        this.gBK.set(i4, qW[0]);
                        this.stack.bcO().release(qW);
                        this.gBK.set(i4, this.gBK.get(i4) * 1.0f);
                    }
                }
                boolean z2 = false;
                int i5 = 0;
                while (i5 < cwA()) {
                    C0616Ik ik2 = this.gBT.get(i5);
                    C6238ajC ajc2 = (C6238ajC) ik2.deK.hPJ;
                    if (ajc2 == null) {
                        f2 = 0.0f;
                    } else if (ik2.dfa != 0.0f) {
                        f2 = ik2.dfa * f;
                    } else {
                        float f3 = 0.0f;
                        if (ik2.dfb != 0.0f) {
                            f3 = ik2.dfb;
                        }
                        f2 = m12068a(new C1726a(this.gBP, ajc2, ik2.deK.hPD, this.gBH.get(i5), f3));
                    }
                    this.gBJ.set(i5, 0.0f);
                    this.gBT.get(i5).dfh = 1.0f;
                    if (ajc2 != null) {
                        this.gBT.get(i5).dfh = 1.0f;
                        float f4 = ik2.dfg * f * ik2.deV;
                        this.gBJ.set(i5, f2);
                        float f5 = this.gBJ.get(i5) * 0.5f;
                        float f6 = this.gBK.get(i5) * 1.0f;
                        float f7 = (f6 * f6) + (f5 * f5);
                        if (f7 > f4 * f4) {
                            float sqrt = f4 / ((float) Math.sqrt((double) f7));
                            C0616Ik ik3 = this.gBT.get(i5);
                            ik3.dfh = sqrt * ik3.dfh;
                            z = true;
                            i5++;
                            z2 = z;
                        }
                    }
                    z = z2;
                    i5++;
                    z2 = z;
                }
                if (z2) {
                    for (int i6 = 0; i6 < cwA(); i6++) {
                        if (this.gBK.get(i6) != 0.0f && this.gBT.get(i6).dfh < 1.0f) {
                            this.gBJ.set(i6, this.gBT.get(i6).dfh * this.gBJ.get(i6));
                            this.gBK.set(i6, this.gBT.get(i6).dfh * this.gBK.get(i6));
                        }
                    }
                }
                int i7 = 0;
                while (true) {
                    int i8 = i7;
                    if (i8 >= cwA()) {
                        this.stack.bcH().pop();
                        this.stack.bcK().pop();
                        return;
                    }
                    C0616Ik ik4 = this.gBT.get(i8);
                    Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                    vec3f3.sub(ik4.deK.hPD, this.gBP.ces());
                    if (this.gBJ.get(i8) != 0.0f) {
                        vec3f.scale(this.gBJ.get(i8), this.gBH.get(i8));
                        this.gBP.mo13937m(vec3f, vec3f3);
                    }
                    if (this.gBK.get(i8) != 0.0f) {
                        C6238ajC ajc3 = (C6238ajC) this.gBT.get(i8).deK.hPJ;
                        Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                        vec3f4.sub(ik4.deK.hPD, ajc3.ces());
                        Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                        vec3f5.scale(this.gBK.get(i8), this.gBI.get(i8));
                        vec3f3.z = ik4.deZ * vec3f3.z;
                        this.gBP.mo13937m(vec3f5, vec3f3);
                        vec3f.negate(vec3f5);
                        ajc3.mo13937m(vec3f, vec3f4);
                    }
                    i7 = i8 + 1;
                }
            }
        } finally {
            this.stack.bcH().pop();
            this.stack.bcK().pop();
        }
    }

    /* renamed from: PU */
    public void mo4840PU() {
    }

    /* renamed from: cv */
    public void mo4847cv(float f) {
    }

    public int cwA() {
        return this.gBT.size();
    }

    /* renamed from: kO */
    public void mo7425kO(float f) {
        this.gBM = f;
    }

    public C6238ajC cwB() {
        return this.gBP;
    }

    public int cwC() {
        return this.gBQ;
    }

    public int aOR() {
        return this.gBR;
    }

    public int cwD() {
        return this.gBS;
    }

    public Vec3f cwE() {
        this.stack.bcH().push();
        try {
            C3978xf cwz = cwz();
            return (Vec3f) this.stack.bcH().mo15197aq(this.stack.bcH().mo4460h(cwz.bFF.getElement(0, this.gBS), cwz.bFF.getElement(1, this.gBS), cwz.bFF.getElement(2, this.gBS)));
        } finally {
            this.stack.bcH().pop();
        }
    }

    public float cwF() {
        return this.gBO;
    }

    /* renamed from: u */
    public void mo7426u(int i, int i2, int i3) {
        this.gBQ = i;
        this.gBR = i2;
        this.gBS = i3;
    }

    /* renamed from: a.ZV$a */
    private static class C1726a {
        public final Vec3f eSb = new Vec3f();
        public final Vec3f eSc = new Vec3f();
        public float dMf;
        public C6238ajC eRZ;
        public C6238ajC eSa;
        public float eSd;

        public C1726a(C6238ajC ajc, C6238ajC ajc2, Vec3f vec3f, Vec3f vec3f2, float f) {
            this.eRZ = ajc;
            this.eSa = ajc2;
            this.eSb.set(vec3f);
            this.eSc.set(vec3f2);
            this.eSd = f;
            this.dMf = 1.0f / (ajc.mo13892A(vec3f, vec3f2) + ajc2.mo13892A(vec3f, vec3f2));
        }
    }
}
