package p001a;

/* renamed from: a.bR */
/* compiled from: a */
public final class C2040bR {

    /* renamed from: pA */
    public static final C2040bR f5792pA = new C2040bR("GrannyRawTextureEncoding");

    /* renamed from: pB */
    public static final C2040bR f5793pB = new C2040bR("GrannyS3TCTextureEncoding");

    /* renamed from: pC */
    public static final C2040bR f5794pC = new C2040bR("GrannyBinkTextureEncoding");

    /* renamed from: pD */
    public static final C2040bR f5795pD = new C2040bR("GrannyOnePastLastTextureEncoding");
    /* renamed from: pz */
    public static final C2040bR f5798pz = new C2040bR("GrannyUserTextureEncoding");
    /* renamed from: pE */
    private static C2040bR[] f5796pE = {f5798pz, f5792pA, f5793pB, f5794pC, f5795pD};
    /* renamed from: pF */
    private static int f5797pF = 0;
    /* renamed from: pG */
    private final int f5799pG;

    /* renamed from: pH */
    private final String f5800pH;

    private C2040bR(String str) {
        this.f5800pH = str;
        int i = f5797pF;
        f5797pF = i + 1;
        this.f5799pG = i;
    }

    private C2040bR(String str, int i) {
        this.f5800pH = str;
        this.f5799pG = i;
        f5797pF = i + 1;
    }

    private C2040bR(String str, C2040bR bRVar) {
        this.f5800pH = str;
        this.f5799pG = bRVar.f5799pG;
        f5797pF = this.f5799pG + 1;
    }

    /* renamed from: Z */
    public static C2040bR m27846Z(int i) {
        if (i < f5796pE.length && i >= 0 && f5796pE[i].f5799pG == i) {
            return f5796pE[i];
        }
        for (int i2 = 0; i2 < f5796pE.length; i2++) {
            if (f5796pE[i2].f5799pG == i) {
                return f5796pE[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C2040bR.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f5799pG;
    }

    public String toString() {
        return this.f5800pH;
    }
}
