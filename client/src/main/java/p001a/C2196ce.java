package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;
import logic.aaa.C6026aey;

/* renamed from: a.ce */
/* compiled from: a */
public class C2196ce {

    /* renamed from: qn */
    public static final C0709KF f6197qn = new C6844auk();
    /* renamed from: qo */
    public static final C0709KF f6198qo = new C6842aui();
    /* renamed from: qp */
    public static final C0709KF f6199qp = new C6841auh();
    /* renamed from: jR */
    static final /* synthetic */ boolean f6196jR = (!C2196ce.class.desiredAssertionStatus());

    /* renamed from: a */
    public static void m28561a(C6238ajC ajc, Vec3f vec3f, C6238ajC ajc2, Vec3f vec3f2, float f, Vec3f vec3f3, float[] fArr, float f2) {
        float lengthSquared = vec3f3.lengthSquared();
        if (!f6196jR && Math.abs(lengthSquared) >= 1.1f) {
            throw new AssertionError();
        } else if (lengthSquared > 1.1f) {
            fArr[0] = 0.0f;
        } else {
            C0763Kt bcE = C0763Kt.bcE();
            C1123QW<C6460anQ> D = C0762Ks.m6640D(C6460anQ.class);
            bcE.bcF();
            try {
                Vec3f vec3f4 = (Vec3f) bcE.bcH().get();
                vec3f4.sub(vec3f, ajc.ces());
                Vec3f vec3f5 = (Vec3f) bcE.bcH().get();
                vec3f5.sub(vec3f2, ajc2.ces());
                Vec3f vec3f6 = (Vec3f) bcE.bcH().get();
                vec3f6.set(ajc.mo13897R(vec3f4));
                Vec3f vec3f7 = (Vec3f) bcE.bcH().get();
                vec3f7.set(ajc2.mo13897R(vec3f5));
                Vec3f vec3f8 = (Vec3f) bcE.bcH().get();
                vec3f8.sub(vec3f6, vec3f7);
                Matrix3fWrap g = bcE.bcK().mo15565g(ajc.ceu().bFF);
                g.transpose();
                Matrix3fWrap g2 = bcE.bcK().mo15565g(ajc2.ceu().bFF);
                g2.transpose();
                C6460anQ anq = D.get();
                anq.mo14967a(g, g2, vec3f4, vec3f5, vec3f3, ajc.ceq(), ajc.aRf(), ajc2.ceq(), ajc2.aRf());
                Vec3f ac = bcE.bcH().mo4458ac(ajc.getAngularVelocity());
                g.transform(ac);
                Vec3f ac2 = bcE.bcH().mo4458ac(ajc2.getAngularVelocity());
                g2.transform(ac2);
                anq.mo14972i(ajc.cev(), ac, ajc2.cev(), ac2);
                D.release(anq);
                fArr[0] = vec3f3.dot(vec3f8) * (-0.2f) * (1.0f / anq.clX());
            } finally {
                bcE.bcG();
            }
        }
    }

    /* renamed from: a */
    public static float m28560a(C6238ajC ajc, C6238ajC ajc2, C6995ayi ayi, C5718aUc auc) {
        C0763Kt bcE = C0763Kt.bcE();
        bcE.bcH().push();
        try {
            Vec3f cDp = ayi.cDp();
            Vec3f cDq = ayi.cDq();
            Vec3f vec3f = ayi.fTL;
            Vec3f vec3f2 = (Vec3f) bcE.bcH().get();
            vec3f2.sub(cDp, ajc.ces());
            Vec3f vec3f3 = (Vec3f) bcE.bcH().get();
            vec3f3.sub(cDq, ajc2.ces());
            Vec3f ac = bcE.bcH().mo4458ac(ajc.mo13897R(vec3f2));
            Vec3f ac2 = bcE.bcH().mo4458ac(ajc2.mo13897R(vec3f3));
            Vec3f vec3f4 = (Vec3f) bcE.bcH().get();
            vec3f4.sub(ac, ac2);
            float dot = vec3f.dot(vec3f4);
            float f = auc.iWs * (1.0f / auc.aGM);
            C6026aey aey = (C6026aey) ayi.gSM;
            if (f6196jR || aey != null) {
                float f2 = f * (-aey.dMg);
                float f3 = ((aey.dMe - dot) * aey.dMf) + (f2 * aey.dMf);
                float f4 = aey.dMa;
                float f5 = f3 + f4;
                if (0.0f > f5) {
                    f5 = 0.0f;
                }
                aey.dMa = f5;
                float f6 = aey.dMa - f4;
                Vec3f vec3f5 = (Vec3f) bcE.bcH().get();
                if (ajc.aRf() != 0.0f) {
                    vec3f5.scale(ajc.aRf(), ayi.fTL);
                    ajc.mo13911b(vec3f5, aey.dLX, f6);
                }
                if (ajc2.aRf() != 0.0f) {
                    vec3f5.scale(ajc2.aRf(), ayi.fTL);
                    ajc2.mo13911b(vec3f5, aey.dLY, -f6);
                }
                return f6;
            }
            throw new AssertionError();
        } finally {
            bcE.bcH().pop();
        }
    }

    /* renamed from: b */
    public static float m28562b(C6238ajC ajc, C6238ajC ajc2, C6995ayi ayi, C5718aUc auc) {
        C0763Kt bcE = C0763Kt.bcE();
        bcE.bcH().push();
        try {
            Vec3f cDp = ayi.cDp();
            Vec3f cDq = ayi.cDq();
            Vec3f vec3f = (Vec3f) bcE.bcH().get();
            vec3f.sub(cDp, ajc.ces());
            Vec3f vec3f2 = (Vec3f) bcE.bcH().get();
            vec3f2.sub(cDq, ajc2.ces());
            C6026aey aey = (C6026aey) ayi.gSM;
            if (f6196jR || aey != null) {
                float f = aey.dMa * aey.dMd;
                if (aey.dMa > 0.0f) {
                    Vec3f vec3f3 = (Vec3f) bcE.bcH().get();
                    vec3f3.set(ajc.mo13897R(vec3f));
                    Vec3f vec3f4 = (Vec3f) bcE.bcH().get();
                    vec3f4.set(ajc2.mo13897R(vec3f2));
                    Vec3f vec3f5 = (Vec3f) bcE.bcH().get();
                    vec3f5.sub(vec3f3, vec3f4);
                    float f2 = (-aey.fpD.dot(vec3f5)) * aey.fpA;
                    float f3 = aey.fpy;
                    aey.fpy = f2 + f3;
                    aey.fpy = Math.min(aey.fpy, f);
                    aey.fpy = Math.max(aey.fpy, -f);
                    float f4 = aey.fpy - f3;
                    float f5 = (-aey.fpE.dot(vec3f5)) * aey.fpB;
                    float f6 = aey.fpz;
                    aey.fpz = f5 + f6;
                    aey.fpz = Math.min(aey.fpz, f);
                    aey.fpz = Math.max(aey.fpz, -f);
                    float f7 = aey.fpz - f6;
                    Vec3f vec3f6 = (Vec3f) bcE.bcH().get();
                    if (ajc.aRf() != 0.0f) {
                        vec3f6.scale(ajc.aRf(), aey.fpD);
                        ajc.mo13911b(vec3f6, aey.fpF, f4);
                        vec3f6.scale(ajc.aRf(), aey.fpE);
                        ajc.mo13911b(vec3f6, aey.fpH, f7);
                    }
                    if (ajc2.aRf() != 0.0f) {
                        vec3f6.scale(ajc2.aRf(), aey.fpD);
                        ajc2.mo13911b(vec3f6, aey.fpG, -f4);
                        vec3f6.scale(ajc2.aRf(), aey.fpE);
                        ajc2.mo13911b(vec3f6, aey.fpI, -f7);
                    }
                }
                return aey.dMa;
            }
            throw new AssertionError();
        } finally {
            bcE.bcH().pop();
        }
    }

    /* renamed from: c */
    public static float m28563c(C6238ajC ajc, C6238ajC ajc2, C6995ayi ayi, C5718aUc auc) {
        C0763Kt bcE = C0763Kt.bcE();
        bcE.bcH().push();
        try {
            Vec3f cDp = ayi.cDp();
            Vec3f cDq = ayi.cDq();
            Vec3f vec3f = ayi.fTL;
            Vec3f vec3f2 = (Vec3f) bcE.bcH().get();
            vec3f2.sub(cDp, ajc.ces());
            Vec3f vec3f3 = (Vec3f) bcE.bcH().get();
            vec3f3.sub(cDq, ajc2.ces());
            Vec3f ac = bcE.bcH().mo4458ac(ajc.mo13897R(vec3f2));
            Vec3f ac2 = bcE.bcH().mo4458ac(ajc2.mo13897R(vec3f3));
            Vec3f vec3f4 = (Vec3f) bcE.bcH().get();
            vec3f4.sub(ac, ac2);
            float dot = vec3f.dot(vec3f4);
            float f = auc.iWs * (1.0f / auc.aGM);
            C6026aey aey = (C6026aey) ayi.gSM;
            if (f6196jR || aey != null) {
                float f2 = f * (-aey.dMg);
                float f3 = aey.dMe - dot;
                float f4 = (f3 * aey.dMf) + (f2 * aey.dMf);
                float f5 = aey.dMa;
                float f6 = f4 + f5;
                if (0.0f > f6) {
                    f6 = 0.0f;
                }
                aey.dMa = f6;
                float f7 = aey.dMa - f5;
                Vec3f vec3f5 = (Vec3f) bcE.bcH().get();
                if (ajc.aRf() != 0.0f) {
                    vec3f5.scale(ajc.aRf(), ayi.fTL);
                    ajc.mo13911b(vec3f5, aey.dLX, f7);
                }
                if (ajc2.aRf() != 0.0f) {
                    vec3f5.scale(ajc2.aRf(), ayi.fTL);
                    ajc2.mo13911b(vec3f5, aey.dLY, -f7);
                }
                ac.set(ajc.mo13897R(vec3f2));
                ac2.set(ajc2.mo13897R(vec3f3));
                vec3f4.sub(ac, ac2);
                vec3f5.scale(vec3f.dot(vec3f4), vec3f);
                Vec3f vec3f6 = (Vec3f) bcE.bcH().get();
                vec3f6.sub(vec3f4, vec3f5);
                float length = vec3f6.length();
                float f8 = aey.dMd;
                if (aey.dMa > 0.0f && length > 1.1920929E-7f) {
                    vec3f6.scale(1.0f / length);
                    Vec3f vec3f7 = (Vec3f) bcE.bcH().get();
                    vec3f7.cross(vec3f2, vec3f6);
                    ajc.aRa().transform(vec3f7);
                    Vec3f vec3f8 = (Vec3f) bcE.bcH().get();
                    vec3f8.cross(vec3f3, vec3f6);
                    ajc2.aRa().transform(vec3f8);
                    Vec3f vec3f9 = (Vec3f) bcE.bcH().get();
                    vec3f9.cross(vec3f7, vec3f2);
                    Vec3f vec3f10 = (Vec3f) bcE.bcH().get();
                    vec3f10.cross(vec3f8, vec3f3);
                    vec3f5.add(vec3f9, vec3f10);
                    float f9 = aey.dMa * f8;
                    float max = Math.max(Math.min(length / ((ajc.aRf() + ajc2.aRf()) + vec3f6.dot(vec3f5)), f9), -f9);
                    vec3f5.scale(-max, vec3f6);
                    ajc.mo13937m(vec3f5, vec3f2);
                    vec3f5.scale(max, vec3f6);
                    ajc2.mo13937m(vec3f5, vec3f3);
                }
                return f7;
            }
            throw new AssertionError();
        } finally {
            bcE.bcH().pop();
        }
    }

    /* renamed from: d */
    public static float m28564d(C6238ajC ajc, C6238ajC ajc2, C6995ayi ayi, C5718aUc auc) {
        return 0.0f;
    }
}
