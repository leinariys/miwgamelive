package p001a;

import game.script.Character;

import java.util.Date;
import java.util.List;
import java.util.Map;

/* renamed from: a.Es */
/* compiled from: a */
public class C0365Es {
    /* renamed from: u */
    public static boolean m3049u(Class<?> cls) {
        for (Class equals : cls.getInterfaces()) {
            if (equals.equals(List.class)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: v */
    public static boolean m3050v(Class<?> cls) {
        for (Class equals : cls.getInterfaces()) {
            if (equals.equals(Map.class)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: w */
    public static boolean m3051w(Class<?> cls) {
        return cls.isPrimitive();
    }

    /* renamed from: x */
    public static boolean m3052x(Class<?> cls) {
        return cls == Integer.class || cls == Long.class || cls == Short.class || cls == Float.class || cls == Double.class || cls == Byte.class || cls == Character.class || cls == Boolean.class || cls == String.class || cls == Date.class;
    }
}
