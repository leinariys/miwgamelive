package p001a;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.vu */
/* compiled from: a */
public class C3859vu implements C3876wB {
    private static final int FULL = -1;
    private static final int bzm = 0;
    private long bzn;
    private List<C3861b> bzo = new ArrayList();

    /* renamed from: a */
    public void mo22672a(float f, long j, int i) {
        this.bzn = Math.max(j, this.bzn);
        this.bzo.add(new C3861b(j, f * f, i));
    }

    /* renamed from: a */
    public C3876wB.C3877a mo22671a(float f, long j, long j2, long j3) {
        long j4 = j - j3;
        int i = 0;
        boolean z = false;
        float f2 = 0.0f;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.bzo.size()) {
                break;
            }
            C3861b bVar = this.bzo.get(i3);
            if (bVar.radius2 >= f && ((j4 / bVar.fua) * bVar.fua) + j3 > j2) {
                i |= bVar.kind;
                f2 = Math.max(bVar.radius2, f2);
                z = true;
            }
            i2 = i3 + 1;
        }
        if (!z) {
            return null;
        }
        return new C3860a(f2, i, j4 >= this.bzn * 4);
    }

    public void clear() {
        this.bzn = 0;
        this.bzo.clear();
    }

    /* renamed from: a.vu$a */
    private static class C3860a implements C3876wB.C3877a {
        private final int kind;
        private final boolean reset;
        private float radius2;

        public C3860a(float f, int i, boolean z) {
            this.radius2 = f;
            this.kind = i;
            this.reset = z;
        }

        public int bcP() {
            return this.kind;
        }

        public boolean bcQ() {
            return this.reset;
        }

        public float bcR() {
            return this.radius2;
        }
    }

    /* renamed from: a.vu$b */
    /* compiled from: a */
    private static class C3861b {
        public final long fua;
        public final int kind;
        public final float radius2;

        public C3861b(long j, float f, int i) {
            this.fua = j;
            this.radius2 = f;
            this.kind = i;
        }
    }
}
