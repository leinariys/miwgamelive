package p001a;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.xb */
/* compiled from: a */
public @interface ClientOnly {
    /* renamed from: pZ */
    boolean mo22945pZ() default true;
}
