package p001a;

import gnu.trove.THashMap;
import gnu.trove.TObjectObjectProcedure;

import java.util.HashMap;
import java.util.Iterator;

/* renamed from: a.Oy */
/* compiled from: a */
public class C1026Oy {
    private static Class inA;

    static {
        try {
            inA = C1029c.class;
        } catch (Throwable th) {
            inA = C1030d.class;
        }
    }

    private C1026Oy() {
    }

    public static <K, V> C1028b<K, V> diz() {
        try {
            return (C1028b) inA.newInstance();
        } catch (IllegalAccessException e) {
            throw new IllegalStateException(e);
        } catch (InstantiationException e2) {
            throw new IllegalStateException(e2);
        }
    }

    /* renamed from: a.Oy$a */
    public interface C1027a<T> {
        boolean execute(T t);
    }

    /* renamed from: a.Oy$b */
    /* compiled from: a */
    public interface C1028b<K, V> {
        /* renamed from: a */
        boolean mo4579a(C1027a<V> aVar);

        /* renamed from: b */
        boolean mo4580b(C1027a<V> aVar);

        V get(K k);

        V put(K k, V v);

        V remove(K k);

        int size();
    }

    /* renamed from: a.Oy$d */
    /* compiled from: a */
    public static class C1030d<K, V> extends HashMap<K, V> implements C1028b<K, V> {
        public C1030d() {
        }

        /* renamed from: a */
        public boolean mo4579a(C1027a<V> aVar) {
            for (Object execute : values()) {
                if (!aVar.execute(execute)) {
                    return false;
                }
            }
            return true;
        }

        /* renamed from: b */
        public boolean mo4580b(C1027a<V> aVar) {
            boolean z = false;
            Iterator it = values().iterator();
            while (it.hasNext()) {
                if (!aVar.execute(it.next())) {
                    it.remove();
                    z = true;
                }
            }
            return z;
        }
    }

    /* renamed from: a.Oy$c */
    /* compiled from: a */
    public static class C1029c<K, V> extends THashMap<K, V> implements C1028b<K, V> {
        private C1031e<K, V> fXa = new C1031e<>();

        public C1029c() {
        }

        /* renamed from: a */
        public boolean mo4579a(C1027a<V> aVar) {
            this.fXa.iPB = aVar;
            return forEachEntry(this.fXa);
        }

        /* renamed from: b */
        public boolean mo4580b(C1027a<V> aVar) {
            this.fXa.iPB = aVar;
            return retainEntries(this.fXa);
        }
    }

    /* renamed from: a.Oy$e */
    /* compiled from: a */
    public static class C1031e<K, V> implements TObjectObjectProcedure<K, V> {
        public C1027a<V> iPB;

        public C1031e() {
        }

        public boolean execute(K k, V v) {
            return this.iPB.execute(v);
        }
    }
}
