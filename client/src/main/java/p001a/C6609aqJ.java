package p001a;

import game.geometry.Matrix3fWrap;

/* renamed from: a.aqJ  reason: case insensitive filesystem */
/* compiled from: a */
public class C6609aqJ extends C6518aoW<Matrix3fWrap> {
    /* renamed from: g */
    public Matrix3fWrap mo15565g(Matrix3fWrap ajd) {
        Matrix3fWrap ajd2 = (Matrix3fWrap) get();
        ajd2.set(ajd);
        return ajd2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: crw */
    public Matrix3fWrap create() {
        return new Matrix3fWrap();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void copy(Matrix3fWrap ajd, Matrix3fWrap ajd2) {
        ajd.set(ajd2);
    }
}
