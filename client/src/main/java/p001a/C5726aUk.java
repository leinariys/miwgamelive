package p001a;

import com.hoplon.geometry.Vec3f;
import com.hoplon.geometry.Vec3l;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.network.message.C0474GZ;
import game.network.message.externalizable.C1291Sy;
import game.network.message.serializable.C0035AS;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.Character;
import gnu.trove.THashMap;
import logic.baa.C1616Xf;
import logic.res.html.DataClassSerializer;

import java.io.Externalizable;
import java.util.Date;

/* renamed from: a.aUk  reason: case insensitive filesystem */
/* compiled from: a */
public class C5726aUk {
    private static THashMap<Class<?>, Class<? extends DataClassSerializer>> iWG = new THashMap<>();
    private static THashMap<Class<?>, DataClassSerializer> iWH = new THashMap<>();

    static {
        iWG.put(Boolean.TYPE, aNL.class);
        iWG.put(Byte.TYPE, C6230aiu.class);
        iWG.put(Character.TYPE, C6776atU.class);
        iWG.put(Double.TYPE, C6376alk.class);
        iWG.put(Float.TYPE, aOM.class);
        iWG.put(Integer.TYPE, C6743asn.class);
        iWG.put(Class.class, C5496aLo.class);
        iWG.put(String.class, C5342aFq.class);
        iWG.put(Long.TYPE, aBU.class);
        iWG.put(Short.TYPE, C6782ata.class);
        iWG.put(Date.class, C6897avl.class);
        iWG.put(Boolean.class, aNL.class);
        iWG.put(Byte.class, C6230aiu.class);
        iWG.put(Character.class, C6776atU.class);
        iWG.put(Double.class, C6376alk.class);
        iWG.put(Float.class, aOM.class);
        iWG.put(Integer.class, C6743asn.class);
        iWG.put(Long.class, aBU.class);
        iWG.put(Short.class, C6782ata.class);
        iWG.put(String.class, C2562gp.class);
        iWG.put(Quat4fWrap.class, aIL.class);
        iWG.put(Vec3f.class, C1125QY.class);
        iWG.put(Vec3d.class, C2630hm.class);
        iWG.put(Vec3l.class, C6088agI.class);
        iWG.put(C3438ra.class, C0897NB.class);
        iWG.put(C1556Wo.class, CustomMagicDataClassSerializer.class);
        iWG.put(C1291Sy.class, CustomMagicDataClassSerializer.class);
        iWG.put(C2686iZ.class, C3908wc.class);
        iWG.put(C0035AS.class, C3908wc.class);
    }

    /* renamed from: e */
    public static void m18700e(Class<?> cls, Class<? extends DataClassSerializer> cls2) {
        iWG.put(cls, cls2);
    }

    /* renamed from: b */
    public static DataClassSerializer m18699b(Class<?> cls, Class<?>[] clsArr) {
        DataClassSerializer kd;
        Class<Object> cls2;
        Class cls3 = (Class) iWG.get(cls);
        if (cls3 != null) {
            try {
                kd = (DataClassSerializer) cls3.newInstance();
                cls2 = cls;
            } catch (InstantiationException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e2) {
                throw new RuntimeException(e2);
            }
        } else if (cls == null) {
            kd = new DataClassSerializer();
            cls2 = Object.class;
        } else if (C1616Xf.class.isAssignableFrom(cls)) {
            kd = new C0474GZ();
            cls2 = cls;
        } else if (Externalizable.class.isAssignableFrom(cls) && (cls.getModifiers() & 16) != 0 && !cls.isInterface()) {
            kd = new C2717iy();
            cls2 = cls;
        } else if (Enum.class.isAssignableFrom(cls)) {
            kd = new C5580aOu();
            cls2 = cls;
        } else {
            kd = new DataClassSerializer();
            cls2 = cls;
        }
        kd.mo3590a(cls2, clsArr);
        return kd;
    }

    /* renamed from: aQ */
    public static DataClassSerializer m18698aQ(Class<? extends Object> cls) {
        DataClassSerializer kd = (DataClassSerializer) iWH.get(cls);
        if (kd == null) {
            kd = m18699b(cls, (Class<?>[]) null);
            iWH.put(cls, kd);
        }
        if (kd.getClass() != DataClassSerializer.class) {
            return kd;
        }
        return null;
    }
}
