package p001a;

import game.script.item.Item;
import game.script.item.ItemType;
import game.script.item.ItemTypeCategory;
import game.script.newmarket.CommercialOrderInfo;
import game.script.resource.Asset;
import logic.res.ILoaderImageInterface;
import logic.swing.C5378aHa;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;

/* renamed from: a.aUt  reason: case insensitive filesystem */
/* compiled from: a */
public class C5735aUt extends JLabel implements ListCellRenderer, TableCellRenderer, TreeCellRenderer {

    /* renamed from: Th */
    private ILoaderImageInterface f3884Th;

    public C5735aUt(ILoaderImageInterface azb) {
        this.f3884Th = azb;
    }

    public Component getTableCellRendererComponent(JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
        if (obj != null && (obj instanceof CommercialOrderInfo)) {
            CommercialOrderInfo aeu = (CommercialOrderInfo) obj;
            setText(String.valueOf(aeu.mo8620eP().mo19891ke().get()) + " x " + aeu.mo8627ff());
            setIcon(new ImageIcon(this.f3884Th.getImage(String.valueOf(C5378aHa.ITEMS.getPath()) + aeu.mo8620eP().mo12100sK().getHandle())));
            if (z2) {
                setBorder(new LineBorder(Color.YELLOW));
            } else if (z) {
                setBorder(new LineBorder(Color.BLUE));
            } else {
                setBorder((Border) null);
            }
        }
        return this;
    }

    public Component getTreeCellRendererComponent(JTree jTree, Object obj, boolean z, boolean z2, boolean z3, int i, boolean z4) {
        Image image;
        Image image2;
        if (obj != null && (obj instanceof DefaultMutableTreeNode)) {
            DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) obj;
            if (defaultMutableTreeNode.getUserObject() instanceof ItemType) {
                ItemType jCVar = (ItemType) defaultMutableTreeNode.getUserObject();
                setText(jCVar.mo19891ke().get());
                Asset sK = jCVar.mo12100sK();
                if (sK != null) {
                    image2 = this.f3884Th.getImage(String.valueOf(C5378aHa.ITEMS.getPath()) + sK.getHandle());
                } else {
                    image2 = this.f3884Th.getImage(String.valueOf(C5378aHa.ICONOGRAPHY.getPath()) + "button_mini_tutorial_selected");
                }
                setIcon(new ImageIcon(image2));
                if (z4) {
                    setBorder(new LineBorder(Color.YELLOW));
                } else if (z) {
                    setBorder(new LineBorder(Color.BLUE));
                } else {
                    setBorder((Border) null);
                }
            } else if (defaultMutableTreeNode.getUserObject() instanceof ItemTypeCategory) {
                ItemTypeCategory aai = (ItemTypeCategory) defaultMutableTreeNode.getUserObject();
                setText(aai.mo12246ke().get());
                Asset sK2 = aai.mo12100sK();
                if (sK2 != null) {
                    image = this.f3884Th.getImage(String.valueOf(C5378aHa.ICONOGRAPHY.getPath()) + sK2.getHandle());
                } else {
                    image = this.f3884Th.getImage(String.valueOf(C5378aHa.ICONOGRAPHY.getPath()) + "button_mini_tutorial_selected");
                }
                setIcon(new ImageIcon(image));
            }
        }
        return this;
    }

    public Component getListCellRendererComponent(JList jList, Object obj, int i, boolean z, boolean z2) {
        if (obj != null && (obj instanceof Item)) {
            Item auq = (Item) obj;
            setText(auq.bAP().mo19891ke().get());
            setIcon(new ImageIcon(this.f3884Th.getImage(String.valueOf(C5378aHa.ITEMS.getPath()) + auq.bAP().mo12100sK().getHandle())));
            if (z2) {
                setBorder(new LineBorder(Color.YELLOW));
            } else if (z) {
                setBorder(new LineBorder(Color.BLUE));
            } else {
                setBorder((Border) null);
            }
        }
        return this;
    }
}
