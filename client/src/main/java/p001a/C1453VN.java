package p001a;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

/* renamed from: a.VN */
/* compiled from: a */
public class C1453VN {
    private static Random random = new Random();
    URLConnection evH;
    Map evI;
    String evJ;

    /* renamed from: os */
    OutputStream f1865os;

    public C1453VN(URLConnection uRLConnection) {
        this.f1865os = null;
        this.evI = new HashMap();
        this.evJ = "---------------------------" + bCQ() + bCQ() + bCQ();
        this.evH = uRLConnection;
        uRLConnection.setDoOutput(true);
        uRLConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + this.evJ);
    }

    public C1453VN(URL url) {
        this(url.openConnection());
    }

    public C1453VN(String str) {
        this(new URL(str));
    }

    public static String bCQ() {
        return Long.toString(random.nextLong(), 36);
    }

    /* renamed from: a */
    private static void m10590a(InputStream inputStream, OutputStream outputStream) {
        int i = 0;
        byte[] bArr = new byte[500000];
        synchronized (inputStream) {
            while (true) {
                int read = inputStream.read(bArr, 0, bArr.length);
                if (read >= 0) {
                    outputStream.write(bArr, 0, read);
                    i += read;
                }
            }
        }
        outputStream.flush();
        byte[] bArr2 = null;
    }

    /* renamed from: a */
    public static InputStream m10586a(URL url, Map map) {
        return new C1453VN(url).mo6088t(map);
    }

    /* renamed from: a */
    public static InputStream m10588a(URL url, Object[] objArr) {
        return new C1453VN(url).mo6081q(objArr);
    }

    /* renamed from: a */
    public static InputStream m10587a(URL url, Map map, Map map2) {
        return new C1453VN(url).mo6073a(map, map2);
    }

    /* renamed from: a */
    public static InputStream m10589a(URL url, String[] strArr, Object[] objArr) {
        return new C1453VN(url).mo6074a(strArr, objArr);
    }

    /* renamed from: a */
    public static InputStream m10582a(URL url, String str, Object obj) {
        return new C1453VN(url).mo6083s(str, obj);
    }

    /* renamed from: a */
    public static InputStream m10583a(URL url, String str, Object obj, String str2, Object obj2) {
        return new C1453VN(url).mo6070a(str, obj, str2, obj2);
    }

    /* renamed from: a */
    public static InputStream m10584a(URL url, String str, Object obj, String str2, Object obj2, String str3, Object obj3) {
        return new C1453VN(url).mo6071a(str, obj, str2, obj2, str3, obj3);
    }

    /* renamed from: a */
    public static InputStream m10585a(URL url, String str, Object obj, String str2, Object obj2, String str3, Object obj3, String str4, Object obj4) {
        return new C1453VN(url).mo6072a(str, obj, str2, obj2, str3, obj3, str4, obj4);
    }

    /* renamed from: g */
    public static String requestPostArgument(String str, Object[] objArr) {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(m10588a(new URL(str), objArr)));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(decode(readLine.trim()));
            }
        } catch (IOException | MalformedURLException e) {
        }
        return sb.toString();
    }

    public static String encode(String str) {
        return URLEncoder.encode(str);
    }

    public static String decode(String str) {
        return URLDecoder.decode(str, "UTF-8");
    }

    /* renamed from: S */
    public static String m10581S(String str, String str2) {
        String str3 = null;
        if (str.startsWith("http://")) {
            try {
                new StringBuffer();
                if (str2 != null && str2.length() > 0) {
                    str = String.valueOf(str) + "?" + str2;
                }
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new URL(str).openConnection().getInputStream()));
                StringBuffer stringBuffer = new StringBuffer();
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    stringBuffer.append(readLine);
                }
                bufferedReader.close();
                str3 = stringBuffer.toString();
            } catch (Exception e) {
            }
        }
        try {
            return decode(str3);
        } catch (UnsupportedEncodingException e2) {
            return str3;
        }
    }

    /* renamed from: h */
    public static String m10592h(String str, Object[] objArr) {
        String str2;
        String str3 = "";
        int i = 0;
        while (true) {
            try {
                int i2 = i;
                if (i2 >= objArr.length - 1) {
                    break;
                }
                if (str3.length() > 0) {
                    str2 = String.valueOf(str3) + "&";
                } else {
                    str2 = str3;
                }
                try {
                    str3 = String.valueOf(str2) + objArr[i2].toString() + "=" + encode(objArr[i2 + 1]);
                    i = i2 + 2;
                } catch (UnsupportedEncodingException e) {
                    str3 = str2;
                }
            } catch (UnsupportedEncodingException e2) {
            }
        }
        return m10581S(str, str3);
    }

    /* access modifiers changed from: protected */
    public void connect() {
        if (this.f1865os == null) {
            this.f1865os = this.evH.getOutputStream();
        }
    }

    /* access modifiers changed from: protected */
    public void write(char c) {
        connect();
        this.f1865os.write(c);
    }

    /* access modifiers changed from: protected */
    public void write(String str) {
        connect();
        this.f1865os.write(str.getBytes());
    }

    /* access modifiers changed from: protected */
    public void bCP() {
        connect();
        write("\r\n");
    }

    /* access modifiers changed from: protected */
    public void writeln(String str) {
        connect();
        write(str);
        bCP();
    }

    private void bCR() {
        write("--");
        write(this.evJ);
    }

    private void bCS() {
        StringBuffer stringBuffer = new StringBuffer();
        Iterator it = this.evI.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            stringBuffer.append(String.valueOf(entry.getKey().toString()) + "=" + entry.getValue());
            if (it.hasNext()) {
                stringBuffer.append("; ");
            }
        }
        if (stringBuffer.length() > 0) {
            this.evH.setRequestProperty("Cookie", stringBuffer.toString());
        }
    }

    /* renamed from: R */
    public void mo6069R(String str, String str2) {
        this.evI.put(str, str2);
    }

    /* renamed from: r */
    public void mo6082r(Map map) {
        if (map != null) {
            this.evI.putAll(map);
        }
    }

    /* renamed from: i */
    public void mo6080i(String[] strArr) {
        if (strArr != null) {
            for (int i = 0; i < strArr.length - 1; i += 2) {
                mo6069R(strArr[i], strArr[i + 1]);
            }
        }
    }

    /* renamed from: hq */
    private void m10593hq(String str) {
        bCP();
        write("Content-Disposition: form-data; name=\"");
        write(str);
        write('\"');
    }

    public void setParameter(String str, String str2) {
        bCR();
        m10593hq(encode(str));
        bCP();
        bCP();
        writeln(encode(str2));
    }

    /* renamed from: a */
    public void mo6076a(String str, String str2, InputStream inputStream) {
        bCR();
        m10593hq(str);
        write("; filename=\"");
        write(str2);
        write('\"');
        bCP();
        write("Content-Type: ");
        String guessContentTypeFromName = URLConnection.guessContentTypeFromName(str2);
        if (guessContentTypeFromName == null) {
            guessContentTypeFromName = "application/octet-stream";
        }
        writeln(guessContentTypeFromName);
        bCP();
        m10590a(inputStream, this.f1865os);
        bCP();
    }

    /* renamed from: a */
    public void mo6075a(String str, File file) {
        mo6076a(str, file.getPath(), (InputStream) new FileInputStream(file));
    }

    public void setParameter(String str, Object obj) {
        if (obj instanceof File) {
            mo6075a(str, (File) obj);
        } else {
            setParameter(str, obj.toString());
        }
    }

    /* renamed from: s */
    public void mo6084s(Map map) {
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                setParameter(entry.getKey().toString(), entry.getValue());
            }
        }
    }

    public void setParameters(Object[] objArr) {
        if (objArr != null) {
            for (int i = 0; i < objArr.length - 1; i += 2) {
                setParameter(objArr[i].toString(), objArr[i + 1]);
            }
        }
    }

    public InputStream bCT() {
        bCR();
        writeln("--");
        this.f1865os.close();
        return this.evH.getInputStream();
    }

    /* renamed from: t */
    public InputStream mo6088t(Map map) {
        mo6084s(map);
        return bCT();
    }

    /* renamed from: q */
    public InputStream mo6081q(Object[] objArr) {
        setParameters(objArr);
        return bCT();
    }

    /* renamed from: a */
    public InputStream mo6073a(Map map, Map map2) {
        mo6082r(map);
        mo6084s(map2);
        return bCT();
    }

    /* renamed from: a */
    public InputStream mo6074a(String[] strArr, Object[] objArr) {
        mo6080i(strArr);
        setParameters(objArr);
        return bCT();
    }

    /* renamed from: s */
    public InputStream mo6083s(String str, Object obj) {
        setParameter(str, obj);
        return bCT();
    }

    /* renamed from: a */
    public InputStream mo6070a(String str, Object obj, String str2, Object obj2) {
        setParameter(str, obj);
        return mo6083s(str2, obj2);
    }

    /* renamed from: a */
    public InputStream mo6071a(String str, Object obj, String str2, Object obj2, String str3, Object obj3) {
        setParameter(str, obj);
        return mo6070a(str2, obj2, str3, obj3);
    }

    /* renamed from: a */
    public InputStream mo6072a(String str, Object obj, String str2, Object obj2, String str3, Object obj3, String str4, Object obj4) {
        setParameter(str, obj);
        return mo6071a(str2, obj2, str3, obj3, str4, obj4);
    }
}
