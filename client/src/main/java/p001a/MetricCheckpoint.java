package p001a;

import logic.thred.LogPrinter;

/* renamed from: a.Ta */
/* compiled from: a */
public class MetricCheckpoint {
    static LogPrinter log = LogPrinter.m10275K(MetricCheckpoint.class);
    private final String name;
    private int counter;
    private long egN;

    public MetricCheckpoint() {
        this((String) null);
    }

    public MetricCheckpoint(String str) {
        this.name = str;
        reset();
    }

    public void reset() {
        this.egN = System.currentTimeMillis();
        this.counter = 0;
        log.info("Checkpoint " + (this.name != null ? this.name : "") + "#0");
    }

    public void logPrintTime() {
        long currentTimeMillis = System.currentTimeMillis() - this.egN;
        this.egN += currentTimeMillis;
        LogPrinter ur = log;
        StringBuilder append = new StringBuilder("Checkpoint ").append(this.name != null ? this.name : "").append("#");
        int i = this.counter + 1;
        this.counter = i;
        ur.info(append.append(i).append(" reached after ").append(currentTimeMillis).append(" millis").toString());
    }
}
