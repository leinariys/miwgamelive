package p001a;

import game.geometry.Vec3d;
import logic.baa.C1616Xf;
import logic.res.html.DataClassSerializer;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.hm */
/* compiled from: a */
public class C2630hm extends DataClassSerializer {

    /* renamed from: Uj */
    public static final C2630hm f8050Uj = new C2630hm();

    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) {
        if (obj == null) {
            objectOutput.writeBoolean(false);
            return;
        }
        objectOutput.writeBoolean(true);
        Vec3d ajr = (Vec3d) obj;
        objectOutput.writeDouble(ajr.x);
        objectOutput.writeDouble(ajr.y);
        objectOutput.writeDouble(ajr.z);
    }

    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) {
        if (!objectInput.readBoolean()) {
            return null;
        }
        return new Vec3d(objectInput.readDouble(), objectInput.readDouble(), objectInput.readDouble());
    }

    /* renamed from: z */
    public Object mo3598z(Object obj) {
        Vec3d ajr = (Vec3d) obj;
        return new Vec3d(ajr.x, ajr.y, ajr.z);
    }

    /* renamed from: yl */
    public boolean mo3597yl() {
        return true;
    }

    /* renamed from: b */
    public Object mo3591b(C1616Xf xf) {
        return null;
    }
}
