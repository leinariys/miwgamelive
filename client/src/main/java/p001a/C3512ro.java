package p001a;

import game.network.manager.C6546aoy;

@C6546aoy
/* renamed from: a.ro */
/* compiled from: a */
public class C3512ro extends Exception {
    private C3513a bav;

    public C3512ro(String str, C3513a aVar) {
        super(str);
        this.bav = aVar;
    }

    /* renamed from: Yr */
    public C3513a mo21846Yr() {
        return this.bav;
    }

    /* renamed from: a.ro$a */
    public enum C3513a {
        MONEY_NO_BALANCE,
        CRAFT_NO_REQUIRED_BLUEPRINT,
        CRAFT_NO_INGREDIENT,
        CRAFT_NO_RESULT,
        CRAFT_NO_ROOM,
        INSUFICIENT_ITENS,
        GIVE_NO_ROOM
    }
}
