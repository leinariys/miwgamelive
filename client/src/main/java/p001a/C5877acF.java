package p001a;

/* renamed from: a.acF  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5877acF {
    public static final boolean fdJ = false;
    public static final boolean fdK = false;
    public static final String fdL = null;
    public static final String fdM = null;
    public static final boolean RUNNING_CLIENT_BOT = SystemProperty.getBoolean("RUNNING_CLIENT_BOT");
    public static final boolean fdO = (RUNNING_CLIENT_BOT);
    public static final boolean fdP = SystemProperty.getBoolean("bitverse.ai.disable");
    public static final boolean fdQ = false;
    public static final boolean fdR = false;
    public static final boolean fdS = false;
    public static final boolean fdT = false;
    public static final boolean fdU;
    public static final boolean fdV = false;
    public static final boolean fdW = false;
    public static final boolean fdX = false;
    public static final boolean fdY = false;
    public static final boolean fdZ = false;
    public static final boolean fea = false;
    public static final boolean feb = false;
    public static final boolean fec = false;
    public static final boolean fed = false;
    public static final boolean fee = false;
    public static final boolean fef = false;
    public static final boolean feg = SystemProperty.getBoolean("glitchLog.enable");
    public static final boolean feh = SystemProperty.getBoolean("watchdog.enable");
    public static final boolean fei = false;
    public static final boolean fej = false;
    public static final boolean fek = false;
    public static final boolean fel;
    public static final boolean fem = false;
    public static final boolean fen = false;
    public static final boolean feo = false;
    public static final boolean fep = false;
    public static final boolean feq = false;
    public static final boolean fer = false;
    public static final boolean fes = false;
    public static final boolean fet = false;
    public static final boolean feu = false;
    public static final boolean fev = false;

    static {
        boolean z;
        boolean z2 = false;
        if (!RUNNING_CLIENT_BOT) {
            z = false;
        } else {
            z = true;
        }
        fdU = z;
        if (RUNNING_CLIENT_BOT) {
            z2 = true;
        }
        fel = z2;
    }
}
