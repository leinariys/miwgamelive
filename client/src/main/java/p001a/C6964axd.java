package p001a;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;

/* renamed from: a.axd  reason: case insensitive filesystem */
/* compiled from: a */
public class C6964axd extends DefaultMutableTreeNode {
    private boolean gOU;
    private C5519aMl gOV;

    public C6964axd(Object obj, C5519aMl aml) {
        super(obj);
        this.gOV = aml;
    }

    public void add(MutableTreeNode mutableTreeNode) {
        if (!(mutableTreeNode instanceof C6964axd)) {
            throw new RuntimeException("Not supported - Use filtered tree node instead");
        }
        C6964axd.super.add(mutableTreeNode);
    }

    public int cBZ() {
        int i;
        if (cCa()) {
            return getChildCount();
        }
        int childCount = getChildCount();
        if (!this.gOV.mo10166z()) {
            return childCount;
        }
        if (childCount == 0) {
            return 0;
        }
        int i2 = 0;
        int i3 = 0;
        while (i2 < childCount) {
            C6964axd childAt = getChildAt(i2);
            if (this.gOV.mo10165a(childAt.getUserObject())) {
                i = i3 + 1;
            } else {
                i = childAt.cBZ() > 0 ? i3 + 1 : i3;
            }
            i2++;
            i3 = i;
        }
        return i3;
    }

    public boolean cCa() {
        return this.gOU;
    }

    /* renamed from: hd */
    public void mo16807hd(boolean z) {
        this.gOU = z;
    }
}
