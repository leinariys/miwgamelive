package p001a;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

/* renamed from: a.Ol */
/* compiled from: a */
public abstract class C1002Ol extends C7020azh {
    /* access modifiers changed from: protected */
    /* renamed from: H */
    public void mo4489H(Class cls) {
        if (cls.getEnclosingClass() == null || (cls.getModifiers() & 8) != 0) {
            Field[] declaredFields = cls.getDeclaredFields();
            ArrayList arrayList = new ArrayList();
            for (Field field : declaredFields) {
                if (!((field.getModifiers() & 8) == 8 || (field.getModifiers() & 128) == 128)) {
                    arrayList.add(field);
                }
            }
            if (arrayList.size() >= 2) {
                boolean z = false;
                boolean z2 = false;
                boolean z3 = false;
                boolean z4 = false;
                for (Method method : cls.getDeclaredMethods()) {
                    if (m8150a(method)) {
                        z4 = true;
                    } else if (m8151b(method)) {
                        z3 = true;
                    } else if (m8152c(method)) {
                        z2 = true;
                    } else if (m8153d(method)) {
                        z = true;
                    }
                }
                if (z4 && z2) {
                    this.gYz.add("Class '" + cls.getName() + "' declares Serializable and Externalizable serialization methods. " + "Keep Externalizable only");
                } else if (!z3 || !z) {
                    if (!isException(cls) && !Externalizable.class.isAssignableFrom(cls)) {
                        this.gYz.add("Class '" + cls.getName() + "' must extend or implement Externalizable");
                    }
                    if (!isException(cls) && !z4 && !z3 && !m8149I(cls)) {
                        this.gYz.add("Class '" + cls.getName() + "' is missing public default constructor " + "for Externalizable access");
                    }
                    if (!z2 && !z4) {
                        if (isException(cls)) {
                            this.gYz.add("Class '" + cls.getName() + "' is missing serialization method " + "'private void writeObject(ObjectOutputStream out) throws IOException'");
                        } else {
                            this.gYz.add("Class '" + cls.getName() + "' is missing serialization method " + "'public void writeExternal(ObjectOutput out) throws IOException'. " + "Make class implements Externalizable and implement it");
                        }
                    }
                    if (!z && !z3) {
                        if (isException(cls)) {
                            this.gYz.add("Class '" + cls.getName() + "' is missing serialization method " + "'private void readObject(ObjectInputStream out) throws ClassNotFoundException, IOException'");
                        } else {
                            this.gYz.add("Class '" + cls.getName() + "' is missing deserialization method " + "'private void readExternal(ObjectInput in) throws ClassNotFoundException, IOException'. " + "Make class implements Externalizable and implement it");
                        }
                    }
                } else {
                    this.gYz.add("Class '" + cls.getName() + "' declares Serializable and Externalizable deserialization methods. " + "Keep Externalizable only");
                }
            }
        } else {
            this.gYz.add("Class '" + cls.getName() + "' is an innerclass, but not static. It must be static");
        }
    }

    /* renamed from: a */
    private boolean m8150a(Method method) {
        Class<ObjectOutputStream>[] parameterTypes = method.getParameterTypes();
        if (parameterTypes.length != 1) {
            return false;
        }
        for (Class<ObjectOutputStream> cls : parameterTypes) {
            if (cls != ObjectOutputStream.class) {
                return false;
            }
        }
        Class<IOException>[] exceptionTypes = method.getExceptionTypes();
        if (exceptionTypes.length != 1) {
            return false;
        }
        for (Class<IOException> cls2 : exceptionTypes) {
            if (cls2 != IOException.class) {
                return false;
            }
        }
        if (method.getName().equals("writeObject") && (method.getModifiers() & 2) == 2 && (method.getModifiers() & 8) == 0) {
            return true;
        }
        return false;
    }

    /* renamed from: b */
    private boolean m8151b(Method method) {
        Class<ObjectInputStream>[] parameterTypes = method.getParameterTypes();
        if (parameterTypes.length != 1) {
            return false;
        }
        for (Class<ObjectInputStream> cls : parameterTypes) {
            if (cls != ObjectInputStream.class) {
                return false;
            }
        }
        Class<IOException>[] exceptionTypes = method.getExceptionTypes();
        if (exceptionTypes.length != 2) {
            return false;
        }
        for (Class<IOException> cls2 : exceptionTypes) {
            if (cls2 != ClassNotFoundException.class && cls2 != IOException.class) {
                return false;
            }
        }
        if (method.getName().equals("readObject") && (method.getModifiers() & 2) == 2 && (method.getModifiers() & 8) == 0) {
            return true;
        }
        return false;
    }

    /* renamed from: c */
    private boolean m8152c(Method method) {
        Class<ObjectOutput>[] parameterTypes = method.getParameterTypes();
        if (parameterTypes.length != 1) {
            return false;
        }
        for (Class<ObjectOutput> cls : parameterTypes) {
            if (cls != ObjectOutput.class) {
                return false;
            }
        }
        Class<IOException>[] exceptionTypes = method.getExceptionTypes();
        if (exceptionTypes.length != 1) {
            return false;
        }
        for (Class<IOException> cls2 : exceptionTypes) {
            if (cls2 != IOException.class) {
                return false;
            }
        }
        if (method.getName().equals("writeExternal") && (method.getModifiers() & 1) == 1 && (method.getModifiers() & 8) == 0) {
            return true;
        }
        return false;
    }

    /* renamed from: d */
    private boolean m8153d(Method method) {
        Class<ObjectInput>[] parameterTypes = method.getParameterTypes();
        if (parameterTypes.length != 1) {
            return false;
        }
        for (Class<ObjectInput> cls : parameterTypes) {
            if (cls != ObjectInput.class) {
                return false;
            }
        }
        Class<IOException>[] exceptionTypes = method.getExceptionTypes();
        if (exceptionTypes.length != 2) {
            return false;
        }
        for (Class<IOException> cls2 : exceptionTypes) {
            if (cls2 != ClassNotFoundException.class && cls2 != IOException.class) {
                return false;
            }
        }
        if (method.getName().equals("readExternal") && (method.getModifiers() & 1) == 1 && (method.getModifiers() & 8) == 0) {
            return true;
        }
        return false;
    }

    /* renamed from: I */
    private boolean m8149I(Class cls) {
        Constructor[] constructors = cls.getConstructors();
        if (constructors.length == 0) {
            return false;
        }
        for (Constructor parameterTypes : constructors) {
            if (parameterTypes.getParameterTypes().length == 0) {
                return true;
            }
        }
        return false;
    }

    private boolean isException(Class cls) {
        return Exception.class.isAssignableFrom(cls);
    }
}
