package p001a;

import jdbm.helper.Serializer;

/* renamed from: a.asD  reason: case insensitive filesystem */
/* compiled from: a */
public class C6707asD implements Serializer {


    public Object deserialize(byte[] bArr) {
        return Long.valueOf(((((long) bArr[0]) & 255) << 56) | ((((long) bArr[1]) & 255) << 48) | ((((long) bArr[2]) & 255) << 40) | ((((long) bArr[3]) & 255) << 32) | ((((long) bArr[4]) & 255) << 24) | ((((long) bArr[5]) & 255) << 16) | ((((long) bArr[6]) & 255) << 8) | (((long) bArr[7]) & 255));
    }

    public byte[] serialize(Object obj) {
        long longValue = ((Long) obj).longValue();
        return new byte[]{(byte) ((int) (longValue >> 56)), (byte) ((int) (longValue >> 48)), (byte) ((int) (longValue >> 40)), (byte) ((int) (longValue >> 32)), (byte) ((int) (longValue >> 24)), (byte) ((int) (longValue >> 16)), (byte) ((int) (longValue >> 8)), (byte) ((int) longValue)};
    }
}
