package p001a;

import logic.res.KeyCode;
import util.Syst;

import java.io.DataOutput;
import java.io.OutputStream;
import java.io.UTFDataFormatException;

/* renamed from: a.aIB */
/* compiled from: a */
public final class aIB extends OutputStream implements DataOutput {
    static final int[] emM = new int[32];

    static {
        int i = 0;
        int i2 = 0;
        while (i < 32) {
            emM[i] = i2;
            i++;
            i2 = (i2 << 1) | 1;
        }
    }

    int fVh;
    int fVi;
    private byte[] message;
    private int count;

    public aIB() {
        this(16);
    }

    public aIB(int i) {
        this.fVh = -1;
        this.fVi = 0;
        if (i < 0) {
            throw new IllegalArgumentException("Invalid size: " + i);
        }
        this.message = new byte[i];
    }

    public void writeBits(int i, int i2) {
        while (i > 0) {
            if (this.fVi == 0) {
                ensure(this.count + 1);
                this.fVh = this.count;
                this.message[this.count] = 0;
                this.count++;
                this.fVi = 8;
            }
            if (i < this.fVi) {
                byte[] bArr = this.message;
                int i3 = this.fVh;
                bArr[i3] = (byte) (bArr[i3] | ((emM[i] & i2) << (this.fVi - i)));
                this.fVi -= i;
                return;
            } else if (i == this.fVi) {
                byte[] bArr2 = this.message;
                int i4 = this.fVh;
                bArr2[i4] = (byte) (bArr2[i4] | (emM[i] & i2));
                this.fVi = 0;
                return;
            } else {
                byte[] bArr3 = this.message;
                int i5 = this.fVh;
                bArr3[i5] = (byte) (bArr3[i5] | ((i2 >> (i - this.fVi)) & emM[this.fVi]));
                i -= this.fVi;
                this.fVi = 0;
            }
        }
    }

    public final void write(int i) {
        int i2 = this.count + 1;
        ensure(i2);
        this.message[this.count] = (byte) i;
        this.count = i2;
    }

    public final void write(byte[] bArr, int i, int i2) {
        if (i2 != 0) {
            int i3 = this.count + i2;
            ensure(i3);
            System.arraycopy(bArr, i, this.message, this.count, i2);
            this.count = i3;
        }
    }

    private final void ensure(int i) {
        if (i > this.message.length) {
            byte[] bArr = new byte[Math.max(this.message.length << 1, i)];
            System.arraycopy(this.message, 0, bArr, 0, this.count);
            this.message = bArr;
        }
    }

    public final void reset() {
        this.count = 0;
        this.fVh = -1;
        this.fVi = 0;
    }

    public final void seek(int i) {
        this.count = i;
    }

    public final byte[] toByteArray() {
        if (this.message.length == this.count) {
            return this.message;
        }
        byte[] bArr = new byte[this.count];
        System.arraycopy(this.message, 0, bArr, 0, this.count);
        return bArr;
    }

    public final int size() {
        return this.count;
    }

    public final void writeBoolean(boolean z) {
        writeBits(1, z ? 1 : 0);
    }

    public final void writeByte(int i) {
        write(i);
    }

    /* renamed from: sn */
    private final void m15353sn(int i) {
        write(i);
    }

    /* renamed from: so */
    private final void m15354so(int i) {
        int i2 = this.count + 2;
        ensure(i2);
        this.message[this.count] = (byte) ((i >>> 8) & 255);
        this.message[this.count + 1] = (byte) ((i >>> 0) & 255);
        this.count = i2;
    }

    /* renamed from: sp */
    private void m15355sp(int i) {
        int i2 = this.count + 3;
        ensure(i2);
        this.message[this.count] = (byte) ((i >>> 16) & 255);
        this.message[this.count + 1] = (byte) ((i >>> 8) & 255);
        this.message[this.count + 2] = (byte) ((i >>> 0) & 255);
        this.count = i2;
    }

    /* renamed from: hP */
    private void m15349hP(long j) {
        int i = this.count + 5;
        ensure(i);
        this.message[this.count] = (byte) ((int) (j >>> 32));
        this.message[this.count + 1] = (byte) ((int) (j >>> 24));
        this.message[this.count + 2] = (byte) ((int) (j >>> 16));
        this.message[this.count + 3] = (byte) ((int) (j >>> 8));
        this.message[this.count + 4] = (byte) ((int) (j >>> 0));
        this.count = i;
    }

    /* renamed from: hQ */
    private void m15350hQ(long j) {
        int i = this.count + 6;
        ensure(i);
        this.message[this.count] = (byte) ((int) (j >>> 40));
        this.message[this.count + 1] = (byte) ((int) (j >>> 32));
        this.message[this.count + 2] = (byte) ((int) (j >>> 24));
        this.message[this.count + 3] = (byte) ((int) (j >>> 16));
        this.message[this.count + 4] = (byte) ((int) (j >>> 8));
        this.message[this.count + 5] = (byte) ((int) (j >>> 0));
        this.count = i;
    }

    /* renamed from: hR */
    private void m15351hR(long j) {
        int i = this.count + 7;
        ensure(i);
        this.message[this.count] = (byte) ((int) (j >>> 48));
        this.message[this.count + 1] = (byte) ((int) (j >>> 40));
        this.message[this.count + 2] = (byte) ((int) (j >>> 32));
        this.message[this.count + 3] = (byte) ((int) (j >>> 24));
        this.message[this.count + 4] = (byte) ((int) (j >>> 16));
        this.message[this.count + 5] = (byte) ((int) (j >>> 8));
        this.message[this.count + 6] = (byte) ((int) (j >>> 0));
        this.count = i;
    }

    public final void writeShort(int i) {
        short s = (short) i;
        if (s < 0) {
            s = (short) (-s);
            writeBits(1, 1);
        } else {
            writeBits(1, 0);
        }
        if (s <= 255) {
            writeBits(1, 0);
            m15353sn(s);
            return;
        }
        writeBits(1, 1);
        m15354so(s);
    }

    public final void writeChar(int i) {
        int i2 = this.count + 2;
        ensure(i2);
        this.message[this.count] = (byte) ((i >>> 8) & 255);
        this.message[this.count + 1] = (byte) ((i >>> 0) & 255);
        this.count = i2;
    }

    /* renamed from: sq */
    public final void mo9309sq(int i) {
        int i2 = this.count + 4;
        ensure(i2);
        this.message[this.count] = (byte) ((i >>> 24) & 255);
        this.message[this.count + 1] = (byte) ((i >>> 16) & 255);
        this.message[this.count + 2] = (byte) ((i >>> 8) & 255);
        this.message[this.count + 3] = (byte) ((i >>> 0) & 255);
        this.count = i2;
    }

    public final void writeInt(int i) {
        if (i < 0) {
            i = -i;
            writeBits(1, 1);
        } else {
            writeBits(1, 0);
        }
        if (i <= 255) {
            writeBits(2, 0);
            m15353sn(i);
        } else if (i <= 65535) {
            writeBits(2, 1);
            m15354so(i);
        } else if (i <= 16777215) {
            writeBits(2, 2);
            m15355sp(i);
        } else {
            writeBits(2, 3);
            mo9309sq(i);
        }
    }

    /* renamed from: hS */
    private final void m15352hS(long j) {
        int i = this.count + 8;
        ensure(i);
        this.message[this.count] = (byte) ((int) (j >>> 56));
        this.message[this.count + 1] = (byte) ((int) (j >>> 48));
        this.message[this.count + 2] = (byte) ((int) (j >>> 40));
        this.message[this.count + 3] = (byte) ((int) (j >>> 32));
        this.message[this.count + 4] = (byte) ((int) (j >>> 24));
        this.message[this.count + 5] = (byte) ((int) (j >>> 16));
        this.message[this.count + 6] = (byte) ((int) (j >>> 8));
        this.message[this.count + 7] = (byte) ((int) (j >>> 0));
        this.count = i;
    }

    public final void writeLong(long j) {
        if (j < 0) {
            j = -j;
            writeBits(1, 1);
        } else {
            writeBits(1, 0);
        }
        if (j <= 255) {
            writeBits(3, 0);
            m15353sn((byte) ((int) j));
        } else if (j <= 65535) {
            writeBits(3, 1);
            m15354so((short) ((int) j));
        } else if (j <= 16777215) {
            writeBits(3, 2);
            m15355sp((int) j);
        } else if (j <= -1) {
            writeBits(3, 3);
            mo9309sq((int) j);
        } else if (j <= 1099511627775L) {
            writeBits(3, 4);
            m15349hP(j);
        } else if (j <= 281474976710655L) {
            writeBits(3, 5);
            m15350hQ(j);
        } else if (j <= 72057594037927935L) {
            writeBits(3, 6);
            m15351hR(j);
        } else {
            writeBits(3, 7);
            m15352hS(j);
        }
    }

    public final void writeFloat(float f) {
        mo9309sq(Float.floatToIntBits(f));
    }

    public final void writeDouble(double d) {
        m15352hS(Double.doubleToLongBits(d));
    }

    public final void writeBytes(String str) {
        int length = str.length();
        ensure(this.count + length);
        int i = this.count;
        int i2 = 0;
        while (i2 < length) {
            this.message[i] = (byte) str.charAt(i2);
            i2++;
            i++;
        }
        this.count = i;
    }

    public final void writeChars(String str) {
        int length = str.length();
        ensure(this.count + (length << 1));
        int i = this.count;
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            int i3 = i + 1;
            this.message[i] = (byte) ((charAt >>> 8) & 255);
            i = i3 + 1;
            this.message[i3] = (byte) ((charAt >>> 0) & 255);
        }
        this.count = i;
    }

    public final void writeUTF(String str) {
        int i;
        int i2;
        int length = str.length();
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4++) {
            char charAt = str.charAt(i4);
            if (charAt >= 1 && charAt <= 127) {
                i3++;
            } else if (charAt > 2047) {
                i3 += 3;
            } else {
                i3 += 2;
            }
        }
        if (i3 > 65535) {
            throw new UTFDataFormatException("encoded string too long: " + i3 + " bytes");
        }
        writeShort(i3);
        ensure(i3 + this.count);
        int i5 = this.count;
        int i6 = 0;
        while (true) {
            if (i6 >= length) {
                i = i6;
                break;
            }
            char charAt2 = str.charAt(i6);
            if (charAt2 < 1) {
                i = i6;
                break;
            } else if (charAt2 > 127) {
                i = i6;
                break;
            } else {
                this.message[i5] = (byte) charAt2;
                i6++;
                i5++;
            }
        }
        while (i < length) {
            char charAt3 = str.charAt(i);
            if (charAt3 >= 1 && charAt3 <= 127) {
                i2 = i5 + 1;
                this.message[i5] = (byte) charAt3;
            } else if (charAt3 > 2047) {
                int i7 = i5 + 1;
                this.message[i5] = (byte) (((charAt3 >> 12) & 15) | KeyCode.ctb);
                int i8 = i7 + 1;
                this.message[i7] = (byte) (((charAt3 >> 6) & 63) | 128);
                i2 = i8 + 1;
                this.message[i8] = (byte) ((charAt3 & '?') | 128);
            } else {
                int i9 = i5 + 1;
                this.message[i5] = (byte) (((charAt3 >> 6) & 31) | 192);
                i2 = i9 + 1;
                this.message[i9] = (byte) ((charAt3 & '?') | 128);
            }
            i++;
            i5 = i2;
        }
        this.count = i5;
    }

    public final int getCount() {
        return this.count;
    }

    public final void writeTo(OutputStream outputStream) {
        outputStream.write(this.message, 0, this.count);
    }

    public final byte[] getMessage() {
        return this.message;
    }

    public String toString() {
        return String.valueOf(Syst.m15900a(this.message, " ", 32)) + "\n" + Syst.m15904b(this.message, "utf-8").replaceAll("[\\x00-\\x08]", "?");
    }
}
