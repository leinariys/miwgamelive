package p001a;

import com.hoplon.geometry.Vec3l;
import game.geometry.Quat4fWrap;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import game.geometry.aLH;
import gnu.trove.THashMap;
import logic.aaa.C2235dB;
import logic.bbb.C4029yK;
import logic.thred.C1362Tq;
import logic.thred.C6615aqP;
import logic.thred.LogPrinter;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: a.jS */
/* compiled from: a */
public class C2748jS<E extends C2235dB, B extends C0461GN> implements C3055nX.C3056a<E, B>, C2235dB, C6615aqP {
    private static final LogPrinter logger = LogPrinter.m10275K(C2748jS.class);
    public final aLH aabb = new aLH();
    final Vec3d position;
    private final C6585apl<List<C5537aNd<E>>> apH = new C2507fy(this);
    private final C3600sn<aJG<B>> apI = new C3600sn<>();
    private final C3752uZ<C2748jS> apJ = new C3752uZ<>();
    private final aEY<E, B> apM;
    /* renamed from: id */
    private final int f8314id;
    aRR apK;
    aRR apL;
    float apN = 0.0f;
    boolean apO;
    private float apP = Float.MAX_VALUE;
    private Map<Object, Object> apQ;
    private Object apR;
    private long lastUpdate;
    private int tag = -1;

    public C2748jS(aEY aey, Vec3d ajr) {
        this.apM = aey;
        int i = aey.nextId + 1;
        aey.nextId = i;
        this.f8314id = i;
        this.position = ajr;
        this.aabb.mo9840aE(ajr);
        this.aabb.mo9839aD(ajr);
    }

    /* renamed from: cy */
    public List<C0463GP<E>> mo11195cy(int i) {
        if (isDisposed()) {
            return Collections.emptyList();
        }
        return this.apH.get(i);
    }

    /* renamed from: a */
    public void mo19944a(aJG<B> ajg) {
        float in = ajg.mo9466in();
        int size = this.apI.size();
        this.apI.add(ajg);
        if (this.apN < in || size == 0) {
            this.apN = in;
            m33999II();
        }
        m33998IH();
    }

    /* renamed from: a */
    public void mo19947a(C2748jS<E, B> jSVar) {
        this.apJ.put(Vec3d.m15655p(jSVar.position, this.position), jSVar);
        m33998IH();
    }

    /* renamed from: IH */
    private void m33998IH() {
        float eV;
        if (this.apI.size() != 0) {
            eV = 0.0f;
        } else {
            eV = this.apJ.size() > 0 ? this.apJ.mo22431eV(0) : Float.MAX_VALUE;
        }
        if (eV != this.apP) {
            this.apM.mo8633a(this.apP, eV, this);
            this.apP = eV;
        }
    }

    /* renamed from: b */
    public void mo19950b(C2748jS jSVar) {
        if (this.apJ.removeElement(jSVar) != null) {
            m33998IH();
        }
    }

    /* renamed from: b */
    public void mo19948b(aJG<B> ajg) {
        float f = 0.0f;
        this.apI.remove(ajg);
        if (!isEmpty() || this.apO) {
            if (this.apI.size() > 0) {
                f = ((aJG) this.apI.get(this.apI.size() - 1)).mo9466in();
            }
            if (this.apN > f) {
                this.apN = f;
                m33999II();
            }
        } else {
            this.apN = 0.0f;
            this.apM.hIm.add(this);
            m33999II();
        }
        m33998IH();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo19945a(aJG<B> ajg, float f) {
        this.apI.remove(ajg);
        ajg.mo9467jD(f);
        this.apI.add(ajg);
        float in = this.apI.size() > 0 ? ((aJG) this.apI.get(this.apI.size() - 1)).mo9466in() : 0.0f;
        if (this.apN > in) {
            this.apN = in;
            m33999II();
        }
    }

    /* renamed from: II */
    private void m33999II() {
        float f = this.apN;
        double d = (double) (f > 0.0f ? this.apM.hIx + f : 1.0E-9f);
        double d2 = (double) (f > 0.0f ? this.apM.hIx + f : 1.0E-9f);
        double d3 = (double) (f > 0.0f ? this.apM.hIx + f : 1.0E-9f);
        this.aabb.mo9824A(this.position.x + d, this.position.y + d2, this.position.z + d3);
        this.aabb.mo9825B(this.position.x - d, this.position.y - d2, this.position.z - d3);
        if (this.apL != null) {
            this.apL.mo1234c(this.aabb);
        } else if (f > 0.0f) {
            this.apL = this.apM.fOr.mo1225a(this.aabb, (C6615aqP) this);
        }
    }

    /* renamed from: a */
    public void mo19946a(C5537aNd<E> and) {
        int size = this.apH.size();
        while (true) {
            int i = size - 1;
            if (i < 0) {
                break;
            }
            List tu = this.apH.mo15491tu(i);
            if (tu != null) {
                tu.remove(and);
            }
            size = i;
        }
        if (!this.apO && isEmpty()) {
            this.apM.hIm.add(this);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isEmpty() {
        if (this.apI.size() > 0) {
            return false;
        }
        if (this.apH.size() == 0) {
            return true;
        }
        int size = this.apH.size();
        while (true) {
            int i = size - 1;
            if (i < 0) {
                return true;
            }
            List tu = this.apH.mo15491tu(i);
            if (tu != null && tu.size() > 0) {
                return false;
            }
            size = i;
        }
    }

    /* renamed from: b */
    public void mo19949b(C5537aNd<E> and) {
        int i = and.irp;
        int i2 = 0;
        while (i != 0) {
            if ((i & 1) != 0) {
                this.apH.get(i2).add(and);
            }
            i >>= 1;
            i2++;
        }
    }

    public int getTag() {
        return this.tag;
    }

    public void setTag(int i) {
        this.tag = i;
    }

    public Vec3d getPosition() {
        return this.position;
    }

    /* renamed from: a */
    public void mo11193a(float f, Set<C6625aqZ> set) {
        C3600sn<aJG<B>> snVar = this.apI;
        if (isDisposed()) {
            return;
        }
        if (snVar.size() != 0 || this.apJ.size() != 0) {
            int size = snVar.size();
            for (int i = 0; i < size; i++) {
                try {
                    set.addAll(((C0461GN) ((aJG) snVar.get(i)).mo2340kP()).aRu());
                } catch (ThreadDeath e) {
                    throw e;
                } catch (Throwable th) {
                    logger.error("Ignoring error getting beholders at voxel: " + this, th);
                }
            }
            int dQ = this.apJ.mo22430dQ(f);
            for (int i2 = 0; i2 < dQ; i2++) {
                C2748jS elementAt = this.apJ.elementAt(i2);
                float aC = this.position.mo9486aC(elementAt.position);
                C3600sn<aJG<B>> snVar2 = elementAt.apI;
                int size2 = snVar2.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    try {
                        aJG ajg = (aJG) snVar2.get(i3);
                        if (ajg.dfH() >= ((C0461GN) ajg.mo2340kP()).getPosition().mo9486aC(this.position)) {
                            set.addAll(((C0461GN) ajg.mo2340kP()).aRu());
                        } else if (ajg.dfH() < aC) {
                            break;
                        }
                    } catch (ThreadDeath e2) {
                        throw e2;
                    } catch (Throwable th2) {
                        logger.error("Ignoring error getting beholders at voxel: " + this, th2);
                    }
                }
            }
        }
    }

    public void dispose() {
        int size = this.apH.size();
        while (true) {
            int i = size - 1;
            if (i < 0) {
                this.apH.clear();
                this.apI.clear();
                this.apK = null;
                return;
            }
            List tu = this.apH.mo15491tu(i);
            if (tu != null) {
                tu.clear();
            }
            size = i;
        }
    }

    public boolean isDisposed() {
        return this.apK == null;
    }

    public int hashCode() {
        return this.f8314id;
    }

    public String toString() {
        return "Voxel:" + this.f8314id + " [" + ((long) (this.position.x * this.apM.hIp)) + "," + ((long) (this.position.y * this.apM.hIq)) + "," + ((long) (this.position.z * this.apM.hIr)) + "] " + this.position + " ey:" + this.apI.size() + " ob:" + this.apJ.size() + " slots:" + this.apH + " aabb: " + this.aabb;
    }

    /* renamed from: IJ */
    public String mo19942IJ() {
        return String.valueOf(this.f8314id) + ":" + "[" + ((long) (this.position.x * this.apM.hIp)) + "," + ((long) (this.position.y * this.apM.hIq)) + "," + ((long) (this.position.z * this.apM.hIr)) + "]";
    }

    /* renamed from: IK */
    public Vec3l mo19943IK() {
        return new Vec3l((long) (this.position.x * this.apM.hIp), (long) (this.position.y * this.apM.hIq), (long) (this.position.z * this.apM.hIr));
    }

    /* renamed from: B */
    public <T> T mo11189B(Object obj) {
        if (obj == null) {
            return this.apR;
        }
        if (this.apQ == null) {
            return null;
        }
        return this.apQ.get(obj);
    }

    /* renamed from: e */
    public void mo11196e(Object obj, Object obj2) {
        if (obj == null) {
            this.apR = obj2;
        }
        if (this.apQ == null) {
            if (obj2 != null) {
                this.apQ = new THashMap();
            } else {
                return;
            }
        }
        this.apQ.put(obj, obj2);
    }

    public Quat4fWrap getOrientation() {
        return null;
    }

    /* renamed from: IL */
    public C4029yK mo2437IL() {
        return null;
    }

    /* renamed from: IM */
    public byte mo2438IM() {
        return (byte) (this.apI.size() == 0 ? 0 : 1);
    }

    public boolean isStatic() {
        return false;
    }

    /* renamed from: a */
    public void mo2543a(C1362Tq tq) {
    }

    /* renamed from: IN */
    public float mo11190IN() {
        return this.apP;
    }

    public int getId() {
        return this.f8314id;
    }

    /* renamed from: IO */
    public long mo11191IO() {
        return this.lastUpdate;
    }

    /* renamed from: bZ */
    public void mo11194bZ(long j) {
        this.lastUpdate = j;
    }

    /* renamed from: IP */
    public List<List<C0463GP<E>>> mo11192IP() {
        if (isDisposed()) {
            return Collections.emptyList();
        }
        return this.apH;
    }

    /* renamed from: IQ */
    public TransformWrap mo2439IQ() {
        return null;
    }

    /* renamed from: IR */
    public void mo2440IR() {
    }

    /* renamed from: IS */
    public Object mo2441IS() {
        return this;
    }

    public boolean isReady() {
        return true;
    }
}
