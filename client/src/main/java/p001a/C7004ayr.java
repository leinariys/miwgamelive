package p001a;

/* renamed from: a.ayr  reason: case insensitive filesystem */
/* compiled from: a */
public class C7004ayr extends RuntimeException {


    public C7004ayr(Throwable th) {
        super(th);
    }

    public C7004ayr(String str, Throwable th) {
        super(str, th);
    }

    public C7004ayr(String str) {
        super(str);
    }
}
