package p001a;

import game.network.channel.client.ClientConnect;
import game.network.message.Blocking;
import game.network.message.C5581aOv;
import game.network.message.externalizable.*;
import gnu.trove.THashMap;
import gnu.trove.THashSet;
import gnu.trove.TIntObjectHashMap;
import logic.baa.*;
import logic.data.mbean.C0677Jd;
import logic.data.mbean.C5439aJj;
import logic.data.mbean.C6064afk;
import logic.res.code.C5663aRz;
import logic.res.html.C2491fm;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import taikodom.addon.C6144ahM;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* renamed from: a.TD */
/* compiled from: a */
public class C1298TD extends C3582se {
    public static final int hFl = 5000;
    private static final Log logger = LogPrinter.setClass(C1298TD.class);
    public Map<C5663aRz, Set<C5473aKr>> hFm = null;
    public Map<C5663aRz, Set<C6200aiQ>> hFn = null;
    ConcurrentMap<Integer, C1302d> hFr;
    private TIntObjectHashMap<C6144ahM<Object>> hFo;
    private long hFp = 0;
    private boolean hFq = true;

    /* renamed from: t */
    public static C1298TD m9830t(aDJ adj) {
        return (C1298TD) adj.bFf();
    }

    /* renamed from: a */
    public C6302akO mo5593a(ClientConnect bVar, C5703aTn atn) {
        Object a;
        if (mo6866PM().bGM()) {
            return null;
        }
        C0495Gr dvF = atn.dvF();
        if (atn.isBlocking() || !bGw()) {
            a = mo6901yn().mo14a(dvF);
        } else {
            mo21998f(new C1301c(dvF));
            a = null;
        }
        if (atn instanceof C2390ek) {
            this.aGA.bGU().mo15549b(bVar, (Blocking) new C0626Is(mo6901yn(), ((C2390ek) atn).getId(), a, (List<Blocking>) null));
            return null;
        } else if (atn.isBlocking()) {
            return new C6212aic(a, (List<Blocking>) null);
        } else {
            return null;
        }
    }

    /* renamed from: dq */
    public final C6064afk mo5608dq() {
        this.hFp = this.aGA.currentTimeMillis();
        if (!this.hFq) {
            this.hFq = true;
            this.aGA.bGU().mo15549b(mo6892hE(), (Blocking) new C0169Bw(mo6901yn(), this.hCm.aBt()));
        }
        return this.hCm;
    }

    /* renamed from: b */
    public C6302akO mo5601b(C5581aOv aov) {
        if (cXw()) {
            this.hFq = false;
            this.aGA.bGU().mo15549b(mo6892hE(), (Blocking) new C6678ara(mo6901yn()));
        }
        if (this.hCm.mo3202hF()) {
            logger.debug("Received update for a hollow object: " + getObjectId().getId() + " - " + mo6901yn().getClass().getName());
        } else {
            C0677Jd Wo = ((C3213pH) aov).mo21129Wo();
            boolean hF = this.hCm.mo3202hF();
            Wo.mo3188c((C0677Jd) this.hCm);
            C6064afk afk = this.hCm;
            if (mo6901yn() instanceof C4072yv) {
                C4072yv yvVar = (C4072yv) mo6901yn();
                for (C5663aRz arz : mo6901yn().mo25c()) {
                    if (!yvVar.mo18315e(arz)) {
                        Wo.mo3193d(arz, afk.mo3199g(arz));
                    }
                }
            }
            Wo.mo3146A(false);
            this.hCm = Wo;
            bFj();
            if (bGw()) {
                mo21998f(new C1300b(hF, afk, Wo));
            } else {
                mo21972a(hF, (C5439aJj) null, afk, Wo);
            }
        }
        return null;
    }

    private boolean cXw() {
        return false;
    }

    /* renamed from: b */
    public final Object mo5602b(C5663aRz arz) {
        C6064afk dq = mo5608dq();
        if (dq.mo3202hF()) {
            mo5609ds();
            dq = mo5608dq();
        }
        return dq.mo3199g(arz);
    }

    /* renamed from: a */
    public final void mo5599a(C5663aRz arz, Object obj) {
        C6064afk dq = mo5608dq();
        if (dq.mo3202hF()) {
            mo5609ds();
            dq = mo5608dq();
        }
        if (mo6901yn().isUseClientOnly()) {
            dq.mo3193d(arz, obj);
        } else {
            dq.mo3195e(arz, obj);
        }
    }

    /* renamed from: a */
    public final void mo5600a(Class<? extends C4062yl> cls, C0495Gr gr) {
        aDJ adj = (aDJ) mo6901yn();
        Collection<?> aJ = adj.mo8323aJ(cls);
        Collection<?> aL = adj.mo8324aL(cls);
        if (aJ == null || this.hCr.get() != null) {
            if (aL != null) {
                mo6901yn().mo15a(new ArrayList(aL), gr);
            }
        } else if (aL != null) {
            ArrayList arrayList = new ArrayList(aL.size() + aJ.size());
            arrayList.addAll(aJ);
            arrayList.addAll(aL);
            mo6901yn().mo15a(arrayList, gr);
        } else {
            mo6901yn().mo15a(new ArrayList(aJ), gr);
        }
    }

    /* renamed from: dj */
    public boolean mo5607dj() {
        return false;
    }

    public void push() {
        throw new IllegalAccessError();
    }

    /* renamed from: a */
    public void mo5595a(aDJ adj) {
    }

    /* access modifiers changed from: protected */
    public boolean hasListeners() {
        return (this.hFn != null && this.hFn.size() > 0) || (this.hFm != null && this.hFm.size() > 0);
    }

    /* renamed from: a */
    public void mo5598a(C5663aRz arz, C6200aiQ aiq) {
        cVl();
        if (aiq == null) {
            throw new IllegalArgumentException("Null field listener is being registered");
        }
        if (this.hFn == null) {
            this.hFn = new THashMap();
        }
        synchronized (this.hFn) {
            THashSet tHashSet = (Set) this.hFn.get(arz);
            if (tHashSet == null) {
                tHashSet = new THashSet();
                this.hFn.put(arz, tHashSet);
            }
            tHashSet.add(aiq);
        }
    }

    /* renamed from: b */
    public void mo5604b(C5663aRz arz, C6200aiQ aiq) {
        cVl();
        if (this.hFn != null) {
            synchronized (this.hFn) {
                Set set = this.hFn.get(arz);
                if (set != null) {
                    set.remove(aiq);
                    if (set.isEmpty()) {
                        this.hFn.remove(arz);
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0023, code lost:
        r2 = r2.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x002b, code lost:
        if (r2.hasNext() == false) goto L_0x0006;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x002d, code lost:
        ((logic.baa.C6200aiQ) r2.next()).mo1143a((logic.baa.aDJ) mo6901yn(), r4, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* renamed from: j */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo5611j(C5663aRz r4, java.lang.Object r5) {
        /*
            r3 = this;
            boolean r0 = r3.bGX()
            if (r0 != 0) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            java.util.Map<a.aRz, java.util.Set<a.aiQ>> r0 = r3.hFn
            if (r0 == 0) goto L_0x0006
            java.util.Map<a.aRz, java.util.Set<a.aiQ>> r1 = r3.hFn
            monitor-enter(r1)
            java.util.Map<a.aRz, java.util.Set<a.aiQ>> r0 = r3.hFn     // Catch:{ all -> 0x001a }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ all -> 0x001a }
            java.util.Set r0 = (java.util.Set) r0     // Catch:{ all -> 0x001a }
            if (r0 != 0) goto L_0x001d
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            goto L_0x0006
        L_0x001a:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            throw r0
        L_0x001d:
            java.util.LinkedList r2 = new java.util.LinkedList     // Catch:{ all -> 0x001a }
            r2.<init>(r0)     // Catch:{ all -> 0x001a }
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            java.util.Iterator r2 = r2.iterator()
        L_0x0027:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0006
            java.lang.Object r0 = r2.next()
            r1 = r0
            a.aiQ r1 = (logic.baa.C6200aiQ) r1
            a.Xf r0 = r3.mo6901yn()
            a.aDJ r0 = (logic.baa.aDJ) r0
            r1.mo1143a(r0, r4, r5)
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C1298TD.mo5611j(a.aRz, java.lang.Object):void");
    }

    /* renamed from: a */
    public void mo5596a(C5663aRz arz, C5473aKr akr) {
        cVl();
        if (akr == null) {
            throw new IllegalArgumentException("Null field listener is being registered");
        }
        if (this.hFm == null) {
            this.hFm = new THashMap();
        }
        synchronized (this.hFm) {
            THashSet tHashSet = (Set) this.hFm.get(arz);
            if (tHashSet == null) {
                tHashSet = new THashSet();
                this.hFm.put(arz, tHashSet);
            }
            tHashSet.add(akr);
        }
    }

    /* renamed from: b */
    public void mo5603b(C5663aRz arz, C5473aKr akr) {
        cVl();
        if (this.hFm != null) {
            synchronized (this.hFm) {
                Set set = this.hFm.get(arz);
                if (set != null) {
                    set.remove(akr);
                    if (set.isEmpty()) {
                        this.hFm.remove(arz);
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0023, code lost:
        r3 = mo21974a(r7, r8, r6);
        r4 = mo21974a(r8, r7, r6);
        r2 = r2.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0033, code lost:
        if (r2.hasNext() == false) goto L_0x0006;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0035, code lost:
        r0 = (logic.baa.C5473aKr) r2.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003b, code lost:
        if (r7 == null) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003e, code lost:
        if (r3.length <= 0) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0040, code lost:
        r0.mo1102b((logic.baa.aDJ) mo6901yn(), r6, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0049, code lost:
        if (r8 == null) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x004c, code lost:
        if (r4.length <= 0) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x004e, code lost:
        r0.mo1101a((logic.baa.aDJ) mo6901yn(), r6, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo5597a(C5663aRz r6, C6064afk r7, C6064afk r8) {
        /*
            r5 = this;
            boolean r0 = r5.bGX()
            if (r0 != 0) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            java.util.Map<a.aRz, java.util.Set<a.aKr>> r0 = r5.hFm
            if (r0 == 0) goto L_0x0006
            java.util.Map<a.aRz, java.util.Set<a.aKr>> r1 = r5.hFm
            monitor-enter(r1)
            java.util.Map<a.aRz, java.util.Set<a.aKr>> r0 = r5.hFm     // Catch:{ all -> 0x001a }
            java.lang.Object r0 = r0.get(r6)     // Catch:{ all -> 0x001a }
            java.util.Set r0 = (java.util.Set) r0     // Catch:{ all -> 0x001a }
            if (r0 != 0) goto L_0x001d
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            goto L_0x0006
        L_0x001a:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            throw r0
        L_0x001d:
            gnu.trove.THashSet r2 = new gnu.trove.THashSet     // Catch:{ all -> 0x001a }
            r2.<init>(r0)     // Catch:{ all -> 0x001a }
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            java.lang.Object[] r3 = r5.mo21974a((logic.data.mbean.C6064afk) r7, (logic.data.mbean.C6064afk) r8, (logic.res.code.C5663aRz) r6)
            java.lang.Object[] r4 = r5.mo21974a((logic.data.mbean.C6064afk) r8, (logic.data.mbean.C6064afk) r7, (logic.res.code.C5663aRz) r6)
            java.util.Iterator r2 = r2.iterator()
        L_0x002f:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0006
            java.lang.Object r0 = r2.next()
            a.aKr r0 = (logic.baa.C5473aKr) r0
            if (r7 == 0) goto L_0x0049
            int r1 = r3.length
            if (r1 <= 0) goto L_0x0049
            a.Xf r1 = r5.mo6901yn()
            a.aDJ r1 = (logic.baa.aDJ) r1
            r0.mo1102b(r1, r6, r3)
        L_0x0049:
            if (r8 == 0) goto L_0x002f
            int r1 = r4.length
            if (r1 <= 0) goto L_0x002f
            a.Xf r1 = r5.mo6901yn()
            a.aDJ r1 = (logic.baa.aDJ) r1
            r0.mo1101a(r1, r6, r4)
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C1298TD.mo5597a(a.aRz, a.afk, a.afk):void");
    }

    private TIntObjectHashMap<C6144ahM<Object>> cXx() {
        if (this.hFo == null) {
            this.hFo = new TIntObjectHashMap<>();
        }
        return this.hFo;
    }

    /* renamed from: a */
    public Object mo5594a(C0495Gr gr, C6144ahM<Object> ahm) {
        C2491fm aQK = gr.aQK();
        if (bGZ()) {
            throw new IllegalStateException("Invalid async call at server side: " + gr);
        }
        if (aQK.mo4791pw()) {
            if (!mo21973a((C1619Xi<C1616Xf>) new C1619Xi(), gr)) {
                ahm.mo1931n(gr.aQK().mo4768pN());
                return gr.aQK().mo4768pN();
            } else if (aQK.isBlocking()) {
                m9828b(gr, ahm);
            } else {
                ahm.mo1931n(gr.aQK().mo4768pN());
            }
        }
        return gr.aQK().mo4768pN();
    }

    /* renamed from: b */
    private void m9828b(C0495Gr gr, C6144ahM<Object> ahm) {
        long qa = gr.aQK().mo4795qa();
        if (qa != -1) {
            long currentTimeMillis = currentTimeMillis();
            if (this.hFr == null) {
                this.hFr = new ConcurrentHashMap();
            }
            C1302d dVar = (C1302d) this.hFr.get(Integer.valueOf(gr.mo2417hq()));
            if (dVar == null || currentTimeMillis - dVar.last >= qa) {
                m9829c(gr, (C6144ahM<Object>) new C1299a(gr, ahm));
            } else {
                ahm.mo1931n(dVar.iAo);
            }
        } else {
            m9829c(gr, ahm);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9826a(int i, Object obj, long j) {
        if (this.hFr == null) {
            this.hFr = new ConcurrentHashMap();
        }
        C1302d dVar = (C1302d) this.hFr.get(Integer.valueOf(i));
        if (dVar == null) {
            dVar = new C1302d();
        }
        dVar.last = j;
        dVar.iAo = obj;
        this.hFr.put(Integer.valueOf(i), dVar);
    }

    /* renamed from: c */
    private void m9829c(C0495Gr gr, C6144ahM<Object> ahm) {
        C2390ek ekVar = new C2390ek(gr);
        cXx().put(ekVar.getId(), ahm);
        mo21970a((Blocking) ekVar);
    }

    /* renamed from: c */
    public void mo5605c(ClientConnect bVar, C5581aOv aov) {
        if (this.hFo == null) {
            logger.error("Received async response without associated callback " + aov);
            return;
        }
        C0626Is is = (C0626Is) aov;
        mo21969a(bVar, is.aWo());
        C6144ahM ahm = (C6144ahM) this.hFo.remove(is.getId());
        if (ahm == null) {
            logger.error("Received async response without associated callback " + aov);
        } else if (is.getException() != null) {
            ahm.mo1930a(is.getException());
        } else {
            ahm.mo1931n(is.getResult());
        }
    }

    /* renamed from: ds */
    public boolean mo5609ds() {
        if (bGX() && System.getProperty("pull.error") != null && System.getProperty("pull.error").equals("true")) {
            logger.error("Should not be pulling objects from server!", new IllegalAccessException("Illegal pull of " + bFY()));
        }
        return super.mo5609ds();
    }

    /* renamed from: d */
    public Object mo5606d(C0495Gr gr) {
        long qa = gr.aQK().mo4795qa();
        if (qa == -1) {
            return super.mo5606d(gr);
        }
        long currentTimeMillis = currentTimeMillis();
        if (this.hFr == null) {
            this.hFr = new ConcurrentHashMap();
        }
        C1302d dVar = (C1302d) this.hFr.get(Integer.valueOf(gr.mo2417hq()));
        if (dVar != null && currentTimeMillis - dVar.last < qa) {
            return dVar.iAo;
        }
        Object d = super.mo5606d(gr);
        m9826a(gr.mo2417hq(), d, currentTimeMillis);
        return d;
    }

    /* renamed from: a.TD$d */
    /* compiled from: a */
    static class C1302d {
        public Object iAo;
        public long last;

        C1302d() {
        }
    }

    /* renamed from: a.TD$c */
    /* compiled from: a */
    class C1301c implements Runnable {
        private final /* synthetic */ C0495Gr dgP;

        C1301c(C0495Gr gr) {
            this.dgP = gr;
        }

        public void run() {
            C1298TD.this.mo6901yn().mo14a(this.dgP);
        }
    }

    /* renamed from: a.TD$b */
    /* compiled from: a */
    class C1300b implements Runnable {
        private final /* synthetic */ boolean bgc;
        private final /* synthetic */ C6064afk bgd;
        private final /* synthetic */ C6064afk bge;

        C1300b(boolean z, C6064afk afk, C6064afk afk2) {
            this.bgc = z;
            this.bgd = afk;
            this.bge = afk2;
        }

        public void run() {
            C1298TD.this.mo21972a(this.bgc, (C5439aJj) null, this.bgd, this.bge);
        }
    }

    /* renamed from: a.TD$a */
    class C1299a implements C6144ahM<Object> {
        private final /* synthetic */ C6144ahM bga;
        private final /* synthetic */ C0495Gr dgP;

        C1299a(C0495Gr gr, C6144ahM ahm) {
            this.dgP = gr;
            this.bga = ahm;
        }

        /* renamed from: n */
        public void mo1931n(Object obj) {
            C1298TD.this.m9826a(this.dgP.mo2417hq(), obj, C1298TD.this.currentTimeMillis());
            this.bga.mo1931n(obj);
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            this.bga.mo1930a(th);
        }
    }
}
