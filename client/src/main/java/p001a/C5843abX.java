package p001a;

import game.geometry.Vec3d;

/* renamed from: a.abX  reason: case insensitive filesystem */
/* compiled from: a */
class C5843abX extends aEH.C1791a<Vec3d> {
    final /* synthetic */ aEH eZT;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C5843abX(aEH aeh) {
        super((aEH.C1791a) null);
        this.eZT = aeh;
    }

    /* renamed from: cs */
    public Vec3d mo8563O(String str) {
        String[] split = str.split("[ \t\r\n]+");
        return new Vec3d(Double.parseDouble(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]));
    }
}
