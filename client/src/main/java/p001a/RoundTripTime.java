package p001a;

import game.network.build.IServerConnect;
import logic.thred.LogPrinter;
import logic.thred.PoolThread;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.LinkedList;
import java.util.TreeSet;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: a.Qa */
/* compiled from: a */
public class RoundTripTime {
    private static final LogPrinter logger = LogPrinter.m10275K(RoundTripTime.class);
    public MessageContainerRtt messageRtt;
    public SynchronizerClientTime synchronizerClientTime = new SynchronizerClientTime();
    private boolean threadIsDaemon = true;
    private DatagramChannel datagramChannel;
    private ReceiverRttThread receiverRttThread;
    private SenderRttThread senderRttThread;
    private SocketAddress socketAddressServer;
    private SocketAddress socketPortListener;
    private ByteBuffer byteBufferReceive;
    private ByteBuffer byteBufferSend;
    private LinkedBlockingQueue<MessageContainerRtt> messageHistory = new LinkedBlockingQueue<>();
    private LinkedList<MessageContainerRtt> iMZ = new LinkedList<>();
    private TreeSet<MessageContainerRtt> iNa = new TreeSet<>();
    private int firstFrontierHistory = 20;
    private int secondFrontierHistory = 40;
    private int countErrorSendRtt;

    public RoundTripTime(InetAddress inetAddresServer, int portServer, int portListener) {
        this.socketAddressServer = new InetSocketAddress(inetAddresServer, portServer);
        this.socketPortListener = new InetSocketAddress((InetAddress) null, portListener);
    }

    public static void main(String[] strArr) throws IOException {
        RoundTripTime roundTripTime = new RoundTripTime(InetAddress.getByName(IServerConnect.DEFAULT_HOST), 11333, 11343);
        roundTripTime.setDaemon(false);
        roundTripTime.start();
    }

    public SynchronizerClientTime getSynchronizerTime() {
        return this.synchronizerClientTime;
    }

    public boolean isDaemon() {
        return this.threadIsDaemon;
    }

    public void setDaemon(boolean z) {
        this.threadIsDaemon = z;
    }

    public void start() throws IOException {
        this.datagramChannel = DatagramChannel.open();
        this.datagramChannel.socket().bind(this.socketPortListener);
        this.datagramChannel.socket().connect(this.socketAddressServer);

        this.byteBufferReceive = ByteBuffer.allocateDirect(64);
        this.byteBufferSend = ByteBuffer.allocateDirect(64);

        this.receiverRttThread = new ReceiverRttThread();
        this.receiverRttThread.setDaemon(this.threadIsDaemon);
        this.receiverRttThread.start();

        this.senderRttThread = new SenderRttThread();
        this.senderRttThread.setDaemon(this.threadIsDaemon);
        this.senderRttThread.start();
    }

    /* access modifiers changed from: protected */
    public void sendRtt() throws InterruptedException {
        PoolThread.sleep(threadSleepTimingMilli());
        this.byteBufferSend.clear();
        this.byteBufferSend.putLong(getCurrentTimeMillis());
        this.byteBufferSend.putLong(getSystemNanoTime());
        this.byteBufferSend.flip();
        try {
            this.datagramChannel.send(this.byteBufferSend, this.socketAddressServer);
        } catch (IOException e) {
            this.countErrorSendRtt++;
            if (this.countErrorSendRtt > 10) {
                throw new CountFailedReadOrWriteException("Error writing to " + this.socketAddressServer, e);
            }
            new CountFailedReadOrWriteException("Error writing to " + this.socketAddressServer, e).printStackTrace();
        }
        while (this.messageHistory.size() > 0) {
            relayingMessages(this.messageHistory.take());
        }
    }

    /* access modifiers changed from: protected */
    public long getSystemNanoTime() {
        return System.nanoTime();
    }

    /* access modifiers changed from: protected */
    /* renamed from: Yu */
    public long getCurrentTimeMillis() {
        return SingletonCurrentTimeMilliImpl.currentTimeMillis();
    }

    /* access modifiers changed from: protected */
    public long threadSleepTimingMilli() {
        if (this.iMZ.size() < this.firstFrontierHistory) {
            return 125;
        }
        if (this.iMZ.size() < this.secondFrontierHistory) {
            return 250;
        }
        return 2000;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void relayingMessages(MessageContainerRtt cVar) {
        if (cVar != null) {
            if (this.iMZ.size() > this.secondFrontierHistory) {
                MessageContainerRtt first = this.iNa.first();
                this.iNa.remove(first);
                this.iMZ.remove(first);
                this.synchronizerClientTime.mo2399a(first.fxE, first.fxC, (long) (((double) first.izv) * 1.0E-6d));
            }
            while (this.iMZ.size() > this.secondFrontierHistory + 1) {
                this.iNa.remove(this.iMZ.removeFirst());
                MessageContainerRtt last = this.iNa.last();
                this.iMZ.remove(last);
                this.iNa.remove(last);
            }
            if (this.iNa.size() <= this.firstFrontierHistory || cVar.izv <= this.iNa.first().izv * 4) {
                this.iMZ.add(cVar);
                this.iNa.add(cVar);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void duH() {
    }

    /* access modifiers changed from: private */
    /* renamed from: Ys */
    public void receiveRtt() {
        try {
            this.byteBufferReceive.clear();
            this.datagramChannel.receive(this.byteBufferReceive);
            this.byteBufferReceive.flip();
            processingReceive(this.byteBufferReceive.getLong(), this.byteBufferReceive.getLong(), this.byteBufferReceive.getLong(), this.byteBufferReceive.getLong(), getCurrentTimeMillis(), getSystemNanoTime());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void processingReceive(long j, long j2, long j3, long j4, long currentTimeMillis, long systemNanoTime) {
        this.messageHistory.offer(new MessageContainerRtt(j, j2, j3, j4, currentTimeMillis, systemNanoTime));
    }

    public int getFirstFrontierHistory() {
        return this.firstFrontierHistory;
    }

    /* renamed from: AG */
    public void setFirstFrontierHistory(int i) {
        this.firstFrontierHistory = i;
    }

    public int getSecondFrontierHistory() {
        return this.secondFrontierHistory;
    }

    /* renamed from: AH */
    public void setSecondFrontierHistory(int i) {
        this.secondFrontierHistory = i;
    }

    /* renamed from: a.Qa$c */
    /* compiled from: a */
    public static class MessageContainerRtt implements Comparable<MessageContainerRtt> {
        private static long izw = 1;
        /* access modifiers changed from: private */
        public final long fxC;
        /* access modifiers changed from: private */
        public final long fxE;
        /* access modifiers changed from: private */
        public final long izv;
        /* renamed from: id */
        private final long f1441id;
        private final long izr;
        private final long izs;
        private final long izt;
        private final long izu;

        public MessageContainerRtt(long j, long j2, long j3, long j4, long currentTimeMillis, long systemNanoTime) {
            long j7 = izw;
            izw = 1 + j7;
            this.f1441id = j7;
            this.fxC = j;
            this.izr = j2;
            this.fxE = j3;
            this.izs = j4;
            this.izt = currentTimeMillis;
            this.izu = systemNanoTime;
            this.izv = systemNanoTime - j2;
        }

        /* renamed from: b */
        public int compareTo(MessageContainerRtt cVar) {
            if (this.izv > cVar.izv) {
                return 1;
            }
            if (this.izv < cVar.izv) {
                return -1;
            }
            if (this.f1441id > cVar.f1441id) {
                return 1;
            }
            if (this.f1441id < cVar.f1441id) {
                return -1;
            }
            return 0;
        }

        public String toString() {
            return String.format("%d Rtt: %,.0f us %,d ms delta: %,d ms", new Object[]{Long.valueOf(this.f1441id), Float.valueOf(((float) this.izv) * 0.001f), Long.valueOf(this.izt - this.fxC), Long.valueOf((long) (((double) (this.fxE + ((this.izt - this.fxC) / 2))) - ((double) this.izt)))});
        }

        public long dnA() {
            return this.izv;
        }

        public long dnB() {
            return this.izt - this.fxC;
        }
    }

    /* renamed from: a.Qa$b */
    /* compiled from: a */
    public class ReceiverRttThread extends Thread {
        public ReceiverRttThread() {
        }

        public void run() {
            while (!Thread.interrupted()) {
                RoundTripTime.this.receiveRtt();
            }
        }
    }

    /* renamed from: a.Qa$a */
    public class SenderRttThread extends Thread {
        public SenderRttThread() {
        }

        public void run() {
            while (!Thread.interrupted()) {
                try {
                    RoundTripTime.this.sendRtt();
                } catch (InterruptedException e) {
                }
            }
        }
    }
}
