package p001a;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.aOD */
/* compiled from: a */
public class aOD<T extends C3223pR> {

    /* renamed from: bX */
    private final int f3456bX;
    private final C0678Je izT;
    /* renamed from: bV */
    List<C3223pR> f3455bV = new ArrayList();
    C0678Je[] izR = new C0678Je[4];
    aOD<T>[] izS = new aOD[4];
    /* renamed from: bY */
    private int f3457bY = 4;

    public aOD(C0678Je je) {
        this.izT = je;
        int i = ((je.f805x * 2) + je.f804w) / 2;
        int i2 = ((je.f806y * 2) + je.diz) / 2;
        int i3 = i - je.f805x;
        int i4 = je.f804w - i3;
        int i5 = i2 - je.f806y;
        int i6 = je.diz - i5;
        this.f3456bX = je.f804w * je.diz;
        this.izR[0] = new C0678Je(je.f805x, je.f806y, i3, i5);
        this.izR[1] = new C0678Je(i, je.f806y, i4, i5);
        this.izR[2] = new C0678Je(i, i2, i4, i6);
        this.izR[3] = new C0678Je(je.f805x, i2, i3, i6);
    }

    /* renamed from: a */
    public void mo10545a(T t) {
        C0678Je WD = t.mo3225WD();
        if (this.izT.mo3226a(WD)) {
            if (WD.diz * WD.f804w >= this.f3456bX || WD.diz < this.f3457bY || WD.f804w < this.f3457bY) {
                this.f3455bV.add(t);
                return;
            }
            for (int i = 0; i < 4; i++) {
                if (this.izR[i].mo3226a(WD)) {
                    if (this.izS[i] == null) {
                        this.izS[i] = new aOD<>(this.izR[i]);
                    }
                    this.izS[i].mo10545a(t);
                }
            }
        }
    }

    /* renamed from: b */
    public boolean mo10546b(C0678Je je) {
        if (!this.izT.mo3226a(je)) {
            return false;
        }
        if (this.f3455bV.size() > 0) {
            for (C3223pR WD : this.f3455bV) {
                if (WD.mo3225WD().mo3226a(je)) {
                    return true;
                }
            }
        }
        for (int i = 0; i < 4; i++) {
            if (this.izS[i] != null && this.izS[i].mo10546b(je)) {
                return true;
            }
        }
        return false;
    }
}
