package p001a;

import java.util.Comparator;

/* renamed from: a.aDH */
/* compiled from: a */
class aDH implements Comparator<aGW> {
    aDH() {
    }

    /* renamed from: a */
    public int compare(aGW agw, aGW agw2) {
        return C6560apM.m24875d(agw) < C6560apM.m24875d(agw2) ? -1 : 1;
    }
}
