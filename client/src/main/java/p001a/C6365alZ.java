package p001a;

import game.geometry.Vec3d;
import game.geometry.aLH;
import logic.bbb.C4029yK;
import logic.thred.C6615aqP;

/* renamed from: a.alZ  reason: case insensitive filesystem */
/* compiled from: a */
final class C6365alZ extends C5487aLf {
    final C0110BO fYl;
    final C0109BN fYm;
    final C0112BQ fYn;
    final C0110BO fYo;
    final C0109BN fYp;
    final C0112BQ fYq;
    private final C6020aes fYt;
    aLH fYr = new aLH();
    boolean fYs = false;
    private float radius;

    public C6365alZ(C6020aes aes, C6615aqP aqp) {
        super(aqp);
        C4029yK IL = aqp.mo2437IL();
        this.radius = IL != null ? IL.getOuterSphereRadius() : -1.0f;
        this.fYt = aes;
        this.fYl = new C0110BO(true, this);
        this.fYm = new C0109BN(true, this);
        this.fYn = new C0112BQ(true, this);
        this.fYo = new C0110BO(false, this);
        this.fYp = new C0109BN(false, this);
        this.fYq = new C0112BQ(false, this);
    }

    /* renamed from: f */
    public void mo14694f(Vec3d ajr, Vec3d ajr2) {
        this.fYl.aVY = ajr.x;
        this.fYm.aVY = ajr.y;
        this.fYn.aVY = ajr.z;
        this.fYo.aVY = ajr2.x;
        this.fYp.aVY = ajr2.y;
        this.fYq.aVY = ajr2.z;
    }

    public boolean cio() {
        return this.fYs;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: sv */
    public final C3311qA mo14696sv(int i) {
        switch (i) {
            case 0:
                return this.fYl;
            case 1:
                return this.fYm;
            case 2:
                return this.fYn;
            default:
                throw new IllegalArgumentException();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: sw */
    public final C3311qA mo14697sw(int i) {
        switch (i) {
            case 0:
                return this.fYo;
            case 1:
                return this.fYp;
            case 2:
                return this.fYq;
            default:
                throw new IllegalArgumentException();
        }
    }

    public void dispose() {
        this.fYt.mo13189c(this);
    }

    /* renamed from: c */
    public void mo1234c(aLH alh) {
        this.fYt.mo13182a(this, alh);
    }

    public boolean cip() {
        return true;
    }

    /* renamed from: b */
    public float mo1233b(aRR arr) {
        return mo14693d((C6365alZ) arr);
    }

    /* renamed from: d */
    public float mo14693d(C6365alZ alz) {
        float max = Math.max(0.0f, (float) Math.max(alz.fYl.aVY - this.fYo.aVY, this.fYl.aVY - alz.fYo.aVY));
        float max2 = Math.max(0.0f, (float) Math.max(alz.fYm.aVY - this.fYp.aVY, this.fYm.aVY - alz.fYp.aVY));
        float max3 = Math.max(0.0f, (float) Math.max(alz.fYn.aVY - this.fYq.aVY, this.fYn.aVY - alz.fYq.aVY));
        return (max * max) + (max2 * max2) + (max3 * max3);
    }

    /* renamed from: F */
    public float mo1232F(Vec3d ajr) {
        float max = Math.max(0.0f, (float) Math.max(ajr.x - this.fYo.aVY, this.fYl.aVY - ajr.x));
        float max2 = Math.max(0.0f, (float) Math.max(ajr.y - this.fYp.aVY, this.fYm.aVY - ajr.y));
        float max3 = Math.max(0.0f, (float) Math.max(ajr.z - this.fYq.aVY, this.fYn.aVY - ajr.z));
        return (max * max) + (max2 * max2) + (max3 * max3);
    }

    public float getRadius() {
        return this.radius;
    }
}
