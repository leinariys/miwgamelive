package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;

/* renamed from: a.Rn */
/* compiled from: a */
public class C1205Rn extends C1083Pr {

    private final C3978xf bmY;
    private final C3978xf bmZ;
    /* renamed from: Tx */
    private float f1505Tx;
    private C6460anQ[] aGJ;
    private C6460anQ[] bmX;
    private float bna;
    private float bnb;
    private boolean bno;
    private float dZA;
    private float dZB;
    private float dZC;
    private float dZD;
    private float dZE;
    private float dZF;
    private float dZG;
    private boolean dZH;
    private boolean dZI;
    private float dZz;

    public C1205Rn() {
        super(aVP.HINGE_CONSTRAINT_TYPE);
        this.bmX = new C6460anQ[]{new C6460anQ(), new C6460anQ(), new C6460anQ()};
        this.aGJ = new C6460anQ[]{new C6460anQ(), new C6460anQ(), new C6460anQ()};
        this.bmY = new C3978xf();
        this.bmZ = new C3978xf();
        this.dZH = false;
    }

    public C1205Rn(C6238ajC ajc, C6238ajC ajc2, Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
        super(aVP.HINGE_CONSTRAINT_TYPE, ajc, ajc2);
        this.bmX = new C6460anQ[]{new C6460anQ(), new C6460anQ(), new C6460anQ()};
        this.aGJ = new C6460anQ[]{new C6460anQ(), new C6460anQ(), new C6460anQ()};
        this.bmY = new C3978xf();
        this.bmZ = new C3978xf();
        this.bno = false;
        this.dZH = false;
        this.stack.bcH().push();
        this.stack.bcN().push();
        try {
            this.bmY.bFG.set(vec3f);
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            ajc.ceu().bFF.getColumn(0, vec3f5);
            float dot = vec3f5.dot(vec3f3);
            if (dot > 1.1920929E-7f) {
                vec3f5.scale(dot);
                vec3f5.sub(vec3f3);
            } else {
                ajc.ceu().bFF.getColumn(1, vec3f5);
            }
            Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
            vec3f6.cross(vec3f5, vec3f3);
            this.bmY.bFF.setRow(0, vec3f5.x, vec3f6.x, vec3f3.x);
            this.bmY.bFF.setRow(1, vec3f5.y, vec3f6.y, vec3f3.y);
            this.bmY.bFF.setRow(2, vec3f5.z, vec3f6.z, vec3f3.z);
            Vec3f ac = this.stack.bcH().mo4458ac(C1777aC.m13122b(this.stack.bcN().mo11474b(C1777aC.m13119a(vec3f3, vec3f4)), vec3f5));
            Vec3f vec3f7 = (Vec3f) this.stack.bcH().get();
            vec3f7.cross(ac, vec3f4);
            this.bmZ.bFG.set(vec3f2);
            this.bmZ.bFF.setRow(0, ac.x, vec3f7.x, -vec3f4.x);
            this.bmZ.bFF.setRow(1, ac.y, vec3f7.y, -vec3f4.y);
            this.bmZ.bFF.setRow(2, ac.z, vec3f7.z, -vec3f4.z);
            this.dZB = 1.0E30f;
            this.dZC = -1.0E30f;
            this.bna = 0.3f;
            this.bnb = 1.0f;
            this.f1505Tx = 0.9f;
            this.dZI = false;
        } finally {
            this.stack.bcH().pop();
            this.stack.bcN().pop();
        }
    }

    public C1205Rn(C6238ajC ajc, Vec3f vec3f, Vec3f vec3f2) {
        super(aVP.HINGE_CONSTRAINT_TYPE, ajc);
        this.bmX = new C6460anQ[]{new C6460anQ(), new C6460anQ(), new C6460anQ()};
        this.aGJ = new C6460anQ[]{new C6460anQ(), new C6460anQ(), new C6460anQ()};
        this.bmY = new C3978xf();
        this.bmZ = new C3978xf();
        this.bno = false;
        this.dZH = false;
        this.stack.bcH().push();
        this.stack.bcN().push();
        try {
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            ajc.ceu().bFF.getColumn(0, vec3f3);
            float dot = vec3f3.dot(vec3f2);
            if (dot > 1.1920929E-7f) {
                vec3f3.scale(dot);
                vec3f3.sub(vec3f2);
            } else {
                ajc.ceu().bFF.getColumn(1, vec3f3);
            }
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            vec3f4.cross(vec3f2, vec3f3);
            this.bmY.bFG.set(vec3f);
            this.bmY.bFF.setRow(0, vec3f3.x, vec3f4.x, vec3f2.x);
            this.bmY.bFF.setRow(1, vec3f3.y, vec3f4.y, vec3f2.y);
            this.bmY.bFF.setRow(2, vec3f3.z, vec3f4.z, vec3f2.z);
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            vec3f5.negate(vec3f2);
            ajc.ceu().bFF.transform(vec3f5);
            Vec3f ac = this.stack.bcH().mo4458ac(C1777aC.m13122b(this.stack.bcN().mo11474b(C1777aC.m13119a(vec3f2, vec3f5)), vec3f3));
            Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
            vec3f6.cross(vec3f5, ac);
            this.bmZ.bFG.set(vec3f);
            ajc.ceu().mo22946G(this.bmZ.bFG);
            this.bmZ.bFF.setRow(0, ac.x, vec3f6.x, vec3f5.x);
            this.bmZ.bFF.setRow(1, ac.y, vec3f6.y, vec3f5.y);
            this.bmZ.bFF.setRow(2, ac.z, vec3f6.z, vec3f5.z);
            this.dZB = 1.0E30f;
            this.dZC = -1.0E30f;
            this.bna = 0.3f;
            this.bnb = 1.0f;
            this.f1505Tx = 0.9f;
            this.dZI = false;
        } finally {
            this.stack.bcH().pop();
            this.stack.bcN().pop();
        }
    }

    public C1205Rn(C6238ajC ajc, C6238ajC ajc2, C3978xf xfVar, C3978xf xfVar2) {
        super(aVP.HINGE_CONSTRAINT_TYPE, ajc, ajc2);
        this.bmX = new C6460anQ[]{new C6460anQ(), new C6460anQ(), new C6460anQ()};
        this.aGJ = new C6460anQ[]{new C6460anQ(), new C6460anQ(), new C6460anQ()};
        this.bmY = new C3978xf();
        this.bmZ = new C3978xf();
        this.bmY.mo22947a(xfVar);
        this.bmZ.mo22947a(xfVar2);
        this.bno = false;
        this.dZH = false;
        this.bmZ.bFF.m02 *= -1.0f;
        this.bmZ.bFF.m12 *= -1.0f;
        this.bmZ.bFF.m22 *= -1.0f;
        this.dZB = 1.0E30f;
        this.dZC = -1.0E30f;
        this.bna = 0.3f;
        this.bnb = 1.0f;
        this.f1505Tx = 0.9f;
        this.dZI = false;
    }

    public C1205Rn(C6238ajC ajc, C3978xf xfVar) {
        super(aVP.HINGE_CONSTRAINT_TYPE, ajc);
        this.bmX = new C6460anQ[]{new C6460anQ(), new C6460anQ(), new C6460anQ()};
        this.aGJ = new C6460anQ[]{new C6460anQ(), new C6460anQ(), new C6460anQ()};
        this.bmY = new C3978xf();
        this.bmZ = new C3978xf();
        this.bmY.mo22947a(xfVar);
        this.bmZ.mo22947a(xfVar);
        this.bno = false;
        this.dZH = false;
        this.bmZ.bFF.m02 *= -1.0f;
        this.bmZ.bFF.m12 *= -1.0f;
        this.bmZ.bFF.m22 *= -1.0f;
        this.bmZ.bFG.set(this.bmY.bFG);
        ajc.ceu().mo22946G(this.bmZ.bFG);
        this.dZB = 1.0E30f;
        this.dZC = -1.0E30f;
        this.bna = 0.3f;
        this.bnb = 1.0f;
        this.f1505Tx = 0.9f;
        this.dZI = false;
    }

    /* renamed from: PU */
    public void mo4840PU() {
        this.stack.bcF();
        try {
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            Matrix3fWrap ajd = (Matrix3fWrap) this.stack.bcK().get();
            Matrix3fWrap ajd2 = (Matrix3fWrap) this.stack.bcK().get();
            this.dMa = 0.0f;
            if (!this.bno) {
                Vec3f ac = this.stack.bcH().mo4458ac(this.bmY.bFG);
                this.dQm.ceu().mo22946G(ac);
                Vec3f ac2 = this.stack.bcH().mo4458ac(this.bmZ.bFG);
                this.dQn.ceu().mo22946G(ac2);
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                vec3f4.sub(ac2, ac);
                Vec3f[] vec3fArr = {(Vec3f) this.stack.bcH().get(), (Vec3f) this.stack.bcH().get(), (Vec3f) this.stack.bcH().get()};
                if (vec3f4.lengthSquared() > 1.1920929E-7f) {
                    vec3fArr[0].set(vec3f4);
                    vec3fArr[0].normalize();
                } else {
                    vec3fArr[0].set(1.0f, 0.0f, 0.0f);
                }
                C0503Gz.m3562e(vec3fArr[0], vec3fArr[1], vec3fArr[2]);
                for (int i = 0; i < 3; i++) {
                    ajd.transpose(this.dQm.ceu().bFF);
                    ajd2.transpose(this.dQn.ceu().bFF);
                    vec3f2.sub(ac, this.dQm.ces());
                    vec3f3.sub(ac2, this.dQn.ces());
                    this.bmX[i].mo14967a(ajd, ajd2, vec3f2, vec3f3, vec3fArr[i], this.dQm.ceq(), this.dQm.aRf(), this.dQn.ceq(), this.dQn.aRf());
                }
            }
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
            this.bmY.bFF.getColumn(2, vec3f);
            C0503Gz.m3562e(vec3f, vec3f5, vec3f6);
            Vec3f ac3 = this.stack.bcH().mo4458ac(vec3f5);
            bnn().ceu().bFF.transform(ac3);
            Vec3f ac4 = this.stack.bcH().mo4458ac(vec3f6);
            bnn().ceu().bFF.transform(ac4);
            Vec3f vec3f7 = (Vec3f) this.stack.bcH().get();
            this.bmY.bFF.getColumn(2, vec3f7);
            bnn().ceu().bFF.transform(vec3f7);
            ajd.transpose(this.dQm.ceu().bFF);
            ajd2.transpose(this.dQn.ceu().bFF);
            this.aGJ[0].mo14969a(ac3, ajd, ajd2, this.dQm.ceq(), this.dQn.ceq());
            this.aGJ[1].mo14969a(ac4, ajd, ajd2, this.dQm.ceq(), this.dQn.ceq());
            this.aGJ[2].mo14969a(vec3f7, ajd, ajd2, this.dQm.ceq(), this.dQn.ceq());
            float brm = brm();
            this.dZF = 0.0f;
            this.dZE = 0.0f;
            this.dZI = false;
            this.dZG = 0.0f;
            if (this.dZB < this.dZC) {
                if (brm <= this.dZB * this.f1505Tx) {
                    this.dZF = this.dZB - brm;
                    this.dZE = 1.0f;
                    this.dZI = true;
                } else if (brm >= this.dZC * this.f1505Tx) {
                    this.dZF = this.dZC - brm;
                    this.dZE = -1.0f;
                    this.dZI = true;
                }
            }
            Vec3f vec3f8 = (Vec3f) this.stack.bcH().get();
            this.bmY.bFF.getColumn(2, vec3f8);
            bnn().ceu().bFF.transform(vec3f8);
            this.dZD = 1.0f / (bno().mo13902aC(vec3f8) + bnn().mo13902aC(vec3f8));
        } finally {
            this.stack.bcG();
        }
    }

    /* renamed from: cv */
    public void mo4847cv(float f) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            Vec3f ac = this.stack.bcH().mo4458ac(this.bmY.bFG);
            this.dQm.ceu().mo22946G(ac);
            Vec3f ac2 = this.stack.bcH().mo4458ac(this.bmZ.bFG);
            this.dQn.ceu().mo22946G(ac2);
            if (!this.bno) {
                Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                vec3f3.sub(ac, this.dQm.ces());
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                vec3f4.sub(ac2, this.dQn.ces());
                Vec3f ac3 = this.stack.bcH().mo4458ac(this.dQm.mo13897R(vec3f3));
                Vec3f ac4 = this.stack.bcH().mo4458ac(this.dQn.mo13897R(vec3f4));
                Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                vec3f5.sub(ac3, ac4);
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= 3) {
                        break;
                    }
                    Vec3f vec3f6 = this.bmX[i2].gfg;
                    float clX = 1.0f / this.bmX[i2].clX();
                    float dot = vec3f6.dot(vec3f5);
                    vec3f.sub(ac, ac2);
                    float f2 = ((((-vec3f.dot(vec3f6)) * 0.3f) / f) * clX) - (clX * dot);
                    this.dMa += f2;
                    Vec3f vec3f7 = (Vec3f) this.stack.bcH().get();
                    vec3f7.scale(f2, vec3f6);
                    vec3f.sub(ac, this.dQm.ces());
                    this.dQm.mo13937m(vec3f7, vec3f);
                    vec3f.negate(vec3f7);
                    vec3f2.sub(ac2, this.dQn.ces());
                    this.dQn.mo13937m(vec3f, vec3f2);
                    i = i2 + 1;
                }
            }
            Vec3f vec3f8 = (Vec3f) this.stack.bcH().get();
            this.bmY.bFF.getColumn(2, vec3f8);
            bnn().ceu().bFF.transform(vec3f8);
            Vec3f vec3f9 = (Vec3f) this.stack.bcH().get();
            this.bmZ.bFF.getColumn(2, vec3f9);
            bno().ceu().bFF.transform(vec3f9);
            Vec3f angularVelocity = bnn().getAngularVelocity();
            Vec3f angularVelocity2 = bno().getAngularVelocity();
            Vec3f vec3f10 = (Vec3f) this.stack.bcH().get();
            vec3f10.scale(vec3f8.dot(angularVelocity), vec3f8);
            Vec3f vec3f11 = (Vec3f) this.stack.bcH().get();
            vec3f11.scale(vec3f9.dot(angularVelocity2), vec3f9);
            Vec3f vec3f12 = (Vec3f) this.stack.bcH().get();
            vec3f12.sub(angularVelocity, vec3f10);
            Vec3f vec3f13 = (Vec3f) this.stack.bcH().get();
            vec3f13.sub(angularVelocity2, vec3f11);
            Vec3f vec3f14 = (Vec3f) this.stack.bcH().get();
            vec3f14.sub(vec3f12, vec3f13);
            if (vec3f14.length() > 1.0E-5f) {
                Vec3f vec3f15 = (Vec3f) this.stack.bcH().get();
                vec3f15.normalize(vec3f14);
                vec3f14.scale((1.0f / (bno().mo13902aC(vec3f15) + bnn().mo13902aC(vec3f15))) * this.bnb);
            }
            Vec3f vec3f16 = (Vec3f) this.stack.bcH().get();
            vec3f16.cross(vec3f8, vec3f9);
            vec3f16.negate();
            vec3f16.scale(1.0f / f);
            if (vec3f16.length() > 1.0E-5f) {
                Vec3f vec3f17 = (Vec3f) this.stack.bcH().get();
                vec3f17.normalize(vec3f16);
                vec3f16.scale((1.0f / (bno().mo13902aC(vec3f17) + bnn().mo13902aC(vec3f17))) * 1.0f);
            }
            vec3f.negate(vec3f14);
            vec3f.add(vec3f16);
            this.dQm.mo13896Q(vec3f);
            vec3f.sub(vec3f14, vec3f16);
            this.dQn.mo13896Q(vec3f);
            if (this.dZI) {
                vec3f.sub(angularVelocity2, angularVelocity);
                float dot2 = ((vec3f.dot(vec3f8) * this.bnb) + (this.dZF * (1.0f / f) * this.bna)) * this.dZE * this.dZD;
                float f3 = this.dZG;
                this.dZG = Math.max(dot2 + this.dZG, 0.0f);
                float f4 = this.dZG - f3;
                Vec3f vec3f18 = (Vec3f) this.stack.bcH().get();
                vec3f18.scale(f4 * this.dZE, vec3f8);
                this.dQm.mo13896Q(vec3f18);
                vec3f.negate(vec3f18);
                this.dQn.mo13896Q(vec3f);
            }
            if (this.dZH) {
                Vec3f h = this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f);
                Vec3f vec3f19 = (Vec3f) this.stack.bcH().get();
                vec3f19.sub(vec3f10, vec3f11);
                float dot3 = (this.dZz - vec3f19.dot(vec3f8)) * this.dZD;
                if (dot3 > this.dZA) {
                    dot3 = this.dZA;
                }
                float f5 = dot3 < (-this.dZA) ? -this.dZA : dot3;
                Vec3f vec3f20 = (Vec3f) this.stack.bcH().get();
                vec3f20.scale(f5, vec3f8);
                vec3f.add(vec3f20, h);
                this.dQm.mo13896Q(vec3f);
                vec3f.negate(vec3f20);
                vec3f.sub(h);
                this.dQn.mo13896Q(vec3f);
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: cw */
    public void mo5278cw(float f) {
    }

    public float brm() {
        this.stack.bcH().push();
        try {
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            this.bmY.bFF.getColumn(0, vec3f);
            bnn().ceu().bFF.transform(vec3f);
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            this.bmY.bFF.getColumn(1, vec3f2);
            bnn().ceu().bFF.transform(vec3f2);
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            this.bmZ.bFF.getColumn(1, vec3f3);
            bno().ceu().bFF.transform(vec3f3);
            return C0920NU.m7657m(vec3f3.dot(vec3f), vec3f3.dot(vec3f2));
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: br */
    public void mo5272br(boolean z) {
        this.bno = z;
    }

    /* renamed from: a */
    public void mo5268a(boolean z, float f, float f2) {
        this.dZH = z;
        this.dZz = f;
        this.dZA = f2;
    }

    /* renamed from: o */
    public void mo5279o(float f, float f2) {
        mo5271b(f, f2, 0.9f, 0.3f, 1.0f);
    }

    /* renamed from: b */
    public void mo5271b(float f, float f2, float f3, float f4, float f5) {
        this.dZB = f;
        this.dZC = f2;
        this.f1505Tx = f3;
        this.bna = f4;
        this.bnb = f5;
    }

    public float brn() {
        return this.dZB;
    }

    public float bro() {
        return this.dZC;
    }

    public C3978xf adO() {
        return this.bmY;
    }

    public C3978xf adP() {
        return this.bmZ;
    }

    public boolean brp() {
        return this.dZI;
    }

    public float brq() {
        return this.dZE;
    }
}
