package p001a;

import gnu.trove.THashSet;
import org.apache.tools.ant.Task;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import taikodom.render.graphics2d.C0559Hm;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: a.aAc  reason: case insensitive filesystem */
/* compiled from: a */
public class C5198aAc extends Task {
    private String classpath;
    private Map<String, C6351alL> dLM = new ConcurrentHashMap();
    private String iSA;
    private String iSB;
    private String[] iSC;
    private THashSet<String> iSD = new THashSet<>();
    private Map<File, C6351alL> iSE = new ConcurrentHashMap();

    public String dwl() {
        return this.iSA;
    }

    public void setFiles(String str) {
        this.iSA = str;
    }

    public String getClasspath() {
        return this.classpath;
    }

    public void setClasspath(String str) {
        this.classpath = str;
    }

    public String dwm() {
        return this.iSB;
    }

    public void setOutput(String str) {
        this.iSB = str;
    }

    /* renamed from: ge */
    public C0244Cz mo7662ge(String str) {
        byte[] bArr;
        File file = null;
        C6351alL all = this.dLM.get(str);
        if (all == null) {
            try {
                byte[] bArr2 = null;
                String[] strArr = this.iSC;
                int length = strArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        bArr = bArr2;
                        break;
                    }
                    file = new File(String.valueOf(strArr[i]) + File.separatorChar + str + ".class");
                    if (file.exists()) {
                        bArr = C0559Hm.m5264q(file);
                        break;
                    }
                    i++;
                }
                all = new C6351alL();
                if (bArr != null) {
                    new ClassReader(bArr).accept(all, 7);
                    this.iSE.put(file.getAbsoluteFile(), all);
                }
                this.dLM.put(str, all);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(str);
            }
        }
        return all;
    }

    public void execute() {
        String[] strArr;
        C6351alL all;
        C5198aAc.super.execute();
        if (this.classpath != null) {
            strArr = this.classpath.split("\\\"(\\s*\\\")?");
        } else {
            strArr = new String[0];
        }
        this.iSC = strArr;
        String[] split = this.iSA.split("\\\"(\\s*\\\")?");
        C1765a aVar = new C1765a();
        for (String str : split) {
            if (str.endsWith(".class")) {
                File absoluteFile = new File(str).getAbsoluteFile();
                byte[] q = C0559Hm.m5264q(absoluteFile);
                if (this.iSE.containsKey(absoluteFile)) {
                    all = this.iSE.get(absoluteFile);
                } else {
                    ClassReader classReader = new ClassReader(q);
                    all = new C6351alL();
                    classReader.accept(all, 7);
                    this.iSE.put(absoluteFile, all);
                    this.dLM.put(all.getName(), all);
                }
                if (all.aBz() && !all.aBA()) {
                    System.out.println("Instrumenting " + all.getName());
                    ClassReader classReader2 = new ClassReader(q);
                    ClassWriter classWriter = new ClassWriter(1);
                    classReader2.accept(new C6416amY(classWriter, aVar), 0);
                    C0559Hm.m5248a(absoluteFile, classWriter.toByteArray());
                    C0559Hm.m5248a(new File(String.valueOf(absoluteFile.getAbsolutePath()) + ".uninstrumented"), q);
                }
            }
        }
    }

    /* renamed from: a.aAc$a */
    class C1765a implements C6380alo {
        C1765a() {
        }

        /* renamed from: a */
        public void mo4567a(C6416amY amy, ayO ayo, int i, String str, Object... objArr) {
            System.out.println(String.format(str, objArr));
        }

        /* renamed from: ge */
        public C0244Cz mo4573ge(String str) {
            return C5198aAc.this.mo7662ge(str);
        }

        public C6380alo.C1937a blO() {
            return C6380alo.C1937a.GENERIC;
        }

        /* renamed from: b */
        public void mo4569b(String str, byte[] bArr, int i, int i2) {
        }
    }
}
