package p001a;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.gx */
/* compiled from: a */
public class C2570gx {

    /* renamed from: QJ */
    public C0308E f7818QJ;

    /* renamed from: QK */
    public ImageIcon f7819QK;

    /* renamed from: QL */
    public String f7820QL;

    /* renamed from: QM */
    public ImageIcon f7821QM;
    public Color color;
    public String name;

    public void setColor(Color color2) {
        this.color = color2;
    }

    /* renamed from: a */
    public void mo19158a(ImageIcon imageIcon) {
        this.f7819QK = imageIcon;
    }

    /* renamed from: a */
    public void mo19157a(C0308E e) {
        this.f7818QJ = e;
    }

    public void setName(String str) {
        this.name = str;
    }

    /* renamed from: av */
    public void mo19159av(String str) {
        this.f7820QL = str;
    }

    /* renamed from: b */
    public void mo19160b(ImageIcon imageIcon) {
        this.f7821QM = imageIcon;
    }

    public boolean isReady() {
        return (this.f7818QJ == null || this.f7819QK == null || this.name == null || this.f7820QL == null) ? false : true;
    }

    public String toString() {
        return this.f7820QL;
    }
}
