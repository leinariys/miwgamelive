package p001a;

import game.network.IReadBits;

import java.io.DataInput;

/* renamed from: a.aEB */
/* compiled from: a */
public class aEB implements IReadBits {
    public static final int[] emM = C6058afe.emM;
    public int emK = 0;
    public DataInput emL;
    int left;

    /* renamed from: a */
    public int readBits(int i) {
        int i2;
        if (i <= this.emK) {
            this.emK -= i;
            return (this.left >> this.emK) & emM[i];
        }
        if (this.emK > 0) {
            i2 = this.left & emM[this.emK];
            this.emK = 0;
            i -= this.emK;
        } else {
            i2 = 0;
        }
        while (i > 8) {
            i2 = (i2 << 8) | (this.emL.readUnsignedByte() & 255);
            i -= 8;
        }
        if (i <= 0) {
            return i2;
        }
        this.left = this.emL.readUnsignedByte();
        this.emK = 8 - i;
        return (i2 << i) | ((this.left >> this.emK) & emM[i]);
    }

    /* renamed from: xM */
    public int mo8533xM(int i) {
        if (i < this.emK) {
            this.emK -= i;
        } else {
            int i2 = i - this.emK;
            this.emL.skipBytes(i2 / 8);
            int i3 = i2 & 7;
            if (i3 > 0) {
                this.left = this.emL.readUnsignedByte();
                this.emK = 8 - i3;
            }
        }
        return i;
    }
}
