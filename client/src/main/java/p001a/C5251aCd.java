package p001a;

import java.io.InputStream;

/* renamed from: a.aCd  reason: case insensitive filesystem */
/* compiled from: a */
public class C5251aCd extends InputStream {
    /* renamed from: in */
    private final InputStream f2485in;
    private int ckI;
    private boolean closed;
    private long hmK;
    private long hmL = System.currentTimeMillis();

    public C5251aCd(InputStream inputStream) {
        this.f2485in = inputStream;
    }

    public float bJV() {
        long currentTimeMillis = System.currentTimeMillis();
        this.hmL = currentTimeMillis;
        float f = ((float) this.hmK) / (((float) (currentTimeMillis - this.hmL)) / 1000.0f);
        this.hmK = 0;
        return f;
    }

    public int cMR() {
        return this.ckI;
    }

    public boolean isClosed() {
        return this.closed;
    }

    public void close() {
        this.f2485in.close();
        this.hmK = 0;
        this.closed = true;
    }

    public int read() {
        int read = this.f2485in.read();
        if (read >= 0) {
            this.ckI++;
        }
        return read;
    }

    public int read(byte[] bArr, int i, int i2) {
        int read = this.f2485in.read(bArr, i, i2);
        this.ckI += read;
        this.hmK += (long) read;
        return read;
    }

    public int read(byte[] bArr) {
        int read = this.f2485in.read(bArr);
        this.ckI += read;
        this.hmK += (long) read;
        return read;
    }
}
