package p001a;

import com.hoplon.geometry.Vec3f;
import logic.abc.aGK;
import logic.abc.azD;

/* renamed from: a.aCf  reason: case insensitive filesystem */
/* compiled from: a */
public class C5253aCf implements C5915acr {
    private static final int cRb = 32;
    public final C0763Kt stack = C0763Kt.bcE();
    private azD hmM;
    private azD hmN;
    /* renamed from: lL */
    private C6331akr f2486lL;

    public C5253aCf(azD azd, azD azd2, C6331akr akr) {
        this.hmM = azd;
        this.hmN = azd2;
        this.f2486lL = akr;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public boolean mo8215a(C3978xf xfVar, C3978xf xfVar2, C3978xf xfVar3, C3978xf xfVar4, C5915acr.C1866a aVar) {
        this.stack.bcF();
        try {
            aGK agk = new aGK(this.hmM, this.hmN);
            C3978xf xfVar5 = (C3978xf) this.stack.bcJ().get();
            C3978xf xfVar6 = (C3978xf) this.stack.bcJ().get();
            xfVar5.mo22952b(xfVar);
            xfVar5.mo22954c(xfVar3);
            xfVar6.mo22952b(xfVar2);
            xfVar6.mo22954c(xfVar4);
            this.f2486lL.reset();
            agk.mo8927l(this.stack.bcJ().mo1156d(xfVar5.bFF));
            float f = 0.0f;
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            vec3f.negate(xfVar5.bFG);
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            vec3f2.sub(xfVar6.bFG, xfVar5.bFG);
            vec3f2.negate();
            Vec3f ac = this.stack.bcH().mo4458ac(vec3f);
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            vec3f3.sub(ac, this.stack.bcH().mo4458ac(agk.localGetSupportingVertex(vec3f2)));
            int i = 32;
            Vec3f h = this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f);
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            float lengthSquared = vec3f3.lengthSquared();
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
            while (lengthSquared > 1.0E-4f) {
                int i2 = i - 1;
                if (i == 0) {
                    break;
                }
                vec3f6.set(agk.localGetSupportingVertex(vec3f3));
                vec3f5.sub(ac, vec3f6);
                float dot = vec3f3.dot(vec3f5);
                if (dot > 0.0f) {
                    float dot2 = vec3f3.dot(vec3f2);
                    if (dot2 >= -1.4210855E-14f) {
                        this.stack.bcG();
                        return false;
                    }
                    f -= dot / dot2;
                    ac.scaleAdd(f, vec3f2, vec3f);
                    this.f2486lL.reset();
                    vec3f5.sub(ac, vec3f6);
                    h.set(vec3f3);
                }
                this.f2486lL.mo1858p(vec3f5, ac, vec3f6);
                if (this.f2486lL.mo1849aK(vec3f3)) {
                    lengthSquared = vec3f3.lengthSquared();
                    i = i2;
                } else {
                    lengthSquared = 0.0f;
                    i = i2;
                }
            }
            aVar.fkF = f;
            aVar.f4276Oj.set(h);
            this.stack.bcG();
            return true;
        } catch (Throwable th) {
            this.stack.bcG();
            throw th;
        }
    }
}
