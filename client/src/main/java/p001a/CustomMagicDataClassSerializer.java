package p001a;

import game.io.IReadExternal;
import game.io.IWriteExternal;
import game.network.manager.MagicDataClassSerializer;
import game.network.message.externalizable.C1291Sy;
import game.network.message.serializable.C1556Wo;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.awl  reason: case insensitive filesystem */
/* compiled from: a */
public class CustomMagicDataClassSerializer extends MagicDataClassSerializer {
    /* renamed from: z */
    public Object mo3598z(Object obj) {
        if (obj == null) {
            return null;
        }
        return ((C1556Wo) obj).clone();
    }

    /* renamed from: yl */
    public boolean mo3597yl() {
        return true;
    }

    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) {
        if (!objectInput.readBoolean()) {
            return null;
        }
        C1291Sy sy = new C1291Sy();
        sy.readExternal(objectInput);
        return sy;
    }

    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) {
        if (obj == null) {
            objectOutput.writeBoolean(false);
            return;
        }
        objectOutput.writeBoolean(true);
        ((C1291Sy) obj).writeExternal(objectOutput);
    }

    /* renamed from: a */
    public Object mo2357a(IReadExternal vm) {
        if (!vm.mo6354gR("notNull")) {
            return null;
        }
        C1291Sy sy = new C1291Sy();
        sy.readExternal((ObjectInput) vm);
        return sy;
    }

    /* renamed from: a */
    public void serializeData(IWriteExternal att, Object obj) {
        if (obj == null) {
            att.writeBoolean("notNull", false);
            return;
        }
        att.writeBoolean("notNull", true);
        ((C1291Sy) obj).writeExternal((ObjectOutput) att);
    }
}
