package p001a;

/* renamed from: a.ams  reason: case insensitive filesystem */
/* compiled from: a */
public class C6436ams implements C3102np {
    public final C5487aLf gaJ;
    public final C5487aLf gaK;
    public final long aBm;
    public final int hash = ((int) (this.aBm ^ (this.aBm >>> 32)));
    private Object data;

    public C6436ams(C5487aLf alf, C5487aLf alf2) {
        this.gaJ = alf;
        this.gaK = alf2;
        this.aBm = m24006af(alf.f3320id, alf2.f3320id);
    }

    /* renamed from: af */
    public static long m24006af(int i, int i2) {
        if (i < i2) {
            return (((long) i) << 32) | ((long) i2);
        }
        return (((long) i2) << 32) | ((long) i);
    }

    /* renamed from: Qg */
    public aRR mo14894Qg() {
        return this.gaJ;
    }

    /* renamed from: Qh */
    public aRR mo14895Qh() {
        return this.gaK;
    }

    /* renamed from: Qi */
    public Object mo14896Qi() {
        return this.data;
    }

    /* renamed from: C */
    public void mo14893C(Object obj) {
        this.data = obj;
    }

    public int hashCode() {
        return this.hash;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C6436ams)) {
            return false;
        }
        if (this.aBm != ((C6436ams) obj).aBm) {
            return false;
        }
        return true;
    }
}
