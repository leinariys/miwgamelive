package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.aGk  reason: case insensitive filesystem */
/* compiled from: a */
public class C5362aGk implements C5852abg.C1855b {

    /* renamed from: jR */
    static final /* synthetic */ boolean f2877jR = (!C5362aGk.class.desiredAssertionStatus());
    public final C1123QW<C6995ayi> hPd = C0762Ks.m6640D(C6995ayi.class);
    public final C0763Kt stack = C0763Kt.bcE();
    private final C3978xf hPe = new C3978xf();
    private final C3978xf hPf = new C3978xf();
    /* renamed from: Ql */
    private aGW f2878Ql;
    private ayY hPg;
    private ayY hPh;
    private int hPi;
    private int hPj;
    private int hPk;
    private int hPl;

    public C5362aGk() {
    }

    public C5362aGk(ayY ayy, ayY ayy2) {
        mo9114d(ayy, ayy2);
    }

    /* renamed from: e */
    private static float m14969e(ayY ayy, ayY ayy2) {
        float cFd = ayy2.cFd() * ayy.cFd();
        if (cFd < (-10.0f)) {
            cFd = -10.0f;
        }
        if (cFd > 10.0f) {
            return 10.0f;
        }
        return cFd;
    }

    /* renamed from: f */
    private static float m14970f(ayY ayy, ayY ayy2) {
        return ayy.cFc() * ayy2.cFc();
    }

    /* renamed from: d */
    public void mo9114d(ayY ayy, ayY ayy2) {
        this.hPg = ayy;
        this.hPh = ayy2;
        this.hPe.mo22947a(ayy.cFf());
        this.hPf.mo22947a(ayy2.cFf());
    }

    public aGW dac() {
        return this.f2878Ql;
    }

    /* renamed from: e */
    public void mo9117e(aGW agw) {
        this.f2878Ql = agw;
    }

    /* renamed from: b */
    public void mo5216b(int i, int i2, int i3, int i4) {
        this.hPi = i;
        this.hPj = i3;
        this.hPk = i2;
        this.hPl = i4;
    }

    /* renamed from: c */
    public void mo5217c(Vec3f vec3f, Vec3f vec3f2, float f) {
        boolean z;
        if (!f2877jR && this.f2878Ql == null) {
            throw new AssertionError();
        } else if (f <= this.f2878Ql.dcB()) {
            this.stack.bcH().push();
            try {
                if (this.f2878Ql.dcy() != this.hPg) {
                    z = true;
                } else {
                    z = false;
                }
                Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                vec3f3.scaleAdd(f, vec3f, vec3f2);
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                if (z) {
                    this.hPf.mo22955f(vec3f3, vec3f4);
                    this.hPe.mo22955f(vec3f2, vec3f5);
                } else {
                    this.hPe.mo22955f(vec3f3, vec3f4);
                    this.hPf.mo22955f(vec3f2, vec3f5);
                }
                C6995ayi ayi = this.hPd.get();
                ayi.mo17074b(vec3f4, vec3f5, vec3f, f);
                ayi.gSI.set(vec3f3);
                ayi.gSH.set(vec3f2);
                int d = this.f2878Ql.mo9011d(ayi);
                ayi.gSK = m14969e(this.hPg, this.hPh);
                ayi.gSL = m14970f(this.hPg, this.hPh);
                if (d >= 0) {
                    this.f2878Ql.mo9007a(ayi, d);
                } else {
                    this.f2878Ql.mo9017e(ayi);
                }
                if (!(C0128Bb.dMD == null || ((this.hPg.cFn() & 8) == 0 && (this.hPh.cFn() & 8) == 0))) {
                    C0128Bb.dMD.mo13578a(ayi, z ? this.hPh : this.hPg, this.hPi, this.hPk, z ? this.hPg : this.hPh, this.hPj, this.hPl);
                }
                this.hPd.release(ayi);
            } finally {
                this.stack.bcH().pop();
            }
        }
    }

    public void dad() {
        if (!f2877jR && this.f2878Ql == null) {
            throw new AssertionError();
        } else if (this.f2878Ql.dcA() != 0) {
            if (this.f2878Ql.dcy() != this.hPg) {
                this.f2878Ql.mo9010c(this.hPf, this.hPe);
            } else {
                this.f2878Ql.mo9010c(this.hPe, this.hPf);
            }
        }
    }
}
