package p001a;

import game.engine.C3658tN;
import logic.sql.C5878acG;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;

/* renamed from: a.Mw */
/* compiled from: a */
public class C0891Mw implements C3658tN {
    public static final String META = "meta";
    public static final String DATA = "data";
    private static final String dBA = "\\\"";
    private static final char dBB = ';';
    private static final char dBC = ',';
    private static final char dBD = '.';
    private static final char dBE = '_';
    private static final SimpleDateFormat dBH = new SimpleDateFormat(C5878acG.feC);
    private static final char dBz = '\"';
    private static final Log logger = LogPrinter.m10275K(C0891Mw.class);

    /* renamed from: pw */
    private PrintWriter f1168pw;

    public C0891Mw(String str) {
        try {
            this.f1168pw = LogPrinter.createLogFile(str, "log.csv");
            for (Class<? extends C5878acG> Z : C2705in.f8246XF) {
                this.f1168pw.print(META);
                this.f1168pw.print(';');
                this.f1168pw.println(C5878acG.m20238Z(Z));
            }
        } catch (FileNotFoundException e) {
            logger.error(e);
        } catch (UnsupportedEncodingException e2) {
            logger.error(e2);
        }
    }

    private static String encodeText(String str) {
        boolean z = false;
        StringBuilder sb = new StringBuilder(34);
        int i = 0;
        while (true) {
            boolean z2 = z;
            if (i >= str.length()) {
                return sb.toString();
            }
            char charAt = str.charAt(i);
            switch (charAt) {
                case '\"':
                    sb.append(dBA);
                    break;
                case '.':
                    sb.append(z2 ? ';' : dBE);
                    z2 = true;
                    break;
                case ';':
                    sb.append(dBC);
                    break;
                default:
                    sb.append(charAt);
                    break;
            }
            z = z2;
            i++;
        }
    }

    /* renamed from: g */
    private synchronized void m7188g(String... strArr) {
        String str = "";
        for (int i = 0; i < strArr.length; i++) {
            str = String.valueOf(str) + encodeText(strArr[i]) + ';';
        }
        this.f1168pw.print(str);
    }

    /* renamed from: h */
    private synchronized void m7189h(String... strArr) {
        String str = "";
        for (int i = 0; i < strArr.length; i++) {
            str = String.valueOf(str) + encodeText(strArr[i]) + ';';
        }
        this.f1168pw.println(str);
    }

    /* renamed from: a */
    public void mo4098a(C5878acG acg) {
        if (!C2705in.m33585f(acg.getClass())) {
            throw new C0381FG(acg.getClass());
        }
        this.f1168pw.print(DATA);
        this.f1168pw.print(';');
        this.f1168pw.println(acg.bPF());
        this.f1168pw.flush();
    }

    /* renamed from: b */
    public void mo4099b(C5878acG acg) {
        if (!C2705in.m33585f(acg.getClass())) {
            throw new C0381FG(acg.getClass());
        }
        this.f1168pw.print(DATA);
        this.f1168pw.print(';');
        this.f1168pw.println(acg.bPF());
        this.f1168pw.flush();
    }
}
