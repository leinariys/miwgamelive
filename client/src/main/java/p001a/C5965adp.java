package p001a;

import com.hoplon.geometry.Vec3f;
import logic.abc.C0802Lc;
import logic.abc.C1226SE;

/* renamed from: a.adp  reason: case insensitive filesystem */
/* compiled from: a */
class C5965adp implements aTC {

    public final C0763Kt stack = C0763Kt.bcE();
    private final Vec3f cam = new Vec3f();
    private final Vec3f can = new Vec3f();
    /* renamed from: Ql */
    public aGW f4338Ql;
    public int car;
    /* renamed from: Uy */
    private C1701ZB f4339Uy;
    private C5408aIe cao;
    private float cap;
    private ayY fhq;
    private ayY fhr;
    private C5362aGk fhs;
    private C1034PA fht = new C1034PA();
    private C1226SE fhu = new C1226SE();

    public C5965adp(C1701ZB zb, ayY ayy, ayY ayy2, boolean z) {
        this.f4339Uy = zb;
        this.cao = null;
        this.fhq = z ? ayy2 : ayy;
        this.fhr = !z ? ayy2 : ayy;
        this.f4338Ql = zb.mo594g(this.fhq, this.fhr);
        clearCache();
    }

    public void destroy() {
        clearCache();
        this.f4339Uy.mo583a(this.f4338Ql);
    }

    /* renamed from: a */
    public void mo12896a(float f, C5408aIe aie, C5362aGk agk) {
        this.stack.bcF();
        try {
            this.cao = aie;
            this.cap = f;
            this.fhs = agk;
            C3978xf xfVar = (C3978xf) this.stack.bcJ().get();
            xfVar.mo22952b(this.fhr.cFf());
            xfVar.mo22954c(this.fhq.cFf());
            this.fhq.cEZ().getAabb(xfVar, this.cam, this.can);
            Vec3f h = this.stack.bcH().mo4460h(f, f, f);
            this.can.add(h);
            this.cam.sub(h);
        } finally {
            this.stack.bcG();
        }
    }

    /* renamed from: a */
    public void mo820a(Vec3f[] vec3fArr, int i, int i2) {
        this.stack.bcH().push();
        try {
            this.fht.czU = this.f4339Uy;
            ayY ayy = this.fhr;
            if (!(this.cao == null || this.cao.iaO == null || this.cao.iaO.bnd() <= 0)) {
                Vec3f h = this.stack.bcH().mo4460h(255.0f, 255.0f, 0.0f);
                C3978xf cFf = ayy.cFf();
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                vec3f.set(vec3fArr[0]);
                cFf.mo22946G(vec3f);
                vec3f2.set(vec3fArr[1]);
                cFf.mo22946G(vec3f2);
                this.cao.iaO.mo4811i(vec3f, vec3f2, h);
                vec3f.set(vec3fArr[1]);
                cFf.mo22946G(vec3f);
                vec3f2.set(vec3fArr[2]);
                cFf.mo22946G(vec3f2);
                this.cao.iaO.mo4811i(vec3f, vec3f2, h);
                vec3f.set(vec3fArr[2]);
                cFf.mo22946G(vec3f);
                vec3f2.set(vec3fArr[0]);
                cFf.mo22946G(vec3f2);
                this.cao.iaO.mo4811i(vec3f, vec3f2, h);
            }
            if (this.fhq.cEZ().aiF()) {
                this.fhu.mo5351k(vec3fArr[0], vec3fArr[1], vec3fArr[2]);
                this.fhu.setMargin(this.cap);
                C0802Lc cEZ = ayy.cEZ();
                ayy.mo17012a(this.fhu);
                C3808vG a = this.fht.czU.mo581a(this.fhq, this.fhr, this.f4338Ql);
                this.fhs.mo5216b(-1, -1, i, i2);
                a.mo8931a(this.fhq, this.fhr, this.cao, this.fhs);
                a.destroy();
                ayy.mo17012a(cEZ);
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void clearCache() {
        this.f4339Uy.mo587b(this.f4338Ql);
    }

    public Vec3f asv() {
        return this.cam;
    }

    public Vec3f asw() {
        return this.can;
    }
}
