package p001a;

/* renamed from: a.JK */
/* compiled from: a */
public class C0646JK implements C6863avD {
    public final C3978xf dlC = new C3978xf();
    public final C3978xf dlD = new C3978xf();
    public final C3978xf dlE = new C3978xf();

    public C0646JK() {
        this.dlC.setIdentity();
        this.dlD.setIdentity();
        this.dlE.setIdentity();
    }

    public C0646JK(C3978xf xfVar) {
        this.dlC.mo22947a(xfVar);
        this.dlD.setIdentity();
        this.dlE.mo22947a(xfVar);
    }

    public C0646JK(C3978xf xfVar, C3978xf xfVar2) {
        this.dlC.mo22947a(xfVar);
        this.dlD.mo22947a(xfVar2);
        this.dlE.mo22947a(xfVar);
    }

    /* renamed from: e */
    public void mo2960e(C3978xf xfVar) {
        xfVar.mo22952b(this.dlD);
        xfVar.mo22954c(this.dlC);
    }

    /* renamed from: f */
    public void mo2961f(C3978xf xfVar) {
        this.dlC.mo22947a(xfVar);
        this.dlC.mo22954c(this.dlD);
    }
}
