package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;

/* renamed from: a.Xw */
/* compiled from: a */
public class C1635Xw extends C1083Pr {
    private final C6460anQ[] bmX = {new C6460anQ(), new C6460anQ(), new C6460anQ()};
    private final Vec3f eJb = new Vec3f();
    private final Vec3f eJc = new Vec3f();
    public C1636a eJd = new C1636a();

    public C1635Xw() {
        super(aVP.POINT2POINT_CONSTRAINT_TYPE);
    }

    public C1635Xw(C6238ajC ajc, C6238ajC ajc2, Vec3f vec3f, Vec3f vec3f2) {
        super(aVP.POINT2POINT_CONSTRAINT_TYPE, ajc, ajc2);
        this.eJb.set(vec3f);
        this.eJc.set(vec3f2);
    }

    public C1635Xw(C6238ajC ajc, Vec3f vec3f) {
        super(aVP.POINT2POINT_CONSTRAINT_TYPE, ajc);
        this.eJb.set(vec3f);
        this.eJc.set(vec3f);
        ajc.ceu().mo22946G(this.eJc);
    }

    /* renamed from: PU */
    public void mo4840PU() {
        this.stack.bcF();
        try {
            this.dMa = 0.0f;
            Vec3f h = this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f);
            Matrix3fWrap ajd = (Matrix3fWrap) this.stack.bcK().get();
            Matrix3fWrap ajd2 = (Matrix3fWrap) this.stack.bcK().get();
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < 3) {
                    C0647JL.m5590a(h, i2, 1.0f);
                    ajd.transpose(this.dQm.ceu().bFF);
                    ajd2.transpose(this.dQn.ceu().bFF);
                    vec3f.set(this.eJb);
                    this.dQm.ceu().mo22946G(vec3f);
                    vec3f.sub(this.dQm.ces());
                    vec3f2.set(this.eJc);
                    this.dQn.ceu().mo22946G(vec3f2);
                    vec3f2.sub(this.dQn.ces());
                    this.bmX[i2].mo14967a(ajd, ajd2, vec3f, vec3f2, h, this.dQm.ceq(), this.dQm.aRf(), this.dQn.ceq(), this.dQn.aRf());
                    C0647JL.m5590a(h, i2, 0.0f);
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        } finally {
            this.stack.bcG();
        }
    }

    /* renamed from: cv */
    public void mo4847cv(float f) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            Vec3f ac = this.stack.bcH().mo4458ac(this.eJb);
            this.dQm.ceu().mo22946G(ac);
            Vec3f ac2 = this.stack.bcH().mo4458ac(this.eJc);
            this.dQn.ceu().mo22946G(ac2);
            Vec3f h = this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f);
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < 3) {
                    C0647JL.m5590a(h, i2, 1.0f);
                    float clX = 1.0f / this.bmX[i2].clX();
                    Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                    vec3f3.sub(ac, this.dQm.ces());
                    Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                    vec3f4.sub(ac2, this.dQn.ces());
                    Vec3f ac3 = this.stack.bcH().mo4458ac(this.dQm.mo13897R(vec3f3));
                    Vec3f ac4 = this.stack.bcH().mo4458ac(this.dQn.mo13897R(vec3f4));
                    Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                    vec3f5.sub(ac3, ac4);
                    float dot = h.dot(vec3f5);
                    vec3f.sub(ac, ac2);
                    float f2 = ((((-vec3f.dot(h)) * this.eJd.feX) / f) * clX) - ((dot * this.eJd.damping) * clX);
                    this.dMa += f2;
                    Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
                    vec3f6.scale(f2, h);
                    vec3f.sub(ac, this.dQm.ces());
                    this.dQm.mo13937m(vec3f6, vec3f);
                    vec3f.negate(vec3f6);
                    vec3f2.sub(ac2, this.dQn.ces());
                    this.dQn.mo13937m(vec3f, vec3f2);
                    C0647JL.m5590a(h, i2, 0.0f);
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: cw */
    public void mo6864cw(float f) {
    }

    /* renamed from: ap */
    public void mo6860ap(Vec3f vec3f) {
        this.eJb.set(vec3f);
    }

    /* renamed from: aq */
    public void mo6861aq(Vec3f vec3f) {
        this.eJc.set(vec3f);
    }

    public Vec3f bFO() {
        return this.eJb;
    }

    public Vec3f bFP() {
        return this.eJc;
    }

    /* renamed from: a.Xw$a */
    public static class C1636a {
        public float damping = 1.0f;
        public float feX = 0.3f;
    }
}
