package p001a;

import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.item.aUR;

import javax.swing.*;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;

/* renamed from: a.AU */
/* compiled from: a */
public abstract class C0037AU implements TreeCellRenderer {
    /* renamed from: a */
    public abstract Component mo307a(aUR aur, Object obj, boolean z, boolean z2, boolean z3, int i, boolean z4);

    public Component getTreeCellRendererComponent(JTree jTree, Object obj, boolean z, boolean z2, boolean z3, int i, boolean z4) {
        Component a = mo307a((aUR) jTree.getClientProperty(aUR.propertyKey), obj, z, z2, z3, i, z4);
        IComponentManager e = ComponentManager.getCssHolder(a);
        int i2 = 0;
        if (z) {
            i2 = 2;
        }
        if (z4) {
            i2 |= 128;
        }
        if (z4) {
            i2 |= 256;
        }
        e.mo13063q(386, i2);
        m407e(a, 386, i2);
        return a;
    }

    /* renamed from: e */
    private void m407e(Component component, int i, int i2) {
        IComponentManager e = ComponentManager.getCssHolder(component);
        if (e != null) {
            e.mo13063q(i, i2);
        }
        if (component instanceof Container) {
            Container container = (Container) component;
            int componentCount = container.getComponentCount();
            for (int i3 = 0; i3 < componentCount; i3++) {
                m407e(container.getComponent(i3), i, i2);
            }
        }
    }
}
