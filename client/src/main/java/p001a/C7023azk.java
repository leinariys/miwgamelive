package p001a;

import game.script.ship.Ship;
import logic.ui.C2698il;
import taikodom.addon.IAddonProperties;

/* renamed from: a.azk  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C7023azk {
    private C2698il djv;
    private Ship gYK;

    /* renamed from: kj */
    private IAddonProperties f5702kj;

    public C7023azk(IAddonProperties vWVar, C2698il ilVar) {
        this.f5702kj = vWVar;
        this.djv = ilVar;
    }

    public C2698il cFB() {
        return this.djv;
    }

    /* access modifiers changed from: protected */
    public IAddonProperties bUA() {
        return this.f5702kj;
    }

    /* renamed from: M */
    public void mo4307M(Ship fAVar) {
        this.gYK = fAVar;
    }

    public boolean isVisible() {
        return cFB().isVisible();
    }

    public void setVisible(boolean z) {
        cFB().setVisible(z);
    }

    public Ship cFC() {
        return this.gYK;
    }
}
