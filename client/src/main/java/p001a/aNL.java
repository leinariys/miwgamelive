package p001a;

import logic.baa.C1616Xf;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aNL */
/* compiled from: a */
public class aNL extends C3813vL {
    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) {
        if (obj == null) {
            objectOutput.writeByte(2);
        } else {
            objectOutput.writeByte(((Boolean) obj).booleanValue() ? 1 : 0);
        }
    }

    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) {
        switch (objectInput.readByte()) {
            case 0:
                return false;
            case 1:
                return true;
            case 2:
                return null;
            default:
                throw new IllegalStateException();
        }
    }

    /* renamed from: b */
    public Object mo3591b(C1616Xf xf) {
        return Boolean.FALSE;
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public Object mo7920f(ObjectInput objectInput) {
        throw new IllegalAccessError();
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo7919d(ObjectOutput objectOutput, Object obj) {
        throw new IllegalAccessError();
    }
}
