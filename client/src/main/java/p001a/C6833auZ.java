package p001a;

import logic.thred.ThreadWrapper;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Constructor;
import java.net.URISyntaxException;
import java.net.URL;

/* renamed from: a.auZ  reason: case insensitive filesystem */
/* compiled from: a */
public class C6833auZ extends ClassLoader {
    public C6833auZ() {
        super(C6833auZ.class.getClassLoader());
    }

    /* renamed from: kh */
    public static Thread m26352kh(String str) {
        return m26351a(str, (Class<?>[]) null, (Object[]) null);
    }

    /* renamed from: a */
    public static Thread m26351a(String str, Class<?>[] clsArr, Object[] objArr) {
        C6833auZ auz = new C6833auZ();
        Constructor<?> declaredConstructor = Class.forName(str, true, auz).getDeclaredConstructor(clsArr);
        declaredConstructor.setAccessible(true);
        ThreadWrapper ano = new ThreadWrapper((Runnable) declaredConstructor.newInstance(objArr), "IsolatedClassLoader");
        ano.setContextClassLoader(auz);
        return ano;
    }

    /* access modifiers changed from: protected */
    public synchronized Class<?> loadClass(String str, boolean z) {
        Class<?> findLoadedClass;
        findLoadedClass = findLoadedClass(str);
        if (findLoadedClass == null) {
            try {
                findLoadedClass = findClass(str);
            } catch (ClassNotFoundException e) {
                findLoadedClass = findSystemClass(str);
            }
        }
        if (z) {
            resolveClass(findLoadedClass);
        }
        return findLoadedClass;
    }

    /* access modifiers changed from: protected */
    public Class<?> findClass(String str) {
        File ki = m26353ki(str);
        if (ki == null) {
            throw new ClassNotFoundException(str);
        }
        try {
            byte[] l = m26354l(ki);
            return defineClass(str, l, 0, l.length);
        } catch (Exception e) {
            throw new ClassNotFoundException(str, e);
        }
    }

    /* renamed from: l */
    private byte[] m26354l(File file) {
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] bArr = new byte[((int) file.length())];
        int i = 0;
        do {
            i += fileInputStream.read(bArr, i, bArr.length - i);
        } while (i < bArr.length);
        return bArr;
    }

    /* renamed from: ki */
    private File m26353ki(String str) {
        File file = null;
        String[] split = str.split("\\.");
        for (int i = 0; i < split.length; i++) {
            StringBuffer stringBuffer = new StringBuffer(split[0]);
            for (int i2 = 1; i2 < split.length; i2++) {
                if (i >= split.length - i2) {
                    stringBuffer.append("$");
                } else {
                    stringBuffer.append(File.separator);
                }
                stringBuffer.append(split[i2]);
            }
            stringBuffer.append(".class");
            URL resource = getParent().getResource(stringBuffer.toString());
            if (resource != null) {
                try {
                    file = new File(resource.toURI().getPath());
                } catch (URISyntaxException e) {
                    throw new Error(e);
                }
            }
            if (file != null) {
                break;
            }
        }
        return file;
    }
}
