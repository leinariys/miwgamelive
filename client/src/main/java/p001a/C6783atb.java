package p001a;

import game.script.Character;
import org.mozilla1.javascript.ScriptRuntime;

import java.lang.reflect.Method;

/* renamed from: a.atb  reason: case insensitive filesystem */
/* compiled from: a */
public class C6783atb {
    /* renamed from: a */
    public static Method m25908a(Object obj, String str, Object[] objArr) {
        boolean z;
        for (Method method : obj.getClass().getMethods()) {
            if (method.getName().equals(str)) {
                Class[] parameterTypes = method.getParameterTypes();
                if (objArr == null && parameterTypes.length == 0) {
                    return method;
                }
                if (objArr.length == parameterTypes.length) {
                    int i = 0;
                    while (true) {
                        if (i >= parameterTypes.length) {
                            z = true;
                            break;
                        } else if (!m25909g(parameterTypes[i], objArr[i])) {
                            z = false;
                            break;
                        } else {
                            i++;
                        }
                    }
                    if (z) {
                        return method;
                    }
                } else {
                    continue;
                }
            }
        }
        throw new NoSuchMethodException(str);
    }

    /* renamed from: g */
    private static boolean m25909g(Class<?> cls, Object obj) {
        if (cls.isInstance(obj)) {
            return true;
        }
        Class<?> cls2 = obj.getClass();
        if (cls == Integer.TYPE && cls2 == Integer.class) {
            return true;
        }
        if (cls == Double.TYPE && cls2 == Double.class) {
            return true;
        }
        if (cls == Long.TYPE && cls2 == Long.class) {
            return true;
        }
        if (cls == Float.TYPE && cls2 == Float.class) {
            return true;
        }
        if (cls == Character.TYPE && cls2 == Character.class) {
            return true;
        }
        if (cls == Byte.TYPE && cls2 == Byte.class) {
            return true;
        }
        if (cls == Character.TYPE && cls2 == Character.class) {
            return true;
        }
        if (cls == Short.TYPE && cls2 == Short.class) {
            return true;
        }
        if (cls == Void.TYPE && cls2 == Void.class) {
            return true;
        }
        return false;
    }

    /* renamed from: h */
    public static Object m25910h(Class<?> cls, Object obj) {
        if (obj != null || !cls.isPrimitive()) {
            return obj;
        }
        if (cls == Integer.TYPE) {
            return 0;
        }
        if (cls == Float.TYPE) {
            return Float.valueOf(0.0f);
        }
        if (cls == Double.TYPE) {
            return Double.valueOf(ScriptRuntime.NaN);
        }
        if (cls == Long.TYPE) {
            return 0L;
        }
        if (cls == Short.TYPE) {
            return (short) 0;
        }
        if (cls == Byte.TYPE) {
            return (byte) 0;
        }
        if (cls == Character.TYPE) {
            return 0;
        }
        if (cls == Boolean.TYPE) {
            return false;
        }
        if (cls == Void.TYPE) {
            return null;
        }
        throw new RuntimeException("I dont know how to deal with " + cls.getName());
    }
}
