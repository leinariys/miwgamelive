package p001a;

import game.network.message.IReadProto;
import util.Syst;

import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;

/* renamed from: a.iW */
/* compiled from: a */
public class C2683iW {
    public IReadProto agC;
    private boolean agD;
    private ArrayList<Object> agE = new ArrayList<>();

    /* renamed from: a */
    public void mo19626a(IReadProto ie) {
        String str;
        this.agE.add((Object) null);
        this.agC = ie;
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        while (true) {
            try {
                int a = ie.readBits(4);
                if (a >= 8) {
                    a = (a << 4) | ie.readBits(4);
                }
                if (a != 135) {
                    String Eh = m33407Eh();
                    if (Eh != null) {
                        sb2.append(sb).append(Eh).append(": ");
                    } else {
                        sb2.append(sb).append("- ");
                    }
                    switch (a) {
                        case 0:
                            if (ie.readBoolean()) {
                                str = "true";
                            } else {
                                str = "false";
                            }
                            sb2.append(str);
                            break;
                        case 1:
                            sb2.append(ie.readByte()).append("b");
                            break;
                        case 2:
                            sb2.append(ie.readShort()).append("s");
                            break;
                        case 3:
                            sb2.append('\'').append(ie.readChar()).append('\'');
                            break;
                        case 4:
                            sb2.append(ie.readInt()).append("i");
                            break;
                        case 5:
                            sb2.append(ie.readLong()).append("L");
                            break;
                        case 6:
                            sb2.append(ie.readFloat()).append("f");
                            break;
                        case 7:
                            int readInt = ie.readInt();
                            sb2.append(ie.readBits(readInt)).append(":").append(readInt);
                            break;
                        case 128:
                            sb2.append(ie.readDouble()).append("d");
                            break;
                        case 129:
                            sb2.append("null");
                            break;
                        case 131:
                            sb2.append('\"').append(readUTF()).append('\"');
                            break;
                        case 133:
                            String str2 = (String) m33408Ei();
                            if (!this.agD) {
                                sb2.append(str2);
                                break;
                            } else {
                                String str3 = "!!enum:" + mo19625Eg() + "." + readUTF() + ":" + ie.readInt();
                                this.agE.add(str3);
                                sb2.append(str3);
                                break;
                            }
                        case 134:
                            sb2.append(" { ");
                            sb.append("  ");
                            break;
                        case 145:
                        case 146:
                        case 147:
                        case 240:
                            sb2.append(" !").append(ayF.names[a]);
                            sb2.append(" - ");
                            sb2.append(mo19625Eg());
                            sb2.append(" { ");
                            sb.append("  ");
                            mo19625Eg();
                            sb.append("  ");
                            break;
                        case 161:
                            int readInt2 = ie.readInt();
                            byte[] bArr = new byte[readInt2];
                            ie.readFully(bArr);
                            sb2.append("!!binary ").append(readInt2).append(" [");
                            sb2.append(13).append(10);
                            sb2.append(Syst.byteToStringLog(sb + " ", bArr, 0, readInt2, " ", 8));
                            sb2.append(sb).append(']');
                            break;
                        case 170:
                            Object Ei = m33408Ei();
                            int readInt3 = ie.readInt();
                            if (!this.agD) {
                                if (Ei == null) {
                                    sb2.append("null");
                                    break;
                                } else {
                                    sb2.append("-->").append(Ei);
                                    break;
                                }
                            } else {
                                sb2.append('!').append(mo19625Eg()).append("%" + readInt3).append(':');
                                sb2.append(" { ");
                                sb.append("  ");
                                this.agE.add(Integer.valueOf(this.agE.size()));
                                break;
                            }
                    }
                } else {
                    sb.setLength(sb.length() - 2);
                    sb2.append(sb).append('}');
                }
                sb2.append(13).append(10);
            } catch (EOFException e) {
                System.out.println(sb2.toString());
                this.agE.clear();
                return;
            } catch (RuntimeException e2) {
                System.out.println(sb2.toString());
                throw e2;
            } catch (Throwable th) {
                this.agE.clear();
                throw th;
            }
        }
    }

    /* access modifiers changed from: protected */
    public String readUTF() {
        String str = (String) m33408Ei();
        if (!this.agD) {
            return str;
        }
        String readUTF = this.agC.readUTF();
        this.agE.add(readUTF);
        return readUTF;
    }

    /* access modifiers changed from: protected */
    /* renamed from: Eg */
    public String mo19625Eg() {
        String str = (String) m33408Ei();
        if (!this.agD) {
            return str;
        }
        String readUTF = readUTF();
        this.agE.add(readUTF);
        return readUTF;
    }

    /* renamed from: Eh */
    private String m33407Eh() {
        return readUTF();
    }

    /* renamed from: Ei */
    private Object m33408Ei() {
        if (this.agC.readBits(1) == 0) {
            this.agD = false;
            return null;
        } else if (this.agC.readBits(1) == 0) {
            this.agD = false;
            return this.agE.get(this.agC.readInt());
        } else {
            this.agD = true;
            return null;
        }
    }

    /* renamed from: a */
    private int m33409a(int i) {
        try {
            return this.agC.readBits(i);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
