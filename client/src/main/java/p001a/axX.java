package p001a;

/* renamed from: a.axX */
/* compiled from: a */
public class axX {
    private static final char[] gRB = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private static byte gRA = -1;
    private static byte[] gRz = null;

    private static void cDn() {
        gRz = new byte[128];
        for (int i = 0; i < 128; i++) {
            if (i < 48 || i > 102) {
                gRz[i] = gRA;
            } else if (i > 57 && i < 65) {
                gRz[i] = gRA;
            } else if (i > 70 && i < 97) {
                gRz[i] = gRA;
            } else if (i <= 57) {
                gRz[i] = (byte) (i - 48);
            } else if (i <= 70) {
                gRz[i] = (byte) ((i - 65) + 10);
            } else if (i <= 102) {
                gRz[i] = (byte) ((i - 97) + 10);
            }
        }
    }

    public static byte[] decode(String str) {
        byte b;
        byte[] bArr = new byte[(str.length() / 2)];
        if (gRz == null) {
            cDn();
        }
        byte b2 = 0;
        boolean z = false;
        int i = 0;
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            if (charAt <= 127 && (b = gRz[charAt]) != gRA) {
                if (z) {
                    bArr[i] = (byte) ((b2 << 4) | b);
                    z = false;
                    i++;
                } else {
                    z = true;
                    b2 = b;
                }
            }
        }
        return bArr;
    }

    public static String encode(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < bArr.length; i++) {
            stringBuffer.append(gRB[(bArr[i] & 240) >> 4]);
            stringBuffer.append(gRB[bArr[i] & 15]);
        }
        return stringBuffer.toString();
    }
}
