package p001a;

import java.io.PrintWriter;
import java.util.StringTokenizer;

/* renamed from: a.dd */
/* compiled from: a */
public class C2287dd {

    /* renamed from: yh */
    C1942an f6574yh;

    public C2287dd(C1942an anVar) {
        this.f6574yh = anVar;
    }

    /* renamed from: a */
    public void mo17899a(String str, PrintWriter printWriter) {
        StringTokenizer stringTokenizer = new StringTokenizer(str.trim(), " ");
        if (!stringTokenizer.hasMoreTokens()) {
            printWriter.println("Unknown command");
            printWriter.flush();
            return;
        }
        String nextToken = stringTokenizer.nextToken();
        try {
            if (nextToken.equals("load")) {
                printWriter.println("Component loaded, handler: " + this.f6574yh.mo2410r(stringTokenizer.nextToken()));
            } else if (nextToken.equals("unload")) {
                this.f6574yh.mo2409q(stringTokenizer.nextToken());
                printWriter.println("Component unloaded");
            } else if (nextToken.equals("start")) {
                this.f6574yh.start(stringTokenizer.nextToken());
                printWriter.println("Component started");
            } else if (nextToken.equals("stop")) {
                this.f6574yh.stop(stringTokenizer.nextToken());
                printWriter.println("Component stopped");
            } else if (nextToken.equals("restart")) {
                this.f6574yh.mo2408o(stringTokenizer.nextToken());
                printWriter.println("Component restarted");
            } else if (nextToken.equals("?") || "help".equals(nextToken) || nextToken.length() == 0) {
                printWriter.println("commands:");
                printWriter.println("   add <path to component descriptor file>");
                printWriter.println("   start <handler>");
                printWriter.println("   stop <handler>");
                printWriter.println("   restart <handler>");
            } else {
                printWriter.println("Unknown command");
            }
        } catch (C1400UX e) {
            printWriter.println("Error executing: " + nextToken);
            e.printStackTrace(printWriter);
        }
        printWriter.flush();
    }
}
