package p001a;

import game.network.message.externalizable.C2651i;
import game.script.ship.Ship;
import taikodom.addon.hud.HudShipVelocityAddon;

/* renamed from: a.lq */
/* compiled from: a */
class C2923lq extends C3428rT<C2651i> {
    final /* synthetic */ HudShipVelocityAddon axl;

    public C2923lq(HudShipVelocityAddon hudShipVelocityAddon) {
        this.axl = hudShipVelocityAddon;
    }

    /* renamed from: a */
    public void mo321b(C2651i iVar) {
        Ship a = iVar.mo19441a();
        if (a != null) {
            this.axl.mo23972b(a);
        }
    }
}
