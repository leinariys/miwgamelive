package p001a;

import org.mozilla1.javascript.ScriptRuntime;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/* renamed from: a.tZ */
/* compiled from: a */
public class C3672tZ {
    public Class<?> clazz;

    /* renamed from: yi */
    public Constructor<?> f9259yi;

    public C3672tZ() {
    }

    public C3672tZ(Class<?> cls) {
        this.clazz = cls;
        this.f9259yi = cls.getConstructor(new Class[]{String.class});
    }

    /* renamed from: O */
    public Object mo22256O(String str) {
        try {
            return this.f9259yi.newInstance(new Object[]{str});
        } catch (InvocationTargetException e) {
            if (Number.class.isAssignableFrom(this.clazz) && e.getCause() != null && (e.getCause() instanceof NumberFormatException)) {
                double parseDouble = Double.parseDouble(str);
                if (parseDouble % 1.0d == ScriptRuntime.NaN) {
                    return this.f9259yi.newInstance(new Object[]{String.valueOf((long) parseDouble)});
                }
            }
            throw e;
        }
    }
}
