package p001a;

/* renamed from: a.dG */
/* compiled from: a */
public class C2240dG {
    private final String file;
    private final String handle;

    public C2240dG(C5990aeO aeo) {
        this.file = aeo.getFile();
        this.handle = aeo.getHandle();
    }

    public C2240dG(String str, String str2) {
        this.file = str;
        this.handle = str2;
    }

    public String getFile() {
        return this.file;
    }

    public String getHandle() {
        return this.handle;
    }
}
