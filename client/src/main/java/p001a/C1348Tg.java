package p001a;

import logic.C6961axa;
import logic.ui.C2698il;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

/* renamed from: a.Tg */
/* compiled from: a */
public class C1348Tg {
    private static final String iXb = "spinnable-combobox.xml";
    private static final String iXc = "next";
    private static final String iXd = "previous";
    private static final String iXe = "combobox";
    /* access modifiers changed from: private */
    public final C2698il iXf;

    public C1348Tg(C6961axa axa, C2698il ilVar) {
        this.iXf = (C2698il) axa.mo16794bQ(iXb);
        ilVar.add(this.iXf);
        JComboBox cd = this.iXf.mo4915cd(iXe);
        JButton cb = this.iXf.mo4913cb(iXc);
        this.iXf.mo4913cb(iXd).addActionListener(new C1351c(cd));
        cb.addActionListener(new C1350b(cd));
        cd.addComponentListener(new C1349a());
    }

    public JComboBox dAb() {
        return this.iXf.mo4915cd(iXe);
    }

    /* renamed from: a.Tg$c */
    /* compiled from: a */
    class C1351c implements ActionListener {
        private final /* synthetic */ JComboBox ehF;

        C1351c(JComboBox jComboBox) {
            this.ehF = jComboBox;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            int itemCount = this.ehF.getItemCount();
            int selectedIndex = this.ehF.getSelectedIndex();
            if (itemCount != 0) {
                if (selectedIndex == -1) {
                    this.ehF.setSelectedIndex(0);
                } else if (selectedIndex == 0) {
                    this.ehF.setSelectedIndex(itemCount - 1);
                } else {
                    this.ehF.setSelectedIndex(selectedIndex - 1);
                }
                this.ehF.repaint();
            }
        }
    }

    /* renamed from: a.Tg$b */
    /* compiled from: a */
    class C1350b implements ActionListener {
        private final /* synthetic */ JComboBox ehF;

        C1350b(JComboBox jComboBox) {
            this.ehF = jComboBox;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            int itemCount = this.ehF.getItemCount();
            int selectedIndex = this.ehF.getSelectedIndex();
            if (itemCount != 0) {
                if (selectedIndex == -1) {
                    this.ehF.setSelectedIndex(0);
                } else if (selectedIndex == itemCount - 1) {
                    this.ehF.setSelectedIndex(0);
                } else {
                    this.ehF.setSelectedIndex(selectedIndex + 1);
                }
                this.ehF.repaint();
            }
        }
    }

    /* renamed from: a.Tg$a */
    class C1349a implements ComponentListener {
        C1349a() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            C1348Tg.this.iXf.setVisible(false);
        }

        public void componentMoved(ComponentEvent componentEvent) {
        }

        public void componentResized(ComponentEvent componentEvent) {
        }

        public void componentShown(ComponentEvent componentEvent) {
            C1348Tg.this.iXf.setVisible(true);
        }
    }
}
