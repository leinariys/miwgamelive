package p001a;

import game.script.Character;
import logic.baa.C1616Xf;
import logic.data.mbean.C6064afk;
import logic.res.XmlNode;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.thred.LogPrinter;
import org.apache.mina.util.IdentityHashSet;

import java.io.*;
import java.lang.reflect.Constructor;
import java.util.*;

/* renamed from: a.azX */
/* compiled from: a */
public final class azX {
    static LogPrinter log = LogPrinter.m10275K(azX.class);
    /* access modifiers changed from: private */
    public Set<Class<?>> cVt = new HashSet();
    /* access modifiers changed from: private */
    public aIX cVu = new aIX();
    ByteArrayOutputStream cVr = new ByteArrayOutputStream();
    ObjectOutputStream cVs = null;
    Set<C1616Xf> hct = new TreeSet(new C0906NI(this));
    Queue<C1616Xf> hcu = new LinkedList();
    Set<Object> objects = new IdentityHashSet();

    public azX(OutputStream outputStream, C1616Xf xf, long j) {
        long currentTimeMillis = System.currentTimeMillis();
        this.hct.add(xf);
        this.hcu.add(xf);
        C2018a aVar = new C2018a(this, (C2018a) null);
        while (true) {
            C1616Xf poll = this.hcu.poll();
            if (poll == null) {
                break;
            }
            m27680a(poll, (C2042bT) aVar);
        }
        log.info("objects to be serialized: " + this.objects.size());
        log.info("scripts to be serialized:" + this.hct.size());
        try {
            PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            printWriter.append("<root structureVersion=\"" + 1 + "\" version=\"").append(new StringBuilder().append(j).toString()).append("\">\n");
            printWriter.append("<scriptClasses>\n");
            C6280ajs ate = xf.bFf().mo6866PM().ate();
            for (Class next : ate.aXU()) {
                if (next != null) {
                    XmlNode agy = new XmlNode("scriptClass");
                    C5829abJ abj = (C5829abJ) next.getAnnotation(C5829abJ.class);
                    String value = abj != null ? abj.value() : "";
                    agy.addAttribute("class", next.getName());
                    C6494any B = ate.mo3258B(next);
                    if (!value.equals(B.getVersion())) {
                        log.warn("Diferent versions: " + value + "<>" + B.getVersion() + " for class " + next);
                    }
                    agy.addAttribute("version", value);
                    for (C5663aRz arz : B.mo15889c()) {
                        if (arz == null) {
                            log.warn("Field null at " + B.getName());
                        } else {
                            XmlNode agy2 = new XmlNode("field");
                            agy.addChildrenTag(agy2);
                            agy2.addAttribute("name", arz.name());
                            agy2.addAttribute("type", arz.mo11291El().getName());
                            if (arz.mo11295Eq() != null && arz.mo11295Eq().length > 0) {
                                StringBuilder sb = new StringBuilder();
                                for (Class cls : arz.mo11295Eq()) {
                                    if (sb.length() > 0) {
                                        sb.append(",");
                                    }
                                    sb.append(cls.getName());
                                }
                                agy2.addAttribute("componentTypes", sb.toString());
                            }
                        }
                    }
                    agy.buildStringFromNode(printWriter);
                }
            }
            printWriter.append("</scriptClasses>\n");
            for (C1616Xf next2 : this.hct) {
                XmlNode agy3 = new XmlNode("scriptObject");
                agy3.addAttribute("id", new StringBuilder().append(next2.bFf().getObjectId().getId()).toString());
                agy3.addAttribute("class", next2.getClass().getName());
                m27680a(next2, (C2042bT) new C2019b(this, agy3, next2, (C2019b) null));
                agy3.buildStringFromNode(printWriter);
            }
            printWriter.append("</root>\n");
            printWriter.flush();
            if (!this.cVt.isEmpty()) {
                log.info("Classes using default java serializer:");
                for (Class<?> canonicalName : this.cVt) {
                    log.info("  " + canonicalName.getCanonicalName());
                }
            }
            this.cVu.dfu();
            log.info("Xml serialization took: " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: a */
    public static C5663aRz m27679a(C1616Xf xf, String str) {
        C5663aRz arz;
        C5663aRz[] c = xf.mo25c();
        int length = c.length;
        int i = 0;
        while (true) {
            if (i < length) {
                arz = c[i];
                if (arz.name().equals(str)) {
                    break;
                }
                i++;
            } else {
                arz = null;
                break;
            }
        }
        if (arz != null) {
            return arz;
        }
        throw new RuntimeException("Field " + str + " not found");
    }

    /* access modifiers changed from: private */
    /* renamed from: w */
    public boolean m27683w(Class<?> cls) {
        return cls.isPrimitive() || cls == Integer.class || cls == Long.class || cls == Short.class || cls == Float.class || cls == Double.class || cls == Byte.class || cls == Character.class || cls == Boolean.class;
    }

    /* renamed from: a */
    private void m27680a(C1616Xf xf, C2042bT bTVar) {
        C5663aRz[] c = xf.mo25c();
        C6064afk cVj = ((C3582se) xf.bFf()).cVj();
        for (C5663aRz arz : c) {
            if (arz.mo11300Ew() != C1020Ot.C1021a.NOT_PERSISTED) {
                bTVar.mo17212b(arz, cVj.mo3216r(arz));
            }
        }
    }

    /* renamed from: a.azX$a */
    private final class C2018a implements C2042bT {
        private C2018a() {
        }

        /* synthetic */ C2018a(azX azx, C2018a aVar) {
            this();
        }

        public void writeFloat(float f) {
            throw new UnsupportedOperationException();
        }

        /* renamed from: b */
        public void mo17212b(C5663aRz arz, Object obj) {
            m27684aP(obj);
        }

        /* renamed from: aP */
        private void m27684aP(Object obj) {
            if (obj != null) {
                if (obj instanceof C1616Xf) {
                    C1616Xf xf = (C1616Xf) obj;
                    if (azX.this.hct.add(xf)) {
                        azX.this.hcu.add(xf);
                    }
                } else if (obj instanceof Collection) {
                    if (azX.this.objects.add(obj)) {
                        for (Object next : (Collection) obj) {
                            if (next != null) {
                                m27684aP(next);
                            }
                        }
                    }
                } else if ((obj instanceof Map) && azX.this.objects.add(obj)) {
                    for (Map.Entry entry : ((Map) obj).entrySet()) {
                        m27684aP(entry.getKey());
                        m27684aP(entry.getValue());
                    }
                }
            }
        }
    }

    /* renamed from: a.azX$b */
    /* compiled from: a */
    private final class C2019b implements C2042bT {
        private final XmlNode gzB;

        /* synthetic */ C2019b(azX azx, XmlNode agy, C1616Xf xf, C2019b bVar) {
            this(agy, xf);
        }

        private C2019b(XmlNode agy, C1616Xf xf) {
            this.gzB = agy;
        }

        public final void writeFloat(float f) {
            throw new UnsupportedOperationException();
        }

        /* renamed from: b */
        public final void mo17212b(C5663aRz arz, Object obj) {
            if (obj != null) {
                if (obj instanceof Collection) {
                    if (((Collection) obj).isEmpty()) {
                        return;
                    }
                } else if ((obj instanceof Map) && ((Map) obj).isEmpty()) {
                    return;
                }
                XmlNode mz = this.gzB.addChildrenTag(arz.name());
                if (!arz.mo11291El().isPrimitive() && arz.mo11291El() != obj.getClass() && !arz.isCollection() && !arz.mo11292En()) {
                    mz.addAttribute("class", obj.getClass().getName());
                }
                m27686a(mz, arz, obj);
            }
        }

        /* renamed from: a */
        private final void m27686a(XmlNode agy, C5663aRz arz, Object obj) {
            if (obj instanceof C1616Xf) {
                C1616Xf xf = (C1616Xf) obj;
                if (!arz.isCollection() || arz.mo11295Eq()[0] != xf.getClass()) {
                    agy.addAttribute("class", xf.getClass().getName());
                }
                agy.addAttribute("scriptObject", new StringBuilder().append(xf.bFf().getObjectId().getId()).toString());
            } else if (obj instanceof Collection) {
                Collection collection = (Collection) obj;
                if (!collection.isEmpty()) {
                    for (Object next : collection) {
                        XmlNode mz = agy.addChildrenTag("item");
                        if (next == null) {
                            mz.addAttribute("null", "yes");
                        } else {
                            m27686a(mz, arz, next);
                        }
                    }
                }
            } else if (obj instanceof Map) {
                Map map = (Map) obj;
                if (map.isEmpty()) {
                    return;
                }
                if (arz.mo11295Eq().length != 2) {
                    throw new RuntimeException("You must specify the component types for the Map (example: DataMap<Integer,String> instead of just DataMap)");
                }
                for (Map.Entry entry : map.entrySet()) {
                    XmlNode mz2 = agy.addChildrenTag("item");
                    m27686a(mz2.addChildrenTag("key"), arz, entry.getKey());
                    m27686a(mz2.addChildrenTag("value"), arz, entry.getValue());
                }
            } else if (obj == null) {
                agy.addAttribute("null", "yes");
            } else {
                Class<?> cls = obj.getClass();
                if (azX.this.m27683w(cls)) {
                    agy.setText(new StringBuilder().append(obj).toString());
                } else if (m27688aw(cls)) {
                    agy.addAttribute("xObject", "true");
                    azX.this.cVu.mo9361g(obj, agy.getTagName());
                    agy.addAttribute("xId", azX.this.cVu.mo9358aI(obj));
                    for (XmlNode k : azX.this.cVu.dft()) {
                        agy.addChildrenTag(k);
                    }
                } else if (cls.isEnum()) {
                    agy.addAttribute("enum", "yes");
                    agy.addAttribute("class", cls.getName());
                    agy.addAttribute("member", ((Enum) obj).name());
                } else if (cls == Class.class) {
                    agy.addAttribute("isClass", "yes");
                    agy.addAttribute("class", ((Class) obj).getName());
                } else {
                    agy.addAttribute("blob", "yes");
                    agy.setText(m27687as(obj));
                }
            }
        }

        /* renamed from: aw */
        private boolean m27688aw(Class<?> cls) {
            try {
                for (Constructor parameterTypes : cls.getConstructors()) {
                    if (parameterTypes.getParameterTypes().length == 0) {
                        return true;
                    }
                }
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        /* renamed from: as */
        private final String m27687as(Object obj) {
            azX.this.cVt.add(obj.getClass());
            try {
                azX.this.cVs = new ObjectOutputStream(azX.this.cVr);
                azX.this.cVs.writeObject(obj);
                azX.this.cVs.flush();
                String i = m27689i(azX.this.cVr.toByteArray());
                azX.this.cVs.reset();
                azX.this.cVr.reset();
                return i;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        /* renamed from: i */
        private final String m27689i(byte[] bArr) {
            StringBuilder sb = new StringBuilder(bArr.length * 2);
            for (byte b : bArr) {
                if ((b & 255) < 16) {
                    sb.append("0").append(Integer.toHexString(b & 255));
                } else {
                    sb.append(Integer.toHexString(b & 255));
                }
            }
            return sb.toString();
        }
    }
}
