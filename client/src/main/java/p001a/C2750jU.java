package p001a;

import game.script.simulation.Space;
import logic.aaa.C1088Pv;
import logic.bbb.C4029yK;
import logic.thred.C1362Tq;

/* renamed from: a.jU */
/* compiled from: a */
public class C2750jU extends aBA {
    private static /* synthetic */ int[] apT;

    public C2750jU(Space ea, Trigger ajx, C4029yK yKVar) {
        super(ea, ajx, yKVar);
    }

    /* renamed from: IU */
    static /* synthetic */ int[] m34029IU() {
        int[] iArr = apT;
        if (iArr == null) {
            iArr = new int[C1088Pv.values().length];
            try {
                iArr[C1088Pv.ENTER.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C1088Pv.EXIT.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C1088Pv.HIT.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            apT = iArr;
        }
        return iArr;
    }

    /* renamed from: IM */
    public byte mo2438IM() {
        return 3;
    }

    /* renamed from: a */
    public void mo2543a(C1362Tq tq) {
        switch (m34029IU()[tq.mo5752Jl().ordinal()]) {
            case 2:
                m34028IT().mo7590z(((C0520HN) tq.mo5749Ji()).mo2568ha());
                return;
            case 3:
                m34028IT().mo7552B(((C0520HN) tq.mo5749Ji()).mo2568ha());
                return;
            default:
                super.mo2543a(tq);
                return;
        }
    }

    /* renamed from: IT */
    private Trigger m34028IT() {
        return (Trigger) mo2568ha();
    }
}
