package p001a;

import logic.res.XmlNode;
import taikodom.render.loader.provider.FilePath;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: a.Ru */
/* compiled from: a */
public class C1212Ru {
    private Map<FilePath, C1213a> hdm = new HashMap();

    /* renamed from: k */
    public C1213a mo5283k(FilePath ain) throws FileNotFoundException {
        C1213a aVar = this.hdm.get(ain);
        if (aVar != null) {
            return aVar;
        }
        if (!ain.exists()) {
            return null;
        }
        C1213a aVar2 = new C1213a();
        InputStream openInputStream = ain.openInputStream();
        try {
            XmlNode a = new LoaderFileXML().loadFileXml(openInputStream, "UTF-8");
            XmlNode mD = a.findNodeChildTagDepth("includes");
            if (mD != null) {
                for (XmlNode attribute : mD.getListChildrenTag()) {
                    aVar2.mo5287gu(attribute.getAttribute("file"));
                }
            }
            XmlNode mD2 = a.findNodeChildTagDepth("trails");
            if (mD2 != null) {
                for (XmlNode next : mD2.getListChildrenTag()) {
                    aVar2.mo5284a(new C1214b(next.getAttribute("attach_point"), next.getAttribute("asset")));
                }
            }
            this.hdm.put(ain, aVar2);
            return aVar2;
        } finally {
            try {
                openInputStream.close();
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: a.Ru$b */
    /* compiled from: a */
    public class C1214b {
        private String edT;
        private String edU;

        C1214b(String str, String str2) {
            this.edT = str;
            this.edU = str2;
        }

        public String btt() {
            return this.edT;
        }

        public String btu() {
            return this.edU;
        }
    }

    /* renamed from: a.Ru$a */
    public class C1213a {
        private List<String> eaf = new ArrayList();
        private List<C1214b> eag = new ArrayList();

        public C1213a() {
        }

        /* renamed from: a */
        public void mo5284a(C1214b bVar) {
            this.eag.add(bVar);
        }

        /* renamed from: gu */
        public void mo5287gu(String str) {
            this.eaf.add(str);
        }

        public List<String> brr() {
            return this.eaf;
        }

        public List<C1214b> brs() {
            return this.eag;
        }
    }
}
