package p001a;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

/* renamed from: a.Wl */
/* compiled from: a */
public class C1551Wl {
    private static final C1551Wl exh = new C1551Wl("java/lang/Double", "doubleValue", "D");
    private static final C1551Wl exi = new C1551Wl("java/lang/Float", "floatValue", "F");
    private static final C1551Wl exj = new C1551Wl("java/lang/Character", "charValue", "C");
    private static final C1551Wl exk = new C1551Wl("java/lang/Long", "longValue", "J");
    private static final C1551Wl exl = new C1551Wl("java/lang/Byte", "byteValue", "B");
    private static final C1551Wl exm = new C1551Wl("java/lang/Boolean", "booleanValue", "Z");
    private static final C1551Wl exn = new C1551Wl("java/lang/Short", "shortValue", "S");
    private static final C1551Wl exo = new C1551Wl("java/lang/Integer", "intValue", "I");
    public final String exp;
    public final String exq;
    public final String exr;
    public final String type;

    private C1551Wl(String str, String str2, String str3) {
        this.type = str;
        this.exp = str2;
        this.exq = "()" + str3;
        this.exr = "(" + str3 + ")V";
    }

    /* renamed from: a */
    public static boolean m11315a(Type type2, MethodVisitor methodVisitor) {
        C1551Wl oX = m11318oX(type2.getSort());
        if (oX == null) {
            return false;
        }
        methodVisitor.visitTypeInsn(192, oX.type);
        methodVisitor.visitMethodInsn(182, oX.type, oX.exp, oX.exq);
        return true;
    }

    /* renamed from: a */
    public static C1551Wl m11312a(Type type2) {
        return m11318oX(type2.getSort());
    }

    /* renamed from: oX */
    private static C1551Wl m11318oX(int i) {
        switch (i) {
            case 1:
                return exm;
            case 2:
                return exj;
            case 3:
                return exl;
            case 4:
                return exn;
            case 5:
                return exo;
            case 6:
                return exi;
            case 7:
                return exk;
            case 8:
                return exh;
            default:
                return null;
        }
    }

    /* renamed from: a */
    public static void m11314a(Type type2, MethodVisitor methodVisitor, int i, String str, String str2, String str3, int i2) {
        C1551Wl a = m11312a(type2);
        if (a == null) {
            methodVisitor.visitVarInsn(25, i2);
            methodVisitor.visitMethodInsn(i, str, str2, str3);
            return;
        }
        methodVisitor.visitTypeInsn(187, a.type);
        methodVisitor.visitInsn(89);
        methodVisitor.visitVarInsn(25, i2);
        methodVisitor.visitMethodInsn(i, str, str2, str3);
        methodVisitor.visitMethodInsn(183, a.type, C2821kb.arR, a.exr);
    }

    /* renamed from: a */
    public static void m11313a(Type type2, MethodVisitor methodVisitor, int i) {
        C1551Wl a = m11312a(type2);
        if (a == null) {
            methodVisitor.visitVarInsn(type2.getOpcode(21), i);
            return;
        }
        methodVisitor.visitTypeInsn(187, a.type);
        methodVisitor.visitInsn(89);
        methodVisitor.visitVarInsn(type2.getOpcode(21), i);
        methodVisitor.visitMethodInsn(183, a.type, C2821kb.arR, a.exr);
    }

    /* renamed from: b */
    public static void m11316b(Type type2, MethodVisitor methodVisitor) {
        switch (type2.getSort()) {
            case 0:
                return;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                methodVisitor.visitInsn(3);
                return;
            case 6:
                methodVisitor.visitInsn(11);
                return;
            case 7:
                methodVisitor.visitInsn(9);
                return;
            case 8:
                methodVisitor.visitInsn(14);
                return;
            default:
                methodVisitor.visitInsn(1);
                return;
        }
    }

    /* renamed from: c */
    public static void m11317c(Type type2, MethodVisitor methodVisitor) {
        switch (type2.getSort()) {
            case 0:
                methodVisitor.visitFieldInsn(178, "java/lang/Void", "TYPE", "Ljava/lang/Class;");
                return;
            case 1:
                methodVisitor.visitFieldInsn(178, exm.type, "TYPE", "Ljava/lang/Class;");
                return;
            case 2:
                methodVisitor.visitFieldInsn(178, exj.type, "TYPE", "Ljava/lang/Class;");
                return;
            case 3:
                methodVisitor.visitFieldInsn(178, exl.type, "TYPE", "Ljava/lang/Class;");
                return;
            case 4:
                methodVisitor.visitFieldInsn(178, exn.type, "TYPE", "Ljava/lang/Class;");
                return;
            case 5:
                methodVisitor.visitFieldInsn(178, exo.type, "TYPE", "Ljava/lang/Class;");
                return;
            case 6:
                methodVisitor.visitFieldInsn(178, exi.type, "TYPE", "Ljava/lang/Class;");
                return;
            case 7:
                methodVisitor.visitFieldInsn(178, exk.type, "TYPE", "Ljava/lang/Class;");
                return;
            case 8:
                methodVisitor.visitFieldInsn(178, exh.type, "TYPE", "Ljava/lang/Class;");
                return;
            default:
                methodVisitor.visitLdcInsn(type2);
                return;
        }
    }
}
