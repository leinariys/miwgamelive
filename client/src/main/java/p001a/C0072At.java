package p001a;

import game.script.Actor;
import game.script.item.BulkItemType;
import game.script.ship.Outpost;
import game.script.storage.OutpostStorage;
import game.script.storage.StorageTransaction;
import logic.ui.C2698il;
import taikodom.addon.IAddonProperties;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/* renamed from: a.At */
/* compiled from: a */
public class C0072At extends C0690Jq {
    private JComboBox ebb;
    private JCheckBox ebc;
    private JCheckBox ebd;
    private JList ebe;
    private JTextField ebf;

    public C0072At(IAddonProperties vWVar, C2698il ilVar) {
        super(vWVar, ilVar);
        this.ebb = ilVar.mo4914cc("last-record-combobox");
        String translate = vWVar.translate("days");
        for (int i = 5; i > 1; i--) {
            this.ebb.addItem(new C5393aHp(String.valueOf(i) + " " + translate, String.valueOf(i) + " " + translate));
        }
        this.ebb.addItem(new C5393aHp(vWVar.translate("today"), vWVar.translate("today")));
        this.ebb.setSelectedIndex(-1);
        this.ebc = ilVar.mo4915cd("addition-field");
        this.ebc.setText(vWVar.translate("Addition"));
        this.ebc.setSelected(true);
        this.ebd = ilVar.mo4915cd("removal-field");
        this.ebd.setText(vWVar.translate("Removal"));
        this.ebd.setSelected(true);
        this.ebe = ilVar.mo4915cd("records-table");
        this.ebe.setModel(new DefaultListModel());
        this.ebe.setCellRenderer(new C0073a(this, (C0073a) null));
        this.ebe.doLayout();
        this.ebf = ilVar.mo4920ci("search-field");
        ilVar.mo4917cf("search-label").setText(String.valueOf(vWVar.translate("Search")) + ": ");
        ilVar.mo4917cf("last-record-label").setText(String.valueOf(vWVar.translate("Last Record")) + ": ");
    }

    /* renamed from: Fa */
    public void mo525Fa() {
        this.ebb.addActionListener(new C0076d());
        this.ebc.addActionListener(new C0077e());
        this.ebd.addActionListener(new C0074b());
        this.ebf.addKeyListener(new C0075c());
    }

    private void brJ() {
        int i;
        OutpostStorage by = m641Ns().mo21390by(this.f880kj.getPlayer());
        this.ebe.getModel().clear();
        for (StorageTransaction next : by.aWn()) {
            Object selectedItem = this.ebb.getSelectedItem();
            if (selectedItem != null) {
                int cVr = ((int) (this.f880kj.ala().cVr() - next.getTimestamp())) / 86400000;
                String obj = selectedItem.toString();
                try {
                    i = Integer.parseInt(obj.substring(0, obj.indexOf(" ")));
                } catch (Exception e) {
                    i = 0;
                }
                if (cVr > i) {
                }
            }
            String text = this.djv.mo4920ci("search-field").getText();
            if (text != null && text.length() > 0) {
                String trim = text.toLowerCase().trim();
                if (!next.bQj().toLowerCase().contains(trim) && !next.mo22612az().mo19891ke().get().toLowerCase().contains(trim)) {
                }
            }
            if ((this.ebc.isSelected() || next.bQm() != StorageTransaction.C3839a.ADDITION) && (this.ebd.isSelected() || next.bQm() != StorageTransaction.C3839a.REMOVAL)) {
                this.ebe.getModel().addElement(next);
            }
        }
        this.ebe.repaint(50);
    }

    public void refresh() {
        brJ();
    }

    /* access modifiers changed from: package-private */
    public void removeListeners() {
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String m643a(StorageTransaction vdVar) {
        String str;
        String translate;
        long cVr = this.f880kj.ala().cVr();
        int timestamp = ((int) (cVr - vdVar.getTimestamp())) / 86400000;
        if (timestamp <= 0) {
            str = String.valueOf(((int) (cVr - vdVar.getTimestamp())) / 3600000) + " " + this.f880kj.translate("hours");
        } else {
            str = String.valueOf(timestamp) + " " + this.f880kj.translate("days");
        }
        if (vdVar.bQm() == StorageTransaction.C3839a.ADDITION) {
            translate = this.f880kj.translate("Addition");
        } else {
            translate = this.f880kj.translate("Removal");
        }
        return String.valueOf(str) + " " + translate + " <" + vdVar.bQj() + "> <" + vdVar.mo22612az().mo19891ke().get() + "> " + (vdVar.mo22612az() instanceof BulkItemType ? "x <" + vdVar.getAmount() + ">" : "");
    }

    /* renamed from: Ns */
    private Outpost m641Ns() {
        Actor bhE = this.f880kj.getPlayer().bhE();
        if (bhE instanceof Outpost) {
            return (Outpost) bhE;
        }
        throw new IllegalStateException("Player should not access an Outpost if he's not docked in it");
    }

    /* renamed from: a.At$d */
    /* compiled from: a */
    class C0076d implements ActionListener {
        C0076d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C0072At.this.refresh();
        }
    }

    /* renamed from: a.At$e */
    /* compiled from: a */
    class C0077e implements ActionListener {
        C0077e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C0072At.this.refresh();
        }
    }

    /* renamed from: a.At$b */
    /* compiled from: a */
    class C0074b implements ActionListener {
        C0074b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C0072At.this.refresh();
        }
    }

    /* renamed from: a.At$c */
    /* compiled from: a */
    class C0075c extends KeyAdapter {
        C0075c() {
        }

        public void keyReleased(KeyEvent keyEvent) {
            C0072At.this.refresh();
        }
    }

    /* renamed from: a.At$a */
    private class C0073a extends JLabel implements ListCellRenderer {


        private C0073a() {
        }

        /* synthetic */ C0073a(C0072At at, C0073a aVar) {
            this();
        }

        public Component getListCellRendererComponent(JList jList, Object obj, int i, boolean z, boolean z2) {
            setText(C0072At.this.m643a((StorageTransaction) obj));
            return this;
        }
    }
}
