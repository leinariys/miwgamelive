package p001a;

/* renamed from: a.aGC */
/* compiled from: a */
public abstract class aGC extends C6622aqW {


    public aGC(String str, String str2) {
        super(str, str2);
    }

    public abstract void setVisible(boolean z);

    /* renamed from: a */
    public void mo315a(Object... objArr) {
        if (objArr.length > 0 && (objArr[0] instanceof Boolean)) {
            setVisible(objArr[0].booleanValue());
        }
    }

    /* renamed from: m */
    public void mo8924m(boolean z) {
    }
}
