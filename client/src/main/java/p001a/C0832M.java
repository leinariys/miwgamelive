package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.network.message.externalizable.C6240ajE;
import game.network.message.serializable.C6400amI;

import javax.vecmath.Tuple3d;
import java.io.Externalizable;

/* renamed from: a.M */
/* compiled from: a */
public class C0832M implements C3735uM {

    /* renamed from: dP */
    private static final float f1066dP = 0.017453292f;
    /* renamed from: dQ */
    final Vec3f f1067dQ = new Vec3f(1000.0f, 1000.0f, 1000.0f);
    /* renamed from: dS */
    final Vec3f f1069dS = new Vec3f(1000.0f, 1000.0f, 1000.0f);
    /* renamed from: dY */
    final Vec3f f1075dY = new Vec3f(0.0f, 0.0f, -1.0f);
    /* renamed from: dR */
    float f1068dR = 1000.0f;
    /* renamed from: dT */
    float f1070dT = 1000.0f;
    /* renamed from: dU */
    Vec3f f1071dU = new Vec3f(0.0f, 0.0f, 0.0f);
    /* renamed from: dV */
    Vec3f f1072dV = new Vec3f(0.0f, 0.0f, 0.0f);
    /* renamed from: dW */
    float f1073dW;
    /* renamed from: dX */
    float f1074dX;
    private transient float PITCH_GAIN = 1.0f;
    private transient float SPEED_GAIN = 1.0f;
    private transient float YAW_GAIN = 1.0f;
    /* renamed from: dZ */
    private C6400amI f1076dZ;

    /* renamed from: a */
    public static boolean m6879a(Vec3f vec3f) {
        return C3241pX.isZero(vec3f.x) && C3241pX.isZero(vec3f.y) && C3241pX.isZero(vec3f.z);
    }

    public static boolean isZero(float f) {
        return C3241pX.isZero(f);
    }

    /* renamed from: a */
    public void mo3861a(Externalizable externalizable) {
        C6240ajE aje = (C6240ajE) externalizable;
        this.f1068dR = aje.f4685dR;
        this.f1070dT = aje.f4687dT;
        this.f1067dQ.set(aje.f4684dQ);
        this.f1069dS.set(aje.f4686dS);
        this.f1071dU.set(aje.f4688dU);
        this.f1071dU.set(aje.f4688dU);
        this.f1073dW = aje.f4690dW;
        this.f1074dX = aje.f4691dX;
    }

    /* renamed from: j */
    public Externalizable mo3862j(int i) {
        return new C6240ajE(this);
    }

    private final float clamp(float f, float f2, float f3) {
        if (f3 > f) {
            return f;
        }
        if (f3 < f2) {
            return f2;
        }
        return f3;
    }

    /* renamed from: a */
    private float m6877a(float f, float f2, float f3, float f4, float f5) {
        float clamp = clamp(f5, -f5, f3);
        if (f2 > f5 || f2 < (-f5)) {
            float f6 = f4 * f;
            float clamp2 = clamp(f6, -f6, clamp - f2) / f;
            float f7 = 10.0f * f6;
            float clamp3 = clamp(f7, -f7, (Math.signum(f2) * f5) - f2) / f;
            return Math.abs(clamp3) > Math.abs(clamp2) ? clamp3 : clamp2;
        }
        float f8 = f4 * f;
        return clamp(f8, -f8, clamp - f2) / f;
    }

    private final float max(float f, float f2) {
        return f < f2 ? f2 : f;
    }

    /* renamed from: a */
    private void m6878a(float f, Vec3f vec3f, float f2, float f3, float f4, float f5) {
        float e = C3241pX.m37169e(-vec3f.z, vec3f.x);
        float e2 = C3241pX.m37169e(-vec3f.z, vec3f.y);
        float f6 = e / (f * 2.5f);
        float f7 = e2 / (f * 2.5f);
        if (Math.abs(f7) > f5 || Math.abs(f6) > f5) {
            if (Math.abs(f7) > Math.abs(f6)) {
                float f8 = f6 / f7;
                f7 = Math.min(Math.abs(f7), f5) * Math.signum(f7);
                f6 = f8 * f7;
            } else {
                float f9 = f7 / f6;
                f6 = Math.min(Math.abs(f6), f5) * Math.signum(f6);
                f7 = f9 * f6;
            }
        }
        if (Math.abs(e2) > 90.0f || Math.abs(e) > 90.0f) {
            if (f3 > 0.75f) {
                f2 = 0.0f;
            } else if (f3 < 0.25f) {
                f2 = Math.max(f2, (f4 / 10.0f) + 1.0f);
            }
            f7 *= f;
            f6 *= f;
        }
        m6881c(new Vec3f(0.0f, 0.0f, -f2));
        m6880b(new Vec3f(f7, -f6, 0.0f));
    }

    /* renamed from: b */
    private void m6880b(Vec3f vec3f) {
        this.f1072dV.set(vec3f);
    }

    /* renamed from: c */
    private void m6881c(Vec3f vec3f) {
        this.f1071dU = vec3f;
        if (vec3f.length() > 0.0f) {
            this.f1075dY.set(vec3f);
            this.f1075dY.cox();
            return;
        }
        this.f1075dY.set(0.0f, 0.0f, 0.0f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Vec3f mo3858a(Vec3f vec3f, float f) {
        Vec3f vec3f2 = new Vec3f();
        if (isZero(f)) {
            vec3f2.set(0.0f, 0.0f, 0.0f);
        } else {
            if (m6879a(vec3f)) {
                vec3f2.set(0.0f, 0.0f, -1.0f);
            } else {
                vec3f2.set(vec3f);
                vec3f2.normalize();
            }
            vec3f2.scale(f);
        }
        return vec3f2;
    }

    /* renamed from: a */
    public Vec3f mo3857a(Quat4fWrap aoy, Vec3d ajr, Vec3d ajr2) {
        return aoy.mo15210aU(ajr2.mo9517f((Tuple3d) ajr).dfV());
    }

    /* renamed from: a */
    public Vec3d mo3856a(Quat4fWrap aoy, Vec3d ajr, Vec3f vec3f) {
        return ajr.mo9531q(aoy.mo15209aT(vec3f));
    }

    /* renamed from: a */
    public void mo3859a(float f, C1003Om om) {
        Vec3f vec3f;
        float f2;
        float f3;
        Vec3f bln = om.bln();
        Vec3f blm = om.blm();
        new Vec3f();
        if (this.f1076dZ != null) {
            C6400amI ami = new C6400amI();
            float length = mo3857a(om.bll(), om.blk(), this.f1076dZ.blk()).length() / this.f1073dW;
            if (((double) length) > 0.3d) {
                this.f1076dZ.mo14818b(length, ami);
                vec3f = mo3857a(om.bll(), om.blk(), ami.blk());
            } else {
                vec3f = mo3857a(om.bll(), om.blk(), this.f1076dZ.blk());
            }
        } else {
            vec3f = new Vec3f(0.0f, 0.0f, -10000.0f);
        }
        Vec3f a = mo3858a(vec3f, this.f1073dW);
        m6878a(f, a, a.length(), 0.0f, this.f1073dW, this.f1074dX);
        Vec3f vec3f2 = this.f1072dV;
        Vec3f blp = om.blp();
        Vec3f blo = om.blo();
        float f4 = vec3f2.x;
        float f5 = vec3f2.y;
        float f6 = vec3f2.z;
        blp.x = m6877a(f, bln.x, f4 * 0.017453292f, this.f1068dR * 0.017453292f, this.f1067dQ.x * 0.017453292f);
        float f7 = f;
        blp.y = m6877a(f7, bln.y, f5 * 0.017453292f, this.f1068dR * 0.017453292f, this.f1067dQ.y * 0.017453292f);
        float f8 = f;
        blp.z = m6877a(f8, bln.z, f6 * 0.017453292f, this.f1068dR * 0.017453292f, this.f1067dQ.z * 0.017453292f);
        Vec3f vec3f3 = this.f1071dU;
        float f9 = vec3f3.x;
        float f10 = vec3f3.y;
        float f11 = vec3f3.z;
        Vec3f vec3f4 = this.f1069dS;
        float f12 = (bln.x * bln.x) + (bln.y * bln.y);
        if (((double) f12) > 1.0E-6d) {
            float clamp = clamp(1.0f, 0.5f, 1.5f - (((float) Math.sqrt((double) f12)) / (((this.f1067dQ.x + this.f1067dQ.y) * 0.5f) * 0.017453292f)));
            Vec3f vec3f5 = this.f1075dY;
            if (f11 < 0.0f) {
                f9 = max(f9, vec3f4.x * clamp * vec3f5.x);
                float max = max(f10, vec3f4.y * clamp * vec3f5.y);
                f2 = max(f11, clamp * vec3f4.z * vec3f5.z);
                f3 = max;
                float f13 = this.f1070dT;
                blo.x = m6877a(f, blm.x, f9, f13 * 10.0f, vec3f4.x);
                blo.y = m6877a(f, blm.y, f3, f13 * 10.0f, vec3f4.y);
                blo.z = m6877a(f, blm.z, f2, f13, vec3f4.z);
                om.mo4490IQ().orientation.transform(blo, om.blr());
                om.mo4490IQ().orientation.transform(blp, om.bls());
            }
        }
        f2 = f11;
        f3 = f10;
        float f132 = this.f1070dT;
        blo.x = m6877a(f, blm.x, f9, f132 * 10.0f, vec3f4.x);
        blo.y = m6877a(f, blm.y, f3, f132 * 10.0f, vec3f4.y);
        blo.z = m6877a(f, blm.z, f2, f132, vec3f4.z);
        om.mo4490IQ().orientation.transform(blo, om.blr());
        om.mo4490IQ().orientation.transform(blp, om.bls());
    }

    /* renamed from: o */
    public void mo3863o(float f) {
        this.f1068dR = 1000.0f * f;
    }

    /* renamed from: p */
    public void mo3864p(float f) {
        float f2 = 1000.0f * f;
        this.f1067dQ.set(f2, f2, f2);
        this.f1074dX = f2;
    }

    /* renamed from: q */
    public void mo3865q(float f) {
        this.f1070dT = f;
    }

    /* renamed from: r */
    public void mo3866r(float f) {
        this.f1069dS.set(f, f, f);
        this.f1073dW = f;
    }

    /* renamed from: a */
    public void mo3860a(C6400amI ami) {
        this.f1076dZ = ami;
    }
}
