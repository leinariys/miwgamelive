package p001a;

import org.mozilla1.classfile.C0147Bi;
import org.objectweb.asm.Type;

/* renamed from: a.Pe */
/* compiled from: a */
public class C1070Pe {
    private Type cUV;
    private String dPB;
    private String descriptor;

    public C1070Pe(String str) {
        this.cUV = Type.getObjectType(str);
        this.dPB = str;
    }

    public C1070Pe(Class<?> cls) {
        this.cUV = Type.getType(cls);
        this.dPB = this.cUV.getInternalName();
    }

    public C1070Pe(Type type) {
        String str;
        this.cUV = type;
        this.descriptor = type.getDescriptor();
        if (this.descriptor.length() > 1) {
            str = type.getInternalName();
        } else {
            str = this.descriptor;
        }
        this.dPB = str;
    }

    /* renamed from: J */
    public static C1070Pe m8585J(Class<?> cls) {
        return new C1070Pe(cls.getName().replace('.', C0147Bi.cla));
    }

    /* renamed from: gi */
    public C1070Pe mo4806gi(String str) {
        return new C1070Pe(str);
    }

    public Type getType() {
        return this.cUV;
    }

    public String getDescriptor() {
        if (this.descriptor != null) {
            return this.descriptor;
        }
        String descriptor2 = this.cUV.getDescriptor();
        this.descriptor = descriptor2;
        return descriptor2;
    }

    public String getInternalName() {
        return this.dPB;
    }
}
