package p001a;

import game.geometry.Vector2fWrap;

/* renamed from: a.abQ  reason: case insensitive filesystem */
/* compiled from: a */
class C5836abQ extends aEH.C1791a<Vector2fWrap> {
    final /* synthetic */ aEH eZT;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C5836abQ(aEH aeh) {
        super((aEH.C1791a) null);
        this.eZT = aeh;
    }

    /* renamed from: in */
    public Vector2fWrap mo8563O(String str) {
        String[] split = str.split("[ \t\r\n]+");
        return new Vector2fWrap(Float.parseFloat(split[0]), Float.parseFloat(split[1]));
    }
}
