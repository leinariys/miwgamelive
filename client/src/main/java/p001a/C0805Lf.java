package p001a;

import game.script.avatar.Avatar;
import game.script.avatar.AvatarSector;
import game.script.avatar.body.AvatarAppearance;
import game.script.avatar.body.BodyPiece;
import gnu.trove.THashMap;
import logic.render.IEngineGraphics;
import logic.res.sound.C0907NJ;
import logic.thred.LogPrinter;
import taikodom.render.RenderView;
import taikodom.render.helpers.MeshUtils;
import taikodom.render.helpers.RenderTask;
import taikodom.render.scene.custom.RCustomMesh;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/* renamed from: a.Lf */
/* compiled from: a */
public class C0805Lf implements C3845vi {
    private static final LogPrinter logger = LogPrinter.setClass(Avatar.class);
    private static /* synthetic */ int[] dtm;
    private final C5207aAl dte;
    private final THashMap<AvatarSector, Boolean> dth;
    /* renamed from: lV */
    private final IEngineGraphics f1054lV;
    /* access modifiers changed from: private */
    public Boolean dtj;
    /* access modifiers changed from: private */
    public List<C3845vi.C3846a> dtl;
    private RCustomMesh dtf;
    private RCustomMesh dtg;
    private RCustomMesh dti;
    private C0907NJ dtk;

    public C0805Lf(AvatarAppearance aoz) {
        this(aoz, aoz.ala().ald().ale());
    }

    public C0805Lf(C5207aAl aal, IEngineGraphics abd) {
        this.dtj = false;
        this.dtl = new ArrayList();
        this.dte = aal;
        this.f1054lV = abd;
        this.dtf = new RCustomMesh();
        this.dtf.setName("Head");
        this.dtg = new RCustomMesh();
        this.dtg.setName("body");
        this.dth = new THashMap<>();
        for (AvatarSector put : this.dte.aFI()) {
            this.dth.put(put, false);
        }
    }

    static /* synthetic */ int[] beV() {
        int[] iArr = dtm;
        if (iArr == null) {
            iArr = new int[AvatarSector.C0955a.values().length];
            try {
                iArr[AvatarSector.C0955a.BODY.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[AvatarSector.C0955a.HAIR.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AvatarSector.C0955a.HEAD.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[AvatarSector.C0955a.OTHER.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            dtm = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: private */
    /* renamed from: cW */
    public void m6836cW(boolean z) {
        this.f1054lV.mo3007a((RenderTask) new C0808c(z));
    }

    /* renamed from: a */
    private void m6834a(C3845vi.C3846a aVar, boolean z) {
        if (aVar != null) {
            this.f1054lV.mo3007a((RenderTask) new C0807b(aVar, z));
        }
    }

    /* renamed from: a */
    public void mo3099a(C3845vi.C3846a aVar) {
        synchronized (this.dtj) {
            if (this.dtj.booleanValue()) {
                m6834a(aVar, true);
                return;
            }
            this.dtl.add(aVar);
            if (this.dtk == null) {
                this.f1054lV.mo3007a((RenderTask) new C0806a());
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean akQ() {
        /*
            r10 = this;
            r2 = 1
            r3 = 0
            java.lang.Boolean r4 = r10.dtj
            monitor-enter(r4)
            java.lang.Boolean r0 = r10.dtj     // Catch:{ all -> 0x01ad }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x01ad }
            if (r0 == 0) goto L_0x0010
            monitor-exit(r4)     // Catch:{ all -> 0x01ad }
            r0 = r2
        L_0x000f:
            return r0
        L_0x0010:
            a.aAl r0 = r10.dte     // Catch:{ all -> 0x01ad }
            a.tC r5 = r0.bea()     // Catch:{ all -> 0x01ad }
            a.aBD r0 = r10.f1054lV     // Catch:{ Exception -> 0x0125 }
            taikodom.render.loader.FileSceneLoader r6 = r0.aej()     // Catch:{ Exception -> 0x0125 }
            a.aBD r0 = r10.f1054lV     // Catch:{ Exception -> 0x0125 }
            java.lang.String r1 = r5.getFile()     // Catch:{ Exception -> 0x0125 }
            r0.mo3049bV(r1)     // Catch:{ Exception -> 0x0125 }
            gnu.trove.THashMap r7 = new gnu.trove.THashMap     // Catch:{ Exception -> 0x0125 }
            r7.<init>()     // Catch:{ Exception -> 0x0125 }
            a.aAl r0 = r10.dte     // Catch:{ Exception -> 0x0125 }
            java.util.Collection r0 = r0.bdU()     // Catch:{ Exception -> 0x0125 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ Exception -> 0x0125 }
        L_0x0034:
            boolean r0 = r1.hasNext()     // Catch:{ Exception -> 0x0125 }
            if (r0 != 0) goto L_0x0112
            java.lang.String r0 = r5.getFile()     // Catch:{ Exception -> 0x0125 }
            taikodom.render.loader.StockFile r0 = r6.getStockFile(r0)     // Catch:{ Exception -> 0x0125 }
            java.util.List r0 = r0.getEntries()     // Catch:{ Exception -> 0x0125 }
            java.util.Iterator r8 = r0.iterator()     // Catch:{ Exception -> 0x0125 }
        L_0x004a:
            boolean r0 = r8.hasNext()     // Catch:{ Exception -> 0x0125 }
            if (r0 != 0) goto L_0x0156
            a.ain r0 = r6.getBaseDir()     // Catch:{ Exception -> 0x0125 }
            java.lang.String r0 = r0.getPath()     // Catch:{ Exception -> 0x0125 }
            taikodom.render.scene.custom.RCustomMesh r1 = r10.dtf     // Catch:{ Exception -> 0x0125 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x0125 }
            java.lang.String r7 = r5.getFile()     // Catch:{ Exception -> 0x0125 }
            r6.<init>(r0, r7)     // Catch:{ Exception -> 0x0125 }
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x0125 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0125 }
            java.lang.String r9 = r5.getFile()     // Catch:{ Exception -> 0x0125 }
            java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ Exception -> 0x0125 }
            r8.<init>(r9)     // Catch:{ Exception -> 0x0125 }
            a.Nw$a r9 = p001a.C0954Nw.C0955a.HEAD     // Catch:{ Exception -> 0x0125 }
            java.lang.String r9 = r9.getSuffix()     // Catch:{ Exception -> 0x0125 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0125 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0125 }
            r7.<init>(r0, r8)     // Catch:{ Exception -> 0x0125 }
            r1.generateWeldingInfo(r6, r7)     // Catch:{ Exception -> 0x0125 }
            taikodom.render.scene.custom.RCustomMesh r1 = r10.dtg     // Catch:{ Exception -> 0x0125 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x0125 }
            java.lang.String r7 = r5.getFile()     // Catch:{ Exception -> 0x0125 }
            r6.<init>(r0, r7)     // Catch:{ Exception -> 0x0125 }
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x0125 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0125 }
            java.lang.String r9 = r5.getFile()     // Catch:{ Exception -> 0x0125 }
            java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ Exception -> 0x0125 }
            r8.<init>(r9)     // Catch:{ Exception -> 0x0125 }
            a.Nw$a r9 = p001a.C0954Nw.C0955a.BODY     // Catch:{ Exception -> 0x0125 }
            java.lang.String r9 = r9.getSuffix()     // Catch:{ Exception -> 0x0125 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0125 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0125 }
            r7.<init>(r0, r8)     // Catch:{ Exception -> 0x0125 }
            r1.generateWeldingInfo(r6, r7)     // Catch:{ Exception -> 0x0125 }
            taikodom.render.scene.custom.RCustomMesh r1 = r10.dti     // Catch:{ Exception -> 0x0125 }
            if (r1 == 0) goto L_0x00ed
            taikodom.render.scene.custom.RCustomMesh r1 = r10.dti     // Catch:{ Exception -> 0x0125 }
            java.lang.String r6 = "Hair"
            r1.setName(r6)     // Catch:{ Exception -> 0x0125 }
            taikodom.render.scene.custom.RCustomMesh r1 = r10.dti     // Catch:{ Exception -> 0x0125 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x0125 }
            java.lang.String r7 = r5.getFile()     // Catch:{ Exception -> 0x0125 }
            r6.<init>(r0, r7)     // Catch:{ Exception -> 0x0125 }
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x0125 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0125 }
            java.lang.String r9 = r5.getFile()     // Catch:{ Exception -> 0x0125 }
            java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ Exception -> 0x0125 }
            r8.<init>(r9)     // Catch:{ Exception -> 0x0125 }
            a.Nw$a r9 = p001a.C0954Nw.C0955a.HAIR     // Catch:{ Exception -> 0x0125 }
            java.lang.String r9 = r9.getSuffix()     // Catch:{ Exception -> 0x0125 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0125 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0125 }
            r7.<init>(r0, r8)     // Catch:{ Exception -> 0x0125 }
            r1.generateWeldingInfo(r6, r7)     // Catch:{ Exception -> 0x0125 }
        L_0x00ed:
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ Exception -> 0x0125 }
            r10.dtj = r0     // Catch:{ Exception -> 0x0125 }
            a.UR r0 = logger     // Catch:{ Exception -> 0x0125 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0125 }
            java.lang.String r6 = "Appearance "
            r1.<init>(r6)     // Catch:{ Exception -> 0x0125 }
            java.lang.StringBuilder r1 = r1.append(r10)     // Catch:{ Exception -> 0x0125 }
            java.lang.String r6 = " loaded resourcer."
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Exception -> 0x0125 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0125 }
            r0.trace(r1)     // Catch:{ Exception -> 0x0125 }
            monitor-exit(r4)     // Catch:{ all -> 0x01ad }
            r0 = r2
            goto L_0x000f
        L_0x0112:
            java.lang.Object r0 = r1.next()     // Catch:{ Exception -> 0x0125 }
            a.Qb r0 = (p001a.C1131Qb) r0     // Catch:{ Exception -> 0x0125 }
            java.lang.String r8 = r0.bpo()     // Catch:{ Exception -> 0x0125 }
            a.Nw r0 = r0.bpk()     // Catch:{ Exception -> 0x0125 }
            r7.put(r8, r0)     // Catch:{ Exception -> 0x0125 }
            goto L_0x0034
        L_0x0125:
            r0 = move-exception
            a.UR r0 = logger     // Catch:{ all -> 0x01ad }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x01ad }
            java.lang.String r2 = "Cannot find granny file for this appearance! [file: "
            r1.<init>(r2)     // Catch:{ all -> 0x01ad }
            java.lang.String r2 = r5.getFile()     // Catch:{ all -> 0x01ad }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x01ad }
            java.lang.String r2 = " , handle: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x01ad }
            java.lang.String r2 = r5.getHandle()     // Catch:{ all -> 0x01ad }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x01ad }
            java.lang.String r2 = "]"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x01ad }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x01ad }
            r0.error(r1)     // Catch:{ all -> 0x01ad }
            monitor-exit(r4)     // Catch:{ all -> 0x01ad }
            r0 = r3
            goto L_0x000f
        L_0x0156:
            java.lang.Object r0 = r8.next()     // Catch:{ Exception -> 0x0125 }
            taikodom.render.loader.StockEntry r0 = (taikodom.render.loader.StockEntry) r0     // Catch:{ Exception -> 0x0125 }
            taikodom.render.loader.RenderAsset r1 = r0.getAsset()     // Catch:{ Exception -> 0x0125 }
            boolean r1 = r1 instanceof taikodom.render.primitives.Mesh     // Catch:{ Exception -> 0x0125 }
            if (r1 == 0) goto L_0x004a
            a.aBD r1 = r10.f1054lV     // Catch:{ Exception -> 0x0125 }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x0125 }
            taikodom.render.loader.RenderAsset r0 = r1.mo3047bT(r0)     // Catch:{ Exception -> 0x0125 }
            taikodom.render.primitives.Mesh r0 = (taikodom.render.primitives.Mesh) r0     // Catch:{ Exception -> 0x0125 }
            a.QZ r1 = r0.getGrannyMesh()     // Catch:{ Exception -> 0x0125 }
            a.ayt r1 = r1.bqM()     // Catch:{ Exception -> 0x0125 }
            if (r1 == 0) goto L_0x004a
            int r9 = r0.getGrannyMaterialIndex()     // Catch:{ Exception -> 0x0125 }
            a.ayt r1 = r1.mo17090wa(r9)     // Catch:{ Exception -> 0x0125 }
            a.CS r1 = r1.cDE()     // Catch:{ Exception -> 0x0125 }
            java.lang.String r1 = r1.getName()     // Catch:{ Exception -> 0x0125 }
            java.lang.Object r1 = r7.get(r1)     // Catch:{ Exception -> 0x0125 }
            a.Nw r1 = (p001a.C0954Nw) r1     // Catch:{ Exception -> 0x0125 }
            if (r1 == 0) goto L_0x004a
            int[] r9 = beV()     // Catch:{ Exception -> 0x0125 }
            a.Nw$a r1 = r1.biX()     // Catch:{ Exception -> 0x0125 }
            int r1 = r1.ordinal()     // Catch:{ Exception -> 0x0125 }
            r1 = r9[r1]     // Catch:{ Exception -> 0x0125 }
            switch(r1) {
                case 1: goto L_0x01a5;
                case 2: goto L_0x01b0;
                case 3: goto L_0x01b8;
                default: goto L_0x01a3;
            }     // Catch:{ Exception -> 0x0125 }
        L_0x01a3:
            goto L_0x004a
        L_0x01a5:
            taikodom.render.scene.custom.RCustomMesh r1 = r10.dtf     // Catch:{ Exception -> 0x0125 }
            r9 = 0
            r1.addPart(r0, r9)     // Catch:{ Exception -> 0x0125 }
            goto L_0x004a
        L_0x01ad:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x01ad }
            throw r0
        L_0x01b0:
            taikodom.render.scene.custom.RCustomMesh r1 = r10.dtg     // Catch:{ Exception -> 0x0125 }
            r9 = 0
            r1.addPart(r0, r9)     // Catch:{ Exception -> 0x0125 }
            goto L_0x004a
        L_0x01b8:
            taikodom.render.scene.custom.RCustomMesh r1 = r10.dti     // Catch:{ Exception -> 0x0125 }
            if (r1 != 0) goto L_0x01ca
            taikodom.render.scene.custom.RCustomMesh r1 = new taikodom.render.scene.custom.RCustomMesh     // Catch:{ Exception -> 0x0125 }
            r1.<init>()     // Catch:{ Exception -> 0x0125 }
            r10.dti = r1     // Catch:{ Exception -> 0x0125 }
            taikodom.render.scene.custom.RCustomMesh r1 = r10.dti     // Catch:{ Exception -> 0x0125 }
            java.lang.String r9 = "Hair"
            r1.setName(r9)     // Catch:{ Exception -> 0x0125 }
        L_0x01ca:
            taikodom.render.scene.custom.RCustomMesh r1 = r10.dti     // Catch:{ Exception -> 0x0125 }
            r9 = 0
            r1.addPart(r0, r9)     // Catch:{ Exception -> 0x0125 }
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C0805Lf.akQ():boolean");
    }

    public void akP() {
        if (this.dtj.booleanValue()) {
            for (BodyPiece next : this.dte.bdU()) {
                if (next.bps()) {
                    AvatarSector.C0955a biX = next.bpk().biX();
                    String bpo = next.bpo();
                    switch (beV()[biX.ordinal()]) {
                        case 1:
                            MeshUtils.setPartVisible(this.dtf, bpo, false);
                            break;
                        case 2:
                            MeshUtils.setPartVisible(this.dtg, bpo, false);
                            break;
                        case 3:
                            if (this.dti == null) {
                                break;
                            } else {
                                MeshUtils.setPartVisible(this.dti, bpo, false);
                                break;
                            }
                    }
                }
            }
            for (AvatarSector next2 : this.dte.aFI()) {
                BodyPiece n = this.dte.mo7728n(next2);
                if (n != null && n.bps()) {
                    AvatarSector.C0955a biX2 = next2.biX();
                    String bpo2 = n.bpo();
                    boolean booleanValue = ((Boolean) this.dth.get(next2)).booleanValue();
                    switch (beV()[biX2.ordinal()]) {
                        case 1:
                            MeshUtils.setPartVisible(this.dtf, bpo2, booleanValue);
                            break;
                        case 2:
                            MeshUtils.setPartVisible(this.dtg, bpo2, booleanValue);
                            break;
                        case 3:
                            if (this.dti == null) {
                                break;
                            } else {
                                MeshUtils.setPartVisible(this.dti, bpo2, booleanValue);
                                break;
                            }
                    }
                }
            }
            MeshUtils.rebuild(this.dtf);
            MeshUtils.rebuild(this.dtg);
            if (this.dti != null) {
                MeshUtils.rebuild(this.dti);
            }
        }
    }

    public C5207aAl aOm() {
        return this.dte;
    }

    /* renamed from: a */
    public RCustomMesh mo3097a(AvatarSector.C0955a aVar) {
        switch (beV()[aVar.ordinal()]) {
            case 1:
                return this.dtf;
            case 2:
                return this.dtg;
            case 3:
                return this.dti;
            default:
                return null;
        }
    }

    /* renamed from: a */
    public void mo3098a(AvatarSector nw, boolean z) {
        this.dth.put(nw, Boolean.valueOf(z));
    }

    public void akN() {
        for (AvatarSector put : this.dte.aFI()) {
            this.dth.put(put, true);
        }
    }

    public Collection<BodyPiece> akO() {
        return Collections.unmodifiableCollection(this.dte.bdU());
    }

    /* renamed from: b */
    public boolean mo3105b(AvatarSector.C0955a aVar) {
        switch (beV()[aVar.ordinal()]) {
            case 1:
            case 2:
            case 3:
                return true;
            default:
                return false;
        }
    }

    public void akR() {
        synchronized (this.dtj) {
            if (this.dtf != null) {
                this.dtf.dispose();
                this.dtf.releaseReferences();
                this.dtf = new RCustomMesh();
                this.dtf.setName("head");
            }
            if (this.dtg != null) {
                this.dtg.dispose();
                this.dtg.releaseReferences();
                this.dtg = new RCustomMesh();
                this.dtg.setName("body");
            }
            if (this.dti != null) {
                this.dti.dispose();
                this.dti.releaseReferences();
                this.dti = null;
            }
            for (AvatarSector put : this.dte.aFI()) {
                this.dth.put(put, false);
            }
            this.dtj = false;
            logger.trace("Appearance " + this + " unloaded resources.");
        }
    }

    /* renamed from: a.Lf$c */
    /* compiled from: a */
    class C0808c implements RenderTask {
        private final /* synthetic */ boolean dmR;

        C0808c(boolean z) {
            this.dmR = z;
        }

        public void run(RenderView renderView) {
            synchronized (C0805Lf.this.dtl) {
                for (C3845vi.C3846a a : C0805Lf.this.dtl) {
                    a.mo4551a(C0805Lf.this, this.dmR);
                }
                C0805Lf.this.dtl.clear();
            }
        }
    }

    /* renamed from: a.Lf$b */
    /* compiled from: a */
    class C0807b implements RenderTask {
        private final /* synthetic */ C3845vi.C3846a dmQ;
        private final /* synthetic */ boolean dmR;

        C0807b(C3845vi.C3846a aVar, boolean z) {
            this.dmQ = aVar;
            this.dmR = z;
        }

        public void run(RenderView renderView) {
            this.dmQ.mo4551a(C0805Lf.this, this.dmR);
        }
    }

    /* renamed from: a.Lf$a */
    class C0806a implements RenderTask {
        C0806a() {
        }

        public void run(RenderView renderView) {
            if (C0805Lf.this.dtj.booleanValue()) {
                C0805Lf.this.m6836cW(true);
            } else {
                C0805Lf.this.m6836cW(C0805Lf.this.akQ());
            }
        }
    }
}
