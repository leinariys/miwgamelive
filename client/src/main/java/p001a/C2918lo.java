package p001a;

import game.network.message.externalizable.C5667aSd;
import logic.C0613Ih;
import logic.C6245ajJ;
import logic.aVH;
import logic.thred.LogPrinter;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: a.lo */
/* compiled from: a */
public class C2918lo {

    /* access modifiers changed from: private */
    public static LogPrinter logger = LogPrinter.m10275K(C2918lo.class);
    /* renamed from: kh */
    private static Map<Object, List<aVH<?>>> f8586kh = new HashMap();

    /* renamed from: a */
    public static void m35303a(C6245ajJ ajj, Object obj) {
        ArrayList arrayList = new ArrayList();
        if (obj instanceof C3274pr) {
            C2920b bVar = new C2920b(obj);
            arrayList.add(bVar);
            ajj.mo13965a(aIU.class, bVar);
        }
        if (obj instanceof C5575aOp) {
            C2919a aVar = new C2919a(obj);
            arrayList.add(aVar);
            ajj.mo13965a(C6009aeh.class, aVar);
        }
        for (Method method : obj.getClass().getDeclaredMethods()) {
            C2602hR hRVar = (C2602hR) method.getAnnotation(C2602hR.class);
            if (hRVar != null) {
                String zf = hRVar.mo19255zf();
                C2921c cVar = new C2921c(zf, obj, method, (C2921c) null);
                arrayList.add(cVar);
                ajj.mo13966a(zf, C5667aSd.class, cVar);
            }
        }
        f8586kh.put(obj, arrayList);
    }

    /* renamed from: a */
    public static void m35302a(C0613Ih ih, Object obj) {
        List<aVH> remove = f8586kh.remove(obj);
        if (remove != null) {
            for (aVH a : remove) {
                ih.mo7497a(a);
            }
        }
    }

    /* renamed from: a.lo$c */
    /* compiled from: a */
    private static final class C2921c extends C6124ags<C5667aSd> {
        private final Object gRd;
        private final Method gRe;
        private final Object gRf;
        private Class<?>[] parameterTypes;

        private C2921c(Object obj, Object obj2, Method method) {
            this.gRf = obj;
            this.gRd = obj2;
            this.gRe = method;
            this.parameterTypes = method.getParameterTypes();
            method.setAccessible(true);
        }

        /* synthetic */ C2921c(Object obj, Object obj2, Method method, C2921c cVar) {
            this(obj, obj2, method);
        }

        /* renamed from: b */
        public boolean updateInfo(C5667aSd asd) {
            try {
                if (this.parameterTypes == null) {
                    C2918lo.logger.warn("Could not invoke: " + this.gRe + " on " + asd);
                } else if (this.parameterTypes.length == 1) {
                    if (this.parameterTypes != null && this.parameterTypes.length == 1 && this.parameterTypes[0] == C5667aSd.class) {
                        this.gRe.invoke(this.gRd, new Object[]{asd});
                    } else if (this.parameterTypes[0] == Boolean.TYPE) {
                        this.gRe.invoke(this.gRd, new Object[]{Boolean.valueOf(asd.isPressed())});
                    }
                }
            } catch (Exception e) {
                C2918lo.logger.error("Error handling: " + this.gRe, e);
            }
            return true;
        }

        public String toString() {
            return "[" + this.gRf + "] " + this.gRe + " " + this.gRd;
        }
    }

    /* renamed from: a.lo$b */
    /* compiled from: a */
    class C2920b extends C6124ags<aIU> {
        private final /* synthetic */ Object aGD;

        C2920b(Object obj) {
            this.aGD = obj;
        }

        /* renamed from: b */
        public boolean updateInfo(aIU aiu) {
            ((C3274pr) this.aGD).mo21230c(aiu);
            return true;
        }
    }

    /* renamed from: a.lo$a */
    class C2919a extends C6124ags<C6009aeh> {
        private final /* synthetic */ Object aGD;

        C2919a(Object obj) {
            this.aGD = obj;
        }

        /* renamed from: a */
        public boolean updateInfo(C6009aeh aeh) {
            ((C5575aOp) this.aGD).mo10677b(aeh);
            return true;
        }
    }
}
