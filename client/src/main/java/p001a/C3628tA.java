package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;

/* renamed from: a.tA */
/* compiled from: a */
public class C3628tA extends C1083Pr {

    private final C3978xf bmY = new C3978xf();
    private final C3978xf bmZ = new C3978xf();
    private final Vec3f bnf = new Vec3f();
    private final Vec3f bng = new Vec3f();
    /* renamed from: Tx */
    private float f9205Tx;
    private C6460anQ[] bmX = {new C6460anQ(), new C6460anQ(), new C6460anQ()};
    private float bna;
    private float bnb;
    private float bnc;
    private float bnd;
    private float bne;
    private float bnh;
    private float bni;
    private float bnj;
    private float bnk;
    private float bnl;
    private float bnm;
    private float bnn;
    private boolean bno = false;
    private boolean bnp;
    private boolean bnq;

    public C3628tA() {
        super(aVP.CONETWIST_CONSTRAINT_TYPE);
    }

    public C3628tA(C6238ajC ajc, C6238ajC ajc2, C3978xf xfVar, C3978xf xfVar2) {
        super(aVP.CONETWIST_CONSTRAINT_TYPE, ajc, ajc2);
        this.bmY.mo22947a(xfVar);
        this.bmZ.mo22947a(xfVar2);
        this.bmZ.bFF.m10 *= -1.0f;
        this.bmZ.bFF.m11 *= -1.0f;
        this.bmZ.bFF.m12 *= -1.0f;
        this.bnc = 1.0E30f;
        this.bnd = 1.0E30f;
        this.bne = 1.0E30f;
        this.bna = 0.3f;
        this.bnb = 1.0f;
        this.bnp = false;
        this.bnq = false;
    }

    public C3628tA(C6238ajC ajc, C3978xf xfVar) {
        super(aVP.CONETWIST_CONSTRAINT_TYPE, ajc);
        this.bmY.mo22947a(xfVar);
        this.bmZ.mo22947a(this.bmY);
        this.bmZ.bFF.m10 *= -1.0f;
        this.bmZ.bFF.m11 *= -1.0f;
        this.bmZ.bFF.m12 *= -1.0f;
        this.bmZ.bFF.m20 *= -1.0f;
        this.bmZ.bFF.m21 *= -1.0f;
        this.bmZ.bFF.m22 *= -1.0f;
        this.bnc = 1.0E30f;
        this.bnd = 1.0E30f;
        this.bne = 1.0E30f;
        this.bna = 0.3f;
        this.bnb = 1.0f;
        this.bnp = false;
        this.bnq = false;
    }

    /* renamed from: PU */
    public void mo4840PU() {
        this.stack.bcF();
        this.stack.bcN().push();
        try {
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            this.dMa = 0.0f;
            this.bnk = 0.0f;
            this.bnj = 0.0f;
            this.bnp = false;
            this.bnq = false;
            this.bnn = 0.0f;
            this.bnm = 0.0f;
            if (!this.bno) {
                Vec3f ac = this.stack.bcH().mo4458ac(this.bmY.bFG);
                this.dQm.ceu().mo22946G(ac);
                Vec3f ac2 = this.stack.bcH().mo4458ac(this.bmZ.bFG);
                this.dQn.ceu().mo22946G(ac2);
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                vec3f4.sub(ac2, ac);
                Vec3f[] vec3fArr = {(Vec3f) this.stack.bcH().get(), (Vec3f) this.stack.bcH().get(), (Vec3f) this.stack.bcH().get()};
                if (vec3f4.lengthSquared() > 1.1920929E-7f) {
                    vec3fArr[0].normalize(vec3f4);
                } else {
                    vec3fArr[0].set(1.0f, 0.0f, 0.0f);
                }
                C0503Gz.m3562e(vec3fArr[0], vec3fArr[1], vec3fArr[2]);
                for (int i = 0; i < 3; i++) {
                    Matrix3fWrap g = this.stack.bcK().mo15565g(this.dQm.ceu().bFF);
                    g.transpose();
                    Matrix3fWrap g2 = this.stack.bcK().mo15565g(this.dQn.ceu().bFF);
                    g2.transpose();
                    vec3f2.sub(ac, this.dQm.ces());
                    vec3f3.sub(ac2, this.dQn.ces());
                    this.bmX[i].mo14967a(g, g2, vec3f2, vec3f3, vec3fArr[i], this.dQm.ceq(), this.dQm.aRf(), this.dQn.ceq(), this.dQn.aRf());
                }
            }
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f7 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f8 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f9 = (Vec3f) this.stack.bcH().get();
            this.bmY.bFF.getColumn(0, vec3f5);
            bnn().ceu().bFF.transform(vec3f5);
            this.bmZ.bFF.getColumn(0, vec3f8);
            bno().ceu().bFF.transform(vec3f8);
            float f = 0.0f;
            float f2 = 0.0f;
            if (this.bnc >= 0.05f) {
                this.bmY.bFF.getColumn(1, vec3f6);
                bnn().ceu().bFF.transform(vec3f6);
                f = C0920NU.m7657m(vec3f8.dot(vec3f6), vec3f8.dot(vec3f5));
            }
            if (this.bnd >= 0.05f) {
                this.bmY.bFF.getColumn(2, vec3f7);
                bnn().ceu().bFF.transform(vec3f7);
                f2 = C0920NU.m7657m(vec3f8.dot(vec3f7), vec3f8.dot(vec3f5));
            }
            float abs = (Math.abs(f2) * (1.0f / (this.bnd * this.bnd))) + (Math.abs(f) * (1.0f / (this.bnc * this.bnc)));
            if (abs > 1.0f) {
                this.bnk = abs - 1.0f;
                this.bnq = true;
                vec3f2.scale(vec3f8.dot(vec3f6), vec3f6);
                vec3f3.scale(vec3f8.dot(vec3f7), vec3f7);
                vec3f.add(vec3f2, vec3f3);
                this.bnf.cross(vec3f8, vec3f);
                this.bnf.normalize();
                this.bnf.scale(vec3f8.dot(vec3f5) >= 0.0f ? 1.0f : -1.0f);
                this.bnh = 1.0f / (bnn().mo13902aC(this.bnf) + bno().mo13902aC(this.bnf));
            }
            if (this.bne >= 0.0f) {
                this.bmZ.bFF.getColumn(1, vec3f9);
                bno().ceu().bFF.transform(vec3f9);
                Vec3f ac3 = this.stack.bcH().mo4458ac(C1777aC.m13122b(this.stack.bcN().mo11474b(C1777aC.m13119a(vec3f8, vec3f5)), vec3f9));
                float m = C0920NU.m7657m(ac3.dot(vec3f7), ac3.dot(vec3f6));
                float f3 = this.bne > 0.05f ? this.f9205Tx : 0.0f;
                if (m <= (-this.bne) * f3) {
                    this.bnl = -(this.bne + m);
                    this.bnp = true;
                    this.bng.add(vec3f8, vec3f5);
                    this.bng.scale(0.5f);
                    this.bng.normalize();
                    this.bng.scale(-1.0f);
                    this.bni = 1.0f / (bnn().mo13902aC(this.bng) + bno().mo13902aC(this.bng));
                } else if (m > f3 * this.bne) {
                    this.bnl = m - this.bne;
                    this.bnp = true;
                    this.bng.add(vec3f8, vec3f5);
                    this.bng.scale(0.5f);
                    this.bng.normalize();
                    this.bni = 1.0f / (bnn().mo13902aC(this.bng) + bno().mo13902aC(this.bng));
                }
            }
        } finally {
            this.stack.bcG();
            this.stack.bcN().pop();
        }
    }

    /* renamed from: cv */
    public void mo4847cv(float f) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            Vec3f ac = this.stack.bcH().mo4458ac(this.bmY.bFG);
            this.dQm.ceu().mo22946G(ac);
            Vec3f ac2 = this.stack.bcH().mo4458ac(this.bmZ.bFG);
            this.dQn.ceu().mo22946G(ac2);
            if (!this.bno) {
                Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                vec3f3.sub(ac, this.dQm.ces());
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                vec3f4.sub(ac2, this.dQn.ces());
                Vec3f ac3 = this.stack.bcH().mo4458ac(this.dQm.mo13897R(vec3f3));
                Vec3f ac4 = this.stack.bcH().mo4458ac(this.dQn.mo13897R(vec3f4));
                Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                vec3f5.sub(ac3, ac4);
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= 3) {
                        break;
                    }
                    Vec3f vec3f6 = this.bmX[i2].gfg;
                    float clX = 1.0f / this.bmX[i2].clX();
                    float dot = vec3f6.dot(vec3f5);
                    vec3f.sub(ac, ac2);
                    float f2 = ((((-vec3f.dot(vec3f6)) * 0.3f) / f) * clX) - (clX * dot);
                    this.dMa += f2;
                    Vec3f vec3f7 = (Vec3f) this.stack.bcH().get();
                    vec3f7.scale(f2, vec3f6);
                    vec3f.sub(ac, this.dQm.ces());
                    this.dQm.mo13937m(vec3f7, vec3f);
                    vec3f.negate(vec3f7);
                    vec3f2.sub(ac2, this.dQn.ces());
                    this.dQn.mo13937m(vec3f, vec3f2);
                    i = i2 + 1;
                }
            }
            Vec3f angularVelocity = bnn().getAngularVelocity();
            Vec3f angularVelocity2 = bno().getAngularVelocity();
            if (this.bnq) {
                vec3f.sub(angularVelocity2, angularVelocity);
                float dot2 = ((vec3f.dot(this.bnf) * this.bnb * this.bnb) + (this.bnk * (1.0f / f) * this.bna)) * this.bnh;
                float f3 = this.bnm;
                this.bnm = Math.max(dot2 + this.bnm, 0.0f);
                float f4 = this.bnm - f3;
                Vec3f vec3f8 = (Vec3f) this.stack.bcH().get();
                vec3f8.scale(f4, this.bnf);
                this.dQm.mo13896Q(vec3f8);
                vec3f.negate(vec3f8);
                this.dQn.mo13896Q(vec3f);
            }
            if (this.bnp) {
                vec3f.sub(angularVelocity2, angularVelocity);
                float dot3 = ((vec3f.dot(this.bng) * this.bnb * this.bnb) + (this.bnl * (1.0f / f) * this.bna)) * this.bni;
                float f5 = this.bnn;
                this.bnn = Math.max(dot3 + this.bnn, 0.0f);
                float f6 = this.bnn - f5;
                Vec3f vec3f9 = (Vec3f) this.stack.bcH().get();
                vec3f9.scale(f6, this.bng);
                this.dQm.mo13896Q(vec3f9);
                vec3f.negate(vec3f9);
                this.dQn.mo13896Q(vec3f);
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: cw */
    public void mo22186cw(float f) {
    }

    /* renamed from: br */
    public void mo22185br(boolean z) {
        this.bno = z;
    }

    /* renamed from: d */
    public void mo22187d(float f, float f2, float f3) {
        mo22188e(f, f2, f3, 0.8f, 0.3f, 1.0f);
    }

    /* renamed from: e */
    public void mo22188e(float f, float f2, float f3, float f4, float f5, float f6) {
        this.bnc = f;
        this.bnd = f2;
        this.bne = f3;
        this.f9205Tx = f4;
        this.bna = f5;
        this.bnb = f6;
    }

    public C3978xf adO() {
        return this.bmY;
    }

    public C3978xf adP() {
        return this.bmZ;
    }

    public boolean adQ() {
        return this.bnp;
    }

    public boolean adR() {
        return this.bnp;
    }

    public float adS() {
        return this.bnj;
    }
}
