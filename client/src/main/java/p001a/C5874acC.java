package p001a;

import game.script.ship.categories.Fighter;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.acC  reason: case insensitive filesystem */
/* compiled from: a */
public class C5874acC extends C6729asZ {

    /* renamed from: TL */
    boolean f4207TL;

    public C5874acC() {
    }

    public C5874acC(int i, Fighter pFVar, C1003Om om, long j) {
        super(i, pFVar, om, j);
        this.f4207TL = pFVar.biN().mo19336xZ();
    }

    /* renamed from: a */
    public void mo12575a(C1355Tk tk) {
        C0520HN cLd;
        super.mo12575a(tk);
        if (mo23123ha() != null && ((C1298TD) mo23123ha().bFf()) != null && (cLd = mo23123ha().cLd()) != null && (cLd instanceof C2623hh)) {
            if (!mo23123ha().bGX() || mo23123ha() != mo23123ha().ala().aLW().ald().mo4089dL().bhE()) {
                ((C2623hh) cLd).mo19335R(this.f4207TL);
            }
        }
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        this.f4207TL = objectInput.readBoolean();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        objectOutput.writeBoolean(this.f4207TL);
    }

    public String toString() {
        return "FighterPacketData[ {" + super.toString() + "} boosting: " + this.f4207TL + " ]";
    }
}
