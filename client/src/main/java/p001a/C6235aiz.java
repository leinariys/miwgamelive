package p001a;

import logic.res.scene.C3087nb;

import java.util.*;

/* renamed from: a.aiz  reason: case insensitive filesystem */
/* compiled from: a */
public class C6235aiz {
    private final Map<String, C6152ahU> fNH = new HashMap();
    private final Map<String, Map<String, C6152ahU>> fNI = new HashMap();
    private final List<C6152ahU> resources = new ArrayList();

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo13873a(String str, String str2, Class<? extends C3087nb> cls) {
        C6152ahU ahu = new C6152ahU(str, cls, str2);
        this.resources.add(ahu);
        if (!this.fNI.containsKey(str2)) {
            this.fNI.put(str2, new HashMap());
        }
        this.fNI.get(str2).put(str, ahu);
        this.fNH.put(str, ahu);
    }

    /* renamed from: iR */
    public C6152ahU mo13876iR(String str) {
        return this.fNH.get(str);
    }

    /* renamed from: iS */
    public boolean mo13877iS(String str) {
        return this.fNH.containsKey(str);
    }

    /* renamed from: aj */
    public List<C6152ahU> mo13874aj(Class<? extends C3087nb> cls) {
        ArrayList arrayList = new ArrayList();
        for (C6152ahU next : this.resources) {
            if (cls.isAssignableFrom(next.caU())) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public List<C6152ahU> cct() {
        return Collections.unmodifiableList(this.resources);
    }

    /* renamed from: iT */
    public boolean mo13878iT(String str) {
        return this.fNI.containsKey(str);
    }
}
