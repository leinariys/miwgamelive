package p001a;

import game.network.message.WriterBits;
import logic.res.KeyCode;

import java.io.DataOutput;
import java.io.UTFDataFormatException;

/* renamed from: a.oF */
/* compiled from: a */
public class C3128oF implements DataOutput {
    public WriterBits aPS;
    private byte[] aPT = null;

    public C3128oF(WriterBits aav) {
        this.aPS = aav;
    }

    /* renamed from: a */
    static int m36602a(String str, C3128oF oFVar) {
        byte[] bArr;
        int i;
        int i2;
        int i3;
        int length = str.length();
        int i4 = 0;
        int i5 = 0;
        while (i4 < length) {
            char charAt = str.charAt(i4);
            if (charAt >= 1 && charAt <= 127) {
                i3 = i5 + 1;
            } else if (charAt > 2047) {
                i3 = i5 + 3;
            } else {
                i3 = i5 + 2;
            }
            i4++;
            i5 = i3;
        }
        if (i5 > 65535) {
            throw new UTFDataFormatException("encoded string too long: " + i5 + " bytes");
        }
        if (oFVar instanceof C3128oF) {
            if (oFVar.aPT == null || oFVar.aPT.length < i5 + 2) {
                oFVar.aPT = new byte[((i5 * 2) + 2)];
            }
            bArr = oFVar.aPT;
        } else {
            bArr = new byte[(i5 + 2)];
        }
        bArr[0] = (byte) ((i5 >>> 8) & 255);
        int i6 = 2;
        bArr[1] = (byte) (i5 & 255);
        int i7 = 0;
        while (true) {
            if (i7 >= length) {
                i = i7;
                break;
            }
            char charAt2 = str.charAt(i7);
            if (charAt2 < 1) {
                i = i7;
                break;
            } else if (charAt2 > 127) {
                i = i7;
                break;
            } else {
                bArr[i6] = (byte) charAt2;
                i7++;
                i6++;
            }
        }
        while (i < length) {
            char charAt3 = str.charAt(i);
            if (charAt3 >= 1 && charAt3 <= 127) {
                i2 = i6 + 1;
                bArr[i6] = (byte) charAt3;
            } else if (charAt3 > 2047) {
                int i8 = i6 + 1;
                bArr[i6] = (byte) (((charAt3 >> 12) & 15) | KeyCode.ctb);
                int i9 = i8 + 1;
                bArr[i8] = (byte) (((charAt3 >> 6) & 63) | 128);
                i2 = i9 + 1;
                bArr[i9] = (byte) ((charAt3 & '?') | 128);
            } else {
                int i10 = i6 + 1;
                bArr[i6] = (byte) (((charAt3 >> 6) & 31) | 192);
                i2 = i10 + 1;
                bArr[i10] = (byte) ((charAt3 & '?') | 128);
            }
            i++;
            i6 = i2;
        }
        oFVar.write(bArr, 0, i5 + 2);
        return i5 + 2;
    }

    public void write(int i) {
        this.aPS.writeBits(8, i);
    }

    public void write(byte[] bArr) {
        int i = 0;
        int length = bArr.length;
        while (true) {
            length--;
            if (length >= 0) {
                this.aPS.writeBits(8, bArr[i]);
                i++;
            } else {
                return;
            }
        }
    }

    public void write(byte[] bArr, int i, int i2) {
        while (true) {
            i2--;
            if (i2 >= 0) {
                this.aPS.writeBits(8, bArr[i]);
                i++;
            } else {
                return;
            }
        }
    }

    public void writeBoolean(boolean z) {
        this.aPS.writeBits(1, z ? 1 : 0);
    }

    public void writeByte(int i) {
        this.aPS.writeBits(8, i);
    }

    public void writeBytes(String str) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            this.aPS.writeBits(8, (byte) str.charAt(i));
        }
    }

    public void writeChar(int i) {
        this.aPS.writeBits(8, i >> 8);
        this.aPS.writeBits(8, i);
    }

    public void writeChars(String str) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            this.aPS.writeBits(8, (charAt >>> 8) & 255);
            this.aPS.writeBits(8, charAt & 255);
        }
    }

    public void writeDouble(double d) {
        writeLong(Double.doubleToRawLongBits(d));
    }

    public void writeFloat(float f) {
        writeInt(Float.floatToRawIntBits(f));
    }

    public void writeInt(int i) {
        if (i == 0) {
            this.aPS.writeBits(2, 0);
        } else if (i == 1) {
            this.aPS.writeBits(2, 1);
        } else if (i < 0) {
            if ((i & -8) == -8) {
                this.aPS.writeBits(3, 4);
                this.aPS.writeBits(4, i);
            } else if ((i & -128) == -128) {
                this.aPS.writeBits(3, 5);
                this.aPS.writeBits(8, i);
            } else if ((i & -2048) == -2048) {
                this.aPS.writeBits(4, 12);
                this.aPS.writeBits(12, i);
            } else if ((i & -32768) == -32768) {
                this.aPS.writeBits(4, 13);
                this.aPS.writeBits(16, i);
            } else if ((i & -8388608) == -8388608) {
                this.aPS.writeBits(4, 14);
                this.aPS.writeBits(24, i);
            } else {
                this.aPS.writeBits(4, 15);
                this.aPS.writeBits(32, i);
            }
        } else if ((i & -8) == 0) {
            this.aPS.writeBits(3, 4);
            this.aPS.writeBits(4, i);
        } else if ((i & -128) == 0) {
            this.aPS.writeBits(3, 5);
            this.aPS.writeBits(8, i);
        } else if ((i & -2048) == 0) {
            this.aPS.writeBits(4, 12);
            this.aPS.writeBits(12, i);
        } else if ((i & -32768) == 0) {
            this.aPS.writeBits(4, 13);
            this.aPS.writeBits(16, i);
        } else if ((i & -8388608) == 0) {
            this.aPS.writeBits(4, 14);
            this.aPS.writeBits(24, i);
        } else {
            this.aPS.writeBits(4, 15);
            this.aPS.writeBits(32, i);
        }
    }

    public void writeLong(long j) {
        writeInt((int) (j >> 32));
        writeInt((int) j);
    }

    public void writeShort(int i) {
        writeInt(i);
    }

    public void writeUTF(String str) {
        m36602a(str, this);
    }
}
