package p001a;

/* renamed from: a.afP  reason: case insensitive filesystem */
/* compiled from: a */
public class C6043afP {
    private long swigCPtr;

    public C6043afP(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6043afP() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m21545a(C6043afP afp) {
        if (afp == null) {
            return 0;
        }
        return afp.swigCPtr;
    }
}
