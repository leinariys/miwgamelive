package p001a;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.file.FileRegion;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.filterchain.IoFilterAdapter;
import org.apache.mina.core.future.WriteFuture;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.WriteRequest;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

import java.net.SocketAddress;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: a.vT */
/* compiled from: a */
public class C3824vT extends IoFilterAdapter {
    private final ProtocolCodecFactory bBw;
    LinkedBlockingQueue<C3826b> bBx = new LinkedBlockingQueue<>();
    LinkedBlockingQueue<C3825a> bBy = new LinkedBlockingQueue<>();

    public C3824vT(ProtocolCodecFactory protocolCodecFactory) {
        this.bBw = protocolCodecFactory;
    }

    public void filterWrite(IoFilter.NextFilter nextFilter, IoSession ioSession, WriteRequest writeRequest) {
        Object message = writeRequest.getMessage();
        if ((message instanceof IoBuffer) || (message instanceof FileRegion)) {
            nextFilter.filterWrite(ioSession, writeRequest);
            return;
        }
        C3826b poll = this.bBx.poll();
        if (poll == null) {
            poll = new C3826b(this, ioSession, nextFilter, writeRequest, (C3826b) null);
        } else {
            poll.mo22591a(ioSession, nextFilter, writeRequest);
        }
        this.bBw.getEncoder(ioSession).encode(ioSession, message, poll);
        this.bBx.offer(poll);
    }

    public void messageReceived(IoFilter.NextFilter nextFilter, IoSession ioSession, Object obj) {
        if (!(obj instanceof IoBuffer)) {
            nextFilter.messageReceived(ioSession, obj);
            return;
        }
        IoBuffer ioBuffer = (IoBuffer) obj;
        ProtocolDecoder decoder = this.bBw.getDecoder(ioSession);
        C3825a poll = this.bBy.poll();
        if (poll == null) {
            poll = new C3825a(this, ioSession, nextFilter, (C3825a) null);
        } else {
            poll.mo22588a(ioSession, nextFilter);
        }
        decoder.decode(ioSession, ioBuffer, poll);
        this.bBy.offer(poll);
    }

    /* renamed from: a.vT$a */
    private final class C3825a implements ProtocolDecoderOutput {
        private IoSession etm;
        private IoFilter.NextFilter etn;

        /* synthetic */ C3825a(C3824vT vTVar, IoSession ioSession, IoFilter.NextFilter nextFilter, C3825a aVar) {
            this(ioSession, nextFilter);
        }

        private C3825a(IoSession ioSession, IoFilter.NextFilter nextFilter) {
            mo22588a(ioSession, nextFilter);
        }

        public void flush() {
        }

        public void write(Object obj) {
            this.etn.messageReceived(this.etm, obj);
        }

        /* renamed from: a */
        public void mo22588a(IoSession ioSession, IoFilter.NextFilter nextFilter) {
            this.etm = ioSession;
            this.etn = nextFilter;
        }
    }

    /* renamed from: a.vT$b */
    /* compiled from: a */
    private final class C3826b implements ProtocolEncoderOutput {
        /* access modifiers changed from: private */
        public WriteRequest iOQ;
        private IoSession etm;
        private IoFilter.NextFilter etn;

        /* synthetic */ C3826b(C3824vT vTVar, IoSession ioSession, IoFilter.NextFilter nextFilter, WriteRequest writeRequest, C3826b bVar) {
            this(ioSession, nextFilter, writeRequest);
        }

        private C3826b(IoSession ioSession, IoFilter.NextFilter nextFilter, WriteRequest writeRequest) {
            mo22591a(ioSession, nextFilter, writeRequest);
        }

        public WriteFuture flush() {
            return this.iOQ.getFuture();
        }

        public void mergeAll() {
            throw new UnsupportedOperationException();
        }

        public void write(Object obj) {
            this.etn.filterWrite(this.etm, new C3827a(obj));
        }

        /* renamed from: a */
        public void mo22591a(IoSession ioSession, IoFilter.NextFilter nextFilter, WriteRequest writeRequest) {
            this.etm = ioSession;
            this.etn = nextFilter;
            this.iOQ = writeRequest;
        }

        /* renamed from: a.vT$b$a */
        class C3827a implements WriteRequest {
            private final /* synthetic */ Object icw;

            C3827a(Object obj) {
                this.icw = obj;
            }

            public SocketAddress getDestination() {
                return C3826b.this.iOQ.getDestination();
            }

            public WriteFuture getFuture() {
                return C3826b.this.iOQ.getFuture();
            }

            public Object getMessage() {
                return this.icw;
            }

            public WriteRequest getOriginalRequest() {
                return C3826b.this.iOQ;
            }
        }
    }
}
