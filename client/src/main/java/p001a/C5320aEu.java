package p001a;

import game.script.player.Player;
import logic.aaa.C5783aaP;
import taikodom.addon.neo.lowerbar.C2379ed;

/* renamed from: a.aEu  reason: case insensitive filesystem */
/* compiled from: a */
class C5320aEu extends C6124ags<C5783aaP> {
    final /* synthetic */ C2379ed hEP;

    C5320aEu(C2379ed edVar) {
        this.hEP = edVar;
    }

    /* renamed from: b */
    public boolean updateInfo(C5783aaP aap) {
        if (this.hEP.f7014kj.ala() != null) {
            if (aap.bMO() == C5783aaP.C1841a.FLYING) {
                Player aPC = this.hEP.f7014kj.ala().getPlayer();
                if (aPC.bQx() != null) {
                    this.hEP.m29960b(aPC.bQx());
                }
                this.hEP.f7014kj.alf().addTask("shield task", this.hEP.f7008Bq, 80);
            } else if (this.hEP.f7008Bq != null) {
                this.hEP.f7008Bq.cancel();
                this.hEP.f7008Bq = null;
            }
        }
        return false;
    }
}
