package p001a;

import game.network.message.serializable.C2550gi;

import java.io.PrintStream;
import java.io.PrintWriter;

/* renamed from: a.XU */
/* compiled from: a */
public class C1605XU extends UnsupportedOperationException implements C5404aIa {
    private static final long serialVersionUID = 2835670257825983478L;
    private Throwable cause;
    private C2550gi eJS;

    public C1605XU() {
        super("Code is not implemented");
        this.eJS = new C2550gi(this);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C1605XU(String str) {
        super(str == null ? "Code is not implemented" : str);
        this.eJS = new C2550gi(this);
    }

    public C1605XU(Throwable th) {
        super("Code is not implemented");
        this.eJS = new C2550gi(this);
        this.cause = th;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C1605XU(String str, Throwable th) {
        super(str == null ? "Code is not implemented" : str);
        this.eJS = new C2550gi(this);
        this.cause = th;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C1605XU(java.lang.Class r3) {
        /*
            r2 = this;
            if (r3 != 0) goto L_0x000f
            java.lang.String r0 = "Code is not implemented"
        L_0x0004:
            r2.<init>(r0)
            a.gi r0 = new a.gi
            r0.<init>(r2)
            r2.eJS = r0
            return
        L_0x000f:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "Code is not implemented in "
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C1605XU.<init>(java.lang.Class):void");
    }

    public Throwable getCause() {
        return this.cause;
    }

    public String getMessage() {
        if (super.getMessage() != null) {
            return super.getMessage();
        }
        if (this.cause != null) {
            return this.cause.toString();
        }
        return null;
    }

    /* renamed from: bg */
    public String mo6733bg(int i) {
        if (i == 0) {
            return super.getMessage();
        }
        return this.eJS.mo19061bg(i);
    }

    public String[] getMessages() {
        return this.eJS.getMessages();
    }

    /* renamed from: bh */
    public Throwable mo6734bh(int i) {
        return this.eJS.mo19062bh(i);
    }

    /* renamed from: vn */
    public int mo6742vn() {
        return this.eJS.mo19069vn();
    }

    /* renamed from: vo */
    public Throwable[] mo6743vo() {
        return this.eJS.mo19070vo();
    }

    /* renamed from: N */
    public int mo6731N(Class cls) {
        return this.eJS.mo19059a(cls, 0);
    }

    /* renamed from: a */
    public int mo6732a(Class cls, int i) {
        return this.eJS.mo19059a(cls, i);
    }

    public void printStackTrace() {
        this.eJS.printStackTrace();
    }

    public void printStackTrace(PrintStream printStream) {
        this.eJS.printStackTrace(printStream);
    }

    public void printStackTrace(PrintWriter printWriter) {
        this.eJS.printStackTrace(printWriter);
    }

    /* renamed from: c */
    public final void mo6735c(PrintWriter printWriter) {
        super.printStackTrace(printWriter);
    }
}
