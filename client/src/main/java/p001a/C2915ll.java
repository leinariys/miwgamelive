package p001a;

import game.script.item.ItemTypeCategory;
import logic.ui.item.ItemIcon;

import javax.swing.*;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;

/* renamed from: a.ll */
/* compiled from: a */
public class C2915ll extends JLabel implements TreeCellRenderer {
    public Component getTreeCellRendererComponent(JTree jTree, Object obj, boolean z, boolean z2, boolean z3, int i, boolean z4) {
        if (obj instanceof ItemTypeCategory) {
            ItemTypeCategory aai = (ItemTypeCategory) obj;
            setText(aai.mo12246ke().get());
            ItemIcon nIVar = new ItemIcon();
            nIVar.mo20664aw(aai.mo12100sK().getHandle());
            setIcon(nIVar);
        }
        return this;
    }
}
