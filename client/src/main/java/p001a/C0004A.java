package p001a;

import logic.swing.C0454GJ;
import logic.swing.C2740jL;

import javax.swing.*;

/* renamed from: a.A */
/* compiled from: a */
class C0004A implements C5912aco.C1865a {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ C2762jf.C2764b bkm;

    C0004A(C2762jf.C2764b bVar) {
        this.bkm = bVar;
    }

    /* renamed from: gZ */
    public void mo4gZ() {
        JScrollBar buy = C2762jf.this.ahE.buy();
        int value = buy.getValue();
        if (value > 0) {
            this.bkm.mo12692a(buy, "[" + value + "..0] dur " + "50" + " regular_in", new C0005a(), new C0006b());
            C2762jf.this.ahJ = value;
            return;
        }
        C2762jf.this.ahJ = 0;
        this.bkm.gqM.mo4gZ();
    }

    /* renamed from: a.A$a */
    class C0005a implements C2740jL<JScrollBar> {
        C0005a() {
        }

        /* renamed from: a */
        public void mo7a(JScrollBar jScrollBar, float f) {
            jScrollBar.setValue((int) f);
        }

        /* renamed from: a */
        public float mo5a(JScrollBar jScrollBar) {
            return (float) jScrollBar.getValue();
        }
    }

    /* renamed from: a.A$b */
    /* compiled from: a */
    class C0006b implements C0454GJ {
        C0006b() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            C0004A.this.bkm.gqM.mo4gZ();
        }
    }
}
