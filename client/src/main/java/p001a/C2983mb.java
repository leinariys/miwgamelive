package p001a;

import jdbm.helper.Tuple;
import jdbm.helper.TupleBrowser;

/* renamed from: a.mb */
/* compiled from: a */
public class C2983mb {
    private Tuple fsA = new Tuple();
    private TupleBrowser fsz;

    public C2983mb(TupleBrowser tupleBrowser) {
        this.fsz = tupleBrowser;
    }

    /* renamed from: a */
    public boolean mo20571a(C2984a aVar) {
        if (!this.fsz.getNext(this.fsA)) {
            return false;
        }
        aVar.mo20573cm(((Long) this.fsA.getKey()).longValue());
        aVar.setValue(this.fsA.getValue());
        return true;
    }

    /* renamed from: a.mb$a */
    public static class C2984a {
        private long aBm;
        private Object value;

        /* renamed from: MW */
        public long mo20572MW() {
            return this.aBm;
        }

        public Object getValue() {
            return this.value;
        }

        public void setValue(Object obj) {
            this.value = obj;
        }

        /* renamed from: cm */
        public void mo20573cm(long j) {
            this.aBm = j;
        }
    }
}
