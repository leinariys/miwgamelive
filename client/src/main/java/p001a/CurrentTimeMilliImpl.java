package p001a;

import logic.thred.LogPrinter;

/* renamed from: a.eC */
/* compiled from: a */
public class CurrentTimeMilliImpl implements CurrentTimeMilli {
    private static final LogPrinter log = LogPrinter.m10275K(CurrentTimeMilliImpl.class);

    /* renamed from: Dp */
    private static final long limitShiftTime = 5000;
    /* renamed from: Dq */
    public volatile long lastCurrentTimeMillis = getCurrentTimeMillis();
    /* renamed from: Dr */
    public volatile long lastNanoTime = getNanoTime();
    /* renamed from: Ds */
    public volatile long lastTimeMillis;
    /* renamed from: Dt */
    public volatile long adjustmentTime;

    public long currentTimeMillis() {
        long currentTimeMillis = getCurrentTimeMillis();
        if (Math.abs(currentTimeMillis - this.lastTimeMillis) > limitShiftTime) {
            long currentTimeMillis1 = getCurrentTimeMillis();
            long nanoTime = getNanoTime();
            synchronized (this) {
                long deltaNanoTime = (nanoTime - this.lastNanoTime) / 1000000;
                long deltaCurrentTimeMillis = deltaNanoTime - ((currentTimeMillis1 - this.lastCurrentTimeMillis) + this.adjustmentTime);
                if (Math.abs(deltaCurrentTimeMillis) > 20000) {
                    this.adjustmentTime += deltaCurrentTimeMillis;
                    if (isVerbose()) {
                        log.warn("Adjusting time offset: " + deltaCurrentTimeMillis + " ms");
                    }
                }
                this.lastNanoTime = (long) (((double) this.lastNanoTime) + (((double) deltaNanoTime) * 1000000.0d));
                this.lastCurrentTimeMillis += deltaNanoTime;
            }
            currentTimeMillis = getCurrentTimeMillis();
        }
        this.lastTimeMillis = currentTimeMillis;
        return currentTimeMillis + this.adjustmentTime;
    }

    /* access modifiers changed from: protected */
    public boolean isVerbose() {
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: mg */
    public long getNanoTime() {
        return System.nanoTime();
    }

    /* access modifiers changed from: protected */
    /* renamed from: mh */
    public long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }

    /* access modifiers changed from: protected */
    /* renamed from: mi */
    public long getZero() {
        return 0;
    }
}
