package p001a;

import game.network.channel.client.ClientConnect;
import game.network.manager.C1810aL;
import game.network.message.externalizable.C6302akO;
import game.network.message.serializable.ReceivedTime;

import java.lang.reflect.ParameterizedType;

/* renamed from: a.axB */
/* compiled from: a */
public abstract class axB<T extends ReceivedTime> implements C1810aL {
    private final Class<T> gQe = ((Class) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public abstract C6302akO mo2392b(ClientConnect bVar, T t);

    public Class<T> getMessageClass() {
        return this.gQe;
    }

    /* renamed from: a */
    public final <M extends ReceivedTime> C6302akO mo3456a(ClientConnect bVar, M m) {
        return mo2392b(bVar, m);
    }
}
