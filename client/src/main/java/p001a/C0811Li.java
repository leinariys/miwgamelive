package p001a;

import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/* renamed from: a.Li */
/* compiled from: a */
public class C0811Li {
    static Log logger = LogPrinter.setClass(C0811Li.class);
    private static PrintWriter dts;

    public static void log(String str) {
        m6852eG(str);
        bfa();
    }

    /* renamed from: eG */
    private static void m6852eG(String str) {
        bfb().println(str);
    }

    private static void bfa() {
        bfb().flush();
    }

    private static PrintWriter bfb() {
        if (dts == null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                String property = System.getProperty("taikodom.stats.root", "./stats");
                File file = new File(property);
                if (!file.exists()) {
                    logger.info("Path to stats dir not found: " + property + ". Trying to create it. The path can be set by setting taikodom.stats.root in properties file.");
                    file.mkdir();
                }
                dts = new PrintWriter(new FileOutputStream(new File(file, "logins-" + simpleDateFormat.format(new Date()) + ".log"), true));
            } catch (FileNotFoundException e) {
                logger.info(e);
            }
        }
        return dts;
    }
}
