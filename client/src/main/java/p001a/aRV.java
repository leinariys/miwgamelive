package p001a;

import game.script.ship.Station;

import javax.swing.tree.DefaultMutableTreeNode;

/* renamed from: a.aRV */
/* compiled from: a */
public class aRV extends DefaultMutableTreeNode {
    public aRV(Station bf) {
        super(bf);
    }

    public String toString() {
        if (getUserObject() instanceof Station) {
            return ((Station) getUserObject()).getName();
        }
        return aRV.super.toString();
    }
}
