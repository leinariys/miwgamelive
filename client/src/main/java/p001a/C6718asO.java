package p001a;

import logic.ui.item.InternalFrame;
import taikodom.addon.IAddonProperties;
import taikodom.addon.chat.ChatAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/* renamed from: a.asO  reason: case insensitive filesystem */
/* compiled from: a */
public class C6718asO {
    private static final long serialVersionUID = -1612133065527622995L;
    /* access modifiers changed from: private */
    public InternalFrame gxX;
    /* access modifiers changed from: private */
    public String gxY;
    /* access modifiers changed from: private */
    public ChatAddon gxZ;

    /* renamed from: nM */
    private InternalFrame f5280nM;

    public C6718asO(IAddonProperties vWVar) {
        this.gxZ = (ChatAddon) vWVar.bHu().getInstanceWrapFileXmlOrJar(ChatAddon.class);
        this.f5280nM = vWVar.bHv().mo16792bN("chatColor.xml");
        this.gxX = vWVar.bHv().mo16792bN("chooser.xml");
        ButtonGroup buttonGroup = new ButtonGroup();
        JToggleButton cd = this.gxX.mo4915cd("red");
        cd.addActionListener(new C1970a());
        buttonGroup.add(cd);
        JToggleButton cd2 = this.gxX.mo4915cd("green");
        cd2.addActionListener(new C1971b());
        buttonGroup.add(cd2);
        JToggleButton cd3 = this.gxX.mo4915cd("blue");
        cd3.addActionListener(new C1972c());
        buttonGroup.add(cd3);
        JToggleButton cd4 = this.gxX.mo4915cd("white");
        cd4.addActionListener(new C1973d());
        buttonGroup.add(cd4);
        JToggleButton cd5 = this.gxX.mo4915cd("yellow");
        cd5.addActionListener(new C1974e());
        buttonGroup.add(cd5);
        this.gxX.mo4913cb("close").addActionListener(new C1975f());
        this.f5280nM.mo4913cb("localB").addActionListener(new C1977h());
        this.f5280nM.mo4913cb("stationB").addActionListener(new C1976g());
        this.f5280nM.mo4913cb("squadB").addActionListener(new C1978i());
        this.f5280nM.mo4913cb("corpB").addActionListener(new C1984o());
        this.f5280nM.mo4913cb("pvtB").addActionListener(new C1985p());
        this.f5280nM.mo4915cd("localBox").addItemListener(new C1981l());
        this.f5280nM.mo4915cd("stationBox").addItemListener(new C1982m());
        this.f5280nM.mo4915cd("squadBox").addItemListener(new C1979j());
        this.f5280nM.mo4915cd("corpBox").addItemListener(new C1980k());
        this.f5280nM.mo4915cd("pvtBox").addItemListener(new C1983n());
        this.f5280nM.pack();
        this.f5280nM.center();
        this.f5280nM.setVisible(true);
    }

    /* access modifiers changed from: private */
    /* renamed from: uz */
    public void m25678uz(int i) {
        this.gxZ.mo23583t(this.gxY, i);
    }

    public void show() {
        this.f5280nM.setVisible(true);
    }

    public void hide() {
        this.f5280nM.setVisible(false);
    }

    /* renamed from: a.asO$a */
    class C1970a implements ActionListener {
        C1970a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C6718asO.this.m25678uz(16726581);
        }
    }

    /* renamed from: a.asO$b */
    /* compiled from: a */
    class C1971b implements ActionListener {
        C1971b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C6718asO.this.m25678uz(1365822);
        }
    }

    /* renamed from: a.asO$c */
    /* compiled from: a */
    class C1972c implements ActionListener {
        C1972c() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C6718asO.this.m25678uz(51455);
        }
    }

    /* renamed from: a.asO$d */
    /* compiled from: a */
    class C1973d implements ActionListener {
        C1973d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C6718asO.this.m25678uz(16777215);
        }
    }

    /* renamed from: a.asO$e */
    /* compiled from: a */
    class C1974e implements ActionListener {
        C1974e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C6718asO.this.m25678uz(16760320);
        }
    }

    /* renamed from: a.asO$f */
    /* compiled from: a */
    class C1975f implements ActionListener {
        C1975f() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C6718asO.this.gxX.setVisible(false);
        }
    }

    /* renamed from: a.asO$h */
    /* compiled from: a */
    class C1977h implements ActionListener {
        C1977h() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C6718asO.this.gxY = "local";
            C6718asO.this.gxX.center();
            C6718asO.this.gxX.setVisible(true);
        }
    }

    /* renamed from: a.asO$g */
    /* compiled from: a */
    class C1976g implements ActionListener {
        C1976g() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C6718asO.this.gxY = "station";
            C6718asO.this.gxX.center();
            C6718asO.this.gxX.setVisible(true);
        }
    }

    /* renamed from: a.asO$i */
    /* compiled from: a */
    class C1978i implements ActionListener {
        C1978i() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C6718asO.this.gxY = "party";
            C6718asO.this.gxX.center();
            C6718asO.this.gxX.setVisible(true);
        }
    }

    /* renamed from: a.asO$o */
    /* compiled from: a */
    class C1984o implements ActionListener {
        C1984o() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C6718asO.this.gxY = "corp";
            C6718asO.this.gxX.center();
            C6718asO.this.gxX.setVisible(true);
        }
    }

    /* renamed from: a.asO$p */
    /* compiled from: a */
    class C1985p implements ActionListener {
        C1985p() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C6718asO.this.gxY = "pvt";
            C6718asO.this.gxX.center();
            C6718asO.this.gxX.setVisible(true);
        }
    }

    /* renamed from: a.asO$l */
    /* compiled from: a */
    class C1981l implements ItemListener {
        C1981l() {
        }

        public void itemStateChanged(ItemEvent itemEvent) {
            if (itemEvent.getStateChange() == 1) {
                C6718asO.this.gxZ.gBe.mo12537ic("local");
            } else {
                C6718asO.this.gxZ.gBe.mo12536ib("local");
            }
            C6718asO.this.gxZ.cwu();
        }
    }

    /* renamed from: a.asO$m */
    /* compiled from: a */
    class C1982m implements ItemListener {
        C1982m() {
        }

        public void itemStateChanged(ItemEvent itemEvent) {
            if (itemEvent.getStateChange() == 1) {
                C6718asO.this.gxZ.gBe.mo12537ic("station");
            } else {
                C6718asO.this.gxZ.gBe.mo12536ib("station");
            }
            C6718asO.this.gxZ.cwu();
        }
    }

    /* renamed from: a.asO$j */
    /* compiled from: a */
    class C1979j implements ItemListener {
        C1979j() {
        }

        public void itemStateChanged(ItemEvent itemEvent) {
            if (itemEvent.getStateChange() == 1) {
                C6718asO.this.gxZ.gBe.mo12537ic("party");
            } else {
                C6718asO.this.gxZ.gBe.mo12536ib("party");
            }
            C6718asO.this.gxZ.cwu();
        }
    }

    /* renamed from: a.asO$k */
    /* compiled from: a */
    class C1980k implements ItemListener {
        C1980k() {
        }

        public void itemStateChanged(ItemEvent itemEvent) {
            if (itemEvent.getStateChange() == 1) {
                C6718asO.this.gxZ.gBe.mo12537ic("corp");
            } else {
                C6718asO.this.gxZ.gBe.mo12536ib("corp");
            }
            C6718asO.this.gxZ.cwu();
        }
    }

    /* renamed from: a.asO$n */
    /* compiled from: a */
    class C1983n implements ItemListener {
        C1983n() {
        }

        public void itemStateChanged(ItemEvent itemEvent) {
            if (itemEvent.getStateChange() == 1) {
                C6718asO.this.gxZ.gBe.mo12537ic("pvt");
            } else {
                C6718asO.this.gxZ.gBe.mo12536ib("pvt");
            }
            C6718asO.this.gxZ.cwu();
        }
    }
}
