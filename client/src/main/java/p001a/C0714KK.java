package p001a;

import game.network.message.externalizable.C2814kV;
import game.script.hazardarea.HazardArea.HazardArea;
import taikodom.addon.hud.HudSelectionInputAddon;

/* renamed from: a.KK */
/* compiled from: a */
class C0714KK extends C6124ags<C2814kV> {

    /* renamed from: Rq */
    final /* synthetic */ HudSelectionInputAddon f936Rq;

    public C0714KK(HudSelectionInputAddon hudSelectionInputAddon) {
        this.f936Rq = hudSelectionInputAddon;
    }

    /* renamed from: a */
    public boolean updateInfo(C2814kV kVVar) {
        if (!(this.f936Rq.f9921kj.getPlayer() == null || this.f936Rq.f9921kj.getPlayer().bQx() == null || (kVVar.mo20073rf() instanceof HazardArea))) {
            if (this.f936Rq.f9921kj.getPlayer().bQx().agB() == null) {
                this.f936Rq.m43752x(kVVar.mo20073rf());
            }
            this.f936Rq.biU = kVVar.mo20073rf();
        }
        return false;
    }
}
