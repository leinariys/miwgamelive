package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;
import game.geometry.Matrix4fWrap;
import game.geometry.Quat4fWrap;

import javax.vecmath.Quat4f;

/* renamed from: a.xf */
/* compiled from: a */
public class C3978xf {
    public final Matrix3fWrap bFF = new Matrix3fWrap();
    public final Vec3f bFG = new Vec3f();
    public C0763Kt stack;

    public C3978xf() {
    }

    public C3978xf(Matrix3fWrap ajd) {
        this.bFF.set(ajd);
    }

    public C3978xf(C3978xf xfVar) {
        mo22947a(xfVar);
    }

    /* renamed from: a */
    public void mo22947a(C3978xf xfVar) {
        this.bFF.set(xfVar.bFF);
        this.bFG.set(xfVar.bFG);
    }

    /* renamed from: G */
    public void mo22946G(Vec3f vec3f) {
        this.bFF.transform(vec3f);
        vec3f.add(this.bFG);
    }

    public void setIdentity() {
        this.bFF.setIdentity();
        this.bFG.set(0.0f, 0.0f, 0.0f);
    }

    public void inverse() {
        this.bFF.transpose();
        this.bFG.scale(-1.0f);
        this.bFF.transform(this.bFG);
    }

    /* renamed from: b */
    public void mo22952b(C3978xf xfVar) {
        mo22947a(xfVar);
        inverse();
    }

    /* renamed from: c */
    public void mo22954c(C3978xf xfVar) {
        if (this.stack == null) {
            this.stack = C0763Kt.bcE();
        }
        this.stack.bcH().push();
        try {
            Vec3f ac = this.stack.bcH().mo4458ac(xfVar.bFG);
            mo22946G(ac);
            this.bFF.mul(xfVar.bFF);
            this.bFG.set(ac);
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a */
    public void mo22948a(C3978xf xfVar, C3978xf xfVar2) {
        mo22947a(xfVar);
        mo22954c(xfVar2);
    }

    /* renamed from: f */
    public void mo22955f(Vec3f vec3f, Vec3f vec3f2) {
        if (this.stack == null) {
            this.stack = C0763Kt.bcE();
        }
        this.stack.bcK().push();
        try {
            vec3f2.sub(vec3f, this.bFG);
            Matrix3fWrap g = this.stack.bcK().mo15565g(this.bFF);
            g.transpose();
            g.transform(vec3f2);
        } finally {
            this.stack.bcK().pop();
        }
    }

    public Quat4f anz() {
        if (this.stack == null) {
            this.stack = C0763Kt.bcE();
        }
        this.stack.bcN().push();
        try {
            Quat4fWrap aoy = (Quat4fWrap) this.stack.bcN().get();
            C3427rS.m38375b(this.bFF, (Quat4f) aoy);
            return (Quat4f) this.stack.bcN().mo15197aq(aoy);
        } finally {
            this.stack.bcN().pop();
        }
    }

    public void setRotation(Quat4f quat4f) {
        C3427rS.m38370a(this.bFF, quat4f);
    }

    /* renamed from: a */
    public void mo22949a(float[] fArr) {
        C3427rS.m38371a(this.bFF, fArr);
        this.bFG.set(fArr[12], fArr[13], fArr[14]);
    }

    /* renamed from: b */
    public void mo22953b(float[] fArr) {
        C3427rS.m38376b(this.bFF, fArr);
        fArr[12] = this.bFG.x;
        fArr[13] = this.bFG.y;
        fArr[14] = this.bFG.z;
        fArr[15] = 1.0f;
    }

    public void set(Matrix4fWrap ajk) {
        Matrix3fWrap ajd = this.bFF;
        ajd.m00 = ajk.m00;
        ajd.m01 = ajk.m01;
        ajd.m02 = ajk.m02;
        ajd.m10 = ajk.m10;
        ajd.m11 = ajk.m11;
        ajd.m12 = ajk.m12;
        ajd.m20 = ajk.m20;
        ajd.m21 = ajk.m21;
        ajd.m22 = ajk.m22;
        Vec3f vec3f = this.bFG;
        vec3f.x = ajk.m03;
        vec3f.y = ajk.m13;
        vec3f.z = ajk.m23;
    }

    public boolean anA() {
        return this.bFG.anA() && this.bFF.anA();
    }
}
