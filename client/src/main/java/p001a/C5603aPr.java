package p001a;

import game.geometry.Vec3d;
import logic.thred.PoolThread;
import org.mozilla1.javascript.ScriptRuntime;

import javax.vecmath.Tuple3d;

/* renamed from: a.aPr  reason: case insensitive filesystem */
/* compiled from: a */
public class C5603aPr extends ayA {
    public static final int iBn = 3;
    private static final int iBm = 60000;
    public static int iBo = 0;
    static boolean[] iBp = new boolean[1];
    static boolean[] iBq = new boolean[1];
    private static double iBr = Math.random();
    private static double iBs = Math.random();
    private static double iBt = Math.random();

    /* renamed from: id */
    int f3559id = iBo;

    public C5603aPr(C1285Sv sv) {
        super(sv);
        iBo++;
        iBp = copyOf(iBp, iBo);
        iBq = copyOf(iBq, (iBo / 3) + 1);
    }

    private boolean[] copyOf(boolean[] zArr, int i) {
        boolean[] zArr2 = new boolean[i];
        for (int i2 = 0; i2 < Math.min(i, zArr.length); i2++) {
            zArr2[i2] = zArr[i2];
        }
        return zArr2;
    }

    /* access modifiers changed from: protected */
    public void aGu() {
        switch (this.state) {
            case 0:
                this.runs++;
                mo16918a("Docked", C1285Sv.C1286a.Docked);
                mo16931r("State:" + this.state + " Init", true);
                mo16928jh((long) (((int) (10000.0d * Math.random())) + 1000));
                this.state++;
                return;
            case 1:
                mo16931r("State:" + this.state + " Undock: " + mo5504eT().getName(), true);
                if (mo5495al().isDead()) {
                    btE();
                }
                btJ().mo16115bS(mo5503dL());
                btG().mo19541a(C2675iR.C2676a.FastMovementsStates);
                btG().mo19543hg(C1285Sv.btM());
                btH().restart();
                mo16917a(btG());
                mo16918a("FlyShort", C1285Sv.C1286a.ShortFlight);
                this.state++;
                return;
            case 2:
                mo16931r("State:" + this.state + " Firing", true);
                btH().finish();
                PoolThread.sleep(400);
                doj();
                iBp[this.f3559id] = true;
                this.state++;
                return;
            case 3:
                if (doh()) {
                    iBq[getGroupId()] = true;
                }
                if (iBq[getGroupId()]) {
                    this.state++;
                }
                mo16918a("WaitPVP", C1285Sv.C1286a.Waiting);
                return;
            case 4:
                cDL();
                mo16928jh(60000);
                mo16918a("Fire", C1285Sv.C1286a.PVPFight);
                this.state++;
                return;
            case 5:
                iBp[this.f3559id] = false;
                mo16931r("State:" + this.state + " Finished", true);
                mo16928jh(2000);
                mo16918a("WaitExpl1", C1285Sv.C1286a.Waiting);
                this.state++;
                return;
            case 6:
                mo16931r("State:" + this.state + " Wait Explode", true);
                if (mo5495al().isAlive() || !mo5503dL().bQB()) {
                    log("Trying to explode Bot " + mo5503dL().getName());
                    btJ().mo16107ae(mo5495al());
                }
                if (mo5495al().isAlive()) {
                    log("Tried to explode Bot " + mo5503dL().getName() + ", no success");
                    throw new IllegalStateException();
                }
                btJ().mo16116bU(mo5503dL());
                if (!mo5503dL().bQB()) {
                    log("Bot " + mo5503dL().getName() + " tried to dock, but something went wrong.");
                    throw new IllegalStateException();
                }
                mo16928jh(2000);
                mo16918a("WaitExpl2", C1285Sv.C1286a.Waiting);
                this.state++;
                return;
            case 7:
                if (doi()) {
                    iBq[getGroupId()] = false;
                }
                if (!iBq[getGroupId()]) {
                    this.state = 0;
                }
                mo16918a("Wait", C1285Sv.C1286a.Waiting);
                return;
            default:
                return;
        }
    }

    private boolean doh() {
        for (int dol = dol(); dol <= dok(); dol++) {
            if (!iBp[dol]) {
                return false;
            }
        }
        return true;
    }

    private boolean doi() {
        for (int dol = dol(); dol <= dok(); dol++) {
            if (iBp[dol]) {
                return false;
            }
        }
        return true;
    }

    private void doj() {
        Vec3d d = mo5504eT().getPosition().mo9504d((Tuple3d) new Vec3d((double) ((float) (iBr * 30000.0d)), (double) ((float) (iBs * 30000.0d)), (double) ((float) (iBt * 30000.0d))));
        Vec3d[] ajrArr = {d.mo9504d((Tuple3d) new Vec3d(200.0d, ScriptRuntime.NaN, ScriptRuntime.NaN)), d.mo9504d((Tuple3d) new Vec3d(ScriptRuntime.NaN, 200.0d, ScriptRuntime.NaN)), d.mo9504d((Tuple3d) new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, 200.0d))};
        int dom = dom();
        btJ().mo16113b(mo5495al(), ajrArr[dom], ajrArr[(dom + 1) % (don() + 1)]);
    }

    private int dok() {
        return Math.min(dol() + 3, iBo) - 1;
    }

    private int dol() {
        return this.f3559id - (this.f3559id % 3);
    }

    private int dom() {
        return this.f3559id % 3;
    }

    private int don() {
        return dok() % 3;
    }

    private int getGroupId() {
        return this.f3559id / 3;
    }
}
