package p001a;

import com.hoplon.geometry.Vec3f;
import logic.abc.C0802Lc;

/* renamed from: a.ayY */
/* compiled from: a */
public class ayY {
    public static final int gXE = 1;
    public static final int gXF = 2;
    public static final int gXG = 3;
    public static final int gXH = 4;
    public static final int gXI = 5;
    public final C3978xf gXJ = new C3978xf();
    public final Vec3f gXK = new Vec3f();
    public final Vec3f gXL = new Vec3f();
    public final C0763Kt stack = C0763Kt.bcE();
    public float dMd = 0.5f;
    public float dMe;
    public C3978xf deL = new C3978xf();
    public C0802Lc fgD;
    public C0783LL gXM;
    public int gXN = 1;
    public int gXO = -1;
    public int gXP = -1;
    public int gXQ = 1;
    public float gXR;
    public Object gXS;
    public Object gXT;
    public float gXU;
    public float gXV;
    public boolean gXW;
    /* renamed from: r */
    public float f5623r = 1.0f;

    /* renamed from: d */
    public boolean mo13925d(ayY ayy) {
        return true;
    }

    public boolean cEU() {
        return (this.gXN & 7) == 0;
    }

    public boolean cEV() {
        return (this.gXN & 1) != 0;
    }

    public boolean cEW() {
        return (this.gXN & 2) != 0;
    }

    public boolean cEX() {
        return (this.gXN & 3) != 0;
    }

    public boolean cEY() {
        return (this.gXN & 4) == 0;
    }

    public C0802Lc cEZ() {
        return this.fgD;
    }

    /* renamed from: a */
    public void mo17012a(C0802Lc lc) {
        this.fgD = lc;
    }

    public int cFa() {
        return this.gXQ;
    }

    /* renamed from: wv */
    public void mo17050wv(int i) {
        if (this.gXQ != 4 && this.gXQ != 5) {
            this.gXQ = i;
        }
    }

    public float cFb() {
        return this.gXR;
    }

    /* renamed from: la */
    public void mo17044la(float f) {
        this.gXR = f;
    }

    /* renamed from: ww */
    public void mo17051ww(int i) {
        this.gXQ = i;
    }

    public void activate() {
        mo17041hx(false);
    }

    /* renamed from: hx */
    public void mo17041hx(boolean z) {
        if (z || (this.gXN & 3) == 0) {
            mo17050wv(1);
            this.gXR = 0.0f;
        }
    }

    public boolean isActive() {
        return (cFa() == 2 || cFa() == 5) ? false : true;
    }

    public float cFc() {
        return this.dMe;
    }

    /* renamed from: lb */
    public void mo17045lb(float f) {
        this.dMe = f;
    }

    public float cFd() {
        return this.dMd;
    }

    /* renamed from: lc */
    public void mo17046lc(float f) {
        this.dMd = f;
    }

    public Object cFe() {
        return this.gXT;
    }

    public C3978xf cFf() {
        return this.deL;
    }

    /* renamed from: f */
    public void mo17040f(C3978xf xfVar) {
        this.deL.mo22947a(xfVar);
    }

    public C0783LL cFg() {
        return this.gXM;
    }

    /* renamed from: c */
    public void mo17015c(C0783LL ll) {
        this.gXM = ll;
    }

    public C3978xf cFh() {
        return this.gXJ;
    }

    /* renamed from: j */
    public void mo17043j(C3978xf xfVar) {
        this.gXJ.mo22947a(xfVar);
    }

    public Vec3f cFi() {
        return this.gXK;
    }

    public Vec3f cFj() {
        return this.gXL;
    }

    public int cFk() {
        return this.gXO;
    }

    /* renamed from: wx */
    public void mo17052wx(int i) {
        this.gXO = i;
    }

    public int cFl() {
        return this.gXP;
    }

    /* renamed from: wy */
    public void mo17053wy(int i) {
        this.gXP = i;
    }

    public float cFm() {
        return this.f5623r;
    }

    /* renamed from: ld */
    public void mo17047ld(float f) {
        this.f5623r = f;
    }

    public int cFn() {
        return this.gXN;
    }

    /* renamed from: wz */
    public void mo17054wz(int i) {
        this.gXN = i;
    }

    public float cFo() {
        return this.gXU;
    }

    /* renamed from: le */
    public void mo17048le(float f) {
        this.gXU = f;
    }

    public float cFp() {
        return this.gXV;
    }

    /* renamed from: lf */
    public void mo17049lf(float f) {
        this.gXV = f;
    }

    public Object cFq() {
        return this.gXS;
    }

    /* renamed from: aA */
    public void mo17013aA(Object obj) {
        this.gXS = obj;
    }

    /* renamed from: e */
    public boolean mo17039e(ayY ayy) {
        if (this.gXW) {
            return mo13925d(ayy);
        }
        return true;
    }
}
