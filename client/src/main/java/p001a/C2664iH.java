package p001a;

import game.script.ship.Ship;
import logic.ui.C2698il;
import logic.ui.item.Picture;
import logic.ui.item.Repeater;
import taikodom.addon.IAddonProperties;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.iH */
/* compiled from: a */
public class C2664iH extends C7023azk {
    private Repeater<Turret> fnv;

    public C2664iH(IAddonProperties vWVar, C2698il ilVar) {
        super(vWVar, ilVar);
        initComponents();
    }

    private void initComponents() {
        this.fnv = cFB().mo4919ch("turretsRep");
        this.fnv.mo22250a(new C2665a());
    }

    /* renamed from: M */
    public void mo4307M(Ship fAVar) {
        super.mo4307M(fAVar);
        this.fnv.clear();
        for (Turret G : fAVar.agF()) {
            this.fnv.mo22248G(G);
        }
    }

    /* renamed from: a.iH$a */
    class C2665a implements Repeater.C3671a<Turret> {
        C2665a() {
        }

        /* renamed from: a */
        public void mo843a(Turret jq, Component component) {
            Picture axm = (Picture) component;
            if (jq.isAlive()) {
                axm.setIcon(new ImageIcon(C2664iH.this.bUA().bHv().adz().getImage("res://imageset_iconography/material/turret_enable")));
            } else {
                axm.setIcon(new ImageIcon(C2664iH.this.bUA().bHv().adz().getImage("res://imageset_iconography/material/turret_disable")));
            }
        }
    }
}
