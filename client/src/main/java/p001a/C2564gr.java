package p001a;

import game.network.message.externalizable.C6302akO;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.gr */
/* compiled from: a */
public class C2564gr extends C6302akO {


    /* renamed from: PY */
    private long f7768PY;

    /* renamed from: PZ */
    private long f7769PZ;

    /* renamed from: Qa */
    private long f7770Qa;

    public C2564gr() {
    }

    public C2564gr(long j, long j2, long j3) {
        this.f7768PY = j;
        this.f7769PZ = j2;
        this.f7770Qa = j3;
    }

    public static long getSerialVersionUID() {
        return 1;
    }

    public void readExternal(ObjectInput objectInput) {
        this.f7768PY = objectInput.readLong();
        this.f7769PZ = objectInput.readLong();
        this.f7770Qa = objectInput.readLong();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeLong(this.f7768PY);
        objectOutput.writeLong(this.f7769PZ);
        objectOutput.writeLong(this.f7770Qa);
    }

    /* renamed from: vJ */
    public long mo19129vJ() {
        return this.f7768PY;
    }

    /* renamed from: vK */
    public long mo19130vK() {
        return this.f7769PZ;
    }

    /* renamed from: vL */
    public long mo19131vL() {
        return this.f7770Qa;
    }
}
