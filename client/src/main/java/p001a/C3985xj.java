package p001a;

import game.network.message.serializable.C2991mi;
import logic.res.html.MessageContainer;
import logic.ui.item.ListJ;

import java.util.*;

/* renamed from: a.xj */
/* compiled from: a */
public class C3985xj<T> extends C0636JA<T> implements ListJ<T> {

    public final ListJ<T> bFI;
    public transient ListJ<C2991mi<T>> bFH;

    public C3985xj(ListJ<T> list) {
        super(Collections.unmodifiableList(list));
        this.bFI = list;
    }

    public T get(int i) {
        return ((ListJ) this.dkv).get(i);
    }

    public void add(int i, T t) {
        anC();
        ((ListJ) this.dkv).add(i, t);
        mo2919a(new C6260ajY(C0651JP.ADD_AT, i, t));
    }

    public int indexOf(Object obj) {
        return ((ListJ) this.dkv).indexOf(obj);
    }

    public int lastIndexOf(Object obj) {
        return ((ListJ) this.dkv).lastIndexOf(obj);
    }

    public ListIterator<T> listIterator() {
        return ((ListJ) this.dkv).listIterator();
    }

    public ListIterator<T> listIterator(int i) {
        return ((ListJ) this.dkv).listIterator(i);
    }

    public T remove(int i) {
        anC();
        T remove = ((ListJ) this.dkv).remove(i);
        mo2919a(new C0402Fb(C0651JP.REMOVE_AT, i));
        return remove;
    }

    public T set(int i, T t) {
        anC();
        T t2 = ((ListJ) this.dkv).set(i, t);
        if (t2 != t) {
            mo2919a(new C6260ajY(C0651JP.SET, i, t));
        }
        return t2;
    }

    public boolean addAll(int i, Collection<? extends T> collection) {
        anC();
        ((ListJ) this.dkv).addAll(i, collection);
        mo2919a(new C6260ajY(C0651JP.ADD_COLLECTION_AT, i, collection));
        return true;
    }

    public ListJ<T> subList(int i, int i2) {
        return ((ListJ) this.dkv).subList(i, i2);
    }

    public ListJ<C2991mi<T>> anB() {
        return this.bFH;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo2919a(C2991mi<T> miVar) {
        if (this.bFH == null) {
            this.bFH = MessageContainer.init();
        }
        this.bFH.add(miVar);
    }

    public void clear() {
        anC();
        if (this.bFH != null) {
            this.bFH.clear();
        }
        super.clear();
    }

    /* access modifiers changed from: protected */
    public final void anC() {
        if (!this.bFM) {
            this.dkv = MessageContainer.m16004w(this.bFI);
            this.bFM = true;
        }
    }

    public ListJ<T> getResult() {
        if (this.bFM) {
            return (ListJ) this.dkv;
        }
        return this.bFI;
    }

    public Iterator<T> iterator() {
        return new C3986a();
    }

    /* renamed from: a.xj$a */
    class C3986a implements Iterator<T> {
        int aVX = 0;
        int expectedModCount;

        C3986a() {
            this.expectedModCount = C3985xj.this.anD();
        }

        public boolean hasNext() {
            amx();
            return this.aVX < C3985xj.this.dkv.size();
        }

        public T next() {
            amx();
            C3985xj xjVar = C3985xj.this;
            int i = this.aVX;
            this.aVX = i + 1;
            return xjVar.get(i);
        }

        private void amx() {
            if (this.expectedModCount != C3985xj.this.anD()) {
                throw new ConcurrentModificationException();
            }
        }

        public void remove() {
            amx();
            C3985xj.this.anC();
            C3985xj xjVar = C3985xj.this;
            int i = this.aVX - 1;
            this.aVX = i;
            C3985xj.this.mo2919a(new C6630aqe(C0651JP.REMOVE, xjVar.remove(i)));
            this.expectedModCount = C3985xj.this.anD();
        }
    }
}
