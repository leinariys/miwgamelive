package p001a;

import logic.aaa.C2235dB;

/* renamed from: a.aNd  reason: case insensitive filesystem */
/* compiled from: a */
public class C5537aNd<E extends C2235dB> implements C0463GP<E> {
    /* renamed from: id */
    public final int f3433id;
    private final E duo;
    public int irp;
    public aEY apM;
    public C2748jS irq;
    boolean fYs = false;
    private boolean disposed = false;

    public C5537aNd(aEY aey, E e) {
        this.apM = aey;
        int i = aey.nextId + 1;
        aey.nextId = i;
        this.f3433id = i;
        this.duo = e;
    }

    public void dispose() {
        this.disposed = true;
        if (!this.fYs) {
            this.fYs = true;
            this.apM.mo8639d(this);
        }
    }

    /* renamed from: kP */
    public E mo2340kP() {
        return this.duo;
    }

    /* renamed from: kO */
    public void mo2339kO() {
        if (!this.fYs) {
            this.fYs = true;
            this.apM.mo8639d(this);
        }
    }

    public boolean isDisposed() {
        return this.disposed;
    }

    public C2748jS djJ() {
        return this.irq;
    }

    /* renamed from: c */
    public void mo9463c(C2748jS jSVar) {
        if (this.irq != null) {
            this.irq.mo19946a(this);
        }
        this.irq = jSVar;
        if (jSVar != null) {
            jSVar.mo19949b(this);
        }
    }

    public int hashCode() {
        return this.f3433id;
    }

    public String toString() {
        return "EntityHandle:" + this.f3433id + "[" + this.duo + "]";
    }
}
