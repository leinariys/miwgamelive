package p001a;

import logic.aaa.C2783jw;
import taikodom.addon.messages.MissileWarningAddon;

/* renamed from: a.ajc  reason: case insensitive filesystem */
/* compiled from: a */
class C6264ajc extends C3428rT<C2783jw> {
    final /* synthetic */ MissileWarningAddon fOT;

    public C6264ajc(MissileWarningAddon missileWarningAddon) {
        this.fOT = missileWarningAddon;
    }

    /* renamed from: a */
    public void mo321b(C2783jw jwVar) {
        if (jwVar.fXd == C2783jw.C2784a.SPAWNED) {
            MissileWarningAddon missileWarningAddon = this.fOT;
            missileWarningAddon.aYk = missileWarningAddon.aYk + 1;
            if (!this.fOT.aYl.isVisible()) {
                this.fOT.m44594XZ();
            }
        } else if (jwVar.fXd == C2783jw.C2784a.UNSPAWNED && this.fOT.aYk > 0) {
            MissileWarningAddon missileWarningAddon2 = this.fOT;
            missileWarningAddon2.aYk = missileWarningAddon2.aYk - 1;
            if (this.fOT.aYk <= 0) {
                this.fOT.m44595Ya();
            }
        }
    }
}
