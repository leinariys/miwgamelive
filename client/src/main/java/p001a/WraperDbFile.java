package p001a;

import game.network.message.serializable.C1695Yw;
import taikodom.render.loader.provider.FilePath;

/* renamed from: a.aqc  reason: case insensitive filesystem */
/* compiled from: a */
public interface WraperDbFile {
    /* renamed from: HY */
    void mo5218HY();

    /* renamed from: a */
    void mo5219a(C1412Uh uh);

    boolean bOc();

    boolean bOd();

    void bOe();

    void bOf();

    /* renamed from: c */
    void mo5226c(C1695Yw yw);

    /* renamed from: f */
    void mo5234f(FilePath ain);

    void flush();

    /* renamed from: h */
    void mo5236h(C3582se seVar);

    /* renamed from: i */
    void mo5238i(C3582se seVar);

    void init();

    void start();
}
