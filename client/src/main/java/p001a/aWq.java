package p001a;

import game.engine.DataGameEvent;
import game.network.channel.client.ClientConnect;
import game.network.message.Blocking;
import game.network.message.externalizable.C0722KO;
import game.network.message.externalizable.C3213pH;
import game.network.message.externalizable.C5887acP;
import game.network.message.serializable.C0010AD;
import game.network.message.serializable.C1695Yw;
import game.network.message.serializable.C5613aQb;
import gnu.trove.THashMap;
import logic.baa.C1616Xf;
import logic.baa.aDJ;
import logic.bbb.aDR;
import logic.data.mbean.C0677Jd;
import logic.data.mbean.C5439aJj;
import logic.data.mbean.C6064afk;
import logic.res.code.C5663aRz;
import logic.res.html.C2491fm;
import logic.thred.LogPrinter;
import logic.thred.ThreadWrapper;
import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.logging.Log;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/* renamed from: a.aWq */
/* compiled from: a */
public final class aWq {
    public static final int jfF = -1;
    public static final int jfG = 0;
    private static final Log logger = LogPrinter.m10275K(aWq.class);
    static ThreadLocal<aWq> jfD = new ThreadLocal<>();
    private static aWq jfE = null;
    public int enr;
    public int env;
    public int enx;
    public int eny;
    public boolean jfH = false;
    public int jgb;
    public int jgc;
    public int jgd;
    public int jge;
    public int jgf;
    public boolean readOnly = false;
    private boolean disposed;
    private C0495Gr fvx;
    private boolean jfI = true;
    private aDR jfJ;
    private List<C2491fm> jfK = null;
    private List<C0495Gr> jfL = null;
    private List<Runnable> jfM;
    private Map<aDR, List<Blocking>> jfN;
    private MultiKeyMap jfO;
    private List<C0010AD> jfP;
    private List<C0677Jd> jfQ;
    private List<C0677Jd> jfR;
    private List<C1352Th> jfS;
    private List<aDR> jfT = null;
    private int jfU = 0;
    private int jfV;
    private int jfW;
    private int jfX;
    private int jfY;
    private int jfZ;
    private int jga;
    private C3814vM jgg = new C3814vM(this);
    private long startTime;

    private aWq(long j, aDR adr, C0495Gr gr) {
        m19284a(j, adr, gr);
    }

    public static aWq dDA() {
        aWq awq;
        Thread currentThread = Thread.currentThread();
        if (currentThread instanceof ThreadWrapper) {
            awq = (aWq) ((ThreadWrapper) currentThread).getContext();
        } else if (currentThread.getId() == 1) {
            awq = jfE;
        } else {
            awq = jfD.get();
        }
        if (awq == null || awq.disposed) {
            return null;
        }
        return awq;
    }

    /* renamed from: b */
    public static aWq m19285b(long j, aDR adr, C0495Gr gr) {
        aWq awq;
        Thread currentThread = Thread.currentThread();
        if (currentThread instanceof ThreadWrapper) {
            ThreadWrapper ano = (ThreadWrapper) currentThread;
            awq = (aWq) ano.getContext();
            if (awq == null) {
                aWq awq2 = new aWq(j, adr, gr);
                ano.setContext(awq2);
                return awq2;
            }
        } else {
            if (currentThread.getId() == 1) {
                awq = jfE;
            } else {
                awq = jfD.get();
            }
            if (awq == null) {
                aWq awq3 = new aWq(j, adr, gr);
                if (currentThread.getId() == 1) {
                    jfE = awq3;
                }
                jfD.set(awq3);
                return awq3;
            }
        }
        awq.m19284a(j, adr, gr);
        return awq;
    }

    /* renamed from: a */
    private void m19284a(long j, aDR adr, C0495Gr gr) {
        this.disposed = false;
        this.startTime = j;
        this.jfJ = adr;
        this.fvx = gr;
    }

    /* renamed from: a */
    public C5397aHt mo12047a(C1875af afVar) {
        return this.jgg.mo22565a(afVar.getObjectId().getId(), afVar);
    }

    /* renamed from: b */
    public C6064afk mo12052b(C1875af afVar) {
        C5397aHt a = mo12047a(afVar);
        if (a == null) {
            return afVar.cVo();
        }
        return a.hXh;
    }

    /* renamed from: i */
    public void mo12083i(Runnable runnable) {
        if (dDE()) {
            throw new C6411amT();
        }
        if (this.jfM == null) {
            this.jfM = new ArrayList();
        }
        this.jfM.add(runnable);
    }

    /* renamed from: e */
    public void mo12076e(C0495Gr gr) {
        if (dDE()) {
            throw new C6411amT();
        }
        if (this.jfL == null) {
            this.jfL = new ArrayList();
        }
        this.jfL.add(gr);
    }

    /* renamed from: k */
    public void mo12085k(C2491fm fmVar) {
        if (!dDE()) {
            if (this.jfK == null) {
                this.jfK = new ArrayList();
            }
            this.jfK.add(fmVar);
            this.jfV++;
        }
    }

    public boolean dDB() {
        return this.jfV != 0;
    }

    public String dDC() {
        StringBuilder sb = new StringBuilder();
        if (this.jfK != null) {
            boolean z = false;
            for (C2491fm next : this.jfK) {
                Class<?> hD = next.mo4752hD();
                String name = next.name();
                if (aDJ.class == hD && "push".equals(name)) {
                    if (!z) {
                        z = true;
                    }
                }
                sb.append("\t").append(hD.getName()).append(".").append(name).append("\n");
                z = z;
            }
        }
        return sb.toString();
    }

    /* renamed from: kL */
    public void mo12086kL(boolean z) {
        this.jfI = z;
    }

    public boolean dDD() {
        return false;
    }

    public boolean dDE() {
        return this.jfH;
    }

    /* renamed from: kM */
    public void mo12087kM(boolean z) {
        this.jfH = z;
    }

    public long getStartTime() {
        return this.startTime;
    }

    public Date bxM() {
        return new Date(this.startTime);
    }

    /* renamed from: a */
    public void mo12050a(PrintStream printStream) {
        if (printStream == null) {
            printStream = System.out;
        }
        printStream.println("====== Transaction context dump START ======");
        printStream.print("Dump time: ");
        printStream.println(new Date().toString());
        printStream.println();
        printStream.print("Transaction start time: ");
        printStream.println(new Date(this.startTime).toString());
        printStream.println();
        printStream.println("From: " + this.jfJ.bgt().toString());
        printStream.println();
        printStream.println("Invoker: " + this.fvx.aQK().name());
        printStream.println();
        printStream.println("Contexts:");
        printStream.println("====== Transaction context dump END ======");
    }

    /* renamed from: a */
    public void mo12048a(aDR adr, Blocking kUVar) {
        if (this.jfN == null) {
            this.jfN = new THashMap();
        }
        List list = this.jfN.get(adr);
        if (list == null) {
            list = new ArrayList();
            this.jfN.put(adr, list);
        }
        list.add(kUVar);
    }

    public Map<aDR, List<Blocking>> dDF() {
        return this.jfN;
    }

    public void dDG() {
        for (C5397aHt aht = this.jgg.bzX; aht != null; aht = aht.hXl) {
            aht.lock();
            aht.ehD.mo13216c(this.startTime);
        }
        if (this.fvx != null) {
            this.fvx.aQK().mo4730aX(this.jgf);
            this.fvx.aQK().mo4729aW(this.jge);
        }
    }

    public void dDH() {
        for (C5397aHt aht = this.jgg.bzX; aht != null; aht = aht.hXl) {
            aht.unlock();
        }
    }

    public void dDI() {
        this.readOnly = true;
        if (this.jfY != 0) {
            int i = 0;
            int i2 = this.jfY;
            while (true) {
                int i3 = i2 - 1;
                if (i3 >= 0) {
                    C0677Jd jd = this.jfR.get(i);
                    C6014aem[] dp = ((C1875af) jd.mo11944yn().bFf()).mo13225dp();
                    jd.aXz();
                    if (dp.length > 0) {
                        for (C6014aem aem : dp) {
                            if (aem.bUi()) {
                                mo12048a(aem.eIu, (Blocking) new C3213pH(jd));
                            }
                        }
                    }
                    i++;
                    i2 = i3;
                } else {
                    return;
                }
            }
        }
    }

    public C1352Th dDJ() {
        if (this.jfS.isEmpty()) {
            return null;
        }
        return this.jfS.remove(this.jfS.size() - 1);
    }

    public void dDK() {
        if (this.jfS != null) {
            THashMap tHashMap = new THashMap();
            while (true) {
                C1352Th dDJ = dDJ();
                if (dDJ == null) {
                    break;
                }
                aDR buX = dDJ.buX();
                C1875af buW = dDJ.buW();
                if (!buX.mo8402f(buW) && buW.mo13209a((C1619Xi<C1616Xf>) null, buX, false) && buX.mo8400e(buW)) {
                    List list = (List) tHashMap.get(buX);
                    if (list == null) {
                        list = new ArrayList();
                        tHashMap.put(buX, list);
                    }
                    list.add(buW.mo22004v(buX));
                }
            }
            if (!tHashMap.isEmpty()) {
                for (Map.Entry entry : tHashMap.entrySet()) {
                    mo12048a((aDR) entry.getKey(), (Blocking) new C5887acP((List<C6064afk>) (List) entry.getValue()));
                }
            }
        }
    }

    public void dDL() {
        if (this.jfL != null) {
            this.readOnly = true;
            mo12086kL(false);
            for (C0495Gr next : this.jfL) {
                try {
                    next.aQL().mo14a(next);
                } catch (Throwable th) {
                    logger.warn("Exception while executing postponed call " + next.aQK().name() + " from " + (this.jfJ == null ? "(dont know)" : this.jfJ.bgt()) + " in a " + next.aQK().name() + " call. see next:", th);
                }
            }
            this.readOnly = false;
        }
    }

    public void dDM() {
        if (this.jfM != null) {
            for (Runnable run : this.jfM) {
                run.run();
            }
            this.jfM.clear();
            this.jfM = null;
        }
    }

    /* renamed from: f */
    public void mo12079f(DataGameEvent jz) {
        C5397aHt aht = this.jgg.bzX;
        while (aht != null) {
            C1875af afVar = aht.ehD;
            try {
                if (!aht.isDirty()) {
                    aht.hXn = true;
                } else {
                    C6064afk cVo = afVar.cVo();
                    boolean hF = cVo.mo3202hF();
                    C0677Jd jd = aht.hXh;
                    jd.mo3160a(cVo, cVo.aBt() + 1);
                    if (jd.aRA() != 0 || aht.isCreated() || jd.mo3194du()) {
                        afVar.mo13206a((C6064afk) jd);
                        afVar.mo21972a(hF, (C5439aJj) null, cVo, jd);
                        if (afVar.mo6901yn().mo11T().mo15115Ew() != C1020Ot.C1021a.NOT_PERSISTED) {
                            if (aht.isCreated()) {
                                this.jga++;
                                if (afVar.mo6901yn().mo11T().mo15115Ew() != C1020Ot.C1021a.NOT_PERSISTED) {
                                    if (this.jfP == null) {
                                        this.jfP = new ArrayList(Math.min(10, this.jgg.size));
                                    }
                                    aht.hXh.aXy();
                                    this.jfP.add(new C0010AD(afVar.getObjectId(), afVar.bFU()));
                                    this.jfW++;
                                }
                                if (aht.isCreated() && !jd.mo3194du()) {
                                    mo12078f(jd);
                                }
                            } else if (jd.aRA() != 0 && !jd.mo3194du()) {
                                if (jd.aXt()) {
                                    mo12078f(jd);
                                } else if (jd.aXv()) {
                                    jz.bHf().mo14759b(jd);
                                }
                            }
                        }
                        if ((jd.aRA() != 0 || jd.mo3194du()) && !aht.isCreated()) {
                            this.jfZ++;
                            if (jd.aXs()) {
                                mo12077e(jd);
                            } else if (jd.aXu()) {
                                jz.bHf().mo14758a(jd);
                            }
                        }
                    } else {
                        aht.hXn = true;
                    }
                }
                aht = aht.hXl;
            } catch (Exception e) {
                throw new RuntimeException("Error computing diff of " + afVar.bFY() + (aht.isCreated() ? " (just created)" : ""), e);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo12077e(C0677Jd jd) {
        if (this.jfR == null) {
            this.jfR = new ArrayList();
        }
        this.jfR.add(jd);
        this.jfY++;
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public void mo12078f(C0677Jd jd) {
        if (this.jfQ == null) {
            this.jfQ = new ArrayList();
        }
        this.jfQ.add(jd);
        this.jfX++;
    }

    public boolean dDN() {
        Object bgt;
        String str;
        for (C5397aHt aht = this.jgg.bzX; aht != null; aht = aht.hXl) {
            C1875af afVar = aht.ehD;
            C0677Jd jd = aht.hXh;
            C6064afk cVo = afVar.cVo();
            if (cVo.aBt() != jd.aBt()) {
                if (afVar.mo10528du()) {
                    return false;
                }
                C5663aRz[] c = afVar.mo6901yn().mo25c();
                int i = 0;
                while (i < c.length) {
                    C5663aRz arz = c[i];
                    if (cVo.mo3210l(arz) == jd.mo3210l(arz) || !jd.mo3222u(arz)) {
                        i++;
                    } else {
                        Log log = logger;
                        StringBuilder append = new StringBuilder("Transaction canceled coz object ").append(afVar.bFU()).append(":").append(afVar.getObjectId().getId()).append(".").append(arz.name()).append(" is outdated. (Call from ");
                        if (this.jfJ == null) {
                            bgt = "(don't know)";
                        } else {
                            bgt = this.jfJ.bgt();
                        }
                        StringBuilder append2 = append.append(bgt).append(", method ");
                        if (this.fvx != null) {
                            str = String.valueOf(C3582se.m38988o(this.fvx.aQL())) + "." + this.fvx.aQK().name();
                        } else {
                            str = "don't know";
                        }
                        log.info(append2.append(str).append(")").toString());
                        dDB();
                        return false;
                    }
                }
                continue;
            }
        }
        return true;
    }

    public void dDO() {
        for (C5397aHt aht = this.jgg.bzX; aht != null; aht = aht.hXl) {
            C1875af afVar = aht.ehD;
            if (aht.isCreated()) {
                afVar.cVg();
            }
        }
    }

    public void freeze() {
        this.readOnly = true;
        if (this.jgg.size != 0) {
            this.jgg.akX();
        }
    }

    public void dDP() {
    }

    /* renamed from: g */
    public int mo12081g(DataGameEvent jz) {
        freeze();
        try {
            dDG();
            if (!dDN()) {
                dDP();
                dDH();
                return -1;
            }
            mo12079f(jz);
            if (this.fvx != null) {
                if (this.jfZ != 0) {
                    this.fvx.aQK().mo4728aV(this.jfZ);
                }
                if (this.jga > 0) {
                    this.fvx.aQK().mo4731aY(this.jga);
                }
            }
            if (!(this.jfX == 0 && this.jfW == 0)) {
                C1695Yw yw = new C1695Yw();
                if (this.jfW != 0) {
                    yw.mo7310v(new ArrayList(this.jfP));
                }
                if (this.jfX != 0) {
                    int i = 0;
                    int i2 = this.jfX;
                    while (true) {
                        i2--;
                        if (i2 < 0) {
                            break;
                        }
                        C0677Jd jd = this.jfQ.get(i);
                        yw.mo7299a(new C0722KO(jd.mo11944yn().bFf().getObjectId(), jd, jd.mo11944yn().getClass()));
                        jd.aXy();
                        i++;
                    }
                }
                C5613aQb bHC = yw.bHC();
                if (this.fvx != null) {
                    bHC.mo11034lL(this.fvx.aQL().bFf().getObjectId().getId());
                    bHC.mo11025Ae(jz.bxy().getMagicNumber(this.fvx.aQK().mo4752hD()));
                    bHC.mo11026Af(this.fvx.aQK().mo7395hq());
                }
                bHC.mo11035p(jz.bGD());
                jz.mo3348a(yw);
            }
            dDP();
            dDI();
            dDK();
            dDL();
            dDK();
            dDH();
            mo12087kM(true);
            dDM();
            dDK();
            return 0;
        } catch (Throwable th) {
            dDH();
            throw th;
        }
    }

    public void dispose() {
        this.readOnly = false;
        this.jfH = false;
        this.jfI = true;
        this.startTime = 0;
        this.jfJ = null;
        this.fvx = null;
        if (this.jfV != 0) {
            this.jfK.clear();
            this.jfV = 0;
        }
        if (this.jfL != null) {
            this.jfL.clear();
        }
        if (this.jfM != null) {
            this.jfM.clear();
        }
        if (this.jfN != null) {
            this.jfN.clear();
        }
        if (this.jfO != null) {
            this.jfO.clear();
        }
        this.jfZ = 0;
        this.jga = 0;
        this.env = 0;
        this.jgb = 0;
        this.eny = 0;
        this.jgc = 0;
        this.jgd = 0;
        this.enx = 0;
        this.enr = 0;
        this.jge = 0;
        this.jgf = 0;
        if (this.jfW != 0) {
            this.jfW = 0;
            this.jfP.clear();
        }
        if (this.jfX != 0) {
            this.jfX = 0;
            this.jfQ.clear();
        }
        if (this.jfY != 0) {
            this.jfR.clear();
            this.jfY = 0;
        }
        if (this.jgg.size != 0) {
            this.jgg.clear();
        }
        if (this.jfS != null) {
            this.jfS.clear();
        }
        if (this.jfT != null) {
            this.jfT.clear();
        }
        this.jfU = 0;
        this.disposed = true;
    }

    public boolean dDQ() {
        return this.readOnly;
    }

    /* renamed from: a */
    public void mo12049a(C1875af afVar, aDR adr) {
        if (this.jfO == null) {
            this.jfO = new MultiKeyMap();
            this.jfS = new ArrayList();
        } else if (this.jfO.containsKey(afVar, adr)) {
            return;
        }
        this.jfO.put(afVar, adr, Boolean.TRUE);
        this.jfS.add(new C1352Th(afVar, adr));
    }

    /* renamed from: a */
    public boolean mo12051a(C3582se seVar, ClientConnect bVar) {
        if (this.jfO != null && this.jfO.containsKey(seVar, bVar)) {
            return true;
        }
        return false;
    }

    public void dDR() {
        this.jfU++;
    }

    public int dDS() {
        int i = this.jfU - 1;
        this.jfU = i;
        return i;
    }

    public boolean isDisposed() {
        return this.disposed;
    }

    /* renamed from: D */
    public void mo12046D(aDR adr) {
        if (this.jfT == null) {
            this.jfT = new ArrayList();
        }
        this.jfT.add(adr);
    }

    public void dDT() {
        if (this.jfT != null && this.jfT.size() > 0) {
            this.jfT.remove(this.jfT.size() - 1);
        }
    }

    public int dDU() {
        return this.jgg.size;
    }

    public aDR dDV() {
        List<aDR> list = this.jfT;
        if (list == null || list.size() <= 0) {
            return null;
        }
        return list.get(list.size() - 1);
    }
}
