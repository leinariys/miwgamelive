package p001a;

/* renamed from: a.tV */
/* compiled from: a */
public enum C3667tV implements C5990aeO {
    CHAR_LEVEL_UP("sfx_level_up"),
    CHAR_XP_GAINED("sfx_xp_won"),
    CHAR_REWARD_GAINED("sfx_reward_won"),
    FLY_MODE_CHANGE("sfx_mode_change"),
    MESS_SYSTEM_MESSAGE_TYPE("sfx_message_type"),
    MESS_NPC_CHAT_LOOP("sfx_npc_chat_loop"),
    MESS_ERROR("sfx_error_message"),
    MOUSE_CLICK_SFX("sfx_mouse_click"),
    MOUSE_CLICK_SCROLL("sfx_scroll_click"),
    MOUSE_RADIO_OVER_SFX("sfx_radio_over"),
    OPEN_WINDOW_SFX("sfx_window_open"),
    EQUIP_AMPLIFIER_MODULE("sfx_equip_amplifier"),
    EQUIP_CANNON_ENERGY("sfx_equip_energy"),
    EQUIP_CANNON_PROJECTILE("sfx_equip_projectile"),
    EQUIP_LAUNCHER("sfx_equip_launcher"),
    EQUIP_REAMMO("sfx_equip_reammo"),
    WEAPON_CHANGE("sfx_equip_change"),
    WEAPON_EMPTY_PROJECTILE_LAUNCHER("sfx_equip_empty_a"),
    WEAPON_EMPTY_ENERGY("sfx_equip_empty_b"),
    CHAT_SEND("sfx_chat_send"),
    AVATAR_SPIN_LOOP("sfx_scroll_loop"),
    LOGIN_START("sfx_mouse_click_login"),
    WELCOME_ABOARD("sfx_welcome_aboard"),
    GATE_ENTER("sfx_gate_enter"),
    GATE_EXIT("sfx_gate_exit"),
    SHIELD_EMPTY("sfx_shield_empty"),
    SHIELD_FULL("sfx_shield_full"),
    MISSILE_LOCKED("sfx_missile_target_lock"),
    MISSILE_LOCKING_LOOP("sfx_missile_search_near"),
    MISSILE_PURSUIT_LOOP("sfx_missile_pursuit_loop"),
    SPECIAL_HAZARD_LOOP("sfx_special_hazard_loop"),
    SPECIAL_BOOST_FULL("sfx_special_boost_full"),
    SPECIAL_BOOST_EMPTY("sfx_special_boost_empty"),
    CAREER_FIGHTER("sfx_fighter_click"),
    CAREER_BOMBER("sfx_bomber_click"),
    CAREER_EXPLORER("sfx_explorer_click"),
    CAREER_FREIGHTER("sfx_freighter_click");

    public static final String buD = "data/audio/interface/interface_sfx.pro";
    private String file;
    private String handle;

    private C3667tV(String str) {
        this.handle = str;
        this.file = buD;
    }

    private C3667tV(String str, String str2) {
        this.handle = str;
        this.file = str2;
    }

    public String getHandle() {
        return this.handle;
    }

    public String getFile() {
        return this.file;
    }
}
