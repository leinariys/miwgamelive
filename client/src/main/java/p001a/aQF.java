package p001a;

import game.script.citizenship.inprovements.InsuranceImprovement;
import game.script.insurance.InsuranceDefaults;
import game.script.item.Clip;
import game.script.item.Item;
import game.script.item.ProjectileWeapon;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.ship.ShipSector;
import game.script.ship.ShipStructure;
import logic.ui.item.ListJ;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

/* renamed from: a.aQF */
/* compiled from: a */
public class aQF {

    /* renamed from: P */
    private final Player f3589P;

    /* renamed from: RT */
    private final InsuranceDefaults f3590RT;
    private final Ship aOO;
    private ListJ<Item> iEP;
    private ListJ<Item> iEQ;
    private long iER;
    private String iES;

    public aQF(InsuranceDefaults ah, Player aku, Ship fAVar) {
        this.f3590RT = ah;
        this.f3589P = aku;
        this.aOO = fAVar;
        if (fAVar == null) {
            throw new RuntimeException("InsurerController can not be initialized with a null ship!");
        }
        dpQ();
    }

    private void dpQ() {
        dpS();
        dpR();
        InsuranceImprovement bqB = this.f3589P.cXk().bqB();
        if (bqB != null) {
            this.iER = bqB.bPR();
            this.iES = bqB.bPT().get();
            return;
        }
        this.iER = this.f3590RT.awt();
        this.iES = this.f3590RT.awz().get();
    }

    private void dpR() {
        this.iEQ = new ArrayList();
    }

    private void dpS() {
        this.iEP = m17447aw(mo10875al());
        this.iEP.add(mo10875al().agr());
        Collections.sort(this.iEP, new C1820a());
    }

    /* renamed from: aw */
    private ListJ<Item> m17447aw(Ship fAVar) {
        return fAVar.aiB();
    }

    /* renamed from: N */
    public long mo10868N(ListJ<Item> list) {
        return mo10874a(list, false);
    }

    /* renamed from: a */
    public long mo10874a(ListJ<Item> list, boolean z) {
        long j = 0;
        Iterator<Item> it = list.iterator();
        while (true) {
            long j2 = j;
            if (!it.hasNext()) {
                return j2;
            }
            j = mo10886g(it.next(), z) + j2;
        }
    }

    /* renamed from: j */
    public long mo10887j(Item auq) {
        return mo10886g(auq, false);
    }

    /* renamed from: g */
    public long mo10886g(Item auq, boolean z) {
        if (z || !auq.cxn()) {
            return auq.bAP().mo19868HG() * ((long) auq.mo302Iq());
        }
        return 0;
    }

    /* renamed from: S */
    public void mo10872S(Item auq) {
        if (this.iEP.contains(auq)) {
            this.iEP.remove(auq);
            this.iEQ.add(auq);
        }
    }

    /* renamed from: T */
    public void mo10873T(Item auq) {
        if (this.iEQ.contains(auq) && !auq.cxn()) {
            this.iEQ.remove(auq);
            this.iEP.add(auq);
        }
    }

    /* renamed from: O */
    public void mo10869O(ListJ<Item> list) {
        for (Item S : list) {
            mo10872S(S);
        }
    }

    /* renamed from: oW */
    public void mo10890oW() {
        ArrayList arrayList = new ArrayList();
        for (Item add : dpX()) {
            arrayList.add(add);
        }
        mo10869O(arrayList);
    }

    /* renamed from: oX */
    public void mo10891oX() {
        ArrayList arrayList = new ArrayList();
        for (Item add : dpY()) {
            arrayList.add(add);
        }
        mo10870P(arrayList);
    }

    /* renamed from: P */
    public void mo10870P(ListJ<Item> list) {
        for (Item T : list) {
            mo10873T(T);
        }
    }

    /* renamed from: lM */
    public long mo10888lM(long j) {
        long dqb = j - dqb();
        if (dqb < 0) {
            return 0;
        }
        return dqb;
    }

    /* renamed from: lN */
    public float mo10889lN(long j) {
        return (((float) this.f3590RT.awv()) / 100.0f) * ((float) j);
    }

    @C5566aOg
    public ListJ<Item> dpT() {
        ArrayList arrayList = new ArrayList();
        for (ShipStructure bVG : this.aOO.agt()) {
            for (ShipSector or : bVG.bVG()) {
                Iterator<Item> iterator = or.getIterator();
                ArrayList<Item> arrayList2 = new ArrayList<>();
                while (iterator.hasNext()) {
                    Item next = iterator.next();
                    if (next != null && m17446a((ListJ<Item>) arrayList, next)) {
                        arrayList2.add(next);
                    }
                }
                for (Item d : arrayList2) {
                    or.mo2690d(d);
                }
            }
        }
        return arrayList;
    }

    /* renamed from: a */
    private boolean m17446a(ListJ<Item> list, Item auq) {
        ProjectileWeapon sg;
        Clip vo;
        if (auq instanceof ProjectileWeapon) {
            sg = (ProjectileWeapon) auq;
            vo = sg.bup();
        } else {
            sg = null;
            vo = null;
        }
        if (dpY().contains(auq)) {
            if (!(sg == null || vo == null || dpY().contains(vo))) {
                sg.mo5366c((Clip) null);
            }
            return false;
        }
        if (vo != null && dpY().contains(vo)) {
            list.add(vo);
        }
        return true;
    }

    public boolean dpU() {
        long lM = mo10888lM(mo10868N(dpY()));
        if (lM <= this.f3589P.mo5156Qw().bSs() || lM <= 0) {
            return true;
        }
        return false;
    }

    public long dpV() {
        return (long) (((double) mo10888lM(mo10868N(dpY()))) + ((double) mo10889lN(mo10874a(dpY(), true))));
    }

    /* renamed from: dL */
    public Player mo10876dL() {
        return this.f3589P;
    }

    /* renamed from: al */
    public Ship mo10875al() {
        return this.aOO;
    }

    public ListJ<Item> dpW() {
        return this.iEP;
    }

    public ListJ<Item> dpX() {
        return this.iEP;
    }

    public ListJ<Item> dpY() {
        return this.iEQ;
    }

    public String dpZ() {
        return this.iES;
    }

    /* renamed from: xv */
    public InsuranceDefaults mo10892xv() {
        return this.f3590RT;
    }

    @C5566aOg
    /* renamed from: Q */
    public void mo10871Q(ListJ<Item> list) {
        for (Item next : list) {
            if (next instanceof ProjectileWeapon) {
                ((ProjectileWeapon) next).mo5366c((Clip) null);
            }
        }
    }

    public void dqa() {
        long N = mo10868N(dpY());
        long j = N;
        for (Item auq : new ArrayList(dpX())) {
            if (auq.cxn()) {
                mo10872S(auq);
            } else {
                long j2 = mo10887j(auq);
                if (mo10888lM(j + j2) <= 0) {
                    mo10872S(auq);
                    j += j2;
                }
            }
        }
    }

    public long dqb() {
        return this.iER;
    }

    /* renamed from: a.aQF$a */
    class C1820a implements Comparator<Item> {
        C1820a() {
        }

        /* renamed from: a */
        public int compare(Item auq, Item auq2) {
            return Long.valueOf(auq.bAP().mo19868HG()).compareTo(Long.valueOf(auq2.bAP().mo19868HG())) * -1;
        }
    }
}
