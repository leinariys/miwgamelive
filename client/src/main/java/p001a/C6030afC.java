package p001a;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

/* renamed from: a.afC  reason: case insensitive filesystem */
/* compiled from: a */
public final class C6030afC {
    static final String fuS = " [wrapped] ";
    private static final String fuR = System.getProperty("line.separator");
    private static final String[] fuT = {"getCause", "getNextException", "getTargetException", "getException", "getSourceException", "getRootCause", "getCausedByException", "getNested"};
    private static final Method fuU;

    static {
        Method method = null;
        try {
            method = Throwable.class.getMethod("getCause", (Class[]) null);
        } catch (Exception e) {
        }
        fuU = method;
    }

    private C6030afC() {
    }

    public static Throwable getCause(Throwable th) {
        return m21468a(th, fuT);
    }

    /* renamed from: a */
    public static Throwable m21468a(Throwable th, String[] strArr) {
        if (th == null) {
            return null;
        }
        Throwable e = m21475e(th);
        if (e != null) {
            return e;
        }
        if (strArr == null) {
            strArr = fuT;
        }
        int i = 0;
        while (i < strArr.length && ((r2 = strArr[i]) == null || (e = m21467a(th, r2)) == null)) {
            i++;
        }
        if (e == null) {
            return m21471b(th, "detail");
        }
        return e;
    }

    /* renamed from: d */
    public static Throwable m21473d(Throwable th) {
        Throwable cause = getCause(th);
        if (cause == null) {
            return cause;
        }
        while (true) {
            Throwable th2 = cause;
            cause = getCause(cause);
            if (cause == null) {
                return th2;
            }
        }
    }

    /* renamed from: e */
    private static Throwable m21475e(Throwable th) {
        if (th instanceof C5404aIa) {
            return ((C5404aIa) th).getCause();
        }
        if (th instanceof SQLException) {
            return ((SQLException) th).getNextException();
        }
        if (th instanceof InvocationTargetException) {
            return ((InvocationTargetException) th).getTargetException();
        }
        return null;
    }

    /* renamed from: a */
    private static Throwable m21467a(Throwable th, String str) {
        Method method;
        try {
            method = th.getClass().getMethod(str, (Class[]) null);
        } catch (NoSuchMethodException e) {
            method = null;
        } catch (SecurityException e2) {
            method = null;
        }
        if (method != null && Throwable.class.isAssignableFrom(method.getReturnType())) {
            try {
                return (Throwable) method.invoke(th, new Object[0]);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e3) {
            }
        }
        return null;
    }

    /* renamed from: b */
    private static Throwable m21471b(Throwable th, String str) {
        Field field;
        try {
            field = th.getClass().getField(str);
        } catch (NoSuchFieldException e) {
            field = null;
        } catch (SecurityException e2) {
            field = null;
        }
        if (field != null && Throwable.class.isAssignableFrom(field.getType())) {
            try {
                return (Throwable) field.get(th);
            } catch (IllegalAccessException | IllegalArgumentException e3) {
            }
        }
        return null;
    }

    public static boolean bVB() {
        return fuU != null;
    }

    /* renamed from: f */
    public static boolean m21476f(Throwable th) {
        if (th == null) {
            return false;
        }
        if (th instanceof C5404aIa) {
            return true;
        }
        if (th instanceof SQLException) {
            return true;
        }
        if (th instanceof InvocationTargetException) {
            return true;
        }
        if (bVB()) {
            return true;
        }
        Class<?> cls = th.getClass();
        int length = fuT.length;
        for (int i = 0; i < length; i++) {
            try {
                Method method = cls.getMethod(fuT[i], (Class[]) null);
                if (method != null && Throwable.class.isAssignableFrom(method.getReturnType())) {
                    return true;
                }
            } catch (NoSuchMethodException | SecurityException e) {
            }
        }
        try {
            if (cls.getField("detail") != null) {
                return true;
            }
            return false;
        } catch (NoSuchFieldException | SecurityException e2) {
            return false;
        }
    }

    /* renamed from: g */
    public static int m21477g(Throwable th) {
        int i = 0;
        while (th != null) {
            i++;
            th = getCause(th);
        }
        return i;
    }

    /* renamed from: h */
    public static Throwable[] m21478h(Throwable th) {
        ArrayList arrayList = new ArrayList();
        while (th != null) {
            arrayList.add(th);
            th = getCause(th);
        }
        return (Throwable[]) arrayList.toArray(new Throwable[arrayList.size()]);
    }

    /* renamed from: a */
    public static int m21465a(Throwable th, Class cls) {
        return m21466a(th, cls, 0);
    }

    /* renamed from: a */
    public static int m21466a(Throwable th, Class cls, int i) {
        if (th == null) {
            return -1;
        }
        if (i < 0) {
            i = 0;
        }
        Throwable[] h = m21478h(th);
        if (i >= h.length) {
            return -1;
        }
        while (i < h.length) {
            if (h[i].getClass().equals(cls)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    /* renamed from: i */
    public static void m21479i(Throwable th) {
        m21469a(th, System.err);
    }

    /* renamed from: a */
    public static void m21469a(Throwable th, PrintStream printStream) {
        if (th != null) {
            if (printStream == null) {
                throw new IllegalArgumentException("The PrintStream must not be null");
            }
            String[] j = m21481j(th);
            for (String println : j) {
                printStream.println(println);
            }
            printStream.flush();
        }
    }

    /* renamed from: a */
    public static void m21470a(Throwable th, PrintWriter printWriter) {
        if (th != null) {
            if (printWriter == null) {
                throw new IllegalArgumentException("The PrintWriter must not be null");
            }
            String[] j = m21481j(th);
            for (String println : j) {
                printWriter.println(println);
            }
            printWriter.flush();
        }
    }

    /* renamed from: j */
    public static String[] m21481j(Throwable th) {
        List<String> list;
        if (th == null) {
            return new String[0];
        }
        Throwable[] h = m21478h(th);
        int length = h.length;
        ArrayList arrayList = new ArrayList();
        List<String> l = m21483l(h[length - 1]);
        int i = length;
        while (true) {
            int i2 = i - 1;
            if (i2 < 0) {
                return (String[]) arrayList.toArray(new String[0]);
            }
            if (i2 != 0) {
                List<String> l2 = m21483l(h[i2 - 1]);
                m21474d(l, l2);
                list = l2;
            } else {
                list = l;
            }
            if (i2 == length - 1) {
                arrayList.add(h[i2].toString());
            } else {
                arrayList.add(fuS + h[i2].toString());
            }
            for (int i3 = 0; i3 < l.size(); i3++) {
                arrayList.add(l.get(i3));
            }
            i = i2;
            l = list;
        }
    }

    /* renamed from: d */
    public static void m21474d(List<String> list, List<String> list2) {
        if (list == null || list2 == null) {
            throw new IllegalArgumentException("The List must not be null");
        }
        int size = list.size() - 1;
        int size2 = list2.size() - 1;
        for (int i = size; i >= 0 && size2 >= 0; i--) {
            if (list.get(i).equals(list2.get(size2))) {
                list.remove(i);
            }
            size2--;
        }
    }

    public static String getStackTrace(Throwable th) {
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter, true));
        return stringWriter.getBuffer().toString();
    }

    /* renamed from: k */
    public static String m21482k(Throwable th) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter, true);
        Throwable[] h = m21478h(th);
        for (int i = 0; i < h.length; i++) {
            h[i].printStackTrace(printWriter);
            if (m21476f(h[i])) {
                break;
            }
        }
        return stringWriter.getBuffer().toString();
    }

    /* renamed from: b */
    public static String[] m21472b(Throwable th) {
        if (th == null) {
            return new String[0];
        }
        return m21480iF(getStackTrace(th));
    }

    /* renamed from: iF */
    static String[] m21480iF(String str) {
        StringTokenizer stringTokenizer = new StringTokenizer(str, fuR);
        LinkedList linkedList = new LinkedList();
        while (stringTokenizer.hasMoreTokens()) {
            linkedList.add(stringTokenizer.nextToken());
        }
        return (String[]) linkedList.toArray(new String[linkedList.size()]);
    }

    /* renamed from: l */
    static List<String> m21483l(Throwable th) {
        StringTokenizer stringTokenizer = new StringTokenizer(getStackTrace(th), fuR);
        LinkedList linkedList = new LinkedList();
        boolean z = false;
        while (stringTokenizer.hasMoreTokens()) {
            String nextToken = stringTokenizer.nextToken();
            int indexOf = nextToken.indexOf("at");
            if (indexOf != -1 && nextToken.substring(0, indexOf).trim().length() == 0) {
                z = true;
                linkedList.add(nextToken);
            } else if (z) {
                break;
            }
        }
        return linkedList;
    }
}
