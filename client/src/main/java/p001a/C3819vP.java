package p001a;

import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.WString;
import com.sun.jna.ptr.NativeLongByReference;
import com.sun.jna.win32.StdCallLibrary;

/* renamed from: a.vP */
/* compiled from: a */
public class C3819vP {
    private static final String eNw = "http://";

    /* renamed from: l */
    public static C4077yz m40251l(String str, String str2, String str3) {
        String str4;
        if (eNw.startsWith(".")) {
            str4 = String.valueOf(eNw) + str.substring(1);
        } else {
            str4 = String.valueOf(eNw) + str;
        }
        WString wString = new WString(String.valueOf(str4) + str2);
        WString wString2 = new WString(str3);
        Memory memory = new Memory(1000);
        if (!C3820a.bAb.mo22579a(wString, wString2, memory, new NativeLongByReference(new NativeLong(1000)))) {
            return null;
        }
        String substring = memory.getString(0, true).substring((String.valueOf(str3) + "=").length());
        System.out.println("*********** " + substring);
        return new C4077yz(str, str2, str3, substring, 0);
    }

    /* renamed from: a.vP$a */
    public interface C3820a extends StdCallLibrary {
        public static final C3820a bAb = ((C3820a) Native.loadLibrary("wininet", C3820a.class));

        /* renamed from: a */
        boolean mo22579a(WString wString, WString wString2, Memory memory, NativeLongByReference nativeLongByReference);
    }
}
