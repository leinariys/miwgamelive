package p001a;

import com.hoplon.geometry.Vec3f;
import logic.abc.*;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.d */
/* compiled from: a */
public class C2225d {
    /* renamed from: jR */
    static final /* synthetic */ boolean f6422jR;
    private static boolean czW = true;

    static {
        boolean z;
        if (!C2225d.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        f6422jR = z;
    }

    public final C0763Kt stack = C0763Kt.bcE();
    /* renamed from: Ux */
    public C1072Pg f6423Ux;
    public C5408aIe cnP = new C5408aIe();
    public List<ayY> czT = new ArrayList();
    public C1701ZB czU;
    public C3737uO czV;

    public C2225d(C1701ZB zb, C3737uO uOVar, C6760atE ate) {
        this.czU = zb;
        this.czV = uOVar;
    }

    public void destroy() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.czT.size()) {
                C0783LL cFg = this.czT.get(i2).cFg();
                if (cFg != null) {
                    aDn().ajX().mo21065b(cFg, this.czU);
                    aDn().mo7771c(cFg, this.czU);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    public void mo17706a(ayY ayy) {
        mo17707a(ayy, 1, 1);
    }

    /* renamed from: a */
    public void mo17707a(ayY ayy, short s, short s2) {
        this.stack.bcF();
        try {
            if (f6422jR || !this.czT.contains(ayy)) {
                this.czT.add(ayy);
                C3978xf d = this.stack.bcJ().mo1157d(ayy.cFf());
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                ayy.cEZ().getAabb(d, vec3f, vec3f2);
                ayy.mo17015c(aDn().mo7767a(vec3f, vec3f2, ayy.cEZ().arV(), ayy, s, s2, this.czU));
                return;
            }
            throw new AssertionError();
        } finally {
            this.stack.bcG();
        }
    }

    public void aDm() {
        C0128Bb.m1253gf("performDiscreteCollisionDetection");
        try {
            aDr();
            this.czV.mo7769a(this.czU);
            C1701ZB aDp = aDp();
            C0128Bb.m1253gf("dispatchAllCollisionPairs");
            if (aDp != null) {
                aDp.mo586a(this.czV.ajX(), this.cnP, this.czU);
            }
            C0128Bb.blS();
        } catch (Throwable th) {
            throw th;
        } finally {
            C0128Bb.blS();
        }
    }

    /* renamed from: b */
    public void mo17719b(ayY ayy) {
        C0783LL cFg = ayy.cFg();
        if (cFg != null) {
            aDn().ajX().mo21065b(cFg, this.czU);
            aDn().mo7771c(cFg, this.czU);
            ayy.mo17015c((C0783LL) null);
        }
        this.czT.remove(ayy);
    }

    public C3737uO aDn() {
        return this.czV;
    }

    public C3180oo aDo() {
        return this.czV.ajX();
    }

    public C1701ZB aDp() {
        return this.czU;
    }

    public C5408aIe aDq() {
        return this.cnP;
    }

    public void aDr() {
        C0128Bb.m1253gf("updateAabbs");
        this.stack.bcF();
        try {
            C3978xf xfVar = (C3978xf) this.stack.bcJ().get();
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            for (int i = 0; i < this.czT.size(); i++) {
                ayY ayy = this.czT.get(i);
                if (ayy.isActive()) {
                    ayy.cEZ().getAabb(ayy.cFf(), vec3f, vec3f2);
                    C3737uO uOVar = this.czV;
                    vec3f3.sub(vec3f2, vec3f);
                    if (ayy.cEV() || vec3f3.lengthSquared() < 1.0E12f) {
                        uOVar.mo7768a(ayy.cFg(), vec3f, vec3f2, this.czU);
                    } else {
                        ayy.mo17050wv(5);
                        if (czW && this.f6423Ux != null) {
                            czW = false;
                            this.f6423Ux.mo4810gj("Overflow in AABB, object removed from simulation");
                            this.f6423Ux.mo4810gj("If you can reproduce this, please email bugs@continuousphysics.com\n");
                            this.f6423Ux.mo4810gj("Please include above information, your Platform, version of OS.\n");
                            this.f6423Ux.mo4810gj("Thanks.\n");
                        }
                    }
                }
            }
        } finally {
            this.stack.bcG();
            C0128Bb.blS();
        }
    }

    public C1072Pg aDs() {
        return this.f6423Ux;
    }

    /* renamed from: a */
    public void mo17705a(C1072Pg pg) {
        this.f6423Ux = pg;
    }

    public int aDt() {
        return this.czT.size();
    }

    /* renamed from: a */
    public void mo17708a(C3978xf xfVar, C3978xf xfVar2, ayY ayy, C0802Lc lc, C3978xf xfVar3, C2227b bVar, short s) {
        this.stack.bcF();
        try {
            C5804aak aak = new C5804aak(0.0f);
            aak.setMargin(0.0f);
            if (lc.aiF()) {
                C5915acr.C1866a aVar = new C5915acr.C1866a();
                aVar.fkF = bVar.f6428nL;
                if (new C5253aCf(aak, (azD) lc, new C0327ES()).mo8215a(xfVar, xfVar2, xfVar3, xfVar3, aVar) && aVar.f4276Oj.lengthSquared() > 1.0E-4f && aVar.fkF < bVar.f6428nL) {
                    xfVar.bFF.transform(aVar.f4276Oj);
                    aVar.f4276Oj.normalize();
                    bVar.mo17721a(new C2226a(ayy, (C2231f) null, aVar.f4276Oj, aVar.fkF), true);
                }
            } else if (lc.aiG()) {
                if (lc.arV() == C3655tK.TRIANGLE_MESH_SHAPE_PROXYTYPE) {
                    C3191ow owVar = (C3191ow) lc;
                    C3978xf xfVar4 = (C3978xf) this.stack.bcJ().get();
                    xfVar4.mo22952b(xfVar3);
                    Vec3f ac = this.stack.bcH().mo4458ac(xfVar.bFG);
                    xfVar4.mo22946G(ac);
                    Vec3f ac2 = this.stack.bcH().mo4458ac(xfVar2.bFG);
                    xfVar4.mo22946G(ac2);
                    C2230e eVar = new C2230e(ac, ac2, bVar, ayy, owVar);
                    eVar.f9100r = bVar.f6428nL;
                    owVar.mo21096a((C3541sM) eVar, ac, ac2);
                } else {
                    C1468Vc vc = (C1468Vc) lc;
                    C3978xf xfVar5 = (C3978xf) this.stack.bcJ().get();
                    xfVar5.mo22952b(xfVar3);
                    Vec3f ac3 = this.stack.bcH().mo4458ac(xfVar.bFG);
                    xfVar5.mo22946G(ac3);
                    Vec3f ac4 = this.stack.bcH().mo4458ac(xfVar2.bFG);
                    xfVar5.mo22946G(ac4);
                    C2230e eVar2 = new C2230e(ac3, ac4, bVar, ayy, vc);
                    eVar2.f9100r = bVar.f6428nL;
                    Vec3f ac5 = this.stack.bcH().mo4458ac(ac3);
                    C0647JL.m5605o(ac5, ac4);
                    Vec3f ac6 = this.stack.bcH().mo4458ac(ac3);
                    C0647JL.m5606p(ac6, ac4);
                    vc.mo2381a(eVar2, ac5, ac6);
                }
            } else if (lc.isCompound()) {
                C6651aqz aqz = (C6651aqz) lc;
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= aqz.crs()) {
                        break;
                    }
                    C3978xf d = this.stack.bcJ().mo1157d(aqz.mo15688tJ(i2));
                    C0802Lc tI = aqz.mo15687tI(i2);
                    C3978xf d2 = this.stack.bcJ().mo1157d(xfVar3);
                    d2.mo22954c(d);
                    mo17708a(xfVar, xfVar2, ayy, tI, d2, bVar, s);
                    i = i2 + 1;
                }
            }
        } finally {
            this.stack.bcG();
        }
    }

    /* renamed from: a */
    public void mo17709a(Vec3f vec3f, Vec3f vec3f2, C2227b bVar) {
        mo17710a(vec3f, vec3f2, bVar, -1);
    }

    /* renamed from: a */
    public void mo17710a(Vec3f vec3f, Vec3f vec3f2, C2227b bVar, short s) {
        this.stack.bcF();
        try {
            C3978xf xfVar = (C3978xf) this.stack.bcJ().get();
            C3978xf xfVar2 = (C3978xf) this.stack.bcJ().get();
            xfVar.setIdentity();
            xfVar.bFG.set(vec3f);
            xfVar2.setIdentity();
            xfVar2.bFG.set(vec3f2);
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            float[] fArr = new float[1];
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.czT.size()) {
                    if (bVar.f6428nL == 0.0f) {
                        break;
                    }
                    ayY ayy = this.czT.get(i2);
                    if ((ayy.cFg().dxz & s) != 0) {
                        ayy.cEZ().getAabb(ayy.cFf(), vec3f3, vec3f4);
                        fArr[0] = bVar.f6428nL;
                        if (aRO.m17756a(vec3f, vec3f2, vec3f3, vec3f4, fArr, (Vec3f) this.stack.bcH().get())) {
                            mo17708a(xfVar, xfVar2, ayy, ayy.cEZ(), ayy.cFf(), bVar, -1);
                        }
                    }
                    i = i2 + 1;
                } else {
                    break;
                }
            }
        } finally {
            this.stack.bcG();
        }
    }

    public List<ayY> aDu() {
        return this.czT;
    }

    /* renamed from: a.d$f */
    /* compiled from: a */
    public static class C2231f {
        public int dQb;
        public int fRD;
    }

    /* renamed from: a.d$a */
    public static class C2226a {

        /* renamed from: q */
        public final Vec3f f6426q = new Vec3f();
        /* renamed from: o */
        public ayY f6424o;
        /* renamed from: p */
        public C2231f f6425p;
        /* renamed from: r */
        public float f6427r;

        public C2226a(ayY ayy, C2231f fVar, Vec3f vec3f, float f) {
            this.f6424o = ayy;
            this.f6425p = fVar;
            this.f6426q.set(vec3f);
            this.f6427r = f;
        }
    }

    /* renamed from: a.d$b */
    /* compiled from: a */
    public static abstract class C2227b {

        /* renamed from: nL */
        public float f6428nL = 1.0f;

        /* renamed from: o */
        public ayY f6429o;

        /* renamed from: a */
        public abstract float mo17721a(C2226a aVar, boolean z);

        /* renamed from: gp */
        public boolean mo17722gp() {
            return this.f6429o != null;
        }
    }

    /* renamed from: a.d$c */
    /* compiled from: a */
    public static class C2228c extends C2227b {

        /* renamed from: jR */
        static final /* synthetic */ boolean f6430jR = (!C2225d.class.desiredAssertionStatus());
        public final Vec3f akr = new Vec3f();
        public final Vec3f aks = new Vec3f();
        public final Vec3f akt = new Vec3f();
        public final Vec3f aku = new Vec3f();

        public C2228c(Vec3f vec3f, Vec3f vec3f2) {
            this.akr.set(vec3f);
            this.aks.set(vec3f2);
        }

        /* renamed from: a */
        public float mo17721a(C2226a aVar, boolean z) {
            if (f6430jR || aVar.f6427r <= this.f6428nL) {
                this.f6428nL = aVar.f6427r;
                this.f6429o = aVar.f6424o;
                if (z) {
                    this.akt.set(aVar.f6426q);
                } else {
                    this.akt.set(aVar.f6426q);
                    this.f6429o.cFf().bFF.transform(this.akt);
                }
                C0647JL.m5591a(this.aku, this.akr, this.aks, aVar.f6427r);
                return aVar.f6427r;
            }
            throw new AssertionError();
        }
    }

    /* renamed from: a.d$h */
    /* compiled from: a */
    public static class C2233h {
        public final Vec3f iqc = new Vec3f();
        /* renamed from: q */
        public final Vec3f f6435q = new Vec3f();
        public ayY deJ;
        /* renamed from: p */
        public C2231f f6434p;
        /* renamed from: r */
        public float f6436r;

        public C2233h(ayY ayy, C2231f fVar, Vec3f vec3f, Vec3f vec3f2, float f) {
            this.deJ = ayy;
            this.f6434p = fVar;
            this.f6435q.set(vec3f);
            this.iqc.set(vec3f2);
            this.f6436r = f;
        }
    }

    /* renamed from: a.d$g */
    /* compiled from: a */
    public static abstract class C2232g {

        /* renamed from: nL */
        public float f6433nL = 1.0f;

        /* renamed from: a */
        public abstract float mo17723a(C2233h hVar, boolean z);

        /* renamed from: gp */
        public boolean mo17725gp() {
            return this.f6433nL < 1.0f;
        }
    }

    /* renamed from: a.d$d */
    /* compiled from: a */
    public static class C2229d extends C2232g {

        /* renamed from: jR */
        static final /* synthetic */ boolean f6431jR = (!C2225d.class.desiredAssertionStatus());
        public final Vec3f akt = new Vec3f();
        public final Vec3f aku = new Vec3f();
        public final Vec3f deH = new Vec3f();
        public final Vec3f deI = new Vec3f();
        public ayY deJ;

        /* renamed from: a */
        public float mo17723a(C2233h hVar, boolean z) {
            if (f6431jR || hVar.f6436r <= this.f6433nL) {
                this.f6433nL = hVar.f6436r;
                this.deJ = hVar.deJ;
                if (z) {
                    this.akt.set(hVar.f6435q);
                } else {
                    this.akt.set(hVar.f6435q);
                    this.deJ.cFf().bFF.transform(this.akt);
                }
                this.aku.set(hVar.iqc);
                return hVar.f6436r;
            }
            throw new AssertionError();
        }
    }

    /* renamed from: a.d$e */
    /* compiled from: a */
    private static class C2230e extends C3541sM {
        public C2227b dHJ;
        public C1468Vc dHK;

        /* renamed from: o */
        public ayY f6432o;

        public C2230e(Vec3f vec3f, Vec3f vec3f2, C2227b bVar, ayY ayy, C1468Vc vc) {
            super(vec3f, vec3f2);
            this.dHJ = bVar;
            this.f6432o = ayy;
            this.dHK = vc;
        }

        /* renamed from: a */
        public float mo17724a(Vec3f vec3f, float f, int i, int i2) {
            C2231f fVar = new C2231f();
            fVar.fRD = i;
            fVar.dQb = i2;
            return this.dHJ.mo17721a(new C2226a(this.f6432o, fVar, vec3f, f), false);
        }
    }
}
