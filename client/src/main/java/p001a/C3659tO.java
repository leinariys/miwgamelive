package p001a;

import gnu.trove.THashSet;
import logic.res.html.DataClassSerializer;

import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Set;

/* renamed from: a.tO */
/* compiled from: a */
public class C3659tO<E> extends THashSet<E> implements Set<E> {
    public C3659tO() {
    }

    public C3659tO(Set<E> set) {
        super(set);
    }

    public C3659tO(int i) {
        super(i);
    }

    /* renamed from: a */
    public void mo22222a(ObjectInput objectInput, int i, DataClassSerializer kd) {
        int readInt = objectInput.readInt();
        if (this._set.length != readInt) {
            this._set = new Object[readInt];
        }
        clear();
        Object[] objArr = this._set;
        int length = objArr.length;
        while (true) {
            int i2 = length - 1;
            if (length <= 0) {
                break;
            }
            objArr[i2] = FREE;
            length = i2;
        }
        this._size = i;
        int i3 = 0;
        int i4 = i;
        while (i4 > 0) {
            int readInt2 = objectInput.readInt();
            if ((Integer.MIN_VALUE & readInt2) != 0) {
                objArr[readInt2 & Integer.MAX_VALUE] = REMOVED;
            } else {
                objArr[readInt2] = kd.inputReadObject(objectInput);
                i4--;
            }
            i3++;
        }
        int capacity = capacity();
        this._maxSize = Math.min(capacity - 1, (int) Math.floor((double) (((float) capacity) * this._loadFactor)));
        this._free = capacity - this._size;
    }

    /* renamed from: a */
    public void mo22223a(ObjectOutput objectOutput, DataClassSerializer kd) {
        Object[] objArr = this._set;
        objectOutput.writeInt(this._set.length);
        int length = objArr.length;
        while (true) {
            int i = length - 1;
            if (length > 0) {
                if (objArr[i] != FREE) {
                    if (objArr[i] == REMOVED) {
                        objectOutput.writeInt(Integer.MIN_VALUE | i);
                        length = i;
                    } else {
                        objectOutput.writeInt(i);
                        kd.serializeData(objectOutput, objArr[i]);
                    }
                }
                length = i;
            } else {
                return;
            }
        }
    }
}
