package p001a;

import game.geometry.Matrix3fWrap;
import logic.res.sound.C0907NJ;
import logic.ui.item.Component3d;
import logic.ui.item.InternalFrame;
import taikodom.addon.IAddonProperties;
import taikodom.addon.map3d.Map3DAddon;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.RGroup;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

import java.awt.*;
import java.awt.event.*;

/* renamed from: a.KV */
/* compiled from: a */
public class C0730KV {
    private static /* synthetic */ int[] drN;
    /* access modifiers changed from: private */
    public Component3d drB;
    /* access modifiers changed from: private */
    public RGroup drL;
    /* access modifiers changed from: private */
    public RGroup drM;
    private Map3DAddon drA;
    private C6499aoD drC;
    private Matrix3fWrap drD = new Matrix3fWrap();
    private Matrix3fWrap drE = new Matrix3fWrap();
    private Point drF;
    private boolean drG;
    private Matrix3fWrap drH;
    private Matrix3fWrap drI;
    private Matrix3fWrap drJ;
    private RGroup drK;
    /* renamed from: kj */
    private IAddonProperties f973kj;

    /* renamed from: nM */
    private InternalFrame f974nM;

    public C0730KV(Map3DAddon map3DAddon) {
        this.drA = map3DAddon;
        this.f973kj = map3DAddon.bUA();
        this.f974nM = map3DAddon.aRY();
        this.drB = this.f974nM.mo4915cd("mapViewer");
        bdM();
        m6420b(this.f973kj);
    }

    static /* synthetic */ int[] bdR() {
        int[] iArr = drN;
        if (iArr == null) {
            iArr = new int[C6785atd.values().length];
            try {
                iArr[C6785atd.INSIDE_NODE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C6785atd.NODE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C6785atd.SYSTEM.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            drN = iArr;
        }
        return iArr;
    }

    private void bdM() {
        this.f974nM.mo4915cd("view2D").addActionListener(new C0731a());
        this.f974nM.mo4915cd("view3D").addActionListener(new C0733c());
    }

    /* renamed from: b */
    private void m6420b(IAddonProperties vWVar) {
        this.drH = new Matrix3fWrap();
        this.drH.setIdentity();
        this.drH.rotateX((float) Math.toRadians(90.0d));
        this.drI = new Matrix3fWrap();
        this.drI.setIdentity();
        this.drI.rotateX((float) Math.toRadians(45.0d));
        this.drJ = new Matrix3fWrap();
        this.drJ.setIdentity();
        this.drJ.rotateY((float) Math.toRadians(45.0d));
        this.drK = new RGroup();
        this.drL = new RGroup();
        this.drK.addChild(this.drL);
        this.drC = new C6499aoD(20, 20, 10.0f);
        this.drL.addChild(this.drC);
        this.drB.getScene().addChild(this.drL);
        vWVar.getEngineGame().getLoaderTrail().mo4995a("data/scene/nod_spa_earth.pro", "nod_spa_earth", (Scene) null, new C0734d(), getClass().getSimpleName());
        vWVar.getEngineGame().getLoaderTrail().mo4995a("data/models/ast_cri_aster/ast_cri_aster01_01.pro", "ast_cri_aster01_01", (Scene) null, new C0735e(), getClass().getSimpleName());
        this.drB.getScene().addChild(this.drC);
        this.drB.addMouseListener(new C0736f());
        this.drB.addMouseMotionListener(new C0732b());
        bdO();
    }

    public boolean bdN() {
        return this.drG;
    }

    /* renamed from: cV */
    public void mo3554cV(boolean z) {
        this.drG = z;
    }

    /* renamed from: a */
    public void mo3551a(Point point) {
        this.drF = point;
    }

    /* renamed from: b */
    public void mo3552b(Point point) {
        float f = this.drL.getTransform().orientation.m22;
        float f2 = this.drL.getTransform().orientation.m12;
        double degrees = Math.toDegrees(Math.acos((double) f));
        int i = this.drF.y - point.y;
        if (degrees > 90.0d && f2 < 0.0f && i < 0) {
            return;
        }
        if (degrees <= 90.0d || f2 <= 0.0f || i <= 0) {
            this.drE.setIdentity();
            this.drE.rotateX(((float) (-i)) * 0.017453292f);
            this.drL.getTransform().orientation.mul(this.drE);
            int i2 = this.drF.x - point.x;
            this.drD.setIdentity();
            this.drD.rotateY(((float) (-i2)) * 0.017453292f);
            this.drK.getTransform().orientation.mul(this.drD);
            this.drK.updateInternalGeometry((SceneObject) null);
            this.drF = point;
        }
    }

    /* access modifiers changed from: private */
    public void bdO() {
        this.drL.getTransform().orientation.setIdentity();
        this.drL.getTransform().orientation.mul(this.drI);
        this.drL.getTransform().orientation.mul(this.drJ);
        this.drL.updateInternalGeometry((SceneObject) null);
    }

    /* access modifiers changed from: private */
    public void bdP() {
        this.drK.resetRotation();
        this.drL.getTransform().orientation.set(this.drH);
        this.drK.updateInternalGeometry((SceneObject) null);
    }

    private void bdQ() {
        switch (bdR()[this.drA.bUB().bsm().mo17807kp().ordinal()]) {
        }
    }

    /* renamed from: a.KV$a */
    class C0731a implements ActionListener {
        C0731a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C0730KV.this.bdP();
        }
    }

    /* renamed from: a.KV$c */
    /* compiled from: a */
    class C0733c implements ActionListener {
        C0733c() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C0730KV.this.bdO();
        }
    }

    /* renamed from: a.KV$d */
    /* compiled from: a */
    class C0734d implements C0907NJ {
        C0734d() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            C0730KV.this.drB.getScene().addChild((SceneObject) renderAsset);
        }
    }

    /* renamed from: a.KV$e */
    /* compiled from: a */
    class C0735e implements C0907NJ {
        C0735e() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            C0730KV.this.drM = (RGroup) renderAsset;
            C0730KV.this.drM.setScaling(0.1f);
            C0730KV.this.drL.addChild(C0730KV.this.drM);
        }
    }

    /* renamed from: a.KV$f */
    /* compiled from: a */
    class C0736f extends MouseAdapter {
        C0736f() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            if (mouseEvent.getButton() == 3) {
                C0730KV.this.mo3554cV(true);
                C0730KV.this.mo3551a(mouseEvent.getPoint());
            }
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            if (mouseEvent.getButton() == 3) {
                C0730KV.this.mo3554cV(false);
            }
        }
    }

    /* renamed from: a.KV$b */
    /* compiled from: a */
    class C0732b extends MouseMotionAdapter {
        C0732b() {
        }

        public void mouseDragged(MouseEvent mouseEvent) {
            if (C0730KV.this.bdN()) {
                C0730KV.this.mo3552b(mouseEvent.getPoint());
            }
        }
    }
}
