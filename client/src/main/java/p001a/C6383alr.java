package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.alr  reason: case insensitive filesystem */
/* compiled from: a */
public class C6383alr {
    public final Vec3f angularVelocity = new Vec3f();
    public final Vec3f fRu = new Vec3f();
    public final Vec3f fWZ = new Vec3f();
    public final C0763Kt stack = C0763Kt.bcE();
    public float cZg;
    public float dMd;
    public float fWX;
    public C6238ajC fWY;

    /* renamed from: n */
    public void mo14744n(Vec3f vec3f, Vec3f vec3f2) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            vec3f3.cross(this.angularVelocity, vec3f);
            vec3f2.add(this.fRu, vec3f3);
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: b */
    public void mo14741b(Vec3f vec3f, Vec3f vec3f2, float f) {
        this.fRu.scaleAdd(f, vec3f, this.fRu);
        this.angularVelocity.scaleAdd(this.cZg * f, vec3f2, this.angularVelocity);
    }

    public void chQ() {
        if (this.fWX != 0.0f) {
            this.fWY.mo13894O(this.fRu);
            this.fWY.setAngularVelocity(this.angularVelocity);
        }
    }

    public void chR() {
        if (this.fWX != 0.0f) {
            this.fRu.set(this.fWY.cev());
            this.angularVelocity.set(this.fWY.getAngularVelocity());
        }
    }
}
