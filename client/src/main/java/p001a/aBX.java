package p001a;

import logic.res.XmlNode;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.aBX */
/* compiled from: a */
public class aBX {
    public static List<aNY> cMP() {
        File file;
        ArrayList arrayList = new ArrayList();
        String property = System.getProperty("physics.resources.rootpath");
        if (property == null) {
            file = new File("res/shots/" + "shotfx_size.xml");
        } else {
            file = new File(property, "/res/shots/" + "shotfx_size.xml");
        }
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            for (XmlNode next : new LoaderFileXML().loadFileXml((InputStream) fileInputStream, "UTF-8").mo9057q("ShotFX", (String) null, (String) null)) {
                arrayList.add(new aNY(next.findNodeChildTagDepth("Name").getAttribute("value"), Float.parseFloat(next.findNodeChildTagDepth("Length").getAttribute("value"))));
            }
            fileInputStream.close();
            return arrayList;
        } catch (IOException e) {
            throw new C6719asP(file.getName());
        }
    }
}
