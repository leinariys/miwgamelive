package p001a;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C6623aqX;
import game.script.item.*;
import logic.baa.aDJ;
import logic.ui.ComponentManager;
import logic.ui.IBaseUiTegXml;
import logic.ui.item.InternalFrame;
import logic.ui.item.ListJ;
import taikodom.addon.C6144ahM;
import taikodom.addon.components.TItemPanel;

import javax.swing.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;

/* renamed from: a.mz */
/* compiled from: a */
public class C3016mz extends TransferHandler {
    private static final long serialVersionUID = 6730774454632290041L;
    /* access modifiers changed from: private */
    public ItemLocation bLj;
    /* access modifiers changed from: private */
    public TItemPanel bLk;

    public C3016mz(TItemPanel tItemPanel) {
        this.bLk = tItemPanel;
    }

    public ItemLocation apL() {
        return this.bLj;
    }

    /* renamed from: b */
    public void mo20634b(ItemLocation aag) {
        this.bLj = aag;
    }

    public boolean canImport(TransferHandler.TransferSupport transferSupport) {
        if (!transferSupport.isDataFlavorSupported(DataFlavor.stringFlavor)) {
            return false;
        }
        transferSupport.setShowDropLocation(false);
        if (transferSupport.getDropLocation().getIndex() == -1) {
            return false;
        }
        return true;
    }

    public boolean importData(TransferHandler.TransferSupport transferSupport) {
        if (this.bLj == null) {
            return false;
        }
        if (!transferSupport.isDrop()) {
            return false;
        }
        if (!transferSupport.isDataFlavorSupported(C2125cH.f6026vP)) {
            return false;
        }
        try {
            Collection<aDJ> collection = (Collection) transferSupport.getTransferable().getTransferData(C2125cH.f6026vP);
            ListJ component = transferSupport.getComponent();
            int locationToIndex = component.locationToIndex(transferSupport.getDropLocation().getDropPoint());
            Item auq = null;
            if (locationToIndex >= 0) {
                auq = ((C1447VJ) component.getModel().getElementAt(locationToIndex)).mo5990XH();
            }
            for (aDJ adj : collection) {
                if (adj instanceof Item) {
                    Item auq2 = (Item) adj;
                    if (auq2.bNh() != this.bLj) {
                        this.bLk.mo4911Kk();
                        ((ItemLocation) C3582se.m38985a(this.bLj, (C6144ahM<?>) new C3022d())).mo2693s(auq2);
                    } else if (auq != null) {
                        if (!auq2.equals(auq) && (auq2 instanceof BulkItem) && (auq instanceof BulkItem) && ((BulkItem) auq2).bAP().equals(((BulkItem) auq).bAP())) {
                            ((BulkItem) auq).mo11562c((BulkItem) auq2);
                        }
                    }
                }
                if (adj instanceof ItemType) {
                    if (C5916acs.getSingolton().getTaikodom().aLS().ads()) {
                        if (adj instanceof BulkItemType) {
                            m35910a((BulkItemType) adj);
                        } else {
                            this.bLk.mo4911Kk();
                            ((ItemLocation) C3582se.m38985a(this.bLj, (C6144ahM<?>) new C3019c())).mo2695u((ItemType) adj);
                        }
                    }
                }
            }
            IBaseUiTegXml.initBaseUItegXML().mo13724cx(ComponentManager.getCssHolder(this.bLk).mo13049Vr().auc());
        } catch (C2293dh e) {
            this.bLj.ald().getEventManager().mo13975h(new C6623aqX(C0939Nn.C0940a.INFO, e.mo17902ju(), new Object[0]));
            IBaseUiTegXml.initBaseUItegXML().mo13724cx(ComponentManager.getCssHolder(this.bLk).mo13049Vr().aud());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return true;
    }

    public int getSourceActions(JComponent jComponent) {
        return 2;
    }

    /* access modifiers changed from: protected */
    public Transferable createTransferable(JComponent jComponent) {
        try {
            Object[] selectedValues = ((JList) jComponent).getSelectedValues();
            ArrayList arrayList = new ArrayList(selectedValues.length);
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= selectedValues.length) {
                    IBaseUiTegXml.initBaseUItegXML().mo13724cx(ComponentManager.getCssHolder(this.bLk).mo13049Vr().aue());
                    return new C5569aOj((Item[]) arrayList.toArray(new Item[arrayList.size()]));
                }
                arrayList.add(((C1447VJ) selectedValues[i2]).mo5990XH());
                i = i2 + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void exportDone(JComponent jComponent, Transferable transferable, int i) {
    }

    /* renamed from: a */
    private void m35910a(BulkItemType amh) {
        InternalFrame d = IBaseUiTegXml.initBaseUItegXML().createJComponent(TItemPanel.class, "quantityPanel.xml");
        d.pack();
        d.center();
        d.setVisible(true);
        JSpinner cd = d.mo4915cd("amount");
        cd.setModel(new SpinnerNumberModel(1, 1, amh.mo13880dX(), 1));
        d.mo4913cb("cancel").addActionListener(new C3018b(d));
        d.mo4913cb("confirm").addActionListener(new C3017a(cd, amh, d));
    }

    /* renamed from: a.mz$d */
    /* compiled from: a */
    class C3022d implements C6144ahM<Item> {
        C3022d() {
        }

        /* renamed from: o */
        public void mo1931n(Item auq) {
            SwingUtilities.invokeLater(new C3023a());
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            SwingUtilities.invokeLater(new C3024b(th));
            IBaseUiTegXml.initBaseUItegXML().mo13724cx(ComponentManager.getCssHolder(C3016mz.this.bLk).mo13049Vr().aud());
        }

        /* renamed from: a.mz$d$a */
        class C3023a implements Runnable {
            C3023a() {
            }

            public void run() {
                C3016mz.this.bLk.mo4912Kl();
                C3016mz.this.bLk.repaint(50);
            }
        }

        /* renamed from: a.mz$d$b */
        /* compiled from: a */
        class C3024b implements Runnable {
            private final /* synthetic */ Throwable dUm;

            C3024b(Throwable th) {
                this.dUm = th;
            }

            public void run() {
                C3016mz.this.bLk.mo4912Kl();
                C3016mz.this.bLj.ald().getEventManager().mo13975h(new C6623aqX(C0939Nn.C0940a.INFO, ((C2293dh) this.dUm).mo17902ju(), new Object[0]));
            }
        }
    }

    /* renamed from: a.mz$c */
    /* compiled from: a */
    class C3019c implements C6144ahM<Item> {
        C3019c() {
        }

        /* renamed from: o */
        public void mo1931n(Item auq) {
            SwingUtilities.invokeLater(new C3021b());
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            SwingUtilities.invokeLater(new C3020a(th));
            IBaseUiTegXml.initBaseUItegXML().mo13724cx(ComponentManager.getCssHolder(C3016mz.this.bLk).mo13049Vr().aud());
        }

        /* renamed from: a.mz$c$b */
        /* compiled from: a */
        class C3021b implements Runnable {
            C3021b() {
            }

            public void run() {
                C3016mz.this.bLk.mo4912Kl();
                C3016mz.this.bLk.repaint(50);
            }
        }

        /* renamed from: a.mz$c$a */
        class C3020a implements Runnable {
            private final /* synthetic */ Throwable dUm;

            C3020a(Throwable th) {
                this.dUm = th;
            }

            public void run() {
                C3016mz.this.bLk.mo4912Kl();
                C3016mz.this.bLj.ald().getEventManager().mo13975h(new C6623aqX(C0939Nn.C0940a.INFO, ((C2293dh) this.dUm).mo17902ju(), new Object[0]));
            }
        }
    }

    /* renamed from: a.mz$b */
    /* compiled from: a */
    class C3018b implements ActionListener {
        private final /* synthetic */ InternalFrame aDu;

        C3018b(InternalFrame nxVar) {
            this.aDu = nxVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            this.aDu.destroy();
        }
    }

    /* renamed from: a.mz$a */
    class C3017a implements ActionListener {
        private final /* synthetic */ JSpinner aDs;
        private final /* synthetic */ BulkItemType aDt;
        private final /* synthetic */ InternalFrame aDu;

        C3017a(JSpinner jSpinner, BulkItemType amh, InternalFrame nxVar) {
            this.aDs = jSpinner;
            this.aDt = amh;
            this.aDu = nxVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            try {
                this.aDt.mo13879b(C3016mz.this.bLj, ((Integer) this.aDs.getModel().getValue()).intValue());
            } catch (C2293dh e) {
            }
            this.aDu.destroy();
        }
    }
}
