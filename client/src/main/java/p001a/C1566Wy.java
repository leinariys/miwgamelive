package p001a;

import game.network.message.externalizable.aCE;
import game.script.Character;
import logic.baa.C1616Xf;
import logic.baa.aDJ;
import logic.res.html.C2491fm;
import logic.res.html.MessageContainer;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/* renamed from: a.Wy */
/* compiled from: a */
public class C1566Wy implements C6402amK {
    private C2491fm exI;
    private C2491fm exJ;
    private C1616Xf exK;
    private String exL;
    private List<C2491fm> exM = MessageContainer.init();
    private C5437aJh exN;
    private C2491fm exO;
    private C2491fm exP;
    private C2491fm exQ;
    private List<C2491fm> exR = MessageContainer.init();
    private C2491fm exS;

    public C1566Wy(C5437aJh ajh, C1616Xf xf, String str) {
        this.exK = xf;
        this.exL = str;
        this.exN = ajh;
    }

    public Object getValue() {
        if (!bDC()) {
            return null;
        }
        try {
            return this.exN.mo7471b(this.exK, new aCE(this.exK, this.exI));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setValue(Object obj) {
        aCE ace;
        if (this.exJ != null) {
            Class cls = this.exJ.getParameterTypes()[0];
            if (obj != null && !cls.isInstance(obj) && !m11365a(obj.getClass(), cls)) {
                Iterator<C2491fm> it = this.exM.iterator();
                while (true) {
                    if (it.hasNext()) {
                        C2491fm next = it.next();
                        if (next.getParameterTypes()[0].isInstance(obj)) {
                            ace = new aCE(this.exK, next, new Object[]{obj});
                            break;
                        }
                    } else {
                        ace = null;
                        break;
                    }
                }
            } else {
                ace = new aCE(this.exK, this.exJ, new Object[]{obj});
            }
            if (ace != null) {
                this.exN.mo7473c(this.exK, ace);
            }
        }
    }

    /* renamed from: n */
    public aDJ mo6661n(Class<? extends aDJ> cls) {
        C2491fm fmVar;
        if (bDC()) {
            fmVar = this.exJ;
        } else if (bDB()) {
            fmVar = this.exO;
        } else {
            mo6662pF();
            fmVar = null;
        }
        if (fmVar != null) {
            return this.exN.mo7472c(this.exK, fmVar, cls);
        }
        return null;
    }

    /* renamed from: b */
    public void mo6637b(Class<? extends aDJ> cls, int i) {
        C2491fm fmVar = null;
        if (bDC()) {
            fmVar = this.exJ;
        } else if (bDB()) {
            fmVar = this.exO;
        } else {
            mo6662pF();
        }
        if (fmVar != null) {
            this.exN.mo7474c(this.exK, fmVar, cls, i);
        }
    }

    /* renamed from: a */
    private boolean m11365a(Class<?> cls, Class<?> cls2) {
        Class<Integer> cls3;
        if (!cls2.isPrimitive()) {
            return false;
        }
        if (cls2 == Integer.TYPE) {
            cls3 = Integer.class;
        } else if (cls2 == Float.TYPE) {
            cls3 = Float.class;
        } else if (cls2 == Double.TYPE) {
            cls3 = Double.class;
        } else if (cls2 == Long.TYPE) {
            cls3 = Long.class;
        } else if (cls2 == Byte.TYPE) {
            cls3 = Byte.class;
        } else if (cls2 == Character.TYPE) {
            cls3 = Character.class;
        } else if (cls2 == Short.TYPE) {
            cls3 = Short.class;
        } else {
            cls3 = cls2;
            if (cls2 == Boolean.TYPE) {
                cls3 = Boolean.class;
            }
        }
        return cls.equals(cls3);
    }

    /* renamed from: a */
    public void mo6635a(C2491fm fmVar) {
        this.exJ = fmVar;
    }

    /* renamed from: b */
    public void mo6636b(C2491fm fmVar) {
        this.exM.add(fmVar);
    }

    /* renamed from: c */
    public void mo6648c(C2491fm fmVar) {
        this.exI = fmVar;
    }

    public String getName() {
        return this.exL;
    }

    public Class<?> getType() {
        Class<?>[] parameterTypes;
        Class<?>[] parameterTypes2;
        if (bDC()) {
            if (this.exJ != null && (parameterTypes2 = this.exJ.getParameterTypes()) != null && parameterTypes2.length > 0) {
                return parameterTypes2[0];
            }
            if (this.exI != null) {
                return this.exI.getReturnType();
            }
        } else if (bDB() && this.exO != null && (parameterTypes = this.exO.getParameterTypes()) != null && parameterTypes.length >= 1) {
            return parameterTypes[0];
        }
        return null;
    }

    public C2491fm bDt() {
        return this.exI;
    }

    public C2491fm bDu() {
        return this.exJ;
    }

    /* renamed from: X */
    public boolean mo6633X(Object obj) {
        if (getType().isInstance(obj)) {
            return true;
        }
        for (C2491fm parameterTypes : this.exM) {
            if (parameterTypes.getParameterTypes()[0].isInstance(obj)) {
                return true;
            }
        }
        return false;
    }

    public boolean isReadOnly() {
        if (bDC()) {
            if (this.exJ == null) {
                return true;
            }
            return false;
        } else if (!bDB()) {
            return false;
        } else {
            if ((this.exO != null || !this.exR.isEmpty()) && this.exP != null) {
                return false;
            }
            return true;
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C1566Wy)) {
            return false;
        }
        C1566Wy wy = (C1566Wy) obj;
        if (wy.bDv() == this.exK && wy.bDt() == this.exI) {
            return true;
        }
        return false;
    }

    /* renamed from: Y */
    public void mo6634Y(Object obj) {
        aCE ace;
        if (!isReadOnly()) {
            if (obj == null || this.exO == null || !this.exO.getParameterTypes()[0].isInstance(obj)) {
                Iterator<C2491fm> it = this.exR.iterator();
                while (true) {
                    if (it.hasNext()) {
                        C2491fm next = it.next();
                        if (next.getParameterTypes()[0].isInstance(obj)) {
                            ace = new aCE(this.exK, next, new Object[]{obj});
                            break;
                        }
                    } else {
                        ace = null;
                        break;
                    }
                }
            } else {
                ace = new aCE(this.exK, this.exO, new Object[]{obj});
            }
            if (ace != null) {
                this.exN.mo7473c(this.exK, ace);
            }
        }
    }

    public Object removeValue(Object obj) {
        aCE ace;
        if (isReadOnly() || this.exP == null) {
            return null;
        }
        if (obj == null || this.exP.getParameterTypes()[0].isInstance(obj)) {
            ace = new aCE(this.exK, this.exP, new Object[]{obj});
        } else {
            ace = null;
        }
        if (ace != null) {
            return this.exN.mo7473c(this.exK, ace);
        }
        return null;
    }

    public C1616Xf bDv() {
        return this.exK;
    }

    public Class<?> bDw() {
        Class<?>[] parameterTypes;
        if (this.exO == null || (parameterTypes = this.exO.getParameterTypes()) == null || parameterTypes.length < 1) {
            return null;
        }
        return parameterTypes[0];
    }

    public boolean isEditable() {
        if (this.exO != null) {
            return this.exO.isEditable();
        }
        return false;
    }

    /* renamed from: d */
    public void mo6649d(C2491fm fmVar) {
        this.exO = fmVar;
    }

    /* renamed from: e */
    public void mo6650e(C2491fm fmVar) {
        this.exP = fmVar;
    }

    /* renamed from: f */
    public void mo6652f(C2491fm fmVar) {
        this.exQ = fmVar;
    }

    public Iterator<?> getIterator() {
        if (this.exQ == null) {
            return null;
        }
        try {
            aCE ace = new aCE(this.exK, this.exQ);
            if (this.exK == null) {
                System.err.println("Por que diabos o game.script object estÃ¡ nulo?!?!?!?!?!");
                return null;
            }
            Object a = this.exN.mo7470a(this.exK, ace, true);
            if (a == null) {
                System.err.println("Por que diabos o supposed iterator estÃ¡ nulo?!?!?!?!?!");
                return null;
            } else if (a instanceof Iterator) {
                return (Iterator) a;
            } else {
                if (a instanceof Collection) {
                    return ((Collection) a).iterator();
                }
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public C2491fm bDx() {
        return this.exO;
    }

    public C2491fm bDy() {
        return this.exP;
    }

    /* renamed from: g */
    public void mo6653g(C2491fm fmVar) {
        this.exR.add(fmVar);
    }

    public void bDz() {
        if (this.exS != null) {
            this.exN.mo7471b(this.exK, new aCE(this.exK, this.exS));
        }
    }

    /* renamed from: h */
    public void mo6658h(C2491fm fmVar) {
        this.exS = fmVar;
    }

    public C2491fm bDA() {
        return this.exS;
    }

    /* renamed from: pF */
    public boolean mo6662pF() {
        return this.exS != null;
    }

    public boolean bDB() {
        return this.exQ != null;
    }

    public boolean bDC() {
        return this.exI != null;
    }
}
