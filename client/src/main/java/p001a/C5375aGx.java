package p001a;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.aGx  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C5375aGx extends C0596IQ {
    private final JList list;

    public C5375aGx(JList jList) {
        this.list = jList;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public abstract JPopupMenu mo9137d(int i, Object[] objArr);

    /* access modifiers changed from: protected */
    /* renamed from: H */
    public void mo2812H(int i, int i2) {
        int locationToIndex = this.list.locationToIndex(new Point(i, i2));
        m15028xZ(locationToIndex);
        JPopupMenu d = mo9137d(locationToIndex, this.list.getSelectedValues());
        if (d != null) {
            d.show(this.list, i, i2);
        }
    }

    /* renamed from: xZ */
    private void m15028xZ(int i) {
        if (i != -1 && i != this.list.getSelectedIndex()) {
            this.list.setSelectedIndex(i);
        }
    }
}
