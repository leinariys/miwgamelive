package p001a;

import org.mozilla1.classfile.ClassFileWriter;
import org.mozilla1.javascript.GeneratedClassLoader;

import java.lang.ref.SoftReference;
import java.lang.reflect.UndeclaredThrowableException;
import java.security.*;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: a.SO */
/* compiled from: a */
public class C1239SO extends org.mozilla1.javascript.SecurityController {
    /* access modifiers changed from: private */
    public static final byte[] gtP = ctt();
    private static final Map<CodeSource, Map<ClassLoader, SoftReference<C1243d>>> gtQ = new WeakHashMap();

    private static byte[] ctt() {
        String name = C1243d.class.getName();
        ClassFileWriter uh = new ClassFileWriter(String.valueOf(name) + "Impl", name, "<generated>");
        uh.mo5795b(C2821kb.arR, "()V", 1);
        uh.mo5824nQ(0);
        uh.mo5794b(183, name, C2821kb.arR, "()V");
        uh.add(177);
        uh.stopMethod(1);
        uh.mo5795b("call", "(Lorg/mozilla/javascript/Callable;" + "Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Scriptable;[Ljava/lang/Object;)Ljava/lang/Object;", 17);
        for (int i = 1; i < 6; i++) {
            uh.mo5824nQ(i);
        }
        uh.mo5794b(185, "org/mozilla/javascript/Callable", "call", "(" + "Lorg/mozilla/javascript/Context;Lorg/mozilla/javascript/Scriptable;Lorg/mozilla/javascript/Scriptable;[Ljava/lang/Object;)Ljava/lang/Object;");
        uh.add(176);
        uh.stopMethod(6);
        return uh.toByteArray();
    }

    public Class<?> getStaticSecurityDomainClassInternal() {
        return CodeSource.class;
    }

    public org.mozilla1.javascript.GeneratedClassLoader createClassLoader(ClassLoader classLoader, Object obj) {
        return (C1244e) AccessController.doPrivileged(new C1242c(classLoader, obj));
    }

    public Object getDynamicSecurityDomain(Object obj) {
        return obj;
    }

    public Object callWithDomain(Object obj, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Callable mFVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr) {
        WeakHashMap weakHashMap;
        C1243d dVar;
        C1243d dVar2;
        ClassLoader classLoader = (ClassLoader) AccessController.doPrivileged(new C1240a(lhVar));
        CodeSource codeSource = (CodeSource) obj;
        synchronized (gtQ) {
            Map map = gtQ.get(codeSource);
            if (map == null) {
                WeakHashMap weakHashMap2 = new WeakHashMap();
                gtQ.put(codeSource, weakHashMap2);
                weakHashMap = weakHashMap2;
            } else {
                weakHashMap = map;
            }
        }
        synchronized (weakHashMap) {
            SoftReference softReference = (SoftReference) weakHashMap.get(classLoader);
            if (softReference != null) {
                dVar = (C1243d) softReference.get();
            } else {
                dVar = null;
            }
            if (dVar == null) {
                try {
                    C1243d dVar3 = (C1243d) AccessController.doPrivileged(new C1241b(classLoader, codeSource));
                    weakHashMap.put(classLoader, new SoftReference(dVar3));
                    dVar2 = dVar3;
                } catch (PrivilegedActionException e) {
                    throw new UndeclaredThrowableException(e.getCause());
                }
            } else {
                dVar2 = dVar;
            }
        }
        return dVar2.mo5389b(mFVar, lhVar, avf, avf2, objArr);
    }

    /* renamed from: a.SO$d */
    /* compiled from: a */
    public static abstract class C1243d {
        /* renamed from: b */
        public abstract Object mo5389b(org.mozilla1.javascript.Callable mFVar, org.mozilla1.javascript.Context lhVar, org.mozilla1.javascript.Scriptable avf, org.mozilla1.javascript.Scriptable avf2, Object[] objArr);
    }

    /* renamed from: a.SO$e */
    /* compiled from: a */
    private static class C1244e extends SecureClassLoader implements GeneratedClassLoader {
        private final CodeSource gcu;

        C1244e(ClassLoader classLoader, CodeSource codeSource) {
            super(classLoader);
            this.gcu = codeSource;
        }

        public Class<?> defineClass(String str, byte[] bArr) {
            return defineClass(str, bArr, 0, bArr.length, this.gcu);
        }

        public void linkClass(Class<?> cls) {
            resolveClass(cls);
        }
    }

    /* renamed from: a.SO$c */
    /* compiled from: a */
    class C1242c implements PrivilegedAction<Object> {
        private final /* synthetic */ ClassLoader efY;
        private final /* synthetic */ Object efZ;

        C1242c(ClassLoader classLoader, Object obj) {
            this.efY = classLoader;
            this.efZ = obj;
        }

        public Object run() {
            return new C1244e(this.efY, (CodeSource) this.efZ);
        }
    }

    /* renamed from: a.SO$a */
    class C1240a implements PrivilegedAction<Object> {
        private final /* synthetic */ org.mozilla1.javascript.Context efV;

        C1240a(org.mozilla1.javascript.Context lhVar) {
            this.efV = lhVar;
        }

        public Object run() {
            return this.efV.getApplicationClassLoader();
        }
    }

    /* renamed from: a.SO$b */
    /* compiled from: a */
    class C1241b implements PrivilegedExceptionAction<Object> {
        private final /* synthetic */ ClassLoader efW;
        private final /* synthetic */ CodeSource efX;

        C1241b(ClassLoader classLoader, CodeSource codeSource) {
            this.efW = classLoader;
            this.efX = codeSource;
        }

        public Object run() {
            return new C1244e(this.efW, this.efX).defineClass(String.valueOf(C1243d.class.getName()) + "Impl", C1239SO.gtP).newInstance();
        }
    }
}
