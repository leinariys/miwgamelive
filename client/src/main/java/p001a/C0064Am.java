package p001a;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.Am */
/* compiled from: a */
public @interface C0064Am {
    String aul() default "";

    int aum() default -1;
}
