package p001a;

import logic.baa.C1616Xf;

import java.util.Map;

/* renamed from: a.arW  reason: case insensitive filesystem */
/* compiled from: a */
public class C6674arW {
    private Map<Class<? extends C1616Xf>, Class<? extends C3582se>> map;

    /* renamed from: b */
    public void mo15831b(Class<? extends C1616Xf> cls, Class<? extends C3582se> cls2) {
        this.map.put(cls, cls2);
    }

    /* renamed from: au */
    public Class<? extends C3582se> mo15830au(Class<? extends C1616Xf> cls) {
        if (cls == null) {
            return null;
        }
        Class<? extends C3582se> cls2 = this.map.get(cls);
        if (cls2 != null) {
            return cls2;
        }
        Class<? extends C3582se> au = mo15830au(cls.getSuperclass());
        if (au == null) {
            return au;
        }
        this.map.put(cls, au);
        return au;
    }
}
