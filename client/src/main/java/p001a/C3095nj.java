package p001a;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.BitSet;

/* renamed from: a.nj */
/* compiled from: a */
class C3095nj extends DefaultHandler {
    private static final String aIk = "    ";
    private PrintWriter aIl;
    private BitSet aIm = new BitSet();
    private int level;

    public C3095nj(Writer writer) {
        this.aIl = writer instanceof PrintWriter ? writer : new PrintWriter(writer);
    }

    public void startElement(String str, String str2, String str3, Attributes attributes) {
        if (this.level > 0 && !this.aIm.get(this.level)) {
            this.aIl.println('>');
            this.aIm.set(this.level);
        }
        this.level++;
        this.aIl.print(m36487dh(this.level - 1));
        this.aIl.print('<');
        this.aIl.print(str3);
        for (int i = 0; i < attributes.getLength(); i++) {
            String qName = attributes.getQName(i);
            String value = attributes.getValue(i);
            if (i == 0) {
                this.aIl.print(' ');
            } else {
                this.aIl.print(m36487dh(this.level));
            }
            this.aIl.print(qName);
            this.aIl.print('=');
            this.aIl.print('\"');
            this.aIl.print(value);
            this.aIl.print('\"');
            if (i != attributes.getLength() - 1) {
                this.aIl.println();
            }
        }
    }

    /* renamed from: dh */
    private Object m36487dh(int i) {
        StringBuffer stringBuffer = new StringBuffer(aIk.length() * i);
        for (int i2 = 0; i2 < i; i2++) {
            stringBuffer.append(aIk);
        }
        return stringBuffer.toString();
    }

    public void endElement(String str, String str2, String str3) {
        if (this.aIm.get(this.level)) {
            this.aIl.print(m36487dh(this.level - 1));
            this.aIl.print("</");
            this.aIl.print(str3);
            this.aIl.println('>');
            this.aIm.clear(this.level);
        } else {
            this.aIl.println("/>");
        }
        this.level--;
    }

    public void endDocument() {
        this.aIl.flush();
    }
}
