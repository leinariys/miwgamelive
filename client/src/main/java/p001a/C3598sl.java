package p001a;

import logic.res.XmlNode;

/* renamed from: a.sl */
/* compiled from: a */
public class C3598sl extends C0704KA {
    public final String bgh;

    public C3598sl(String str) {
        this.bgh = str;
    }

    /* renamed from: a */
    public Object mo1200a(C6146ahO aho, XmlNode agy) {
        String attribute = agy.getAttribute("scriptObject");
        if (attribute != null) {
            return aho.mo13514hu(Long.parseLong(attribute));
        }
        return agy;
    }
}
