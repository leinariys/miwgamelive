package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import logic.bbb.C0899ND;
import logic.bbb.C2051bb;
import logic.bbb.C3165ob;
import logic.bbb.azP;

/* renamed from: a.zF */
/* compiled from: a */
class C4084zF implements aTC {
    public final C0763Kt stack = C0763Kt.bcE();
    private final Vec3f cam = new Vec3f();
    private final Vec3f can = new Vec3f();
    private final Matrix4fWrap cas = new Matrix4fWrap();
    private final Matrix4fWrap cat = new Matrix4fWrap();
    public int car;
    private C3165ob caj;
    private C3978xf cak;
    private C3978xf cal;
    private C5408aIe cao;
    private float cap;
    private C2051bb caq = new C2051bb(new C0327ES(), new C5360aGi());
    private C2678iT cau;
    private azP cav = new azP();

    public C4084zF(Matrix4fWrap ajk, C3165ob obVar, Matrix4fWrap ajk2, C0899ND nd) {
        this.cat.set(ajk);
        this.cas.set(ajk2);
        this.caj = obVar;
        this.cak = (C3978xf) this.stack.bcJ().get();
        this.cal = (C3978xf) this.stack.bcJ().get();
        ajk.get(this.cak.bFF);
        ajk2.get(this.cal.bFF);
        ajk.getTranslation(this.cak.bFG);
        ajk2.getTranslation(this.cal.bFG);
    }

    /* renamed from: a */
    public void mo23266a(float f, C2678iT iTVar) {
        this.stack.bcF();
        try {
            this.cap = f;
            this.cau = iTVar;
            C3978xf xfVar = (C3978xf) this.stack.bcJ().get();
            xfVar.mo22952b(this.cal);
            xfVar.mo22954c(this.cak);
            this.caj.getAabb(xfVar, this.cam, this.can);
            Vec3f h = this.stack.bcH().mo4460h(f, f, f);
            this.can.add(h);
            this.cam.sub(h);
        } finally {
            this.stack.bcG();
        }
    }

    /* renamed from: a */
    public void mo820a(Vec3f[] vec3fArr, int i, int i2) {
        this.stack.bcH().push();
        try {
            if (!(this.cao == null || this.cao.iaO == null || this.cao.iaO.bnd() <= 0)) {
                Vec3f h = this.stack.bcH().mo4460h(255.0f, 255.0f, 0.0f);
                C3978xf xfVar = this.cal;
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                vec3f.set(vec3fArr[0]);
                xfVar.mo22946G(vec3f);
                vec3f2.set(vec3fArr[1]);
                xfVar.mo22946G(vec3f2);
                this.cao.iaO.mo4811i(vec3f, vec3f2, h);
                vec3f.set(vec3fArr[1]);
                xfVar.mo22946G(vec3f);
                vec3f2.set(vec3fArr[2]);
                xfVar.mo22946G(vec3f2);
                this.cao.iaO.mo4811i(vec3f, vec3f2, h);
                vec3f.set(vec3fArr[2]);
                xfVar.mo22946G(vec3f);
                vec3f2.set(vec3fArr[0]);
                xfVar.mo22946G(vec3f2);
                this.cao.iaO.mo4811i(vec3f, vec3f2, h);
            }
            this.cav.mo17193k(vec3fArr[0], vec3fArr[1], vec3fArr[2]);
            this.cav.setMargin(this.cap);
            this.caq.mo7270a(this.cat, this.caj, this.cas, this.cav, this.cau);
        } finally {
            this.stack.bcH().pop();
        }
    }

    public Vec3f asv() {
        return this.cam;
    }

    public Vec3f asw() {
        return this.can;
    }
}
