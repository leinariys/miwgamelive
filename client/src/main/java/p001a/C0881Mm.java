package p001a;

import game.geometry.Vec3d;
import game.script.main.bot.MissionABC;
import game.script.ship.Ship;
import logic.thred.PoolThread;

import javax.vecmath.Tuple3d;

/* renamed from: a.Mm */
/* compiled from: a */
public class C0881Mm extends ayA {
    MissionABC dzG;

    public C0881Mm(C1285Sv sv) {
        super(sv);
    }

    /* access modifiers changed from: protected */
    public void aGu() {
        if (this.state == 0) {
            this.runs++;
            mo16918a("Docked", C1285Sv.C1286a.Docked);
            PoolThread.sleep(1000);
            log("Undock: " + mo5504eT().getName());
            if (mo5495al().isDead()) {
                btE();
            }
            btJ().mo16115bS(mo5503dL());
            bgW();
            btG().mo19541a(C2675iR.C2676a.HiSpeedStates);
            btG().mo19543hg(C1285Sv.btK());
            mo16917a(btG());
            mo16918a("FlyFar", C1285Sv.C1286a.LongFlight);
            this.state++;
        } else if (this.state == 1) {
            bgV();
            btH().restart();
            btG().mo19541a(C2675iR.C2676a.FastMovementsStates);
            btG().mo19543hg(C1285Sv.btN());
            mo16917a(btG());
            mo16918a("FlyMov", C1285Sv.C1286a.NPCFightMoving);
            this.state++;
        } else if (this.state == 2) {
            Ship arI = this.dzG.arI();
            if (arI == null) {
                log("Mission did not spawn NPC.");
                this.state++;
                return;
            }
            btH().finish();
            this.dzG.arG();
            float aY = btJ().mo16105aY(arI) + btJ().mo16105aY(mo5495al()) + 50.0f;
            PoolThread.sleep(400);
            btJ().mo16112b(mo5495al(), arI.getPosition().mo9517f((Tuple3d) new Vec3d((double) aY, (double) aY, (double) aY)));
            PoolThread.sleep(400);
            btJ().mo16124d(mo5495al(), arI.getPosition());
            PoolThread.sleep(400);
            log("Firing on NPC");
            cDL();
            mo16918a("FireNPC", C1285Sv.C1286a.NPCFightStopped);
            mo16928jh(10000);
            this.state++;
        } else if (this.state == 3) {
            bai();
            if (!this.dzG.arK()) {
                this.dzG.arM();
            }
            cleanUp();
            btG().mo19541a(C2675iR.C2676a.HiSpeedStates);
            btG().mo19543hg(C1285Sv.btK());
            mo16917a(btG());
            mo16918a("FlyToSt", C1285Sv.C1286a.LongFlight);
            this.state++;
        } else if (this.state == 4) {
            log("Dock");
            btJ().mo16116bU(mo5503dL());
            mo16918a("Docked", C1285Sv.C1286a.Docked);
            this.state = 0;
        }
    }

    private void bgV() {
        Vec3d arA = this.dzG.arA();
        PoolThread.sleep(400);
        btJ().mo16112b(mo5495al(), arA);
        PoolThread.sleep(400);
    }

    private void cleanUp() {
        this.dzG = null;
    }

    private void bgW() {
        try {
            this.dzG = btJ().cvc();
        } catch (C3243pZ e) {
            e.printStackTrace();
        }
    }
}
