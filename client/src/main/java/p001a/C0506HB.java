package p001a;

import java.io.ByteArrayOutputStream;

/* renamed from: a.HB */
/* compiled from: a */
public class C0506HB extends ByteArrayOutputStream {
    public C0506HB() {
    }

    public C0506HB(int i) {
        super(i);
    }

    public C0506HB(int i, int i2) {
        super(i);
        m3586C(i2, i);
        this.count = i2;
    }

    public C0506HB(byte[] bArr) {
        this(bArr, 0);
    }

    public C0506HB(byte[] bArr, int i) {
        super(0);
        m3586C(i, bArr.length);
        this.buf = bArr;
        this.count = i;
    }

    /* renamed from: C */
    private void m3586C(int i, int i2) {
        if (i > i2) {
            throw new IllegalArgumentException("Offset is greated then array size: " + i + " > " + i2);
        }
    }

    public byte[] getInternalBuffer() {
        return this.buf;
    }

    public int aRN() {
        return this.buf.length;
    }
}
