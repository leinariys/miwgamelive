package p001a;

/* renamed from: a.avE  reason: case insensitive filesystem */
/* compiled from: a */
public class C6864avE {

    /* renamed from: PI */
    public static final float f5434PI = 3.1415927f;
    public static final float gJo = 0.017453292f;
    public static final float gJp = 1.4959E8f;

    /* renamed from: kR */
    public static float m26482kR(float f) {
        return 0.017453292f * f;
    }

    /* renamed from: kS */
    public static float m26483kS(float f) {
        return 57.295776f * f;
    }

    /* renamed from: a.avE$a */
    public class C1999a {
        public float ima = 0.0f;
        public float radius = 0.0f;

        public C1999a(float f, float f2) {
            this.radius = f;
            this.ima = f2;
        }

        public float dij() {
            return C6864avE.m26482kR(this.ima);
        }

        public float dik() {
            return this.ima;
        }

        /* renamed from: mX */
        public void mo16418mX(float f) {
            this.ima = f;
        }

        /* renamed from: mY */
        public void mo16419mY(float f) {
            this.ima = f;
        }

        public float getRadius() {
            return this.radius;
        }

        public void setRadius(float f) {
            this.radius = f;
        }
    }
}
