package p001a;

/* renamed from: a.pX */
/* compiled from: a */
public final class C3241pX {
    /* renamed from: cQ */
    public static boolean m37167cQ(float f) {
        return f < -1.0E-7f;
    }

    /* renamed from: cR */
    public static boolean m37168cR(float f) {
        return f > 1.0E-7f;
    }

    public static boolean isZero(float f) {
        return f < 1.0E-7f && f > -1.0E-7f;
    }

    /* renamed from: e */
    public static float m37169e(float f, float f2) {
        if (!isZero(f)) {
            boolean cQ = m37167cQ(f);
            if (cQ) {
                f *= -1.0f;
            }
            double atan = Math.atan((double) (f2 / f));
            if (cQ) {
                atan = 3.141592653589793d - atan;
            }
            float degrees = ((float) Math.toDegrees(atan)) % 360.0f;
            if (degrees > 180.0f) {
                degrees -= 360.0f;
            }
            if (degrees < -180.0f) {
                return degrees + 360.0f;
            }
            return degrees;
        } else if (isZero(f2)) {
            return 0.0f;
        } else {
            if (f2 < 0.0f) {
                return (float) Math.toDegrees(-1.5707963267948966d);
            }
            return (float) Math.toDegrees(1.5707963267948966d);
        }
    }
}
