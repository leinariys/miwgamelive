package p001a;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

/* renamed from: a.avN  reason: case insensitive filesystem */
/* compiled from: a */
public class C6873avN {
    public static final int daD = Integer.MAX_VALUE;

    /* renamed from: b */
    public static String m26580b(String[] strArr, String str) {
        if (strArr.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < strArr.length; i++) {
            if (i > 0) {
                sb.append(str);
            }
            sb.append(strArr[i]);
        }
        return sb.toString();
    }

    /* renamed from: a */
    public static String[] m26578a(String[] strArr, int i, int i2) {
        return (String[]) C0550He.m5235b(strArr, i, i2);
    }

    /* renamed from: km */
    public static String m26581km(String str) {
        int length = str.length();
        while (length > 0 && str.charAt(length - 1) <= ' ') {
            length--;
        }
        return length == str.length() ? str : str.substring(0, length);
    }

    public static String toString(String[] strArr) {
        if (strArr == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder("String[]");
        sb.append('{');
        boolean z = true;
        for (String str : strArr) {
            if (!z) {
                sb.append(',');
            } else {
                z = false;
            }
            if (str == null) {
                sb.append("null");
            } else {
                sb.append('\"');
                sb.append(str);
                sb.append('\"');
            }
        }
        sb.append("}");
        return sb.toString();
    }

    /* renamed from: m */
    public static String[] m26582m(File file) {
        String[] strArr = null;
        ArrayList arrayList = new ArrayList();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                break;
            }
            arrayList.add(readLine);
        }
        String[] strArr2 = new String[arrayList.size()];
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            strArr2[i] = (String) arrayList.get(i);
        }
        return strArr2;
    }

    /* renamed from: ay */
    public static int m26579ay(String str, String str2) {
        int length = str.length();
        int length2 = str2.length();
        if (length <= length2) {
            length2 = length;
        }
        int i = 0;
        while (i < length2 && str.charAt(i) == str2.charAt(i)) {
            i++;
        }
        return i;
    }
}
