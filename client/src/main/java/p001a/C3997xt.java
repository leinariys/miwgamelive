package p001a;

import org.mozilla1.javascript.ScriptRuntime;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.xt */
/* compiled from: a */
public class C3997xt extends ImageIcon {
    private static final int BORDER = 6;


    /* renamed from: cW */
    private boolean f9562cW;

    public C3997xt(Image image) {
        super(image);
    }

    public int getIconHeight() {
        return C3997xt.super.getIconHeight() + 6;
    }

    public int getIconWidth() {
        return C3997xt.super.getIconWidth() + 6;
    }

    public void start() {
        this.f9562cW = true;
    }

    public void stop() {
        this.f9562cW = false;
    }

    public synchronized void paintIcon(Component component, Graphics graphics, int i, int i2) {
        Graphics2D create = graphics.create();
        create.translate(3, 3);
        if (this.f9562cW) {
            create.rotate((((double) ((System.currentTimeMillis() / 10) % 360)) * 3.141592653589793d) / 180.0d, (double) ((getImage().getWidth(component) / 2) + i), (double) ((getImage().getHeight(component) / 2) + i2));
        } else {
            create.rotate(ScriptRuntime.NaN);
        }
        create.drawImage(getImage(), i, i2, component);
    }
}
