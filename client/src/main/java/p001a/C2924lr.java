package p001a;

import game.script.player.Player;
import logic.aaa.C5783aaP;
import taikodom.addon.hud.HudShipVelocityAddon;

/* renamed from: a.lr */
/* compiled from: a */
class C2924lr extends C3428rT<C5783aaP> {
    final /* synthetic */ HudShipVelocityAddon axl;

    public C2924lr(HudShipVelocityAddon hudShipVelocityAddon) {
        this.axl = hudShipVelocityAddon;
    }

    /* renamed from: a */
    public void mo321b(C5783aaP aap) {
        if (this.axl.f9932kj.ala() != null) {
            if (aap.bMO() != C5783aaP.C1841a.FLYING) {
                this.axl.f9931DV.setVisible(false);
                if (this.axl.bvu != null) {
                    this.axl.bvu.cancel();
                    this.axl.bvu = null;
                    return;
                }
                return;
            }
            Player aPC = this.axl.f9932kj.ala().getPlayer();
            if (aPC != null && aPC.bQx() != null) {
                this.axl.mo23972b(aPC.bQx());
                this.axl.f9931DV.setVisible(true);
            }
        }
    }
}
