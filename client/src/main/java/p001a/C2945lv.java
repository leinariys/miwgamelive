package p001a;

import game.script.npc.NPC;
import game.script.spacezone.population.NPCSpawn;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: a.lv */
/* compiled from: a */
public class C2945lv {
    /* renamed from: a */
    public static List<C2782jv> m35367a(List<NPC> list, List<NPCSpawn> list2, int i) {
        ArrayList<C2782jv> arrayList = new ArrayList<>();
        for (NPCSpawn next : list2) {
            arrayList.add(new C2782jv(next.abs(), ((float) next.dfh()) / 100.0f));
        }
        float f = 1.0f / ((float) i);
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            C2782jv jvVar = (C2782jv) it.next();
            int i2 = 0;
            for (NPC Fs : list) {
                if (Fs.mo11654Fs() == jvVar.mo19997Fs()) {
                    i2++;
                }
            }
            float Ft = jvVar.mo19998Ft() - (((float) i2) * f);
            if (Ft <= 0.0f) {
                it.remove();
            } else {
                jvVar.mo20000aM(Ft);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        float f2 = 0.0f;
        for (C2782jv Ft2 : arrayList) {
            f2 = Ft2.mo19998Ft() + f2;
        }
        if (f2 == 0.0f) {
            return null;
        }
        float f3 = 1.0f / f2;
        for (C2782jv jvVar2 : arrayList) {
            jvVar2.mo20000aM(jvVar2.mo19998Ft() * f3);
        }
        return arrayList;
    }
}
