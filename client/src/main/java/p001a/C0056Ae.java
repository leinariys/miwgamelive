package p001a;

import game.network.WhoAmI;
import game.network.channel.NetworkChannel;

import java.io.IOException;

/* renamed from: a.Ae */
/* compiled from: a */
public class C0056Ae extends C6094agO {
    public C0056Ae() {
        mo13376ez(false);
    }

    public C0056Ae(NetworkChannel ahg, WhoAmI lt, Class<?> cls) {
        setServerSocket(ahg);
        setWhoAmI(lt);
        setClassObject(cls);
        mo13376ez(false);
    }

    public C6280ajs ate() {
        C6280ajs ate = super.ate();
        C0433GA ga = ate;
        if (ate == null) {
            C0433GA ga2 = new C0433GA((C6781atZ) null);
            try {
                ga2.mo2248d(getClass().getResource("/taikodom/infra/script/InfraScripts"));
                ga2.mo2248d(getClass().getResource("/taikodom/infra/script/tests/InfraTestScripts"));
                super.mo13364b((C6280ajs) ga2);
                ga = ga2;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return ga;
    }

    public boolean atf() {
        return false;
    }
}
