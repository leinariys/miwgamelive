package p001a;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C5966adq;
import game.script.item.Component;
import game.script.item.*;
import game.script.ship.SectorCategory;
import game.script.ship.Ship;
import game.script.ship.ShipSector;
import game.script.ship.ShipStructure;
import logic.baa.C5473aKr;
import logic.baa.aDJ;
import logic.data.link.C2743jO;
import logic.res.code.C5663aRz;
import logic.ui.C2698il;
import logic.ui.ComponentManager;
import logic.ui.IBaseUiTegXml;
import logic.ui.item.*;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.messagedialog.MessageDialogAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

/* renamed from: a.fd */
/* compiled from: a */
public class C2459fd extends C7023azk {
    /* access modifiers changed from: private */
    public ShipSector iyu;
    /* access modifiers changed from: private */
    public java.util.List iyv = new ArrayList();
    private Repeater.C3671a<ShipSector> iyr;
    private Repeater<ShipSector> iys;
    private ListJ iyt;
    private C2460a iyw;
    private C5473aKr<aDJ, Object> iyx;
    private DefaultListModel iyy;

    public C2459fd(IAddonProperties vWVar, C2698il ilVar) {
        super(vWVar, ilVar);
        initComponents();
        m31207iG();
        dnk();
    }

    /* renamed from: iG */
    private void m31207iG() {
        this.iyx = new C2474b();
    }

    /* renamed from: p */
    public void mo18743p(SectorCategory fMVar) {
        if (cFC() != null) {
            for (ShipStructure bVG : cFC().agt()) {
                for (ShipSector or : bVG.bVG()) {
                    if (or.bmW() == fMVar) {
                        m31206f(or);
                        for (JToggleButton next : this.iyv) {
                            if (next.getText().equals(fMVar.mo18438ke().get())) {
                                next.setSelected(true);
                            } else {
                                next.setSelected(false);
                            }
                        }
                    }
                }
            }
        }
    }

    private void initComponents() {
        this.iys = cFB().mo4915cd("sectorButtons");
        this.iyt = cFB().mo4915cd("slots");
        this.iyw = new C2460a(this, (C2460a) null);
        this.iyy = new DefaultListModel();
        this.iyt.setModel(this.iyy);
        this.iyt.setDragEnabled(true);
        this.iyt.setTransferHandler(this.iyw);
        this.iyt.setDropMode(DropMode.ON_OR_INSERT);
    }

    private void dnk() {
        this.iyr = new C2475c();
        this.iys.mo22250a(this.iyr);
        this.iyt.setCellRenderer(new C2477d());
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public void m31206f(ShipSector or) {
        boolean z;
        this.iyw.mo18745b((ItemLocation) or);
        this.iyy.clear();
        Iterator<Item> iterator = or.getIterator();
        int i = 0;
        int i2 = 0;
        while (iterator.hasNext()) {
            Item next = iterator.next();
            if (next instanceof Component) {
                i2 += ((Component) next).cJS().cuJ();
                this.iyy.addElement(new C2479f((Component) next, false));
                i++;
            }
        }
        int dAF = or.bmQ().dAF();
        for (int i3 = 0; i3 < dAF - i; i3++) {
            DefaultListModel defaultListModel = this.iyy;
            String handle = or.bmQ().bmW().getHandle();
            if (i2 < dAF) {
                z = true;
            } else {
                z = false;
            }
            defaultListModel.addElement(new C2479f(handle, z));
            i2++;
        }
        this.iyu = or;
    }

    /* renamed from: M */
    public void mo4307M(Ship fAVar) {
        super.mo4307M(fAVar);
        refresh();
    }

    private void refresh() {
        dnl();
    }

    private void dnl() {
        this.iyv.clear();
        this.iys.clear();
        if (cFC() != null) {
            boolean z = true;
            for (ShipStructure bVG : cFC().agt()) {
                ArrayList arrayList = new ArrayList(bVG.bVG());
                Collections.sort(arrayList, new C2478e(this, (C2478e) null));
                Iterator it = arrayList.iterator();
                boolean z2 = z;
                while (it.hasNext()) {
                    ShipSector or = (ShipSector) it.next();
                    JToggleButton jToggleButton = this.iys.mo22248G(or).get(0);
                    if (z2) {
                        jToggleButton.setSelected(true);
                        this.iyu = or;
                        z2 = false;
                    }
                    if (or.bmQ().bmW() != null) {
                        jToggleButton.setToolTipText(or.bmQ().bmW().mo18438ke().get());
                    }
                    this.iyv.add(jToggleButton);
                    or.mo8319a(C2743jO.apm, (C5473aKr<?, ?>) this.iyx);
                }
                z = z2;
            }
            m31206f(this.iyu);
        }
    }

    /* renamed from: a.fd$b */
    /* compiled from: a */
    class C2474b implements C5473aKr<aDJ, Object> {
        C2474b() {
        }

        /* renamed from: a */
        public void mo1101a(aDJ adj, C5663aRz arz, Object[] objArr) {
            if (adj == C2459fd.this.iyu) {
                C2459fd.this.m31206f((ShipSector) adj);
            }
        }

        /* renamed from: b */
        public void mo1102b(aDJ adj, C5663aRz arz, Object[] objArr) {
            if (adj == C2459fd.this.iyu) {
                C2459fd.this.m31206f((ShipSector) adj);
            }
        }
    }

    /* renamed from: a.fd$c */
    /* compiled from: a */
    class C2475c implements Repeater.C3671a<ShipSector> {
        C2475c() {
        }

        /* renamed from: a */
        public void mo843a(ShipSector or, java.awt.Component component) {
            JToggleButton jToggleButton = (JToggleButton) component;
            if (or == null) {
                C2459fd.this.bUA().getLog().error("Ship '" + C2459fd.this.cFC().agH().getHandle() + "' has a null ship sector");
                return;
            }
            jToggleButton.setText(or.mo4432ke().get());
            jToggleButton.addActionListener(new C2476a(or, jToggleButton));
        }

        /* renamed from: a.fd$c$a */
        class C2476a implements ActionListener {
            private final /* synthetic */ ShipSector gGe;
            private final /* synthetic */ JToggleButton gGf;

            C2476a(ShipSector or, JToggleButton jToggleButton) {
                this.gGe = or;
                this.gGf = jToggleButton;
            }

            public void actionPerformed(ActionEvent actionEvent) {
                C2459fd.this.m31206f(this.gGe);
                for (JToggleButton jToggleButton : C2459fd.this.iyv) {
                    jToggleButton.setSelected(this.gGf == jToggleButton);
                }
            }
        }
    }

    /* renamed from: a.fd$d */
    /* compiled from: a */
    class C2477d extends C5517aMj {
        C2477d() {
        }

        /* renamed from: a */
        public java.awt.Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            String str;
            LabeledIcon dAx = aur.dAx();
            Picture a = dAx.mo2601a(LabeledIcon.C0530a.NORTH_WEST);
            if (obj instanceof C2479f) {
                C2479f fVar = (C2479f) obj;
                if (fVar.bHV() != null) {
                    ComponentManager.getCssHolder(dAx.bHq()).setAttribute("class", "");
                    dAx.mo2605a((Icon) new C2539gY((Item) fVar.bHV()));
                    ComponentManager.getCssHolder(a).setAttribute("class", "size" + fVar.bHV().cJS().cuJ());
                    ComponentManager.getCssHolder(dAx).setAttribute("class", "available");
                    a.setVisible(true);
                    LabeledIcon.C0530a aVar = LabeledIcon.C0530a.SOUTH_EAST;
                    if (fVar.bHV() instanceof ProjectileWeapon) {
                        str = String.valueOf(((ProjectileWeapon) fVar.bHV()).aqh());
                    } else {
                        str = "";
                    }
                    dAx.mo2602a(aVar, str);
                } else {
                    if (fVar.bHW()) {
                        ComponentManager.getCssHolder(dAx).setAttribute("class", "available");
                    } else {
                        ComponentManager.getCssHolder(dAx).setAttribute("class", "unavailable");
                    }
                    ComponentManager.getCssHolder(dAx.bHq()).setAttribute("class", fVar.getHandle());
                    a.setVisible(false);
                    dAx.mo2602a(LabeledIcon.C0530a.SOUTH_EAST, "");
                }
            } else {
                dAx.mo2605a((Icon) new ImageIcon());
                dAx.mo2602a(LabeledIcon.C0530a.SOUTH_EAST, "");
            }
            dAx.setPreferredSize(new Dimension(48, 54));
            return dAx;
        }
    }

    /* renamed from: a.fd$e */
    /* compiled from: a */
    private class C2478e<S extends ShipSector> implements Comparator<ShipSector> {
        private C2478e() {
        }

        /* synthetic */ C2478e(C2459fd fdVar, C2478e eVar) {
            this();
        }

        /* renamed from: a */
        public int compare(ShipSector or, ShipSector or2) {
            if (or.bmU() < or2.bmU()) {
                return -1;
            }
            return 1;
        }
    }

    /* renamed from: a.fd$f */
    /* compiled from: a */
    public class C2479f {
        private final String handle;
        private Component ciQ;
        private boolean eMU;

        public C2479f(Component abl, boolean z) {
            this.handle = "";
            this.ciQ = abl;
            this.eMU = z;
        }

        public C2479f(String str, boolean z) {
            this.handle = str.replace('_', '-');
            this.ciQ = null;
            this.eMU = z;
        }

        public Component bHV() {
            return this.ciQ;
        }

        public boolean bHW() {
            return this.eMU;
        }

        public String getHandle() {
            return this.handle;
        }
    }

    /* renamed from: a.fd$a */
    private class C2460a extends TransferHandler {

        private static /* synthetic */ int[] hQq = null;
        /* access modifiers changed from: private */
        public ItemLocation bLj;

        private C2460a() {
        }

        /* synthetic */ C2460a(C2459fd fdVar, C2460a aVar) {
            this();
        }

        static /* synthetic */ int[] dau() {
            int[] iArr = hQq;
            if (iArr == null) {
                iArr = new int[C3904wY.values().length];
                try {
                    iArr[C3904wY.AMPLIFIER.ordinal()] = 3;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C3904wY.CANNON.ordinal()] = 1;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C3904wY.LAUNCHER.ordinal()] = 2;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C3904wY.PROGRESSION.ordinal()] = 5;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[C3904wY.SYSTEM.ordinal()] = 4;
                } catch (NoSuchFieldError e5) {
                }
                hQq = iArr;
            }
            return iArr;
        }

        public ItemLocation apL() {
            return this.bLj;
        }

        /* renamed from: b */
        public void mo18745b(ItemLocation aag) {
            this.bLj = aag;
        }

        public boolean canImport(TransferHandler.TransferSupport transferSupport) {
            if (!transferSupport.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                return false;
            }
            if (!C2459fd.this.bUA().getPlayer().bQB()) {
                return false;
            }
            if (transferSupport.getDropLocation().getIndex() == -1) {
                return false;
            }
            return true;
        }

        public boolean importData(TransferHandler.TransferSupport transferSupport) {
            boolean z;
            if (!C2459fd.this.bUA().getPlayer().bQB()) {
                return false;
            }
            if (this.bLj == null) {
                return false;
            }
            if (!transferSupport.isDrop()) {
                return false;
            }
            if (!transferSupport.isDataFlavorSupported(C2125cH.f6026vP)) {
                return false;
            }
            try {
                for (aDJ adj : (Collection) transferSupport.getTransferable().getTransferData(C2125cH.f6026vP)) {
                    if (adj instanceof Item) {
                        Item auq = (Item) adj;
                        if (auq.bNh() != this.bLj) {
                            JList component = transferSupport.getComponent();
                            C2479f fVar = (C2479f) component.getModel().getElementAt(component.locationToIndex(transferSupport.getDropLocation().getDropPoint()));
                            if (auq instanceof ClipBox) {
                                if (fVar.bHV() != null && (fVar.bHV() instanceof ProjectileWeapon)) {
                                    C2459fd.this.cFB().mo4911Kk();
                                    ((ProjectileWeapon) C3582se.m38985a((ProjectileWeapon) fVar.bHV(), (C6144ahM<?>) new C2469d(fVar, auq))).mo5362b((ClipBox) auq);
                                }
                            } else if ((auq instanceof Clip) && fVar.bHV() != null && (fVar.bHV() instanceof ProjectileWeapon)) {
                                C2459fd.this.cFB().mo4911Kk();
                                ((ProjectileWeapon) C3582se.m38985a((ProjectileWeapon) fVar.bHV(), (C6144ahM<?>) new C2466c(fVar, auq))).mo5368e((Clip) auq);
                            }
                            if (!auq.cxn()) {
                                m31211Q(auq);
                            } else if (!auq.isBound()) {
                                ((MessageDialogAddon) C2459fd.this.bUA().mo11830U(MessageDialogAddon.class)).mo24307a(C5956adg.format(C2459fd.this.bUA().translate("The item [{0}] will be bounded and durability count should start. Do you proced?"), auq.bAP().mo19891ke().get()), (aEP) new C2472e(auq), C2459fd.this.bUA().translate("No"), C2459fd.this.bUA().translate("Yes"));
                            } else if (auq.cxq()) {
                                ((MessageDialogAddon) C2459fd.this.bUA().mo11830U(MessageDialogAddon.class)).mo24307a(C5956adg.format(C2459fd.this.bUA().translate("You trying to equip with an inactive item [{0}]. Do you proced?"), auq.bAP().mo19891ke().get()), (aEP) new C2473f(auq), C2459fd.this.bUA().translate("No"), C2459fd.this.bUA().translate("Yes"));
                            } else {
                                m31211Q(auq);
                            }
                        }
                    }
                    if (adj instanceof ItemType) {
                        if (!C5916acs.getSingolton().getTaikodom().aLS().ads()) {
                            z = false;
                        } else {
                            z = true;
                        }
                        if (z) {
                            m31210J((ItemType) adj);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }

        public int getSourceActions(JComponent jComponent) {
            return 2;
        }

        /* access modifiers changed from: protected */
        public Transferable createTransferable(JComponent jComponent) {
            JList jList = (JList) jComponent;
            try {
                Object[] selectedValues = jList.getSelectedValues();
                ArrayList arrayList = new ArrayList(selectedValues.length);
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= selectedValues.length) {
                        IBaseUiTegXml.initBaseUItegXML().mo13724cx(ComponentManager.getCssHolder(jList).mo13049Vr().aue());
                        return new C5569aOj((Item[]) arrayList.toArray(new Item[arrayList.size()]));
                    }
                    arrayList.add(((C2479f) selectedValues[i2]).bHV());
                    i = i2 + 1;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public void exportDone(JComponent jComponent, Transferable transferable, int i) {
        }

        /* access modifiers changed from: private */
        /* renamed from: Q */
        public void m31211Q(Item auq) {
            try {
                C2459fd.this.cFB().mo4911Kk();
                ((ItemLocation) C3582se.m38985a(this.bLj, (C6144ahM<?>) new C2463b())).mo2693s(auq);
            } catch (C2293dh e) {
            }
        }

        /* renamed from: J */
        private void m31210J(ItemType jCVar) {
            try {
                ((ItemLocation) C3582se.m38985a(this.bLj, (C6144ahM<?>) new C2461a(jCVar))).mo2695u(jCVar);
            } catch (C2293dh e) {
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m31215a(ItemType jCVar, C3904wY wYVar) {
            switch (dau()[wYVar.ordinal()]) {
                case 1:
                    if (jCVar instanceof EnergyWeaponType) {
                        C2459fd.this.bUA().aVU().mo13975h(new C6811auD(C3667tV.EQUIP_CANNON_ENERGY));
                        return;
                    } else if (jCVar instanceof ProjectileWeaponType) {
                        C2459fd.this.bUA().aVU().mo13975h(new C6811auD(C3667tV.EQUIP_CANNON_PROJECTILE));
                        return;
                    } else {
                        return;
                    }
                case 2:
                    C2459fd.this.bUA().aVU().mo13975h(new C6811auD(C3667tV.EQUIP_LAUNCHER));
                    return;
                case 3:
                    C2459fd.this.bUA().aVU().mo13975h(new C6811auD(C3667tV.EQUIP_AMPLIFIER_MODULE));
                    return;
                case 4:
                    C2459fd.this.bUA().aVU().mo13975h(new C6811auD(C3667tV.EQUIP_AMPLIFIER_MODULE));
                    return;
                default:
                    return;
            }
        }

        /* renamed from: a.fd$a$d */
        /* compiled from: a */
        class C2469d implements C6144ahM<Item> {
            private final /* synthetic */ C2479f cQL;
            private final /* synthetic */ Item cQM;

            C2469d(C2479f fVar, Item auq) {
                this.cQL = fVar;
                this.cQM = auq;
            }

            /* renamed from: o */
            public void mo1931n(Item auq) {
                SwingUtilities.invokeLater(new C2471b(this.cQL, this.cQM));
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
                SwingUtilities.invokeLater(new C2470a());
                th.printStackTrace();
            }

            /* renamed from: a.fd$a$d$b */
            /* compiled from: a */
            class C2471b implements Runnable {
                private final /* synthetic */ C2479f cQL;
                private final /* synthetic */ Item cQM;

                C2471b(C2479f fVar, Item auq) {
                    this.cQL = fVar;
                    this.cQM = auq;
                }

                public void run() {
                    C2459fd.this.cFB().mo4912Kl();
                    C2459fd.this.bUA().aVU().mo13975h(new C5966adq(this.cQL.bHV(), this.cQM));
                }
            }

            /* renamed from: a.fd$a$d$a */
            class C2470a implements Runnable {
                C2470a() {
                }

                public void run() {
                    C2459fd.this.cFB().mo4912Kl();
                }
            }
        }

        /* renamed from: a.fd$a$c */
        /* compiled from: a */
        class C2466c implements C6144ahM<Item> {
            private final /* synthetic */ C2479f cQL;
            private final /* synthetic */ Item cQM;

            C2466c(C2479f fVar, Item auq) {
                this.cQL = fVar;
                this.cQM = auq;
            }

            /* renamed from: o */
            public void mo1931n(Item auq) {
                SwingUtilities.invokeLater(new C2467a(this.cQL, this.cQM));
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
                SwingUtilities.invokeLater(new C2468b());
                th.printStackTrace();
            }

            /* renamed from: a.fd$a$c$a */
            class C2467a implements Runnable {
                private final /* synthetic */ C2479f cQL;
                private final /* synthetic */ Item cQM;

                C2467a(C2479f fVar, Item auq) {
                    this.cQL = fVar;
                    this.cQM = auq;
                }

                public void run() {
                    C2459fd.this.cFB().mo4912Kl();
                    C2459fd.this.bUA().aVU().mo13975h(new C5966adq(this.cQL.bHV(), this.cQM));
                }
            }

            /* renamed from: a.fd$a$c$b */
            /* compiled from: a */
            class C2468b implements Runnable {
                C2468b() {
                }

                public void run() {
                    C2459fd.this.cFB().mo4912Kl();
                }
            }
        }

        /* renamed from: a.fd$a$e */
        /* compiled from: a */
        class C2472e extends aEP {
            private final /* synthetic */ Item cQM;

            C2472e(Item auq) {
                this.cQM = auq;
            }

            /* renamed from: a */
            public void mo8596a(int i, String str, boolean z) {
                if (i == 1) {
                    C2460a.this.m31211Q(this.cQM);
                }
            }

            /* access modifiers changed from: protected */
            /* renamed from: yD */
            public C0939Nn.C0940a mo8601yD() {
                return C0939Nn.C0940a.WARNING;
            }
        }

        /* renamed from: a.fd$a$f */
        /* compiled from: a */
        class C2473f extends aEP {
            private final /* synthetic */ Item cQM;

            C2473f(Item auq) {
                this.cQM = auq;
            }

            /* renamed from: a */
            public void mo8596a(int i, String str, boolean z) {
                if (i == 1) {
                    C2460a.this.m31211Q(this.cQM);
                }
            }
        }

        /* renamed from: a.fd$a$b */
        /* compiled from: a */
        class C2463b implements C6144ahM<Item> {
            C2463b() {
            }

            /* renamed from: o */
            public void mo1931n(Item auq) {
                SwingUtilities.invokeLater(new C2464a());
                if (auq instanceof Component) {
                    C2460a.this.m31215a(auq.bAP(), ((Component) auq).mo7860sM());
                }
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
                SwingUtilities.invokeLater(new C2465b());
            }

            /* renamed from: a.fd$a$b$a */
            class C2464a implements Runnable {
                C2464a() {
                }

                public void run() {
                    C2459fd.this.cFB().mo4912Kl();
                }
            }

            /* renamed from: a.fd$a$b$b */
            /* compiled from: a */
            class C2465b implements Runnable {
                C2465b() {
                }

                public void run() {
                    C2459fd.this.cFB().mo4912Kl();
                    C2459fd.this.bUA().aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, C2459fd.this.bUA().translate("System location is full or is invalid for this type of item."), new Object[0]));
                }
            }
        }

        /* renamed from: a.fd$a$a */
        class C2461a implements C6144ahM<Item> {
            private final /* synthetic */ ItemType gFb;

            C2461a(ItemType jCVar) {
                this.gFb = jCVar;
            }

            /* renamed from: o */
            public void mo1931n(Item auq) {
                if ((this.gFb instanceof ComponentType) && (C2460a.this.bLj instanceof ShipSector) && ((ShipSector) C2460a.this.bLj).bmW() != null) {
                    C2460a.this.m31215a(this.gFb, ((ShipSector) C2460a.this.bLj).bmW().mo18441sM());
                }
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
                SwingUtilities.invokeLater(new C2462a());
            }

            /* renamed from: a.fd$a$a$a */
            class C2462a implements Runnable {
                C2462a() {
                }

                public void run() {
                    C2459fd.this.bUA().aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, C2459fd.this.bUA().translate("System location is full or is invalid for this type of item."), new Object[0]));
                }
            }
        }
    }
}
