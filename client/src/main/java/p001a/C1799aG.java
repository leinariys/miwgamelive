package p001a;

import logic.ui.PropertiesUiFromCss;

import javax.swing.*;
import java.awt.*;
import java.util.*;

/* renamed from: a.aG */
/* compiled from: a */
public class C1799aG extends TimerTask {

    /* renamed from: ko */
    private ArrayList<JLabel> f2846ko = new ArrayList<>();

    /* renamed from: kp */
    private ArrayList<JLabel> f2847kp = new ArrayList<>();

    /* renamed from: kq */
    private HashMap<JLabel, Integer> f2848kq = new HashMap<>();

    /* renamed from: kr */
    private HashMap<JLabel, Point> f2849kr = new HashMap<>();

    /* renamed from: a */
    public void mo8916a(JLabel jLabel) {
        synchronized (this.f2846ko) {
            this.f2846ko.add(jLabel);
            this.f2848kq.put(jLabel, Integer.valueOf(jLabel.getY()));
            this.f2846ko.notify();
        }
    }

    public void run() {
        synchronized (this.f2846ko) {
            Iterator<JLabel> it = this.f2846ko.iterator();
            while (it.hasNext()) {
                JLabel next = it.next();
                int y = next.getY() - 2;
                this.f2849kr.put(next, new Point(next.getX(), y));
                float intValue = (((float) y) / ((float) this.f2848kq.get(next).intValue())) * 255.0f;
                PropertiesUiFromCss f = PropertiesUiFromCss.m461f(next);
                if (f != null) {
                    int i = (int) intValue;
                    if (i < 0) {
                        i = 0;
                    }
                    f.setColorMultiplier(new Color(i, i, i, i));
                }
                if (next.getY() <= 0) {
                    this.f2847kp.add(next);
                }
            }
            Iterator<JLabel> it2 = this.f2847kp.iterator();
            while (it2.hasNext()) {
                this.f2846ko.remove(it2.next());
            }
        }
        for (Map.Entry next2 : this.f2849kr.entrySet()) {
            ((JLabel) next2.getKey()).setLocation((Point) next2.getValue());
        }
        Iterator<JLabel> it3 = this.f2847kp.iterator();
        while (it3.hasNext()) {
            JLabel next3 = it3.next();
            next3.setVisible(false);
            this.f2849kr.remove(next3);
            this.f2848kq.remove(next3);
        }
        this.f2847kp.clear();
        synchronized (this.f2846ko) {
            if (this.f2846ko.size() == 0) {
                try {
                    this.f2846ko.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
