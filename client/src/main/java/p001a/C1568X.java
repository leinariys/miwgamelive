package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;
import logic.bbb.C1223SC;
import logic.bbb.C3165ob;

import javax.vecmath.Quat4f;
import java.lang.reflect.Array;
import java.util.Arrays;

/* renamed from: a.X */
/* compiled from: a */
public class C1568X {

    /* renamed from: Aj */
    private static final float f2074Aj = Float.MAX_VALUE;
    /* renamed from: Ak */
    private static final float f2075Ak = 3.1415927f;
    /* renamed from: Al */
    private static final float f2076Al = 6.2831855f;
    /* renamed from: Am */
    private static final int f2077Am = 128;
    /* renamed from: An */
    private static final int f2078An = 64;
    /* renamed from: Ao */
    private static final int f2079Ao = 63;
    /* renamed from: Ap */
    private static final float f2080Ap = 1.0E-4f;
    /* renamed from: Aq */
    private static final float f2081Aq = 9.999999E-9f;
    /* renamed from: Ar */
    private static final int f2082Ar = 256;
    /* renamed from: As */
    private static final float f2083As = 0.01f;
    /* renamed from: At */
    private static final float f2084At = 0.001f;
    /* renamed from: Ag */
    public static aFF<C1569a.C1570a> f2071Ag = new aFF<>(C1569a.C1570a.class);
    /* renamed from: Ah */
    public static aFF<C1569a.C1571b> f2072Ah = new aFF<>(C1569a.C1571b.class);
    /* renamed from: Ai */
    public static aFF<C1573c.C1574a> f2073Ai = new aFF<>(C1573c.C1574a.class);
    private static C1569a eML = new C1569a();
    public final C0763Kt stack = C0763Kt.bcE();

    /* renamed from: kh */
    public static void m11406kh() {
        f2071Ag.push();
        f2072Ah.push();
        f2073Ai.push();
    }

    /* renamed from: ki */
    public static void m11407ki() {
        f2071Ag.pop();
        f2072Ah.pop();
        f2073Ai.pop();
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public static boolean m11405a(C3165ob obVar, C3978xf xfVar, C3165ob obVar2, C3978xf xfVar2, C1572b bVar) {
        bVar.f2088Oi[0].set(0.0f, 0.0f, 0.0f);
        bVar.f2088Oi[1].set(0.0f, 0.0f, 0.0f);
        bVar.f2089Oj.set(0.0f, 0.0f, 0.0f);
        bVar.f2090Ok = 0.0f;
        bVar.f2087Oh = C1575d.Separated;
        bVar.f2091Ol = 0;
        bVar.f2092Om = 0;
        eML.mo6674a(xfVar.bFF, xfVar.bFG, obVar, xfVar2.bFF, xfVar2.bFG, obVar2, f2084At);
        try {
            boolean MT = eML.mo6671MT();
            bVar.f2092Om = eML.iterations + 1;
            if (MT) {
                C1573c cVar = new C1573c(eML);
                float aSb = cVar.aSb();
                bVar.f2091Ol = cVar.iterations + 1;
                if (aSb > 0.0f) {
                    bVar.f2087Oh = C1575d.Penetrating;
                    bVar.f2089Oj.set(cVar.f2093Oj);
                    bVar.f2090Ok = aSb;
                    bVar.f2088Oi[0].set(cVar.dcW[0]);
                    bVar.f2088Oi[1].set(cVar.dcW[1]);
                    eML.destroy();
                    return true;
                } else if (cVar.failed) {
                    bVar.f2087Oh = C1575d.EPA_Failed;
                }
            } else if (eML.failed) {
                bVar.f2087Oh = C1575d.GJK_Failed;
            }
            eML.destroy();
            return false;
        } catch (Throwable th) {
            eML.destroy();
            throw th;
        }
    }

    /* renamed from: a.X$d */
    /* compiled from: a */
    public enum C1575d {
        Separated,
        Penetrating,
        GJK_Failed,
        EPA_Failed
    }

    /* renamed from: a.X$b */
    /* compiled from: a */
    public static class C1572b {

        /* renamed from: Oi */
        public final Vec3f[] f2088Oi = {new Vec3f(), new Vec3f()};
        /* renamed from: Oj */
        public final Vec3f f2089Oj = new Vec3f();
        /* renamed from: Oh */
        public C1575d f2087Oh;
        /* renamed from: Ok */
        public float f2090Ok;

        /* renamed from: Ol */
        public int f2091Ol;

        /* renamed from: Om */
        public int f2092Om;
    }

    /* renamed from: a.X$a */
    public static class C1569a {
        public final C1571b[] aBf;
        public final Matrix3fWrap[] aBg;
        public final Vec3f[] aBh;
        public final C3165ob[] aBi;
        public final C1570a[] aBj;
        public final Vec3f aBk;
        public final C0763Kt stack;
        public float aBl;
        public boolean failed;
        public int iterations;
        public int order;

        public C1569a() {
            this.stack = C0763Kt.bcE();
            this.aBf = new C1571b[64];
            this.aBg = new Matrix3fWrap[]{new Matrix3fWrap(), new Matrix3fWrap()};
            this.aBh = new Vec3f[]{new Vec3f(), new Vec3f()};
            this.aBi = new C3165ob[2];
            this.aBj = new C1570a[5];
            this.aBk = new Vec3f();
            for (int i = 0; i < this.aBj.length; i++) {
                this.aBj[i] = new C1570a();
            }
        }

        public C1569a(Matrix3fWrap ajd, Vec3f vec3f, C1223SC sc, Matrix3fWrap ajd2, Vec3f vec3f2, C1223SC sc2) {
            this(ajd, vec3f, sc, ajd2, vec3f2, sc2, 0.0f);
        }

        public C1569a(Matrix3fWrap ajd, Vec3f vec3f, C1223SC sc, Matrix3fWrap ajd2, Vec3f vec3f2, C1223SC sc2, float f) {
            this.stack = C0763Kt.bcE();
            this.aBf = new C1571b[64];
            this.aBg = new Matrix3fWrap[]{new Matrix3fWrap(), new Matrix3fWrap()};
            this.aBh = new Vec3f[]{new Vec3f(), new Vec3f()};
            this.aBi = new C3165ob[2];
            this.aBj = new C1570a[5];
            this.aBk = new Vec3f();
            for (int i = 0; i < this.aBj.length; i++) {
                this.aBj[i] = new C1570a();
            }
            mo6674a(ajd, vec3f, sc, ajd2, vec3f2, sc2, f);
        }

        /* renamed from: t */
        public static int m11408t(Vec3f vec3f) {
            return (((((int) (vec3f.x * 15461.0f)) ^ ((int) (vec3f.y * 83003.0f))) ^ ((int) (vec3f.z * 15473.0f))) * 169639) & 63;
        }

        /* renamed from: a */
        public void mo6674a(Matrix3fWrap ajd, Vec3f vec3f, C3165ob obVar, Matrix3fWrap ajd2, Vec3f vec3f2, C3165ob obVar2, float f) {
            C1568X.m11406kh();
            this.aBg[0].set(ajd);
            this.aBh[0].set(vec3f);
            this.aBi[0] = obVar;
            this.aBg[1].set(ajd2);
            this.aBh[1].set(vec3f2);
            this.aBi[1] = obVar2;
            this.aBl = f;
            this.failed = false;
        }

        public void destroy() {
            C1568X.m11407ki();
        }

        /* renamed from: a */
        public Vec3f mo6673a(Vec3f vec3f, int i) {
            this.stack.bcH().push();
            try {
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                C3427rS.m38372a(vec3f2, vec3f, this.aBg[i]);
                Vec3f ac = this.stack.bcH().mo4458ac(this.aBi[i].localGetSupportingVertex(vec3f2));
                this.aBg[i].transform(ac);
                ac.add(this.aBh[i]);
                return (Vec3f) this.stack.bcH().mo15197aq(ac);
            } finally {
                this.stack.bcH().pop();
            }
        }

        /* renamed from: a */
        public void mo6675a(Vec3f vec3f, C1570a aVar) {
            this.stack.bcH().push();
            try {
                aVar.f2086gw.set(vec3f);
                Vec3f ac = this.stack.bcH().mo4458ac(mo6673a(vec3f, 0));
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                vec3f2.set(vec3f);
                vec3f2.negate();
                aVar.f2085gv.sub(ac, this.stack.bcH().mo4458ac(mo6673a(vec3f2, 1)));
                aVar.f2085gv.scaleAdd(this.aBl, vec3f, aVar.f2085gv);
            } finally {
                this.stack.bcH().pop();
            }
        }

        /* renamed from: MS */
        public boolean mo6670MS() {
            int t = m11408t(this.aBk);
            for (C1571b bVar = this.aBf[t]; bVar != null; bVar = bVar.biv) {
                if (bVar.aLZ.equals(this.aBk)) {
                    this.order--;
                    return false;
                }
            }
            C1571b bVar2 = C1568X.f2072Ah.get();
            bVar2.aLZ.set(this.aBk);
            bVar2.biv = this.aBf[t];
            this.aBf[t] = bVar2;
            Vec3f vec3f = this.aBk;
            C1570a[] aVarArr = this.aBj;
            int i = this.order + 1;
            this.order = i;
            mo6675a(vec3f, aVarArr[i]);
            if (this.aBk.dot(this.aBj[this.order].f2085gv) > 0.0f) {
                return true;
            }
            return false;
        }

        /* JADX INFO: finally extract failed */
        /* renamed from: e */
        public boolean mo6680e(Vec3f vec3f, Vec3f vec3f2) {
            this.stack.bcH().push();
            try {
                if (vec3f2.dot(vec3f) >= 0.0f) {
                    Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                    vec3f3.cross(vec3f2, vec3f);
                    if (vec3f3.lengthSquared() > C1568X.f2081Aq) {
                        this.aBk.cross(vec3f3, vec3f2);
                    } else {
                        this.stack.bcH().pop();
                        return true;
                    }
                } else {
                    this.order = 0;
                    this.aBj[0].mo6682a(this.aBj[1]);
                    this.aBk.set(vec3f);
                }
                this.stack.bcH().pop();
                return false;
            } catch (Throwable th) {
                this.stack.bcH().pop();
                throw th;
            }
        }

        /* renamed from: c */
        public boolean mo6677c(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
            this.stack.bcH().push();
            try {
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                vec3f4.cross(vec3f2, vec3f3);
                return mo6676b(vec3f, vec3f2, vec3f3, vec3f4);
            } finally {
                this.stack.bcH().pop();
            }
        }

        /* renamed from: b */
        public boolean mo6676b(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
            this.stack.bcH().push();
            try {
                Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                vec3f5.cross(vec3f4, vec3f2);
                Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
                vec3f6.cross(vec3f4, vec3f3);
                if (vec3f5.dot(vec3f) < -1.0E-4f) {
                    this.order = 1;
                    this.aBj[0].mo6682a(this.aBj[1]);
                    this.aBj[1].mo6682a(this.aBj[2]);
                    return mo6680e(vec3f, vec3f2);
                } else if (vec3f6.dot(vec3f) > 1.0E-4f) {
                    this.order = 1;
                    this.aBj[1].mo6682a(this.aBj[2]);
                    boolean e = mo6680e(vec3f, vec3f3);
                    this.stack.bcH().pop();
                    return e;
                } else {
                    float dot = vec3f4.dot(vec3f);
                    if (Math.abs(dot) > 1.0E-4f) {
                        if (dot > 0.0f) {
                            this.aBk.set(vec3f4);
                        } else {
                            this.aBk.negate(vec3f4);
                            C1570a aVar = new C1570a();
                            aVar.mo6682a(this.aBj[0]);
                            this.aBj[0].mo6682a(this.aBj[1]);
                            this.aBj[1].mo6682a(aVar);
                        }
                        this.stack.bcH().pop();
                        return false;
                    }
                    this.stack.bcH().pop();
                    return true;
                }
            } finally {
                this.stack.bcH().pop();
            }
        }

        /* renamed from: c */
        public boolean mo6678c(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
            this.stack.bcH().push();
            try {
                Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
                vec3f6.cross(vec3f2, vec3f3);
                Vec3f vec3f7 = (Vec3f) this.stack.bcH().get();
                vec3f7.cross(vec3f3, vec3f4);
                Vec3f vec3f8 = (Vec3f) this.stack.bcH().get();
                vec3f8.cross(vec3f4, vec3f2);
                if (vec3f6.dot(vec3f) > 1.0E-4f) {
                    vec3f5.set(vec3f6);
                    this.order = 2;
                    this.aBj[0].mo6682a(this.aBj[1]);
                    this.aBj[1].mo6682a(this.aBj[2]);
                    this.aBj[2].mo6682a(this.aBj[3]);
                    return mo6676b(vec3f, vec3f2, vec3f3, vec3f5);
                } else if (vec3f7.dot(vec3f) > 1.0E-4f) {
                    vec3f5.set(vec3f7);
                    this.order = 2;
                    this.aBj[2].mo6682a(this.aBj[3]);
                    boolean b = mo6676b(vec3f, vec3f3, vec3f4, vec3f5);
                    this.stack.bcH().pop();
                    return b;
                } else if (vec3f8.dot(vec3f) > 1.0E-4f) {
                    vec3f5.set(vec3f8);
                    this.order = 2;
                    this.aBj[1].mo6682a(this.aBj[0]);
                    this.aBj[0].mo6682a(this.aBj[2]);
                    this.aBj[2].mo6682a(this.aBj[3]);
                    boolean b2 = mo6676b(vec3f, vec3f4, vec3f2, vec3f5);
                    this.stack.bcH().pop();
                    return b2;
                } else {
                    this.stack.bcH().pop();
                    return true;
                }
            } finally {
                this.stack.bcH().pop();
            }
        }

        /* renamed from: MT */
        public boolean mo6671MT() {
            this.stack.bcH().push();
            try {
                return mo6681u(this.stack.bcH().mo4460h(1.0f, 0.0f, 0.0f));
            } finally {
                this.stack.bcH().pop();
            }
        }

        /* JADX INFO: finally extract failed */
        /* renamed from: u */
        public boolean mo6681u(Vec3f vec3f) {
            boolean c;
            this.stack.bcH().push();
            try {
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                this.iterations = 0;
                this.order = -1;
                this.failed = false;
                this.aBk.set(vec3f);
                this.aBk.normalize();
                Arrays.fill(this.aBf, (Object) null);
                mo6670MS();
                this.aBk.negate(this.aBj[0].f2085gv);
                while (true) {
                    if (this.iterations >= 128) {
                        this.failed = true;
                    } else {
                        float length = this.aBk.length();
                        Vec3f vec3f6 = this.aBk;
                        if (length <= 0.0f) {
                            length = 1.0f;
                        }
                        vec3f6.scale(1.0f / length);
                        if (mo6670MS()) {
                            switch (this.order) {
                                case 1:
                                    vec3f2.negate(this.aBj[1].f2085gv);
                                    vec3f3.sub(this.aBj[0].f2085gv, this.aBj[1].f2085gv);
                                    c = mo6680e(vec3f2, vec3f3);
                                    break;
                                case 2:
                                    vec3f2.negate(this.aBj[2].f2085gv);
                                    vec3f3.sub(this.aBj[1].f2085gv, this.aBj[2].f2085gv);
                                    vec3f4.sub(this.aBj[0].f2085gv, this.aBj[2].f2085gv);
                                    c = mo6677c(vec3f2, vec3f3, vec3f4);
                                    break;
                                case 3:
                                    vec3f2.negate(this.aBj[3].f2085gv);
                                    vec3f3.sub(this.aBj[2].f2085gv, this.aBj[3].f2085gv);
                                    vec3f4.sub(this.aBj[1].f2085gv, this.aBj[3].f2085gv);
                                    vec3f5.sub(this.aBj[0].f2085gv, this.aBj[3].f2085gv);
                                    c = mo6678c(vec3f2, vec3f3, vec3f4, vec3f5);
                                    break;
                                default:
                                    c = false;
                                    break;
                            }
                            if (c) {
                                this.stack.bcH().pop();
                                return true;
                            }
                            this.iterations++;
                        }
                    }
                }
                this.stack.bcH().pop();
                return false;
            } catch (Throwable th) {
                this.stack.bcH().pop();
                throw th;
            }
        }

        /* JADX INFO: finally extract failed */
        /* renamed from: MU */
        public boolean mo6672MU() {
            char c = 0;
            this.stack.bcF();
            this.stack.bcN().push();
            try {
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                switch (this.order) {
                    case 1:
                        Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                        vec3f4.sub(this.aBj[1].f2085gv, this.aBj[0].f2085gv);
                        Vec3f[] vec3fArr = {this.stack.bcH().mo4460h(1.0f, 0.0f, 0.0f), this.stack.bcH().mo4460h(0.0f, 1.0f, 0.0f), this.stack.bcH().mo4460h(0.0f, 0.0f, 1.0f)};
                        vec3fArr[0].cross(vec3f4, vec3fArr[0]);
                        vec3fArr[1].cross(vec3f4, vec3fArr[1]);
                        vec3fArr[2].cross(vec3f4, vec3fArr[2]);
                        float[] fArr = {vec3fArr[0].lengthSquared(), vec3fArr[1].lengthSquared(), vec3fArr[2].lengthSquared()};
                        Quat4f quat4f = (Quat4f) this.stack.bcN().get();
                        vec3f.normalize(vec3f4);
                        C1777aC.m13121a(quat4f, vec3f, 2.0943952f);
                        Matrix3fWrap ajd = (Matrix3fWrap) this.stack.bcK().get();
                        C3427rS.m38370a(ajd, quat4f);
                        C0988OY bcH = this.stack.bcH();
                        if (fArr[0] <= fArr[1]) {
                            c = fArr[1] > fArr[2] ? (char) 1 : 2;
                        } else if (fArr[0] <= fArr[2]) {
                            c = 2;
                        }
                        Vec3f ac = bcH.mo4458ac(vec3fArr[c]);
                        vec3f.normalize(ac);
                        mo6675a(vec3f, this.aBj[4]);
                        ajd.transform(ac);
                        vec3f.normalize(ac);
                        mo6675a(vec3f, this.aBj[2]);
                        ajd.transform(ac);
                        vec3f.normalize(ac);
                        mo6675a(vec3f, this.aBj[3]);
                        ajd.transform(ac);
                        this.order = 4;
                        break;
                    case 2:
                        vec3f2.sub(this.aBj[1].f2085gv, this.aBj[0].f2085gv);
                        vec3f3.sub(this.aBj[2].f2085gv, this.aBj[0].f2085gv);
                        Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                        vec3f5.cross(vec3f2, vec3f3);
                        vec3f5.normalize();
                        mo6675a(vec3f5, this.aBj[3]);
                        vec3f.negate(vec3f5);
                        mo6675a(vec3f, this.aBj[4]);
                        this.order = 4;
                        break;
                    case 3:
                    case 4:
                        break;
                    default:
                        this.stack.bcG();
                        this.stack.bcN().pop();
                        return false;
                }
                this.stack.bcG();
                this.stack.bcN().pop();
                return true;
            } catch (Throwable th) {
                this.stack.bcG();
                this.stack.bcN().pop();
                throw th;
            }
        }

        /* renamed from: a.X$a$b */
        /* compiled from: a */
        public static class C1571b {
            public final Vec3f aLZ = new Vec3f();
            public C1571b biv;
        }

        /* renamed from: a.X$a$a */
        public static class C1570a {

            /* renamed from: gv */
            public final Vec3f f2085gv = new Vec3f();

            /* renamed from: gw */
            public final Vec3f f2086gw = new Vec3f();

            /* renamed from: a */
            public void mo6682a(C1570a aVar) {
                this.f2085gv.set(aVar.f2085gv);
                this.f2086gw.set(aVar.f2086gw);
            }
        }
    }

    /* renamed from: a.X$c */
    /* compiled from: a */
    public static class C1573c {
        private static final int[][] dcY;
        private static final int[][] dcZ;
        private static final int[][] dda;
        private static final int[][] ddb;
        private static int[] dcX;

        static {
            int[] iArr = new int[5];
            iArr[1] = 1;
            iArr[2] = 2;
            iArr[4] = 1;
            dcX = iArr;
            int[] iArr2 = new int[3];
            iArr2[0] = 2;
            iArr2[1] = 1;
            int[] iArr3 = new int[3];
            iArr3[0] = 3;
            iArr3[2] = 1;
            int[] iArr4 = new int[3];
            iArr4[0] = 3;
            iArr4[1] = 2;
            dcY = new int[][]{iArr2, iArr3, new int[]{3, 1, 2}, iArr4};
            int[] iArr5 = new int[4];
            iArr5[2] = 2;
            iArr5[3] = 1;
            int[] iArr6 = new int[4];
            iArr6[1] = 1;
            iArr6[2] = 1;
            iArr6[3] = 1;
            int[] iArr7 = new int[4];
            iArr7[1] = 2;
            iArr7[2] = 3;
            iArr7[3] = 1;
            int[] iArr8 = new int[4];
            iArr8[0] = 1;
            iArr8[2] = 3;
            iArr8[3] = 2;
            int[] iArr9 = new int[4];
            iArr9[0] = 2;
            iArr9[2] = 1;
            iArr9[3] = 2;
            int[] iArr10 = new int[4];
            iArr10[0] = 3;
            iArr10[2] = 2;
            iArr10[3] = 2;
            dcZ = new int[][]{iArr5, iArr6, iArr7, iArr8, iArr9, iArr10};
            int[] iArr11 = new int[3];
            iArr11[0] = 2;
            iArr11[2] = 4;
            int[] iArr12 = new int[3];
            iArr12[0] = 1;
            iArr12[1] = 4;
            int[] iArr13 = new int[3];
            iArr13[1] = 3;
            iArr13[2] = 1;
            int[] iArr14 = new int[3];
            iArr14[1] = 2;
            iArr14[2] = 3;
            dda = new int[][]{iArr11, new int[]{4, 1, 2}, iArr12, iArr13, iArr14, new int[]{1, 3, 2}};
            int[] iArr15 = new int[4];
            iArr15[2] = 4;
            int[] iArr16 = new int[4];
            iArr16[1] = 1;
            iArr16[2] = 2;
            iArr16[3] = 1;
            int[] iArr17 = new int[4];
            iArr17[1] = 2;
            iArr17[2] = 1;
            iArr17[3] = 2;
            int[] iArr18 = new int[4];
            iArr18[0] = 1;
            iArr18[2] = 2;
            int[] iArr19 = new int[4];
            iArr19[0] = 3;
            iArr19[1] = 1;
            iArr19[2] = 5;
            int[] iArr20 = new int[4];
            iArr20[0] = 3;
            iArr20[2] = 4;
            iArr20[3] = 2;
            ddb = new int[][]{iArr15, iArr16, iArr17, new int[]{1, 1, 5, 2}, iArr18, new int[]{2, 2, 3, 2}, iArr19, iArr20, new int[]{5, 1, 4, 1}};
        }

        /* renamed from: Oj */
        public final Vec3f f2093Oj = new Vec3f();
        public final Vec3f[][] dcV = ((Vec3f[][]) Array.newInstance(Vec3f.class, new int[]{2, 3}));
        public final Vec3f[] dcW = {new Vec3f(), new Vec3f()};
        public final C0763Kt stack = C0763Kt.bcE();
        /* renamed from: Ok */
        public float f2094Ok;
        public int dcU;
        public C1569a eML;
        public C1574a eWY;
        public boolean failed;
        public int iterations;

        public C1573c(C1569a aVar) {
            for (int i = 0; i < this.dcV.length; i++) {
                for (int i2 = 0; i2 < this.dcV[i].length; i2++) {
                    this.dcV[i][i2] = new Vec3f();
                }
            }
            this.eML = aVar;
        }

        /* renamed from: a */
        public Vec3f mo6685a(C1574a aVar) {
            this.stack.bcH().push();
            try {
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                vec3f4.scale(-aVar.bak, aVar.baj);
                float[] qW = this.stack.bcO().mo1166qW(3);
                vec3f2.sub(aVar.dVH[0].f2085gv, vec3f4);
                vec3f3.sub(aVar.dVH[1].f2085gv, vec3f4);
                vec3f.cross(vec3f2, vec3f3);
                qW[0] = vec3f.length();
                vec3f2.sub(aVar.dVH[1].f2085gv, vec3f4);
                vec3f3.sub(aVar.dVH[2].f2085gv, vec3f4);
                vec3f.cross(vec3f2, vec3f3);
                qW[1] = vec3f.length();
                vec3f2.sub(aVar.dVH[2].f2085gv, vec3f4);
                vec3f3.sub(aVar.dVH[0].f2085gv, vec3f4);
                vec3f.cross(vec3f2, vec3f3);
                qW[2] = vec3f.length();
                float f = qW[0] + qW[1] + qW[2];
                Vec3f h = this.stack.bcH().mo4460h(qW[1], qW[2], qW[0]);
                if (f <= 0.0f) {
                    f = 1.0f;
                }
                h.scale(1.0f / f);
                this.stack.bcO().release(qW);
                return (Vec3f) this.stack.bcH().mo15197aq(h);
            } finally {
                this.stack.bcH().pop();
            }
        }

        public C1574a bMK() {
            C1574a aVar = null;
            if (this.eWY != null) {
                C1574a aVar2 = this.eWY;
                float f = Float.MAX_VALUE;
                do {
                    if (aVar2.bak < f) {
                        f = aVar2.bak;
                        aVar = aVar2;
                    }
                    aVar2 = aVar2.dVK;
                } while (aVar2 != null);
            }
            return aVar;
        }

        /* renamed from: a */
        public boolean mo6687a(C1574a aVar, C1569a.C1570a aVar2, C1569a.C1570a aVar3, C1569a.C1570a aVar4) {
            this.stack.bcH().push();
            try {
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                vec3f.sub(aVar3.f2085gv, aVar2.f2085gv);
                vec3f2.sub(aVar4.f2085gv, aVar2.f2085gv);
                vec3f4.cross(vec3f, vec3f2);
                float length = vec3f4.length();
                vec3f.cross(aVar2.f2085gv, aVar3.f2085gv);
                vec3f2.cross(aVar3.f2085gv, aVar4.f2085gv);
                vec3f3.cross(aVar4.f2085gv, aVar2.f2085gv);
                boolean z = vec3f.dot(vec3f4) >= -0.01f && vec3f2.dot(vec3f4) >= -0.01f && vec3f3.dot(vec3f4) >= -0.01f;
                aVar.dVH[0] = aVar2;
                aVar.dVH[1] = aVar3;
                aVar.dVH[2] = aVar4;
                aVar.mark = 0;
                aVar.baj.scale(1.0f / (length > 0.0f ? length : Float.MAX_VALUE), vec3f4);
                aVar.bak = Math.max(0.0f, -aVar.baj.dot(aVar2.f2085gv));
                return z;
            } finally {
                this.stack.bcH().pop();
            }
        }

        /* renamed from: a */
        public C1574a mo6684a(C1569a.C1570a aVar, C1569a.C1570a aVar2, C1569a.C1570a aVar3) {
            C1574a aVar4 = C1568X.f2073Ai.get();
            if (mo6687a(aVar4, aVar, aVar2, aVar3)) {
                if (this.eWY != null) {
                    this.eWY.dVJ = aVar4;
                }
                aVar4.dVJ = null;
                aVar4.dVK = this.eWY;
                this.eWY = aVar4;
                this.dcU++;
            } else {
                aVar4.dVK = null;
                aVar4.dVJ = null;
            }
            return aVar4;
        }

        /* renamed from: b */
        public void mo6690b(C1574a aVar) {
            if (aVar.dVJ != null || aVar.dVK != null) {
                this.dcU--;
                if (aVar == this.eWY) {
                    this.eWY = aVar.dVK;
                    this.eWY.dVJ = null;
                } else if (aVar.dVK == null) {
                    aVar.dVJ.dVK = null;
                } else {
                    aVar.dVJ.dVK = aVar.dVK;
                    aVar.dVK.dVJ = aVar.dVJ;
                }
                aVar.dVK = null;
                aVar.dVJ = null;
            }
        }

        /* renamed from: a */
        public void mo6686a(C1574a aVar, int i, C1574a aVar2, int i2) {
            aVar.dVI[i] = aVar2;
            aVar2.bai[i2] = i;
            aVar2.dVI[i2] = aVar;
            aVar.bai[i] = i2;
        }

        /* renamed from: ar */
        public C1569a.C1570a mo6689ar(Vec3f vec3f) {
            C1569a.C1570a aVar = C1568X.f2071Ag.get();
            this.eML.mo6675a(vec3f, aVar);
            return aVar;
        }

        /* renamed from: a */
        public int mo6683a(int i, C1569a.C1570a aVar, C1574a aVar2, int i2, C1574a[] aVarArr, C1574a[] aVarArr2) {
            if (aVar2.mark == i) {
                return 0;
            }
            int i3 = dcX[i2 + 1];
            if (aVar2.baj.dot(aVar.f2085gv) + aVar2.bak > 0.0f) {
                C1574a a = mo6684a(aVar2.dVH[i3], aVar2.dVH[i2], aVar);
                mo6686a(a, 0, aVar2, i2);
                if (aVarArr[0] != null) {
                    mo6686a(aVarArr[0], 1, a, 2);
                } else {
                    aVarArr2[0] = a;
                }
                aVarArr[0] = a;
                return 1;
            }
            int i4 = dcX[i2 + 2];
            mo6690b(aVar2);
            aVar2.mark = i;
            return mo6683a(i, aVar, aVar2.dVI[i4], aVar2.bai[i4], aVarArr, aVarArr2) + 0 + mo6683a(i, aVar, aVar2.dVI[i3], aVar2.bai[i3], aVarArr, aVarArr2);
        }

        public float aSb() {
            return mo6692fu(C1568X.f2084At);
        }

        /* renamed from: fu */
        public float mo6692fu(float f) {
            C1574a aVar;
            float f2;
            C1574a bMK;
            int[][] iArr;
            int[][] iArr2;
            this.stack.bcH().push();
            C1568X.m11406kh();
            try {
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                this.f2094Ok = -3.4028235E38f;
                this.f2093Oj.set(0.0f, 0.0f, 0.0f);
                this.eWY = null;
                this.dcU = 0;
                this.iterations = 0;
                this.failed = false;
                if (this.eML.mo6672MU()) {
                    int[][] iArr3 = null;
                    int i = 0;
                    int i2 = 0;
                    int[][] iArr4 = null;
                    int i3 = 0;
                    int i4 = 0;
                    C1569a.C1570a[] aVarArr = new C1569a.C1570a[5];
                    C1574a[] aVarArr2 = new C1574a[6];
                    switch (this.eML.order) {
                        case 3:
                            iArr = dcY;
                            i = 0;
                            i2 = 4;
                            iArr2 = dcZ;
                            i4 = 6;
                            i3 = 0;
                            break;
                        case 4:
                            iArr = dda;
                            i = 0;
                            i2 = 6;
                            iArr2 = ddb;
                            i4 = 9;
                            i3 = 0;
                            break;
                        default:
                            iArr2 = iArr4;
                            iArr = iArr3;
                            break;
                    }
                    for (int i5 = 0; i5 <= this.eML.order; i5++) {
                        aVarArr[i5] = new C1569a.C1570a();
                        aVarArr[i5].mo6682a(this.eML.aBj[i5]);
                    }
                    int i6 = 0;
                    while (i6 < i2) {
                        aVarArr2[i6] = mo6684a(aVarArr[iArr[i][0]], aVarArr[iArr[i][1]], aVarArr[iArr[i][2]]);
                        i6++;
                        i++;
                    }
                    int i7 = 0;
                    while (i7 < i4) {
                        mo6686a(aVarArr2[iArr2[i3][0]], iArr2[i3][1], aVarArr2[iArr2[i3][2]], iArr2[i3][3]);
                        i7++;
                        i3++;
                    }
                }
                if (this.dcU == 0) {
                    f2 = this.f2094Ok;
                } else {
                    int i8 = 1;
                    C1574a aVar2 = null;
                    while (true) {
                        if (this.iterations < 256 && (bMK = bMK()) != null) {
                            vec3f.negate(bMK.baj);
                            C1569a.C1570a ar = mo6689ar(vec3f);
                            if (bMK.baj.dot(ar.f2085gv) + bMK.bak < (-f)) {
                                C1574a[] aVarArr3 = new C1574a[1];
                                C1574a[] aVarArr4 = new C1574a[1];
                                mo6690b(bMK);
                                i8++;
                                bMK.mark = i8;
                                int i9 = 0;
                                int i10 = 0;
                                while (i9 < 3) {
                                    i9++;
                                    i10 += mo6683a(i8, ar, bMK.dVI[i9], bMK.bai[i9], aVarArr3, aVarArr4);
                                }
                                if (i10 <= 2) {
                                    aVar = bMK;
                                } else {
                                    mo6686a(aVarArr3[0], 1, aVarArr4[0], 2);
                                    this.iterations++;
                                    aVar2 = bMK;
                                }
                            } else {
                                aVar = bMK;
                            }
                        }
                    }
                    aVar = aVar2;
                    if (aVar != null) {
                        Vec3f ac = this.stack.bcH().mo4458ac(mo6685a(aVar));
                        this.f2093Oj.set(aVar.baj);
                        this.f2094Ok = Math.max(0.0f, aVar.bak);
                        int i11 = 0;
                        while (i11 < 2) {
                            float f3 = i11 != 0 ? -1.0f : 1.0f;
                            for (int i12 = 0; i12 < 3; i12++) {
                                vec3f.scale(f3, aVar.dVH[i12].f2086gw);
                                this.dcV[i11][i12].set(this.eML.mo6673a(vec3f, i11));
                            }
                            i11++;
                        }
                        Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                        Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                        Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                        vec3f2.scale(ac.x, this.dcV[0][0]);
                        vec3f3.scale(ac.y, this.dcV[0][1]);
                        vec3f4.scale(ac.z, this.dcV[0][2]);
                        C0647JL.m5602f(this.dcW[0], vec3f2, vec3f3, vec3f4);
                        vec3f2.scale(ac.x, this.dcV[1][0]);
                        vec3f3.scale(ac.y, this.dcV[1][1]);
                        vec3f4.scale(ac.z, this.dcV[1][2]);
                        C0647JL.m5602f(this.dcW[1], vec3f2, vec3f3, vec3f4);
                    } else {
                        this.failed = true;
                    }
                    f2 = this.f2094Ok;
                    this.stack.bcH().pop();
                    C1568X.m11407ki();
                }
                return f2;
            } finally {
                this.stack.bcH().pop();
                C1568X.m11407ki();
            }
        }

        /* renamed from: a.X$c$a */
        public static class C1574a {
            public final int[] bai = new int[3];
            public final Vec3f baj = new Vec3f();
            public final C1569a.C1570a[] dVH = new C1569a.C1570a[3];
            public final C1574a[] dVI = new C1574a[3];
            public float bak;
            public C1574a dVJ;
            public C1574a dVK;
            public int mark;
        }
    }
}
