package p001a;

import game.script.corporation.Corporation;
import game.script.corporation.CorporationPermissionDefaults;
import game.script.corporation.CorporationRoleInfo;
import game.script.player.Player;
import logic.swing.C5378aHa;
import logic.ui.C2698il;
import logic.ui.item.GBox;
import logic.ui.item.Picture;
import logic.ui.item.Repeater;
import logic.ui.item.TextField;
import taikodom.addon.IAddonProperties;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/* renamed from: a.TH */
/* compiled from: a */
public class C1307TH extends C1023Ov {
    private static final int iaR = 18;
    private static final int iaS = 32;
    /* access modifiers changed from: private */
    public C2698il edQ;

    public C1307TH(IAddonProperties vWVar, C2698il ilVar, Player aku, Corporation aPVar) {
        super(vWVar, aku, aPVar);
        this.edQ = ilVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: Fa */
    public void mo4555Fa() {
        this.edQ.mo4913cb("resetRoles").addActionListener(new C1308a());
        this.edQ.mo4913cb("resetPermissions").addActionListener(new C1309b());
    }

    /* access modifiers changed from: package-private */
    public void blJ() {
        deb().mo22250a(new C1310c());
        Repeater<C5741aUz> dec = dec();
        if (dec != null) {
            dec.mo22250a((Repeater.C3671a<C5741aUz>) new C1313d());
        }
        ded().mo22250a(new C1314e());
    }

    /* access modifiers changed from: package-private */
    public void removeListeners() {
    }

    /* access modifiers changed from: package-private */
    public void refresh() {
        mo4562c(this.edQ);
        if (!this.dLu.mo10718b(this.f1342P, C6704asA.EDIT_ROLE_NAMES) || this.dLu.mo10718b(this.f1342P, C6704asA.EDIT_ROLE_FUNCTION_DESCRIPTION)) {
        }
        this.edQ.mo4913cb("resetRoles").setEnabled(false);
        deb().clear();
        for (C5741aUz G : C5741aUz.values()) {
            deb().mo22248G(G);
        }
        Repeater<C5741aUz> dec = dec();
        dec.clear();
        Repeater<C6704asA> ded = ded();
        ded.clear();
        this.edQ.mo4915cd("permission-table").setColumns(1);
        for (C5741aUz G2 : C5741aUz.values()) {
            dec.mo22248G(G2);
        }
        for (C6704asA G3 : C6704asA.values()) {
            ded.mo22248G(G3);
        }
        this.dLu.mo10718b(this.f1342P, C6704asA.EDIT_ROLE_PERMISSIONS);
        this.edQ.mo4913cb("resetPermissions").setEnabled(false);
    }

    private Repeater<C5741aUz> deb() {
        return this.edQ.mo4919ch("levels");
    }

    private Repeater<C5741aUz> dec() {
        return this.edQ.mo4919ch("permissionIcons");
    }

    private Repeater<C6704asA> ded() {
        return this.edQ.mo4919ch("permissions");
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9894a(C5741aUz auz, C6704asA asa, Corporation aPVar, Player aku, Component component) {
        if (component instanceof C2698il) {
            JCheckBox cd = ((C2698il) component).mo4915cd("permissionFlag");
            cd.setSelected(aPVar.mo10711b(auz).mo12095f(asa));
            cd.setEnabled(aPVar.mo10695I(aku).mo12095f(C6704asA.EDIT_ROLE_PERMISSIONS));
            cd.addActionListener(new C1316f(aPVar, auz, asa, cd));
            if (auz == C5741aUz.dAq() && asa.cuB()) {
                cd.setEnabled(false);
                cd.setVisible(false);
            }
            if (auz == C5741aUz.dAr()) {
                cd.setEnabled(false);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
    }

    /* renamed from: a.TH$a */
    class C1308a implements ActionListener {
        C1308a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C1307TH.this.dLu.mo10700QI();
        }
    }

    /* renamed from: a.TH$b */
    /* compiled from: a */
    class C1309b implements ActionListener {
        C1309b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C1307TH.this.dLu.mo10701QK();
        }
    }

    /* renamed from: a.TH$c */
    /* compiled from: a */
    class C1310c implements Repeater.C3671a<C5741aUz> {
        C1310c() {
        }

        /* renamed from: a */
        public void mo843a(C5741aUz auz, Component component) {
            CorporationRoleInfo b = C1307TH.this.dLu.mo10711b(auz);
            if ("levelIcon".equals(component.getName())) {
                ((Picture) component).mo16824a(C5378aHa.ICONOGRAPHY, C1307TH.this.mo4556a(C1307TH.this.dLu, auz));
            } else if ("levelRole".equals(component.getName())) {
                logic.ui.item.TextField ahw = (logic.ui.item.TextField) component;
                ahw.setText(b.getRoleName());
                ahw.mo44h(18);
                boolean b2 = C1307TH.this.dLu.mo10718b(C1307TH.this.f1342P, C6704asA.EDIT_ROLE_NAMES);
                ahw.setEnabled(b2);
                if (b2) {
                    ahw.addFocusListener(new C1312b(ahw, auz));
                }
            } else if ("levelFunction".equals(component.getName())) {
                logic.ui.item.TextField ahw2 = (logic.ui.item.TextField) component;
                ahw2.setText(b.dEe());
                ahw2.mo44h(32);
                boolean b3 = C1307TH.this.dLu.mo10718b(C1307TH.this.f1342P, C6704asA.EDIT_ROLE_FUNCTION_DESCRIPTION);
                ahw2.setEnabled(b3);
                if (b3) {
                    ahw2.addFocusListener(new C1311a(ahw2, auz));
                }
            }
        }

        /* renamed from: a.TH$c$b */
        /* compiled from: a */
        class C1312b implements FocusListener {
            private final /* synthetic */ C5741aUz ekL;
            private final /* synthetic */ logic.ui.item.TextField gCz;

            C1312b(logic.ui.item.TextField ahw, C5741aUz auz) {
                this.gCz = ahw;
                this.ekL = auz;
            }

            public void focusGained(FocusEvent focusEvent) {
            }

            public void focusLost(FocusEvent focusEvent) {
                if (this.gCz.getText() != null) {
                    C1307TH.this.dLu.mo10713b(this.ekL, this.gCz.getText().trim());
                }
            }
        }

        /* renamed from: a.TH$c$a */
        class C1311a implements FocusListener {
            private final /* synthetic */ C5741aUz ekL;
            private final /* synthetic */ logic.ui.item.TextField gCy;

            C1311a(TextField ahw, C5741aUz auz) {
                this.gCy = ahw;
                this.ekL = auz;
            }

            public void focusGained(FocusEvent focusEvent) {
            }

            public void focusLost(FocusEvent focusEvent) {
                if (this.gCy.getText() != null) {
                    C1307TH.this.dLu.mo10722d(this.ekL, this.gCy.getText().trim());
                }
            }
        }
    }

    /* renamed from: a.TH$d */
    /* compiled from: a */
    class C1313d implements Repeater.C3671a<C5741aUz> {
        C1313d() {
        }

        /* renamed from: a */
        public void mo843a(C5741aUz auz, Component component) {
            GBox cd = C1307TH.this.edQ.mo4915cd("permission-table");
            cd.setColumns(cd.getColumns() + 1);
            ((Picture) component).mo16824a(C5378aHa.ICONOGRAPHY, C1307TH.this.mo4556a(C1307TH.this.dLu, auz));
        }
    }

    /* renamed from: a.TH$e */
    /* compiled from: a */
    class C1314e implements Repeater.C3671a<C6704asA> {
        C1314e() {
        }

        /* renamed from: a */
        public void mo843a(C6704asA asa, Component component) {
            CorporationPermissionDefaults b = C1307TH.this.f1343kj.ala().aJe().mo19009xb().mo23b(asa);
            if ("permissionName".equals(component.getName())) {
                JLabel jLabel = (JLabel) component;
                jLabel.setText(b.mo16645ke().get());
                jLabel.setToolTipText(b.mo16646vW().get());
            } else if ("permissionFlags".equals(component.getName())) {
                Repeater tYVar = (Repeater) component;
                tYVar.mo22250a(new C1315a(asa));
                tYVar.clear();
                for (C5741aUz G : C5741aUz.values()) {
                    tYVar.mo22248G(G);
                }
            }
        }

        /* renamed from: a.TH$e$a */
        class C1315a implements Repeater.C3671a<C5741aUz> {
            private final /* synthetic */ C6704asA ekM;

            C1315a(C6704asA asa) {
                this.ekM = asa;
            }

            /* renamed from: a */
            public void mo843a(C5741aUz auz, Component component) {
                C1307TH.this.m9894a(auz, this.ekM, C1307TH.this.dLu, C1307TH.this.f1342P, component);
            }
        }
    }

    /* renamed from: a.TH$f */
    /* compiled from: a */
    class C1316f implements ActionListener {
        private final /* synthetic */ Corporation aTb;
        private final /* synthetic */ C5741aUz ekL;
        private final /* synthetic */ C6704asA ekM;
        private final /* synthetic */ JCheckBox ekN;

        C1316f(Corporation aPVar, C5741aUz auz, C6704asA asa, JCheckBox jCheckBox) {
            this.aTb = aPVar;
            this.ekL = auz;
            this.ekM = asa;
            this.ekN = jCheckBox;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            this.aTb.mo10712b(this.ekL, this.ekM, this.ekN.isSelected());
        }
    }
}
