package p001a;

import logic.baa.C1616Xf;
import logic.bbb.aDR;
import logic.res.html.C2491fm;
import logic.res.html.DataClassSerializer;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: a.Pb */
/* compiled from: a */
public final class C1066Pb extends C1720ZS implements C2491fm {

    private final Class<?> agS;
    private final boolean editable;
    private final boolean hUU;
    private final C3248pc.C3250b hUV;
    private final C3248pc.C3249a hUW;
    private final DataClassSerializer[] hUX;
    private final Object hUY;
    private final C6580apg hUZ;
    private final int hVa;
    private final int hVb;
    private final int hVc;
    private final boolean hVd;
    private final boolean hVe;
    private final boolean hVf;
    private final boolean hVg;
    private final boolean hVh;
    private final boolean hVi;
    private final boolean hVj;
    private final boolean hVk;
    private final boolean hVl;
    private final boolean hVm;
    private final boolean hVn;
    private final boolean hVo;
    private final boolean hVp;
    private final Class<?> returnType;
    private String exL;
    private AtomicInteger hVA;
    private AtomicInteger hVB;
    private AtomicInteger hVC;
    private AtomicInteger hVD;
    private AtomicInteger hVE;
    private AtomicInteger hVF;
    private AtomicInteger hVG;
    private AtomicInteger hVH;
    private AtomicInteger hVI;
    private AtomicInteger hVJ;
    private AtomicInteger hVK;
    private AtomicInteger hVL;
    private boolean hVM;
    private Type hVN;
    private boolean hVO;
    private AtomicLong hVq;
    private AtomicInteger hVr;
    private AtomicLong hVs;
    private AtomicInteger hVt;
    private AtomicInteger hVu;
    private AtomicInteger hVv;
    private AtomicInteger hVw;
    private AtomicInteger hVx;
    private AtomicInteger hVy;
    private AtomicInteger hVz;
    private Class<?>[] parameterTypes;

    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(Unknown Source)
        	at java.util.ArrayList.get(Unknown Source)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b0  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0194  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0197  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x019d  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x01a0  */
    public C1066Pb(java.lang.reflect.Method r16, java.lang.Class<?> r17, java.lang.String r18, boolean r19) {
        /*
            r15 = this;
            r15.<init>()
            r0 = r19
            r15.eRG = r0
            java.lang.Class<a.Am> r2 = p001a.C0064Am.class
            r0 = r16
            java.lang.annotation.Annotation r2 = r0.getAnnotation(r2)
            a.Am r2 = (p001a.C0064Am) r2
            java.lang.Class<a.pc> r3 = p001a.C3248pc.class
            r0 = r16
            java.lang.annotation.Annotation r3 = r0.getAnnotation(r3)
            a.pc r3 = (p001a.C3248pc) r3
            if (r3 == 0) goto L_0x016b
            java.lang.String r4 = ""
            java.lang.String r5 = r3.displayName()
            boolean r4 = r4.equals(r5)
            if (r4 != 0) goto L_0x016b
            r4 = 1
        L_0x002a:
            r15.editable = r4
            if (r3 == 0) goto L_0x016e
            a.pc$b r4 = r3.aYR()
            r15.hUV = r4
            a.pc$a r4 = r3.aYT()
            r15.hUW = r4
        L_0x003a:
            r4 = 0
            r15.hVl = r4
            r4 = 0
            r15.hVd = r4
            r4 = 0
            r15.hVh = r4
            java.lang.Class r4 = r16.getDeclaringClass()
            r15.agS = r4
            java.lang.String r4 = r16.getName()
            r15.name = r4
            int r2 = r2.aum()
            r15.eRF = r2
            r0 = r18
            r15.cUW = r0
            java.lang.Class[] r4 = r16.getParameterTypes()
            int r2 = r4.length
            a.Kd[] r2 = new logic.res.html.C0744Kd[r2]
            r15.hUX = r2
            r2 = 0
        L_0x0063:
            int r5 = r4.length
            if (r2 < r5) goto L_0x0178
            java.lang.Class<a.XL> r2 = p001a.C1594XL.class
            r0 = r16
            java.lang.annotation.Annotation r2 = r0.getAnnotation(r2)
            if (r2 == 0) goto L_0x0187
            r2 = 1
            r4 = r2
        L_0x0072:
            java.lang.Class<a.aOg> r2 = p001a.C5566aOg.class
            r0 = r16
            java.lang.annotation.Annotation r2 = r0.getAnnotation(r2)
            if (r2 == 0) goto L_0x018b
            r2 = 1
        L_0x007d:
            r15.eRH = r2
            java.lang.Class<a.xb> r2 = p001a.C3974xb.class
            r0 = r16
            java.lang.annotation.Annotation r2 = r0.getAnnotation(r2)
            if (r2 == 0) goto L_0x018e
            r2 = 1
        L_0x008a:
            r15.eRL = r2
            boolean r2 = r15.eRL
            if (r2 == 0) goto L_0x0191
            java.lang.Class<a.xb> r2 = p001a.C3974xb.class
            r0 = r16
            java.lang.annotation.Annotation r2 = r0.getAnnotation(r2)
            a.xb r2 = (p001a.C3974xb) r2
            boolean r2 = r2.mo22945pZ()
            if (r2 == 0) goto L_0x0191
            r2 = 1
        L_0x00a1:
            r15.hVO = r2
            r15.m8517f(r16)
            java.lang.Class<a.fr> r2 = p001a.C2499fr.class
            r0 = r16
            java.lang.annotation.Annotation r2 = r0.getAnnotation(r2)
            if (r2 == 0) goto L_0x0194
            r2 = 1
        L_0x00b1:
            r15.eRJ = r2
            java.lang.Class<a.yP> r2 = p001a.C4034yP.class
            r0 = r16
            java.lang.annotation.Annotation r2 = r0.getAnnotation(r2)
            if (r2 == 0) goto L_0x0197
            r2 = 1
        L_0x00be:
            r15.eRK = r2
            java.lang.Class<a.apg> r2 = p001a.C6580apg.class
            r0 = r16
            java.lang.annotation.Annotation r2 = r0.getAnnotation(r2)
            a.apg r2 = (p001a.C6580apg) r2
            r15.hUZ = r2
            java.lang.Class<a.fr> r2 = p001a.C2499fr.class
            r0 = r16
            java.lang.annotation.Annotation r2 = r0.getAnnotation(r2)
            a.fr r2 = (p001a.C2499fr) r2
            r15.eRM = r2
            if (r2 == 0) goto L_0x019a
            r2 = 1
        L_0x00db:
            r15.eRJ = r2
            java.lang.Class<a.ayk> r2 = p001a.C6997ayk.class
            r0 = r16
            java.lang.annotation.Annotation r2 = r0.getAnnotation(r2)
            if (r2 == 0) goto L_0x019d
            r2 = 1
        L_0x00e8:
            r15.hUU = r2
            java.lang.Class<a.Fa> r2 = p001a.C0401Fa.class
            r0 = r16
            java.lang.annotation.Annotation r2 = r0.getAnnotation(r2)
            if (r2 == 0) goto L_0x01a0
            r2 = 1
        L_0x00f5:
            r15.hVM = r2
            java.lang.Class r2 = r16.getReturnType()
            r15.returnType = r2
            java.lang.Class<a.m> r2 = p001a.C2950m.class
            r0 = r16
            java.lang.annotation.Annotation r2 = r0.getAnnotation(r2)
            a.m r2 = (p001a.C2950m) r2
            r15.eRT = r2
            java.util.HashMap r5 = eRE
            monitor-enter(r5)
            java.util.HashMap r2 = eRE     // Catch:{ all -> 0x01a3 }
            r0 = r18
            r2.put(r0, r15)     // Catch:{ all -> 0x01a3 }
            monitor-exit(r5)     // Catch:{ all -> 0x01a3 }
            java.lang.Class<?> r2 = r15.returnType
            boolean r2 = r2.isPrimitive()
            if (r2 == 0) goto L_0x0207
            java.lang.Class<?> r2 = r15.returnType
            java.lang.Class r5 = java.lang.Boolean.TYPE
            if (r2 != r5) goto L_0x01a6
            r2 = 0
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r15.hUY = r2
        L_0x0129:
            r6 = 0
            boolean r2 = r15.mo7389hk()
            if (r2 == 0) goto L_0x020c
            r12 = 1
            r14 = 1
            boolean r2 = r15.mo7392hn()
            if (r2 == 0) goto L_0x034b
            r2 = 1
            r13 = 1
            boolean r5 = r15.mo7390hl()
            if (r5 != 0) goto L_0x0343
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "@ClientOnly @Replicated methods must be @Event: "
            r3.<init>(r4)
            java.lang.Class r4 = r16.getClass()
            java.lang.String r4 = r4.getName()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = " -> "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r16.getName()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3)
            throw r2
        L_0x016b:
            r4 = 0
            goto L_0x002a
        L_0x016e:
            a.pc$b r4 = p001a.C3248pc.C3250b.NONE
            r15.hUV = r4
            a.pc$a r4 = p001a.C3248pc.C3249a.PLAIN
            r15.hUW = r4
            goto L_0x003a
        L_0x0178:
            a.Kd[] r5 = r15.hUX
            r6 = r4[r2]
            r7 = 0
            a.Kd r6 = p001a.C5726aUk.m18699b(r6, r7)
            r5[r2] = r6
            int r2 = r2 + 1
            goto L_0x0063
        L_0x0187:
            r2 = 0
            r4 = r2
            goto L_0x0072
        L_0x018b:
            r2 = 0
            goto L_0x007d
        L_0x018e:
            r2 = 0
            goto L_0x008a
        L_0x0191:
            r2 = 0
            goto L_0x00a1
        L_0x0194:
            r2 = 0
            goto L_0x00b1
        L_0x0197:
            r2 = 0
            goto L_0x00be
        L_0x019a:
            r2 = 0
            goto L_0x00db
        L_0x019d:
            r2 = 0
            goto L_0x00e8
        L_0x01a0:
            r2 = 0
            goto L_0x00f5
        L_0x01a3:
            r2 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x01a3 }
            throw r2
        L_0x01a6:
            java.lang.Class<?> r2 = r15.returnType
            java.lang.Class r5 = java.lang.Integer.TYPE
            if (r2 != r5) goto L_0x01b5
            r2 = 0
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r15.hUY = r2
            goto L_0x0129
        L_0x01b5:
            java.lang.Class<?> r2 = r15.returnType
            java.lang.Class r5 = java.lang.Long.TYPE
            if (r2 != r5) goto L_0x01c5
            r6 = 0
            java.lang.Long r2 = java.lang.Long.valueOf(r6)
            r15.hUY = r2
            goto L_0x0129
        L_0x01c5:
            java.lang.Class<?> r2 = r15.returnType
            java.lang.Class r5 = java.lang.Short.TYPE
            if (r2 != r5) goto L_0x01d4
            r2 = 0
            java.lang.Short r2 = java.lang.Short.valueOf(r2)
            r15.hUY = r2
            goto L_0x0129
        L_0x01d4:
            java.lang.Class<?> r2 = r15.returnType
            java.lang.Class r5 = java.lang.Byte.TYPE
            if (r2 != r5) goto L_0x01e3
            r2 = 0
            java.lang.Byte r2 = java.lang.Byte.valueOf(r2)
            r15.hUY = r2
            goto L_0x0129
        L_0x01e3:
            java.lang.Class<?> r2 = r15.returnType
            java.lang.Class r5 = java.lang.Float.TYPE
            if (r2 != r5) goto L_0x01f2
            r2 = 0
            java.lang.Float r2 = java.lang.Float.valueOf(r2)
            r15.hUY = r2
            goto L_0x0129
        L_0x01f2:
            java.lang.Class<?> r2 = r15.returnType
            java.lang.Class r5 = java.lang.Double.TYPE
            if (r2 != r5) goto L_0x0202
            r6 = 0
            java.lang.Double r2 = java.lang.Double.valueOf(r6)
            r15.hUY = r2
            goto L_0x0129
        L_0x0202:
            r2 = 0
            r15.hUY = r2
            goto L_0x0129
        L_0x0207:
            r2 = 0
            r15.hUY = r2
            goto L_0x0129
        L_0x020c:
            boolean r2 = r15.mo7393ho()
            if (r2 == 0) goto L_0x02b7
            r11 = 1
            r13 = 1
            a.fr r2 = r15.eRM
            if (r2 == 0) goto L_0x033a
            r9 = 1
            a.fr r2 = r15.eRM
            int r2 = r2.mo18857qh()
            r2 = r2 & 1
            if (r2 == 0) goto L_0x02b1
            r2 = 1
        L_0x0224:
            a.fr r5 = r15.eRM
            int r5 = r5.mo18857qh()
            r5 = r5 & 2
            if (r5 == 0) goto L_0x02b4
            r5 = 1
        L_0x022f:
            r7 = r6
            r8 = r6
            r10 = r6
            r12 = r6
            r14 = r2
        L_0x0234:
            java.lang.Class<a.ze> r2 = p001a.C4114ze.class
            r0 = r16
            java.lang.annotation.Annotation r2 = r0.getAnnotation(r2)
            if (r2 == 0) goto L_0x0241
            r12 = 1
            r10 = 1
            r9 = 1
        L_0x0241:
            boolean r2 = r15.mo7390hl()
            if (r2 != 0) goto L_0x0249
            if (r4 == 0) goto L_0x0314
        L_0x0249:
            r2 = 0
        L_0x024a:
            r15.hVp = r2
            r15.hVe = r14
            r15.hVg = r13
            r15.hVf = r5
            r15.hVi = r12
            r15.hVk = r11
            r15.hVj = r7
            r15.hVm = r10
            r15.hVo = r9
            r15.hVn = r8
            if (r14 == 0) goto L_0x0317
            if (r12 == 0) goto L_0x0317
            r2 = 1
            r4 = r2
        L_0x0264:
            if (r14 == 0) goto L_0x031b
            if (r9 == 0) goto L_0x031b
            r2 = 2
        L_0x0269:
            r2 = r2 | r4
            r15.hVa = r2
            if (r14 == 0) goto L_0x0270
            if (r12 != 0) goto L_0x0274
        L_0x0270:
            if (r5 == 0) goto L_0x031e
            if (r7 == 0) goto L_0x031e
        L_0x0274:
            r2 = 1
            r4 = r2
        L_0x0276:
            if (r14 != 0) goto L_0x027a
            if (r5 == 0) goto L_0x0322
        L_0x027a:
            if (r9 == 0) goto L_0x0322
            r2 = 2
        L_0x027d:
            r2 = r2 | r4
            r15.hVb = r2
            if (r13 == 0) goto L_0x0325
            if (r11 == 0) goto L_0x0325
            boolean r2 = r15.mo4793py()
            if (r2 != 0) goto L_0x0325
            r2 = 1
            r4 = r2
        L_0x028c:
            if (r13 == 0) goto L_0x0329
            if (r10 != 0) goto L_0x0298
            boolean r2 = r15.mo4793py()
            if (r2 != 0) goto L_0x0298
            if (r8 == 0) goto L_0x0329
        L_0x0298:
            r2 = 2
        L_0x0299:
            r2 = r2 | r4
            r15.hVc = r2
            if (r3 == 0) goto L_0x02a4
            java.lang.String r2 = r3.displayName()
            r15.exL = r2
        L_0x02a4:
            java.lang.Class[] r2 = r16.getParameterTypes()
            r15.parameterTypes = r2
            java.lang.reflect.Type r2 = r16.getGenericReturnType()
            r15.hVN = r2
            return
        L_0x02b1:
            r2 = 0
            goto L_0x0224
        L_0x02b4:
            r5 = 0
            goto L_0x022f
        L_0x02b7:
            if (r4 == 0) goto L_0x02c5
            r12 = 1
            r14 = 1
            r10 = 1
            r2 = 1
            r5 = r6
            r7 = r6
            r8 = r6
            r9 = r2
            r11 = r6
            r13 = r6
            goto L_0x0234
        L_0x02c5:
            boolean r2 = r15.mo7394hp()
            if (r2 == 0) goto L_0x0308
            r2 = 1
            r5 = 1
            r12 = 1
            r14 = 1
            boolean r7 = r15.mo7392hn()
            if (r7 == 0) goto L_0x0332
            r8 = 1
            r13 = 1
            boolean r7 = r15.mo7390hl()
            if (r7 != 0) goto L_0x032c
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "@Offloaded @Replicated methods must be @Event: "
            r3.<init>(r4)
            java.lang.Class r4 = r16.getClass()
            java.lang.String r4 = r4.getName()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = " -> "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r16.getName()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3)
            throw r2
        L_0x0308:
            r12 = 1
            r14 = 1
            r11 = 1
            r13 = 1
            r2 = 1
            r5 = 1
            r7 = r2
            r8 = r6
            r9 = r6
            r10 = r6
            goto L_0x0234
        L_0x0314:
            r2 = 1
            goto L_0x024a
        L_0x0317:
            r2 = 0
            r4 = r2
            goto L_0x0264
        L_0x031b:
            r2 = 0
            goto L_0x0269
        L_0x031e:
            r2 = 0
            r4 = r2
            goto L_0x0276
        L_0x0322:
            r2 = 0
            goto L_0x027d
        L_0x0325:
            r2 = 0
            r4 = r2
            goto L_0x028c
        L_0x0329:
            r2 = 0
            goto L_0x0299
        L_0x032c:
            r7 = r2
            r9 = r6
            r10 = r6
            r11 = r6
            goto L_0x0234
        L_0x0332:
            r7 = r2
            r8 = r6
            r9 = r6
            r10 = r6
            r11 = r6
            r13 = r6
            goto L_0x0234
        L_0x033a:
            r5 = r6
            r7 = r6
            r8 = r6
            r9 = r6
            r10 = r6
            r12 = r6
            r14 = r6
            goto L_0x0234
        L_0x0343:
            r5 = r6
            r7 = r6
            r8 = r6
            r9 = r6
            r10 = r2
            r11 = r6
            goto L_0x0234
        L_0x034b:
            r5 = r6
            r7 = r6
            r8 = r6
            r9 = r6
            r10 = r6
            r11 = r6
            r13 = r6
            goto L_0x0234
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C1066Pb.<init>(java.lang.reflect.Method, java.lang.Class, java.lang.String, boolean):void");
    }

    /* renamed from: f */
    private void m8517f(Method method) {
        this.eRI = method.getAnnotation(C1253SX.class) != null;
        if (this.eRI) {
            this.eRP = true;
            this.eRO = new C1067a();
        }
    }

    /* renamed from: pM */
    public DataClassSerializer[] mo4767pM() {
        return this.hUX;
    }

    public String toString() {
        return "MethodInfo[name=" + name() + "]";
    }

    public Object writeReplace() {
        return super.writeReplace();
    }

    /* renamed from: pK */
    public String mo4765pK() {
        return null;
    }

    public boolean isEditable() {
        return this.editable;
    }

    public String getDisplayName() {
        return this.exL;
    }

    public Class<?> getReturnType() {
        return this.returnType;
    }

    public Class<?>[] getParameterTypes() {
        return this.parameterTypes;
    }

    /* renamed from: pE */
    public boolean mo4759pE() {
        return this.hUV == C3248pc.C3250b.ADDER;
    }

    /* renamed from: pF */
    public boolean mo4760pF() {
        return this.hUV == C3248pc.C3250b.ACTION;
    }

    /* renamed from: pB */
    public boolean mo4756pB() {
        return this.hUV == C3248pc.C3250b.GETTER;
    }

    /* renamed from: pI */
    public boolean mo4763pI() {
        return this.hUV == C3248pc.C3250b.ITERATOR;
    }

    /* renamed from: pH */
    public boolean mo4762pH() {
        return this.hUV == C3248pc.C3250b.REMOVER;
    }

    /* renamed from: pC */
    public boolean mo4757pC() {
        return this.hUV == C3248pc.C3250b.SETTER;
    }

    /* renamed from: pD */
    public boolean mo4758pD() {
        return this.hUV == C3248pc.C3250b.ALTERNATE_SETTER;
    }

    /* renamed from: pL */
    public boolean mo4766pL() {
        return this.hUU;
    }

    /* renamed from: pG */
    public boolean mo4761pG() {
        return this.hUV == C3248pc.C3250b.ALTERNATE_ADDER;
    }

    /* renamed from: pJ */
    public C3248pc.C3249a mo4764pJ() {
        return this.hUW;
    }

    /* renamed from: py */
    public boolean mo4793py() {
        return this.hVM;
    }

    /* renamed from: pN */
    public Object mo4768pN() {
        return this.hUY;
    }

    /* renamed from: pO */
    public C6580apg mo4769pO() {
        return this.hUZ;
    }

    /* renamed from: pz */
    public int mo4794pz() {
        return this.hVa;
    }

    /* renamed from: pY */
    public int mo4779pY() {
        return this.hVb;
    }

    /* renamed from: pA */
    public int mo4755pA() {
        return this.hVc;
    }

    /* renamed from: pr */
    public boolean mo4786pr() {
        return this.hVe;
    }

    /* renamed from: ps */
    public boolean mo4787ps() {
        return this.hVg;
    }

    /* renamed from: pt */
    public boolean mo4788pt() {
        return this.hVf;
    }

    /* renamed from: pn */
    public boolean mo4782pn() {
        return this.hVi;
    }

    /* renamed from: po */
    public boolean mo4783po() {
        return this.hVk;
    }

    /* renamed from: pp */
    public boolean mo4784pp() {
        return this.hVj;
    }

    /* renamed from: pv */
    public boolean mo4790pv() {
        return this.hVm;
    }

    /* renamed from: pw */
    public boolean mo4791pw() {
        return this.hVo;
    }

    /* renamed from: px */
    public boolean mo4792px() {
        return this.hVn;
    }

    public boolean isBlocking() {
        return this.hVp;
    }

    /* renamed from: hD */
    public Class<?> mo4752hD() {
        return this.agS;
    }

    /* renamed from: A */
    public void mo4715A(long j) {
        if (this.hVq == null) {
            this.hVq = new AtomicLong();
        }
        if (this.hVr == null) {
            this.hVr = new AtomicInteger();
        }
        this.hVq.addAndGet(j / 1000);
        this.hVr.incrementAndGet();
    }

    /* renamed from: B */
    public void mo4716B(long j) {
        if (this.hVs == null) {
            this.hVs = new AtomicLong();
        }
        if (this.hVt == null) {
            this.hVt = new AtomicInteger();
        }
        this.hVs.addAndGet(j / 1000);
        this.hVt.incrementAndGet();
    }

    public long getExecuteTime() {
        if (this.hVq == null) {
            return 0;
        }
        return this.hVq.get();
    }

    public int getExecuteCount() {
        if (this.hVr == null) {
            return 0;
        }
        return this.hVr.get();
    }

    public long getTransactionTime() {
        if (this.hVs == null) {
            return 0;
        }
        return this.hVs.get();
    }

    public int getTransactionCount() {
        if (this.hVt == null) {
            return 0;
        }
        return this.hVt.get();
    }

    /* renamed from: aO */
    public void mo4721aO(int i) {
        if (i != 0) {
            if (this.hVu == null) {
                this.hVu = new AtomicInteger();
            }
            this.hVu.addAndGet(i);
        }
    }

    /* renamed from: aQ */
    public void mo4723aQ(int i) {
        if (i != 0) {
            if (this.hVv == null) {
                this.hVv = new AtomicInteger();
            }
            this.hVv.addAndGet(i);
        }
    }

    /* renamed from: aN */
    public void mo4720aN(int i) {
        if (i != 0) {
            if (this.hVw == null) {
                this.hVw = new AtomicInteger();
            }
            this.hVw.addAndGet(i);
        }
    }

    /* renamed from: aR */
    public void mo4724aR(int i) {
        if (i != 0) {
            if (this.hVx == null) {
                this.hVx = new AtomicInteger();
            }
            this.hVx.addAndGet(i);
        }
    }

    /* renamed from: aS */
    public void mo4725aS(int i) {
        if (i != 0) {
            if (this.hVy == null) {
                this.hVy = new AtomicInteger();
            }
            this.hVy.addAndGet(i);
        }
    }

    /* renamed from: aP */
    public void mo4722aP(int i) {
        if (i != 0) {
            if (this.hVz == null) {
                this.hVz = new AtomicInteger();
            }
            this.hVz.addAndGet(i);
        }
    }

    /* renamed from: aK */
    public void mo4717aK(int i) {
        if (i != 0) {
            if (this.hVD == null) {
                this.hVD = new AtomicInteger();
            }
            this.hVD.addAndGet(i);
        }
    }

    /* renamed from: aT */
    public void mo4726aT(int i) {
        if (i != 0) {
            if (this.hVE == null) {
                this.hVE = new AtomicInteger();
            }
            this.hVE.addAndGet(i);
        }
    }

    /* renamed from: aM */
    public void mo4719aM(int i) {
        if (i != 0) {
            if (this.hVF == null) {
                this.hVF = new AtomicInteger();
            }
            this.hVF.addAndGet(i);
        }
    }

    /* renamed from: aL */
    public void mo4718aL(int i) {
        if (i != 0) {
            if (this.hVG == null) {
                this.hVG = new AtomicInteger();
            }
            this.hVG.addAndGet(i);
        }
    }

    /* renamed from: aU */
    public void mo4727aU(int i) {
        if (i != 0) {
            if (this.hVH == null) {
                this.hVH = new AtomicInteger();
            }
            this.hVH.addAndGet(i);
        }
    }

    /* renamed from: aV */
    public void mo4728aV(int i) {
        if (i != 0) {
            if (this.hVC == null) {
                this.hVC = new AtomicInteger();
            }
            this.hVC.addAndGet(i);
        }
    }

    /* renamed from: aW */
    public void mo4729aW(int i) {
        if (i != 0) {
            if (this.hVB == null) {
                this.hVB = new AtomicInteger();
            }
            this.hVB.addAndGet(i);
        }
    }

    /* renamed from: aX */
    public void mo4730aX(int i) {
        if (i != 0) {
            if (this.hVA == null) {
                this.hVA = new AtomicInteger();
            }
            this.hVA.addAndGet(i);
        }
    }

    /* renamed from: pP */
    public void mo4770pP() {
        if (this.hVI == null) {
            this.hVI = new AtomicInteger();
        }
        this.hVI.addAndGet(1);
    }

    public int getInfraMethodsCalled() {
        if (this.hVu == null) {
            return 0;
        }
        return this.hVu.get();
    }

    /* renamed from: pQ */
    public int mo4771pQ() {
        if (this.hVv == null) {
            return 0;
        }
        return this.hVv.get();
    }

    /* renamed from: pR */
    public int mo4772pR() {
        if (this.hVw == null) {
            return 0;
        }
        return this.hVw.get();
    }

    /* renamed from: pS */
    public int mo4773pS() {
        if (this.hVx == null) {
            return 0;
        }
        return this.hVx.get();
    }

    public int getObjectsTouchedInPostponed() {
        if (this.hVy == null) {
            return 0;
        }
        return this.hVy.get();
    }

    public int getObjectsTouchedInTransaction() {
        if (this.hVz == null) {
            return 0;
        }
        return this.hVz.get();
    }

    public int getTransactionalGets() {
        if (this.hVD == null) {
            return 0;
        }
        return this.hVD.get();
    }

    public int getTransactionalGetsInPostponed() {
        if (this.hVE == null) {
            return 0;
        }
        return this.hVE.get();
    }

    public int getTransactionalMethodsCalled() {
        if (this.hVF == null) {
            return 0;
        }
        return this.hVF.get();
    }

    public int getTransactionalSets() {
        if (this.hVG == null) {
            return 0;
        }
        return this.hVG.get();
    }

    public int getTransactionalSetsInPostponed() {
        if (this.hVH == null) {
            return 0;
        }
        return this.hVH.get();
    }

    /* renamed from: pT */
    public int mo4774pT() {
        if (this.hVI == null) {
            return 0;
        }
        return this.hVI.get();
    }

    public int getObjectsChangedInTransaction() {
        if (this.hVC == null) {
            return 0;
        }
        return this.hVC.get();
    }

    public int getReadLocksInTransaction() {
        if (this.hVB == null) {
            return 0;
        }
        return this.hVB.get();
    }

    public int getWriteLocksInTransaction() {
        if (this.hVA == null) {
            return 0;
        }
        return this.hVA.get();
    }

    /* renamed from: aY */
    public void mo4731aY(int i) {
        if (i != 0) {
            if (this.hVJ == null) {
                this.hVJ = new AtomicInteger();
            }
            this.hVJ.addAndGet(i);
        }
    }

    public int getObjectsCreatedInTransaction() {
        if (this.hVJ == null) {
            return 0;
        }
        return this.hVJ.get();
    }

    public Type getGenericReturnType() {
        return this.hVN;
    }

    /* renamed from: pU */
    public void mo4775pU() {
        if (this.hVK == null) {
            this.hVK = new AtomicInteger();
        }
        this.hVK.incrementAndGet();
    }

    /* renamed from: pV */
    public void mo4776pV() {
        if (this.hVL == null) {
            this.hVL = new AtomicInteger();
        }
        this.hVL.incrementAndGet();
    }

    /* renamed from: pW */
    public long mo4777pW() {
        return (long) (this.hVL == null ? 0 : this.hVL.get());
    }

    /* renamed from: pX */
    public long mo4778pX() {
        return (long) (this.hVK == null ? 0 : this.hVK.get());
    }

    /* renamed from: pq */
    public boolean mo4785pq() {
        return this.hVd;
    }

    /* renamed from: pm */
    public boolean mo4781pm() {
        return this.hVh;
    }

    /* renamed from: pu */
    public boolean mo4789pu() {
        return this.hVl;
    }

    /* renamed from: pZ */
    public boolean mo4780pZ() {
        return this.hVO;
    }

    /* renamed from: qa */
    public long mo4795qa() {
        return super.mo4795qa();
    }

    /* renamed from: a.Pb$a */
    class C1067a implements C3538sJ {
        C1067a() {
        }

        /* renamed from: a */
        public Collection<aDR> mo4799a(C1616Xf xf, C6913awB awb) {
            return xf.bFf().mo6866PM().mo3454l(xf);
        }

        /* renamed from: Xf */
        public C2491fm mo4798Xf() {
            throw new UnsupportedOperationException();
        }
    }
}
