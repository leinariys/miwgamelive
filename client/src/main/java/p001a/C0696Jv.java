package p001a;

import game.network.message.externalizable.C6869avJ;

/* renamed from: a.Jv */
/* compiled from: a */
public class C0696Jv extends C6869avJ {
    private final int cQJ;
    private final boolean visible;

    public C0696Jv() {
        this.visible = false;
        this.cQJ = 0;
    }

    public C0696Jv(int i, boolean z) {
        this.cQJ = i;
        this.visible = z;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public int aMP() {
        return this.cQJ;
    }
}
