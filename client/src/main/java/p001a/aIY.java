package p001a;

import game.engine.DataGameEvent;
import game.network.message.IReadProto;
import game.network.message.WriterBitsData;

/* renamed from: a.aIY */
/* compiled from: a */
public class aIY implements C3828vU {
    private DataGameEvent aGA;

    /* renamed from: a */
    public void mo9369a(DataGameEvent jz) {
        this.aGA = jz;
    }

    /* renamed from: a */
    public C3380qs mo9366a(WriterBitsData oUVar, boolean z) {
        return new C3380qs(this.aGA, oUVar, z);
    }

    /* renamed from: a */
    public C3380qs mo9368a(WriterBitsData oUVar, boolean z, boolean z2) {
        return new C3380qs(this.aGA, oUVar, z, z2);
    }

    /* renamed from: a */
    public C3380qs mo9367a(WriterBitsData oUVar, boolean z, int i, boolean z2) {
        return new C3380qs(this.aGA, oUVar, z, i, z2);
    }

    /* renamed from: a */
    public aPZ mo9363a(IReadProto ie, boolean z) {
        return new aPZ(this.aGA, ie, z);
    }

    /* renamed from: a */
    public aPZ mo9365a(IReadProto ie, boolean z, boolean z2) {
        return new aPZ(this.aGA, ie, z, z2);
    }

    /* renamed from: a */
    public aPZ mo9364a(IReadProto ie, boolean z, int i) {
        return new aPZ(this.aGA, ie, z, i);
    }
}
