package p001a;

import logic.res.KeyCodeName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.asU  reason: case insensitive filesystem */
/* compiled from: a */
public class C6724asU {
    public static final int gxq = 0;
    public static final int gxr = 1;
    public static final int gxs = 0;
    public static final int gxt = 1;
    public static final int gxu = 2;
    private static ArrayList<Integer> gxv = new ArrayList<>();
    private static ArrayList<Integer> gxw = new ArrayList<>();
    private static Map<String, Integer> gxx = new HashMap();

    static {
        m25700ux(27);
        m25701uy(16);
        m25702w("GRAB_ALL_LOOT", 1);
        m25702w("RELOAD_ADDONS", 0);
        m25702w("PDA_WINDOW", 2);
        m25702w("MAP_WINDOW", 2);
        m25702w("PREV_CANNON", 0);
        m25702w("INTERACT", 0);
        m25702w("SHUTDOWN", 0);
        m25702w("INVENTORY_WINDOW", 0);
        m25702w("SHOW_RENDER_INFO", 0);
        m25702w("TOGGLE_MOUSE", 0);
        m25702w("ROLL_CW", 0);
        m25702w("SHOW_OBJECTS", 0);
        m25702w("ZOOM_IN", 0);
        m25702w("RADAR_OUT", 0);
        m25702w("MODULE8", 0);
        m25702w("ORBIT_AUTO_PILOT", 0);
        m25702w("MODULE7", 0);
        m25702w("TOGGLE_RADAR", 0);
        m25702w("MODULE6", 0);
        m25702w("MODULE5", 0);
        m25702w("MODULE4", 0);
        m25702w("MODULE3", 0);
        m25702w("MODULE2", 0);
        m25702w("MOVE_TO", 0);
        m25702w("MODULE1", 0);
        m25702w("PREV_LAUNCHER", 0);
        m25702w("OUTPOST_WINDOW", 0);
        m25702w("STATION_WINDOW", 0);
        m25702w("DEACCELERATE", 0);
        m25702w("HIDE_HUD", 0);
        m25702w("TOOGLE_CONSOLE", 0);
        m25702w("ASSOCIATES_WINDOW", 0);
        m25702w("CORPORATION_WINDOW", 0);
        m25702w("AUTO_PILOT", 0);
        m25702w("NEXT_THREAT", 0);
        m25702w("SELECT_CLICKED_TARGET", 0);
        m25702w("NEXT_CANNON", 0);
        m25702w("YAW_RIGHT", 0);
        m25702w("SHOOT_LAUNCHER", 0);
        m25702w("HELP_WINDOW", 0);
        m25702w("SELECT_NEAREST_TARGET", 0);
        m25702w("CYCLE_PVT", 0);
        m25702w("MICROJUMP", 0);
        m25702w("CONTRACT_WINDOW", 0);
        m25702w("PITCH_UP", 0);
        m25702w("MARKET_WINDOW", 0);
        m25702w("SCREENSHOT_HIGHRES", 0);
        m25702w("NEAREST_TARGET", 0);
        m25702w("CAMERA_ORBITAL", 0);
        m25702w("ALIGN", 0);
        m25702w("SELECT_ATTACKER", 0);
        m25702w("INTERACT_AUTO_PILOT", 0);
        m25702w("VAULT_WINDOW", 0);
        m25702w("ZOOM_OUT", 0);
        m25702w("NEXT_LOOT", 0);
        m25702w("SPECIAL1_ON", 0);
        m25702w("YAW_LEFT", 0);
        m25702w("TRADE_INVITE", 0);
        m25702w("HOPLONS_WINDOW", 0);
        m25702w("PHYSICS_DEBUG", 0);
        m25702w("PREV_TARGET", 0);
        m25702w("TOGGLE_INERTIAL_FLIGHT", 0);
        m25702w("FLASH_PLAYER_WINDOW", 0);
        m25702w("REQUEST_CHAT", 0);
        m25702w("OUTPOST_STORAGE_WINDOW", 0);
        m25702w("STORAGE_WINDOW", 0);
        m25702w("SPECIAL1_OFF", 0);
        m25702w("ACCELERATE", 0);
        m25702w("CLONING_CENTER_WINDOW", 0);
        m25702w("ROLL_CCW", 0);
        m25702w("CHANGE_CAMERA", 0);
        m25702w("EXIT", 0);
        m25702w("PARTY_INVITE", 0);
        m25702w("NEXT_LAUNCHER", 0);
        m25702w("NEXT_INTEREST", 0);
        m25702w("PITCH_DOWN", 0);
        m25702w("NEXT_TURRET", 0);
        m25702w("KILL_AUTO_PILOT", 0);
        m25702w("SEC_SLOT8", 0);
        m25702w("TARGET_OF_TARGET", 0);
        m25702w("SPECIAL2", 0);
        m25702w("SEC_SLOT7", 0);
        m25702w("SPECIAL1", 0);
        m25702w("ACTIVATE_MOUSE", 0);
        m25702w("SEC_SLOT6", 0);
        m25702w("SEC_SLOT5", 0);
        m25702w("SEC_SLOT4", 0);
        m25702w("SEC_SLOT3", 0);
        m25702w("SHOOT_CANNON", 0);
        m25702w("SEC_SLOT2", 0);
        m25702w("SEC_SLOT1", 0);
        m25702w("WARDROBE_WINDOW", 0);
        m25702w("SCREENSHOT", 0);
        m25702w("RADAR_IN", 0);
        m25702w("MAIL_WINDOW", 0);
        m25702w("SHIP_WINDOW", 0);
        m25702w("SLOT6", 0);
        m25702w("SLOT5", 0);
        m25702w("SHOWTEXTURESSTATES", 0);
        m25702w("SLOT4", 0);
        m25702w("TOOGLE_SCONSOLE", 0);
        m25702w("SLOT3", 0);
        m25702w("SLOT2", 0);
        m25702w("SLOT1", 0);
        m25702w("NEXT_TARGET", 0);
        m25702w("AGENTS_WINDOW", 0);
    }

    /* renamed from: ut */
    public static String m25696ut(int i) {
        if (gxv.contains(Integer.valueOf(i))) {
            return null;
        }
        return KeyCodeName.m6645ls(C0927Nb.m7693lP(i));
    }

    /* renamed from: uu */
    public static boolean m25697uu(int i) {
        return gxw.contains(Integer.valueOf(i));
    }

    /* renamed from: uv */
    public static String m25698uv(int i) {
        switch (i) {
            case 1:
                return "MOUSE_LEFT";
            case 2:
                return "MOUSE_MIDDLE";
            case 3:
                return "MOUSE_RIGHT";
            default:
                return null;
        }
    }

    /* renamed from: uw */
    public static String m25699uw(int i) {
        switch (i) {
            case 0:
                return "MOUSE_WHEELUP";
            case 1:
                return "MOUSE_WHEELDOWN";
            default:
                return null;
        }
    }

    /* renamed from: ux */
    private static void m25700ux(int i) {
        gxv.add(Integer.valueOf(i));
    }

    /* renamed from: uy */
    private static void m25701uy(int i) {
        gxw.add(Integer.valueOf(i));
    }

    /* renamed from: w */
    private static void m25702w(String str, int i) {
        gxx.put(str, Integer.valueOf(i));
    }
}
