package p001a;

import game.network.message.serializable.C0035AS;
import game.network.message.serializable.C1662YU;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3988xl;
import logic.data.mbean.C0677Jd;
import logic.res.html.C0029AO;
import logic.res.html.C1867ad;
import logic.res.html.MessageContainer;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Set;

/* renamed from: a.wt */
/* compiled from: a */
public class C3929wt<T> extends C3988xl<T> implements C2686iZ<T> {

    /* access modifiers changed from: private */
    public Set<T> set;
    private Set<T> eVA;
    private C0029AO<T> eVB;

    public C3929wt(C0677Jd jd, C0035AS as) {
        super(jd, as);
        Set<T> aya = as.aya();
        this.eVA = aya;
        this.set = aya;
    }

    /* access modifiers changed from: protected */
    public final void anC() {
        if (this.bFM) {
            return;
        }
        if (this.bFN || this.bFL.aXo() == null || this.bFL.aXo().isDisposed()) {
            throw new IllegalStateException("Already finished");
        }
        Set<T> v = MessageContainer.m16003v(this.eVA);
        this.set = v;
        this.collection = v;
        this.bFM = true;
    }

    public Set<T> bLG() {
        if (this.bFM) {
            return this.set;
        }
        return this.eVA;
    }

    public Iterator<T> iterator() {
        Iterator<T> it;
        if (this.bFM) {
            it = new ArrayList(this.set).iterator();
        } else {
            it = this.set.iterator();
        }
        return new C3930a(it);
    }

    /* renamed from: XC */
    public C1867ad<T> mo271XC() {
        return new C1662YU(this);
    }

    public C0029AO<T> anE() {
        if (this.bFN) {
            return this.eVB;
        }
        this.bFN = true;
        if (!this.bFM) {
            return this.bFO;
        }
        this.bFM = false;
        C0035AS as = new C0035AS(this.bFO.mo19379yn(), this.bFO.mo19378ym(), this.set);
        this.eVB = as;
        return as;
    }

    /* renamed from: a.wt$a */
    class C3930a implements Iterator<T> {
        Iterator<T> bDl;
        T bDm = null;
        int count = 0;
        private int expectedModCount;

        C3930a(Iterator it) {
            this.bDl = it;
            this.expectedModCount = C3929wt.this.anD();
        }

        public boolean hasNext() {
            amx();
            return this.bDl.hasNext();
        }

        public T next() {
            this.bDm = this.bDl.next();
            this.count++;
            amx();
            return this.bDm;
        }

        private void amx() {
            if (this.expectedModCount != C3929wt.this.anD()) {
                throw new ConcurrentModificationException();
            }
        }

        public void remove() {
            amx();
            if (!C3929wt.this.bFM) {
                ArrayList arrayList = new ArrayList(C3929wt.this.set.size() - this.count);
                while (this.bDl.hasNext()) {
                    arrayList.add(this.bDl.next());
                }
                this.bDl = arrayList.iterator();
                C3929wt.this.anC();
            }
            C3929wt.this.remove(this.bDm);
            this.expectedModCount = C3929wt.this.anD();
        }
    }
}
