package p001a;

import game.network.message.C0474GZ;
import game.script.Actor;
import game.script.damage.DamageType;
import game.script.item.WeaponType;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.aCm  reason: case insensitive filesystem */
/* compiled from: a */
public final class C5260aCm implements Externalizable, Cloneable {

    private transient WeaponType hqA;
    private transient Map<DamageType, Float> hqv;
    private transient String hqw;
    private transient Actor hqx;
    private transient boolean hqy;
    private transient boolean hqz;

    public C5260aCm() {
        this.hqv = new HashMap();
    }

    public C5260aCm(C5260aCm acm, String str, Actor cr) {
        this.hqw = str;
        this.hqx = cr;
        this.hqy = false;
        this.hqv = acm.cPK();
    }

    public C5260aCm(Map<DamageType, Float> map, String str, Actor cr) {
        this.hqw = str;
        this.hqx = cr;
        this.hqy = false;
        this.hqv = map;
    }

    /* renamed from: g */
    public void mo8227g(DamageType fr, float f) {
        this.hqv.put(fr, Float.valueOf(f));
    }

    public void putAll(Map<DamageType, Float> map) {
        this.hqv.putAll(map);
    }

    public String cPF() {
        return this.hqw;
    }

    public Actor cPG() {
        return this.hqx;
    }

    public String toString() {
        return "";
    }

    public Object clone() {
        return this;
    }

    public boolean cPH() {
        return this.hqy;
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeBoolean(this.hqy);
        C2562gp.f7767PX.serializeData(objectOutput, this.hqw);
        C0474GZ.dah.serializeData(objectOutput, (Object) this.hqx);
        C0474GZ.dah.serializeData(objectOutput, (Object) this.hqA);
        objectOutput.writeObject(this.hqv);
    }

    public void readExternal(ObjectInput objectInput) {
        this.hqy = objectInput.readBoolean();
        this.hqw = (String) C2562gp.f7767PX.inputReadObject(objectInput);
        this.hqx = (Actor) C0474GZ.dah.inputReadObject(objectInput);
        this.hqA = (WeaponType) C0474GZ.dah.inputReadObject(objectInput);
        this.hqv = (Map) objectInput.readObject();
    }

    /* renamed from: iz */
    public void mo8228iz(boolean z) {
        this.hqz = z;
    }

    public boolean cPI() {
        return this.hqz;
    }

    public WeaponType cPJ() {
        return this.hqA;
    }

    /* renamed from: w */
    public void mo8232w(WeaponType apt) {
        this.hqA = apt;
    }

    public Map<DamageType, Float> cPK() {
        return this.hqv;
    }
}
