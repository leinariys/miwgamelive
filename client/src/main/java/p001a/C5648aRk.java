package p001a;

import game.geometry.Vec3d;

import java.util.List;
import java.util.Set;

/* renamed from: a.aRk  reason: case insensitive filesystem */
/* compiled from: a */
class C5648aRk implements C3055nX.C3056a<E, B> {

    /* renamed from: AS */
    final /* synthetic */ C2268dT f3717AS;
    private int tag;

    C5648aRk(C2268dT dTVar) {
        this.f3717AS = dTVar;
    }

    /* renamed from: B */
    public <T> T mo11189B(Object obj) {
        return this.f3717AS.cTp.get(obj);
    }

    /* renamed from: cy */
    public List<C0463GP<E>> mo11195cy(int i) {
        return (List) this.f3717AS.cTq.get(i);
    }

    public int getTag() {
        return this.tag;
    }

    public void setTag(int i) {
        this.tag = i;
    }

    /* renamed from: e */
    public void mo11196e(Object obj, Object obj2) {
        this.f3717AS.cTp.put(obj, obj2);
    }

    /* renamed from: a */
    public void mo11193a(float f, Set<C6625aqZ> set) {
        for (C2268dT.C2269a a : this.f3717AS.cTr) {
            set.addAll(a.f6525AR.aRu());
        }
    }

    public Vec3d getPosition() {
        return null;
    }

    /* renamed from: IO */
    public long mo11191IO() {
        return 0;
    }

    /* renamed from: bZ */
    public void mo11194bZ(long j) {
    }

    /* renamed from: IP */
    public List<List<C0463GP<E>>> mo11192IP() {
        return null;
    }

    /* renamed from: IN */
    public float mo11190IN() {
        return 0.0f;
    }
}
