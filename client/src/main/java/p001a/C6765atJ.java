package p001a;

import org.apache.mina.core.buffer.IoBuffer;

import java.io.ObjectInput;
import java.nio.charset.CharsetDecoder;

/* renamed from: a.atJ  reason: case insensitive filesystem */
/* compiled from: a */
public final class C6765atJ implements ObjectInput {
    private final IoBuffer gAO;
    private CharsetDecoder aDj;

    public C6765atJ(IoBuffer ioBuffer, CharsetDecoder charsetDecoder) {
        this.gAO = ioBuffer;
        this.aDj = charsetDecoder;
    }

    public int available() {
        return this.gAO.remaining();
    }

    public void close() {
    }

    public int read() {
        return this.gAO.get();
    }

    public int read(byte[] bArr) {
        this.gAO.get(bArr);
        return bArr.length;
    }

    public int read(byte[] bArr, int i, int i2) {
        this.gAO.get(bArr, i, i2);
        return i2;
    }

    public Object readObject() {
        throw new UnsupportedOperationException();
    }

    public long skip(long j) {
        this.gAO.skip((int) j);
        return j;
    }

    public boolean readBoolean() {
        return this.gAO.get() == 1;
    }

    public byte readByte() {
        return this.gAO.get();
    }

    public char readChar() {
        return (char) this.gAO.getInt();
    }

    public double readDouble() {
        return this.gAO.getDouble();
    }

    public float readFloat() {
        return this.gAO.getFloat();
    }

    public void readFully(byte[] bArr) {
        this.gAO.get(bArr);
    }

    public void readFully(byte[] bArr, int i, int i2) {
        this.gAO.get(bArr, i, i2);
    }

    public int readInt() {
        return this.gAO.getInt();
    }

    public String readLine() {
        throw new UnsupportedOperationException();
    }

    public long readLong() {
        return this.gAO.getLong();
    }

    public short readShort() {
        return this.gAO.getShort();
    }

    public String readUTF() {
        return this.gAO.getString(this.aDj);
    }

    public int readUnsignedByte() {
        return this.gAO.get() & 255;
    }

    public int readUnsignedShort() {
        return 65535 & this.gAO.getShort();
    }

    public int skipBytes(int i) {
        this.gAO.skip(i);
        return i;
    }
}
