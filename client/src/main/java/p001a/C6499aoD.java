package p001a;

import taikodom.render.scene.RLine;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.Material;

/* renamed from: a.aoD  reason: case insensitive filesystem */
/* compiled from: a */
public class C6499aoD extends RLine {
    private float cellSize;
    private int giJ;
    private int rows;

    public C6499aoD(int i, int i2, float f) {
        this.rows = i;
        this.giJ = i2;
        this.cellSize = f;
        cnR();
    }

    private void cnR() {
        ShaderPass shaderPass = new ShaderPass();
        Shader shader = new Shader();
        shader.addPass(shaderPass);
        Material material = new Material();
        material.setShader(shader);
        setMaterial(material);
        float f = -Math.abs((this.cellSize * ((float) this.giJ)) / 2.0f);
        float abs = Math.abs((this.cellSize * ((float) this.rows)) / 2.0f);
        float f2 = abs;
        for (int i = 0; i <= this.rows; i++) {
            addPoint(f, 0.0f, f2);
            addPoint(f * -1.0f, 0.0f, f2);
            f2 -= this.cellSize;
        }
        float f3 = f;
        for (int i2 = 0; i2 <= this.giJ; i2++) {
            addPoint(f3, 0.0f, abs);
            addPoint(f3, 0.0f, abs * -1.0f);
            f3 += this.cellSize;
        }
    }
}
