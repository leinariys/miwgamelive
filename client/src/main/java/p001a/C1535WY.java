package p001a;

/* renamed from: a.WY */
/* compiled from: a */
public class C1535WY {
    private long eAk;
    private long eAl;

    public void start() {
        this.eAk = System.nanoTime();
    }

    public void stop() {
        this.eAl += System.nanoTime() - this.eAk;
    }

    public float bFa() {
        return ((float) this.eAl) * 1.0E-9f;
    }

    public void reset() {
        this.eAl = 0;
        start();
    }
}
