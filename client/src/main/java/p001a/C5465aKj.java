package p001a;

import game.io.IReadExternal;
import game.io.IWriteExternal;
import game.network.exception.CreatListenerChannelException;
import game.network.message.externalizable.C2631hn;
import game.network.message.externalizable.C3328qK;
import game.network.message.serializable.C0035AS;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C2991mi;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import logic.res.html.DataClassSerializer;
import logic.res.html.MessageContainer;

import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* renamed from: a.aKj  reason: case insensitive filesystem */
/* compiled from: a */
public class C5465aKj extends C5546aNm {

    private static /* synthetic */ int[] ihZ = null;
    private static /* synthetic */ int[] iia = null;
    private final List<C2991mi<Object>> bFH;
    /* renamed from: Um */
    public transient DataClassSerializer f3262Um;

    public C5465aKj() {
        this.bFH = MessageContainer.init();
    }

    public C5465aKj(List<C2991mi<Object>> list) {
        this.bFH = list;
    }

    static /* synthetic */ int[] dgg() {
        int[] iArr = ihZ;
        if (iArr == null) {
            iArr = new int[C0651JP.values().length];
            try {
                iArr[C0651JP.ADD.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C0651JP.ADD_AT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C0651JP.ADD_COLLECTION.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[C0651JP.ADD_COLLECTION_AT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[C0651JP.CLEAR.ordinal()] = 9;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[C0651JP.REMOVE.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[C0651JP.REMOVE_AT.ordinal()] = 6;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[C0651JP.REMOVE_COLLECTION.ordinal()] = 7;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[C0651JP.SET.ordinal()] = 8;
            } catch (NoSuchFieldError e9) {
            }
            ihZ = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] dgh() {
        int[] iArr = iia;
        if (iArr == null) {
            iArr = new int[C2991mi.C2992a.values().length];
            try {
                iArr[C2991mi.C2992a.INDEXED.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C2991mi.C2992a.INDEXED_VALUE.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C2991mi.C2992a.VALUE.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[C2991mi.C2992a.VOID.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            iia = iArr;
        }
        return iArr;
    }

    /* renamed from: c */
    public void mo9792c(C5663aRz arz) {
        super.mo9792c(arz);
        this.f3262Um = C5726aUk.m18699b(arz.mo11295Eq()[0], (Class<?>[]) null);
    }

    public int getType() {
        return C6976axp._collection.ordinal();
    }

    /* renamed from: K */
    public Object mo880K(Object obj) {
        Collection XD;
        C0035AS as;
        C2631hn hnVar = (C2631hn) obj;
        if (hnVar instanceof C2686iZ) {
            C0035AS as2 = new C0035AS(hnVar.mo19379yn(), this.f3436Ul);
            XD = as2.aya();
            as = as2;
        } else {
            C3328qK qKVar = new C3328qK(hnVar.mo19379yn(), this.f3436Ul);
            XD = qKVar.mo21318XD();
            as = qKVar;
        }
        XD.addAll(hnVar.getCollection());
        for (C2991mi next : this.bFH) {
            switch (dgg()[next.aPk().ordinal()]) {
                case 1:
                    XD.add(next.value());
                    break;
                case 2:
                    ((List) XD).add(next.index(), next.value());
                    break;
                case 3:
                    XD.addAll((Collection) next.value());
                    break;
                case 4:
                    ((List) XD).addAll(next.index(), (Collection) next.value());
                    break;
                case 5:
                    XD.remove(next.value());
                    break;
                case 6:
                    ((List) XD).remove(next.index());
                    break;
                case 7:
                    XD.removeAll((Collection) next.value());
                    break;
                case 8:
                    ((List) XD).set(next.index(), next.value());
                    break;
                case 9:
                    XD.clear();
                    break;
                default:
                    throw new CreatListenerChannelException("Unknown change " + next.aPk().ordinal());
            }
        }
        return as;
    }

    /* renamed from: b */
    public void mo886b(C1616Xf xf, ObjectInput objectInput) {
        Object b;
        Object b2;
        int readInt = objectInput.readInt();
        while (true) {
            int i = readInt - 1;
            if (i >= 0) {
                C0651JP jp = C0651JP.values()[objectInput.readByte()];
                switch (dgh()[jp.aZB().ordinal()]) {
                    case 1:
                        this.bFH.add(new C6786ate(jp));
                        readInt = i;
                        break;
                    case 2:
                        if (jp.isCollection()) {
                            b2 = C5441aJl.ifE.mo9622a(objectInput, this.f3262Um);
                        } else {
                            b2 = this.f3262Um.inputReadObject(objectInput);
                        }
                        this.bFH.add(new C6630aqe(jp, b2));
                        readInt = i;
                        break;
                    case 3:
                        this.bFH.add(new C0402Fb(jp, objectInput.readInt()));
                        readInt = i;
                        break;
                    case 4:
                        int readInt2 = objectInput.readInt();
                        if (jp.isCollection()) {
                            b = C5441aJl.ifE.mo9622a(objectInput, this.f3262Um);
                        } else {
                            b = this.f3262Um.inputReadObject(objectInput);
                        }
                        this.bFH.add(new C6260ajY(jp, readInt2, b));
                        readInt = i;
                        break;
                    default:
                        throw new IllegalArgumentException();
                }
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    public void mo883a(C1616Xf xf, ObjectOutput objectOutput) {
        objectOutput.writeInt(this.bFH.size());
        for (C2991mi next : this.bFH) {
            C0651JP aPk = next.aPk();
            objectOutput.writeByte(aPk.ordinal());
            switch (dgh()[aPk.aZB().ordinal()]) {
                case 1:
                    break;
                case 2:
                    if (!aPk.isCollection()) {
                        this.f3262Um.serializeData(objectOutput, next.value());
                        break;
                    } else {
                        C5441aJl.ifE.mo9624a(objectOutput, (Collection) next.value(), this.f3262Um);
                        break;
                    }
                case 3:
                    objectOutput.writeInt(next.index());
                    break;
                case 4:
                    objectOutput.writeInt(next.index());
                    if (!aPk.isCollection()) {
                        this.f3262Um.serializeData(objectOutput, next.value());
                        break;
                    } else {
                        C5441aJl.ifE.mo9624a(objectOutput, (Collection) next.value(), this.f3262Um);
                        break;
                    }
                default:
                    throw new IllegalArgumentException();
            }
        }
    }

    /* renamed from: a */
    public C5546aNm mo881a(C5546aNm anm) {
        C5465aKj akj = (C5465aKj) anm;
        for (C2991mi<Object> aPk : this.bFH) {
            if (aPk.aPk() == C0651JP.CLEAR) {
                return this;
            }
        }
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(akj.bFH);
        arrayList.addAll(this.bFH);
        C5465aKj akj2 = new C5465aKj(arrayList);
        akj2.mo9792c(this.f3436Ul);
        return akj2;
    }

    public String toString() {
        return "CollectionStateChangeEntry: " + this.bFH;
    }

    public C5546aNm dgf() {
        C5465aKj akj = new C5465aKj(this.bFH);
        akj.f3262Um = this.f3262Um;
        akj.f3436Ul = this.f3436Ul;
        return akj;
    }

    /* renamed from: b */
    public void mo885b(C1616Xf xf, IReadExternal vm) {
        Object a;
        Object a2;
        int gX = vm.readInt("size");
        while (true) {
            int i = gX - 1;
            if (i >= 0) {
                C0651JP jp = (C0651JP) vm.mo6368hf("type");
                switch (dgh()[jp.aZB().ordinal()]) {
                    case 1:
                        this.bFH.add(new C6786ate(jp));
                        gX = i;
                        break;
                    case 2:
                        if (jp.isCollection()) {
                            a2 = vm.mo6366hd("values");
                        } else {
                            a2 = this.f3262Um.mo2357a(vm);
                        }
                        this.bFH.add(new C6630aqe(jp, a2));
                        gX = i;
                        break;
                    case 3:
                        this.bFH.add(new C0402Fb(jp, vm.readInt("index")));
                        gX = i;
                        break;
                    case 4:
                        int gX2 = vm.readInt("index");
                        if (jp.isCollection()) {
                            a = vm.mo6366hd("values");
                        } else {
                            a = this.f3262Um.mo2357a(vm);
                        }
                        this.bFH.add(new C6260ajY(jp, gX2, a));
                        gX = i;
                        break;
                    default:
                        throw new IllegalArgumentException();
                }
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    public void mo882a(C1616Xf xf, IWriteExternal att) {
        att.writeInt("size", this.bFH.size());
        for (C2991mi next : this.bFH) {
            C0651JP aPk = next.aPk();
            att.mo16262a("type", (Enum<?>) aPk);
            switch (dgh()[aPk.aZB().ordinal()]) {
                case 1:
                    break;
                case 2:
                    if (!aPk.isCollection()) {
                        this.f3262Um.serializeData(att, next.value());
                        break;
                    } else {
                        att.mo16273g("values", next.value());
                        break;
                    }
                case 3:
                    att.writeInt("index", next.index());
                    break;
                case 4:
                    att.writeInt("index", next.index());
                    if (!aPk.isCollection()) {
                        this.f3262Um.serializeData(att, next.value());
                        break;
                    } else {
                        att.mo16273g("values", next.value());
                        break;
                    }
                default:
                    throw new IllegalArgumentException();
            }
        }
    }
}
