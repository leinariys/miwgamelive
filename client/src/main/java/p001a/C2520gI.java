package p001a;

import logic.baa.aDJ;
import logic.res.code.C5663aRz;
import taikodom.infra.script.I18NString;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: a.gI */
/* compiled from: a */
public class C2520gI {
    /* renamed from: a */
    public static List<String> m31894a(Object obj, C5663aRz... arzArr) {
        boolean z;
        ArrayList arrayList = new ArrayList();
        if (obj == null) {
            return arrayList;
        }
        if (obj instanceof aDJ) {
            aDJ adj = (aDJ) obj;
            for (C5663aRz arz : adj.mo25c()) {
                int length = arzArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        z = false;
                        break;
                    } else if (arz == arzArr[i]) {
                        z = true;
                        break;
                    } else {
                        i++;
                    }
                }
                if (!z) {
                    Object w = adj.mo6767w(arz);
                    if (w instanceof I18NString) {
                        m31895a((I18NString) w, arrayList, arz, (Object) null);
                    } else if (w instanceof Map) {
                        for (Map.Entry entry : ((Map) w).entrySet()) {
                            if (entry.getValue() instanceof I18NString) {
                                m31895a((I18NString) entry.getValue(), arrayList, arz, entry.getKey());
                            }
                        }
                    }
                }
            }
        } else if (obj instanceof I18NString) {
            m31895a((I18NString) obj, arrayList, (C5663aRz) null, (Object) null);
        } else {
            arrayList.add("Invalid class for I18NString check: " + obj.getClass().getName());
        }
        return arrayList;
    }

    /* renamed from: a */
    private static void m31895a(I18NString i18NString, List<String> list, C5663aRz arz, Object obj) {
        boolean z;
        boolean[] zArr = new boolean[C1584XI.values().length];
        for (String next : i18NString.getMap().keySet()) {
            int i = 0;
            while (true) {
                if (i >= C1584XI.values().length) {
                    z = false;
                    break;
                } else if (C1584XI.values()[i].getCode().equals(next)) {
                    zArr[i] = true;
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
            if (z) {
                String str = i18NString.get(next);
                if (arz != null && str.endsWith("." + arz.name())) {
                    list.add("field '" + arz.name() + "'" + (obj != null ? " [" + obj + "]" : "") + " -> I18N content may have not been set for locale '" + next + "'");
                }
                if (str.toLowerCase().contains("{o}")) {
                    if (arz != null) {
                        list.add("field '" + arz.name() + "'" + (obj != null ? " [" + obj + "]" : "") + " -> I18N content contains wrong replacement chars for locale '" + next + "': {o}. It must be {0}");
                    } else {
                        list.add("I18N content contains wrong replacement chars for locale '" + next + "': {o}. It must be {0}");
                    }
                }
            } else if (arz != null) {
                list.add("field '" + arz.name() + "'" + (obj != null ? " [" + obj + "]" : "") + " -> I18N content has an invalid locale: '" + next + "'");
            } else {
                list.add("I18N content has an invalid locale: '" + next + "'");
            }
        }
        for (int i2 = 0; i2 < zArr.length; i2++) {
            if (!zArr[i2]) {
                if (arz != null) {
                    list.add("field '" + arz.name() + "'" + (obj != null ? " [" + obj + "]" : "") + " -> there is no I18N content for locale '" + C1584XI.values()[i2].getCode() + "'");
                } else {
                    list.add("There is no I18N content for locale '" + C1584XI.values()[i2].getCode() + "'");
                }
            }
        }
    }
}
