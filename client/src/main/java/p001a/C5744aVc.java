package p001a;

import game.geometry.Matrix3fWrap;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.camera.Camera;
import taikodom.render.scene.SceneObject;

/* renamed from: a.aVc  reason: case insensitive filesystem */
/* compiled from: a */
public class C5744aVc extends C6258ajW {
    public final Vec3d distance = new Vec3d();
    public float maxPitch;
    public float pitch;
    public float yaw;
    double maxZoomOut;
    double zoom;
    double zoomFactor;
    private C0763Kt stack = C0763Kt.bcE();

    public C5744aVc(Camera camera) {
        super(camera);
        this.distance.set(ScriptRuntime.NaN, 30.0d, 60.0d);
        this.maxPitch = 60.0f;
        this.maxZoomOut = 10.0d;
        this.zoomFactor = 1.0d;
    }

    public Vec3d getDistance() {
        return this.distance;
    }

    public void setDistance(Vec3d ajr) {
        if (ajr.lengthSquared() < 0.01d) {
            this.distance.set(ScriptRuntime.NaN, ScriptRuntime.NaN, 1.0d);
        }
        this.distance.mo9484aA(ajr);
    }

    public double getZoomFactor() {
        return this.zoomFactor;
    }

    public void setZoomFactor(double d) {
        this.zoomFactor = d;
    }

    public float getMaxPitch() {
        return this.maxPitch;
    }

    public void setMaxPitch(float f) {
        this.maxPitch = f;
    }

    public double getZoom() {
        return this.zoom;
    }

    public void setZoom(double d) {
        this.zoom = d;
    }

    public float getYaw() {
        return this.yaw;
    }

    public void setYaw(float f) {
        this.yaw = f;
    }

    public float getPitch() {
        return this.pitch;
    }

    public void setPitch(float f) {
        this.pitch = f;
    }

    public double getMaxZoomOut() {
        return this.maxZoomOut;
    }

    public void setMaxZoomOut(double d) {
        this.maxZoomOut = d;
    }

    /* renamed from: a */
    public void mo3859a(float f, C1003Om om) {
        this.stack = this.stack.bcD();
        this.stack.bcI().push();
        this.stack.bcH().push();
        this.stack.bcN().push();
        this.stack.bcK().push();
        try {
            Vec3d ajr = (Vec3d) this.stack.bcI().get();
            ajr.mo9484aA(this.distance);
            ajr.normalize();
            ajr.scale(this.zoom * this.zoomFactor);
            ajr.add(this.distance);
            Matrix3fWrap ajd = (Matrix3fWrap) this.stack.bcK().get();
            Matrix3fWrap ajd2 = (Matrix3fWrap) this.stack.bcK().get();
            ajd.setIdentity();
            ajd2.setIdentity();
            ajd2.rotateY(this.yaw * 0.017453292f);
            ajd.rotateX(this.pitch * 0.017453292f);
            ajd2.mul(ajd);
            ajd2.transform(ajr);
            TransformWrap IQ = om.mo4490IQ();
            om.bls().set(0.0f, 0.0f, 0.0f);
            om.blt().set(0.0f, 0.0f, 0.0f);
            om.blp().set(0.0f, 0.0f, 0.0f);
            om.bln().set(0.0f, 0.0f, 0.0f);
            om.blr().set(0.0f, 0.0f, 0.0f);
            om.blq().set(0.0f, 0.0f, 0.0f);
            om.blo().set(0.0f, 0.0f, 0.0f);
            om.blm().set(0.0f, 0.0f, 0.0f);
            IQ.orientation.mul(this.fSF.mo2472kQ().mo4490IQ().orientation, ajd2);
            this.fSF.mo2472kQ().mo4490IQ().mo17343b(ajr, IQ.position);
            this.camera.setFovY(this.fSD);
        } finally {
            this.stack.bcI().pop();
            this.stack.bcH().pop();
            this.stack.bcN().pop();
            this.stack.bcK().pop();
        }
    }

    public SceneObject cfO() {
        return this.fSG;
    }

    /* renamed from: m */
    public void mo11822m(SceneObject sceneObject) {
        this.fSG = sceneObject;
    }
}
