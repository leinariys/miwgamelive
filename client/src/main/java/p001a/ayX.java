package p001a;

import logic.res.scene.C2036bO;
import logic.res.scene.C3038nJ;
import logic.res.scene.C5821abB;
import logic.res.scene.C6980axt;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/* renamed from: a.ayX */
/* compiled from: a */
public class ayX implements C3915wj {
    private static final Log logger = LogFactory.getLog(ayX.class);
    private final C2036bO dOK;
    private final C5821abB dOL;
    private final String gXl;
    private final String gXm;

    public ayX(String str, String str2, C2036bO bOVar, C5821abB abb) {
        this.gXl = str;
        this.gXm = str2;
        this.dOK = bOVar;
        this.dOL = abb;
    }

    public void execute() {
        C2036bO bHA = new aUD(this.gXl).mo7458j(this.dOK).bHA();
        if (bHA == null) {
            C5226aBe abe = new C5226aBe("Render object not found: " + this.gXl);
            logger.error(abe);
            throw abe;
        } else if (!(bHA instanceof C6980axt)) {
            C5226aBe abe2 = new C5226aBe("Specified object '" + this.gXl + "' must be an instance of RenderObject instead of '" + bHA.getClass().getSimpleName() + "'");
            logger.error(abe2);
            throw abe2;
        } else {
            C3038nJ nJVar = (C3038nJ) this.dOL.mo12393ih(this.gXm);
            if (nJVar == null) {
                C5226aBe abe3 = new C5226aBe("Material not found: " + this.gXm);
                logger.error(abe3);
                throw abe3;
            }
            ((C6980axt) bHA).mo9377b(nJVar);
        }
    }
}
