package p001a;

import game.script.space.StellarSystem;

import javax.swing.tree.DefaultMutableTreeNode;

/* renamed from: a.Re */
/* compiled from: a */
public class C1196Re extends DefaultMutableTreeNode {
    public C1196Re(StellarSystem jj) {
        super(jj);
    }

    public String toString() {
        if (getUserObject() instanceof StellarSystem) {
            return ((StellarSystem) getUserObject()).mo3250ke().get();
        }
        return C1196Re.super.toString();
    }
}
