package p001a;

import com.hoplon.geometry.Color;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import game.script.Actor;
import game.script.missile.Missile;
import game.script.space.Asteroid;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.scene.RBillboard;
import taikodom.render.scene.SceneObject;

import java.util.Map;

/* renamed from: a.aDK */
/* compiled from: a */
class aDK implements C6296akI.C1925a {

    /* renamed from: gu */
    final /* synthetic */ C1494W f2548gu;
    float totalTime = 0.0f;

    aDK(C1494W w) {
        this.f2548gu = w;
    }

    /* renamed from: a */
    public void mo1968a(float f, long j) {
        float f2;
        SceneObject cHg;
        SceneObject cHg2;
        float f3;
        if (this.f2548gu.f1938mb) {
            this.totalTime += f;
            if (((float) Math.cos((double) (this.totalTime * 20.0f))) > 0.0f) {
                f2 = 1.0f;
            } else {
                f2 = 0.0f;
            }
            Vec3d ajr = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
            TransformWrap bcVar = new TransformWrap();
            this.f2548gu.f1936lZ.setRender(false);
            this.f2548gu.f1948mm.setPrimitiveColor(1.0f, 1.0f, 1.0f, 0.05f);
            ajr.z = 200.0d;
            float sin = (float) Math.sin(-0.17453292519943295d);
            bcVar.setTranslation(ajr);
            this.f2548gu.f1934lX.setTransform(bcVar);
            if (this.f2548gu.f1931lU != null && (cHg = this.f2548gu.f1931lU.cHg()) != null) {
                TransformWrap globalTransform = cHg.getGlobalTransform();
                TransformWrap bcVar2 = new TransformWrap();
                globalTransform.mo17336a(bcVar2);
                Vec3d ajr2 = globalTransform.position;
                this.f2548gu.f1935lY.getMesh().getIndexes().clear();
                C1494W.C1497c cVar = this.f2548gu.f1939md[this.f2548gu.f1940me];
                float w = this.f2548gu.f1941mf.mo6478w(f);
                float f4 = w * w;
                float f5 = 156.0f / w;
                Vec3d ajr3 = new Vec3d();
                Color color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
                Vec3d ajr4 = new Vec3d();
                Vec3d ajr5 = ajr4;
                int i = 0;
                for (Map.Entry entry : this.f2548gu.f1942mg.entrySet()) {
                    Actor cr = (Actor) entry.getKey();
                    if (!(cr == this.f2548gu.f1931lU || (cHg2 = cr.cHg()) == null)) {
                        RBillboard rBillboard = (RBillboard) entry.getValue();
                        Vec3d ajr6 = cHg2.getGlobalTransform().position;
                        ajr5.sub(ajr6, globalTransform.position);
                        C1494W.C1496b a = cVar.mo6479a(cr, ajr2, ajr6, f4);
                        if (a != C1494W.C1496b.NOT_VISIBLE || cr == this.f2548gu.f1949mn) {
                            bcVar2.multiply3x3(ajr5, ajr5);
                            if (a == C1494W.C1496b.NOT_VISIBLE) {
                                this.f2548gu.f1936lZ.setMaterial(this.f2548gu.f1953mr);
                                double length = ajr5.length();
                                if (length > ((double) cVar.getRadius())) {
                                    ajr5.scale(1.0d / length);
                                    ajr5.scale((double) cVar.getRadius());
                                }
                            } else {
                                this.f2548gu.f1936lZ.setMaterial(this.f2548gu.f1952mq);
                            }
                            Vec3d Z = ajr5.mo9476Z((double) f5);
                            color.x = 0.75f;
                            color.y = 0.75f;
                            color.z = 0.75f;
                            color.w = 0.75f;
                            if (a == C1494W.C1496b.ALLIED) {
                                color.x = 0.0f;
                                color.z = 0.0f;
                            } else if (a == C1494W.C1496b.ENEMY) {
                                color.y = 0.0f;
                                color.z = 0.0f;
                            }
                            if (cr instanceof Missile) {
                                if (((Missile) cr).agB() == this.f2548gu.f1931lU) {
                                    color.x = 0.75f;
                                    color.y = 0.0f;
                                    color.z = 0.0f;
                                    f3 = f2;
                                }
                                f3 = 1.0f;
                            } else {
                                if (cr instanceof Asteroid) {
                                    color.w = 0.5f;
                                }
                                f3 = 1.0f;
                            }
                            Z.x = (double) ((int) Z.x);
                            double d = ((double) sin) * Z.z;
                            float f6 = (float) ((int) (Z.y + d));
                            if (Z.y > ScriptRuntime.NaN) {
                                Z.y -= 4.0d;
                                if (Z.y < ScriptRuntime.NaN) {
                                    Z.y = ScriptRuntime.NaN;
                                }
                            } else if (Z.y < ScriptRuntime.NaN) {
                                Z.y += 4.0d;
                                if (Z.y > ScriptRuntime.NaN) {
                                    Z.y = ScriptRuntime.NaN;
                                }
                            }
                            Z.y = (double) ((int) (Z.y + d));
                            Z.z = (double) ((int) Z.z);
                            int transformedHeight = (int) rBillboard.getMaterial().getDiffuseTexture().getTransformedHeight();
                            Z.x += 0.5d * ((double) (((int) rBillboard.getMaterial().getDiffuseTexture().getTransformedWidth()) % 2));
                            Z.y += 0.5d * ((double) (transformedHeight % 2));
                            float f7 = (float) (((double) f6) + (((double) (transformedHeight % 2)) * 0.5d));
                            this.f2548gu.f1935lY.setVertex(i, (float) Z.x, (float) d, (float) Z.z);
                            this.f2548gu.f1935lY.setVertex(i + 1, (float) Z.x, (float) Z.y, (float) Z.z);
                            int i2 = 0;
                            while (true) {
                                int i3 = i2;
                                if (i3 >= 4) {
                                    break;
                                }
                                this.f2548gu.f1935lY.setColor(i + i3, color.x * 0.6f, color.y * 0.6f, color.z * 0.6f, color.w * 0.75f);
                                i2 = i3 + 1;
                            }
                            int i4 = 4;
                            while (true) {
                                int i5 = i4;
                                if (i5 >= 8) {
                                    break;
                                }
                                this.f2548gu.f1935lY.setColor(i + i5, 0.0f, 0.0f, 0.0f, 0.3f);
                                i4 = i5 + 1;
                            }
                            if (Z.y < ScriptRuntime.NaN) {
                                this.f2548gu.f1935lY.setVertex(i + 2, (float) Z.x, (float) d, (float) Z.z);
                                this.f2548gu.f1935lY.setVertex(i + 3, ((float) Z.x) + 5.0f, (float) d, (float) Z.z);
                                this.f2548gu.f1935lY.setVertex(i + 6, ((float) Z.x) + 1.0f, ((float) d) - 1.0f, (float) Z.z);
                                this.f2548gu.f1935lY.setVertex(i + 7, ((float) Z.x) + 6.0f, ((float) d) - 1.0f, (float) Z.z);
                            } else {
                                this.f2548gu.f1935lY.setVertex(i + 2, (float) Z.x, (float) d, (float) Z.z);
                                this.f2548gu.f1935lY.setVertex(i + 3, ((float) Z.x) - 5.0f, (float) d, (float) Z.z);
                                this.f2548gu.f1935lY.setVertex(i + 6, ((float) Z.x) + 1.0f, ((float) d) - 1.0f, (float) Z.z);
                                this.f2548gu.f1935lY.setVertex(i + 7, ((float) Z.x) - 4.0f, ((float) d) - 1.0f, (float) Z.z);
                            }
                            color.w = 1.0f;
                            if (cr instanceof Asteroid) {
                                color.w = 0.5f;
                            }
                            this.f2548gu.f1947ml.show();
                            color.x *= color.w * f3;
                            color.y *= color.w * f3;
                            color.z *= color.w * f3;
                            color.w *= f3;
                            rBillboard.setPrimitiveColor(color);
                            this.f2548gu.f1935lY.setVertex(i + 4, ((float) Z.x) + 1.0f, ((float) d) - 1.0f, (float) Z.z);
                            this.f2548gu.f1935lY.setVertex(i + 5, ((float) Z.x) + 1.0f, ((float) Z.y) - 1.0f, (float) Z.z);
                            for (int i6 = 0; i6 < 8; i6++) {
                                this.f2548gu.f1935lY.setIndex(i + i6, i + i6);
                            }
                            int i7 = i + 8;
                            ajr3.mo9484aA(Z);
                            ajr3.y = (double) f7;
                            rBillboard.setPosition(ajr3);
                            rBillboard.show();
                            if (this.f2548gu.f1949mn == cr) {
                                color.w = 1.0f;
                                this.f2548gu.f1936lZ.setRender(true);
                                this.f2548gu.f1936lZ.setPosition(ajr3);
                                this.f2548gu.f1936lZ.setPrimitiveColor(color);
                                ajr5 = Z;
                                i = i7;
                            } else {
                                ajr5 = Z;
                                i = i7;
                            }
                        } else {
                            rBillboard.hide();
                        }
                    }
                }
            }
        }
    }
}
