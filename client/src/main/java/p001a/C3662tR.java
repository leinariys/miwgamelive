package p001a;

import gnu.trove.TIntObjectHashMap;

/* renamed from: a.tR */
/* compiled from: a */
public class C3662tR {
    private int btE;
    private TIntObjectHashMap<C6696ars> btF;
    private TIntObjectHashMap<C6524aoc> btG;
    private Class<?> clazz;
    private String clazzName;

    public C3662tR(int i, String str) {
        this.btE = i;
        this.clazzName = str;
    }

    public C3662tR(int i, Class<?> cls) {
        this.btE = i;
        this.clazz = cls;
        this.clazzName = cls.getName();
    }

    public int aiK() {
        return this.btE;
    }

    public void aiL() {
        if (this.clazz == null) {
            this.clazz = Class.forName(this.clazzName);
        }
    }

    public Class<?> getClazz() {
        if (this.clazz == null) {
            this.clazz = Class.forName(this.clazzName);
        }
        return this.clazz;
    }

    public TIntObjectHashMap<C6696ars> aiM() {
        return this.btF;
    }

    public TIntObjectHashMap<C6524aoc> aiN() {
        return this.btG;
    }
}
