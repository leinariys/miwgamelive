package p001a;

/* renamed from: a.PS */
/* compiled from: a */
public class C1055PS {
    private long swigCPtr;

    public C1055PS(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C1055PS() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m8429a(C1055PS ps) {
        if (ps == null) {
            return 0;
        }
        return ps.swigCPtr;
    }
}
