package p001a;

import game.network.message.externalizable.C6302akO;
import logic.baa.C1616Xf;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aTd  reason: case insensitive filesystem */
/* compiled from: a */
public class C5693aTd extends C6302akO {

    private Class<? extends C1616Xf> clazz;

    public C5693aTd() {
    }

    public C5693aTd(Class<? extends C1616Xf> cls) {
        this.clazz = cls;
    }

    public Class<? extends C1616Xf> getClazz() {
        return this.clazz;
    }

    public void readExternal(ObjectInput objectInput) {
        this.clazz = (Class) C5496aLo.ilg.inputReadObject(objectInput);
    }

    public void writeExternal(ObjectOutput objectOutput) {
        C5496aLo.ilg.serializeData(objectOutput, this.clazz);
    }
}
