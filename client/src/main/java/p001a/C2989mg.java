package p001a;

/* renamed from: a.mg */
/* compiled from: a */
public final class C2989mg extends aOG {
    public static final C5961adl aBH = new C5961adl("KeyboardEvent");
    private int aBD;
    private boolean aBE;
    private int aBF;
    private boolean aBG;
    private int modifiers;

    public C2989mg(int i, boolean z, int i2, int i3, boolean z2) {
        this.aBD = i;
        this.aBE = z;
        this.aBF = i2;
        this.modifiers = i3;
        this.aBG = z2;
    }

    /* renamed from: Ni */
    public static C5961adl m35830Ni() {
        return aBH;
    }

    /* renamed from: Nh */
    public final int mo20584Nh() {
        return this.aBD;
    }

    public final boolean getState() {
        return this.aBE;
    }

    public final int getUnicode() {
        return this.aBF;
    }

    /* renamed from: Fp */
    public C5961adl mo2005Fp() {
        return aBH;
    }

    public int getModifiers() {
        return this.modifiers;
    }

    public boolean isPressed() {
        return this.aBE;
    }

    public boolean isTyped() {
        return this.aBG;
    }
}
