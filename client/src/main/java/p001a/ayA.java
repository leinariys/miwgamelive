package p001a;

import game.script.Actor;
import game.script.item.EnergyWeapon;
import game.script.main.bot.BotScript;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.ship.Station;

/* renamed from: a.ayA */
/* compiled from: a */
public abstract class ayA {
    private final C1285Sv gTV;
    public String gTY;
    public C1285Sv.C1286a gTZ;
    public int modifier;
    public int runs = 0;
    public int state = 0;
    boolean cRV = true;
    boolean finished = true;
    String gTT = "";
    long gTU = System.currentTimeMillis();
    ayA gTW = null;
    long gTX;

    public ayA(C1285Sv sv) {
        this.gTV = sv;
    }

    /* access modifiers changed from: protected */
    public abstract void aGu();

    /* access modifiers changed from: protected */
    /* renamed from: jh */
    public void mo16928jh(long j) {
        this.gTX = System.currentTimeMillis() + j;
    }

    public void resume() {
        this.cRV = false;
    }

    public void restart() {
        this.finished = false;
        this.state = 0;
        resume();
    }

    public void pause() {
        this.cRV = true;
    }

    public boolean cDJ() {
        return this.cRV;
    }

    public void finish() {
        mo16918a("", C1285Sv.C1286a.Waiting);
        this.finished = true;
    }

    public boolean finished() {
        return this.finished;
    }

    /* renamed from: gZ */
    public final void mo16926gZ() {
        if (!this.cRV && !this.finished && System.currentTimeMillis() >= this.gTX) {
            if (this.gTW != null) {
                if (this.gTW.finished()) {
                    this.gTW = null;
                } else {
                    return;
                }
            }
            aGu();
        }
    }

    /* access modifiers changed from: protected */
    public void log(String str) {
        mo16931r(str, false);
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public void mo16931r(String str, boolean z) {
        this.gTT = str;
        this.gTU = System.currentTimeMillis();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo16917a(ayA aya) {
        aya.restart();
        this.gTW = aya;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: dL */
    public Player mo5503dL() {
        return this.gTV.mo5503dL();
    }

    /* access modifiers changed from: package-private */
    public EnergyWeapon btF() {
        return this.gTV.btF();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: al */
    public Ship mo5495al() {
        return this.gTV.mo5495al();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: eT */
    public Station mo5504eT() {
        return this.gTV.mo5504eT();
    }

    /* access modifiers changed from: package-private */
    public C2675iR btG() {
        return this.gTV.btG();
    }

    /* access modifiers changed from: package-private */
    public C6563apP btH() {
        return this.gTV.btH();
    }

    /* access modifiers changed from: package-private */
    public ayA btI() {
        return this.gTV.btI();
    }

    /* access modifiers changed from: package-private */
    public BotScript btJ() {
        return this.gTV.btJ();
    }

    /* access modifiers changed from: package-private */
    public Taikodom ala() {
        return this.gTV.ala();
    }

    /* access modifiers changed from: package-private */
    public void btE() {
        this.gTV.btE();
    }

    public String getStateString() {
        return this.gTY;
    }

    public C1285Sv cDK() {
        return this.gTV;
    }

    /* renamed from: a */
    public void mo16918a(String str, C1285Sv.C1286a aVar) {
        this.gTY = str;
        this.gTZ = aVar;
        if (this.gTV != null) {
            this.gTV.mo16918a(str, aVar);
        }
    }

    /* renamed from: a */
    public void mo16919a(String str, C1285Sv.C1287b bVar) {
        this.gTY = str;
        this.modifier |= bVar.modifier;
        if (this.gTV != null) {
            this.gTV.mo16919a(str, bVar);
        }
    }

    /* renamed from: b */
    public void mo16920b(String str, C1285Sv.C1287b bVar) {
        this.gTY = str;
        this.modifier &= bVar.modifier ^ -1;
        if (this.gTV != null) {
            this.gTV.mo16920b(str, bVar);
        }
    }

    /* access modifiers changed from: protected */
    public void bai() {
        mo5495al().mo18250A(C3904wY.CANNON);
    }

    /* access modifiers changed from: protected */
    public void cDL() {
        mo5495al().mo2967X((Actor) null);
        mo5495al().mo18331y(C3904wY.CANNON);
    }
}
