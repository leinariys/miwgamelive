package p001a;

/* renamed from: a.asL  reason: case insensitive filesystem */
/* compiled from: a */
public final class C6715asL {
    public static final C6715asL gwa = new C6715asL("GrannyColorMapTextureType");
    public static final C6715asL gwb = new C6715asL("GrannyCubeMapTextureType");
    public static final C6715asL gwc = new C6715asL("GrannyOnePastLastTextureType");
    private static C6715asL[] gwd = {gwa, gwb, gwc};

    /* renamed from: pF */
    private static int f5276pF = 0;

    /* renamed from: pG */
    private final int f5277pG;

    /* renamed from: pH */
    private final String f5278pH;

    private C6715asL(String str) {
        this.f5278pH = str;
        int i = f5276pF;
        f5276pF = i + 1;
        this.f5277pG = i;
    }

    private C6715asL(String str, int i) {
        this.f5278pH = str;
        this.f5277pG = i;
        f5276pF = i + 1;
    }

    private C6715asL(String str, C6715asL asl) {
        this.f5278pH = str;
        this.f5277pG = asl.f5277pG;
        f5276pF = this.f5277pG + 1;
    }

    /* renamed from: us */
    public static C6715asL m25653us(int i) {
        if (i < gwd.length && i >= 0 && gwd[i].f5277pG == i) {
            return gwd[i];
        }
        for (int i2 = 0; i2 < gwd.length; i2++) {
            if (gwd[i2].f5277pG == i) {
                return gwd[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C6715asL.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f5277pG;
    }

    public String toString() {
        return this.f5278pH;
    }
}
