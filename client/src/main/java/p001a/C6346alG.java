package p001a;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.alG  reason: case insensitive filesystem */
/* compiled from: a */
public @interface C6346alG {
    String value();
}
