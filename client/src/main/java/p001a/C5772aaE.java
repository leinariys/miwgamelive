package p001a;

import game.network.channel.client.ClientConnectImpl;
import org.apache.mina.core.session.IoSession;

import java.net.InetSocketAddress;

/* renamed from: a.aaE  reason: case insensitive filesystem */
/* compiled from: a */
public final class C5772aaE extends ClientConnectImpl {

    private final transient IoSession etm;
    private OpenSocketMina.C2680a eVF;

    public C5772aaE(IoSession ioSession) {
        super(Attitude.CHILD, OpenSocketMina.nextId.getAndIncrement(), ((InetSocketAddress) ioSession.getRemoteAddress()).getAddress());
        this.etm = ioSession;
    }

    public IoSession getSession() {
        return this.etm;
    }

    public OpenSocketMina.C2680a bLH() {
        return this.eVF;
    }

    /* renamed from: a */
    public void mo12141a(OpenSocketMina.C2680a aVar) {
        this.eVF = aVar;
    }

    public void close() {
        this.etm.close();
    }
}
