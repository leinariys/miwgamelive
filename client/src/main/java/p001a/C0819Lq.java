package p001a;

import game.geometry.Vec3d;
import game.geometry.aLH;
import logic.aaa.C2235dB;
import logic.thred.C6615aqP;
import org.mozilla1.classfile.C6192aiI;

import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: a.Lq */
/* compiled from: a */
public class C0819Lq implements aNW, C2235dB {
    final int flags;
    private final C6192aiI dun;
    aLH aabb;
    long buF;
    C2235dB duo;
    long dup;
    int duq;
    aRR dut;
    private Map<C6625aqZ, C0821b> dur;
    private C0463GP<C0819Lq> dus;
    private boolean valid = true;

    public C0819Lq(C6192aiI aii, C2235dB dBVar, int i) {
        this.dun = aii;
        this.duo = dBVar;
        this.flags = i;
    }

    /* access modifiers changed from: package-private */
    public void bft() {
        this.dup = this.buF + ((long) this.dun.fOo);
    }

    public int bfu() {
        return this.duq;
    }

    /* renamed from: lz */
    public void mo3842lz(int i) {
        this.duq = i;
    }

    public void dispose() {
        if (this.valid) {
            this.valid = false;
        }
        if (this.dus != null) {
            this.dus.dispose();
            this.dus = null;
        }
        if (this.dut != null) {
            this.dut.dispose();
            this.dut = null;
        }
    }

    /* renamed from: kO */
    public void mo3841kO() {
        if (this.dut != null) {
            C6615aqP aqp = (C6615aqP) this.duo;
            this.aabb.reset();
            aqp.mo2437IL().getBoundingBox().mo23419a(aqp.getOrientation(), this.duo.getPosition(), this.aabb);
            this.dut.mo1234c(this.aabb);
        }
        if (this.dus != null) {
            this.dus.mo2339kO();
        }
    }

    private boolean bfv() {
        return this.valid;
    }

    public C2235dB bfw() {
        return this.duo;
    }

    /* renamed from: a */
    public void mo3830a(C0463GP<C0819Lq> gp) {
        this.dus = gp;
    }

    public Vec3d getPosition() {
        return this.duo.getPosition();
    }

    public long bfx() {
        return this.buF;
    }

    /* renamed from: eW */
    public void mo3839eW(long j) {
        this.buF = j;
    }

    /* renamed from: b */
    public float mo3831b(aNW anw) {
        if (anw != null) {
            aRR arr = ((C0819Lq) anw).dut;
            if (arr == null || this.dut == null) {
                return this.duo.getPosition().mo9486aC(anw.bfw().getPosition());
            }
            return this.dut.mo1233b(arr);
        }
        System.err.println("NanoHandle: " + this + " - trying to get distance to null");
        return -1.0f;
    }

    /* renamed from: F */
    public float mo3829F(Vec3d ajr) {
        return this.dut.mo1232F(ajr);
    }

    public String toString() {
        return "NanoHandle:" + this.dun.hashCode() + "[" + this.duo + "]";
    }

    public C0463GP<C0819Lq> bfy() {
        return this.dus;
    }

    public Map<C6625aqZ, C0821b> bfz() {
        if (this.dur == null) {
            this.dur = new C0820a();
        }
        return this.dur;
    }

    public int getFlags() {
        return this.flags;
    }

    public boolean isReady() {
        return true;
    }

    /* renamed from: a.Lq$b */
    /* compiled from: a */
    static class C0821b {
        long hQp;

        C0821b() {
        }
    }

    /* renamed from: a.Lq$a */
    class C0820a extends WeakHashMap<C6625aqZ, C0821b> {
        C0820a() {
        }

        /* renamed from: ah */
        public C0821b get(Object obj) {
            C0821b bVar = (C0821b) super.get(obj);
            if (bVar != null) {
                return bVar;
            }
            C0821b bVar2 = new C0821b();
            super.put((C6625aqZ) obj, bVar2);
            return bVar2;
        }
    }
}
