package p001a;

import game.script.aggro.AggroTable;

import java.util.Comparator;

/* renamed from: a.yk */
/* compiled from: a */
class C4061yk implements Comparator<AggroTable.C2308a> {
    C4061yk() {
    }

    /* renamed from: a */
    public int compare(AggroTable.C2308a aVar, AggroTable.C2308a aVar2) {
        float f = aVar2.bDJ - aVar.bDJ;
        if (f == 0.0f) {
            return 0;
        }
        return f > 0.0f ? 1 : -1;
    }
}
