package p001a;

/* renamed from: a.aOX */
/* compiled from: a */
public class aOX extends RuntimeException {
    private static final long serialVersionUID = 1313554518481407136L;

    public aOX() {
    }

    public aOX(String str) {
        super(str);
    }

    public aOX(Throwable th) {
        super(th);
    }

    public aOX(String str, Throwable th) {
        super(str, th);
    }
}
