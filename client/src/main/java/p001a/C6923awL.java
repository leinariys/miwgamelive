package p001a;

/* renamed from: a.awL  reason: case insensitive filesystem */
/* compiled from: a */
public class C6923awL {

    /* renamed from: a */
    private int f5508a = 0;

    /* renamed from: b */
    private int f5509b = 0;
    private int gNl = 0;
    private int gNm = 0;

    public C6923awL(String str) {
        int i = 0;
        i = str.charAt(0) == '#' ? 1 : i;
        int length = str.length() - i;
        if (length == 3) {
            this.gNl = Integer.parseInt(str.substring(i + 0, i + 1), 16);
            this.gNm = Integer.parseInt(str.substring(i + 1, i + 2), 16);
            this.f5509b = Integer.parseInt(str.substring(i + 2, i + 3), 16);
            this.gNl = (this.gNl << 4) | this.gNl;
            this.gNm = (this.gNm << 4) | this.gNm;
            this.f5509b = (this.f5509b << 4) | this.f5509b;
            this.f5508a = 255;
        } else if (length == 6) {
            this.gNl = Integer.parseInt(str.substring(i + 0, i + 2), 16);
            this.gNm = Integer.parseInt(str.substring(i + 2, i + 4), 16);
            this.f5509b = Integer.parseInt(str.substring(i + 4, i + 6), 16);
            this.f5508a = 255;
        } else if (length == 8) {
            this.f5508a = Integer.parseInt(str.substring(i + 0, i + 2), 16);
            this.gNl = Integer.parseInt(str.substring(i + 2, i + 4), 16);
            this.gNm = Integer.parseInt(str.substring(i + 4, i + 6), 16);
            this.f5509b = Integer.parseInt(str.substring(i + 6, i + 8), 16);
        }
    }

    /* access modifiers changed from: package-private */
    public int getRed() {
        return this.gNl;
    }

    /* access modifiers changed from: package-private */
    public int getGreen() {
        return this.gNm;
    }

    /* access modifiers changed from: package-private */
    public int getBlue() {
        return this.f5509b;
    }

    /* access modifiers changed from: package-private */
    public int getAlpha() {
        return this.f5509b;
    }
}
