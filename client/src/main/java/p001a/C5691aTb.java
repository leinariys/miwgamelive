package p001a;

import game.script.ship.Hull;
import game.script.ship.Shield;
import logic.WrapRunnable;
import taikodom.addon.hud.HudSelectedTargetAddon;

/* renamed from: a.aTb  reason: case insensitive filesystem */
/* compiled from: a */
class C5691aTb extends WrapRunnable {
    final /* synthetic */ HudSelectedTargetAddon agx;

    public C5691aTb(HudSelectedTargetAddon hudSelectedTargetAddon) {
        this.agx = hudSelectedTargetAddon;
    }

    public void run() {
        Hull zt;
        Shield zv;
        if (this.agx.ams instanceof aDA) {
            aDA ada = (aDA) this.agx.ams;
            if (!(this.agx.f9917Bp == null || (zv = ada.mo8288zv()) == null)) {
                if (zv.mo19089hh() > 0.0f) {
                    this.agx.f9917Bp.setValue(zv.mo19079Tx());
                } else {
                    this.agx.f9917Bp.setValue(0);
                }
            }
            if (this.agx.f9916Bo != null && (zt = ada.mo8287zt()) != null) {
                if (zt.mo19934hh() > 0.0f) {
                    this.agx.f9916Bo.setValue(zt.mo19923Tx());
                } else {
                    this.agx.f9916Bo.setValue(0);
                }
            }
        }
    }
}
