package p001a;

/* renamed from: a.agf  reason: case insensitive filesystem */
/* compiled from: a */
public class C6111agf extends C6760atE {
    private C6548apA fwA = new C2887lV.C2889b();
    private C6548apA fwB = new C2887lV.C2890c();
    private C6548apA fwC = C3806vE.f9395Qm;
    private C6548apA fwD = C3806vE.bzA;
    private C6548apA fwE = aGN.f2859Qm;
    private C6548apA fwF = C2566gt.f7785Qm;
    private C6548apA fwG;
    private C6548apA fwH;
    private C6548apA fwI;
    private C6548apA fwJ;
    private C6548apA fwK = new C2544gd.C2545a();
    private C6548apA fwL = new C2544gd.C2545a();
    private C0327ES fwx = new C0327ES();
    private C5999aeX fwy = new C5999aeX();
    private C6548apA fwz = new C1818aQ.C1819a(this.fwx, this.fwy);

    public C6111agf() {
        this.fwK.gmK = true;
    }

    /* renamed from: a */
    public C6548apA mo13454a(C3655tK tKVar, C3655tK tKVar2) {
        if (tKVar == C3655tK.SPHERE_SHAPE_PROXYTYPE && tKVar2 == C3655tK.SPHERE_SHAPE_PROXYTYPE) {
            return this.fwF;
        }
        if (tKVar.aiF() && tKVar2 == C3655tK.STATIC_PLANE_PROXYTYPE) {
            return this.fwL;
        }
        if (tKVar2.aiF() && tKVar == C3655tK.STATIC_PLANE_PROXYTYPE) {
            return this.fwK;
        }
        if (tKVar.aiF() && tKVar2.aiF()) {
            return this.fwz;
        }
        if (tKVar.aiF() && tKVar2.aiG()) {
            return this.fwA;
        }
        if (tKVar2.aiF() && tKVar.aiG()) {
            return this.fwB;
        }
        if (tKVar.isCompound()) {
            return this.fwC;
        }
        if (tKVar2.isCompound()) {
            return this.fwD;
        }
        return this.fwE;
    }
}
