package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.Up */
/* compiled from: a */
public class C1422Up {
    private Vec3f direction = new Vec3f();
    private Vec3f enR = new Vec3f();

    public C1422Up(C1422Up up) {
        this.enR.set(up.bxH());
        this.direction.set(up.getDirection());
    }

    public C1422Up(Vec3f vec3f) {
        this.enR.set(vec3f);
    }

    public C1422Up(Vec3f vec3f, Vec3f vec3f2) {
        mo5918s(vec3f, vec3f2);
    }

    public C1422Up() {
    }

    public Vec3f getDirection() {
        return this.direction;
    }

    public void setDirection(Vec3f vec3f) {
        this.direction.set(vec3f);
    }

    public Vec3f bxH() {
        return this.enR;
    }

    /* renamed from: s */
    public void mo5918s(Vec3f vec3f, Vec3f vec3f2) {
        this.enR.set(vec3f);
        this.direction.set(vec3f2);
    }

    /* renamed from: ag */
    public void mo5915ag(Vec3f vec3f) {
        this.enR.set(vec3f);
    }
}
