package p001a;

import game.network.message.externalizable.C0939Nn;
import game.script.Actor;
import game.script.corporation.Corporation;
import game.script.corporation.CorporationMembership;
import game.script.corporation.CorporationRoleInfo;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.ship.Station;
import logic.baa.C6200aiQ;
import logic.baa.aDJ;
import logic.data.link.C0274DX;
import logic.res.code.C5663aRz;
import logic.swing.C5378aHa;
import logic.ui.C2698il;
import logic.ui.item.Picture;
import logic.ui.item.Table;
import logic.ui.item.TextArea;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.sY */
/* compiled from: a */
public class C3554sY extends C1023Ov {
    /* access modifiers changed from: private */
    public C2698il edQ;
    /* access modifiers changed from: private */
    public List<Player> ixP = new ArrayList();
    private C6200aiQ<aDJ> ixN;
    private C6200aiQ<aDJ> ixO;
    private Table ixQ;
    private JButton ixR;
    private JButton ixS;
    private JButton ixT;
    private JButton ixU;
    private JButton ixV;

    public C3554sY(IAddonProperties vWVar, C2698il ilVar, Player aku, Corporation aPVar) {
        super(vWVar, aku, aPVar);
        this.edQ = ilVar;
        this.ixR = this.edQ.mo4913cb("promote");
        this.ixS = this.edQ.mo4913cb("demote");
        this.ixT = this.edQ.mo4913cb("hire");
        this.ixU = this.edQ.mo4913cb("fire");
        this.ixV = this.edQ.mo4913cb("leave");
        this.ixN = new C3555a();
        this.ixO = new C3556b();
    }

    /* access modifiers changed from: package-private */
    public void refresh() {
        String QG;
        mo4562c(this.edQ);
        TextArea cd = this.edQ.mo4915cd("messageOfTheDayInput");
        if (this.dLu.mo10699QG() == null) {
            QG = "";
        } else {
            QG = this.dLu.mo10699QG();
        }
        cd.setText(QG);
        cd.setEnabled(this.dLu.mo10718b(this.f1342P, C6704asA.POST_MESSAGE_OF_THE_DAY));
        this.ixR.setEnabled(this.dLu.mo10718b(this.f1342P, C6704asA.PROMOTE));
        this.ixS.setEnabled(this.dLu.mo10718b(this.f1342P, C6704asA.DEMOTE));
        this.ixT.setEnabled(this.dLu.mo10718b(this.f1342P, C6704asA.HIRE));
        this.ixU.setEnabled(this.dLu.mo10718b(this.f1342P, C6704asA.FIRE));
        if (!this.dLu.mo10726m(this.f1342P) || this.dLu.size() <= 1) {
            this.ixV.setEnabled(true);
            this.ixV.setToolTipText((String) null);
        } else {
            this.ixV.setEnabled(false);
            this.ixV.setToolTipText(this.f1343kj.translate("ceoLeaveButtonTooltipText"));
        }
        Table dmW = dmW();
        dmW.getModel().mo21912K(new ArrayList(this.dLu.mo10698QC()));
        int i = 0;
        for (CorporationMembership next : this.dLu.mo10698QC()) {
            if (next.mo17177dL().dyf()) {
                this.ixP.add(next.mo17177dL());
                i++;
            }
        }
        this.edQ.mo4917cf("currMembers").setText(String.valueOf(dmW.getModel().getRowCount()));
        this.edQ.mo4917cf("maxMembers").setText(String.valueOf(this.dLu.mo10702QQ()));
        this.edQ.mo4917cf("onlineMembers").setText(String.valueOf(i));
    }

    /* access modifiers changed from: package-private */
    public void blJ() {
    }

    /* access modifiers changed from: private */
    public CorporationMembership dmV() {
        try {
            Table dmW = dmW();
            return dmW.getModel().mo21918yI(dmW.getSelectedRow());
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: Fa */
    public void mo4555Fa() {
        TextArea cd = this.edQ.mo4915cd("messageOfTheDayInput");
        cd.addFocusListener(new C3563g(cd));
        this.ixR.addActionListener(new C3562f());
        this.ixS.addActionListener(new C3565i());
        this.ixT.addActionListener(new C3564h());
        this.ixU.addActionListener(new C3558d());
        this.ixV.addActionListener(new C3557c());
        this.dLu.mo8320a(C0274DX.aZe, (C6200aiQ<?>) this.ixN);
        this.dLu.mo8320a(C0274DX.cQO, (C6200aiQ<?>) this.ixO);
    }

    /* access modifiers changed from: package-private */
    public void removeListeners() {
        this.dLu.mo8326b(C0274DX.aZe, (C6200aiQ<?>) this.ixN);
        this.dLu.mo8326b(C0274DX.cQO, (C6200aiQ<?>) this.ixO);
    }

    /* access modifiers changed from: private */
    /* renamed from: zN */
    public String m38817zN(int i) {
        if (i == 0) {
            return this.f1343kj.translate("Today");
        }
        return MessageFormat.format(this.f1343kj.translate("{0} days"), new Object[]{Integer.valueOf(i)});
    }

    private Table dmW() {
        if (this.ixQ == null) {
            this.ixQ = this.edQ.mo4915cd("membersTable");
            this.ixQ.setModel(new C3575o(this, (C3575o) null));
            this.ixQ.setRowHeight(24);
            this.ixQ.getTableHeader().setReorderingAllowed(false);
        }
        return this.ixQ;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m38810c(Player aku, Corporation aPVar) {
        String format;
        Player aPC = this.f1343kj.ala().getPlayer();
        if (aku != aPC) {
            if (aPVar.mo10726m(aku)) {
                this.f1343kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, this.f1343kj.translate("You cannot promote the CEO"), new Object[0]));
                return;
            }
            if (!aPVar.mo10695I(aPC).cyU().dAp() || aPVar.mo10695I(aku).cyU().ordinal() != C5741aUz.dAr().ordinal() - 1) {
                format = MessageFormat.format(this.f1343kj.translate("Do you want to promote player {0}?"), new Object[]{aku.getName()});
            } else {
                format = MessageFormat.format(this.f1343kj.translate("Do you want to promote player {0}? After that, you will be demoted and {0} will be the new CEO"), new Object[]{aku.getName()});
            }
            mo4557a((aEP) new C3559e(aPVar, aku), format);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m38812d(Player aku, Corporation aPVar) {
        if (aku != this.f1343kj.ala().getPlayer()) {
            if (aPVar.mo10695I(aku).cyU() == C5741aUz.dAq()) {
                this.f1343kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, this.f1343kj.translate("This member has the lowest role level already"), new Object[0]));
                return;
            }
            mo4557a((aEP) new C3571m(aPVar, aku), MessageFormat.format(this.f1343kj.translate("Do you want to demote player {0}?"), new Object[]{aku.getName()}));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: n */
    public void m38816n(Corporation aPVar) {
        mo4557a((aEP) new C3574n(aPVar), this.f1343kj.translate("Please type the hiring player name"));
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m38814e(Player aku, Corporation aPVar) {
        if (aku != this.f1343kj.ala().getPlayer()) {
            mo4557a((aEP) new C3566j(aPVar, aku), MessageFormat.format(this.f1343kj.translate("Do you want to fire player {0}?"), new Object[]{aku.getName()}));
        }
    }

    /* access modifiers changed from: private */
    public void dmX() {
        mo4557a((aEP) new C3569k(), MessageFormat.format(this.f1343kj.translate("Do you really want to leave Corporation {0}? It will be finished after that"), new Object[]{this.dLu.getName()}));
    }

    /* access modifiers changed from: private */
    public void dmY() {
        mo4557a((aEP) new C3570l(), MessageFormat.format(this.f1343kj.translate("Do you want to leave Corporation {0}?"), new Object[]{this.dLu.getName()}));
    }

    /* access modifiers changed from: protected */
    public void blL() {
        if (this.dLu != null) {
            JTextField cd = this.edQ.mo4915cd("messageOfTheDayInput");
            if (cd.getText() != null) {
                this.dLu.mo10710aS(cd.getText().trim());
            }
        }
    }

    /* renamed from: a.sY$o */
    /* compiled from: a */
    private class C3575o extends AbstractTableModel {

        private List<CorporationMembership> ipv;

        private C3575o() {
        }

        /* synthetic */ C3575o(C3554sY sYVar, C3575o oVar) {
            this();
        }

        public Class<?> getColumnClass(int i) {
            if (i == 0) {
                return Picture.class;
            }
            return JLabel.class;
        }

        public String getColumnName(int i) {
            switch (i) {
                case 0:
                    return C3554sY.this.f1343kj.translate("Rank");
                case 1:
                    return C3554sY.this.f1343kj.translate("Name");
                case 2:
                    return C3554sY.this.f1343kj.translate("Status");
                case 3:
                    return C3554sY.this.f1343kj.translate("Last Time On");
                case 4:
                    return C3554sY.this.f1343kj.translate("Investment (T$)");
                default:
                    return C3554sY.super.getColumnName(i);
            }
        }

        public int getColumnCount() {
            return 5;
        }

        public int getRowCount() {
            if (this.ipv == null) {
                return 0;
            }
            return this.ipv.size();
        }

        /* renamed from: yI */
        public CorporationMembership mo21918yI(int i) {
            if (this.ipv == null) {
                return null;
            }
            return this.ipv.get(i);
        }

        public Object getValueAt(int i, int i2) {
            CorporationMembership azk = this.ipv.get(i);
            Player dL = azk.mo17177dL();
            if (!C3554sY.this.dLu.mo10694G(dL)) {
                return null;
            }
            switch (i2) {
                case 0:
                    CorporationRoleInfo I = C3554sY.this.dLu.mo10695I(dL);
                    Picture axm = new Picture(C3554sY.this.f1343kj.bHv().adz());
                    axm.mo16824a(C5378aHa.ICONOGRAPHY, C3554sY.this.mo4556a(C3554sY.this.dLu, I.cyU()));
                    axm.setToolTipText(I.getRoleName());
                    return axm;
                case 1:
                    JLabel jLabel = new JLabel();
                    jLabel.setText(dL.getName());
                    return jLabel;
                case 2:
                    JLabel jLabel2 = new JLabel();
                    if (!C3554sY.this.ixP.contains(dL)) {
                        jLabel2.setText(C3554sY.this.f1343kj.translate("ONI Offline"));
                    } else {
                        Actor bhE = dL.bhE();
                        if (bhE instanceof Station) {
                            jLabel2.setText(((Station) bhE).getName());
                        } else if (bhE instanceof Ship) {
                            jLabel2.setText(((Ship) bhE).azW().mo21665ke().get());
                        }
                    }
                    return jLabel2;
                case 3:
                    JLabel jLabel3 = new JLabel();
                    jLabel3.setText(C3554sY.this.m38817zN(azk.cGG()));
                    return jLabel3;
                case 4:
                    JLabel jLabel4 = new JLabel();
                    jLabel4.setText(String.valueOf(azk.cGI()));
                    return jLabel4;
                default:
                    return null;
            }
        }

        /* renamed from: K */
        public void mo21912K(List<CorporationMembership> list) {
            this.ipv = list;
            fireTableDataChanged();
        }
    }

    /* renamed from: a.sY$a */
    class C3555a implements C6200aiQ<aDJ> {
        C3555a() {
        }

        /* renamed from: a */
        public void mo1143a(aDJ adj, C5663aRz arz, Object obj) {
            if (obj != null) {
                C3554sY.this.edQ.mo4915cd("messageOfTheDayInput").setText(obj.toString());
            }
        }
    }

    /* renamed from: a.sY$b */
    /* compiled from: a */
    class C3556b implements C6200aiQ<aDJ> {
        C3556b() {
        }

        /* renamed from: a */
        public void mo1143a(aDJ adj, C5663aRz arz, Object obj) {
            if (C3554sY.this.dLu.mo10694G(C3554sY.this.f1342P)) {
                C3554sY.this.refresh();
            }
        }
    }

    /* renamed from: a.sY$g */
    /* compiled from: a */
    class C3563g extends FocusAdapter {
        private final /* synthetic */ TextArea bkM;

        C3563g(TextArea ae) {
            this.bkM = ae;
        }

        public void focusLost(FocusEvent focusEvent) {
            if (this.bkM.getText() != null) {
                C3554sY.this.dLu.mo10710aS(this.bkM.getText().trim());
            }
        }
    }

    /* renamed from: a.sY$f */
    /* compiled from: a */
    class C3562f implements ActionListener {
        C3562f() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            CorporationMembership c = C3554sY.this.dmV();
            if (c != null) {
                C3554sY.this.m38810c(c.mo17177dL(), C3554sY.this.dLu);
            }
        }
    }

    /* renamed from: a.sY$i */
    /* compiled from: a */
    class C3565i implements ActionListener {
        C3565i() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            CorporationMembership c = C3554sY.this.dmV();
            if (c != null) {
                C3554sY.this.m38812d(c.mo17177dL(), C3554sY.this.dLu);
            }
        }
    }

    /* renamed from: a.sY$h */
    /* compiled from: a */
    class C3564h implements ActionListener {
        C3564h() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3554sY.this.m38816n(C3554sY.this.dLu);
        }
    }

    /* renamed from: a.sY$d */
    /* compiled from: a */
    class C3558d implements ActionListener {
        C3558d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            CorporationMembership c = C3554sY.this.dmV();
            if (c != null) {
                C3554sY.this.m38814e(c.mo17177dL(), C3554sY.this.dLu);
            }
        }
    }

    /* renamed from: a.sY$c */
    /* compiled from: a */
    class C3557c implements ActionListener {
        C3557c() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (!C3554sY.this.dLu.mo10726m(C3554sY.this.f1342P)) {
                C3554sY.this.dmY();
            } else if (C3554sY.this.dLu.size() == 1) {
                C3554sY.this.dmX();
            }
        }
    }

    /* renamed from: a.sY$e */
    /* compiled from: a */
    class C3559e extends aEP {
        private final /* synthetic */ Corporation aTb;
        private final /* synthetic */ Player agw;

        C3559e(Corporation aPVar, Player aku) {
            this.aTb = aPVar;
            this.agw = aku;
        }

        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            if (i == 1) {
                ((Corporation) C3582se.m38985a(this.aTb, (C6144ahM<?>) new C3560a())).mo10691A(this.agw);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.CONFIRM;
        }

        /* renamed from: a.sY$e$a */
        class C3560a implements C6144ahM<Boolean> {
            C3560a() {
            }

            /* renamed from: a */
            public void mo1931n(Boolean bool) {
                if (!bool.booleanValue()) {
                    SwingUtilities.invokeLater(new C3561a());
                }
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
            }

            /* renamed from: a.sY$e$a$a */
            class C3561a implements Runnable {
                C3561a() {
                }

                public void run() {
                    C3554sY.this.blK();
                }
            }
        }
    }

    /* renamed from: a.sY$m */
    /* compiled from: a */
    class C3571m extends aEP {
        private final /* synthetic */ Corporation aTb;
        private final /* synthetic */ Player agw;

        C3571m(Corporation aPVar, Player aku) {
            this.aTb = aPVar;
            this.agw = aku;
        }

        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            if (i == 1) {
                ((Corporation) C3582se.m38985a(this.aTb, (C6144ahM<?>) new C3572a())).mo10692C(this.agw);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.CONFIRM;
        }

        /* renamed from: a.sY$m$a */
        class C3572a implements C6144ahM<Boolean> {
            C3572a() {
            }

            /* renamed from: a */
            public void mo1931n(Boolean bool) {
                if (!bool.booleanValue()) {
                    SwingUtilities.invokeLater(new C3573a());
                }
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
            }

            /* renamed from: a.sY$m$a$a */
            class C3573a implements Runnable {
                C3573a() {
                }

                public void run() {
                    C3554sY.this.blK();
                }
            }
        }
    }

    /* renamed from: a.sY$n */
    /* compiled from: a */
    class C3574n extends aEP {
        private final /* synthetic */ Corporation aTb;

        C3574n(Corporation aPVar) {
            this.aTb = aPVar;
        }

        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            if (i == 1) {
                this.aTb.mo10709aP(str.trim());
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: VC */
        public boolean mo8595VC() {
            return true;
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.INFO;
        }
    }

    /* renamed from: a.sY$j */
    /* compiled from: a */
    class C3566j extends aEP {
        private final /* synthetic */ Corporation aTb;
        private final /* synthetic */ Player agw;

        C3566j(Corporation aPVar, Player aku) {
            this.aTb = aPVar;
            this.agw = aku;
        }

        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            if (i == 1) {
                ((Corporation) C3582se.m38985a(this.aTb, (C6144ahM<?>) new C3567a())).mo10724d(C3554sY.this.f1343kj.ala().getPlayer(), this.agw);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.CONFIRM;
        }

        /* renamed from: a.sY$j$a */
        class C3567a implements C6144ahM<Boolean> {
            C3567a() {
            }

            /* renamed from: a */
            public void mo1931n(Boolean bool) {
                if (!bool.booleanValue()) {
                    SwingUtilities.invokeLater(new C3568a());
                }
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
            }

            /* renamed from: a.sY$j$a$a */
            class C3568a implements Runnable {
                C3568a() {
                }

                public void run() {
                    C3554sY.this.blK();
                }
            }
        }
    }

    /* renamed from: a.sY$k */
    /* compiled from: a */
    class C3569k extends aEP {
        C3569k() {
        }

        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            if (i == 1) {
                C3554sY.this.f1342P.mo14374d(false, true);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.WARNING;
        }
    }

    /* renamed from: a.sY$l */
    /* compiled from: a */
    class C3570l extends aEP {
        C3570l() {
        }

        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            if (i == 1) {
                C3554sY.this.f1342P.mo14374d(false, true);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.WARNING;
        }
    }
}
