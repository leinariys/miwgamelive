package p001a;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.afu  reason: case insensitive filesystem */
/* compiled from: a */
public final class C6074afu {
    public static final C6074afu fus = new C6074afu("GrannyAnyMarshalling", grannyJNI.GrannyAnyMarshalling_get());
    public static final C6074afu fut = new C6074afu("GrannyInt8Marshalling", grannyJNI.GrannyInt8Marshalling_get());
    public static final C6074afu fuu = new C6074afu("GrannyInt16Marshalling", grannyJNI.GrannyInt16Marshalling_get());
    public static final C6074afu fuv = new C6074afu("GrannyInt32Marshalling", grannyJNI.GrannyInt32Marshalling_get());
    public static final C6074afu fuw = new C6074afu("GrannyMarshallingMask", grannyJNI.GrannyMarshallingMask_get());
    private static C6074afu[] fux = {fus, fut, fuu, fuv, fuw};

    /* renamed from: pF */
    private static int f4481pF = 0;

    /* renamed from: pG */
    private final int f4482pG;

    /* renamed from: pH */
    private final String f4483pH;

    private C6074afu(String str) {
        this.f4483pH = str;
        int i = f4481pF;
        f4481pF = i + 1;
        this.f4482pG = i;
    }

    private C6074afu(String str, int i) {
        this.f4483pH = str;
        this.f4482pG = i;
        f4481pF = i + 1;
    }

    private C6074afu(String str, C6074afu afu) {
        this.f4483pH = str;
        this.f4482pG = afu.f4482pG;
        f4481pF = this.f4482pG + 1;
    }

    /* renamed from: qx */
    public static C6074afu m21656qx(int i) {
        if (i < fux.length && i >= 0 && fux[i].f4482pG == i) {
            return fux[i];
        }
        for (int i2 = 0; i2 < fux.length; i2++) {
            if (fux[i2].f4482pG == i) {
                return fux[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C6074afu.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f4482pG;
    }

    public String toString() {
        return this.f4483pH;
    }
}
