package p001a;

import logic.res.XmlNode;
import org.xmlpull.mxp1.MXParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Iterator;
import java.util.Stack;

/* renamed from: a.Vy */
/* compiled from: a */
public class C1492Vy {
    int[] aQl = new int[2];
    private MXParser esX;
    private int esY;
    private boolean esZ = true;

    public C1492Vy(InputStream inputStream, String str) {
        try {
            this.esX = new MXParser();
            this.esX.setInput(new InputStreamReader(inputStream, str));
        } catch (XmlPullParserException e) {
            throw new RuntimeException(e);
        }
    }

    public C1492Vy(Reader reader) {
        try {
            this.esX = new MXParser();
            this.esX.setInput(reader);
        } catch (XmlPullParserException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: a */
    private XmlNode m10948a(XmlPullParser xmlPullParser) {
        XmlNode agy = new XmlNode();
        Stack stack = new Stack();
        stack.push(agy);
        boolean z = false;
        while (!z) {
            switch (xmlPullParser.next()) {
                case 1:
                    z = true;
                    break;
                case 2:
                    stack.add(m10947a((XmlNode) stack.lastElement(), xmlPullParser));
                    break;
                case 3:
                    stack.pop();
                    if (stack.size() != 1) {
                        break;
                    } else {
                        Iterator<XmlNode> it = agy.getListChildrenTag().iterator();
                        if (!it.hasNext()) {
                            return null;
                        }
                        return it.next();
                    }
                case 4:
                    m10949b((XmlNode) stack.lastElement(), xmlPullParser);
                    break;
            }
        }
        Iterator<XmlNode> it2 = agy.getListChildrenTag().iterator();
        if (!it2.hasNext()) {
            return null;
        }
        return it2.next();
    }

    /* renamed from: a */
    private XmlNode m10947a(XmlNode agy, XmlPullParser xmlPullParser) {
        XmlNode mz = agy.addChildrenTag(this.esZ ? XmlNode.checkAZaz09deCode(xmlPullParser.getName()) : xmlPullParser.getName());
        for (int i = 0; i < xmlPullParser.getAttributeCount(); i++) {
            mz.addAttribute(xmlPullParser.getAttributeName(i), xmlPullParser.getAttributeValue(i));
        }
        return mz;
    }

    /* renamed from: b */
    private void m10949b(XmlNode agy, XmlPullParser xmlPullParser) {
        agy.setText(new String(xmlPullParser.getTextCharacters(this.aQl), this.aQl[0], this.aQl[1]));
    }

    public XmlNode bBf() {
        do {
            try {
                this.esY = this.esX.next();
                switch (this.esY) {
                    case 2:
                        XmlNode agy = new XmlNode(XmlNode.checkAZaz09deCode(this.esX.getName()));
                        for (int i = 0; i < this.esX.getAttributeCount(); i++) {
                            agy.addAttribute(this.esX.getAttributeName(i), this.esX.getAttributeValue(i));
                        }
                        return agy;
                }
            } catch (XmlPullParserException e) {
                throw new IOException("Problems parsing xml");
            }
        } while (this.esY != 2);
        return null;
    }

    public XmlNode bBg() {
        try {
            return m10948a(this.esX);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
            throw new CountFailedReadOrWriteException("Problems parsing xml", e);
        }
    }

    public boolean bBh() {
        return this.esZ;
    }

    /* renamed from: dB */
    public void mo6459dB(boolean z) {
        this.esZ = z;
    }
}
