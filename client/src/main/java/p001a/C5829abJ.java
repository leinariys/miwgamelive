package p001a;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.abJ  reason: case insensitive filesystem */
/* compiled from: a */
public @interface C5829abJ {
    String value();
}
