package p001a;

import logic.res.html.MessageContainer;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import org.mozilla1.classfile.C0147Bi;

import java.util.Map;

/* renamed from: a.aMz  reason: case insensitive filesystem */
/* compiled from: a */
public class C5533aMz implements C6473and {
    private static final Log logger = LogPrinter.m10275K(C5533aMz.class);
    aLV ipJ;
    private Map<Integer, C5395aHr> ipK = MessageContainer.newMap();
    private Map<Integer, aEI> ipL = MessageContainer.newMap();
    private Map<String, aEI> ipM = MessageContainer.newMap();

    public C5533aMz(aLV alv) {
        this.ipJ = alv;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0035, code lost:
        r3 = new byte[(r0 - 8)];
        r11.ipJ.mo7626a(r1, 0, r3, 0, r3.length);
        r1 = new java.io.ByteArrayInputStream(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r2 = new java.io.DataInputStream(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r0 = new p001a.C5395aHr(r2.readInt());
        r11.ipK.put(java.lang.Integer.valueOf(r0.hXb), r0);
        r0.hXc = r2.readInt();
        r2.readInt();
        r3 = r2.readInt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006c, code lost:
        if (0 >= r3) goto L_0x000e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006e, code lost:
        r4 = new p001a.aEI();
        r4.hHM = r8;
        r4.f2667id = r2.readInt();
        r4.hHL = r2.readUTF();
        r4.fileName = r2.readUTF();
        r4.hHN = r2.readInt();
        r0.entries.add(r4);
        r11.ipL.put(java.lang.Integer.valueOf(r4.f2667id), r4);
        r11.ipM.put(r4.fileName, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00a5, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00e4, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00e5, code lost:
        r2 = null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void init() {
        /*
            r11 = this;
            r6 = 0
            r10 = 4
            r9 = 0
            a.aLV r0 = r11.ipJ
            int r7 = r0.cIE()
            r1 = r7
        L_0x000a:
            if (r1 > r10) goto L_0x0018
            r1 = r6
            r2 = r6
        L_0x000e:
            r1.close()     // Catch:{ Exception -> 0x00da }
        L_0x0011:
            r2.close()     // Catch:{ Exception -> 0x00dd }
        L_0x0014:
            r0 = r7
        L_0x0015:
            if (r0 > r10) goto L_0x00ad
            return
        L_0x0018:
            a.aLV r0 = r11.ipJ     // Catch:{ all -> 0x00e0 }
            int r2 = r1 + -4
            int r0 = r0.getInt(r2)     // Catch:{ all -> 0x00e0 }
            a.aLV r2 = r11.ipJ     // Catch:{ all -> 0x00e0 }
            int r3 = r1 + -8
            int r8 = r2.getInt(r3)     // Catch:{ all -> 0x00e0 }
            int r1 = r1 - r0
            org.apache.commons.logging.Log r2 = logger     // Catch:{ all -> 0x00e0 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x00e0 }
            r2.debug(r3)     // Catch:{ all -> 0x00e0 }
            r2 = -1
            if (r8 != r2) goto L_0x000a
            int r0 = r0 + -8
            byte[] r3 = new byte[r0]     // Catch:{ all -> 0x00e0 }
            a.aLV r0 = r11.ipJ     // Catch:{ all -> 0x00e0 }
            r2 = 0
            r4 = 0
            int r5 = r3.length     // Catch:{ all -> 0x00e0 }
            r0.mo7626a(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x00e0 }
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ all -> 0x00e0 }
            r1.<init>(r3)     // Catch:{ all -> 0x00e0 }
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ all -> 0x00e4 }
            r2.<init>(r1)     // Catch:{ all -> 0x00e4 }
            a.aHr r0 = new a.aHr     // Catch:{ all -> 0x00a5 }
            int r3 = r2.readInt()     // Catch:{ all -> 0x00a5 }
            r0.<init>(r3)     // Catch:{ all -> 0x00a5 }
            java.util.Map<java.lang.Integer, a.aHr> r3 = r11.ipK     // Catch:{ all -> 0x00a5 }
            int r4 = r0.hXb     // Catch:{ all -> 0x00a5 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x00a5 }
            r3.put(r4, r0)     // Catch:{ all -> 0x00a5 }
            int r3 = r2.readInt()     // Catch:{ all -> 0x00a5 }
            r0.hXc = r3     // Catch:{ all -> 0x00a5 }
            r2.readInt()     // Catch:{ all -> 0x00a5 }
            int r3 = r2.readInt()     // Catch:{ all -> 0x00a5 }
        L_0x006c:
            if (r9 >= r3) goto L_0x000e
            a.aEI r4 = new a.aEI     // Catch:{ all -> 0x00a5 }
            r4.<init>()     // Catch:{ all -> 0x00a5 }
            r4.hHM = r8     // Catch:{ all -> 0x00a5 }
            int r5 = r2.readInt()     // Catch:{ all -> 0x00a5 }
            r4.f2667id = r5     // Catch:{ all -> 0x00a5 }
            java.lang.String r5 = r2.readUTF()     // Catch:{ all -> 0x00a5 }
            r4.hHL = r5     // Catch:{ all -> 0x00a5 }
            java.lang.String r5 = r2.readUTF()     // Catch:{ all -> 0x00a5 }
            r4.fileName = r5     // Catch:{ all -> 0x00a5 }
            int r5 = r2.readInt()     // Catch:{ all -> 0x00a5 }
            r4.hHN = r5     // Catch:{ all -> 0x00a5 }
            java.util.List<a.aEI> r5 = r0.entries     // Catch:{ all -> 0x00a5 }
            r5.add(r4)     // Catch:{ all -> 0x00a5 }
            java.util.Map<java.lang.Integer, a.aEI> r5 = r11.ipL     // Catch:{ all -> 0x00a5 }
            int r6 = r4.f2667id     // Catch:{ all -> 0x00a5 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x00a5 }
            r5.put(r6, r4)     // Catch:{ all -> 0x00a5 }
            java.util.Map<java.lang.String, a.aEI> r5 = r11.ipM     // Catch:{ all -> 0x00a5 }
            java.lang.String r6 = r4.fileName     // Catch:{ all -> 0x00a5 }
            r5.put(r6, r4)     // Catch:{ all -> 0x00a5 }
            goto L_0x006c
        L_0x00a5:
            r0 = move-exception
        L_0x00a6:
            r1.close()     // Catch:{ Exception -> 0x00d6 }
        L_0x00a9:
            r2.close()     // Catch:{ Exception -> 0x00d8 }
        L_0x00ac:
            throw r0
        L_0x00ad:
            a.aLV r1 = r11.ipJ
            int r2 = r0 + -4
            int r2 = r1.getInt(r2)
            a.aLV r1 = r11.ipJ
            int r3 = r0 + -8
            int r3 = r1.getInt(r3)
            int r1 = r0 - r2
            if (r3 <= 0) goto L_0x00d3
            java.util.Map<java.lang.Integer, a.aHr> r0 = r11.ipK
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            java.lang.Object r0 = r0.get(r3)
            a.aHr r0 = (p001a.C5395aHr) r0
            r0.hXc = r1
            int r2 = r2 + -8
            r0.size = r2
        L_0x00d3:
            r0 = r1
            goto L_0x0015
        L_0x00d6:
            r1 = move-exception
            goto L_0x00a9
        L_0x00d8:
            r1 = move-exception
            goto L_0x00ac
        L_0x00da:
            r0 = move-exception
            goto L_0x0011
        L_0x00dd:
            r0 = move-exception
            goto L_0x0014
        L_0x00e0:
            r0 = move-exception
            r1 = r6
            r2 = r6
            goto L_0x00a6
        L_0x00e4:
            r0 = move-exception
            r2 = r6
            goto L_0x00a6
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C5533aMz.init():void");
    }

    /* renamed from: jx */
    public byte[] mo10202jx(String str) {
        return mo10201jw(String.valueOf(str.replace('.', C0147Bi.cla)) + ".class");
    }

    /* renamed from: jw */
    public byte[] mo10201jw(String str) {
        return null;
    }

    /* renamed from: iS */
    public boolean mo10199iS(String str) {
        return false;
    }
}
