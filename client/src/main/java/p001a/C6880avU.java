package p001a;

import org.mozilla1.classfile.C0147Bi;
import taikodom.render.graphics2d.C0559Hm;
import taikodom.render.loader.provider.C0399FY;
import taikodom.render.loader.provider.FilePath;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.avU  reason: case insensitive filesystem */
/* compiled from: a */
public class C6880avU implements FilePath {

    /* renamed from: YO */
    static File f5438YO = null;
    public byte[] hNJ;
    /* renamed from: Ne */
    private long f5439Ne;
    /* renamed from: YI */
    private boolean f5440YI;
    /* renamed from: YL */
    private Map<String, C6880avU> f5441YL;
    private C2001b hNH;
    private C6880avU hNI;
    private String name;

    public C6880avU() {
        this.hNH = null;
        this.hNI = null;
        this.f5441YL = new HashMap();
        this.hNJ = new byte[0];
        this.hNH = C2001b.Dir;
        touch();
    }

    public C6880avU(FilePath ain, String str) {
        this.hNH = null;
        this.hNI = null;
        this.f5441YL = new HashMap();
        this.hNJ = new byte[0];
        this.hNI = (C6880avU) ain;
        this.name = str;
    }

    public boolean exists() {
        return this.f5440YI;
    }

    /* renamed from: aC */
    public FilePath concat(String str) {
        int indexOf = str.indexOf(47);
        if (indexOf == -1) {
            return m26589mv(str);
        }
        return m26589mv(str.substring(0, indexOf)).concat(str.substring(indexOf + 1));
    }

    /* renamed from: mv */
    private C6880avU m26589mv(String str) {
        C6880avU avu = this.f5441YL.get(str);
        if (avu != null) {
            return avu;
        }
        C6880avU avu2 = new C6880avU(this, str);
        this.f5441YL.put(str, avu2);
        return avu2;
    }

    /* renamed from: BB */
    public FilePath mo2249BB() {
        return this.hNI;
    }

    /* renamed from: BC */
    public FilePath[] mo2250BC() {
        FilePath[] ainArr = new FilePath[this.f5441YL.size()];
        this.f5441YL.values().toArray(ainArr);
        return ainArr;
    }

    /* renamed from: a */
    public FilePath[] mo2258a(C0399FY fy) {
        ArrayList arrayList = new ArrayList();
        for (FilePath next : this.f5441YL.values()) {
            if (fy.mo2163a(this, next.getName())) {
                arrayList.add(next);
            }
        }
        return (FilePath[]) arrayList.toArray(new FilePath[arrayList.size()]);
    }

    /* renamed from: BD */
    public void mo2251BD() {
        if (this.hNI != null) {
            if (!this.hNI.exists()) {
                this.hNI.mo2251BD();
            }
            touch();
            this.hNH = C2001b.Dir;
        }
    }

    /* access modifiers changed from: private */
    public void touch() {
        this.f5439Ne = System.currentTimeMillis();
        this.f5440YI = true;
    }

    public String getName() {
        return this.name;
    }

    public InputStream openInputStream() throws FileNotFoundException {
        if (exists()) {
            return new ByteArrayInputStream(this.hNJ);
        }
        throw new FileNotFoundException(getName());
    }

    /* renamed from: BE */
    public PrintWriter mo2252BE() {
        return new PrintWriter(openOutputStream());
    }

    public boolean isDir() {
        return this.hNH == C2001b.Dir;
    }

    public OutputStream openOutputStream() {
        mo2249BB().mo2251BD();
        touch();
        this.hNH = C2001b.File;
        this.hNJ = new byte[0];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        return new C2000a(byteArrayOutputStream, byteArrayOutputStream);
    }

    public boolean delete() {
        if (!this.f5440YI) {
            return false;
        }
        this.f5440YI = false;
        if (this.hNI != null) {
            return this.hNI.m26587a(this);
        }
        return true;
    }

    /* renamed from: a */
    private boolean m26587a(C6880avU avu) {
        return this.f5441YL.remove(avu.name) != null;
    }

    public String getPath() {
        String str = this.name;
        for (C6880avU avu = this.hNI; avu != null; avu = avu.hNI) {
            str = String.valueOf(avu.name) + C0147Bi.SEPARATOR + str;
        }
        return str;
    }

    /* renamed from: d */
    public boolean mo2261d(FilePath ain) {
        if (!this.f5440YI) {
            return false;
        }
        C6880avU avu = (C6880avU) ain;
        avu.hNJ = this.hNJ;
        avu.f5441YL = this.f5441YL;
        avu.touch();
        delete();
        return true;
    }

    /* renamed from: BF */
    public FilePath mo2253BF() {
        return this;
    }

    public long lastModified() {
        return this.f5439Ne;
    }

    /* renamed from: BG */
    public File getFile() {
        if (f5438YO == null) {
            try {
                File createTempFile = File.createTempFile("memoryFile", "dir");
                f5438YO = createTempFile.getParentFile();
                createTempFile.delete();
                f5438YO = new File(f5438YO, createTempFile.getName());
                f5438YO.mkdirs();
                f5438YO.deleteOnExit();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        File file = new File(f5438YO, getName());
        C0559Hm.m5248a(file, this.hNJ);
        file.deleteOnExit();
        System.out.println("MemoryFile: creating temp file: " + file.getAbsolutePath());
        return file;
    }

    /* renamed from: BH */
    public byte[] mo2255BH() {
        return this.hNJ;
    }

    /* renamed from: d */
    public void mo2260d(byte[] bArr) {
        this.hNJ = bArr;
    }

    /* renamed from: BI */
    public ByteBuffer mo2256BI() {
        return ByteBuffer.wrap(mo2255BH());
    }

    public long length() {
        return (long) (this.hNJ == null ? 0 : this.hNJ.length);
    }

    /* renamed from: a.avU$b */
    /* compiled from: a */
    enum C2001b {
        File,
        Dir
    }

    /* renamed from: a.avU$a */
    class C2000a extends BufferedOutputStream {
        private final /* synthetic */ ByteArrayOutputStream gLq;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C2000a(OutputStream outputStream, ByteArrayOutputStream byteArrayOutputStream) {
            super(outputStream);
            this.gLq = byteArrayOutputStream;
        }

        public void flush() {
            super.flush();
            C6880avU.this.touch();
            C6880avU.this.hNJ = this.gLq.toByteArray();
        }
    }
}
