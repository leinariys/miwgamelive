package p001a;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.C6853aut;
import game.script.ship.Ship;
import game.script.ship.categories.BattleCruiserType;
import game.script.ship.categories.BomberType;
import game.script.ship.categories.ExplorerType;
import game.script.ship.categories.FreighterType;
import logic.res.sound.C0907NJ;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.RParticleSystem;
import taikodom.render.scene.SSoundSource;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/* renamed from: a.w */
/* compiled from: a */
public class C3869w {
    /* access modifiers changed from: private */

    /* renamed from: br */
    private final int f9449br = 5;
    /* access modifiers changed from: private */
    /* renamed from: bA */
    public SSoundSource f9441bA;
    /* renamed from: bB */
    public SSoundSource f9442bB;
    /* renamed from: bE */
    public boolean f9445bE;
    /* access modifiers changed from: private */
    /* renamed from: bF */
    public float f9446bF;
    /* access modifiers changed from: private */
    /* renamed from: bH */
    public Scene f9448bH = new Scene();
    /* renamed from: bs */
    public boolean f9450bs;
    /* access modifiers changed from: private */
    /* renamed from: bz */
    public SceneObject f9457bz;
    /* renamed from: bt */
    List<C6853aut> f9451bt;
    /* access modifiers changed from: private */
    /* renamed from: bu */
    List<C3874e> f9452bu;
    /* renamed from: bv */
    float f9453bv = 1.0f;
    /* renamed from: bw */
    double f9454bw = 10.0d;
    /* renamed from: bx */
    boolean f9455bx;
    Random random = new Random();
    float scale;
    /* renamed from: bC */
    private String[] f9443bC = {"bleed_gas1_sfx", "bleed_gas2_sfx", "bleed_gas3_sfx"};
    /* access modifiers changed from: private */
    /* renamed from: bD */
    private String f9444bD = "bleed_fire_sfx";
    /* renamed from: bG */
    private float f9447bG;
    /* renamed from: by */
    private Ship f9456by;

    public C3869w(SceneObject sceneObject, Ship fAVar) {
        this.f9456by = fAVar;
        this.f9457bz = sceneObject;
        this.f9451bt = fAVar.agH().mo21118VN();
        m40446X();
        this.f9445bE = false;
        this.f9450bs = false;
        this.f9446bF = 0.0f;
        this.f9452bu = new ArrayList();
        sceneObject.computeWorldSpaceAABB();
        this.f9447bG = sceneObject.getAABB().length();
    }

    public C3869w(SceneObject sceneObject, C3869w wVar) {
        this.f9456by = wVar.f9456by;
        this.f9457bz = sceneObject;
        this.f9451bt = this.f9456by.agH().mo21118VN();
        this.f9452bu = new ArrayList();
        for (C3874e next : wVar.mo22695aj()) {
            C3874e eVar = new C3874e(this, (C3874e) null);
            if (next.gbU != null) {
                eVar.gbU = (RParticleSystem) next.gbU.cloneAsset();
            }
            if (next.gbV != null) {
                eVar.gbV = (RParticleSystem) next.gbV.cloneAsset();
            }
            eVar.f9458Oj = next.f9458Oj;
            this.f9452bu.add(eVar);
        }
        this.f9445bE = false;
        this.f9450bs = false;
        this.f9446bF = 0.0f;
        sceneObject.computeWorldSpaceAABB();
        this.f9447bG = sceneObject.getAABB().length();
    }

    /* renamed from: X */
    private void m40446X() {
        C5916acs.getSingolton().getLoaderTrail().mo4995a("data/gfx/misc/bleed_ship.pro", "smoke_bleeding", (Scene) null, new C3870a(), "Ship bleed");
    }

    /* access modifiers changed from: private */
    /* renamed from: Y */
    public void m40447Y() {
        C5916acs.getSingolton().getLoaderTrail().mo4995a("data/gfx/misc/bleed_ship.pro", "fire_bleeding", (Scene) null, new C3873d(), "Ship bleed");
    }

    /* renamed from: a */
    private void m40452a(String str, SceneObject sceneObject) {
        if (this.f9441bA == null) {
            C5916acs.getSingolton().getLoaderTrail().mo4995a("data/audio/fx/bleed/bleed_sfx.pro", str, (Scene) null, new C3872c(sceneObject), "Ship gas sfx");
        }
    }

    /* renamed from: a */
    private void m40453a(SceneObject sceneObject) {
        if (this.f9442bB == null) {
            C5916acs.getSingolton().getLoaderTrail().mo4995a("data/audio/fx/bleed/bleed_sfx.pro", this.f9444bD, (Scene) null, new C3871b(sceneObject), "Ship gas sfx");
        }
    }

    /* renamed from: b */
    public void mo22698b(SceneObject sceneObject) {
        if (this.f9441bA != null) {
            C5916acs.getSingolton().getEngineGraphics().adZ().removeChild(this.f9441bA);
            this.f9441bA.stop();
            this.f9441bA.dispose();
            this.f9441bA = null;
        }
    }

    /* renamed from: c */
    public void mo22699c(SceneObject sceneObject) {
        if (this.f9442bB != null) {
            C5916acs.getSingolton().getEngineGraphics().adZ().removeChild(this.f9442bB);
            this.f9442bB.stop();
            this.f9442bB.dispose();
            this.f9442bB = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: Z */
    public int m40448Z() {
        if (this.f9456by.agH() instanceof BomberType) {
            return 2;
        }
        if (this.f9456by.agH() instanceof ExplorerType) {
            return 3;
        }
        if (this.f9456by.agH() instanceof FreighterType) {
            return 10;
        }
        if (this.f9456by.agH() instanceof BattleCruiserType) {
            return 15;
        }
        return 1;
    }

    /* renamed from: aa */
    public float mo22686aa() {
        return ((((float) this.random.nextInt(100)) / 100.0f) * 2.0f) - 1.0f;
    }

    /* renamed from: ab */
    public float mo22687ab() {
        return ((float) this.random.nextInt(100)) / 100.0f;
    }

    /* renamed from: ac */
    public Vec3f mo22688ac() {
        return this.f9451bt.remove(this.random.nextInt(this.f9451bt.size() - 1)).mo16402gL();
    }

    /* renamed from: h */
    public void mo22702h(float f) {
        this.f9446bF = ((4.0f * f) - 2.0f) / 2.0f;
        float exp = (float) Math.exp((double) this.f9446bF);
        float pow = (float) Math.pow(1.5d, (double) exp);
        float f2 = this.f9446bF * this.f9453bv;
        float pow2 = (float) Math.pow(2.1d, (double) exp);
        for (C3874e o : this.f9452bu) {
            o.mo22710o(30.0f, pow, f2, pow2);
        }
        if (this.f9441bA != null) {
            this.f9441bA.setPitch(f);
        }
    }

    /* renamed from: ad */
    public void mo22689ad() {
        if (this.f9457bz != null) {
            for (C3874e ckc : this.f9452bu) {
                ckc.ckc();
            }
        }
        this.f9445bE = true;
    }

    /* renamed from: ae */
    public void mo22690ae() {
        for (C3874e ckd : this.f9452bu) {
            ckd.ckd();
        }
        this.f9445bE = false;
    }

    /* renamed from: af */
    public void mo22691af() {
        if (this.f9457bz != null) {
            for (C3874e af : this.f9452bu) {
                af.mo22704af();
            }
        }
        this.f9450bs = true;
    }

    /* renamed from: ag */
    public void mo22692ag() {
        for (C3874e ag : this.f9452bu) {
            ag.mo22705ag();
        }
        this.f9450bs = false;
    }

    /* renamed from: ah */
    public boolean mo22693ah() {
        return !this.f9445bE;
    }

    /* renamed from: ai */
    public boolean mo22694ai() {
        return !this.f9450bs;
    }

    public void dispose() {
        for (C3874e next : this.f9452bu) {
            if (this.f9450bs) {
                next.mo22705ag();
            }
            if (this.f9445bE) {
                next.ckd();
            }
            next.dispose();
        }
    }

    /* renamed from: d */
    public void mo22700d(SceneObject sceneObject) {
        this.f9457bz = sceneObject;
    }

    /* renamed from: aj */
    public List<C3874e> mo22695aj() {
        return this.f9452bu;
    }

    /* renamed from: ak */
    public SceneObject mo22696ak() {
        return this.f9457bz;
    }

    /* renamed from: al */
    public Ship mo22697al() {
        return this.f9456by;
    }

    /* renamed from: a.w$a */
    class C3870a implements C0907NJ {
        C3870a() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if (renderAsset != null) {
                for (int i = 0; i < C3869w.this.m40448Z(); i++) {
                    C3874e eVar = new C3874e(C3869w.this, (C3874e) null);
                    eVar.mo22703a((RParticleSystem) renderAsset.cloneAsset());
                    C3869w.this.f9452bu.add(eVar);
                }
                C3869w.this.f9453bv = C3869w.this.f9452bu.get(0).gbU.getParticlesMaxLifeTime();
                C3869w.this.f9454bw = C3869w.this.f9452bu.get(0).gbU.getParticlesVelocity().length();
                C3869w.this.m40447Y();
            }
        }
    }

    /* renamed from: a.w$d */
    /* compiled from: a */
    class C3873d implements C0907NJ {
        C3873d() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if (renderAsset != null) {
                for (C3874e b : C3869w.this.f9452bu) {
                    b.mo22706b((RParticleSystem) renderAsset.cloneAsset());
                }
            }
            C3869w.this.mo22702h(C3869w.this.f9446bF);
            if (C3869w.this.f9445bE) {
                for (C3874e ckc : C3869w.this.f9452bu) {
                    ckc.ckc();
                }
            }
            if (C3869w.this.f9450bs) {
                for (C3874e af : C3869w.this.f9452bu) {
                    af.mo22704af();
                }
            }
        }
    }

    /* renamed from: a.w$c */
    /* compiled from: a */
    class C3872c implements C0907NJ {
        private final /* synthetic */ SceneObject fWW;

        C3872c(SceneObject sceneObject) {
            this.fWW = sceneObject;
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if (renderAsset != null) {
                C3869w.this.f9441bA = (SSoundSource) renderAsset;
                C3869w.this.f9441bA.setLoopCount(0);
                C3869w.this.f9441bA.play();
                C3869w.this.f9441bA.setSquaredSoundSource(this.fWW);
                C5916acs.getSingolton().getEngineGraphics().adZ().addChild(C3869w.this.f9441bA);
            }
        }
    }

    /* renamed from: a.w$b */
    /* compiled from: a */
    class C3871b implements C0907NJ {
        private final /* synthetic */ SceneObject fWW;

        C3871b(SceneObject sceneObject) {
            this.fWW = sceneObject;
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if (renderAsset != null) {
                C3869w.this.f9442bB = (SSoundSource) renderAsset;
                C3869w.this.f9442bB.setLoopCount(0);
                C3869w.this.f9442bB.play();
                C3869w.this.f9442bB.setSquaredSoundSource(this.fWW);
                C5916acs.getSingolton().getEngineGraphics().adZ().addChild(C3869w.this.f9442bB);
            }
        }
    }

    /* renamed from: a.w$e */
    /* compiled from: a */
    private class C3874e {

        /* renamed from: Oj */
        public Vec3f f9458Oj;
        public RParticleSystem gbU;
        public RParticleSystem gbV;
        private Vec3f gbW;

        private C3874e() {
            this.f9458Oj = new Vec3f(C3869w.this.mo22686aa(), C3869w.this.mo22686aa(), C3869w.this.mo22686aa());
        }

        /* synthetic */ C3874e(C3869w wVar, C3874e eVar) {
            this();
        }

        /* renamed from: a */
        public void mo22703a(RParticleSystem rParticleSystem) {
        }

        /* renamed from: b */
        public void mo22706b(RParticleSystem rParticleSystem) {
        }

        public void dispose() {
            if (this.gbV != null) {
                this.gbV.dispose();
            }
            if (this.gbU != null) {
                this.gbU.dispose();
            }
            if (C3869w.this.f9448bH != null) {
                C3869w.this.f9448bH = null;
            }
        }

        /* renamed from: af */
        public void mo22704af() {
            if (C3869w.this.f9457bz != null && !C3869w.this.f9457bz.isDisposed() && this.gbV != null) {
                C3869w.this.f9457bz.addChild(this.gbV);
            }
        }

        public void ckc() {
            if (C3869w.this.f9457bz != null && !C3869w.this.f9457bz.isDisposed() && this.gbU != null) {
                C3869w.this.f9457bz.addChild(this.gbU);
            }
        }

        /* renamed from: ag */
        public void mo22705ag() {
            if (C3869w.this.f9457bz != null && !C3869w.this.f9457bz.isDisposed()) {
                C3869w.this.f9457bz.removeChild(this.gbV);
            }
            Scene adZ = C5916acs.getSingolton().getEngineGraphics().adZ();
            if (adZ.hasChild(this.gbV)) {
                adZ.removeChild(this.gbV);
            }
        }

        public void ckd() {
            if (C3869w.this.f9457bz != null && !C3869w.this.f9457bz.isDisposed()) {
                C3869w.this.f9457bz.removeChild(this.gbU);
            }
            Scene adZ = C5916acs.getSingolton().getEngineGraphics().adZ();
            if (adZ.hasChild(this.gbU)) {
                adZ.removeChild(this.gbU);
            }
        }

        /* renamed from: o */
        public void mo22710o(float f, float f2, float f3, float f4) {
            if (this.gbU != null) {
                this.gbU.getParticlesVelocity().mo9497bG(this.f9458Oj);
                this.gbU.getParticlesVelocity().scale(C3869w.this.f9454bw);
                this.gbU.setParticlesMaxLifeTime(f3);
            }
        }
    }
}
