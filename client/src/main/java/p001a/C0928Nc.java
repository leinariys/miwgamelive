package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.Nc */
/* compiled from: a */
public class C0928Nc {
    /* renamed from: aa */
    public static String m7696aa(Vec3f vec3f) {
        if (vec3f == null) {
            return null;
        }
        return String.valueOf(String.valueOf(vec3f.x)) + ";" + String.valueOf(vec3f.y) + ";" + String.valueOf(vec3f.z);
    }

    /* renamed from: eP */
    public static Vec3f m7697eP(String str) {
        int indexOf;
        int indexOf2;
        if (str == null || "null".equals(str) || "".equals(str) || (indexOf = str.indexOf(";")) == -1 || (indexOf2 = str.indexOf(";", indexOf + 1)) == -1) {
            return null;
        }
        return new Vec3f(Float.parseFloat(str.substring(0, indexOf)), Float.parseFloat(str.substring(indexOf + 1, indexOf2)), Float.parseFloat(str.substring(indexOf2 + 1)));
    }
}
