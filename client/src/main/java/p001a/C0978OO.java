package p001a;

import logic.res.scene.C2036bO;
import logic.res.scene.C5821abB;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/* renamed from: a.OO */
/* compiled from: a */
public class C0978OO implements C3915wj {
    private static final Log logger = LogFactory.getLog(C0978OO.class);
    private final String dOJ;
    private final C2036bO dOK;
    private final C5821abB dOL;
    private final String targetName;

    public C0978OO(String str, String str2, C2036bO bOVar, C5821abB abb) {
        this.dOJ = str;
        this.targetName = str2;
        this.dOK = bOVar;
        this.dOL = abb;
    }

    public void execute() {
        C1693Yu n = new aUD(this.dOJ).mo7458j(this.dOK);
        C2036bO bHA = n.bHA();
        if (bHA == null) {
            C5226aBe abe = new C5226aBe("Source object not found: " + this.dOJ);
            logger.error(abe);
            throw abe;
        }
        C2036bO bHB = n.bHB();
        if (bHB == null) {
            C5226aBe abe2 = new C5226aBe("Source object '" + this.dOJ + "' must have a parent object");
            logger.error(abe2);
            throw abe2;
        }
        C2036bO bOVar = (C2036bO) this.dOL.mo12393ih(this.targetName);
        if (bOVar == null) {
            C5226aBe abe3 = new C5226aBe("Target object not found: " + this.targetName);
            logger.error(abe3);
            throw abe3;
        }
        bHB.mo13081c(bHA);
        bHB.mo13079a(bOVar);
    }
}
