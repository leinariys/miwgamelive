package p001a;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.*;
import game.network.message.serializable.C6400amI;
import logic.bbb.C4029yK;
import logic.thred.C6615aqP;
import logic.thred.LogPrinter;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: a.Gx */
/* compiled from: a */
public abstract class C0501Gx implements aKR, C6243ajH, C6615aqP, C2278dY {
    public static final int DEFAULT = 0;
    public static final int STATIC = 1;
    public static final int cYT = 3;
    public static final int cYU = 2;
    public static final int cYV = 3;
    public static final int cYW = 4;
    static final AtomicLong cZh = new AtomicLong();
    private static final LogPrinter logger = LogPrinter.setClass(C0501Gx.class);
    private final Matrix3fWrap cZf;
    public C6400amI cYX;
    public aNW cYY;
    private aLH aabb;
    private C4029yK cYZ;
    private boolean cZa;
    private C3735uM cZb;
    private Quat4fWrap cZc;
    private float cZd;
    private float cZe;
    private float cZg;
    private boolean enabled;

    /* renamed from: id */
    private long f646id;
    private Vec3d lastPosition;
    private float mass;
    private transient C0763Kt stack;

    public C0501Gx() {
        this.enabled = true;
        this.cZa = false;
        this.lastPosition = new Vec3d();
        this.cZc = new Quat4fWrap();
        this.cZd = 0.0f;
        this.cZe = 1.0f;
        this.cZf = new Matrix3fWrap();
        this.cZg = 1.0f;
        this.stack = C0763Kt.bcE();
        aRa().setIdentity();
    }

    public C0501Gx(C4029yK yKVar) {
        this();
        this.f646id = cZh.incrementAndGet();
        this.cYX = new C6400amI();
        mo2463b(yKVar);
    }

    /* renamed from: b */
    public abstract void mo2462b(int i, long j, Collection<C6625aqZ> collection, C6705asB asb);

    public Vec3d getPosition() {
        return this.cYX.blk();
    }

    public C3735uM aQX() {
        return this.cZb;
    }

    public Vec3d aQY() {
        return this.lastPosition;
    }

    public void step(float f) {
        if (!aQZ() && f > 0.0f) {
            this.lastPosition.mo9484aA(this.cYX.blk());
            this.cZc.mo15242f(this.cYX.bll());
            mo2448a(f, this.cYX);
            if (!this.lastPosition.equals(this.cYX.blk()) || !this.cZc.equals(this.cYX.bll())) {
                this.cZd = 0.0f;
            } else {
                this.cZd += f;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo2448a(float f, C6400amI ami) {
        if (this.enabled && !this.cZa) {
            mo2461b(f, ami);
            ami.mo14818b(f, ami);
            this.aabb = null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo2461b(float f, C6400amI ami) {
        if (this.cZb != null) {
            this.cZb.mo3859a(f, ami);
        }
    }

    /* renamed from: N */
    public void mo2442N(Vec3f vec3f) {
        wake();
        this.cYX.blk().x += (double) vec3f.x;
        this.cYX.blk().y += (double) vec3f.y;
        this.cYX.blk().z += (double) vec3f.z;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean z) {
        wake();
        this.enabled = z;
    }

    public boolean isStatic() {
        return this.cZa;
    }

    public void setStatic(boolean z) {
        wake();
        this.cZa = z;
    }

    /* renamed from: IL */
    public C4029yK mo2437IL() {
        return this.cYZ;
    }

    /* renamed from: b */
    public void mo2463b(C4029yK yKVar) {
        wake();
        this.cYZ = yKVar;
        this.aabb = null;
        this.mass = Math.max(1.0f, yKVar.getVolume());
        this.cZe = 1.0f / this.mass;
        this.cZf.setIdentity();
        this.cZf.mul(this.cZe);
    }

    public aLH getAABB() {
        if (this.aabb != null) {
            return this.aabb;
        }
        this.aabb = m3536A(this.cYX.blk());
        return this.aabb;
    }

    /* renamed from: A */
    private aLH m3536A(Vec3d ajr) {
        BoundingBox boundingBox = mo2437IL().getBoundingBox();
        aLH alh = new aLH();
        alh.addPoint(boundingBox.dqQ());
        alh.addPoint(boundingBox.dqP());
        alh.mo9832a(getOrientation().cow(), alh);
        alh.mo9844aI(ajr);
        return alh;
    }

    public Quat4fWrap getOrientation() {
        return mo2472kQ().bll();
    }

    public String toString() {
        return "Solid " + this.f646id + ":" + " P=" + getPosition() + " " + mo2437IL() + " LV=" + mo2472kQ().blm();
    }

    /* renamed from: kQ */
    public C1003Om mo2472kQ() {
        return this.cYX;
    }

    /* renamed from: a */
    public void mo2450a(C3735uM uMVar) {
        this.cZb = uMVar;
    }

    /* renamed from: IM */
    public byte mo2438IM() {
        return 0;
    }

    public boolean aQZ() {
        return this.cZd > 3.0f;
    }

    public void wake() {
        this.cZd = 0.0f;
    }

    /* renamed from: O */
    public void mo2443O(Vec3f vec3f) {
        mo2472kQ().mo4491ab(vec3f);
    }

    public Matrix3fWrap aRa() {
        return this.cZf;
    }

    /* renamed from: fo */
    public void mo2465fo(float f) {
        this.cZe = f;
    }

    public float aRb() {
        return this.cZe;
    }

    /* renamed from: IQ */
    public TransformWrap mo2439IQ() {
        return mo2472kQ().mo4490IQ();
    }

    /* renamed from: IR */
    public void mo2440IR() {
        wake();
    }

    /* renamed from: P */
    public void mo2444P(Vec3f vec3f) {
        Vec3f aRc = aRc();
        aRc.scaleAdd(this.cZe, vec3f, aRc);
    }

    private Vec3f aRc() {
        return mo2472kQ().blq();
    }

    /* renamed from: m */
    public void mo2473m(Vec3f vec3f, Vec3f vec3f2) {
        C0763Kt bcE = C0763Kt.bcE();
        bcE.bcH().push();
        try {
            if (this.cZe != 0.0f) {
                mo2444P(vec3f);
                if (this.cZg != 0.0f) {
                    Vec3f vec3f3 = (Vec3f) bcE.bcH().get();
                    vec3f3.cross(vec3f2, vec3f);
                    vec3f3.scale(this.cZg);
                    mo2445Q(vec3f3);
                }
            }
        } finally {
            bcE.bcH().pop();
        }
    }

    /* renamed from: Q */
    public void mo2445Q(Vec3f vec3f) {
        C0763Kt bcE = C0763Kt.bcE();
        bcE.bcH().push();
        try {
            Vec3f ac = bcE.bcH().mo4458ac(vec3f);
            this.cZf.transform(ac);
            aRd().add(ac);
        } finally {
            bcE.bcH().pop();
        }
    }

    public Vec3f aRd() {
        return mo2472kQ().blt();
    }

    /* renamed from: n */
    public void mo2474n(Vec3f vec3f, Vec3f vec3f2) {
        vec3f2.cross(mo2472kQ().bln(), vec3f);
        vec3f2.add(aRc());
    }

    /* renamed from: R */
    public Vec3f mo2446R(Vec3f vec3f) {
        C0763Kt bcE = C0763Kt.bcE();
        bcE.bcH().push();
        try {
            Vec3f vec3f2 = (Vec3f) bcE.bcH().get();
            if (!isStatic()) {
                mo2474n(vec3f, vec3f2);
            } else {
                vec3f2.set(0.0f, 0.0f, 0.0f);
            }
            return (Vec3f) bcE.bcH().mo15197aq(vec3f2);
        } finally {
            bcE.bcH().pop();
        }
    }

    public Vec3d aRe() {
        return mo2439IQ().position;
    }

    public float aRf() {
        return this.cZe;
    }

    /* renamed from: b */
    public void mo2464b(Vec3f vec3f, Vec3f vec3f2, float f) {
        if (this.cZe != 0.0f) {
            aRc().scaleAdd(f, vec3f, aRc());
            if (this.cZg != 0.0f) {
                aRd().scaleAdd(this.cZg * f, vec3f2, aRd());
            }
        }
    }

    public float aRg() {
        return this.cZg * -100.0f;
    }

    /* renamed from: a */
    public void mo2449a(aNW anw) {
        this.cYY = anw;
    }

    public aNW aRh() {
        return this.cYY;
    }

    /* renamed from: a */
    public float mo2447a(C0501Gx gx) {
        return gx.cYY != null ? this.cYY.mo3831b(gx.cYY) : this.cYY.mo3829F(gx.getPosition());
    }

    /* renamed from: IS */
    public Object mo2441IS() {
        return Long.valueOf(this.f646id);
    }

    public boolean isReady() {
        return true;
    }
}
