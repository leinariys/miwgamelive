package p001a;

import game.network.message.C0474GZ;
import game.network.message.externalizable.C0465GR;
import game.network.message.externalizable.C6950awp;
import game.script.Actor;
import logic.res.code.C1625Xn;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.yM */
/* compiled from: a */
public class C4031yM implements C6950awp {
    private C0465GR bMv;
    private long buF;
    private int kind;

    /* renamed from: pI */
    private Actor f9582pI;

    public C4031yM() {
    }

    public C4031yM(int i, Actor cr, C1003Om om, long j) {
        int i2;
        Externalizable j2;
        this.kind = i;
        this.f9582pI = cr;
        this.buF = j;
        C3735uM aQX = cr.cLd().aQX();
        if (i != 0) {
            i2 = 23;
        } else {
            i2 = 20;
        }
        if (aQX == null) {
            j2 = null;
        } else {
            j2 = aQX.mo3862j(i);
        }
        this.bMv = new C0465GR(i2, om, j2);
    }

    /* renamed from: a */
    public void mo12575a(C1355Tk tk) {
        C1298TD td;
        if (this.f9582pI != null && (td = (C1298TD) this.f9582pI.bFf()) != null) {
            if (td.bFT()) {
                td.mo5609ds();
            }
            C0520HN cLd = this.f9582pI.cLd();
            if (cLd == null) {
                if (!this.f9582pI.isDisposed() && this.f9582pI.bae()) {
                    this.f9582pI.mo965Zc().mo1893b(this.buF, this.f9582pI);
                    cLd = this.f9582pI.cLd();
                    if (cLd == null) {
                        return;
                    }
                } else {
                    return;
                }
            }
            if (!this.f9582pI.bGX() || this.f9582pI != this.f9582pI.ala().aLW().ald().mo4089dL().bhE()) {
                cLd.mo2542a(this.bMv, this.buF);
                return;
            }
            C1625Xn bGK = this.f9582pI.bFf().mo6866PM().bGK();
            if (bGK != null && bGK.brL()) {
                try {
                    Thread.yield();
                    bGK.brK();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void readExternal(ObjectInput objectInput) {
        this.f9582pI = (Actor) C0474GZ.dah.inputReadObject(objectInput);
        this.bMv = new C0465GR();
        this.bMv.readExternal(objectInput);
        this.buF = objectInput.readLong();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        C0474GZ.dah.serializeData(objectOutput, (Object) this.f9582pI);
        this.bMv.writeExternal(objectOutput);
        objectOutput.writeLong(this.buF);
    }

    public String toString() {
        return "ActorPacketData[actor=" + this.f9582pI + ", state=" + this.bMv + "]";
    }

    /* renamed from: ha */
    public Actor mo23123ha() {
        return this.f9582pI;
    }
}
