package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;

/* renamed from: a.Om */
/* compiled from: a */
public interface C1003Om {
    /* renamed from: IQ */
    TransformWrap mo4490IQ();

    /* renamed from: ab */
    void mo4491ab(Vec3f vec3f);

    boolean anA();

    Vec3d blk();

    Quat4fWrap bll();

    Vec3f blm();

    Vec3f bln();

    Vec3f blo();

    Vec3f blp();

    Vec3f blq();

    Vec3f blr();

    Vec3f bls();

    Vec3f blt();
}
