package p001a;

import java.text.MessageFormat;

/* renamed from: a.adg  reason: case insensitive filesystem */
/* compiled from: a */
public class C5956adg {
    public static String format(String str, Object... objArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (char c : str.toCharArray()) {
            stringBuffer.append(c);
            if (c == '\'') {
                stringBuffer.append('\'');
            }
        }
        return MessageFormat.format(stringBuffer.toString(), objArr);
    }
}
