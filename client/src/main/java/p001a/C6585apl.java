package p001a;

import java.util.ArrayList;

/* renamed from: a.apl  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C6585apl<T> extends ArrayList<T> {
    /* access modifiers changed from: protected */
    /* renamed from: qY */
    public abstract T mo15490qY();

    /* renamed from: tu */
    public T mo15491tu(int i) {
        return super.get(i);
    }

    public T get(int i) {
        while (size() <= i) {
            add((Object) null);
        }
        T t = super.get(i);
        if (t != null) {
            return t;
        }
        T qY = mo15490qY();
        set(i, qY);
        return qY;
    }
}
