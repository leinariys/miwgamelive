package p001a;

import game.script.ship.categories.Fighter;
import game.script.simulation.Space;
import logic.bbb.C4029yK;

/* renamed from: a.hh */
/* compiled from: a */
public class C2623hh extends C6901avp {

    /* renamed from: TL */
    private boolean f8024TL = false;

    public C2623hh(Space ea, Fighter pFVar, C4029yK yKVar) {
        super(ea, pFVar, yKVar);
    }

    /* renamed from: xY */
    private Fighter m32910xY() {
        return (Fighter) mo2568ha();
    }

    /* renamed from: xZ */
    public boolean mo19336xZ() {
        return this.f8024TL;
    }

    /* renamed from: R */
    public void mo19335R(boolean z) {
        if (z && !this.f8024TL) {
            m32910xY().biJ().dfa();
        } else if (!z && this.f8024TL) {
            m32910xY().biJ().dfc();
        }
        this.f8024TL = z;
    }

    /* renamed from: as */
    public void mo2560as(float f) {
        if (this.f8024TL) {
            f = m32910xY().biJ().mo17893mM(f);
        }
        super.mo2560as(f);
    }

    /* renamed from: at */
    public void mo2561at(float f) {
        if (this.f8024TL) {
            f = m32910xY().biJ().mo17892mK(f);
        }
        super.mo2561at(f);
    }

    /* renamed from: au */
    public void mo2562au(float f) {
        if (this.f8024TL) {
            f = m32910xY().biJ().mo17894mO(f);
        }
        super.mo2562au(f);
    }

    /* renamed from: av */
    public void mo2563av(float f) {
        if (this.f8024TL) {
            f = m32910xY().biJ().mo17895mQ(f);
        }
        super.mo2563av(f);
    }
}
