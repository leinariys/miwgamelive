package p001a;

import logic.baa.C1616Xf;
import org.mozilla1.javascript.ScriptRuntime;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.alk  reason: case insensitive filesystem */
/* compiled from: a */
public class C6376alk extends C3813vL {
    private static final Double ZERO = Double.valueOf(ScriptRuntime.NaN);

    /* renamed from: d */
    public void mo7919d(ObjectOutput objectOutput, Object obj) {
        objectOutput.writeDouble(((Double) obj).doubleValue());
    }

    /* renamed from: f */
    public Object mo7920f(ObjectInput objectInput) {
        return Double.valueOf(objectInput.readDouble());
    }

    /* renamed from: b */
    public Object mo3591b(C1616Xf xf) {
        return ZERO;
    }
}
