package p001a;

import game.script.associates.Associate;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;

/* renamed from: a.aLu  reason: case insensitive filesystem */
/* compiled from: a */
public class C5502aLu implements Transferable {
    private Associate ilS;

    public C5502aLu(Associate ff) {
        this.ilS = ff;
    }

    public Associate dhU() {
        return this.ilS;
    }

    public Object getTransferData(DataFlavor dataFlavor) {
        if (dataFlavor.equals(DataFlavor.stringFlavor)) {
            return dhV();
        }
        throw new UnsupportedFlavorException(dataFlavor);
    }

    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[]{DataFlavor.stringFlavor};
    }

    public boolean isDataFlavorSupported(DataFlavor dataFlavor) {
        if (dataFlavor.equals(DataFlavor.stringFlavor)) {
            return true;
        }
        return false;
    }

    private String dhV() {
        if (this.ilS != null) {
            return this.ilS.mo2077dL().getName();
        }
        return "";
    }
}
