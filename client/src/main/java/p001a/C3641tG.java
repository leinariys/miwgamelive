package p001a;

import com.hoplon.geometry.Color;
import game.geometry.Vector2fWrap;
import logic.WrapRunnable;
import taikodom.addon.IAddonProperties;

import javax.vecmath.Vector4f;

/* renamed from: a.tG */
/* compiled from: a */
public class C3641tG extends WrapRunnable {
    private final float scale;
    private final float time;
    private C6017aep bss;
    private Vector2fWrap bst;
    private Color color;
    private long startTime = System.currentTimeMillis();
    private Color white;

    public C3641tG(IAddonProperties vWVar, C6017aep aep, float f, float f2, Color color2) {
        this.bss = aep;
        this.bss.mo13161eh(true);
        this.time = f;
        this.scale = f2;
        this.color = color2;
        if (aep.bUs() != null && this.color != null) {
            this.white = new Color((Vector4f) color2.nScale(0.5f));
            this.bst = aep.bUs().getCenterBillboardSize();
            if (this.bst == null) {
                this.bst = new Vector2fWrap(0.0f, 0.0f);
            }
            vWVar.alf().addTask("MaximizeMarkAnim" + aep.toString(), this, 30);
        }
    }

    public void run() {
        if (this.bss == null || this.bss.bUs() == null) {
            cancel();
        }
        float currentTimeMillis = (((float) (System.currentTimeMillis() - this.startTime)) / 1000.0f) / this.time;
        if (currentTimeMillis < 1.0f || this.bss == null || this.bss.bUs() == null) {
            Color color2 = new Color((this.white.x * (1.0f - currentTimeMillis)) + (this.color.x * currentTimeMillis), (this.white.y * (1.0f - currentTimeMillis)) + (this.color.y * currentTimeMillis), (this.white.z * (1.0f - currentTimeMillis)) + (this.color.z * currentTimeMillis), (this.white.w * (1.0f - currentTimeMillis)) + (this.color.w * currentTimeMillis));
            if (this.bss == null || this.bss.bUs() == null) {
                cancel();
                return;
            }
            this.bss.setPrimitiveColor(color2);
            this.bss.bUs().setCenterBillboardSize(new Vector2fWrap(this.bst.x + (this.scale * (1.0f - currentTimeMillis)), ((1.0f - currentTimeMillis) * this.scale) + this.bst.y));
            return;
        }
        this.bss.setPrimitiveColor(this.color);
        this.bss.bUs().setCenterBillboardSize(this.bst);
        this.bss.mo13161eh(false);
        this.bss = null;
        cancel();
    }
}
