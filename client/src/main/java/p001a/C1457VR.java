package p001a;

import org.mozilla1.javascript.ScriptRuntime;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.VR */
/* compiled from: a */
public class C1457VR extends C4124zo {
    public static final int evS = 64;
    public static final int evT = 2047;
    public static final int evU = 1023;
    public static final int evV = 52;
    private static final int evW = -1022;
    private final int evX;
    private final int evY;
    private final int evZ;
    private final int ewa;
    private final int ewb;
    private final long ewc;
    private final long ewd;
    private final long ewe;
    private final int ewf;
    private final double ewg;
    private final double ewh;
    private final boolean ewi;
    private final int ewj;
    private final int ewk;
    private final long ewl;
    private final int ewm;
    private final boolean signed;
    private final double tolerance;
    public String name;

    public C1457VR(boolean z, int i, int i2, boolean z2, int i3) {
        this((String) null, z, i, i2, z2, i3);
    }

    public C1457VR(String str, boolean z, int i, int i2, boolean z2, int i3) {
        int i4;
        long j;
        double longBitsToDouble;
        this.name = str == null ? getClass().getSimpleName() : str;
        this.signed = z;
        this.evX = i;
        this.evY = i2;
        this.ewi = z2;
        this.ewc = (1 << i2) - 1;
        this.ewd = (1 << i) - 1;
        this.ewb = i3;
        long j2 = this.ewd - ((long) i3);
        if (z2) {
            i4 = 2;
        } else {
            i4 = 1;
        }
        this.ewa = (int) (-(j2 - ((long) i4)));
        this.ewf = ((-this.ewa) - 1023) + 1;
        this.evZ = (z ? 1 : 0) + i + i2;
        if (i2 < 52) {
            j = 1 << ((52 - i2) - 1);
        } else {
            j = 0;
        }
        this.ewl = j;
        this.ewj = 52 - i2;
        this.ewk = 64 - this.evZ;
        this.ewe = 1 << (this.evZ - 1);
        this.ewm = ((this.evZ & 7) != 0 ? 1 : 0) + (this.evZ >> 3);
        this.ewg = Double.longBitsToDouble((((long) this.ewa) + 1023) << 52);
        this.ewh = Double.longBitsToDouble(((((long) i3) + 1023) << 52) | (this.ewc << this.ewj));
        if (52 <= i2) {
            longBitsToDouble = ScriptRuntime.NaN;
        } else {
            longBitsToDouble = Double.longBitsToDouble(4607182418800017408L | ((1 << (this.ewj - 1)) - 1)) - 1.0d;
        }
        this.tolerance = longBitsToDouble;
    }

    /* renamed from: fS */
    public static int m10642fS(long j) {
        int i;
        int i2 = 32;
        if ((j >>> 32) != 0) {
            i = (int) (j >> 32);
        } else {
            i = (int) j;
            i2 = 0;
        }
        if ((i >>> 16) != 0) {
            i >>>= 16;
            i2 += 16;
        }
        if ((i >>> 8) != 0) {
            i >>>= 8;
            i2 += 8;
        }
        if ((i >>> 4) != 0) {
            i >>>= 4;
            i2 += 4;
        }
        if ((i >>> 2) != 0) {
            i >>>= 2;
            i2 += 2;
        }
        if ((i >>> 1) != 0) {
            i >>>= 1;
            i2++;
        }
        return i + i2;
    }

    /* renamed from: Kc */
    public int mo6132Kc() {
        return this.evZ;
    }

    /* renamed from: Ka */
    public int mo6130Ka() {
        return this.ewb;
    }

    /* renamed from: Kb */
    public int mo6131Kb() {
        return this.ewa;
    }

    /* renamed from: Ke */
    public int mo6134Ke() {
        return this.evX;
    }

    /* renamed from: Kd */
    public int mo6133Kd() {
        return this.evY;
    }

    public double arQ() {
        return this.ewh;
    }

    public double arR() {
        return this.ewg;
    }

    /* renamed from: h */
    public long mo6144h(double d) {
        int i;
        long j;
        long doubleToRawLongBits = Double.doubleToRawLongBits(d);
        long j2 = doubleToRawLongBits & 4503599627370495L;
        int i2 = (int) ((doubleToRawLongBits >> 52) & 2047);
        if (i2 != evT) {
            if ((this.ewl & j2) != 0) {
                doubleToRawLongBits += this.ewl;
                i2 = (int) ((doubleToRawLongBits >> 52) & 2047);
                j2 = doubleToRawLongBits & 4503599627370495L;
            }
            if (i2 != 0) {
                i2 += this.ewf;
                if (i2 <= 0) {
                    if (i2 > (-this.evY)) {
                        j = (4503599627370496L | j2) >>> (1 - i2);
                    } else {
                        j = 0;
                    }
                    i = 0;
                    j2 = j;
                } else {
                    if (((long) i2) >= this.ewd && (((long) i2) != this.ewd || this.ewi)) {
                        j2 = 4503599627370495L;
                        i = this.ewb + (-this.ewa) + 1;
                    }
                    i = i2;
                }
            } else {
                if (this.ewa > evW) {
                    if (this.ewa + 1022 < this.evY) {
                        j2 >>= this.ewa + 1022;
                        i = i2;
                    } else {
                        j2 = 0;
                        i = i2;
                    }
                }
                i = i2;
            }
        } else {
            if (!this.ewi && (i2 = i2 + this.ewf) <= 0 && i2 > evW) {
                j2 = (4503599627370496L | j2) >> (1 - i2);
                i = 0;
            }
            i = i2;
        }
        if (this.signed) {
            return ((doubleToRawLongBits >> this.ewk) & this.ewe) | ((((long) i) & this.ewd) << this.evY) | (j2 >> this.ewj);
        }
        return ((((long) i) & this.ewd) << this.evY) | (j2 >> this.ewj);
    }

    /* renamed from: du */
    public double mo6141du(long j) {
        int i = (int) ((j >> this.evY) & this.ewd);
        long j2 = this.ewc & j;
        if (((long) i) == this.ewd) {
            i = this.ewi ? evT : i - this.ewf;
        } else if (i != 0) {
            i -= this.ewf;
        } else if (this.ewa > evW && j2 != 0) {
            int fS = (this.evY - m10642fS(j2)) + 1;
            int i2 = this.ewa + 1022;
            if (i2 > fS) {
                i = (this.ewa - fS) + evU;
                j2 = (j2 << fS) & this.ewc;
            } else {
                j2 = (j2 << i2) & this.ewc;
            }
        }
        if (this.signed) {
            return Double.longBitsToDouble((j2 << this.ewj) | (((long) i) << 52) | ((j << this.ewk) & Long.MIN_VALUE));
        }
        return Double.longBitsToDouble((j2 << this.ewj) | (((long) i) << 52));
    }

    /* renamed from: a */
    public void mo6137a(ObjectOutput objectOutput, double d) {
        switch (this.ewm) {
            case 1:
                objectOutput.writeByte((int) mo6144h(d));
                break;
            case 2:
                objectOutput.writeShort((int) mo6144h(d));
                break;
            case 3:
                objectOutput.writeByte((int) (mo6144h(d) >> 16));
                objectOutput.writeShort((int) mo6144h(d));
                break;
            case 4:
                objectOutput.writeInt((int) mo6144h(d));
                break;
            case 5:
                objectOutput.writeByte((int) (mo6144h(d) >> 32));
                objectOutput.writeInt((int) mo6144h(d));
                break;
            case 6:
                objectOutput.writeShort((int) (mo6144h(d) >> 32));
                objectOutput.writeInt((int) mo6144h(d));
                break;
            case 7:
                objectOutput.writeByte((int) (mo6144h(d) >> 56));
                objectOutput.writeShort((int) (mo6144h(d) >> 32));
                objectOutput.writeInt((int) mo6144h(d));
                break;
            case 8:
                objectOutput.writeLong(mo6144h(d));
                break;
        }
        throw new IllegalArgumentException();
    }

    /* renamed from: g */
    public double mo6142g(ObjectInput objectInput) {
        switch (this.ewm) {
            case 1:
                return mo6141du((long) (objectInput.readUnsignedByte() & 255));
            case 2:
                return mo6141du((long) objectInput.readUnsignedShort());
            case 3:
                return mo6141du((long) ((objectInput.readUnsignedByte() << 16) | objectInput.readUnsignedShort()));
            case 4:
                return mo6141du((long) objectInput.readInt());
            case 5:
                int readUnsignedByte = objectInput.readUnsignedByte();
                return mo6141du(((long) objectInput.readInt()) | (((long) readUnsignedByte) << 32));
            case 6:
                int readUnsignedShort = objectInput.readUnsignedShort();
                return mo6141du(((long) objectInput.readInt()) | (((long) readUnsignedShort) << 32));
            case 7:
                int readUnsignedByte2 = objectInput.readUnsignedByte();
                return mo6141du((((long) objectInput.readUnsignedShort()) << 32) | (((long) readUnsignedByte2) << 56) | (((long) objectInput.readInt()) & 4294967295L));
            case 8:
                return mo6141du(objectInput.readLong());
            default:
                throw new IllegalArgumentException();
        }
    }

    public double arS() {
        return this.tolerance;
    }

    /* renamed from: Ki */
    public boolean mo6135Ki() {
        return this.signed;
    }

    public String getName() {
        return this.name;
    }

    /* renamed from: Kj */
    public boolean mo6136Kj() {
        return this.ewi;
    }
}
