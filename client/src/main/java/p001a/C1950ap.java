package p001a;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Vec3d;
import game.script.ship.Ship;
import game.script.ship.ShipType;
import game.script.ship.categories.Bomber;
import game.script.ship.categories.Explorer;
import game.script.ship.categories.Fighter;
import game.script.ship.categories.Freighter;
import game.script.space.Gate;
import logic.aaa.C1506WA;
import logic.aaa.C5783aaP;
import logic.render.IEngineGraphics;
import logic.res.sound.C0907NJ;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.addon.IAddonProperties;
import taikodom.render.SceneView;
import taikodom.render.camera.ChaseCamera;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.RSceneAreaInfo;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

import javax.vecmath.Tuple3d;
import java.util.Random;

/* renamed from: a.ap */
/* compiled from: a */
public class C1950ap {
    private static final String gvR = "data/scene/nod_hjp_cin.pro";
    private static final String gvS = "nod_hjp_cin";
    /* access modifiers changed from: private */

    /* renamed from: BN */
    public Gate f5079BN;
    /* access modifiers changed from: private */
    public PlayerController gvT;
    /* access modifiers changed from: private */
    public SceneObject gvU = null;
    /* access modifiers changed from: private */
    public SceneView gvV;
    /* renamed from: kj */
    public IAddonProperties f5080kj;
    SceneObject gvW;
    SceneObject gvX;
    SceneObject gvY;
    C1952b gvZ;
    /* access modifiers changed from: private */
    private Ship aOO;

    public C1950ap(IAddonProperties vWVar, Gate fFVar, SceneObject sceneObject) {
        this.f5080kj = vWVar;
        this.gvT = this.f5080kj.alb();
        this.f5079BN = fFVar;
        this.aOO = this.gvT.mo22061al();
        this.gvX = sceneObject;
        IEngineGraphics ale = C5916acs.getSingolton().getEngineGraphics();
        this.gvV = new SceneView();
        this.gvV.getScene().setName("Jumping scene");
        RSceneAreaInfo rSceneAreaInfo = new RSceneAreaInfo();
        rSceneAreaInfo.setFogColor(new Color(1.0f, 1.0f, 1.0f, 1.0f));
        rSceneAreaInfo.setFogEnd(7000.0f);
        rSceneAreaInfo.setFogStart(0.0f);
        this.gvV.getScene().addSceneAreaInfo(rSceneAreaInfo);
        this.gvV.setViewport(0, 0, ale.aes(), ale.aet());
    }

    public void dispose() {
        if (this.gvY != null) {
            this.gvY.dispose();
            this.gvY = null;
        }
        if (this.gvX != null) {
            this.gvX.dispose();
            this.gvX = null;
        }
        if (this.gvW != null) {
            this.gvW.dispose();
            this.gvW = null;
        }
    }

    public float bBx() {
        return (float) C5916acs.getSingolton().getEngineGraphics().aes();
    }

    public float bBy() {
        return (float) C5916acs.getSingolton().getEngineGraphics().aet();
    }

    /* access modifiers changed from: private */
    public PlayerController cuN() {
        return this.gvT;
    }

    public void start() {
        if (this.gvV == null) {
            System.err.println("jumpScene is null at jump animation start!");
            return;
        }
        this.gvZ = new C1952b(this);
        this.gvZ.start();
        C5916acs.getSingolton().getLoaderTrail().mo4995a(gvR, gvS, (Scene) null, new C1951a(), "jump scene loading");
        cuN().cPf().mo22164F();
        cuN().cPf().mo22166J();
    }

    public void stop() {
        IEngineGraphics ale = C5916acs.getSingolton().getEngineGraphics();
        ale.mo3046b(this.gvV);
        this.gvV.getScene().removeAllChildren();
        ale.aea().enable();
        cuN().cPf().mo22165H();
        cuN().cPf().mo22167L();
        cuN().mo22135dL().mo14419f((C1506WA) new C5783aaP(C5783aaP.C1841a.FLYING));
    }

    public void cuO() {
        this.gvZ.api = true;
    }

    /* renamed from: a.ap$a */
    class C1951a implements C0907NJ {
        C1951a() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if (renderAsset != null) {
                C1950ap.this.gvU = (SceneObject) renderAsset;
                C1950ap.this.gvV.getScene().addChild(C1950ap.this.gvU);
            }
            C1950ap.this.gvZ.iLn.mo18955dC(false);
        }
    }

    /* renamed from: a.ap$b */
    /* compiled from: a */
    private class C1952b implements C6296akI.C1925a {
        /* renamed from: iQ */
        public final /* synthetic */ C1950ap f5082iQ;
        private final float axO = 300.0f;
        /* access modifiers changed from: private */
        public C1255SZ ajg;
        /* access modifiers changed from: private */
        public boolean api = false;
        public float buL = 30.0f;
        public float buM = 30.0f;
        public float buN = 2.0f;
        public float buO = 5.0f;
        public float buP = 2.0f;
        public float buQ = 1.0f;
        public boolean iLE = true;
        public boolean iLF = true;
        /* access modifiers changed from: private */
        public boolean iLI = false;
        /* access modifiers changed from: private */
        public C2523gL iLn;
        /* access modifiers changed from: private */
        public C2523gL iLo;
        /* access modifiers changed from: private */
        public C2523gL iLp;
        /* access modifiers changed from: private */
        public long iLr = 0;
        /* access modifiers changed from: private */
        public boolean iLu = false;
        public float iLv = 3.0f;
        public float iLw = 10.0f;
        public float iLx = 1.0f;
        /* access modifiers changed from: private */
        public boolean iLy;
        float buR = 2.0f;
        float iLJ;
        float iLK = 0.0f;
        private long buF = System.currentTimeMillis();
        private float buG = 0.0f;
        private float buH = 0.0f;
        private long buS;
        private Vec3f dir = new Vec3f(0.0f, 0.0f, -1.0f);
        private float iLA;
        private Vec3f iLB;
        private float iLC;
        private double iLD;
        private float iLG;
        private float iLH;
        private boolean iLl = false;
        private C2523gL iLm;
        private float iLq = 0.001f;
        private boolean iLs = false;
        private float iLt;
        /* access modifiers changed from: package-private */
        private float iLz;
        private Random random = new Random();

        /* renamed from: vT */
        private ChaseCamera f5083vT;

        public C1952b(C1950ap apVar) {
            int i = 1;
            this.f5082iQ = apVar;
            i = new Random().nextInt(1) == 1 ? -1 : i;
            this.buP = 3.0f;
            this.iLx = 5.0f;
            this.iLw = 35.0f;
            this.iLv = 50.0f;
            this.buN = 1.0f;
            this.buO = 10.0f;
            this.buG = 0.0f;
            this.buH = (float) (i * 90);
            Ship al = apVar.cuN().mo22061al();
            if (al == null) {
                this.buQ = 2.0f;
                this.buL = 10.0f;
                this.buM = 7.0f;
            } else if (al instanceof Fighter) {
                this.buQ = 1.0f;
                this.buL = 7.0f;
                this.buM = 5.0f;
                this.buP = 3.0f;
            } else if (al instanceof Bomber) {
                this.buQ = 0.5f;
                this.buL = 4.0f;
                this.buM = 8.0f;
                this.buP = 1.2f;
            } else if (al instanceof Explorer) {
                this.buQ = 0.8f;
                this.buL = 2.5f;
                this.buM = 5.0f;
                this.buP = 1.3f;
            } else if (al instanceof Freighter) {
                this.buQ = 0.2f;
                this.buL = 1.0f;
                this.buM = 2.0f;
                this.buP = 1.2f;
            }
        }

        /* access modifiers changed from: private */
        public void start() {
            System.out.println("Starting fade out anim");
            this.iLn = new C2523gL(0.0f, 90.0f);
            this.iLn.mo18955dC(true);
            this.iLn.mo18949a((C2523gL.C2524a) new C1953a());
            this.iLn.mo18948a(0.5f, this.f5082iQ.f5080kj);
            this.buF = System.currentTimeMillis();
            if (this.iLE) {
                dtW();
            } else {
                this.iLA = this.iLx;
            }
            if (this.iLF) {
                dtX();
            } else {
                this.iLz = this.buP;
            }
        }

        /* access modifiers changed from: private */
        public void dtU() {
            C5916acs.getSingolton().getEngineGraphics().aea().disable();
            System.out.println("Showing jump scene");
            C5916acs.getSingolton().getEngineGraphics().mo3005a(this.f5082iQ.gvV);
            this.f5082iQ.gvY.setTransform(this.f5082iQ.gvX.getTransform());
            this.f5082iQ.gvV.getScene().addChild(this.f5082iQ.gvX);
            System.currentTimeMillis();
            C5916acs.getSingolton().getLoaderTrail().mo4995a(this.f5082iQ.f5079BN.mo649ir(), this.f5082iQ.f5079BN.mo648ip(), (Scene) null, new C1954b(), "node pre load");
            dtV();
            if (this.ajg != null) {
                this.ajg.destroy();
            }
            this.ajg = new C1255SZ();
            this.ajg.mo5422kc(true);
            this.ajg.mo5419a(this.f5082iQ.cuN().mo22061al(), this.f5082iQ.gvX);
            this.ajg.mo5424nU(50.0f);
            this.ajg.mo5418F(3.0f, 6.0f);
            this.ajg.mo5420aO(new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, 100.0d));
            this.ajg.setFollowEmitter(true);
            this.ajg.mo5423nT(1.0f);
            this.f5082iQ.gvX.getTransform().orientation.transform(new Vec3f(0.0f, 0.0f, -1.0f), this.dir);
            this.iLo = new C2523gL(90.0f, 180.0f);
            this.iLo.mo18949a((C2523gL.C2524a) new C1957e());
            this.iLo.mo18948a(0.25f, this.f5082iQ.f5080kj);
            this.iLr = System.currentTimeMillis();
            C5916acs.getSingolton().getEngineGraphics().mo3004a((C6296akI.C1925a) this);
        }

        private void dtV() {
            this.f5083vT = new ChaseCamera();
            this.f5083vT.setName("JUMP CAMERA");
            this.f5083vT.setFovY(85.0f);
            ShipType agH = this.f5082iQ.cuN().mo22061al().agH();
            this.iLJ = this.f5082iQ.cuN().mo22064b(this.f5082iQ.gvX, agH, false);
            this.iLK = this.iLJ;
            float bkz = agH.bkz();
            if (bkz <= 0.0f) {
                bkz = 0.5f;
            }
            float f = bkz * this.iLJ;
            Vec3d ajr = new Vec3d(ScriptRuntime.NaN, (double) f, (double) this.iLJ);
            Vec3d ajr2 = new Vec3d(ScriptRuntime.NaN, (double) f, ScriptRuntime.NaN);
            this.f5083vT.setTarget(this.f5082iQ.gvY);
            this.f5083vT.setCameraOffset(ajr);
            this.f5083vT.setCameraTargetOffset(ajr2);
            this.f5083vT.setPosition(this.f5082iQ.gvY.getPosition().mo9504d((Tuple3d) ajr));
            if (this.f5083vT == null) {
                System.out.println("Null chaseCamera into CreateCamera ( JumpAnimation )");
            }
            this.f5082iQ.gvV.setCamera(this.f5083vT);
        }

        /* renamed from: a */
        public void mo1968a(float f, long j) {
            if (!this.iLu) {
                if (this.f5082iQ.gvX == null || this.f5082iQ.gvY == null || this.iLl) {
                    destroy();
                    stop();
                    this.f5082iQ.stop();
                    C5916acs.getSingolton().getEngineGraphics().mo3044b((C6296akI.C1925a) this);
                    return;
                }
                long currentTimeMillis = System.currentTimeMillis();
                this.buF = currentTimeMillis;
                if (this.api && !this.iLs) {
                    float f2 = ((float) (currentTimeMillis - this.iLr)) / 1000.0f;
                    if (!this.iLs && f2 - 0.2f > this.iLq) {
                        this.iLs = true;
                        this.iLm = new C2523gL(0.0f, 90.0f);
                        this.iLm.mo18949a((C2523gL.C2524a) new C1956d());
                        this.iLm.mo18948a(1.0f, this.f5082iQ.f5080kj);
                    }
                }
                this.buG += this.buL * f;
                if (this.buG > 360.0f) {
                    this.buG = 0.0f;
                }
                this.buH += this.buM * f;
                if (this.buH > 360.0f) {
                    this.buH = 0.0f;
                }
                this.iLD += (double) (this.iLv * f);
                this.iLC += this.iLw * f;
                float sin = (float) Math.sin(((double) this.buG) * 0.017453292519943295d);
                float sin2 = (float) Math.sin(((double) this.buH) * 0.017453292519943295d);
                float sin3 = (float) Math.sin(this.iLD * 0.017453292519943295d);
                float sin4 = (float) Math.sin(((double) this.iLC) * 0.017453292519943295d);
                Vec3d position = this.f5082iQ.gvX.getPosition();
                this.iLt = 300.0f * f;
                Vec3f mS = this.dir.mo23510mS(this.iLt);
                float f3 = (this.iLz * sin4) - this.iLG;
                float f4 = (this.iLA * sin3) - this.iLH;
                Vec3d q = position.mo9531q(mS);
                this.f5082iQ.gvY.setPosition(this.f5082iQ.gvY.getPosition().mo9531q(mS));
                this.iLG = sin4 * this.iLz;
                this.iLH = sin3 * this.iLA;
                if (this.iLE && this.iLD > 360.0d) {
                    this.iLD /= 360.0d;
                    dtW();
                }
                if (this.iLF && this.iLC > 360.0f) {
                    this.iLC /= 360.0f;
                    dtX();
                }
                Matrix4fWrap ajk = new Matrix4fWrap();
                ajk.rotateZ(sin2 * this.buO * this.buQ);
                ajk.rotateX(sin * this.buN * this.buQ);
                this.f5082iQ.gvX.setTransform(ajk);
                this.f5082iQ.gvX.setPosition(new Vec3d(q.x + ((double) f4), q.y + ((double) f3), q.z));
                if (this.iLI) {
                    this.f5082iQ.gvW.setPosition(this.f5082iQ.gvY.getPosition().mo9531q(this.dir.mo23510mS(10.0f * 300.0f * (this.iLq + 1.0f))));
                    this.f5082iQ.gvV.getScene().addChild(this.f5082iQ.gvW);
                    this.iLI = false;
                }
                if (!this.iLI && this.f5082iQ.gvW != null && !this.iLy) {
                    Vec3d position2 = this.f5082iQ.gvW.getPosition();
                    this.f5082iQ.gvW.setPosition(position2.x, position2.y, position2.z + ((double) (10.0f * this.iLq * f * 300.0f)));
                }
            }
        }

        private void dtW() {
            this.iLA = this.iLx * (((float) this.random.nextInt(100)) / 100.0f);
        }

        private void dtX() {
            this.iLz = this.buP * (((float) this.random.nextInt(100)) / 100.0f);
        }

        public void cancel() {
            this.iLl = true;
        }

        /* access modifiers changed from: private */
        public void stop() {
            this.iLp = new C2523gL(90.0f, 180.0f);
            this.iLp.mo18949a((C2523gL.C2524a) new C1955c());
            this.iLp.mo18950a(this.f5082iQ.f5080kj);
            if (this.iLm != null) {
                this.iLm.destroy();
            }
            C5916acs.getSingolton().getEngineGraphics().mo3044b((C6296akI.C1925a) this);
        }

        /* access modifiers changed from: private */
        public void destroy() {
            if (this.f5082iQ.gvX != null) {
                this.f5082iQ.gvX.dispose();
                this.f5082iQ.gvX = null;
            }
            if (this.ajg != null) {
                this.ajg.destroy();
                this.ajg = null;
            }
            if (this.iLm != null) {
                this.iLm.cancel();
                this.iLm.destroy();
                this.iLm = null;
            }
            if (this.iLn != null) {
                this.iLn.cancel();
                this.iLn.destroy();
                this.iLn = null;
            }
            if (this.iLo != null) {
                this.iLo.cancel();
                this.iLo.destroy();
                this.iLo = null;
            }
            if (this.iLp != null) {
                this.iLp.cancel();
                this.iLp.destroy();
                this.iLp = null;
            }
        }

        /* renamed from: a.ap$b$a */
        class C1953a implements C2523gL.C2524a {
            C1953a() {
            }

            /* renamed from: wk */
            public void mo15343wk() {
                if (C1952b.this.f5082iQ.gvY == null) {
                    C1952b.this.f5082iQ.gvY = new SceneObject();
                    C1952b.this.dtU();
                }
            }
        }

        /* renamed from: a.ap$b$b */
        /* compiled from: a */
        class C1954b implements C0907NJ {
            C1954b() {
            }

            /* renamed from: a */
            public void mo968a(RenderAsset renderAsset) {
                if (renderAsset != null) {
                    C1952b.this.f5082iQ.gvW = (SceneObject) renderAsset;
                    C1952b.this.iLI = true;
                    C1952b.this.iLr = System.currentTimeMillis();
                }
            }
        }

        /* renamed from: a.ap$b$e */
        /* compiled from: a */
        class C1957e implements C2523gL.C2524a {
            C1957e() {
            }

            /* renamed from: wk */
            public void mo15343wk() {
                C1952b.this.iLn.destroy();
                C1952b.this.iLo.destroy();
                C1952b.this.f5082iQ.gvT.mo22135dL().bQx().mo18321i(C1952b.this.f5082iQ.gvX);
            }
        }

        /* renamed from: a.ap$b$d */
        /* compiled from: a */
        class C1956d implements C2523gL.C2524a {
            C1956d() {
            }

            /* renamed from: wk */
            public void mo15343wk() {
                C1952b.this.iLu = true;
                C1952b.this.stop();
                C1952b.this.f5082iQ.stop();
                C1952b.this.f5082iQ.gvV.getScene().removeChild(C1952b.this.f5082iQ.gvX);
                C1952b.this.ajg.destroy();
                C1952b.this.iLy = true;
            }
        }

        /* renamed from: a.ap$b$c */
        /* compiled from: a */
        class C1955c implements C2523gL.C2524a {
            C1955c() {
            }

            /* renamed from: wk */
            public void mo15343wk() {
                if (C1952b.this.iLp != null) {
                    C1952b.this.iLp.destroy();
                    C1952b.this.iLp = null;
                }
                C1952b.this.destroy();
            }
        }
    }
}
