package p001a;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/* renamed from: a.Co */
/* compiled from: a */
public class C0223Co<T> {
    private static Comparator fLj = new C5996aeU();
    private static Comparator fLk = new C5994aeS();
    private Comparator comparator;
    private Class fLh;
    private C0224a fLi = new C0224a((C0224a) null);
    private ArrayList list = new ArrayList();

    public C0223Co(Class cls) {
        this.fLh = cls;
        if (cls == Float.TYPE) {
            this.comparator = fLj;
        } else if (!cls.isPrimitive()) {
            this.comparator = fLk;
        } else {
            throw new UnsupportedOperationException("unsupported type " + cls);
        }
    }

    private T create(int i) {
        return Array.newInstance(this.fLh, i);
    }

    /* renamed from: qW */
    public T mo1166qW(int i) {
        this.fLi.value = i;
        int binarySearch = Collections.binarySearch(this.list, this.fLi, this.comparator);
        if (binarySearch < 0) {
            return create(i);
        }
        return this.list.remove(binarySearch);
    }

    /* renamed from: qX */
    public T mo1167qX(int i) {
        this.fLi.value = i;
        int binarySearch = Collections.binarySearch(this.list, this.fLi, this.comparator);
        if (binarySearch >= 0) {
            return this.list.remove(binarySearch);
        }
        int i2 = (-binarySearch) - 1;
        if (i2 < this.list.size()) {
            return this.list.remove(i2);
        }
        return create(i);
    }

    public void release(T t) {
        int binarySearch = Collections.binarySearch(this.list, t, this.comparator);
        if (binarySearch < 0) {
            binarySearch = (-binarySearch) - 1;
        }
        this.list.add(binarySearch, t);
        if (this.comparator == fLk) {
            Object[] objArr = (Object[]) t;
            for (int i = 0; i < objArr.length; i++) {
                objArr[i] = null;
            }
        }
    }

    /* renamed from: a.Co$a */
    private static class C0224a {
        public int value;

        private C0224a() {
        }

        /* synthetic */ C0224a(C0224a aVar) {
            this();
        }
    }
}
