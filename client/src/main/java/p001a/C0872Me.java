package p001a;

import java.util.HashMap;
import java.util.Map;

/* renamed from: a.Me */
/* compiled from: a */
public class C0872Me {
    private String[] dyW;
    private Map<String, String> map = null;

    public C0872Me(String[] strArr) {
        this.dyW = strArr;
    }

    /* renamed from: f */
    private static Map<String, String> m7081f(String[] strArr) {
        String str;
        HashMap hashMap = new HashMap();
        int i = 0;
        String str2 = null;
        while (i < strArr.length) {
            String str3 = strArr[i];
            if (str3.startsWith("-")) {
                str = str3.substring(1);
                hashMap.put(str, (Object) null);
            } else {
                hashMap.put(str2, str3);
                str = null;
            }
            i++;
            str2 = str;
        }
        return hashMap;
    }

    public String get(String str, String str2) {
        Map<String, String> map2 = getMap();
        return !map2.containsKey(str) ? str2 : map2.get(str);
    }

    public String get(String str) {
        return getMap().get(str);
    }

    public boolean contains(String str) {
        return getMap().containsKey(str);
    }

    public String bgC() {
        return get((String) null);
    }

    public Map<String, String> getMap() {
        if (this.map == null) {
            this.map = m7081f(this.dyW);
        }
        return this.map;
    }
}
