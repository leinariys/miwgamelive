package p001a;

import game.network.exception.IsDisposedException;

/* renamed from: a.aoL  reason: case insensitive filesystem */
/* compiled from: a */
public class C6507aoL extends IsDisposedException {


    public C6507aoL() {
    }

    public C6507aoL(String str) {
        super(str);
    }

    public C6507aoL(Throwable th) {
        super(th);
    }

    public C6507aoL(String str, Throwable th) {
        super(str, th);
    }
}
