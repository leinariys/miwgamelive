package p001a;

import game.script.damage.DamageType;

import java.util.Map;

/* renamed from: a.asI  reason: case insensitive filesystem */
/* compiled from: a */
public class C6712asI extends C5761aVt {
    private static final long serialVersionUID = -5063787289018783630L;
    private long bpa = -1;
    private int gvN = 0;
    private float gvO = 0.0f;
    private float gvP = 0.0f;
    private long gvQ = -1;

    /* renamed from: d */
    public void mo15903d(C5260aCm acm) {
        for (Map.Entry<DamageType, Float> value : acm.cPK().entrySet()) {
            this.gvO = ((Float) value.getValue()).floatValue() + this.gvO;
        }
    }

    /* renamed from: e */
    public void mo15904e(C5260aCm acm) {
        for (Map.Entry<DamageType, Float> value : acm.cPK().entrySet()) {
            this.gvP = ((Float) value.getValue()).floatValue() + this.gvP;
        }
    }

    public float cuK() {
        return -1.0f;
    }

    /* renamed from: a */
    public int mo11955a(C5761aVt avt) {
        if (!(avt instanceof C6712asI)) {
            return super.mo11955a(avt);
        }
        C6712asI asi = (C6712asI) avt;
        int[] iArr = new int[2];
        if (cuM() > asi.cuM()) {
            iArr[0] = iArr[0] + 1;
        } else if (cuM() < asi.cuM()) {
            iArr[1] = iArr[1] + 1;
        }
        if (this.gvP > asi.gvP) {
            iArr[0] = iArr[0] + 1;
        } else if (this.gvP < asi.gvP) {
            iArr[1] = iArr[1] + 1;
        }
        if (this.gvO < asi.gvO) {
            iArr[0] = iArr[0] + 1;
        } else if (this.gvO > asi.gvO) {
            iArr[1] = iArr[1] + 1;
        }
        if (this.gvN > asi.gvN) {
            iArr[0] = iArr[0] + 1;
        } else if (this.gvN < asi.gvN) {
            iArr[1] = iArr[1] + 1;
        }
        if (iArr[0] > iArr[1]) {
            return -1;
        }
        if (iArr[1] <= iArr[0]) {
            return 0;
        }
        return 1;
    }

    /* renamed from: iI */
    public void mo15905iI(long j) {
        this.bpa = j;
    }

    /* renamed from: iJ */
    public void mo15906iJ(long j) {
        this.gvQ = j;
    }

    public void cuL() {
        this.gvN++;
    }

    public long cuM() {
        return this.gvQ - this.bpa;
    }
}
