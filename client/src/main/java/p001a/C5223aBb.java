package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import logic.aaa.C1088Pv;
import logic.thred.C1362Tq;
import logic.thred.C6339akz;
import logic.thred.C6615aqP;

import java.util.List;

/* renamed from: a.aBb  reason: case insensitive filesystem */
/* compiled from: a */
public class C5223aBb implements C6268ajg, C2678iT {
    /* access modifiers changed from: private */
    public final C6615aqP aRv;
    /* access modifiers changed from: private */
    public final C6615aqP aRw;
    /* access modifiers changed from: private */
    @Deprecated
    public final Vec3f ijl = new Vec3f();
    private final Vec3d ijj = new Vec3d();
    /* access modifiers changed from: private */
    public C1088Pv ijk = C1088Pv.HIT;
    C0114BS ijq = new C0114BS();
    private int hashCode;
    private C1362Tq ijm;
    private C1362Tq ijn;
    private boolean ijo;
    private boolean ijp;

    public C5223aBb(C6615aqP aqp, C6615aqP aqp2) {
        this.aRv = aqp;
        this.aRw = aqp2;
        this.hashCode = aqp.hashCode() + (aqp2.hashCode() * 31);
    }

    /* renamed from: a */
    public void mo7945a(Vec3f vec3f, Vec3d ajr, float f) {
        C6339akz aBa = this.ijq.aBa();
        aBa.fTL.set(vec3f);
        aBa.fTM.mo23512n(vec3f).negate();
        aBa.fTK.mo9484aA(ajr);
        aBa.f4816Ok = f;
        this.aRv.mo2439IQ().mo17333a(ajr, aBa.fTI);
        this.aRw.mo2439IQ().mo17333a(ajr, aBa.fTJ);
        this.ijo = true;
    }

    public C1362Tq ccP() {
        if (this.ijm != null) {
            return this.ijm;
        }
        C1775a aVar = new C1775a();
        this.ijm = aVar;
        return aVar;
    }

    public C1362Tq ccQ() {
        if (this.ijn == null) {
            this.ijn = new C1776b();
        }
        return this.ijn;
    }

    /* renamed from: UV */
    public C6615aqP mo7942UV() {
        return this.aRv;
    }

    /* renamed from: UW */
    public C6615aqP mo7943UW() {
        return this.aRw;
    }

    public Vec3f dgv() {
        return this.ijl;
    }

    public C1088Pv ccR() {
        return this.ijk;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != C5223aBb.class) {
            return false;
        }
        C5223aBb abb = (C5223aBb) obj;
        if (abb.aRv == this.aRv && abb.aRw == this.aRw) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.hashCode;
    }

    public String toString() {
        return super.toString();
    }

    /* renamed from: a */
    public void mo7944a(C1088Pv pv) {
        this.ijk = pv;
    }

    public void reset() {
        this.ijo = false;
        this.ijq.clear();
    }

    /* renamed from: Ee */
    public Vec3d mo7940Ee() {
        return this.ijj;
    }

    /* renamed from: Ef */
    public boolean mo7941Ef() {
        return this.ijo;
    }

    /* renamed from: jL */
    public void mo7954jL(boolean z) {
        this.ijp = z;
    }

    public boolean ccS() {
        return this.ijp;
    }

    public List<C6339akz> ccT() {
        return this.ijq;
    }

    /* renamed from: a.aBb$a */
    class C1775a extends C2639hs {
        C1775a() {
        }

        /* renamed from: Jj */
        public C6615aqP mo5750Jj() {
            return C5223aBb.this.aRv;
        }

        /* renamed from: Ji */
        public C6615aqP mo5749Ji() {
            return C5223aBb.this.aRw;
        }

        /* renamed from: Jh */
        public Vec3f mo5748Jh() {
            if (C5223aBb.this.ijq.size() > 0) {
                return C5223aBb.this.ijq.get(0).fTM;
            }
            return C5223aBb.this.ijl;
        }

        /* renamed from: Jk */
        public Vec3f mo5751Jk() {
            if (C5223aBb.this.ijq.size() > 0) {
                return C5223aBb.this.ijq.get(0).fTI;
            }
            return C5223aBb.this.ijl;
        }

        /* renamed from: Jl */
        public C1088Pv mo5752Jl() {
            return C5223aBb.this.ijk;
        }

        /* renamed from: Jm */
        public C6339akz mo5753Jm() {
            return C5223aBb.this.ijq.get(0);
        }
    }

    /* renamed from: a.aBb$b */
    /* compiled from: a */
    class C1776b extends C2639hs {
        C1776b() {
        }

        /* renamed from: Jj */
        public C6615aqP mo5750Jj() {
            return C5223aBb.this.aRw;
        }

        /* renamed from: Ji */
        public C6615aqP mo5749Ji() {
            return C5223aBb.this.aRv;
        }

        /* renamed from: Jh */
        public Vec3f mo5748Jh() {
            if (C5223aBb.this.ijq.size() > 0) {
                return C5223aBb.this.ijq.get(0).fTL;
            }
            return C5223aBb.this.ijl;
        }

        /* renamed from: Jk */
        public Vec3f mo5751Jk() {
            if (C5223aBb.this.ijq.size() > 0) {
                return C5223aBb.this.ijq.get(0).fTJ;
            }
            return C5223aBb.this.ijl;
        }

        /* renamed from: Jl */
        public C1088Pv mo5752Jl() {
            return C5223aBb.this.ijk;
        }

        /* renamed from: Jm */
        public C6339akz mo5753Jm() {
            return C5223aBb.this.ijq.get(0);
        }
    }
}
