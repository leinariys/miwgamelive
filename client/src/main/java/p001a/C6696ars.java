package p001a;

import java.lang.reflect.Field;

/* renamed from: a.ars  reason: case insensitive filesystem */
/* compiled from: a */
public class C6696ars {
    private final String fieldName;
    private final C3662tR grV;
    private final int grW;
    private Field field;

    public C6696ars(C3662tR tRVar, int i, String str) {
        this.grV = tRVar;
        this.grW = i;
        this.fieldName = str;
    }

    public C3662tR cse() {
        return this.grV;
    }

    public int csf() {
        return this.grW;
    }

    /* renamed from: a */
    public Field mo15880a(C3662tR tRVar) {
        if (this.field == null) {
            this.field = tRVar.getClass().getDeclaredField(this.fieldName);
        }
        return this.field;
    }

    public String getFieldName() {
        return this.fieldName;
    }
}
