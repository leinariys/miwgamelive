package p001a;

import game.geometry.Vec3d;
import gnu.trove.TObjectProcedure;
import logic.aaa.C2235dB;
import logic.bbb.C3601so;
import logic.thred.C6615aqP;
import logic.thred.LogPrinter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: a.aEY */
/* compiled from: a */
public class aEY<E extends C2235dB, B extends C0461GN> implements C3055nX<E, B> {
    private static final LogPrinter log = LogPrinter.m10275K(aEY.class);
    final C3286py fOr = new C6020aes();
    final List<C2748jS<E, B>> hIm = new ArrayList();
    final double hIp;
    final double hIq;
    final double hIr;
    final double hIs;
    final double hIt;
    final double hIu;
    final float hIw;
    final float hIx;
    final C3601so hIy;
    private final C3752uZ<C2748jS<E, B>> hIn = new C3752uZ<>();
    private final aJL<C2748jS<E, B>> hIo = new aJL<>();
    int nextId;
    private CurrentTimeMilli cVg = SingletonCurrentTimeMilliImpl.CURRENT_TIME_MILLI;
    private List<C5537aNd> hIk = Collections.synchronizedList(new ArrayList());
    private List<C5537aNd> hIl = Collections.synchronizedList(new ArrayList());
    private long hIv;

    public aEY(double d, double d2, double d3) {
        this.hIy = new C3601so((float) d, (float) d2, (float) d3);
        this.hIs = d;
        this.hIt = d2;
        this.hIu = d3;
        this.hIp = 1.0d / d;
        this.hIq = 1.0d / d2;
        this.hIr = 1.0d / d3;
        this.hIx = (float) Math.sqrt((d * d) + (d2 * d2) + (d3 * d3));
        this.hIw = this.hIx / 2.0f;
    }

    /* renamed from: a */
    public void mo8633a(float f, float f2, C2748jS<E, B> jSVar) {
        if (f2 == Float.MAX_VALUE) {
            this.hIn.mo22426b(f, jSVar);
        } else {
            this.hIn.mo22424a(f, f2, jSVar);
        }
    }

    /* renamed from: a */
    public C0463GP<E> mo8631a(E e, int i) {
        C5537aNd and = new C5537aNd(this, e);
        and.irp = i;
        if (i != 0) {
            this.hIk.add(and);
        }
        return and;
    }

    /* renamed from: a */
    public C3387qy<B> mo8632a(B b) {
        aJG ajg = new aJG(this, b);
        this.hIk.add(ajg);
        return ajg;
    }

    /* renamed from: gZ */
    public void mo8641gZ() {
        List<C5537aNd> list = this.hIk;
        this.hIk = this.hIl;
        this.hIl = list;
        int size = list.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                m14245c(list.get(i));
            }
            list.clear();
        }
        this.fOr.mo1231gZ();
        C0250DD Vz = this.fOr.mo1224Vz();
        if (Vz.size() > 0) {
            for (C3102np next : Vz) {
                C2748jS jSVar = (C2748jS) next.mo14894Qg().dhC();
                C2748jS jSVar2 = (C2748jS) next.mo14895Qh().dhC();
                if (jSVar.apL == next.mo14894Qg() && jSVar2.apK == next.mo14895Qh()) {
                    jSVar2.mo19950b(jSVar);
                } else if (jSVar2.apL == next.mo14895Qh() && jSVar.apK == next.mo14894Qg()) {
                    jSVar.mo19950b(jSVar2);
                }
            }
        }
        C0250DD VA = this.fOr.mo1222VA();
        if (VA.size() > 0) {
            for (C3102np next2 : VA) {
                C2748jS jSVar3 = (C2748jS) next2.mo14894Qg().dhC();
                C2748jS jSVar4 = (C2748jS) next2.mo14895Qh().dhC();
                if (!jSVar3.isDisposed() && !jSVar4.isDisposed() && jSVar3 != jSVar4) {
                    if (jSVar3.apL == next2.mo14894Qg() && jSVar4.apK == next2.mo14895Qh()) {
                        jSVar4.mo19947a(jSVar3);
                    } else if (jSVar4.apL == next2.mo14895Qh() && jSVar3.apK == next2.mo14894Qg()) {
                        jSVar3.mo19947a(jSVar4);
                    }
                }
            }
        }
        if (this.cVg.currentTimeMillis() - this.hIv > 1000) {
            this.hIv = this.cVg.currentTimeMillis();
            int size2 = this.hIm.size();
            if (size2 > 0) {
                for (int i2 = 0; i2 < size2; i2++) {
                    C2748jS jSVar5 = this.hIm.get(i2);
                    if (jSVar5.isEmpty()) {
                        this.hIo.remove(jSVar5.getPosition());
                        if (jSVar5.apK != null) {
                            jSVar5.apK.dispose();
                        }
                        if (jSVar5.apL != null) {
                            jSVar5.apL.dispose();
                        }
                        this.hIn.mo22426b(jSVar5.mo11190IN(), jSVar5);
                        jSVar5.dispose();
                    } else {
                        jSVar5.apO = false;
                    }
                }
                this.hIm.clear();
            }
        }
    }

    /* renamed from: c */
    private void m14245c(C5537aNd and) {
        C2748jS m;
        if (and.isDisposed()) {
            and.mo9463c((C2748jS) null);
            return;
        }
        and.fYs = false;
        C2748jS djJ = and.djJ();
        if (djJ != null) {
            Vec3d position = and.mo2340kP().getPosition();
            Vec3d ajr = djJ.position;
            double l = m14246l(position.x, this.hIp, this.hIs);
            double l2 = m14246l(position.y, this.hIq, this.hIt);
            double l3 = m14246l(position.z, this.hIr, this.hIu);
            if ((ajr.x != position.x || ajr.y != position.y || ajr.z != position.z) && (m = m14247m(l, l2, l3)) != djJ) {
                if (m == null) {
                    log.error("Null voxel for: " + and);
                }
                and.mo9463c(m);
                return;
            }
            return;
        }
        C2748jS aw = m14244aw(and.mo2340kP().getPosition());
        if (aw == null) {
            log.error("Null voxel for previously null voxel onwer: " + and);
        }
        and.mo9463c(aw);
    }

    /* renamed from: l */
    private double m14246l(double d, double d2, double d3) {
        return Math.ceil(d * d2) * d3;
    }

    /* renamed from: aw */
    private C2748jS<E, B> m14244aw(Vec3d ajr) {
        return m14247m(m14246l(ajr.x, this.hIp, this.hIs), m14246l(ajr.y, this.hIq, this.hIt), m14246l(ajr.z, this.hIr, this.hIu));
    }

    /* renamed from: m */
    private C2748jS<E, B> m14247m(double d, double d2, double d3) {
        C2748jS<E, B> n = this.hIo.mo9468n(d, d2, d3);
        if (n != null) {
            return n;
        }
        double d4 = d + d2 + d3;
        if (Double.isNaN(d4) || Double.isInfinite(d4)) {
            System.out.println("NaN");
            return null;
        }
        C2748jS<E, B> jSVar = new C2748jS<>(this, new Vec3d(d, d2, d3));
        this.hIo.put(jSVar.position, jSVar);
        jSVar.apK = this.fOr.mo1225a(jSVar.aabb, (C6615aqP) jSVar);
        return jSVar;
    }

    /* renamed from: p */
    public C3055nX.C3056a<E, B> mo8644p(Vec3d ajr) {
        return this.hIo.mo9468n(m14246l(ajr.x, this.hIp, this.hIs), m14246l(ajr.y, this.hIq, this.hIt), m14246l(ajr.z, this.hIr, this.hIu));
    }

    /* renamed from: a */
    public void mo8634a(float f, C3055nX.C3057b<E, B> bVar) {
        float f2 = f * f;
        int size = this.hIn.size();
        int i = 0;
        while (i < size && this.hIn.mo22431eV(i) <= f2) {
            bVar.mo1933a(this.hIn.elementAt(i));
            i++;
        }
    }

    /* renamed from: a */
    public void mo8636a(C3055nX.C3057b<E, B> bVar) {
        this.hIo.forEachValue(new C1794a(bVar));
    }

    public CurrentTimeMilli bvh() {
        return this.cVg;
    }

    /* renamed from: a */
    public void mo8635a(CurrentTimeMilli ahw) {
        this.cVg = ahw;
    }

    public double getXRes() {
        return this.hIs;
    }

    public double getYRes() {
        return this.hIt;
    }

    public double cYg() {
        return this.hIu;
    }

    public void dispose() {
        this.hIk.clear();
        this.hIm.clear();
        this.hIn.clear();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public void mo8639d(C5537aNd and) {
        this.hIk.add(and);
    }

    /* renamed from: a.aEY$a */
    class C1794a implements TObjectProcedure<C2748jS<E, B>> {
        private final /* synthetic */ C3055nX.C3057b izK;

        C1794a(C3055nX.C3057b bVar) {
            this.izK = bVar;
        }

        /* renamed from: d */
        public boolean execute(C2748jS<E, B> jSVar) {
            this.izK.mo1933a(jSVar);
            return true;
        }
    }
}
