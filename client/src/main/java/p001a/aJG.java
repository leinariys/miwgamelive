package p001a;

/* renamed from: a.aJG */
/* compiled from: a */
class aJG<B extends C0461GN> extends C5537aNd<B> implements C3387qy<B>, Comparable<aJG<B>> {
    private float igx;
    private float range;

    public aJG(aEY aey, B b) {
        super(aey, b);
        mo9467jD(b.mo2337in());
    }

    /* renamed from: kO */
    public void mo2339kO() {
        super.mo2339kO();
        float in = ((C0461GN) mo2340kP()).mo2337in();
        if (in != this.range && this.irq != null) {
            this.irq.mo19945a(this, in);
        }
    }

    /* renamed from: c */
    public void mo9463c(C2748jS jSVar) {
        if (this.irq != jSVar) {
            if (this.irq != null) {
                this.irq.mo19948b(this);
            }
            this.irq = jSVar;
            if (jSVar != null) {
                mo9467jD(((C0461GN) mo2340kP()).mo2337in());
                jSVar.mo19944a(this);
            }
        }
    }

    /* renamed from: c */
    public int compareTo(aJG<B> ajg) {
        float f = ajg.igx - this.igx;
        if (f < 0.0f) {
            return -1;
        }
        if (f == 0.0f) {
            return this.f3433id - ajg.f3433id;
        }
        return 1;
    }

    /* renamed from: in */
    public float mo9466in() {
        return this.range;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: jD */
    public void mo9467jD(float f) {
        if (this.range != f) {
            this.range = f;
            float f2 = this.range + this.apM.hIw;
            this.igx = f2 * f2;
        }
    }

    public float dfH() {
        return this.igx;
    }
}
