package p001a;

/* renamed from: a.qg */
/* compiled from: a */
public class C3360qg {
    /* renamed from: k */
    public static String m37769k(Class cls) {
        if (cls == null) {
            return null;
        }
        return cls.getName();
    }

    /* renamed from: bf */
    public static Class m37768bf(String str) {
        if (str == null || "null".equals(str) || "".equals(str)) {
            return null;
        }
        try {
            return Class.forName(str);
        } catch (Exception e) {
            return null;
        }
    }
}
