package p001a;

/* renamed from: a.aeh  reason: case insensitive filesystem */
/* compiled from: a */
public class C6009aeh extends aOG {
    public static final C5961adl fnq = new C5961adl("JoystickAxisEvent");
    private int fnp;
    private float value;

    public C6009aeh(int i, float f) {
        this.fnp = i;
        this.value = f;
    }

    public final int getAxis() {
        return this.fnp;
    }

    public final float getValue() {
        return this.value;
    }

    /* renamed from: Fp */
    public C5961adl mo2005Fp() {
        return fnq;
    }
}
