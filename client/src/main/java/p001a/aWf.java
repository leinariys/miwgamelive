package p001a;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aWf */
/* compiled from: a */
public final class aWf {
    public static final aWf jdQ = new aWf("GrannyAccumulationExtracted", grannyJNI.GrannyAccumulationExtracted_get());
    public static final aWf jdR = new aWf("GrannyTrackGroupIsSorted", grannyJNI.GrannyTrackGroupIsSorted_get());
    public static final aWf jdS = new aWf("GrannyAccumulationIsVDA", grannyJNI.GrannyAccumulationIsVDA_get());
    private static aWf[] jdT = {jdQ, jdR, jdS};

    /* renamed from: pF */
    private static int f4002pF = 0;

    /* renamed from: pG */
    private final int f4003pG;

    /* renamed from: pH */
    private final String f4004pH;

    private aWf(String str) {
        this.f4004pH = str;
        int i = f4002pF;
        f4002pF = i + 1;
        this.f4003pG = i;
    }

    private aWf(String str, int i) {
        this.f4004pH = str;
        this.f4003pG = i;
        f4002pF = i + 1;
    }

    private aWf(String str, aWf awf) {
        this.f4004pH = str;
        this.f4003pG = awf.f4003pG;
        f4002pF = this.f4003pG + 1;
    }

    /* renamed from: Bi */
    public static aWf m19250Bi(int i) {
        if (i < jdT.length && i >= 0 && jdT[i].f4003pG == i) {
            return jdT[i];
        }
        for (int i2 = 0; i2 < jdT.length; i2++) {
            if (jdT[i2].f4003pG == i) {
                return jdT[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + aWf.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f4003pG;
    }

    public String toString() {
        return this.f4004pH;
    }
}
