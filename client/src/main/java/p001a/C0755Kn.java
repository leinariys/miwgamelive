package p001a;

import game.network.message.C0474GZ;
import game.network.message.externalizable.C6302akO;
import logic.baa.C1616Xf;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Kn */
/* compiled from: a */
public class C0755Kn extends C6302akO {

    private C1616Xf dpI;

    public C0755Kn() {
    }

    public C0755Kn(C1616Xf xf) {
        this.dpI = xf;
    }

    public C1616Xf bcB() {
        return this.dpI;
    }

    public void readExternal(ObjectInput objectInput) {
        this.dpI = (C1616Xf) C0474GZ.dah.inputReadObject(objectInput);
    }

    public void writeExternal(ObjectOutput objectOutput) {
        C0474GZ.dah.serializeData(objectOutput, (Object) this.dpI);
    }
}
