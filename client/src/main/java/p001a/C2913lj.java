package p001a;

import game.network.message.externalizable.ObjectId;

import java.util.Iterator;

/* renamed from: a.lj */
/* compiled from: a */
public interface C2913lj {
    /* renamed from: a */
    C3582se mo15459a(ObjectId apo);

    /* renamed from: a */
    void mo15460a(C3582se seVar);

    /* renamed from: b */
    void mo15461b(C3582se seVar);

    /* renamed from: b */
    boolean mo15462b(ObjectId apo);

    /* renamed from: c */
    boolean mo15463c(C3582se seVar);

    void dispose();

    Iterator<C3582se> iterator();

    int size();
}
