package p001a;

import logic.swing.C0454GJ;
import logic.ui.item.C2830kk;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.tf */
/* compiled from: a */
class C3679tf implements C5912aco.C1865a {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ C2762jf.C2764b bkm;

    C3679tf(C2762jf.C2764b bVar) {
        this.bkm = bVar;
    }

    /* renamed from: gZ */
    public void mo4gZ() {
        C2762jf.this.ahE.mo23034dt(false);
        this.bkm.mo12691a(this.bkm.glY);
        for (Component component : C2762jf.this.ahE.buw()) {
            if (!component.isPinned()) {
                this.bkm.bOE();
                this.bkm.mo12692a(component, "[255..0] dur 50", C2830kk.asS, new C3680a(component));
            }
        }
    }

    /* renamed from: a.tf$a */
    class C3680a implements C0454GJ {
        private final /* synthetic */ Component iGf;

        C3680a(Component component) {
            this.iGf = component;
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            this.iGf.setVisible(false);
            C3679tf.this.bkm.bOG();
        }
    }
}
