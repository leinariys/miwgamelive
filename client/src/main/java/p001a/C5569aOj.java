package p001a;

import game.script.item.Item;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.util.Arrays;
import java.util.Collection;

/* renamed from: a.aOj  reason: case insensitive filesystem */
/* compiled from: a */
public class C5569aOj implements Transferable {
    private static final DataFlavor[] iyA = {DataFlavor.stringFlavor, C2125cH.f6026vP};
    private Item[] iyB;

    public C5569aOj(Item[] auqArr) {
        this.iyB = auqArr;
    }

    public Object getTransferData(DataFlavor dataFlavor) {
        if (dataFlavor.getRepresentationClass() == String.class) {
            return dhV();
        }
        if (dataFlavor.getRepresentationClass() == Collection.class) {
            return Arrays.asList(this.iyB);
        }
        throw new UnsupportedFlavorException(dataFlavor);
    }

    public DataFlavor[] getTransferDataFlavors() {
        return iyA;
    }

    public boolean isDataFlavorSupported(DataFlavor dataFlavor) {
        for (DataFlavor equals : iyA) {
            if (equals.equals(dataFlavor)) {
                return true;
            }
        }
        return false;
    }

    private String dhV() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.iyB.length; i++) {
            sb.append(this.iyB[i].bAP().mo19891ke().get());
            if (i != this.iyB.length - 1) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }
}
