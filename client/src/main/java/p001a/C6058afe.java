package p001a;

import game.network.message.WriterBits;

import java.io.DataOutput;

/* renamed from: a.afe  reason: case insensitive filesystem */
/* compiled from: a */
public class C6058afe implements WriterBits {
    public static final int[] emM = new int[32];

    static {
        int i = 0;
        int i2 = 0;
        while (i < 32) {
            emM[i] = i2;
            i++;
            i2 = (i2 << 1) | 1;
        }
    }

    public int emK = 0;
    public DataOutput fti;
    public int left;

    public C6058afe() {
    }

    public C6058afe(DataOutput dataOutput) {
        this.fti = dataOutput;
    }

    public void writeBits(int i, int i2) {
        if (this.emK + i < 8) {
            this.left = (this.left << i) | (emM[i] & i2);
            this.emK += i;
            return;
        }
        if (this.emK > 0) {
            int i3 = 8 - this.emK;
            i -= i3;
            this.fti.writeByte(((emM[i3] & (i2 >> i)) | (this.left << i3)) & 255);
        }
        while (i > 8) {
            i -= 8;
            this.fti.writeByte((i2 >> i) & 255);
        }
        this.left = emM[i] & i2;
        this.emK = i;
    }

    /* renamed from: d */
    public void writeBitLong(int i, long j) {
        if (this.emK + i < 8) {
            this.left = (this.left << i) | (((int) j) & emM[i]);
            this.emK += i;
            return;
        }
        if (this.emK > 0) {
            int i2 = 8 - this.emK;
            i -= i2;
            this.fti.writeByte(((emM[i2] & ((int) (j >> i))) | (this.left << i2)) & 255);
        }
        int i3 = (int) j;
        while (i > 8) {
            i -= 8;
            this.fti.writeByte((i3 >> i) & 255);
        }
        this.left = i3 & emM[i];
        this.emK = i;
    }

    public void finish() {
        if (this.emK > 0) {
            this.fti.writeByte((this.left << (8 - this.emK)) & 255);
            this.emK = 0;
        }
    }
}
