package p001a;

import logic.ui.item.C2830kk;

import javax.swing.*;

/* renamed from: a.MX */
/* compiled from: a */
class C0865MX implements C5912aco.C1865a {
    final /* synthetic */ C2762jf.C2763a dCS;

    C0865MX(C2762jf.C2763a aVar) {
        this.dCS = aVar;
    }

    /* renamed from: gZ */
    public void mo4gZ() {
        this.dCS.mo12691a(this.dCS.glY);
        for (int buv = C2762jf.this.ahE.buv() - 1; buv >= 0; buv--) {
            JComponent nn = C2762jf.this.ahE.mo23038nn(buv);
            if (nn.isPinned()) {
                this.dCS.bOE();
                int intValue = ((Integer) C2762jf.this.ahF.get(buv)).intValue();
                nn.setLocation(nn.getLocation());
                if (nn.getY() != intValue) {
                    this.dCS.mo12692a(nn, "[" + nn.getY() + ".." + intValue + "] dur " + "50" + " regular_in", C2830kk.asP, this.dCS.atu);
                } else {
                    this.dCS.atu.mo9a(nn);
                }
            }
        }
        int buv2 = C2762jf.this.ahE.buv() + 1;
        while (true) {
            int i = buv2;
            if (i >= C2762jf.this.ahE.bux()) {
                break;
            }
            JComponent nn2 = C2762jf.this.ahE.mo23038nn(i);
            if (nn2.isPinned()) {
                this.dCS.bOE();
                int intValue2 = ((Integer) C2762jf.this.ahF.get(i)).intValue();
                nn2.setLocation(nn2.getLocation());
                if (nn2.getY() != intValue2) {
                    this.dCS.mo12692a(nn2, "[" + nn2.getY() + ".." + intValue2 + "] dur " + "50" + " regular_in", C2830kk.asP, this.dCS.atu);
                } else {
                    this.dCS.atu.mo9a(nn2);
                }
            }
            buv2 = i + 1;
        }
        if (this.dCS.getCount() == 0) {
            this.dCS.glY.mo4gZ();
        }
    }
}
