package p001a;

import game.network.message.serializable.C2991mi;
import logic.res.html.MessageContainer;
import logic.ui.item.ListJ;

import java.util.*;

/* renamed from: a.awN  reason: case insensitive filesystem */
/* compiled from: a */
public class C6925awN<T> extends C0636JA<T> implements Set<T> {

    private ListJ<C2991mi<T>> bFH;
    private Set<T> eVA;

    public C6925awN(Set<T> set) {
        super(Collections.unmodifiableSet(set));
        this.eVA = set;
    }

    public C6925awN() {
        super(MessageContainer.dgI());
    }

    /* access modifiers changed from: protected */
    public final void anC() {
        if (!this.bFM) {
            this.dkv = MessageContainer.m16003v(this.eVA);
            this.bFM = true;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo2919a(C2991mi<T> miVar) {
        if (this.bFH == null) {
            this.bFH = MessageContainer.init();
        }
        this.bFH.add(miVar);
    }

    public ListJ<C2991mi<T>> anB() {
        return this.bFH;
    }

    public Set<T> bLG() {
        if (this.bFM) {
            return (Set) this.dkv;
        }
        return this.eVA;
    }

    public Iterator<T> iterator() {
        Iterator it;
        if (this.bFM) {
            it = new ArrayList(this.dkv).iterator();
        } else {
            it = this.dkv.iterator();
        }
        return new C2005a(it);
    }

    /* renamed from: a.awN$a */
    class C2005a implements Iterator<T> {
        Iterator<T> bDl;
        T bDm = null;
        int count = 0;
        private int expectedModCount;

        C2005a(Iterator it) {
            this.bDl = it;
            this.expectedModCount = C6925awN.this.anD();
        }

        public boolean hasNext() {
            amx();
            return this.bDl.hasNext();
        }

        public T next() {
            this.bDm = this.bDl.next();
            this.count++;
            amx();
            return this.bDm;
        }

        private void amx() {
            if (this.expectedModCount != C6925awN.this.anD()) {
                throw new ConcurrentModificationException();
            }
        }

        public void remove() {
            amx();
            if (!C6925awN.this.bFM) {
                ArrayList arrayList = new ArrayList(C6925awN.this.dkv.size() - this.count);
                while (this.bDl.hasNext()) {
                    arrayList.add(this.bDl.next());
                }
                this.bDl = arrayList.iterator();
                C6925awN.this.anC();
            }
            C6925awN.this.remove(this.bDm);
            this.expectedModCount = C6925awN.this.anD();
        }
    }
}
