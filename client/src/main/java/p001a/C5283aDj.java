package p001a;

/* renamed from: a.aDj  reason: case insensitive filesystem */
/* compiled from: a */
public class C5283aDj {
    private int[] hyt = new int[16];
    private int size;

    public void add(int i) {
        if (this.size == this.hyt.length) {
            expand();
        }
        int[] iArr = this.hyt;
        int i2 = this.size;
        this.size = i2 + 1;
        iArr[i2] = i;
    }

    private void expand() {
        int[] iArr = new int[(this.hyt.length << 1)];
        System.arraycopy(this.hyt, 0, iArr, 0, this.hyt.length);
        this.hyt = iArr;
    }

    public int remove(int i) {
        if (i >= this.size) {
            throw new IndexOutOfBoundsException();
        }
        int i2 = this.hyt[i];
        System.arraycopy(this.hyt, i + 1, this.hyt, i, (this.size - i) - 1);
        this.size--;
        return i2;
    }

    public int get(int i) {
        if (i < this.size) {
            return this.hyt[i];
        }
        throw new IndexOutOfBoundsException();
    }

    public void set(int i, int i2) {
        if (i >= this.size) {
            throw new IndexOutOfBoundsException();
        }
        this.hyt[i] = i2;
    }

    public int size() {
        return this.size;
    }
}
