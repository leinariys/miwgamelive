package p001a;

import game.io.IReadExternal;
import game.io.IWriteExternal;
import game.network.message.externalizable.C3328qK;
import logic.baa.C1616Xf;
import logic.data.mbean.C0677Jd;
import logic.res.html.C0029AO;

import java.lang.reflect.Field;

/* renamed from: a.Fl */
/* compiled from: a */
public class C0412Fl extends C3607sr {


    public C0412Fl(Class<?> cls, Field field, String str, Class<?>[] clsArr) {
        super(cls, field, str, clsArr);
    }

    /* renamed from: a */
    public Object mo2185a(C0677Jd jd, Object obj) {
        return new C1666YX(jd, (C3328qK) obj);
    }

    /* renamed from: c */
    public C0029AO mo2189c(C1616Xf xf) {
        return new C3328qK(xf, this);
    }

    /* renamed from: b */
    public Object mo2188b(C1616Xf xf) {
        return new C3328qK(xf, this);
    }

    /* renamed from: a */
    public Object mo2186a(C1616Xf xf, IReadExternal vm) {
        boolean gR = vm.mo6354gR("null");
        String property = System.getProperty("FIX_COLLECTIONS");
        if (property != null && property.equalsIgnoreCase("TRUE")) {
            return super.mo2186a(xf, vm);
        }
        if (gR) {
            return vm.mo6366hd("unknow");
        }
        C3328qK qKVar = new C3328qK(xf, this);
        qKVar.readExternal(vm);
        return qKVar;
    }

    /* renamed from: a */
    public void mo2187a(C1616Xf xf, IWriteExternal att, Object obj) {
        boolean z = obj == null;
        att.writeBoolean("null", z);
        if (z) {
            att.mo16273g("unknow", obj);
        } else {
            ((C3328qK) obj).writeExternal(att);
        }
    }
}
