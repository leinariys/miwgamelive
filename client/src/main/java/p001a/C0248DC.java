package p001a;

import game.geometry.Vec3d;
import game.geometry.aLH;
import logic.thred.C6615aqP;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.DC */
/* compiled from: a */
public class C0248DC implements C3286py {
    /* access modifiers changed from: private */
    public List<C0249a> list = new ArrayList();
    private C0837MD cIj = new C0837MD();

    /* renamed from: a */
    public aRR mo1225a(aLH alh, C6615aqP aqp) {
        C0249a aVar = new C0249a(alh, aqp);
        this.list.add(aVar);
        return aVar;
    }

    /* renamed from: a */
    public void mo1229a(aLH alh, C6178ahu ahu) {
        for (C0249a next : this.list) {
            if (next.hFY.mo9863e(alh) && !ahu.mo13625a(next)) {
                return;
            }
        }
    }

    /* renamed from: a */
    public void mo1228a(Vec3d ajr, Vec3d ajr2, C6178ahu ahu, List<C6615aqP> list2) {
    }

    /* renamed from: gZ */
    public void mo1231gZ() {
        List<C0249a> list2 = this.list;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < list2.size()) {
                C0249a aVar = list2.get(i2);
                int i3 = i2 + 1;
                while (true) {
                    int i4 = i3;
                    if (i4 >= list2.size()) {
                        break;
                    }
                    C0249a aVar2 = list2.get(i4);
                    C3102np aJ = this.cIj.mo3874aJ(aVar.f3320id, aVar2.f3320id);
                    if (aVar2.hFY.mo9863e(aVar.hFY)) {
                        if (aJ == null) {
                            this.cIj.mo3873a(new C6436ams(aVar, aVar2));
                        }
                    } else if (aJ != null) {
                        this.cIj.mo3876b(aJ);
                    }
                    i3 = i4 + 1;
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* renamed from: Vy */
    public C0250DD mo1223Vy() {
        return this.cIj;
    }

    /* renamed from: a */
    public void mo1226a(C1037PD pd) {
    }

    /* renamed from: Vz */
    public C0250DD mo1224Vz() {
        return null;
    }

    /* renamed from: VA */
    public C0250DD mo1222VA() {
        return null;
    }

    /* renamed from: a */
    public void mo1227a(Vec3d ajr, float f, C6178ahu ahu) {
    }

    public void dispose() {
    }

    /* renamed from: a.DC$a */
    class C0249a extends C5487aLf {
        /* access modifiers changed from: private */
        public final aLH hFY;

        public C0249a(aLH alh, C6615aqP aqp) {
            super(aqp);
            this.hFY = alh;
        }

        public void dispose() {
            C0248DC.this.list.remove(this);
        }

        /* renamed from: c */
        public void mo1234c(aLH alh) {
        }

        public boolean cip() {
            return true;
        }

        /* renamed from: b */
        public float mo1233b(aRR arr) {
            return 0.0f;
        }

        /* renamed from: F */
        public float mo1232F(Vec3d ajr) {
            return 0.0f;
        }
    }
}
