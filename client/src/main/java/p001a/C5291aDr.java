package p001a;

import gnu.trove.THashMap;
import logic.res.html.DataClassSerializer;

import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Map;

/* renamed from: a.aDr  reason: case insensitive filesystem */
/* compiled from: a */
public class C5291aDr<K, V> extends THashMap<K, V> implements Map<K, V> {
    public C5291aDr() {
    }

    public C5291aDr(Map<K, V> map) {
        super(map);
    }

    public C5291aDr(int i) {
        super(i);
    }

    /* renamed from: a */
    public void mo8518a(ObjectInput objectInput, int i, DataClassSerializer kd, DataClassSerializer kd2) {
        int readInt = objectInput.readInt();
        if (this._set.length != readInt) {
            this._set = new Object[readInt];
            this._values = new Object[readInt];
        }
        clear();
        Object[] objArr = this._set;
        Object[] objArr2 = this._values;
        int length = objArr.length;
        while (true) {
            int i2 = length - 1;
            if (length <= 0) {
                break;
            }
            objArr[i2] = FREE;
            objArr2[i2] = null;
            length = i2;
        }
        this._size = i;
        int i3 = 0;
        int i4 = i;
        while (i4 > 0) {
            int readInt2 = objectInput.readInt();
            if ((Integer.MIN_VALUE & readInt2) != 0) {
                objArr[readInt2 & Integer.MAX_VALUE] = REMOVED;
            } else {
                Object b = kd.inputReadObject(objectInput);
                Object b2 = kd2.inputReadObject(objectInput);
                objArr[readInt2] = b;
                objArr2[readInt2] = b2;
                i4--;
            }
            i3++;
        }
        int capacity = capacity();
        this._maxSize = Math.min(capacity - 1, (int) Math.floor((double) (((float) capacity) * this._loadFactor)));
        this._free = capacity - this._size;
    }

    /* renamed from: a */
    public void mo8519a(ObjectOutput objectOutput, DataClassSerializer kd, DataClassSerializer kd2) {
        Object[] objArr = this._set;
        Object[] objArr2 = this._values;
        objectOutput.writeInt(this._values.length);
        int length = objArr.length;
        while (true) {
            int i = length - 1;
            if (length > 0) {
                if (objArr[i] != FREE) {
                    if (objArr[i] == REMOVED) {
                        objectOutput.writeInt(Integer.MIN_VALUE | i);
                        length = i;
                    } else {
                        objectOutput.writeInt(i);
                        kd.serializeData(objectOutput, objArr[i]);
                        kd2.serializeData(objectOutput, objArr2[i]);
                    }
                }
                length = i;
            } else {
                return;
            }
        }
    }
}
