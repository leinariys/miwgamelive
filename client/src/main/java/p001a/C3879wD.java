package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Vector2fWrap;
import game.geometry.Vector4fWrap;
import logic.res.scene.C2036bO;
import logic.res.scene.C5347aFv;
import logic.res.scene.C6980axt;
import logic.res.scene.aMF;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.wD */
/* compiled from: a */
public class C3879wD {
    /* renamed from: a */
    public static <T extends C2036bO> List<T> m40491a(C2036bO bOVar, Class<T> cls) {
        ArrayList arrayList = new ArrayList();
        m40492a(bOVar, arrayList, cls);
        return arrayList;
    }

    /* renamed from: a */
    private static <T extends C2036bO> void m40492a(C2036bO bOVar, List<T> list, Class<T> cls) {
        if (bOVar != null) {
            if (cls.isInstance(bOVar)) {
                list.add(bOVar);
            } else if (bOVar instanceof C5347aFv) {
                m40492a(((C5347aFv) bOVar).biv(), list, cls);
            }
            for (C2036bO a : bOVar.mo15350Ix()) {
                m40492a(a, list, cls);
            }
        }
    }

    /* renamed from: b */
    public static <T extends C2036bO> List<T> m40493b(C2036bO bOVar, Class<T> cls) {
        ArrayList arrayList = new ArrayList();
        m40494b(bOVar, arrayList, cls);
        return arrayList;
    }

    /* renamed from: b */
    private static <T extends C2036bO> boolean m40494b(C2036bO bOVar, List<T> list, Class<T> cls) {
        boolean z;
        if (bOVar == null) {
            return false;
        }
        if (cls.isInstance(bOVar)) {
            list.add(bOVar);
            z = true;
        } else {
            if ((bOVar instanceof C5347aFv) && m40494b(((C5347aFv) bOVar).biv(), list, cls)) {
                ((C5347aFv) bOVar).mo8882e((C6980axt) null);
            }
            z = false;
        }
        for (C2036bO bOVar2 : bOVar.mo15350Ix()) {
            if (m40494b(bOVar2, list, cls)) {
                bOVar.mo13081c(bOVar2);
            }
        }
        return z;
    }

    /* renamed from: a */
    public static Vec3f m40490a(Vector2fWrap aka, Vector2fWrap aka2, aMF amf, float f) {
        Vec3f vec3f = new Vec3f();
        vec3f.x = aka.x / aka2.x;
        vec3f.y = aka.y / aka2.y;
        vec3f.z = f;
        Matrix4fWrap ajk = new Matrix4fWrap();
        amf.getTransform().mo14037i(ajk);
        Matrix4fWrap ajk2 = new Matrix4fWrap();
        Matrix4fWrap ajk3 = new Matrix4fWrap();
        Vector4fWrap ajf = new Vector4fWrap();
        Vector4fWrap ajf2 = new Vector4fWrap();
        ajf.x = (vec3f.x * 2.0f) - 1.0f;
        ajf.y = (vec3f.y * 2.0f) - 1.0f;
        ajf.z = (vec3f.z * 2.0f) - 1.0f;
        ajf.w = 1.0f;
        ajk2.mul(amf.getProjection(), ajk);
        ajk3.invert(ajk2);
        ajk3.transform(ajf, ajf2);
        if (ajf2.w == 0.0f) {
            return null;
        }
        vec3f.x = ajf2.x / ajf2.w;
        vec3f.y = ajf2.y / ajf2.w;
        vec3f.z = ajf2.z / ajf2.w;
        return vec3f;
    }
}
