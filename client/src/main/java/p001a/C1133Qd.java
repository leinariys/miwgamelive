package p001a;

import logic.baa.C1616Xf;
import logic.res.html.C2491fm;

import java.util.Iterator;
import java.util.SortedMap;
import java.util.TreeMap;

/* renamed from: a.Qd */
/* compiled from: a */
public class C1133Qd {
    /* renamed from: a */
    public static SortedMap<String, C1566Wy> m8961a(C5437aJh ajh, C1616Xf xf) {
        TreeMap treeMap = new TreeMap();
        for (int i = 0; i < xf.mo12U().length; i++) {
            C2491fm fmVar = xf.mo12U()[i];
            try {
                if (fmVar.isEditable()) {
                    if (fmVar.mo4756pB() || fmVar.mo4757pC() || fmVar.mo4758pD()) {
                        String displayName = fmVar.getDisplayName();
                        C1566Wy wy = (C1566Wy) treeMap.get(displayName);
                        if (wy == null) {
                            wy = new C1566Wy(ajh, xf, displayName);
                            treeMap.put(displayName, wy);
                        }
                        if (fmVar.mo4757pC()) {
                            wy.mo6635a(fmVar);
                        } else if (fmVar.mo4756pB()) {
                            wy.mo6648c(fmVar);
                        } else if (fmVar.mo4758pD()) {
                            wy.mo6636b(fmVar);
                        }
                    } else if (fmVar.mo4759pE() || fmVar.mo4761pG() || fmVar.mo4762pH() || fmVar.mo4763pI()) {
                        String displayName2 = fmVar.getDisplayName();
                        C1566Wy wy2 = (C1566Wy) treeMap.get(displayName2);
                        if (wy2 == null) {
                            wy2 = new C1566Wy(ajh, xf, displayName2);
                            treeMap.put(displayName2, wy2);
                        }
                        if (fmVar.mo4759pE()) {
                            wy2.mo6649d(fmVar);
                        } else if (fmVar.mo4762pH()) {
                            wy2.mo6650e(fmVar);
                        } else if (fmVar.mo4763pI()) {
                            wy2.mo6652f(fmVar);
                        } else if (fmVar.mo4761pG()) {
                            wy2.mo6653g(fmVar);
                        }
                    } else if (fmVar.mo4760pF()) {
                        String displayName3 = fmVar.getDisplayName();
                        C1566Wy wy3 = new C1566Wy(ajh, xf, displayName3);
                        treeMap.put(displayName3, wy3);
                        wy3.mo6658h(fmVar);
                    }
                }
            } catch (NullPointerException e) {
                System.out.println("[Sperry is going to check this later]\nMethod is null for iscript " + xf.getClass().getName() + ", " + i + ", length = " + xf.mo12U().length);
                e.printStackTrace();
            }
        }
        Iterator it = treeMap.keySet().iterator();
        while (it.hasNext()) {
            C1566Wy wy4 = (C1566Wy) treeMap.get((String) it.next());
            if (wy4.getIterator() == null && wy4.bDt() == null && wy4.bDA() == null) {
                it.remove();
            }
        }
        return treeMap;
    }
}
