package p001a;

import org.objectweb.asm.MethodAdapter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/* renamed from: a.aKi  reason: case insensitive filesystem */
/* compiled from: a */
public class C5464aKi extends MethodAdapter implements C2821kb, Opcodes {
    private final C6416amY cUO;
    private String desc;
    private String name;

    public C5464aKi(C6416amY amy, int i, String str, String str2, String str3, String[] strArr) {
        super((MethodVisitor) null);
        this.cUO = amy;
        this.mv = amy.ckg().visitMethod(i & -1281, str, str2, str3, strArr);
        this.name = str;
        this.desc = str2;
    }

    public void visitEnd() {
        this.mv.visitCode();
        if (this.desc.equals(C2821kb.ash)) {
            C0404Fd jv = this.cUO.mo14853jv(this.name);
            if (jv != null) {
                this.mv.visitFieldInsn(178, this.cUO.name, jv.cUQ, aqF.getDescriptor());
            } else {
                this.cUO.gci.mo4567a(this.cUO, (ayO) null, -1, "Illegal field reflection for unknow field %s", this.name);
                this.mv.visitInsn(1);
            }
        } else {
            C1554Wn wn = null;
            for (C1554Wn next : this.cUO.ckq()) {
                if (next.name.equals(this.name) || (next.name.startsWith(this.name) && C6423amf.m23962jr(String.valueOf(next.name) + next.desc).equals(this.name))) {
                    if (wn != null) {
                        this.cUO.gci.mo4567a(this.cUO, (ayO) null, -1, "Illegal method reflection for duplicated method name %s", this.name);
                        this.mv.visitInsn(1);
                        this.mv.visitInsn(176);
                        this.mv.visitMaxs(2, 1);
                        C5464aKi.super.visitEnd();
                        return;
                    }
                    wn = next;
                }
            }
            if (wn != null) {
                this.mv.visitFieldInsn(178, this.cUO.name, wn.cUQ, aqI.getDescriptor());
            } else {
                this.cUO.gci.mo4567a(this.cUO, (ayO) null, -1, "Illegal method reflection for unknow method %s", this.name);
                this.mv.visitInsn(1);
            }
        }
        this.mv.visitInsn(176);
        this.mv.visitMaxs(2, 1);
        C5464aKi.super.visitEnd();
    }
}
