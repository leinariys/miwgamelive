package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.camera.Camera;

/* renamed from: a.wE */
/* compiled from: a */
public class C3880wE extends C6258ajW {
    public final Vec3d distance = new Vec3d();
    private float bEa = 0.0f;
    private C0763Kt stack = C0763Kt.bcE();

    public C3880wE(Camera camera) {
        super(camera);
        this.distance.set(ScriptRuntime.NaN, ScriptRuntime.NaN, 100.0d);
    }

    public Vec3d getDistance() {
        return this.distance;
    }

    public void setDistance(Vec3d ajr) {
        if (ajr.lengthSquared() < 0.01d) {
            this.distance.set(ScriptRuntime.NaN, ScriptRuntime.NaN, 1.0d);
        }
        this.distance.mo9484aA(ajr);
    }

    /* renamed from: dV */
    private void m40495dV(float f) {
        this.bEa += 25.0f * f;
        if (this.bEa > 360.0f) {
            this.bEa = (this.bEa % 360.0f) + ((float) (((double) this.bEa) - Math.floor((double) this.bEa)));
        }
    }

    /* renamed from: a */
    public void mo3859a(float f, C1003Om om) {
        this.stack = this.stack.bcD();
        this.stack.bcI().push();
        this.stack.bcH().push();
        this.stack.bcN().push();
        this.stack.bcK().push();
        try {
            m40495dV(f);
            float f2 = this.bEa * 0.017453292f;
            double sin = Math.sin((double) f2);
            double sin2 = Math.sin((double) (f2 * 2.0f));
            TransformWrap IQ = om.mo4490IQ();
            TransformWrap IQ2 = this.fSF.mo2472kQ().mo4490IQ();
            om.bls().set(0.0f, 0.0f, 0.0f);
            om.blt().set(0.0f, 0.0f, 0.0f);
            om.blp().set(0.0f, 0.0f, 0.0f);
            om.bln().set(0.0f, 0.0f, 0.0f);
            om.blr().set(0.0f, 0.0f, 0.0f);
            om.blq().set(0.0f, 0.0f, 0.0f);
            om.blo().set(0.0f, 0.0f, 0.0f);
            om.blm().set(0.0f, 0.0f, 0.0f);
            Vec3d ajr = (Vec3d) this.stack.bcI().get();
            float length = (float) (this.distance.length() / 100.0d);
            ajr.x = this.distance.x + (((double) length) * sin);
            ajr.y = this.distance.y + (((double) length) * sin2);
            ajr.z = (((sin - sin2) * ((double) length)) / 2.0d) + this.distance.z;
            IQ2.mo17343b(ajr, IQ.position);
            Vec3d ajr2 = (Vec3d) this.stack.bcI().get();
            ajr2.mo9484aA(IQ.position);
            ajr2.sub(this.fSF.mo2472kQ().blk());
            ajr2.normalize();
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            Vec3f dfV = ajr2.dfV();
            vec3f.cross(IQ2.getVectorY(), dfV);
            vec3f2.cross(dfV, vec3f);
            IQ.setX(vec3f);
            IQ.setY(vec3f2);
            IQ.setZ(dfV);
            this.camera.setFovY(this.fSD);
        } finally {
            this.stack.bcI().pop();
            this.stack.bcH().pop();
            this.stack.bcN().pop();
            this.stack.bcK().pop();
        }
    }
}
