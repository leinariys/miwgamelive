package p001a;

import game.script.npc.NPC;
import logic.swing.C3940wz;
import logic.ui.Panel;
import taikodom.addon.IAddonProperties;
import taikodom.addon.tooltip.TooltipAddon;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.aEV */
/* compiled from: a */
public class aEV extends C3940wz {

    /* renamed from: DV */
    private Panel f2718DV;

    /* renamed from: kj */
    private IAddonProperties f2719kj;

    public aEV(IAddonProperties vWVar) {
        this.f2719kj = vWVar;
    }

    /* renamed from: b */
    public Component mo2675b(Component component) {
        if (this.f2718DV == null) {
            this.f2718DV = (Panel) this.f2719kj.bHv().mo16789b(TooltipAddon.class, "npcTooltip.xml");
        }
        NPC auf = (NPC) ((JComponent) component).getClientProperty("npc");
        this.f2718DV.mo4917cf("name").setText(auf.getName());
        this.f2718DV.mo4917cf("title").setText(auf.bTQ().get());
        return this.f2718DV;
    }
}
