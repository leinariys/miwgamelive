package p001a;

import game.script.item.Item;

import java.util.Comparator;

/* renamed from: a.ux */
/* compiled from: a */
class C3780ux implements Comparator<Item> {
    C3780ux() {
    }

    /* renamed from: a */
    public int compare(Item auq, Item auq2) {
        if (auq.getVolume() < auq2.getVolume()) {
            return -1;
        }
        if (auq.getVolume() > auq2.getVolume()) {
            return 1;
        }
        return Long.valueOf(auq.mo8317Ej()).compareTo(Long.valueOf(auq2.mo8317Ej()));
    }
}
