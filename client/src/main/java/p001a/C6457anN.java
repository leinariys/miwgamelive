package p001a;

import logic.C6245ajJ;
import logic.aaa.C5476aKu;
import logic.swing.C0454GJ;
import logic.swing.aDX;
import logic.ui.item.C2830kk;
import logic.ui.item.StackJ;
import taikodom.addon.IAddonProperties;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/* renamed from: a.anN  reason: case insensitive filesystem */
/* compiled from: a */
public class C6457anN {

    /* access modifiers changed from: private */

    /* renamed from: SZ */
    public aDX f4952SZ;
    private C6245ajJ del;
    private StackJ hUM;
    private JLabel hUN;
    private JButton hUO;
    private JPopupMenu hUP = new JPopupMenu();
    private String identifier;

    public C6457anN(IAddonProperties vWVar, C5476aKu aku) {
        this.del = vWVar.aVU();
        this.identifier = aku.getIdentifier();
    }

    public String getIdentifier() {
        return this.identifier;
    }

    /* renamed from: d */
    public void mo14956d(MouseEvent mouseEvent) {
        switch (this.hUP.getComponentCount()) {
            case 0:
                return;
            case 1:
                m24143b(this.hUP.getComponent(0));
                return;
            default:
                this.hUP.show(this.hUO, this.hUO.getX(), this.hUO.getY() - this.hUP.getHeight());
                this.hUP.show(this.hUO, this.hUO.getX(), this.hUO.getY() - this.hUP.getHeight());
                return;
        }
    }

    /* renamed from: d */
    public boolean mo14957d(C5476aKu aku) {
        for (C2768jj jjVar : this.hUP.getComponents()) {
            if ((jjVar instanceof C2768jj) && jjVar.mo19985a(aku)) {
                return false;
            }
        }
        C2768jj jjVar2 = new C2768jj(aku);
        this.hUP.add(jjVar2);
        jjVar2.addActionListener(new C1946a(jjVar2));
        this.hUN.setText(String.valueOf(this.hUP.getComponentCount()));
        bxV();
        return true;
    }

    /* renamed from: a */
    private void m24142a(C2768jj jjVar) {
        if (this.hUP.getComponentCount() > 0 && jjVar != null) {
            this.hUP.remove(jjVar);
            if (this.hUP.getComponentCount() <= 0) {
                this.del.mo13975h(new C5476aKu(C5476aKu.C1809a.REMOVE, this.identifier));
            } else {
                this.hUN.setText(String.valueOf(this.hUP.getComponentCount()));
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m24143b(C2768jj jjVar) {
        m24142a(jjVar);
        jjVar.execute();
    }

    /* renamed from: a */
    public void mo14955a(StackJ uHVar) {
        this.hUM = uHVar;
        this.hUN = uHVar.mo4917cf("label");
        this.hUO = uHVar.mo4913cb("button");
        this.hUO.addMouseListener(new C1947b());
    }

    public void dcO() {
        if (this.hUP.isVisible()) {
            this.hUP.setVisible(false);
        }
    }

    public void dispose() {
        this.hUM.destroy();
    }

    private void bxV() {
        this.hUO.setVisible(true);
        if (this.f4952SZ != null) {
            this.f4952SZ.kill();
        }
        this.f4952SZ = new aDX(this.hUO, "[0..255] dur 50; [255..0] dur 50; [0..255] dur 50; [255..0] dur 50; [0..255] dur 50;", C2830kk.asS, new C1948c());
    }

    /* renamed from: a.anN$a */
    class C1946a implements ActionListener {
        private final /* synthetic */ C2768jj geM;

        C1946a(C2768jj jjVar) {
            this.geM = jjVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C6457anN.this.m24143b(this.geM);
        }
    }

    /* renamed from: a.anN$b */
    /* compiled from: a */
    class C1947b extends MouseAdapter {
        C1947b() {
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            C6457anN.this.mo14956d(mouseEvent);
        }
    }

    /* renamed from: a.anN$c */
    /* compiled from: a */
    class C1948c implements C0454GJ {
        C1948c() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            C6457anN.this.f4952SZ = null;
        }
    }
}
