package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;

/* renamed from: a.aGB */
/* compiled from: a */
public interface aGB {
    Vec3d blk();

    Quat4fWrap bll();

    Vec3f blm();

    Vec3f bln();

    Vec3f blo();

    Vec3f blp();
}
