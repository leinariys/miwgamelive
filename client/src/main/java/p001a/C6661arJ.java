package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.network.message.externalizable.C6853aut;
import game.script.Actor;
import game.script.item.Weapon;
import game.script.space.Node;

/* renamed from: a.arJ  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6661arJ {
    /* renamed from: X */
    void mo2967X(Actor cr);

    void afE();

    C6853aut afN();

    Weapon afR();

    Weapon afT();

    Vec3f afV();

    float agD();

    Pawn agv();

    Vec3f agx();

    void ahE();

    Vec3f aip();

    Node azW();

    Quat4fWrap getOrientation();

    Vec3d getPosition();

    /* renamed from: hb */
    Controller mo2998hb();
}
