package p001a;

import logic.ui.Panel;
import logic.ui.item.Repeater;

import java.awt.*;

/* renamed from: a.Kq */
/* compiled from: a */
public class C0758Kq {
    public static final int inf = 250;
    private final Repeater<C0759a> bOK;

    public C0758Kq(Repeater<C0759a> tYVar) {
        this.bOK = tYVar;
        tYVar.mo22250a((Repeater.C3671a<C0759a>) new C0760b());
    }

    /* renamed from: a */
    public void mo3679a(C0759a aVar) {
        this.bOK.mo22248G(aVar);
    }

    /* renamed from: b */
    public void mo3680b(C0759a aVar) {
        this.bOK.mo22249H(aVar);
    }

    /* renamed from: a.Kq$a */
    public static abstract class C0759a {
        final String text;

        public C0759a(String str) {
            this.text = str;
        }

        /* renamed from: b */
        public abstract void mo749b(Panel aco);
    }

    /* renamed from: a.Kq$b */
    /* compiled from: a */
    class C0760b implements Repeater.C3671a<C0759a> {
        C0760b() {
        }

        /* renamed from: a */
        public void mo843a(C0759a aVar, Component component) {
            if (component instanceof Panel) {
                Panel aco = (Panel) component;
                aco.mo4917cf("status").setText(aVar.text);
                aVar.mo749b(aco);
            }
        }
    }
}
