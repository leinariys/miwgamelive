package p001a;

import game.network.message.externalizable.C5703aTn;

import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: a.ek */
/* compiled from: a */
public class C2390ek extends C5703aTn {
    private static AtomicInteger nextId = new AtomicInteger();

    /* renamed from: id */
    private int id;

    public C2390ek() {
    }

    public C2390ek(C0495Gr gr) {
        super(gr);
        this.id = nextId.incrementAndGet();
    }

    public int getId() {
        return this.id;
    }

    public void readExternal(ObjectInput objectInput) {
        this.id = objectInput.readInt();
        super.readExternal(objectInput);
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeInt(this.id);
        super.writeExternal(objectOutput);
    }
}
