package p001a;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.YI */
/* compiled from: a */
public class C1649YI implements Externalizable, Cloneable {
    private C1650a jfv;
    private float value;

    public C1649YI() {
        this.value = 0.0f;
        this.jfv = C1650a.ABSOLUTE;
    }

    public C1649YI(float f, C1650a aVar) {
        this.jfv = aVar;
        this.value = f;
    }

    /* renamed from: oI */
    public float mo6928oI(float f) {
        if (this.jfv == C1650a.PERCENT) {
            return (this.value * f) / 100.0f;
        }
        return this.value;
    }

    public String toString() {
        if (this.jfv == C1650a.PERCENT) {
            return String.valueOf(this.value) + "%";
        }
        return Float.toString(this.value);
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeFloat(this.value);
        objectOutput.writeByte(this.jfv.ordinal());
    }

    public void readExternal(ObjectInput objectInput) {
        this.value = objectInput.readFloat();
        this.jfv = C1650a.values()[objectInput.read()];
    }

    public Object clone() {
        return new C1649YI(this.value, this.jfv);
    }

    public float getValue() {
        return this.value;
    }

    public C1650a dDw() {
        return this.jfv;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C1649YI)) {
            return super.equals(obj);
        }
        C1649YI yi = (C1649YI) obj;
        return this.jfv == yi.jfv && this.value == yi.value;
    }

    /* renamed from: a.YI$a */
    public enum C1650a {
        PERCENT,
        ABSOLUTE
    }
}
