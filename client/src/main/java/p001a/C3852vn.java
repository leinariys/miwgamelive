package p001a;

import java.util.Collection;

/* renamed from: a.vn */
/* compiled from: a */
public class C3852vn<T> {
    C5595aPj<T> bzd = new C5595aPj<>();

    public void clear() {
        this.bzd.clear();
    }

    public T get(int i) {
        if (i >= this.bzd.size()) {
            return null;
        }
        return this.bzd.get(i);
    }

    public boolean isEmpty() {
        return this.bzd.isEmpty();
    }

    public void put(int i, T t) {
        this.bzd.set(i, t);
    }

    public Collection<T> values() {
        return this.bzd;
    }
}
