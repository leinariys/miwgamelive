package p001a;

import logic.aVH;
import logic.aaa.C1506WA;

import javax.swing.*;
import java.awt.event.ActionEvent;

/* renamed from: a.aqW  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C6622aqW extends AbstractAction implements aVH<C1506WA> {
    private boolean active;
    private String grg;
    private String grh;
    private String gri;
    private String grj;
    private String grk;

    public C6622aqW(String str, String str2, String str3) {
        this.active = true;
        this.grk = str;
        this.grg = str2;
        this.grj = str3;
    }

    public C6622aqW(String str, String str2) {
        this("", str, str2);
    }

    /* renamed from: a */
    public abstract void mo315a(Object... objArr);

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean z) {
        this.active = z;
    }

    public Object getValue(String str) {
        if ("ShortDescription".equals(str)) {
            return this.grg;
        }
        if ("CHAT_COMMAND".equals(str)) {
            return crI();
        }
        if ("cssClass".equals(str)) {
            return this.grk;
        }
        if ("isActive".equals(str)) {
            return Boolean.valueOf(isActive());
        }
        return C6622aqW.super.getValue(str);
    }

    private final void postActionEvent() {
        C5916acs.m20629am(this.grj, "");
    }

    /* renamed from: u */
    public final void mo15605u(Object... objArr) {
        mo315a(objArr);
        if (isActive()) {
            postActionEvent();
        }
    }

    /* renamed from: a */
    public void mo2004k(String str, C1506WA wa) {
        mo15605u(wa);
    }

    /* renamed from: b */
    public void mo2003f(String str, C1506WA wa) {
    }

    /* renamed from: jG */
    public void mo15601jG(String str) {
        mo15605u(str);
    }

    public String crH() {
        return this.grg;
    }

    public boolean isEnabled() {
        return true;
    }

    public String crI() {
        return this.grj;
    }

    public String crJ() {
        return this.grk;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent != null) {
            mo15605u(actionEvent.getActionCommand());
            return;
        }
        mo15605u(new Object[0]);
    }

    public String crK() {
        return this.grh;
    }

    /* renamed from: jH */
    public void mo15602jH(String str) {
        this.grh = str;
    }

    public String crL() {
        return this.gri;
    }

    /* renamed from: jI */
    public void mo15603jI(String str) {
        this.gri = str;
    }
}
