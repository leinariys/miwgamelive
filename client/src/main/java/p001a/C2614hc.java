package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import logic.bbb.*;
import logic.res.LoaderFileXML;
import logic.res.XmlNode;
import taikodom.addon.neo.preferences.GUIPrefAddon;
import taikodom.render.loader.provider.FilePath;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.hc */
/* compiled from: a */
public class C2614hc {
    public static void main(String[] strArr) {
    }

    /* renamed from: ay */
    public static C0512HH.C0513a[] m32761ay(String str) {
        return m32762b(C2606hU.getRootPathResource().concat(str));
    }

    /* renamed from: b */
    public static C0512HH.C0513a[] m32762b(FilePath ain) {
        InputStream openInputStream = ain.openInputStream();
        try {
            List<XmlNode> q = new LoaderFileXML().loadFileXml(openInputStream, "UTF-8").mo9057q("Shape", (String) null, (String) null);
            ArrayList arrayList = new ArrayList();
            for (XmlNode next : q) {
                XmlNode p = next.findNodeChildTagDepth("Transform", "type", "translate");
                XmlNode p2 = next.findNodeChildTagDepth("Transform", "type", "euler");
                XmlNode o = next.findNodeChild("Dimensions", (String) null, (String) null);
                Vec3f vec3f = new Vec3f(Float.parseFloat(p.getAttribute(GUIPrefAddon.C4817c.X)), Float.parseFloat(p.getAttribute(GUIPrefAddon.C4817c.Y)), Float.parseFloat(p.getAttribute("z")));
                Vec3f vec3f2 = new Vec3f(Float.parseFloat(p2.getAttribute(GUIPrefAddon.C4817c.X)), Float.parseFloat(p2.getAttribute(GUIPrefAddon.C4817c.Y)), Float.parseFloat(p2.getAttribute("z")));
                Matrix4fWrap ajk = new Matrix4fWrap();
                ajk.setTranslation(vec3f);
                ajk.rotateX(vec3f2.x);
                ajk.rotateY(vec3f2.y);
                ajk.rotateZ(vec3f2.z);
                C0512HH.C0513a aVar = new C0512HH.C0513a();
                if ("box".equals(next.getAttribute("type"))) {
                    Vec3f vec3f3 = new Vec3f(Float.parseFloat(o.getAttribute(GUIPrefAddon.C4817c.X)), Float.parseFloat(o.getAttribute(GUIPrefAddon.C4817c.Y)), Float.parseFloat(o.getAttribute("z")));
                    C3601so soVar = new C3601so();
                    soVar.mo22022A(vec3f3);
                    aVar.mo2520b((C4029yK) soVar);
                    aVar.setTransform(ajk);
                    arrayList.add(aVar);
                } else if ("sphere".equals(next.getAttribute("type"))) {
                    float parseFloat = Float.parseFloat(o.getAttribute("radius"));
                    C6348alI ali = new C6348alI();
                    ali.setRadius(parseFloat);
                    aVar.mo2520b((C4029yK) ali);
                    aVar.setTransform(ajk);
                    arrayList.add(aVar);
                } else if ("mesh".equals(next.getAttribute("type"))) {
                    String[] split = next.findNodeChildTagDepth("Vertices").getContent().trim().split("[, \\r\\n\\t]+");
                    Vec3f[] vec3fArr = new Vec3f[(split.length / 3)];
                    int i = 0;
                    for (int i2 = 0; i2 < vec3fArr.length; i2++) {
                        int i3 = i + 1;
                        int i4 = i3 + 1;
                        i = i4 + 1;
                        vec3fArr[i2] = new Vec3f(Float.parseFloat(split[i]), Float.parseFloat(split[i3]), Float.parseFloat(split[i4]));
                    }
                    String[] split2 = next.findNodeChildTagDepth("Indexes").getContent().trim().split("[, \\r\\n\\t]+");
                    System.out.println(split2.length);
                    int[] iArr = new int[((split2.length / 4) * 3)];
                    int i5 = 0;
                    int i6 = 0;
                    while (i5 < split2.length) {
                        int i7 = i6 + 1;
                        int i8 = i5 + 1;
                        iArr[i6] = Integer.parseInt(split2[i5]);
                        int i9 = i7 + 1;
                        int i10 = i8 + 1;
                        iArr[i7] = Integer.parseInt(split2[i8]);
                        i6 = i9 + 1;
                        iArr[i9] = Integer.parseInt(split2[i10]);
                        i5 = i10 + 1 + 1;
                    }
                    aVar.mo2520b((C4029yK) new C1188RY(vec3fArr, iArr));
                    aVar.setTransform(ajk);
                    arrayList.add(aVar);
                }
            }
            return (C0512HH.C0513a[]) arrayList.toArray(new C0512HH.C0513a[arrayList.size()]);
        } finally {
            try {
                openInputStream.close();
            } catch (Exception e) {
            }
        }
    }
}
