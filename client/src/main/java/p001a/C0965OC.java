package p001a;

import org.mozilla1.javascript.Context;
import taikodom.addon.console.ConsoleAddon;

/* renamed from: a.OC */
/* compiled from: a */
public class C0965OC extends ThreadLocal<Context> {
    final /* synthetic */ ConsoleAddon dMZ;

    public C0965OC(ConsoleAddon consoleAddon) {
        this.dMZ = consoleAddon;
    }

    /* access modifiers changed from: protected */
    /* renamed from: bin */
    public Context initialValue() {
        return this.dMZ.aRv();
    }
}
