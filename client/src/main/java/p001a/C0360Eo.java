package p001a;

import logic.thred.LogPrinter;
import logic.thred.ThreadWrapper;
import org.apache.commons.logging.Log;

/* renamed from: a.Eo */
/* compiled from: a */
public class C0360Eo extends ThreadWrapper implements C6296akI {
    private static final long cRQ = 1000000;
    private static final long cRR = 1000000000;
    private static final long cRS = 1000;
    private static final Log logger = LogPrinter.m10275K(C0360Eo.class);
    private final float aGM;
    private final long cRT;
    private C6296akI.C1925a cRU;
    private boolean cRV;
    private C0361a cRW;
    private int cRX;
    private boolean disposed;

    public C0360Eo(String str, float f) {
        this(str, f, 0.0f);
    }

    public C0360Eo(String str, float f, C6296akI.C1925a aVar) {
        this(str, f, 0.0f, aVar);
    }

    public C0360Eo(String str, float f, float f2) {
        this(str, f, f2, (C6296akI.C1925a) null);
    }

    public C0360Eo(String str, float f, float f2, int i) {
        this(str, f, f2, (C6296akI.C1925a) null, i);
    }

    public C0360Eo(String str, float f, float f2, C6296akI.C1925a aVar) {
        this(str, f, f2, aVar, 5);
    }

    public C0360Eo(String str, float f, float f2, C6296akI.C1925a aVar, int i) {
        super(str);
        this.disposed = false;
        this.aGM = f;
        this.cRT = Math.max(1, (long) (1000.0f * f2));
        setDaemon(true);
        this.cRV = false;
        this.cRU = aVar;
        this.cRX = i;
    }

    public void run() {
        try {
            long nanoTime = System.nanoTime();
            while (!this.disposed) {
                Thread.sleep(Math.max(((((long) (this.aGM * 1.0E9f)) + nanoTime) - System.nanoTime()) / cRQ, this.cRT));
                if (!this.disposed) {
                    long nanoTime2 = System.nanoTime();
                    float f = ((float) (nanoTime2 - nanoTime)) / 1.0E9f;
                    if (f > this.aGM * ((float) this.cRX)) {
                        f = this.aGM * ((float) this.cRX);
                        if (logger.isDebugEnabled()) {
                            logger.debug("Something is using too much CPU. " + getName() + " was not called often enought. Called after " + f + " (should be " + this.aGM + ")");
                        }
                    }
                    if (!this.cRV) {
                        if (this.cRW != null) {
                            this.cRW.bsB();
                        }
                        this.cRU.mo1968a(f, System.currentTimeMillis());
                        if (this.cRW != null) {
                            this.cRW.bsC();
                        }
                    }
                    nanoTime = nanoTime2;
                } else {
                    return;
                }
            }
        } catch (InterruptedException e) {
            throw e;
        } catch (RuntimeException e2) {
            if (e2.getClass().getName().endsWith(".RMIException")) {
                throw e2;
            }
            System.err.println("at StepperThread.run Ignoring " + e2);
            e2.printStackTrace();
        } catch (Error e3) {
            System.err.println("at StepperThread.run Ignoring " + e3);
            e3.printStackTrace();
        } catch (InterruptedException e4) {
            logger.warn(String.valueOf(getName()) + " ended");
        }
    }

    public void dispose() {
        this.disposed = true;
        interrupt();
        try {
            join();
        } catch (InterruptedException e) {
        }
    }

    /* renamed from: cG */
    public void mo1991cG(boolean z) {
        this.cRV = z;
    }

    /* renamed from: a */
    public void mo1989a(C0361a aVar) {
        this.cRW = aVar;
    }

    /* renamed from: c */
    public void mo1990c(C6296akI.C1925a aVar) {
        this.cRU = aVar;
    }

    /* renamed from: a.Eo$a */
    public interface C0361a {
        void bsB();

        void bsC();
    }
}
