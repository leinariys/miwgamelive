package p001a;

import game.script.ship.Ship;
import logic.aaa.C3551sV;
import taikodom.addon.hud.HudShipAimAddon;
import taikodom.addon.hud.visibility.HudVisibilityAddon;

import java.util.Collection;

/* renamed from: a.nh */
/* compiled from: a */
class C3093nh extends C6124ags<C3551sV> {
    final /* synthetic */ HudShipAimAddon aGF;

    public C3093nh(HudShipAimAddon hudShipAimAddon) {
        this.aGF = hudShipAimAddon;
    }

    /* renamed from: a */
    public boolean updateInfo(C3551sV sVVar) {
        if (!(this.aGF.bKo == null || this.aGF.bKp == null)) {
            Collection<HudVisibilityAddon.C4534d> b = this.aGF.f9929kj.aVU().mo13967b(HudVisibilityAddon.C4534d.class);
            if (b != null && !b.isEmpty() && !b.iterator().next().isVisible()) {
                this.aGF.apJ();
            } else if (sVVar.acv() == null || !(sVVar.acv() instanceof Ship)) {
                this.aGF.bKs = null;
                this.aGF.apI();
            } else {
                this.aGF.bKs = sVVar.acv();
                this.aGF.bKo.setTarget(this.aGF.bKs.cHg());
                if (!this.aGF.f9929kj.alb().cNM()) {
                    this.aGF.apK();
                }
            }
        }
        return false;
    }
}
