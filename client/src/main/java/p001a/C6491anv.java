package p001a;

import game.geometry.Vec3d;
import game.script.Character;
import game.script.ai.npc.AIController;
import game.script.ship.Ship;

/* renamed from: a.anv  reason: case insensitive filesystem */
/* compiled from: a */
public class C6491anv {
    /* renamed from: a */
    public static boolean m24341a(Controller jx, Vec3d ajr, float f) {
        if (!(jx instanceof aJO)) {
            return false;
        }
        aJO ajo = (aJO) jx;
        ajo.mo4610h(ajr);
        ajo.setRadius(f);
        return true;
    }

    /* renamed from: a */
    public static boolean m24343a(Controller jx, Ship fAVar) {
        if (fAVar == null || !(jx instanceof C6534aom)) {
            return false;
        }
        ((C6534aom) jx).mo4588J(fAVar);
        return true;
    }

    /* renamed from: a */
    public static boolean m24342a(Controller jx, Character acx) {
        if (jx instanceof C6188aiE) {
            ((C6188aiE) jx).mo4589L(acx);
            return true;
        } else if (!(jx instanceof C6534aom)) {
            return false;
        } else {
            ((C6534aom) jx).mo4588J(acx.bQx());
            return true;
        }
    }

    /* renamed from: f */
    public static boolean m24344f(Controller jx) {
        if (!(jx instanceof AIController)) {
            return false;
        }
        ((AIController) jx).start();
        return true;
    }

    /* renamed from: g */
    public static boolean m24345g(Controller jx) {
        if (!(jx instanceof AIController)) {
            return false;
        }
        ((AIController) jx).stop();
        return true;
    }
}
