package p001a;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.script.Actor;
import logic.render.IEngineGraphics;
import taikodom.render.scene.RLine;
import taikodom.render.textures.Material;

import java.util.HashMap;
import java.util.Map;

/* renamed from: a.akG  reason: case insensitive filesystem */
/* compiled from: a */
public class C6294akG {
    public static final Color fTX = new Color(1.0f, 0.0f, 0.0f, 1.0f);
    public static final Color fTY = new Color(0.0f, 0.0f, 1.0f, 1.0f);
    public static final Color fTZ = new Color(0.0f, 1.0f, 0.0f, 1.0f);
    public static final Color fUa = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    public static final Color fUb = new Color(1.0f, 1.0f, 0.0f, 1.0f);
    private static Map<String, RLine> fUc;
    private static IEngineGraphics fcy;
    private static Material material;

    private static void cgv() {
        if (fUc == null) {
            fUc = new HashMap();
        }
    }

    /* renamed from: jc */
    private static RLine m23134jc(String str) {
        cgv();
        if (fUc.containsKey(str)) {
            return fUc.get(str);
        }
        RLine rLine = new RLine();
        rLine.resetLine();
        fUc.put(str, rLine);
        if (fcy == null) {
            return rLine;
        }
        rLine.setMaterial(material);
        fcy.adZ().addChild(rLine);
        return rLine;
    }

    /* renamed from: c */
    public static void m23133c(IEngineGraphics abd) {
    }

    public static boolean cgw() {
        return fcy != null;
    }

    public static void cgx() {
    }

    /* renamed from: a */
    public static void m23130a(String str, Color color, float f) {
    }

    /* renamed from: a */
    public static void m23131a(String str, Vec3f vec3f, Vec3f vec3f2) {
    }

    /* renamed from: b */
    public static void m23132b(Actor cr, Vec3f vec3f) {
        m23128a(cr.getName(), cr, vec3f);
    }

    /* renamed from: a */
    public static void m23128a(String str, Actor cr, Vec3f vec3f) {
    }

    /* renamed from: a */
    public static void m23129a(String str, Actor cr, Vec3f vec3f, Vec3f vec3f2) {
    }

    /* renamed from: jd */
    public static void m23135jd(String str) {
    }

    public static void cgy() {
        cgv();
    }

    /* renamed from: je */
    public static void m23136je(String str) {
    }
}
