package p001a;

import logic.abc.C0802Lc;
import logic.abc.C6651aqz;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.vE */
/* compiled from: a */
public class C3806vE extends C3808vG {

    /* renamed from: Qm */
    public static final C6548apA f9395Qm = new C5249aCb();
    public static final C6548apA bzA = new C5248aCa();

    /* renamed from: jR */
    static final /* synthetic */ boolean f9396jR;

    static {
        boolean z;
        if (!C3806vE.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        f9396jR = z;
    }

    private final List<C3808vG> bzy = new ArrayList();
    private boolean bzz;

    public C3806vE(C1034PA pa, ayY ayy, ayY ayy2, boolean z) {
        super(pa);
        ayY ayy3;
        this.bzz = z;
        if (z) {
            ayy3 = ayy2;
        } else {
            ayy3 = ayy;
        }
        ayy = !z ? ayy2 : ayy;
        if (f9396jR || ayy3.cEZ().isCompound()) {
            C6651aqz aqz = (C6651aqz) ayy3.cEZ();
            int crs = aqz.crs();
            for (int i = 0; i < crs; i++) {
                C0802Lc tI = aqz.mo15687tI(i);
                C0802Lc cEZ = ayy3.cEZ();
                ayy3.mo17012a(tI);
                this.bzy.add(pa.czU.mo7323a(ayy3, ayy));
                ayy3.mo17012a(cEZ);
            }
            return;
        }
        throw new AssertionError();
    }

    public void destroy() {
        int size = this.bzy.size();
        for (int i = 0; i < size; i++) {
            this.bzy.get(i).destroy();
        }
    }

    /* renamed from: a */
    public void mo8931a(ayY ayy, ayY ayy2, C5408aIe aie, C5362aGk agk) {
        this.stack.bcJ().push();
        try {
            ayY ayy3 = this.bzz ? ayy2 : ayy;
            if (!this.bzz) {
                ayy = ayy2;
            }
            if (f9396jR || ayy3.cEZ().isCompound()) {
                C6651aqz aqz = (C6651aqz) ayy3.cEZ();
                C3978xf xfVar = (C3978xf) this.stack.bcJ().get();
                C3978xf xfVar2 = (C3978xf) this.stack.bcJ().get();
                int size = this.bzy.size();
                for (int i = 0; i < size; i++) {
                    C0802Lc tI = aqz.mo15687tI(i);
                    xfVar2.mo22947a(ayy3.cFf());
                    C0802Lc cEZ = ayy3.cEZ();
                    C3978xf tJ = aqz.mo15688tJ(i);
                    xfVar.mo22947a(xfVar2);
                    xfVar.mo22954c(tJ);
                    ayy3.mo17040f(xfVar);
                    ayy3.mo17012a(tI);
                    this.bzy.get(i).mo8931a(ayy3, ayy, aie, agk);
                    ayy3.mo17012a(cEZ);
                    ayy3.mo17040f(xfVar2);
                }
                return;
            }
            throw new AssertionError();
        } finally {
            this.stack.bcJ().pop();
        }
    }

    /* renamed from: b */
    public float mo8932b(ayY ayy, ayY ayy2, C5408aIe aie, C5362aGk agk) {
        this.stack.bcJ().push();
        try {
            ayY ayy3 = this.bzz ? ayy2 : ayy;
            if (!this.bzz) {
                ayy = ayy2;
            }
            if (f9396jR || ayy3.cEZ().isCompound()) {
                C6651aqz aqz = (C6651aqz) ayy3.cEZ();
                C3978xf xfVar = (C3978xf) this.stack.bcJ().get();
                C3978xf xfVar2 = (C3978xf) this.stack.bcJ().get();
                float f = 1.0f;
                int size = this.bzy.size();
                int i = 0;
                while (i < size) {
                    C0802Lc tI = aqz.mo15687tI(i);
                    xfVar2.mo22947a(ayy3.cFf());
                    C0802Lc cEZ = ayy3.cEZ();
                    C3978xf tJ = aqz.mo15688tJ(i);
                    xfVar.mo22947a(xfVar2);
                    xfVar.mo22954c(tJ);
                    ayy3.mo17040f(xfVar);
                    ayy3.mo17012a(tI);
                    float b = this.bzy.get(i).mo8932b(ayy3, ayy, aie, agk);
                    if (b >= f) {
                        b = f;
                    }
                    ayy3.mo17012a(cEZ);
                    ayy3.mo17040f(xfVar2);
                    i++;
                    f = b;
                }
                return f;
            }
            throw new AssertionError();
        } finally {
            this.stack.bcJ().pop();
        }
    }
}
