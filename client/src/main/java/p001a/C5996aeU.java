package p001a;

import java.util.Comparator;

/* renamed from: a.aeU  reason: case insensitive filesystem */
/* compiled from: a */
class C5996aeU implements Comparator {
    C5996aeU() {
    }

    public int compare(Object obj, Object obj2) {
        int length = obj instanceof C0223Co.C0224a ? ((C0223Co.C0224a) obj).value : ((float[]) obj).length;
        int length2 = obj2 instanceof C0223Co.C0224a ? ((C0223Co.C0224a) obj2).value : ((float[]) obj2).length;
        if (length > length2) {
            return 1;
        }
        return length < length2 ? -1 : 0;
    }
}
