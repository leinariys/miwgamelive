package p001a;

import game.script.Character;
import org.objectweb.asm.MethodVisitor;

/* renamed from: a.amf  reason: case insensitive filesystem */
/* compiled from: a */
public class C6423amf {
    static final char[] fZN = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /* renamed from: c */
    public static void m23960c(MethodVisitor methodVisitor, int i) {
        if (i <= 5) {
            methodVisitor.visitInsn(i + 3);
        } else if (i < 127) {
            methodVisitor.visitIntInsn(16, i);
        } else if (i < 32768) {
            methodVisitor.visitIntInsn(17, i);
        } else {
            methodVisitor.visitLdcInsn(Integer.valueOf(i));
        }
    }

    /* renamed from: jq */
    public static String m23961jq(String str) {
        StringBuilder sb = new StringBuilder();
        int length = str.length();
        boolean z = false;
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (!Character.isLetterOrDigit(charAt)) {
                z = true;
                if (charAt == '_') {
                    sb.append('_');
                    sb.append('_');
                } else {
                    sb.append('_');
                    sb.append(fZN[(charAt >> 12) & 15]);
                    sb.append(fZN[(charAt >> 8) & 15]);
                    sb.append(fZN[(charAt >> 4) & 15]);
                    sb.append(fZN[charAt & 15]);
                }
            } else {
                sb.append(charAt);
            }
        }
        if (z) {
            return sb.toString();
        }
        return str.toString();
    }

    /* renamed from: jr */
    public static String m23962jr(String str) {
        StringBuilder sb = new StringBuilder();
        int length = str.length();
        boolean z = false;
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (!Character.isLetterOrDigit(charAt)) {
                z = true;
                sb.append('_');
            } else {
                sb.append(charAt);
            }
        }
        if (z) {
            return sb.toString();
        }
        return str.toString();
    }
}
