package p001a;

import com.hoplon.geometry.Vec3f;

import javax.vecmath.Tuple3f;

/* renamed from: a.sM */
/* compiled from: a */
public abstract class C3541sM implements aTC {
    public final Vec3f biw = new Vec3f();
    public final Vec3f bix = new Vec3f();
    public final C0763Kt stack = C0763Kt.bcE();
    /* renamed from: r */
    public float f9100r;

    public C3541sM(Vec3f vec3f, Vec3f vec3f2) {
        this.biw.set(vec3f);
        this.bix.set(vec3f2);
        this.f9100r = 1.0f;
    }

    /* renamed from: a */
    public abstract float mo17724a(Vec3f vec3f, float f, int i, int i2);

    /* renamed from: a */
    public void mo820a(Vec3f[] vec3fArr, int i, int i2) {
        this.stack.bcH().push();
        try {
            Tuple3f tuple3f = vec3fArr[0];
            Tuple3f tuple3f2 = vec3fArr[1];
            Tuple3f tuple3f3 = vec3fArr[2];
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            vec3f.sub(tuple3f2, tuple3f);
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            vec3f2.sub(tuple3f3, tuple3f);
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            vec3f3.cross(vec3f, vec3f2);
            float dot = tuple3f.dot(vec3f3);
            float dot2 = vec3f3.dot(this.biw) - dot;
            float dot3 = vec3f3.dot(this.bix) - dot;
            if (dot2 * dot3 < 0.0f) {
                float f = dot2 / (dot2 - dot3);
                if (f < this.f9100r) {
                    float lengthSquared = vec3f3.lengthSquared() * -1.0E-4f;
                    Vec3f vec3f4 = new Vec3f();
                    C0647JL.m5591a(vec3f4, this.biw, this.bix, f);
                    Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                    vec3f5.sub(tuple3f, vec3f4);
                    Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
                    vec3f6.sub(tuple3f2, vec3f4);
                    Vec3f vec3f7 = (Vec3f) this.stack.bcH().get();
                    vec3f7.cross(vec3f5, vec3f6);
                    if (vec3f7.dot(vec3f3) >= lengthSquared) {
                        Vec3f vec3f8 = (Vec3f) this.stack.bcH().get();
                        vec3f8.sub(tuple3f3, vec3f4);
                        Vec3f vec3f9 = (Vec3f) this.stack.bcH().get();
                        vec3f9.cross(vec3f6, vec3f8);
                        if (vec3f9.dot(vec3f3) >= lengthSquared) {
                            Vec3f vec3f10 = (Vec3f) this.stack.bcH().get();
                            vec3f10.cross(vec3f8, vec3f5);
                            if (vec3f10.dot(vec3f3) >= lengthSquared) {
                                if (dot2 > 0.0f) {
                                    this.f9100r = mo17724a(vec3f3, f, i, i2);
                                } else {
                                    Vec3f vec3f11 = (Vec3f) this.stack.bcH().get();
                                    vec3f11.negate(vec3f3);
                                    this.f9100r = mo17724a(vec3f11, f, i, i2);
                                }
                            }
                        }
                    }
                }
                this.stack.bcH().pop();
            }
        } finally {
            this.stack.bcH().pop();
        }
    }
}
