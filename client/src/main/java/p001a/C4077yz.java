package p001a;

/* renamed from: a.yz */
/* compiled from: a */
public class C4077yz {
    private long bLR;
    private String host;
    private String name;
    private String path;
    private String value;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public C4077yz(String str, String str2, String str3, String str4, String str5) {
        this(str, str2, str3, str4, 0);
        long j = 0;
        try {
            j = Long.parseLong(str5);
        } catch (NumberFormatException e) {
        }
        m41480ds(j);
    }

    public C4077yz(String str, String str2, String str3, String str4, long j) {
        this.host = str;
        this.path = str2;
        this.name = str3;
        this.value = str4;
        this.bLR = j;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String str) {
        this.host = str;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String str) {
        this.path = str;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String str) {
        this.value = str;
    }

    /* renamed from: ds */
    private void m41480ds(long j) {
        this.bLR = j;
    }

    public long apY() {
        return this.bLR;
    }

    public String toString() {
        return "Host: " + this.host + "\n" + "path: " + this.path + "\n" + "name: " + this.name + "\n" + "value: " + this.value + "\n" + "expiry: " + this.bLR;
    }
}
