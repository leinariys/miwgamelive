package p001a;

/* renamed from: a.RL */
/* compiled from: a */
public class C1171RL {
    private long swigCPtr;

    public C1171RL(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C1171RL() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m9080a(C1171RL rl) {
        if (rl == null) {
            return 0;
        }
        return rl.swigCPtr;
    }
}
