package p001a;

import com.hoplon.geometry.Vec3f;
import taikodom.render.textures.DDSLoader;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.aAs  reason: case insensitive filesystem */
/* compiled from: a */
public class C5214aAs implements C3737uO {

    /* renamed from: jR */
    static final /* synthetic */ boolean f2343jR = (!C5214aAs.class.desiredAssertionStatus());
    private final List<C1734Zd> hdQ;
    private int hdR;
    private C3180oo hdS;
    private boolean hdT;

    public C5214aAs() {
        this(DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ, (C3180oo) null);
    }

    public C5214aAs(int i) {
        this(i, (C3180oo) null);
    }

    public C5214aAs(int i, C3180oo ooVar) {
        this.hdQ = new ArrayList();
        this.hdS = ooVar;
        if (ooVar == null) {
            this.hdS = new C3180oo();
            this.hdT = true;
        }
    }

    /* renamed from: a */
    private static boolean m12640a(C1734Zd zd, C1734Zd zd2) {
        return zd.eNi.x <= zd2.eNj.x && zd2.eNi.x <= zd.eNj.x && zd.eNi.y <= zd2.eNj.y && zd2.eNi.y <= zd.eNj.y && zd.eNi.z <= zd2.eNj.z && zd2.eNi.z <= zd.eNj.z;
    }

    /* renamed from: a */
    public C0783LL mo7767a(Vec3f vec3f, Vec3f vec3f2, C3655tK tKVar, Object obj, short s, short s2, C1701ZB zb) {
        if (f2343jR || (vec3f.x <= vec3f2.x && vec3f.y <= vec3f2.y && vec3f.z <= vec3f2.z)) {
            C1734Zd zd = new C1734Zd(vec3f, vec3f2, tKVar, obj, s, s2);
            zd.dxB = this.hdQ.size();
            this.hdQ.add(zd);
            return zd;
        }
        throw new AssertionError();
    }

    /* renamed from: c */
    public void mo7771c(C0783LL ll, C1701ZB zb) {
        this.hdQ.remove(ll);
        this.hdS.mo21061a(ll, zb);
    }

    /* renamed from: a */
    public void mo7768a(C0783LL ll, Vec3f vec3f, Vec3f vec3f2, C1701ZB zb) {
        C1734Zd zd = (C1734Zd) ll;
        zd.eNi.set(vec3f);
        zd.eNj.set(vec3f2);
    }

    /* renamed from: a */
    public void mo7769a(C1701ZB zb) {
        for (int i = 0; i < this.hdQ.size(); i++) {
            C1734Zd zd = this.hdQ.get(i);
            for (int i2 = 0; i2 < this.hdQ.size(); i2++) {
                C1734Zd zd2 = this.hdQ.get(i2);
                if (zd != zd2) {
                    if (m12640a(zd, zd2)) {
                        if (this.hdS.mo21067c(zd, zd2) == null) {
                            this.hdS.mo21059a((C0783LL) zd, (C0783LL) zd2);
                        }
                    } else if (this.hdS.mo21067c(zd, zd2) != null) {
                        this.hdS.mo21060a(zd, zd2, zb);
                    }
                }
            }
        }
    }

    public C3180oo ajX() {
        return this.hdS;
    }
}
