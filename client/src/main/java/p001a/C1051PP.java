package p001a;

import java.io.FilterOutputStream;
import java.io.OutputStream;

/* renamed from: a.PP */
/* compiled from: a */
public class C1051PP extends FilterOutputStream {
    private boolean dSo;

    public C1051PP(OutputStream outputStream) {
        super(outputStream);
    }

    public void write(int i) {
        if (i == 13) {
            if (this.dSo) {
                super.write(10);
            }
            super.write(i);
            this.dSo = true;
        } else if (i == 10) {
            if (!this.dSo) {
                super.write(13);
            }
            super.write(i);
            this.dSo = false;
        } else {
            if (this.dSo) {
                super.write(10);
                this.dSo = false;
            }
            super.write(i);
        }
    }
}
