package p001a;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.Ot */
/* compiled from: a */
public @interface C1020Ot {

    /* renamed from: Ur */
    long mo4553Ur() default 5000;

    C1021a drz() default C1021a.IMMEDIATE;

    /* renamed from: a.Ot$a */
    public enum C1021a {
        NOT_PERSISTED,
        TIME_BASED,
        IMMEDIATE
    }
}
