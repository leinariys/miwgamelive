package p001a;

import game.network.IReadBits;

import java.io.DataInput;
import java.io.DataInputStream;

/* renamed from: a.apy  reason: case insensitive filesystem */
/* compiled from: a */
public class C6598apy implements DataInput {
    public IReadBits gmJ;

    public C6598apy(IReadBits fVar) {
        this.gmJ = fVar;
    }

    public boolean readBoolean() {
        return this.gmJ.readBits(1) != 0;
    }

    public byte readByte() {
        return (byte) this.gmJ.readBits(8);
    }

    public char readChar() {
        return (char) this.gmJ.readBits(16);
    }

    public double readDouble() {
        return Double.longBitsToDouble(readLong());
    }

    public float readFloat() {
        return Float.intBitsToFloat(readInt());
    }

    public void readFully(byte[] bArr) {
        int i = 0;
        int length = bArr.length;
        while (true) {
            length--;
            if (length >= 0) {
                bArr[i] = (byte) this.gmJ.readBits(8);
                i++;
            } else {
                return;
            }
        }
    }

    public void readFully(byte[] bArr, int i, int i2) {
        while (true) {
            i2--;
            if (i2 >= 0) {
                bArr[i] = (byte) this.gmJ.readBits(8);
                i++;
            } else {
                return;
            }
        }
    }

    public int readInt() {
        switch (this.gmJ.readBits(2)) {
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                if (this.gmJ.readBits(1) == 0) {
                    int a = this.gmJ.readBits(4);
                    if ((a & 8) != 0) {
                        return a | -16;
                    }
                    return a;
                }
                int a2 = this.gmJ.readBits(8);
                if ((a2 & 128) != 0) {
                    return a2 | -256;
                }
                return a2;
            default:
                switch (this.gmJ.readBits(2)) {
                    case 0:
                        int a3 = this.gmJ.readBits(12);
                        if ((a3 & 2048) != 0) {
                            return a3 | -4096;
                        }
                        return a3;
                    case 1:
                        int a4 = this.gmJ.readBits(16);
                        if ((32768 & a4) != 0) {
                            return a4 | -65536;
                        }
                        return a4;
                    case 2:
                        int a5 = this.gmJ.readBits(24);
                        if ((8388608 & a5) != 0) {
                            return a5 | -16777216;
                        }
                        return a5;
                    default:
                        return this.gmJ.readBits(32);
                }
        }
    }

    @Deprecated
    public String readLine() {
        throw new UnsupportedOperationException();
    }

    public long readLong() {
        return (((long) readInt()) << 32) | ((long) readInt());
    }

    public short readShort() {
        return (short) readInt();
    }

    public String readUTF() {
        return DataInputStream.readUTF(this);
    }

    public int readUnsignedByte() {
        return readByte() & 255;
    }

    public int readUnsignedShort() {
        return readShort() & 65535;
    }

    public int skipBytes(int i) {
        this.gmJ.readBits(i * 8);
        return i;
    }
}
