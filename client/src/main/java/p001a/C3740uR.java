package p001a;

import game.script.item.ItemType;
import game.script.item.ItemTypeCategory;
import game.script.newmarket.CommercialOrderInfo;
import logic.swing.C5378aHa;
import logic.ui.Panel;
import logic.ui.item.C5923acz;
import logic.ui.item.aUR;
import taikodom.addon.IAddonProperties;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.util.List;
import java.util.*;

/* renamed from: a.uR */
/* compiled from: a */
public class C3740uR {
    /* access modifiers changed from: private */
    public String ejz;
    /* renamed from: kj */
    public IAddonProperties f9359kj;
    /* access modifiers changed from: private */
    private C5519aMl ejA = new C3747uU(this);

    public C3740uR(IAddonProperties vWVar) {
        this.f9359kj = vWVar;
    }

    /* renamed from: a */
    public void mo22382a(C5923acz acz, List<? extends CommercialOrderInfo> list) {
        m40019a((Map<ItemTypeCategory, Set<ItemTypeCategory>>) new HashMap(), list);
        mo22383a(list, acz);
        acz.expandPath(acz.bOY());
    }

    /* renamed from: a */
    private void m40019a(Map<ItemTypeCategory, Set<ItemTypeCategory>> map, List<? extends CommercialOrderInfo> list) {
        for (CommercialOrderInfo eP : list) {
            ItemTypeCategory HC = eP.mo8620eP().mo19866HC();
            if (HC != null) {
                ItemTypeCategory bJS = HC.bJS();
                if (!map.containsKey(bJS)) {
                    map.put(bJS, new HashSet());
                }
                map.get(bJS).add(HC);
            }
        }
    }

    /* renamed from: a */
    public void mo22383a(List<? extends CommercialOrderInfo> list, C5923acz acz) {
        C6964axd axd = new C6964axd("hidden root", new C3743c());
        TreePath treePath = new TreePath(axd);
        acz.setModel(new C5518aMk(axd, this.ejA));
        ArrayList<ItemType> arrayList = new ArrayList<>();
        for (CommercialOrderInfo eP : list) {
            arrayList.add(eP.mo8620eP());
        }
        HashMap hashMap = new HashMap();
        for (ItemType jCVar : arrayList) {
            ItemTypeCategory HC = jCVar.mo19866HC();
            if (HC != null) {
                ItemTypeCategory bJS = HC.bJS();
                if (!hashMap.containsKey(bJS)) {
                    C6964axd axd2 = new C6964axd(bJS, this.ejA);
                    hashMap.put(bJS, axd2);
                    axd.add(axd2);
                }
                if (!hashMap.containsKey(HC)) {
                    C6964axd axd3 = new C6964axd(HC, this.ejA);
                    hashMap.put(HC, axd3);
                    ((DefaultMutableTreeNode) hashMap.get(bJS)).add(axd3);
                }
                ((DefaultMutableTreeNode) hashMap.get(HC)).add(new C6964axd(jCVar, this.ejA));
            }
        }
        acz.expandPath(treePath);
    }

    /* renamed from: a */
    public void mo22381a(C5923acz acz) {
        acz.setCellRenderer(new C3742b());
    }

    /* renamed from: b */
    public void mo22384b(C5923acz acz) {
        acz.setCellRenderer(new C3741a());
    }

    /* renamed from: a.uR$c */
    /* compiled from: a */
    class C3743c extends C5519aMl {
        C3743c() {
        }

        /* renamed from: a */
        public boolean mo10165a(Object obj) {
            return false;
        }

        /* renamed from: z */
        public boolean mo10166z() {
            return false;
        }
    }

    /* renamed from: a.uR$b */
    /* compiled from: a */
    class C3742b extends C0037AU {
        C3742b() {
        }

        /* renamed from: a */
        public Component mo307a(aUR aur, Object obj, boolean z, boolean z2, boolean z3, int i, boolean z4) {
            String str;
            Image image;
            Panel nR;
            JLabel jLabel;
            JLabel jLabel2;
            Object userObject = ((DefaultMutableTreeNode) obj).getUserObject();
            if (userObject instanceof ItemType) {
                ItemType jCVar = (ItemType) userObject;
                str = jCVar.mo19891ke().get();
                image = C3740uR.this.f9359kj.bHv().adz().getImage(String.valueOf(C5378aHa.ITEMS.getPath()) + jCVar.mo12100sK().getHandle());
            } else if (userObject instanceof ItemTypeCategory) {
                ItemTypeCategory aai = (ItemTypeCategory) userObject;
                str = aai.mo12246ke().get();
                image = C3740uR.this.f9359kj.bHv().adz().getImage(String.valueOf(C5378aHa.ICONOGRAPHY.getPath()) + aai.mo12100sK().getHandle());
            } else {
                str = null;
                image = null;
            }
            if (z3) {
                nR = aur.mo11631nR("buyItemsTreeLeaf");
                JLabel cf = nR.mo4917cf("buyItemsTreeNameLeaf");
                jLabel = cf;
                jLabel2 = cf;
            } else if (z2) {
                nR = aur.mo11631nR("buyItemsTreeParentExpanded");
                JLabel cf2 = nR.mo4917cf("buyItemsTreeNameParentExpanded");
                jLabel = cf2;
                jLabel2 = cf2;
            } else {
                nR = aur.mo11631nR("buyItemsTreeParent");
                JLabel cf3 = nR.mo4917cf("buyItemsTreeNameParent");
                jLabel = cf3;
                jLabel2 = cf3;
            }
            jLabel2.setText(str);
            if (image != null) {
                if (userObject instanceof ItemType) {
                    jLabel.setIcon(new C2539gY(image));
                } else {
                    jLabel.setIcon(new ImageIcon(image));
                }
            }
            return nR;
        }
    }

    /* renamed from: a.uR$a */
    class C3741a extends C0037AU {
        C3741a() {
        }

        /* renamed from: a */
        public Component mo307a(aUR aur, Object obj, boolean z, boolean z2, boolean z3, int i, boolean z4) {
            String str;
            Image image;
            Panel nR;
            JLabel jLabel;
            JLabel jLabel2;
            Object userObject = ((DefaultMutableTreeNode) obj).getUserObject();
            if (userObject instanceof ItemType) {
                ItemType jCVar = (ItemType) userObject;
                str = jCVar.mo19891ke().get();
                image = C3740uR.this.f9359kj.bHv().adz().getImage(String.valueOf(C5378aHa.ITEMS.getPath()) + jCVar.mo12100sK().getHandle());
            } else if (userObject instanceof ItemTypeCategory) {
                ItemTypeCategory aai = (ItemTypeCategory) userObject;
                str = aai.mo12246ke().get();
                image = C3740uR.this.f9359kj.bHv().adz().getImage(String.valueOf(C5378aHa.ICONOGRAPHY.getPath()) + aai.mo12100sK().getHandle());
            } else {
                str = null;
                image = null;
            }
            if (z3) {
                nR = aur.mo11631nR("sellItemsTreeLeaf");
                JLabel cf = nR.mo4917cf("sellItemsTreeNameLeaf");
                jLabel = cf;
                jLabel2 = cf;
            } else if (z2) {
                nR = aur.mo11631nR("sellItemsTreeParentExpanded");
                JLabel cf2 = nR.mo4917cf("sellItemsTreeNameParentExpanded");
                jLabel = cf2;
                jLabel2 = cf2;
            } else {
                nR = aur.mo11631nR("sellItemsTreeParent");
                JLabel cf3 = nR.mo4917cf("sellItemsTreeNameParent");
                jLabel = cf3;
                jLabel2 = cf3;
            }
            jLabel2.setText(str);
            if (image != null) {
                if (userObject instanceof ItemType) {
                    jLabel.setIcon(new C2539gY(image));
                } else {
                    jLabel.setIcon(new ImageIcon(image));
                }
            }
            return nR;
        }
    }
}
