package p001a;

import com.hoplon.geometry.Vec3f;
import logic.res.KeyCode;

/* renamed from: a.hD */
/* compiled from: a */
public class C2581hD {
    public static final C2581hD ixx = new C2581hD(1024, KeyCode.cuB, 60, true, C2585d.VERY_LOW, C2582a.LOW, C2588g.OFF, false, C2586e.LOW, C2587f.OFF, C2584c.LOW, C2583b.LOW);
    public static final C2581hD ixy = new C2581hD(1024, KeyCode.cuB, 60, true, C2585d.HIGH, C2582a.HIGH, C2588g.LOW, true, C2586e.MEDIUM, C2587f.LOW, C2584c.MEDIUM, C2583b.MEDIUM);
    public static final C2581hD ixz = new C2581hD(1440, 900, 60, true, C2585d.VERY_HIGH, C2582a.HIGH, C2588g.VERY_HIGH, true, C2586e.HIGH, C2587f.VERY_HIGH, C2584c.HIGH, C2583b.HIGH);
    private float egF = 1.0f;
    private float egG = 0.8f;
    private float egH = 1.0f;
    private float egI = 0.5f;
    private float egJ = 1.0f;
    private boolean fullscreen;
    private Vec3f ixA;
    private C2585d ixB;
    private C2582a ixC;
    private C2588g ixD;
    private boolean ixE;
    private C2586e ixF;
    private C2587f ixG;
    private C2584c ixH;
    private C2583b ixI;
    private String ixJ;

    public C2581hD(int i, int i2, int i3, boolean z, C2585d dVar, C2582a aVar, C2588g gVar, boolean z2, C2586e eVar, C2587f fVar, C2584c cVar, C2583b bVar) {
        this.ixA = new Vec3f((float) i, (float) i2, (float) i3);
        this.fullscreen = z;
        this.ixB = dVar;
        this.ixC = aVar;
        this.ixD = gVar;
        this.ixE = z2;
        this.ixF = eVar;
        this.ixG = fVar;
        this.ixH = cVar;
        this.ixI = bVar;
    }

    public Vec3f dmL() {
        return this.ixA;
    }

    public boolean isFullscreen() {
        return this.fullscreen;
    }

    public C2585d dmM() {
        return this.ixB;
    }

    public C2582a dmN() {
        return this.ixC;
    }

    public C2588g dmO() {
        return this.ixD;
    }

    public boolean isPostProcessingFX() {
        return this.ixE;
    }

    public C2586e dmP() {
        return this.ixF;
    }

    public C2587f dmQ() {
        return this.ixG;
    }

    public float aek() {
        return this.egF;
    }

    /* renamed from: dq */
    public void mo19205dq(float f) {
        this.egF = f;
    }

    public float adX() {
        return this.egG;
    }

    /* renamed from: dm */
    public void mo19193dm(float f) {
        this.egG = f;
    }

    public float aeg() {
        return this.egH;
    }

    /* renamed from: dp */
    public void mo19204dp(float f) {
        this.egH = f;
    }

    public float aed() {
        return this.egJ;
    }

    /* renamed from: do */
    public void mo19203do(float f) {
        this.egJ = f;
    }

    public float aeb() {
        return this.egI;
    }

    /* renamed from: dn */
    public void mo19202dn(float f) {
        this.egI = f;
    }

    /* renamed from: a */
    public void mo19186a(C2584c cVar) {
        this.ixH = cVar;
    }

    public C2584c dmR() {
        return this.ixH;
    }

    /* renamed from: a */
    public void mo19185a(C2583b bVar) {
        this.ixI = bVar;
    }

    public C2583b dmS() {
        return this.ixI;
    }

    public String getLanguage() {
        return this.ixJ;
    }

    public void setLanguage(String str) {
        this.ixJ = str;
    }

    /* renamed from: c */
    public boolean mo19192c(C2581hD hDVar) {
        return this.ixA.equals(hDVar.ixA) && this.fullscreen == hDVar.fullscreen && this.ixB.equals(hDVar.ixB) && this.ixC.equals(hDVar.ixC) && this.ixD.equals(hDVar.ixD) && this.ixE == hDVar.ixE && this.ixF.equals(hDVar.ixF) && this.ixG.equals(hDVar.ixG) && this.ixH.equals(hDVar.ixH) && this.ixI.equals(hDVar.ixI);
    }

    /* renamed from: a.hD$d */
    /* compiled from: a */
    public enum C2585d {
        VERY_LOW(256),
        LOW(512),
        HIGH(1024),
        VERY_HIGH(2048);

        private int value;

        private C2585d(int i) {
            this.value = i;
        }

        public int getValue() {
            return this.value;
        }
    }

    /* renamed from: a.hD$a */
    public enum C2582a {
        LOW(2),
        MEDIUM(1),
        HIGH(0);

        private int value;

        private C2582a(int i) {
            this.value = i;
        }

        /* access modifiers changed from: package-private */
        public int getValue() {
            return this.value;
        }
    }

    /* renamed from: a.hD$g */
    /* compiled from: a */
    public enum C2588g {
        OFF(0),
        LOW(2),
        MEDIUM(4),
        HIGH(6),
        VERY_HIGH(8);

        private int value;

        private C2588g(int i) {
            this.value = i;
        }

        /* access modifiers changed from: package-private */
        public int getValue() {
            return this.value;
        }
    }

    /* renamed from: a.hD$e */
    /* compiled from: a */
    public enum C2586e {
        LOW(2),
        MEDIUM(1),
        HIGH(0);

        private int value;

        private C2586e(int i) {
            this.value = i;
        }

        public int getValue() {
            return this.value;
        }
    }

    /* renamed from: a.hD$f */
    /* compiled from: a */
    public enum C2587f {
        OFF(0.0f),
        LOW(2.0f),
        MEDIUM(4.0f),
        HIGH(8.0f),
        VERY_HIGH(16.0f);

        private float value;

        private C2587f(float f) {
            this.value = f;
        }

        /* access modifiers changed from: package-private */
        public float getValue() {
            return this.value;
        }
    }

    /* renamed from: a.hD$c */
    /* compiled from: a */
    public enum C2584c {
        LOW(3.0f),
        MEDIUM(1.0f),
        HIGH(0.0f);

        private float value;

        private C2584c(float f) {
            this.value = f;
        }

        public float getValue() {
            return this.value;
        }
    }

    /* renamed from: a.hD$b */
    /* compiled from: a */
    public enum C2583b {
        LOW(2),
        MEDIUM(1),
        HIGH(0);

        private int value;

        private C2583b(int i) {
            this.value = i;
        }

        public int getValue() {
            return this.value;
        }
    }
}
