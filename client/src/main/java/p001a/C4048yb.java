package p001a;

import javax.vecmath.Vector4f;

/* renamed from: a.yb */
/* compiled from: a */
public class C4048yb extends C6518aoW<Vector4f> {
    /* renamed from: k */
    public Vector4f mo23153k(float f, float f2, float f3, float f4) {
        Vector4f vector4f = (Vector4f) get();
        vector4f.set(f, f2, f3, f4);
        return vector4f;
    }

    /* renamed from: a */
    public Vector4f mo23150a(Vector4f vector4f) {
        Vector4f vector4f2 = (Vector4f) get();
        vector4f2.set(vector4f);
        return vector4f2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: apx */
    public Vector4f create() {
        return new Vector4f();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void copy(Vector4f vector4f, Vector4f vector4f2) {
        vector4f.set(vector4f2);
    }
}
