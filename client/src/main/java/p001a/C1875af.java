package p001a;

import game.network.WhoAmI;
import game.network.channel.client.ClientConnect;
import game.network.exception.aOQ;
import game.network.message.Blocking;
import game.network.message.C5581aOv;
import game.network.message.externalizable.*;
import logic.baa.C1616Xf;
import logic.baa.C4062yl;
import logic.baa.aDJ;
import logic.bbb.aDR;
import logic.data.mbean.C0677Jd;
import logic.data.mbean.C6064afk;
import logic.res.code.C5663aRz;
import logic.res.html.C2491fm;
import logic.res.html.C6663arL;
import logic.res.html.GameScriptClassesImpl;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: a.af */
/* compiled from: a */
public class C1875af extends C3582se {

    /* renamed from: hn */
    private static final C6014aem[] f4442hn = new C6014aem[0];
    private static final Log logger = LogPrinter.setClass(C1875af.class);
    /* renamed from: hu */
    private static ConcurrentHashMap<Class<? extends C1616Xf>, AtomicLong> f4443hu = new ConcurrentHashMap<>();
    /* renamed from: ho */
    private Map<ClientConnect, C6014aem> f4444ho;

    /* renamed from: hp */
    private C6014aem[] f4445hp = f4442hn;

    /* renamed from: hq */
    private ReadWriteLock f4446hq = new ReentrantReadWriteLock();

    /* renamed from: hr */
    private GameScriptClassesImpl f4447hr = null;

    /* renamed from: hs */
    private long f4448hs;

    /* renamed from: ht */
    private long f4449ht;

    /* renamed from: dx */
    public static Map<Class<?>, Long> m21423dx() {
        HashMap hashMap = new HashMap();
        for (Map.Entry next : f4443hu.entrySet()) {
            hashMap.put((Class) next.getKey(), Long.valueOf(((AtomicLong) next.getValue()).longValue()));
        }
        return Collections.unmodifiableMap(hashMap);
    }

    /* renamed from: di */
    public long mo13219di() {
        return this.f4449ht;
    }

    /* renamed from: a */
    public void mo13202a(long j) {
        this.f4449ht = j;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return r1;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public p001a.C6014aem mo13200a(aDR r5) {
        /*
            r4 = this;
            java.util.Map<a.b, a.aem> r0 = r4.f4444ho
            if (r0 != 0) goto L_0x000f
            gnu.trove.THashMap r0 = new gnu.trove.THashMap
            r0.<init>()
            java.util.Map r0 = java.util.Collections.synchronizedMap(r0)
            r4.f4444ho = r0
        L_0x000f:
            java.util.Map<a.b, a.aem> r2 = r4.f4444ho
            monitor-enter(r2)
            java.util.Map<a.b, a.aem> r0 = r4.f4444ho     // Catch:{ all -> 0x0056 }
            a.b r1 = r5.bgt()     // Catch:{ all -> 0x0056 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x0056 }
            a.aem r0 = (p001a.C6014aem) r0     // Catch:{ all -> 0x0056 }
            if (r0 == 0) goto L_0x0022
            monitor-exit(r2)     // Catch:{ all -> 0x0056 }
        L_0x0021:
            return r0
        L_0x0022:
            a.aem r1 = new a.aem     // Catch:{ all -> 0x0056 }
            r1.<init>(r5)     // Catch:{ all -> 0x0056 }
            java.util.Map<a.b, a.aem> r0 = r4.f4444ho     // Catch:{ all -> 0x0056 }
            a.b r3 = r5.bgt()     // Catch:{ all -> 0x0056 }
            r0.put(r3, r1)     // Catch:{ all -> 0x0056 }
            java.util.Map<a.b, a.aem> r0 = r4.f4444ho     // Catch:{ all -> 0x0056 }
            int r0 = r0.size()     // Catch:{ all -> 0x0056 }
            if (r0 <= 0) goto L_0x0051
            java.util.Map<a.b, a.aem> r0 = r4.f4444ho     // Catch:{ all -> 0x0056 }
            java.util.Collection r0 = r0.values()     // Catch:{ all -> 0x0056 }
            java.util.Map<a.b, a.aem> r3 = r4.f4444ho     // Catch:{ all -> 0x0056 }
            int r3 = r3.size()     // Catch:{ all -> 0x0056 }
            a.aem[] r3 = new p001a.C6014aem[r3]     // Catch:{ all -> 0x0056 }
            java.lang.Object[] r0 = r0.toArray(r3)     // Catch:{ all -> 0x0056 }
            a.aem[] r0 = (p001a.C6014aem[]) r0     // Catch:{ all -> 0x0056 }
            r4.f4445hp = r0     // Catch:{ all -> 0x0056 }
        L_0x004e:
            monitor-exit(r2)     // Catch:{ all -> 0x0056 }
            r0 = r1
            goto L_0x0021
        L_0x0051:
            a.aem[] r0 = f4442hn     // Catch:{ all -> 0x0056 }
            r4.f4445hp = r0     // Catch:{ all -> 0x0056 }
            goto L_0x004e
        L_0x0056:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0056 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C1875af.mo13200a(a.aDR):a.aem");
    }

    /* renamed from: b */
    public void mo13214b(aDR adr) {
        if (this.f4444ho != null) {
            synchronized (this.f4444ho) {
                this.f4444ho.remove(adr.bgt());
                Collection<C6014aem> values = this.f4444ho.values();
                this.f4445hp = (C6014aem[]) values.toArray(new C6014aem[values.size()]);
            }
        }
    }

    /* renamed from: a */
    public boolean mo13211a(ClientConnect bVar) {
        if (this.f4444ho == null) {
            return false;
        }
        return this.f4444ho.containsKey(bVar);
    }

    /* renamed from: c */
    public boolean mo13217c(aDR adr) {
        return this.f4444ho.containsKey(adr.bgt());
    }

    /* renamed from: dj */
    public boolean mo5607dj() {
        if (!bGZ()) {
            throw new IllegalAccessError("Invalid at client");
        } else if (this.f4444ho == null) {
            return false;
        } else {
            aDR bGF = mo6866PM().bGF();
            if (bGF == null) {
                throw new IllegalAccessError("Invalid outside client calls");
            }
            ClientConnect bgt = bGF.bgt();
            C6014aem aem = this.f4444ho.get(bgt);
            if (aem != null && aem.fnt) {
                return true;
            }
            aWq dDA = aWq.dDA();
            if (dDA != null) {
                return dDA.mo12051a((C3582se) this, bgt);
            }
            return false;
        }
    }

    /* renamed from: d */
    public boolean mo13218d(aDR adr) {
        if (this.f4444ho == null) {
            return false;
        }
        C6014aem aem = this.f4444ho.get(adr.bgt());
        return aem == null ? false : aem.fnt;
    }

    /* renamed from: b */
    public void mo13212b(long j) {
        mo6866PM().mo3441gd(j);
    }

    public void push() {
        aDR dDV;
        if (!bGZ()) {
            throw new IllegalAccessError();
        }
        aWq dDA = aWq.dDA();
        if (dDA == null || (dDV = dDA.dDV()) == null) {
            mo13228e(mo6866PM().bGF());
        } else {
            mo13228e(dDV);
        }
    }

    /* renamed from: e */
    public void mo13228e(aDR adr) {
        aWq dDA = aWq.dDA();
        if (dDA != null && adr != null && !mo13218d(adr)) {
            dDA.mo12049a(this, adr);
        }
    }

    /* renamed from: a */
    public void mo13204a(aDJ adj) {
        for (C6014aem aem : ((C1875af) adj.bFf()).mo13225dp()) {
            if (aem.fnt) {
                mo13228e(aem.eIu);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo13208a(Blocking kUVar, aDR adr) {
        for (C6014aem aem : mo13225dp()) {
            if (adr == null || aem.eIu != adr) {
                this.aGA.bGU().mo15549b(aem.eIu.bgt(), kUVar);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo13213b(C0495Gr gr) {
        mo13203a(gr, (aDR) null);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo13203a(C0495Gr gr, aDR adr) {
        mo6866PM().bGR().mo5930a(gr, adr, (aWq) null, (C5581aOv) null);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo13205a(aWq awq, C0495Gr gr, aDR adr, C5581aOv aov) {
        mo6866PM().bGR().mo5930a(gr, adr, awq, aov);
    }

    /* renamed from: a */
    public boolean mo13209a(C1619Xi<C1616Xf> xi, aDR adr, boolean z) {
        if (adr.cST()) {
            return true;
        }
        if (!mo6901yn().isUseServerOnly() || adr.cWs()) {
            C6147ahP[] bFh = mo6901yn().bFh();
            if (bFh == null || bFh.length <= 0) {
                return true;
            }
            C6663arL cWq = adr.cWq();
            if (xi == null) {
                xi = new C1619Xi<>();
            }
            if (cWq != null) {
                xi.mo6768Z(cWq.mo6901yn());
            }
            xi.mo6776h(adr.bgt());
            xi.mo6779j(adr);
            int length = bFh.length;
            int i = 0;
            while (i < length) {
                C6147ahP ahp = bFh[i];
                if (ahp.mo13516a(mo6901yn(), xi)) {
                    i++;
                } else if (!z) {
                    return false;
                } else {
                    throw new aOQ("Access to object " + bFU().getName() + " id " + getObjectId().getId() + " denied by replication rule: " + ahp.mo13515Xf().name());
                }
            }
            return true;
        } else if (!z) {
            return false;
        } else {
            throw new RuntimeException("The requested object " + bFU().getName() + " id " + getObjectId().getId() + " is ServerOnly");
        }
    }

    /* renamed from: dk */
    public void mo13220dk() {
        this.f4446hq.writeLock().unlock();
    }

    /* renamed from: dl */
    public void mo13221dl() {
        this.f4446hq.readLock().unlock();
    }

    /* renamed from: dm */
    public void mo13222dm() {
        this.f4446hq.writeLock().lock();
    }

    /* renamed from: dn */
    public void mo13223dn() {
        this.f4446hq.readLock().lock();
    }

    /* renamed from: do */
    public boolean mo13224do() {
        return mo13209a((C1619Xi<C1616Xf>) null, mo6866PM().bGF(), false);
    }

    /* renamed from: a */
    public C6302akO mo5593a(ClientConnect bVar, C5703aTn atn) {
        List<Blocking> list;
        aWq awq;
        if (mo6866PM().bGM()) {
            return null;
        }
        aDR f = mo6866PM().bGJ().mo6442f(bVar);
        C0495Gr dvF = atn.dvF();
        C2491fm aQK = dvF.aQK();
        if (aQK.mo4790pv()) {
            if (aWq.dDA() == null) {
                awq = aWq.m19285b(currentTimeMillis(), f, dvF);
                awq.mo12087kM(true);
            } else {
                awq = null;
            }
            try {
                mo13203a(dvF, f);
                if (awq != null) {
                    awq.mo12081g(this.aGA);
                    awq.dispose();
                }
                if (!aQK.mo4783po()) {
                    return null;
                }
            } catch (Throwable th) {
                if (awq != null) {
                    awq.dispose();
                }
                throw th;
            }
        }
        try {
            Object a = mo6866PM().bGR().mo5928a(f, dvF, atn.isBlocking());
            if (atn.isBlocking()) {
                C1830aW aWVar = (C1830aW) a;
                Object fC = aWVar.mo12004fC();
                list = aWVar.mo12003fB();
                a = fC;
            } else {
                list = null;
            }
            if (atn instanceof C2390ek) {
                this.aGA.bGU().mo15549b(f.bgt(), (Blocking) new C0626Is(mo6901yn(), ((C2390ek) atn).getId(), a, list));
                return null;
            } else if (atn.isBlocking()) {
                return new C6212aic(a, list);
            } else {
                return null;
            }
        } catch (Exception e) {
            if (atn instanceof C2390ek) {
                this.aGA.bGU().mo15549b(f.bgt(), (Blocking) new C0626Is(mo6901yn(), ((C2390ek) atn).getId(), (Object) null, e, (List<Blocking>) null));
                return null;
            }
            throw e;
        }
    }

    /* renamed from: dp */
    public C6014aem[] mo13225dp() {
        return this.f4445hp;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C6302akO mo13201a(ClientConnect r6, C5581aOv r7) {
        /*
            r5 = this;
            r4 = 0
            a.afk r0 = r5.hCm     // Catch:{ Exception -> 0x008b }
            boolean r0 = r0.mo3202hF()     // Catch:{ Exception -> 0x008b }
            if (r0 == 0) goto L_0x0014
            a.afk r0 = r5.hCm     // Catch:{ Exception -> 0x008b }
            boolean r0 = r0.mo3194du()     // Catch:{ Exception -> 0x008b }
            if (r0 != 0) goto L_0x0014
            r5.mo5609ds()     // Catch:{ Exception -> 0x008b }
        L_0x0014:
            a.Jz r0 = r5.mo6866PM()     // Catch:{ Exception -> 0x008b }
            a.Vv r0 = r0.bGJ()     // Catch:{ Exception -> 0x008b }
            a.aDR r2 = r0.mo6442f(r6)     // Catch:{ Exception -> 0x008b }
            a.arL r0 = r2.hDb     // Catch:{ Exception -> 0x008b }
            if (r0 == 0) goto L_0x0050
            java.util.concurrent.ConcurrentHashMap<java.lang.Class<? extends a.Xf>, java.util.concurrent.atomic.AtomicLong> r0 = f4443hu     // Catch:{ Exception -> 0x008b }
            a.Xf r1 = r5.mo6901yn()     // Catch:{ Exception -> 0x008b }
            java.lang.Class r1 = r1.getClass()     // Catch:{ Exception -> 0x008b }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Exception -> 0x008b }
            java.util.concurrent.atomic.AtomicLong r0 = (java.util.concurrent.atomic.AtomicLong) r0     // Catch:{ Exception -> 0x008b }
            if (r0 != 0) goto L_0x004d
            java.util.concurrent.atomic.AtomicLong r1 = new java.util.concurrent.atomic.AtomicLong     // Catch:{ Exception -> 0x008b }
            r1.<init>()     // Catch:{ Exception -> 0x008b }
            java.util.concurrent.ConcurrentHashMap<java.lang.Class<? extends a.Xf>, java.util.concurrent.atomic.AtomicLong> r0 = f4443hu     // Catch:{ Exception -> 0x008b }
            a.Xf r3 = r5.mo6901yn()     // Catch:{ Exception -> 0x008b }
            java.lang.Class r3 = r3.getClass()     // Catch:{ Exception -> 0x008b }
            java.lang.Object r0 = r0.putIfAbsent(r3, r1)     // Catch:{ Exception -> 0x008b }
            java.util.concurrent.atomic.AtomicLong r0 = (java.util.concurrent.atomic.AtomicLong) r0     // Catch:{ Exception -> 0x008b }
            if (r0 == 0) goto L_0x00ab
        L_0x004d:
            r0.incrementAndGet()     // Catch:{ Exception -> 0x008b }
        L_0x0050:
            a.qX r7 = (game.network.message.externalizable.C3345qX) r7     // Catch:{ Exception -> 0x008b }
            boolean r0 = r7.mo21371Yg()     // Catch:{ Exception -> 0x008b }
            if (r0 == 0) goto L_0x0082
            boolean r0 = r2.cST()     // Catch:{ Exception -> 0x008b }
            if (r0 != 0) goto L_0x0082
            r0 = 1
        L_0x005f:
            a.Jz r1 = r5.aGA     // Catch:{ Exception -> 0x008b }
            a.azC r1 = r1.bGr()     // Catch:{ Exception -> 0x008b }
            java.util.concurrent.atomic.AtomicLong r1 = r1.hat     // Catch:{ Exception -> 0x008b }
            r1.incrementAndGet()     // Catch:{ Exception -> 0x008b }
            boolean r1 = r5.bGY()     // Catch:{ Exception -> 0x008b }
            if (r1 != 0) goto L_0x0077
            r1 = 0
            boolean r0 = r5.mo13209a((p001a.C1619Xi<logic.baa.C1616Xf>) r1, (logic.bbb.aDR) r2, (boolean) r0)     // Catch:{ Exception -> 0x008b }
            if (r0 == 0) goto L_0x00a5
        L_0x0077:
            r2.mo8400e(r5)     // Catch:{ InterruptedException -> 0x0084 }
            a.aiN r0 = new a.aiN     // Catch:{ Exception -> 0x008b }
            a.afk r1 = r5.hCm     // Catch:{ Exception -> 0x008b }
            r0.<init>(r1)     // Catch:{ Exception -> 0x008b }
        L_0x0081:
            return r0
        L_0x0082:
            r0 = 0
            goto L_0x005f
        L_0x0084:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ Exception -> 0x008b }
            r1.<init>(r0)     // Catch:{ Exception -> 0x008b }
            throw r1     // Catch:{ Exception -> 0x008b }
        L_0x008b:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Error on retrive data command of "
            r2.<init>(r3)
            java.lang.String r3 = r5.bFY()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2, r0)
            throw r1
        L_0x00a5:
            a.aiN r0 = new a.aiN
            r0.<init>(r4)
            goto L_0x0081
        L_0x00ab:
            r0 = r1
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C1875af.mo13201a(a.b, a.aOv):a.akO");
    }

    /* renamed from: a */
    public boolean mo13210a(aDR adr, C1619Xi<C1616Xf> xi, C0495Gr gr) {
        xi.mo6769a(WhoAmI.CLIENT);
        xi.mo6775c(gr);
        if (!mo13209a(xi, adr, false)) {
            return false;
        }
        C6147ahP[] hi = gr.aQK().mo7387hi();
        if (hi != null) {
            C6663arL cWq = adr.cWq();
            if (cWq != null) {
                xi.mo6768Z(cWq.mo6901yn());
            }
            xi.mo6776h(adr.bgt());
            xi.mo6779j(adr);
            try {
                for (C6147ahP a : hi) {
                    if (!a.mo13516a(mo6901yn(), xi)) {
                        return false;
                    }
                }
            } catch (Exception e) {
                logger.warn("Exception during replication check to " + adr.bgt() + " of object " + mo6901yn().getClass() + ":" + getObjectId().getId() + " method " + gr.aQK().name(), e);
                return false;
            }
        }
        return true;
    }

    /* renamed from: dq */
    public final C6064afk mo5608dq() {
        C6064afk afk;
        aWq dDA = aWq.dDA();
        if (dDA == null) {
            afk = this.hCm;
        } else if (dDA.dDE()) {
            afk = this.hCm;
        } else {
            afk = dDA.mo12052b(this);
        }
        if (!afk.mo3202hF()) {
            return afk;
        }
        mo5609ds();
        if (dDA == null) {
            return this.hCm;
        }
        if (dDA.dDE()) {
            return this.hCm;
        }
        return dDA.mo12052b(this);
    }

    /* renamed from: dr */
    public void mo10527dr() {
        if (!mo10528du()) {
            aWq dDA = aWq.dDA();
            if (dDA == null) {
                throw new IllegalStateException("Infra.infraDispose() cannot be called outside transaction");
            } else if (dDA.dDE()) {
                throw new IllegalStateException("Infra.infraDispose() cannot be called in safe context transaction");
            } else {
                dDA.mo12047a(this).mo9272cS(true);
            }
        }
    }

    /* renamed from: ds */
    public boolean mo5609ds() {
        try {
            if (this.hCm.mo3194du()) {
                return true;
            }
            this.aGA.bGT().mo5236h(this);
            return true;
        } catch (IOException e) {
            throw new RuntimeException("Error acquiring data of " + bFY(), e);
        }
    }

    /* renamed from: b */
    public final Object mo5602b(C5663aRz arz) {
        C6064afk afk;
        aWq dDA = aWq.dDA();
        if (dDA == null) {
            afk = this.hCm;
        } else if (dDA.dDE()) {
            dDA.jgb++;
            afk = this.hCm;
        } else {
            dDA.env++;
            afk = dDA.mo12052b(this);
        }
        if (afk.mo3202hF()) {
            mo5609ds();
            afk = mo5608dq();
        }
        return afk.mo3199g(arz);
    }

    /* renamed from: a */
    public final void mo5599a(C5663aRz arz, Object obj) {
        C6064afk afk;
        aWq dDA = aWq.dDA();
        if (dDA == null) {
            afk = this.hCm;
        } else if (dDA.readOnly) {
            throw new IllegalThreadStateException("All fields are read only and events cannot be fired from within a postponed infra call");
        } else if (dDA.dDE()) {
            dDA.jgc++;
            afk = this.hCm;
        } else {
            dDA.eny++;
            afk = dDA.mo12052b(this);
        }
        if (dDA != null || (!mo6866PM().bGo() && (bHa() || !mo6901yn().isUseClientOnly()))) {
            afk.mo3195e(arz, obj);
        } else {
            afk.mo3193d(arz, obj);
        }
    }

    /* renamed from: dt */
    public void mo13226dt() {
        aWq dDA;
        if (this.aGA.bGo() || (dDA = aWq.dDA()) == null) {
            return;
        }
        if (dDA.dDE()) {
            throw new C6411amT();
        }
        dDA.mo12047a(this).mo9271cR(true);
    }

    /* renamed from: du */
    public boolean mo10528du() {
        if (this.hCm.mo3202hF()) {
            mo5609ds();
        }
        return super.mo10528du();
    }

    /* renamed from: a */
    public final void mo5600a(Class<? extends C4062yl> cls, C0495Gr gr) {
        aDJ adj = (aDJ) mo6901yn();
        if (gr.aQK().mo4769pO().cpn() && adj.cWh()) {
            aWq dDA = aWq.dDA();
            dDA.mo12083i(new C1876a(dDA, gr));
        }
        Collection<?> aJ = adj.mo8323aJ(cls);
        if (aJ != null) {
            mo6901yn().mo15a(new ArrayList(aJ), gr);
        }
    }

    /* renamed from: dv */
    public GameScriptClassesImpl mo6888dv() {
        if (this.f4447hr == null) {
            this.f4447hr = new GameScriptClassesImpl(this);
        }
        return this.f4447hr;
    }

    /* renamed from: a */
    public void mo13206a(C6064afk afk) {
        if (afk.mo3194du()) {
            C3582se g = this.aGA.mo3440g(this);
            this.hCm = g.hCm;
            mo6901yn().mo6754b(g);
            return;
        }
        super.mo13206a(afk);
    }

    /* renamed from: c */
    public void mo13216c(long j) {
        this.f4448hs = j;
    }

    /* renamed from: dw */
    public long mo13227dw() {
        return this.f4448hs;
    }

    /* renamed from: a */
    public void mo13207a(ClientConnect bVar, long j) {
        C6014aem aem = this.f4444ho.get(bVar);
        if (aem == null) {
            logger.warn("Client is trying to enable the state update for an object it does not have (or at least no longer has), from " + bVar + " object " + bFY());
            return;
        }
        if (aem.bUi()) {
            logger.warn("Client is trying to enable the state update for an object that is already sending the state update, from " + bVar + " object " + bFY());
        }
        C6064afk cVj = cVj();
        if (j != cVj.aBt()) {
            this.aGA.bGU().mo15549b(bVar, (Blocking) new C3213pH((C0677Jd) cVj));
        }
        aem.mo13140eg(true);
        if (logger.isDebugEnabled()) {
            logger.debug("enableStateUpdate: " + bFY() + " from " + bVar);
        }
    }

    /* renamed from: b */
    public void mo13215b(ClientConnect bVar) {
        C6014aem aem = this.f4444ho.get(bVar);
        if (aem == null) {
            logger.warn("Client is trying to disable the state update for an object it does not have (or at least no longer has), from " + bVar + " object " + bFY());
            return;
        }
        if (!aem.bUi() && logger.isDebugEnabled()) {
            logger.warn("Client is trying to disable the state update for an object that is already NOT sending the state update, from " + bVar + " object " + bFY());
        }
        aem.mo13140eg(false);
        if (logger.isDebugEnabled()) {
            logger.debug("disableStateUpdate: " + bFY() + " from " + bVar);
        }
    }

    /* renamed from: a.af$a */
    class C1876a implements Runnable {
        private final /* synthetic */ C0495Gr dgP;
        private final /* synthetic */ aWq eNa;

        C1876a(aWq awq, C0495Gr gr) {
            this.eNa = awq;
            this.dgP = gr;
        }

        public void run() {
            C1875af.this.mo13205a(this.eNa, this.dgP, (aDR) null, new C3962xP(this.dgP));
        }
    }
}
