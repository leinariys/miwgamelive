package p001a;

import taikodom.addon.p002fx.InterfaceSFXAddon;
import taikodom.render.scene.SoundObject;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;

/* renamed from: a.azi  reason: case insensitive filesystem */
/* compiled from: a */
class C7021azi implements ActionListener {
    final /* synthetic */ InterfaceSFXAddon eNv;

    public C7021azi(InterfaceSFXAddon interfaceSFXAddon) {
        this.eNv = interfaceSFXAddon;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        ArrayList<SoundObject> arrayList = new ArrayList<>();
        for (Map.Entry entry : this.eNv.iYh.entrySet()) {
            SoundObject soundObject = (SoundObject) entry.getKey();
            if (System.currentTimeMillis() - ((Long) entry.getValue()).longValue() > 2000 || soundObject.isPlaying() || soundObject.isDisposed()) {
                soundObject.stop();
                arrayList.add(soundObject);
            }
        }
        for (SoundObject remove : arrayList) {
            this.eNv.iYh.remove(remove);
        }
        if (this.eNv.iYh.size() == 0) {
            this.eNv.iYq.stop();
        }
    }
}
