package p001a;

import game.network.message.externalizable.C3328qK;
import game.network.message.serializable.C1662YU;
import game.network.message.serializable.C3438ra;
import game.network.message.serializable.C3988xl;
import logic.data.mbean.C0677Jd;
import logic.res.html.C0029AO;
import logic.res.html.C1867ad;
import logic.res.html.MessageContainer;
import logic.ui.item.ListJ;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.ListIterator;

/* renamed from: a.YX */
/* compiled from: a */
public class C1666YX<T> extends C3988xl<T> implements C3438ra<T> {

    private final ListJ<T> bFI;
    /* access modifiers changed from: private */
    public ListJ<T> list;
    private C3328qK<T> ijr;

    public C1666YX(C0677Jd jd, C3328qK<T> qKVar) {
        super(jd, qKVar);
        ListJ<T> list2 = (ListJ) qKVar.getCollection();
        this.bFI = list2;
        this.list = list2;
    }

    public T get(int i) {
        return this.list.get(i);
    }

    public void add(int i, T t) {
        anC();
        this.list.add(i, t);
        mo22982a(new C6260ajY(C0651JP.ADD_AT, i, t));
    }

    public int indexOf(Object obj) {
        return this.list.indexOf(obj);
    }

    public int lastIndexOf(Object obj) {
        return this.list.lastIndexOf(obj);
    }

    public ListIterator<T> listIterator() {
        return this.list.listIterator();
    }

    public ListIterator<T> listIterator(int i) {
        return this.list.listIterator(i);
    }

    public T remove(int i) {
        anC();
        T remove = this.list.remove(i);
        mo22982a(new C0402Fb(C0651JP.REMOVE_AT, i));
        return remove;
    }

    public T set(int i, T t) {
        anC();
        T t2 = this.list.set(i, t);
        mo22982a(new C6260ajY(C0651JP.SET, i, t));
        return t2;
    }

    public boolean addAll(int i, Collection<? extends T> collection) {
        anC();
        this.list.addAll(i, collection);
        mo22982a(new C6260ajY(C0651JP.ADD_COLLECTION_AT, i, collection));
        return true;
    }

    public ListJ<T> subList(int i, int i2) {
        return this.list.subList(i, i2);
    }

    /* access modifiers changed from: protected */
    public final void anC() {
        if (this.bFM) {
            return;
        }
        if (this.bFN) {
            throw new IllegalStateException("Already finished");
        }
        ListJ<T> w = MessageContainer.m16004w(this.bFI);
        this.list = w;
        this.collection = w;
        this.bFM = true;
    }

    public ListJ<T> getResult() {
        if (this.bFM) {
            return this.list;
        }
        return this.bFI;
    }

    public Iterator<T> iterator() {
        return new C1667a();
    }

    /* renamed from: XC */
    public C1867ad<T> mo271XC() {
        return new C1662YU(this);
    }

    public C0029AO<T> anE() {
        if (this.bFN) {
            return this.ijr;
        }
        this.bFN = true;
        if (!this.bFM) {
            return this.bFO;
        }
        this.bFM = false;
        C3328qK<T> qKVar = new C3328qK<>(this.bFO.mo19379yn(), this.bFO.mo19378ym(), this.list);
        this.ijr = qKVar;
        return qKVar;
    }

    /* renamed from: a.YX$a */
    class C1667a implements Iterator<T> {
        int aVX = 0;
        int expectedModCount;

        C1667a() {
            this.expectedModCount = C1666YX.this.anD();
        }

        public boolean hasNext() {
            amx();
            return this.aVX < C1666YX.this.list.size();
        }

        public T next() {
            amx();
            C1666YX yx = C1666YX.this;
            int i = this.aVX;
            this.aVX = i + 1;
            return yx.get(i);
        }

        private void amx() {
            if (this.expectedModCount != C1666YX.this.anD()) {
                throw new ConcurrentModificationException();
            }
        }

        public void remove() {
            amx();
            C1666YX.this.anC();
            C1666YX yx = C1666YX.this;
            int i = this.aVX - 1;
            this.aVX = i;
            C1666YX.this.mo22982a(new C6630aqe(C0651JP.REMOVE, yx.remove(i)));
            this.expectedModCount = C1666YX.this.anD();
        }
    }
}
