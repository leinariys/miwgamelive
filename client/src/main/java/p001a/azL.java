package p001a;

import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.PropertiesUiFromCss;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

/* renamed from: a.azL */
/* compiled from: a */
public class azL extends DefaultTableCellRenderer {

    private boolean hbi = false;

    /* renamed from: a */
    private void m27624a(JLabel jLabel, C2570gx gxVar) {
        jLabel.setText(gxVar.f7820QL);
        if (gxVar.f7821QM != null) {
            jLabel.setIcon(gxVar.f7821QM);
            jLabel.setHorizontalTextPosition(11);
        } else {
            jLabel.setIcon((Icon) null);
        }
        if (!this.hbi) {
            ComponentManager.getCssHolder(jLabel).setAttribute("class", "number");
        }
        this.hbi = true;
    }

    public Component getTableCellRendererComponent(JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
        JLabel tableCellRendererComponent = azL.super.getTableCellRendererComponent(jTable, "", z, z2, i, i2);
        if (obj != null) {
            C2570gx gxVar = (C2570gx) obj;
            if (gxVar.isReady()) {
                switch (i2) {
                    case 0:
                        tableCellRendererComponent.setIcon(gxVar.f7819QK);
                        IComponentManager e = ComponentManager.getCssHolder(tableCellRendererComponent);
                        PropertiesUiFromCss Vr = e.mo13049Vr();
                        if (Vr.getColorMultiplier() != gxVar.color) {
                            Vr.setColorMultiplier(gxVar.color);
                            e.mo13045Vk();
                            break;
                        }
                        break;
                    case 1:
                        tableCellRendererComponent.setIcon((Icon) null);
                        tableCellRendererComponent.setText(gxVar.name);
                        break;
                    case 2:
                        m27624a(tableCellRendererComponent, gxVar);
                        break;
                }
            }
        } else {
            tableCellRendererComponent.setText("");
            tableCellRendererComponent.setIcon((Icon) null);
        }
        return tableCellRendererComponent;
    }
}
