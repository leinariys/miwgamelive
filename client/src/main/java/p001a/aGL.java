package p001a;

import game.script.item.Item;

/* renamed from: a.aGL */
/* compiled from: a */
public class aGL extends C0400FZ<Item> {
    private String ewp = "";

    /* renamed from: R */
    public boolean evaluate(Item auq) {
        if (this.ewp == null || this.ewp.length() == 0 || auq.bAP().mo19891ke().get().toLowerCase().indexOf(this.ewp.toString()) >= 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: N */
    public boolean mo2164N(Object obj) {
        String lowerCase = obj.toString().trim().toLowerCase();
        if (lowerCase.equals(this.ewp)) {
            return false;
        }
        this.ewp = lowerCase;
        return true;
    }
}
