package p001a;

/* renamed from: a.S */
/* compiled from: a */
public class C1220S {

    /* renamed from: ek */
    private float[] f1524ek = new float[16];
    private int size;

    public void add(float f) {
        if (this.size == this.f1524ek.length) {
            expand();
        }
        float[] fArr = this.f1524ek;
        int i = this.size;
        this.size = i + 1;
        fArr[i] = f;
    }

    private void expand() {
        float[] fArr = new float[(this.f1524ek.length << 1)];
        System.arraycopy(this.f1524ek, 0, fArr, 0, this.f1524ek.length);
        this.f1524ek = fArr;
    }

    public float remove(int i) {
        if (i >= this.size) {
            throw new IndexOutOfBoundsException();
        }
        float f = this.f1524ek[i];
        System.arraycopy(this.f1524ek, i + 1, this.f1524ek, i, (this.size - i) - 1);
        this.size--;
        return f;
    }

    public float get(int i) {
        if (i < this.size) {
            return this.f1524ek[i];
        }
        throw new IndexOutOfBoundsException();
    }

    public void set(int i, float f) {
        if (i >= this.size) {
            throw new IndexOutOfBoundsException();
        }
        this.f1524ek[i] = f;
    }

    public int size() {
        return this.size;
    }
}
