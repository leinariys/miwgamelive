package p001a;

import java.io.File;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/* renamed from: a.fJ */
/* compiled from: a */
public class C2423fJ implements Runnable {

    /* renamed from: Nc */
    private final long f7276Nc;

    /* renamed from: Nd */
    private final Set<C1715ZN> f7277Nd;
    private final File file;
    /* renamed from: Ne */
    private long f7278Ne;
    private boolean stop;

    public C2423fJ(String str) {
        this.f7277Nd = new CopyOnWriteArraySet();
        this.stop = false;
        this.f7276Nc = 1000;
        this.file = new File(str);
        this.f7278Ne = this.file.lastModified();
    }

    public C2423fJ(String str, long j) {
        this.f7277Nd = new CopyOnWriteArraySet();
        this.stop = false;
        this.file = new File(str);
        this.f7278Ne = this.file.lastModified();
        this.f7276Nc = j;
    }

    public void run() {
        while (!this.stop) {
            try {
                if (this.file.lastModified() > this.f7278Ne) {
                    this.f7278Ne = this.file.lastModified();
                    C5982aeG aeg = new C5982aeG(this.file);
                    for (C1715ZN a : this.f7277Nd) {
                        a.mo7381a(aeg);
                    }
                }
                Thread.sleep(this.f7276Nc);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void stop() {
        this.stop = true;
    }

    /* renamed from: a */
    public void mo18407a(C1715ZN zn) {
        this.f7277Nd.add(zn);
    }

    /* renamed from: b */
    public void mo18408b(C1715ZN zn) {
        this.f7277Nd.remove(zn);
    }
}
