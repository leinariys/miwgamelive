package p001a;

/* renamed from: a.aul  reason: case insensitive filesystem */
/* compiled from: a */
public class C6845aul extends RuntimeException {
    public C6845aul() {
        this("The requested game.script was already garbage collected");
    }

    public C6845aul(String str, Throwable th) {
        super(str, th);
    }

    public C6845aul(String str) {
        super(str);
    }

    public C6845aul(Throwable th) {
        super(th);
    }
}
