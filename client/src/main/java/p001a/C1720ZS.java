package p001a;

import logic.res.html.C2194cc;
import logic.res.html.C2491fm;
import logic.res.html.MessageContainer;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/* renamed from: a.ZS */
/* compiled from: a */
public abstract class C1720ZS implements C2194cc {
    public static final HashMap<String, C1720ZS> eRE = new HashMap<>();

    private final transient C1721a eRU = new C1721a(this);
    public String cUW;
    public int eRF;
    public boolean eRG;
    public boolean eRH;
    public boolean eRI;
    public boolean eRJ;
    public boolean eRK;
    public boolean eRL;
    public C2499fr eRM;
    public transient C6147ahP[] eRN;
    public transient C3538sJ eRO;
    public transient boolean eRP = false;
    public C3122oB eRQ;
    public C1020Ot eRR;
    public long eRS;
    public C2950m eRT;
    public String name;

    public C1720ZS(String str, int i, boolean z) {
        this.eRF = i;
        this.eRG = z;
        this.name = str;
    }

    public C1720ZS() {
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (this.eRF != ((C1720ZS) obj).eRF) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.eRF;
    }

    /* renamed from: hk */
    public boolean mo7389hk() {
        return this.eRL;
    }

    /* renamed from: hl */
    public boolean mo7390hl() {
        return this.eRK;
    }

    /* renamed from: hm */
    public boolean mo7391hm() {
        return this.eRG;
    }

    /* renamed from: hn */
    public boolean mo7392hn() {
        return this.eRJ;
    }

    /* renamed from: ho */
    public boolean mo7393ho() {
        return this.eRH;
    }

    /* renamed from: qa */
    public long mo4795qa() {
        if (this.eRT == null) {
            return -1;
        }
        if (this.eRS == 0) {
            this.eRS = this.eRT.timeout();
        }
        return this.eRS;
    }

    /* renamed from: hq */
    public int mo7395hq() {
        return this.eRF;
    }

    public String name() {
        return this.name;
    }

    public String getName() {
        return this.name;
    }

    public Object writeReplace() {
        return this.eRU;
    }

    public C3122oB bJF() {
        return this.eRQ;
    }

    public C1020Ot bJG() {
        return this.eRR;
    }

    /* renamed from: pq */
    public void mo7398pq(int i) {
        this.eRF = i;
    }

    /* renamed from: hi */
    public C6147ahP[] mo7387hi() {
        C2491fm fmVar;
        if (this.eRN != null) {
            return this.eRN;
        }
        if (!this.eRJ) {
            C6147ahP[] ahpArr = new C6147ahP[0];
            this.eRN = ahpArr;
            return ahpArr;
        }
        List dgK = MessageContainer.init();
        for (String str : this.eRM.mo18855qf()) {
            synchronized (eRE) {
                fmVar = (C2491fm) eRE.get(str);
            }
            if (fmVar != null) {
                dgK.add(new C3384qv(fmVar));
            } else {
                System.err.println("Warning: replication rule " + str + " not found for " + fmVar);
            }
        }
        C6147ahP[] ahpArr2 = (C6147ahP[]) dgK.toArray(new C6147ahP[dgK.size()]);
        this.eRN = ahpArr2;
        return ahpArr2;
    }

    /* renamed from: hj */
    public C3538sJ mo7388hj() {
        String qg;
        C2491fm fmVar;
        if (this.eRP) {
            return this.eRO;
        }
        if (!this.eRJ || (qg = this.eRM.mo18856qg()) == null || qg.length() == 0) {
            return null;
        }
        synchronized (eRE) {
            fmVar = (C2491fm) eRE.get(qg);
        }
        if (fmVar == null) {
            System.err.println("Warning: replication rule " + qg + " not found for " + fmVar);
        } else {
            this.eRO = new aOH(fmVar);
        }
        this.eRP = true;
        return this.eRO;
    }

    /* renamed from: hp */
    public boolean mo7394hp() {
        return this.eRI;
    }

    /* renamed from: hr */
    public String mo7396hr() {
        return this.cUW;
    }

    /* renamed from: a.ZS$a */
    public static class C1721a implements Externalizable, Serializable {

        private transient C1720ZS gjm;

        public C1721a() {
        }

        public C1721a(C1720ZS zs) {
            this.gjm = zs;
        }

        public void readExternal(ObjectInput objectInput) {
            this.gjm = C1720ZS.eRE.get(objectInput.readUTF());
        }

        public void writeExternal(ObjectOutput objectOutput) {
            objectOutput.writeUTF(this.gjm.cUW);
        }

        public Object readResolve() {
            return this.gjm;
        }
    }
}
