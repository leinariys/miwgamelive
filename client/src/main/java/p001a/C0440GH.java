package p001a;

import game.network.message.externalizable.C3689to;
import game.script.hangar.Bay;
import game.script.item.BulkItem;
import game.script.item.Component;
import game.script.item.ComponentType;
import game.script.item.Item;
import game.script.player.Player;
import game.script.player.PlayerAssistant;
import game.script.ship.CargoHold;
import game.script.ship.Station;
import game.script.storage.Storage;
import logic.aaa.C5783aaP;
import logic.baa.C6200aiQ;
import logic.baa.aDJ;
import logic.render.GUIModule;
import logic.res.code.C5663aRz;
import logic.swing.C5378aHa;
import logic.ui.ComponentManager;
import logic.ui.Panel;
import logic.ui.item.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.newmarket.C2331eF;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

/* renamed from: a.GH */
/* compiled from: a */
public class C0440GH {
    /* access modifiers changed from: private */
    public C2331eF aoM;
    /* access modifiers changed from: private */
    public JSpinner aoS;
    /* access modifiers changed from: private */
    public JComboBox ejv;
    /* access modifiers changed from: private */
    public JTextField ejw;
    /* access modifiers changed from: private */
    public JTextField eyh;
    /* access modifiers changed from: private */
    public JList eyk;
    /* renamed from: kj */
    public IAddonProperties f616kj;
    /* renamed from: nM */
    public InternalFrame f617nM;
    private C3428rT<C5783aaP> aoN = new C0452k();
    private JButton aoO;
    private JButton aoP;
    private C2495fo apa;
    private JTextField ejx;
    /* access modifiers changed from: private */
    private C6200aiQ<aDJ> eyi;
    /* access modifiers changed from: private */
    private ItemIcon eyj;
    /* renamed from: nP */
    private C3428rT<C3689to> f618nP = new C0450i();

    public C0440GH(IAddonProperties vWVar, C2331eF eFVar, C2495fo foVar) {
        this.aoM = eFVar;
        this.f616kj = vWVar;
        this.apa = foVar;
        m3356Im();
        m3371xL();
        m3355Fa();
        GUIModule bhd = C5916acs.getSingolton().getGuiModule();
        this.f617nM.setLocation((bhd.getScreenWidth() / 2) - (this.f617nM.getWidth() / 2), ((bhd.getScreenHeight() / 2) - (this.f617nM.getHeight() / 2)) - 50);
        this.f617nM.setVisible(true);
    }

    /* access modifiers changed from: private */
    public void clear() {
        hide();
    }

    /* renamed from: xL */
    private void m3371xL() {
        this.ejw.setEnabled(false);
        this.ejx.setEnabled(false);
        this.ejv.removeAllItems();
        this.ejv.addItem(new C0441a(C3666tU.VERY_SHORT, this.f616kj.translate("Very Short")));
        this.ejv.addItem(new C0441a(C3666tU.SHORT, this.f616kj.translate("Short")));
        this.ejv.addItem(new C0441a(C3666tU.AVERAGE, this.f616kj.translate("Average")));
        this.ejv.addItem(new C0441a(C3666tU.LONG, this.f616kj.translate("Long")));
        this.ejv.addItem(new C0441a(C3666tU.VERY_LONG, this.f616kj.translate("Very Long")));
        this.ejv.setSelectedIndex(0);
        this.eyk.setCellRenderer(new C0451j());
        this.eyi = new C0447g();
        if (this.f616kj.getPlayer().bhE() instanceof Station) {
            Station bf = (Station) this.f616kj.getPlayer().bhE();
            DefaultListModel defaultListModel = new DefaultListModel();
            this.eyk.setModel(defaultListModel);
            this.f617nM.mo4911Kk();
            ((Player) C3582se.m38985a(this.f616kj.getPlayer(), (C6144ahM<?>) new C0448h(defaultListModel))).dxU();
            if (this.f616kj.getPlayer().bQx() != null) {
                Iterator<Item> iterator = this.f616kj.getPlayer().bQx().afI().getIterator();
                while (iterator.hasNext()) {
                    defaultListModel.addElement(iterator.next());
                }
            }
            for (Bay ajm : this.f616kj.getPlayer().mo14346aD(bf).mo20056KU()) {
                if (!ajm.isEmpty() && ajm.mo9625al() != this.f616kj.getPlayer().bQx()) {
                    defaultListModel.addElement(ajm.cIB());
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void bDG() {
        this.aoO.setEnabled(false);
        if (bvm()) {
            long parseLong = Long.parseLong(this.eyh.getText()) * ((long) ((Integer) this.aoS.getValue()).intValue());
            this.ejx.setText(Long.toString(parseLong));
            ((PlayerAssistant) C3582se.m38985a(this.f616kj.getPlayer().dyl(), (C6144ahM<?>) new C0445e())).mo11983fk(parseLong);
            this.aoO.setEnabled(true);
        }
    }

    /* renamed from: Fa */
    private void m3355Fa() {
        this.eyh.getDocument().addDocumentListener(new C0446f());
        this.eyk.addListSelectionListener(new C0443c());
        this.aoO.addActionListener(new C0444d());
        this.aoP.addActionListener(new C0442b());
    }

    /* access modifiers changed from: private */
    public void bDH() {
        Item auq = (Item) this.eyk.getSelectedValue();
        if (auq != null) {
            this.aoS.setModel(new SpinnerNumberModel(Integer.valueOf(auq.mo302Iq()), 1, Integer.valueOf(auq.mo302Iq()), this.aoS.getModel().getStepSize()));
        }
    }

    /* access modifiers changed from: private */
    public boolean bvm() {
        if (this.eyk.getSelectedValue() == null) {
            return false;
        }
        try {
            Long.parseLong(this.eyh.getText());
            if (this.ejv.getSelectedItem() != null) {
                return true;
            }
            return false;
        } catch (RuntimeException e) {
            return false;
        }
    }

    /* renamed from: Im */
    private void m3356Im() {
        this.f617nM = this.f616kj.bHv().mo16795c(this.apa.getClass(), "sellorder.xml");
        this.aoO = this.f617nM.mo4913cb("okButton");
        this.aoP = this.f617nM.mo4913cb("cancelButton");
        this.eyh = this.f617nM.mo4920ci("unityValue");
        this.ejw = this.f617nM.mo4915cd("tax");
        this.aoS = this.f617nM.mo4915cd("amount");
        this.ejx = this.f617nM.mo4920ci("totalValue");
        this.ejv = this.f617nM.mo4914cc("lifeTime");
        this.eyj = this.f617nM.mo4915cd("picture");
        this.eyk = this.f617nM.mo4915cd("itemList");
        this.aoO.setEnabled(false);
    }

    /* access modifiers changed from: private */
    public void hide() {
        this.f617nM.setVisible(false);
        removeListeners();
        this.f617nM.destroy();
    }

    private void removeListeners() {
        this.f616kj.aVU().mo13964a(this.f618nP);
        this.f616kj.aVU().mo13964a(this.aoN);
    }

    /* access modifiers changed from: private */
    public boolean isVisible() {
        return false;
    }

    /* renamed from: a.GH$a */
    private class C0441a<T> {
        private String name;
        private T object;

        public C0441a(T t, String str) {
            this.name = str;
            this.object = t;
        }

        public T getObject() {
            return this.object;
        }

        public String toString() {
            return this.name;
        }
    }

    /* renamed from: a.GH$k */
    /* compiled from: a */
    class C0452k extends C3428rT<C5783aaP> {
        C0452k() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (C0440GH.this.isVisible()) {
                C0440GH.this.hide();
            }
        }
    }

    /* renamed from: a.GH$i */
    /* compiled from: a */
    class C0450i extends C3428rT<C3689to> {
        C0450i() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (C0440GH.this.isVisible()) {
                C0440GH.this.hide();
                toVar.adC();
            }
        }
    }

    /* renamed from: a.GH$j */
    /* compiled from: a */
    class C0451j extends C5517aMj {
        C0451j() {
        }

        /* renamed from: a */
        public java.awt.Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            Panel dAx = aur.dAx();
            if (obj instanceof Item) {
                Item auq = (Item) obj;
                LabeledIcon cd = dAx.mo4915cd("itemPicture");
                JLabel cf = dAx.mo4917cf("itemName");
                JLabel cf2 = dAx.mo4917cf("origin");
                cd.mo2605a((Icon) new C2539gY(C0440GH.this.f616kj.bHv().adz().getImage(String.valueOf(C5378aHa.ITEMS.getPath()) + auq.bAP().mo12100sK().getHandle())));
                Picture a = cd.mo2601a(LabeledIcon.C0530a.NORTH_WEST);
                if (auq instanceof Component) {
                    ComponentManager.getCssHolder(a).setAttribute("class", "size" + ((ComponentType) auq.bAP()).cuJ());
                    a.setVisible(true);
                } else {
                    a.setVisible(false);
                }
                cf.setText(auq.bAP().mo19891ke().get());
                if (auq.bNh() instanceof CargoHold) {
                    cf2.setText(C0440GH.this.f616kj.translate("Cargo Hold"));
                } else if (auq.bNh() instanceof Storage) {
                    cf2.setText(C0440GH.this.f616kj.translate("Storage"));
                } else {
                    cf2.setText(C0440GH.this.f616kj.translate("Hangar"));
                }
            }
            return dAx;
        }
    }

    /* renamed from: a.GH$g */
    /* compiled from: a */
    class C0447g implements C6200aiQ<aDJ> {
        C0447g() {
        }

        /* renamed from: a */
        public void mo1143a(aDJ adj, C5663aRz arz, Object obj) {
            if (adj.isDisposed()) {
                C0440GH.this.clear();
            }
        }
    }

    /* renamed from: a.GH$h */
    /* compiled from: a */
    class C0448h implements C6144ahM<Storage> {
        private final /* synthetic */ DefaultListModel itf;

        C0448h(DefaultListModel defaultListModel) {
            this.itf = defaultListModel;
        }

        /* renamed from: e */
        public void mo1931n(Storage qzVar) {
            SwingUtilities.invokeLater(new C0449a(qzVar, this.itf));
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }

        /* renamed from: a.GH$h$a */
        class C0449a implements Runnable {
            private final /* synthetic */ Storage dvt;
            private final /* synthetic */ DefaultListModel itf;

            C0449a(Storage qzVar, DefaultListModel defaultListModel) {
                this.dvt = qzVar;
                this.itf = defaultListModel;
            }

            public void run() {
                C0440GH.this.f617nM.mo4912Kl();
                if (this.dvt != null) {
                    Iterator<Item> iterator = this.dvt.getIterator();
                    while (iterator.hasNext()) {
                        Item next = iterator.next();
                        if (next.aNP()) {
                            this.itf.addElement(next);
                        }
                    }
                }
            }
        }
    }

    /* renamed from: a.GH$e */
    /* compiled from: a */
    class C0445e implements C6144ahM<Long> {
        C0445e() {
        }

        /* renamed from: a */
        public void mo1931n(Long l) {
            C0440GH.this.ejw.setText(Long.toString(l.longValue()));
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }
    }

    /* renamed from: a.GH$f */
    /* compiled from: a */
    class C0446f implements DocumentListener {
        C0446f() {
        }

        public void changedUpdate(DocumentEvent documentEvent) {
        }

        public void insertUpdate(DocumentEvent documentEvent) {
            C0440GH.this.bDG();
        }

        public void removeUpdate(DocumentEvent documentEvent) {
            C0440GH.this.bDG();
        }
    }

    /* renamed from: a.GH$c */
    /* compiled from: a */
    class C0443c implements ListSelectionListener {
        C0443c() {
        }

        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            C0440GH.this.bDH();
            C0440GH.this.bDG();
        }
    }

    /* renamed from: a.GH$d */
    /* compiled from: a */
    class C0444d implements ActionListener {
        C0444d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            BulkItem atv;
            if (C0440GH.this.bvm()) {
                int intValue = ((Integer) C0440GH.this.aoS.getValue()).intValue();
                Item auq = (Item) C0440GH.this.eyk.getSelectedValue();
                if (auq instanceof BulkItem) {
                    BulkItem atv2 = (BulkItem) C0440GH.this.eyk.getSelectedValue();
                    if (intValue < atv2.mo302Iq()) {
                        atv = atv2.mo11560AQ(intValue);
                        C0440GH.this.aoM.mo17963a(atv, 1, Long.parseLong(C0440GH.this.eyh.getText()), (long) ((C3666tU) ((C0441a) C0440GH.this.ejv.getSelectedItem()).getObject()).getLifeTime());
                    }
                }
                atv = auq;
                C0440GH.this.aoM.mo17963a(atv, 1, Long.parseLong(C0440GH.this.eyh.getText()), (long) ((C3666tU) ((C0441a) C0440GH.this.ejv.getSelectedItem()).getObject()).getLifeTime());
            }
            C0440GH.this.hide();
        }
    }

    /* renamed from: a.GH$b */
    /* compiled from: a */
    class C0442b implements ActionListener {
        C0442b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C0440GH.this.hide();
        }
    }
}
