package p001a;

import game.geometry.Vec3d;
import logic.aaa.C2235dB;

import java.util.List;
import java.util.Set;

/* renamed from: a.nX */
/* compiled from: a */
public interface C3055nX<E extends C2235dB, B extends C0461GN> {

    /* renamed from: a */
    C0463GP<E> mo8631a(E e, int i);

    /* renamed from: a */
    C3387qy<B> mo8632a(B b);

    /* renamed from: a */
    void mo8634a(float f, C3057b<E, B> bVar);

    /* renamed from: a */
    void mo8636a(C3057b<E, B> bVar);

    void dispose();

    /* renamed from: gZ */
    void mo8641gZ();

    /* renamed from: p */
    C3056a<E, B> mo8644p(Vec3d ajr);

    /* renamed from: a.nX$a */
    public interface C3056a<E extends C2235dB, B extends C0461GN> {
        /* renamed from: B */
        <T> T mo11189B(Object obj);

        /* renamed from: IN */
        float mo11190IN();

        /* renamed from: IO */
        long mo11191IO();

        /* renamed from: IP */
        List<List<C0463GP<E>>> mo11192IP();

        /* renamed from: a */
        void mo11193a(float f, Set<C6625aqZ> set);

        /* renamed from: bZ */
        void mo11194bZ(long j);

        /* renamed from: cy */
        List<C0463GP<E>> mo11195cy(int i);

        /* renamed from: e */
        void mo11196e(Object obj, Object obj2);

        Vec3d getPosition();

        int getTag();

        void setTag(int i);
    }

    /* renamed from: a.nX$b */
    /* compiled from: a */
    public interface C3057b<E extends C2235dB, B extends C0461GN> {
        /* renamed from: a */
        void mo1933a(C3056a<E, B> aVar);
    }
}
