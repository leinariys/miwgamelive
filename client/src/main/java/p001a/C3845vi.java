package p001a;

import game.script.avatar.AvatarSector;
import game.script.avatar.body.BodyPiece;
import taikodom.render.scene.custom.RCustomMesh;

import java.util.Collection;

/* renamed from: a.vi */
/* compiled from: a */
public interface C3845vi {

    /* renamed from: a */
    RCustomMesh mo3097a(AvatarSector.C0955a aVar);

    /* renamed from: a */
    void mo3098a(AvatarSector nw, boolean z);

    /* renamed from: a */
    void mo3099a(C3846a aVar);

    void akN();

    Collection<BodyPiece> akO();

    void akP();

    boolean akQ();

    void akR();

    /* renamed from: b */
    boolean mo3105b(AvatarSector.C0955a aVar);

    /* renamed from: a.vi$b */
    /* compiled from: a */
    public enum C3847b {
        DIFFUSE("_dfs"),
        NORMAL("_nrm");

        private final String suffix;

        private C3847b(String str) {
            this.suffix = str;
        }

        public String getSuffix() {
            return this.suffix;
        }
    }

    /* renamed from: a.vi$a */
    public interface C3846a {
        /* renamed from: a */
        void mo4551a(C3845vi viVar, boolean z);
    }
}
