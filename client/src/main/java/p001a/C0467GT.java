package p001a;

import game.engine.DataGameEvent;
import game.io.IWriteExternal;

/* renamed from: a.GT */
/* compiled from: a */
public class C0467GT {
    /* renamed from: a */
    public static boolean m3408a(IWriteExternal att) {
        if (att instanceof C1507WB) {
            return ((C1507WB) att).mo6493Xc();
        }
        return false;
    }

    /* renamed from: b */
    public static DataGameEvent m3409b(IWriteExternal att) {
        if (att instanceof C3380qs) {
            return ((C3380qs) att).mo21517PM();
        }
        return null;
    }
}
