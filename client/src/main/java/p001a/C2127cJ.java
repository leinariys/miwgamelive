package p001a;

import game.geometry.Vec3d;
import game.script.Actor;
import game.script.simulation.Space;
import logic.EngineGame;
import taikodom.render.camera.ChaseCamera;

/* renamed from: a.cJ */
/* compiled from: a */
public class C2127cJ {
    /* renamed from: vR */
    private final Actor f6027vR;
    /* renamed from: vS */
    private final EngineGame f6028vS;
    private Vec3d center = new Vec3d();
    /* renamed from: vT */
    private ChaseCamera f6029vT = null;

    public C2127cJ(EngineGame aou, Actor cr) {
        this.f6028vS = aou;
        this.f6027vR = cr;
    }

    /* renamed from: iF */
    public EngineGame mo17547iF() {
        return this.f6028vS;
    }

    /* renamed from: gZ */
    public void mo17546gZ() {
        Space Zc = this.f6027vR.mo965Zc();
        if (Zc != null) {
            Zc.mo1913gZ();
        }
    }
}
