package p001a;

import java.util.HashMap;
import java.util.Map;

/* renamed from: a.QK */
/* compiled from: a */
public class C1108QK {

    /* renamed from: OC */
    public static final String f1423OC = "UNDEFINED";
    public static final int dWG = 1;
    public static final int dWH = 2;
    public static final int dWI = 4;
    public static final int dWJ = 8;
    public static final int dWK = 3;
    public static final int dWL = 9;
    public static final int dWM = 6;
    public static final int dWN = 12;
    /* renamed from: Ow */
    public static Map<Integer, String> f1424Ow = new HashMap();
    /* renamed from: Ox */
    public static Map<String, Integer> f1425Ox = new HashMap();

    static {
        f1424Ow.put(1, "JOY_HAT_UP");
        f1424Ow.put(2, "JOY_HAT_RIGHT");
        f1424Ow.put(4, "JOY_HAT_DOWN");
        f1424Ow.put(8, "JOY_HAT_LEFT");
        f1424Ow.put(3, "JOY_HAT_UP_RIGHT");
        f1424Ow.put(9, "JOY_HAT_UP_LEFT");
        f1424Ow.put(6, "JOY_HAT_DOWN_RIGHT");
        f1424Ow.put(12, "JOY_HAT_DOWN_LEFT");
        f1425Ox.put("JOY_HAT_UP", 1);
        f1425Ox.put("JOY_HAT_RIGHT", 2);
        f1425Ox.put("JOY_HAT_DOWN", 4);
        f1425Ox.put("JOY_HAT_LEFT", 8);
        f1425Ox.put("JOY_HAT_UP_RIGHT", 3);
        f1425Ox.put("JOY_HAT_UP_LEFT", 9);
        f1425Ox.put("JOY_HAT_DOWN_RIGHT", 6);
        f1425Ox.put("JOY_HAT_DOWN_LEFT", 12);
    }

    /* renamed from: ao */
    public static int m8805ao(String str) {
        Integer num = f1425Ox.get(str);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    /* renamed from: bf */
    public static String m8806bf(int i) {
        String str = f1424Ow.get(Integer.valueOf(i));
        return str != null ? str : "UNDEFINED";
    }

    /* renamed from: n */
    public static int m8807n(float f, float f2) {
        if (f == 0.0f) {
            if (f2 > 0.0f) {
                return 4;
            }
            if (f2 < 0.0f) {
                return 1;
            }
        }
        if (f2 == 0.0f) {
            if (f > 0.0f) {
                return 2;
            }
            if (f < 0.0f) {
                return 8;
            }
        }
        if (f2 > 0.0f) {
            if (f > 0.0f) {
                return 6;
            }
            if (f < 0.0f) {
                return 12;
            }
        }
        if (f2 < 0.0f) {
            if (f > 0.0f) {
                return 3;
            }
            if (f < 0.0f) {
                return 9;
            }
        }
        return 0;
    }
}
