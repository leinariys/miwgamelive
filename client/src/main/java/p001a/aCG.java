package p001a;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aCG */
/* compiled from: a */
public final class aCG {
    public static final aCG hwN = new aCG("GrannyHasPosition", grannyJNI.GrannyHasPosition_get());
    public static final aCG hwO = new aCG("GrannyHasOrientation", grannyJNI.GrannyHasOrientation_get());
    public static final aCG hwP = new aCG("GrannyHasScaleShear", grannyJNI.GrannyHasScaleShear_get());
    private static aCG[] hwQ = {hwN, hwO, hwP};

    /* renamed from: pF */
    private static int f2461pF = 0;

    /* renamed from: pG */
    private final int f2462pG;

    /* renamed from: pH */
    private final String f2463pH;

    private aCG(String str) {
        this.f2463pH = str;
        int i = f2461pF;
        f2461pF = i + 1;
        this.f2462pG = i;
    }

    private aCG(String str, int i) {
        this.f2463pH = str;
        this.f2462pG = i;
        f2461pF = i + 1;
    }

    private aCG(String str, aCG acg) {
        this.f2463pH = str;
        this.f2462pG = acg.f2462pG;
        f2461pF = this.f2462pG + 1;
    }

    /* renamed from: xk */
    public static aCG m13143xk(int i) {
        if (i < hwQ.length && i >= 0 && hwQ[i].f2462pG == i) {
            return hwQ[i];
        }
        for (int i2 = 0; i2 < hwQ.length; i2++) {
            if (hwQ[i2].f2462pG == i) {
                return hwQ[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + aCG.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f2462pG;
    }

    public String toString() {
        return this.f2463pH;
    }
}
