package p001a;

import gnu.trove.THashMap;
import logic.render.IEngineGraphics;
import logic.render.bink.C0972OJ;
import logic.res.ILoaderImageInterface;
import logic.ui.item.Picture;
import org.mozilla1.classfile.C0147Bi;
import taikodom.infra.script.I18NString;
import taikodom.render.BinkTexture;
import taikodom.render.TexBackedImage;
import taikodom.render.enums.TexMagFilter;
import taikodom.render.enums.TexMinFilter;
import taikodom.render.loader.provider.C3176ol;
import taikodom.render.loader.provider.FilePath;
import taikodom.render.textures.BinkDataSource;
import taikodom.render.textures.FlashTexture;

import java.awt.*;
import java.util.Collections;
import java.util.Map;

/* renamed from: a.atC  reason: case insensitive filesystem */
/* compiled from: a */
public class C6758atC implements ILoaderImageInterface {
    private FilePath gAI;
    private Map<String, Image> gAJ = Collections.synchronizedMap(new THashMap());
    private Map<String, Cursor> gAK = Collections.synchronizedMap(new THashMap());

    public C6758atC(IEngineGraphics abd) {
        this.gAK.put("default-cursor", Cursor.getPredefinedCursor(0));
        this.gAK.put("crosshair-cursor", Cursor.getPredefinedCursor(1));
        this.gAK.put("text-cursor", Cursor.getPredefinedCursor(2));
        this.gAK.put("wait-cursor", Cursor.getPredefinedCursor(3));
        this.gAK.put("sw-resize-cursor", Cursor.getPredefinedCursor(4));
        this.gAK.put("se-resize-cursor", Cursor.getPredefinedCursor(5));
        this.gAK.put("nw-resize-cursor", Cursor.getPredefinedCursor(6));
        this.gAK.put("ne-resize-cursor", Cursor.getPredefinedCursor(7));
        this.gAK.put("n-resize-cursor", Cursor.getPredefinedCursor(8));
        this.gAK.put("s-resize-cursor", Cursor.getPredefinedCursor(9));
        this.gAK.put("w-resize-cursor", Cursor.getPredefinedCursor(10));
        this.gAK.put("e-resize-cursor", Cursor.getPredefinedCursor(11));
        this.gAK.put("hand-cursor", Cursor.getPredefinedCursor(12));
        this.gAK.put("move-cursor", Cursor.getPredefinedCursor(13));
    }

    /* renamed from: jX */
    private Image m25821jX(String str) {
        float f;
        float f2;
        String eH = m25820eH(str);
        C0972OJ.wBinkSoundUseDirectSound(0);
        BinkDataSource binkDataSource = new BinkDataSource(C0972OJ.m7939d(this.gAI.concat(eH).getFile().getPath(), (long) BinkTexture.BINKALPHA), 1056768);
        binkDataSource.setFileName(eH);
        BinkTexture binkTexture = new BinkTexture(binkDataSource);
        binkTexture.setName(eH);
        binkTexture.setMagFilter(TexMagFilter.LINEAR);
        binkTexture.setMinFilter(TexMinFilter.LINEAR);
        if (binkTexture.getType() == 3553) {
            f2 = ((float) binkTexture.getVideoSizeX()) / ((float) binkTexture.getSizeX());
            f = ((float) binkTexture.getVideoSizeY()) / ((float) binkTexture.getSizeY());
        } else {
            f = 1.0f;
            f2 = 1.0f;
        }
        return new TexBackedImage(binkTexture.getSizeY(), binkTexture.getSizeX(), f2, f, binkTexture);
    }

    /* renamed from: eH */
    private String m25820eH(String str) {
        String cGs = I18NString.getCurrentLocation();
        if (cGs == null) {
            return str;
        }
        String replaceAll = str.replaceAll("([.][^.]+)$", "_" + cGs + "$1");
        if (this.gAI.concat(replaceAll).exists()) {
            return replaceAll;
        }
        return str;
    }

    /* renamed from: jY */
    private Image m25822jY(String str) {
        String eH = m25820eH(str);
        FilePath aC = this.gAI.concat(eH);
        FlashTexture flashTexture = new FlashTexture(new C3176ol(aC.mo2256BI(), aC.length()));
        flashTexture.setName(eH);
        return new TexBackedImage(flashTexture.getSizeY(), flashTexture.getSizeX(), flashTexture);
    }

    public Image getImage(String str) {
        String str2;
        FilePath aC;
        Image createImage;
        String str3;
        if (str.startsWith("res://")) {
            str = str.substring("res://".length());
        }
        Image image = this.gAJ.get(str);
        if (image == null) {
            image = null;
        }
        if (image != null) {
            return image;
        }
        if (str.endsWith(".bik")) {
            return m25821jX(str);
        }
        if (str.endsWith(".swf")) {
            return m25822jY(str);
        }
        if (!str.startsWith("data/")) {
            str2 = "/data/gui/imageset/" + str.replaceAll("[/]material", "");
        } else {
            str2 = C0147Bi.SEPARATOR + str.replaceAll("[/]material", "");
        }
        String cGs = I18NString.getCurrentLocation();
        Image image2 = this.gAJ.get(str2);
        if (image2 == null) {
            image2 = null;
        }
        if (image2 != null) {
            return image2;
        }
        FilePath aC2 = this.gAI.concat(String.valueOf(str2) + "_" + cGs + ".png");
        Image image3 = this.gAJ.get(aC2.getPath());
        if (image3 == null) {
            image3 = null;
        }
        if (image3 != null) {
            return image3;
        }
        if (aC2.exists()) {
            str3 = String.valueOf(str2) + "_" + cGs + ".png";
            createImage = Toolkit.getDefaultToolkit().createImage(aC2.mo2255BH());
            aC = aC2;
        } else {
            String str4 = String.valueOf(str2) + ".png";
            aC = this.gAI.concat(str4);
            if (aC.exists()) {
                createImage = Toolkit.getDefaultToolkit().createImage(aC.mo2255BH());
                str3 = str4;
            } else {
                throw new IllegalArgumentException("Image '" + str4 + "' was not found in resources dir");
            }
        }
        MediaTracker mediaTracker = new MediaTracker(new Picture());
        mediaTracker.addImage(createImage, 0);
        try {
            mediaTracker.waitForID(0, 10000);
            this.gAJ.put(str, createImage);
            this.gAJ.put(str3, createImage);
            this.gAJ.put(aC.getPath(), createImage);
            return createImage;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: j */
    public void mo16003j(FilePath ain) {
        this.gAI = ain;
    }

    /* renamed from: a */
    public Cursor getNull(String str, Point point) {
        String str2;
        if (point == null || (point.x == 0 && point.y == 0)) {
            str2 = str;
        } else {
            str2 = String.valueOf(str) + ":" + point;
        }
        Cursor cursor = this.gAK.get(str2);
        if (cursor != null) {
            return cursor;
        }
        Cursor createCustomCursor = Toolkit.getDefaultToolkit().createCustomCursor(getImage(str), point, str);
        this.gAK.put(str2, createCustomCursor);
        return createCustomCursor;
    }
}
