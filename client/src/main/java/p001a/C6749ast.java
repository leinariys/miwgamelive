package p001a;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: a.ast  reason: case insensitive filesystem */
/* compiled from: a */
public class C6749ast extends Thread {
    public static C6749ast gvb;
    List<C1285Sv> guY = new ArrayList();
    String[] guZ = new String[0];
    String gva = "          ";

    public static C6749ast cun() {
        if (gvb == null) {
            gvb = new C6749ast();
            gvb.setDaemon(true);
            gvb.start();
        }
        return gvb;
    }

    /* renamed from: a */
    public synchronized void mo15995a(C1285Sv sv) {
        if (this.guY.size() + 2 > this.guZ.length) {
            this.guZ = new String[(this.guY.size() + 2)];
        }
        this.guY.add(sv);
    }

    public void run() {
        while (true) {
            try {
                cuo();
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
        }
    }

    private synchronized void cuo() {
        cuy();
        cuz();
        cuv();
        cux();
        cuw();
        cuq();
        cur();
        cus();
        cut();
        cuu();
        cup();
    }

    private void cup() {
        resetLine();
        this.guZ[0] = "Runs:";
        int i = 1;
        Iterator<C1285Sv> it = this.guY.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                cuA();
                return;
            } else {
                this.guZ[i2] = new StringBuilder().append(it.next().eeo.runs).toString();
                i = i2 + 1;
            }
        }
    }

    private void cuq() {
        resetLine();
        this.guZ[0] = "RTime:";
        int i = 1;
        Iterator<C1285Sv> it = this.guY.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                cuA();
                return;
            } else {
                this.guZ[i2] = it.next().eet.gTY;
                i = i2 + 1;
            }
        }
    }

    private void cur() {
        resetLine();
        this.guZ[0] = "Chat:";
        int i = 1;
        Iterator<C1285Sv> it = this.guY.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                cuA();
                return;
            } else {
                this.guZ[i2] = it.next().ees.gTY;
                i = i2 + 1;
            }
        }
    }

    private void cus() {
        resetLine();
        this.guZ[0] = "SMine:";
        int i = 1;
        Iterator<C1285Sv> it = this.guY.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                cuA();
                return;
            } else {
                this.guZ[i2] = it.next().eer.gTY;
                i = i2 + 1;
            }
        }
    }

    private void cut() {
        resetLine();
        this.guZ[0] = "Kill:";
        int i = 1;
        Iterator<C1285Sv> it = this.guY.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                cuA();
                return;
            } else {
                this.guZ[i2] = it.next().eeq.gTY;
                i = i2 + 1;
            }
        }
    }

    private void cuu() {
        resetLine();
        this.guZ[0] = "Travel:";
        int i = 1;
        Iterator<C1285Sv> it = this.guY.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                cuA();
                return;
            } else {
                this.guZ[i2] = it.next().eep.gTY;
                i = i2 + 1;
            }
        }
    }

    private void cuv() {
        resetLine();
        this.guZ[0] = "Station:";
        int i = 1;
        Iterator<C1285Sv> it = this.guY.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                cuA();
                return;
            } else {
                this.guZ[i2] = it.next().mo5504eT().getName();
                i = i2 + 1;
            }
        }
    }

    private void cuw() {
        resetLine();
        this.guZ[0] = "State:";
        int i = 1;
        Iterator<C1285Sv> it = this.guY.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                cuA();
                return;
            } else {
                this.guZ[i2] = it.next().eeo.gTY;
                i = i2 + 1;
            }
        }
    }

    private void cux() {
        resetLine();
        this.guZ[0] = "Node:";
        int i = 1;
        Iterator<C1285Sv> it = this.guY.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                cuA();
                return;
            } else {
                this.guZ[i2] = it.next().eev;
                i = i2 + 1;
            }
        }
    }

    private void cuy() {
        resetLine();
        int i = 1;
        Iterator<C1285Sv> it = this.guY.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                cuA();
                return;
            } else {
                this.guZ[i2] = it.next().eeo.getClass().getSimpleName();
                i = i2 + 1;
            }
        }
    }

    private void cuz() {
        resetLine();
        this.guZ[0] = "Player:";
        int i = 1;
        Iterator<C1285Sv> it = this.guY.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                cuA();
                return;
            } else {
                this.guZ[i2] = it.next().mo5503dL().getName();
                i = i2 + 1;
            }
        }
    }

    private void resetLine() {
        for (int i = 0; i < this.guZ.length; i++) {
            this.guZ[i] = "";
        }
    }

    private void cuA() {
        String[] strArr = this.guZ;
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            String str = strArr[i];
            if (str == null) {
                str = "";
            }
            if (str.length() > 9) {
                str = str.substring(0, 8);
            }
            System.out.print(str);
            System.out.print(this.gva.substring(0, 9 - str.length()));
        }
        System.out.println();
    }
}
