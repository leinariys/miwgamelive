package p001a;

import com.hoplon.geometry.Vec3f;
import logic.baa.C1616Xf;
import logic.res.html.DataClassSerializer;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.QY */
/* compiled from: a */
public class C1125QY extends DataClassSerializer {
    public static final C1125QY dXr = new C1125QY();

    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) {
        if (obj == null) {
            objectOutput.writeBoolean(false);
            return;
        }
        objectOutput.writeBoolean(true);
        Vec3f vec3f = (Vec3f) obj;
        objectOutput.writeFloat(vec3f.x);
        objectOutput.writeFloat(vec3f.y);
        objectOutput.writeFloat(vec3f.z);
    }

    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) {
        if (!objectInput.readBoolean()) {
            return null;
        }
        return new Vec3f(objectInput.readFloat(), objectInput.readFloat(), objectInput.readFloat());
    }

    /* renamed from: z */
    public Object mo3598z(Object obj) {
        Vec3f vec3f = (Vec3f) obj;
        return new Vec3f(vec3f.x, vec3f.y, vec3f.z);
    }

    /* renamed from: yl */
    public boolean mo3597yl() {
        return true;
    }

    /* renamed from: b */
    public Object mo3591b(C1616Xf xf) {
        return null;
    }
}
