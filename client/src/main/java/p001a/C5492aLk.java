package p001a;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.aLk  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C5492aLk extends C0596IQ {
    private final JTable table;

    public C5492aLk(JTable jTable) {
        this.table = jTable;
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public abstract JPopupMenu mo9938j(int i, int i2);

    /* access modifiers changed from: protected */
    /* renamed from: H */
    public void mo2812H(int i, int i2) {
        int rowAtPoint = this.table.rowAtPoint(new Point(i, i2));
        m16273xZ(rowAtPoint);
        JPopupMenu j = mo9938j(rowAtPoint, this.table.getSelectedColumn());
        if (j != null) {
            j.show(this.table, i, i2);
        }
    }

    /* renamed from: xZ */
    private void m16273xZ(int i) {
        if (i != -1 && i != this.table.getSelectedRow()) {
            this.table.getSelectionModel().setSelectionInterval(i, i);
        }
    }
}
