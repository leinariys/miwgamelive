package p001a;

/* renamed from: a.mu */
/* compiled from: a */
public final class C3010mu extends aOG {
    public static final C5961adl aCM = new C5961adl("MouseButtonEvent");
    private boolean aCJ;
    private int aCK;
    private int aCL;
    private int button;
    private int modifiers;

    /* renamed from: x */
    private float f8669x;

    /* renamed from: y */
    private float f8670y;

    public C3010mu(int i, boolean z, float f, float f2, int i2, int i3, int i4) {
        this.button = i;
        this.aCJ = z;
        this.f8669x = f;
        this.f8670y = f2;
        this.aCK = i2;
        this.aCL = i3;
        this.modifiers = i4;
    }

    public final int getButton() {
        return this.button;
    }

    public final boolean getState() {
        return this.aCJ;
    }

    public final boolean isPressed() {
        return this.aCJ;
    }

    public final float getX() {
        return this.f8669x;
    }

    public final float getY() {
        return this.f8670y;
    }

    public final int getScreenX() {
        return this.aCK;
    }

    public final int getScreenY() {
        return this.aCL;
    }

    /* renamed from: Fp */
    public C5961adl mo2005Fp() {
        return aCM;
    }

    public int getModifiers() {
        return this.modifiers;
    }
}
