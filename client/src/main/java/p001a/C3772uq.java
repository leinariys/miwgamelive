package p001a;

import logic.res.XmlNode;

import java.util.Collection;

/* renamed from: a.uq */
/* compiled from: a */
public abstract class C3772uq extends C3598sl {
    public C3772uq(String str) {
        super(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Object mo22461a(C6146ahO aho, XmlNode agy, Collection<Object> collection) {
        for (XmlNode next : agy.getListChildrenTag()) {
            if ("item".equals(next.getTagName())) {
                collection.add(aho.mo13513f(next));
            }
        }
        return collection;
    }
}
