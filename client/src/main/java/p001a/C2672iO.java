package p001a;

import game.network.channel.client.ClientConnect;
import logic.res.html.MessageContainer;

import java.util.Map;

/* renamed from: a.iO */
/* compiled from: a */
public class C2672iO {
    Map<ClientConnect, ServerSocketSnapshot> aed = MessageContainer.dgM();

    /* renamed from: a */
    public void mo19477a(ServerSocketSnapshot mCVar) {
        synchronized (this.aed) {
            this.aed.put(mCVar.getClientConnect(), mCVar);
            mCVar.mo20453a(this);
        }
    }

    /* renamed from: b */
    public void mo19479b(ServerSocketSnapshot mCVar) {
        synchronized (this.aed) {
            this.aed.remove(mCVar.getClientConnect());
            mCVar.mo20453a((C2672iO) null);
        }
    }

    public int count() {
        return this.aed.size();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo19478b(ClientConnect bVar, byte[] bArr, int i, int i2) {
        synchronized (this.aed) {
            for (ServerSocketSnapshot next : this.aed.values()) {
                if (!next.getClientConnect().equals(bVar)) {
                    next.mo20454c(bVar, bArr, i, i2);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo19476a(ClientConnect bVar, ClientConnect bVar2, byte[] bArr, int i, int i2) {
        ServerSocketSnapshot mCVar = this.aed.get(bVar2);
        if (mCVar != null) {
            mCVar.mo20454c(bVar, bArr, i, i2);
        }
    }
}
