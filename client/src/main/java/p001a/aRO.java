package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.aRO */
/* compiled from: a */
public class aRO {
    /* renamed from: N */
    public static int m17755N(Vec3f vec3f, Vec3f vec3f2) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        int i6 = vec3f.x < (-vec3f2.x) ? 1 : 0;
        if (vec3f.x > vec3f2.x) {
            i = 8;
        } else {
            i = 0;
        }
        int i7 = i | i6;
        if (vec3f.y < (-vec3f2.y)) {
            i2 = 2;
        } else {
            i2 = 0;
        }
        int i8 = i7 | i2;
        if (vec3f.y > vec3f2.y) {
            i3 = 16;
        } else {
            i3 = 0;
        }
        int i9 = i8 | i3;
        if (vec3f.z < (-vec3f2.z)) {
            i4 = 4;
        } else {
            i4 = 0;
        }
        int i10 = i4 | i9;
        if (vec3f.z > vec3f2.z) {
            i5 = 32;
        }
        return i10 | i5;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public static boolean m17756a(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4, float[] fArr, Vec3f vec3f5) {
        float f;
        C0763Kt bcE = C0763Kt.bcE();
        bcE.bcH().push();
        try {
            Vec3f vec3f6 = (Vec3f) bcE.bcH().get();
            Vec3f vec3f7 = (Vec3f) bcE.bcH().get();
            Vec3f vec3f8 = (Vec3f) bcE.bcH().get();
            Vec3f vec3f9 = (Vec3f) bcE.bcH().get();
            Vec3f vec3f10 = (Vec3f) bcE.bcH().get();
            Vec3f vec3f11 = (Vec3f) bcE.bcH().get();
            vec3f6.sub(vec3f4, vec3f3);
            vec3f6.scale(0.5f);
            vec3f7.add(vec3f4, vec3f3);
            vec3f7.scale(0.5f);
            vec3f8.sub(vec3f, vec3f7);
            vec3f9.sub(vec3f2, vec3f7);
            int N = m17755N(vec3f8, vec3f6);
            int N2 = m17755N(vec3f9, vec3f6);
            if ((N & N2) == 0) {
                float f2 = 0.0f;
                float f3 = fArr[0];
                vec3f10.sub(vec3f9, vec3f8);
                float f4 = 1.0f;
                vec3f11.set(0.0f, 0.0f, 0.0f);
                int i = 1;
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    float f5 = f4;
                    if (i3 >= 2) {
                        break;
                    }
                    int i4 = 0;
                    while (i4 != 3) {
                        if ((N & i) != 0) {
                            f = ((-C0647JL.m5594b(vec3f8, i4)) - (C0647JL.m5594b(vec3f6, i4) * f5)) / C0647JL.m5594b(vec3f10, i4);
                            if (f2 <= f) {
                                vec3f11.set(0.0f, 0.0f, 0.0f);
                                C0647JL.m5590a(vec3f11, i4, f5);
                            }
                            f = f2;
                        } else {
                            if ((N2 & i) != 0) {
                                f3 = Math.min(f3, ((-C0647JL.m5594b(vec3f8, i4)) - (C0647JL.m5594b(vec3f6, i4) * f5)) / C0647JL.m5594b(vec3f10, i4));
                                f = f2;
                            }
                            f = f2;
                        }
                        i <<= 1;
                        i4++;
                        f2 = f;
                    }
                    f4 = -1.0f;
                    i2 = i3 + 1;
                }
                if (f2 <= f3) {
                    fArr[0] = f2;
                    vec3f5.set(vec3f11);
                    bcE.bcH().pop();
                    return true;
                }
            }
            bcE.bcH().pop();
            return false;
        } catch (Throwable th) {
            bcE.bcH().pop();
            throw th;
        }
    }

    /* renamed from: j */
    public static boolean m17758j(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
        boolean z = true;
        if (vec3f.x > vec3f4.x || vec3f2.x < vec3f3.x) {
            z = false;
        }
        if (vec3f.z > vec3f4.z || vec3f2.z < vec3f3.z) {
            z = false;
        }
        if (vec3f.y > vec3f4.y || vec3f2.y < vec3f3.y) {
            return false;
        }
        return z;
    }

    /* renamed from: a */
    public static boolean m17757a(Vec3f[] vec3fArr, Vec3f vec3f, Vec3f vec3f2) {
        Vec3f vec3f3 = vec3fArr[0];
        Vec3f vec3f4 = vec3fArr[1];
        Vec3f vec3f5 = vec3fArr[2];
        if (Math.min(Math.min(vec3f3.x, vec3f4.x), vec3f5.x) <= vec3f2.x && Math.max(Math.max(vec3f3.x, vec3f4.x), vec3f5.x) >= vec3f.x && Math.min(Math.min(vec3f3.z, vec3f4.z), vec3f5.z) <= vec3f2.z && Math.max(Math.max(vec3f3.z, vec3f4.z), vec3f5.z) >= vec3f.z && Math.min(Math.min(vec3f3.y, vec3f4.y), vec3f5.y) <= vec3f2.y && Math.max(Math.max(vec3f3.y, vec3f4.y), vec3f5.y) >= vec3f.y) {
            return true;
        }
        return false;
    }
}
