package p001a;

import logic.thred.PoolThread;
import logic.thred.WatchDog;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/* renamed from: a.aDi  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C5282aDi extends Thread {
    private final SimpleDateFormat dBH = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS ");
    private CurrentTimeMilli cVg = SingletonCurrentTimeMilliImpl.CURRENT_TIME_MILLI;
    private PrintWriter eqC = new PrintWriter(System.out, true);
    private WatchDog hyo = new WatchDog();
    private int hyp;
    private long hyq;
    private long hyr = this.cVg.currentTimeMillis();
    private long hys = this.cVg.currentTimeMillis();

    public C5282aDi(String str) {
        super(str);
        setDaemon(true);
    }

    /* access modifiers changed from: protected */
    /* renamed from: cR */
    public abstract long mo8429cR(long j);

    /* renamed from: a */
    public void mo8428a(CurrentTimeMilli ahw) {
        this.cVg = ahw;
        this.hyr = ahw.currentTimeMillis();
        this.hys = ahw.currentTimeMillis();
    }

    public void run() {
        this.hyo.setPrintWriter(this.eqC);
        while (true) {
            doRun();
        }
    }

    /* access modifiers changed from: protected */
    public void doRun() {
        long cR = mo8429cR(this.cVg.currentTimeMillis());
        if (cR > 100) {
            PoolThread.sleep(10);
        } else if (cR > 50) {
            PoolThread.sleep(5);
        } else if (cR > 10) {
            PoolThread.sleep(1);
        } else {
            PoolThread.sleep(5);
        }
        long currentTimeMillis = this.cVg.currentTimeMillis();
        long cR2 = mo8429cR(currentTimeMillis);
        if (cR2 > 200) {
            this.hyo.caL();
            this.hyr = currentTimeMillis - cR2;
            this.hyp++;
            this.hys = 0;
            return;
        }
        if (this.hys == 0) {
            this.hyq += currentTimeMillis - this.hyr;
        }
        this.hys = currentTimeMillis;
        if (this.hyp > 5000 || (this.hyp > 0 && currentTimeMillis - this.hyr > 1000)) {
            String format = this.dBH.format(new Date(System.currentTimeMillis()));
            this.eqC.println(String.valueOf(format) + "----------------------- Glitch log --------------------------");
            this.eqC.println(String.valueOf(format) + " Glitch " + this.hyq + " ms " + this.hyp + " samples " + this.hyr);
            this.eqC.println(String.valueOf(format) + "--------------------------------------------------------------");
            this.hyo.mo14456f((Thread) null);
            this.eqC.println(String.valueOf(format) + "----------------------- Glitch log end --------------------------");
            this.eqC.flush();
            this.hyo.reset();
            this.hyq = 0;
            this.hyp = 0;
        }
    }

    public void setPrintWriter(PrintWriter printWriter) {
        this.eqC = printWriter;
    }
}
