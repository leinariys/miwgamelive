package p001a;

import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.ship.Ship;
import game.script.ship.categories.Freighter;
import logic.ui.C2698il;
import logic.ui.Panel;
import logic.ui.item.Progress;
import logic.ui.item.TextField;
import org.mozilla1.classfile.C0147Bi;
import taikodom.addon.IAddonProperties;
import taikodom.addon.components.TItemPanel;
import taikodom.addon.components.popup.DefaultMenuItems;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/* renamed from: a.aaH  reason: case insensitive filesystem */
/* compiled from: a */
public class C5775aaH extends C7023azk {
    /* access modifiers changed from: private */

    /* renamed from: IU */
    public C3987xk f4051IU = C3987xk.bFK;
    /* access modifiers changed from: private */
    public TItemPanel eVH;
    /* access modifiers changed from: private */
    public TItemPanel eVI;
    private TextField cpa;
    private Panel eVJ;

    public C5775aaH(IAddonProperties vWVar, C2698il ilVar) {
        super(vWVar, ilVar);
        initComponents();
    }

    private void initComponents() {
        this.eVJ = (Panel) cFB().mo4916ce("vault");
        this.eVI = new C1838d();
        this.eVJ.add(this.eVI);
        this.eVH = new C1839e();
        cFB().mo4915cd("items-container").add(this.eVH);
        cFB().mo4918cg("capacity").mo17403a(new C1836b());
        cFB().mo4918cg("vault-capacity").mo17403a(new C1835a());
        this.cpa = cFB().mo4920ci("search-field");
        this.cpa.addKeyListener(new C1837c());
    }

    /* renamed from: M */
    public void mo4307M(Ship fAVar) {
        super.mo4307M(fAVar);
        if (fAVar != null) {
            this.eVH.mo23647b((ItemLocation) fAVar.afI());
            if (fAVar instanceof Freighter) {
                this.eVJ.setVisible(true);
                this.eVI.mo23647b((ItemLocation) ((Freighter) fAVar).aZy());
                return;
            }
            this.eVJ.setVisible(false);
        }
    }

    /* renamed from: a.aaH$d */
    /* compiled from: a */
    class C1838d extends TItemPanel {


        C1838d() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: r */
        public JPopupMenu mo12148r(Item auq) {
            JPopupMenu jPopupMenu = new JPopupMenu();
            JMenu a = DefaultMenuItems.m43020a(51, new Item[]{auq});
            if (a != null) {
                jPopupMenu.add(a);
            }
            JMenuItem w = DefaultMenuItems.m43038w(auq);
            if (w != null) {
                jPopupMenu.add(w);
            }
            return jPopupMenu;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public JPopupMenu mo12147a(Item[] auqArr) {
            JPopupMenu jPopupMenu = new JPopupMenu();
            jPopupMenu.add(DefaultMenuItems.m43020a(51, auqArr));
            return jPopupMenu;
        }
    }

    /* renamed from: a.aaH$e */
    /* compiled from: a */
    class C1839e extends TItemPanel {


        C1839e() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: r */
        public JPopupMenu mo12148r(Item auq) {
            JPopupMenu jPopupMenu = new JPopupMenu();
            JMenu a = DefaultMenuItems.m43020a(51, new Item[]{auq});
            if (a != null) {
                jPopupMenu.add(a);
            }
            JMenuItem w = DefaultMenuItems.m43038w(auq);
            if (w != null) {
                jPopupMenu.add(w);
            }
            return jPopupMenu;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public JPopupMenu mo12147a(Item[] auqArr) {
            JPopupMenu jPopupMenu = new JPopupMenu();
            jPopupMenu.add(DefaultMenuItems.m43020a(51, auqArr));
            return jPopupMenu;
        }
    }

    /* renamed from: a.aaH$b */
    /* compiled from: a */
    class C1836b implements Progress.C2065a {
        C1836b() {
        }

        public String getText() {
            if (C5775aaH.this.cFC() == null || C5775aaH.this.cFC().afI() == null) {
                return null;
            }
            return String.valueOf(C5775aaH.this.f4051IU.mo22980dY(C5775aaH.this.cFC().afI().aRB())) + C0147Bi.SEPARATOR + C5775aaH.this.f4051IU.mo22980dY(C5775aaH.this.cFC().afI().mo2696wE());
        }

        public float getValue() {
            if (C5775aaH.this.cFC() == null || C5775aaH.this.cFC().afI() == null) {
                return 0.0f;
            }
            return C5775aaH.this.cFC().afI().aRB();
        }

        public float getMaximum() {
            if (C5775aaH.this.cFC() == null || C5775aaH.this.cFC().afI() == null) {
                return 0.0f;
            }
            return C5775aaH.this.cFC().afI().mo2696wE();
        }

        public float getMinimum() {
            return 0.0f;
        }
    }

    /* renamed from: a.aaH$a */
    class C1835a implements Progress.C2065a {
        C1835a() {
        }

        public Freighter cHl() {
            if (C5775aaH.this.cFC() instanceof Freighter) {
                return (Freighter) C5775aaH.this.cFC();
            }
            return null;
        }

        public String getText() {
            if (cHl() == null || cHl().aZy() == null) {
                return null;
            }
            return String.valueOf(C5775aaH.this.f4051IU.mo22980dY(cHl().aZy().aRB())) + C0147Bi.SEPARATOR + C5775aaH.this.f4051IU.mo22980dY(cHl().aZy().mo2696wE());
        }

        public float getValue() {
            if (cHl() == null || cHl().aZy() == null) {
                return 0.0f;
            }
            return cHl().aZy().aRB();
        }

        public float getMaximum() {
            if (cHl() == null || cHl().aZy() == null) {
                return 0.0f;
            }
            return cHl().aZy().mo2696wE();
        }

        public float getMinimum() {
            return 0.0f;
        }
    }

    /* renamed from: a.aaH$c */
    /* compiled from: a */
    class C1837c extends KeyAdapter {
        C1837c() {
        }

        public void keyReleased(KeyEvent keyEvent) {
            Component component = (JTextField) keyEvent.getComponent();
            if (keyEvent.getKeyCode() != 27) {
                if (C5775aaH.this.eVH != null) {
                    C5775aaH.this.eVH.bCU().mo2165O(component.getText());
                }
                if (C5775aaH.this.eVI != null) {
                    C5775aaH.this.eVI.bCU().mo2165O(component.getText());
                }
            } else if (component == KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner()) {
                component.transferFocusUpCycle();
            }
        }
    }
}
