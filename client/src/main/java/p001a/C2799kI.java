package p001a;

import org.objectweb.asm.MethodVisitor;

/* renamed from: a.kI */
/* compiled from: a */
public class C2799kI {
    private C1070Pe auR;
    private C1070Pe auS;
    private C1070Pe[] auT;
    private String descriptor;
    private String name;

    public C2799kI(C1070Pe pe, String str, C1070Pe pe2, C1070Pe... peArr) {
        this.auR = pe;
        this.name = str;
        this.auS = pe2;
        this.auT = peArr;
    }

    public String getDescriptor() {
        if (this.descriptor != null) {
            return this.descriptor;
        }
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        if (this.auT != null) {
            for (C1070Pe descriptor2 : this.auT) {
                sb.append(descriptor2.getDescriptor());
            }
        }
        sb.append(')');
        sb.append(this.auS.getDescriptor());
        String sb2 = sb.toString();
        this.descriptor = sb2;
        return sb2;
    }

    public String getName() {
        return this.name;
    }

    /* renamed from: KM */
    public C1070Pe mo20038KM() {
        return this.auR;
    }

    /* renamed from: KN */
    public C1070Pe mo20039KN() {
        return this.auS;
    }

    /* renamed from: KO */
    public C1070Pe[] mo20040KO() {
        return this.auT;
    }

    /* renamed from: a */
    public void mo20041a(MethodVisitor methodVisitor, int i) {
        methodVisitor.visitMethodInsn(i, this.auR.getInternalName(), this.name, getDescriptor());
    }
}
