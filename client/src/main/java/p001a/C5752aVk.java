package p001a;

/* renamed from: a.aVk  reason: case insensitive filesystem */
/* compiled from: a */
public class C5752aVk {
    private long swigCPtr;

    public C5752aVk(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5752aVk() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m19075a(C5752aVk avk) {
        if (avk == null) {
            return 0;
        }
        return avk.swigCPtr;
    }
}
