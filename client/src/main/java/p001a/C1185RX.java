package p001a;

import game.engine.DataGameEvent;
import game.network.message.externalizable.C0722KO;
import game.network.message.serializable.C0010AD;
import game.network.message.serializable.C1695Yw;
import game.network.message.serializable.C5613aQb;
import logic.res.code.C6708asE;
import logic.res.html.C2491fm;
import logic.res.html.MessageContainer;
import logic.res.html.axI;
import logic.thred.LogPrinter;
import logic.thred.PoolThread;
import logic.thred.ThreadWrapper;
import org.apache.commons.logging.Log;
import taikodom.render.loader.provider.FilePath;

import java.io.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/* renamed from: a.RX */
/* compiled from: a */
public class C1185RX implements WraperDbFile {
    private static final String gup = "Repository-Version";
    private static final boolean guq = false;
    private static final Log logger = LogPrinter.setClass(C1185RX.class);
    C1412Uh guy;
    private DataGameEvent aGA;
    private boolean disposed;
    private FilePath eKA;
    private ObjectOutputStream fov = null;
    private LinkedBlockingQueue<C1695Yw> gur = new LinkedBlockingQueue<>();
    private ThreadWrapper gus;
    private OutputStream gut = null;
    private long guu = 0;
    private AtomicLong guv = new AtomicLong(0);
    private List<C3582se> guw = new LinkedList();
    private Object gux = new Object();

    public C1185RX(DataGameEvent jz, String str) {
        this.gus = new ThreadWrapper(new C1187b(), str);
        this.aGA = jz;
        this.gus.setDaemon(true);
    }

    /* renamed from: a */
    public void mo5219a(C1412Uh uh) {
        this.guy = uh;
    }

    public FilePath aQe() {
        return this.eKA;
    }

    /* renamed from: f */
    public void mo5234f(FilePath ain) {
        this.eKA = ain;
    }

    /* renamed from: c */
    public void mo5226c(C1695Yw yw) {
        try {
            this.gur.put(yw);
            m9185d(yw);
        } catch (InterruptedException e) {
            throw new CountFailedReadOrWriteException((Throwable) e);
        }
    }

    public boolean ctT() {
        return mo5242up(0).exists();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo5220a(OutputStream outputStream, long j) {
        StringBuilder sb = new StringBuilder();
        sb.append(gup).append(": ").append(j).append("\n");
        sb.append("\n");
        outputStream.write(sb.toString().getBytes());
        outputStream.flush();
    }

    /* access modifiers changed from: protected */
    public void run() {
        Class<?> tS;
        C2491fm fmVar;
        List<C1695Yw> dgK = MessageContainer.init();
        while (!this.disposed) {
            try {
                dgK.clear();
                if (this.gur.size() > 0) {
                    this.gur.drainTo(dgK);
                } else {
                    dgK.add(this.gur.take());
                }
                if (dgK.size() != 0) {
                    for (C1695Yw yw : dgK) {
                        if (DataGameEvent.eKb) {
                            break;
                        }
                        m9185d(yw);
                        synchronized (this.gux) {
                            yw.bHC().mo11033lK(this.guv.getAndIncrement());
                            if (this.gut == null) {
                                this.gut = ctU().openOutputStream();
                                mo5220a(this.gut, yw.bHC().dpb());
                                this.fov = new aAD(this.aGA, new C2883lR(new BufferedOutputStream(this.gut)));
                            }
                            try {
                                this.fov.writeObject(yw.bHC());
                                this.fov.writeObject(yw.bHD());
                                this.fov.writeObject(yw.bHG());
                                this.fov.flush();
                            } catch (RuntimeException e) {
                                logger.error("IGNORING UBBER ERROR!!!!", e);
                                try {
                                    C5613aQb bHC = yw.bHC();
                                    Date date = new Date(bHC.getTransactionTime());
                                    if (bHC.dpe() < 0) {
                                        tS = null;
                                    } else {
                                        tS = this.aGA.bxy().mo12017tS(bHC.dpe());
                                    }
                                    C6708asE av = C6708asE.m25631av(tS);
                                    if (av == null) {
                                        fmVar = null;
                                    } else {
                                        fmVar = av.clx()[bHC.cmf()];
                                    }
                                    logger.error(date + " error at " + (tS == null ? "no-class" : tS.getName()) + ":" + bHC.dpc() + " method:" + fmVar, e);
                                    if (yw.bHD() != null) {
                                        for (C0010AD error : yw.bHD()) {
                                            logger.error(error);
                                        }
                                    }
                                    if (yw.bHG() != null) {
                                        for (C0722KO error2 : yw.bHG()) {
                                            logger.error(error2);
                                        }
                                    }
                                } catch (RuntimeException e2) {
                                    logger.error(e2);
                                }
                                ctY();
                            }
                            if (this.gut != null) {
                                this.gut.flush();
                            }
                            long currentTimeMillis = System.currentTimeMillis();
                            if (currentTimeMillis - this.guu > 10000) {
                                if (this.fov != null) {
                                    this.fov.reset();
                                }
                                this.guu = currentTimeMillis;
                            }
                        }
                    }
                } else {
                    return;
                }
            } catch (InterruptedException e3) {
                logger.warn("queue disposed");
                return;
            } catch (axI e4) {
                logger.warn("queue disposed");
            } catch (Exception e5) {
                logger.error("Exception while saving transaction log", e5);
                logger.error("SHUTTING DOWN!");
                System.exit(333);
            }
        }
        return;
    }

    /* renamed from: d */
    private void m9185d(C1695Yw yw) {
    }

    /* renamed from: HY */
    public void mo5218HY() {
        int i = 0;
        while (i < 100) {
            try {
                if (this.gur.size() <= 0) {
                    break;
                }
                PoolThread.sleep(100);
                i++;
            } catch (InterruptedException e) {
                throw new CountFailedReadOrWriteException((Throwable) e);
            }
        }
        if (this.gur.size() > 0) {
            throw new IOException("Still have pending transactions");
        }
        this.disposed = true;
        if (this.gus.isAlive()) {
            this.gus.join(200);
            this.gus.interrupt();
            this.gus.join();
        }
        ctY();
    }

    /* access modifiers changed from: protected */
    public FilePath ctU() {
        int i = 0;
        while (true) {
            FilePath up = mo5242up(i);
            if (!up.exists()) {
                return up;
            }
            i++;
        }
    }

    /* access modifiers changed from: protected */
    public int ctV() {
        int i = 0;
        while (mo5242up(i).exists()) {
            i++;
        }
        return i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: up */
    public FilePath mo5242up(int i) {
        FilePath aC = this.eKA.concat(String.format("environment-transactions-%06d.log", new Object[]{Integer.valueOf(i)}));
        if (!aC.mo2249BB().exists()) {
            aC.mo2249BB().mo2251BD();
        }
        return aC;
    }

    /* access modifiers changed from: protected */
    public void ctW() {
        int i = 0;
        synchronized (this.gux) {
            while (true) {
                FilePath up = mo5242up(i);
                if (!up.exists()) {
                    this.guw.clear();
                } else {
                    long currentTimeMillis = System.currentTimeMillis();
                    System.out.print("Reading " + up.getName() + "... ");
                    System.out.flush();
                    try {
                        int h = m9186h(up);
                        System.out.printf("done. (%d transactions in %.2f seconds)\n", new Object[]{Integer.valueOf(h), Double.valueOf(((double) (System.currentTimeMillis() - currentTimeMillis)) / 1000.0d)});
                        i++;
                    } catch (Throwable th) {
                        throw new RuntimeException("Error reading transaction log from file: " + up, th);
                    }
                }
            }
        }
    }

    /* renamed from: d */
    public String mo5233d(InputStream inputStream) {
        int i = 0;
        int i2 = 0;
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read();
            if (read == 10 && i2 == 10) {
                return new String(bArr, 0, i);
            }
            if (bArr.length == i) {
                byte[] bArr2 = new byte[(bArr.length * 2)];
                System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
                bArr = bArr2;
            }
            bArr[i] = (byte) read;
            i++;
            i2 = read;
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:26:0x0104=Splitter:B:26:0x0104, B:87:0x02cb=Splitter:B:87:0x02cb, B:76:0x028d=Splitter:B:76:0x028d} */
    /* renamed from: h */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int m9186h(FilePath r13) {
        /*
            r12 = this;
            r2 = -1
            r4 = 0
            java.io.InputStream r5 = r13.openInputStream()
            java.lang.String r0 = r12.mo5233d((java.io.InputStream) r5)
            java.util.StringTokenizer r6 = new java.util.StringTokenizer
            java.lang.String r1 = "\n"
            r6.<init>(r0, r1)
            r0 = r2
        L_0x0013:
            boolean r7 = r6.hasMoreTokens()
            if (r7 != 0) goto L_0x0036
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 != 0) goto L_0x0057
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Repository-Version not found in the transaction log: "
            r1.<init>(r2)
            java.lang.String r2 = r13.getName()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0036:
            java.lang.String r7 = r6.nextToken()
            java.lang.String r8 = "Repository-Version"
            boolean r8 = r7.startsWith(r8)
            if (r8 == 0) goto L_0x0013
            r0 = 58
            int r0 = r7.indexOf(r0)
            int r0 = r0 + 1
            java.lang.String r0 = r7.substring(r0)
            java.lang.String r0 = r0.trim()
            long r0 = java.lang.Long.parseLong(r0)
            goto L_0x0013
        L_0x0057:
            java.util.concurrent.atomic.AtomicLong r2 = r12.guv
            long r2 = r2.get()
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 == 0) goto L_0x0090
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Wrong repository version, expecting "
            r3.<init>(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r1 = ", but "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.util.concurrent.atomic.AtomicLong r1 = r12.guv
            long r4 = r1.get()
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r1 = " found. File: "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r13)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            throw r2
        L_0x0090:
            a.na r6 = new a.na
            a.Jz r0 = r12.aGA
            a.aBq r1 = new a.aBq
            r1.<init>(r5)
            r6.<init>(r0, r1)
            r1 = r4
        L_0x009d:
            java.lang.Object r0 = r6.readObject()     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            a.aQb r0 = (game.network.message.serializable.C5613aQb) r0     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            if (r0 != 0) goto L_0x00ad
            r6.close()
            r5.close()
            r0 = r1
        L_0x00ac:
            return r0
        L_0x00ad:
            long r2 = r0.dpb()     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            java.util.concurrent.atomic.AtomicLong r4 = r12.guv     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            long r8 = r4.get()     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            int r2 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r2 == 0) goto L_0x010e
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            java.lang.String r4 = "Transaction "
            r3.<init>(r4)     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            java.lang.String r4 = " in file "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            java.lang.String r4 = r13.getName()     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            java.lang.String r4 = " is expecting repository version "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            long r8 = r0.dpb()     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            java.lang.StringBuilder r0 = r3.append(r8)     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            java.lang.String r3 = " , bud "
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            java.util.concurrent.atomic.AtomicLong r3 = r12.guv     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            long r8 = r3.get()     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            java.lang.String r3 = " found"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            java.lang.String r0 = r0.toString()     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            r2.<init>(r0)     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            throw r2     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
        L_0x0102:
            r2 = move-exception
            r0 = r1
        L_0x0104:
            r2.printStackTrace()     // Catch:{ all -> 0x0307 }
            r6.close()
            r5.close()
            goto L_0x00ac
        L_0x010e:
            java.util.concurrent.atomic.AtomicLong r2 = r12.guv     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            r2.incrementAndGet()     // Catch:{ IllegalStateException -> 0x0102, EOFException -> 0x0356, InstantiationException -> 0x0352, IllegalAccessException -> 0x034e }
            int r3 = r1 + 1
            java.lang.Object r1 = r6.readObject()     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            java.util.List r1 = (java.util.List) r1     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            java.util.Iterator r2 = r1.iterator()     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
        L_0x011f:
            boolean r1 = r2.hasNext()     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            if (r1 != 0) goto L_0x0138
            java.lang.Object r1 = r6.readObject()     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            java.util.List r1 = (java.util.List) r1     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            java.util.Iterator r2 = r1.iterator()     // Catch:{ RuntimeException -> 0x01ce }
        L_0x012f:
            boolean r1 = r2.hasNext()     // Catch:{ RuntimeException -> 0x01ce }
            if (r1 != 0) goto L_0x016b
            r1 = r3
            goto L_0x009d
        L_0x0138:
            java.lang.Object r1 = r2.next()     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            a.AD r1 = (game.network.message.serializable.C0010AD) r1     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            a.Jz r4 = r12.aGA     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            java.lang.Class r7 = r1.mo37hD()     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            a.apO r1 = r1.mo36hC()     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            long r8 = r1.cqo()     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            a.se r1 = r4.mo3425d(r7, r8)     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            java.util.List<a.se> r4 = r12.guw     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            r4.add(r1)     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
        L_0x0155:
            java.util.List<a.se> r1 = r12.guw     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            int r1 = r1.size()     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            r4 = 400000(0x61a80, float:5.6052E-40)
            if (r1 <= r4) goto L_0x011f
            java.util.List<a.se> r1 = r12.guw     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            r4 = 0
            r1.remove(r4)     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            goto L_0x0155
        L_0x0167:
            r1 = move-exception
            r2 = r1
            r0 = r3
            goto L_0x0104
        L_0x016b:
            java.lang.Object r1 = r2.next()     // Catch:{ RuntimeException -> 0x01ce }
            a.KO r1 = (game.network.message.externalizable.C0722KO) r1     // Catch:{ RuntimeException -> 0x01ce }
            a.Jz r4 = r12.aGA     // Catch:{ RuntimeException -> 0x01ce }
            a.lj r4 = r4.bGA()     // Catch:{ RuntimeException -> 0x01ce }
            a.apO r7 = r1.mo3514hC()     // Catch:{ RuntimeException -> 0x01ce }
            a.se r4 = r4.mo15459a((p001a.C6562apO) r7)     // Catch:{ RuntimeException -> 0x01ce }
            if (r4 == 0) goto L_0x0256
            java.lang.Class r7 = r4.bFU()     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.Class r8 = r1.mo3515hD()     // Catch:{ RuntimeException -> 0x01ce }
            if (r7 == r8) goto L_0x0256
            java.lang.RuntimeException r2 = new java.lang.RuntimeException     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r8 = "A transaction log is expecting object "
            r7.<init>(r8)     // Catch:{ RuntimeException -> 0x01ce }
            a.apO r8 = r1.mo3514hC()     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r8 = " to be "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.Class r1 = r1.mo3515hD()     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r1 = r1.getName()     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.StringBuilder r1 = r7.append(r1)     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r7 = " and a "
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.Class r4 = r4.bFU()     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r4 = r4.getName()     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r4 = " was found."
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r1 = r1.toString()     // Catch:{ RuntimeException -> 0x01ce }
            r2.<init>(r1)     // Catch:{ RuntimeException -> 0x01ce }
            throw r2     // Catch:{ RuntimeException -> 0x01ce }
        L_0x01ce:
            r1 = move-exception
            int r2 = r0.dpe()     // Catch:{ NullPointerException -> 0x0245 }
            if (r2 <= 0) goto L_0x0348
            int r2 = r0.cmf()     // Catch:{ NullPointerException -> 0x0245 }
            if (r2 < 0) goto L_0x0348
            java.util.Date r2 = new java.util.Date     // Catch:{ NullPointerException -> 0x0245 }
            long r8 = r0.getTransactionTime()     // Catch:{ NullPointerException -> 0x0245 }
            r2.<init>(r8)     // Catch:{ NullPointerException -> 0x0245 }
            a.Jz r4 = r12.aGA     // Catch:{ NullPointerException -> 0x0245 }
            a.arE r4 = r4.bxy()     // Catch:{ NullPointerException -> 0x0245 }
            int r7 = r0.dpe()     // Catch:{ NullPointerException -> 0x0245 }
            java.lang.Class r4 = r4.mo12017tS(r7)     // Catch:{ NullPointerException -> 0x0245 }
            a.asE r7 = logic.res.code.C6708asE.m25631av(r4)     // Catch:{ NullPointerException -> 0x0245 }
            a.fm[] r7 = r7.clx()     // Catch:{ NullPointerException -> 0x0245 }
            int r8 = r0.cmf()     // Catch:{ NullPointerException -> 0x0245 }
            r7 = r7[r8]     // Catch:{ NullPointerException -> 0x0245 }
            java.lang.RuntimeException r8 = new java.lang.RuntimeException     // Catch:{ NullPointerException -> 0x0245 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0245 }
            java.lang.String r10 = "T"
            r9.<init>(r10)     // Catch:{ NullPointerException -> 0x0245 }
            java.lang.StringBuilder r9 = r9.append(r3)     // Catch:{ NullPointerException -> 0x0245 }
            java.lang.String r10 = " "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ NullPointerException -> 0x0245 }
            java.lang.StringBuilder r2 = r9.append(r2)     // Catch:{ NullPointerException -> 0x0245 }
            java.lang.String r9 = " error at "
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ NullPointerException -> 0x0245 }
            java.lang.String r4 = r4.getName()     // Catch:{ NullPointerException -> 0x0245 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ NullPointerException -> 0x0245 }
            java.lang.String r4 = ":"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ NullPointerException -> 0x0245 }
            long r10 = r0.dpc()     // Catch:{ NullPointerException -> 0x0245 }
            java.lang.StringBuilder r0 = r2.append(r10)     // Catch:{ NullPointerException -> 0x0245 }
            java.lang.String r2 = " "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ NullPointerException -> 0x0245 }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ NullPointerException -> 0x0245 }
            java.lang.String r0 = r0.toString()     // Catch:{ NullPointerException -> 0x0245 }
            r8.<init>(r0, r1)     // Catch:{ NullPointerException -> 0x0245 }
            throw r8     // Catch:{ NullPointerException -> 0x0245 }
        L_0x0245:
            r0 = move-exception
            org.apache.commons.logging.Log r0 = logger     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            r0.error(r1)     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
            throw r1     // Catch:{ IllegalStateException -> 0x0167, EOFException -> 0x024c, InstantiationException -> 0x028a, IllegalAccessException -> 0x02c8 }
        L_0x024c:
            r0 = move-exception
            r0 = r3
        L_0x024e:
            r6.close()
            r5.close()
            goto L_0x00ac
        L_0x0256:
            if (r4 != 0) goto L_0x0298
            org.apache.commons.logging.Log r4 = logger     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r8 = "Transaction log for object "
            r7.<init>(r8)     // Catch:{ RuntimeException -> 0x01ce }
            a.apO r8 = r1.mo3514hC()     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r8 = " for version "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ RuntimeException -> 0x01ce }
            a.Jd r1 = r1.bdh()     // Catch:{ RuntimeException -> 0x01ce }
            long r8 = r1.aBt()     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.StringBuilder r1 = r7.append(r8)     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r7 = " is being ignored since the object was not found in the repository"
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r1 = r1.toString()     // Catch:{ RuntimeException -> 0x01ce }
            r4.warn(r1)     // Catch:{ RuntimeException -> 0x01ce }
            goto L_0x012f
        L_0x028a:
            r1 = move-exception
            r2 = r1
            r0 = r3
        L_0x028d:
            r2.printStackTrace()     // Catch:{ all -> 0x0307 }
            r6.close()
            r5.close()
            goto L_0x00ac
        L_0x0298:
            boolean r7 = r4.bFT()     // Catch:{ RuntimeException -> 0x01ce }
            if (r7 == 0) goto L_0x030f
            java.lang.String r1 = "fill-hollow-hack"
            java.lang.String r1 = java.lang.System.getProperty(r1)     // Catch:{ RuntimeException -> 0x01ce }
            if (r1 == 0) goto L_0x02d6
            a.afk r1 = r4.cVj()     // Catch:{ RuntimeException -> 0x01ce }
            r7 = 0
            r1.mo3146A(r7)     // Catch:{ RuntimeException -> 0x01ce }
            org.apache.commons.logging.Log r1 = logger     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r8 = "Filling up hollow object "
            r7.<init>(r8)     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r4 = r4.bFY()     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.StringBuilder r4 = r7.append(r4)     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r4 = r4.toString()     // Catch:{ RuntimeException -> 0x01ce }
            r1.error(r4)     // Catch:{ RuntimeException -> 0x01ce }
            goto L_0x012f
        L_0x02c8:
            r1 = move-exception
            r2 = r1
            r0 = r3
        L_0x02cb:
            r2.printStackTrace()     // Catch:{ all -> 0x0307 }
            r6.close()
            r5.close()
            goto L_0x00ac
        L_0x02d6:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r7 = "Trying to execute a transaction on a hollow object ("
            r2.<init>(r7)     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.Class r7 = r4.bFU()     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r7 = " id "
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ RuntimeException -> 0x01ce }
            a.apO r4 = r4.mo6891hC()     // Catch:{ RuntimeException -> 0x01ce }
            long r8 = r4.cqo()     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r4 = " is still hollow"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r2 = r2.toString()     // Catch:{ RuntimeException -> 0x01ce }
            r1.<init>(r2)     // Catch:{ RuntimeException -> 0x01ce }
            throw r1     // Catch:{ RuntimeException -> 0x01ce }
        L_0x0307:
            r0 = move-exception
            r6.close()
            r5.close()
            throw r0
        L_0x030f:
            r4.mo21976b((game.network.message.externalizable.C0722KO) r1)     // Catch:{ RuntimeException -> 0x0314 }
            goto L_0x012f
        L_0x0314:
            r1 = move-exception
            java.lang.RuntimeException r2 = new java.lang.RuntimeException     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r8 = "Error applying change at "
            r7.<init>(r8)     // Catch:{ RuntimeException -> 0x01ce }
            a.Xf r8 = r4.mo6901yn()     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.Class r8 = r8.getClass()     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r8 = r8.getName()     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r8 = ":"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ RuntimeException -> 0x01ce }
            a.apO r4 = r4.mo6891hC()     // Catch:{ RuntimeException -> 0x01ce }
            long r8 = r4.cqo()     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.StringBuilder r4 = r7.append(r8)     // Catch:{ RuntimeException -> 0x01ce }
            java.lang.String r4 = r4.toString()     // Catch:{ RuntimeException -> 0x01ce }
            r2.<init>(r4, r1)     // Catch:{ RuntimeException -> 0x01ce }
            throw r2     // Catch:{ RuntimeException -> 0x01ce }
        L_0x0348:
            org.apache.commons.logging.Log r0 = logger     // Catch:{ NullPointerException -> 0x0245 }
            r0.error(r1)     // Catch:{ NullPointerException -> 0x0245 }
            throw r1     // Catch:{ NullPointerException -> 0x0245 }
        L_0x034e:
            r2 = move-exception
            r0 = r1
            goto L_0x02cb
        L_0x0352:
            r2 = move-exception
            r0 = r1
            goto L_0x028d
        L_0x0356:
            r0 = move-exception
            r0 = r1
            goto L_0x024e
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C1185RX.m9186h(a.ain):int");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: i */
    public void mo5237i(FilePath ain) {
        FilePath ctZ = ctZ();
        if (!ctZ.exists() || ctZ.delete()) {
            File file = new File(ctZ.getPath().replaceAll("[.]gz$", ""));
            if (!file.exists() || file.delete()) {
                ain.mo2261d(ctZ);
                ctX();
                return;
            }
            throw new RuntimeException("Could not delete old format snapshot file: " + file.getName());
        }
        throw new RuntimeException("Could not delete snapshot file: " + ctZ.getName());
    }

    /* access modifiers changed from: package-private */
    public void ctX() {
        ctY();
        int i = 0;
        while (true) {
            FilePath up = mo5242up(i);
            if (up.exists()) {
                if (!up.delete()) {
                    throw new RuntimeException("Could not delete or rename transaction log file: " + up.getName());
                }
                i++;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void ctY() {
        synchronized (this.gux) {
            if (this.fov != null) {
                this.fov.close();
                this.fov = null;
            }
            if (this.gut != null) {
                this.gut.close();
                this.gut = null;
            }
        }
    }

    public void flush() {
    }

    private FilePath ctZ() {
        return this.eKA.concat("environment-snapshot.xml.gz");
    }

    public boolean bOd() {
        InputStream openInputStream;
        FilePath ctZ = ctZ();
        if (!ctZ.exists()) {
            ctZ = ctZ.mo2249BB().concat(ctZ.getName().replaceAll("[.]gz$", ""));
        }
        if (!ctZ.exists()) {
            logger.info("Persisted data file could not be found: " + ctZ);
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis();
        logger.info("Reading " + ctZ.getName() + "...");
        if (ctZ.getName().toLowerCase().endsWith(".gz")) {
            openInputStream = new GZIPInputStream(ctZ.openInputStream());
        } else {
            openInputStream = ctZ.openInputStream();
        }
        try {
            C0233Cv cv = new C0233Cv(this.aGA, openInputStream);
            if (openInputStream != null) {
                try {
                    openInputStream.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            this.guv.set(cv.aBt());
            logger.info("Snapshot version: " + this.guv.get());
            logger.info(String.format("done. (took %.2fs)", new Object[]{Double.valueOf(((double) (System.currentTimeMillis() - currentTimeMillis)) / 1000.0d)}));
            System.gc();
            System.gc();
            return true;
        } catch (Exception e2) {
            throw new CountFailedReadOrWriteException((Throwable) e2);
        } catch (Throwable th) {
            if (openInputStream != null) {
                try {
                    openInputStream.close();
                } catch (IOException e3) {
                    throw new RuntimeException(e3);
                }
            }
            throw th;
        }
    }

    /* renamed from: i */
    public void mo5238i(C3582se seVar) {
    }

    public boolean bOc() {
        FilePath aC = this.eKA.concat("environment-snapshot-temp.xml.gz");
        if (!aC.exists() || aC.delete()) {
            if (!aC.mo2249BB().exists()) {
                aC.mo2249BB().mo2251BD();
            }
            for (int i = 0; i < 100 && this.gur.size() > 0; i++) {
                PoolThread.sleep(100);
                this.gus.isAlive();
            }
            if (this.gur.size() > 0) {
                throw new IOException("Still have pending transactions");
            }
            ctY();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(aC.openOutputStream());
            new azX(gZIPOutputStream, this.guy.getTaikodom(), this.guv.get());
            gZIPOutputStream.flush();
            gZIPOutputStream.close();
            mo5237i(aC);
            logger.info("Persist finished successfully");
            return true;
        }
        logger.error("Could not delete temp file " + aC.getName() + " make sure the persist process isnt already running");
        return false;
    }

    public void bOf() {
        ctY();
    }

    public void bOe() {
        try {
            if (mo5242up(0).exists()) {
                ctW();
            }
            logger.info("Current repository version: " + this.guv.get());
        } catch (Throwable th) {
            throw new RuntimeException("Error reading transaction logs", th);
        }
    }

    public void start() {
        this.gus.start();
    }

    public void init() {
    }

    /* renamed from: h */
    public void mo5236h(C3582se seVar) {
    }

    /* renamed from: a.RX$a */
    public static class C1186a {
        long agR;

        public C1186a(long j) {
            this.agR = j;
        }
    }

    /* renamed from: a.RX$b */
    /* compiled from: a */
    class C1187b implements Runnable {
        C1187b() {
        }

        public void run() {
            C1185RX.this.run();
        }
    }
}
