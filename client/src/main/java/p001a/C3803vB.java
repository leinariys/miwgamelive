package p001a;

import game.geometry.Vec3d;

/* renamed from: a.vB */
/* compiled from: a */
public class C3803vB {
    /* renamed from: w */
    public static String m40160w(Vec3d ajr) {
        if (ajr == null) {
            return null;
        }
        return String.valueOf(String.valueOf(ajr.x)) + ";" + String.valueOf(ajr.y) + ";" + String.valueOf(ajr.z);
    }

    /* renamed from: cs */
    public static Vec3d m40159cs(String str) {
        int indexOf;
        int indexOf2;
        if (str == null || "null".equals(str) || "".equals(str) || (indexOf = str.indexOf(";")) == -1 || (indexOf2 = str.indexOf(";", indexOf + 1)) == -1) {
            return null;
        }
        return new Vec3d(Double.parseDouble(str.substring(0, indexOf)), Double.parseDouble(str.substring(indexOf + 1, indexOf2)), Double.parseDouble(str.substring(indexOf2 + 1)));
    }
}
