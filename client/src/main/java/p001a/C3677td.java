package p001a;

import logic.swing.C0454GJ;
import logic.ui.item.C2830kk;

import javax.swing.*;

/* renamed from: a.td */
/* compiled from: a */
class C3677td implements C5912aco.C1865a {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ C2762jf.C2764b bkm;
    C0454GJ atu = new aKJ(this);

    C3677td(C2762jf.C2764b bVar) {
        this.bkm = bVar;
    }

    /* renamed from: gZ */
    public void mo4gZ() {
        int height;
        this.bkm.mo12691a(this.bkm.glZ);
        int i = 0;
        int i2 = 0;
        while (i < C2762jf.this.ahE.buv()) {
            JComponent nn = C2762jf.this.ahE.mo23038nn(i);
            if (!nn.isPinned()) {
                height = i2;
            } else {
                this.bkm.bOE();
                if (nn.getY() != i2) {
                    this.bkm.mo12692a(nn, "[" + nn.getY() + ".." + i2 + "] dur " + "50" + " regular_in", C2830kk.asP, this.atu);
                }
                height = i2 + nn.getHeight();
            }
            i++;
            i2 = height;
        }
        int height2 = C2762jf.this.ahE.getHeight();
        int bux = C2762jf.this.ahE.bux() - 1;
        while (true) {
            int i3 = bux;
            int i4 = height2;
            if (i3 <= C2762jf.this.ahE.buv()) {
                break;
            }
            JComponent nn2 = C2762jf.this.ahE.mo23038nn(i3);
            if (nn2.isPinned()) {
                i4 -= nn2.getHeight();
                this.bkm.bOE();
                this.bkm.mo12692a(nn2, "[" + nn2.getY() + ".." + i4 + "] dur " + "50" + " regular_in", C2830kk.asP, this.atu);
            }
            height2 = i4;
            bux = i3 - 1;
        }
        if (this.bkm.getCount() == 0) {
            this.bkm.glZ.mo4gZ();
        }
    }
}
