package p001a;

import game.engine.DataGameEvent;
import game.network.manager.C0387FM;

import java.io.ObjectOutput;

/* renamed from: a.agx  reason: case insensitive filesystem */
/* compiled from: a */
public class C6129agx {
    /* renamed from: d */
    public static boolean m22158d(ObjectOutput objectOutput) {
        if (objectOutput instanceof C6744aso) {
            return ((C6744aso) objectOutput).mo2092Xc();
        }
        return false;
    }

    /* renamed from: e */
    public static DataGameEvent m22159e(ObjectOutput objectOutput) {
        if (objectOutput instanceof C0387FM) {
            return ((C0387FM) objectOutput).mo2090PM();
        }
        if (objectOutput instanceof aAD) {
            return ((aAD) objectOutput).mo2090PM();
        }
        return null;
    }
}
