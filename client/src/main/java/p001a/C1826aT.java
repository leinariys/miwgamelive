package p001a;

import game.geometry.Quat4fWrap;

import javax.vecmath.Quat4f;

/* renamed from: a.aT */
/* compiled from: a */
public class C1826aT extends C6518aoW<Quat4fWrap> {
    /* renamed from: a */
    public Quat4fWrap mo11472a(float f, float f2, float f3, float f4) {
        Quat4fWrap aoy = (Quat4fWrap) get();
        aoy.set(f, f2, f3, f4);
        return aoy;
    }

    /* renamed from: b */
    public Quat4fWrap mo11474b(Quat4f quat4f) {
        Quat4fWrap aoy = (Quat4fWrap) get();
        aoy.set(quat4f);
        return aoy;
    }

    /* access modifiers changed from: protected */
    /* renamed from: fv */
    public Quat4fWrap create() {
        return new Quat4fWrap();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void copy(Quat4fWrap aoy, Quat4fWrap aoy2) {
        aoy.mo15242f(aoy2);
    }
}
