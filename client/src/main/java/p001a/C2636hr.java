package p001a;

import com.hoplon.geometry.Vec3f;
import logic.abc.C0802Lc;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/* renamed from: a.hr */
/* compiled from: a */
public class C2636hr extends C0285Dg {
    private static final Comparator<C1083Pr> fdI = new C6519aoX();
    public final List<C1083Pr> fdA = new ArrayList();
    public final Vec3f fdB = new Vec3f(0.0f, -10.0f, 0.0f);
    /* renamed from: Ut */
    public C5718aUc f8063Ut = new C5718aUc();
    public float fdC = 0.016666668f;
    public boolean fdD;
    public boolean fdE;
    public List<C1725ZV> fdF = new ArrayList();
    public int fdG = 0;
    public C2849lA fdy;
    public C6560apM fdz;
    /* renamed from: Uv */
    private List<C1083Pr> f8064Uv = new ArrayList();
    private C2637a fdH = new C2637a((C2637a) null);

    public C2636hr(C1701ZB zb, C3737uO uOVar, C2849lA lAVar, C6760atE ate) {
        super(zb, uOVar, ate);
        this.fdy = lAVar;
        if (this.fdy == null) {
            this.fdy = new C3354qc();
            this.fdE = true;
        } else {
            this.fdE = false;
        }
        this.fdz = new C6560apM();
        this.fdD = true;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public static int m32976c(C1083Pr pr) {
        C6238ajC bnn = pr.bnn();
        return bnn.cFk() >= 0 ? bnn.cFk() : pr.bno().cFk();
    }

    /* access modifiers changed from: protected */
    /* renamed from: iX */
    public void mo19411iX(float f) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.czT.size()) {
                C6238ajC c = C6238ajC.m22755c((ayY) this.czT.get(i2));
                if (!(c == null || c.cFa() == 2 || !c.cEW())) {
                    c.mo13932iX(f);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void aDR() {
        this.stack.bcH().push();
        try {
            if (!(aDs() == null || (aDs().bnd() & 3) == 0)) {
                for (int i = 0; i < this.czT.size(); i++) {
                    ayY ayy = (ayY) this.czT.get(i);
                    if (!(aDs() == null || (aDs().bnd() & 1) == 0)) {
                        Vec3f h = this.stack.bcH().mo4460h(255.0f, 255.0f, 255.0f);
                        switch (ayy.cFa()) {
                            case 1:
                                h.set(255.0f, 255.0f, 255.0f);
                                break;
                            case 2:
                                h.set(0.0f, 255.0f, 0.0f);
                                break;
                            case 3:
                                h.set(0.0f, 255.0f, 255.0f);
                                break;
                            case 4:
                                h.set(255.0f, 0.0f, 0.0f);
                                break;
                            case 5:
                                h.set(255.0f, 255.0f, 0.0f);
                                break;
                            default:
                                h.set(255.0f, 0.0f, 0.0f);
                                break;
                        }
                        mo19404a(ayy.cFf(), ayy.cEZ(), h);
                    }
                    if (!(this.f6423Ux == null || (this.f6423Ux.bnd() & 2) == 0)) {
                        Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                        Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                        Vec3f h2 = this.stack.bcH().mo4460h(1.0f, 0.0f, 0.0f);
                        ayy.cEZ().getAabb(ayy.cFf(), vec3f, vec3f2);
                        this.f6423Ux.mo4812j(vec3f, vec3f2, h2);
                    }
                }
                Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
                for (int i2 = 0; i2 < this.fdF.size(); i2++) {
                    for (int i3 = 0; i3 < this.fdF.get(i2).cwA(); i3++) {
                        vec3f3.set(0.0f, 255.0f, 255.0f);
                        if (this.fdF.get(i2).mo7430uG(i3).deK.hPI) {
                            vec3f3.set(0.0f, 0.0f, 255.0f);
                        } else {
                            vec3f3.set(255.0f, 0.0f, 255.0f);
                        }
                        vec3f4.set(this.fdF.get(i2).mo7430uG(i3).deL.bFG);
                        vec3f5.set(this.fdF.get(i2).mo7430uG(i3).deL.bFF.getElement(0, this.fdF.get(i2).cwC()), this.fdF.get(i2).mo7430uG(i3).deL.bFF.getElement(1, this.fdF.get(i2).cwC()), this.fdF.get(i2).mo7430uG(i3).deL.bFF.getElement(2, this.fdF.get(i2).cwC()));
                        vec3f6.add(vec3f4, vec3f5);
                        this.f6423Ux.mo4811i(vec3f4, vec3f6, vec3f3);
                        this.f6423Ux.mo4811i(vec3f4, this.fdF.get(i2).mo7430uG(i3).deK.hPD, vec3f3);
                    }
                }
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void aDV() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.czT.size()) {
                C6238ajC c = C6238ajC.m22755c((ayY) this.czT.get(i2));
                if (c != null) {
                    c.aDV();
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void bPy() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.czT.size()) {
                C6238ajC c = C6238ajC.m22755c((ayY) this.czT.get(i2));
                if (c != null && c.isActive()) {
                    c.bPy();
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void bPz() {
        this.stack.bcJ().push();
        try {
            C3978xf xfVar = (C3978xf) this.stack.bcJ().get();
            for (int i = 0; i < this.czT.size(); i++) {
                C6238ajC c = C6238ajC.m22755c((ayY) this.czT.get(i));
                if (!(c == null || c.cey() == null || c.cEX())) {
                    C0503Gz.m3561a(c.cFh(), c.cFi(), c.cFj(), this.fdC, xfVar);
                    c.cey().mo2961f(xfVar);
                }
            }
            if (!(aDs() == null || (aDs().bnd() & 1) == 0)) {
                for (int i2 = 0; i2 < this.fdF.size(); i2++) {
                    for (int i3 = 0; i3 < this.fdF.get(i2).cwA(); i3++) {
                        this.fdF.get(i2).mo7421i(i3, true);
                    }
                }
            }
        } finally {
            this.stack.bcJ().pop();
        }
    }

    /* renamed from: a */
    public int mo1649a(float f, int i, float f2) {
        int i2;
        int i3;
        boolean z = true;
        mo19417jd(f);
        long nanoTime = System.nanoTime();
        C0128Bb.m1253gf("stepSimulation");
        if (i != 0) {
            try {
                this.fdC += f;
                if (this.fdC >= f2) {
                    i2 = (int) (this.fdC / f2);
                    this.fdC -= ((float) i2) * f2;
                    i3 = i;
                } else {
                    i2 = 0;
                    i3 = i;
                }
            } catch (Throwable th) {
                C0128Bb.blS();
                C0128Bb.dMX = (System.nanoTime() - nanoTime) / 1000000;
                throw th;
            }
        } else {
            this.fdC = f;
            if (C0920NU.m7656hc(f)) {
                i2 = 0;
                f2 = f;
                i3 = 0;
            } else {
                i2 = 1;
                f2 = f;
                i3 = 1;
            }
        }
        if (aDs() != null) {
            if ((aDs().bnd() & 16) == 0) {
                z = false;
            }
            C0128Bb.dMG = z;
        }
        if (i2 != 0) {
            mo19411iX(f2);
            bPy();
            if (i2 <= i3) {
                i3 = i2;
            }
            for (int i4 = 0; i4 < i3; i4++) {
                mo19412iY(f2);
                bPz();
            }
        }
        bPz();
        aDV();
        C0128Bb.blS();
        C0128Bb.dMX = (System.nanoTime() - nanoTime) / 1000000;
        return i2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: iY */
    public void mo19412iY(float f) {
        C0128Bb.m1253gf("internalSingleStepSimulation");
        try {
            mo19416jc(f);
            C5408aIe aDq = aDq();
            aDq.aGM = f;
            aDq.dTw = 0;
            aDq.iaO = aDs();
            aDm();
            bPA();
            bPD().aGM = f;
            mo19402a(bPD());
            mo19415jb(f);
            mo19413iZ(f);
            mo19414ja(f);
        } finally {
            C0128Bb.blS();
        }
    }

    /* renamed from: K */
    public void mo1647K(Vec3f vec3f) {
        this.fdB.set(vec3f);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.czT.size()) {
                C6238ajC c = C6238ajC.m22755c((ayY) this.czT.get(i2));
                if (c != null) {
                    c.mo13893K(vec3f);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* renamed from: b */
    public void mo1662b(C6238ajC ajc) {
        mo17719b(ajc);
    }

    /* renamed from: a */
    public void mo1653a(C6238ajC ajc) {
        if (!ajc.cEX()) {
            ajc.mo13893K(this.fdB);
        }
        if (ajc.cEZ() != null) {
            boolean z = !ajc.cEV() && !ajc.cEW();
            mo17707a((ayY) ajc, z ? (short) 1 : 2, z ? (short) 31 : 29);
        }
    }

    /* renamed from: a */
    public void mo19403a(C6238ajC ajc, short s, short s2) {
        if (!ajc.cEX()) {
            ajc.mo13893K(this.fdB);
        }
        if (ajc.cEZ() != null) {
            mo17707a((ayY) ajc, s, s2);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: iZ */
    public void mo19413iZ(float f) {
        C0128Bb.m1253gf("updateVehicles");
        int i = 0;
        while (true) {
            try {
                int i2 = i;
                if (i2 < this.fdF.size()) {
                    this.fdF.get(i2).mo7422kL(f);
                    i = i2 + 1;
                } else {
                    return;
                }
            } finally {
                C0128Bb.blS();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ja */
    public void mo19414ja(float f) {
        C0128Bb.m1253gf("updateActivationState");
        int i = 0;
        while (i < this.czT.size()) {
            try {
                C6238ajC c = C6238ajC.m22755c((ayY) this.czT.get(i));
                if (c != null) {
                    c.mo13935jG(f);
                    if (c.cew()) {
                        if (c.cEX()) {
                            c.mo17050wv(2);
                        } else if (c.cFa() == 1) {
                            c.mo17050wv(3);
                        }
                    } else if (c.cFa() != 4) {
                        c.mo17050wv(1);
                    }
                }
                i++;
            } finally {
                C0128Bb.blS();
            }
        }
    }

    /* renamed from: a */
    public void mo1651a(C1083Pr pr, boolean z) {
        this.fdA.add(pr);
        if (z) {
            pr.bnn().mo13927e(pr);
            pr.bno().mo13927e(pr);
        }
    }

    /* renamed from: b */
    public void mo1660b(C1083Pr pr) {
        this.fdA.remove(pr);
        pr.bnn().mo13928f(pr);
        pr.bno().mo13928f(pr);
    }

    /* renamed from: a */
    public void mo1652a(C1725ZV zv) {
        this.fdF.add(zv);
    }

    /* renamed from: b */
    public void mo1661b(C1725ZV zv) {
        this.fdF.remove(zv);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo19402a(C5718aUc auc) {
        C0128Bb.m1253gf("solveConstraints");
        try {
            this.f8064Uv.clear();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.fdA.size()) {
                    break;
                }
                this.f8064Uv.add(this.fdA.get(i2));
                i = i2 + 1;
            }
            C3696tt.m39764a(this.f8064Uv, fdI);
            this.fdH.mo19418a(auc, this.fdy, aDT() != 0 ? this.f8064Uv : null, this.f8064Uv.size(), this.f6423Ux, this.czU);
            this.fdy.mo20163n(bPC().aDt(), bPC().aDp().bJa());
            this.fdz.mo15398a(bPC().aDp(), bPC().aDu(), this.fdH);
            this.fdy.mo20162a(auc, this.f6423Ux);
        } finally {
            C0128Bb.blS();
        }
    }

    /* access modifiers changed from: protected */
    public void bPA() {
        C0128Bb.m1253gf("calculateSimulationIslands");
        try {
            bPB().mo15400a(bPC(), bPC().aDp());
            int size = this.fdA.size();
            for (int i = 0; i < size; i++) {
                C1083Pr pr = this.fdA.get(i);
                C6238ajC bnn = pr.bnn();
                C6238ajC bno = pr.bno();
                if (bnn != null && bnn.cEU() && bno != null && bno.cEU() && (bnn.isActive() || bno.isActive())) {
                    bPB().cqn().mo5901aH(bnn.cFk(), bno.cFk());
                }
            }
            bPB().mo15399a(bPC());
        } finally {
            C0128Bb.blS();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: jb */
    public void mo19415jb(float f) {
        C0128Bb.m1253gf("integrateTransforms");
        this.stack.bcJ().push();
        try {
            C3978xf xfVar = (C3978xf) this.stack.bcJ().get();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.czT.size()) {
                    C6238ajC c = C6238ajC.m22755c((ayY) this.czT.get(i2));
                    if (c != null && c.isActive() && !c.cEX()) {
                        c.mo13898a(f, xfVar);
                        c.mo13930h(xfVar);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        } finally {
            this.stack.bcJ().pop();
            C0128Bb.blS();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: jc */
    public void mo19416jc(float f) {
        C0128Bb.m1253gf("predictUnconstraintMotion");
        int i = 0;
        while (true) {
            try {
                int i2 = i;
                if (i2 < this.czT.size()) {
                    C6238ajC c = C6238ajC.m22755c((ayY) this.czT.get(i2));
                    if (c != null && !c.cEX() && c.isActive()) {
                        c.mo13934jF(f);
                        c.mo13933jE(f);
                        c.mo13898a(f, c.cFh());
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            } finally {
                C0128Bb.blS();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: jd */
    public void mo19417jd(float f) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo19401a(float f, C3978xf xfVar, Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f ac = this.stack.bcH().mo4458ac(xfVar.bFG);
            Vec3f h = this.stack.bcH().mo4460h(f, 0.0f, 0.0f);
            xfVar.bFF.transform(h);
            Vec3f h2 = this.stack.bcH().mo4460h(0.0f, f, 0.0f);
            xfVar.bFF.transform(h2);
            Vec3f h3 = this.stack.bcH().mo4460h(0.0f, 0.0f, f);
            xfVar.bFF.transform(h3);
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            vec3f2.sub(ac, h);
            vec3f3.add(ac, h2);
            aDs().mo4811i(vec3f2, vec3f3, vec3f);
            vec3f2.add(ac, h2);
            vec3f3.add(ac, h);
            aDs().mo4811i(vec3f2, vec3f3, vec3f);
            vec3f2.add(ac, h);
            vec3f3.sub(ac, h2);
            aDs().mo4811i(vec3f2, vec3f3, vec3f);
            vec3f2.sub(ac, h2);
            vec3f3.sub(ac, h);
            aDs().mo4811i(vec3f2, vec3f3, vec3f);
            vec3f2.sub(ac, h);
            vec3f3.add(ac, h3);
            aDs().mo4811i(vec3f2, vec3f3, vec3f);
            vec3f2.add(ac, h3);
            vec3f3.add(ac, h);
            aDs().mo4811i(vec3f2, vec3f3, vec3f);
            vec3f2.add(ac, h);
            vec3f3.sub(ac, h3);
            aDs().mo4811i(vec3f2, vec3f3, vec3f);
            vec3f2.sub(ac, h3);
            vec3f3.sub(ac, h);
            aDs().mo4811i(vec3f2, vec3f3, vec3f);
            vec3f2.sub(ac, h2);
            vec3f3.add(ac, h3);
            aDs().mo4811i(vec3f2, vec3f3, vec3f);
            vec3f2.add(ac, h3);
            vec3f3.add(ac, h2);
            aDs().mo4811i(vec3f2, vec3f3, vec3f);
            vec3f2.add(ac, h2);
            vec3f3.sub(ac, h3);
            aDs().mo4811i(vec3f2, vec3f3, vec3f);
            vec3f2.sub(ac, h3);
            vec3f3.sub(ac, h2);
            aDs().mo4811i(vec3f2, vec3f3, vec3f);
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a */
    public void mo19404a(C3978xf xfVar, C0802Lc lc, Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            Vec3f ac = this.stack.bcH().mo4458ac(xfVar.bFG);
            vec3f2.set(1.0f, 0.0f, 0.0f);
            xfVar.bFF.transform(vec3f2);
            vec3f2.add(ac);
            aDs().mo4811i(ac, vec3f2, this.stack.bcH().mo4460h(1.0f, 0.0f, 0.0f));
            vec3f2.set(0.0f, 1.0f, 0.0f);
            xfVar.bFF.transform(vec3f2);
            vec3f2.add(ac);
            aDs().mo4811i(ac, vec3f2, this.stack.bcH().mo4460h(0.0f, 1.0f, 0.0f));
            vec3f2.set(0.0f, 0.0f, 1.0f);
            xfVar.bFF.transform(vec3f2);
            vec3f2.add(ac);
            aDs().mo4811i(ac, vec3f2, this.stack.bcH().mo4460h(0.0f, 0.0f, 1.0f));
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a */
    public void mo1654a(C2849lA lAVar) {
        this.fdE = false;
        this.fdy = lAVar;
    }

    public C2849lA aDS() {
        return this.fdy;
    }

    public int aDT() {
        return this.fdA.size();
    }

    /* renamed from: gF */
    public C1083Pr mo1664gF(int i) {
        return this.fdA.get(i);
    }

    public C6560apM bPB() {
        return this.fdz;
    }

    public C2225d bPC() {
        return this;
    }

    public aVG aDU() {
        return aVG.DISCRETE_DYNAMICS_WORLD;
    }

    public C5718aUc bPD() {
        return this.f8063Ut;
    }

    /* renamed from: a.hr$a */
    private static class C2637a implements C6560apM.C1959a {

        /* renamed from: Ut */
        public C5718aUc f8065Ut;

        /* renamed from: Uu */
        public C2849lA f8066Uu;

        /* renamed from: Uv */
        public List<C1083Pr> f8067Uv;

        /* renamed from: Uw */
        public int f8068Uw;

        /* renamed from: Ux */
        public C1072Pg f8069Ux;

        /* renamed from: Uy */
        public C1701ZB f8070Uy;

        private C2637a() {
        }

        /* synthetic */ C2637a(C2637a aVar) {
            this();
        }

        /* renamed from: a */
        public void mo19418a(C5718aUc auc, C2849lA lAVar, List<C1083Pr> list, int i, C1072Pg pg, C1701ZB zb) {
            this.f8065Ut = auc;
            this.f8066Uu = lAVar;
            this.f8067Uv = list;
            this.f8068Uw = i;
            this.f8069Ux = pg;
            this.f8070Uy = zb;
        }

        /* renamed from: a */
        public void mo15403a(List<ayY> list, int i, List<aGW> list2, int i2, int i3, int i4) {
            int i5;
            if (i4 < 0) {
                this.f8066Uu.mo20161a(list, i, list2, i2, i3, this.f8067Uv, 0, this.f8068Uw, this.f8065Ut, this.f8069Ux, this.f8070Uy);
                return;
            }
            int i6 = -1;
            int i7 = 0;
            int i8 = 0;
            while (true) {
                int i9 = i8;
                if (i9 >= this.f8068Uw) {
                    i5 = i9;
                    break;
                } else if (C2636hr.m32976c(this.f8067Uv.get(i9)) == i4) {
                    i5 = i9;
                    i6 = i9;
                    break;
                } else {
                    i8 = i9 + 1;
                }
            }
            while (i5 < this.f8068Uw) {
                if (C2636hr.m32976c(this.f8067Uv.get(i5)) == i4) {
                    i7++;
                }
                i5++;
            }
            this.f8066Uu.mo20161a(list, i, list2, i2, i3, this.f8067Uv, i6, i7, this.f8065Ut, this.f8069Ux, this.f8070Uy);
        }
    }

    /* renamed from: a.hr$b */
    /* compiled from: a */
    private static class C2638b implements C1245SP, aTC {

        private final Vec3f cIe = new Vec3f();
        private final C3978xf cIf = new C3978xf();
        private final Vec3f cIg = new Vec3f();
        private final Vec3f cIh = new Vec3f();
        private final Vec3f cIi = new Vec3f();
        /* renamed from: Ux */
        private C1072Pg f8071Ux;

        public C2638b(C1072Pg pg, C3978xf xfVar, Vec3f vec3f) {
            this.f8071Ux = pg;
            this.cIf.mo22947a(xfVar);
            this.cIe.set(vec3f);
        }

        /* renamed from: b */
        public void mo819b(Vec3f[] vec3fArr, int i, int i2) {
            mo820a(vec3fArr, i, i2);
        }

        /* renamed from: a */
        public void mo820a(Vec3f[] vec3fArr, int i, int i2) {
            this.cIg.set(vec3fArr[0]);
            this.cIf.mo22946G(this.cIg);
            this.cIh.set(vec3fArr[1]);
            this.cIf.mo22946G(this.cIh);
            this.cIi.set(vec3fArr[2]);
            this.cIf.mo22946G(this.cIi);
            this.f8071Ux.mo4811i(this.cIg, this.cIh, this.cIe);
            this.f8071Ux.mo4811i(this.cIh, this.cIi, this.cIe);
            this.f8071Ux.mo4811i(this.cIi, this.cIg, this.cIe);
        }
    }
}
