package p001a;

import game.geometry.Vec3d;

/* renamed from: a.aro  reason: case insensitive filesystem */
/* compiled from: a */
class C6692aro {
    int grJ;
    float grK;
    float grL;
    float grM;
    boolean isDead;
    int time;
    private Vec3d grI = new Vec3d();

    public C6692aro() {
    }

    public C6692aro(C6692aro aro) {
        mo15861a(aro);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: kx */
    public void mo15866kx(float f) {
        this.grK = f;
    }

    /* access modifiers changed from: package-private */
    public float crY() {
        return this.grK;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo15861a(C6692aro aro) {
        this.grI.x = aro.grI.x;
        this.grI.y = aro.grI.y;
        this.grI.z = aro.grI.z;
        this.grJ = aro.grJ;
        this.time = aro.time;
        this.grK = aro.grK;
    }

    public Vec3d crZ() {
        return this.grI;
    }

    /* renamed from: X */
    public void mo15860X(Vec3d ajr) {
        this.grI.x = ajr.x;
        this.grI.y = ajr.y;
        this.grI.z = ajr.z;
    }

    public boolean isDead() {
        return this.isDead;
    }

    /* renamed from: ge */
    public void mo15864ge(boolean z) {
        this.isDead = z;
    }
}
