package p001a;

import logic.sql.C0535HW;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/* renamed from: a.FS */
/* compiled from: a */
public class C0393FS {
    private static String cWZ = System.getProperty("obfuscation.map.repository", ".");
    private String cWY;
    private List<C0535HW> exceptions;
    private BufferedReader reader;
    private String version;

    /* renamed from: eq */
    private static File m3222eq(String str) {
        File file = new File(cWZ, "ob-client." + str + ".map");
        if (!file.exists()) {
            file = new File(cWZ, "ob-client." + str + ".map.gz");
        }
        if (!file.exists()) {
            return null;
        }
        return file;
    }

    /* renamed from: z */
    public static String m3223z(String str, String str2) {
        InputStream fileInputStream;
        int indexOf;
        File eq = m3222eq(str2);
        if (eq == null) {
            System.out.println("Could not find obfuscation map for version " + str2);
            return str;
        }
        try {
            if (eq.getName().endsWith(".gz")) {
                fileInputStream = new GZIPInputStream(new FileInputStream(eq));
            } else {
                fileInputStream = new FileInputStream(eq);
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    return str;
                }
                int indexOf2 = readLine.indexOf("->");
                if (indexOf2 >= 0 && (indexOf = readLine.indexOf(58, indexOf2 + 2)) >= 0) {
                    String trim = readLine.substring(0, indexOf2).trim();
                    if (readLine.substring(indexOf2 + 2, indexOf).trim().equals(str)) {
                        return trim;
                    }
                }
            }
        } catch (IOException e) {
            return str;
        }
    }

    /* renamed from: eo */
    public void mo2151eo(String str) {
        ZipFile zipFile = new ZipFile(str);
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry zipEntry = (ZipEntry) entries.nextElement();
            if (zipEntry.getName().equals("console.log")) {
                this.reader = new BufferedReader(new InputStreamReader(zipFile.getInputStream(zipEntry)));
                return;
            }
        }
        this.reader = null;
    }

    public void closeFile() {
        if (this.reader != null) {
            this.reader.close();
        }
    }

    public void parse() {
        if (this.reader != null) {
            this.exceptions = new ArrayList();
            this.version = aQt();
            this.cWY = "true";
            if (this.version != null) {
                System.out.println("Version is " + this.version);
            } else {
                System.out.println("No version found");
            }
            while (true) {
                C0535HW aQs = aQs();
                if (aQs != null) {
                    this.exceptions.add(aQs);
                } else {
                    return;
                }
            }
        }
    }

    private C0535HW aQs() {
        String str;
        String str2;
        String str3;
        int i = -1;
        Pattern compile = Pattern.compile("^([a-zA-Z]+(\\.[a-zA-Z$]+)+)(: (.*))?");
        Pattern compile2 = Pattern.compile("^\tat (([a-zA-Z]+\\.)+)[a-zA-Z$]+\\(((.+:([0-9]+)|Unknown Source))\\)");
        while (true) {
            String readLine = this.reader.readLine();
            if (readLine == null) {
                str = null;
                str2 = null;
                break;
            }
            m3221ep(readLine);
            Matcher matcher = compile.matcher(readLine);
            if (matcher.find()) {
                str2 = matcher.group(1);
                if (matcher.groupCount() >= 4) {
                    str = matcher.group(4);
                } else {
                    str = null;
                }
            }
        }
        if (str2 == null) {
            return null;
        }
        this.reader.mark(1000);
        String readLine2 = this.reader.readLine();
        if (readLine2 != null) {
            Matcher matcher2 = compile2.matcher(readLine2);
            if (matcher2.find()) {
                String group = matcher2.group(1);
                if (group.endsWith(".")) {
                    group = group.substring(0, group.length() - 1);
                }
                if (matcher2.groupCount() >= 5) {
                    try {
                        i = Integer.parseInt(matcher2.group(5));
                        str3 = group;
                    } catch (NumberFormatException e) {
                        str3 = group;
                    }
                } else {
                    str3 = group;
                }
                this.reader.reset();
                return new C0535HW(str2, this.version, str3, i, str);
            }
        }
        str3 = null;
        this.reader.reset();
        return new C0535HW(str2, this.version, str3, i, str);
    }

    /* renamed from: ep */
    private void m3221ep(String str) {
        if (Pattern.compile("ClientMain:  \\*\\*\\* terminated \\*\\*\\*").matcher(str).find()) {
            System.out.println("Client finished normally.");
            this.cWY = "false";
        }
    }

    private String aQt() {
        Pattern compile = Pattern.compile("ClientMain: Starting taikodom client (\\S+)");
        while (true) {
            String readLine = this.reader.readLine();
            if (readLine == null) {
                return null;
            }
            Matcher matcher = compile.matcher(readLine);
            if (matcher.find() && matcher.groupCount() >= 1) {
                return matcher.group(1).trim();
            }
        }
    }

    public String getVersion() {
        return this.version;
    }

    public List<C0535HW> aQu() {
        return this.exceptions;
    }

    public String aQv() {
        return this.cWY;
    }
}
