package p001a;

import com.hoplon.geometry.Vec3f;

import javax.vecmath.Vector3f;

/* renamed from: a.zL */
/* compiled from: a */
public class C4092zL {
    private static final float cbc = 0.01f;
    private final Vec3f aAq = new Vec3f();
    private final C1422Up[] cbh = new C1422Up[3];
    /* renamed from: Oj */
    private Vec3f f9654Oj = new Vec3f();
    private int[] cbd = new int[3];
    private C4092zL[] cbe = new C4092zL[3];
    private boolean cbf = false;
    private float cbg = 0.0f;
    private boolean cbi;

    public C4092zL(C1422Up up, C1422Up up2, C1422Up up3) {
        Vec3f bxH = up.bxH();
        Vec3f bxH2 = up2.bxH();
        Vec3f bxH3 = up3.bxH();
        this.aAq.set(bxH2);
        this.aAq.mo23513o(bxH);
        this.f9654Oj.set(bxH3);
        this.f9654Oj.mo23513o(bxH).mo23489d((Vector3f) this.aAq);
        this.cbi = bxH.mo23476b(bxH2).dot(this.f9654Oj) < cbc && bxH2.mo23476b(bxH3).dot(this.f9654Oj) < cbc && bxH3.mo23476b(bxH).dot(this.f9654Oj) < cbc;
        this.f9654Oj.normalize();
        this.cbh[0] = up;
        this.cbh[1] = up2;
        this.cbh[2] = up3;
        this.cbg = Math.max(0.0f, this.f9654Oj.dot(bxH));
    }

    /* renamed from: a */
    public static void m41527a(C4092zL zLVar, int i, C4092zL zLVar2, int i2) {
        zLVar.mo23287a(i, i2, zLVar2);
        zLVar2.mo23287a(i2, i, zLVar);
    }

    /* renamed from: fM */
    public C4092zL mo23291fM(int i) {
        return this.cbe[i];
    }

    /* renamed from: fN */
    public int mo23292fN(int i) {
        return this.cbd[i];
    }

    /* renamed from: fO */
    public C1422Up mo23293fO(int i) {
        return this.cbh[i];
    }

    /* renamed from: vO */
    public Vec3f mo23296vO() {
        return this.f9654Oj;
    }

    public float asz() {
        return this.cbg;
    }

    public boolean isObsolete() {
        return this.cbf;
    }

    public boolean asA() {
        return this.cbi;
    }

    /* renamed from: a */
    public void mo23287a(int i, int i2, C4092zL zLVar) {
        this.cbe[i] = zLVar;
        this.cbd[i] = i2;
    }

    /* renamed from: bK */
    public void mo23290bK(boolean z) {
        this.cbf = z;
    }

    public String toString() {
        return "Face:  [" + this.cbh[0] + " " + this.cbh[1] + " " + this.cbh[2] + "], normal " + this.f9654Oj + ", plane distance " + this.cbg;
    }
}
