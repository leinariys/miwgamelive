package p001a;

import game.network.message.externalizable.C0939Nn;
import game.script.corporation.Corporation;
import game.script.player.Player;
import logic.baa.C6200aiQ;
import logic.baa.aDJ;
import logic.data.link.C0274DX;
import logic.data.link.C1643YC;
import logic.res.code.C5663aRz;
import logic.ui.C2698il;
import logic.ui.item.FormattedTextField;
import taikodom.addon.IAddonProperties;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

/* renamed from: a.pA */
/* compiled from: a */
public class C3198pA extends C1023Ov {
    /* access modifiers changed from: private */
    public C2698il edQ;
    private C6200aiQ<aDJ> edR = new C3200b();
    private C6200aiQ<aDJ> edS = new C3199a();

    public C3198pA(IAddonProperties vWVar, C2698il ilVar, Player aku, Corporation aPVar) {
        super(vWVar, aku, aPVar);
        this.edQ = ilVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: Fa */
    public void mo4555Fa() {
        JTextArea cd = this.edQ.mo4915cd("descriptionInput");
        cd.addFocusListener(new C3204f(cd));
        this.edQ.mo4913cb("creditButton").addActionListener(new C3203e());
        this.edQ.mo4913cb("drawButton").addActionListener(new C3202d());
        this.dLu.mo8320a(C0274DX.f421vE, (C6200aiQ<?>) this.edS);
        this.dLu.mo5156Qw().mo8320a(C1643YC.eMu, (C6200aiQ<?>) this.edR);
    }

    /* access modifiers changed from: package-private */
    public void blJ() {
    }

    /* access modifiers changed from: package-private */
    public void refresh() {
        mo4562c(this.edQ);
        JTextArea cd = this.edQ.mo4915cd("descriptionInput");
        cd.setText(this.dLu.getDescription() == null ? "" : this.dLu.getDescription());
        cd.setEnabled(this.dLu.mo10718b(this.f1342P, C6704asA.EDIT_CORP_DESCRIPTION));
        this.edQ.mo4917cf("balance").setText(String.valueOf(this.dLu.mo5156Qw().bSs()));
        this.edQ.mo4913cb("drawButton").setEnabled(this.dLu.mo10718b(this.f1342P, C6704asA.TAKE_MONEY));
    }

    /* access modifiers changed from: package-private */
    public void removeListeners() {
        this.dLu.mo8326b(C0274DX.f421vE, (C6200aiQ<?>) this.edS);
        this.dLu.mo5156Qw().mo8326b(C1643YC.eMu, (C6200aiQ<?>) this.edR);
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public void m36974i(Corporation aPVar) {
        mo4558a(new C3201c(aPVar), this.f1343kj.translate("Please inform the amount (T$) to be transferred to the Corporation"), "integer");
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public void m36975j(Corporation aPVar) {
        mo4558a(new C3205g(aPVar), this.f1343kj.translate("Please inform the amount (T$) to be taken from the Corporation"), "integer");
    }

    /* renamed from: a.pA$b */
    /* compiled from: a */
    class C3200b implements C6200aiQ<aDJ> {
        C3200b() {
        }

        /* renamed from: a */
        public void mo1143a(aDJ adj, C5663aRz arz, Object obj) {
            C3198pA.this.edQ.mo4917cf("balance").setText(obj.toString());
        }
    }

    /* renamed from: a.pA$a */
    class C3199a implements C6200aiQ<aDJ> {
        C3199a() {
        }

        /* renamed from: a */
        public void mo1143a(aDJ adj, C5663aRz arz, Object obj) {
            C3198pA.this.edQ.mo4915cd("descriptionInput").setText(obj.toString());
        }
    }

    /* renamed from: a.pA$f */
    /* compiled from: a */
    class C3204f extends FocusAdapter {
        private final /* synthetic */ JTextArea aTc;

        C3204f(JTextArea jTextArea) {
            this.aTc = jTextArea;
        }

        public void focusLost(FocusEvent focusEvent) {
            C3198pA.this.dLu.setDescription(this.aTc.getText().trim());
        }
    }

    /* renamed from: a.pA$e */
    /* compiled from: a */
    class C3203e implements ActionListener {
        C3203e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3198pA.this.m36974i(C3198pA.this.dLu);
        }
    }

    /* renamed from: a.pA$d */
    /* compiled from: a */
    class C3202d implements ActionListener {
        C3202d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3198pA.this.m36975j(C3198pA.this.dLu);
        }
    }

    /* renamed from: a.pA$c */
    /* compiled from: a */
    class C3201c extends aEP {
        private final /* synthetic */ Corporation aTb;

        C3201c(Corporation aPVar) {
            this.aTb = aPVar;
        }

        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            if (i == 1) {
                try {
                    this.aTb.mo10720cB(Long.parseLong(str.trim()));
                } catch (Exception e) {
                    C3198pA.this.f1343kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, C3198pA.this.f1343kj.translate("Invalid value."), new Object[0]));
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: VC */
        public boolean mo8595VC() {
            return true;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo8597a(FormattedTextField rAVar) {
            rAVar.setFormat("integer");
            rAVar.mo44h(9);
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.CONFIRM;
        }
    }

    /* renamed from: a.pA$g */
    /* compiled from: a */
    class C3205g extends aEP {
        private final /* synthetic */ Corporation aTb;

        C3205g(Corporation aPVar) {
            this.aTb = aPVar;
        }

        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            if (i == 1) {
                this.aTb.mo10721cz(Long.parseLong(str.trim()));
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: VC */
        public boolean mo8595VC() {
            return true;
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.CONFIRM;
        }
    }
}
