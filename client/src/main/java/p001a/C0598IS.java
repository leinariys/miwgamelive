package p001a;

import logic.render.IEngineGraphics;
import logic.res.ConfigIniKeyValue;
import logic.res.ConfigManager;
import logic.res.ConfigManagerSection;
import logic.thred.LogPrinter;
import taikodom.infra.script.I18NString;

import java.io.IOException;

/* renamed from: a.IS */
/* compiled from: a */
public class C0598IS {
    private static final LogPrinter dhs = LogPrinter.setClass(C0598IS.class);

    C0598IS() {
    }

    /* renamed from: a */
    public static C2581hD m5381a(ConfigManager aao) {
        float f;
        ConfigManagerSection kW = aao.getSection("render");
        C2581hD hDVar = C2581hD.ixx;
        Integer a = kW.mo7195a(ConfigIniKeyValue.XRES, Integer.valueOf((int) hDVar.dmL().getX()));
        Integer a2 = kW.mo7195a(ConfigIniKeyValue.YRES, Integer.valueOf((int) hDVar.dmL().getY()));
        Integer a3 = kW.mo7195a(ConfigIniKeyValue.REFRESH_RATE, Integer.valueOf((int) hDVar.dmL().getZ()));
        Boolean a4 = kW.mo7191a(ConfigIniKeyValue.ENABLE_FULLSCREEN, Boolean.valueOf(hDVar.isFullscreen()));
        Integer a5 = kW.mo7195a(ConfigIniKeyValue.TEXTURE_QUALITY, Integer.valueOf(hDVar.dmM().getValue()));
        Integer a6 = kW.mo7195a(ConfigIniKeyValue.TEXTURE_FILTER, Integer.valueOf(hDVar.dmN().getValue()));
        Integer a7 = kW.mo7195a(ConfigIniKeyValue.ANTI_ALIASING, Integer.valueOf(hDVar.dmO().getValue()));
        Boolean a8 = kW.mo7191a(ConfigIniKeyValue.ENABLE_POST_PROCESSING_FX, Boolean.valueOf(hDVar.isPostProcessingFX()));
        Integer a9 = kW.mo7195a(ConfigIniKeyValue.SHADER_QUALITY, Integer.valueOf(hDVar.dmP().getValue()));
        Float a10 = kW.mo7193a(ConfigIniKeyValue.ANISOTROPIC_FILTER, Float.valueOf(hDVar.dmQ().getValue()));
        Float a11 = kW.mo7193a(ConfigIniKeyValue.LOD_QUALITY, Float.valueOf(hDVar.dmR().getValue()));
        Float a12 = kW.mo7193a(ConfigIniKeyValue.VOL_MUSIC, Float.valueOf(hDVar.aek()));
        Float a13 = kW.mo7193a(ConfigIniKeyValue.VOL_AMBIENCE, Float.valueOf(hDVar.adX()));
        Float a14 = kW.mo7193a(ConfigIniKeyValue.VOL_GUI, Float.valueOf(hDVar.aeg()));
        Float a15 = kW.mo7193a(ConfigIniKeyValue.VOL_FX, Float.valueOf(hDVar.aed()));
        Float a16 = kW.mo7193a(ConfigIniKeyValue.VOL_ENGINE, Float.valueOf(hDVar.aeb()));
        Integer a17 = kW.mo7195a(ConfigIniKeyValue.ENVIRONMENT_FX_QUALITY, Integer.valueOf(hDVar.dmS().getValue()));
        String a18 = aao.getSection(ConfigIniKeyValue.LANGUAGE.getValue()).getValuePropertyString(ConfigIniKeyValue.LANGUAGE, I18NString.getCurrentLocation());
        C2581hD hDVar2 = new C2581hD(a.intValue(), a2.intValue(), a3.intValue(), a4 == null ? true : a4.booleanValue(), m5390e(a5), m5391f(a6), m5392g(a7), a8 == null ? false : a8.booleanValue(), m5393h(a9), m5388b(a10), m5380a(a11), m5389d(a17));
        hDVar2.setLanguage(a18);
        hDVar2.mo19205dq((a12 == null || a12.floatValue() > 1.0f || a12.floatValue() < 0.0f) ? 0.0f : a12.floatValue());
        if (a13 == null || a13.floatValue() > 1.0f || a13.floatValue() < 0.0f) {
            f = 0.0f;
        } else {
            f = a13.floatValue();
        }
        hDVar2.mo19193dm(f);
        hDVar2.mo19203do((a15 == null || a15.floatValue() > 1.0f || a15.floatValue() < 0.0f) ? 0.0f : a15.floatValue());
        hDVar2.mo19204dp((a14 == null || a14.floatValue() > 1.0f || a14.floatValue() < 0.0f) ? 0.0f : a14.floatValue());
        hDVar2.mo19202dn((a16 == null || a16.floatValue() > 1.0f || a16.floatValue() < 0.0f) ? 0.0f : a16.floatValue());
        return hDVar2;
    }

    /* renamed from: d */
    private static C2581hD.C2583b m5389d(Integer num) {
        if (num == null) {
            return C2581hD.C2583b.values()[0];
        }
        for (C2581hD.C2583b bVar : C2581hD.C2583b.values()) {
            if (bVar.getValue() == num.intValue()) {
                return bVar;
            }
        }
        return C2581hD.C2583b.values()[0];
    }

    /* renamed from: a */
    private static C2581hD.C2584c m5380a(Float f) {
        float f2 = 100000.0f;
        C2581hD.C2584c cVar = C2581hD.C2584c.HIGH;
        C2581hD.C2584c[] values = C2581hD.C2584c.values();
        int length = values.length;
        int i = 0;
        while (i < length) {
            C2581hD.C2584c cVar2 = values[i];
            if (cVar2.getValue() == f.floatValue()) {
                return cVar2;
            }
            float abs = Math.abs(cVar2.getValue() - f.floatValue());
            if (abs >= f2) {
                cVar2 = cVar;
                abs = f2;
            }
            i++;
            cVar = cVar2;
            f2 = abs;
        }
        return cVar;
    }

    /* renamed from: a */
    public static boolean m5387a(ConfigManager aao, C2581hD hDVar) {
        ConfigManagerSection yr;
        String cbs = ConfigIniKeyValue.XRES.getValue();
        ConfigManagerSection kW = aao.getSection(cbs);
        if (kW == null) {
            kW = new ConfigManagerSection();
            aao.sectionOptions.put(cbs, kW);
        }
        boolean a = m5383a(kW, ConfigIniKeyValue.ANISOTROPIC_FILTER, hDVar.dmQ().getValue()) | m5384a(kW, ConfigIniKeyValue.XRES, (int) hDVar.dmL().getX()) | m5384a(kW, ConfigIniKeyValue.YRES, (int) hDVar.dmL().getY()) | m5384a(kW, ConfigIniKeyValue.REFRESH_RATE, (int) hDVar.dmL().getZ()) | m5386a(kW, ConfigIniKeyValue.ENABLE_FULLSCREEN, hDVar.isFullscreen()) | m5384a(kW, ConfigIniKeyValue.TEXTURE_FILTER, hDVar.dmN().getValue()) | m5384a(kW, ConfigIniKeyValue.ANTI_ALIASING, hDVar.dmO().getValue()) | m5386a(kW, ConfigIniKeyValue.ENABLE_POST_PROCESSING_FX, hDVar.isPostProcessingFX());
        kW.mo7202b(ConfigIniKeyValue.TEXTURE_QUALITY, Integer.valueOf(hDVar.dmM().getValue()));
        kW.mo7202b(ConfigIniKeyValue.SHADER_QUALITY, Integer.valueOf(hDVar.dmP().getValue()));
        kW.mo7201b(ConfigIniKeyValue.LOD_QUALITY, Float.valueOf(hDVar.dmR().getValue()));
        kW.mo7202b(ConfigIniKeyValue.ENVIRONMENT_FX_QUALITY, Integer.valueOf(hDVar.dmS().getValue()));
        kW.mo7201b(ConfigIniKeyValue.VOL_MUSIC, Float.valueOf(hDVar.aek()));
        kW.mo7201b(ConfigIniKeyValue.VOL_AMBIENCE, Float.valueOf(hDVar.adX()));
        kW.mo7201b(ConfigIniKeyValue.VOL_GUI, Float.valueOf(hDVar.aeg()));
        kW.mo7201b(ConfigIniKeyValue.VOL_FX, Float.valueOf(hDVar.aed()));
        kW.mo7201b(ConfigIniKeyValue.VOL_ENGINE, Float.valueOf(hDVar.aeb()));
        String cbs2 = ConfigIniKeyValue.LANGUAGE.getValue();
        ConfigManagerSection kW2 = aao.getSection(cbs2);
        if (kW2 == null) {
            ConfigManagerSection yr2 = new ConfigManagerSection();
            aao.sectionOptions.put(cbs2, kW);
            yr = yr2;
        } else {
            yr = kW2;
        }
        boolean a2 = m5385a(yr, ConfigIniKeyValue.LANGUAGE, hDVar.getLanguage()) | a;
        try {
            aao.loadConfigIni("config/user.ini");
        } catch (IOException e) {
            dhs.error("Could not save configuration!");
        }
        return a2;
    }

    /* renamed from: a */
    private static boolean m5384a(ConfigManagerSection yr, ConfigIniKeyValue ahy, int i) {
        boolean z = true;
        try {
            if (yr.mo7199b(ahy) == i) {
                z = false;
            }
        } catch (Exception e) {
        }
        if (z) {
            yr.mo7202b(ahy, Integer.valueOf(i));
        }
        return z;
    }

    /* renamed from: a */
    private static boolean m5383a(ConfigManagerSection yr, ConfigIniKeyValue ahy, float f) {
        boolean z = true;
        try {
            if (yr.mo7209c(ahy).floatValue() == f) {
                z = false;
            }
        } catch (Exception e) {
        }
        if (z) {
            yr.mo7201b(ahy, Float.valueOf(f));
        }
        return z;
    }

    /* renamed from: a */
    private static boolean m5386a(ConfigManagerSection yr, ConfigIniKeyValue ahy, boolean z) {
        boolean z2;
        try {
            z2 = yr.mo7211d(ahy).booleanValue() ^ z;
        } catch (Exception e) {
            z2 = true;
        }
        if (z2) {
            yr.mo7200b(ahy, Boolean.valueOf(z));
        }
        return z2;
    }

    /* renamed from: a */
    private static boolean m5385a(ConfigManagerSection yr, ConfigIniKeyValue ahy, String str) {
        boolean z = true;
        try {
            if (yr.mo7197a(ahy).equals(str)) {
                z = false;
            }
        } catch (Exception e) {
        }
        if (z) {
            yr.mo7203b(ahy, str);
        }
        return z;
    }

    /* renamed from: e */
    private static C2581hD.C2585d m5390e(Integer num) {
        if (num == null) {
            return C2581hD.C2585d.values()[0];
        }
        for (C2581hD.C2585d dVar : C2581hD.C2585d.values()) {
            if (dVar.getValue() == num.intValue()) {
                return dVar;
            }
        }
        return C2581hD.C2585d.values()[0];
    }

    /* renamed from: f */
    private static C2581hD.C2582a m5391f(Integer num) {
        if (num == null) {
            return C2581hD.C2582a.values()[0];
        }
        for (C2581hD.C2582a aVar : C2581hD.C2582a.values()) {
            if (aVar.getValue() == num.intValue()) {
                return aVar;
            }
        }
        return C2581hD.C2582a.values()[0];
    }

    /* renamed from: g */
    private static C2581hD.C2588g m5392g(Integer num) {
        if (num == null) {
            return C2581hD.C2588g.values()[0];
        }
        for (C2581hD.C2588g gVar : C2581hD.C2588g.values()) {
            if (gVar.getValue() == num.intValue()) {
                return gVar;
            }
        }
        return C2581hD.C2588g.values()[0];
    }

    /* renamed from: h */
    private static C2581hD.C2586e m5393h(Integer num) {
        if (num == null) {
            return C2581hD.C2586e.values()[0];
        }
        for (C2581hD.C2586e eVar : C2581hD.C2586e.values()) {
            if (eVar.getValue() == num.intValue()) {
                return eVar;
            }
        }
        return C2581hD.C2586e.values()[0];
    }

    /* renamed from: b */
    private static C2581hD.C2587f m5388b(Float f) {
        if (f == null) {
            return C2581hD.C2587f.values()[0];
        }
        for (C2581hD.C2587f fVar : C2581hD.C2587f.values()) {
            if (fVar.getValue() == f.floatValue()) {
                return fVar;
            }
        }
        return C2581hD.C2587f.values()[0];
    }

    /* renamed from: a */
    public static void m5382a(IEngineGraphics abd, C2581hD hDVar) {
        abd.mo3061dq(hDVar.aek());
        abd.mo3057dm(hDVar.adX());
        abd.mo3060dp(hDVar.aeg());
        abd.mo3059do(hDVar.aed());
        abd.mo3058dn(hDVar.aeb());
    }
}
