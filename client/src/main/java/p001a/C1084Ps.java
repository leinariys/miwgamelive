package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.network.message.serializable.C6400amI;
import game.script.ai.npc.TurretAIController;
import game.script.missile.Missile;
import game.script.ship.Ship;
import game.script.simulation.Space;
import logic.bbb.C4029yK;
import org.mozilla1.javascript.ScriptRuntime;

/* renamed from: a.Ps */
/* compiled from: a */
public class C1084Ps extends C1714ZM {
    /* renamed from: dX */
    private final float f1387dX;
    private final float dlX;
    private final float maxPitch;
    private C1039PF dQo;
    private float dQp;
    private float dQq;
    private Quat4fWrap dQr;
    private Quat4fWrap dQs;
    private Quat4fWrap dQt;
    private C6400amI dQu;
    private transient C0763Kt stack = C0763Kt.bcE();

    public C1084Ps(Space ea, Turret jq, C4029yK yKVar) {
        super(ea, jq, yKVar);
        this.f1387dX = jq.mo963VH();
        this.maxPitch = jq.bac().getMaxPitch();
        this.dlX = jq.bac().clO();
        this.dQp = 0.0f;
        this.dQq = 0.0f;
        this.dQr = new Quat4fWrap();
        this.dQs = new Quat4fWrap();
        this.dQt = new Quat4fWrap();
        this.dQu = new C6400amI();
        this.dQo = new C1039PF();
        this.dQo.mo4627p(jq.mo963VH());
        this.dQo.mo4625hi(jq.bac().clS().getSpeed());
        mo2450a((C3735uM) this.dQo);
    }

    /* renamed from: a */
    public boolean mo2545a(C0520HN hn) {
        if (hn == aSo()) {
            return false;
        }
        if (hn instanceof C1084Ps) {
            return false;
        }
        if (hn instanceof C6901avp) {
            return false;
        }
        if (hn instanceof aBA) {
            return false;
        }
        if (!(hn instanceof C6486anq) || ((Missile) hn.aSp()).cLw() != bak().aZS()) {
            return super.mo2545a(hn);
        }
        return false;
    }

    /* renamed from: IM */
    public byte mo2438IM() {
        return super.mo2438IM();
    }

    public void step(float f) {
        super.step(f);
    }

    public Turret bak() {
        return (Turret) aSp();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo2448a(float f, C6400amI ami) {
        this.stack = this.stack.bcD();
        this.stack.bcN().push();
        this.stack.bcH().push();
        this.stack.bcI().push();
        try {
            Turret jq = (Turret) aSp();
            C1003Om aSl = jq.aZS().aiD().aSl();
            Vec3d ajr = (Vec3d) this.stack.bcI().get();
            ajr.mo9484aA(aSl.blk());
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            aSl.bll().mo15204F(jq.aZU().mo16402gL(), vec3f);
            ajr.add(vec3f);
            this.cYX.blk().mo9484aA(ajr);
            this.dQq += this.dQu.bln().y * f;
            this.dQp += this.dQu.bln().x * f;
            if (this.dQp > this.maxPitch) {
                this.dQp = this.maxPitch;
            }
            if (this.dQp < this.dlX) {
                this.dQp = this.dlX;
            }
            if (this.dQq > 360.0f) {
                this.dQq -= 360.0f;
            }
            if (this.dQq < -360.0f) {
                this.dQq += 360.0f;
            }
            Quat4fWrap bll = this.cYX.bll();
            bll.mo15242f(aSl.bll());
            bll.mul(jq.aZU().getOrientation());
            this.dQr.mo15242f(bll);
            this.dQs.mo15246i(ScriptRuntime.NaN, (double) bas(), ScriptRuntime.NaN);
            this.dQt.mo15246i((double) baq(), ScriptRuntime.NaN, ScriptRuntime.NaN);
            bll.mul(this.dQs);
            bll.mul(this.dQt);
        } finally {
            this.stack.bcN().pop();
            this.stack.bcH().pop();
            this.stack.bcI().pop();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: hf */
    public void mo4860hf(float f) {
        if (bak().aZS() != null && !bak().aZS().isDisposed() && bak().aZS().bae() && bak().mo2998hb() != null) {
            TurretAIController aqs = (TurretAIController) bak().mo2998hb();
            this.eQm += f;
            if (this.eQm > 0.05f) {
                aqs.step(this.eQm);
                this.eQm = 0.0f;
            }
        }
    }

    /* renamed from: s */
    private boolean m8637s(Vec3f vec3f) {
        return Float.isNaN(vec3f.x) || Float.isInfinite(vec3f.x) || Float.isNaN(vec3f.y) || Float.isInfinite(vec3f.y) || Float.isNaN(vec3f.z) || Float.isInfinite(vec3f.z);
    }

    public Vec3f aSf() {
        return this.dQu.bln();
    }

    public float baq() {
        return this.dQp;
    }

    public float bas() {
        return this.dQq;
    }

    /* renamed from: f */
    public void mo2567f(float f, float f2, float f3) {
        wake();
        float f4 = -f2;
        if (f4 > this.f1387dX) {
            f4 = this.f1387dX;
        } else if (f4 < (-this.f1387dX)) {
            f4 = -this.f1387dX;
        }
        if (f3 > this.f1387dX) {
            f3 = this.f1387dX;
        } else if (f3 < (-this.f1387dX)) {
            f3 = -this.f1387dX;
        }
        this.dQu.bln().x = f3;
        this.dQu.bln().y = f4;
        if (Float.isNaN(f3) || Float.isInfinite(f3) || Float.isNaN(f4) || Float.isInfinite(f4)) {
            System.err.println("Setting invalid angular speed for " + this + " Pitch: " + f3 + " Yaw: " + f4);
        }
    }

    public Quat4fWrap getOrientation() {
        return this.cYX.bll();
    }

    public Quat4fWrap aSd() {
        return getOrientation();
    }

    public Vec3d getPosition() {
        return this.cYX.blk();
    }

    public Quat4fWrap bnt() {
        return this.dQr;
    }

    public Quat4fWrap bnu() {
        return this.dQs;
    }

    public Quat4fWrap bnv() {
        return this.dQt;
    }

    public Vec3d bnw() {
        this.stack.bcI().push();
        this.stack.bcH().push();
        this.stack.bcN().push();
        try {
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            Quat4fWrap orientation = bak().aZS().getOrientation();
            orientation.mo15204F(bak().aZU().mo16402gL(), vec3f);
            Quat4fWrap aoy = (Quat4fWrap) this.stack.bcN().get();
            aoy.mul(orientation, bak().aZU().getOrientation());
            aoy.mul(this.dQs);
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            vec3f2.set(vec3f);
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            aoy.mo15204F(bak().bac().cny().mo16402gL(), vec3f3);
            vec3f2.add(vec3f3);
            Vec3d ajr = (Vec3d) this.stack.bcI().get();
            ajr.mo9484aA(bak().aZS().getPosition());
            ajr.add(vec3f2);
            return ajr;
        } finally {
            this.stack.bcI().pop();
            this.stack.bcH().pop();
            this.stack.bcN().pop();
        }
    }

    public Vec3d aSe() {
        return getPosition();
    }

    public Vec3f agx() {
        this.stack.bcN().push();
        this.stack.bcH().push();
        try {
            Quat4fWrap orientation = bak().aZU().getOrientation();
            Quat4fWrap aoy = (Quat4fWrap) this.stack.bcN().get();
            aoy.mo15246i(ScriptRuntime.NaN, (double) bas(), ScriptRuntime.NaN);
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            vec3f.set(bak().aZU().mo16402gL());
            Quat4fWrap aoy2 = (Quat4fWrap) this.stack.bcN().get();
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            aoy2.mo15242f(orientation);
            aoy2.mul(aoy);
            aoy2.mo15204F(bak().bac().cny().mo16402gL(), vec3f2);
            vec3f.add(vec3f2);
            Quat4fWrap aoy3 = (Quat4fWrap) this.stack.bcN().get();
            aoy3.mo15246i((double) baq(), ScriptRuntime.NaN, ScriptRuntime.NaN);
            aoy2.mo15242f(orientation);
            aoy2.mul(aoy3);
            aoy3.mo15242f(aoy);
            aoy3.mul(aoy2);
            aoy3.mo15204F(bak().bac().cnA().mo16402gL(), vec3f2);
            vec3f.add(vec3f2);
            return vec3f;
        } finally {
            this.stack.bcN().pop();
            this.stack.bcH().pop();
        }
    }

    public C1003Om bnx() {
        return this.cYX;
    }

    /* renamed from: b */
    public void mo4851b(Vec3d ajr, Vec3f vec3f) {
        this.stack.bcI().push();
        try {
            Vec3d ajr2 = (Vec3d) this.stack.bcI().get();
            ajr2.mo9484aA(ajr);
            ajr2.sub(bnw());
            getOrientation().mo15203E(ajr2.dfV(), vec3f);
        } finally {
            this.stack.bcI().pop();
        }
    }

    public boolean isReady() {
        Pawn aZS = ((Turret) aSp()).aZS();
        if (aZS.aiD() == null) {
            return false;
        }
        return ((Ship) aZS).ahO().isReady();
    }
}
