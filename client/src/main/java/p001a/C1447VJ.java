package p001a;

import game.script.item.Item;
import logic.swing.C5378aHa;
import logic.ui.IBaseUiTegXml;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.VJ */
/* compiled from: a */
public class C1447VJ {
    private final Item aoR;
    private Icon icon;
    private Image image;

    public C1447VJ(Item auq) {
        this.aoR = auq;
    }

    public Image getImage() {
        if (this.image == null) {
            this.image = IBaseUiTegXml.initBaseUItegXML().adz().getImage(C5378aHa.ITEMS + this.aoR.bAP().mo12100sK().getHandle());
        }
        return this.image;
    }

    public Icon getIcon() {
        if (this.icon == null) {
            this.icon = new C2539gY(this.aoR);
        }
        return this.icon;
    }

    /* renamed from: XH */
    public Item mo5990XH() {
        return this.aoR;
    }

    public String toString() {
        return this.aoR.toString();
    }
}
