package p001a;

/* renamed from: a.aJn  reason: case insensitive filesystem */
/* compiled from: a */
public class C5443aJn {
    private static Object ifP;
    private static boolean ifQ = false;
    private static boolean ifR = false;

    /* renamed from: aJ */
    public static synchronized void m15868aJ(Object obj) {
        synchronized (C5443aJn.class) {
            if (ifR) {
                dfG();
            }
            if (ifQ) {
                m15867T(obj);
            } else {
                ifQ = true;
                ifP = obj;
                dfG();
                ifQ = false;
                C5443aJn.class.notify();
            }
        }
    }

    /* renamed from: T */
    private static void m15867T(Object obj) {
        ifR = true;
        m15869aK(obj);
        ifR = false;
        C5443aJn.class.notify();
        dfG();
    }

    /* renamed from: aK */
    private static void m15869aK(Object obj) {
        if (obj != null || ifP != null) {
            if (obj == null) {
                obj = "null";
            }
            if (!obj.equals(ifP)) {
                throw new IllegalStateException("One thread produced: " + ifP + "  the other produced: " + obj);
            }
        }
    }

    private static void dfG() {
        try {
            C5443aJn.class.wait();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
