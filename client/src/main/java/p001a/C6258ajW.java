package p001a;

import taikodom.render.camera.Camera;
import taikodom.render.scene.SceneObject;

import java.io.Externalizable;

/* renamed from: a.ajW  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C6258ajW implements C3735uM {
    public Camera camera;
    public float fSB = 110.0f;
    public float fSC = 55.0f;
    public float fSD = 55.0f;
    public float fSE = 60.0f;
    public C0501Gx fSF;
    public SceneObject fSG;

    public C6258ajW(Camera camera2) {
        this.camera = camera2;
    }

    /* renamed from: a */
    public void mo3861a(Externalizable externalizable) {
    }

    /* renamed from: j */
    public Externalizable mo3862j(int i) {
        return null;
    }

    public float cfL() {
        return this.fSB;
    }

    /* renamed from: jL */
    public void mo14132jL(float f) {
        this.fSB = f;
    }

    public float cfM() {
        return this.fSC;
    }

    /* renamed from: jM */
    public void mo14133jM(float f) {
        this.fSC = f;
    }

    public void setCamera(Camera camera2) {
        this.camera = camera2;
    }

    public C0501Gx cfN() {
        return this.fSF;
    }

    /* renamed from: b */
    public void mo14128b(C0501Gx gx) {
        this.fSF = gx;
    }

    public SceneObject cfO() {
        return this.fSG;
    }

    /* renamed from: m */
    public void mo11822m(SceneObject sceneObject) {
        this.fSG = sceneObject;
    }
}
