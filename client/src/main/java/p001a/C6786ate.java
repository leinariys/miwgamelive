package p001a;

import game.network.message.serializable.C2991mi;

/* renamed from: a.ate  reason: case insensitive filesystem */
/* compiled from: a */
public class C6786ate<T> implements C2991mi<T> {

    private final C0651JP cUJ;

    public C6786ate(C0651JP jp) {
        this.cUJ = jp;
    }

    public C0651JP aPk() {
        return this.cUJ;
    }

    public int index() {
        return -1;
    }

    public Object value() {
        return null;
    }

    public String toString() {
        return String.valueOf(getClass().getSimpleName()) + " " + this.cUJ;
    }
}
