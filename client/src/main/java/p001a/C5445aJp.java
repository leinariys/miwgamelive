package p001a;

/* renamed from: a.aJp  reason: case insensitive filesystem */
/* compiled from: a */
public class C5445aJp {
    public long ifX;
    public float ifY = 0.0f;
    public long ifZ = 0;
    public long iga = Long.MAX_VALUE;
    public long igb = 0;

    public void set(long j) {
        this.ifX = j;
        this.ifY = ((this.ifY * ((float) this.ifZ)) + ((float) this.ifX)) / ((float) (this.ifZ + 1));
        this.ifZ = Math.min(this.ifZ + 1, 199);
        if (this.ifX > this.igb) {
            this.igb = this.ifX;
        }
        if (this.ifX < this.iga) {
            this.iga = this.ifX;
        }
    }

    public void reset() {
        this.ifY = 0.0f;
        this.ifZ = 0;
        this.iga = Long.MAX_VALUE;
        this.igb = 0;
    }
}
