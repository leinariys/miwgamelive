package p001a;

/* renamed from: a.afV  reason: case insensitive filesystem */
/* compiled from: a */
public class C6049afV {
    private long swigCPtr;

    public C6049afV(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6049afV() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m21556a(C6049afV afv) {
        if (afv == null) {
            return 0;
        }
        return afv.swigCPtr;
    }
}
