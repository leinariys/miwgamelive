package p001a;

import game.geometry.Quat4fWrap;

/* renamed from: a.qa */
/* compiled from: a */
public class C3352qa {
    /* renamed from: c */
    public static String m37726c(Quat4fWrap aoy) {
        if (aoy == null) {
            return null;
        }
        return String.valueOf(String.valueOf(aoy.x)) + ";" + String.valueOf(aoy.y) + ";" + String.valueOf(aoy.z) + ";" + String.valueOf(aoy.w);
    }

    /* renamed from: be */
    public static Quat4fWrap m37725be(String str) {
        int indexOf;
        int indexOf2;
        int indexOf3;
        if (str == null || "null".equals(str) || "".equals(str) || (indexOf = str.indexOf(";")) == -1 || (indexOf2 = str.indexOf(";", indexOf + 1)) == -1 || (indexOf3 = str.indexOf(";", indexOf2 + 1)) == -1) {
            return null;
        }
        return new Quat4fWrap(Float.parseFloat(str.substring(0, indexOf)), Float.parseFloat(str.substring(indexOf + 1, indexOf2)), Float.parseFloat(str.substring(indexOf2 + 1, indexOf3)), Float.parseFloat(str.substring(indexOf3 + 1)));
    }
}
