package p001a;

import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import logic.aaa.C0752Kk;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.addon.IAddonProperties;
import taikodom.render.camera.Camera;
import taikodom.render.camera.OrbitalCamera;
import taikodom.render.scene.SceneObject;

import javax.vecmath.Vector3d;

/* renamed from: a.Ei */
/* compiled from: a */
public class C0354Ei implements C6296akI.C1925a {
    private static final long cRy = 3000;
    long cRG;
    Quat4fWrap cRH;
    private SceneObject ajd;
    private Vec3d cRA;
    private Vec3d cRB;
    private Vec3d cRC = new Vec3d();
    private Vec3d cRD = new Vec3d();
    private Vec3d cRE = new Vec3d();
    private double cRF;
    private Vec3d cRz;

    /* renamed from: kj */
    private IAddonProperties f508kj;

    /* renamed from: t */
    private double f509t;

    public C0354Ei(IAddonProperties vWVar, SceneObject sceneObject, Vec3d ajr) {
        int i;
        double random = Math.random();
        if (Math.random() < 0.5d) {
            i = -1;
        } else {
            i = 1;
        }
        this.cRF = ((double) i) * random;
        this.cRG = 0;
        this.cRH = new Quat4fWrap();
        this.f508kj = vWVar;
        this.ajd = sceneObject;
        this.cRz = aNQ();
        this.cRA = m3021c(this.cRz, ajr);
        this.cRB = ajr;
        this.f509t = ScriptRuntime.NaN;
        sceneObject.setPosition(this.cRz);
    }

    private Vec3d aNQ() {
        Vec3d position = this.ajd.getPosition();
        Camera adY = this.f508kj.ale().adY();
        if (adY instanceof OrbitalCamera) {
            ((OrbitalCamera) adY).computeTransformation();
        }
        Vec3d ajr = new Vec3d(position);
        ajr.sub(adY.getGlobalTransform().position);
        ajr.mo9505d((Vector3d) new Vec3d(ajr.x + 1.0d, ajr.y + this.cRF, ajr.z + 1.0d));
        ajr.normalize();
        ajr.scale(200.0d);
        ajr.y = 150.0d;
        this.ajd.getTransform().mo17331a(ajr);
        return ajr;
    }

    /* renamed from: c */
    private Vec3d m3021c(Vec3d ajr, Vec3d ajr2) {
        Vec3d ajr3 = new Vec3d(ajr);
        Vec3d ajr4 = new Vec3d(ajr2);
        ajr3.scale(1.0d - 0.2d);
        ajr4.scale(0.2d);
        ajr3.add(ajr4);
        ajr3.y = ajr2.y + ((ajr3.y - ajr2.y) * 0.2d);
        return ajr3;
    }

    /* renamed from: a */
    public void mo1968a(float f, long j) {
        long j2 = (long) (1000.0f * f);
        if (this.cRG == 0) {
            this.cRG = j2;
            return;
        }
        this.cRG = j2 + this.cRG;
        this.cRG /= 2;
        this.f509t = (((double) this.cRG) / 3000.0d) + this.f509t;
        if (this.f509t > 1.0d) {
            this.ajd.setPosition(this.cRB);
            this.f508kj.ale().mo3044b((C6296akI.C1925a) this);
            this.f508kj.aVU().mo13975h(new C0752Kk());
            return;
        }
        this.cRC.mo9484aA(this.cRz);
        this.cRD.mo9484aA(this.cRA);
        this.cRE.mo9484aA(this.cRB);
        this.cRC.scale(Math.pow(1.0d - this.f509t, 2.0d));
        this.cRD.scale(this.f509t * 2.0d * (1.0d - this.f509t));
        this.cRE.scale(Math.pow(this.f509t, 2.0d));
        this.cRC.add(this.cRD);
        this.cRC.add(this.cRE);
        this.ajd.setPosition(this.cRC);
    }
}
