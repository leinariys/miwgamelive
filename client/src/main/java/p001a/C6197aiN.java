package p001a;

import game.network.message.C0474GZ;
import game.network.message.externalizable.C6302akO;
import logic.baa.C1616Xf;
import logic.data.mbean.C0677Jd;
import logic.data.mbean.C6064afk;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aiN  reason: case insensitive filesystem */
/* compiled from: a */
public class C6197aiN extends C6302akO {

    private C0677Jd fOG;

    public C6197aiN() {
    }

    public C6197aiN(C6064afk afk) {
        this.fOG = (C0677Jd) afk;
    }

    public C6064afk ayZ() {
        return this.fOG;
    }

    public void readExternal(ObjectInput objectInput) {
        if (objectInput.readBoolean()) {
            this.fOG = (C0677Jd) ((C1616Xf) C0474GZ.dah.inputReadObject(objectInput)).mo13W();
            this.fOG.mo3158a(objectInput, 0);
        }
    }

    public void writeExternal(ObjectOutput objectOutput) {
        if (this.fOG != null) {
            objectOutput.writeBoolean(true);
            C0474GZ.dah.serializeData(objectOutput, (Object) this.fOG.mo11944yn());
            this.fOG.mo3186b(objectOutput, 0);
            return;
        }
        objectOutput.writeBoolean(false);
    }
}
