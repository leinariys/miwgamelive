package p001a;

import game.network.ProxyTyp;
import logic.baa.C5473aKr;
import logic.baa.C6200aiQ;
import logic.baa.aDJ;
import logic.bbb.aDR;
import logic.res.code.C5663aRz;
import logic.res.html.C1867ad;

import java.util.Date;

/* renamed from: a.aNl  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5545aNl {
    /* renamed from: a */
    void mo5596a(C5663aRz arz, C5473aKr<?, ?> akr);

    /* renamed from: a */
    void mo5598a(C5663aRz arz, C6200aiQ<?> aiq);

    /* renamed from: aG */
    C1867ad<? extends aDJ> mo10512aG(Class<? extends aDJ> cls);

    /* renamed from: b */
    void mo5603b(C5663aRz arz, C5473aKr<?, ?> akr);

    /* renamed from: b */
    void mo5604b(C5663aRz arz, C6200aiQ<?> aiq);

    void bFR();

    aDR bGF();

    boolean bGX();

    boolean bGY();

    boolean bGZ();

    boolean bHa();

    boolean bHb();

    Date bxM();

    void cVf();

    boolean cVh();

    ProxyTyp cVk();

    void cVm();

    long cVn();

    long cVr();

    /* renamed from: dj */
    boolean mo5607dj();

    /* renamed from: dr */
    void mo10527dr();

    /* renamed from: du */
    boolean mo10528du();

    /* renamed from: g */
    void mo10529g(C5663aRz arz, Object obj);

    /* renamed from: ms */
    void mo10530ms(float f);

    /* renamed from: mt */
    <T extends aDJ> T mo10531mt(float f);

    /* renamed from: n */
    aDJ mo10532n(Class<? extends aDJ> cls);

    /* renamed from: w */
    Object mo10533w(C5663aRz arz);

    /* renamed from: w */
    void mo10534w(aDR adr);
}
