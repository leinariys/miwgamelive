package p001a;

import game.script.Character;
import logic.baa.C1616Xf;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.atU  reason: case insensitive filesystem */
/* compiled from: a */
public class C6776atU extends C3813vL {
    private static final Character gCw = 0;

    /* renamed from: d */
    public void mo7919d(ObjectOutput objectOutput, Object obj) {
        objectOutput.writeChar(((Character) obj).charValue());
    }

    /* renamed from: f */
    public Object mo7920f(ObjectInput objectInput) {
        return Character.valueOf(objectInput.readChar());
    }

    /* renamed from: b */
    public Object mo3591b(C1616Xf xf) {
        return gCw;
    }
}
