package p001a;

import java.util.List;

/* renamed from: a.ZB */
/* compiled from: a */
public abstract class C1701ZB {
    /* renamed from: a */
    public abstract C3808vG mo581a(ayY ayy, ayY ayy2, aGW agw);

    /* renamed from: a */
    public abstract void mo583a(aGW agw);

    /* renamed from: a */
    public abstract void mo586a(C3180oo ooVar, C5408aIe aie, C1701ZB zb);

    /* renamed from: b */
    public abstract void mo587b(aGW agw);

    /* renamed from: b */
    public abstract boolean mo588b(ayY ayy, ayY ayy2);

    public abstract int bJa();

    public abstract List<aGW> bJb();

    /* renamed from: c */
    public abstract boolean mo591c(ayY ayy, ayY ayy2);

    /* renamed from: g */
    public abstract aGW mo594g(Object obj, Object obj2);

    /* renamed from: po */
    public abstract aGW mo595po(int i);

    /* renamed from: a */
    public final C3808vG mo7323a(ayY ayy, ayY ayy2) {
        return mo581a(ayy, ayy2, (aGW) null);
    }
}
