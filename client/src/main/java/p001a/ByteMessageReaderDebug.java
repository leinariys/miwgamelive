package p001a;

import game.network.message.ByteMessageReader;

import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: a.amW  reason: case insensitive filesystem */
/* compiled from: a */
public class ByteMessageReaderDebug extends ByteMessageReader {
    private ArrayList<String> gca = new ArrayList<>();

    public ByteMessageReaderDebug(byte[] bArr) {
        super(bArr);
    }

    public ByteMessageReaderDebug(byte[] bArr, int i, int i2) {
        super(bArr, i, i2);
    }

    /* renamed from: a */
    public int readBits(int i) {
        int a = super.readBits(i);
        StringBuilder sb = new StringBuilder();
        sb.append("int : readBits(").append(i).append(") -> ").append(a);
        this.gca.add(sb.toString());
        return a;
    }

    public int read() {
        int read = super.read();
        this.gca.add("int : read() -> " + read);
        return read;
    }

    public int read(byte[] bArr, int i, int i2) {
        int read = super.read(bArr, i, i2);
        StringBuilder sb = new StringBuilder();
        sb.append("int : read(").append(bArr).append(", ").append(i).append(", ").append(i2).append(") -> ").append(read);
        this.gca.add(sb.toString());
        return read;
    }

    public long skip(long j) {
        long skip = super.skip(j);
        StringBuilder sb = new StringBuilder();
        sb.append("long : skip(").append(j).append(") -> ").append(skip);
        this.gca.add(sb.toString());
        return skip;
    }

    public void mark(int i) {
        super.mark(i);
        this.gca.add("mark(" + i + ")");
    }

    public void reset() {
        super.reset();
        this.gca.add("reset()");
    }

    public void setPas(int i) {
        super.setPas(i);
        this.gca.add("seek(" + i + ")");
    }

    public void readFully(byte[] bArr) {
        super.readFully(bArr);
        this.gca.add("readFully(" + bArr + ")");
    }

    public void readFully(byte[] bArr, int i, int i2) {
        super.readFully(bArr, i, i2);
        StringBuilder sb = new StringBuilder();
        sb.append("readFully(").append(bArr).append(", ").append(i).append(", ").append(i2).append(")");
        this.gca.add(sb.toString());
    }

    public int skipBytes(int i) {
        int skipBytes = super.skipBytes(i);
        StringBuilder sb = new StringBuilder();
        sb.append("int : skipBytes(").append(i).append(") -> ").append(skipBytes);
        this.gca.add(sb.toString());
        return skipBytes;
    }

    public boolean readBoolean() {
        boolean readBoolean = super.readBoolean();
        this.gca.add("boolean : readBoolean() -> " + readBoolean);
        return readBoolean;
    }

    public byte readByte() {
        byte readByte = super.readByte();
        this.gca.add("byte : readByte() -> " + readByte);
        return readByte;
    }

    public int readUnsignedByte() {
        int readUnsignedByte = super.readUnsignedByte();
        this.gca.add("int : readUnsignedByte() -> " + readUnsignedByte);
        return readUnsignedByte;
    }

    public short readShort() {
        short readShort = super.readShort();
        this.gca.add("short : readShort() -> " + readShort);
        return readShort;
    }

    public short readShort0() {
        short cjB = super.readShort0();
        this.gca.add("short : readShort0() -> " + cjB);
        return cjB;
    }

    public int readUnsignedShort() {
        int readUnsignedShort = super.readUnsignedShort();
        this.gca.add("int : readUnsignedShort() -> " + readUnsignedShort);
        return readUnsignedShort;
    }

    public int readUnsignedShort0() {
        int cjC = super.readUnsignedShort0();
        this.gca.add("int : readUnsignedShort0() -> " + cjC);
        return cjC;
    }

    public char readChar() {
        char readChar = super.readChar();
        this.gca.add("char : readChar() -> " + readChar);
        return readChar;
    }

    public int readInt() {
        int readInt = super.readInt();
        this.gca.add("int : readInt() -> " + readInt);
        return readInt;
    }

    public int readPositiveInt() {
        int cjD = super.readPositiveInt();
        this.gca.add("int : readPositiveInt() -> " + cjD);
        return cjD;
    }

    public int readInt0() {
        int cjE = super.readInt0();
        this.gca.add("int : readInt0() -> " + cjE);
        return cjE;
    }

    public int read3bytes() {
        int cjF = super.read3bytes();
        this.gca.add("int : read3bytes() -> " + cjF);
        return cjF;
    }

    public long readLong() {
        long readLong = super.readLong();
        this.gca.add("long : readLong() -> " + readLong);
        return readLong;
    }

    public long readLong0() {
        long cjG = super.readLong0();
        this.gca.add("long : readLong0() -> " + cjG);
        return cjG;
    }

    public float readFloat() {
        float readFloat = super.readFloat();
        this.gca.add("float : readFloat() -> " + readFloat);
        return readFloat;
    }

    public double readDouble() {
        double readDouble = super.readDouble();
        this.gca.add("double : readDouble() -> " + readDouble);
        return readDouble;
    }

    public String readLine() {
        String readLine = super.readLine();
        this.gca.add("String : readLine() -> " + readLine);
        return readLine;
    }

    public String readUTF() {
        String readUTF = super.readUTF();
        this.gca.add("String : readUTF() -> " + readUTF);
        return readUTF;
    }

    public String dump() {
        StringBuilder sb = new StringBuilder();
        sb.append("--- SimpleByteArrayInputStreamDebug DUMP ---\n");
        Iterator<String> it = this.gca.iterator();
        while (it.hasNext()) {
            sb.append(String.valueOf(it.next()) + "\n");
        }
        return sb.toString();
    }
}
