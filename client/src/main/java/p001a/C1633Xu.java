package p001a;

import java.text.NumberFormat;

/* renamed from: a.Xu */
/* compiled from: a */
public class C1633Xu {
    static {
        NumberFormat.getInstance().setMaximumFractionDigits(2);
    }

    /* renamed from: e */
    public static String m11724e(double d, double d2) {
        if (d > d2) {
            return String.valueOf(NumberFormat.getInstance().format(d / 1000.0d)) + "km";
        }
        return String.valueOf(NumberFormat.getInstance().format(d)) + "m";
    }
}
