package p001a;


import game.network.message.externalizable.C6950awp;

import java.util.*;

/* renamed from: a.aeD  reason: case insensitive filesystem */
/* compiled from: a */
final class C5979aeD implements C6705asB {
    private final Map<C6625aqZ, List<C6950awp>> map = new IdentityHashMap();

    /* renamed from: a */
    public void mo13024a(Collection<C6625aqZ> collection, C6950awp awp) {
        for (C6625aqZ next : collection) {
            List list = this.map.get(next);
            if (list == null) {
                Map<C6625aqZ, List<C6950awp>> map2 = this.map;
                list = new ArrayList();
                map2.put(next, list);
            }
            list.add(awp);
        }
    }

    public Map<C6625aqZ, List<C6950awp>> getMap() {
        return this.map;
    }

    /* renamed from: a */
    public void mo13023a(C6625aqZ aqz, C6950awp awp) {
        List list = this.map.get(aqz);
        if (list == null) {
            Map<C6625aqZ, List<C6950awp>> map2 = this.map;
            list = new ArrayList();
            map2.put(aqz, list);
        }
        list.add(awp);
    }
}
