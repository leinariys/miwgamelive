package p001a;

import game.network.aQD;
import org.apache.commons.jxpath.JXPathContext;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: a.apX  reason: case insensitive filesystem */
/* compiled from: a */
public class C6571apX extends aQD {
    /* renamed from: a */
    public String mo2073a(Iterator<?> it, List<String> list, Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        sb.append("<root>");
        while (it.hasNext()) {
            Object next = it.next();
            JXPathContext newContext = JXPathContext.newContext(next);
            newContext.setFunctions(this.iEM);
            sb.append("<").append(next.getClass().getSimpleName()).append(">");
            for (int i = 0; i < list.size(); i++) {
                try {
                    String str = list.get(i);
                    for (Object next2 : newContext.selectNodes(str)) {
                        sb.append("<").append(map.get(str)).append(">");
                        sb.append(next2);
                        sb.append("</").append(map.get(str)).append(">");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            sb.append("</").append(next.getClass().getSimpleName()).append(">");
        }
        sb.append("</root>");
        return sb.toString();
    }
}
