package p001a;

import logic.res.sound.C0907NJ;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.LogFactoryImpl;
import taikodom.render.loader.FileSceneLoader;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.provider.FilePath;
import taikodom.render.scene.RenderObject;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

/* renamed from: a.uD */
/* compiled from: a */
public abstract class C3724uD extends C6928awQ {
    private static final Log log = LogFactoryImpl.getLog(C3724uD.class);

    public C3724uD(FilePath ain, FileSceneLoader fileSceneLoader) {
        super(ain, fileSceneLoader);
    }

    /* renamed from: q */
    public <T extends RenderAsset> T mo22333q(String str, String str2) {
        return null;
    }

    /* renamed from: a */
    public C5622aQk mo22328a(String str, String str2, boolean z, Scene scene) {
        mo16690bV(str);
        C5622aQk aqk = new C5622aQk(scene);
        aqk.mo11067r((SceneObject) mo16689bT(str2));
        return aqk;
    }

    public float ajs() {
        return 0.0f;
    }

    public boolean isReady() {
        return false;
    }

    /* renamed from: a */
    public void mo22329a(String str, String str2, C0907NJ nj) {
    }

    /* renamed from: b */
    public RenderObject mo22331b(String str, String str2, C0907NJ nj) {
        return null;
    }

    /* renamed from: a */
    public void mo4993a(C5964ado ado) {
    }

    public void step(float f) {
    }

    /* renamed from: b */
    public void mo5001b(C5964ado ado) {
    }
}
