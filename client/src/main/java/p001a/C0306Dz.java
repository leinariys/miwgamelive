package p001a;

import com.hoplon.geometry.Color;
import game.script.Actor;
import game.script.Character;
import game.script.player.Player;
import game.script.ship.Ship;
import logic.swing.C5378aHa;
import logic.ui.item.Table;
import org.mozilla1.classfile.C0147Bi;
import taikodom.addon.IAddonProperties;
import taikodom.addon.hudmarkmanager.HudMarkManagerAddon;
import taikodom.addon.selection.HudObjectListAddon;

import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.Dz */
/* compiled from: a */
public class C0306Dz {
    public Table ehG;
    boolean hXJ = true;
    private HudObjectListAddon hXE;
    private int hXF;
    private C6415amX hXG;
    private ArrayList<Actor> hXH = new ArrayList<>();
    private Map<Actor, C2570gx> hXI = new HashMap();
    private HashMap<Color, java.awt.Color> hXK = new HashMap<>();

    public C0306Dz(HudObjectListAddon hudObjectListAddon, Table pzVar) {
        this.hXE = hudObjectListAddon;
        this.ehG = pzVar;
        ddp();
    }

    private void ddp() {
        this.hXG = new C6415amX(this);
        this.ehG.setModel(this.hXG);
        this.ehG.setRowSorter(new C6368alc(this.hXG));
        if (this.ehG.getTableHeader() != null) {
            this.ehG.getTableHeader().setReorderingAllowed(false);
        }
        this.ehG.setSelectionMode(0);
        this.ehG.aBh();
        this.ehG.addMouseListener(new C0307a());
        TableColumn column = this.ehG.getColumnModel().getColumn(0);
        if (column != null) {
            column.setPreferredWidth(1);
        }
        TableColumn column2 = this.ehG.getColumnModel().getColumn(1);
        TableColumn column3 = this.ehG.getColumnModel().getColumn(2);
        column.setCellRenderer(new azL());
        column2.setCellRenderer(new azL());
        column3.setCellRenderer(new azL());
        this.ehG.getRowSorter().toggleSortOrder(2);
    }

    public void ddq() {
        if (this.ehG != null && this.ehG.getModel() != null) {
            C6415amX model = this.ehG.getModel();
            this.ehG.getParent().setEnabled(model.getRowCount() > 0);
            model.fireTableDataChanged();
        }
    }

    /* access modifiers changed from: protected */
    public HudObjectListAddon ddr() {
        return this.hXE;
    }

    /* access modifiers changed from: protected */
    /* renamed from: JW */
    public IAddonProperties mo1787JW() {
        return this.hXE.mo24557JW();
    }

    public void cpQ() {
        this.hXH.clear();
        this.hXI.clear();
    }

    /* renamed from: f */
    public void mo1798f(Actor cr) {
        C2570gx gxVar = new C2570gx();
        this.hXI.put(cr, gxVar);
        this.hXH.add(cr);
        m2764a(cr, gxVar);
        this.hXJ = true;
    }

    /* access modifiers changed from: protected */
    public void dds() {
        for (Actor next : this.hXI.keySet()) {
            this.hXI.get(next).f7820QL = m2766cl(next);
        }
    }

    /* renamed from: e */
    public void mo1797e(Actor cr) {
        this.hXH.remove(cr);
        this.hXI.remove(cr);
        this.hXJ = true;
    }

    /* renamed from: yi */
    public Actor mo1801yi(int i) {
        return this.hXH.get(i);
    }

    /* renamed from: yj */
    public C2570gx mo1802yj(int i) {
        if (i >= this.hXH.size()) {
            return null;
        }
        return this.hXI.get(this.hXH.get(i));
    }

    public int ddt() {
        return this.hXH.size();
    }

    public C6415amX ddu() {
        return this.hXG;
    }

    public int getSelectedRow() {
        return this.hXF;
    }

    public void ddv() {
        this.hXF = -1;
    }

    /* renamed from: eu */
    private String m2771eu(float f) {
        if (f < 10000.0f) {
            return String.valueOf(Math.round(f)) + " m";
        }
        return String.valueOf(Math.round(((double) f) / 1000.0d)) + " km";
    }

    /* renamed from: cl */
    private String m2766cl(Actor cr) {
        return m2771eu((float) Math.round(this.hXE.mo24557JW().getPlayer().dxc().mo22061al().mo1011bv(cr)));
    }

    /* renamed from: a */
    private void m2764a(Actor cr, C2570gx gxVar) {
        gxVar.f7820QL = m2766cl(cr);
        gxVar.f7818QJ = m2769co(cr);
        gxVar.f7819QK = m2768cn(cr);
        gxVar.color = m2770cp(cr);
        gxVar.f7821QM = m2767cm(cr);
        m2765b(cr, gxVar);
    }

    /* renamed from: cm */
    private ImageIcon m2767cm(Actor cr) {
        String aOQ;
        if (!(cr instanceof Ship)) {
            return null;
        }
        Character agj = ((Ship) cr).agj();
        if (agj == null || !(agj instanceof Player)) {
            return null;
        }
        Player aku = (Player) agj;
        if (aku.dxy().bFK() == null || (aOQ = aku.dxy().bFK().aOQ()) == null || aOQ.length() <= 0) {
            return null;
        }
        return new ImageIcon(this.hXE.mo24557JW().bHv().adz().getImage(String.valueOf(C5378aHa.CORP_IMAGES.getPath()) + C0147Bi.SEPARATOR + aOQ));
    }

    /* renamed from: b */
    private void m2765b(Actor cr, C2570gx gxVar) {
        gxVar.name = cr.ahQ();
    }

    /* renamed from: cn */
    private ImageIcon m2768cn(Actor cr) {
        return new ImageIcon(this.hXE.mo24557JW().bHv().adz().getImage(String.valueOf(C5378aHa.HUD.getPath()) + cr.mo647iA()));
    }

    /* renamed from: co */
    private C0308E m2769co(Actor cr) {
        if (cr == this.hXE.cpK()) {
            return C0308E.BEACON;
        }
        return ((HudMarkManagerAddon) this.hXE.mo24557JW().mo11830U(HudMarkManagerAddon.class)).mo24067a(this.hXE.mo24557JW().getPlayer(), cr);
    }

    /* renamed from: cp */
    private java.awt.Color m2770cp(Actor cr) {
        return m2763X(cr.getColor());
    }

    /* renamed from: X */
    private java.awt.Color m2763X(Color color) {
        java.awt.Color color2 = this.hXK.get(color);
        if (color2 != null) {
            return color2;
        }
        java.awt.Color color3 = new java.awt.Color(color.getRed(), color.getGreen(), color.getBlue());
        this.hXK.put(color, color3);
        return color3;
    }

    public boolean ddw() {
        int indexOf;
        Ship al = this.hXE.mo24557JW().getPlayer().dxc().mo22061al();
        if (al == null || (indexOf = this.hXH.indexOf(al.agB())) <= -1 || this.ehG.getRowCount() <= 0) {
            return false;
        }
        try {
            int convertRowIndexToView = this.ehG.convertRowIndexToView(indexOf);
            this.ehG.addRowSelectionInterval(convertRowIndexToView, convertRowIndexToView);
            this.hXF = convertRowIndexToView;
            this.hXE.mo24558a(this);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean ddx() {
        return this.hXF < 0 || this.hXE.cpJ() != this;
    }

    public void updateSize() {
        if (this.hXJ) {
            this.hXE.updateSize();
            this.hXJ = false;
        }
    }

    /* renamed from: a.Dz$a */
    class C0307a extends MouseAdapter {
        C0307a() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            int selectedRow = C0306Dz.this.ehG.getSelectedRow();
            if (selectedRow >= 0) {
                C0306Dz.this.ehG.getModel().mo14841sE(C0306Dz.this.ehG.convertRowIndexToModel(selectedRow));
            }
        }
    }
}
