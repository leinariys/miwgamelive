package p001a;

import game.engine.DataGameEvent;
import logic.thred.LogPrinter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: a.Fm */
/* compiled from: a */
public class C0413Fm {
    /* access modifiers changed from: private */
    public static final AtomicInteger cVj = new AtomicInteger();
    static LogPrinter logger = LogPrinter.m10275K(C0413Fm.class);
    /* access modifiers changed from: private */

    /* renamed from: PL */
    public ReentrantLock f582PL = new ReentrantLock();
    /* access modifiers changed from: private */
    public TreeSet<C0415b> cVh = new TreeSet<>();
    /* access modifiers changed from: private */
    public TreeSet<C0415b> cVi = new TreeSet<>();
    CurrentTimeMilli cVg;
    private Executor executor = new C3531sC();

    public C0413Fm(CurrentTimeMilli ahw) {
        this.cVg = ahw;
    }

    /* renamed from: a */
    public C0415b mo2190a(C0414a aVar) {
        return new C0415b(aVar);
    }

    /* renamed from: a */
    public void mo2191a(C0415b bVar, long j, TimeUnit timeUnit, long j2, TimeUnit timeUnit2) {
        this.f582PL.lock();
        try {
            this.cVh.remove(bVar);
            boolean remove = this.cVi.remove(bVar);
            if (bVar.izf > 0) {
                bVar.izf = Math.min(this.cVg.currentTimeMillis() + timeUnit.toMillis(j), Math.max(this.cVg.currentTimeMillis(), bVar.izf));
            } else {
                bVar.izf = this.cVg.currentTimeMillis() + timeUnit.toMillis(j);
            }
            if (j2 > 0) {
                bVar.ize = timeUnit2.toNanos(j2);
            } else {
                bVar.ize = 0;
            }
            bVar.izi = false;
            if (!bVar.aKE) {
                this.cVh.add(bVar);
            }
            if (remove) {
                this.cVi.add(bVar);
            }
        } finally {
            this.f582PL.unlock();
        }
    }

    /* renamed from: gZ */
    public void mo2194gZ() {
        this.f582PL.lock();
        try {
            this.cVi.clear();
            ArrayList arrayList = new ArrayList(this.cVh);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                if (it.next() == null) {
                    it.remove();
                }
            }
            this.cVi.addAll(arrayList);
            while (true) {
                this.f582PL.lock();
                try {
                    if (this.cVi.size() != 0 && this.cVh.size() != 0) {
                        C0415b first = this.cVi.first();
                        if (this.cVg.currentTimeMillis() < first.izf) {
                            break;
                        }
                        this.cVh.remove(first);
                        this.cVi.remove(first);
                        first.aKE = true;
                        first.izg = first.izf;
                        if (first.ize > 0) {
                            first.izf = first.izf + first.ize;
                        } else {
                            first.izf = 0;
                            first.izi = true;
                        }
                        if (first != null) {
                            this.executor.execute(first.izk);
                        } else {
                            return;
                        }
                    } else {
                        break;
                    }
                } finally {
                    this.f582PL.unlock();
                }
            }
            this.f582PL.unlock();
        } finally {
            this.f582PL.unlock();
        }
    }

    public Executor getExecutor() {
        return this.executor;
    }

    public void setExecutor(Executor executor2) {
        this.executor = executor2;
    }

    public String aPn() {
        return this.cVh.toString();
    }

    /* renamed from: c */
    public void mo2193c(DataGameEvent jz) {
        this.cVg = jz;
    }

    /* renamed from: a.Fm$a */
    public interface C0414a {
        /* renamed from: a */
        void mo2197a(C0415b bVar);
    }

    /* renamed from: a.Fm$b */
    /* compiled from: a */
    public class C0415b implements Comparable<C0415b> {
        final Runnable izk;
        /* access modifiers changed from: private */
        public boolean aKE;
        /* access modifiers changed from: private */
        public long ize;
        /* access modifiers changed from: private */
        public long izf;
        public long izg;
        /* access modifiers changed from: private */
        public boolean izi;
        /* renamed from: id */
        private int f583id = C0413Fm.cVj.incrementAndGet();
        private long izh;
        private C0414a izj;

        public C0415b(C0414a aVar) {
            this.izh = C0413Fm.this.cVg.currentTimeMillis();
            this.aKE = false;
            this.izk = new C6891avf(this);
            this.izj = aVar;
        }

        public void cancel() {
            C0413Fm.this.f582PL.lock();
            try {
                C0413Fm.this.cVh.remove(this);
                C0413Fm.this.cVi.remove(this);
                this.izi = true;
            } finally {
                C0413Fm.this.f582PL.unlock();
            }
        }

        /* access modifiers changed from: private */
        public void dnw() {
            try {
                this.izj.mo2197a(this);
                this.izh = this.izg;
                C0413Fm.this.f582PL.lock();
                try {
                    this.aKE = false;
                    if (!this.izi) {
                        C0413Fm.this.cVh.remove(this);
                        C0413Fm.this.cVh.add(this);
                    }
                } finally {
                    C0413Fm.this.f582PL.unlock();
                }
            } catch (Throwable th) {
                this.izh = this.izg;
                C0413Fm.this.f582PL.lock();
                try {
                    this.aKE = false;
                    if (!this.izi) {
                        C0413Fm.this.cVh.remove(this);
                        C0413Fm.this.cVh.add(this);
                    }
                    throw th;
                } finally {
                    C0413Fm.this.f582PL.unlock();
                }
            } finally {
            }
        }

        /* renamed from: b */
        public int compareTo(C0415b bVar) {
            if (bVar == this) {
                return 0;
            }
            long j = this.izf - bVar.izf;
            if (j < 0) {
                return -1;
            }
            if (j > 0) {
                return 1;
            }
            return this.f583id - bVar.f583id;
        }

        public long dnx() {
            return this.izg;
        }

        public long dny() {
            return this.izh;
        }

        public String toString() {
            return "task to " + this.izf + " " + this.izj;
        }

        public boolean isCancelled() {
            return this.izi;
        }
    }
}
