package p001a;

import game.network.message.C1408Ue;
import game.network.message.C3767um;
import game.network.message.C6972axl;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: a.AR */
/* compiled from: a */
public class C0032AR implements C3767um {
    Set<C6972axl> dfY = new HashSet();
    C6972axl dfZ = null;
    ReentrantLock dga = new ReentrantLock(true);
    int dgb = 0;
    Set<C0033a> dgc = new HashSet();

    /* renamed from: a */
    public C1408Ue mo279a(C6972axl axl) {
        return new C0034b(axl);
    }

    /* renamed from: a.AR$a */
    private class C0033a {
        final boolean ckg;
        C6972axl ckh;

        public C0033a(boolean z, C6972axl axl) {
            this.ckg = z;
            this.ckh = axl;
        }
    }

    /* renamed from: a.AR$b */
    /* compiled from: a */
    class C0034b implements C1408Ue {
        private final /* synthetic */ C6972axl ikL;

        C0034b(C6972axl axl) {
            this.ikL = axl;
        }

        public void bxk() {
            C0032AR.this.dga.lock();
            try {
                if (C0032AR.this.dgb > 0) {
                    C0032AR.this.dgc.add(new C0033a(true, this.ikL));
                } else if (C0032AR.this.dfZ != null) {
                    C0032AR.this.dgc.add(new C0033a(true, this.ikL));
                    C0032AR.this.dgb++;
                    C0032AR.this.dfZ.cbv();
                } else {
                    C0032AR.this.dfY.add(this.ikL);
                    this.ikL.cbt();
                }
            } finally {
                C0032AR.this.dga.unlock();
            }
        }

        public void bxl() {
            C0032AR.this.dga.lock();
            try {
                if (C0032AR.this.dgb > 0) {
                    C0032AR.this.dgc.add(new C0033a(false, this.ikL));
                } else if (C0032AR.this.dfZ != null) {
                    C0032AR.this.dgc.add(new C0033a(false, this.ikL));
                    C0032AR.this.dgb++;
                    C0032AR.this.dfZ.cbv();
                } else if (!C0032AR.this.dfY.isEmpty()) {
                    C0032AR.this.dgc.add(new C0033a(false, this.ikL));
                    for (C6972axl cbu : C0032AR.this.dfY) {
                        C0032AR.this.dgb++;
                        cbu.cbu();
                    }
                } else {
                    C0032AR.this.dfZ = this.ikL;
                    C0032AR.this.dfZ.cbw();
                }
            } finally {
                C0032AR.this.dga.unlock();
            }
        }

        public void bxm() {
            C0032AR.this.dga.lock();
            try {
                if (!C0032AR.this.dfY.remove(this.ikL)) {
                    throw new RuntimeException("Unexpected read lock release from " + this.ikL);
                }
                C0032AR ar = C0032AR.this;
                int i = ar.dgb - 1;
                ar.dgb = i;
                if (i == 0) {
                    dhE();
                }
            } finally {
                C0032AR.this.dga.unlock();
            }
        }

        public void bxn() {
            C0032AR.this.dga.lock();
            try {
                C0032AR.this.dfZ = null;
                C0032AR ar = C0032AR.this;
                int i = ar.dgb - 1;
                ar.dgb = i;
                if (i == 0) {
                    dhE();
                }
                if (C0032AR.this.dgb != 0) {
                    throw new RuntimeException("requestings still != 0 after a write lock released (" + C0032AR.this.dgb + ")");
                }
            } finally {
                C0032AR.this.dga.unlock();
            }
        }

        private void dhE() {
            boolean z = false;
            int size = C0032AR.this.dgc.size();
            Iterator<C0033a> it = C0032AR.this.dgc.iterator();
            int i = size;
            while (it.hasNext()) {
                C0033a next = it.next();
                i--;
                if (next.ckg) {
                    z = true;
                    C0032AR.this.dfY.add(next.ckh);
                    next.ckh.cbt();
                    it.remove();
                } else if (!z) {
                    C0032AR.this.dfZ = next.ckh;
                    next.ckh.cbw();
                    if (i > 0) {
                        C0032AR.this.dgb++;
                        next.ckh.cbv();
                    }
                    it.remove();
                    return;
                } else {
                    for (C6972axl cbu : C0032AR.this.dfY) {
                        C0032AR.this.dgb++;
                        cbu.cbu();
                    }
                    return;
                }
            }
        }
    }
}
