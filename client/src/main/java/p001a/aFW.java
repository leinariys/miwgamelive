package p001a;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.aFW */
/* compiled from: a */
public @interface aFW {
    boolean cZD() default false;

    String value();
}
