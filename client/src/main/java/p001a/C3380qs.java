package p001a;

import game.engine.DataGameEvent;
import game.network.message.WriterBitsData;
import game.network.message.serializable.C2483fh;
import logic.baa.C1616Xf;

/* renamed from: a.qs */
/* compiled from: a */
public class C3380qs extends C2483fh {

    private final DataGameEvent aGA;
    private int aGC;
    private boolean aVN;
    private boolean aVO;

    public C3380qs(DataGameEvent jz, WriterBitsData oUVar, boolean z) {
        super(oUVar);
        this.aGC = 255;
        this.aVN = false;
        this.aVO = true;
        mo18779E(true);
        this.aGA = jz;
    }

    public C3380qs(DataGameEvent jz, WriterBitsData oUVar, boolean z, boolean z2) {
        this(jz, oUVar, z);
        this.aVO = z2;
    }

    public C3380qs(DataGameEvent jz, WriterBitsData oUVar, boolean z, int i, boolean z2) {
        this(jz, oUVar, z);
        this.aGC = i;
        this.aVN = z2;
    }

    /* renamed from: PM */
    public DataGameEvent mo21517PM() {
        return this.aGA;
    }

    /* renamed from: PN */
    public int mo21518PN() {
        return this.aGC;
    }

    /* renamed from: Xc */
    public boolean mo21519Xc() {
        return this.aVN;
    }

    /* renamed from: Xd */
    public boolean mo21520Xd() {
        return this.aVO;
    }

    /* renamed from: g */
    public void mo16273g(String str, Object obj) {
        if (!(obj instanceof C1616Xf)) {
            super.mo16273g(str, obj);
        } else if (!mo18783a(str, obj, false)) {
            mo7917i(str, obj);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: i */
    public void mo7917i(String str, Object obj) {
        mo18788h(str, 240);
        C1616Xf xf = (C1616Xf) obj;
        writeLong("oid", xf.bFf().getObjectId().getId());
        mo16273g("class", xf.getClass());
        mo7783aJ(135);
        mo18793t(obj);
    }
}
