package p001a;

/* renamed from: a.auD  reason: case insensitive filesystem */
/* compiled from: a */
public class C6811auD {
    private final boolean gEh;
    private final C5990aeO gEi;
    private final boolean stop;

    public C6811auD(C5990aeO aeo) {
        this(aeo, false, false);
    }

    public C6811auD(C5990aeO aeo, boolean z) {
        this(aeo, z, false);
    }

    public C6811auD(C5990aeO aeo, boolean z, boolean z2) {
        this.gEi = aeo;
        this.stop = z;
        this.gEh = z2;
    }

    public C5990aeO aDI() {
        return this.gEi;
    }

    public boolean cxL() {
        return this.gEh;
    }

    public boolean aDJ() {
        return this.stop;
    }
}
