package p001a;

import game.script.Character;
import game.script.avatar.*;
import game.script.avatar.body.AvatarAppearance;
import game.script.avatar.body.AvatarAppearanceType;
import game.script.avatar.body.BodyPiece;
import game.script.avatar.body.TextureScheme;
import game.script.avatar.cloth.Cloth;
import game.script.player.Player;
import game.script.resource.Asset;
import gnu.trove.THashMap;
import logic.C6961axa;
import logic.render.IEngineGraphics;
import logic.res.sound.C0907NJ;
import logic.thred.LogPrinter;
import logic.ui.C2698il;
import logic.ui.ComponentManager;
import logic.ui.item.InternalFrame;
import logic.ui.item.aUR;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.render.RenderView;
import taikodom.render.helpers.MeshUtils;
import taikodom.render.helpers.RenderTask;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.RMesh;
import taikodom.render.scene.RModel;
import taikodom.render.scene.custom.RCustomMesh;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.geom.Rectangle2D;
import java.util.List;
import java.util.*;

/* renamed from: a.oL */
/* compiled from: a */
class C3135oL implements C5207aAl, aVE {
    private static final String drO = "MOUTH";
    private static final String drP = "NOSE";
    private static final String drQ = "JAW";
    private static final String drR = "EYE";
    private static final String drS = "FHAIR";
    private static final String drT = "HAIR";
    private static final String drU = "CHEEK";
    private static final LogPrinter logger = LogPrinter.setClass(Avatar.class);
    /* renamed from: kj */
    public final IAddonProperties f8757kj;
    private final AvatarManager cKJ;
    /* renamed from: lV */
    private final IEngineGraphics f8758lV;
    /* access modifiers changed from: private */
    public AvatarAppearanceType drV;
    /* access modifiers changed from: private */
    public Map<String, JComboBox> drW;
    /* access modifiers changed from: private */
    public JList dsc;
    /* access modifiers changed from: private */
    public JList dsd;
    /* access modifiers changed from: private */
    public C1009Os dsi;
    /* access modifiers changed from: private */
    public List<Cloth> dsj;
    /* access modifiers changed from: private */
    public boolean dsl = false;
    /* access modifiers changed from: private */
    public int dsm = 0;
    /* access modifiers changed from: private */
    public int dsn = 0;
    /* access modifiers changed from: private */
    public int dso = 0;
    private JComboBox drX;
    private JComboBox drY;
    private JScrollBar drZ;
    private JScrollBar dsa;
    private JList dsb;
    private JList dse;
    private JList dsf;
    private JScrollBar dsg;
    private Set<BodyPiece> dsh;
    /* access modifiers changed from: private */
    private JComponent dsk;
    private ImageIcon dsp;

    public C3135oL(IAddonProperties vWVar, AvatarManager dq, InternalFrame nxVar) {
        this.f8757kj = vWVar;
        this.f8758lV = vWVar.ale();
        this.cKJ = dq;
        this.dsk = nxVar.mo4915cd("facial-hair-panel");
        m36625b(nxVar);
        m36629c(nxVar);
        m36624b((C2698il) nxVar);
        bdS();
        this.dsj = new ArrayList(1);
    }

    /* renamed from: e */
    public void mo20965e(AvatarAppearanceType t) {
        clear();
        this.drV = t;
        this.dsi = new C1009Os(this, this.f8758lV);
        m36634f(t);
        m36636g(t);
        this.dsc.getModel().clear();
        this.dsd.getModel().clear();
        if (t.mo5531bC()) {
            this.dsk.setVisible(false);
        } else {
            this.dsk.setVisible(true);
        }
    }

    /* renamed from: f */
    private void m36634f(AvatarAppearanceType t) {
        DefaultListModel model = this.dsb.getModel();
        model.clear();
        for (TextureScheme addElement : t.mo5559cr()) {
            model.addElement(addElement);
        }
        m36619a(this.dse.getModel(), mo7728n(bdV()).bpm());
        DefaultListModel model2 = this.dsf.getModel();
        model2.clear();
        for (ColorWrapper addElement2 : t.mo5563cz()) {
            model2.addElement(addElement2);
        }
    }

    /* renamed from: a */
    private void m36619a(DefaultListModel defaultListModel, List<TextureGrid> list) {
        defaultListModel.clear();
        for (TextureGrid addElement : list) {
            defaultListModel.addElement(addElement);
        }
    }

    /* renamed from: b */
    private void m36625b(InternalFrame nxVar) {
        this.drZ = nxVar.mo4915cd("shininessSlider");
        this.dsa = nxVar.mo4915cd("makeupWeightSlider");
        this.dsg = nxVar.mo4915cd("heightSlider");
    }

    /* renamed from: c */
    private void m36629c(InternalFrame nxVar) {
        C6961axa bHv = this.f8757kj.bHv();
        this.drW = new THashMap();
        this.drW.put(drU, new C1348Tg(bHv, nxVar.mo4916ce("selectCheeks")).dAb());
        this.drW.put(drT, new C1348Tg(bHv, nxVar.mo4916ce("selectHair")).dAb());
        this.drW.put(drS, new C1348Tg(bHv, nxVar.mo4916ce("selectFacialHair")).dAb());
        this.drW.put(drR, new C1348Tg(bHv, nxVar.mo4916ce("selectEyes")).dAb());
        this.drW.put(drQ, new C1348Tg(bHv, nxVar.mo4916ce("selectChin")).dAb());
        this.drW.put(drP, new C1348Tg(bHv, nxVar.mo4916ce("selectNose")).dAb());
        this.drW.put(drO, new C1348Tg(bHv, nxVar.mo4916ce("selectMouth")).dAb());
        this.drX = new C1348Tg(bHv, nxVar.mo4916ce("selectFeatures")).dAb();
        this.drY = new C1348Tg(bHv, nxVar.mo4916ce("selectDetails")).dAb();
    }

    /* renamed from: b */
    private void m36624b(C2698il ilVar) {
        C3142f fVar = new C3142f();
        this.dsb = ilVar.mo4915cd("skinColorRepeater");
        this.dsb.setModel(new DefaultListModel());
        this.dsb.addListSelectionListener(fVar);
        this.dsb.setCellRenderer(new C3141e());
        C3144h hVar = new C3144h();
        this.dsc = ilVar.mo4915cd("hairColorRepeater");
        this.dsc.setModel(new DefaultListModel());
        this.dsc.setCellRenderer(hVar);
        this.dsc.addListSelectionListener(fVar);
        this.dsc.addListSelectionListener(new C3143g());
        this.dsd = ilVar.mo4915cd("facialhairColorRepeater");
        this.dsd.setModel(new DefaultListModel());
        this.dsd.setCellRenderer(hVar);
        this.dsd.addListSelectionListener(fVar);
        this.dsd.addListSelectionListener(new C3146j());
        this.dse = ilVar.mo4915cd("eyesColorRepeater");
        this.dse.setModel(new DefaultListModel());
        this.dse.setCellRenderer(hVar);
        this.dse.addListSelectionListener(fVar);
        this.dsf = ilVar.mo4915cd("featuresColorRepeater");
        this.dsf.setModel(new DefaultListModel());
        this.dsf.setCellRenderer(new C3145i());
        this.dsf.addListSelectionListener(fVar);
    }

    /* renamed from: g */
    private void m36636g(AvatarAppearanceType t) {
        this.drW.get(drU).setModel(new aLY(t.mo5535bL()));
        this.drW.get(drT).setModel(new aLY(t.mo5553cf()));
        this.drW.get(drS).setModel(new aLY(t.mo5555cj()));
        this.drW.get(drR).setModel(new aLY(t.mo5551cb()));
        this.drW.get(drQ).setModel(new aLY(t.mo5537bP()));
        this.drW.get(drP).setModel(new aLY(t.mo5539bT()));
        this.drW.get(drO).setModel(new aLY(t.mo5541bX()));
        this.drX.setModel(new aLY(t.mo5561cv()));
        this.drY.setModel(new aLY(t.mo5546cD()));
    }

    private void bdS() {
        C3148l lVar = new C3148l();
        for (JComboBox addItemListener : this.drW.values()) {
            addItemListener.addItemListener(lVar);
        }
        C3147k kVar = new C3147k();
        this.drY.addItemListener(kVar);
        this.drX.addItemListener(kVar);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m36614a(BodyPiece qb, JList jList) {
        m36619a(jList.getModel(), qb.bpm());
        RCustomMesh a = this.dsi.blH().mo3097a(AvatarSector.C0955a.HAIR);
        if (a != null) {
            this.f8758lV.mo3007a((RenderTask) new C3140d(a));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: G */
    public void m36610G(BodyPiece qb) {
        if (qb != null && qb.bps() && !qb.bpo().isEmpty()) {
            this.dsi.blH().mo3098a(qb.bpk(), true);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: H */
    public void m36611H(BodyPiece qb) {
        if (qb != null && qb.bps()) {
            RCustomMesh a = this.dsi.blH().mo3097a(qb.bpk().biX());
            String bpo = qb.bpo();
            if (a != null && !bpo.isEmpty()) {
                this.dsi.blH().mo3098a(qb.bpk(), false);
            }
        }
    }

    public void clear() {
        this.drV = null;
        this.dsh = null;
        if (this.dsi != null) {
            this.dsi.mo4544c((C1009Os.C1019i) null);
        }
        this.dsi = null;
    }

    /* renamed from: aY */
    public void mo20962aY(Player aku) {
        if (aku != null && this.drV != null) {
            HashSet hashSet = new HashSet();
            for (JComboBox selectedItem : this.drW.values()) {
                Object selectedItem2 = selectedItem.getSelectedItem();
                if (selectedItem2 instanceof BodyPiece) {
                    hashSet.add((BodyPiece) selectedItem2);
                }
            }
            Player aku2 = aku;
            ((Player) C3582se.m38985a(aku2, (C6144ahM<?>) new C3136a(hashSet, bec(), bdW(), bed(), beg(), beh(), bef(), bdZ(), bdX(), bdY(), aOD(), new ArrayList(this.dsj), aku))).mo12660m(this.drV);
        }
    }

    public Collection<AvatarSector> aFI() {
        return this.cKJ.aFI();
    }

    /* renamed from: n */
    public BodyPiece mo7728n(AvatarSector nw) {
        String prefix = nw.getPrefix();
        if (this.drW.containsKey(prefix)) {
            return (BodyPiece) this.drW.get(prefix).getSelectedItem();
        }
        for (BodyPiece qb : this.drV.mo5557cn()) {
            if (qb.bpk().equals(nw)) {
                return qb;
            }
        }
        return null;
    }

    public Asset bdT() {
        return this.drV.mo5534bJ();
    }

    public Collection<BodyPiece> bdU() {
        if (this.dsh == null) {
            this.dsh = new HashSet();
            this.dsh.addAll(this.drV.mo5553cf());
            this.dsh.addAll(this.drV.mo5555cj());
            this.dsh.addAll(this.drV.mo5551cb());
            this.dsh.addAll(this.drV.mo5537bP());
            this.dsh.addAll(this.drV.mo5535bL());
            this.dsh.addAll(this.drV.mo5539bT());
            this.dsh.addAll(this.drV.mo5541bX());
            this.dsh.addAll(this.drV.mo5557cn());
        }
        return this.dsh;
    }

    public AvatarSector bdV() {
        return this.cKJ.aFM();
    }

    public Asset bdW() {
        return (Asset) this.drY.getSelectedItem();
    }

    public ColorWrapper bdX() {
        return (ColorWrapper) this.dsf.getSelectedValue();
    }

    public Rectangle2D.Float aGk() {
        return this.cKJ.aGk();
    }

    public float bdY() {
        return 1.0f;
    }

    public TextureGrid bdZ() {
        return (TextureGrid) this.drX.getSelectedItem();
    }

    public AvatarSector aFO() {
        return this.cKJ.aFO();
    }

    public AvatarSector aFQ() {
        return this.cKJ.aFQ();
    }

    public float aOD() {
        return ((float) ((this.dsg.getMinimum() + this.dsg.getMaximum()) - this.dsg.getValue())) / (this.drV.mo5548cH() * 100.0f);
    }

    public Asset bea() {
        return this.drV.mo5532bF();
    }

    public Asset beb() {
        return this.drV.mo5533bH();
    }

    public float bec() {
        return ((float) this.drZ.getValue()) / ((float) this.drZ.getMaximum());
    }

    public TextureScheme bed() {
        return (TextureScheme) this.dsb.getSelectedValue();
    }

    public boolean bee() {
        return this.drV.mo5531bC();
    }

    public String getPrefix() {
        return this.drV.getPrefix();
    }

    /* renamed from: r */
    public void mo2035r(Cloth wj) {
        if (!aOu().contains(wj)) {
            this.f8758lV.mo3007a((RenderTask) new C3139c(wj));
        }
    }

    public C5207aAl aOm() {
        return this;
    }

    public Collection<Cloth> aOu() {
        return Collections.unmodifiableCollection(this.dsj);
    }

    public Cloth aOw() {
        return null;
    }

    public RModel aOo() {
        if (this.dsi == null) {
            return null;
        }
        return this.dsi.aOo();
    }

    /* renamed from: cJ */
    public Asset mo7725cJ() {
        return this.drV.mo5549cJ();
    }

    public Cloth aOs() {
        return null;
    }

    /* renamed from: t */
    public void mo2036t(Cloth wj) {
        if (wj != null) {
            if (this.dsi != null) {
                this.dsi.mo4548y(wj);
            }
            this.dsj.remove(wj);
        }
    }

    public TextureGrid bef() {
        return (TextureGrid) this.dse.getSelectedValue();
    }

    public TextureGrid beg() {
        return (TextureGrid) this.dsc.getSelectedValue();
    }

    public TextureGrid beh() {
        return (TextureGrid) this.dsd.getSelectedValue();
    }

    public float getHeight() {
        return this.drV.mo5548cH() * aOD();
    }

    /* renamed from: ak */
    public void mo2026ak(Asset tCVar) {
        logger.trace("Set animation: Begin");
        if (this.dsi == null || tCVar == null) {
            logger.trace("Set animation: Fail");
            return;
        }
        this.dsi.mo4538ak(tCVar);
        logger.trace("Set animation: End");
    }

    /* renamed from: aC */
    public void mo20961aC(Asset tCVar) {
        logger.trace("Set animation: Begin");
        if (this.dsi == null || tCVar == null) {
            logger.trace("Set animation: Fail");
            return;
        }
        this.dsi.mo4536aC(tCVar);
        logger.trace("Set animation: End");
    }

    public String aOy() {
        return "Under construction";
    }

    /* renamed from: a */
    private void m36620a(JComboBox jComboBox) {
        if (jComboBox.getModel().getSize() > 0) {
            jComboBox.setSelectedIndex((int) Math.floor(((double) jComboBox.getItemCount()) * Math.random()));
        } else {
            jComboBox.setSelectedIndex(-1);
        }
    }

    /* renamed from: a */
    private void m36621a(JList jList) {
        if (jList.getModel().getSize() > 0) {
            jList.setSelectedIndex((int) Math.floor(((double) jList.getModel().getSize()) * Math.random()));
        } else {
            jList.setSelectedIndex(-1);
        }
    }

    public void randomize() {
        this.dsl = false;
        m36620a(this.drW.get(drO));
        m36620a(this.drW.get(drP));
        m36620a(this.drW.get(drQ));
        m36620a(this.drW.get(drR));
        m36620a(this.drW.get(drS));
        m36620a(this.drW.get(drT));
        m36620a(this.drW.get(drU));
        m36620a(this.drX);
        m36620a(this.drY);
        m36619a(this.dsc.getModel(), ((BodyPiece) this.drW.get(drT).getSelectedItem()).bpm());
        BodyPiece qb = (BodyPiece) this.drW.get(drS).getSelectedItem();
        if (qb != null) {
            m36619a(this.dsd.getModel(), qb.bpm());
        }
        m36621a(this.dsb);
        m36621a(this.dse);
        m36621a(this.dsc);
        m36621a(this.dsd);
        m36621a(this.dsf);
        this.dsg.setValue(this.dsg.getMinimum() + ((int) Math.floor(((double) (this.dsg.getMaximum() - this.dsg.getMinimum())) * Math.random())));
        this.dsl = true;
    }

    public C3138b bei() {
        C3138b bVar = new C3138b();
        bVar.iiU = Math.max(0, this.drW.get(drO).getSelectedIndex());
        bVar.iiV = Math.max(0, this.drW.get(drP).getSelectedIndex());
        bVar.iiW = Math.max(0, this.drW.get(drQ).getSelectedIndex());
        bVar.iiX = Math.max(0, this.drW.get(drR).getSelectedIndex());
        bVar.iiY = Math.max(0, this.dso);
        bVar.iiZ = Math.max(0, this.drW.get(drT).getSelectedIndex());
        bVar.ija = Math.max(0, this.drW.get(drU).getSelectedIndex());
        bVar.ijc = Math.max(0, this.drX.getSelectedIndex());
        bVar.ijb = Math.max(0, this.drY.getSelectedIndex());
        bVar.ijd = Math.max(0, this.dsb.getSelectedIndex());
        bVar.ije = Math.max(0, this.dse.getSelectedIndex());
        bVar.ijf = Math.max(0, this.dsm);
        bVar.ijg = Math.max(0, this.dsn);
        bVar.ijh = Math.max(0, this.dsf.getSelectedIndex());
        bVar.height = this.dsg.getValue();
        return bVar;
    }

    /* renamed from: a */
    private void m36612a(int i, JComboBox jComboBox) {
        if (jComboBox.getItemCount() <= 0) {
            jComboBox.setSelectedIndex(-1);
        } else if (i < jComboBox.getItemCount()) {
            jComboBox.setSelectedIndex(i);
        } else {
            jComboBox.setSelectedIndex(0);
        }
    }

    /* renamed from: a */
    private void m36613a(int i, JList jList) {
        if (jList.getModel().getSize() <= 0) {
            jList.setSelectedIndex(-1);
        } else if (i < jList.getModel().getSize()) {
            jList.setSelectedIndex(i);
        } else {
            jList.setSelectedIndex(0);
        }
    }

    /* renamed from: a */
    public void mo20960a(C3138b bVar) {
        m36612a(bVar.iiU, this.drW.get(drO));
        m36612a(bVar.iiV, this.drW.get(drP));
        m36612a(bVar.iiW, this.drW.get(drQ));
        m36612a(bVar.iiX, this.drW.get(drR));
        m36612a(bVar.iiY, this.drW.get(drS));
        m36612a(bVar.iiZ, this.drW.get(drT));
        m36612a(bVar.ija, this.drW.get(drU));
        m36612a(bVar.ijc, this.drX);
        m36612a(bVar.ijb, this.drY);
        m36619a(this.dsc.getModel(), ((BodyPiece) this.drW.get(drT).getSelectedItem()).bpm());
        BodyPiece qb = (BodyPiece) this.drW.get(drS).getSelectedItem();
        if (qb != null) {
            m36619a(this.dsd.getModel(), qb.bpm());
        }
        m36613a(bVar.ijd, this.dsb);
        m36613a(bVar.ije, this.dse);
        m36613a(bVar.ijf, this.dsc);
        m36613a(bVar.ijg, this.dsd);
        m36613a(bVar.ijh, this.dsf);
        this.dsg.setValue(bVar.height);
    }

    public ImageIcon aOk() {
        return this.dsp;
    }

    /* renamed from: d */
    public void mo2031d(ImageIcon imageIcon) {
        this.dsp = imageIcon;
    }

    /* renamed from: b */
    public void mo2027b(C0907NJ nj) {
        if (this.dsi != null) {
            this.dsi.mo4539b(nj);
        } else {
            nj.mo968a((RenderAsset) null);
        }
    }

    public Character agj() {
        return null;
    }

    /* renamed from: b */
    public void mo2028b(C1009Os.C1019i iVar) {
        if (this.dsi != null) {
            this.dsi.mo4544c(iVar);
        }
    }

    /* renamed from: yf */
    public void mo20967yf() {
        if (this.dsi != null) {
            this.dsi.blC();
        }
    }

    /* renamed from: qV */
    public void mo2034qV() {
    }

    /* renamed from: a.oL$f */
    /* compiled from: a */
    class C3142f implements ListSelectionListener {
        C3142f() {
        }

        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            if (!listSelectionEvent.getValueIsAdjusting()) {
                if (C3135oL.this.dsl) {
                    C3135oL.this.f8757kj.aVU().mo13975h(new C6811auD(C3667tV.MOUSE_RADIO_OVER_SFX));
                }
                if (C3135oL.this.dsi != null) {
                    C3135oL.this.dsi.blA();
                }
            }
        }
    }

    /* renamed from: a.oL$e */
    /* compiled from: a */
    class C3141e extends C5517aMj {
        C3141e() {
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            ComponentManager.getCssHolder(aur.dAx().mo4915cd("color")).mo13049Vr().setColorMultiplier(((TextureScheme) obj).byn());
            return aur.dAx();
        }
    }

    /* renamed from: a.oL$h */
    /* compiled from: a */
    class C3144h extends C5517aMj {
        C3144h() {
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            ComponentManager.getCssHolder(aur.dAx().mo4915cd("color")).mo13049Vr().setColorMultiplier(((TextureGrid) obj).byn());
            return aur.dAx();
        }
    }

    /* renamed from: a.oL$g */
    /* compiled from: a */
    class C3143g implements ListSelectionListener {
        C3143g() {
        }

        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            if (!listSelectionEvent.getValueIsAdjusting() && C3135oL.this.dsc.getSelectedIndex() >= 0) {
                C3135oL.this.dsm = C3135oL.this.dsc.getSelectedIndex();
            }
        }
    }

    /* renamed from: a.oL$j */
    /* compiled from: a */
    class C3146j implements ListSelectionListener {
        C3146j() {
        }

        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            if (!listSelectionEvent.getValueIsAdjusting() && C3135oL.this.dsd.getSelectedIndex() >= 0 && !C3135oL.this.bee()) {
                C3135oL.this.dsn = C3135oL.this.dsd.getSelectedIndex();
            }
        }
    }

    /* renamed from: a.oL$i */
    /* compiled from: a */
    class C3145i extends C5517aMj {
        C3145i() {
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            ComponentManager.getCssHolder(aur.dAx().mo4915cd("color")).mo13049Vr().setColorMultiplier(((ColorWrapper) obj).getColor());
            return aur.dAx();
        }
    }

    /* renamed from: a.oL$l */
    /* compiled from: a */
    class C3148l implements ItemListener {
        C3148l() {
        }

        public void itemStateChanged(ItemEvent itemEvent) {
            if (C3135oL.this.dsi != null) {
                BodyPiece qb = (BodyPiece) itemEvent.getItem();
                boolean a = C3135oL.this.dsl;
                C3135oL.this.dsl = false;
                switch (itemEvent.getStateChange()) {
                    case 1:
                        AvatarSector bpk = qb.bpk();
                        C3135oL.this.m36610G(qb);
                        if (!bpk.equals(C3135oL.this.aFO())) {
                            if (bpk.equals(C3135oL.this.aFQ())) {
                                if (!C3135oL.this.drV.mo5531bC()) {
                                    C3135oL.this.dso = ((JComboBox) C3135oL.this.drW.get(C3135oL.drS)).getSelectedIndex();
                                    if (C3135oL.this.dsd.getSelectedIndex() > 0) {
                                        C3135oL.this.dsn = C3135oL.this.dsd.getSelectedIndex();
                                    }
                                }
                                C3135oL.this.m36614a(qb, C3135oL.this.dsd);
                                if (!qb.bpm().isEmpty()) {
                                    if (C3135oL.this.dsn < qb.bpm().size()) {
                                        C3135oL.this.dsd.setSelectedIndex(C3135oL.this.dsn);
                                    } else {
                                        C3135oL.this.dsd.setSelectedIndex(0);
                                    }
                                }
                                if (C3135oL.this.dsi != null) {
                                    C3135oL.this.dsi.blA();
                                    break;
                                }
                            }
                        } else {
                            if (C3135oL.this.dsc.getSelectedIndex() > 0) {
                                C3135oL.this.dsm = C3135oL.this.dsc.getSelectedIndex();
                            }
                            C3135oL.this.m36614a(qb, C3135oL.this.dsc);
                            if (qb.bpm().isEmpty()) {
                                C3135oL.this.dsc.setSelectedIndex(-1);
                            } else if (C3135oL.this.dsm < qb.bpm().size()) {
                                C3135oL.this.dsc.setSelectedIndex(C3135oL.this.dsm);
                            } else {
                                C3135oL.this.dsc.setSelectedIndex(0);
                            }
                            if (C3135oL.this.dsi != null) {
                                C3135oL.this.dsi.blA();
                                break;
                            }
                        }
                        break;
                    case 2:
                        C3135oL.this.m36611H(qb);
                        break;
                }
                if (C3135oL.this.dsi != null) {
                    C3135oL.this.dsi.mo4549yf();
                }
                C3135oL.this.dsl = a;
            }
        }
    }

    /* renamed from: a.oL$k */
    /* compiled from: a */
    class C3147k implements ItemListener {
        C3147k() {
        }

        public void itemStateChanged(ItemEvent itemEvent) {
            switch (itemEvent.getStateChange()) {
                case 1:
                    if (C3135oL.this.dsi != null) {
                        C3135oL.this.dsi.blA();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    /* renamed from: a.oL$d */
    /* compiled from: a */
    class C3140d implements RenderTask {
        private final /* synthetic */ RMesh iMJ;

        C3140d(RMesh rMesh) {
            this.iMJ = rMesh;
        }

        public void run(RenderView renderView) {
            MeshUtils.sortTrianglesByDistance(this.iMJ.getMesh(), C3135oL.this.dsi.blH().mo3097a(AvatarSector.C0955a.HEAD).getAABB().mo9875zO().dfV());
        }
    }

    /* renamed from: a.oL$a */
    class C3136a implements C6144ahM<aVE> {
        private final /* synthetic */ Set aPZ;
        private final /* synthetic */ float aQa;
        private final /* synthetic */ Asset aQb;
        private final /* synthetic */ TextureScheme aQc;
        private final /* synthetic */ TextureGrid aQd;
        private final /* synthetic */ TextureGrid aQe;
        private final /* synthetic */ TextureGrid aQf;
        private final /* synthetic */ TextureGrid aQg;
        private final /* synthetic */ ColorWrapper aQh;
        private final /* synthetic */ float aQi;
        private final /* synthetic */ float aQj;
        private final /* synthetic */ List aQk;
        private final /* synthetic */ Player agw;

        C3136a(Set set, float f, Asset tCVar, TextureScheme ark, TextureGrid uk, TextureGrid uk2, TextureGrid uk3, TextureGrid uk4, ColorWrapper apf, float f2, float f3, List list, Player aku) {
            this.aPZ = set;
            this.aQa = f;
            this.aQb = tCVar;
            this.aQc = ark;
            this.aQd = uk;
            this.aQe = uk2;
            this.aQf = uk3;
            this.aQg = uk4;
            this.aQh = apf;
            this.aQi = f2;
            this.aQj = f3;
            this.aQk = list;
            this.agw = aku;
        }

        /* renamed from: a */
        public void mo1931n(aVE ave) {
            C3137a aVar = new C3137a(ave, this.aPZ, this.aQa, this.aQb, this.aQc, this.aQd, this.aQe, this.aQf, this.aQg, this.aQh, this.aQi, this.aQj, this.aQk, this.agw);
            aVar.setDaemon(true);
            aVar.start();
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            th.printStackTrace();
        }

        /* renamed from: a.oL$a$a */
        class C3137a extends Thread {
            private final /* synthetic */ aVE aPY;
            private final /* synthetic */ Set aPZ;
            private final /* synthetic */ float aQa;
            private final /* synthetic */ Asset aQb;
            private final /* synthetic */ TextureScheme aQc;
            private final /* synthetic */ TextureGrid aQd;
            private final /* synthetic */ TextureGrid aQe;
            private final /* synthetic */ TextureGrid aQf;
            private final /* synthetic */ TextureGrid aQg;
            private final /* synthetic */ ColorWrapper aQh;
            private final /* synthetic */ float aQi;
            private final /* synthetic */ float aQj;
            private final /* synthetic */ List aQk;
            private final /* synthetic */ Player agw;

            C3137a(aVE ave, Set set, float f, Asset tCVar, TextureScheme ark, TextureGrid uk, TextureGrid uk2, TextureGrid uk3, TextureGrid uk4, ColorWrapper apf, float f2, float f3, List list, Player aku) {
                this.aPY = ave;
                this.aPZ = set;
                this.aQa = f;
                this.aQb = tCVar;
                this.aQc = ark;
                this.aQd = uk;
                this.aQe = uk2;
                this.aQf = uk3;
                this.aQg = uk4;
                this.aQh = apf;
                this.aQi = f2;
                this.aQj = f3;
                this.aQk = list;
                this.agw = aku;
            }

            public void run() {
                AvatarAppearance aoz = (AvatarAppearance) this.aPY.aOm();
                for (BodyPiece qb : this.aPZ) {
                    aoz.mo15257b(qb.bpk(), qb);
                }
                aoz.mo15262kh(this.aQa);
                aoz.mo15258bx(this.aQb);
                aoz.mo15259g(this.aQc);
                aoz.mo15269r(this.aQd);
                aoz.mo15270t(this.aQe);
                aoz.mo15267p(this.aQf);
                aoz.mo15266n(this.aQg);
                aoz.mo15260j(this.aQh);
                aoz.mo15264kl(this.aQi);
                aoz.mo15263kj(this.aQj);
                for (Cloth r : this.aQk) {
                    this.aPY.mo2035r(r);
                }
                if (this.aPY instanceof Avatar) {
                    ((Avatar) this.aPY).aOA();
                    this.agw.dxy().aOy();
                }
            }
        }
    }

    /* renamed from: a.oL$c */
    /* compiled from: a */
    class C3139c implements RenderTask {
        private final /* synthetic */ Cloth ihX;

        C3139c(Cloth wj) {
            this.ihX = wj;
        }

        public void run(RenderView renderView) {
            if (C3135oL.this.dsi != null) {
                for (Cloth z : C3135oL.this.dsj) {
                    C3135oL.this.dsi.mo4550z(z);
                }
            }
            C3135oL.this.dsj.clear();
            if (this.ihX != null) {
                C3135oL.this.dsj.add(this.ihX);
                if (C3135oL.this.dsi != null) {
                    C3135oL.this.dsi.mo4534A(this.ihX);
                }
            }
        }
    }

    /* renamed from: a.oL$b */
    /* compiled from: a */
    public class C3138b {
        int height = 0;
        int iiU = 0;
        int iiV = 0;
        int iiW = 0;
        int iiX = 0;
        int iiY = 0;
        int iiZ = 0;
        int ija = 0;
        int ijb = 0;
        int ijc = 0;
        int ijd = 0;
        int ije = 0;
        int ijf = 0;
        int ijg = 0;
        int ijh = 0;

        public C3138b() {
        }
    }
}
