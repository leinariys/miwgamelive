package p001a;

import game.script.space.AsteroidType;

/* renamed from: a.alJ  reason: case insensitive filesystem */
/* compiled from: a */
public class C6349alJ {
    private AsteroidType dhL;
    private int fXG;

    public AsteroidType cii() {
        return this.dhL;
    }

    /* renamed from: l */
    public void mo14648l(AsteroidType aqn) {
        this.dhL = aqn;
    }

    public int cij() {
        return this.fXG;
    }

    /* renamed from: su */
    public void mo14649su(int i) {
        this.fXG = i;
    }

    public String toString() {
        if (this.dhL == null) {
            return "null";
        }
        return String.valueOf(this.dhL.getHandle()) + " sizeFactor=" + this.fXG;
    }
}
