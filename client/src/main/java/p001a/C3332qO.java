package p001a;

import logic.IAddonSettings;
import logic.swing.C0454GJ;
import logic.ui.item.InternalFrame;
import taikodom.addon.IAddonProperties;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/* renamed from: a.qO */
/* compiled from: a */
public class C3332qO {
    private static final int aWO = 35;
    private static final double aWP = 0.95d;
    private C3333a aWQ;
    /* access modifiers changed from: private */
    public Timer aWR = new Timer(35, this.aWQ);

    public C3332qO(JTextComponent jTextComponent, C0454GJ gj, InternalFrame nxVar, IAddonProperties vWVar) {
        this.aWQ = new C3333a(jTextComponent, gj, vWVar);
        this.aWR.start();
    }

    public void clear() {
        this.aWR.stop();
        this.aWQ.clear();
    }

    public boolean isRunning() {
        return this.aWR.isRunning();
    }

    /* renamed from: a */
    public void mo21333a(JTextComponent jTextComponent, C0454GJ gj) {
        this.aWQ.mo21338a(jTextComponent, gj);
        this.aWR.restart();
    }

    public void start() {
        this.aWQ.rewind();
        this.aWR.restart();
    }

    public void stop() {
        this.aWR.stop();
        this.aWQ.cancel();
    }

    /* renamed from: a.qO$a */
    private class C3333a implements ActionListener {
        private C0454GJ atu;
        private JTextComponent gFF;
        private String gFG;
        private long gFH = 0;

        /* renamed from: kj */
        private IAddonProperties f8905kj;

        public C3333a(JTextComponent jTextComponent, C0454GJ gj, IAddonProperties vWVar) {
            this.gFF = jTextComponent;
            this.atu = gj;
            this.f8905kj = vWVar;
            rewind();
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (!C5905ach.m20567b((IAddonSettings) this.f8905kj).isActive()) {
                if (this.gFG.length() > 0) {
                    this.gFF.setText(String.valueOf(this.gFF.getText()) + this.gFG.charAt(0));
                    this.gFG = this.gFG.substring(1);
                    if (Math.random() <= C3332qO.aWP && this.gFH == 0) {
                        this.gFH = System.currentTimeMillis();
                        this.f8905kj.aVU().mo13975h(new C0208CZ(C3667tV.MESS_SYSTEM_MESSAGE_TYPE));
                    }
                    if (this.gFH != 0 && System.currentTimeMillis() > this.gFH + 35) {
                        this.gFH = 0;
                        return;
                    }
                    return;
                }
                C3332qO.this.aWR.stop();
                this.gFG = "";
                if (this.atu != null) {
                    this.atu.mo9a(this.gFF);
                }
            }
        }

        public void clear() {
            this.gFF = null;
            this.atu = null;
            this.gFG = null;
        }

        public void cancel() {
            if (this.gFF != null && this.gFG != null && this.gFG.length() > 0) {
                SwingUtilities.invokeLater(new C3334a(this.gFF, this.gFG));
            }
        }

        public C0454GJ cyB() {
            return this.atu;
        }

        public JTextComponent getComponent() {
            return this.gFF;
        }

        /* renamed from: a */
        public void mo21338a(JTextComponent jTextComponent, C0454GJ gj) {
            this.gFF = jTextComponent;
            this.atu = gj;
            rewind();
        }

        public void rewind() {
            if (this.gFF != null) {
                this.gFG = this.gFF.getText();
                this.gFF.setText("");
                this.gFF.setVisible(true);
            }
        }

        /* renamed from: a.qO$a$a */
        class C3334a implements Runnable {
            private final /* synthetic */ JTextComponent fsx;
            private final /* synthetic */ String fsy;

            C3334a(JTextComponent jTextComponent, String str) {
                this.fsx = jTextComponent;
                this.fsy = str;
            }

            public void run() {
                this.fsx.setText(String.valueOf(this.fsx.getText()) + this.fsy);
            }
        }
    }
}
