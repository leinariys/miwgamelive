package p001a;

/* renamed from: a.aVa  reason: case insensitive filesystem */
/* compiled from: a */
public class C5742aVa {
    private long iYd;

    public C5742aVa() {
        reset();
    }

    public void reset() {
        this.iYd = System.currentTimeMillis();
    }

    public long dAK() {
        return System.currentTimeMillis() - this.iYd;
    }

    public long dAL() {
        return dAK() * 1000;
    }
}
