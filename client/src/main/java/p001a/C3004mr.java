package p001a;

import game.script.Actor;
import game.script.Character;
import game.script.corporation.Corporation;
import game.script.npc.NPCType;
import game.script.player.User;
import game.script.space.Node;
import game.script.space.StellarSystem;

import java.util.Iterator;
import java.util.StringTokenizer;

/* renamed from: a.mr */
/* compiled from: a */
public class C3004mr {
    public static final int aCh = 2;
    public static final int aCi = 22;
    public static final int aCj = 2;
    private static final String aCk = "áàãâä";
    private static final String aCl = "éèêë";
    private static final String aCm = "íìîï";
    private static final String aCn = "óòõôö";
    private static final String aCo = "íìûü";
    private static final String aCp = "ýÿ";
    private static final String aCq = "ñ";

    /* renamed from: a */
    public static void m35861a(String str, Taikodom dn) {
        m35860a((User) null, str, dn);
    }

    /* renamed from: a */
    public static void m35860a(User adk, String str, Taikodom dn) {
        if (str == null) {
            throw new C0849MO(C0849MO.C0850a.NAME_IS_NULL, new Object[0]);
        } else if (str.length() < 2) {
            throw new C0849MO(C0849MO.C0850a.TOO_SHORT, str);
        } else if (str.length() > 22) {
            throw new C0849MO(C0849MO.C0850a.TOO_LONG, str);
        } else {
            int i = 0;
            int i2 = 2;
            while (i < str.length()) {
                char charAt = str.charAt(i);
                if (!m35866cW(charAt)) {
                    throw new C0849MO(C0849MO.C0850a.INVALID_CHARACTER, str, Character.valueOf(charAt));
                } else if (charAt == ' ' && i2 - 1 == 0) {
                    throw new C0849MO(C0849MO.C0850a.TOO_MANY_SPACES, new Object[0]);
                } else {
                    i++;
                }
            }
            if (m35864b(str, dn)) {
                throw new C0849MO(C0849MO.C0850a.FORBIDDEN, str);
            } else if (m35862a(str, dn, adk)) {
                throw new C0849MO(C0849MO.C0850a.RESERVED, str);
            } else if (m35867d(str, dn)) {
                throw new C0849MO(C0849MO.C0850a.BELONGS_TO_USER, str);
            } else if (m35865c(str, dn)) {
                throw new C0849MO(C0849MO.C0850a.BELONGS_TO_NPC, str);
            } else if (m35868e(str, dn)) {
                throw new C0849MO(C0849MO.C0850a.BELONGS_TO_CORP, str);
            } else if (m35869f(str, dn)) {
                throw new C0849MO(C0849MO.C0850a.BELONGS_TO_THING, str);
            }
        }
    }

    /* renamed from: a */
    private static boolean m35862a(String str, Taikodom dn, User adk) {
        return !dn.aMy().mo10048b(str, adk);
    }

    /* renamed from: cW */
    public static boolean m35866cW(int i) {
        return (i >= 97 && i <= 122) || (i >= 65 && i <= 90) || i == 231 || i == 199 || i == 32;
    }

    /* renamed from: aL */
    private static String m35863aL(String str) {
        if (str == null) {
            return "";
        }
        StringBuilder append = new StringBuilder().append(str.toLowerCase().replace('_', ' '));
        for (int length = append.length() - 1; length >= 0; length--) {
            char charAt = append.charAt(length);
            if (charAt == ' ' || charAt == '@') {
                append.deleteCharAt(length);
            } else if (aCk.indexOf(charAt) >= 0) {
                append.setCharAt(length, 'a');
            } else if (aCl.indexOf(charAt) >= 0) {
                append.setCharAt(length, 'e');
            } else if (aCm.indexOf(charAt) >= 0) {
                append.setCharAt(length, 'i');
            } else if (aCn.indexOf(charAt) >= 0) {
                append.setCharAt(length, 'o');
            } else if (aCo.indexOf(charAt) >= 0) {
                append.setCharAt(length, 'u');
            } else if (aCp.indexOf(charAt) >= 0) {
                append.setCharAt(length, 'y');
            } else if (aCq.indexOf(charAt) >= 0) {
                append.setCharAt(length, 'n');
            }
        }
        return append.toString();
    }

    /* renamed from: b */
    private static boolean m35864b(String str, Taikodom dn) {
        String aL = m35863aL(str);
        if (aL == null) {
            return true;
        }
        StringTokenizer stringTokenizer = new StringTokenizer(dn.aLq(), ",");
        while (stringTokenizer.hasMoreTokens()) {
            if (m35863aL(stringTokenizer.nextToken()).indexOf(aL) >= 0) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: c */
    private static boolean m35865c(String str, Taikodom dn) {
        String aL = m35863aL(str);
        if (aL == null) {
            return true;
        }
        for (NPCType next : dn.aJY()) {
            if (next.mo11470ke() == null) {
                System.out.println("Warning npc with null name " + next.bFY());
            } else {
                for (String aL2 : next.mo11470ke().getMap().values()) {
                    if (aL.equals(m35863aL(aL2))) {
                        return true;
                    }
                }
                continue;
            }
        }
        return false;
    }

    /* renamed from: d */
    private static boolean m35867d(String str, Taikodom dn) {
        String aL = m35863aL(str);
        if (aL == null) {
            return true;
        }
        for (User cSQ : dn.aJQ()) {
            Iterator it = cSQ.cSQ().iterator();
            while (true) {
                if (it.hasNext()) {
                    if (aL.equals(m35863aL(((C6308akU) it.next()).getName()))) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /* renamed from: e */
    private static boolean m35868e(String str, Taikodom dn) {
        String aL = m35863aL(str);
        if (aL == null) {
            return true;
        }
        for (Corporation name : dn.aLK()) {
            if (aL.equals(m35863aL(name.getName()))) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: f */
    private static boolean m35869f(String str, Taikodom dn) {
        String aL = m35863aL(str);
        if (aL == null) {
            return true;
        }
        for (StellarSystem nodes : dn.aKa()) {
            Iterator<Node> it = nodes.getNodes().iterator();
            while (true) {
                if (it.hasNext()) {
                    Iterator<Actor> it2 = it.next().mo21624Zw().iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            if (aL.equals(m35863aL(it2.next().getName()))) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
}
