package p001a;

import game.geometry.Vec3d;
import taikodom.render.scene.SceneObject;

/* renamed from: a.afg  reason: case insensitive filesystem */
/* compiled from: a */
public class C6060afg {
    private Vec3d ftj;
    private int ftk = 0;
    private boolean ftl = false;
    private float objectSize;
    private SceneObject target;

    public C6060afg(SceneObject sceneObject, Vec3d ajr, int i, boolean z, float f) {
        this.target = sceneObject;
        this.ftj = ajr;
        this.ftk = i;
        this.objectSize = f;
        this.ftl = z;
    }

    public float bVr() {
        return this.objectSize;
    }

    public SceneObject getTarget() {
        return this.target;
    }

    public Vec3d bVs() {
        return this.ftj;
    }

    public int bVt() {
        return this.ftk;
    }

    public boolean bQB() {
        return this.ftl;
    }
}
