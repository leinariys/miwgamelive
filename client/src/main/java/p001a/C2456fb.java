package p001a;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.fb */
/* compiled from: a */
public abstract class C2456fb extends AbstractListModel {
    private ListModel aRE;
    private List<Object> aRF;
    private boolean aRG;

    public C2456fb() {
        this(new DefaultListModel());
    }

    public C2456fb(ListModel listModel) {
        this.aRE = listModel;
        this.aRF = new ArrayList();
        this.aRG = false;
        listModel.addListDataListener(new C2457a());
    }

    /* access modifiers changed from: protected */
    /* renamed from: D */
    public abstract boolean mo18734D(Object obj);

    public Object getElementAt(int i) {
        if (!this.aRG) {
            validate();
        }
        return this.aRF.get(i);
    }

    public int getSize() {
        if (!this.aRG) {
            validate();
        }
        return this.aRF.size();
    }

    public void invalidate() {
        this.aRG = false;
    }

    public void validate() {
        this.aRF.clear();
        int size = this.aRE.getSize();
        for (int i = 0; i < size; i++) {
            Object elementAt = this.aRE.getElementAt(i);
            if (mo18734D(elementAt)) {
                this.aRF.add(elementAt);
            }
        }
        this.aRG = true;
        mo18735Va();
    }

    /* access modifiers changed from: protected */
    /* renamed from: Va */
    public void mo18735Va() {
        for (ListDataListener contentsChanged : getListeners(ListDataListener.class)) {
            contentsChanged.contentsChanged(new ListDataEvent(this, 0, 0, getSize() - 1));
        }
    }

    /* renamed from: a.fb$a */
    class C2457a implements ListDataListener {
        C2457a() {
        }

        public void contentsChanged(ListDataEvent listDataEvent) {
            C2456fb.this.invalidate();
        }

        public void intervalAdded(ListDataEvent listDataEvent) {
            C2456fb.this.invalidate();
        }

        public void intervalRemoved(ListDataEvent listDataEvent) {
            C2456fb.this.invalidate();
        }
    }
}
