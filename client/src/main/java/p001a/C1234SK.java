package p001a;

import org.objectweb.asm.MethodVisitor;

/* renamed from: a.SK */
/* compiled from: a */
public class C1234SK {
    private C1070Pe auR;
    private C1070Pe[] auT;
    private String descriptor;

    public C1234SK(C1070Pe pe, C1070Pe... peArr) {
        this.auR = pe;
        this.auT = peArr;
    }

    public String getDescriptor() {
        if (this.descriptor != null) {
            return this.descriptor;
        }
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        if (this.auT != null) {
            for (C1070Pe descriptor2 : this.auT) {
                sb.append(descriptor2.getDescriptor());
            }
        }
        sb.append(')').append('V');
        String sb2 = sb.toString();
        this.descriptor = sb2;
        return sb2;
    }

    /* renamed from: KM */
    public C1070Pe mo5377KM() {
        return this.auR;
    }

    /* renamed from: KO */
    public C1070Pe[] mo5378KO() {
        return this.auT;
    }

    /* renamed from: a */
    public void mo5379a(MethodVisitor methodVisitor) {
        methodVisitor.visitMethodInsn(183, this.auR.getInternalName(), C2821kb.arR, getDescriptor());
    }
}
