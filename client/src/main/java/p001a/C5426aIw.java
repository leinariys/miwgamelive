package p001a;

import game.script.Actor;
import game.script.citizenship.CitizenshipControl;
import game.script.corporation.Corporation;
import game.script.faction.Faction;
import game.script.item.ItemType;
import game.script.mission.Mission;
import game.script.npc.NPC;
import game.script.progression.ProgressionCareer;
import game.script.ship.Ship;
import game.script.ship.Station;

/* renamed from: a.aIw  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5426aIw {
    /* renamed from: B */
    void mo9402B(Faction xm);

    /* renamed from: Rn */
    ProgressionCareer mo9403Rn();

    Faction azN();

    /* renamed from: b */
    boolean mo9405b(ItemType jCVar, int i);

    /* renamed from: b */
    boolean mo9406b(ItemType jCVar, int i, Mission af);

    Ship bQx();

    Corporation bYd();

    boolean cWR();

    boolean cWT();

    Actor cWW();

    void cWY();

    Station cXi();

    CitizenshipControl cXk();

    aMU cXm();

    /* renamed from: eb */
    boolean mo9416eb(long j);

    /* renamed from: f */
    int mo9417f(ItemType jCVar, int i);

    String getName();

    String getParameter(String str);

    /* renamed from: h */
    boolean mo9420h(ItemType jCVar, int i);

    /* renamed from: kx */
    boolean mo9421kx(long j);

    /* renamed from: kz */
    boolean mo9422kz(long j);

    /* renamed from: lt */
    int mo9423lt();

    /* renamed from: mg */
    boolean mo9424mg(String str);

    /* renamed from: mi */
    boolean mo9425mi(String str);

    /* renamed from: mk */
    boolean mo9426mk(String str);

    /* renamed from: mm */
    boolean mo9427mm(String str);

    /* renamed from: mo */
    void mo9428mo(String str);

    void setParameter(String str, String str2);

    /* renamed from: x */
    void mo9430x(NPC auf);
}
