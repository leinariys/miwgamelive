package p001a;

/* renamed from: a.aUJ */
/* compiled from: a */
public class aUJ {
    public C0783LL iXA;
    public C3808vG iXB;
    public Object iXC;
    public C0783LL iXz;

    /* renamed from: g */
    public void mo11609g(C0783LL ll, C0783LL ll2) {
        this.iXz = ll;
        this.iXA = ll2;
        this.iXB = null;
        this.iXC = null;
    }

    /* renamed from: c */
    public void mo11607c(aUJ auj) {
        this.iXz = auj.iXz;
        this.iXA = auj.iXA;
        this.iXB = auj.iXB;
        this.iXC = auj.iXC;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof aUJ)) {
            return false;
        }
        aUJ auj = (aUJ) obj;
        if ((this.iXz == auj.iXz && this.iXA == auj.iXA) || (this.iXz == auj.iXA && this.iXA == auj.iXz)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.iXz.hashCode() ^ this.iXA.hashCode();
    }
}
