package p001a;

/* renamed from: a.DH */
/* compiled from: a */
public class C0254DH {
    private float cIA;
    private float cIB;
    private float cIu;
    private float cIv;
    private float cIw;
    private float cIx;
    private float cIy;
    private float cIz;
    private long start;

    public float getFactor() {
        return this.cIu;
    }

    public float aHd() {
        if (this.cIu < 0.0f) {
            return this.cIy;
        }
        long currentTimeMillis = SingletonCurrentTimeMilliImpl.currentTimeMillis() - this.start;
        if (this.cIu < 0.0f || ((double) currentTimeMillis) > 500.0d) {
            this.cIu = 0.0f;
        } else {
            this.cIu = (float) (1.0d - (((double) currentTimeMillis) / 500.0d));
        }
        float f = this.cIu * this.cIu;
        float f2 = ((1.0f - f) * this.cIy) + (this.cIw * f);
        if (this.cIB > 0.0f) {
            float min = Math.min(this.cIB, this.cIB / (((float) currentTimeMillis) / 1000.0f));
            if (f2 > this.cIv && f2 > this.cIv + min) {
                f2 = this.cIv + min;
            } else if (f2 < this.cIv && f2 < this.cIv - min) {
                f2 = this.cIv - min;
            }
        }
        this.cIv = f2;
        return this.cIv;
    }

    /* renamed from: eU */
    public void mo1268eU(float f) {
        this.cIv = f;
        this.cIw = f;
        this.cIy = f;
        this.cIu = -1.0f;
    }

    /* renamed from: eV */
    public void mo1269eV(float f) {
        this.cIw = aHd();
        this.cIy = f;
        this.start = SingletonCurrentTimeMilliImpl.currentTimeMillis();
        this.cIu = 1.0f;
    }

    /* renamed from: eW */
    public void mo1270eW(float f) {
        this.cIB = Math.abs(f);
    }
}
