package p001a;

import game.network.message.externalizable.C6302akO;

import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.ub */
/* compiled from: a */
public class C3754ub extends C6302akO {

    private List<Long> bva;

    public C3754ub() {
    }

    public C3754ub(List<Long> list) {
        this.bva = list;
    }

    public void readExternal(ObjectInput objectInput) {
        int readInt = objectInput.readInt();
        this.bva = new ArrayList(readInt);
        for (int i = 0; i < readInt; i++) {
            this.bva.add(Long.valueOf(objectInput.readLong()));
        }
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeInt(this.bva.size());
        for (Long longValue : this.bva) {
            objectOutput.writeLong(longValue.longValue());
        }
    }

    public List<Long> ajb() {
        return this.bva;
    }
}
