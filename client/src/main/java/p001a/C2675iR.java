package p001a;

import game.geometry.Vec3d;
import game.script.item.Weapon;
import logic.thred.PoolThread;

import javax.vecmath.Tuple3d;

/* renamed from: a.iR */
/* compiled from: a */
public class C2675iR extends ayA {
    private int fyi = 0;
    private long fyj;
    private C2676a fyk;
    private Vec3d fyl;
    private long fym;

    public C2675iR(C1285Sv sv) {
        super(sv);
    }

    public void restart() {
        super.restart();
        this.fyi = 0;
        mo5495al().mo9105k((Weapon) btF());
        mo5503dL().dxc().mo22161x(btF());
        mo5495al().mo964W(200.0f);
        mo5495al().mo1063f(10.0f, 60.0f, 20.0f);
        mo5495al().mo964W(200.0f);
        mo5495al().mo1063f(10.0f, 60.0f, 20.0f);
        this.fyj = System.currentTimeMillis();
        bWL();
    }

    private C1096QA[] bWK() {
        if (this.fyk == C2676a.HiSpeedStates) {
            return C1096QA.bqb();
        }
        return C1096QA.bqa();
    }

    private void bWL() {
        this.fyl = mo5495al().getPosition().mo9517f((Tuple3d) bWK()[0].position);
    }

    public void aGu() {
        bWM();
    }

    /* renamed from: a */
    public void mo19541a(C2676a aVar) {
        this.fyk = aVar;
        bWL();
    }

    private void bWM() {
        C1096QA[] bWK = bWK();
        C1096QA qa = bWK[this.fyi];
        if (m33301a(bWK, this.fyi) <= bWN()) {
            qa.mo4910a(mo5503dL().dxc(), this.fyl);
            this.fyi = (this.fyi + 1) % bWK.length;
            if (System.currentTimeMillis() - this.fyj > bWO()) {
                PoolThread.sleep(400);
                btJ().mo16106ac(mo5495al());
                PoolThread.sleep(400);
                finish();
            } else if (this.fyi == 0) {
                bWL();
            }
        }
    }

    /* renamed from: a */
    private long m33301a(C1096QA[] qaArr, int i) {
        if (i == 0) {
            return 0;
        }
        return qaArr[i].time - qaArr[0].time;
    }

    private long bWN() {
        return System.currentTimeMillis() - this.fyj;
    }

    public long bWO() {
        return this.fym;
    }

    /* renamed from: hg */
    public void mo19543hg(long j) {
        this.fym = j;
    }

    /* renamed from: a.iR$a */
    public enum C2676a {
        HiSpeedStates,
        FastMovementsStates
    }
}
