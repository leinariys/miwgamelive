package p001a;

import game.geometry.Vec3d;
import game.network.message.externalizable.C6950awp;
import org.mozilla1.classfile.C6192aiI;

import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.afJ  reason: case insensitive filesystem */
/* compiled from: a */
public class C6037afJ implements C6950awp {
    private List<Vec3d> fvr;

    public C6037afJ() {
    }

    public C6037afJ(List<C3055nX.C3056a<C0819Lq, C0461GN>> list) {
        this.fvr = new ArrayList();
        for (C3055nX.C3056a<C0819Lq, C0461GN> position : list) {
            this.fvr.add(position.getPosition());
        }
    }

    /* renamed from: a */
    public void mo12575a(C1355Tk tk) {
        C6192aiI aii = (C6192aiI) tk;
        long currentTimeMillis = tk.bvh().currentTimeMillis();
        tk.mo5730bZ(currentTimeMillis);
        for (Vec3d p : this.fvr) {
            C3055nX.C3056a<C0819Lq, C0461GN> p2 = aii.bvi().mo8644p(p);
            if (p2 != null) {
                p2.mo11194bZ(currentTimeMillis);
            }
        }
    }

    public void readExternal(ObjectInput objectInput) {
        this.fvr = new ArrayList();
        while (objectInput.available() > 0) {
            Vec3d ajr = new Vec3d();
            ajr.x = objectInput.readDouble();
            ajr.y = objectInput.readDouble();
            ajr.z = objectInput.readDouble();
            this.fvr.add(ajr);
        }
    }

    public void writeExternal(ObjectOutput objectOutput) {
        for (Vec3d next : this.fvr) {
            objectOutput.writeDouble(next.x);
            objectOutput.writeDouble(next.y);
            objectOutput.writeDouble(next.z);
        }
    }
}
