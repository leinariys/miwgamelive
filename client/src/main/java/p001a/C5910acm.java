package p001a;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;

/* renamed from: a.acm  reason: case insensitive filesystem */
/* compiled from: a */
public class C5910acm {
    private static Instrumentation fbd;
    private static ClassFileTransformer fbe;

    /* renamed from: a */
    public static void m20581a(String str, Instrumentation instrumentation) {
        C6380alo.C1937a aVar;
        fbd = instrumentation;
        System.out.println("Infra agent loaded. Using asm.");
        String property = System.getProperty("instrumentation.target");
        if (property != null) {
            aVar = C6380alo.C1937a.valueOf(property.toUpperCase());
        } else {
            aVar = C6380alo.C1937a.GENERIC;
        }
        System.out.println("Instrumentation target: " + aVar);
        C1024Ow ow = new C1024Ow();
        ow.mo4566a(aVar);
        fbe = ow;
        instrumentation.addTransformer(fbe);
    }

    public static Instrumentation bOB() {
        return fbd;
    }
}
