package p001a;

import com.hoplon.geometry.Color;
import taikodom.render.helpers.SceneHelper;
import taikodom.render.scene.RMesh;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.parameters.ShaderParameter;
import taikodom.render.textures.Material;
import taikodom.render.textures.Texture;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.uP */
/* compiled from: a */
public class C3738uP {
    private List<RMesh> aWi;
    private List<RMesh> aWj = new ArrayList();
    private SceneObject bxn;
    private Scene bxo;
    private SceneObject bxp;

    public C3738uP(SceneObject sceneObject, Scene scene) {
        this.bxo = scene;
        this.bxn = sceneObject;
        this.aWi = SceneHelper.getChildInstances(sceneObject, RMesh.class);
        m40008Xz();
    }

    public C3738uP(SceneObject sceneObject, SceneObject sceneObject2) {
        this.bxp = sceneObject2;
        this.bxn = sceneObject;
        this.aWi = SceneHelper.getChildInstances(sceneObject, RMesh.class);
        m40008Xz();
    }

    /* renamed from: Xz */
    private void m40008Xz() {
        for (RMesh cloneAsset : this.aWi) {
            RMesh rMesh = (RMesh) cloneAsset.cloneAsset();
            rMesh.invalidateHandle();
            this.aWj.add(rMesh);
            if (this.bxo != null) {
                this.bxo.addChild(rMesh);
            }
            if (this.bxp != null) {
                this.bxp.addChild(rMesh);
            }
        }
    }

    /* renamed from: c */
    public void mo22380c(Color color) {
        for (RMesh primitiveColor : this.aWj) {
            primitiveColor.setPrimitiveColor(color);
        }
    }

    /* renamed from: a */
    public void mo22379a(Texture texture, int i) {
        for (RMesh material : this.aWj) {
            material.getMaterial().setTexture(i, texture);
        }
    }

    /* renamed from: a */
    public void mo22378a(Material material) {
        for (RMesh material2 : this.aWj) {
            material2.setMaterial(material);
        }
    }

    /* renamed from: a */
    public void mo22376a(Shader shader) {
        for (RMesh next : this.aWj) {
            if (next.getMaterial() != null) {
                next.getMaterial().setShader(shader);
            } else {
                next.setShader(shader);
            }
        }
    }

    /* renamed from: a */
    public void mo22377a(ShaderParameter shaderParameter) {
        for (RMesh next : this.aWj) {
            next.getMaterial().addShaderParameter(shaderParameter);
            if (next.getMaterial().getShader() != null) {
                next.getMaterial().setShader(next.getMaterial().getShader());
            }
        }
    }

    /* renamed from: XA */
    public void mo22374XA() {
        this.aWj.clear();
    }

    /* renamed from: XB */
    public void mo22375XB() {
        for (RMesh next : this.aWj) {
            if (this.bxo != null) {
                this.bxo.removeChild(next);
            }
            if (this.bxp != null) {
                this.bxp.removeChild(next);
            }
        }
        mo22374XA();
    }
}
