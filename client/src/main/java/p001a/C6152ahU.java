package p001a;

import logic.res.scene.C3087nb;

/* renamed from: a.ahU  reason: case insensitive filesystem */
/* compiled from: a */
public class C6152ahU {
    private final Class<? extends C3087nb> fLM;
    private final String fLN;
    private final String handle;

    public C6152ahU(String str, Class<? extends C3087nb> cls, String str2) {
        this.fLM = cls;
        this.handle = str;
        this.fLN = str2;
    }

    public Class<? extends C3087nb> caU() {
        return this.fLM;
    }

    public String getHandle() {
        return this.handle;
    }

    public String caV() {
        return this.fLN;
    }
}
