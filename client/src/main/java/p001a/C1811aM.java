package p001a;

import gnu.trove.THashMap;
import org.objectweb.asm.AnnotationVisitor;

import java.util.Map;

/* renamed from: a.aM */
/* compiled from: a */
public class C1811aM implements AnnotationVisitor {
    private final String desc;

    /* renamed from: lI */
    THashMap<String, Object> f3338lI;

    public C1811aM(String str) {
        this.desc = str;
    }

    /* renamed from: fs */
    public String mo9961fs() {
        return this.desc;
    }

    public void visit(String str, Object obj) {
        mo9962ft().put(str, obj);
    }

    /* renamed from: ft */
    public Map<String, Object> mo9962ft() {
        if (this.f3338lI == null) {
            this.f3338lI = new THashMap<>();
        }
        return this.f3338lI;
    }

    public AnnotationVisitor visitAnnotation(String str, String str2) {
        C1811aM aMVar = new C1811aM(str2);
        mo9962ft().put(str, aMVar);
        return aMVar;
    }

    public AnnotationVisitor visitArray(String str) {
        return new C1811aM(str);
    }

    public void visitEnd() {
    }

    public void visitEnum(String str, String str2, String str3) {
    }

    public String toString() {
        return String.valueOf(this.desc) + mo9962ft();
    }
}
