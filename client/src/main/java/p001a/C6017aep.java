package p001a;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.script.Actor;
import logic.render.IEngineGraphics;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import taikodom.render.gui.GHudMark;
import taikodom.render.gui.GuiSceneObject;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.SceneObject;

/* renamed from: a.aep  reason: case insensitive filesystem */
/* compiled from: a */
public class C6017aep {
    private static final Log logger = LogPrinter.setClass(C6017aep.class);
    private GHudMark fnw;
    private boolean fnx;
    private boolean fny;
    private Color fnz;

    /* renamed from: lV */
    private IEngineGraphics f4426lV;

    /* renamed from: pI */
    private Actor f4427pI;

    public C6017aep(Actor cr) {
        this.f4427pI = cr;
        String iA = cr.aSp().mo647iA();
        this.f4426lV = cr.ald().getEngineGraphics();
        if (iA != null) {
            RenderAsset bT = this.f4426lV.mo3047bT(iA);
            GHudMark gHudMark = (GHudMark) bT;
            if (gHudMark == null) {
                logger.error("Mark " + iA + " not found.");
                GHudMark gHudMark2 = new GHudMark();
                gHudMark2.setName("Default hudmark");
                gHudMark2.setMark(this.f4426lV.aej().getDefaultMaterial());
                gHudMark2.setLock(this.f4426lV.aej().getDefaultMaterial());
                gHudMark2.setMarkOut(this.f4426lV.aej().getDefaultMaterial());
                gHudMark2.setMarkSelected(this.f4426lV.aej().getDefaultMaterial());
                gHudMark2.setMaxLockSize(200);
                gHudMark2.setMinLockSize(1);
                gHudMark2.setAreaRadius(250.0f);
                gHudMark2.setCenterColor(new Color(0.0f, 0.0f, 0.0f, 0.0f));
                gHudMark2.setOutColor(new Color(0.5f, 0.5f, 0.5f, 0.5f));
                gHudMark2.setSatellitesColor(new Color(1.0f, 1.0f, 1.0f, 0.5f));
                gHudMark2.setSelectedColor(new Color(1.0f, 1.0f, 1.0f, 1.0f));
                gHudMark2.setShowWhenOffscreen(false);
                gHudMark2.setMaxVisibleDistance(0.0f);
                gHudMark = gHudMark2;
                bT = gHudMark2;
            }
            if (gHudMark.getMark() == null) {
                logger.error("Mark " + iA + " no mark material.");
                gHudMark.setMark(this.f4426lV.aej().getDefaultMaterial());
            }
            if (gHudMark.getLock() == null) {
                logger.error("Mark " + iA + " no lock material.");
                gHudMark.setLock(this.f4426lV.aej().getDefaultMaterial());
            }
            if (gHudMark.getMarkOut() == null) {
                logger.error("Mark " + iA + " no out material.");
                gHudMark.setMarkOut(this.f4426lV.aej().getDefaultMaterial());
            }
            if (gHudMark.getMarkSelected() == null) {
                logger.error("Mark " + iA + " no selected material.");
                gHudMark.setMarkSelected(this.f4426lV.aej().getDefaultMaterial());
            }
            this.f4426lV.aeh().addChild((GuiSceneObject) bT);
            if (!(bT instanceof GHudMark)) {
                throw new RuntimeException("RenderMark invalid in " + cr.aSp().getName() + ". Expected RHudMark received " + bT.getClass().getSimpleName() + ". Actor type " + cr.getClass().getSimpleName());
            }
            this.fnw = (GHudMark) bT;
            this.fnw.setTarget(cr.cHg(), this.f4426lV.aea());
            this.fnw.setRender(false);
            return;
        }
        throw new NullPointerException("RenderMark is null in " + cr.aSp().getName());
    }

    public void bUj() {
        this.fnw.setShowWhenOffscreen(false);
    }

    public void bUk() {
        this.fnw.setShowWhenOffscreen(true);
    }

    /* renamed from: l */
    public void mo13172l(SceneObject sceneObject) {
        if (bUl()) {
            logger.info("Not executed updateTarget(SceneObject) because mark is null");
        } else {
            this.fnw.setTarget(sceneObject, this.f4426lV.aea());
        }
    }

    public void show() {
        if (this.fnw != null) {
            this.fnw.setRender(true);
            this.fnx = true;
        }
    }

    public void hide() {
        if (this.fnw != null) {
            this.fnw.setRender(false);
            this.fnx = false;
        }
    }

    public boolean isVisible() {
        return this.fnw.isRender();
    }

    public boolean isOffScreen() {
        return this.fnw.isOffScreen();
    }

    public void destroy() {
        if (this.fnw != null) {
            this.f4426lV.aeh().removeChild(this.fnw);
            this.fnw = null;
        }
    }

    public boolean isValid() {
        return this.fnw == null || !this.fnw.isDisposed() || this.f4427pI == null || this.f4427pI.isDisposed() || !this.f4427pI.cLZ();
    }

    private boolean bUl() {
        return this.fnw == null;
    }

    public float getMarkSize() {
        if (!bUl()) {
            return this.fnw.getMarkSize();
        }
        logger.info("Not executed getMarkSize() because mark is null");
        return 0.0f;
    }

    public Vec3f getScreenPosition() {
        if (!bUl()) {
            return this.fnw.getScreenPosition();
        }
        logger.info("Called getScreenPosition() but was returned new object because mark is null for target " + mo13167ha());
        return new Vec3f();
    }

    public SceneObject getTarget() {
        return this.f4427pI.cHg();
    }

    public void bUm() {
        if (bUl()) {
            logger.info("Not executed markAsSelected() because mark is null");
        } else {
            this.fnw.setShowSatellites(true);
        }
    }

    public void bUn() {
        if (bUl()) {
            logger.info("Not executed markAsDeselected() because mark is null");
        } else {
            this.fnw.setShowSatellites(false);
        }
    }

    /* renamed from: Vv */
    public Vec3d mo13149Vv() {
        return this.f4427pI.aSp().getPosition();
    }

    public void bUo() {
        if (bUl()) {
            logger.info("Not executed showFrame() because mark is null");
            return;
        }
        Color satellitesColor = this.fnw.getSatellitesColor();
        satellitesColor.w = 1.0f;
        this.fnw.setSatellitesColor(satellitesColor);
    }

    public void bUp() {
        if (bUl()) {
            logger.info("Not executed hideFrame() because mark is null");
            return;
        }
        Color satellitesColor = this.fnw.getSatellitesColor();
        satellitesColor.w = 0.0f;
        this.fnw.setSatellitesColor(satellitesColor);
    }

    public float getAreaRadius() {
        if (!bUl()) {
            return this.fnw.getAreaRadius();
        }
        logger.info("Called getAreaRadius() and returned 0 (zero) because mark is null");
        return 0.0f;
    }

    public void setAreaRadius(float f) {
        if (bUl()) {
            logger.info("Not executed setAreaRadius(float) because mark is null");
        } else {
            this.fnw.setAreaRadius(f);
        }
    }

    public boolean bUq() {
        return this.fnx;
    }

    /* renamed from: ha */
    public Actor mo13167ha() {
        return this.f4427pI.aSp();
    }

    public Color bUr() {
        if (this.fnz == null) {
            this.fnz = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        }
        return this.fnz;
    }

    public Color getPrimitiveColor() {
        if (bUl()) {
            return bUr();
        }
        return this.fnw.getCenterColor();
    }

    public void setPrimitiveColor(Color color) {
        if (!bUl()) {
            this.fnw.setCenterColor(color);
            this.fnw.setOutColor(color);
        }
    }

    public GHudMark bUs() {
        return this.fnw;
    }

    public boolean bUt() {
        return this.fny;
    }

    /* renamed from: eh */
    public void mo13161eh(boolean z) {
        this.fny = z;
    }
}
