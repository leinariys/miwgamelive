package p001a;

import sdljava.joystick.SDLJoystick;

/* renamed from: a.aqp  reason: case insensitive filesystem */
/* compiled from: a */
public class C6641aqp {
    private SDLJoystick gpu;

    public C6641aqp(SDLJoystick sDLJoystick) {
        this.gpu = sDLJoystick;
    }

    public int crl() {
        return this.gpu.joystickNumAxes();
    }

    public int crm() {
        return this.gpu.joystickNumButtons();
    }

    public int crn() {
        return this.gpu.joystickNumHats();
    }

    public int cro() {
        return this.gpu.joystickNumBalls();
    }

    public void close() {
        this.gpu.joystickClose();
    }
}
