package p001a;

import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.item.aUR;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.aMj  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C5517aMj implements ListCellRenderer {
    /* renamed from: a */
    public abstract Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2);

    public Component getListCellRendererComponent(JList jList, Object obj, int i, boolean z, boolean z2) {
        Component a = mo1307a((aUR) jList.getClientProperty(aUR.propertyKey), jList, obj, i, z, z2);
        IComponentManager e = ComponentManager.getCssHolder(a);
        if (e != null) {
            int i2 = 0;
            if (z) {
                i2 = 2;
            }
            if (z2) {
                i2 |= 128;
            }
            e.mo13063q(130, i2);
        }
        return a;
    }
}
