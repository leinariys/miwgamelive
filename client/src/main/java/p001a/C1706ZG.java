package p001a;

import com.hoplon.geometry.Color;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.script.Actor;
import logic.WrapRunnable;
import logic.res.ILoaderTrail;
import logic.res.sound.C0907NJ;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.RGroup;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;
import taikodom.render.shaders.Shader;

/* renamed from: a.ZG */
/* compiled from: a */
public class C1706ZG extends WrapRunnable {
    C3738uP fye;
    private float aAc;
    private Color finalColor;
    private C1707a fyf;
    private RGroup fyg;
    private Actor fyh;
    private Color initialColor;
    private long startTime;

    /* renamed from: a */
    public void mo7337a(Actor cr, float f, Color color, Color color2) {
        this.finalColor = color2;
        this.initialColor = color;
        this.aAc = f;
        this.fyg = new RGroup();
        if (this.fye != null) {
            this.fye.mo22374XA();
            this.fye = null;
        }
        this.fyh = cr;
        this.fye = new C3738uP(cr.cHg(), (SceneObject) this.fyg);
        Vec3d position = cr.cMs().getPosition();
        Quat4fWrap orientation = cr.cMs().getOrientation();
        this.fyg.setPosition(position);
        this.fyg.setOrientation(orientation);
        ILoaderTrail alg = C5916acs.getSingolton().getLoaderTrail();
        RenderAsset asset = alg.getAsset("fadeout_shader");
        if (asset != null) {
            setShader((Shader) asset);
        } else {
            alg.mo4995a("data/shaders/fx_passes.pro", "fadeout_shader", (Scene) null, new C1708b(), "FadeMeshInOut");
        }
    }

    /* access modifiers changed from: private */
    public void setShader(Shader shader) {
        this.fye.mo22376a(shader);
        this.fye.mo22380c(this.initialColor);
        this.startTime = System.currentTimeMillis();
        C5916acs.getSingolton().getEngineGraphics().adZ().addChild(this.fyg);
        C5916acs.getSingolton().alf().addTask("fade object", this, 50);
    }

    /* renamed from: a */
    public void mo7338a(C1707a aVar) {
        this.fyf = aVar;
    }

    public void run() {
        if (C5916acs.getSingolton() == null || C5916acs.getSingolton().mo4089dL() == null) {
            terminate();
        } else if (C5916acs.getSingolton().mo4089dL().bQx() == null) {
            terminate();
        } else if (!C5916acs.getSingolton().mo4089dL().bQx().cLZ() || this.fyh == null || this.fyh.cHg() == null || this.fyg == null) {
            terminate();
        } else {
            this.fyg.setTransform(this.fyh.cHg().getTransform());
            float currentTimeMillis = (((float) (System.currentTimeMillis() - this.startTime)) * 0.001f) / this.aAc;
            if (currentTimeMillis > 1.0f) {
                terminate();
                return;
            }
            this.fye.mo22380c(new Color((this.finalColor.x * currentTimeMillis) + (this.initialColor.x * (1.0f - currentTimeMillis)), (this.finalColor.y * currentTimeMillis) + (this.initialColor.y * (1.0f - currentTimeMillis)), (this.finalColor.z * currentTimeMillis) + (this.initialColor.z * (1.0f - currentTimeMillis)), ((1.0f - currentTimeMillis) * this.initialColor.w) + (this.finalColor.w * currentTimeMillis)));
        }
    }

    private void terminate() {
        cancel();
        if (this.fye != null) {
            this.fye.mo22380c(this.finalColor);
            this.fye.mo22375XB();
            this.fye = null;
        }
        if (this.fyg != null) {
            C5916acs.getSingolton().getEngineGraphics().adZ().removeChild(this.fyg);
            this.fyg.dispose();
            this.fyg = null;
        }
        if (this.fyf != null) {
            this.fyf.aDx();
            this.fyf = null;
        }
        this.fyh = null;
    }

    /* renamed from: a.ZG$a */
    public interface C1707a {
        void aDx();
    }

    /* renamed from: a.ZG$b */
    /* compiled from: a */
    class C1708b implements C0907NJ {
        C1708b() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            C1706ZG.this.setShader((Shader) renderAsset);
        }
    }
}
