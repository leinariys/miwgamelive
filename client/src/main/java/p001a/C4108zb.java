package p001a;

import com.hoplon.commons.thread.ProfilingThreadPoolExecutor$ProfilingThreadPoolExecutorConsole;
import logic.thred.PoolThread;
import logic.thred.ThreadWrapper;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: a.zb */
/* compiled from: a */
public class C4108zb extends ThreadPoolExecutor {
    /* access modifiers changed from: private */
    public AtomicLong bNb = new AtomicLong();
    /* access modifiers changed from: private */
    public AtomicLong bNc = new AtomicLong();
    /* access modifiers changed from: private */
    public AtomicLong bNd = new AtomicLong();
    /* access modifiers changed from: private */
    public AtomicInteger bNe = new AtomicInteger();
    /* access modifiers changed from: private */
    public C4110b bNf;
    private String name;

    public C4108zb(int i, int i2, long j, TimeUnit timeUnit, BlockingQueue<Runnable> blockingQueue, C4110b bVar, String str) {
        super(i, i2, j, timeUnit, blockingQueue, bVar);
        this.bNf = bVar;
        this.name = str;
        aqT();
    }

    public void execute(Runnable runnable) {
        super.execute(new C4109a(runnable));
    }

    private void aqT() {
        MBeanServer platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
        if (platformMBeanServer != null) {
            try {
                platformMBeanServer.registerMBean(new ProfilingThreadPoolExecutor$ProfilingThreadPoolExecutorConsole(this), new ObjectName("Queues:name=" + this.name));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a.zb$b */
    /* compiled from: a */
    public static class C4110b extends PoolThread.ThreadFactoryWrapper {
        public AtomicInteger gvI = new AtomicInteger();

        public C4110b(String prefix, boolean isDaemon) {
            super(prefix, isDaemon);
        }

        public Thread newThread(Runnable runnable) {
            C4111a aVar = new C4111a(runnable, String.valueOf(this.prefix) + " " + this.atomicInteger.incrementAndGet());
            aVar.setDaemon(this.isDaemon);
            return aVar;
        }

        /* renamed from: a.zb$b$a */
        class C4111a extends ThreadWrapper {
            C4111a(Runnable runnable, String str) {
                super(runnable, str);
            }

            public void run() {
                C4110b.this.gvI.incrementAndGet();
                try {
                    super.run();
                } finally {
                    C4110b.this.gvI.decrementAndGet();
                }
            }
        }
    }

    /* renamed from: a.zb$a */
    class C4109a implements Runnable {
        private final /* synthetic */ Runnable bsv;

        C4109a(Runnable runnable) {
            this.bsv = runnable;
        }

        public void run() {
            long j;
            long waitedCount = getWaitedCount();
            long nanoTime = System.nanoTime();
            this.bsv.run();
            long nanoTime2 = (System.nanoTime() - nanoTime) / 1000;
            long waitedCount2 = getWaitedCount() - waitedCount;
            C4108zb.this.bNe.getAndIncrement();
            C4108zb.this.bNb.getAndAdd(nanoTime2);
            C4108zb.this.bNd.getAndAdd(waitedCount2);
            do {
                j = C4108zb.this.bNc.get();
                if (nanoTime2 < j || C4108zb.this.bNc.compareAndSet(j, nanoTime2)) {
                }
                j = C4108zb.this.bNc.get();
                return;
            } while (C4108zb.this.bNc.compareAndSet(j, nanoTime2));
        }

        private long getWaitedCount() {
            ThreadInfo threadInfo = ManagementFactory.getThreadMXBean().getThreadInfo(Thread.currentThread().getId());
            return threadInfo.getBlockedCount() + threadInfo.getWaitedCount();
        }
    }
}
