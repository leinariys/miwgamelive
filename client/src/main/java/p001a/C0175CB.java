package p001a;

import game.io.IReadExternal;
import game.io.IWriteExternal;
import logic.baa.C1616Xf;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.CB */
/* compiled from: a */
public class C0175CB extends C5546aNm {

    Object cvX;

    public C0175CB() {
    }

    public C0175CB(Object obj) {
        this.cvX = obj;
    }

    public int getType() {
        return C6976axp._default.ordinal();
    }

    public String toString() {
        return "Info[newValue=" + this.cvX + "]";
    }

    public Object aBB() {
        return this.cvX;
    }

    /* renamed from: K */
    public Object mo880K(Object obj) {
        return this.cvX;
    }

    /* renamed from: b */
    public void mo886b(C1616Xf xf, ObjectInput objectInput) {
        this.cvX = this.f3436Ul.mo11304a(xf, objectInput);
    }

    /* renamed from: a */
    public void mo883a(C1616Xf xf, ObjectOutput objectOutput) {
        this.f3436Ul.mo11305a(xf, objectOutput, this.cvX);
    }

    /* renamed from: b */
    public void mo885b(C1616Xf xf, IReadExternal vm) {
        this.cvX = this.f3436Ul.mo2186a(xf, vm);
    }

    /* renamed from: a */
    public C5546aNm mo881a(C5546aNm anm) {
        return this;
    }

    /* renamed from: a */
    public void mo882a(C1616Xf xf, IWriteExternal att) {
        this.f3436Ul.mo2187a(xf, att, this.cvX);
    }
}
