package p001a;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aFK */
/* compiled from: a */
public final class aFK {
    public static final aFK hJR = new aFK("GrannyExcludeTypeTree", grannyJNI.GrannyExcludeTypeTree_get());
    private static aFK[] hJS = {hJR};

    /* renamed from: pF */
    private static int f2792pF = 0;

    /* renamed from: pG */
    private final int f2793pG;

    /* renamed from: pH */
    private final String f2794pH;

    private aFK(String str) {
        this.f2794pH = str;
        int i = f2792pF;
        f2792pF = i + 1;
        this.f2793pG = i;
    }

    private aFK(String str, int i) {
        this.f2794pH = str;
        this.f2793pG = i;
        f2792pF = i + 1;
    }

    private aFK(String str, aFK afk) {
        this.f2794pH = str;
        this.f2793pG = afk.f2793pG;
        f2792pF = this.f2793pG + 1;
    }

    /* renamed from: xU */
    public static aFK m14542xU(int i) {
        if (i < hJS.length && i >= 0 && hJS[i].f2793pG == i) {
            return hJS[i];
        }
        for (int i2 = 0; i2 < hJS.length; i2++) {
            if (hJS[i2].f2793pG == i) {
                return hJS[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + aFK.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f2793pG;
    }

    public String toString() {
        return this.f2794pH;
    }
}
