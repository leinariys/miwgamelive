package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;

/* renamed from: a.nd */
/* compiled from: a */
public class C3089nd extends C1083Pr {
    public final C3978xf aGG;
    public final C3978xf aGH;
    public final C6460anQ[] aGI;
    public final C6460anQ[] aGJ;
    public final C6439amv aGK;
    public final C2615hd[] aGL;
    public final C3978xf aGN;
    public final C3978xf aGO;
    public final Vec3f aGP;
    public final Vec3f[] aGQ;
    public float aGM;
    public boolean aGR;

    public C3089nd() {
        super(aVP.D6_CONSTRAINT_TYPE);
        this.aGG = new C3978xf();
        this.aGH = new C3978xf();
        this.aGI = new C6460anQ[]{new C6460anQ(), new C6460anQ(), new C6460anQ()};
        this.aGJ = new C6460anQ[]{new C6460anQ(), new C6460anQ(), new C6460anQ()};
        this.aGK = new C6439amv();
        this.aGL = new C2615hd[]{new C2615hd(), new C2615hd(), new C2615hd()};
        this.aGN = new C3978xf();
        this.aGO = new C3978xf();
        this.aGP = new Vec3f();
        this.aGQ = new Vec3f[]{new Vec3f(), new Vec3f(), new Vec3f()};
        this.aGR = true;
    }

    public C3089nd(C6238ajC ajc, C6238ajC ajc2, C3978xf xfVar, C3978xf xfVar2, boolean z) {
        super(aVP.D6_CONSTRAINT_TYPE, ajc, ajc2);
        this.aGG = new C3978xf();
        this.aGH = new C3978xf();
        this.aGI = new C6460anQ[]{new C6460anQ(), new C6460anQ(), new C6460anQ()};
        this.aGJ = new C6460anQ[]{new C6460anQ(), new C6460anQ(), new C6460anQ()};
        this.aGK = new C6439amv();
        this.aGL = new C2615hd[]{new C2615hd(), new C2615hd(), new C2615hd()};
        this.aGN = new C3978xf();
        this.aGO = new C3978xf();
        this.aGP = new Vec3f();
        this.aGQ = new Vec3f[]{new Vec3f(), new Vec3f(), new Vec3f()};
        this.aGG.mo22947a(xfVar);
        this.aGH.mo22947a(xfVar2);
        this.aGR = z;
    }

    /* renamed from: a */
    private static float m36305a(Matrix3fWrap ajd, int i) {
        return ajd.getElement(i % 3, i / 3);
    }

    /* renamed from: a */
    private static boolean m36306a(Matrix3fWrap ajd, Vec3f vec3f) {
        if (m36305a(ajd, 2) >= 1.0f) {
            vec3f.x = (float) Math.atan2((double) m36305a(ajd, 3), (double) m36305a(ajd, 4));
            vec3f.y = 1.5707964f;
            vec3f.z = 0.0f;
            return false;
        } else if (m36305a(ajd, 2) > -1.0f) {
            vec3f.x = (float) Math.atan2((double) (-m36305a(ajd, 5)), (double) m36305a(ajd, 8));
            vec3f.y = (float) Math.asin((double) m36305a(ajd, 2));
            vec3f.z = (float) Math.atan2((double) (-m36305a(ajd, 1)), (double) m36305a(ajd, 0));
            return true;
        } else {
            vec3f.x = -((float) Math.atan2((double) m36305a(ajd, 3), (double) m36305a(ajd, 4)));
            vec3f.y = -1.5707964f;
            vec3f.z = 0.0f;
            return false;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: PS */
    public void mo20815PS() {
        this.stack.bcF();
        try {
            Matrix3fWrap ajd = (Matrix3fWrap) this.stack.bcK().get();
            Matrix3fWrap ajd2 = (Matrix3fWrap) this.stack.bcK().get();
            ajd.set(this.aGN.bFF);
            C3427rS.m38378c(ajd);
            ajd2.mul(ajd, this.aGO.bFF);
            m36306a(ajd2, this.aGP);
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            this.aGO.bFF.getColumn(0, vec3f);
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            this.aGN.bFF.getColumn(2, vec3f2);
            this.aGQ[1].cross(vec3f2, vec3f);
            this.aGQ[0].cross(this.aGQ[1], vec3f2);
            this.aGQ[2].cross(vec3f, this.aGQ[1]);
        } finally {
            this.stack.bcG();
        }
    }

    /* renamed from: PT */
    public void mo20816PT() {
        this.aGN.mo22947a(this.dQm.ceu());
        this.aGN.mo22954c(this.aGG);
        this.aGO.mo22947a(this.dQn.ceu());
        this.aGO.mo22954c(this.aGH);
        mo20815PS();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo20824a(int i, Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        this.stack.bcF();
        try {
            Matrix3fWrap g = this.stack.bcK().mo15565g(this.dQm.ceu().bFF);
            g.transpose();
            Matrix3fWrap g2 = this.stack.bcK().mo15565g(this.dQn.ceu().bFF);
            g2.transpose();
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            vec3f4.sub(vec3f2, this.dQm.ces());
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            vec3f5.sub(vec3f3, this.dQn.ces());
            this.aGI[i].mo14967a(g, g2, vec3f4, vec3f5, vec3f, this.dQm.ceq(), this.dQm.aRf(), this.dQn.ceq(), this.dQn.aRf());
        } finally {
            this.stack.bcG();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo20823a(int i, Vec3f vec3f) {
        this.stack.bcK().push();
        try {
            Matrix3fWrap g = this.stack.bcK().mo15565g(this.dQm.ceu().bFF);
            g.transpose();
            Matrix3fWrap g2 = this.stack.bcK().mo15565g(this.dQn.ceu().bFF);
            g2.transpose();
            this.aGJ[i].mo14969a(vec3f, g, g2, this.dQm.ceq(), this.dQn.ceq());
        } finally {
            this.stack.bcK().pop();
        }
    }

    /* renamed from: dc */
    public boolean mo20826dc(int i) {
        this.aGL[i].mo19289ar(C0647JL.m5594b(this.aGP, i));
        return this.aGL[i].mo19291xX();
    }

    /* renamed from: PU */
    public void mo4840PU() {
        this.stack.bcH().push();
        try {
            mo20816PT();
            Vec3f vec3f = this.aGN.bFG;
            Vec3f vec3f2 = this.aGO.bFG;
            ((Vec3f) this.stack.bcH().get()).sub(vec3f, this.dQm.ces());
            ((Vec3f) this.stack.bcH().get()).sub(vec3f2, this.dQn.ces());
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            for (int i = 0; i < 3; i++) {
                if (this.aGK.mo14912dg(i)) {
                    if (this.aGR) {
                        this.aGN.bFF.getColumn(i, vec3f3);
                    } else {
                        this.aGO.bFF.getColumn(i, vec3f3);
                    }
                    mo20824a(i, vec3f3, vec3f, vec3f2);
                }
            }
            for (int i2 = 0; i2 < 3; i2++) {
                if (mo20826dc(i2)) {
                    vec3f3.set(mo20827dd(i2));
                    mo20823a(i2, vec3f3);
                }
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: cv */
    public void mo4847cv(float f) {
        this.stack.bcH().push();
        try {
            this.aGM = f;
            Vec3f ac = this.stack.bcH().mo4458ac(this.aGN.bFG);
            Vec3f ac2 = this.stack.bcH().mo4458ac(this.aGO.bFG);
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            for (int i = 0; i < 3; i++) {
                if (this.aGK.mo14912dg(i)) {
                    float clX = 1.0f / this.aGI[i].clX();
                    if (this.aGR) {
                        this.aGN.bFF.getColumn(i, vec3f);
                    } else {
                        this.aGO.bFF.getColumn(i, vec3f);
                    }
                    this.aGK.mo14911a(this.aGM, clX, this.dQm, ac, this.dQn, ac2, i, vec3f);
                }
            }
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            for (int i2 = 0; i2 < 3; i2++) {
                if (this.aGL[i2].mo19291xX()) {
                    vec3f2.set(mo20827dd(i2));
                    this.aGL[i2].mo19288a(this.aGM, vec3f2, 1.0f / this.aGJ[i2].clX(), this.dQm, this.dQn);
                }
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: cw */
    public void mo20825cw(float f) {
    }

    /* renamed from: dd */
    public Vec3f mo20827dd(int i) {
        return this.aGQ[i];
    }

    /* renamed from: de */
    public float mo20828de(int i) {
        return C0647JL.m5594b(this.aGP, i);
    }

    /* renamed from: PV */
    public C3978xf mo20817PV() {
        return this.aGN;
    }

    /* renamed from: PW */
    public C3978xf mo20818PW() {
        return this.aGO;
    }

    /* renamed from: PX */
    public C3978xf mo20819PX() {
        return this.aGG;
    }

    /* renamed from: PY */
    public C3978xf mo20820PY() {
        return this.aGH;
    }

    /* renamed from: w */
    public void mo20831w(Vec3f vec3f) {
        this.aGK.gaQ.set(vec3f);
    }

    /* renamed from: x */
    public void mo20832x(Vec3f vec3f) {
        this.aGK.gaR.set(vec3f);
    }

    /* renamed from: y */
    public void mo20833y(Vec3f vec3f) {
        this.aGL[0].f7981Ts = vec3f.x;
        this.aGL[1].f7981Ts = vec3f.y;
        this.aGL[2].f7981Ts = vec3f.z;
    }

    /* renamed from: z */
    public void mo20834z(Vec3f vec3f) {
        this.aGL[0].f7982Tt = vec3f.x;
        this.aGL[1].f7982Tt = vec3f.y;
        this.aGL[2].f7982Tt = vec3f.z;
    }

    /* renamed from: df */
    public C2615hd mo20829df(int i) {
        return this.aGL[i];
    }

    /* renamed from: PZ */
    public C6439amv mo20821PZ() {
        return this.aGK;
    }

    /* renamed from: a */
    public void mo20822a(int i, float f, float f2) {
        if (i < 3) {
            C0647JL.m5590a(this.aGK.gaQ, i, f);
            C0647JL.m5590a(this.aGK.gaR, i, f2);
            return;
        }
        this.aGL[i - 3].f7981Ts = f;
        this.aGL[i - 3].f7982Tt = f2;
    }

    /* renamed from: dg */
    public boolean mo20830dg(int i) {
        if (i < 3) {
            return this.aGK.mo14912dg(i);
        }
        return this.aGL[i - 3].mo19290xW();
    }
}
