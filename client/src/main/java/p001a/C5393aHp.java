package p001a;

/* renamed from: a.aHp  reason: case insensitive filesystem */
/* compiled from: a */
public class C5393aHp<T> {
    private String name;
    private T object;

    public C5393aHp(T t, String str) {
        this.name = str;
        this.object = t;
    }

    /* renamed from: D */
    public boolean mo4586D(T t) {
        if (this.object == null || this.object == t) {
            return true;
        }
        return false;
    }

    public String toString() {
        return this.name;
    }

    public T getObject() {
        return this.object;
    }
}
