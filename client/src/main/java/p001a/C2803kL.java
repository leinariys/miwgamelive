package p001a;

import game.io.IExternalIO;
import game.script.Character;
import util.Syst;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.kL */
/* compiled from: a */
public class C2803kL implements C1296TB {
    List<C2804a> auU = new ArrayList();

    /* renamed from: KP */
    public List<C2804a> mo20048KP() {
        return this.auU;
    }

    public void close() {
    }

    public void flush() {
    }

    /* renamed from: ab */
    public void writeString(String str) {
        this.auU.add(new C2804a(str, 134));
    }

    /* renamed from: pe */
    public void mo16277pe() {
        this.auU.add(new C2804a((String) null, 135));
    }

    /* renamed from: c */
    public void mo16268c(String str, int i) {
        this.auU.add(new C2804a(str, 1, Byte.valueOf((byte) i)));
    }

    /* renamed from: a */
    public void mo16264a(String str, byte[] bArr) {
        this.auU.add(new C2804a(str, 161, bArr));
    }

    /* renamed from: a */
    public void mo16265a(String str, byte[] bArr, int i, int i2) {
        this.auU.add(new C2804a(str, 161, Syst.copyOfRange(bArr, i, i2)));
    }

    /* renamed from: a */
    public void mo16261a(String str, IExternalIO afa) {
        this.auU.add(new C2804a(str, 145, afa));
    }

    /* renamed from: a */
    public void mo16259a(String str, int i, int i2) {
        this.auU.add(new C2804a(str, 7, i, Integer.valueOf(i2)));
    }

    /* renamed from: a */
    public void writeBoolean(String str, boolean z) {
        this.auU.add(new C2804a(str, 0, Boolean.valueOf(z)));
    }

    /* renamed from: d */
    public void writeByte(String str, int i) {
        this.auU.add(new C2804a(str, 1, Byte.valueOf((byte) i)));
    }

    /* renamed from: g */
    public void mo16274g(String str, String str2) {
    }

    /* renamed from: e */
    public void writeChar(String str, int i) {
        this.auU.add(new C2804a(str, 3, Character.valueOf((char) i)));
    }

    /* renamed from: h */
    public void mo16275h(String str, String str2) {
    }

    /* renamed from: a */
    public void writeDouble(String str, double d) {
        this.auU.add(new C2804a(str, 128, Double.valueOf(d)));
    }

    /* renamed from: a */
    public void mo16262a(String str, Enum<?> enumR) {
        this.auU.add(new C2804a(str, 133, enumR));
    }

    /* renamed from: a */
    public void writeFloat(String str, float f) {
        this.auU.add(new C2804a(str, 6, Float.valueOf(f)));
    }

    /* renamed from: f */
    public void writeInt(String str, int i) {
        this.auU.add(new C2804a(str, 4, Integer.valueOf(i)));
    }

    /* renamed from: a */
    public void writeLong(String str, long j) {
        this.auU.add(new C2804a(str, 5, Long.valueOf(j)));
    }

    /* renamed from: g */
    public void mo16273g(String str, Object obj) {
        this.auU.add(new C2804a(str, 147, obj));
    }

    /* renamed from: g */
    public void writeShort(String str, int i) {
        this.auU.add(new C2804a(str, 2, Short.valueOf((short) i)));
    }

    /* renamed from: i */
    public void mo16276i(String str, String str2) {
        this.auU.add(new C2804a(str, 131, str2));
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        String str2 = "";
        for (C2804a next : this.auU) {
            if (next.type == 134) {
                sb.append(str2);
                sb.append(next);
                str = String.valueOf(str2) + "  ";
            } else if (next.type == 135) {
                String substring = str2.substring(0, str2.length() - 2);
                sb.append(substring);
                sb.append(next);
                str = substring;
            } else {
                sb.append(str2);
                sb.append(next);
                str = str2;
            }
            sb.append("\r\n");
            str2 = str;
        }
        return sb.toString();
    }

    /* renamed from: b */
    public void mo16267b(String str, Class<?> cls) {
        this.auU.add(new C2804a(str, 144, cls));
    }

    /* renamed from: a.kL$a */
    public static class C2804a {
        public String key;
        public int type;
        public Object value;

        public C2804a(String str, int i) {
            this.key = str;
            this.type = i;
        }

        public C2804a(String str, int i, Object obj) {
            this.key = str;
            this.type = i;
            this.value = obj;
        }

        public C2804a(String str, int i, int i2, Object obj) {
            this.key = str;
            this.type = i;
            this.type = i2;
            this.value = obj;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            if (this.key != null) {
                sb.append(this.key).append(": ");
            } else if (this.type != 135) {
                sb.append("- ");
            }
            switch (this.type) {
                case 134:
                    sb.append('{');
                    break;
                case 135:
                    sb.append('}');
                    break;
                default:
                    sb.append(ayF.names[this.type]);
                    sb.append(' ').append(this.value);
                    break;
            }
            return sb.toString();
        }
    }
}
