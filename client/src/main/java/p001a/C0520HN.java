package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.network.message.externalizable.C0465GR;
import game.network.message.serializable.C6400amI;
import game.script.Actor;
import game.script.simulation.Space;
import logic.baa.C6200aiQ;
import logic.bbb.C4029yK;
import logic.data.link.C3396rD;
import logic.res.code.C5663aRz;
import logic.thred.C1362Tq;

import java.util.Collection;

/* renamed from: a.HN */
/* compiled from: a */
public class C0520HN extends C0501Gx {
    private static final float ddu = 1000.0f;
    /* renamed from: pI */
    private final Actor f662pI;
    public C6400amI ddj;
    C6200aiQ<Actor> ddm;
    private C3387qy<C0461GN> ddk;
    private Collection<C6625aqZ> ddl;
    private C0465GR ddn;
    private long ddo;
    private long ddp;
    private C6400amI ddq;
    private C6400amI ddr;
    private C6400amI ddt;
    private float ddv = 0.0f;
    private long ddw;
    private C0520HN ddx;
    private long ddy;

    public C0520HN(Space ea, Actor cr, C4029yK yKVar) {
        super(yKVar);
        this.f662pI = cr;
        if (this.f662pI.cKZ() != null) {
            this.ddx = this.f662pI.cKZ().aiD();
        }
        if (cr.cLl() != null) {
            this.cYX.blk().mo9484aA(cr.cLl());
        }
        if (cr.cLh() != null) {
            this.cYX.blm().set(cr.cLh());
        }
        if (cr.cLj() != null) {
            this.cYX.bll().mo15242f(cr.cLj());
            this.cYX.mo4490IQ().orientation.set(cr.cLj());
        }
        if (cr.cLf() != null) {
            this.cYX.bln().set(cr.cLf());
        }
        this.cYX.cjS();
        if (!cr.bGY()) {
            this.ddm = new C0521a();
            cr.mo8320a(C3396rD.bbE, (C6200aiQ<?>) this.ddm);
        }
    }

    /* renamed from: ha */
    public Actor mo2568ha() {
        return this.f662pI;
    }

    public void step(float f) {
        if (this.cYY != null) {
            if (!this.f662pI.bae()) {
                this.f662pI.aia();
                return;
            }
            float fv = m3609fv(f);
            if (fv != 0.0f) {
                super.step(fv);
            }
            this.f662pI.step(f);
            if (!aQZ() && this.f662pI.aiD() == this) {
                aSp().ala().aLW().mo11536cy(this.f662pI);
                if (this.ddk != null) {
                    this.ddk.mo2339kO();
                }
                if (this.cYY != null) {
                    this.cYY.mo3841kO();
                }
            }
        }
    }

    /* renamed from: b */
    public void mo2462b(int i, long j, Collection<C6625aqZ> collection, C6705asB asb) {
        this.f662pI.mo990b(i, j, collection, asb);
    }

    /* renamed from: W */
    public void mo2540W(float f) {
        wake();
        if (aQX() instanceof C5917act) {
            ((C5917act) aQX()).bON().set(0.0f, 0.0f, -f);
        }
    }

    /* renamed from: f */
    public void mo2567f(float f, float f2, float f3) {
        wake();
        if (aQX() instanceof C5917act) {
            ((C5917act) aQX()).bOM().set(f3, -f2, -f);
        }
    }

    /* renamed from: cO */
    public void mo2565cO(boolean z) {
        wake();
    }

    public boolean aSc() {
        return false;
    }

    public Quat4fWrap aSd() {
        return aSl().bll();
    }

    public Vec3d aSe() {
        return aSl().blk();
    }

    /* renamed from: qZ */
    public Vec3f mo2571qZ() {
        return this.cYX.blm();
    }

    public Vec3f aSf() {
        return this.cYX.bln();
    }

    /* renamed from: a */
    public void mo2541a(long j, Vec3d ajr, Quat4fWrap aoy, Vec3f vec3f, Vec3f vec3f2) {
        wake();
        this.cYX.mo14822gC(j);
        this.cYX.blk().mo9484aA(ajr);
        this.cYX.bll().mo15242f(aoy);
        this.cYX.mo4490IQ().orientation.set(aoy);
        this.cYX.blm().set(vec3f);
        this.cYX.bln().set(vec3f2);
        this.cYX.mo4490IQ().orientation.transform(vec3f, this.cYX.blq());
        this.cYX.mo4490IQ().orientation.transform(vec3f2, this.cYX.blt());
    }

    public boolean isDisposed() {
        return false;
    }

    public float aSg() {
        return ((C5917act) aQX()).bOM().x;
    }

    public float aSh() {
        float f = ((C5917act) aQX()).bOM().z;
        return f == 0.0f ? f : -f;
    }

    public float aSi() {
        float f = ((C5917act) aQX()).bOM().y;
        return f == 0.0f ? f : -f;
    }

    /* renamed from: as */
    public void mo2560as(float f) {
        wake();
        if (aQX() instanceof C5917act) {
            ((C5917act) aQX()).mo12729p(f);
        }
    }

    /* renamed from: at */
    public void mo2561at(float f) {
        wake();
        if (aQX() instanceof C5917act) {
            ((C5917act) aQX()).mo12728o(f);
        }
    }

    /* renamed from: av */
    public void mo2563av(float f) {
        wake();
        if (aQX() instanceof C5917act) {
            ((C5917act) aQX()).mo12732r(f);
        }
    }

    /* renamed from: au */
    public void mo2562au(float f) {
        wake();
        if (aQX() instanceof C5917act) {
            ((C5917act) aQX()).mo12731q(f);
        }
    }

    /* renamed from: VH */
    public float mo2539VH() {
        if (aQX() instanceof C5917act) {
            return ((C5917act) aQX()).bnO().y;
        }
        return 0.0f;
    }

    /* renamed from: VF */
    public float mo2538VF() {
        if (aQX() instanceof C5917act) {
            return ((C5917act) aQX()).bOL();
        }
        return 0.0f;
    }

    /* renamed from: ra */
    public float mo2572ra() {
        if (aQX() instanceof C5917act) {
            return ((C5917act) aQX()).bOP().z;
        }
        return 0.0f;
    }

    /* renamed from: rb */
    public float mo2573rb() {
        if (aQX() instanceof C5917act) {
            return ((C5917act) aQX()).bOO();
        }
        return 0.0f;
    }

    public void dispose() {
        if (this.cYY != null) {
            this.cYY.dispose();
            this.cYY = null;
        }
        if (this.ddk != null) {
            this.ddk.dispose();
            this.ddk = null;
        }
        if (this.ddm != null) {
            this.f662pI.mo8326b(C3396rD.bbE, (C6200aiQ<?>) this.ddm);
            this.ddm = null;
        }
    }

    public float aSj() {
        if (aQX() instanceof C5917act) {
            return -((C5917act) aQX()).bON().z;
        }
        return 0.0f;
    }

    /* renamed from: a */
    public void mo2543a(C1362Tq tq) {
        boolean z = this instanceof C1084Ps;
        wake();
        if (this.cYY != null) {
            if (!this.f662pI.bae()) {
                this.f662pI.aia();
                return;
            }
            C0520HN hn = (C0520HN) tq.mo5749Ji();
            if (hn.mo2438IM() != 3) {
                this.f662pI.mo992b(hn.f662pI, tq.mo5751Jk(), tq.mo5748Jh(), 1.0f, tq.mo5753Jm());
            }
        }
    }

    /* renamed from: a */
    public void mo2544a(C3387qy<C0461GN> qyVar) {
        wake();
        this.ddk = qyVar;
    }

    public C3387qy<C0461GN> aSk() {
        return this.ddk;
    }

    /* renamed from: l */
    public void mo2570l(Collection<C6625aqZ> collection) {
        wake();
        this.ddl = collection;
    }

    public Collection<C6625aqZ> aRu() {
        return this.ddl;
    }

    /* renamed from: a */
    public boolean mo2545a(C0520HN hn) {
        return true;
    }

    /* renamed from: b */
    public boolean mo2564b(C0520HN hn) {
        return false;
    }

    /* renamed from: a */
    public void mo2542a(C0465GR gr, long j) {
        wake();
        this.ddn = gr;
        this.ddo = j;
        this.ddp = this.ddo;
    }

    public C1003Om aSl() {
        if (!aSm() || !aSn() || this.ddr == null) {
            return mo2472kQ();
        }
        if (this.ddt == null) {
            this.ddt = new C6400amI();
        }
        this.ddt.mo14819b(this.ddr);
        this.ddt.mo14817a(this.cYX, 1.0f - this.ddv);
        return this.ddt;
    }

    /* renamed from: fv */
    private float m3609fv(float f) {
        if (this.cYY == null) {
            return f;
        }
        long bfx = this.cYY.bfx();
        if (this.ddo != 0 && this.ddo <= bfx) {
            if (aSm()) {
                if (this.ddr == null) {
                    this.ddr = new C6400amI();
                    this.ddr.mo14819b(this.cYX);
                }
                this.ddr.mo14819b(aSl());
                this.ddw = SingletonCurrentTimeMilliImpl.currentTimeMillis();
                this.ddv = 1.0f;
            }
            if (!isStatic()) {
                if (this.ddq == null) {
                    this.ddq = new C6400amI();
                    this.ddq.mo14819b(this.cYX);
                } else if (this.ddn.getMask() != 23) {
                    long timestamp = this.ddq.getTimestamp();
                    while (timestamp < this.ddo) {
                        long eR = m3607eR(this.ddo - timestamp);
                        mo2461b(((float) eR) * 0.001f, this.ddq);
                        this.ddq.mo14818b(((float) eR) * 0.001f, this.ddq);
                        timestamp += eR;
                    }
                }
                this.ddq.mo14822gC(this.ddo);
                this.ddn.mo2341a(this.ddq, aQX());
                this.cYX.mo14819b(this.ddq);
                long j = this.ddo;
                while (j < bfx) {
                    long eR2 = m3607eR(bfx - j);
                    mo2461b(((float) eR2) * 0.001f, this.cYX);
                    this.cYX.mo14818b(((float) eR2) * 0.001f, this.cYX);
                    j += eR2;
                }
            } else {
                this.ddn.mo2341a(this.cYX, aQX());
            }
            if (aSm()) {
                mo2448a(f, this.ddr);
            }
            this.ddo = 0;
            this.ddn = null;
            return 0.0f;
        } else if (!aSm() || !aSn()) {
            return f;
        } else {
            mo2448a(f, this.ddr);
            return f;
        }
    }

    /* renamed from: eR */
    private long m3607eR(long j) {
        if (j > 3000) {
            return 500;
        }
        if (j > 2000) {
            return 100;
        }
        if (j > 1000) {
            return 50;
        }
        if (j > 500) {
            return 30;
        }
        if (j > 10) {
            return 10;
        }
        return j;
    }

    private boolean aSm() {
        return this.f662pI.bGX();
    }

    private boolean aSn() {
        m3608eS(SingletonCurrentTimeMilliImpl.currentTimeMillis());
        return this.ddv > 0.0f;
    }

    /* renamed from: eS */
    private synchronized void m3608eS(long j) {
        if (this.ddv > 0.0f) {
            this.ddv = 1.0f - (((float) (j - this.ddw)) / ddu);
            if (this.ddv <= 0.0f) {
                this.ddv = 0.0f;
            }
        }
    }

    public String toString() {
        return this.f662pI + " " + this.f662pI.bFY() + " ActorSolid[" + hashCode() + ":" + mo2437IL() + ": P=" + getPosition() + " LV=" + mo2472kQ().blm();
    }

    /* renamed from: IS */
    public Object mo2441IS() {
        return this.f662pI;
    }

    public C0520HN aSo() {
        return this.ddx;
    }

    public Actor aSp() {
        return this.f662pI;
    }

    /* renamed from: IR */
    public void mo2440IR() {
        super.mo2440IR();
        if (this.cYY != null) {
            this.ddy = this.cYY.bfx();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo2461b(float f, C6400amI ami) {
        if (this.cYY.bfx() - this.ddy > 1000) {
            super.mo2461b(f, ami);
            return;
        }
        mo2472kQ().bls().set(0.0f, 0.0f, 0.0f);
        mo2472kQ().blr().set(0.0f, 0.0f, 0.0f);
    }

    public long aSq() {
        return this.ddp;
    }

    public boolean isReady() {
        return true;
    }

    /* renamed from: a.HN$a */
    class C0521a implements C6200aiQ<Actor> {
        C0521a() {
        }

        /* renamed from: a */
        public void mo1143a(Actor cr, C5663aRz arz, Object obj) {
            if (!((Boolean) obj).booleanValue()) {
                cr.mo8326b(C3396rD.bbE, (C6200aiQ<?>) C0520HN.this.ddm);
                C0520HN.this.ddm = null;
                cr.aia();
            }
        }
    }
}
