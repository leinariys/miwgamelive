package p001a;

import logic.res.sound.C0907NJ;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

/* renamed from: a.acj  reason: case insensitive filesystem */
/* compiled from: a */
public class C5907acj extends aGU {
    public RenderAsset fbb;
    public C0907NJ fbc;
    private Scene fba;

    public C5907acj(String str, String str2, C0907NJ nj, Scene scene, aOT aot, int i) {
        super(str, str2, aot);
        this.priority = i;
        this.fba = scene;
        this.fbc = nj;
    }

    /* access modifiers changed from: protected */
    public RenderAsset bOA() {
        return this.fbb;
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo12688c(RenderAsset renderAsset) {
        this.fbb = renderAsset;
    }

    /* access modifiers changed from: protected */
    public void boh() {
        this.fbc.mo968a(bOA());
    }

    /* access modifiers changed from: protected */
    public void boi() {
        RenderAsset bOA = bOA();
        if ((bOA instanceof SceneObject) && this.fba != null) {
            this.fba.addChild((SceneObject) bOA);
        }
    }
}
