package p001a;

import game.geometry.Matrix4fWrap;

/* renamed from: a.acb  reason: case insensitive filesystem */
/* compiled from: a */
class C5899acb extends aEH.C1791a<Matrix4fWrap> {
    final /* synthetic */ aEH eZT;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C5899acb(aEH aeh) {
        super((aEH.C1791a) null);
        this.eZT = aeh;
    }

    /* renamed from: iq */
    public Matrix4fWrap mo8563O(String str) {
        String[] split = str.split("[ \t\r\n]+");
        if (split.length != 16) {
            throw new IllegalArgumentException("Invalid number of arguments.");
        }
        float[] fArr = new float[16];
        for (int i = 0; i < fArr.length; i++) {
            fArr[i] = Float.parseFloat(split[i]);
        }
        return new Matrix4fWrap(fArr[0], fArr[4], fArr[8], fArr[12], fArr[1], fArr[5], fArr[9], fArr[13], fArr[2], fArr[6], fArr[10], fArr[14], fArr[3], fArr[7], fArr[11], fArr[15]);
    }
}
