package p001a;

import org.apache.commons.logging.LogFactory;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: a.Ss */
/* compiled from: a */
class C1277Ss extends ayA {
    private static final AtomicReference<ayA> eec = new AtomicReference<>();
    private static long eed;
    private static Socket eee;
    private static String eef;
    private static boolean eeg = false;
    private final int cHD = C1298TD.hFl;

    public C1277Ss(C1285Sv sv, String str) {
        super(sv);
        eef = str;
        restart();
        btx();
        btx();
    }

    private static void btx() {
        if (!eeg) {
            try {
                bty();
            } catch (IOException e) {
                LogFactory.getLog(C1277Ss.class).error("Raw ping to server is not working.", e);
                eeg = true;
            }
        }
    }

    private static void bty() {
        if (eee == null) {
            btz();
        }
        btB();
        btA();
    }

    private static void btz() {
        eee = new Socket(eef, 5458);
    }

    private static void btA() {
        String readLine = new BufferedReader(new InputStreamReader(eee.getInputStream())).readLine();
        if (readLine.indexOf("pong") == -1) {
            throw new RuntimeException("Ping expects a 'pong' reply but was: " + readLine);
        }
    }

    private static void btB() {
        OutputStream outputStream = eee.getOutputStream();
        PrintWriter printWriter = new PrintWriter(outputStream);
        printWriter.println("ping");
        printWriter.flush();
        outputStream.flush();
    }

    /* access modifiers changed from: protected */
    public void aGu() {
        switch (this.state) {
            case 0:
                mo16928jh(5000);
                this.state++;
                return;
            case 1:
                long nanoTime = System.nanoTime();
                btJ().cvr();
                long round = Math.round(((double) (System.nanoTime() - nanoTime)) / 1000000.0d);
                log("Send response time: " + round + " ms");
                mo16919a(Long.toString(round), C1285Sv.C1287b.None);
                btJ().mo16111b(round, cDK().gTZ, cDK().modifier, btv());
                this.state = 0;
                return;
            default:
                return;
        }
    }

    private long btv() {
        eec.compareAndSet((Object) null, this);
        if (eec.get() == this) {
            eed = btw();
        }
        return eed;
    }

    private long btw() {
        long nanoTime = System.nanoTime();
        btx();
        long round = Math.round(((double) (System.nanoTime() - nanoTime)) / 1000000.0d);
        log("Raw ping time: " + round + " ms");
        return round;
    }
}
