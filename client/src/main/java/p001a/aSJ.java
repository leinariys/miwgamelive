package p001a;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aSJ */
/* compiled from: a */
public final class aSJ {
    public static final aSJ iNp = new aSJ("GrannyUseAccumulatorNeighborhood", grannyJNI.GrannyUseAccumulatorNeighborhood_get());
    private static aSJ[] iNq = {iNp};

    /* renamed from: pF */
    private static int f3739pF = 0;

    /* renamed from: pG */
    private final int f3740pG;

    /* renamed from: pH */
    private final String f3741pH;

    private aSJ(String str) {
        this.f3741pH = str;
        int i = f3739pF;
        f3739pF = i + 1;
        this.f3740pG = i;
    }

    private aSJ(String str, int i) {
        this.f3741pH = str;
        this.f3740pG = i;
        f3739pF = i + 1;
    }

    private aSJ(String str, aSJ asj) {
        this.f3741pH = str;
        this.f3740pG = asj.f3740pG;
        f3739pF = this.f3740pG + 1;
    }

    /* renamed from: AI */
    public static aSJ m18169AI(int i) {
        if (i < iNq.length && i >= 0 && iNq[i].f3740pG == i) {
            return iNq[i];
        }
        for (int i2 = 0; i2 < iNq.length; i2++) {
            if (iNq[i2].f3740pG == i) {
                return iNq[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + aSJ.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f3740pG;
    }

    public String toString() {
        return this.f3741pH;
    }
}
