package p001a;

import java.io.File;
import java.io.FileFilter;

/* renamed from: a.bJ */
/* compiled from: a */
public class C2031bJ implements FileFilter {
    private final String suffix;

    public C2031bJ(String str) {
        this.suffix = str;
    }

    public boolean accept(File file) {
        if (file == null) {
            return false;
        }
        if (file.getAbsoluteFile().getName().endsWith(this.suffix) || file.isDirectory()) {
            return true;
        }
        return false;
    }
}
