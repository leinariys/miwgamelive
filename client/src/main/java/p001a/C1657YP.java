package p001a;

import org.w3c.dom.css.CSS2Properties;

import java.io.Serializable;

/* renamed from: a.YP */
/* compiled from: a */
public class C1657YP implements Serializable, CSS2Properties {
    public String getAzimuth() {
        return null;
    }

    public void setAzimuth(String str) {
    }

    public String getBackground() {
        return null;
    }

    public void setBackground(String str) {
    }

    public String getBackgroundAttachment() {
        return null;
    }

    public void setBackgroundAttachment(String str) {
    }

    public String getBackgroundColor() {
        return null;
    }

    public void setBackgroundColor(String str) {
    }

    public String getBackgroundImage() {
        return null;
    }

    public void setBackgroundImage(String str) {
    }

    public String getBackgroundPosition() {
        return null;
    }

    public void setBackgroundPosition(String str) {
    }

    public String getBackgroundRepeat() {
        return null;
    }

    public void setBackgroundRepeat(String str) {
    }

    public String getBorder() {
        return null;
    }

    public void setBorder(String str) {
    }

    public String getBorderCollapse() {
        return null;
    }

    public void setBorderCollapse(String str) {
    }

    public String getBorderColor() {
        return null;
    }

    public void setBorderColor(String str) {
    }

    public String getBorderSpacing() {
        return null;
    }

    public void setBorderSpacing(String str) {
    }

    public String getBorderStyle() {
        return null;
    }

    public void setBorderStyle(String str) {
    }

    public String getBorderTop() {
        return null;
    }

    public void setBorderTop(String str) {
    }

    public String getBorderRight() {
        return null;
    }

    public void setBorderRight(String str) {
    }

    public String getBorderBottom() {
        return null;
    }

    public void setBorderBottom(String str) {
    }

    public String getBorderLeft() {
        return null;
    }

    public void setBorderLeft(String str) {
    }

    public String getBorderTopColor() {
        return null;
    }

    public void setBorderTopColor(String str) {
    }

    public String getBorderRightColor() {
        return null;
    }

    public void setBorderRightColor(String str) {
    }

    public String getBorderBottomColor() {
        return null;
    }

    public void setBorderBottomColor(String str) {
    }

    public String getBorderLeftColor() {
        return null;
    }

    public void setBorderLeftColor(String str) {
    }

    public String getBorderTopStyle() {
        return null;
    }

    public void setBorderTopStyle(String str) {
    }

    public String getBorderRightStyle() {
        return null;
    }

    public void setBorderRightStyle(String str) {
    }

    public String getBorderBottomStyle() {
        return null;
    }

    public void setBorderBottomStyle(String str) {
    }

    public String getBorderLeftStyle() {
        return null;
    }

    public void setBorderLeftStyle(String str) {
    }

    public String getBorderTopWidth() {
        return null;
    }

    public void setBorderTopWidth(String str) {
    }

    public String getBorderRightWidth() {
        return null;
    }

    public void setBorderRightWidth(String str) {
    }

    public String getBorderBottomWidth() {
        return null;
    }

    public void setBorderBottomWidth(String str) {
    }

    public String getBorderLeftWidth() {
        return null;
    }

    public void setBorderLeftWidth(String str) {
    }

    public String getBorderWidth() {
        return null;
    }

    public void setBorderWidth(String str) {
    }

    public String getBottom() {
        return null;
    }

    public void setBottom(String str) {
    }

    public String getCaptionSide() {
        return null;
    }

    public void setCaptionSide(String str) {
    }

    public String getClear() {
        return null;
    }

    public void setClear(String str) {
    }

    public String getClip() {
        return null;
    }

    public void setClip(String str) {
    }

    public String getColor() {
        return null;
    }

    public void setColor(String str) {
    }

    public String getContent() {
        return null;
    }

    public void setContent(String str) {
    }

    public String getCounterIncrement() {
        return null;
    }

    public void setCounterIncrement(String str) {
    }

    public String getCounterReset() {
        return null;
    }

    public void setCounterReset(String str) {
    }

    public String getCue() {
        return null;
    }

    public void setCue(String str) {
    }

    public String getCueAfter() {
        return null;
    }

    public void setCueAfter(String str) {
    }

    public String getCueBefore() {
        return null;
    }

    public void setCueBefore(String str) {
    }

    public String getCursor() {
        return null;
    }

    public void setCursor(String str) {
    }

    public String getDirection() {
        return null;
    }

    public void setDirection(String str) {
    }

    public String getDisplay() {
        return null;
    }

    public void setDisplay(String str) {
    }

    public String getElevation() {
        return null;
    }

    public void setElevation(String str) {
    }

    public String getEmptyCells() {
        return null;
    }

    public void setEmptyCells(String str) {
    }

    public String getCssFloat() {
        return null;
    }

    public void setCssFloat(String str) {
    }

    public String getFont() {
        return null;
    }

    public void setFont(String str) {
    }

    public String getFontFamily() {
        return null;
    }

    public void setFontFamily(String str) {
    }

    public String getFontSize() {
        return null;
    }

    public void setFontSize(String str) {
    }

    public String getFontSizeAdjust() {
        return null;
    }

    public void setFontSizeAdjust(String str) {
    }

    public String getFontStretch() {
        return null;
    }

    public void setFontStretch(String str) {
    }

    public String getFontStyle() {
        return null;
    }

    public void setFontStyle(String str) {
    }

    public String getFontVariant() {
        return null;
    }

    public void setFontVariant(String str) {
    }

    public String getFontWeight() {
        return null;
    }

    public void setFontWeight(String str) {
    }

    public String getHeight() {
        return null;
    }

    public void setHeight(String str) {
    }

    public String getLeft() {
        return null;
    }

    public void setLeft(String str) {
    }

    public String getLetterSpacing() {
        return null;
    }

    public void setLetterSpacing(String str) {
    }

    public String getLineHeight() {
        return null;
    }

    public void setLineHeight(String str) {
    }

    public String getListStyle() {
        return null;
    }

    public void setListStyle(String str) {
    }

    public String getListStyleImage() {
        return null;
    }

    public void setListStyleImage(String str) {
    }

    public String getListStylePosition() {
        return null;
    }

    public void setListStylePosition(String str) {
    }

    public String getListStyleType() {
        return null;
    }

    public void setListStyleType(String str) {
    }

    public String getMargin() {
        return null;
    }

    public void setMargin(String str) {
    }

    public String getMarginTop() {
        return null;
    }

    public void setMarginTop(String str) {
    }

    public String getMarginRight() {
        return null;
    }

    public void setMarginRight(String str) {
    }

    public String getMarginBottom() {
        return null;
    }

    public void setMarginBottom(String str) {
    }

    public String getMarginLeft() {
        return null;
    }

    public void setMarginLeft(String str) {
    }

    public String getMarkerOffset() {
        return null;
    }

    public void setMarkerOffset(String str) {
    }

    public String getMarks() {
        return null;
    }

    public void setMarks(String str) {
    }

    public String getMaxHeight() {
        return null;
    }

    public void setMaxHeight(String str) {
    }

    public String getMaxWidth() {
        return null;
    }

    public void setMaxWidth(String str) {
    }

    public String getMinHeight() {
        return null;
    }

    public void setMinHeight(String str) {
    }

    public String getMinWidth() {
        return null;
    }

    public void setMinWidth(String str) {
    }

    public String getOrphans() {
        return null;
    }

    public void setOrphans(String str) {
    }

    public String getOutline() {
        return null;
    }

    public void setOutline(String str) {
    }

    public String getOutlineColor() {
        return null;
    }

    public void setOutlineColor(String str) {
    }

    public String getOutlineStyle() {
        return null;
    }

    public void setOutlineStyle(String str) {
    }

    public String getOutlineWidth() {
        return null;
    }

    public void setOutlineWidth(String str) {
    }

    public String getOverflow() {
        return null;
    }

    public void setOverflow(String str) {
    }

    public String getPadding() {
        return null;
    }

    public void setPadding(String str) {
    }

    public String getPaddingTop() {
        return null;
    }

    public void setPaddingTop(String str) {
    }

    public String getPaddingRight() {
        return null;
    }

    public void setPaddingRight(String str) {
    }

    public String getPaddingBottom() {
        return null;
    }

    public void setPaddingBottom(String str) {
    }

    public String getPaddingLeft() {
        return null;
    }

    public void setPaddingLeft(String str) {
    }

    public String getPage() {
        return null;
    }

    public void setPage(String str) {
    }

    public String getPageBreakAfter() {
        return null;
    }

    public void setPageBreakAfter(String str) {
    }

    public String getPageBreakBefore() {
        return null;
    }

    public void setPageBreakBefore(String str) {
    }

    public String getPageBreakInside() {
        return null;
    }

    public void setPageBreakInside(String str) {
    }

    public String getPause() {
        return null;
    }

    public void setPause(String str) {
    }

    public String getPauseAfter() {
        return null;
    }

    public void setPauseAfter(String str) {
    }

    public String getPauseBefore() {
        return null;
    }

    public void setPauseBefore(String str) {
    }

    public String getPitch() {
        return null;
    }

    public void setPitch(String str) {
    }

    public String getPitchRange() {
        return null;
    }

    public void setPitchRange(String str) {
    }

    public String getPlayDuring() {
        return null;
    }

    public void setPlayDuring(String str) {
    }

    public String getPosition() {
        return null;
    }

    public void setPosition(String str) {
    }

    public String getQuotes() {
        return null;
    }

    public void setQuotes(String str) {
    }

    public String getRichness() {
        return null;
    }

    public void setRichness(String str) {
    }

    public String getRight() {
        return null;
    }

    public void setRight(String str) {
    }

    public String getSize() {
        return null;
    }

    public void setSize(String str) {
    }

    public String getSpeak() {
        return null;
    }

    public void setSpeak(String str) {
    }

    public String getSpeakHeader() {
        return null;
    }

    public void setSpeakHeader(String str) {
    }

    public String getSpeakNumeral() {
        return null;
    }

    public void setSpeakNumeral(String str) {
    }

    public String getSpeakPunctuation() {
        return null;
    }

    public void setSpeakPunctuation(String str) {
    }

    public String getSpeechRate() {
        return null;
    }

    public void setSpeechRate(String str) {
    }

    public String getStress() {
        return null;
    }

    public void setStress(String str) {
    }

    public String getTableLayout() {
        return null;
    }

    public void setTableLayout(String str) {
    }

    public String getTextAlign() {
        return null;
    }

    public void setTextAlign(String str) {
    }

    public String getTextDecoration() {
        return null;
    }

    public void setTextDecoration(String str) {
    }

    public String getTextIndent() {
        return null;
    }

    public void setTextIndent(String str) {
    }

    public String getTextShadow() {
        return null;
    }

    public void setTextShadow(String str) {
    }

    public String getTextTransform() {
        return null;
    }

    public void setTextTransform(String str) {
    }

    public String getTop() {
        return null;
    }

    public void setTop(String str) {
    }

    public String getUnicodeBidi() {
        return null;
    }

    public void setUnicodeBidi(String str) {
    }

    public String getVerticalAlign() {
        return null;
    }

    public void setVerticalAlign(String str) {
    }

    public String getVisibility() {
        return null;
    }

    public void setVisibility(String str) {
    }

    public String getVoiceFamily() {
        return null;
    }

    public void setVoiceFamily(String str) {
    }

    public String getVolume() {
        return null;
    }

    public void setVolume(String str) {
    }

    public String getWhiteSpace() {
        return null;
    }

    public void setWhiteSpace(String str) {
    }

    public String getWidows() {
        return null;
    }

    public void setWidows(String str) {
    }

    public String getWidth() {
        return null;
    }

    public void setWidth(String str) {
    }

    public String getWordSpacing() {
        return null;
    }

    public void setWordSpacing(String str) {
    }

    public String getZIndex() {
        return null;
    }

    public void setZIndex(String str) {
    }
}
