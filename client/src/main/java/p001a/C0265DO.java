package p001a;

import com.hoplon.geometry.Color;

import javax.vecmath.Vector4f;

/* renamed from: a.DO */
/* compiled from: a */
public class C0265DO {
    private C0265DO() {
    }

    /* renamed from: N */
    public static Color m2388N(Color color) {
        float f;
        float f2;
        float f3;
        float f4 = color.x;
        float f5 = color.y;
        float f6 = color.z;
        float min = Math.min(f4, Math.min(f5, f6));
        float max = Math.max(f4, Math.max(f5, f6));
        float f7 = max - min;
        float f8 = (max + min) / 2.0f;
        if (f7 == 0.0f) {
            f3 = 0.0f;
            f2 = 0.0f;
        } else {
            if (((double) f8) <= 0.5d) {
                f = f7 / (min + max);
            } else {
                f = f7 / ((2.0f - max) - min);
            }
            float f9 = (f5 - f6) / (6.0f * f7);
            float f10 = (f6 - f4) / (6.0f * f7);
            float f11 = (f4 - f5) / (f7 * 6.0f);
            if (f4 == max) {
                if (f5 < f6) {
                    f9 += 1.0f;
                }
                f2 = f9;
            } else if (f5 == max) {
                f2 = f10 + 0.33333334f;
            } else {
                f2 = f11 + 0.6666667f;
            }
            if (f2 < 0.0f) {
                f2 += 1.0f;
            }
            if (f2 > 1.0f) {
                f2 -= 1.0f;
                f3 = f;
            } else {
                f3 = f;
            }
        }
        color.x = f2;
        color.y = f3;
        color.z = f8;
        return color;
    }

    /* renamed from: O */
    public static Color m2389O(Color color) {
        return m2388N(new Color((Vector4f) color));
    }

    /* renamed from: P */
    public static Color m2390P(Color color) {
        float f;
        float e;
        float e2;
        float f2;
        float f3 = color.x;
        float f4 = color.y;
        float f5 = color.z;
        if (f4 == 0.0f) {
            e2 = f5;
            f2 = f5;
            e = f5;
        } else {
            if (((double) f5) < 0.5d) {
                f = (f4 + 1.0f) * f5;
            } else {
                f = (f5 + f4) - (f4 * f5);
            }
            float f6 = (f5 * 2.0f) - f;
            e = m2392e(f6, f, f3 + 0.33333334f);
            float e3 = m2392e(f6, f, f3);
            e2 = m2392e(f6, f, f3 - 0.33333334f);
            f2 = e3;
        }
        color.x = e;
        color.y = f2;
        color.z = e2;
        return color;
    }

    /* renamed from: Q */
    public static Color m2391Q(Color color) {
        return m2390P(new Color((Vector4f) color));
    }

    /* renamed from: e */
    private static float m2392e(float f, float f2, float f3) {
        float f4;
        if (f3 < 0.0f) {
            f4 = f3 + 1.0f;
        } else {
            f4 = f3;
        }
        if (f4 > 1.0f) {
            f4 -= 1.0f;
        }
        if (((double) f4) < 0.16666666666666666d) {
            return f + (f4 * (f2 - f) * 6.0f);
        }
        if (((double) f4) < 0.5d) {
            return f2;
        }
        if (((double) f4) >= 0.6666666666666666d) {
            return f;
        }
        return f + ((0.6666667f - f4) * (f2 - f) * 6.0f);
    }
}
