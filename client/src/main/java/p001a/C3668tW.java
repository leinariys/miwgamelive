package p001a;

import game.geometry.Matrix3fWrap;
import game.geometry.TransformWrap;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.ship.categories.Bomber;
import game.script.ship.categories.Explorer;
import game.script.ship.categories.Fighter;
import game.script.ship.categories.Freighter;
import logic.WrapRunnable;
import taikodom.render.scene.SceneObject;

import java.util.Random;

/* renamed from: a.tW */
/* compiled from: a */
public class C3668tW extends WrapRunnable {

    boolean api;
    float buL;
    float buM;
    float buN;
    float buO;
    float buP;
    float buQ;
    float buR;
    /* renamed from: P */
    private Player f9257P;
    private long buF;
    private float buG;
    private float buH;
    private float buI;
    private float buJ;
    private float buK;
    private long buS;

    /* renamed from: bz */
    private SceneObject f9258bz;

    public C3668tW(Player aku, SceneObject sceneObject) {
        int i = 1;
        this.buF = 0;
        this.buG = 0.0f;
        this.buH = 0.0f;
        this.buL = 120.0f;
        this.buM = 125.0f;
        this.buN = 1.0f;
        this.buO = 2.0f;
        this.buP = 0.05f;
        this.buQ = 1.0f;
        this.buR = 2.0f;
        this.api = false;
        this.buF = System.currentTimeMillis();
        i = new Random().nextInt(1) == 1 ? -1 : i;
        this.buG = 0.0f;
        this.buH = (float) (i * 90);
        this.f9257P = aku;
        this.f9258bz = sceneObject;
        Ship bQx = aku.bQx();
        if (bQx == null) {
            return;
        }
        if (bQx instanceof Fighter) {
            this.buQ = 1.0f;
            this.buL = 7.0f;
            this.buM = 5.0f;
        } else if (bQx instanceof Bomber) {
            this.buQ = 0.5f;
            this.buL = 4.0f;
            this.buM = 8.0f;
        } else if (bQx instanceof Explorer) {
            this.buQ = 0.8f;
            this.buL = 2.5f;
            this.buM = 5.0f;
        } else if (bQx instanceof Freighter) {
            this.buQ = 0.2f;
            this.buL = 1.0f;
            this.buM = 2.0f;
        }
    }

    public void run() {
        if (this.f9258bz != null) {
            float currentTimeMillis = ((float) (System.currentTimeMillis() - this.buF)) / 1000.0f;
            this.buF = System.currentTimeMillis();
            if (this.buI < 0.0f) {
            }
            if (this.buJ < 0.0f) {
            }
            this.buG += this.buL * currentTimeMillis;
            if (this.buG > 360.0f) {
                this.buG = 0.0f;
            }
            this.buH += this.buM * currentTimeMillis;
            if (this.buH > 360.0f) {
                this.buH = 0.0f;
            }
            this.buK = (currentTimeMillis * 180.0f) + this.buK;
            float sin = (float) Math.sin(((double) this.buG) * 0.017453292519943295d);
            float cos = (float) Math.cos(((double) this.buH) * 0.017453292519943295d);
            this.buJ = sin;
            this.buI = cos;
            TransformWrap transform = this.f9258bz.getTransform();
            Matrix3fWrap ajd = new Matrix3fWrap();
            ajd.setIdentity();
            Matrix3fWrap ajd2 = new Matrix3fWrap();
            ajd2.setIdentity();
            transform.position.y += (double) (((float) Math.cos(((double) this.buK) * 0.017453292519943295d)) * this.buP);
            ajd.rotateZ(cos * 0.017453292f * this.buO * this.buQ);
            ajd2.rotateX(sin * 0.017453292f * this.buN * this.buQ);
            ajd.mul(ajd2);
            transform.orientation.set(ajd);
            this.f9258bz.setTransform(transform);
            if (this.api) {
                float currentTimeMillis2 = ((float) (System.currentTimeMillis() - this.buS)) / 1000.0f;
                if (currentTimeMillis2 > this.buR) {
                    super.cancel();
                    return;
                }
                float f = 1.0f - (currentTimeMillis2 / this.buR);
                this.buO *= f;
                this.buN *= f;
                this.buP = f * this.buP;
            }
        }
    }

    public void aiR() {
        this.f9258bz.resetRotation();
        super.cancel();
    }

    public void cancel() {
        this.api = true;
        this.buS = System.currentTimeMillis();
    }
}
