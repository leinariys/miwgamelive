package p001a;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.hR */
/* compiled from: a */
public @interface C2602hR {
    /* renamed from: zf */
    String mo19255zf() default "<NO_NAME_COMMAND>";

    /* renamed from: zg */
    String mo19256zg() default "<NO_CATEGORY>";
}
