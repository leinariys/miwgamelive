package p001a;

/* renamed from: a.ayQ */
/* compiled from: a */
public class ayQ {
    private boolean dirty;
    private Class<?> gWc;
    private Object gWd;

    ayQ(Class<?> cls, Object obj) {
        this.gWc = cls;
        this.gWd = obj;
        this.dirty = true;
    }

    ayQ(Class<?> cls, Object obj, boolean z) {
        this.gWc = cls;
        this.gWd = obj;
        this.dirty = z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ayQ ayq = (ayQ) obj;
        if (this.gWc != ayq.gWc || ((this.gWd != null || ayq.gWd != null) && !this.gWd.equals(ayq.gWd))) {
            return false;
        }
        return true;
    }

    public Object clone() {
        return new ayQ(this.gWc, this.gWd, this.dirty);
    }

    public Class<?> getType() {
        return this.gWc;
    }

    public Object getValue() {
        return this.gWd;
    }

    public void setValue(Object obj) {
        if (obj.getClass() != this.gWc) {
            throw new Exception("New value must be of the same type");
        }
        this.gWd = obj;
        this.dirty = true;
    }

    public final boolean isDirty() {
        return this.dirty;
    }

    public void setDirty(boolean z) {
        this.dirty = z;
    }

    public String toString() {
        return String.valueOf(this.gWd);
    }
}
