package p001a;

import logic.ui.item.ListJ;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

/* renamed from: a.c */
/* compiled from: a */
public class C2107c {

    /* renamed from: m */
    private final C2108a f5946m;

    public C2107c(C2108a aVar) {
        this.f5946m = aVar;
    }

    /* renamed from: a */
    public static void m28141a(List<String[]> list, int i) {
        Collections.sort(list, new C2113c(i));
    }

    /* renamed from: b */
    public static void m28143b(List<String[]> list, int i) {
        Collections.sort(list, new C2115e(i));
    }

    /* renamed from: c */
    public static void m28144c(List<String[]> list, int i) {
        Collections.sort(list, new C2116f(i));
    }

    /* renamed from: a */
    public static void m28142a(List<String[]> list, int... iArr) {
        Collections.sort(list, new C2114d(iArr));
    }

    /* renamed from: a */
    public static void m28139a(OutputStream outputStream, String[] strArr, List<String[]> list) {
        m28140a(new PrintWriter(outputStream), strArr, list);
    }

    /* renamed from: a */
    public static void m28140a(PrintWriter printWriter, String[] strArr, List<String[]> list) {
        if (!list.isEmpty()) {
            printWriter.print(new C2107c(new C2110b(strArr, list)).toString());
            printWriter.flush();
        }
    }

    public String toString() {
        int i;
        HashMap hashMap = new HashMap();
        String[] cCH = this.f5946m.cCH();
        int length = cCH.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            hashMap.put(Integer.valueOf(i3), Integer.valueOf(cCH[i2].length()));
            i2++;
            i3++;
        }
        Iterator<C2108a.C2109a> cCI = this.f5946m.cCI();
        while (cCI.hasNext()) {
            int i4 = 0;
            for (String str : cCI.next().aNU()) {
                try {
                    i = ((Integer) hashMap.get(Integer.valueOf(i4))).intValue();
                } catch (Exception e) {
                    i = 0;
                }
                if (str != null && str.length() > i) {
                    i = str.length();
                }
                hashMap.put(Integer.valueOf(i4), Integer.valueOf(i));
                i4++;
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("+");
        for (Integer intValue : hashMap.values()) {
            sb.append("-").append(m28138a("-", intValue.intValue())).append("-+");
        }
        sb.append("\n");
        sb.append("|");
        String[] cCH2 = this.f5946m.cCH();
        int length2 = cCH2.length;
        int i5 = 0;
        int i6 = 0;
        while (i5 < length2) {
            sb.append(" ").append(String.format("%-" + hashMap.get(Integer.valueOf(i6)) + "s", new Object[]{cCH2[i5]})).append(" |");
            i5++;
            i6++;
        }
        sb.append("\n");
        sb.append("+");
        for (Integer intValue2 : hashMap.values()) {
            sb.append("-").append(m28138a("-", intValue2.intValue())).append("-+");
        }
        sb.append("\n");
        Iterator<C2108a.C2109a> cCI2 = this.f5946m.cCI();
        while (cCI2.hasNext()) {
            C2108a.C2109a next = cCI2.next();
            sb.append("|");
            String[] aNU = next.aNU();
            int length3 = aNU.length;
            int i7 = 0;
            int i8 = 0;
            while (i7 < length3) {
                sb.append(" ").append(String.format(next.mo17497A(i8, ((Integer) hashMap.get(Integer.valueOf(i8))).intValue()), new Object[]{aNU[i7]})).append(" |");
                i7++;
                i8++;
            }
            sb.append("\n");
        }
        sb.append("+");
        for (Integer intValue3 : hashMap.values()) {
            sb.append("-").append(m28138a("-", intValue3.intValue())).append("-+");
        }
        sb.append("\n");
        return sb.toString();
    }

    /* renamed from: a */
    private String m28138a(String str, int i) {
        String str2 = "";
        for (int i2 = 0; i2 < i; i2++) {
            str2 = String.valueOf(str2) + str;
        }
        return str2;
    }

    /* renamed from: a.c$a */
    public interface C2108a {

        String[] cCH();

        Iterator<C2109a> cCI();

        /* renamed from: a.c$a$a */
        public interface C2109a {
            /* renamed from: A */
            String mo17497A(int i, int i2);

            String[] aNU();
        }
    }

    /* renamed from: a.c$c */
    /* compiled from: a */
    static class C2113c implements Comparator<String[]> {
        private final /* synthetic */ int hHt;

        C2113c(int i) {
            this.hHt = i;
        }

        /* renamed from: a */
        public int compare(String[] strArr, String[] strArr2) {
            double parseDouble = Double.parseDouble(strArr[this.hHt].replace(',', '.'));
            double parseDouble2 = Double.parseDouble(strArr2[this.hHt].replace(',', '.'));
            if (parseDouble == parseDouble2) {
                return 0;
            }
            return parseDouble > parseDouble2 ? -1 : 1;
        }
    }

    /* renamed from: a.c$e */
    /* compiled from: a */
    static class C2115e implements Comparator<String[]> {
        private final /* synthetic */ int hHt;

        C2115e(int i) {
            this.hHt = i;
        }

        /* renamed from: a */
        public int compare(String[] strArr, String[] strArr2) {
            float parseFloat = Float.parseFloat(strArr[this.hHt].replace(',', '.'));
            float parseFloat2 = Float.parseFloat(strArr2[this.hHt].replace(',', '.'));
            if (parseFloat == parseFloat2) {
                return 0;
            }
            return parseFloat > parseFloat2 ? -1 : 1;
        }
    }

    /* renamed from: a.c$f */
    /* compiled from: a */
    static class C2116f implements Comparator<String[]> {
        private final /* synthetic */ int hHt;

        C2116f(int i) {
            this.hHt = i;
        }

        /* renamed from: a */
        public int compare(String[] strArr, String[] strArr2) {
            long parseLong = Long.parseLong(strArr[this.hHt]);
            long parseLong2 = Long.parseLong(strArr2[this.hHt]);
            if (parseLong == parseLong2) {
                return 0;
            }
            return parseLong > parseLong2 ? -1 : 1;
        }
    }

    /* renamed from: a.c$d */
    /* compiled from: a */
    static class C2114d implements Comparator<String[]> {
        private final /* synthetic */ int[] hHx;

        C2114d(int[] iArr) {
            this.hHx = iArr;
        }

        /* renamed from: a */
        public int compare(String[] strArr, String[] strArr2) {
            int i = 0;
            int i2 = 0;
            while (true) {
                int i3 = i;
                if (i2 >= this.hHx.length) {
                    return i3;
                }
                i = (strArr[this.hHx[i2]].compareToIgnoreCase(strArr2[this.hHx[i2]]) * (10 - i2) * 10000) + i3;
                i2++;
            }
        }
    }

    /* renamed from: a.c$b */
    /* compiled from: a */
    class C2110b implements C2108a {
        private final /* synthetic */ String[] hHD;
        private final /* synthetic */ ListJ hHE;

        C2110b(String[] strArr, ListJ list) {
            this.hHD = strArr;
            this.hHE = list;
        }

        public String[] cCH() {
            return this.hHD;
        }

        public Iterator<C2108a.C2109a> cCI() {
            return new C2111a(this.hHE.iterator());
        }

        /* renamed from: a.c$b$a */
        class C2111a implements Iterator<C2108a.C2109a> {
            private final /* synthetic */ Iterator hfP;

            C2111a(Iterator it) {
                this.hfP = it;
            }

            public boolean hasNext() {
                return this.hfP.hasNext();
            }

            /* renamed from: dfv */
            public C2108a.C2109a next() {
                return new C2112a((String[]) this.hfP.next());
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }

            /* renamed from: a.c$b$a$a */
            class C2112a implements C2108a.C2109a {
                private final /* synthetic */ String[] fqI;

                C2112a(String[] strArr) {
                    this.fqI = strArr;
                }

                public String[] aNU() {
                    return this.fqI;
                }

                /* renamed from: A */
                public String mo17497A(int i, int i2) {
                    try {
                        Double.parseDouble(this.fqI[i].replace(',', '.'));
                        return "%" + i2 + "s";
                    } catch (Throwable th) {
                        return "%-" + i2 + "s";
                    }
                }
            }
        }
    }
}
