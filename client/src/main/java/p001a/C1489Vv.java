package p001a;

import game.network.channel.client.ClientConnect;
import logic.bbb.aDR;
import logic.res.html.C6663arL;
import logic.res.html.MessageContainer;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* renamed from: a.Vv */
/* compiled from: a */
public class C1489Vv {
    private Map<ClientConnect, aDR> esV = Collections.synchronizedMap(new HashMap());
    private Map<C3582se, aDR> esW = Collections.synchronizedMap(new HashMap());

    /* renamed from: a */
    public aDR mo6438a(aDR adr, C3582se seVar, boolean z) {
        adr.mo8399d(seVar);
        this.esW.put(seVar, adr);
        adr.hge = z;
        return adr;
    }

    /* renamed from: f */
    public aDR mo6442f(ClientConnect bVar) {
        aDR adr = null;
        if (bVar != null && (adr = this.esV.get(bVar)) == null) {
            synchronized (this) {
                if (adr == null) {
                    adr = new aDR(bVar, (C6663arL) null);
                    this.esV.put(bVar, adr);
                }
            }
        }
        return adr;
    }

    /* renamed from: g */
    public aDR mo6443g(ClientConnect bVar) {
        aDR remove = this.esV.remove(bVar);
        if (remove != null) {
            remove.cleanUp();
            this.esW.remove(remove.hDb);
        }
        return remove;
    }

    /* renamed from: d */
    public aDR mo6441d(C3582se seVar) {
        return this.esW.get(seVar);
    }

    public int bAZ() {
        return this.esV.size();
    }

    public Set<aDR> bBa() {
        if (this.esV.isEmpty()) {
            return MessageContainer.dgI();
        }
        return MessageContainer.m16003v(this.esV.values());
    }
}
