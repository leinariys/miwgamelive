package p001a;

import logic.sql.C5878acG;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.zo */
/* compiled from: a */
public abstract class C4124zo {
    /* renamed from: Ka */
    public abstract int mo6130Ka();

    /* renamed from: Kb */
    public abstract int mo6131Kb();

    /* renamed from: Kc */
    public abstract int mo6132Kc();

    /* renamed from: Kd */
    public abstract int mo6133Kd();

    /* renamed from: Ke */
    public abstract int mo6134Ke();

    /* renamed from: Ki */
    public abstract boolean mo6135Ki();

    /* renamed from: Kj */
    public abstract boolean mo6136Kj();

    /* renamed from: a */
    public abstract void mo6137a(ObjectOutput objectOutput, double d);

    public abstract double arQ();

    public abstract double arR();

    public abstract double arS();

    /* renamed from: du */
    public abstract double mo6141du(long j);

    /* renamed from: g */
    public abstract double mo6142g(ObjectInput objectInput);

    /* renamed from: h */
    public abstract long mo6144h(double d);

    public String getName() {
        return getClass().getSimpleName();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getName()).append(": ");
        if (mo6135Ki()) {
            sb.append("1+");
        }
        sb.append(mo6134Ke()).append('+');
        sb.append(mo6133Kd());
        sb.append('=').append(mo6132Kc());
        sb.append(" Exp:[").append(mo6131Kb()).append(C5878acG.dBB).append(mo6130Ka()).append("] ");
        int Kc = mo6132Kc();
        if (mo6135Ki()) {
            sb.append('S');
            Kc--;
            if (Kc % 4 == 0) {
                sb.append(' ');
            }
        }
        int i = Kc;
        for (int i2 = 0; i2 < mo6134Ke(); i2++) {
            sb.append('E');
            i--;
            if (i % 4 == 0) {
                sb.append(' ');
            }
        }
        for (int i3 = 0; i3 < mo6133Kd(); i3++) {
            sb.append('M');
            i--;
            if (i % 4 == 0 && i > 0) {
                sb.append(' ');
            }
        }
        return sb.toString();
    }
}
