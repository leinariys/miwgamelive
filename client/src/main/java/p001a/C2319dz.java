package p001a;

import game.script.npcchat.PlayerSpeech;
import game.script.npcchat.SpeechAction;
import game.script.npcchat.actions.OpenMissionBriefingSpeechAction;

/* renamed from: a.dz */
/* compiled from: a */
public abstract class C2319dz {
    /* renamed from: a */
    public static boolean m29372a(PlayerSpeech av) {
        for (SpeechAction zkVar : av.avP()) {
            if (zkVar instanceof OpenMissionBriefingSpeechAction) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x0010  */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m29373b(PlayerSpeech r3) {
        /*
            java.util.List r0 = r3.avP()
            java.util.Iterator r1 = r0.iterator()
        L_0x0008:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x0010
            r0 = 0
        L_0x000f:
            return r0
        L_0x0010:
            java.lang.Object r0 = r1.next()
            a.zk r0 = (p001a.C4120zk) r0
            boolean r2 = r0 instanceof p001a.C1724ZU
            if (r2 != 0) goto L_0x001e
            boolean r0 = r0 instanceof p001a.C6169ahl
            if (r0 == 0) goto L_0x0008
        L_0x001e:
            r0 = 1
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C2319dz.m29373b(a.Av):boolean");
    }

    /* renamed from: c */
    public static boolean m29374c(PlayerSpeech av) {
        return av.avN().isEmpty();
    }
}
