package p001a;

import logic.baa.C1616Xf;

import java.util.Comparator;

/* renamed from: a.NI */
/* compiled from: a */
class C0906NI implements Comparator<C1616Xf> {
    final /* synthetic */ azX dHG;

    C0906NI(azX azx) {
        this.dHG = azx;
    }

    /* renamed from: a */
    public int compare(C1616Xf xf, C1616Xf xf2) {
        long cqo = xf.bFf().getObjectId().getId() - xf2.bFf().getObjectId().getId();
        if (cqo > 0) {
            return 1;
        }
        return cqo < 0 ? -1 : 0;
    }
}
