package p001a;

import game.geometry.Vec3d;
import gnu.trove.THashMap;

/* renamed from: a.aJL */
/* compiled from: a */
public class aJL<T> extends THashMap<Vec3d, T> {
    private final Vec3d igA = new Vec3d();

    /* renamed from: n */
    public T mo9468n(double d, double d2, double d3) {
        this.igA.set(d, d2, d3);
        return aJL.super.get(this.igA);
    }
}
