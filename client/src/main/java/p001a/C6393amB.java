package p001a;

import game.engine.DataGameEvent;
import game.network.message.externalizable.C3213pH;
import logic.data.mbean.C0677Jd;
import logic.thred.LogPrinter;
import logic.thred.PoolThread;
import logic.thred.ThreadWrapper;
import logic.ui.item.ListJ;
import org.apache.commons.logging.Log;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: a.amB  reason: case insensitive filesystem */
/* compiled from: a */
public class C6393amB implements C2844kw {
    /* access modifiers changed from: private */
    public static final Log logger = LogPrinter.setClass(C6393amB.class);
    /* access modifiers changed from: private */
    public static int iGJ = 500;
    /* access modifiers changed from: private */
    public boolean disposed = false;
    public long iGO = ((long) iGJ);
    public boolean iGP;
    TreeSet<C0677Jd> iGK = new TreeSet<>(new C6398amG(this));
    TreeSet<C0677Jd> iGL = new TreeSet<>(new C6395amD(this));
    private DataGameEvent aGA;
    private LinkedBlockingQueue<C0677Jd> iGH = new LinkedBlockingQueue<>();
    private LinkedBlockingQueue<C0677Jd> iGI = new LinkedBlockingQueue<>();
    private ThreadWrapper iGM;
    private ThreadWrapper iGN;

    public C6393amB(DataGameEvent jz) {
        this.aGA = jz;
    }

    /* renamed from: a */
    public void mo14758a(C0677Jd jd) {
        this.iGH.add(jd);
    }

    /* renamed from: b */
    public void mo14759b(C0677Jd jd) {
        this.iGI.add(jd);
    }

    /* access modifiers changed from: protected */
    public void drk() {
        ArrayList arrayList = new ArrayList();
        this.iGH.drainTo(arrayList);
        this.iGL.addAll(arrayList);
        arrayList.clear();
        long currentTimeMillis = System.currentTimeMillis();
        Iterator<C0677Jd> it = this.iGL.iterator();
        while (it.hasNext()) {
            C0677Jd next = it.next();
            if (next.aXB() >= currentTimeMillis) {
                break;
            }
            arrayList.add(next);
            it.remove();
        }
        if (arrayList.size() > 0) {
            HashMap hashMap = new HashMap();
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                C0677Jd jd = (C0677Jd) it2.next();
                C1875af afVar = (C1875af) jd.f3984Uk.bFf();
                afVar.mo13222dm();
                try {
                    if (jd.aXu()) {
                        if (afVar.mo13219di() < jd.aBt()) {
                            C6014aem[] dp = afVar.mo13225dp();
                            afVar.mo13202a(jd.aBt());
                            for (C6014aem aem : dp) {
                                ListJ list = (ListJ) hashMap.get(aem.eIu);
                                if (list == null) {
                                    list = new LinkedList();
                                    hashMap.put(aem.eIu, list);
                                }
                                list.add(new C3213pH(jd));
                            }
                            this.aGA.bGU().mo15541C(hashMap);
                            hashMap.clear();
                        }
                        jd.aXz();
                    }
                } finally {
                    afVar.mo13220dk();
                }
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public void bGp() {
        ArrayList arrayList = new ArrayList();
        this.iGI.drainTo(arrayList);
        this.iGK.addAll(arrayList);
        arrayList.clear();
        long currentTimeMillis = System.currentTimeMillis();
        Iterator<C0677Jd> it = this.iGK.iterator();
        while (it.hasNext()) {
            C0677Jd next = it.next();
            if (next.aXC() >= currentTimeMillis) {
                break;
            }
            arrayList.add(next);
            it.remove();
        }
        if (arrayList.size() > 0) {
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                C0677Jd jd = (C0677Jd) it2.next();
                C1875af afVar = (C1875af) jd.f3984Uk.bFf();
                afVar.mo13222dm();
                try {
                    if (afVar.cVe() < jd.aBt()) {
                        this.aGA.bGT().mo5238i(afVar);
                    }
                    afVar.mo13220dk();
                    jd.aXy();
                } catch (Throwable th) {
                    afVar.mo13220dk();
                    throw th;
                }
            }
        }
    }

    public void start() {
        this.iGM = new ThreadWrapper(new C1940b(), "Timed Persit");
        this.iGM.setDaemon(true);
        this.iGM.start();
        this.iGN = new ThreadWrapper(new C1939a(), "Timed Propagate");
        this.iGN.setDaemon(true);
        this.iGN.start();
    }

    /* renamed from: HY */
    public void mo14757HY() {
        this.disposed = true;
        m23843lQ(2000);
        if (this.iGN.isAlive()) {
            this.iGN.interrupt();
        }
        if (this.iGM.isAlive()) {
            this.iGM.interrupt();
        }
        m23843lQ(2000);
        if (this.iGN.isAlive()) {
            logger.error("Had to stop the propagate thread!");
            this.iGN.stop();
        }
        if (this.iGM.isAlive()) {
            logger.error("Had to stop the persist thread!");
            this.iGM.stop();
        }
    }

    /* renamed from: lQ */
    private void m23843lQ(long j) {
        if (this.iGN.isAlive()) {
            try {
                this.iGN.join(j);
            } catch (InterruptedException e) {
                logger.error(e);
            }
        }
        if (this.iGM.isAlive()) {
            try {
                this.iGM.join(j);
            } catch (InterruptedException e2) {
                logger.error(e2);
            }
        }
    }

    /* renamed from: a.amB$c */
    /* compiled from: a */
    public static abstract class C1941c implements Comparator<C0677Jd> {
        /* renamed from: b */
        public int mo14765b(C0677Jd jd, C0677Jd jd2) {
            long cqo = jd.f3984Uk.bFf().getObjectId().getId();
            long cqo2 = jd.f3984Uk.bFf().getObjectId().getId();
            if (cqo < cqo2) {
                return -1;
            }
            if (cqo > cqo2) {
                return 1;
            }
            long aBt = jd.aBt();
            long aBt2 = jd.aBt();
            if (aBt < aBt2) {
                return 1;
            }
            if (aBt <= aBt2) {
                return 0;
            }
            return -1;
        }
    }

    /* renamed from: a.amB$b */
    /* compiled from: a */
    class C1940b implements Runnable {
        C1940b() {
        }

        public void run() {
            while (!C6393amB.this.disposed) {
                PoolThread.sleep((long) C6393amB.iGJ);
                try {
                    C6393amB.this.bGp();
                } catch (Exception e) {
                    C6393amB.logger.error(e);
                }
            }
        }
    }

    /* renamed from: a.amB$a */
    class C1939a implements Runnable {
        C1939a() {
        }

        public void run() {
            while (!C6393amB.this.disposed) {
                PoolThread.sleep(500);
                try {
                    C6393amB.this.drk();
                } catch (Exception e) {
                    C6393amB.logger.error(e);
                }
            }
        }
    }
}
