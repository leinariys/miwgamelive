package p001a;

import game.network.manager.C6546aoy;

@C6546aoy
/* renamed from: a.aEp  reason: case insensitive filesystem */
/* compiled from: a */
public class C5315aEp extends Exception {


    public C5315aEp() {
    }

    public C5315aEp(String str, Throwable th) {
        super(str, th);
    }

    public C5315aEp(String str) {
        super(str);
    }

    public C5315aEp(Throwable th) {
        super(th);
    }
}
