package p001a;

import logic.res.XmlNode;
import logic.res.html.MessageContainer;
import logic.ui.item.ListJ;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: a.aIX */
/* compiled from: a */
public class aIX {
    public static String ies = "null";
    private aKZ iet = new aKZ(1);
    private Set<String> ieu = MessageContainer.dgI();
    private ListJ<XmlNode> iev = MessageContainer.init();

    /* renamed from: g */
    public void mo9361g(Object obj, String str) {
        this.iev.clear();
        this.ieu.clear();
        try {
            m15413i(obj, str);
            for (String nb : this.ieu) {
                Object nb2 = this.iet.mo9754nb(nb);
                String aM = this.iet.mo9748aM(nb2);
                XmlNode h = m15412h(nb2, "pobject");
                if (aM.equals(str) && obj.getClass().getName().equals(nb2.getClass().getName())) {
                    h.setTagName("field");
                    h.addAttribute("fieldName", aM);
                }
                this.iev.add(h);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* renamed from: l */
    public String mo9362l(XmlNode agy) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(byteArrayOutputStream, "UTF-8"));
            agy.buildStringFromNode(printWriter);
            printWriter.flush();
            return byteArrayOutputStream.toString();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: h */
    private XmlNode m15412h(Object obj, String str) {
        if (C0365Es.m3049u(obj.getClass())) {
            XmlNode agy = new XmlNode(str);
            agy.addAttribute("id", this.iet.mo9747aL(obj));
            agy.addAttribute("class", obj.getClass().getName());
            for (Object next : (ListJ) obj) {
                XmlNode mz = agy.addChildrenTag("item");
                if (next == null) {
                    mz.addAttribute("value", ies);
                } else if (C0365Es.m3051w(next.getClass())) {
                    mz.addAttribute("value", next.toString());
                } else {
                    mz.addAttribute("id", this.iet.mo9747aL(next));
                }
            }
            return agy;
        } else if (C0365Es.m3050v(obj.getClass())) {
            XmlNode agy2 = new XmlNode(str);
            agy2.addAttribute("id", this.iet.mo9747aL(obj));
            agy2.addAttribute("class", obj.getClass().getName());
            Map map = (Map) obj;
            Iterator it = map.keySet().iterator();
            Object obj2 = null;
            Object obj3 = null;
            while (it.hasNext()) {
                obj3 = it.next();
                XmlNode mz2 = agy2.addChildrenTag("item");
                if (obj3 == null) {
                    mz2.addAttribute("key", (String) null);
                } else if (C0365Es.m3051w(obj3.getClass())) {
                    mz2.addAttribute("key", obj3.toString());
                } else {
                    mz2.addAttribute("keyID", this.iet.mo9747aL(obj3));
                }
                obj2 = map.get(obj3);
                if (obj2 == null) {
                    mz2.addAttribute("value", (String) null);
                } else if (C0365Es.m3051w(obj2.getClass())) {
                    mz2.addAttribute("value", obj2.toString());
                } else {
                    mz2.addAttribute("valueID", this.iet.mo9747aL(obj2));
                }
            }
            if (obj3 != null) {
                agy2.addAttribute("keyType", obj3.getClass().getName());
            }
            if (obj2 != null) {
                agy2.addAttribute("valueType", obj2.getClass().getName());
            }
            return agy2;
        } else if (obj.getClass().isArray()) {
            XmlNode agy3 = new XmlNode(str);
            agy3.addAttribute("id", this.iet.mo9747aL(obj));
            agy3.addAttribute("class", obj.getClass().getName());
            int length = Array.getLength(obj);
            for (int i = 0; i < length; i++) {
                XmlNode mz3 = agy3.addChildrenTag("item");
                if (C0365Es.m3051w(obj.getClass().getComponentType())) {
                    mz3.addAttribute("value", Array.get(obj, i).toString());
                } else {
                    mz3.addAttribute("id", this.iet.mo9747aL(Array.get(obj, i)));
                }
            }
            return agy3;
        } else if (obj.getClass().equals(String.class)) {
            XmlNode agy4 = new XmlNode(str);
            agy4.addAttribute("id", this.iet.mo9747aL(obj));
            agy4.addAttribute("class", obj.getClass().getName());
            XmlNode mz4 = agy4.addChildrenTag("value");
            String obj4 = obj.toString();
            StringBuffer stringBuffer = new StringBuffer();
            for (char c : obj4.toCharArray()) {
                int i2 = (c + 256) % 256;
                if (c == '<' || c == '>' || c == 10 || c == '&' || i2 <= 32 || i2 > 126 || c == '\\') {
                    stringBuffer.append("\\u");
                    String hexString = Integer.toHexString(i2);
                    for (int i3 = 0; i3 < 4 - hexString.length(); i3++) {
                        stringBuffer.append("0");
                    }
                    stringBuffer.append(hexString);
                } else {
                    stringBuffer.append(c);
                }
            }
            mz4.setText(stringBuffer.toString());
            return agy4;
        } else if (obj.getClass().equals(Date.class)) {
            XmlNode agy5 = new XmlNode(str);
            agy5.addAttribute("id", this.iet.mo9747aL(obj));
            agy5.addAttribute("class", obj.getClass().getName());
            agy5.addChildrenTag("value").setText(String.valueOf(((Date) obj).getTime()));
            return agy5;
        } else {
            ListJ<Field> Y = m15411Y(obj.getClass());
            XmlNode agy6 = new XmlNode(str);
            agy6.addAttribute("id", this.iet.mo9747aL(obj));
            agy6.addAttribute("class", obj.getClass().getName());
            for (Field next2 : Y) {
                next2.setAccessible(true);
                if (!Modifier.isTransient(next2.getModifiers()) && !Modifier.isStatic(next2.getModifiers()) && next2.get(obj) != null) {
                    XmlNode mz5 = agy6.addChildrenTag(next2.getName());
                    if (next2.getType().isEnum()) {
                        Enum enumR = (Enum) next2.get(obj);
                        if (enumR != null) {
                            mz5.addAttribute("enum", "yes");
                            mz5.addAttribute("class", next2.getType().getName());
                            mz5.addAttribute("member", enumR.name());
                        }
                    } else if (C0365Es.m3051w(next2.getType())) {
                        String valueOf = String.valueOf(next2.get(obj));
                        valueOf.replace("\"", "&quot");
                        valueOf.replace("'", "&apos");
                        mz5.setText(valueOf);
                    } else {
                        mz5.addAttribute("id", this.iet.mo9747aL(next2.get(obj)));
                    }
                }
            }
            return agy6;
        }
    }

    /* renamed from: i */
    private void m15413i(Object obj, String str) {
        Object obj2;
        if (obj != null && this.iet.mo9747aL(obj) == null) {
            this.ieu.add(this.iet.mo9753j(obj, str));
            if (C0365Es.m3050v(obj.getClass())) {
                Map map = (Map) obj;
                for (Object next : map.keySet()) {
                    if (next != null && !C0365Es.m3051w(next.getClass())) {
                        try {
                            m15413i(next, "item");
                        } catch (Exception e) {
                            throw new Exception("Map key: " + next.toString() + " (" + next.getClass().getName() + ")", e);
                        }
                    }
                    Object obj3 = map.get(next);
                    if (obj3 != null && !C0365Es.m3051w(obj3.getClass())) {
                        try {
                            m15413i(obj3, "item");
                        } catch (Exception e2) {
                            throw new Exception("Map value: " + obj3.toString() + " (" + obj3.getClass().getName() + ")", e2);
                        }
                    }
                }
            } else if (C0365Es.m3049u(obj.getClass())) {
                for (Object next2 : (ListJ) obj) {
                    if (next2 != null && !C0365Es.m3051w(next2.getClass())) {
                        try {
                            m15413i(next2, "item");
                        } catch (Exception e3) {
                            throw new Exception("List item: " + next2.toString() + " (" + next2.getClass().getName() + ")", e3);
                        }
                    }
                }
            } else if (obj.getClass().isArray()) {
                if (!C0365Es.m3051w(obj.getClass().getComponentType())) {
                    int length = Array.getLength(obj);
                    int i = 0;
                    while (i < length) {
                        Object obj4 = Array.get(obj, i);
                        try {
                            m15413i(obj4, "item");
                            i++;
                        } catch (Exception e4) {
                            throw new Exception("Array item: " + obj4.toString() + " (" + obj4.getClass().getName() + ")", e4);
                        }
                    }
                }
            } else if (!obj.getClass().equals(String.class)) {
                for (Field next3 : m15411Y(obj.getClass())) {
                    next3.setAccessible(true);
                    if (!next3.getType().isEnum() && !C0365Es.m3051w(next3.getType()) && !Modifier.isTransient(next3.getModifiers()) && !Modifier.isStatic(next3.getModifiers()) && (obj2 = next3.get(obj)) != null) {
                        try {
                            m15413i(obj2, next3.getName());
                        } catch (Exception e5) {
                            throw new Exception("Field: " + obj2.toString() + " (" + obj2.getClass().getName() + ")", e5);
                        }
                    }
                }
            }
        }
    }

    public ListJ<XmlNode> dft() {
        return this.iev;
    }

    /* renamed from: Y */
    private ListJ<Field> m15411Y(Class<?> cls) {
        ListJ<Field> dgK = MessageContainer.init();
        for (Class<? super Object> cls2 = cls; cls2 != null; cls2 = cls2.getSuperclass()) {
            for (Field add : cls2.getDeclaredFields()) {
                dgK.add(add);
            }
        }
        return dgK;
    }

    public void dfu() {
        this.iet.clear();
    }

    /* renamed from: aI */
    public String mo9358aI(Object obj) {
        return this.iet.mo9747aL(obj);
    }
}
