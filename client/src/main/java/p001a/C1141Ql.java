package p001a;

import game.network.Transport;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.AttributeKey;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

import java.io.Externalizable;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

/* renamed from: a.Ql */
/* compiled from: a */
public final class C1141Ql implements ProtocolEncoder {
    private static final Object dVo = new AttributeKey(C1141Ql.class, "context");

    public void dispose(IoSession ioSession) {
    }

    public void encode(IoSession ioSession, Object obj, ProtocolEncoderOutput protocolEncoderOutput) {
        C1142a aVar;
        if (OpenSocketMina.m33375a(ioSession).bLH() == OpenSocketMina.C2680a.UP) {
            Transport xuVar = (Transport) obj;
            IoBuffer allocate = IoBuffer.allocate(xuVar.getOutLength() + 4);
            allocate.putInt(xuVar.getOutLength());
            allocate.put(xuVar.getOutBuffer(), 0, xuVar.getOutLength());
            allocate.flip();
            protocolEncoderOutput.write(allocate);
            return;
        }
        C1142a aVar2 = (C1142a) ioSession.getAttribute(dVo);
        if (aVar2 == null) {
            C1142a aVar3 = new C1142a();
            ioSession.setAttribute(dVo, aVar3);
            aVar = aVar3;
        } else {
            aVar = aVar2;
        }
        if (obj instanceof Externalizable) {
            IoBuffer allocate2 = IoBuffer.allocate(64);
            allocate2.setAutoExpand(true);
            allocate2.putInt(0);
            allocate2.putInt(OpenSocketMina.classes.get(obj.getClass()).intValue());
            ((Externalizable) obj).writeExternal(new C5568aOi(allocate2, aVar.encoder));
            int position = allocate2.position();
            allocate2.putInt(allocate2.remaining() - 4);
            allocate2.position(position);
            allocate2.flip();
            protocolEncoderOutput.write(allocate2);
            return;
        }
        throw new RuntimeException("message type " + obj.getClass() + " not supported, message gotta be Externalizable if connection state is not UP");
    }

    /* renamed from: a.Ql$a */
    static final class C1142a {
        /* access modifiers changed from: private */
        public CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();

        C1142a() {
        }
    }
}
