package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;

/* renamed from: a.aOd  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5563aOd {
    /* renamed from: aL */
    void mo10633aL(Vec3d ajr);

    /* renamed from: bI */
    void mo10634bI(Vec3f vec3f);

    /* renamed from: bJ */
    void mo10635bJ(Vec3f vec3f);

    /* renamed from: bK */
    void mo10636bK(Vec3f vec3f);

    /* renamed from: bL */
    void mo10637bL(Vec3f vec3f);

    /* renamed from: bM */
    void mo10638bM(Vec3f vec3f);

    long dmD();

    Vec3d dmE();

    Vec3f dmF();

    Vec3f dmG();

    Vec3f dmH();

    Vec3f dmI();

    Quat4fWrap dmJ();

    Vec3f dmK();

    /* renamed from: n */
    void mo10647n(Quat4fWrap aoy);
}
