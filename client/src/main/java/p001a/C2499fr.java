package p001a;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.fr */
/* compiled from: a */
public @interface C2499fr {

    /* renamed from: JY */
    public static final int f7466JY = 1;

    /* renamed from: JZ */
    public static final int f7467JZ = 2;

    /* renamed from: Ka */
    public static final int f7468Ka = 4;

    /* renamed from: qf */
    String[] mo18855qf() default {};

    /* renamed from: qg */
    String mo18856qg() default "";

    /* renamed from: qh */
    int mo18857qh() default 65535;
}
