package p001a;

/* renamed from: a.Et */
/* compiled from: a */
public class C0366Et extends aOG {
    public static final C5961adl cSc = new C5961adl("JoystickHatEvent");
    private boolean aCJ;
    private int button;

    public C0366Et(int i, boolean z) {
        this.button = i;
        this.aCJ = z;
    }

    public final int getButton() {
        return this.button;
    }

    public final boolean getState() {
        return this.aCJ;
    }

    public final boolean isPressed() {
        return this.aCJ;
    }

    /* renamed from: Fp */
    public C5961adl mo2005Fp() {
        return cSc;
    }
}
