package p001a;

import taikodom.render.textures.DDSLoader;

import java.io.FilterInputStream;
import java.io.InputStream;

/* renamed from: a.apu  reason: case insensitive filesystem */
/* compiled from: a */
public final class C6594apu extends FilterInputStream {
    private final byte[] buf;
    private int count;
    private int pos;

    public C6594apu(InputStream inputStream) {
        this(inputStream, DDSLoader.DDSCAPS2_CUBEMAP_NEGATIVEY);
    }

    public C6594apu(InputStream inputStream, int i) {
        super(inputStream);
        if (i <= 0) {
            throw new IllegalArgumentException("Invalid buffer size");
        }
        this.buf = new byte[i];
    }

    public final int available() {
        return this.in.available() + (this.count - this.pos);
    }

    private final void fill() {
        this.count = this.in.read(this.buf, 0, this.buf.length);
        this.pos = 0;
    }

    public int read() {
        if (this.pos == this.count) {
            fill();
            if (this.pos == this.count) {
                return -1;
            }
        }
        byte[] bArr = this.buf;
        int i = this.pos;
        this.pos = i + 1;
        return bArr[i] & 255;
    }

    public final int read(byte[] bArr, int i, int i2) {
        int i3 = 0;
        if ((i | i2 | (i + i2) | (bArr.length - (i + i2))) < 0) {
            throw new IndexOutOfBoundsException();
        } else if (i2 == 0) {
            return 0;
        } else {
            do {
                int i4 = this.count - this.pos;
                if (i4 == 0) {
                    if (i2 > this.buf.length) {
                        int read = this.in.read(bArr, i + i3, i2 - i3);
                        if (read > 0 || i3 != 0) {
                            return i3;
                        }
                        return read;
                    }
                    fill();
                    i4 = this.count - this.pos;
                    if (i4 == 0) {
                        return -1;
                    }
                }
                int i5 = i2 - i3;
                if (i5 > i4) {
                    System.arraycopy(this.buf, this.pos, bArr, i + i3, i4);
                    i3 += i4;
                    this.pos = this.count;
                } else {
                    System.arraycopy(this.buf, this.pos, bArr, i3 + i, i5);
                    this.pos += i5;
                    return i2;
                }
            } while (this.in.available() > 0);
            return i3;
        }
    }

    public final long skip(long j) {
        if (j <= 0) {
            return 0;
        }
        long j2 = (long) (this.count - this.pos);
        if (j2 <= 0) {
            fill();
            j2 = (long) (this.count - this.pos);
            if (j2 <= 0) {
                return 0;
            }
        }
        long j3 = j2;
        if (j3 < j) {
            j = j3;
        }
        this.pos = (int) (((long) this.pos) + j);
        return j;
    }

    public final void close() {
        this.in.close();
    }
}
