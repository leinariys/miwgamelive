package p001a;

import java.util.Arrays;

/* renamed from: a.uZ */
/* compiled from: a */
public class C3752uZ<T> {

    private float[] byj;
    private Object[] elements;
    private int size;

    public C3752uZ() {
        this(5);
    }

    public C3752uZ(int i) {
        this.elements = new Object[i];
        this.byj = new float[i];
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int mo22423a(int i, T t, float f) {
        float f2 = this.byj[i];
        if (f == f2) {
            return compare(t, elementAt(i));
        }
        if (f > f2) {
            return 1;
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public int compare(T t, T t2) {
        return t.hashCode() - t2.hashCode();
    }

    public T elementAt(int i) {
        return this.elements[i];
    }

    private boolean equals(T t, T t2) {
        return t == t2;
    }

    /* renamed from: dP */
    public int mo22429dP(float f) {
        int i = 0;
        int size2 = size() - 1;
        while (i <= size2) {
            int i2 = ((size2 - i) >> 1) + i;
            float f2 = this.byj[i2];
            if (f > f2) {
                i = i2 + 1;
            } else if (f < f2) {
                size2 = i2 - 1;
            } else {
                while (i2 > i && this.byj[i2 - 1] == f) {
                    i2--;
                }
                return i2;
            }
        }
        return i;
    }

    /* renamed from: a */
    public int mo22422a(float f, T t) {
        int i = this.size - 1;
        int i2 = 0;
        while (i2 <= i) {
            int i3 = ((i - i2) >> 1) + i2;
            int a = mo22423a(i3, t, f);
            if (a > 0) {
                i2 = i3 + 1;
            } else if (a == 0) {
                return i3;
            } else {
                i = i3 - 1;
            }
        }
        return -1;
    }

    /* renamed from: dQ */
    public int mo22430dQ(float f) {
        int i = 0;
        int size2 = size() - 1;
        while (true) {
            if (i > size2) {
                break;
            }
            int i2 = ((size2 - i) >> 1) + i;
            float f2 = this.byj[i2];
            if (f > f2) {
                i = i2 + 1;
            } else if (f < f2) {
                size2 = i2 - 1;
            } else {
                i = i2;
                while (i < size2 && this.byj[i + 1] == f) {
                    i++;
                }
            }
        }
        return i;
    }

    public T put(float f, T t) {
        int i = this.size - 1;
        int i2 = 0;
        while (i2 <= i) {
            int i3 = ((i - i2) >> 1) + i2;
            int a = mo22423a(i3, t, f);
            if (a > 0) {
                i2 = i3 + 1;
            } else if (a == 0) {
                T elementAt = elementAt(i3);
                this.elements[i3] = t;
                return elementAt;
            } else {
                i = i3 - 1;
            }
        }
        mo22425a(i2, f, t);
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo22425a(int i, float f, T t) {
        Object[] objArr;
        float[] fArr;
        int length = this.elements.length;
        Object[] objArr2 = this.elements;
        float[] fArr2 = this.byj;
        int i2 = this.size;
        if (i2 == length) {
            int i3 = ((length * 3) >> 1) + 1;
            objArr = new Object[i3];
            fArr = new float[i3];
            this.elements = objArr;
            this.byj = fArr;
        } else {
            objArr = this.elements;
            fArr = this.byj;
        }
        if (i > 0) {
            System.arraycopy(objArr2, 0, objArr, 0, i);
            System.arraycopy(fArr2, 0, fArr, 0, i);
        }
        if (i < i2) {
            System.arraycopy(objArr2, i, objArr, i + 1, i2 - i);
            System.arraycopy(fArr2, i, fArr, i + 1, i2 - i);
        }
        this.elements[i] = t;
        this.byj[i] = f;
        this.size = i2 + 1;
    }

    /* renamed from: b */
    public T mo22426b(float f, T t) {
        int i = this.size - 1;
        int i2 = 0;
        while (i2 <= i) {
            int i3 = ((i - i2) >> 1) + i2;
            int a = mo22423a(i3, t, f);
            if (a > 0) {
                i2 = i3 + 1;
            } else if (a == 0) {
                T elementAt = elementAt(i3);
                removeAt(i3);
                return elementAt;
            } else {
                i = i3 - 1;
            }
        }
        return null;
    }

    private void removeAt(int i) {
        int i2 = this.size - 1;
        this.size = i2;
        if (i < i2) {
            System.arraycopy(this.elements, i + 1, this.elements, i, i2 - i);
            System.arraycopy(this.byj, i + 1, this.byj, i, i2 - i);
        }
        this.elements[i2] = null;
    }

    public T removeElement(T t) {
        int i = this.size;
        for (int i2 = 0; i2 <= i; i2++) {
            T t2 = this.elements[i2];
            if (equals(t2, t)) {
                removeAt(i2);
                return t2;
            }
        }
        return null;
    }

    /* renamed from: a */
    public void mo22424a(float f, float f2, T t) {
        int i;
        if (f != f2) {
            int a = mo22422a(f, t);
            if (a < 0) {
                put(f2, t);
                return;
            }
            if (f >= f2) {
                int a2 = m40061a(0, a - 1, f2, t);
                if (mo22423a(a2, t, f2) > 0) {
                    a2++;
                }
                if (i != a) {
                    System.arraycopy(this.elements, i, this.elements, i + 1, a - i);
                    System.arraycopy(this.byj, i, this.byj, i + 1, a - i);
                }
            } else if (a != this.size - 1) {
                i = m40061a(a + 1, this.size - 1, f2, t);
                if (mo22423a(i, t, f2) < 0) {
                    i--;
                }
                if (i != a) {
                    System.arraycopy(this.elements, a + 1, this.elements, a, i - a);
                    System.arraycopy(this.byj, a + 1, this.byj, a, i - a);
                }
            } else {
                i = a;
            }
            this.elements[i] = t;
            this.byj[i] = f2;
        }
    }

    /* renamed from: a */
    private int m40061a(int i, int i2, float f, T t) {
        int i3 = i;
        int i4 = i;
        while (i4 <= i2) {
            i3 = ((i2 - i4) >> 1) + i4;
            int a = mo22423a(i3, t, f);
            if (a <= 0) {
                if (a == 0) {
                    break;
                }
                i2 = i3 - 1;
            } else {
                i4 = i3 + 1;
            }
        }
        return i3;
    }

    public int size() {
        return this.size;
    }

    /* renamed from: eV */
    public float mo22431eV(int i) {
        return this.byj[i];
    }

    public String toString() {
        if (this.size == 0) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        int i = this.size;
        for (int i2 = 0; i2 < i; i2++) {
            if (i2 > 0) {
                sb.append(", ");
            }
            sb.append(mo22431eV(i2)).append(": ");
            Object elementAt = elementAt(i2);
            if (elementAt == this) {
                elementAt = "(this Collection)";
            }
            sb.append(elementAt);
        }
        return sb.append(']').toString();
    }

    public void clear() {
        if (this.size > 0) {
            Arrays.fill(this.elements, 0, this.size, (Object) null);
            Arrays.fill(this.byj, 0, this.size, 0.0f);
            this.size = 0;
        }
    }
}
