package p001a;

import logic.thred.LogPrinter;
import taikodom.render.graphics2d.C0559Hm;
import taikodom.render.loader.provider.C0399FY;
import taikodom.render.loader.provider.FilePath;

import java.io.*;
import java.nio.ByteBuffer;

/* renamed from: a.sU */
/* compiled from: a */
public class C3549sU implements FilePath {
    static LogPrinter log = LogPrinter.m10275K(C3549sU.class);
    private final C2657iF bjv;
    private final FilePath bjw;
    private File bjx;
    private RandomAccessFile bjy;

    public C3549sU(String str, FilePath ain) throws IOException {
        this.bjw = ain;
        this.bjy = new RandomAccessFile(String.valueOf(str) + ".dat", "rw");
        this.bjx = new File(String.valueOf(str) + ".idx");
        if (this.bjx.exists()) {
            DataInputStream dataInputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(this.bjx)));
            this.bjv = C2657iF.m33059a(this, (DataInput) dataInputStream);
            dataInputStream.close();
            return;
        }
        this.bjv = new C2657iF(this);
    }

    public synchronized void sync() throws IOException {
        DataOutputStream dataOutputStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(this.bjx)));
        this.bjv.mo19454a((DataOutput) dataOutputStream);
        dataOutputStream.flush();
        dataOutputStream.close();
        this.bjy.getFD().sync();
    }

    /* renamed from: b */
    public boolean mo21891b(C2657iF iFVar) {
        return this.bjw.concat(iFVar.getPath()).exists();
    }

    /* renamed from: c */
    public InputStream mo21892c(C2657iF iFVar) throws FileNotFoundException {
        if (!iFVar.isCached()) {
            InputStream openInputStream = this.bjw.concat(iFVar.getPath()).openInputStream();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                C0559Hm.copyStream(openInputStream, byteArrayOutputStream);
                m38782a(iFVar, byteArrayOutputStream.toByteArray());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return new ByteArrayInputStream(m38784d(iFVar));
    }

    /* renamed from: d */
    private synchronized byte[] m38784d(C2657iF iFVar) {
        byte[] bArr;
        try {
            log.info("getting from cache: " + iFVar.getPath());
            this.bjy.seek(iFVar.getOffset());
            if (!this.bjy.readBoolean()) {
                throw new FileNotFoundException(new StringBuilder().append(iFVar).toString());
            }
            bArr = new byte[iFVar.getLength()];
            int i = 0;
            while (i < bArr.length) {
                i += this.bjy.read(bArr, i, bArr.length - i);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return bArr;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public synchronized void m38782a(C2657iF iFVar, byte[] bArr) {
        try {
            this.bjy.seek(this.bjy.length());
            iFVar.setOffset(this.bjy.getFilePointer());
            iFVar.setLength(bArr.length);
            iFVar.mo19453Z(true);
            this.bjy.writeBoolean(true);
            this.bjy.write(bArr);
            sync();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: e */
    public OutputStream mo21893e(C2657iF iFVar) {
        return new C3550a(iFVar);
    }

    /* renamed from: f */
    public synchronized void mo21894f(C2657iF iFVar) {
        try {
            this.bjy.seek(iFVar.getOffset());
            this.bjy.writeBoolean(false);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: g */
    public long mo21895g(C2657iF iFVar) {
        return this.bjw.concat(iFVar.getPath()).length();
    }

    /* renamed from: h */
    public long mo21896h(C2657iF iFVar) {
        return this.bjw.concat(iFVar.getPath()).lastModified();
    }

    public boolean delete() {
        return this.bjv.delete();
    }

    public boolean exists() {
        return this.bjv.exists();
    }

    /* renamed from: aC */
    public FilePath concat(String str) {
        return this.bjv.concat(str);
    }

    /* renamed from: BF */
    public FilePath mo2253BF() {
        return this.bjv.mo2253BF();
    }

    /* renamed from: BG */
    public File getFile() {
        return this.bjv.getFile();
    }

    public String getName() {
        return this.bjv.getName();
    }

    /* renamed from: BB */
    public FilePath mo2249BB() {
        return this.bjv.mo2249BB();
    }

    public String getPath() {
        return this.bjv.getPath();
    }

    public boolean isDir() {
        return this.bjv.isDir();
    }

    public long lastModified() {
        return this.bjv.lastModified();
    }

    public long length() {
        return this.bjv.length();
    }

    /* renamed from: BC */
    public FilePath[] mo2250BC() {
        return this.bjv.mo2250BC();
    }

    /* renamed from: a */
    public FilePath[] mo2258a(C0399FY fy) {
        return this.bjv.mo2258a(fy);
    }

    /* renamed from: BD */
    public void mo2251BD() {
        this.bjv.mo2251BD();
    }

    public InputStream openInputStream() throws FileNotFoundException {
        return this.bjv.openInputStream();
    }

    public OutputStream openOutputStream() {
        return this.bjv.openOutputStream();
    }

    /* renamed from: BE */
    public PrintWriter mo2252BE() {
        return this.bjv.mo2252BE();
    }

    /* renamed from: BH */
    public byte[] mo2255BH() {
        return this.bjv.mo2255BH();
    }

    /* renamed from: d */
    public boolean mo2261d(FilePath ain) {
        return this.bjv.mo2261d(ain);
    }

    /* renamed from: BI */
    public ByteBuffer mo2256BI() {
        return this.bjv.mo2256BI();
    }

    /* renamed from: d */
    public void mo2260d(byte[] bArr) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a.sU$a */
    class C3550a extends ByteArrayOutputStream {
        private final /* synthetic */ C2657iF grO;

        C3550a(C2657iF iFVar) {
            this.grO = iFVar;
        }

        public void close() {
            C3549sU.this.m38782a(this.grO, toByteArray());
        }
    }
}
