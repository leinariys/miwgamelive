package p001a;

import logic.data.mbean.C0677Jd;
import logic.res.html.MessageContainer;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/* renamed from: a.awW  reason: case insensitive filesystem */
/* compiled from: a */
public class C6934awW extends ClassLoader {
    public static final String gOm = "java.protocol.handler.pkgs";
    private static final Map<String, C6934awW> gOl = MessageContainer.dgM();
    private static final String gOn = "com.hoplon.tkd.client.rloader";
    private static final Log log = LogPrinter.m10275K(C6934awW.class);

    static {
        String property = System.getProperty(gOm);
        if (property == null) {
            property = "";
        } else if (property.length() > 0) {
            property = "|" + property;
        }
        System.setProperty(gOm, gOn + property);
    }

    final String gOo = (String.valueOf(getClass().getName()) + hashCode());
    public String name;
    Map<String, byte[]> gOk = MessageContainer.newMap();
    private C6473and gOj;

    public C6934awW() {
        gOl.put(this.gOo, this);
    }

    /* renamed from: kA */
    static C6934awW m26882kA(String str) {
        return gOl.get(str);
    }

    /* renamed from: a */
    public void mo16693a(C6473and and) {
        this.gOj = and;
    }

    public URL getResource(String str) {
        if (this.gOk.containsKey(str)) {
            try {
                return new URL(String.valueOf(C3507rk.ban) + ":" + this.gOo + ":" + str);
            } catch (MalformedURLException e) {
                log.debug(e.getMessage(), e);
            }
        }
        if (this.gOj != null) {
            try {
                if (this.gOj.mo10199iS(str)) {
                    return new URL(String.valueOf(C3507rk.ban) + ":" + this.gOo + ":" + str);
                }
            } catch (MalformedURLException e2) {
                log.debug(e2.getMessage(), e2);
            }
        }
        return super.getResource(str);
    }

    public URL findResource(String str) {
        if (this.gOk.containsKey(str)) {
            try {
                return new URL(String.valueOf(C3507rk.ban) + ":" + this.gOo + ":" + str);
            } catch (MalformedURLException e) {
                log.debug(e.getMessage(), e);
            }
        }
        if (this.gOj != null) {
            try {
                if (this.gOj.mo10199iS(str)) {
                    return new URL(String.valueOf(C3507rk.ban) + ":" + this.gOo + ":" + str);
                }
            } catch (MalformedURLException e2) {
                log.debug(e2.getMessage(), e2);
            }
        }
        return super.findResource(str);
    }

    public synchronized InputStream getResourceAsStream(String str) {
        ByteArrayInputStream byteArrayInputStream;
        byte[] bArr = this.gOk.get(str);
        if (bArr == null) {
            bArr = this.gOj.mo10201jw(str);
            this.gOk.put(str, bArr);
        }
        byte[] bArr2 = bArr;
        if (bArr2 == null) {
            byteArrayInputStream = null;
        } else {
            byteArrayInputStream = new ByteArrayInputStream(bArr2);
        }
        return byteArrayInputStream;
    }

    /* renamed from: j */
    public void mo16698j(byte[] bArr) {
        ZipInputStream zipInputStream;
        try {
            zipInputStream = new ZipInputStream(new ByteArrayInputStream(bArr));
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1048576);
                while (true) {
                    ZipEntry nextEntry = zipInputStream.getNextEntry();
                    if (nextEntry == null) {
                        try {
                            zipInputStream.close();
                            return;
                        } catch (Exception e) {
                            return;
                        }
                    } else {
                        byte[] bArr2 = new byte[C0677Jd.dij];
                        while (true) {
                            int read = zipInputStream.read(bArr2, 0, bArr2.length);
                            if (read == -1) {
                                break;
                            }
                            byteArrayOutputStream.write(bArr2, 0, read);
                        }
                        this.gOk.put(nextEntry.getName(), byteArrayOutputStream.toByteArray());
                        byteArrayOutputStream.reset();
                    }
                }
            } catch (Throwable th) {
                th = th;
            }
        } catch (Throwable th2) {
            th = th2;
            zipInputStream = null;
            try {
                zipInputStream.close();
            } catch (Exception e2) {
            }
            throw th;
        }
    }

    public Class loadClass(String str) {
        return loadClass(str, false);
    }

    public Class loadClass(String str, boolean z) {
        Class findLoadedClass = findLoadedClass(str);
        if (findLoadedClass == null && (findLoadedClass = mo16701q(str, false)) == null) {
            return super.loadClass(str, z);
        }
        if (!z) {
            return findLoadedClass;
        }
        resolveClass(findLoadedClass);
        return findLoadedClass;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0028, code lost:
        if (r5.startsWith("sun.") == false) goto L_0x002a;
     */
    /* renamed from: q */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.Class mo16701q(java.lang.String r5, boolean r6) {
        /*
            r4 = this;
            r0 = 0
            monitor-enter(r4)
            a.and r1 = r4.gOj     // Catch:{ all -> 0x004c }
            if (r1 != 0) goto L_0x0008
        L_0x0006:
            monitor-exit(r4)
            return r0
        L_0x0008:
            if (r6 != 0) goto L_0x002a
            java.lang.String r1 = "java."
            boolean r1 = r5.startsWith(r1)     // Catch:{ all -> 0x004c }
            if (r1 != 0) goto L_0x0006
            java.lang.String r1 = "org.xml."
            boolean r1 = r5.startsWith(r1)     // Catch:{ all -> 0x004c }
            if (r1 != 0) goto L_0x0006
            java.lang.String r1 = "org.dom4j."
            boolean r1 = r5.startsWith(r1)     // Catch:{ all -> 0x004c }
            if (r1 != 0) goto L_0x0006
            java.lang.String r1 = "sun."
            boolean r1 = r5.startsWith(r1)     // Catch:{ all -> 0x004c }
            if (r1 != 0) goto L_0x0006
        L_0x002a:
            java.lang.Class r1 = super.findLoadedClass(r5)     // Catch:{ RemoteException -> 0x0041 }
            if (r1 == 0) goto L_0x0032
            r0 = r1
            goto L_0x0006
        L_0x0032:
            a.and r1 = r4.gOj     // Catch:{ RemoteException -> 0x0041 }
            byte[] r1 = r1.mo10202jx(r5)     // Catch:{ RemoteException -> 0x0041 }
            if (r1 == 0) goto L_0x0006
            r2 = 0
            int r3 = r1.length     // Catch:{ RemoteException -> 0x0041 }
            java.lang.Class r0 = super.defineClass(r5, r1, r2, r3)     // Catch:{ RemoteException -> 0x0041 }
            goto L_0x0006
        L_0x0041:
            r1 = move-exception
            org.apache.commons.logging.Log r2 = log     // Catch:{ all -> 0x004c }
            java.lang.String r3 = r1.getMessage()     // Catch:{ all -> 0x004c }
            r2.debug(r3, r1)     // Catch:{ all -> 0x004c }
            goto L_0x0006
        L_0x004c:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6934awW.mo16701q(java.lang.String, boolean):java.lang.Class");
    }

    public Class findClass(String str) {
        Class q = mo16701q(str, true);
        return q != null ? q : super.findClass(str);
    }
}
