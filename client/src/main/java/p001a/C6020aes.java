package p001a;

import game.geometry.Matrix4fWrap;
import game.geometry.Vec3d;
import game.geometry.aLH;
import logic.thred.C6615aqP;
import logic.thred.LogPrinter;

import javax.vecmath.Tuple3d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: a.aes  reason: case insensitive filesystem */
/* compiled from: a */
public final class C6020aes implements C3286py {
    private static final boolean DEBUG = false;
    private static final LogPrinter logger = LogPrinter.setClass(C6020aes.class);
    private final C0837MD cIj;
    private final C6794atm fnD;
    private final C6794atm fnE;
    private final C6794atm fnF;
    private final float fnJ;
    private final float fnK;
    private final float fnL;
    private final float fnM;
    private final float fnN;
    private final float fnO;
    private final float fnP;
    private final float fnQ;
    private final float fnR;
    private final C0837MD fnS;
    private final C0837MD fnT;
    private final List<C6365alZ> fnU;
    private final Vec3d fnV;
    private final Vec3d fnW;
    private final List<C6365alZ> inserted;
    private final List<C6365alZ> removed;
    private C6794atm[] fnG;
    private Matrix4fWrap fnH;
    private int fnI;
    private int fnX;
    private int fnY;
    private int fnZ;
    private int foa;
    private int fob;
    private int foc;
    private int fod;
    private int foe;
    private int fof;
    private int fog;
    private int foh;
    private int foi;
    private int foj;
    private int fok;
    private int fol;
    private int fom;
    /* renamed from: ox */
    private C1037PD f4430ox;

    public C6020aes() {
        this(45.0f, 45.0f, 45.0f);
    }

    public C6020aes(float f, float f2, float f3) {
        this.fnD = new C6794atm();
        this.fnE = new C6794atm();
        this.fnF = new C6794atm();
        this.fnG = new C6794atm[]{this.fnD, this.fnE, this.fnF};
        this.cIj = new C0837MD();
        this.fnS = new C0837MD();
        this.fnT = new C0837MD();
        this.inserted = new ArrayList();
        this.fnU = new ArrayList();
        this.removed = new ArrayList();
        this.fnV = new Vec3d();
        this.fnW = new Vec3d();
        this.fnH = new Matrix4fWrap();
        this.fnH.rotateX(f);
        this.fnH.rotateY(f2);
        this.fnH.rotateZ(f3);
        this.fnJ = Math.abs(this.fnH.m00);
        this.fnM = Math.abs(this.fnH.m01);
        this.fnP = Math.abs(this.fnH.m02);
        this.fnK = Math.abs(this.fnH.m10);
        this.fnN = Math.abs(this.fnH.m11);
        this.fnQ = Math.abs(this.fnH.m12);
        this.fnL = Math.abs(this.fnH.m20);
        this.fnO = Math.abs(this.fnH.m21);
        this.fnR = Math.abs(this.fnH.m22);
    }

    /* renamed from: a */
    private int m21353a(C6794atm atm, int i, C3311qA qAVar) {
        int size = atm.size() - 1;
        double d = qAVar.aVY;
        while (i <= size) {
            int i2 = ((size - i) >> 1) + i;
            if (atm.mo16250uA(i2).aVY < d) {
                i = i2 + 1;
            } else {
                size = i2 - 1;
            }
        }
        atm.mo16244b(i, qAVar);
        qAVar.aVX = i;
        int size2 = atm.size();
        while (true) {
            size2--;
            if (size2 <= i) {
                return i;
            }
            atm.mo16250uA(size2).aVX = size2;
        }
    }

    /* renamed from: a */
    private int m21352a(C6794atm atm, int i, double d) {
        int size = atm.size() - 1;
        while (i <= size) {
            int i2 = ((size - i) >> 1) + i;
            if (atm.mo16250uA(i2).aVY < d) {
                i = i2 + 1;
            } else {
                size = i2 - 1;
            }
        }
        return i;
    }

    /* renamed from: a */
    private void m21357a(C6794atm atm, C3311qA qAVar) {
        int i;
        int i2 = qAVar.aVX;
        if (i2 > 0) {
            C3311qA uA = atm.mo16250uA(i2 - 1);
            double d = uA.aVY;
            double d2 = qAVar.aVY;
            if (d > d2) {
                do {
                    atm.mo16242a(i2, uA);
                    uA.aVX = i2;
                    uA.aVZ += qAVar.aVU;
                    if (qAVar.mo21298Xj() && !uA.mo21298Xj()) {
                        qAVar.aVX = i2 - 1;
                        m21359a(qAVar, uA);
                    } else if (!qAVar.mo21298Xj() && uA.mo21298Xj()) {
                        m21364c(qAVar.aVW, uA.aVW);
                    }
                    i2--;
                    if (i2 <= 0) {
                        break;
                    }
                    uA = atm.mo16250uA(i2 - 1);
                } while (uA.aVY > d2);
                qAVar.aVX = i2;
                if (i2 > 0) {
                    i = atm.mo16250uA(i2 - 1).mo21297Xi();
                } else {
                    i = 0;
                }
                qAVar.aVZ = i;
                atm.mo16242a(i2, qAVar);
            }
        }
    }

    /* renamed from: b */
    private void m21363b(C6794atm atm, C3311qA qAVar) {
        int i = qAVar.aVX;
        int size = atm.size() - 1;
        if (i < size) {
            C3311qA uA = atm.mo16250uA(i + 1);
            double d = uA.aVY;
            double d2 = qAVar.aVY;
            if (d < d2) {
                do {
                    atm.mo16242a(i, uA);
                    uA.aVX = i;
                    uA.aVZ -= qAVar.aVU;
                    if (!qAVar.mo21298Xj() && uA.mo21298Xj()) {
                        qAVar.aVX = i + 1;
                        m21359a(qAVar, uA);
                    } else if (qAVar.mo21298Xj() && !uA.mo21298Xj()) {
                        m21364c(qAVar.aVW, uA.aVW);
                    }
                    i++;
                    if (i >= size) {
                        break;
                    }
                    uA = atm.mo16250uA(i + 1);
                } while (uA.aVY < d2);
                qAVar.aVX = i;
                qAVar.aVZ = atm.mo16250uA(i - 1).mo21297Xi();
                atm.mo16242a(i, qAVar);
            }
        }
    }

    /* renamed from: a */
    private void m21359a(C3311qA qAVar, C3311qA qAVar2) {
        if (qAVar.mo728a(qAVar2.aVW)) {
            m21362b(qAVar.aVW, qAVar2.aVW);
        }
    }

    public void dump() {
        logger.info(String.valueOf(getClass().getName()) + " dump");
        logger.info("x: " + this.fnG[0]);
        logger.info("y: " + this.fnG[1]);
        logger.info("z: " + this.fnG[2]);
    }

    /* renamed from: a */
    public aRR mo1225a(aLH alh, C6615aqP aqp) {
        C6365alZ alz = new C6365alZ(this, aqp);
        alz.fYr.mo9867g(alh);
        this.inserted.add(alz);
        return alz;
    }

    /* renamed from: b */
    private void m21361b(C6365alZ alz) {
        int i;
        for (int i2 = 0; i2 < 3; i2++) {
            C6794atm atm = this.fnG[i2];
            if (atm.size() == 0) {
                atm.mo16243a(alz.mo14696sv(i2));
                atm.mo16243a(alz.mo14697sw(i2));
                alz.mo14696sv(i2).aVX = 0;
                alz.mo14696sv(i2).aVZ = 0;
                alz.mo14697sw(i2).aVX = 1;
                alz.mo14697sw(i2).aVZ = 1;
            } else {
                int a = m21353a(atm, 0, alz.mo14696sv(i2));
                int a2 = m21353a(atm, a + 1, alz.mo14697sw(i2));
                C3311qA sv = alz.mo14696sv(i2);
                if (a > 0) {
                    i = atm.mo16250uA(a - 1).mo21297Xi();
                } else {
                    i = 0;
                }
                sv.aVZ = i;
                alz.mo14697sw(i2).aVZ = atm.mo16250uA(a2 - 1).mo21297Xi();
                int i3 = a;
                while (i3 < a2) {
                    i3++;
                    atm.mo16250uA(i3).aVZ = atm.mo16250uA(i3).mo21297Xi();
                }
            }
        }
        C6794atm atm2 = this.fnG[0];
        if (alz.fYl.aVZ > 0) {
            int i4 = alz.fYl.aVZ;
            int i5 = alz.fYl.aVX;
            int i6 = i5;
            while (true) {
                if (i4 <= 0) {
                    break;
                } else if (i6 == 0) {
                    logger.warn("Problems during insert: " + i5 + " " + i4 + " " + alz.fYl.aVZ + ". Rebuilding stabbing indexes.");
                    long nanoTime = System.nanoTime();
                    bUv();
                    logger.warn("Stabbing indexes rebuild took " + (System.nanoTime() - nanoTime) + " nano seconds.");
                    break;
                } else {
                    i6--;
                    C3311qA uA = atm2.mo16250uA(i6);
                    if (uA.mo21298Xj() && uA.aVW.fYo.aVX > i5) {
                        i4--;
                        if (m21360a(alz, uA.aVW)) {
                            m21362b(alz, uA.aVW);
                        }
                    }
                }
            }
        }
        int i7 = alz.fYo.aVX - alz.fYl.aVX;
        if (i7 > 1) {
            int i8 = alz.fYl.aVX;
            while (true) {
                i7--;
                if (i7 >= 0) {
                    i8++;
                    C3311qA uA2 = this.fnD.mo16250uA(i8);
                    if (uA2.mo21298Xj() && m21360a(alz, uA2.aVW)) {
                        m21362b(alz, uA2.aVW);
                    }
                } else {
                    return;
                }
            }
        }
    }

    private void bUv() {
        for (int i = 0; i < 3; i++) {
            C6794atm atm = this.fnG[i];
            int size = atm.size();
            int i2 = 0;
            int i3 = 0;
            while (true) {
                size--;
                if (size < 0) {
                    break;
                }
                atm.mo16250uA(i2).aVZ = i3;
                i3 += atm.mo16250uA(i2).aVU;
                i2++;
            }
        }
    }

    /* renamed from: a */
    private boolean m21360a(C6365alZ alz, C6365alZ alz2) {
        if (alz.fYl.aVX <= alz2.fYo.aVX && alz2.fYl.aVX <= alz.fYo.aVX && alz.fYm.aVX <= alz2.fYp.aVX && alz2.fYm.aVX <= alz.fYp.aVX && alz.fYn.aVX <= alz2.fYq.aVX && alz2.fYn.aVX <= alz.fYq.aVX && alz != alz2) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo13189c(C6365alZ alz) {
        if (!alz.isValid()) {
            m21356a(alz, "Invalid collider entry at remove");
            return;
        }
        alz.dhD();
        this.removed.add(alz);
    }

    /* renamed from: a */
    private void m21356a(C6365alZ alz, String str) {
        this.fnX++;
        if (this.fnX < 10) {
            logger.warn(String.valueOf(str) + " " + alz);
            return;
        }
        int pow = (int) Math.pow(10.0d, (double) ((int) Math.log10((double) this.fnX)));
        if (this.fnX % pow == 0) {
            logger.warn("Several invalid collider entries found. Until now there were: " + this.fnX + " invalid entries. Issuing warnings at each " + pow + " errors");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13182a(C6365alZ alz, aLH alh) {
        if (!alz.isValid()) {
            m21356a(alz, "Invalid collider entry at reposition");
        }
        alz.fYr.mo9867g(alh);
        if (!alz.fYs) {
            alz.fYs = true;
            this.fnU.add(alz);
        }
    }

    /* renamed from: a */
    private void m21358a(C6794atm atm, C3311qA qAVar, C3311qA qAVar2, double d, double d2) {
        if (d > qAVar2.aVY) {
            qAVar2.aVY = d2;
            m21363b(atm, qAVar2);
            qAVar.aVY = d;
            m21363b(atm, qAVar);
            return;
        }
        mo13183a(atm, qAVar, d);
        mo13183a(atm, qAVar2, d2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13183a(C6794atm atm, C3311qA qAVar, double d) {
        double d2 = qAVar.aVY;
        if (d > d2) {
            qAVar.aVY = d;
            m21363b(atm, qAVar);
        } else if (d < d2) {
            qAVar.aVY = d;
            m21357a(atm, qAVar);
        }
    }

    /* renamed from: a */
    private void m21354a(aLH alh, Vec3d ajr, Vec3d ajr2) {
        mo13184b(alh, ajr, ajr2);
    }

    /* renamed from: a */
    private void m21355a(C6365alZ alz, Vec3d ajr, Vec3d ajr2) {
        float radius = alz.getRadius();
        if (radius > 0.0f) {
            this.fnH.mo13986a((Tuple3d) alz.dhC().getPosition(), (Tuple3d) ajr);
            ajr2.mo9484aA(ajr);
            ajr.mo9475E(-radius, -radius, -radius);
            ajr2.mo9475E(radius, radius, radius);
            return;
        }
        m21354a(alz.fYr, ajr, ajr2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final void mo13184b(aLH alh, Vec3d ajr, Vec3d ajr2) {
        Vec3d dim = alh.dim();
        Vec3d din = alh.din();
        float f = (float) ((din.x - dim.x) * 0.5d);
        float f2 = (float) ((din.y - dim.y) * 0.5d);
        float f3 = (float) ((din.z - dim.z) * 0.5d);
        float f4 = (this.fnJ * f) + (this.fnM * f2) + (this.fnP * f3);
        float f5 = (this.fnK * f) + (this.fnN * f2) + (this.fnQ * f3);
        float f6 = (this.fnL * f) + (this.fnO * f2) + (this.fnR * f3);
        double d = dim.x + ((double) f);
        double d2 = ((double) f2) + dim.y;
        double d3 = ((double) f3) + dim.z;
        double d4 = (((double) this.fnH.m00) * d) + (((double) this.fnH.m01) * d2) + (((double) this.fnH.m02) * d3);
        double d5 = (((double) this.fnH.m10) * d) + (((double) this.fnH.m11) * d2) + (((double) this.fnH.m12) * d3);
        double d6 = (d3 * ((double) this.fnH.m22)) + (d2 * ((double) this.fnH.m21)) + (d * ((double) this.fnH.m20));
        ajr2.x = ((double) f4) + d4;
        ajr2.y = ((double) f5) + d5;
        ajr2.z = ((double) f6) + d6;
        ajr.x = d4 - ((double) f4);
        ajr.y = d5 - ((double) f5);
        ajr.z = d6 - ((double) f6);
    }

    /* renamed from: b */
    private void m21362b(C6365alZ alz, C6365alZ alz2) {
        if ((this.f4430ox == null || this.f4430ox.mo1932a(alz.dhC(), alz2.dhC())) && this.cIj.mo3874aJ(alz.f3320id, alz2.f3320id) == null) {
            C1167RH rh = new C1167RH(alz, alz2);
            this.cIj.mo3873a(rh);
            this.fnS.mo3873a(rh);
            this.fnT.mo3875b((C6436ams) rh);
        }
    }

    /* renamed from: c */
    private void m21364c(C6365alZ alz, C6365alZ alz2) {
        C3102np a = this.cIj.mo3872a(alz, alz2);
        if (a != null) {
            this.fnT.mo3873a((C6436ams) a);
            this.fnS.mo3876b(a);
        }
    }

    public boolean bUw() {
        this.fom++;
        boolean z = true;
        for (int i = 0; i < 3; i++) {
            C6794atm atm = this.fnG[i];
            int size = atm.size();
            int i2 = 0;
            int i3 = 0;
            while (true) {
                size--;
                if (size < 0) {
                    break;
                }
                if (atm.mo16250uA(i2).aVW.mo14696sv(i) != atm.mo16250uA(atm.mo16250uA(i2).aVW.mo14696sv(i).aVX)) {
                    logger.error(String.valueOf(this.fom) + " Wrong start index at " + i2 + " axis " + i + "  " + atm.mo16250uA(atm.mo16250uA(i2).aVW.mo14696sv(i).aVX) + " != " + atm.mo16250uA(i2).aVW.mo14696sv(i));
                    z = false;
                }
                if (atm.mo16250uA(i2).aVW.mo14697sw(i) != atm.mo16250uA(atm.mo16250uA(i2).aVW.mo14697sw(i).aVX)) {
                    logger.error(String.valueOf(this.fom) + " Wrong end index at " + i2 + " axis " + i + "  " + atm.mo16250uA(atm.mo16250uA(i2).aVW.mo14697sw(i).aVX) + " != " + atm.mo16250uA(i2).aVW.mo14697sw(i));
                    z = false;
                }
                if (i3 != atm.mo16250uA(i2).aVZ) {
                    logger.error(String.valueOf(this.fom) + " Wrong stabbing at " + i2 + " axis " + i + " was " + atm.mo16250uA(i2).aVZ + " expected " + i3);
                    z = false;
                }
                i3 += atm.mo16250uA(i2).aVU;
                if (i2 > 0 && atm.mo16250uA(i2 - 1).aVY > atm.mo16250uA(i2).aVY) {
                    logger.error(String.valueOf(this.fom) + " Wrong order at " + i2 + " axis " + i);
                    z = false;
                }
                i2++;
            }
        }
        return z;
    }

    /* renamed from: a */
    public void mo1229a(aLH alh, C6178ahu ahu) {
        if (this.fnD.size() != 0) {
            Vec3d ajr = new Vec3d();
            Vec3d ajr2 = new Vec3d();
            m21354a(alh, ajr, ajr2);
            this.fnI += 3;
            int a = m21352a(this.fnD, 0, ajr.x);
            if (a < this.fnD.size()) {
                int a2 = m21352a(this.fnD, a + 1, ajr2.x);
                if (a2 >= this.fnD.size()) {
                    a2 = this.fnD.size() - 1;
                }
                double d = ajr2.y;
                double d2 = ajr.y;
                double d3 = ajr2.z;
                double d4 = ajr.z;
                int i = this.fnD.mo16250uA(a).aVZ;
                int i2 = a;
                while (i > 0) {
                    i2--;
                    if (i2 < 0) {
                        break;
                    }
                    C3311qA uA = this.fnD.mo16250uA(i2);
                    if (uA.mo21298Xj()) {
                        C6365alZ alz = uA.aVW;
                        if (alz.fYo.aVX >= a && alz.fYq.aVY >= d4 && alz.fYn.aVY <= d3 && alz.fYp.aVY >= d2 && alz.fYm.aVY <= d && !ahu.mo13625a(alz)) {
                            return;
                        }
                    }
                }
                int i3 = (a2 - a) + 1;
                int i4 = a - 1;
                while (true) {
                    i3--;
                    if (i3 > 0) {
                        i4++;
                        C3311qA uA2 = this.fnD.mo16250uA(i4);
                        if (uA2.mo21298Xj()) {
                            C6365alZ alz2 = uA2.aVW;
                            if (alz2.fYq.aVY >= d4 && alz2.fYn.aVY <= d3 && alz2.fYp.aVY >= d2 && alz2.fYm.aVY <= d && !ahu.mo13625a(alz2)) {
                                return;
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        }
    }

    private void bUx() {
        Vec3d ajr = this.fnV;
        Vec3d ajr2 = this.fnW;
        int size = this.inserted.size();
        for (int i = 0; i < size; i++) {
            C6365alZ alz = this.inserted.get(i);
            if (alz.isValid()) {
                m21355a(alz, ajr, ajr2);
                alz.mo14694f(ajr, ajr2);
                m21361b(alz);
            }
        }
        this.inserted.clear();
    }

    /* access modifiers changed from: protected */
    public void bUy() {
        int size = this.removed.size();
        for (int i = 0; i < size; i++) {
            C6365alZ alz = this.removed.get(i);
            if (alz.fYl.aVX != -1) {
                for (int i2 = 0; i2 < 3; i2++) {
                    C6794atm atm = this.fnG[i2];
                    int i3 = alz.mo14696sv(i2).aVX;
                    int i4 = alz.mo14697sw(i2).aVX;
                    atm.remove(i4);
                    atm.remove(i3);
                    int size2 = atm.size();
                    for (int i5 = i3; i5 < size2; i5++) {
                        atm.mo16250uA(i5).aVX = i5;
                    }
                    int i6 = i4 - 1;
                    while (i3 < i6) {
                        C3311qA uA = atm.mo16250uA(i3);
                        uA.aVZ--;
                        i3++;
                    }
                }
            }
            Iterator<C3102np> it = this.cIj.iterator();
            while (it.hasNext()) {
                C6436ams ams = (C6436ams) it.next();
                if (ams.mo14894Qg() == alz || ams.mo14895Qh() == alz) {
                    this.fnT.mo3873a(ams);
                    it.remove();
                }
            }
        }
        this.removed.clear();
    }

    private void bUz() {
        Vec3d ajr = this.fnV;
        Vec3d ajr2 = this.fnW;
        int size = this.fnU.size();
        for (int i = 0; i < size; i++) {
            C6365alZ alz = this.fnU.get(i);
            if (alz.isValid()) {
                m21355a(alz, ajr, ajr2);
                m21358a(this.fnD, alz.fYl, alz.fYo, ajr.x, ajr2.x);
                m21358a(this.fnF, alz.fYn, alz.fYq, ajr.z, ajr2.z);
                m21358a(this.fnE, alz.fYm, alz.fYp, ajr.y, ajr2.y);
                alz.fYs = false;
            }
        }
        this.fnU.clear();
    }

    /* renamed from: gZ */
    public void mo1231gZ() {
        this.fnT.clear();
        this.fnS.clear();
        bUx();
        bUy();
        bUz();
    }

    /* renamed from: Vy */
    public C0250DD mo1223Vy() {
        return this.cIj;
    }

    /* renamed from: Vz */
    public C0250DD mo1224Vz() {
        return this.fnT;
    }

    public C1037PD bvg() {
        return this.f4430ox;
    }

    /* renamed from: a */
    public void mo1226a(C1037PD pd) {
        this.f4430ox = pd;
    }

    /* renamed from: VA */
    public C0250DD mo1222VA() {
        return this.fnS;
    }

    /* renamed from: a */
    public void mo1227a(Vec3d ajr, float f, C6178ahu ahu) {
        if (this.fnD.size() != 0) {
            int i = 0;
            Vec3d ajr2 = new Vec3d();
            Vec3d ajr3 = new Vec3d();
            this.fnH.mo13986a((Tuple3d) ajr, (Tuple3d) ajr2);
            double d = ajr2.x;
            double d2 = ajr2.y;
            double d3 = ajr2.z;
            ajr3.mo9484aA(ajr2);
            ajr2.mo9475E(-f, -f, -f);
            ajr3.mo9475E(f, f, f);
            this.fnI += 3;
            int a = m21352a(this.fnD, 0, ajr2.x);
            if (a < this.fnD.size()) {
                int a2 = m21352a(this.fnD, a + 1, ajr3.x);
                if (a2 >= this.fnD.size()) {
                    a2 = this.fnD.size() - 1;
                }
                double d4 = ajr3.y;
                double d5 = ajr2.y;
                double d6 = ajr3.z;
                double d7 = ajr2.z;
                float f2 = f * f;
                int i2 = this.fnD.mo16250uA(a).aVZ;
                int i3 = a;
                int i4 = 0;
                while (i2 > 0) {
                    int i5 = i3 - 1;
                    if (i5 < 0) {
                        break;
                    }
                    int i6 = i + 1;
                    C3311qA uA = this.fnD.mo16250uA(i5);
                    int i7 = i2 + (uA.mo21298Xj() ? -1 : 1);
                    if (uA.mo21298Xj()) {
                        C6365alZ alz = uA.aVW;
                        if (alz.fYo.aVX >= a) {
                            if (alz.fYq.aVY < d7) {
                                i3 = i5;
                                i2 = i7;
                                i = i6;
                            } else if (alz.fYn.aVY > d6) {
                                i3 = i5;
                                i2 = i7;
                                i = i6;
                            } else if (alz.fYp.aVY < d5) {
                                i3 = i5;
                                i2 = i7;
                                i = i6;
                            } else if (alz.fYm.aVY > d4) {
                                i3 = i5;
                                i2 = i7;
                                i = i6;
                            } else {
                                float max = Math.max((float) (alz.fYl.aVY - d), (float) (d - alz.fYo.aVY));
                                if (max < 0.0f) {
                                    max = 0.0f;
                                }
                                float max2 = Math.max((float) (alz.fYm.aVY - d2), (float) (d2 - alz.fYp.aVY));
                                if (max2 < 0.0f) {
                                    max2 = 0.0f;
                                }
                                float max3 = Math.max((float) (alz.fYn.aVY - d3), (float) (d3 - alz.fYq.aVY));
                                if (max3 < 0.0f) {
                                    max3 = 0.0f;
                                }
                                if ((max * max) + (max2 * max2) + (max3 * max3) <= f2) {
                                    i4++;
                                    if (!ahu.mo13625a(alz)) {
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    i3 = i5;
                    i2 = i7;
                    i = i6;
                }
                int i8 = (a2 - a) + 1;
                int i9 = a - 1;
                while (true) {
                    i8--;
                    if (i8 > 0) {
                        i++;
                        i9++;
                        C3311qA uA2 = this.fnD.mo16250uA(i9);
                        if (uA2.mo21298Xj()) {
                            C6365alZ alz2 = uA2.aVW;
                            if (alz2.fYq.aVY >= d7 && alz2.fYn.aVY <= d6 && alz2.fYp.aVY >= d5 && alz2.fYm.aVY <= d4) {
                                float min = Math.min(Math.abs((float) (d - alz2.fYl.aVY)), Math.abs((float) (d - alz2.fYo.aVY)));
                                float min2 = Math.min(Math.abs((float) (d2 - alz2.fYm.aVY)), Math.abs((float) (d2 - alz2.fYp.aVY)));
                                float min3 = Math.min(Math.abs((float) (d3 - alz2.fYn.aVY)), Math.abs((float) (d3 - alz2.fYq.aVY)));
                                if ((min * min) + (min2 * min2) + (min3 * min3) <= f2 && !ahu.mo13625a(alz2)) {
                                    int i10 = i4 + 1;
                                    return;
                                }
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        }
    }

    /* renamed from: a */
    public void mo1228a(Vec3d ajr, Vec3d ajr2, C6178ahu ahu, List<C6615aqP> list) {
        Vec3d ajr3 = new Vec3d();
        Vec3d ajr4 = new Vec3d();
        this.fnH.mo13986a((Tuple3d) ajr, (Tuple3d) ajr3);
        this.fnH.mo13986a((Tuple3d) ajr2, (Tuple3d) ajr4);
        double min = Math.min(ajr3.x, ajr4.x);
        double max = Math.max(ajr3.x, ajr4.x);
        double min2 = Math.min(ajr3.y, ajr4.y);
        double max2 = Math.max(ajr3.y, ajr4.y);
        double min3 = Math.min(ajr3.z, ajr4.z);
        double max3 = Math.max(ajr3.z, ajr4.z);
        int a = m21352a(this.fnD, 0, min);
        if (a < this.fnD.size()) {
            int a2 = m21352a(this.fnD, a + 1, max);
            if (a2 >= this.fnD.size()) {
                a2 = this.fnD.size() - 1;
            }
            int i = a;
            int i2 = this.fnD.mo16250uA(a).aVZ;
            while (i2 > 0) {
                int i3 = i - 1;
                if (i3 < 0) {
                    break;
                }
                C3311qA uA = this.fnD.mo16250uA(i3);
                i2 += uA.mo21298Xj() ? -1 : 1;
                if (!uA.mo21298Xj()) {
                    i = i3;
                } else {
                    C6365alZ alz = uA.aVW;
                    if (alz.fYo.aVX < a) {
                        i = i3;
                    } else if (alz.fYq.aVY < min3) {
                        i = i3;
                    } else if (alz.fYn.aVY > max3) {
                        i = i3;
                    } else if (alz.fYp.aVY < min2) {
                        i = i3;
                    } else if (alz.fYm.aVY > max2) {
                        i = i3;
                    } else {
                        if (mo13185b(alz, ajr3, ajr4)) {
                            if (!list.contains(alz.dhC())) {
                                ahu.mo13625a(alz);
                                return;
                            }
                        }
                        i = i3;
                    }
                }
            }
            int i4 = (a2 - a) + 1;
            int i5 = a - 1;
            while (true) {
                i4--;
                if (i4 > 0) {
                    i5++;
                    C3311qA uA2 = this.fnD.mo16250uA(i5);
                    if (uA2.mo21298Xj()) {
                        C6365alZ alz2 = uA2.aVW;
                        if (alz2.fYq.aVY >= min3 && alz2.fYn.aVY <= max3 && alz2.fYp.aVY >= min2 && alz2.fYm.aVY <= max2 && mo13185b(alz2, ajr3, ajr4)) {
                            if (!list.contains(alz2.dhC())) {
                                ahu.mo13625a(alz2);
                                return;
                            }
                        }
                    }
                } else {
                    return;
                }
            }
        }
    }

    /* renamed from: b */
    public boolean mo13185b(C6365alZ alz, Vec3d ajr, Vec3d ajr2) {
        double d;
        double d2;
        double d3;
        Vec3d f = ajr2.mo9517f((Tuple3d) ajr);
        Vec3d ajr3 = new Vec3d(Math.max(alz.fYo.aVY, alz.fYl.aVY), Math.max(alz.fYp.aVY, alz.fYm.aVY), Math.max(alz.fYq.aVY, alz.fYn.aVY));
        Vec3d ajr4 = new Vec3d(Math.min(alz.fYo.aVY, alz.fYl.aVY), Math.min(alz.fYp.aVY, alz.fYm.aVY), Math.min(alz.fYq.aVY, alz.fYn.aVY));
        if (ajr.x < ajr4.x) {
            double d4 = ajr4.x - ajr.x;
            if (d4 > f.x) {
                return false;
            }
            d = d4 / f.x;
        } else if (ajr.x > ajr3.x) {
            double d5 = ajr3.x - ajr.x;
            if (d5 < f.x) {
                return false;
            }
            d = d5 / f.x;
        } else {
            d = -1.0d;
        }
        if (ajr.y < ajr4.y) {
            double d6 = ajr4.y - ajr.y;
            if (d6 > f.y) {
                return false;
            }
            d2 = d6 / f.y;
        } else if (ajr.y > ajr3.y) {
            double d7 = ajr3.y - ajr.y;
            if (d7 < f.y) {
                return false;
            }
            d2 = d7 / f.y;
        } else {
            d2 = -1.0d;
        }
        if (ajr.z < ajr4.z) {
            double d8 = ajr4.z - ajr.z;
            if (d8 > f.z) {
                return false;
            }
            d3 = d8 / f.z;
        } else if (ajr.z > ajr3.z) {
            double d9 = ajr3.z - ajr.z;
            if (d9 < f.z) {
                return false;
            }
            d3 = d9 / f.z;
        } else {
            d3 = -1.0d;
        }
        char c = 0;
        if (d2 > d) {
            c = 1;
            d = d2;
        }
        if (d3 > d) {
            c = 2;
            d = d3;
        }
        switch (c) {
            case 0:
                double d10 = ajr.y + (f.y * d);
                if (d10 < ajr4.y || d10 > ajr3.y) {
                    return false;
                }
                double d11 = (d * f.z) + ajr.z;
                if (d11 < ajr4.z || d11 > ajr3.z) {
                    return false;
                }
                break;
            case 1:
                double d12 = ajr.x + (f.x * d);
                if (d12 < ajr4.x || d12 > ajr3.x) {
                    return false;
                }
                double d13 = (d * f.z) + ajr.z;
                if (d13 < ajr4.z || d13 > ajr3.z) {
                    return false;
                }
                break;
            case 2:
                double d14 = ajr.x + (f.x * d);
                if (d14 < ajr4.x || d14 > ajr3.x) {
                    return false;
                }
                double d15 = (d * f.y) + ajr.y;
                if (d15 < ajr4.y || d15 > ajr3.y) {
                    return false;
                }
                break;
        }
        return true;
    }

    public void dispose() {
        this.fnD.clear();
        this.fnE.clear();
        this.fnF.clear();
        this.fnG[0] = null;
        this.fnG[1] = null;
        this.fnG[2] = null;
        this.fnG = null;
        this.cIj.clear();
        this.fnS.clear();
        this.fnT.clear();
        this.inserted.clear();
        this.fnU.clear();
        this.removed.clear();
    }
}
