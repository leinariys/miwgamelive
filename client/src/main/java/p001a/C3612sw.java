package p001a;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.sw */
/* compiled from: a */
public final class C3612sw {
    public static final C3612sw bhJ = new C3612sw("GrannyBSplineSolverEvaluateAsQuaternions", grannyJNI.GrannyBSplineSolverEvaluateAsQuaternions_get());
    public static final C3612sw bhK = new C3612sw("GrannyBSplineSolverAllowC0Splitting", grannyJNI.GrannyBSplineSolverAllowC0Splitting_get());
    public static final C3612sw bhL = new C3612sw("GrannyBSplineSolverAllowC1Splitting", grannyJNI.GrannyBSplineSolverAllowC1Splitting_get());
    public static final C3612sw bhM = new C3612sw("GrannyBSplineSolverExtraDOFKnotZero", grannyJNI.GrannyBSplineSolverExtraDOFKnotZero_get());
    public static final C3612sw bhN = new C3612sw("GrannyBSplineSolverForceEndpointAlignment", grannyJNI.GrannyBSplineSolverForceEndpointAlignment_get());
    public static final C3612sw bhO = new C3612sw("GrannyBSplineSolverAllowReduceKeys", grannyJNI.GrannyBSplineSolverAllowReduceKeys_get());
    private static C3612sw[] bhP = {bhJ, bhK, bhL, bhM, bhN, bhO};

    /* renamed from: pF */
    private static int f9166pF = 0;

    /* renamed from: pG */
    private final int f9167pG;

    /* renamed from: pH */
    private final String f9168pH;

    private C3612sw(String str) {
        this.f9168pH = str;
        int i = f9166pF;
        f9166pF = i + 1;
        this.f9167pG = i;
    }

    private C3612sw(String str, int i) {
        this.f9168pH = str;
        this.f9167pG = i;
        f9166pF = i + 1;
    }

    private C3612sw(String str, C3612sw swVar) {
        this.f9168pH = str;
        this.f9167pG = swVar.f9167pG;
        f9166pF = this.f9167pG + 1;
    }

    /* renamed from: ew */
    public static C3612sw m39209ew(int i) {
        if (i < bhP.length && i >= 0 && bhP[i].f9167pG == i) {
            return bhP[i];
        }
        for (int i2 = 0; i2 < bhP.length; i2++) {
            if (bhP[i2].f9167pG == i) {
                return bhP[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C3612sw.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f9167pG;
    }

    public String toString() {
        return this.f9168pH;
    }
}
