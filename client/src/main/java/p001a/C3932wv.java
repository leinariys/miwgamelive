package p001a;

/* renamed from: a.wv */
/* compiled from: a */
public class C3932wv {
    public static final int bDA = 8;
    public static final int bDB = 9;
    public static final int bDC = 20;
    public static final int bDD = 10;
    public static final int bDE = 11;
    public static final int bDF = 12;
    public static final int bDG = 13;
    public static final int bDH = 14;
    public static final int bDt = 1;
    public static final int bDu = 2;
    public static final int bDv = 3;
    public static final int bDw = 4;
    public static final int bDx = 5;
    public static final int bDy = 6;
    public static final int bDz = 7;
    private int codeStepLoad;
    private float bDr;
    private float bDs;
    private String msg;

    public C3932wv(int i) {
        this(i, (String) null, 0.0f, 0.0f);
    }

    public C3932wv(int i, String str) {
        this(i, str, 0.0f, 0.0f);
    }

    public C3932wv(int i, String str, float f, float f2) {
        this.codeStepLoad = i;
        this.msg = str;
        this.bDr = f;
        this.bDs = f2;
    }

    public int codeStepLoad() {
        return this.codeStepLoad;
    }

    public String amz() {
        return this.msg;
    }

    public float amA() {
        return this.bDr;
    }

    public float amB() {
        return this.bDs;
    }
}
