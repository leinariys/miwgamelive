package p001a;

import game.engine.DataGameEvent;
import game.engine.SocketMessage;
import game.io.IExternalIO;
import game.network.message.ByteMessageReader;
import game.network.message.IReadProto;
import game.network.message.WriterBitsData;
import game.network.message.serializable.C1695Yw;
import game.network.message.serializable.C2436fS_1;
import gnu.trove.TLongArrayList;
import jdbm.RecordManager;
import jdbm.RecordManagerFactory;
import jdbm.btree.BTree;
import logic.baa.C1616Xf;
import logic.data.mbean.C0677Jd;
import logic.data.mbean.C6064afk;
import logic.res.html.MessageContainer;
import logic.thred.LogPrinter;
import logic.thred.PoolThread;
import logic.thred.ThreadWrapper;
import logic.ui.item.ListJ;
import org.apache.commons.logging.Log;
import taikodom.infra.script.repository.jdbm.jmx.JDBMConsole;
import taikodom.render.loader.provider.FilePath;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: a.aBR */
/* compiled from: a */
public class WraperDbFileImpl implements WraperDbFile {
    /* access modifiers changed from: private */
    public static final Log logger = LogPrinter.setClass(WraperDbFileImpl.class);
    private static final boolean guq = false;
    private static final String REPOSITORY = "repository";
    private static final String REPOSITORY_TEMP = "repository-temp";
    private static final String REFERENCE_TREE = "reference-tree";
    private static final String REFERENCE_TREE_TEMP = "reference-tree-temp";
    private static final String REPOSITORY_LAST_ID = "repository.lastId";
    private static boolean iFO = true;
    /* access modifiers changed from: private */
    public ReentrantReadWriteLock iFQ;
    /* access modifiers changed from: private */
    public BTree bTreeReferenceTree;
    /* access modifiers changed from: private */
    public BTree bTreeReferenceTreeTemp;
    private BTree bTreeRepository;
    private BTree bTreeRepositoryTemp;
    /* access modifiers changed from: private */
    public boolean iGa;
    private DataGameEvent aGA;
    private boolean disposed;
    private FilePath eKA;
    private LinkedBlockingQueue<C1695Yw> gur;
    private ThreadWrapper gus;
    private C1412Uh guy;
    private ConcurrentHashMap<Class, Boolean> iFP;
    private RecordManager recordManager;

    private long recid;
    private String nameFileEnvironment;
    //iFX
    private boolean isBTreeNewRepository;

    private boolean iFZ;
    private JDBMConsole iGb;
    private int iGc;
    private Properties props;

    public WraperDbFileImpl(boolean z, Properties properties) {
        this.gur = new LinkedBlockingQueue<>();
        this.iFP = new ConcurrentHashMap<>();
        this.iFQ = new ReentrantReadWriteLock();
        this.nameFileEnvironment = "environment";
        this.iFZ = false;
        this.iGa = false;
        this.iGc = 0;
        this.props = properties;
        iFO = z;
        this.gus = new ThreadWrapper(new C1770b(), "JDBM Repository transaction dumper");
        this.gus.setDaemon(true);
    }

    public WraperDbFileImpl(boolean z) {
        this(z, (Properties) null);
    }

    public WraperDbFileImpl() {
        this(true, (Properties) null);
    }

    public String dqh() {
        return this.nameFileEnvironment;
    }

    public void setDbname(String str) {
        this.nameFileEnvironment = str;
    }

    /* renamed from: a */
    public void mo5219a(C1412Uh uh) {
        this.guy = uh;
        this.aGA = (DataGameEvent) uh;
    }

    public FilePath aQe() {
        return this.eKA;
    }

    /* renamed from: f */
    public void mo5234f(FilePath ain) {
        this.eKA = ain;
    }

    /* renamed from: c */
    public void mo5226c(C1695Yw yw) {
        try {
            this.gur.put(yw);
            m12923d(yw);
        } catch (InterruptedException e) {
            throw new CountFailedReadOrWriteException((Throwable) e);
        }
    }

    public boolean dqi() {
        if (this.iFZ) {
            return false;
        }
        this.iFZ = true;
        return true;
    }

    public void dqj() {
        this.iFZ = false;
        this.iGa = true;
        Executors.newSingleThreadExecutor().execute(new C1772d());
    }

    /* renamed from: nr */
    public void mo7910nr(String str) {
        this.iFQ.writeLock().lock();
        try {
            this.recordManager.commit();
            this.recordManager.close();

            File BG = this.eKA.concat(String.valueOf(this.nameFileEnvironment) + ".db").getFile();
            File BG2 = this.eKA.concat(String.valueOf(this.nameFileEnvironment) + ".lg").getFile();
            BG.renameTo(this.eKA.concat("environment-backup.db").getFile());
            BG2.renameTo(this.eKA.concat("environment-backup.lg").getFile());
            File BG3 = this.eKA.concat(String.valueOf(str) + ".db").getFile();
            File BG4 = this.eKA.concat(String.valueOf(str) + ".lg").getFile();
            BG3.renameTo(this.eKA.concat(String.valueOf(this.nameFileEnvironment) + ".db").getFile());
            BG4.renameTo(this.eKA.concat(String.valueOf(this.nameFileEnvironment) + ".lg").getFile());
            initBTree();
        } finally {
            this.iFQ.writeLock().unlock();
        }
    }

    /* access modifiers changed from: protected */
    public void run() {
        ListJ dgK = MessageContainer.init();
        while (!this.disposed) {
            m12916R(dgK);
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: R */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m12916R(java.util.List<C1695Yw> r13) {
        /*
            r12 = this;
            r2 = 50
            p001a.C0917NT.sleep(r2)     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            r13.clear()     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            java.util.concurrent.LinkedBlockingQueue<a.Yw> r2 = r12.gur     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            int r2 = r2.size()     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            if (r2 <= 0) goto L_0x001c
            java.util.concurrent.LinkedBlockingQueue<a.Yw> r2 = r12.gur     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            r2.drainTo(r13)     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
        L_0x0015:
            int r2 = r13.size()     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            if (r2 != 0) goto L_0x0031
        L_0x001b:
            return
        L_0x001c:
            java.util.concurrent.LinkedBlockingQueue<a.Yw> r2 = r12.gur     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            java.lang.Object r2 = r2.take()     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            a.Yw r2 = (game.network.message.serializable.C1695Yw) r2     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            r13.add(r2)     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            goto L_0x0015
        L_0x0028:
            r2 = move-exception
            org.apache.commons.logging.Log r2 = logger
            java.lang.String r3 = "queue disposed"
            r2.warn(r3)
            goto L_0x001b
        L_0x0031:
            java.util.concurrent.locks.ReentrantReadWriteLock r2 = r12.iFQ     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r2 = r2.readLock()     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            r2.lock()     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            int r2 = r13.size()     // Catch:{ Exception -> 0x00c2 }
        L_0x003e:
            int r6 = r2 + -1
            if (r6 >= 0) goto L_0x0071
        L_0x0042:
            jdbm.RecordManager r2 = r12.iFR     // Catch:{ Exception -> 0x00c2 }
            long r4 = r12.iFV     // Catch:{ Exception -> 0x00c2 }
            a.Jz r3 = r12.aGA     // Catch:{ Exception -> 0x00c2 }
            long r6 = r3.getNextId()     // Catch:{ Exception -> 0x00c2 }
            java.lang.Long r3 = java.lang.Long.valueOf(r6)     // Catch:{ Exception -> 0x00c2 }
            r2.update(r4, r3)     // Catch:{ Exception -> 0x00c2 }
            jdbm.RecordManager r2 = r12.iFR     // Catch:{ Exception -> 0x00c2 }
            r2.commit()     // Catch:{ Exception -> 0x00c2 }
            int r2 = r12.iGc     // Catch:{ Exception -> 0x00c2 }
            int r2 = r2 + 1
            r12.iGc = r2     // Catch:{ Exception -> 0x00c2 }
        L_0x005e:
            java.util.concurrent.locks.ReentrantReadWriteLock r2 = r12.iFQ     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r2 = r2.readLock()     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            r2.unlock()     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            goto L_0x001b
        L_0x0068:
            r2 = move-exception
            org.apache.commons.logging.Log r2 = logger
            java.lang.String r3 = "queue disposed"
            r2.warn(r3)
            goto L_0x001b
        L_0x0071:
            boolean r2 = game.engine.C0700Jz.eKb     // Catch:{ Exception -> 0x00c2 }
            if (r2 != 0) goto L_0x0042
            java.lang.Object r2 = r13.get(r6)     // Catch:{ Exception -> 0x00c2 }
            a.Yw r2 = (game.network.message.serializable.C1695Yw) r2     // Catch:{ Exception -> 0x00c2 }
            java.util.List r3 = r2.bHD()     // Catch:{ Exception -> 0x00c2 }
            java.util.Iterator r4 = r3.iterator()     // Catch:{ Exception -> 0x00c2 }
        L_0x0083:
            boolean r3 = r4.hasNext()     // Catch:{ Exception -> 0x00c2 }
            if (r3 != 0) goto L_0x0097
            java.util.List r3 = r2.bHG()     // Catch:{ Exception -> 0x00c2 }
            int r3 = r3.size()     // Catch:{ Exception -> 0x00c2 }
        L_0x0091:
            int r5 = r3 + -1
            if (r5 >= 0) goto L_0x00ef
            r2 = r6
            goto L_0x003e
        L_0x0097:
            java.lang.Object r3 = r4.next()     // Catch:{ Exception -> 0x00c2 }
            a.AD r3 = (game.network.message.serializable.C0010AD) r3     // Catch:{ Exception -> 0x00c2 }
            a.Jz r5 = r12.aGA     // Catch:{ Exception -> 0x00bb }
            java.lang.Class r7 = r3.mo37hD()     // Catch:{ Exception -> 0x00bb }
            a.apO r3 = r3.mo36hC()     // Catch:{ Exception -> 0x00bb }
            long r8 = r3.cqo()     // Catch:{ Exception -> 0x00bb }
            java.lang.Object r3 = r5.mo3365b((java.lang.Class<?>) r7, (long) r8)     // Catch:{ Exception -> 0x00bb }
            a.Xf r3 = (logic.baa.C1616Xf) r3     // Catch:{ Exception -> 0x00bb }
            a.arL r3 = r3.bFf()     // Catch:{ Exception -> 0x00bb }
            a.se r3 = (p001a.C3582se) r3     // Catch:{ Exception -> 0x00bb }
            r12.mo5238i(r3)     // Catch:{ Exception -> 0x00bb }
            goto L_0x0083
        L_0x00bb:
            r3 = move-exception
            org.apache.commons.logging.Log r5 = logger     // Catch:{ Exception -> 0x00c2 }
            r5.error(r3)     // Catch:{ Exception -> 0x00c2 }
            goto L_0x0083
        L_0x00c2:
            r2 = move-exception
            org.apache.commons.logging.Log r3 = logger     // Catch:{ all -> 0x00ce }
            r3.error(r2)     // Catch:{ all -> 0x00ce }
            jdbm.RecordManager r2 = r12.iFR     // Catch:{ all -> 0x00ce }
            r2.rollback()     // Catch:{ all -> 0x00ce }
            goto L_0x005e
        L_0x00ce:
            r2 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r3 = r12.iFQ     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r3 = r3.readLock()     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            r3.unlock()     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
            throw r2     // Catch:{ InterruptedException -> 0x0028, axI -> 0x0068, Exception -> 0x00d9 }
        L_0x00d9:
            r2 = move-exception
            org.apache.commons.logging.Log r3 = logger
            java.lang.String r4 = "Exception while saving transaction log"
            r3.error(r4, r2)
            org.apache.commons.logging.Log r2 = logger
            java.lang.String r3 = "SHUTTING DOWN!"
            r2.error(r3)
            r2 = 333(0x14d, float:4.67E-43)
            java.lang.System.exit(r2)
            goto L_0x001b
        L_0x00ef:
            java.util.List r3 = r2.bHG()     // Catch:{ Exception -> 0x00c2 }
            java.lang.Object r3 = r3.get(r5)     // Catch:{ Exception -> 0x00c2 }
            r0 = r3
            a.KO r0 = (game.network.message.externalizable.C0722KO) r0     // Catch:{ Exception -> 0x00c2 }
            r4 = r0
            a.arL r3 = r4.bdg()     // Catch:{ Exception -> 0x0130 }
            a.se r3 = (p001a.C3582se) r3     // Catch:{ Exception -> 0x0130 }
            a.Jd r7 = r4.bdh()     // Catch:{ Exception -> 0x0130 }
            long r8 = r7.aBt()     // Catch:{ Exception -> 0x0130 }
            r0 = r3
            a.af r0 = (p001a.C1875af) r0     // Catch:{ Exception -> 0x0130 }
            r4 = r0
            r4.mo13222dm()     // Catch:{ Exception -> 0x0130 }
            long r10 = r3.cVe()     // Catch:{ all -> 0x0129 }
            int r4 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r4 <= 0) goto L_0x0121
            r12.mo7898a((p001a.C3582se) r3, (logic.data.mbean.C6064afk) r7)     // Catch:{ all -> 0x0129 }
            r3.mo22001kv(r8)     // Catch:{ all -> 0x0129 }
            r7.aXy()     // Catch:{ all -> 0x0129 }
        L_0x0121:
            a.af r3 = (p001a.C1875af) r3     // Catch:{ Exception -> 0x0130 }
            r3.mo13220dk()     // Catch:{ Exception -> 0x0130 }
            r3 = r5
            goto L_0x0091
        L_0x0129:
            r4 = move-exception
            a.af r3 = (p001a.C1875af) r3     // Catch:{ Exception -> 0x0130 }
            r3.mo13220dk()     // Catch:{ Exception -> 0x0130 }
            throw r4     // Catch:{ Exception -> 0x0130 }
        L_0x0130:
            r3 = move-exception
            org.apache.commons.logging.Log r4 = logger     // Catch:{ Exception -> 0x00c2 }
            r4.error(r3)     // Catch:{ Exception -> 0x00c2 }
            r3 = r5
            goto L_0x0091
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.aBR.m12916R(java.util.List):void");
    }

    /* renamed from: d */
    private void m12923d(C1695Yw yw) {
    }

    /* renamed from: HY */
    public void mo5218HY() {
        int i = 0;
        while (i < 100) {
            try {
                if (this.gur.size() <= 0) {
                    break;
                }
                PoolThread.sleep(100);
                i++;
            } catch (InterruptedException e) {
                this.recordManager.close();
                throw new CountFailedReadOrWriteException((Throwable) e);
            }
        }
        if (this.gur.size() > 0) {
            throw new IOException("Still have pending transactions");
        }
        this.disposed = true;
        if (this.gus.isAlive()) {
            this.gus.join(200);
            this.gus.interrupt();
            this.gus.join();
        }
        this.recordManager.close();
    }

    public boolean bOd() {
        long currentTimeMillis = System.currentTimeMillis();
        if (this.isBTreeNewRepository) {
            return false;
        }
        logger.info("Reading...");
        mo5236h((C3582se) this.aGA.getTaikodom().bFf());
        logger.info(String.format("done. (took %.2fs)", new Object[]{Double.valueOf(((double) (System.currentTimeMillis() - currentTimeMillis)) / 1000.0d)}));
        return true;
    }

    /* renamed from: h */
    public void mo5236h(C3582se seVar) {
        C6064afk cVo = seVar.cVo();
        this.iFQ.readLock().lock();
        try {
            byte[] bArr = (byte[]) this.bTreeRepository.find(Long.valueOf(seVar.getObjectId().getId()));
            if (bArr != null) {
                m12919a(cVo, bArr);
                cVo.mo3146A(false);
                return;
            }
            cVo.mo3146A(false);
            cVo.mo3191cS(true);
            seVar.delete();
        } finally {
            this.iFQ.readLock().unlock();
        }
    }

    /* renamed from: i */
    public void mo5238i(C3582se seVar) {
        mo7898a(seVar, seVar.cVo());
    }

    /* renamed from: a */
    public void mo7898a(C3582se seVar, C6064afk afk) {
        long cqo = seVar.getObjectId().getId();
        try {
            if (seVar.mo10528du()) {
                mo7909lO(cqo);
                return;
            }
            SocketMessage ala = new SocketMessage();
            C1773e eVar = new C1773e(this.aGA, ala, false, false);
            byte[] a = m12920a(afk, ala, eVar);
            C0677Jd jd = (C0677Jd) afk.mo11944yn().mo13W();
            if (iFO) {
                m12919a((C6064afk) jd, a);
                SocketMessage ala2 = new SocketMessage();
                byte[] a2 = m12920a(jd, ala2, new C3380qs(this.aGA, ala2, false, false));
                if (this.iFP.putIfAbsent(afk.mo11944yn().getClass(), true) == null && !Arrays.equals(a, a2)) {
                    logger.warn("no match for serialization-deserialization-serialization test of " + seVar.bFY() + " version " + afk.aBt());
                }
            }
            this.bTreeRepository.insert(Long.valueOf(cqo), a, true);
            BTree bTree = this.bTreeReferenceTree;
            if (this.iFZ) {
                bTree = this.bTreeReferenceTreeTemp;
            }
            mo7897a(seVar.getObjectId().getId(), afk.aBt(), eVar.djN(), bTree);
        } catch (IOException e) {
            throw new CountFailedReadOrWriteException("Error saving object: " + seVar.bFY(), e);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: lO */
    public void mo7909lO(long j) {
        System.out.println("trying to delete " + j);
        if (this.bTreeRepository.find(Long.valueOf(j)) != null) {
            System.out.println("really deleting " + j);
            this.bTreeRepository.remove(Long.valueOf(j));
        }
        BTree bTree = this.bTreeReferenceTree;
        if (this.iFZ) {
            bTree = this.bTreeReferenceTreeTemp;
        }
        if (bTree.find(Long.valueOf(j)) != null) {
            bTree.remove(Long.valueOf(j));
        }
    }

    /* renamed from: a */
    private byte[] m12920a(C6064afk afk, SocketMessage ala, C3380qs qsVar) {
        qsVar.mo16273g("class", afk.mo11944yn().getClass());
        qsVar.writeString(C0891Mw.DATA);
        ((IExternalIO) afk).writeExternal(qsVar);
        qsVar.mo16277pe();
        qsVar.flush();
        return ala.toByteArray();
    }

    /* renamed from: a */
    private void m12919a(C6064afk afk, byte[] bArr) {
        C1771C1 cVar = new C1771C1(this.aGA, new ByteMessageReader(bArr), false, false);
        cVar.mo10816jY(true);
        try {
            Class<?> cls = (Class) cVar.mo6366hd("class");
            cVar.mo6369hg(C0891Mw.DATA);
            if (cls != afk.mo11944yn().getClass()) {
                logger.error("EXTREMELY DANGEROUS ERROR: wrong class " + cls + " for " + afk.mo11944yn().bFf().bFY());
            }
            ((IExternalIO) afk).readExternal(cVar);
            cVar.mo6370pe();
        } catch (ClassNotFoundException e) {
            try {
                new C2683iW().mo19626a((IReadProto) new ByteMessageReader(bArr));
            } catch (Exception e2) {
            }
            throw new CountFailedReadOrWriteException("Error reading object: " + afk.mo11944yn().bFf().bFY(), e);
        } catch (Exception e3) {
            try {
                new C2683iW().mo19626a((IReadProto) new ByteMessageReader(bArr));
            } catch (Exception e4) {
                System.out.println(e4);
            }
            throw new CountFailedReadOrWriteException((Throwable) e3);
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean bOc() {
        logger.info("Persist start");
        this.iFQ.writeLock().lock();
        try {
            int i = 0;
            int i2 = 0;
            for (C3582se next : this.aGA.bGA()) {
                C6064afk cVo = next.cVo();
                i2++;
                if (cVo.aBt() > next.cVe() && next.mo6901yn().mo11T().mo15115Ew() != C1020Ot.C1021a.NOT_PERSISTED) {
                    mo7898a(next, cVo);
                    next.mo22001kv(cVo.aBt());
                    i++;
                }
            }
            this.recordManager.update(this.recid, Long.valueOf(this.aGA.getNextId()));
            this.recordManager.commit();
            this.iFQ.writeLock().unlock();
            logger.info("Done persisting " + i + " scripts written of " + i2 + " found in memory.");
            return true;
        } catch (Throwable th) {
            this.iFQ.writeLock().unlock();
            throw th;
        }
    }

    public void commit() {
        this.recordManager.commit();
    }

    public void bOf() {
    }

    public void flush() {
    }

    public void bOe() {
    }

    public void start() {
        this.gus.start();
    }

    public void init() {
        initBTree();
        this.recid = this.recordManager.getNamedObject(REPOSITORY_LAST_ID);
        if (this.recid != 0) {
            this.aGA.setNextId(((Long) this.recordManager.fetch(this.recid)).longValue() + 1);
        } else {
            this.recid = this.recordManager.insert(Long.valueOf(this.aGA.getNextId()));
            this.recordManager.setNamedObject(REPOSITORY_LAST_ID, this.recid);
            this.recordManager.commit();
            this.isBTreeNewRepository = true;
            logger.warn("Created a new empty repository");
        }
        newRegisterMBean();
    }

    /* access modifiers changed from: protected  public void dqk ()  */
    public void initBTree() throws IOException {
        this.eKA.mo2251BD();
        String path = this.eKA.concat(this.nameFileEnvironment).getPath();
        logger.info("Opening repository: " + path);
        this.recordManager = RecordManagerFactory.createRecordManager(path, new Properties());
        this.bTreeRepository = getBTree(REPOSITORY, new LongComparator(), new LongSerializerCustom(), new ByteArraySerializer(), 256);
        this.bTreeRepositoryTemp = getBTree(REPOSITORY_TEMP, new LongComparator(), new LongSerializerCustom(), new ByteArraySerializer(), 256);
        this.bTreeReferenceTree = getBTree(REFERENCE_TREE, new LongComparator(), new LongSerializerCustom(), new ByteArraySerializer(), 256);
        this.bTreeReferenceTreeTemp = getBTree(REFERENCE_TREE_TEMP, new LongComparator(), new LongSerializerCustom(), new ByteArraySerializer(), 256);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public BTree getBTree(String str, Comparator<?> comparator, Serializer serializerKey, Serializer serializerValue, int pageSize) throws IOException {
        long namedObject = this.recordManager.getNamedObject(str);
        if (namedObject != 0) {
            return BTree.load(this.recordManager, namedObject);
        }
        BTree createInstance = BTree.createInstance(this.recordManager, comparator, serializerKey, serializerValue, pageSize);
        this.recordManager.setNamedObject(str, createInstance.getRecid());
        logger.warn("Created a new btree: " + str);
        this.recordManager.commit();
        return createInstance;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo7897a(long j, long j2, TLongArrayList tLongArrayList, BTree bTree) {
        long j3;
        int size = tLongArrayList.size();
        byte[] bArr = new byte[((size + 1) * 8)];
        int i = -1;
        for (int i2 = -1; i2 < size; i2++) {
            if (i2 >= 0) {
                j3 = tLongArrayList.get(i2);
            } else {
                j3 = j2;
            }
            int i3 = i + 1;
            bArr[i3] = (byte) ((int) (j3 >> 56));
            int i4 = i3 + 1;
            bArr[i4] = (byte) ((int) (j3 >> 48));
            int i5 = i4 + 1;
            bArr[i5] = (byte) ((int) (j3 >> 40));
            int i6 = i5 + 1;
            bArr[i6] = (byte) ((int) (j3 >> 32));
            int i7 = i6 + 1;
            bArr[i7] = (byte) ((int) (j3 >> 24));
            int i8 = i7 + 1;
            bArr[i8] = (byte) ((int) (j3 >> 16));
            int i9 = i8 + 1;
            bArr[i9] = (byte) ((int) (j3 >> 8));
            i = i9 + 1;
            bArr[i] = (byte) ((int) j3);
        }
        bTree.insert(Long.valueOf(j), bArr, true);
    }

    public C2983mb dql() {
        return new C2983mb(this.bTreeRepository.browse());
    }

    /* renamed from: e */
    public ListJ<C3582se> mo7908e(Class<?>... clsArr) {
        ArrayList arrayList = new ArrayList();
        C2983mb dql = dql();
        C2983mb.C2984a aVar = new C2983mb.C2984a();
        while (dql.mo20571a(aVar)) {
            long MW = aVar.mo20572MW();
            C1769A1 aVar2 = new C1769A1(this.aGA, new ByteMessageReader((byte[]) aVar.getValue()), false, false);
            aVar2.mo10816jY(true);
            Class cls = (Class) aVar2.mo6366hd("class");
            int length = clsArr.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                } else if (clsArr[i].equals(cls)) {
                    C1616Xf xf = (C1616Xf) this.aGA.mo3365b((Class<?>) cls, MW);
                    if (xf.bFf().bFT()) {
                        mo5236h((C3582se) xf.bFf());
                    }
                    arrayList.add((C3582se) xf.bFf());
                } else {
                    i++;
                }
            }
        }
        return arrayList;
    }

    /**
     * aqT
     */
    private void newRegisterMBean() {
        MBeanServer platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
        if (platformMBeanServer != null) {
            this.iGb = new JDBMConsole(this);
            try {
                platformMBeanServer.registerMBean(this.iGb, new ObjectName("Bitverse:name=Repository"));
            } catch (InstanceAlreadyExistsException e) {
                System.err.println(e.getMessage());
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public int dqm() {
        if (this.gur != null) {
            return this.gur.size();
        }
        return 0;
    }

    public int dqn() {
        int i = this.iGc;
        this.iGc = 0;
        return i;
    }

    /* renamed from: a.aBR$b */
    /* compiled from: a */
    class C1770b implements Runnable {
        C1770b() {
        }

        public void run() {
            try {
                WraperDbFileImpl.this.run();
            } catch (IOException e) {
                WraperDbFileImpl.logger.error(e);
            }
        }
    }

    /* renamed from: a.aBR$d */
    /* compiled from: a */
    class C1772d implements Runnable {
        C1772d() {
        }

        public void run() {
            long j;
            Tuple tuple = new Tuple();
            try {
                TupleBrowser browse = WraperDbFileImpl.this.bTreeReferenceTreeTemp.browse();
                while (browse.getNext(tuple)) {
                    WraperDbFileImpl.this.iFQ.writeLock().lock();
                    long e = C2790kB.m34297e((byte[]) tuple.getValue());
                    Long l = (Long) WraperDbFileImpl.this.bTreeReferenceTree.find(tuple.getKey());
                    if (l != null) {
                        j = C2790kB.m34297e((byte[]) WraperDbFileImpl.this.bTreeReferenceTree.find(l));
                    } else {
                        j = -1;
                    }
                    if (e > j) {
                        WraperDbFileImpl.this.bTreeReferenceTree.insert(tuple.getKey(), tuple.getValue(), true);
                    }
                    WraperDbFileImpl.this.bTreeReferenceTreeTemp.remove(tuple.getKey());
                    WraperDbFileImpl.this.iFQ.writeLock().unlock();
                }
                WraperDbFileImpl.this.iGa = false;
            } catch (IOException e2) {
                throw new RuntimeException(e2);
            } catch (Throwable th) {
                WraperDbFileImpl.this.iFQ.writeLock().unlock();
                throw th;
            }
        }
    }

    /* renamed from: a.aBR$e */
    /* compiled from: a */
    private class C1773e extends C3380qs {

        private TLongArrayList irA = new TLongArrayList(1000);

        public C1773e(DataGameEvent jz, WriterBitsData oUVar, boolean z, boolean z2) {
            super(jz, oUVar, z, z2);
        }

        /* access modifiers changed from: protected */
        /* renamed from: i */
        public void mo7917i(String str, Object obj) {
            this.irA.add(((C1616Xf) obj).bFf().getObjectId().getId());
            super.mo7917i(str, obj);
        }

        public TLongArrayList djN() {
            return this.irA;
        }
    }

    /* renamed from: a.aBR$c */
    /* compiled from: a */
    class C1771C1 extends aPZ {
        C1771C1(DataGameEvent jz, IReadProto ie, boolean z, boolean z2) {
            super(jz, ie, z, z2);
        }

        /* access modifiers changed from: protected */
        /* renamed from: A */
        public C2436fS_1.C2437a mo7913A(String str, int i) {
            if (i == 134) {
                return null;
            }
            return mo18653d(i, str);
        }
    }

    /* renamed from: a.aBR$a */
    class C1769A1 extends aPZ {
        C1769A1(DataGameEvent jz, IReadProto ie, boolean z, boolean z2) {
            super(jz, ie, z, z2);
        }

        /* access modifiers changed from: protected */
        /* renamed from: A */
        public C2436fS_1.C2437a mo7913A(String str, int i) {
            if (i == 134) {
                return null;
            }
            return mo18653d(i, str);
        }
    }
}
