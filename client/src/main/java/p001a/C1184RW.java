package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.RW */
/* compiled from: a */
public class C1184RW implements C5852abg.C1855b {
    public final Vec3f ebV = new Vec3f();
    public final Vec3f ebW = new Vec3f();
    public float distance = 1.0E30f;
    public boolean ebX = false;

    /* renamed from: b */
    public void mo5216b(int i, int i2, int i3, int i4) {
    }

    /* renamed from: c */
    public void mo5217c(Vec3f vec3f, Vec3f vec3f2, float f) {
        if (f < this.distance) {
            this.ebX = true;
            this.ebV.set(vec3f);
            this.ebW.set(vec3f2);
            this.distance = f;
        }
    }
}
