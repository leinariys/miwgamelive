package p001a;

import game.script.item.Item;
import game.script.item.ItemType;
import game.script.player.Player;
import logic.aaa.C0633Iy;
import logic.aaa.C1506WA;
import logic.res.ILoaderImageInterface;
import logic.thred.LogPrinter;
import logic.ui.IBaseUiTegXml;
import taikodom.render.graphics2d.RGraphics2;

import javax.swing.*;
import java.awt.*;
import java.awt.image.ImageObserver;

/* renamed from: a.gY */
/* compiled from: a */
public class C2539gY implements Icon {

    /* renamed from: Tb */
    private static final String f7665Tb = "res://imageset_items/";

    /* renamed from: Tc */
    private static final String f7666Tc = "res://data/gui/imageset/imageset_items/";
    private static final LogPrinter logger = LogPrinter.m10275K(C2539gY.class);


    /* renamed from: Td */
    private Image f7667Td;
    @Deprecated

    /* renamed from: Te */
    private String f7668Te;
    @Deprecated

    /* renamed from: Tf */
    private Color f7669Tf;
    @Deprecated

    /* renamed from: Tg */
    private Color f7670Tg;

    /* renamed from: Th */
    private ILoaderImageInterface f7671Th;
    private Object data;
    @Deprecated
    private boolean disabled;
    @Deprecated
    private Insets insets;
    private Dimension size;

    public C2539gY() {
        this.f7669Tf = Color.WHITE;
        this.insets = new Insets(2, 2, 0, 2);
        this.f7670Tg = new Color(0, 0, 0, 200);
        this.f7671Th = IBaseUiTegXml.initBaseUItegXML().adz();
        m32100xN();
    }

    public C2539gY(Image image) {
        this();
        setImage(image);
    }

    public C2539gY(String str) {
        this();
        mo19024aw(str);
    }

    public C2539gY(Item auq) {
        this();
        setData(auq);
        m32099h(auq.bAP());
    }

    public C2539gY(ItemType jCVar) {
        this();
        setData(jCVar);
        m32099h(jCVar);
    }

    /* renamed from: h */
    private void m32099h(ItemType jCVar) {
        mo19024aw(jCVar.mo12100sK().getHandle());
    }

    /* renamed from: xN */
    private void m32100xN() {
    }

    public int getIconHeight() {
        return this.size.height;
    }

    public int getIconWidth() {
        return this.size.width;
    }

    public void paintIcon(Component component, Graphics graphics, int i, int i2) {
        int i3 = this.size.width;
        int i4 = this.size.height;
        graphics.translate(i, i2);
        if (this.f7667Td != null) {
            graphics.drawImage(this.f7667Td, 0, 0, i3, i4, 0, 0, this.f7667Td.getWidth((ImageObserver) null), this.f7667Td.getHeight((ImageObserver) null), component);
        }
        if (this.data != null) {
            if (this.data instanceof Item) {
                if (!((Item) this.data).mo16383bW(C5916acs.getSingolton().mo4089dL())) {
                    graphics.setColor(new Color(255, 0, 0, 125));
                    graphics.fillRect(0, 0, i3, i4);
                    ((RGraphics2) graphics).setColorMultiplier(Color.red);
                }
            } else if ((this.data instanceof ItemType) && !((ItemType) this.data).mo22858m(C5916acs.getSingolton().mo4089dL())) {
                graphics.setColor(new Color(255, 0, 0, 125));
                graphics.fillRect(0, 0, i3, i4);
                ((RGraphics2) graphics).setColorMultiplier(Color.red);
            }
        }
        String str = this.f7668Te;
        if (str != null && str.length() != 0) {
            FontMetrics fontMetrics = graphics.getFontMetrics(graphics.getFont());
            int[] widths = fontMetrics.getWidths();
            int i5 = 0;
            for (char c : str.toCharArray()) {
                i5 += widths[c];
            }
            int i6 = (i3 - i5) - this.insets.right;
            int i7 = i4 - this.insets.bottom;
            graphics.setColor(this.f7670Tg);
            graphics.fillRect(i6 - 1, i7 - fontMetrics.getHeight(), i5, fontMetrics.getHeight());
            graphics.setColor(this.f7669Tf);
            graphics.drawString(str, i6, i7 - 3);
        }
    }

    /* renamed from: aw */
    public void mo19024aw(String str) {
        try {
            setImage(this.f7671Th.getImage(f7666Tc + str));
        } catch (Exception e) {
            logger.error(e);
        }
    }

    public void setImage(Image image) {
        this.f7667Td = image;
        this.size = new Dimension();
        if (this.f7667Td != null) {
            this.size.width = 32;
            this.size.height = 32;
        }
    }

    @Deprecated
    /* renamed from: ax */
    public void mo19025ax(String str) {
        this.f7668Te = str;
    }

    @Deprecated
    /* renamed from: bo */
    public void mo19026bo(int i) {
        if (i > 0) {
            mo19025ax(String.valueOf(i));
        } else {
            mo19025ax((String) null);
        }
    }

    @Deprecated
    /* renamed from: a */
    public void mo19023a(Color color) {
        this.f7669Tf = color;
    }

    public boolean isDisabled() {
        return this.disabled;
    }

    public void setDisabled(boolean z) {
        this.disabled = z;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object obj) {
        Player dL = C5916acs.getSingolton().mo4089dL();
        if (dL != null) {
            if (obj instanceof Item) {
                dL.mo14419f((C1506WA) new C0633Iy(((Item) obj).bAP()));
            } else if (obj instanceof ItemType) {
                dL.mo14419f((C1506WA) new C0633Iy((ItemType) obj));
            }
            this.data = obj;
        }
    }
}
