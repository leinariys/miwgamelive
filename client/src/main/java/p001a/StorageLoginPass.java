package p001a;

/* renamed from: a.YB */
/* compiled from: a */
public class StorageLoginPass {
    private final String password;
    private final String userName;
    private boolean isForcingNewUser;

    public StorageLoginPass(String str, String str2, boolean z) {
        this.userName = str;
        this.password = str2;
        this.isForcingNewUser = z;
    }

    public String getPassword() {
        return this.password;
    }

    public String getUserName() {
        return this.userName;
    }

    public boolean getForcingNewUser() {
        return this.isForcingNewUser;
    }
}
