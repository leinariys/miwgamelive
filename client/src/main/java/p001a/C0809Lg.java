package p001a;

import java.io.OutputStream;

/* renamed from: a.Lg */
/* compiled from: a */
class C0809Lg extends OutputStream {
    OutputStream out;
    private int count = 0;
    private long dtn = 997732322341L;
    private long dto = 999323423421L;
    private long dtp = 999323423241L;
    private int dtq;

    public C0809Lg(OutputStream outputStream) {
        this.out = outputStream;
    }

    public C0809Lg(OutputStream outputStream, long j, long j2, long j3) {
        this.out = outputStream;
        this.dtn = j;
        this.dto = j2;
        this.dtp = j3;
    }

    /* access modifiers changed from: package-private */
    public void beW() {
        this.dtq = this.count;
    }

    /* access modifiers changed from: package-private */
    public void beX() {
        this.dtq = 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: K */
    public int mo3808K(int i, int i2) {
        return (((int) ((this.dtn * (this.dto + ((long) i))) + this.dtp)) ^ i2) & 255;
    }

    public void write(int i) {
        this.out.write(mo3808K(this.count - this.dtq, i));
        this.count++;
    }
}
