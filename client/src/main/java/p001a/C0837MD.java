package p001a;

import gnu.trove.TLongObjectHashMap;
import gnu.trove.TLongObjectIterator;

import java.util.Iterator;

/* renamed from: a.MD */
/* compiled from: a */
public class C0837MD implements C0250DD {
    TLongObjectHashMap<C3102np> iLd = new TLongObjectHashMap<>();

    /* renamed from: aJ */
    public C3102np mo3874aJ(int i, int i2) {
        return (C3102np) this.iLd.get(C6436ams.m24006af(i, i2));
    }

    /* renamed from: a */
    public void mo3873a(C6436ams ams) {
        this.iLd.put(ams.aBm, ams);
    }

    /* renamed from: b */
    public void mo3876b(C3102np npVar) {
        this.iLd.remove(((C6436ams) npVar).aBm);
    }

    /* renamed from: b */
    public void mo3875b(C6436ams ams) {
        this.iLd.remove(ams.aBm);
    }

    /* renamed from: a */
    public C3102np mo3872a(C5487aLf alf, C5487aLf alf2) {
        return (C3102np) this.iLd.remove(C6436ams.m24006af(alf.f3320id, alf2.f3320id));
    }

    public Iterator<C3102np> iterator() {
        return new C0838a();
    }

    public int size() {
        return this.iLd.size();
    }

    public void clear() {
        this.iLd.clear();
    }

    /* renamed from: a.MD$a */
    class C0838a implements Iterator<C3102np> {
        final TLongObjectIterator<C3102np> dBX;

        C0838a() {
            this.dBX = C0837MD.this.iLd.iterator();
        }

        public boolean hasNext() {
            return this.dBX.hasNext();
        }

        /* renamed from: bhn */
        public C3102np next() {
            this.dBX.advance();
            return (C3102np) this.dBX.value();
        }

        public void remove() {
            this.dBX.remove();
        }
    }
}
