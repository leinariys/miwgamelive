package p001a;

import game.network.message.WriteEncryptionOutputStream;
import logic.thred.LogPrinter;
import taikodom.render.textures.DDSLoader;
import util.Syst;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: a.anE  reason: case insensitive filesystem */
/* compiled from: a */
public class C6448anE implements C6965axe {
    private static final int hfo = -1442840574;
    static LogPrinter logger = LogPrinter.m10275K(C6448anE.class);
    private final String name;
    /* access modifiers changed from: private */
    public RandomAccessFile bjy;
    private Map<Long, C1944b> cache;
    private RandomAccessFile hfp;
    private boolean readOnly;

    public C6448anE(String str) {
        this(str, false);
    }

    public C6448anE(String str, boolean z) {
        this.cache = new ConcurrentHashMap();
        this.name = str;
        this.readOnly = z;
        try {
            cIG();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void cIG() {
        this.bjy = new RandomAccessFile(this.name, isReadOnly() ? "r" : "rw");
        if (!this.readOnly) {
            this.hfp = new RandomAccessFile(String.valueOf(this.name) + ".lck", "rw");
            this.readOnly = this.hfp.getChannel().tryLock() == null;
            if (this.readOnly) {
                logger.warn("Could not acquire lock for file " + this.name + ", changes to cache will be ignored.");
            }
        }
        load();
    }

    public boolean isReadOnly() {
        return this.readOnly;
    }

    /* renamed from: ja */
    public byte[] mo14919ja(long j) {
        C1944b bVar = this.cache.get(Long.valueOf(j));
        if (bVar == null) {
            return null;
        }
        byte[] buffer = bVar.getBuffer();
        if (buffer != null) {
            return buffer;
        }
        remove(j);
        return buffer;
    }

    private void load() {
        if (this.bjy.length() < 4) {
            cIH();
        } else if (this.bjy.readInt() != hfo) {
            logger.warn("File outdated or corrupted, reseting " + this.name);
            this.bjy.setLength(0);
            cIH();
        } else {
            byte[] bArr = new byte[DDSLoader.DDSCAPS2_CUBEMAP_NEGATIVEY];
            while (this.bjy.getFilePointer() < this.bjy.length()) {
                long filePointer = this.bjy.getFilePointer();
                int readInt = this.bjy.readInt();
                if (this.bjy.length() - this.bjy.getFilePointer() < ((long) readInt)) {
                    logger.warn("insufficient byte to be read, expecting more " + readInt + ", but only have " + (this.bjy.length() - this.bjy.getFilePointer()));
                    this.bjy.seek(filePointer);
                    return;
                }
                C1943a aVar = C1943a.values()[this.bjy.readByte()];
                long readLong = this.bjy.readLong();
                if (aVar != C1943a.REMOVE) {
                    long filePointer2 = this.bjy.getFilePointer();
                    int i = readInt - 9;
                    if (bArr.length < i) {
                        bArr = new byte[i];
                    }
                    this.bjy.readFully(bArr, 0, i);
                    if (bArr[i - 1] != 1) {
                        logger.warn("verification byte is not set, ignoring last buffer");
                        this.bjy.seek(filePointer);
                        return;
                    }
                    C1944b bVar = new C1944b(this, (C1944b) null);
                    bVar.offset = filePointer2;
                    this.cache.put(Long.valueOf(readLong), bVar);
                } else if (this.bjy.readByte() != 1) {
                    logger.warn("verification byte is not set, ignoring last buffer");
                    this.bjy.seek(filePointer);
                    return;
                } else {
                    this.cache.remove(Long.valueOf(readLong));
                }
            }
        }
    }

    private void cIH() {
        if (!isReadOnly()) {
            this.bjy.writeInt(hfo);
        }
    }

    /* renamed from: a */
    public void mo14913a(long j, byte[] bArr) {
        if (!isReadOnly()) {
            synchronized (this.bjy) {
                C1944b bVar = new C1944b(this, (C1944b) null);
                this.cache.put(Long.valueOf(j), bVar);
                try {
                    this.bjy.seek(this.bjy.length());
                    long filePointer = this.bjy.getFilePointer();
                    this.bjy.writeInt(0);
                    this.bjy.writeByte(C1943a.ADD.ordinal());
                    this.bjy.writeLong(j);
                    bVar.offset = this.bjy.getFilePointer();
                    bVar.mo14921a(this.bjy, bArr);
                    this.bjy.writeByte(0);
                    long filePointer2 = this.bjy.getFilePointer();
                    int filePointer3 = (int) (this.bjy.getFilePointer() - filePointer);
                    this.bjy.seek(filePointer);
                    this.bjy.writeInt(filePointer3 - 4);
                    this.bjy.seek(filePointer2 - 1);
                    this.bjy.writeByte(1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return;
        }
        return;
    }

    /* renamed from: a */
    public void mo14914a(C6965axe.C2008a aVar) {
        for (Map.Entry next : this.cache.entrySet()) {
            byte[] buffer = ((C1944b) next.getValue()).getBuffer();
            if (buffer == null) {
                remove(((Long) next.getKey()).longValue());
            } else {
                aVar.mo16808b(((Long) next.getKey()).longValue(), buffer);
            }
        }
    }

    public void remove(long j) {
        this.cache.remove(Long.valueOf(j));
        synchronized (this.bjy) {
            try {
                this.bjy.seek(this.bjy.length());
                long filePointer = this.bjy.getFilePointer();
                this.bjy.writeInt(0);
                this.bjy.writeByte(C1943a.REMOVE.ordinal());
                this.bjy.writeLong(j);
                this.bjy.writeByte(0);
                long filePointer2 = this.bjy.getFilePointer();
                int filePointer3 = (int) (this.bjy.getFilePointer() - filePointer);
                this.bjy.seek(filePointer);
                this.bjy.writeInt(filePointer3 - 4);
                this.bjy.seek(filePointer2 - 1);
                this.bjy.writeByte(1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void flush() {
        if (!isReadOnly()) {
            try {
                this.bjy.getFD().sync();
            } catch (SyncFailedException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void close() {
        try {
            this.bjy.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            this.hfp.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public void cCc() {
        if (!isReadOnly()) {
            synchronized (this.bjy) {
                HashMap hashMap = new HashMap();
                for (Map.Entry next : this.cache.entrySet()) {
                    byte[] buffer = ((C1944b) next.getValue()).getBuffer();
                    if (buffer != null) {
                        hashMap.put((Long) next.getKey(), buffer);
                    }
                }
                this.cache.clear();
                try {
                    this.bjy.setLength(0);
                    cIH();
                    for (Map.Entry entry : hashMap.entrySet()) {
                        mo14913a(((Long) entry.getKey()).longValue(), (byte[]) entry.getValue());
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    /* renamed from: a.anE$a */
    private enum C1943a {
        ADD,
        REMOVE
    }

    /* renamed from: a.anE$b */
    /* compiled from: a */
    private class C1944b {
        long offset;

        private C1944b() {
        }

        /* synthetic */ C1944b(C6448anE ane, C1944b bVar) {
            this();
        }

        /* renamed from: b */
        public byte[] mo14922b(DataInput dataInput) {
            int readInt = dataInput.readInt();
            byte[] bArr = new byte[readInt];
            byte[] bArr2 = new byte[readInt];
            dataInput.readFully(bArr2);
            new DataInputStream(new aLT(new ByteArrayInputStream(bArr2))).readFully(bArr);
            byte[] bArr3 = new byte[16];
            dataInput.readFully(bArr3);
            if (!Arrays.equals(bArr3, Syst.getMd5(bArr))) {
                return null;
            }
            return bArr;
        }

        /* renamed from: a */
        public void mo14921a(DataOutput dataOutput, byte[] bArr) {
            dataOutput.writeInt(bArr.length);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bArr.length);
            WriteEncryptionOutputStream afj = new WriteEncryptionOutputStream(byteArrayOutputStream);
            afj.write(bArr);
            afj.flush();
            dataOutput.write(byteArrayOutputStream.toByteArray());
            dataOutput.write(Syst.getMd5(bArr));
        }

        public byte[] getBuffer() {
            byte[] b;
            synchronized (C6448anE.this.bjy) {
                try {
                    C6448anE.this.bjy.seek(this.offset);
                    b = mo14922b(C6448anE.this.bjy);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return b;
        }
    }
}
