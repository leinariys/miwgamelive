package p001a;

/* renamed from: a.tU */
/* compiled from: a */
public enum C3666tU {
    DISPOSED(0.0f, "Disposed"),
    VERY_SHORT(2.0f, "Very Short"),
    SHORT(6.0f, "Short"),
    AVERAGE(12.0f, "Average"),
    LONG(24.0f, "Long"),
    VERY_LONG(48.0f, "Very Long");

    private float lifeTime;
    private String string;

    private C3666tU(float f, String str) {
        this.lifeTime = 1000.0f * f * 60.0f * 60.0f;
        this.string = str;
    }

    public String aiP() {
        return this.string;
    }

    public String aiQ() {
        return String.valueOf(this.string) + " (" + getLifeTime() + "h)";
    }

    public String toString() {
        return this.string;
    }

    public float getLifeTime() {
        return this.lifeTime;
    }
}
