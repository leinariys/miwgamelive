package p001a;

import logic.res.ILoaderTrail;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import org.mozilla1.classfile.C0147Bi;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.Mesh;
import taikodom.render.scene.RMesh;
import taikodom.render.scene.SceneObject;
import taikodom.render.textures.Material;

/* renamed from: a.eu */
/* compiled from: a */
public class C2403eu {

    /* renamed from: CL */
    private static final String f7090CL = "corporation_decal";
    private static final Log logger = LogPrinter.setClass(C2403eu.class);

    /* renamed from: a */
    public static void m30125a(String str, String str2, SceneObject sceneObject) {
        if (sceneObject != null) {
            String str3 = String.valueOf(new String(str)) + "_material";
            ILoaderTrail alg = C5916acs.getSingolton().getLoaderTrail();
            alg.mo4999as("data/corporations/" + str3 + ".pro");
            RenderAsset asset = alg.getAsset(str3);
            if (asset != null) {
                String str4 = "data/meshes/" + str2 + C0147Bi.SEPARATOR + str2 + "_decal.gr2";
                try {
                    alg.mo4999as(str4);
                } catch (Exception e) {
                    logger.warn("Unable to load decal mesh for " + str2);
                }
                RenderAsset asset2 = alg.getAsset(String.valueOf(str4) + "/mesh/decal");
                if (asset2 != null) {
                    RMesh rMesh = new RMesh();
                    rMesh.setName(f7090CL);
                    rMesh.setMesh((Mesh) asset2);
                    rMesh.setMaterial((Material) asset);
                    sceneObject.addChild(rMesh);
                }
            }
        }
    }

    /* renamed from: e */
    public static void m30126e(SceneObject sceneObject) {
        SceneObject findChildByName;
        if (sceneObject != null && (findChildByName = sceneObject.findChildByName(f7090CL)) != null) {
            sceneObject.removeChild(findChildByName);
            findChildByName.dispose();
        }
    }
}
