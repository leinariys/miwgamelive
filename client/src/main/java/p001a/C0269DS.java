package p001a;

import game.network.message.externalizable.C6869avJ;
import logic.ui.item.TaskPane;

/* renamed from: a.DS */
/* compiled from: a */
public class C0269DS extends C6869avJ {
    private final TaskPane cQI;
    private final int cQJ;
    private final boolean visible;

    public C0269DS() {
        this.cQI = null;
        this.visible = false;
        this.cQJ = 0;
    }

    public C0269DS(aSZ asz, boolean z) {
        this.cQI = asz.aMO();
        this.cQJ = asz.aMP();
        this.visible = z;
    }

    public TaskPane aMO() {
        return this.cQI;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public int aMP() {
        return this.cQJ;
    }
}
