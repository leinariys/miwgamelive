package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.aGW */
/* compiled from: a */
public class aGW {
    public static final int hTV = 4;

    /* renamed from: jR */
    static final /* synthetic */ boolean f2866jR = (!aGW.class.desiredAssertionStatus());
    public final C0763Kt stack = C0763Kt.bcE();
    private final C6995ayi[] hTW = new C6995ayi[4];
    public int hUa;
    private Object hTX;
    private Object hTY;
    private int hTZ;

    public aGW() {
        for (int i = 0; i < this.hTW.length; i++) {
            this.hTW[i] = new C6995ayi();
        }
    }

    public aGW(Object obj, Object obj2, int i) {
        for (int i2 = 0; i2 < this.hTW.length; i2++) {
            this.hTW[i2] = new C6995ayi();
        }
        mo9008a(obj, obj2, i);
    }

    /* renamed from: a */
    public void mo9008a(Object obj, Object obj2, int i) {
        this.hTX = obj;
        this.hTY = obj2;
        this.hTZ = 0;
        this.hUa = 0;
    }

    /* renamed from: b */
    private int m14871b(C6995ayi ayi) {
        float f;
        float f2;
        float f3;
        float f4;
        this.stack.bcH().push();
        this.stack.bcM().push();
        int i = -1;
        try {
            float distance = ayi.getDistance();
            for (int i2 = 0; i2 < 4; i2++) {
                if (this.hTW[i2].getDistance() < distance) {
                    distance = this.hTW[i2].getDistance();
                    i = i2;
                }
            }
            if (i != 0) {
                Vec3f ac = this.stack.bcH().mo4458ac(ayi.fTI);
                ac.sub(this.hTW[1].fTI);
                Vec3f ac2 = this.stack.bcH().mo4458ac(this.hTW[3].fTI);
                ac2.sub(this.hTW[2].fTI);
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                vec3f.cross(ac, ac2);
                f = vec3f.lengthSquared();
            } else {
                f = 0.0f;
            }
            if (i != 1) {
                Vec3f ac3 = this.stack.bcH().mo4458ac(ayi.fTI);
                ac3.sub(this.hTW[0].fTI);
                Vec3f ac4 = this.stack.bcH().mo4458ac(this.hTW[3].fTI);
                ac4.sub(this.hTW[2].fTI);
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                vec3f2.cross(ac3, ac4);
                f2 = vec3f2.lengthSquared();
            } else {
                f2 = 0.0f;
            }
            if (i != 2) {
                Vec3f ac5 = this.stack.bcH().mo4458ac(ayi.fTI);
                ac5.sub(this.hTW[0].fTI);
                Vec3f ac6 = this.stack.bcH().mo4458ac(this.hTW[3].fTI);
                ac6.sub(this.hTW[1].fTI);
                Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                vec3f3.cross(ac5, ac6);
                f3 = vec3f3.lengthSquared();
            } else {
                f3 = 0.0f;
            }
            if (i != 3) {
                Vec3f ac7 = this.stack.bcH().mo4458ac(ayi.fTI);
                ac7.sub(this.hTW[0].fTI);
                Vec3f ac8 = this.stack.bcH().mo4458ac(this.hTW[2].fTI);
                ac8.sub(this.hTW[1].fTI);
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                vec3f4.cross(ac7, ac8);
                f4 = vec3f4.lengthSquared();
            } else {
                f4 = 0.0f;
            }
            return C0647JL.m5598c(this.stack.bcM().mo23153k(f, f2, f3, f4));
        } finally {
            this.stack.bcH().pop();
            this.stack.bcM().pop();
        }
    }

    public Object dcy() {
        return this.hTX;
    }

    public Object dcz() {
        return this.hTY;
    }

    /* renamed from: j */
    public void mo9018j(Object obj, Object obj2) {
        this.hTX = obj;
        this.hTY = obj2;
    }

    /* renamed from: c */
    public void mo9009c(C6995ayi ayi) {
        if (ayi.gSM != null && ayi.gSM != null && C0128Bb.dMC != null) {
            C0128Bb.dMC.mo12228ag(ayi.gSM);
            ayi.gSM = null;
        }
    }

    public int dcA() {
        return this.hTZ;
    }

    /* renamed from: yd */
    public C6995ayi mo9019yd(int i) {
        return this.hTW[i];
    }

    public float dcB() {
        return C0128Bb.dME;
    }

    /* renamed from: d */
    public int mo9011d(C6995ayi ayi) {
        this.stack.bcH().push();
        try {
            float dcB = dcB() * dcB();
            int dcA = dcA();
            int i = -1;
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            int i2 = 0;
            while (i2 < dcA) {
                vec3f.sub(this.hTW[i2].fTI, ayi.fTI);
                float dot = vec3f.dot(vec3f);
                if (dot < dcB) {
                    i = i2;
                } else {
                    dot = dcB;
                }
                i2++;
                dcB = dot;
            }
            return i;
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: e */
    public void mo9017e(C6995ayi ayi) {
        if (f2866jR || m14872f(ayi)) {
            int dcA = dcA();
            if (dcA == 4) {
                dcA = m14871b(ayi);
            } else {
                this.hTZ++;
            }
            mo9007a(ayi, dcA);
            return;
        }
        throw new AssertionError();
    }

    /* renamed from: ye */
    public void mo9020ye(int i) {
        mo9009c(this.hTW[i]);
        int dcA = dcA() - 1;
        if (i != dcA) {
            this.hTW[i].mo17073a(this.hTW[dcA]);
            this.hTW[dcA].gSM = null;
            this.hTW[dcA].lifeTime = 0;
        }
        if (f2866jR || this.hTW[dcA].gSM == null) {
            this.hTZ--;
            return;
        }
        throw new AssertionError();
    }

    /* renamed from: a */
    public void mo9007a(C6995ayi ayi, int i) {
        if (f2866jR || m14872f(ayi)) {
            int lifeTime = this.hTW[i].getLifeTime();
            if (f2866jR || lifeTime >= 0) {
                Object obj = this.hTW[i].gSM;
                this.hTW[i].mo17073a(ayi);
                this.hTW[i].gSM = obj;
                this.hTW[i].lifeTime = lifeTime;
                return;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    /* renamed from: f */
    private boolean m14872f(C6995ayi ayi) {
        return ayi.gSJ <= dcB();
    }

    /* renamed from: c */
    public void mo9010c(C3978xf xfVar, C3978xf xfVar2) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            for (int dcA = dcA() - 1; dcA >= 0; dcA--) {
                C6995ayi ayi = this.hTW[dcA];
                ayi.gSI.set(ayi.fTI);
                xfVar.mo22946G(ayi.gSI);
                ayi.gSH.set(ayi.fTJ);
                xfVar2.mo22946G(ayi.gSH);
                vec3f.set(ayi.gSI);
                vec3f.sub(ayi.gSH);
                ayi.gSJ = vec3f.dot(ayi.fTL);
                ayi.lifeTime++;
            }
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            for (int dcA2 = dcA() - 1; dcA2 >= 0; dcA2--) {
                C6995ayi ayi2 = this.hTW[dcA2];
                if (!m14872f(ayi2)) {
                    mo9020ye(dcA2);
                } else {
                    vec3f.scale(ayi2.gSJ, ayi2.fTL);
                    vec3f3.sub(ayi2.gSI, vec3f);
                    vec3f2.sub(ayi2.gSH, vec3f3);
                    if (vec3f2.dot(vec3f2) > dcB() * dcB()) {
                        mo9020ye(dcA2);
                    }
                }
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void dcC() {
        for (int i = 0; i < this.hTZ; i++) {
            mo9009c(this.hTW[i]);
        }
        this.hTZ = 0;
    }
}
