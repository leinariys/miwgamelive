package p001a;

import game.network.message.serializable.C3901wW;
import game.network.message.serializable.C5737aUv;
import game.network.message.serializable.C5933adJ;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.aRF */
/* compiled from: a */
public class aRF extends C5933adJ {

    /* renamed from: jR */
    static final /* synthetic */ boolean f3684jR = (!aRF.class.desiredAssertionStatus());
    private List<C3901wW> iKX = new ArrayList();

    public aRF() {
    }

    public aRF(int i, ByteBuffer byteBuffer, int i2, int i3, ByteBuffer byteBuffer2, int i4) {
        C3901wW wWVar = new C3901wW();
        wWVar.bFf = i;
        wWVar.bFg = byteBuffer;
        wWVar.bFh = i2;
        wWVar.numVertices = i3;
        wWVar.bFi = byteBuffer2;
        wWVar.bFj = i4;
        mo11092a(wWVar);
    }

    /* renamed from: a */
    public void mo11092a(C3901wW wWVar) {
        mo11093a(wWVar, C6537aop.PHY_INTEGER);
    }

    /* renamed from: a */
    public void mo11093a(C3901wW wWVar, C6537aop aop) {
        this.iKX.add(wWVar);
        this.iKX.get(this.iKX.size() - 1).bFk = aop;
    }

    /* renamed from: a */
    public void mo11091a(C5737aUv auv, int i) {
        if (f3684jR || i < bUc()) {
            C3901wW wWVar = this.iKX.get(i);
            auv.iXj = wWVar.numVertices;
            auv.iXi = wWVar.bFi;
            auv.iXk = C6537aop.PHY_FLOAT;
            auv.stride = wWVar.bFj;
            auv.iXn = wWVar.bFf;
            auv.iXl = wWVar.bFg;
            auv.iXm = wWVar.bFh;
            auv.iXo = wWVar.bFk;
            return;
        }
        throw new AssertionError();
    }

    /* renamed from: b */
    public void mo11094b(C5737aUv auv, int i) {
        C3901wW wWVar = this.iKX.get(i);
        auv.iXj = wWVar.numVertices;
        auv.iXi = wWVar.bFi;
        auv.iXk = C6537aop.PHY_FLOAT;
        auv.stride = wWVar.bFj;
        auv.iXn = wWVar.bFf;
        auv.iXl = wWVar.bFg;
        auv.iXm = wWVar.bFh;
        auv.iXo = wWVar.bFk;
    }

    /* renamed from: qg */
    public void mo11097qg(int i) {
    }

    /* renamed from: qh */
    public void mo11098qh(int i) {
    }

    public int bUc() {
        return this.iKX.size();
    }

    public List<C3901wW> dtP() {
        return this.iKX;
    }

    /* renamed from: qi */
    public void mo11099qi(int i) {
    }

    /* renamed from: qj */
    public void mo11100qj(int i) {
    }
}
