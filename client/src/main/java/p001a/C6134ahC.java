package p001a;

import com.hoplon.geometry.Vec3f;
import logic.aaa.C1088Pv;
import logic.thred.C1362Tq;
import logic.thred.C6339akz;
import logic.thred.C6615aqP;

/* renamed from: a.ahC  reason: case insensitive filesystem */
/* compiled from: a */
public class C6134ahC implements C1362Tq {
    private final C1362Tq fLn;
    private final C1088Pv fLo;

    public C6134ahC(C1362Tq tq, C1088Pv pv) {
        this.fLn = tq;
        this.fLo = pv;
    }

    /* renamed from: Jm */
    public C6339akz mo5753Jm() {
        return this.fLn.mo5753Jm();
    }

    /* renamed from: Jh */
    public Vec3f mo5748Jh() {
        return this.fLn.mo5748Jh();
    }

    /* renamed from: Jl */
    public C1088Pv mo5752Jl() {
        return this.fLo;
    }

    /* renamed from: Ji */
    public C6615aqP mo5749Ji() {
        return this.fLn.mo5749Ji();
    }

    /* renamed from: Jj */
    public C6615aqP mo5750Jj() {
        return this.fLn.mo5750Jj();
    }

    /* renamed from: Jk */
    public Vec3f mo5751Jk() {
        return this.fLn.mo5751Jk();
    }
}
