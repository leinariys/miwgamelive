package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.Zd */
/* compiled from: a */
public class C1734Zd extends C0783LL {
    public final Vec3f eNi = new Vec3f();
    public final Vec3f eNj = new Vec3f();

    public C1734Zd() {
    }

    public C1734Zd(Vec3f vec3f, Vec3f vec3f2, C3655tK tKVar, Object obj, short s, short s2) {
        super(obj, s, s2);
        this.eNi.set(vec3f);
        this.eNj.set(vec3f2);
    }
}
