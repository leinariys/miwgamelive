package p001a;

import game.network.message.ByteMessageReader;
import util.Syst;

import java.io.EOFException;

/* renamed from: a.amu  reason: case insensitive filesystem */
/* compiled from: a */
public class ByteMessageReaderLogger extends ByteMessageReader {
    public ByteMessageReaderLogger(byte[] bArr, int i, int i2) {
        super(bArr, i, i2);
    }

    public ByteMessageReaderLogger(byte[] bArr) {
        super(bArr);
    }

    /* renamed from: a */
    public int readBits(int i) throws EOFException {
        int a = super.readBits(i);
        String binaryString = Integer.toBinaryString(a);
        while (binaryString.length() < i) {
            binaryString = String.valueOf('0') + binaryString;
        }
        System.out.println("* readBits " + binaryString);
        return a;
    }

    public int read() {
        int read = super.read();
        System.out.println("read " + read);
        return read;
    }

    public int read(byte[] bArr, int i, int i2) {
        int read = read(bArr, i, i2);
        System.out.println("read len=" + i2 + " " + Syst.m15898a(bArr, i, Math.min(i2, 256), " ", 16));
        return read;
    }

    public boolean readBoolean() throws EOFException {
        boolean readBoolean = super.readBoolean();
        System.out.println("readBoolean " + readBoolean);
        return readBoolean;
    }

    public byte readByte() throws EOFException {
        byte readByte = super.readByte();
        System.out.println("readByte " + readByte);
        return readByte;
    }

    public int readUnsignedByte() throws EOFException {
        int readUnsignedByte = super.readUnsignedByte();
        System.out.println("readUnsignedByte " + readUnsignedByte);
        return readUnsignedByte;
    }

    public short readShort() {
        short readShort = super.readShort();
        System.out.println("readShort " + readShort);
        return readShort;
    }

    public short readShort0() throws EOFException {
        short cjB = super.readShort0();
        System.out.println("readShort0 " + cjB);
        return cjB;
    }

    public int readUnsignedShort() {
        int readUnsignedShort = super.readUnsignedShort();
        System.out.println("readUnsignedShort " + readUnsignedShort);
        return readUnsignedShort;
    }

    public int readUnsignedShort0() throws EOFException {
        int cjC = super.readUnsignedShort0();
        System.out.println("readUnsignedShort0 " + cjC);
        return cjC;
    }

    public char readChar() throws EOFException {
        char readChar = super.readChar();
        System.out.println("readChar " + readChar);
        return readChar;
    }

    public int readInt() throws EOFException {
        int readInt = super.readInt();
        System.out.println("readInt " + readInt);
        return readInt;
    }

    public int readPositiveInt() {
        int cjD = super.readPositiveInt();
        System.out.println("readPositiveInt " + cjD);
        return cjD;
    }

    public int readInt0() throws EOFException {
        int cjE = super.readInt0();
        System.out.println("readInt0 " + cjE);
        return cjE;
    }

    public int read3bytes() throws EOFException {
        int cjF = super.read3bytes();
        System.out.println("read3bytes " + cjF);
        return cjF;
    }

    public long readLong() throws EOFException {
        long readLong = super.readLong();
        System.out.println("readLong " + readLong);
        return readLong;
    }

    public long readLong0() throws EOFException {
        long cjG = super.readLong0();
        System.out.println("readLong0 " + cjG);
        return cjG;
    }
}
