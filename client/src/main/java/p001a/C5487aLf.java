package p001a;

import logic.thred.C6615aqP;

import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: a.aLf  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C5487aLf implements aRR {
    private static final AtomicInteger nextId = new AtomicInteger();

    /* renamed from: id */
    public final int f3320id = nextId.incrementAndGet();
    private final C6615aqP ikH;
    private boolean valid = true;

    public C5487aLf(C6615aqP aqp) {
        this.ikH = aqp;
    }

    public C6615aqP dhC() {
        return this.ikH;
    }

    public final int hashCode() {
        return this.f3320id;
    }

    public boolean isValid() {
        return this.valid;
    }

    public void dhD() {
        this.valid = false;
    }
}
