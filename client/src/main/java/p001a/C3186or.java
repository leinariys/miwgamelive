package p001a;

import logic.baa.C1616Xf;

import java.util.Comparator;
import java.util.Iterator;

/* renamed from: a.or */
/* compiled from: a */
public class C3186or<K> implements Comparator<K> {
    private final C6656arE aPx;

    public C3186or(C6656arE are) {
        this.aPx = are;
    }

    /* renamed from: a */
    public static boolean m36906a(Iterator it) {
        Object next = it.next();
        if (next == null) {
            if (!it.hasNext()) {
                return true;
            }
            next = it.next();
        }
        if (next instanceof C1616Xf) {
            return true;
        }
        if (next instanceof Comparable) {
            return true;
        }
        if (next instanceof Class) {
            return true;
        }
        return false;
    }

    public int compare(K k, K k2) {
        if ((k instanceof C1616Xf) && (k2 instanceof C1616Xf)) {
            long cqo = ((C1616Xf) k).bFf().getObjectId().getId();
            long cqo2 = ((C1616Xf) k2).bFf().getObjectId().getId();
            if (cqo2 == cqo) {
                return 0;
            }
            return cqo2 > cqo ? 1 : -1;
        } else if ((k instanceof Class) && (k2 instanceof Class)) {
            return this.aPx.mo12015jL(((Class) k).getName()).compareTo(this.aPx.mo12015jL(((Class) k2).getName()));
        } else {
            if ((k instanceof Comparable) && (k2 instanceof Comparable)) {
                return ((Comparable) k).compareTo(k2);
            }
            if (k == null) {
                if (k2 != null) {
                    return -1;
                }
                return 0;
            } else if (k2 == null) {
                return 1;
            } else {
                throw new RuntimeException("Deterministic mode not supported for " + k);
            }
        }
    }
}
