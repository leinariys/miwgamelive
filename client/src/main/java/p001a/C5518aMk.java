package p001a;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;

/* renamed from: a.aMk  reason: case insensitive filesystem */
/* compiled from: a */
public class C5518aMk extends DefaultTreeModel {
    private C5519aMl gOV;

    public C5518aMk(C6964axd axd, C5519aMl aml) {
        super(axd);
        this.gOV = aml;
    }

    public Object getChild(Object obj, int i) {
        int i2;
        int childCount = ((TreeNode) obj).getChildCount();
        if (!this.gOV.mo10166z()) {
            return ((TreeNode) obj).getChildAt(i);
        }
        if (childCount == 0) {
            throw new ArrayIndexOutOfBoundsException("node has no children");
        } else if (((C6964axd) obj).cCa()) {
            for (int i3 = 0; i3 < childCount; i3++) {
                ((TreeNode) obj).getChildAt(i3).mo16807hd(true);
            }
            return ((TreeNode) obj).getChildAt(i);
        } else {
            int i4 = 0;
            int i5 = -1;
            int i6 = -1;
            while (i4 < childCount) {
                C6964axd childAt = ((TreeNode) obj).getChildAt(i4);
                if (this.gOV.mo10165a(childAt.getUserObject())) {
                    childAt.mo16807hd(true);
                    i2 = i5 + 1;
                } else {
                    childAt.mo16807hd(false);
                    if (childAt.cBZ() > 0) {
                        i2 = i5 + 1;
                    } else {
                        i2 = i5;
                    }
                }
                int i7 = i6 + 1;
                if (i2 == i) {
                    return ((TreeNode) obj).getChildAt(i7);
                }
                i4++;
                i5 = i2;
                i6 = i7;
            }
            throw new ArrayIndexOutOfBoundsException("index unmatched");
        }
    }

    public int getChildCount(Object obj) {
        int i;
        int childCount = ((TreeNode) obj).getChildCount();
        if (!this.gOV.mo10166z()) {
            return childCount;
        }
        if (childCount == 0) {
            return 0;
        }
        if (!(obj instanceof C6964axd) || !((C6964axd) obj).cCa()) {
            int i2 = 0;
            int i3 = 0;
            while (i2 < childCount) {
                C6964axd childAt = ((TreeNode) obj).getChildAt(i2);
                if (this.gOV.mo10165a(childAt.getUserObject())) {
                    childAt.mo16807hd(true);
                    i = i3 + 1;
                } else {
                    childAt.mo16807hd(false);
                    i = childAt.cBZ() > 0 ? i3 + 1 : i3;
                }
                i2++;
                i3 = i;
            }
            return i3;
        }
        for (int i4 = 0; i4 < childCount; i4++) {
            ((TreeNode) obj).getChildAt(i4).mo16807hd(true);
        }
        return childCount;
    }
}
