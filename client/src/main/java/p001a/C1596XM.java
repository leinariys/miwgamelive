package p001a;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Properties;

/* renamed from: a.XM */
/* compiled from: a */
public class C1596XM {
    public Properties props = new Properties();

    public String getText() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        new StringWriter();
        try {
            this.props.save(byteArrayOutputStream, "");
            return new String(byteArrayOutputStream.toByteArray(), "8859_1");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void setText(String str) {
        try {
            this.props.clear();
            this.props.load(new ByteArrayInputStream(str.getBytes("8859_1")));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: hD */
    public String mo6724hD(String str) {
        return (String) this.props.get(str);
    }

    /* renamed from: T */
    public String mo6722T(String str, String str2) {
        return (String) this.props.get(str);
    }
}
