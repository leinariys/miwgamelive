package p001a;

import logic.res.html.DataClassSerializer;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aOu  reason: case insensitive filesystem */
/* compiled from: a */
public class C5580aOu extends DataClassSerializer {
    int izp;
    private Object[] values;

    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) {
        int readShort;
        if (this.izp == 1) {
            readShort = objectInput.readByte();
        } else {
            readShort = objectInput.readShort();
        }
        if (readShort == -1) {
            return null;
        }
        if (readShort >= 0 && readShort < this.values.length) {
            return this.values[readShort];
        }
        throw new ArrayIndexOutOfBoundsException("Error reading enum " + this.clazz + " index " + readShort + " is out of bounds  [0," + this.values.length + ")");
    }

    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) {
        if (this.izp == 1) {
            if (obj == null) {
                objectOutput.writeByte(-1);
            } else {
                objectOutput.writeByte(((Enum) obj).ordinal());
            }
        } else if (obj == null) {
            objectOutput.writeShort(-1);
        } else {
            objectOutput.writeShort(((Enum) obj).ordinal());
        }
    }

    /* renamed from: z */
    public Object mo3598z(Object obj) {
        return obj;
    }

    /* renamed from: yl */
    public boolean mo3597yl() {
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo3590a(Class<?> cls, Class<?>[] clsArr) {
        this.values = cls.getEnumConstants();
        if (this.values.length < 128) {
            this.izp = 1;
        } else {
            this.izp = 2;
        }
        super.mo3590a(cls, clsArr);
    }
}
