package p001a;

import com.hoplon.geometry.Vec3f;

import javax.vecmath.Tuple3f;

/* renamed from: a.ES */
/* compiled from: a */
public class C0327ES implements C6331akr {
    /* renamed from: jR */
    static final /* synthetic */ boolean f490jR = (!C0327ES.class.desiredAssertionStatus());
    private static final int gZr = 5;
    private static final int gZs = 0;
    private static final int gZt = 1;
    private static final int gZu = 2;
    private static final int gZv = 3;
    public final Vec3f gZA = new Vec3f();
    public final Vec3f gZB = new Vec3f();
    public final Vec3f gZC = new Vec3f();
    public final C0329b gZE = new C0329b();
    public final Vec3f[] gZw = new Vec3f[5];
    public final Vec3f[] gZx = new Vec3f[5];
    public final Vec3f[] gZy = new Vec3f[5];
    public final Vec3f gZz = new Vec3f();
    public final C1123QW<C0329b> gZq = C0762Ks.m6640D(C0329b.class);
    public boolean gZD;
    public boolean gZF;
    public int numVertices;
    public C0763Kt stack = C0763Kt.bcE();

    public C0327ES() {
        for (int i = 0; i < 5; i++) {
            this.gZw[i] = new Vec3f();
            this.gZx[i] = new Vec3f();
            this.gZy[i] = new Vec3f();
        }
    }

    /* renamed from: wD */
    public void mo1860wD(int i) {
        if (f490jR || this.numVertices > 0) {
            this.numVertices--;
            this.gZw[i].set(this.gZw[this.numVertices]);
            this.gZx[i].set(this.gZx[this.numVertices]);
            this.gZy[i].set(this.gZy[this.numVertices]);
            return;
        }
        throw new AssertionError();
    }

    /* renamed from: a */
    public void mo1846a(C0328a aVar) {
        if (numVertices() >= 4 && !aVar.cUo) {
            mo1860wD(3);
        }
        if (numVertices() >= 3 && !aVar.cUn) {
            mo1860wD(2);
        }
        if (numVertices() >= 2 && !aVar.cUm) {
            mo1860wD(1);
        }
        if (numVertices() >= 1 && !aVar.cUl) {
            mo1860wD(0);
        }
    }

    public boolean cFU() {
        float f;
        this.stack = this.stack.bcD();
        this.stack.bcH().push();
        try {
            if (this.gZF) {
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                this.gZE.reset();
                this.gZF = false;
                switch (numVertices()) {
                    case 0:
                        this.gZD = false;
                        break;
                    case 1:
                        this.gZz.set(this.gZx[0]);
                        this.gZA.set(this.gZy[0]);
                        this.gZB.sub(this.gZz, this.gZA);
                        this.gZE.reset();
                        this.gZE.mo1863m(1.0f, 0.0f, 0.0f, 0.0f);
                        this.gZD = this.gZE.isValid();
                        break;
                    case 2:
                        Tuple3f tuple3f = this.gZw[0];
                        Tuple3f tuple3f2 = this.gZw[1];
                        Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
                        Vec3f h = this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f);
                        Vec3f vec3f7 = (Vec3f) this.stack.bcH().get();
                        vec3f7.sub(h, tuple3f);
                        Vec3f vec3f8 = (Vec3f) this.stack.bcH().get();
                        vec3f8.sub(tuple3f2, tuple3f);
                        float dot = vec3f8.dot(vec3f7);
                        if (dot > 0.0f) {
                            float dot2 = vec3f8.dot(vec3f8);
                            if (dot < dot2) {
                                float f2 = dot / dot2;
                                vec3f.scale(f2, vec3f8);
                                vec3f7.sub(vec3f);
                                this.gZE.fcc.cUl = true;
                                this.gZE.fcc.cUm = true;
                                f = f2;
                            } else {
                                vec3f7.sub(vec3f8);
                                this.gZE.fcc.cUm = true;
                                f = 1.0f;
                            }
                        } else {
                            this.gZE.fcc.cUl = true;
                            f = 0.0f;
                        }
                        this.gZE.mo1863m(1.0f - f, f, 0.0f, 0.0f);
                        vec3f.scale(f, vec3f8);
                        vec3f6.add(tuple3f, vec3f);
                        vec3f.sub(this.gZx[1], this.gZx[0]);
                        vec3f.scale(f);
                        this.gZz.add(this.gZx[0], vec3f);
                        vec3f.sub(this.gZy[1], this.gZy[0]);
                        vec3f.scale(f);
                        this.gZA.add(this.gZy[0], vec3f);
                        this.gZB.sub(this.gZz, this.gZA);
                        mo1846a(this.gZE.fcc);
                        this.gZD = this.gZE.isValid();
                        break;
                    case 3:
                        mo1847a(this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f), this.gZw[0], this.gZw[1], this.gZw[2], this.gZE);
                        vec3f2.scale(this.gZE.fcd[0], this.gZx[0]);
                        vec3f3.scale(this.gZE.fcd[1], this.gZx[1]);
                        vec3f4.scale(this.gZE.fcd[2], this.gZx[2]);
                        C0647JL.m5602f(this.gZz, vec3f2, vec3f3, vec3f4);
                        vec3f2.scale(this.gZE.fcd[0], this.gZy[0]);
                        vec3f3.scale(this.gZE.fcd[1], this.gZy[1]);
                        vec3f4.scale(this.gZE.fcd[2], this.gZy[2]);
                        C0647JL.m5602f(this.gZA, vec3f2, vec3f3, vec3f4);
                        this.gZB.sub(this.gZz, this.gZA);
                        mo1846a(this.gZE.fcc);
                        this.gZD = this.gZE.isValid();
                        break;
                    case 4:
                        if (!mo1848a(this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f), this.gZw[0], this.gZw[1], this.gZw[2], this.gZw[3], this.gZE)) {
                            if (!this.gZE.fce) {
                                this.gZD = true;
                                this.gZB.set(0.0f, 0.0f, 0.0f);
                                break;
                            } else {
                                this.gZD = false;
                                break;
                            }
                        } else {
                            vec3f2.scale(this.gZE.fcd[0], this.gZx[0]);
                            vec3f3.scale(this.gZE.fcd[1], this.gZx[1]);
                            vec3f4.scale(this.gZE.fcd[2], this.gZx[2]);
                            vec3f5.scale(this.gZE.fcd[3], this.gZx[3]);
                            C0647JL.m5592a(this.gZz, vec3f2, vec3f3, vec3f4, vec3f5);
                            vec3f2.scale(this.gZE.fcd[0], this.gZy[0]);
                            vec3f3.scale(this.gZE.fcd[1], this.gZy[1]);
                            vec3f4.scale(this.gZE.fcd[2], this.gZy[2]);
                            vec3f5.scale(this.gZE.fcd[3], this.gZy[3]);
                            C0647JL.m5592a(this.gZA, vec3f2, vec3f3, vec3f4, vec3f5);
                            this.gZB.sub(this.gZz, this.gZA);
                            mo1846a(this.gZE.fcc);
                            this.gZD = this.gZE.isValid();
                            break;
                        }
                    default:
                        this.gZD = false;
                        break;
                }
            }
            return this.gZD;
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public boolean mo1847a(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4, C0329b bVar) {
        this.stack.bcH().push();
        try {
            bVar.fcc.reset();
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            vec3f5.sub(vec3f3, vec3f2);
            Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
            vec3f6.sub(vec3f4, vec3f2);
            Vec3f vec3f7 = (Vec3f) this.stack.bcH().get();
            vec3f7.sub(vec3f, vec3f2);
            float dot = vec3f5.dot(vec3f7);
            float dot2 = vec3f6.dot(vec3f7);
            if (dot > 0.0f || dot2 > 0.0f) {
                Vec3f vec3f8 = (Vec3f) this.stack.bcH().get();
                vec3f8.sub(vec3f, vec3f3);
                float dot3 = vec3f5.dot(vec3f8);
                float dot4 = vec3f6.dot(vec3f8);
                if (dot3 < 0.0f || dot4 > dot3) {
                    float f = (dot * dot4) - (dot3 * dot2);
                    if (f > 0.0f || dot < 0.0f || dot3 > 0.0f) {
                        Vec3f vec3f9 = (Vec3f) this.stack.bcH().get();
                        vec3f9.sub(vec3f, vec3f4);
                        float dot5 = vec3f5.dot(vec3f9);
                        float dot6 = vec3f6.dot(vec3f9);
                        if (dot6 < 0.0f || dot5 > dot6) {
                            float f2 = (dot5 * dot2) - (dot * dot6);
                            if (f2 > 0.0f || dot2 < 0.0f || dot6 > 0.0f) {
                                float f3 = (dot3 * dot6) - (dot5 * dot4);
                                if (f3 > 0.0f || dot4 - dot3 < 0.0f || dot5 - dot6 < 0.0f) {
                                    float f4 = 1.0f / ((f3 + f2) + f);
                                    float f5 = f2 * f4;
                                    float f6 = f * f4;
                                    Vec3f vec3f10 = (Vec3f) this.stack.bcH().get();
                                    Vec3f vec3f11 = (Vec3f) this.stack.bcH().get();
                                    vec3f10.scale(f5, vec3f5);
                                    vec3f11.scale(f6, vec3f6);
                                    C0647JL.m5602f(bVar.fcb, vec3f2, vec3f10, vec3f11);
                                    bVar.fcc.cUl = true;
                                    bVar.fcc.cUm = true;
                                    bVar.fcc.cUn = true;
                                    bVar.mo1863m((1.0f - f5) - f6, f5, f6, 0.0f);
                                } else {
                                    float f7 = (dot4 - dot3) / ((dot4 - dot3) + (dot5 - dot6));
                                    Vec3f vec3f12 = (Vec3f) this.stack.bcH().get();
                                    vec3f12.sub(vec3f4, vec3f3);
                                    bVar.fcb.scaleAdd(f7, vec3f12, vec3f3);
                                    bVar.fcc.cUm = true;
                                    bVar.fcc.cUn = true;
                                    bVar.mo1863m(0.0f, 1.0f - f7, f7, 0.0f);
                                }
                            } else {
                                float f8 = dot2 / (dot2 - dot6);
                                bVar.fcb.scaleAdd(f8, vec3f6, vec3f2);
                                bVar.fcc.cUl = true;
                                bVar.fcc.cUn = true;
                                bVar.mo1863m(1.0f - f8, 0.0f, f8, 0.0f);
                            }
                        } else {
                            bVar.fcb.set(vec3f4);
                            bVar.fcc.cUn = true;
                            bVar.mo1863m(0.0f, 0.0f, 1.0f, 0.0f);
                        }
                    } else {
                        float f9 = dot / (dot - dot3);
                        bVar.fcb.scaleAdd(f9, vec3f5, vec3f2);
                        bVar.fcc.cUl = true;
                        bVar.fcc.cUm = true;
                        bVar.mo1863m(1.0f - f9, f9, 0.0f, 0.0f);
                    }
                } else {
                    bVar.fcb.set(vec3f3);
                    bVar.fcc.cUm = true;
                    bVar.mo1863m(0.0f, 1.0f, 0.0f, 0.0f);
                }
            } else {
                bVar.fcb.set(vec3f2);
                bVar.fcc.cUl = true;
                bVar.mo1863m(1.0f, 0.0f, 0.0f, 0.0f);
            }
            this.stack.bcH().pop();
            return true;
        } catch (Throwable th) {
            this.stack.bcH().pop();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: b */
    public int mo1852b(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4, Vec3f vec3f5) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f7 = (Vec3f) this.stack.bcH().get();
            vec3f7.sub(vec3f3, vec3f2);
            vec3f6.sub(vec3f4, vec3f2);
            vec3f7.cross(vec3f7, vec3f6);
            vec3f6.sub(vec3f, vec3f2);
            float dot = vec3f6.dot(vec3f7);
            vec3f6.sub(vec3f5, vec3f2);
            float dot2 = vec3f6.dot(vec3f7);
            if (dot2 * dot2 < 9.999999E-9f) {
                this.stack.bcH().pop();
                return -1;
            }
            int i = dot2 * dot < 0.0f ? 1 : 0;
            this.stack.bcH().pop();
            return i;
        } catch (Throwable th) {
            this.stack.bcH().pop();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0123 A[Catch:{ all -> 0x0285 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0189 A[Catch:{ all -> 0x0285 }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x01f0 A[Catch:{ all -> 0x0285 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean mo1848a(com.hoplon.geometry.Vec3f r15, com.hoplon.geometry.Vec3f r16, com.hoplon.geometry.Vec3f r17, com.hoplon.geometry.Vec3f r18, com.hoplon.geometry.Vec3f r19, p001a.C0327ES.C0329b r20) {
        /*
            r14 = this;
            a.Kt r1 = r14.stack
            a.OY r1 = r1.bcH()
            r1.push()
            a.QW<a.ES$b> r1 = r14.gZq
            java.lang.Object r1 = r1.get()
            r7 = r1
            a.ES$b r7 = (p001a.C0327ES.C0329b) r7
            r7.reset()
            a.Kt r1 = r14.stack     // Catch:{ all -> 0x0285 }
            a.OY r1 = r1.bcH()     // Catch:{ all -> 0x0285 }
            java.lang.Object r1 = r1.get()     // Catch:{ all -> 0x0285 }
            r0 = r1
            com.hoplon.geometry.Vec3f r0 = (com.hoplon.geometry.Vec3f) r0     // Catch:{ all -> 0x0285 }
            r8 = r0
            a.Kt r1 = r14.stack     // Catch:{ all -> 0x0285 }
            a.OY r1 = r1.bcH()     // Catch:{ all -> 0x0285 }
            java.lang.Object r1 = r1.get()     // Catch:{ all -> 0x0285 }
            com.hoplon.geometry.Vec3f r1 = (com.hoplon.geometry.Vec3f) r1     // Catch:{ all -> 0x0285 }
            a.Kt r1 = r14.stack     // Catch:{ all -> 0x0285 }
            a.OY r1 = r1.bcH()     // Catch:{ all -> 0x0285 }
            java.lang.Object r1 = r1.get()     // Catch:{ all -> 0x0285 }
            com.hoplon.geometry.Vec3f r1 = (com.hoplon.geometry.Vec3f) r1     // Catch:{ all -> 0x0285 }
            r0 = r20
            com.hoplon.geometry.Vec3f r1 = r0.fcb     // Catch:{ all -> 0x0285 }
            r1.set(r15)     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            r1.reset()     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            r2 = 1
            r1.cUl = r2     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            r2 = 1
            r1.cUm = r2     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            r2 = 1
            r1.cUn = r2     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            r2 = 1
            r1.cUo = r2     // Catch:{ all -> 0x0285 }
            int r10 = r14.mo1852b(r15, r16, r17, r18, r19)     // Catch:{ all -> 0x0285 }
            r1 = r14
            r2 = r15
            r3 = r16
            r4 = r18
            r5 = r19
            r6 = r17
            int r11 = r1.mo1852b(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0285 }
            r1 = r14
            r2 = r15
            r3 = r16
            r4 = r19
            r5 = r17
            r6 = r18
            int r12 = r1.mo1852b(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0285 }
            r1 = r14
            r2 = r15
            r3 = r17
            r4 = r19
            r5 = r18
            r6 = r16
            int r13 = r1.mo1852b(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0285 }
            if (r10 < 0) goto L_0x009b
            if (r11 < 0) goto L_0x009b
            if (r12 < 0) goto L_0x009b
            if (r13 >= 0) goto L_0x00b0
        L_0x009b:
            r1 = 1
            r0 = r20
            r0.fce = r1     // Catch:{ all -> 0x0285 }
        L_0x00a0:
            a.Kt r1 = r14.stack
            a.OY r1 = r1.bcH()
            r1.pop()
            a.QW<a.ES$b> r1 = r14.gZq
            r1.release(r7)
            r1 = 0
        L_0x00af:
            return r1
        L_0x00b0:
            if (r10 != 0) goto L_0x00b8
            if (r11 != 0) goto L_0x00b8
            if (r12 != 0) goto L_0x00b8
            if (r13 == 0) goto L_0x00a0
        L_0x00b8:
            r9 = 2139095039(0x7f7fffff, float:3.4028235E38)
            if (r10 == 0) goto L_0x0298
            r1 = r14
            r2 = r15
            r3 = r16
            r4 = r17
            r5 = r18
            r6 = r7
            r1.mo1847a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0285 }
            a.Kt r1 = r14.stack     // Catch:{ all -> 0x0285 }
            a.OY r1 = r1.bcH()     // Catch:{ all -> 0x0285 }
            com.hoplon.geometry.Vec3f r2 = r7.fcb     // Catch:{ all -> 0x0285 }
            com.hoplon.geometry.Vec3f r1 = r1.mo4458ac(r2)     // Catch:{ all -> 0x0285 }
            r8.sub(r1, r15)     // Catch:{ all -> 0x0285 }
            float r10 = r8.dot(r8)     // Catch:{ all -> 0x0285 }
            int r2 = (r10 > r9 ? 1 : (r10 == r9 ? 0 : -1))
            if (r2 >= 0) goto L_0x0298
            r0 = r20
            com.hoplon.geometry.Vec3f r2 = r0.fcb     // Catch:{ all -> 0x0285 }
            r2.set(r1)     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            r1.reset()     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            a.ES$a r2 = r7.fcc     // Catch:{ all -> 0x0285 }
            boolean r2 = r2.cUl     // Catch:{ all -> 0x0285 }
            r1.cUl = r2     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            a.ES$a r2 = r7.fcc     // Catch:{ all -> 0x0285 }
            boolean r2 = r2.cUm     // Catch:{ all -> 0x0285 }
            r1.cUm = r2     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            a.ES$a r2 = r7.fcc     // Catch:{ all -> 0x0285 }
            boolean r2 = r2.cUn     // Catch:{ all -> 0x0285 }
            r1.cUn = r2     // Catch:{ all -> 0x0285 }
            float[] r1 = r7.fcd     // Catch:{ all -> 0x0285 }
            r2 = 0
            r1 = r1[r2]     // Catch:{ all -> 0x0285 }
            float[] r2 = r7.fcd     // Catch:{ all -> 0x0285 }
            r3 = 1
            r2 = r2[r3]     // Catch:{ all -> 0x0285 }
            float[] r3 = r7.fcd     // Catch:{ all -> 0x0285 }
            r4 = 2
            r3 = r3[r4]     // Catch:{ all -> 0x0285 }
            r4 = 0
            r0 = r20
            r0.mo1863m(r1, r2, r3, r4)     // Catch:{ all -> 0x0285 }
        L_0x0121:
            if (r11 == 0) goto L_0x0295
            r1 = r14
            r2 = r15
            r3 = r16
            r4 = r18
            r5 = r19
            r6 = r7
            r1.mo1847a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0285 }
            a.Kt r1 = r14.stack     // Catch:{ all -> 0x0285 }
            a.OY r1 = r1.bcH()     // Catch:{ all -> 0x0285 }
            com.hoplon.geometry.Vec3f r2 = r7.fcb     // Catch:{ all -> 0x0285 }
            com.hoplon.geometry.Vec3f r1 = r1.mo4458ac(r2)     // Catch:{ all -> 0x0285 }
            r8.sub(r1, r15)     // Catch:{ all -> 0x0285 }
            float r9 = r8.dot(r8)     // Catch:{ all -> 0x0285 }
            int r2 = (r9 > r10 ? 1 : (r9 == r10 ? 0 : -1))
            if (r2 >= 0) goto L_0x0295
            r0 = r20
            com.hoplon.geometry.Vec3f r2 = r0.fcb     // Catch:{ all -> 0x0285 }
            r2.set(r1)     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            r1.reset()     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            a.ES$a r2 = r7.fcc     // Catch:{ all -> 0x0285 }
            boolean r2 = r2.cUl     // Catch:{ all -> 0x0285 }
            r1.cUl = r2     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            a.ES$a r2 = r7.fcc     // Catch:{ all -> 0x0285 }
            boolean r2 = r2.cUm     // Catch:{ all -> 0x0285 }
            r1.cUn = r2     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            a.ES$a r2 = r7.fcc     // Catch:{ all -> 0x0285 }
            boolean r2 = r2.cUn     // Catch:{ all -> 0x0285 }
            r1.cUo = r2     // Catch:{ all -> 0x0285 }
            float[] r1 = r7.fcd     // Catch:{ all -> 0x0285 }
            r2 = 0
            r1 = r1[r2]     // Catch:{ all -> 0x0285 }
            r2 = 0
            float[] r3 = r7.fcd     // Catch:{ all -> 0x0285 }
            r4 = 1
            r3 = r3[r4]     // Catch:{ all -> 0x0285 }
            float[] r4 = r7.fcd     // Catch:{ all -> 0x0285 }
            r5 = 2
            r4 = r4[r5]     // Catch:{ all -> 0x0285 }
            r0 = r20
            r0.mo1863m(r1, r2, r3, r4)     // Catch:{ all -> 0x0285 }
        L_0x0187:
            if (r12 == 0) goto L_0x01ee
            r1 = r14
            r2 = r15
            r3 = r16
            r4 = r19
            r5 = r17
            r6 = r7
            r1.mo1847a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0285 }
            a.Kt r1 = r14.stack     // Catch:{ all -> 0x0285 }
            a.OY r1 = r1.bcH()     // Catch:{ all -> 0x0285 }
            com.hoplon.geometry.Vec3f r2 = r7.fcb     // Catch:{ all -> 0x0285 }
            com.hoplon.geometry.Vec3f r2 = r1.mo4458ac(r2)     // Catch:{ all -> 0x0285 }
            r8.sub(r2, r15)     // Catch:{ all -> 0x0285 }
            float r1 = r8.dot(r8)     // Catch:{ all -> 0x0285 }
            int r3 = (r1 > r9 ? 1 : (r1 == r9 ? 0 : -1))
            if (r3 >= 0) goto L_0x01ee
            r0 = r20
            com.hoplon.geometry.Vec3f r3 = r0.fcb     // Catch:{ all -> 0x0285 }
            r3.set(r2)     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r2 = r0.fcc     // Catch:{ all -> 0x0285 }
            r2.reset()     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r2 = r0.fcc     // Catch:{ all -> 0x0285 }
            a.ES$a r3 = r7.fcc     // Catch:{ all -> 0x0285 }
            boolean r3 = r3.cUl     // Catch:{ all -> 0x0285 }
            r2.cUl = r3     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r2 = r0.fcc     // Catch:{ all -> 0x0285 }
            a.ES$a r3 = r7.fcc     // Catch:{ all -> 0x0285 }
            boolean r3 = r3.cUn     // Catch:{ all -> 0x0285 }
            r2.cUm = r3     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r2 = r0.fcc     // Catch:{ all -> 0x0285 }
            a.ES$a r3 = r7.fcc     // Catch:{ all -> 0x0285 }
            boolean r3 = r3.cUm     // Catch:{ all -> 0x0285 }
            r2.cUo = r3     // Catch:{ all -> 0x0285 }
            float[] r2 = r7.fcd     // Catch:{ all -> 0x0285 }
            r3 = 0
            r2 = r2[r3]     // Catch:{ all -> 0x0285 }
            float[] r3 = r7.fcd     // Catch:{ all -> 0x0285 }
            r4 = 2
            r3 = r3[r4]     // Catch:{ all -> 0x0285 }
            r4 = 0
            float[] r5 = r7.fcd     // Catch:{ all -> 0x0285 }
            r6 = 1
            r5 = r5[r6]     // Catch:{ all -> 0x0285 }
            r0 = r20
            r0.mo1863m(r2, r3, r4, r5)     // Catch:{ all -> 0x0285 }
            r9 = r1
        L_0x01ee:
            if (r13 == 0) goto L_0x0254
            r1 = r14
            r2 = r15
            r3 = r17
            r4 = r19
            r5 = r18
            r6 = r7
            r1.mo1847a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0285 }
            a.Kt r1 = r14.stack     // Catch:{ all -> 0x0285 }
            a.OY r1 = r1.bcH()     // Catch:{ all -> 0x0285 }
            com.hoplon.geometry.Vec3f r2 = r7.fcb     // Catch:{ all -> 0x0285 }
            com.hoplon.geometry.Vec3f r1 = r1.mo4458ac(r2)     // Catch:{ all -> 0x0285 }
            r8.sub(r1, r15)     // Catch:{ all -> 0x0285 }
            float r2 = r8.dot(r8)     // Catch:{ all -> 0x0285 }
            int r2 = (r2 > r9 ? 1 : (r2 == r9 ? 0 : -1))
            if (r2 >= 0) goto L_0x0254
            r0 = r20
            com.hoplon.geometry.Vec3f r2 = r0.fcb     // Catch:{ all -> 0x0285 }
            r2.set(r1)     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            r1.reset()     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            a.ES$a r2 = r7.fcc     // Catch:{ all -> 0x0285 }
            boolean r2 = r2.cUl     // Catch:{ all -> 0x0285 }
            r1.cUm = r2     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            a.ES$a r2 = r7.fcc     // Catch:{ all -> 0x0285 }
            boolean r2 = r2.cUn     // Catch:{ all -> 0x0285 }
            r1.cUn = r2     // Catch:{ all -> 0x0285 }
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            a.ES$a r2 = r7.fcc     // Catch:{ all -> 0x0285 }
            boolean r2 = r2.cUm     // Catch:{ all -> 0x0285 }
            r1.cUo = r2     // Catch:{ all -> 0x0285 }
            r1 = 0
            float[] r2 = r7.fcd     // Catch:{ all -> 0x0285 }
            r3 = 0
            r2 = r2[r3]     // Catch:{ all -> 0x0285 }
            float[] r3 = r7.fcd     // Catch:{ all -> 0x0285 }
            r4 = 2
            r3 = r3[r4]     // Catch:{ all -> 0x0285 }
            float[] r4 = r7.fcd     // Catch:{ all -> 0x0285 }
            r5 = 1
            r4 = r4[r5]     // Catch:{ all -> 0x0285 }
            r0 = r20
            r0.mo1863m(r1, r2, r3, r4)     // Catch:{ all -> 0x0285 }
        L_0x0254:
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            boolean r1 = r1.cUl     // Catch:{ all -> 0x0285 }
            if (r1 == 0) goto L_0x0274
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            boolean r1 = r1.cUm     // Catch:{ all -> 0x0285 }
            if (r1 == 0) goto L_0x0274
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            boolean r1 = r1.cUn     // Catch:{ all -> 0x0285 }
            if (r1 == 0) goto L_0x0274
            r0 = r20
            a.ES$a r1 = r0.fcc     // Catch:{ all -> 0x0285 }
            boolean r1 = r1.cUo     // Catch:{ all -> 0x0285 }
            if (r1 == 0) goto L_0x0274
        L_0x0274:
            a.Kt r1 = r14.stack
            a.OY r1 = r1.bcH()
            r1.pop()
            a.QW<a.ES$b> r1 = r14.gZq
            r1.release(r7)
            r1 = 1
            goto L_0x00af
        L_0x0285:
            r1 = move-exception
            a.Kt r2 = r14.stack
            a.OY r2 = r2.bcH()
            r2.pop()
            a.QW<a.ES$b> r2 = r14.gZq
            r2.release(r7)
            throw r1
        L_0x0295:
            r9 = r10
            goto L_0x0187
        L_0x0298:
            r10 = r9
            goto L_0x0121
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C0327ES.mo1848a(com.hoplon.geometry.Vec3f, com.hoplon.geometry.Vec3f, com.hoplon.geometry.Vec3f, com.hoplon.geometry.Vec3f, com.hoplon.geometry.Vec3f, a.ES$b):boolean");
    }

    public void reset() {
        this.gZD = false;
        this.numVertices = 0;
        this.gZF = true;
        this.gZC.set(1.0E30f, 1.0E30f, 1.0E30f);
        this.gZE.reset();
    }

    /* renamed from: p */
    public void mo1858p(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        this.gZC.set(vec3f);
        this.gZF = true;
        this.gZw[this.numVertices].set(vec3f);
        this.gZx[this.numVertices].set(vec3f2);
        this.gZy[this.numVertices].set(vec3f3);
        this.numVertices++;
    }

    /* renamed from: aK */
    public boolean mo1849aK(Vec3f vec3f) {
        boolean cFU = cFU();
        vec3f.set(this.gZB);
        return cFU;
    }

    public float cgi() {
        int numVertices2 = numVertices();
        float f = 0.0f;
        int i = 0;
        while (i < numVertices2) {
            float lengthSquared = this.gZw[i].lengthSquared();
            if (f >= lengthSquared) {
                lengthSquared = f;
            }
            i++;
            f = lengthSquared;
        }
        return f;
    }

    public boolean cgj() {
        return this.numVertices == 4;
    }

    /* renamed from: a */
    public int mo1845a(Vec3f[] vec3fArr, Vec3f[] vec3fArr2, Vec3f[] vec3fArr3) {
        for (int i = 0; i < numVertices(); i++) {
            vec3fArr3[i].set(this.gZw[i]);
            vec3fArr[i].set(this.gZx[i]);
            vec3fArr2[i].set(this.gZy[i]);
        }
        return numVertices();
    }

    /* renamed from: aL */
    public boolean mo1850aL(Vec3f vec3f) {
        int numVertices2 = numVertices();
        boolean z = false;
        for (int i = 0; i < numVertices2; i++) {
            if (this.gZw[i].equals(vec3f)) {
                z = true;
            }
        }
        if (vec3f.equals(this.gZC)) {
            return true;
        }
        return z;
    }

    /* renamed from: aM */
    public void mo1851aM(Vec3f vec3f) {
        vec3f.set(this.gZB);
    }

    public boolean cgk() {
        return numVertices() == 0;
    }

    /* renamed from: D */
    public void mo1844D(Vec3f vec3f, Vec3f vec3f2) {
        cFU();
        vec3f.set(this.gZz);
        vec3f2.set(this.gZA);
    }

    public int numVertices() {
        return this.numVertices;
    }

    /* renamed from: a.ES$a */
    public static class C0328a {
        public boolean cUl;
        public boolean cUm;
        public boolean cUn;
        public boolean cUo;

        public void reset() {
            this.cUl = false;
            this.cUm = false;
            this.cUn = false;
            this.cUo = false;
        }
    }

    /* renamed from: a.ES$b */
    /* compiled from: a */
    public static class C0329b {
        public final Vec3f fcb = new Vec3f();
        public final C0328a fcc = new C0328a();
        public final float[] fcd = new float[4];
        public boolean fce;

        public void reset() {
            this.fce = false;
            mo1863m(0.0f, 0.0f, 0.0f, 0.0f);
            this.fcc.reset();
        }

        public boolean isValid() {
            if (this.fcd[0] < 0.0f || this.fcd[1] < 0.0f || this.fcd[2] < 0.0f || this.fcd[3] < 0.0f) {
                return false;
            }
            return true;
        }

        /* renamed from: m */
        public void mo1863m(float f, float f2, float f3, float f4) {
            this.fcd[0] = f;
            this.fcd[1] = f2;
            this.fcd[2] = f3;
            this.fcd[3] = f4;
        }
    }
}
