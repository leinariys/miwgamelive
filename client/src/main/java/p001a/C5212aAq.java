package p001a;

import game.script.player.Player;
import gnu.trove.TIntHashSet;
import logic.ui.IBaseUiTegXml;
import taikodom.addon.IAddonProperties;

import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.KeyEvent;

/* renamed from: a.aAq  reason: case insensitive filesystem */
/* compiled from: a */
public class C5212aAq implements KeyEventDispatcher {
    /* renamed from: kj */
    private final IAddonProperties f2342kj;
    long hdO;
    boolean hdP;
    private int hdM;
    private TIntHashSet hdN = new TIntHashSet();

    public C5212aAq(IAddonProperties vWVar) {
        this.f2342kj = vWVar;
    }

    public int cHM() {
        return this.hdM;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        PlayerController dxc;
        if (keyEvent.getID() == 402 && keyEvent.getKeyCode() == 18) {
            m12637b(keyEvent);
            return true;
        }
        JTextComponent focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        IBaseUiTegXml anu = IBaseUiTegXml.initBaseUItegXML();
        Player dL = this.f2342kj.getPlayer();
        boolean z = (focusOwner instanceof JTextComponent) && focusOwner.isEditable();
        if (dL != null && (((dxc = dL.dxc()) != null && dxc.anX() && !dxc.mo22135dL().bQB()) || focusOwner == anu.getRootPane() || focusOwner == anu.getRootPane().getParent().getParent() || focusOwner == anu.getRootPane().getParent())) {
            m12637b(keyEvent);
            keyEvent.consume();
            return true;
        } else if (focusOwner != null && !focusOwner.isShowing()) {
            m12637b(keyEvent);
            return false;
        } else if (keyEvent.getKeyCode() == 20 && !z) {
            m12637b(keyEvent);
            return false;
        } else if (focusOwner instanceof Panel) {
            m12637b(keyEvent);
            return false;
        } else if (z && keyEvent.getKeyCode() == 27) {
            focusOwner.transferFocusUpCycle();
            return false;
        } else if (keyEvent.getKeyCode() == 27) {
            m12637b(keyEvent);
            return false;
        } else if (!z && (keyEvent.getModifiers() != 0 || this.hdM != 0)) {
            m12637b(keyEvent);
            return false;
        } else if (keyEvent.getID() != 402 || !this.hdN.contains(keyEvent.getKeyCode())) {
            return false;
        } else {
            m12637b(keyEvent);
            return false;
        }
    }

    /* renamed from: b */
    private void m12637b(KeyEvent keyEvent) {
        int i;
        boolean z = true;
        switch (keyEvent.getID()) {
            case 400:
                if (keyEvent.getKeyCode() != 20 || !this.hdP || ((double) (System.nanoTime() - this.hdO)) >= 2.0E9d) {
                    mo7764d(new C2989mg(C0927Nb.m7693lP(keyEvent.getKeyCode()), true, keyEvent.getKeyChar(), C0927Nb.m7694lQ(keyEvent.getModifiersEx()), true));
                    return;
                }
                return;
            case 401:
                this.hdN.add(keyEvent.getKeyCode());
                if (keyEvent.getKeyCode() != 20 || !this.hdP || ((double) (System.nanoTime() - this.hdO)) >= 1.0E9d) {
                    int lP = C0927Nb.m7693lP(keyEvent.getKeyCode());
                    if (keyEvent.getKeyCode() == 32) {
                        this.hdM = keyEvent.getKeyCode();
                        mo7764d(new C2989mg(lP, true, keyEvent.getKeyChar(), C0927Nb.m7694lQ(keyEvent.getModifiersEx()), false));
                        return;
                    } else if (keyEvent.getKeyCode() == 16) {
                        mo7764d(new C2989mg(C0927Nb.m7693lP(keyEvent.getKeyCode()), true, keyEvent.getKeyChar(), 0, false));
                        return;
                    } else if (keyEvent.getKeyCode() == 17) {
                        mo7764d(new C2989mg(C0927Nb.m7693lP(keyEvent.getKeyCode()), true, keyEvent.getKeyChar(), 0, false));
                        return;
                    } else {
                        int modifiersEx = keyEvent.getModifiersEx();
                        if (this.hdM != 0) {
                            i = modifiersEx | this.hdM;
                        } else {
                            i = modifiersEx;
                        }
                        mo7764d(new C2989mg(C0927Nb.m7693lP(keyEvent.getKeyCode()), true, keyEvent.getKeyChar(), C0927Nb.m7694lQ(i), false));
                        return;
                    }
                } else {
                    return;
                }
            case 402:
                this.hdN.remove(keyEvent.getKeyCode());
                if (keyEvent.getKeyCode() == 20) {
                    if (!this.hdP || ((double) (System.nanoTime() - this.hdO)) >= 1.0E9d) {
                        this.hdO = System.nanoTime();
                        this.hdP = true;
                        try {
                            Toolkit defaultToolkit = Toolkit.getDefaultToolkit();
                            if (Toolkit.getDefaultToolkit().getLockingKeyState(20)) {
                                z = false;
                            }
                            defaultToolkit.setLockingKeyState(20, z);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        this.hdP = false;
                        return;
                    }
                }
                int lP2 = C0927Nb.m7693lP(keyEvent.getKeyCode());
                int modifiersEx2 = keyEvent.getModifiersEx();
                if (lP2 == 32 && this.hdM == 32) {
                    mo7764d(new C2989mg(lP2, false, keyEvent.getKeyChar(), C0927Nb.m7694lQ(modifiersEx2), false));
                    this.hdM = 0;
                } else {
                    if (this.hdM != 0) {
                        modifiersEx2 |= this.hdM;
                    }
                    mo7764d(new C2989mg(C0927Nb.m7693lP(keyEvent.getKeyCode()), false, keyEvent.getKeyChar(), C0927Nb.m7694lQ(modifiersEx2), false));
                }
                if (modifiersEx2 != 0) {
                    mo7764d(new C2989mg(lP2, false, keyEvent.getKeyChar(), 0, false));
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public void mo7764d(C2989mg mgVar) {
        this.f2342kj.getEngineGame().getEventManager().mo13975h(mgVar);
    }
}
