package p001a;

import game.script.Character;
import logic.res.XmlNode;
import logic.res.html.MessageContainer;
import logic.ui.item.ListJ;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

/* renamed from: a.alV  reason: case insensitive filesystem */
/* compiled from: a */
public class C6361alV {

    /* renamed from: Xa */
    private Map<String, Object> f4873Xa = new HashMap();
    private Set<C1936a> gGG = MessageContainer.dgI();

    /* renamed from: g */
    public Object mo14688g(XmlNode agy) {
        Object obj;
        String attribute = agy.getAttribute("xId");
        if (attribute != null && (obj = this.f4873Xa.get(attribute)) != null) {
            return obj;
        }
        m23765h(agy);
        cyZ();
        XmlNode p = agy.findNodeChildTagDepth("field", "fieldName", agy.getTagName());
        if (p == null) {
            return null;
        }
        Object obj2 = this.f4873Xa.get(p.getAttribute("id"));
        if (obj2 == null) {
            return null;
        }
        return obj2;
    }

    private char[] copyOfRange(char[] cArr, int i, int i2) {
        char[] cArr2 = new char[(i2 - i)];
        for (int i3 = i; i3 < i2; i3++) {
            cArr2[i3 - i] = cArr[i3];
        }
        return cArr2;
    }

    /* renamed from: h */
    private void m23765h(XmlNode agy) {
        Object obj;
        XmlNode mD;
        String content;
        long currentTimeMillis;
        for (int i = 0; i < agy.getListChildrenTag().size(); i++) {
            XmlNode agy2 = agy.getListChildrenTag().get(i);
            try {
                Class<?> objectClass = getObjectClass(agy2.getAttribute("class"));
                if (C0365Es.m3050v(objectClass)) {
                    Object newInstance = objectClass.newInstance();
                    String str = "";
                    for (XmlNode next : agy2.getListChildrenTag()) {
                        str = String.valueOf(str) + next.getAttribute("keyID") + "," + next.getAttribute("valueID") + ";";
                    }
                    this.gGG.add(new C1936a(agy2.getAttribute("id"), (Field) null, str));
                    obj = newInstance;
                } else if (C0365Es.m3049u(objectClass)) {
                    Object newInstance2 = objectClass.newInstance();
                    if (agy2.findNodeChildTagDepth("item") != null) {
                        String str2 = "";
                        for (XmlNode attribute : agy2.getListChildrenTag()) {
                            str2 = String.valueOf(str2) + attribute.getAttribute("id") + ",";
                        }
                        this.gGG.add(new C1936a(agy2.getAttribute("id"), (Field) null, str2));
                        obj = newInstance2;
                    } else {
                        obj = newInstance2;
                    }
                } else if (objectClass.isArray()) {
                    Object newInstance3 = Array.newInstance(objectClass.getComponentType(), agy2.getListChildrenTag().size());
                    if (C0365Es.m3051w(objectClass.getComponentType())) {
                        for (int i2 = 0; i2 < agy2.getListChildrenTag().size(); i2++) {
                            m23763a(newInstance3, i2, agy2.getListChildrenTag().get(i2).getAttribute("value"));
                        }
                        obj = newInstance3;
                    } else {
                        String str3 = "";
                        for (int i3 = 0; i3 < agy2.getListChildrenTag().size(); i3++) {
                            str3 = String.valueOf(str3) + agy2.getListChildrenTag().get(i3).getAttribute("id") + ",";
                        }
                        this.gGG.add(new C1936a(agy2.getAttribute("id"), (Field) null, str3));
                        obj = newInstance3;
                    }
                } else if (C0365Es.m3052x(objectClass)) {
                    XmlNode mD2 = agy2.findNodeChildTagDepth("value");
                    if (objectClass == Date.class && mD2 == null) {
                        content = String.valueOf(System.currentTimeMillis());
                    } else {
                        content = mD2.getContent();
                    }
                    if (objectClass == Character.class) {
                        obj = objectClass.getConstructor(new Class[]{Character.TYPE}).newInstance(new Object[]{Character.valueOf(content.charAt(0))});
                    } else if (objectClass == String.class) {
                        if (content == null) {
                            obj = objectClass.getConstructor(new Class[]{String.class}).newInstance(new Object[]{""});
                        } else {
                            StringBuffer stringBuffer = new StringBuffer();
                            char[] charArray = content.toCharArray();
                            int i4 = 0;
                            while (i4 < content.length()) {
                                if (charArray[i4] == '\\' && i4 + 1 < charArray.length && charArray[i4 + 1] == 'u') {
                                    stringBuffer.append(new String(Character.toChars(Integer.parseInt(new String(copyOfRange(charArray, i4 + 2, i4 + 6)), 16))));
                                    i4 += 6;
                                } else {
                                    stringBuffer.append(charArray[i4]);
                                    i4++;
                                }
                            }
                            obj = objectClass.getConstructor(new Class[]{String.class}).newInstance(new Object[]{stringBuffer.toString()});
                        }
                    } else if (objectClass == Date.class) {
                        try {
                            currentTimeMillis = Long.parseLong(content);
                        } catch (Exception e) {
                            currentTimeMillis = System.currentTimeMillis();
                        }
                        obj = objectClass.getConstructor(new Class[]{Long.TYPE}).newInstance(new Object[]{Long.valueOf(currentTimeMillis)});
                    } else {
                        obj = objectClass.getConstructor(new Class[]{String.class}).newInstance(new Object[]{content});
                    }
                } else {
                    Object newInstance4 = objectClass.getConstructor(new Class[0]).newInstance(new Object[0]);
                    for (Field next2 : m23762Y(objectClass)) {
                        next2.setAccessible(true);
                        if (!Modifier.isTransient(next2.getModifiers()) && !Modifier.isStatic(next2.getModifiers()) && (mD = agy2.findNodeChildTagDepth(next2.getName())) != null) {
                            if (next2.getType().isEnum()) {
                                Class<?> cls = Class.forName(mD.getAttribute("class"));
                                String attribute2 = mD.getAttribute("member");
                                Object[] enumConstants = cls.getEnumConstants();
                                int length = enumConstants.length;
                                int i5 = 0;
                                while (true) {
                                    if (i5 >= length) {
                                        break;
                                    }
                                    Object obj2 = enumConstants[i5];
                                    if (((Enum) obj2).name().equals(attribute2)) {
                                        next2.set(newInstance4, obj2);
                                        break;
                                    }
                                    i5++;
                                }
                            } else if (C0365Es.m3051w(next2.getType())) {
                                next2.set(newInstance4, m23764c(next2.getType(), String.class).newInstance(new Object[]{mD.getContent()}));
                            } else if (mD.getAttribute("id") != null) {
                                this.gGG.add(new C1936a(agy2.getAttribute("id"), next2, mD.getAttribute("id")));
                            }
                        }
                    }
                    obj = newInstance4;
                }
                if (obj != null) {
                    this.f4873Xa.put(agy2.getAttribute("id"), obj);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* renamed from: Y */
    private ListJ<Field> m23762Y(Class<?> cls) {
        ListJ<Field> dgK = MessageContainer.init();
        for (Class<? super Object> cls2 = cls; cls2 != null; cls2 = cls2.getSuperclass()) {
            for (Field add : cls2.getDeclaredFields()) {
                dgK.add(add);
            }
        }
        return dgK;
    }

    private Class<?> getObjectClass(String str) {
        return Class.forName(str);
    }

    /* renamed from: a */
    private void m23763a(Object obj, int i, String str) {
        if (obj.getClass().getName().equals("[I")) {
            Array.setInt(obj, i, new Integer(str).intValue());
        } else if (obj.getClass().getName().equals("[F")) {
            Array.setFloat(obj, i, new Float(str).floatValue());
        } else if (obj.getClass().getName().equals("[D")) {
            Array.setDouble(obj, i, new Double(str).doubleValue());
        } else if (obj.getClass().getName().equals("[J")) {
            Array.setLong(obj, i, new Long(str).longValue());
        } else if (obj.getClass().getName().equals("[B")) {
            Array.setByte(obj, i, new Byte(str).byteValue());
        } else if (obj.getClass().getName().equals("[C")) {
            Array.setChar(obj, i, str.toCharArray()[0]);
        } else if (obj.getClass().getName().equals("[S")) {
            Array.setShort(obj, i, new Short(str).shortValue());
        } else if (obj.getClass().getName().equals("[Z")) {
            Array.setBoolean(obj, i, new Boolean(str).booleanValue());
        }
    }

    public void cyZ() {
        Object obj;
        Object obj2;
        Iterator<C1936a> it = this.gGG.iterator();
        while (it.hasNext()) {
            C1936a next = it.next();
            Field field = next.field;
            Object obj3 = this.f4873Xa.get(next.fYj);
            if (field != null) {
                try {
                    field.set(obj3, this.f4873Xa.get(next.fYk));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (C0365Es.m3049u(obj3.getClass())) {
                StringTokenizer stringTokenizer = new StringTokenizer(next.fYk, ",");
                while (stringTokenizer.hasMoreTokens()) {
                    String nextToken = stringTokenizer.nextToken();
                    if (!nextToken.equals(aIX.ies)) {
                        Object obj4 = this.f4873Xa.get(nextToken);
                        if (obj4 == null) {
                            throw new RuntimeException("Pendent object does not exist");
                        }
                        obj2 = obj4;
                    } else {
                        obj2 = null;
                    }
                    ((ListJ) obj3).add(obj2);
                }
                continue;
            } else if (C0365Es.m3050v(obj3.getClass())) {
                StringTokenizer stringTokenizer2 = new StringTokenizer(next.fYk, ";");
                while (stringTokenizer2.hasMoreTokens()) {
                    StringTokenizer stringTokenizer3 = new StringTokenizer(stringTokenizer2.nextToken(), ",");
                    while (true) {
                        if (stringTokenizer3.hasMoreTokens()) {
                            String nextToken2 = stringTokenizer3.nextToken();
                            if (!nextToken2.equals(aIX.ies)) {
                                Object obj5 = this.f4873Xa.get(nextToken2);
                                if (obj5 == null) {
                                    throw new RuntimeException("Pendent key does not exist and is null: " + nextToken2);
                                }
                                obj = obj5;
                            } else {
                                obj = null;
                            }
                            ((Map) obj3).put(obj, this.f4873Xa.get(stringTokenizer3.nextToken()));
                        }
                    }
                }
                continue;
            } else if (obj3.getClass().isArray()) {
                StringTokenizer stringTokenizer4 = new StringTokenizer(next.fYk, ",");
                int i = 0;
                while (stringTokenizer4.hasMoreTokens()) {
                    String nextToken3 = stringTokenizer4.nextToken();
                    if (nextToken3.equals(aIX.ies)) {
                        Array.set(obj3, i, (Object) null);
                        i++;
                    } else {
                        Object obj6 = this.f4873Xa.get(nextToken3);
                        if (obj6 == null) {
                            throw new RuntimeException("Pendent object does not exist");
                        }
                        Array.set(obj3, i, obj6);
                        i++;
                    }
                }
                continue;
            } else {
                throw new RuntimeException("Type not supported");
            }
            it.remove();
        }
    }

    /* renamed from: c */
    private Constructor<?> m23764c(Class<?> cls, Class<?> cls2) {
        Class<Integer> cls3;
        try {
            if (cls == Integer.TYPE) {
                cls3 = Integer.class;
            } else if (cls == Float.TYPE) {
                cls3 = Float.class;
            } else if (cls == Double.TYPE) {
                cls3 = Double.class;
            } else if (cls == Long.TYPE) {
                cls3 = Long.class;
            } else if (cls == Byte.TYPE) {
                cls3 = Byte.class;
            } else if (cls == Character.TYPE) {
                cls3 = Character.class;
            } else if (cls == Short.TYPE) {
                cls3 = Short.class;
            } else {
                cls3 = cls;
                if (cls == Boolean.TYPE) {
                    cls3 = Boolean.class;
                }
            }
            return cls3.getConstructor(new Class[]{cls2});
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void cza() {
        for (Object next : this.f4873Xa.values()) {
            if (next instanceof C2886lU) {
                ((C2886lU) next).mo20230aH();
            }
        }
        this.f4873Xa.clear();
    }

    /* renamed from: a.alV$a */
    private static class C1936a {
        final String fYj;
        final String fYk;
        final Field field;

        public C1936a(String str, Field field2, String str2) {
            if (str == null || str2 == null) {
                throw new RuntimeException();
            }
            this.fYj = str;
            this.field = field2;
            this.fYk = str2;
        }

        public String toString() {
            return "Pendency[objID=" + this.fYj + ",field=" + this.field.getName() + ",pendentID=" + this.fYk + "]";
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            C1936a aVar = (C1936a) obj;
            if ((aVar.field == null || aVar.field.equals(this.field)) && aVar.fYj.equals(this.fYj) && aVar.fYk.equals(this.fYk)) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return ((this.field == null ? 0 : this.field.hashCode()) ^ this.fYk.hashCode()) ^ this.fYj.hashCode();
        }
    }
}
