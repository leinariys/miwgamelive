package p001a;

import taikodom.addon.C6144ahM;

import javax.swing.*;

/* renamed from: a.Nt */
/* compiled from: a */
public abstract class C0947Nt<T> implements C6144ahM<T> {
    /* renamed from: E */
    public abstract void mo4278E(T t);

    /* renamed from: c */
    public abstract void mo4279c(Throwable th);

    /* renamed from: n */
    public final void mo1931n(T t) {
        SwingUtilities.invokeLater(new C0949b(t));
    }

    /* renamed from: a */
    public final void mo1930a(Throwable th) {
        SwingUtilities.invokeLater(new C0948a(th));
    }

    /* renamed from: a.Nt$b */
    /* compiled from: a */
    class C0949b implements Runnable {
        private final /* synthetic */ Object ews;

        C0949b(Object obj) {
            this.ews = obj;
        }

        public void run() {
            C0947Nt.this.mo4278E(this.ews);
        }
    }

    /* renamed from: a.Nt$a */
    class C0948a implements Runnable {
        private final /* synthetic */ Throwable dUm;

        C0948a(Throwable th) {
            this.dUm = th;
        }

        public void run() {
            C0947Nt.this.mo4279c(this.dUm);
        }
    }
}
