package p001a;

import java.util.ArrayList;

/* renamed from: a.QW */
/* compiled from: a */
public class C1123QW<T> {
    private Class<T> dXq;
    private ArrayList<T> list = new ArrayList<>();

    public C1123QW(Class<T> cls) {
        this.dXq = cls;
    }

    private T create() {
        try {
            return this.dXq.newInstance();
        } catch (InstantiationException e) {
            throw new IllegalStateException(e);
        } catch (IllegalAccessException e2) {
            throw new IllegalStateException(e2);
        }
    }

    public T get() {
        if (this.list.size() > 0) {
            return this.list.remove(this.list.size() - 1);
        }
        return create();
    }

    public void release(T t) {
        this.list.add(t);
    }
}
