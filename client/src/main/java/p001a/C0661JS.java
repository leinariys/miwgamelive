package p001a;

import game.script.avatar.AvatarSector;
import game.script.avatar.body.BodyPiece;
import game.script.avatar.cloth.Cloth;
import gnu.trove.THashMap;
import logic.res.sound.C0907NJ;
import logic.thred.LogPrinter;
import logic.ui.item.ListJ;
import taikodom.render.RenderView;
import taikodom.render.helpers.MeshUtils;
import taikodom.render.helpers.RenderTask;
import taikodom.render.loader.FileSceneLoader;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.StockEntry;
import taikodom.render.primitives.Mesh;
import taikodom.render.scene.custom.RCustomMesh;

import java.io.File;
import java.io.IOException;
import java.util.*;

/* renamed from: a.JS */
/* compiled from: a */
public class C0661JS implements C3845vi {
    private static final LogPrinter logger = LogPrinter.setClass(C0661JS.class);
    private final THashMap<AvatarSector.C0955a, String> dKR;
    private final THashMap<AvatarSector.C0955a, RCustomMesh> ekA;
    private final THashMap<AvatarSector.C0955a, String> ekB;
    private final String ekC;
    private final Cloth eky;
    private final Map<AvatarSector, Boolean> ekz;
    /* access modifiers changed from: private */
    public Boolean dtj = false;
    /* access modifiers changed from: private */
    public ListJ<C3846a> dtl = new ArrayList();
    private C0907NJ ekD;

    public C0661JS(Cloth wj, String str) {
        this.eky = wj;
        this.ekC = str;
        this.ekA = new THashMap<>();
        this.ekB = new THashMap<>();
        this.dKR = new THashMap<>();
        this.ekz = new THashMap();
        for (BodyPiece bpk : wj.bEj()) {
            this.ekz.put(bpk.bpk(), false);
        }
        for (AvatarSector.C0955a aVar : AvatarSector.C0955a.values()) {
            String b = wj.mo6508b(aVar, C3845vi.C3847b.DIFFUSE, str);
            if (b != null) {
                try {
                    this.ekB.put(aVar, b);
                    String b2 = wj.mo6508b(aVar, C3845vi.C3847b.NORMAL, str);
                    if (b2 != null) {
                        this.dKR.put(aVar, b2);
                    }
                } catch (Exception e) {
                    logger.error("Cannot find texture file for this cloth! [file: " + b + "]");
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: cW */
    public void m5760cW(boolean z) {
        this.eky.ald().ale().mo3007a((RenderTask) new C0664c(z));
    }

    /* renamed from: a */
    private void m5758a(C3845vi.C3846a aVar, boolean z) {
        if (aVar != null) {
            this.eky.ald().ale().mo3007a((RenderTask) new C0662a(aVar, z));
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        akR();
        super.finalize();
    }

    public void akR() {
        synchronized (this.dtj) {
            if (this.dtj.booleanValue()) {
                for (RCustomMesh rCustomMesh : this.ekA.values()) {
                    rCustomMesh.dispose();
                    rCustomMesh.releaseReferences();
                }
                this.ekA.clear();
                this.dtj = false;
                logger.trace("ClothLayer " + this + " released resources.");
            }
        }
    }

    /* renamed from: a */
    public void mo3099a(C3845vi.C3846a aVar) {
        synchronized (this.dtj) {
            if (this.dtj.booleanValue()) {
                m5758a(aVar, true);
            } else if (this.eky.mo6524hx(this.ekC) == null) {
                this.dtj = true;
                m5758a(aVar, true);
            } else {
                this.dtl.add(aVar);
                if (this.ekD == null) {
                    this.eky.ald().ale().mo3007a((RenderTask) new C0663b());
                }
            }
        }
    }

    public boolean akQ() {
        synchronized (this.dtj) {
            String hx = this.eky.mo6524hx(this.ekC);
            if (hx == null) {
                this.dtj = true;
                return true;
            }
            try {
                FileSceneLoader aej = this.eky.ald().ale().aej();
                aej.loadFile(hx);
                THashMap tHashMap = new THashMap();
                for (BodyPiece next : this.eky.bEj()) {
                    tHashMap.put(next.bpo(), next.bpk());
                }
                Set keySet = tHashMap.keySet();
                for (StockEntry next2 : aej.getStockFile(hx).getEntries()) {
                    RenderAsset asset = next2.getAsset();
                    if (asset instanceof Mesh) {
                        String name = ((Mesh) asset).getGrannyMesh().bqM().mo17090wa(((Mesh) asset).getGrannyMaterialIndex()).cDE().getName();
                        if (keySet.contains(name)) {
                            Mesh mesh = (Mesh) aej.instantiateAsset(next2.getName());
                            AvatarSector nw = (AvatarSector) tHashMap.get(name);
                            if (nw != null) {
                                AvatarSector.C0955a biX = nw.biX();
                                RCustomMesh rCustomMesh = (RCustomMesh) this.ekA.get(biX);
                                if (rCustomMesh == null) {
                                    rCustomMesh = new RCustomMesh();
                                    this.ekA.put(biX, rCustomMesh);
                                }
                                rCustomMesh.addPart(mesh, false);
                            }
                        }
                    }
                }
                String path = aej.getBaseDir().getPath();
                for (Map.Entry entry : this.ekA.entrySet()) {
                    ((RCustomMesh) entry.getValue()).generateWeldingInfo(new File(path, hx), new File(path, String.valueOf(hx) + ((AvatarSector.C0955a) entry.getKey()).getSuffix()));
                }
                this.dtj = true;
                logger.trace("ClothLayer " + this + " released resources.");
                return true;
            } catch (IOException e) {
                logger.error("Cannot find granny file for this cloth! [file: " + hx + "]");
                return false;
            }
        }
    }

    /* renamed from: a */
    public RCustomMesh mo3097a(AvatarSector.C0955a aVar) {
        return (RCustomMesh) this.ekA.get(aVar);
    }

    /* renamed from: h */
    public String mo3108h(AvatarSector.C0955a aVar) {
        return (String) this.ekB.get(aVar);
    }

    /* renamed from: i */
    public String mo3109i(AvatarSector.C0955a aVar) {
        return (String) this.dKR.get(aVar);
    }

    /* renamed from: a */
    public void mo3098a(AvatarSector nw, boolean z) {
        this.ekz.put(nw, Boolean.valueOf(z));
    }

    public void akN() {
        for (AvatarSector put : this.ekz.keySet()) {
            this.ekz.put(put, true);
        }
    }

    public Collection<BodyPiece> akO() {
        return Collections.unmodifiableCollection(this.eky.bEj());
    }

    public void akP() {
        for (BodyPiece next : this.eky.bEj()) {
            if (next.bps()) {
                AvatarSector bpk = next.bpk();
                RCustomMesh rCustomMesh = (RCustomMesh) this.ekA.get(bpk.biX());
                Boolean bool = this.ekz.get(bpk);
                if (rCustomMesh != null) {
                    MeshUtils.setPartVisible(rCustomMesh, next.bpo(), bool.booleanValue());
                }
            }
        }
        for (RCustomMesh rebuild : this.ekA.values()) {
            MeshUtils.rebuild(rebuild);
        }
    }

    /* renamed from: b */
    public boolean mo3105b(AvatarSector.C0955a aVar) {
        return this.ekA.get(aVar) != null;
    }

    public Cloth bwa() {
        return this.eky;
    }

    /* renamed from: a.JS$c */
    /* compiled from: a */
    class C0664c implements RenderTask {
        private final /* synthetic */ boolean dmR;

        C0664c(boolean z) {
            this.dmR = z;
        }

        public void run(RenderView renderView) {
            synchronized (C0661JS.this.dtl) {
                for (C3845vi.C3846a a : C0661JS.this.dtl) {
                    a.mo4551a(C0661JS.this, this.dmR);
                }
                C0661JS.this.dtl.clear();
            }
        }
    }

    /* renamed from: a.JS$a */
    class C0662a implements RenderTask {
        private final /* synthetic */ C3845vi.C3846a dmQ;
        private final /* synthetic */ boolean dmR;

        C0662a(C3845vi.C3846a aVar, boolean z) {
            this.dmQ = aVar;
            this.dmR = z;
        }

        public void run(RenderView renderView) {
            this.dmQ.mo4551a(C0661JS.this, this.dmR);
        }
    }

    /* renamed from: a.JS$b */
    /* compiled from: a */
    class C0663b implements RenderTask {
        C0663b() {
        }

        public void run(RenderView renderView) {
            synchronized (C0661JS.this.dtj) {
                if (C0661JS.this.dtj.booleanValue()) {
                    C0661JS.this.m5760cW(true);
                } else {
                    C0661JS.this.m5760cW(C0661JS.this.akQ());
                }
            }
        }
    }
}
