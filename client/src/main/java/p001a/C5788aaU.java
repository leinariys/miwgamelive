package p001a;

import com.hoplon.geometry.Vec3f;

import javax.vecmath.Vector4f;
import java.util.List;

/* renamed from: a.aaU  reason: case insensitive filesystem */
/* compiled from: a */
public class C5788aaU {
    /* renamed from: a */
    public static boolean m19490a(List<Vector4f> list, Vec3f vec3f, float f) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Vector4f vector4f = list.get(i);
            if ((vector4f.w + C0647JL.m5589a(vector4f, vec3f)) - f > 0.0f) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    public static boolean m19492a(Vector4f vector4f, List<Vec3f> list, float f) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if ((C0647JL.m5589a(vector4f, list.get(i)) + vector4f.w) - f > 0.0f) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    private static boolean m19491a(Vector4f vector4f, List<Vector4f> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (C0647JL.m5595b(vector4f, list.get(i)) > 0.999f) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: b */
    public static void m19493b(List<Vec3f> list, List<Vector4f> list2) {
        C0763Kt bcE = C0763Kt.bcE();
        bcE.bcH().push();
        bcE.bcM().push();
        try {
            Vector4f vector4f = (Vector4f) bcE.bcM().get();
            Vec3f vec3f = (Vec3f) bcE.bcH().get();
            Vec3f vec3f2 = (Vec3f) bcE.bcH().get();
            Vec3f vec3f3 = (Vec3f) bcE.bcH().get();
            int size = list.size();
            for (int i = 0; i < size; i++) {
                Vec3f vec3f4 = list.get(i);
                for (int i2 = i + 1; i2 < size; i2++) {
                    Vec3f vec3f5 = list.get(i2);
                    for (int i3 = i2 + 1; i3 < size; i3++) {
                        vec3f.sub(vec3f5, vec3f4);
                        vec3f2.sub(list.get(i3), vec3f4);
                        float f = 1.0f;
                        for (int i4 = 0; i4 < 2; i4++) {
                            vec3f3.cross(vec3f, vec3f2);
                            vector4f.x = vec3f3.x * f;
                            vector4f.y = vec3f3.y * f;
                            vector4f.z = f * vec3f3.z;
                            if (C0647JL.m5599d(vector4f) > 1.0E-4f) {
                                C0647JL.m5600e(vector4f);
                                if (m19491a(vector4f, list2)) {
                                    vector4f.w = -C0647JL.m5589a(vector4f, vec3f4);
                                    if (m19492a(vector4f, list, 0.01f)) {
                                        list2.add(vector4f);
                                    }
                                }
                            }
                            f = -1.0f;
                        }
                    }
                }
            }
        } finally {
            bcE.bcH().pop();
            bcE.bcM().pop();
        }
    }

    /* renamed from: c */
    public static void m19494c(List<Vector4f> list, List<Vec3f> list2) {
        C0763Kt bcE = C0763Kt.bcE();
        bcE.bcH().push();
        try {
            Vec3f vec3f = (Vec3f) bcE.bcH().get();
            Vec3f vec3f2 = (Vec3f) bcE.bcH().get();
            Vec3f vec3f3 = (Vec3f) bcE.bcH().get();
            Vec3f vec3f4 = (Vec3f) bcE.bcH().get();
            int size = list.size();
            for (int i = 0; i < size; i++) {
                Vector4f vector4f = list.get(i);
                for (int i2 = i + 1; i2 < size; i2++) {
                    Vector4f vector4f2 = list.get(i2);
                    for (int i3 = i2 + 1; i3 < size; i3++) {
                        Vector4f vector4f3 = list.get(i3);
                        C0647JL.m5593a(vec3f, vector4f2, vector4f3);
                        C0647JL.m5593a(vec3f2, vector4f3, vector4f);
                        C0647JL.m5593a(vec3f3, vector4f, vector4f2);
                        if (vec3f.lengthSquared() > 1.0E-4f && vec3f2.lengthSquared() > 1.0E-4f && vec3f3.lengthSquared() > 1.0E-4f) {
                            float a = C0647JL.m5589a(vector4f, vec3f);
                            if (Math.abs(a) > 1.0E-6f) {
                                vec3f.scale(vector4f.w);
                                vec3f2.scale(vector4f2.w);
                                vec3f3.scale(vector4f3.w);
                                vec3f4.set(vec3f);
                                vec3f4.add(vec3f2);
                                vec3f4.add(vec3f3);
                                vec3f4.scale(-1.0f / a);
                                if (m19490a(list, vec3f4, 0.01f)) {
                                    list2.add(vec3f4);
                                }
                            }
                        }
                    }
                }
            }
        } finally {
            bcE.bcH().pop();
        }
    }
}
