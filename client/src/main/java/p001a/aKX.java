package p001a;

import game.network.aQD;
import org.apache.commons.jxpath.JXPathContext;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: a.aKX */
/* compiled from: a */
public class aKX extends aQD {
    /* renamed from: a */
    public String mo2073a(Iterator<?> it, List<String> list, Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        while (it.hasNext()) {
            JXPathContext newContext = JXPathContext.newContext(it.next());
            newContext.setFunctions(this.iEM);
            for (String value : list) {
                sb.append(";").append(newContext.getValue(value));
            }
            sb.append(";");
        }
        return sb.toString();
    }
}
