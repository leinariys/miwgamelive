package p001a;

import game.geometry.Matrix3fWrap;

/* renamed from: a.Cg */
/* compiled from: a */
public class C0215Cg extends C6518aoW<C3978xf> {
    /* renamed from: d */
    public C3978xf mo1157d(C3978xf xfVar) {
        C3978xf xfVar2 = (C3978xf) get();
        xfVar2.mo22947a(xfVar);
        return xfVar2;
    }

    /* renamed from: d */
    public C3978xf mo1156d(Matrix3fWrap ajd) {
        C3978xf xfVar = (C3978xf) get();
        xfVar.bFF.set(ajd);
        xfVar.bFG.set(0.0f, 0.0f, 0.0f);
        return xfVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: aBo */
    public C3978xf create() {
        return new C3978xf();
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void copy(C3978xf xfVar, C3978xf xfVar2) {
        xfVar.mo22947a(xfVar2);
    }
}
