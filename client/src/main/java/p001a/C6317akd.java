package p001a;

import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;

import java.io.IOException;

/* renamed from: a.akd  reason: case insensitive filesystem */
/* compiled from: a */
public class C6317akd implements aOT<C1058PU> {
    private static final Log logger = LogPrinter.m10275K(C6317akd.class);

    /* renamed from: PN */
    private final C1212Ru f4792PN;

    public C6317akd(C1212Ru ru) {
        this.f4792PN = ru;
    }

    /* renamed from: a */
    public void mo4838c(C1058PU pu) {
        pu.boj();
    }

    /* renamed from: b */
    public void mo4836b(C1058PU pu) {
        try {
            pu.mo4686a(this.f4792PN.mo5283k(pu.boe()));
        } catch (IOException e) {
            logger.warn("Trying to load a null trail file, ignoring");
        }
    }
}
