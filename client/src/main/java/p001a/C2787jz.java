package p001a;

import game.script.player.Player;
import logic.ui.item.C5923acz;

import javax.swing.*;

/* renamed from: a.jz */
/* compiled from: a */
public class C2787jz extends RowFilter {

    /* renamed from: P */
    private Player f8361P;
    private C5923acz akV;
    private C5923acz akW;
    private JCheckBox akX;
    private JTextField akY;
    private boolean akZ;
    private boolean ala;

    public C2787jz(C5923acz acz, C5923acz acz2, JCheckBox jCheckBox, Player aku, JTextField jTextField, boolean z, boolean z2) {
        this.akV = acz;
        this.akW = acz2;
        this.akX = jCheckBox;
        this.f8361P = aku;
        this.akY = jTextField;
        this.akZ = z;
        this.ala = z2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x013f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean include(javax.swing.RowFilter.Entry r9) {
        /*
            r8 = this;
            r3 = 1
            r4 = 0
            r0 = 2
            java.lang.Object r5 = r9.getValue(r0)
            java.lang.Object r0 = r9.getValue(r4)
            a.aEU r0 = (p001a.aEU) r0
            if (r0 != 0) goto L_0x0010
        L_0x000f:
            return r4
        L_0x0010:
            a.acz r1 = r8.akW
            java.lang.Object r1 = r1.getLastSelectedPathComponent()
            javax.swing.tree.DefaultMutableTreeNode r1 = (javax.swing.tree.DefaultMutableTreeNode) r1
            a.acz r2 = r8.akV
            java.lang.Object r2 = r2.getLastSelectedPathComponent()
            javax.swing.tree.DefaultMutableTreeNode r2 = (javax.swing.tree.DefaultMutableTreeNode) r2
            if (r2 == 0) goto L_0x0142
            boolean r6 = r2.isRoot()
            if (r6 == 0) goto L_0x0097
            r6 = r3
        L_0x0029:
            if (r1 == 0) goto L_0x013f
            java.lang.String r2 = r1.toString()
            boolean r2 = r5.equals(r2)
            boolean r5 = r1.isRoot()
            if (r5 == 0) goto L_0x00cf
            r2 = r3
        L_0x003a:
            javax.swing.JCheckBox r1 = r8.akX
            if (r1 == 0) goto L_0x013c
            javax.swing.JCheckBox r1 = r8.akX
            boolean r1 = r1.isSelected()
            if (r1 == 0) goto L_0x013c
            a.jC r1 = r0.mo8620eP()
            a.akU r5 = r8.f8361P
            boolean r1 = r1.mo22858m(r5)
            r5 = r1
        L_0x0051:
            boolean r1 = r8.akZ
            if (r1 == 0) goto L_0x0139
            java.lang.Object r1 = r9.getValue(r4)
            boolean r7 = r8.ala
            if (r7 == 0) goto L_0x0122
            boolean r7 = r1 instanceof p001a.C3592sf
            if (r7 == 0) goto L_0x0122
            a.sf r1 = (p001a.C3592sf) r1
            a.MM r1 = r1.abg()
            a.akU r7 = r8.f8361P
            if (r1 != r7) goto L_0x011f
            r1 = r3
        L_0x006c:
            if (r2 == 0) goto L_0x000f
            if (r6 == 0) goto L_0x000f
            a.jC r0 = r0.mo8620eP()
            taikodom.infra.game.script.I18NString r0 = r0.mo19891ke()
            java.lang.String r0 = r0.get()
            java.lang.String r0 = r0.toLowerCase()
            javax.swing.JTextField r2 = r8.akY
            java.lang.String r2 = r2.getText()
            java.lang.String r2 = r2.toLowerCase()
            boolean r0 = r0.contains(r2)
            if (r0 == 0) goto L_0x000f
            if (r5 == 0) goto L_0x000f
            if (r1 == 0) goto L_0x000f
            r4 = r3
            goto L_0x000f
        L_0x0097:
            java.lang.Object r6 = r2.getUserObject()
            if (r6 == 0) goto L_0x0142
            boolean r6 = r2.isLeaf()
            if (r6 != 0) goto L_0x00bb
            a.jC r6 = r0.mo8620eP()
            a.aai r6 = r6.mo19866HC()
            a.aai r6 = r6.bJS()
            java.lang.Object r2 = r2.getUserObject()
            if (r6 != r2) goto L_0x00b9
            r2 = r3
        L_0x00b6:
            r6 = r2
            goto L_0x0029
        L_0x00b9:
            r2 = r4
            goto L_0x00b6
        L_0x00bb:
            a.jC r6 = r0.mo8620eP()
            a.aai r6 = r6.mo19866HC()
            java.lang.Object r2 = r2.getUserObject()
            if (r6 != r2) goto L_0x00cd
            r2 = r3
        L_0x00ca:
            r6 = r2
            goto L_0x0029
        L_0x00cd:
            r2 = r4
            goto L_0x00ca
        L_0x00cf:
            java.lang.Object r5 = r1.getUserObject()
            if (r5 == 0) goto L_0x003a
            java.lang.Object r2 = r1.getUserObject()
            boolean r2 = r2 instanceof p001a.C0683Jj
            if (r2 == 0) goto L_0x00f5
            a.BF r2 = r0.mo8622eT()
            a.rP r2 = r2.azW()
            a.Jj r2 = r2.mo21606Nc()
            java.lang.Object r1 = r1.getUserObject()
            if (r2 != r1) goto L_0x00f3
            r1 = r3
        L_0x00f0:
            r2 = r1
            goto L_0x003a
        L_0x00f3:
            r1 = r4
            goto L_0x00f0
        L_0x00f5:
            boolean r2 = r1.isLeaf()
            if (r2 != 0) goto L_0x010f
            a.BF r2 = r0.mo8622eT()
            a.rP r2 = r2.azW()
            java.lang.Object r1 = r1.getUserObject()
            if (r2 != r1) goto L_0x010d
            r1 = r3
        L_0x010a:
            r2 = r1
            goto L_0x003a
        L_0x010d:
            r1 = r4
            goto L_0x010a
        L_0x010f:
            a.BF r2 = r0.mo8622eT()
            java.lang.Object r1 = r1.getUserObject()
            if (r2 != r1) goto L_0x011d
            r1 = r3
        L_0x011a:
            r2 = r1
            goto L_0x003a
        L_0x011d:
            r1 = r4
            goto L_0x011a
        L_0x011f:
            r1 = r4
            goto L_0x006c
        L_0x0122:
            boolean r7 = r1 instanceof p001a.C3966xT
            if (r7 == 0) goto L_0x0136
            a.xT r1 = (p001a.C3966xT) r1
            a.MM r1 = r1.apa()
            a.akU r7 = r8.f8361P
            if (r1 != r7) goto L_0x0133
            r1 = r3
            goto L_0x006c
        L_0x0133:
            r1 = r4
            goto L_0x006c
        L_0x0136:
            r1 = r4
            goto L_0x006c
        L_0x0139:
            r1 = r3
            goto L_0x006c
        L_0x013c:
            r5 = r3
            goto L_0x0051
        L_0x013f:
            r2 = r3
            goto L_0x003a
        L_0x0142:
            r6 = r4
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C2787jz.include(javax.swing.RowFilter$Entry):boolean");
    }
}
