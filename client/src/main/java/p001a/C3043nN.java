package p001a;

import logic.res.html.MessageContainer;
import logic.thred.PoolThread;

import java.util.List;

/* renamed from: a.nN */
/* compiled from: a */
public class C3043nN {
    /* access modifiers changed from: private */
    public List<C3044a> aLA = MessageContainer.init();
    /* access modifiers changed from: private */
    public List<C3044a> aLz = MessageContainer.init();
    private int aLB;

    public C3043nN(int i) {
        this.aLB = i;
    }

    /* renamed from: RG */
    public int mo20745RG() {
        int size;
        synchronized (this.aLz) {
            size = this.aLA.size();
        }
        return size;
    }

    /* renamed from: RH */
    public int mo20746RH() {
        return this.aLB;
    }

    /* renamed from: dl */
    public void mo20749dl(int i) {
        this.aLB = i;
    }

    /* renamed from: b */
    public void mo20747b(Runnable runnable) {
        synchronized (this.aLz) {
            if (this.aLz.isEmpty()) {
                C3044a aVar = new C3044a(this, (C3044a) null);
                aVar.setDaemon(true);
                this.aLA.add(aVar);
                aVar.mo20751h(runnable);
                aVar.start();
            } else {
                C3044a remove = this.aLz.remove(0);
                this.aLA.add(remove);
                remove.mo20751h(runnable);
            }
        }
    }

    public void dispose() {
        synchronized (this.aLz) {
            for (C3044a dispose : this.aLz) {
                dispose.dispose();
            }
            for (C3044a dispose2 : this.aLA) {
                dispose2.dispose();
            }
        }
    }

    /* renamed from: a.nN$a */
    private class C3044a extends Thread {
        private Runnable runnable;
        private boolean running;

        private C3044a() {
            this.running = true;
        }

        /* synthetic */ C3044a(C3043nN nNVar, C3044a aVar) {
            this();
        }

        public void run() {
            while (isRunning()) {
                if (this.runnable != null) {
                    this.runnable.run();
                }
                synchronized (C3043nN.this.aLz) {
                    C3043nN.this.aLA.remove(this);
                    if (C3043nN.this.aLz.size() < C3043nN.this.mo20746RH()) {
                        C3043nN.this.aLz.add(this);
                    }
                }
                dvT();
            }
        }

        private synchronized void dvT() {
            PoolThread.wait(this);
        }

        /* renamed from: h */
        public synchronized void mo20751h(Runnable runnable2) {
            this.runnable = runnable2;
            notify();
        }

        public synchronized boolean isRunning() {
            return this.running;
        }

        public synchronized void dispose() {
            this.running = false;
            notify();
        }
    }
}
