package p001a;

import gnu.trove.THashMap;
import logic.ui.item.Picture;
import org.objectweb.asm.tree.AnnotationNode;

import java.util.Map;

/* renamed from: a.ayO */
/* compiled from: a */
public class ayO extends MethodAdapter implements Opcodes {
    public final String _name;
    public final String desc;
    public final Type gVW;
    public final Type[] gVX;
    public final String[] gVY;
    public final String name;
    public final String signature;
    public final int access;
    public final C6416amY cUO;
    public int index;
    public Map<String, AnnotationNode> cUY;
    public int gWa = 1;
    public int gWb = 2;
    public int maxLocals;
    public int maxStack;
    private int gVZ;

    public ayO(MethodVisitor methodVisitor, C6416amY amy, int i, String str, String str2, String str3, String[] strArr) {
        super(methodVisitor);
        this.access = i;
        this.name = str;
        this.desc = str2;
        this._name = String.valueOf(str) + "_x_";
        this.signature = str3;
        this.gVY = strArr;
        this.gVW = Type.getReturnType(str2);
        this.gVX = Type.getArgumentTypes(str2);
        this.cUO = amy;
    }

    public void visitMethodInsn(int i, String str, String str2, String str3) {
        if (i != 183 || !C2821kb.arR.equals(str2) || !this.cUO.gci.mo4573ge(str).aBz()) {
            ayO.super.visitMethodInsn(i, str, str2, str3);
        } else {
            ayO.super.visitMethodInsn(182, str, C2821kb.arY, str3);
        }
    }

    public void visitTypeInsn(int i, String str) {
        if (i != 187 || !this.cUO.gci.mo4573ge(str).aBz()) {
            ayO.super.visitTypeInsn(i, str);
            return;
        }
        this.cUO.mo14851e(this.mv);
        this.mv.visitLdcInsn(Type.getObjectType(str));
        C2821kb.are.mo20041a(this.mv, 185);
        this.mv.visitTypeInsn(192, str);
    }

    public AnnotationVisitor visitAnnotation(String str, boolean z) {
        AnnotationNode annotationNode = new AnnotationNode(str);
        C0925NZ nz = new C0925NZ(ayO.super.visitAnnotation(str, z), annotationNode);
        if (this.cUY == null) {
            this.cUY = new THashMap();
        }
        this.cUY.put(str, annotationNode);
        return nz;
    }

    public void visitLineNumber(int i, Picture label) {
        ayO.super.visitLineNumber(i, label);
        this.gVZ = i;
    }

    public void visitFieldInsn(int i, String str, String str2, String str3) {
        C0404Fd jv;
        if (!str.equals(this.cUO.name) || (jv = this.cUO.mo14853jv(str2)) == null) {
            ayO.super.visitFieldInsn(i, str, str2, str3);
            return;
        }
        switch (i) {
            case 178:
                if ((this.access | 8) == 0) {
                    this.cUO.gci.mo4567a(this.cUO, this, this.gVZ, "Access to templated field %s in static method.", jv.name);
                    break;
                } else {
                    ayO.super.visitVarInsn(25, 0);
                    ayO.super.visitMethodInsn(183, str, jv.cUR, jv.cUS);
                    return;
                }
            case 180:
                ayO.super.visitMethodInsn(183, str, jv.cUR, jv.cUS);
                return;
            case 181:
                ayO.super.visitMethodInsn(183, str, jv.cUT, jv.cUU);
                return;
        }
        ayO.super.visitFieldInsn(i, str, str2, str3);
    }

    public void visitMaxs(int i, int i2) {
        ayO.super.visitMaxs(this.gWb + i, this.gWa + i2);
    }
}
