package p001a;

import game.network.message.externalizable.C3689to;
import game.script.item.ComponentType;
import game.script.item.ItemType;
import game.script.item.ItemTypeCategory;
import game.script.player.PlayerAssistant;
import logic.aaa.C5783aaP;
import logic.render.GUIModule;
import logic.swing.C5378aHa;
import logic.ui.ComponentManager;
import logic.ui.Panel;
import logic.ui.item.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.newmarket.C2331eF;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;

/* renamed from: a.bw */
/* compiled from: a */
public class C2092bw {
    /* access modifiers changed from: private */
    public C2331eF aoM;
    /* access modifiers changed from: private */
    public JTextField ebf;
    /* access modifiers changed from: private */
    public JSpinner ejt;
    /* access modifiers changed from: private */
    public JTextField eju;
    /* access modifiers changed from: private */
    public JComboBox ejv;
    /* access modifiers changed from: private */
    public JTextField ejw;
    /* access modifiers changed from: private */
    public C5923acz ejy;
    /* access modifiers changed from: private */
    public String ejz;
    /* renamed from: kj */
    public IAddonProperties f5928kj;
    private C3428rT<C5783aaP> aoN;
    private JButton aoO;
    private JButton aoP;
    private C2495fo apa;
    private C5519aMl ejA = new C6375alj(this);
    private JTextField ejx;
    /* access modifiers changed from: private */
    /* renamed from: hP */
    private List<? extends ItemType> f5927hP;
    /* renamed from: nM */
    private InternalFrame f5929nM;

    /* renamed from: nP */
    private C3428rT<C3689to> f5930nP;

    public C2092bw(IAddonProperties vWVar, C2331eF eFVar, List<? extends ItemType> list, C2495fo foVar) {
        this.aoM = eFVar;
        this.f5928kj = vWVar;
        this.f5928kj.bHv().mo16790b((Object) foVar, "buyorder.css");
        this.f5927hP = list;
        this.apa = foVar;
        this.aoN = new C2099g();
        this.f5930nP = new C2098f();
        m28104Im();
        bvn();
        m28103Fa();
        GUIModule bhd = C5916acs.getSingolton().getGuiModule();
        this.f5929nM.setLocation((bhd.getScreenWidth() / 2) - (this.f5929nM.getWidth() / 2), ((bhd.getScreenHeight() / 2) - (this.f5929nM.getHeight() / 2)) - 50);
        this.f5929nM.setVisible(true);
    }

    /* access modifiers changed from: private */
    /* renamed from: Ip */
    public void m28105Ip() {
        this.aoO.setEnabled(false);
        if (bvm()) {
            long parseLong = Long.parseLong(this.eju.getText()) * ((long) ((Integer) this.ejt.getValue()).intValue());
            this.ejx.setText(Long.toString(parseLong));
            ((PlayerAssistant) C3582se.m38985a(this.f5928kj.getPlayer().dyl(), (C6144ahM<?>) new C2101i())).mo11983fk(parseLong);
            this.aoO.setEnabled(true);
        }
    }

    /* renamed from: Fa */
    private void m28103Fa() {
        this.ebf.getDocument().addDocumentListener(new C2100h());
        this.ejt.addChangeListener(new C2103k());
        this.eju.getDocument().addDocumentListener(new C2102j());
        this.aoO.addActionListener(new C2097e());
        this.aoP.addActionListener(new C2096d());
        this.f5928kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        this.f5928kj.aVU().mo13965a(C3689to.class, this.f5930nP);
    }

    /* access modifiers changed from: private */
    public boolean bvm() {
        if (!(this.ejt.getValue() instanceof Integer)) {
            return false;
        }
        if (this.ejy.getLastSelectedPathComponent() == null) {
            return false;
        }
        if (!(((DefaultMutableTreeNode) this.ejy.getLastSelectedPathComponent()).getUserObject() instanceof ItemType)) {
            return false;
        }
        try {
            Long.parseLong(this.eju.getText());
            if (this.ejv.getSelectedItem() == null) {
                return false;
            }
            return true;
        } catch (RuntimeException e) {
            return false;
        }
    }

    private void bvn() {
        this.aoO.setEnabled(false);
        this.ejv.removeAllItems();
        this.ejv.addItem(new C2095c(C3666tU.VERY_SHORT, this.f5928kj.translate("Very Short")));
        this.ejv.addItem(new C2095c(C3666tU.SHORT, this.f5928kj.translate("Short")));
        this.ejv.addItem(new C2095c(C3666tU.AVERAGE, this.f5928kj.translate("Average")));
        this.ejv.addItem(new C2095c(C3666tU.LONG, this.f5928kj.translate("Long")));
        this.ejv.addItem(new C2095c(C3666tU.VERY_LONG, this.f5928kj.translate("Very Long")));
        this.ejv.setSelectedIndex(0);
        C6964axd axd = new C6964axd("hidden root", new C2093a());
        TreePath treePath = new TreePath(axd);
        this.ejy.setModel(new C5518aMk(axd, this.ejA));
        this.ejy.setCellRenderer(new C2094b());
        HashMap hashMap = new HashMap();
        for (ItemType jCVar : this.f5927hP) {
            ItemTypeCategory HC = jCVar.mo19866HC();
            if (HC != null) {
                ItemTypeCategory bJS = HC.bJS();
                if (!hashMap.containsKey(bJS)) {
                    C6964axd axd2 = new C6964axd(bJS, this.ejA);
                    hashMap.put(bJS, axd2);
                    axd.add(axd2);
                }
                if (!hashMap.containsKey(HC)) {
                    C6964axd axd3 = new C6964axd(HC, this.ejA);
                    hashMap.put(HC, axd3);
                    ((DefaultMutableTreeNode) hashMap.get(bJS)).add(axd3);
                }
                ((DefaultMutableTreeNode) hashMap.get(HC)).add(new C6964axd(jCVar, this.ejA));
            }
        }
        this.ejy.expandPath(treePath);
    }

    /* renamed from: Im */
    private void m28104Im() {
        this.f5929nM = this.f5928kj.bHv().mo16795c(this.apa.getClass(), "buyorder.xml");
        this.ejt = this.f5929nM.mo4915cd("amount");
        this.eju = this.f5929nM.mo4915cd("unityValue");
        this.ejy = this.f5929nM.mo4915cd("itemTree");
        this.ejx = this.f5929nM.mo4915cd("totalValue");
        this.ejx.setEditable(false);
        this.ejw = this.f5929nM.mo4915cd("tax");
        this.ejw.setEditable(false);
        this.ejv = this.f5929nM.mo4914cc("lifeTime");
        this.aoO = this.f5929nM.mo4913cb("ok");
        this.aoP = this.f5929nM.mo4913cb("cancel");
        this.ebf = this.f5929nM.mo4915cd("search");
    }

    /* access modifiers changed from: private */
    public void hide() {
        this.f5929nM.setVisible(false);
        removeListeners();
        this.f5929nM.destroy();
    }

    private void removeListeners() {
        this.f5928kj.aVU().mo13964a(this.f5930nP);
        this.f5928kj.aVU().mo13964a(this.aoN);
    }

    /* access modifiers changed from: private */
    public boolean isVisible() {
        return this.f5929nM != null && this.f5929nM.isVisible();
    }

    /* renamed from: a.bw$c */
    /* compiled from: a */
    private class C2095c<T> {
        private String name;
        private T object;

        public C2095c(T t, String str) {
            this.name = str;
            this.object = t;
        }

        public T getObject() {
            return this.object;
        }

        public String toString() {
            return this.name;
        }
    }

    /* renamed from: a.bw$g */
    /* compiled from: a */
    class C2099g extends C3428rT<C5783aaP> {
        C2099g() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (C2092bw.this.isVisible()) {
                C2092bw.this.hide();
            }
        }
    }

    /* renamed from: a.bw$f */
    /* compiled from: a */
    class C2098f extends C3428rT<C3689to> {
        C2098f() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (C2092bw.this.isVisible()) {
                C2092bw.this.hide();
                toVar.adC();
            }
        }
    }

    /* renamed from: a.bw$i */
    /* compiled from: a */
    class C2101i implements C6144ahM<Long> {
        C2101i() {
        }

        /* renamed from: a */
        public void mo1931n(Long l) {
            C2092bw.this.ejw.setText(Long.toString(l.longValue()));
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }
    }

    /* renamed from: a.bw$h */
    /* compiled from: a */
    class C2100h implements DocumentListener {
        C2100h() {
        }

        public void changedUpdate(DocumentEvent documentEvent) {
        }

        public void insertUpdate(DocumentEvent documentEvent) {
            C2092bw.this.ejz = C2092bw.this.ebf.getText();
            if (C2092bw.this.ejz.equals("")) {
                C2092bw.this.ejz = null;
            }
            C2092bw.this.ejy.getModel().reload();
        }

        public void removeUpdate(DocumentEvent documentEvent) {
            C2092bw.this.ejz = C2092bw.this.ebf.getText();
            if (C2092bw.this.ejz.equals("")) {
                C2092bw.this.ejz = null;
            }
            C2092bw.this.ejy.getModel().reload();
        }
    }

    /* renamed from: a.bw$k */
    /* compiled from: a */
    class C2103k implements ChangeListener {
        C2103k() {
        }

        public void stateChanged(ChangeEvent changeEvent) {
            C2092bw.this.m28105Ip();
        }
    }

    /* renamed from: a.bw$j */
    /* compiled from: a */
    class C2102j implements DocumentListener {
        C2102j() {
        }

        public void changedUpdate(DocumentEvent documentEvent) {
            C2092bw.this.m28105Ip();
        }

        public void insertUpdate(DocumentEvent documentEvent) {
            C2092bw.this.m28105Ip();
        }

        public void removeUpdate(DocumentEvent documentEvent) {
            C2092bw.this.m28105Ip();
        }
    }

    /* renamed from: a.bw$e */
    /* compiled from: a */
    class C2097e implements ActionListener {
        C2097e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (C2092bw.this.bvm()) {
                C2092bw.this.aoM.mo17964a((ItemType) ((DefaultMutableTreeNode) C2092bw.this.ejy.getLastSelectedPathComponent()).getUserObject(), ((Integer) C2092bw.this.ejt.getValue()).intValue(), 1, Long.parseLong(C2092bw.this.eju.getText()), (long) ((C3666tU) ((C2095c) C2092bw.this.ejv.getSelectedItem()).getObject()).getLifeTime());
            }
            C2092bw.this.hide();
        }
    }

    /* renamed from: a.bw$d */
    /* compiled from: a */
    class C2096d implements ActionListener {
        C2096d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C2092bw.this.hide();
        }
    }

    /* renamed from: a.bw$a */
    class C2093a extends C5519aMl {
        C2093a() {
        }

        /* renamed from: a */
        public boolean mo10165a(Object obj) {
            return false;
        }

        /* renamed from: z */
        public boolean mo10166z() {
            return false;
        }
    }

    /* renamed from: a.bw$b */
    /* compiled from: a */
    class C2094b extends C0037AU {
        C2094b() {
        }

        /* renamed from: a */
        public Component mo307a(aUR aur, Object obj, boolean z, boolean z2, boolean z3, int i, boolean z4) {
            String str;
            Image image;
            Panel nR;
            JLabel cf;
            JLabel jLabel;
            Object userObject = ((DefaultMutableTreeNode) obj).getUserObject();
            if (userObject instanceof ItemType) {
                ItemType jCVar = (ItemType) userObject;
                str = jCVar.mo19891ke().get();
                image = C2092bw.this.f5928kj.bHv().adz().getImage(String.valueOf(C5378aHa.ITEMS.getPath()) + jCVar.mo12100sK().getHandle());
            } else if (userObject instanceof ItemTypeCategory) {
                ItemTypeCategory aai = (ItemTypeCategory) userObject;
                String str2 = aai.mo12246ke().get();
                if (aai.mo12100sK() != null) {
                    str = str2;
                    image = C2092bw.this.f5928kj.bHv().adz().getImage(String.valueOf(C5378aHa.ICONOGRAPHY.getPath()) + aai.mo12100sK().getHandle());
                } else {
                    str = str2;
                    image = null;
                }
            } else {
                str = null;
                image = null;
            }
            if (z3) {
                nR = aur.mo11631nR("itemType");
                JLabel cf2 = nR.mo4917cf("itemIconLabel");
                LabeledIcon cd = nR.mo4915cd("item");
                if (image == null) {
                    cd.mo2605a((Icon) new ImageIcon());
                    jLabel = cf2;
                } else if (userObject instanceof ItemType) {
                    cd.mo2605a((Icon) new C2539gY(image));
                    Picture a = cd.mo2601a(LabeledIcon.C0530a.NORTH_WEST);
                    if (userObject instanceof ComponentType) {
                        ComponentManager.getCssHolder(a).setAttribute("class", "size" + ((ComponentType) userObject).cuJ());
                        a.setVisible(true);
                        jLabel = cf2;
                    } else {
                        a.setVisible(false);
                        jLabel = cf2;
                    }
                } else {
                    cd.mo2605a((Icon) new ImageIcon(image));
                    jLabel = cf2;
                }
            } else {
                if (z2) {
                    nR = aur.mo11631nR("categoryExpanded");
                    cf = nR.mo4917cf("categoryIconLabel");
                } else {
                    nR = aur.mo11631nR("category");
                    cf = nR.mo4917cf("categoryIconLabel");
                }
                if (image == null) {
                    cf.setIcon((Icon) null);
                    jLabel = cf;
                } else if (userObject instanceof ItemType) {
                    cf.setIcon(new C2539gY(image));
                    jLabel = cf;
                } else {
                    cf.setIcon(new ImageIcon(image));
                    jLabel = cf;
                }
            }
            jLabel.setText(str);
            return nR;
        }
    }
}
