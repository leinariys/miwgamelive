package p001a;

/* renamed from: a.NU */
/* compiled from: a */
public class C0920NU {
    /* renamed from: g */
    public static float m7655g(float f, float f2, float f3) {
        return f >= 0.0f ? f2 : f3;
    }

    /* renamed from: hc */
    public static boolean m7656hc(float f) {
        return Math.abs(f) < 1.1920929E-7f;
    }

    /* renamed from: m */
    public static float m7657m(float f, float f2) {
        float f3;
        float f4 = 3.0f * 0.7853982f;
        float abs = Math.abs(f);
        if (f2 >= 0.0f) {
            f3 = 0.7853982f - (((f2 - abs) / (abs + f2)) * 0.7853982f);
        } else {
            f3 = f4 - (0.7853982f * ((f2 + abs) / (abs - f2)));
        }
        return f < 0.0f ? -f3 : f3;
    }
}
