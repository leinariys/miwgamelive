package p001a;

import game.script.player.Player;
import logic.swing.C0454GJ;
import logic.swing.C3940wz;
import logic.ui.ComponentManager;
import logic.ui.Panel;
import logic.ui.item.C2877lL;
import logic.ui.item.InternalFrame;
import logic.ui.item.TaskPane;
import logic.ui.item.TaskPaneContainer;
import taikodom.addon.IAddonProperties;
import taikodom.addon.neo.preferences.GUIPrefAddon;
import taikodom.addon.neo.sidebar.SideBarManagerAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/* renamed from: a.ls */
/* compiled from: a */
public class C2925ls {
    /* access modifiers changed from: private */

    /* renamed from: kj */
    public static IAddonProperties f8587kj;
    public final boolean aTI;
    /* renamed from: P */
    private final Player f8588P;
    /* access modifiers changed from: private */
    public String aTJ;
    /* access modifiers changed from: private */
    public InternalFrame aTK;
    /* access modifiers changed from: private */
    public TaskPaneContainer ahE;
    private C3629tB aTL;
    private JButton aTM;
    private C2762jf aTN;
    private int aTO;
    private Panel ahK;

    public C2925ls(IAddonProperties vWVar, boolean z, C2877lL lLVar, C2877lL lLVar2) {
        Point point;
        f8587kj = vWVar;
        this.aTI = z;
        this.f8588P = vWVar.getPlayer();
        this.aTO = vWVar.bHv().getScreenHeight();
        if (this.f8588P != null) {
            m35314Im();
            m35327iM();
            lLVar.setComponent(this.aTK);
            lLVar2.setComponent(this.aTK);
            this.aTK.mo20867b(lLVar);
            this.aTK.mo20864a(lLVar2);
            m35317Ws();
            if (z) {
                ComponentManager.getCssHolder(this.aTK).setAttribute("class", "left");
                point = new Point(0, 0);
                this.aTK.add((Component) this.aTM, (Object) "East");
                this.aTJ = "sbar.left";
            } else {
                ComponentManager.getCssHolder(this.aTK).setAttribute("class", "right");
                point = new Point(vWVar.bHv().getScreenWidth() - this.aTK.getWidth(), 0);
                this.aTK.add((Component) this.aTM, (Object) "West");
                this.aTJ = "sbar.right";
            }
            this.aTK.setLocation(point);
        }
    }

    /* renamed from: Im */
    private void m35314Im() {
        this.aTK = f8587kj.bHv().mo16792bN("sidebar.xml");
        this.aTK.setSize(this.aTK.getPreferredSize().width, f8587kj.bHv().getScreenHeight());
    }

    /* renamed from: iM */
    private void m35327iM() {
        this.ahE = this.aTK.mo4915cd("contentPanel");
        this.ahK = this.aTK.mo4915cd("overlay");
        this.ahK.addMouseListener(new C2930e());
        this.aTN = new C2762jf(this.ahE, this.ahK);
        this.aTL = new C3629tB(f8587kj, this.aTI);
        this.ahE.add((Component) this.aTL, (int) Integer.MIN_VALUE);
        m35316Wr();
    }

    /* renamed from: Wr */
    private void m35316Wr() {
        this.aTM = new JButton();
        ComponentManager.getCssHolder(this.aTM).setAttribute("name", "resize-button");
        C2926a aVar = new C2926a();
        this.aTM.addMouseListener(aVar);
        this.aTM.addMouseMotionListener(aVar);
    }

    /* renamed from: Ws */
    private void m35317Ws() {
        int intValue = GUIPrefAddon.m44886a(String.valueOf(this.aTJ) + "." + "divisor", GUIPrefAddon.C4817c.O, (Integer) - 1).intValue();
        TaskPaneContainer xxVar = this.ahE;
        if (intValue == -1) {
            intValue = 500;
        }
        xxVar.mo23037nm(intValue);
        int intValue2 = GUIPrefAddon.m44886a(this.aTJ, GUIPrefAddon.C4817c.WIDTH, (Integer) - 1).intValue();
        if (intValue2 != -1) {
            Dimension dimension = new Dimension(intValue2, this.aTK.getHeight());
            this.aTK.setSize(dimension);
            this.aTK.setPreferredSize(dimension);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: dL */
    public boolean m35326dL(int i) {
        return i >= this.aTK.getMinimumSize().width && i <= this.aTK.getMaximumSize().width;
    }

    /* renamed from: m */
    public void mo20420m(boolean z) {
        this.aTN.mo19983a(z, (C0454GJ) new C2927b(z));
    }

    /* renamed from: a */
    public void mo20415a(aGC agc, TaskPane bpVar, int i) {
        if (agc != null && bpVar != null) {
            if (!SideBarManagerAddon.m44918iL()) {
                JLabel djn = bpVar.djn();
                djn.putClientProperty("command", agc);
                djn.setToolTipText(agc.crH());
                C3940wz.m40777a((JComponent) djn, "windowTooltipProvider");
            }
            String crI = agc.crI();
            int intValue = GUIPrefAddon.m44886a(crI, GUIPrefAddon.C4817c.O, (Integer) - 1).intValue();
            GUIPrefAddon.m44889a(this.ahE, bpVar, crI);
            TaskPaneContainer xxVar = this.ahE;
            if (intValue != -1) {
                i = intValue;
            }
            xxVar.add((Component) bpVar, i);
            GUIPrefAddon.m44886a(crI, GUIPrefAddon.C4817c.P, (Integer) 0).intValue();
            m35321a(bpVar, agc);
            bpVar.setVisible(agc.isEnabled());
        }
    }

    /* renamed from: a */
    public void mo20416a(TaskPane bpVar) {
        if (bpVar != null) {
            this.ahE.remove((Component) bpVar);
        }
    }

    public void clear() {
        this.ahE.removeAll();
        this.aTL.clear();
    }

    /* renamed from: a */
    private void m35321a(TaskPane bpVar, aGC agc) {
        String crI = agc.crI();
        bpVar.djp().addComponentListener(new C2928c(agc));
        bpVar.mo17421a((TaskPane.C2072c) new C2929d(agc));
        bpVar.mo17424a((TaskPane.C2075f) new C2931f(crI));
        bpVar.mo17427a((TaskPane.C2083k) new C2932g());
        if (bpVar.mo17419LW()) {
            bpVar.mo17423a((TaskPane.C2074e) new C2933h(crI));
            bpVar.mo17426a((TaskPane.C2082j) new C2934i());
        }
        bpVar.mo17425a((TaskPane.C2080h) new C2935j(crI));
        bpVar.mo17422a((TaskPane.C2073d) new C2937l(crI));
        bpVar.mo17420a((TaskPane.C2070a) new C2936k(agc));
        if (!agc.isEnabled()) {
            bpVar.setVisible(false);
        }
    }

    /* renamed from: a */
    public void mo20417a(TaskPane bpVar, boolean z) {
        this.ahE.mo23024a(bpVar, z);
    }

    public boolean isVisible() {
        return this.aTK.isVisible();
    }

    public void setVisible(boolean z) {
        if (z) {
            for (TaskPane bpVar : this.ahE.buw()) {
                if (bpVar instanceof TaskPane) {
                    bpVar.setVisible(true);
                }
            }
        }
        this.aTK.setVisible(z);
    }

    /* renamed from: Wt */
    private synchronized int m35318Wt() {
        int i;
        int i2;
        TaskPane[] buw = this.ahE.buw();
        int length = buw.length;
        int i3 = 0;
        i = 0;
        while (i3 < length) {
            TaskPane bpVar = buw[i3];
            if (!bpVar.isVisible() || !(bpVar instanceof TaskPane) || !bpVar.isPinned()) {
                i2 = i;
            } else {
                i2 = bpVar.getHeight() + i;
            }
            i3++;
            i = i2;
        }
        return i;
    }

    /* access modifiers changed from: private */
    /* renamed from: LW */
    public synchronized boolean m35315LW() {
        return m35318Wt() < this.aTO;
    }

    /* renamed from: a.ls$e */
    /* compiled from: a */
    class C2930e extends MouseAdapter {
        C2930e() {
        }

        public void mouseEntered(MouseEvent mouseEvent) {
        }
    }

    /* renamed from: a.ls$a */
    class C2926a extends MouseAdapter {
        private int axm = -1;
        private int axn = 0;
        private int screenWidth = C2925ls.f8587kj.bHv().getScreenWidth();

        C2926a() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            this.axm = mouseEvent.getXOnScreen();
            this.axn = C2925ls.this.aTK.getWidth();
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            this.axm = -1;
            C2925ls.this.aTK.setPreferredSize(C2925ls.this.aTK.getSize());
            GUIPrefAddon.m44895b(C2925ls.this.aTJ, 4, String.valueOf(C2925ls.this.aTK.getWidth()));
        }

        public void mouseDragged(MouseEvent mouseEvent) {
            if (this.axm >= 0) {
                if (C2925ls.this.aTI) {
                    int xOnScreen = (this.axn + mouseEvent.getXOnScreen()) - this.axm;
                    if (C2925ls.this.m35326dL(xOnScreen)) {
                        C2925ls.this.aTK.setSize(xOnScreen, C2925ls.this.aTK.getHeight());
                    } else {
                        return;
                    }
                } else {
                    int xOnScreen2 = (this.axn + this.axm) - mouseEvent.getXOnScreen();
                    if (C2925ls.this.m35326dL(xOnScreen2)) {
                        C2925ls.this.aTK.setBounds(this.screenWidth - xOnScreen2, C2925ls.this.aTK.getY(), xOnScreen2, C2925ls.this.aTK.getHeight());
                    } else {
                        return;
                    }
                }
                C2925ls.this.aTK.validate();
            }
        }
    }

    /* renamed from: a.ls$b */
    /* compiled from: a */
    class C2927b implements C0454GJ {
        private final /* synthetic */ boolean axp;

        C2927b(boolean z) {
            this.axp = z;
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            for (TaskPane bpVar : C2925ls.this.ahE.buw()) {
                if (bpVar instanceof TaskPane) {
                    bpVar.mo17445m(this.axp);
                }
            }
        }
    }

    /* renamed from: a.ls$c */
    /* compiled from: a */
    class C2928c extends ComponentAdapter {
        private final /* synthetic */ aGC axN;

        C2928c(aGC agc) {
            this.axN = agc;
        }

        public void componentShown(ComponentEvent componentEvent) {
            this.axN.setVisible(true);
        }

        public void componentHidden(ComponentEvent componentEvent) {
            this.axN.setVisible(false);
        }
    }

    /* renamed from: a.ls$d */
    /* compiled from: a */
    class C2929d implements TaskPane.C2072c {
        private final /* synthetic */ aGC axN;

        C2929d(aGC agc) {
            this.axN = agc;
        }

        /* renamed from: av */
        public void mo17454av(boolean z) {
            this.axN.mo8924m(z);
        }
    }

    /* renamed from: a.ls$f */
    /* compiled from: a */
    class C2931f implements TaskPane.C2075f {
        private final /* synthetic */ String axQ;

        C2931f(String str) {
            this.axQ = str;
        }

        /* renamed from: l */
        public void mo17457l(int i, int i2) {
            GUIPrefAddon.m44895b(this.axQ, 8, String.valueOf(i2));
        }
    }

    /* renamed from: a.ls$g */
    /* compiled from: a */
    class C2932g implements TaskPane.C2083k {
        C2932g() {
        }

        /* renamed from: m */
        public boolean mo17465m(int i, int i2) {
            if (i < i2) {
                return C2925ls.this.m35315LW();
            }
            return true;
        }
    }

    /* renamed from: a.ls$h */
    /* compiled from: a */
    class C2933h implements TaskPane.C2074e {
        private final /* synthetic */ String axQ;

        C2933h(String str) {
            this.axQ = str;
        }

        /* renamed from: aw */
        public void mo17456aw(boolean z) {
            String str;
            String str2 = this.axQ;
            if (z) {
                str = "1";
            } else {
                str = "0";
            }
            GUIPrefAddon.m44895b(str2, 16, str);
        }
    }

    /* renamed from: a.ls$i */
    /* compiled from: a */
    class C2934i implements TaskPane.C2082j {
        C2934i() {
        }

        /* renamed from: LW */
        public boolean mo17464LW() {
            return C2925ls.this.m35315LW();
        }
    }

    /* renamed from: a.ls$j */
    /* compiled from: a */
    class C2935j implements TaskPane.C2080h {
        private final /* synthetic */ String axQ;

        C2935j(String str) {
            this.axQ = str;
        }

        public void collapse(boolean z) {
            String str;
            String str2 = this.axQ;
            if (z) {
                str = "1";
            } else {
                str = "0";
            }
            GUIPrefAddon.m44895b(str2, 32, str);
        }
    }

    /* renamed from: a.ls$l */
    /* compiled from: a */
    class C2937l implements TaskPane.C2073d {
        private final /* synthetic */ String axQ;

        C2937l(String str) {
            this.axQ = str;
        }

        /* renamed from: cX */
        public void mo17455cX(int i) {
            GUIPrefAddon.m44895b(this.axQ, 64, String.valueOf(i));
            GUIPrefAddon.m44895b(String.valueOf(C2925ls.this.aTJ) + "." + "divisor", 64, String.valueOf(C2925ls.this.ahE.buu()));
        }
    }

    /* renamed from: a.ls$k */
    /* compiled from: a */
    class C2936k implements TaskPane.C2070a {
        private final /* synthetic */ aGC axN;

        C2936k(aGC agc) {
            this.axN = agc;
        }

        public boolean isEnabled() {
            return this.axN.isEnabled();
        }
    }
}
