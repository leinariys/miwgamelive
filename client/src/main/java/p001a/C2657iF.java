package p001a;

import org.mozilla1.classfile.C0147Bi;
import taikodom.render.graphics2d.C0559Hm;
import taikodom.render.loader.provider.C0399FY;
import taikodom.render.loader.provider.FilePath;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.iF */
/* compiled from: a */
public class C2657iF implements FilePath {

    /* renamed from: YO */
    static File f8117YO = null;
    /* renamed from: YM */
    private final C3549sU f8123YM;
    private final int hash;
    /* renamed from: Ne */
    private long f8118Ne;
    /* renamed from: YI */
    private boolean f8119YI;
    /* renamed from: YJ */
    private C2658a f8120YJ = null;
    /* renamed from: YK */
    private C2657iF f8121YK = null;
    /* renamed from: YL */
    private Map<String, C2657iF> f8122YL = new HashMap();
    /* renamed from: YN */
    private boolean f8124YN = false;
    private int length;
    private String name;
    private long offset;

    public C2657iF(C3549sU sUVar) {
        this.f8123YM = sUVar;
        this.f8120YJ = C2658a.Dir;
        this.name = ".";
        touch();
        this.hash = 0;
    }

    public C2657iF(C3549sU sUVar, FilePath ain, String str) {
        this.f8123YM = sUVar;
        this.f8121YK = (C2657iF) ain;
        this.name = str;
        this.hash = getPath().hashCode();
    }

    /* renamed from: a */
    static C2657iF m33059a(C3549sU sUVar, DataInput dataInput) throws IOException {
        byte readByte = dataInput.readByte();
        C2657iF iFVar = new C2657iF(sUVar);
        iFVar.m33060a((int) readByte, dataInput);
        return iFVar;
    }

    public boolean exists() {
        if (!this.f8119YI) {
            this.f8119YI = this.f8123YM.mo21891b(this);
            if (this.f8119YI && this.f8121YK != null) {
                this.f8121YK.m33058BA();
            }
        }
        return this.f8119YI;
    }

    /* renamed from: BA */
    private void m33058BA() {
        this.f8119YI = true;
        this.f8120YJ = C2658a.Dir;
        if (this.f8121YK != null) {
            this.f8121YK.m33058BA();
        }
    }

    /* renamed from: aC */
    public FilePath concat(String str) {
        int indexOf = str.indexOf(47);
        if (indexOf == -1) {
            return m33063aD(str);
        }
        return m33063aD(str.substring(0, indexOf)).concat(str.substring(indexOf + 1));
    }

    /* renamed from: aD */
    private C2657iF m33063aD(String str) {
        C2657iF iFVar = this.f8122YL.get(str);
        if (iFVar != null) {
            return iFVar;
        }
        C2657iF iFVar2 = new C2657iF(this.f8123YM, this, str);
        this.f8122YL.put(str, iFVar2);
        return iFVar2;
    }

    /* renamed from: BB */
    public FilePath mo2249BB() {
        return this.f8121YK;
    }

    /* renamed from: BC */
    public FilePath[] mo2250BC() {
        FilePath[] ainArr = new FilePath[this.f8122YL.size()];
        this.f8122YL.values().toArray(ainArr);
        return ainArr;
    }

    /* renamed from: a */
    public FilePath[] mo2258a(C0399FY fy) {
        ArrayList arrayList = new ArrayList();
        for (FilePath next : this.f8122YL.values()) {
            if (fy.mo2163a(this, next.getName())) {
                arrayList.add(next);
            }
        }
        return (FilePath[]) arrayList.toArray(new FilePath[arrayList.size()]);
    }

    /* renamed from: BD */
    public void mo2251BD() {
        if (this.f8121YK != null) {
            if (!this.f8121YK.exists()) {
                this.f8121YK.mo2251BD();
            }
            touch();
            this.f8120YJ = C2658a.Dir;
        }
    }

    private void touch() {
        this.f8118Ne = System.currentTimeMillis();
        this.f8119YI = true;
    }

    public String getName() {
        return this.name;
    }

    public InputStream openInputStream() throws FileNotFoundException {
        if (!exists()) {
            throw new FileNotFoundException(getName());
        }
        this.f8120YJ = C2658a.File;
        return this.f8123YM.mo21892c(this);
    }

    /* renamed from: BE */
    public PrintWriter mo2252BE() {
        return new PrintWriter(openOutputStream());
    }

    public boolean isDir() {
        return this.f8120YJ == C2658a.Dir;
    }

    public OutputStream openOutputStream() {
        touch();
        this.f8120YJ = C2658a.File;
        return this.f8123YM.mo21893e(this);
    }

    public boolean delete() {
        if (!this.f8119YI) {
            return false;
        }
        this.f8119YI = false;
        this.f8123YM.mo21894f(this);
        if (this.f8121YK != null) {
            return this.f8121YK.m33062a(this);
        }
        return true;
    }

    /* renamed from: a */
    private boolean m33062a(C2657iF iFVar) {
        return this.f8122YL.remove(iFVar.name) != null;
    }

    public String getPath() {
        String str = this.name;
        for (C2657iF iFVar = this.f8121YK; iFVar != null; iFVar = iFVar.f8121YK) {
            str = String.valueOf(iFVar.name) + C0147Bi.SEPARATOR + str;
        }
        return str;
    }

    public int hashCode() {
        return this.hash;
    }

    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == C2657iF.class && this.hash == ((C2657iF) obj).hash) {
            return true;
        }
        return false;
    }

    /* renamed from: d */
    public boolean mo2261d(FilePath ain) {
        if (!this.f8119YI) {
            return false;
        }
        C2657iF iFVar = (C2657iF) ain;
        iFVar.touch();
        iFVar.offset = this.offset;
        iFVar.length = this.length;
        iFVar.f8124YN = this.f8124YN;
        iFVar.f8122YL = this.f8122YL;
        return true;
    }

    /* renamed from: BF */
    public FilePath mo2253BF() {
        return this;
    }

    public long lastModified() {
        if (this.f8119YI || this.f8124YN) {
            return this.f8118Ne;
        }
        return this.f8123YM.mo21896h(this);
    }

    /* renamed from: BG */
    public File getFile() {
        if (f8117YO == null) {
            try {
                File createTempFile = File.createTempFile("memoryFile", "dir");
                f8117YO = createTempFile.getParentFile();
                createTempFile.delete();
                f8117YO = new File(f8117YO, createTempFile.getName());
                f8117YO.mkdirs();
                f8117YO.deleteOnExit();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        File file = new File(f8117YO, getName());
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            C0559Hm.copyStream(openInputStream(), fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            file.deleteOnExit();
            System.out.println("MemorFile: creating temp file: " + file.getAbsolutePath());
            return file;
        } catch (IOException e2) {
            throw new RuntimeException(e2);
        }
    }

    /* renamed from: BH */
    public byte[] mo2255BH() {
        try {
            return C0559Hm.m5261e(openInputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: BI */
    public ByteBuffer mo2256BI() {
        return ByteBuffer.wrap(mo2255BH());
    }

    public long length() {
        if (this.f8119YI || this.f8124YN) {
            return (long) this.length;
        }
        return this.f8123YM.mo21895g(this);
    }

    public long getOffset() {
        return this.offset;
    }

    public void setOffset(long j) {
        this.offset = j;
    }

    public int getLength() {
        return this.length;
    }

    public void setLength(int i) {
        this.length = i;
    }

    public boolean isCached() {
        return this.f8124YN;
    }

    /* renamed from: Z */
    public void mo19453Z(boolean z) {
        this.f8124YN = z;
    }

    /* renamed from: a */
    private void m33060a(int i, DataInput dataInput) throws IOException {
        this.f8120YJ = C2658a.values()[dataInput.readByte()];
        this.f8118Ne = dataInput.readLong();
        this.offset = dataInput.readLong();
        this.length = dataInput.readInt();
        while (dataInput.readBoolean()) {
            C2657iF iFVar = new C2657iF(this.f8123YM, this, dataInput.readUTF());
            iFVar.m33060a(i, dataInput);
            iFVar.f8119YI = true;
            iFVar.f8124YN = true;
            this.f8122YL.put(iFVar.name, iFVar);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo19454a(DataOutput dataOutput) throws IOException {
        dataOutput.writeByte(1);
        m33061a(1, dataOutput);
    }

    /* renamed from: a */
    private void m33061a(int i, DataOutput dataOutput) throws IOException {
        dataOutput.writeByte(this.f8120YJ.ordinal());
        dataOutput.writeLong(this.f8118Ne);
        dataOutput.writeLong(this.offset);
        dataOutput.writeInt(this.length);
        for (Map.Entry<String, C2657iF> value : this.f8122YL.entrySet()) {
            C2657iF iFVar = (C2657iF) value.getValue();
            if (iFVar.f8119YI && (iFVar.f8124YN || iFVar.f8120YJ != C2658a.File)) {
                dataOutput.writeBoolean(true);
                dataOutput.writeUTF(iFVar.name);
                iFVar.m33061a(i, dataOutput);
            }
        }
        dataOutput.writeBoolean(false);
    }

    /* renamed from: d */
    public void mo2260d(byte[] bArr) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a.iF$a */
    enum C2658a {
        File,
        Dir
    }
}
