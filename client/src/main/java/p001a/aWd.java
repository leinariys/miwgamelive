package p001a;

import gnu.trove.THashMap;
import gnu.trove.TObjectIntHashMap;
import logic.res.html.MessageContainer;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.ObjectStreamClass;
import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: a.aWd */
/* compiled from: a */
public class aWd implements C6656arE {
    static Log logger = LogPrinter.m10275K(aWd.class);
    private final ReentrantReadWriteLock hIb = new ReentrantReadWriteLock();
    private final TObjectIntHashMap<Class<?>> jdF = new TObjectIntHashMap<>(4096);
    private final C3852vn<Class<?>> jdG = new C3852vn<>();
    private final C3852vn<ObjectStreamClass> jdH = new C3852vn<>();
    private final Set<Class<?>> jdI = MessageContainer.dgI();
    private final THashMap<String, String> jdJ = new THashMap<>();
    private final THashMap<String, String> jdK = new THashMap<>();
    private final C3852vn<String> jdL = new C3852vn<>();
    private final HashMap<String, Integer> jdM = new HashMap<>(4096);
    private final AtomicInteger nextId = new AtomicInteger(0);
    private int jdN;

    /* renamed from: jM */
    public void mo12016jM(String str) {
        String intern = str.intern();
        if (!this.jdM.containsKey(intern)) {
            int incrementAndGet = this.nextId.incrementAndGet();
            this.jdL.put(incrementAndGet, intern);
            this.jdM.put(intern, Integer.valueOf(incrementAndGet));
            if (incrementAndGet > this.jdN) {
                this.jdN = incrementAndGet;
            }
        }
    }

    /* renamed from: at */
    public void mo12009at(Class<?> cls) {
        int i;
        if (!this.jdF.containsKey(cls)) {
            ObjectStreamClass lookup = ObjectStreamClass.lookup(cls);
            if (lookup != null) {
                i = this.nextId.incrementAndGet();
            } else {
                i = -1;
            }
            if (i >= 0) {
                this.jdH.put(i, lookup);
                this.jdG.put(i, cls);
                this.jdL.put(i, cls.getName());
            }
            this.jdM.put(cls.getName(), Integer.valueOf(i));
            this.jdF.put(cls, i);
            if (!cls.isArray()) {
                try {
                    mo12009at(Array.newInstance(cls, 0).getClass());
                } catch (Exception e) {
                }
            }
            if (i > this.jdN) {
                this.jdN = i;
            }
        }
    }

    /* renamed from: D */
    public boolean mo12006D(String str, int i) {
        int i2;
        boolean z = false;
        this.hIb.readLock().unlock();
        this.hIb.writeLock().lock();
        try {
            Class<?> cls = Class.forName(str);
            if (!this.jdF.containsKey(cls)) {
                ObjectStreamClass lookup = ObjectStreamClass.lookup(cls);
                if (lookup == null) {
                    i2 = -1;
                } else {
                    i2 = i;
                }
                if (i2 >= 0) {
                    this.jdH.put(i2, lookup);
                    this.jdG.put(i2, cls);
                }
                this.jdL.put(i, null);
                this.jdM.remove(str);
                this.jdF.put(cls, i2);
                if (i2 > this.jdN) {
                    this.jdN = i2;
                }
                i = i2;
            }
            if (i != -1) {
                z = true;
            }
        } catch (ClassNotFoundException e) {
            this.jdL.put(i, null);
            this.jdM.remove(str);
        } finally {
            this.hIb.readLock().lock();
            this.hIb.writeLock().unlock();
        }
        return z;
    }

    /* renamed from: aT */
    private boolean m19238aT(Class<?> cls) {
        String name = cls.getName();
        Integer num = this.jdM.get(name);
        if (num == null || num.intValue() <= 0) {
            return false;
        }
        return mo12006D(name, num.intValue());
    }

    /* renamed from: Bh */
    private boolean m19237Bh(int i) {
        String str = this.jdL.get(i);
        if (str != null) {
            return mo12006D(str, i);
        }
        return false;
    }

    /* renamed from: tS */
    public Class<?> mo12017tS(int i) {
        this.hIb.readLock().lock();
        try {
            Class<?> cls = this.jdG.get(i);
            if (cls != null || !m19237Bh(i)) {
                this.hIb.readLock().unlock();
            } else {
                cls = this.jdG.get(i);
            }
            return cls;
        } finally {
            this.hIb.readLock().unlock();
        }
    }

    public int getMagicNumber(Class<?> cls) {
        int i;
        Integer num;
        if (cls != null && (num = this.jdM.get(cls.getName())) != null && num.intValue() > 0) {
            return num.intValue();
        }
        this.hIb.readLock().lock();
        try {
            int i2 = this.jdF.get(cls);
            if (i2 > 0) {
                this.hIb.readLock().unlock();
                return i2;
            } else if (m19238aT(cls) && (i = this.jdF.get(cls)) > 0) {
                return i;
            } else {
                if (!this.jdI.contains(cls)) {
                    logger.warn("Consider precaching: " + cls);
                    this.jdI.add(cls);
                }
                this.hIb.readLock().unlock();
                return -1;
            }
        } finally {
            this.hIb.readLock().unlock();
        }
    }

    public ObjectStreamClass getObjectStreamClassFromMagicNumber(int i) {
        this.hIb.readLock().lock();
        try {
            ObjectStreamClass objectStreamClass = this.jdH.get(i);
            if (objectStreamClass == null && m19237Bh(i)) {
                objectStreamClass = this.jdH.get(i);
            }
            return objectStreamClass;
        } finally {
            this.hIb.readLock().unlock();
        }
    }

    public int dDt() {
        return this.jdN;
    }

    /* renamed from: a */
    public final void mo12008a(DataOutput dataOutput, int i) {
        if (i < 0) {
            if (i == -1) {
                dataOutput.writeByte(255);
            } else if (i == -2) {
                dataOutput.writeByte(254);
            } else {
                throw new IllegalArgumentException();
            }
        } else if (i > this.jdN) {
            throw new IllegalArgumentException();
        } else if (this.jdN < 254) {
            dataOutput.writeByte(i);
        } else if (this.jdN < 65024) {
            dataOutput.writeByte(i >> 8);
            dataOutput.writeByte(i & 255);
        } else {
            dataOutput.writeByte(i >> 16);
            dataOutput.writeShort(i);
        }
    }

    /* renamed from: a */
    public final int mo12007a(DataInput dataInput) {
        int readUnsignedByte = dataInput.readUnsignedByte();
        if (readUnsignedByte == 255) {
            return -1;
        }
        if (readUnsignedByte == 254) {
            return -2;
        }
        if (this.jdN >= 254) {
            if (this.jdN < 65024) {
                readUnsignedByte = (readUnsignedByte << 8) | dataInput.readUnsignedByte();
            } else {
                readUnsignedByte = (readUnsignedByte << 16) | dataInput.readUnsignedShort();
            }
        }
        if (readUnsignedByte <= this.jdN) {
            return readUnsignedByte;
        }
        throw new IllegalArgumentException("Invalid magic " + readUnsignedByte + " > " + this.jdN);
    }

    /* renamed from: jL */
    public final String mo12015jL(String str) {
        String str2 = (String) this.jdJ.get(str);
        return str2 == null ? str : str2;
    }

    /* renamed from: jK */
    public final String mo12014jK(String str) {
        String str2 = (String) this.jdK.get(str);
        return str2 == null ? str : str2;
    }

    /* renamed from: B */
    public final void mo12005B(Map<String, String> map) {
        this.jdJ.clear();
        this.jdJ.putAll(map);
        this.jdK.clear();
        for (Map.Entry next : map.entrySet()) {
            this.jdK.put((String) next.getValue(), (String) next.getKey());
        }
    }

    public Collection<Class<?>> getClasses() {
        HashSet hashSet = new HashSet();
        for (int i = 0; i < this.jdN; i++) {
            Class<?> tS = mo12017tS(i);
            if (tS != null) {
                hashSet.add(tS);
            }
        }
        return hashSet;
    }
}
