package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import logic.aaa.C2235dB;
import logic.thred.C6615aqP;

import java.util.List;

/* renamed from: a.Tk */
/* compiled from: a */
public interface C1355Tk extends C6083agD {
    public static final int ehN = 0;
    public static final int ehO = 1;
    public static final int ehP = 2;
    public static final int ehQ = 3;
    public static final int ehR = 8;
    public static final int ehS = 2;
    public static final int ehT = 1;
    public static final int ehU = 4;
    public static final int ehV = 11;
    public static final int ehW = 12;
    public static final int ehX = 10;
    public static final int ehY = 13;
    public static final int ehZ = -1;
    public static final int eia = 0;

    /* renamed from: IO */
    long mo5724IO();

    /* renamed from: a */
    aNW mo5725a(long j, C2235dB dBVar, int i);

    /* renamed from: a */
    void mo5726a(C1037PD pd);

    /* renamed from: a */
    void mo5727a(Vec3d ajr, float f, C1356a aVar);

    /* renamed from: a */
    void mo5728a(Vec3d ajr, Vec3f vec3f, float f, C1356a aVar, List<C6615aqP> list);

    /* renamed from: a */
    void mo5729a(CurrentTimeMilli ahw);

    /* renamed from: bZ */
    void mo5730bZ(long j);

    long bvf();

    C1037PD bvg();

    CurrentTimeMilli bvh();

    C3055nX<C0819Lq, C0461GN> bvi();

    long bvj();

    /* renamed from: c */
    int mo5736c(C6615aqP aqp, C6615aqP aqp2);

    void dispose();

    /* renamed from: fI */
    void mo5738fI(long j);

    /* renamed from: gZ */
    void mo5739gZ();

    /* renamed from: a.Tk$a */
    public interface C1356a {
        /* renamed from: c */
        boolean mo1934c(C6615aqP aqp);
    }
}
