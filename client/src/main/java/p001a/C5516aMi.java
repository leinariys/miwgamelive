package p001a;

import taikodom.infra.script.Measure;

import java.util.SortedMap;

/* renamed from: a.aMi  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5516aMi {
    float getCPU_usage();

    float getDataReq_perSecond();

    int getPendingRequestsCount();

    long getPullCount();

    float getPulls_perSecond();

    int getScriptObjectCount();

    long getTrans_averageTime();

    long getTrans_maxTime();

    long getTrans_minTime();

    float getTrans_perSecond();

    float getTrans_rerunPerSecond();

    double getTrans_stdDeviation();

    SortedMap<String, Measure> getTransactionalMethods();

    String getTransactionalReport();

    String getUpTime();

    long getUpTimeInMillis();

    float getWriteLocks_perSecond();
}
