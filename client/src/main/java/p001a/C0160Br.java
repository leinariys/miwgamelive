package p001a;

import game.network.message.externalizable.C5344aFs;
import logic.res.KeyCode;
import logic.swing.C0454GJ;
import logic.swing.C3940wz;
import logic.swing.aDX;
import logic.ui.C2698il;
import logic.ui.ComponentManager;
import logic.ui.item.Repeater;
import taikodom.addon.IAddonProperties;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.*;

/* renamed from: a.Br */
/* compiled from: a */
public class C0160Br {
    private static final String clH = "rep-button";
    private static final String clI = "button-caption";
    /* access modifiers changed from: private */
    private final boolean clM;
    /* renamed from: SZ */
    public aDX f258SZ;
    /* access modifiers changed from: private */
    public Map<AbstractButton, Action> clL = new HashMap();
    private C2698il aoL;
    private Repeater.C3671a<C5344aFs> buV;
    private Repeater<C5344aFs> clJ;
    private List<C5344aFs> clK;
    private C0454GJ clN;
    private boolean dirty;

    /* renamed from: kj */
    private IAddonProperties f259kj;

    public C0160Br(IAddonProperties vWVar, boolean z) {
        this.f259kj = vWVar;
        this.clM = z;
        this.aoL = (C2698il) this.f259kj.bHv().mo16794bQ("leftmenu.xml");
        this.buV = new C0161a();
        this.clK = new ArrayList();
        this.clJ = this.aoL.mo4919ch(clH);
        this.clJ.mo22250a(this.buV);
        this.clN = new C0164c();
        ComponentManager.getCssHolder(this.aoL).setAttribute("class", z ? "left" : "right");
        ayK();
        if (!this.clM) {
            this.aoL.setLocation((this.f259kj.bHv().getScreenWidth() - this.aoL.getWidth()) - 300, this.aoL.getLocation().y);
        } else {
            this.aoL.setLocation(KeyCode.cua, this.aoL.getLocation().y);
        }
    }

    /* renamed from: a */
    public void mo838a(Action action, int i) {
        this.clK.add(new C5344aFs(action, i, this.clM));
        ayI();
    }

    /* renamed from: a */
    public void mo837a(Action action) {
        Iterator<C5344aFs> it = this.clK.iterator();
        while (true) {
            if (it.hasNext()) {
                if (it.next().getAction() == action) {
                    it.remove();
                    break;
                }
            } else {
                break;
            }
        }
        ayI();
    }

    private void ayI() {
        Collections.sort(this.clK, new C0163b());
        this.clL.clear();
        this.clJ.clear();
        for (C5344aFs G : this.clK) {
            this.clJ.mo22248G(G);
        }
        ayJ();
    }

    public void ayJ() {
        if (this.f259kj.ala() == null || !this.aoL.isVisible()) {
            this.dirty = true;
            return;
        }
        for (Map.Entry next : this.clL.entrySet()) {
            AbstractButton abstractButton = (AbstractButton) next.getKey();
            Action action = (Action) next.getValue();
            abstractButton.setSelected(Boolean.TRUE.equals(action.getValue("isActive")));
            abstractButton.setVisible(action.isEnabled());
            abstractButton.setFocusable(false);
        }
        ayK();
        this.dirty = false;
    }

    private void ayK() {
        Dimension preferredSize = this.aoL.getPreferredSize();
        this.aoL.setSize(preferredSize);
        this.aoL.setLocation(this.aoL.getLocation().x, (this.f259kj.bHv().getScreenHeight() / 3) - (preferredSize.height / 2));
        this.aoL.validate();
    }

    public void setVisible(boolean z) {
        this.aoL.getWidth();
        this.f259kj.bHv().getScreenWidth();
        int i = this.aoL.getLocation().x;
        if (z && this.dirty) {
            ayJ();
        }
        this.aoL.setVisible(z);
    }

    public void destroy() {
        this.aoL.destroy();
        this.aoL = null;
    }

    /* access modifiers changed from: private */
    /* renamed from: iL */
    public boolean m1339iL() {
        return this.f259kj != null && this.f259kj.ala().aLS().mo22273iL();
    }

    /* renamed from: a.Br$a */
    class C0161a implements Repeater.C3671a<C5344aFs> {
        C0161a() {
        }

        /* renamed from: a */
        public void mo843a(C5344aFs afs, Component component) {
            if (component instanceof AbstractButton) {
                AbstractButton abstractButton = (AbstractButton) component;
                Action action = afs.getAction();
                abstractButton.addActionListener(new C0162a(action));
                if (component.getName().equals(C0160Br.clI)) {
                    abstractButton.setText((String) afs.getAction().getValue("ShortDescription"));
                    if (!C0160Br.this.m1339iL()) {
                        C6622aqW action2 = afs.getAction();
                        abstractButton.putClientProperty("command", action2);
                        abstractButton.setToolTipText(action2.crH());
                        C3940wz.m40777a((JComponent) abstractButton, "windowTooltipProvider");
                    }
                    C0160Br.this.clL.put(abstractButton, action);
                }
            }
        }

        /* renamed from: a.Br$a$a */
        class C0162a implements ActionListener {
            private final /* synthetic */ Action dCF;

            C0162a(Action action) {
                this.dCF = action;
            }

            public void actionPerformed(ActionEvent actionEvent) {
                this.dCF.actionPerformed(actionEvent);
            }
        }
    }

    /* renamed from: a.Br$c */
    /* compiled from: a */
    class C0164c implements C0454GJ {
        C0164c() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            C0160Br.this.f258SZ = null;
        }
    }

    /* renamed from: a.Br$b */
    /* compiled from: a */
    class C0163b implements Comparator<C5344aFs> {
        C0163b() {
        }

        /* renamed from: a */
        public int compare(C5344aFs afs, C5344aFs afs2) {
            return afs.getOrder() - afs2.getOrder();
        }
    }
}
