package p001a;

import org.objectweb.asm.*;

/* renamed from: a.alL  reason: case insensitive filesystem */
/* compiled from: a */
public class C6351alL implements C0244Cz, ClassVisitor {
    private boolean fXK;
    private boolean fXL;
    private String fXM;
    private String name;

    public void visit(int i, int i2, String str, String str2, String str3, String[] strArr) {
        this.name = str;
    }

    public AnnotationVisitor visitAnnotation(String str, boolean z) {
        if (C2821kb.aqx.getDescriptor().equals(str)) {
            this.fXK = true;
            return null;
        } else if (!C2821kb.aqt.getDescriptor().equals(str)) {
            return null;
        } else {
            this.fXL = true;
            return null;
        }
    }

    public void visitAttribute(Attribute attribute) {
    }

    public void visitEnd() {
    }

    public FieldVisitor visitField(int i, String str, String str2, String str3, Object obj) {
        return null;
    }

    public void visitInnerClass(String str, String str2, String str3, int i) {
    }

    public MethodVisitor visitMethod(int i, String str, String str2, String str3, String[] strArr) {
        return null;
    }

    public void visitOuterClass(String str, String str2, String str3) {
        this.fXM = str;
    }

    public void visitSource(String str, String str2) {
    }

    public boolean aBz() {
        return this.fXK;
    }

    public boolean aBA() {
        return this.fXL;
    }

    public String getName() {
        return this.name;
    }
}
