package p001a;

import game.network.exception.CreatListenerChannelException;
import game.network.message.serializable.C1556Wo;
import gnu.trove.THashMap;
import gnu.trove.THashSet;
import logic.data.mbean.C0677Jd;
import logic.res.code.C5663aRz;
import logic.res.html.C1867ad;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/* renamed from: a.aBM */
/* compiled from: a */
public class aBM<K, V> implements C1556Wo<K, V> {

    public final transient C0677Jd bFL;
    private final transient C1556Wo<K, V> hiC;
    private transient boolean hiA = true;
    private transient Set<K> hiB;
    private transient Map<K, V> hiz;

    public aBM(C0677Jd jd, C1556Wo<K, V> wo) {
        this.bFL = jd;
        this.hiC = wo;
    }

    /* renamed from: ym */
    public C5663aRz mo5521ym() {
        return this.hiC.mo5521ym();
    }

    /* renamed from: XC */
    public C1867ad<V> mo5505XC() {
        throw new CreatListenerChannelException();
    }

    /* renamed from: c */
    public void mo5508c(C5663aRz arz) {
        throw new IllegalAccessError();
    }

    public void clear() {
        this.bFL.mo3223v(this.hiC.mo5521ym());
        if (this.hiz == null) {
            this.hiz = new THashMap();
        } else {
            this.hiz.clear();
        }
        if (this.hiB != null) {
            this.hiB.clear();
            this.hiB = null;
        }
        this.hiA = false;
    }

    public boolean containsKey(Object obj) {
        if (this.hiz != null && this.hiz.containsKey(obj)) {
            return true;
        }
        if (!this.hiA) {
            return false;
        }
        if (this.hiB == null || !this.hiB.contains(obj)) {
            return this.hiC.containsKey(obj);
        }
        return false;
    }

    public boolean containsValue(Object obj) {
        if (this.hiz == null && this.hiB == null) {
            return this.hiC.containsValue(obj);
        }
        if (this.hiz != null && this.hiz.containsValue(obj)) {
            return true;
        }
        if (this.hiA) {
            for (Map.Entry entry : this.hiC.entrySet()) {
                if (entry.getValue().equals(obj) && ((this.hiz == null || !this.hiz.containsKey(entry.getKey())) && (this.hiB == null || !this.hiB.contains(entry.getKey())))) {
                    return true;
                }
            }
        }
        return false;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        if (this.hiz == null && this.hiB == null) {
            return this.hiC.entrySet();
        }
        if (!this.hiA) {
            return this.hiz.entrySet();
        }
        Set<Map.Entry<K, V>> tHashSet = new THashSet<>();
        for (Map.Entry entry : this.hiC.entrySet()) {
            if ((this.hiz == null || !this.hiz.containsKey(entry.getKey())) && (this.hiB == null || !this.hiB.contains(entry.getKey()))) {
                tHashSet.add(new C1768a(entry.getKey()));
            }
        }
        if (this.hiz != null) {
            for (Map.Entry key : this.hiC.entrySet()) {
                tHashSet.add(new C1768a(key.getKey()));
            }
        }
        return tHashSet;
    }

    public V get(Object obj) {
        V v;
        if (this.hiz == null && this.hiB == null) {
            return this.hiC.get(obj);
        }
        if (this.hiz != null && (v = this.hiz.get(obj)) != null) {
            return v;
        }
        if (!this.hiA) {
            return null;
        }
        if (this.hiB == null || !this.hiB.contains(obj)) {
            return this.hiC.get(obj);
        }
        return null;
    }

    public boolean isEmpty() {
        return size() > 0;
    }

    public Set<K> keySet() {
        if (this.hiz == null && this.hiB == null) {
            return this.hiC.keySet();
        }
        if (!this.hiA) {
            return this.hiz.keySet();
        }
        Set<K> tHashSet = new THashSet<>();
        for (Map.Entry entry : this.hiC.entrySet()) {
            if ((this.hiz == null || !this.hiz.containsKey(entry.getKey())) && (this.hiB == null || !this.hiB.contains(entry.getKey()))) {
                tHashSet.add(entry.getKey());
            }
        }
        if (this.hiz != null) {
            for (Map.Entry key : this.hiC.entrySet()) {
                tHashSet.add(key.getKey());
            }
        }
        return tHashSet;
    }

    public V put(K k, V v) {
        this.bFL.mo3223v(this.hiC.mo5521ym());
        if (this.hiz == null) {
            this.hiz = new THashMap();
        }
        V put = this.hiz.put(k, v);
        if (this.hiA) {
            if (put == null) {
                put = this.hiC.get(k);
            }
            if (this.hiB != null) {
                this.hiB.remove(k);
            }
        }
        return put;
    }

    public void putAll(Map<? extends K, ? extends V> map) {
        this.bFL.mo3223v(this.hiC.mo5521ym());
        if (this.hiz == null) {
            this.hiz = new THashMap();
        }
        this.hiz.putAll(map);
        if (this.hiB != null) {
            this.hiB.removeAll(map.keySet());
        }
    }

    public V remove(Object obj) {
        V v;
        V v2 = null;
        this.bFL.mo3223v(this.hiC.mo5521ym());
        if (this.hiz != null) {
            v2 = this.hiz.remove(obj);
        }
        if (!this.hiA || (v = this.hiC.get(obj)) == null) {
            return v2;
        }
        if (this.hiB == null) {
            this.hiB = new THashSet();
        }
        this.hiB.add(obj);
        if (v2 == null) {
            return v;
        }
        return v2;
    }

    public int size() {
        int i = 0;
        if (this.hiz != null) {
            i = 0 + this.hiz.size();
        }
        if (!this.hiA) {
            return i;
        }
        if (this.hiB != null) {
            i -= this.hiB.size();
        }
        return i + this.hiC.size();
    }

    public Collection<V> values() {
        if (this.hiz == null && this.hiB == null) {
            return this.hiC.values();
        }
        if (!this.hiA) {
            return this.hiz.values();
        }
        ArrayList arrayList = new ArrayList();
        for (Map.Entry entry : this.hiC.entrySet()) {
            if ((this.hiz == null || !this.hiz.containsKey(entry.getKey())) && (this.hiB == null || !this.hiB.contains(entry.getKey()))) {
                arrayList.add(entry.getValue());
            }
        }
        if (this.hiz != null) {
            for (Map.Entry value : this.hiC.entrySet()) {
                arrayList.add(value.getValue());
            }
        }
        return arrayList;
    }

    public Object clone() {
        throw new IllegalStateException("DataMap cloning not supported during transactions");
    }

    public Map<K, V> cJU() {
        return this.hiz;
    }

    public Set<K> cJV() {
        return this.hiB;
    }

    public boolean cJW() {
        return this.hiA;
    }

    /* renamed from: a.aBM$a */
    private class C1768a implements Map.Entry<K, V> {
        private K hWZ;

        public C1768a(K k) {
            this.hWZ = k;
        }

        public K getKey() {
            return this.hWZ;
        }

        public V getValue() {
            return aBM.this.get(this.hWZ);
        }

        public V setValue(V v) {
            return aBM.this.put(getKey(), v);
        }
    }
}
