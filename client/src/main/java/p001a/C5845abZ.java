package p001a;

import com.hoplon.geometry.BoundingBox;

/* renamed from: a.abZ  reason: case insensitive filesystem */
/* compiled from: a */
class C5845abZ extends aEH.C1791a<BoundingBox> {
    final /* synthetic */ aEH eZT;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C5845abZ(aEH aeh) {
        super((aEH.C1791a) null);
        this.eZT = aeh;
    }

    /* renamed from: ip */
    public BoundingBox mo8563O(String str) {
        String[] split = str.split("[ \t\r\n]+");
        return new BoundingBox(Float.parseFloat(split[0]), Float.parseFloat(split[1]), Float.parseFloat(split[2]), Float.parseFloat(split[3]), Float.parseFloat(split[4]), Float.parseFloat(split[5]));
    }
}
