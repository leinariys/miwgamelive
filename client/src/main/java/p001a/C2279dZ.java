package p001a;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.IGeometryF;
import game.geometry.Matrix4fWrap;
import game.geometry.Vec3d;
import game.geometry.aLH;
import game.script.ship.Ship;
import logic.res.sound.C0907NJ;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.RLine;
import taikodom.render.scene.RParticleSystem;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3f;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.dZ */
/* compiled from: a */
public class C2279dZ {
    /* access modifiers changed from: private */

    /* renamed from: Bi */
    private final boolean f6545Bi = true;
    /* renamed from: Bd */
    public RParticleSystem f6540Bd;
    /* renamed from: Bf */
    RLine f6542Bf = new RLine();
    /* renamed from: Be */
    private List<SceneObject> f6541Be = new ArrayList();
    /* renamed from: Bg */
    private Vec3f f6543Bg = null;
    /* renamed from: Bh */
    private long f6544Bh = 0;
    /* renamed from: Bj */
    private C3257pi f6546Bj = new C3257pi();

    /* renamed from: a */
    public void mo17863a(Ship fAVar, Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        if (fAVar.cHg() != null) {
            Vec3f vec3f4 = new Vec3f();
            new Vec3f();
            fAVar.cHg().getTransform().orientation.mo13947a((Vector3f) vec3f2, (Vector3f) vec3f4);
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.f6544Bh >= 10) {
                this.f6544Bh = currentTimeMillis;
                if (mo17865kR() != null) {
                    RParticleSystem kR = mo17865kR();
                    Vec3f vec3f5 = new Vec3f((Vector3f) vec3f4);
                    vec3f5.negate();
                    Vec3f b = new Vec3f(0.0f, 1.0f, 0.0f).mo23476b(vec3f5);
                    Vec3f b2 = vec3f5.mo23476b(b);
                    Matrix4fWrap ajk = new Matrix4fWrap();
                    ajk.mo13988aD(b);
                    ajk.mo13989aE(b2);
                    ajk.mo13990aF(vec3f5);
                    this.f6543Bg = new Vec3f((Vector3f) vec3f4);
                    this.f6543Bg.scale((float) (-1.2999999523162842d * mo17862a(vec3f4, fAVar)));
                    ajk.setTranslation(this.f6543Bg);
                    kR.setFollowEmitter(true);
                    kR.setTransform(ajk);
                    kR.setParticlesVelocity(vec3f4.mo23510mS(10.0f).dfR());
                    kR.setParticlesAcceleration(vec3f4.mo23510mS(10.0f).dfR());
                    fAVar.cHg().addChild(kR);
                }
            }
        }
    }

    /* renamed from: a */
    private void m29040a(Vec3f vec3f, Vec3f vec3f2, SceneObject sceneObject) {
        this.f6542Bf.reset();
        this.f6542Bf.addPoint(vec3f2);
        this.f6542Bf.addPoint(vec3f.mo23503i(vec3f2.mo23510mS(-50.0f)));
        this.f6542Bf.setPrimitiveColor(new Color(1.0f, 0.0f, 0.0f, 1.0f));
        if (!sceneObject.hasChild(this.f6542Bf)) {
            sceneObject.addChild(this.f6542Bf);
        }
    }

    /* renamed from: a */
    public double mo17862a(Vec3f vec3f, Ship fAVar) {
        aLH aabb = fAVar.cHg().getAABB();
        Vec3d din = aabb.din();
        Vec3d dim = aabb.dim();
        double[] dArr = {din.mo9504d((Tuple3d) new Vec3d(-100.0d, ScriptRuntime.NaN, ScriptRuntime.NaN)).dfW().mo9494b((IGeometryF) vec3f), din.mo9504d((Tuple3d) new Vec3d(ScriptRuntime.NaN, -100.0d, ScriptRuntime.NaN)).dfW().mo9494b((IGeometryF) vec3f), din.mo9504d((Tuple3d) new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, -100.0d)).dfW().mo9494b((IGeometryF) vec3f), dim.mo9504d((Tuple3d) new Vec3d(100.0d, ScriptRuntime.NaN, ScriptRuntime.NaN)).dfW().mo9494b((IGeometryF) vec3f), dim.mo9504d((Tuple3d) new Vec3d(ScriptRuntime.NaN, 100.0d, ScriptRuntime.NaN)).dfW().mo9494b((IGeometryF) vec3f), dim.mo9504d((Tuple3d) new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, 100.0d)).dfW().mo9494b((IGeometryF) vec3f)};
        double d = dArr[0];
        for (int i = 1; i < dArr.length; i++) {
            if (dArr[i] > d) {
                d = dArr[i];
            }
        }
        if (d == dArr[0] || d == dArr[3]) {
            return (double) (aabb.dio() * 0.5f);
        }
        if (d == dArr[1] || d == dArr[4]) {
            return (double) (aabb.dip() * 0.5f);
        }
        if (d == dArr[2] || d == dArr[5]) {
            return (double) (aabb.diq() * 0.5f);
        }
        return d;
    }

    /* renamed from: kR */
    public RParticleSystem mo17865kR() {
        if (this.f6540Bd != null) {
            return (RParticleSystem) this.f6540Bd.cloneAsset();
        }
        C5916acs.getSingolton().getLoaderTrail().mo4995a("data/gfx/misc/shield_splash.pro", "gfx_shield_splash_wave", (Scene) null, new C2280a(), "shield hit");
        return null;
    }

    public void dispose() {
        if (this.f6540Bd != null) {
            this.f6540Bd.dispose();
            this.f6540Bd = null;
        }
        if (this.f6542Bf != null) {
            this.f6542Bf.dispose();
        }
    }

    /* renamed from: kS */
    public Vec3f mo17866kS() {
        return this.f6543Bg;
    }

    /* renamed from: a.dZ$a */
    class C2280a implements C0907NJ {
        C2280a() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if (renderAsset != null) {
                C2279dZ.this.f6540Bd = (RParticleSystem) renderAsset;
            } else {
                System.err.println("could not load shield hit!");
            }
        }
    }
}
