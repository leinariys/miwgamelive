package p001a;

import game.network.WhoAmI;
import game.network.channel.client.ClientConnect;
import logic.baa.aDJ;
import logic.bbb.aDR;

/* renamed from: a.Xi */
/* compiled from: a */
public class C1619Xi<T> implements C1722ZT<T> {
    private ClientConnect eIr;
    private T eIs;
    private C1722ZT.C1723a eIt;
    private aDR eIu;

    /* renamed from: pX */
    private C0495Gr f2122pX;

    /* renamed from: pY */
    private WhoAmI f2123pY;

    public ClientConnect bFp() {
        return this.eIr;
    }

    /* renamed from: h */
    public void mo6776h(ClientConnect bVar) {
        this.eIr = bVar;
    }

    public T bFq() {
        return this.eIs;
    }

    /* renamed from: Z */
    public void mo6768Z(T t) {
        this.eIs = t;
    }

    public C1722ZT.C1723a bFr() {
        return this.eIt;
    }

    /* renamed from: a */
    public void mo6770a(C1722ZT.C1723a aVar) {
        this.eIt = aVar;
    }

    /* renamed from: m */
    public boolean mo6780m(aDJ adj) {
        if (adj != null && this.f2123pY == WhoAmI.CLIENT) {
            return ((C1875af) adj.bFf()).mo13211a(this.eIr);
        }
        return true;
    }

    /* renamed from: hd */
    public C0495Gr mo6777hd() {
        return this.f2122pX;
    }

    /* renamed from: c */
    public void mo6775c(C0495Gr gr) {
        this.f2122pX = gr;
    }

    /* renamed from: a */
    public void mo6769a(WhoAmI lt) {
        this.f2123pY = lt;
    }

    /* renamed from: he */
    public WhoAmI mo6778he() {
        return this.f2123pY;
    }

    public aDR bFs() {
        return this.eIu;
    }

    /* renamed from: j */
    public void mo6779j(aDR adr) {
        this.eIu = adr;
    }
}
