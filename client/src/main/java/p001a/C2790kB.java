package p001a;

import gnu.trove.TByteArrayList;

/* renamed from: a.kB */
/* compiled from: a */
public class C2790kB {
    private C2790kB() {
    }

    /* renamed from: e */
    public static long m34297e(byte[] bArr) {
        return ((((long) bArr[0]) & 255) << 56) | ((((long) bArr[1]) & 255) << 48) | ((((long) bArr[2]) & 255) << 40) | ((((long) bArr[3]) & 255) << 32) | ((((long) bArr[4]) & 255) << 24) | ((((long) bArr[5]) & 255) << 16) | ((((long) bArr[6]) & 255) << 8) | (((long) bArr[7]) & 255);
    }

    /* renamed from: ce */
    public static byte[] m34296ce(long j) {
        return new byte[]{(byte) ((int) (j >> 56)), (byte) ((int) (j >> 48)), (byte) ((int) (j >> 40)), (byte) ((int) (j >> 32)), (byte) ((int) (j >> 24)), (byte) ((int) (j >> 16)), (byte) ((int) (j >> 8)), (byte) ((int) j)};
    }

    /* renamed from: f */
    public static long[] m34298f(byte[] bArr) {
        long[] jArr = new long[(bArr.length / 8)];
        byte[] bArr2 = new byte[8];
        int i = -1;
        for (int i2 = 0; i2 < bArr.length; i2 += 8) {
            bArr2[0] = bArr[i2];
            bArr2[1] = bArr[i2 + 1];
            bArr2[2] = bArr[i2 + 2];
            bArr2[3] = bArr[i2 + 3];
            bArr2[4] = bArr[i2 + 4];
            bArr2[5] = bArr[i2 + 5];
            bArr2[6] = bArr[i2 + 6];
            bArr2[7] = bArr[i2 + 7];
            i++;
            jArr[i] = m34297e(bArr2);
        }
        return jArr;
    }

    /* renamed from: a */
    public static byte[] m34295a(long[] jArr) {
        TByteArrayList tByteArrayList = new TByteArrayList(jArr.length * 8);
        for (long ce : jArr) {
            tByteArrayList.add(m34296ce(ce));
        }
        return tByteArrayList.toNativeArray();
    }
}
