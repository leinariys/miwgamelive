package p001a;

/* renamed from: a.aHI */
/* compiled from: a */
public class aHI {
    /* renamed from: mH */
    public static boolean m15140mH(float f) {
        int round = Math.round(f);
        return (round > 120 && round % 60 == 0) || (round > 60 && round <= 120 && round % 30 == 0) || ((round > 30 && round <= 60 && round % 10 == 0) || (round > 0 && round <= 30 && round % 5 == 0));
    }
}
