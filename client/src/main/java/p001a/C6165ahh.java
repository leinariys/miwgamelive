package p001a;

import game.script.pda.DatabaseCategory;
import logic.baa.C4033yO;
import logic.ui.Panel;
import logic.ui.item.aUR;
import taikodom.addon.pda.DataBase;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.ahh  reason: case insensitive filesystem */
/* compiled from: a */
class C6165ahh extends C0037AU {
    final /* synthetic */ DataBase cDE;

    public C6165ahh(DataBase dataBase) {
        this.cDE = dataBase;
    }

    /* renamed from: a */
    public Component mo307a(aUR aur, Object obj, boolean z, boolean z2, boolean z3, int i, boolean z4) {
        String str;
        String str2;
        Panel nR;
        JLabel cf;
        Object userObject = ((C6964axd) obj).getUserObject();
        if (userObject instanceof DatabaseCategory) {
            str = ((DatabaseCategory) userObject).mo195rP().get();
        } else if (userObject instanceof C4033yO) {
            C4033yO yOVar = (C4033yO) userObject;
            if (!this.cDE.f10208P.dyl().dBQ().get(yOVar).booleanValue() || yOVar == this.cDE.giD) {
                str2 = "";
            } else {
                str2 = "(" + this.cDE.f10209kj.translate("New") + ") ";
            }
            str = String.valueOf(str2) + yOVar.mo195rP().get();
        } else {
            str = null;
        }
        if (z3) {
            nR = aur.mo11631nR("database");
            cf = nR.mo4917cf("databaseEntry");
        } else if (z2) {
            nR = aur.mo11631nR("categoryExpanded");
            cf = nR.mo4917cf("category");
        } else {
            nR = aur.mo11631nR("category");
            cf = nR.mo4917cf("category");
        }
        cf.setText(str);
        return nR;
    }
}
