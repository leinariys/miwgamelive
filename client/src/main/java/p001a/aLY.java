package p001a;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import java.util.List;

/* renamed from: a.aLY */
/* compiled from: a */
class aLY<T> implements ComboBoxModel {
    List<? extends T> ioc;
    Object iod;

    public aLY(List<? extends T> list) {
        this.ioc = list;
        if (!list.isEmpty()) {
            this.iod = list.get(0);
        }
    }

    public Object getElementAt(int i) {
        return this.ioc.get(i);
    }

    public Object getSelectedItem() {
        return this.iod;
    }

    public void setSelectedItem(Object obj) {
        if (this.ioc.contains(obj)) {
            this.iod = obj;
        }
    }

    public int getSize() {
        return this.ioc.size();
    }

    public void removeListDataListener(ListDataListener listDataListener) {
    }

    public void addListDataListener(ListDataListener listDataListener) {
    }
}
