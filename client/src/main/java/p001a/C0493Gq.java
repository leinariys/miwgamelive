package p001a;

import logic.thred.LogPrinter;
import taikodom.render.loader.provider.C0399FY;
import taikodom.render.loader.provider.FilePath;

import java.util.HashMap;
import java.util.Map;

/* renamed from: a.Gq */
/* compiled from: a */
public class C0493Gq implements C1942an {
    static LogPrinter logger = LogPrinter.m10275K(C0493Gq.class);
    private final FilePath ecK;
    private final Object root;
    private Map<String, azH> ecJ = new HashMap();

    public C0493Gq(FilePath ain, Object obj) {
        this.ecK = ain;
        this.root = obj;
        bsD();
    }

    private void bsD() {
        if (this.ecK != null) {
            FilePath[] BC = this.ecK.mo2250BC();
            if (BC == null) {
                logger.error("No files found in 'components' directory. The directory itself might be missing");
                return;
            }
            for (FilePath ain : BC) {
                if (ain.isDir()) {
                    FilePath[] a = ain.mo2258a(new C0494a());
                    if (a.length == 0) {
                        logger.warn("no components found in " + ain.getName());
                    }
                    for (FilePath a2 : a) {
                        try {
                            start(mo2406a(a2));
                        } catch (Exception e) {
                            logger.warn("Error loading components", e);
                        }
                    }
                } else {
                    logger.info("Inoring file components/" + ain.getName());
                }
            }
        }
    }

    /* renamed from: a */
    public String mo2406a(FilePath ain) throws Exception {
        azH azh = new azH(ain, this.root, this);
        if (this.ecJ.containsKey(azh.cHn())) {
            throw new Exception("Component '" + azh.cHn() + "' already added");
        }
        this.ecJ.put(azh.cHn(), azh);
        return azh.cHn();
    }

    public void stop(String str) {
        this.ecJ.get(str).stop();
    }

    public void start(String str) throws Exception {
        this.ecJ.get(str).start();
    }

    /* renamed from: o */
    public void mo2408o(String str) throws Exception {
        azH azh = this.ecJ.get(str);
        azh.stop();
        azh.start();
    }

    public void dispose() {
        for (azH stop : this.ecJ.values()) {
            stop.stop();
        }
    }

    /* renamed from: q */
    public void mo2409q(String str) {
        stop(str);
        this.ecJ.remove(str);
    }

    /* renamed from: r */
    public String mo2410r(String str) throws Exception {
        return mo2406a(this.ecK.concat(str));
    }

    /* renamed from: a.Gq$a */
    class C0494a implements C0399FY {
        C0494a() {
        }

        /* renamed from: a */
        public boolean mo2163a(FilePath ain, String str) {
            return str.endsWith(".xml");
        }
    }
}
