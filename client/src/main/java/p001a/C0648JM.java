package p001a;

/* renamed from: a.JM */
/* compiled from: a */
public enum C0648JM {
    CORE("messageDialogCore.css"),
    NEUTRAL("messageDialogNeutral.css");

    private String css;

    private C0648JM(String str) {
        this.css = str;
    }

    /* access modifiers changed from: package-private */
    public String aZz() {
        return this.css;
    }
}
