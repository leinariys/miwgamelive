package p001a;

/* renamed from: a.nZ */
/* compiled from: a */
public class C3085nZ {
    public static final long aMA = Long.MIN_VALUE;
    public static final short aMB = 48;
    public static final long aMC = 63;
    public static final long aMx = -9223090561878065153L;
    public static final long aMy = 281474976710655L;
    public static final long aMz = 9223090561878065152L;

    /* renamed from: cC */
    public static boolean m36287cC(long j) {
        return (aMA & j) != 0;
    }

    /* renamed from: cD */
    public static short m36288cD(long j) {
        return (short) ((int) ((aMz & j) >> 48));
    }

    /* renamed from: cE */
    public static long m36289cE(long j) {
        return aMx & j;
    }

    /* renamed from: cF */
    public static long m36290cF(long j) {
        return aMy & j;
    }

    /* renamed from: a */
    public static long m36284a(short s, long j) {
        return ((((long) s) << 48) & aMz) | (aMx & j);
    }

    /* renamed from: a */
    public static long m36285a(boolean z, short s, long j) {
        return ((z ? 1 : 0) << aMC) | ((((long) s) << 48) & aMz) | (aMy & j);
    }

    /* renamed from: b */
    public static long m36286b(short s, long j) {
        return m36284a(s, j);
    }

    public static String toString(long j) {
        return String.valueOf(j) + "[" + m36287cC(j) + "," + m36288cD(j) + "," + m36290cF(j) + "]";
    }
}
