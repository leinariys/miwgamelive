package p001a;

import logic.ui.item.Picture;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

/* renamed from: a.adi  reason: case insensitive filesystem */
/* compiled from: a */
public class C5958adi extends ayO {
    int fhi = 0;
    private boolean fhj = false;

    public C5958adi(C6416amY amy, int i, String str, String str2, String str3, String[] strArr) {
        super((MethodVisitor) null, amy, i, C2821kb.arY, str2, str3, strArr);
        this.mv = amy.ckg().visitMethod(i, C2821kb.arY, str2, str3, strArr);
    }

    public void visitMethodInsn(int i, String str, String str2, String str3) {
        if (i == 183) {
            if (this.cUO.isTopLevel() && !this.fhj && C2821kb.arR.equals(str2) && str.equals(C2821kb.aqo.getInternalName())) {
                this.fhj = true;
                super.visitInsn(87);
                return;
            } else if (C2821kb.arR.equals(str2) && (this.cUO.superName.equals(str) || this.cUO.name.equals(str))) {
                super.visitMethodInsn(183, str, C2821kb.arY, str3);
                return;
            }
        }
        super.visitMethodInsn(i, str, str2, str3);
    }

    public String toString() {
        return String.valueOf(this.name) + this.desc;
    }

    public void visitEnd() {
        super.visitEnd();
        MethodVisitor visitMethod = this.cUO.ckg().visitMethod(this.access, C2821kb.arR, this.desc, this.signature, this.gVY);
        Picture label = new Picture();
        visitMethod.visitCode();
        visitMethod.visitLabel(label);
        visitMethod.visitVarInsn(25, 0);
        visitMethod.visitInsn(1);
        visitMethod.visitMethodInsn(183, this.cUO.superName, C2821kb.arR, C2821kb.arW);
        visitMethod.visitVarInsn(25, 0);
        int i = 1;
        for (Type type : this.gVX) {
            visitMethod.visitVarInsn(type.getOpcode(21), i);
            i += type.getSize();
        }
        visitMethod.visitMethodInsn(183, this.cUO.superName, C2821kb.arY, this.desc);
        visitMethod.visitInsn(177);
        visitMethod.visitMaxs(3, i);
        visitMethod.visitEnd();
    }
}
