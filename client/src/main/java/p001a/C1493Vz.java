package p001a;

import game.script.item.Item;
import game.script.item.ItemType;
import logic.baa.C5473aKr;
import logic.res.code.C5663aRz;

import java.util.Collection;
import java.util.Set;

/* renamed from: a.Vz */
/* compiled from: a */
public interface C1493Vz {
    /* renamed from: a */
    void mo2685a(C5663aRz arz, C5473aKr<?, ?> akr);

    float aRB();

    Set<Item> aRz();

    /* renamed from: b */
    void mo2689b(C5663aRz arz, C5473aKr<?, ?> akr);

    /* renamed from: d */
    boolean mo2690d(Item auq);

    /* renamed from: k */
    void mo2691k(Collection<? extends Item> collection);

    void removeAll();

    /* renamed from: s */
    Item mo2693s(Item auq);

    /* renamed from: t */
    boolean mo2694t(Item auq);

    /* renamed from: u */
    Item mo2695u(ItemType jCVar);

    /* renamed from: wE */
    float mo2696wE();
}
