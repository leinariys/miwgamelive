package p001a;

/* renamed from: a.ja */
/* compiled from: a */
public class C2756ja {
    /* renamed from: ca */
    public static final int m34042ca(int i) {
        switch (i) {
            case 0:
                return -1;
            case 1:
                return 0;
            case 2:
                return 1;
            case 3:
                return 2;
            default:
                throw new RuntimeException("Unknown button: " + i);
        }
    }

    /* renamed from: cb */
    public static final int m34043cb(int i) {
        switch (i) {
            case -1:
                return 0;
            case 0:
                return 1;
            case 1:
                return 2;
            case 2:
                return 3;
            default:
                throw new RuntimeException("Unknown button: " + i);
        }
    }
}
