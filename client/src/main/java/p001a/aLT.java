package p001a;

import game.network.message.WriteEncryptionOutputStream;

import java.io.InputStream;

/* renamed from: a.aLT */
/* compiled from: a */
public class aLT extends InputStream {

    /* renamed from: in */
    private final InputStream f3315in;

    /* renamed from: oZ */
    private int f3316oZ = 48151623;

    public aLT(InputStream inputStream) {
        this.f3315in = inputStream;
    }

    public void close() {
        this.f3315in.close();
    }

    public int read() {
        int read = this.f3315in.read();
        if (read < 0) {
            return read;
        }
        int i = ((read & 255) ^ (this.f3316oZ & 255)) & 255;
        this.f3316oZ = WriteEncryptionOutputStream.calculationSaltKey(this.f3316oZ, i);
        return i;
    }

    public int available() {
        return this.f3315in.available();
    }
}
