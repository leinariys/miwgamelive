package p001a;

import com.hoplon.geometry.Color;
import game.geometry.Vec3d;
import game.network.message.externalizable.C6853aut;
import game.script.ship.Ship;
import game.script.ship.ShipType;
import logic.res.ILoaderTrail;
import taikodom.render.RenderView;
import taikodom.render.helpers.RenderTask;
import taikodom.render.scene.RGroup;
import taikodom.render.scene.RParticleSystem;
import taikodom.render.scene.RTrail;
import taikodom.render.scene.SceneObject;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.SZ */
/* compiled from: a */
public class C1255SZ {
    private static int iFI = 0;
    /* access modifiers changed from: private */
    public RGroup iFF;
    private boolean boG = false;
    private List<RTrail> iFG;
    private List<RParticleSystem> iFH;

    public static int dqg() {
        return iFI;
    }

    /* renamed from: Al */
    public static void m9468Al(int i) {
        iFI = i;
    }

    public void destroy() {
        if (this.iFG != null) {
            for (RTrail dispose : this.iFG) {
                dispose.dispose();
            }
            this.iFG.clear();
            this.iFG = null;
        }
        if (this.iFH != null) {
            for (RParticleSystem dispose2 : this.iFH) {
                dispose2.dispose();
            }
            this.iFH.clear();
            this.iFH = null;
        }
        this.iFF = null;
    }

    /* renamed from: kc */
    public void mo5422kc(boolean z) {
        this.boG = z;
    }

    /* renamed from: a */
    public void mo5419a(Ship fAVar, SceneObject sceneObject) {
        this.iFF = new RGroup();
        if (sceneObject != null) {
            sceneObject.addChild(this.iFF);
        }
        this.iFG = new ArrayList();
        this.iFH = new ArrayList();
        m9474z(fAVar.agH());
    }

    /* access modifiers changed from: private */
    /* renamed from: s */
    public void m9473s(SceneObject sceneObject) {
        if (sceneObject instanceof RTrail) {
            this.iFG.add((RTrail) sceneObject);
        } else if (sceneObject instanceof RParticleSystem) {
            if (this.boG) {
                this.iFH.add((RParticleSystem) sceneObject);
            } else if (iFI == 0) {
                this.iFH.add((RParticleSystem) sceneObject);
                ((RParticleSystem) sceneObject).setSpawnLimitStart(10000.0d);
                ((RParticleSystem) sceneObject).setSpawnLimitEnd(50000.0d);
            } else {
                sceneObject.dispose();
            }
        }
        for (SceneObject s : sceneObject.getChildren()) {
            m9473s(s);
        }
    }

    /* renamed from: z */
    private void m9474z(ShipType ng) {
        C5916acs.getSingolton().getLoaderTrail().mo4994a(ng.mo4151Nu().getHandle(), new C1257b(ng), ng.mo19891ke().get());
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9469a(ShipType ng, C1212Ru.C1213a aVar) {
        ILoaderTrail alg = C5916acs.getSingolton().getLoaderTrail();
        if (aVar != null) {
            for (String as : aVar.brr()) {
                alg.mo4999as(as);
            }
            for (C1212Ru.C1214b next : aVar.brs()) {
                C6853aut bc = ng.mo21127bc(next.btt());
                SceneObject sceneObject = (SceneObject) alg.getAsset(next.btu());
                if (!(sceneObject == null || bc == null)) {
                    sceneObject.setPosition(bc.mo16402gL());
                    C5916acs.getSingolton().getEngineGraphics().mo3007a((RenderTask) new C1256a(sceneObject));
                }
            }
        }
    }

    /* renamed from: nT */
    public void mo5423nT(float f) {
        for (RTrail lifeTimeModifier : this.iFG) {
            lifeTimeModifier.setLifeTimeModifier(f);
        }
        for (RParticleSystem next : this.iFH) {
            Color particlesInitialColor = next.getParticlesInitialColor();
            particlesInitialColor.w = f;
            next.setParticlesInitialColor(particlesInitialColor);
        }
    }

    /* renamed from: nU */
    public void mo5424nU(float f) {
        for (RParticleSystem newPartsSecond : this.iFH) {
            newPartsSecond.setNewPartsSecond(f);
        }
    }

    /* renamed from: F */
    public void mo5418F(float f, float f2) {
        for (RParticleSystem next : this.iFH) {
            next.setParticlesInitialSize(next.getParticlesInitialSize() * f);
            next.setParticlesFinalSize(next.getParticlesFinalSize() * f2);
        }
    }

    /* renamed from: aO */
    public void mo5420aO(Vec3d ajr) {
        for (RParticleSystem particlesVelocityVariation : this.iFH) {
            particlesVelocityVariation.setParticlesVelocityVariation(ajr);
        }
    }

    public void setFollowEmitter(boolean z) {
        for (RParticleSystem followEmitter : this.iFH) {
            followEmitter.setFollowEmitter(z);
        }
    }

    /* renamed from: a.SZ$b */
    /* compiled from: a */
    class C1257b implements C1249ST {

        /* renamed from: OV */
        private final ShipType f1545OV;

        public C1257b(ShipType ng) {
            this.f1545OV = ng;
        }

        /* renamed from: a */
        public void mo5417a(C1212Ru.C1213a aVar) {
            C1255SZ.this.m9469a(this.f1545OV, aVar);
        }
    }

    /* renamed from: a.SZ$a */
    class C1256a implements RenderTask {
        private final /* synthetic */ SceneObject egM;

        C1256a(SceneObject sceneObject) {
            this.egM = sceneObject;
        }

        public void run(RenderView renderView) {
            if (C1255SZ.this.iFF != null) {
                C1255SZ.this.iFF.addChild(this.egM);
                C1255SZ.this.m9473s(this.egM);
                C1255SZ.this.mo5423nT(0.0f);
            }
        }
    }
}
