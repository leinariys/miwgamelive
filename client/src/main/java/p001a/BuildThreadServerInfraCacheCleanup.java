package p001a;

import game.engine.DataGameEvent;
import game.network.channel.client.ClientConnect;
import game.network.message.externalizable.ObjectId;
import logic.baa.C1616Xf;
import logic.thred.LogPrinter;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* renamed from: a.ch */
/* compiled from: a */
public class BuildThreadServerInfraCacheCleanup implements C2913lj {
    static LogPrinter logger = LogPrinter.setClass(BuildThreadServerInfraCacheCleanup.class);
    /* access modifiers changed from: private */
    public ReferenceQueue<C3582se> exF;
    /* access modifiers changed from: private */
    public Thread exH;
    DataGameEvent aGA;
    private ConcurrentMap<ObjectId, C2201b> exE;
    private LinkedHashMap<ObjectId, C3582se> exG;

    public BuildThreadServerInfraCacheCleanup(DataGameEvent jz) {
        this();
        this.aGA = jz;
    }

    public BuildThreadServerInfraCacheCleanup() {
        this.exE = new ConcurrentHashMap();
        this.exF = new ReferenceQueue<>();
        this.exG = new LinkedHashMap<>();
        this.exH = null;
        this.exH = new C2202c("ServerInfraCache cleanup");
        this.exH.setDaemon(true);
        this.exH.start();
    }

    public void dispose() {
        if (this.exH != null) {
            Thread thread = this.exH;
            this.exH = null;
            thread.interrupt();
            try {
                thread.join();
            } catch (InterruptedException e) {
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public boolean m28566a(C2201b bVar) {
        if (!this.exE.remove(bVar.mo17669hC(), bVar)) {
            return false;
        }
        this.aGA.mo3353a(bVar.dqS);
        return true;
    }

    /* renamed from: a */
    public void mo15460a(C3582se seVar) {
        if (this.exG != null) {
            while (this.exG.size() > 10000) {
                this.exG.remove(this.exG.keySet().iterator().next());
            }
            this.exG.put(seVar.getObjectId(), seVar);
        }
        C2201b bVar = new C2201b(seVar);
        C2201b putIfAbsent = this.exE.putIfAbsent(seVar.getObjectId(), bVar);
        if (putIfAbsent != null) {
            bVar.clear();
            if (putIfAbsent.get() == null) {
                throw new RuntimeException("The object was already garbage collected: " + seVar.getObjectId().getId() + " - " + seVar.bFU());
            }
            throw new RuntimeException("Given OID is already in use: " + seVar.getObjectId());
        }
    }

    /* renamed from: c */
    public boolean mo15463c(C3582se seVar) {
        if (this.exE.putIfAbsent(seVar.getObjectId(), new C2201b(seVar)) != null) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public C3582se mo15459a(ObjectId apo) {
        C2201b bVar = (C2201b) this.exE.get(apo);
        if (bVar == null) {
            return null;
        }
        C3582se seVar = (C3582se) bVar.get();
        if (seVar != null) {
            return seVar;
        }
        m28566a(bVar);
        return seVar;
    }

    /* renamed from: b */
    public boolean mo15462b(ObjectId apo) {
        return mo15459a(apo) != null;
    }

    /* renamed from: b */
    public void mo15461b(C3582se seVar) {
        C2201b bVar = (C2201b) this.exE.remove(seVar.getObjectId());
        if (bVar != null) {
            bVar.clear();
        }
        if (this.exG != null) {
            this.exG.remove(seVar.getObjectId());
        }
    }

    public Iterator<C3582se> iterator() {
        return new C2203d(new ArrayList(this.exE.values()).iterator());
    }

    public int size() {
        return this.exE.size();
    }

    /* renamed from: a.ch$a */
    public class C2200a {
        private Class<? extends C1616Xf> clazz;

        /* renamed from: qT */
        private ObjectId f6204qT;

        /* renamed from: qU */
        private ClientConnect f6205qU;

        /* renamed from: qV */
        private boolean f6206qV;

        public C2200a(ObjectId apo, Class<? extends C1616Xf> cls, ClientConnect bVar) {
            this.f6204qT = apo;
            this.clazz = cls;
            this.f6205qU = bVar;
        }

        /* renamed from: hC */
        public ObjectId mo17665hC() {
            return this.f6204qT;
        }

        /* renamed from: hD */
        public Class<? extends C1616Xf> mo17666hD() {
            return this.clazz;
        }

        /* renamed from: hE */
        public ClientConnect mo17667hE() {
            return this.f6205qU;
        }

        /* renamed from: A */
        public void mo17664A(boolean z) {
            this.f6206qV = z;
        }

        /* renamed from: hF */
        public boolean mo17668hF() {
            return this.f6206qV;
        }
    }

    /* renamed from: a.ch$b */
    /* compiled from: a */
    private class C2201b extends WeakReference<C3582se> {
        C2200a dqS;

        public C2201b(C3582se seVar) {
            super(seVar, BuildThreadServerInfraCacheCleanup.this.exF);
            this.dqS = new C2200a(seVar.getObjectId(), seVar.bFU(), seVar.mo6892hE());
            seVar.mo21977b(this.dqS);
        }

        /* renamed from: hE */
        public ClientConnect mo17671hE() {
            return this.dqS.mo17667hE();
        }

        /* renamed from: hC */
        public ObjectId mo17669hC() {
            return this.dqS.mo17665hC();
        }

        /* renamed from: hD */
        public Class<? extends C1616Xf> mo17670hD() {
            return this.dqS.mo17666hD();
        }
    }

    /* renamed from: a.ch$c */
    /* compiled from: a */
    class C2202c extends Thread {
        C2202c(String str) {
            super(str);
        }

        public void run() {
            while (BuildThreadServerInfraCacheCleanup.this.exH == Thread.currentThread()) {
                try {
                    boolean unused = BuildThreadServerInfraCacheCleanup.this.m28566a((C2201b) BuildThreadServerInfraCacheCleanup.this.exF.remove());
                } catch (InterruptedException e) {
                    return;
                }
            }
        }
    }

    /* renamed from: a.ch$d */
    /* compiled from: a */
    class C2203d implements Iterator<C3582se> {
        private final /* synthetic */ Iterator hfP;
        C3582se hfO = null;

        C2203d(Iterator it) {
            this.hfP = it;
        }

        public boolean hasNext() {
            while (this.hfP.hasNext()) {
                C2201b bVar = (C2201b) this.hfP.next();
                if (bVar != null) {
                    C3582se seVar = (C3582se) bVar.get();
                    if (seVar != null) {
                        this.hfO = seVar;
                        return true;
                    }
                    boolean unused = BuildThreadServerInfraCacheCleanup.this.m28566a(bVar);
                }
            }
            return false;
        }

        /* renamed from: cIS */
        public C3582se next() {
            return this.hfO;
        }

        public void remove() {
            this.hfP.remove();
        }
    }
}
