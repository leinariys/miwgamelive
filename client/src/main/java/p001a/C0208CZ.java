package p001a;

/* renamed from: a.CZ */
/* compiled from: a */
public class C0208CZ {
    private final C5990aeO cDc;
    private final boolean stop;

    public C0208CZ(C5990aeO aeo) {
        this(aeo, false);
    }

    public C0208CZ(C5990aeO aeo, boolean z) {
        this.cDc = aeo;
        this.stop = z;
    }

    public C5990aeO aDI() {
        return this.cDc;
    }

    public boolean aDJ() {
        return this.stop;
    }
}
