package p001a;

import game.script.Actor;
import game.script.corporation.Corporation;
import game.script.ship.Outpost;
import logic.ui.C2698il;
import logic.ui.Panel;
import logic.ui.item.Progress;
import taikodom.addon.IAddonProperties;
import taikodom.addon.outpost.OutpostUpgradeDialogAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/* renamed from: a.asp  reason: case insensitive filesystem */
/* compiled from: a */
public class C6745asp extends C5884acM {
    private JButton ijA = this.djv.mo4913cb("upgradeBtn");
    private JLabel ijB = this.djv.mo4917cf("stationName");
    private JLabel ijC = this.djv.mo4917cf("stationType");
    private JLabel ijD = this.djv.mo4917cf("stationClass");
    private JLabel ijE = this.djv.mo4917cf("stationStatus");
    private OutpostUpgradeDialogAddon ijx = ((OutpostUpgradeDialogAddon) this.f4217kj.mo11830U(OutpostUpgradeDialogAddon.class));
    private Panel ijy = this.djv.mo4915cd("southPanel");
    private Progress ijz = this.djv.mo4915cd("upgradeBar");

    public C6745asp(IAddonProperties vWVar, C2698il ilVar) {
        super(vWVar, ilVar);
        this.ijA.addActionListener(new C1988a());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo12600a(Outpost qZVar, boolean z) {
        String translate;
        boolean z2 = true;
        this.djv.setEnabled(qZVar.isEnabled());
        Corporation bYd = this.f4217kj.getPlayer().bYd();
        if (bYd == null || !bYd.mo10707Qy().contains(qZVar)) {
            this.djv.setEnabled(false);
            return;
        }
        this.djv.setEnabled(true);
        Outpost.OutpostUpgradeStatus bYj = qZVar.bYj();
        if (bYj == null) {
            boolean z3 = !qZVar.mo21372Mb().mo19281Ab();
            boolean b = bYd.mo10718b(this.f4217kj.getPlayer(), C6704asA.OUTPOST_LEVEL_UP);
            JButton jButton = this.ijA;
            if (!z3 || !b) {
                z2 = false;
            }
            jButton.setEnabled(z2);
            this.ijy.getLayout().show(this.ijy, "BUTTON");
        } else {
            this.ijz.setValue(bYj.dvv());
            this.ijz.setString(String.valueOf(String.valueOf(bYj.dvv())) + " %");
            this.ijy.getLayout().show(this.ijy, "PROGRESS");
        }
        if (!z) {
            this.ijB.setText(qZVar.getName());
            this.ijC.setText(qZVar.bYh().beF().get());
            this.ijD.setText(qZVar.bYn());
            JLabel jLabel = this.ijE;
            if (qZVar.isEnabled()) {
                translate = this.f4217kj.translate("Active");
            } else {
                translate = this.f4217kj.translate("Unactive");
            }
            jLabel.setText(translate);
        }
        this.djv.validate();
    }

    /* access modifiers changed from: package-private */
    public void destroy() {
        if (this.ijx != null && this.ijx.isVisible()) {
            this.ijx.stop();
        }
    }

    /* renamed from: a.asp$a */
    class C1988a implements ActionListener {
        C1988a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            Actor bhE = C6745asp.this.f4217kj.getPlayer().bhE();
            if (bhE instanceof Outpost) {
                Outpost qZVar = (Outpost) bhE;
                ((OutpostUpgradeDialogAddon) C6745asp.this.f4217kj.mo11830U(OutpostUpgradeDialogAddon.class)).mo24458a((OutpostUpgradeDialogAddon.C4868d) new C1989a(qZVar), qZVar.bYp());
            }
        }

        /* renamed from: a.asp$a$a */
        class C1989a implements OutpostUpgradeDialogAddon.C4868d {
            private final /* synthetic */ Outpost iBl;

            C1989a(Outpost qZVar) {
                this.iBl = qZVar;
            }

            public void bmD() {
                try {
                    this.iBl.bYl();
                } catch (Outpost.C3350c e) {
                    C6745asp.this.mo12599a(e);
                }
            }
        }
    }
}
