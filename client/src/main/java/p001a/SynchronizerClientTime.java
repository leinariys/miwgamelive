package p001a;

import org.mozilla1.javascript.ScriptRuntime;

/* renamed from: a.Gm */
/* compiled from: a */
public class SynchronizerClientTime implements CurrentTimeMilli {
    private static final int cXU = 100;
    private static final int cYe = 16;
    private static final double cYf = 1.5d;
    double cXQ = 1.0d;
    double cXR = ScriptRuntime.NaN;
    long cXS;
    long cXT;
    int cXV = 0;
    long[] cXW = new long[100];
    long[] cXX = new long[100];
    long cXY = 0;
    long cXZ = 0;
    long cYa = 0;
    long cYb = 0;
    double cYc = ScriptRuntime.NaN;
    int samples = 0;
    private long cYd;

    public void reset() {
        this.samples = 0;
    }

    /* renamed from: a */
    public void mo2399a(long j, long j2, long j3) {
        if (this.samples <= 0) {
            this.cXS = j2;
            this.cXT = j;
            this.samples = 1;
            this.cYc = (double) j3;
            return;
        }
        if (this.samples < 16) {
            this.cYc = ((((double) this.samples) * this.cYc) + ((double) j3)) / ((double) (this.samples + 1));
        } else if (((double) j3) <= this.cYc * cYf) {
            this.cYc = ((15.0d * this.cYc) + ((double) j3)) / 16.0d;
        } else {
            return;
        }
        if (this.samples < 100) {
            this.samples++;
        } else {
            long j4 = this.cXW[this.cXV];
            long j5 = this.cXX[this.cXV];
            this.cXZ -= j4;
            this.cYa -= j5;
            this.cXY -= j4 * j4;
            this.cYb -= j4 * j5;
        }
        long j6 = j2 - this.cXS;
        long j7 = j - this.cXT;
        this.cXZ += j6;
        this.cYa += j7;
        this.cXY += j6 * j6;
        this.cYb += j6 * j7;
        this.cXQ = ((double) ((((long) this.samples) * this.cYb) - (this.cXZ * this.cYa))) / ((double) ((((long) this.samples) * this.cXY) - (this.cXZ * this.cXZ)));
        this.cXR = (((double) this.cYa) - (this.cXQ * ((double) this.cXZ))) / ((double) this.samples);
        this.cXW[this.cXV] = j6;
        this.cXX[this.cXV] = j7;
        this.cXV++;
        if (this.cXV >= 100) {
            this.cXV = 0;
            for (int i = 0; i < 100; i++) {
                long[] jArr = this.cXW;
                jArr[i] = jArr[i] - j6;
                long[] jArr2 = this.cXX;
                jArr2[i] = jArr2[i] - j7;
            }
            this.cXZ -= 100 * j6;
            this.cYa -= 100 * j7;
            this.cXY -= ((2 * j6) * this.cXZ) + ((100 * j6) * j6);
            this.cYb -= ((this.cYa * j6) + (this.cXZ * j7)) + ((100 * j6) * j7);
            this.cXR = ((this.cXQ * ((double) j6)) + this.cXR) - ((double) j7);
            this.cXS = j2;
            this.cXT = j;
        }
    }

    /* renamed from: eM */
    public long mo2402eM(long j) {
        return this.samples < 10 ? j : ((long) ((this.cXQ * ((double) (j - this.cXS))) + this.cXR)) + this.cXT;
    }

    /* renamed from: eN */
    public long mo2403eN(long j) {
        return ((long) ((((double) (j - this.cXT)) - this.cXR) / this.cXQ)) + this.cXS;
    }

    public String debugString() {
        return String.format("[alpha=%f, beta=%f, clientRef=%d, serverRef=%d, avgRTT=%f]\n[sx=%d, sy=%d, sxx=%d, sxy=%d]", new Object[]{Double.valueOf(this.cXQ), Double.valueOf(this.cXR), Long.valueOf(this.cXS), Long.valueOf(this.cXT), Double.valueOf(this.cYc), Long.valueOf(this.cXZ), Long.valueOf(this.cYa), Long.valueOf(this.cXY), Long.valueOf(this.cYb)});
    }

    public long currentTimeMillis() {
        if (this.samples < 10) {
            return SingletonCurrentTimeMilliImpl.currentTimeMillis();
        }
        long eM = mo2402eM(SingletonCurrentTimeMilliImpl.currentTimeMillis());
        if (this.cYd > eM) {
            return this.cYd;
        }
        this.cYd = eM;
        return eM;
    }
}
