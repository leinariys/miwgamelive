package p001a;

/* renamed from: a.agy  reason: case insensitive filesystem */
/* compiled from: a */
public class C6130agy extends RuntimeException {
    private static final long serialVersionUID = -5799361694650354632L;
    private String entryName;
    private Class<?> expectedType;
    private Class<?> fys;

    public C6130agy(Class<?> cls, Class<?> cls2, String str) {
        this.expectedType = cls;
        this.fys = cls2;
        this.entryName = str;
    }

    public final Class<?> bWR() {
        return this.fys;
    }

    public final String getEntryName() {
        return this.entryName;
    }

    public final Class<?> bWS() {
        return this.expectedType;
    }
}
