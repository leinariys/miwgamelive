package p001a;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.iu */
/* compiled from: a */
public @interface C2712iu {

    /* renamed from: Yn */
    public static final String f8282Yn = "$1Type";

    /* renamed from: Yo */
    public static final String f8283Yo = "(^.*)";

    /* renamed from: Bs */
    Class<?>[] mo19785Bs() default {};

    /* renamed from: Bt */
    Class<?> mo19786Bt() default Void.class;

    /* renamed from: Bu */
    String mo19787Bu() default "(^.*)";

    boolean isAbstract() default false;

    String typeName() default "$1Type";

    String version() default "";
}
