package p001a;

/* renamed from: a.aUz  reason: case insensitive filesystem */
/* compiled from: a */
public enum C5741aUz {
    LEVEL_1,
    LEVEL_2,
    LEVEL_3,
    LEVEL_4,
    LEVEL_5;

    public static C5741aUz dAq() {
        return values()[0];
    }

    public static C5741aUz dAr() {
        C5741aUz[] values = values();
        return values[values.length - 1];
    }

    public boolean dAp() {
        return this == dAr();
    }

    public C5741aUz dAs() {
        C5741aUz[] values = values();
        return values[Math.min(ordinal() + 1, values.length - 1)];
    }

    public C5741aUz dAt() {
        return values()[Math.max(ordinal() - 1, 0)];
    }
}
