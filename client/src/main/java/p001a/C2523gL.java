package p001a;

import com.hoplon.geometry.Color;
import game.geometry.Vector2fWrap;
import taikodom.addon.IAddonProperties;
import taikodom.render.enums.BlendType;
import taikodom.render.gui.GBillboard;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;

/* renamed from: a.gL */
/* compiled from: a */
public class C2523gL implements C6296akI.C1925a {
    public float finalAngle = 180.0f;
    public float initialAngle = 0.0f;
    C2524a euS;
    float euW;
    private boolean api;
    private float euT = 1.0f;
    private GBillboard euU;
    private boolean euV = false;
    private float euX = -1.0f;
    private Shader euY = new Shader();
    private Color finalColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    private Color initialColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
    private long start;

    public C2523gL(float f, float f2) {
        ShaderPass shaderPass = new ShaderPass();
        shaderPass.setBlendEnabled(true);
        shaderPass.setSrcBlend(BlendType.SRC_ALPHA);
        shaderPass.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
        shaderPass.setDepthMask(false);
        shaderPass.setDepthTestEnabled(false);
        this.euY.addPass(shaderPass);
        this.euU = new GBillboard();
        this.euU.setSize(new Vector2fWrap(bBx() * 3.0f, bBy() * 3.0f));
        this.euU.setName("jumpin");
        this.euW = f;
        this.euX = (float) Math.sin((((double) this.euW) * 3.141592653589793d) / 180.0d);
        this.euU.setPrimitiveColor(new Color((this.finalColor.x * this.euX) - (this.initialColor.x * (1.0f - this.euX)), (this.finalColor.y * this.euX) - (this.initialColor.y * (1.0f - this.euX)), (this.finalColor.z * this.euX) - (this.initialColor.z * (1.0f - this.euX)), (this.finalColor.w * this.euX) - (this.initialColor.w * (1.0f - this.euX))));
        this.euU.setRender(true);
        this.euU.setShader(this.euY);
        C5916acs.getSingolton().getEngineGraphics().aeh().addChild(this.euU);
        this.initialAngle = f;
        this.finalAngle = f2;
    }

    public float bBx() {
        return (float) C5916acs.getSingolton().getEngineGraphics().aes();
    }

    public float bBy() {
        return (float) C5916acs.getSingolton().getEngineGraphics().aet();
    }

    /* renamed from: p */
    public void mo18957p(float f, float f2) {
        this.initialAngle = f;
        this.finalAngle = f2;
    }

    /* renamed from: a */
    public void mo18951a(Color color, Color color2) {
        this.initialColor = color;
        this.finalColor = color2;
    }

    /* renamed from: a */
    public void mo18949a(C2524a aVar) {
        this.euS = aVar;
    }

    /* renamed from: a */
    public void mo18950a(IAddonProperties vWVar) {
        this.start = System.currentTimeMillis();
        C5916acs.getSingolton().getEngineGraphics().mo3004a((C6296akI.C1925a) this);
    }

    /* renamed from: a */
    public void mo18948a(float f, IAddonProperties vWVar) {
        this.euT = f;
        mo18950a(vWVar);
    }

    /* renamed from: a */
    public void mo1968a(float f, long j) {
        if (this.api) {
            C5916acs.getSingolton().getEngineGraphics().mo3044b((C6296akI.C1925a) this);
        }
        float currentTimeMillis = ((float) (System.currentTimeMillis() - this.start)) / 1000.0f;
        if (this.euU == null) {
            if (this.euS != null) {
                this.euS.mo15343wk();
            }
            destroy();
            cancel();
        } else if (currentTimeMillis < this.euT || this.euV) {
            float f2 = currentTimeMillis / this.euT;
            if (f2 > 1.0f) {
                f2 = 1.0f;
            }
            this.euW = ((1.0f - f2) * this.initialAngle) + (this.finalAngle * f2);
            this.euX = (float) Math.sin((((double) this.euW) * 3.141592653589793d) / 180.0d);
            this.euU.setPrimitiveColor(new Color((this.finalColor.x * this.euX) - (this.initialColor.x * (1.0f - this.euX)), (this.finalColor.y * this.euX) - (this.initialColor.y * (1.0f - this.euX)), (this.finalColor.z * this.euX) - (this.initialColor.z * (1.0f - this.euX)), (this.finalColor.w * this.euX) - ((1.0f - this.euX) * this.initialColor.w)));
        } else {
            if (this.euS != null) {
                this.euS.mo15343wk();
            }
            destroy();
            cancel();
        }
    }

    public void destroy() {
        if (this.euU != null) {
            this.euU.setPrimitiveColor(new Color(0.0f, 0.0f, 0.0f, 0.0f));
            C5916acs.getSingolton().getEngineGraphics().aeh().removeChild(this.euU);
            this.euU.dispose();
            this.euU = null;
        }
    }

    public void cancel() {
        this.api = true;
        this.euV = false;
    }

    /* renamed from: dC */
    public void mo18955dC(boolean z) {
        this.euV = z;
    }

    /* renamed from: a.gL$a */
    public interface C2524a {
        /* renamed from: wk */
        void mo15343wk();
    }
}
