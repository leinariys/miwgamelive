package p001a;

import game.script.hangar.Bay;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.item.SimpleBag;
import game.script.ship.CargoHold;
import game.script.ship.Station;
import game.script.storage.Storage;

/* renamed from: a.amo  reason: case insensitive filesystem */
/* compiled from: a */
public class C6432amo {
    /* renamed from: a */
    public static boolean m24003a(Item auq, C1493Vz vz) {
        if (vz.aRz().contains(auq)) {
            return true;
        }
        ItemLocation bNh = auq.bNh();
        if ((bNh instanceof Storage) || (bNh instanceof Bay)) {
            return false;
        }
        if (bNh instanceof SimpleBag.SimpleBagLocation) {
            return m24003a((Item) ((SimpleBag.SimpleBagLocation) bNh).mo18991wC(), vz);
        }
        if (bNh instanceof CargoHold) {
            return m24003a((Item) ((CargoHold) bNh).mo22732al().agr(), vz);
        }
        return false;
    }

    /* renamed from: a */
    public static boolean m24002a(Item auq, Station bf) {
        ItemLocation bNh = auq.bNh();
        if (bNh instanceof Storage) {
            return bf.azF().contains((Storage) bNh);
        } else if (bNh instanceof Bay) {
            return ((Bay) bNh).dfF().mo20066eT().equals(bf);
        } else {
            if (bNh instanceof CargoHold) {
                return m24002a((Item) ((CargoHold) bNh).mo22732al().agr(), bf);
            }
            if (bNh instanceof SimpleBag.SimpleBagLocation) {
                return m24002a((Item) ((SimpleBag.SimpleBagLocation) bNh).mo18991wC(), bf);
            }
            return false;
        }
    }
}
