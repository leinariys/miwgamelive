package p001a;

/* renamed from: a.aNY */
/* compiled from: a */
public class aNY {
    private final float length;
    private final String name;

    public aNY(String str, float f) {
        this.name = str;
        this.length = f;
    }

    public String getName() {
        return this.name;
    }

    public float getLength() {
        return this.length;
    }
}
