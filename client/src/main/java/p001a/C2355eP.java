package p001a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

/* renamed from: a.eP */
/* compiled from: a */
public abstract class C2355eP implements Serializable, Cloneable {
    public static final transient Random emH = new Random(System.currentTimeMillis());
    public C2356a[] emI;

    /* access modifiers changed from: protected */
    /* renamed from: Ph */
    public abstract void mo18003Ph();

    /* renamed from: b */
    public abstract <T extends C2355eP> T[] mo18004b(T t);

    /* renamed from: mu */
    public void mo18006mu() {
        int nextInt = emH.nextInt(this.emI.length);
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < nextInt; i++) {
            int i2 = -1;
            while (true) {
                if (i2 != -1 && !arrayList.contains(Integer.valueOf(i2))) {
                    break;
                }
                i2 = emH.nextInt(this.emI.length);
            }
            this.emI[i2].mo18014mu();
        }
    }

    public Object clone() {
        return super.clone();
    }

    /* renamed from: a.eP$a */
    public class C2356a<T> implements Serializable, Cloneable {
        private static final long serialVersionUID = -8799022021648759662L;

        /* renamed from: DW */
        private final T f6744DW;

        /* renamed from: DX */
        private final T f6745DX;

        /* renamed from: DY */
        private final T[] f6746DY;
        private T value;

        public C2356a(T t, T t2) {
            this.f6744DW = t;
            this.f6745DX = t2;
            this.f6746DY = null;
        }

        public C2356a() {
            this.f6744DW = null;
            this.f6745DX = null;
            this.f6746DY = null;
        }

        public C2356a(T[] tArr) {
            this.f6744DW = null;
            this.f6745DX = null;
            this.f6746DY = tArr;
        }

        public Object clone() {
            return super.clone();
        }

        public Class getType() {
            return this.value.getClass();
        }

        public void set(T t) {
            this.value = t;
        }

        public T get() {
            return this.value;
        }

        /* renamed from: mq */
        public T mo18010mq() {
            return this.f6744DW;
        }

        /* renamed from: mr */
        public T mo18011mr() {
            return this.f6745DX;
        }

        /* renamed from: ms */
        public T[] mo18012ms() {
            return this.f6746DY;
        }

        /* renamed from: mt */
        public int mo18013mt() {
            if (this.value instanceof Integer) {
                return 0;
            }
            if (this.value instanceof Float) {
                return 1;
            }
            if (this.value instanceof Boolean) {
                return 2;
            }
            if (this.value instanceof Enum) {
                return 3;
            }
            return -1;
        }

        /* renamed from: mu */
        public void mo18014mu() {
            switch (mo18013mt()) {
                case 0:
                    int intValue = ((Integer) get()).intValue();
                    int intValue2 = ((Integer) mo18010mq()).intValue();
                    int intValue3 = ((Integer) mo18011mr()).intValue();
                    int i = intValue;
                    while (intValue == i) {
                        i = C2355eP.emH.nextInt(intValue3 - intValue2) + intValue2;
                    }
                    set(new Integer(i));
                    return;
                case 1:
                    float floatValue = ((Float) get()).floatValue();
                    float floatValue2 = ((Float) mo18010mq()).floatValue();
                    float floatValue3 = ((Float) mo18011mr()).floatValue();
                    float f = floatValue;
                    while (f == floatValue) {
                        f = (C2355eP.emH.nextFloat() * (floatValue3 - floatValue2)) + floatValue2;
                    }
                    set(new Float(f));
                    return;
                case 2:
                    set(new Boolean(!((Boolean) get()).booleanValue()));
                    return;
                case 3:
                    int ordinal = ((Enum) get()).ordinal();
                    int i2 = ordinal;
                    while (ordinal == i2) {
                        i2 = C2355eP.emH.nextInt(mo18012ms().length);
                    }
                    set(mo18012ms()[i2]);
                    return;
                default:
                    return;
            }
        }
    }
}
