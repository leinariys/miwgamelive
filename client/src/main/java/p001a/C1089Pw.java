package p001a;

import com.hoplon.geometry.Vec3f;
import game.network.message.serializable.C5737aUv;
import game.network.message.serializable.C5933adJ;
import game.network.message.serializable.aVM;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.Pw */
/* compiled from: a */
public class C1089Pw {
    public static final int hbM = 2048;
    public static final int hbN = 10;
    /* renamed from: jR */
    static final /* synthetic */ boolean f1388jR = (!C1089Pw.class.desiredAssertionStatus());
    private static final boolean hbI = false;
    private static int hbJ = 0;
    private static int hbK = 0;
    private static int hbL = 0;
    public final List<aVM> hbY = new ArrayList();
    private final List<C1080Po> hbO = new ArrayList();
    private final List<C1080Po> hbP = new ArrayList();
    private final Vec3f hbU = new Vec3f();
    private final Vec3f hbV = new Vec3f();
    private final Vec3f hbW = new Vec3f();
    public C1477Vk hbX;
    public int hbZ;
    public C0763Kt stack = C0763Kt.bcE();
    /* renamed from: BD */
    private C5737aUv f1389BD = new C5737aUv();
    private C5317aEr hbQ = new C5317aEr();
    private C5317aEr hbR = new C5317aEr();
    private int hbS;
    private boolean hbT;

    /* renamed from: c */
    public void mo4900c(int i, Vec3f vec3f) {
        if (this.hbT) {
            this.hbR.mo8679i(i, mo4899bd(vec3f));
        } else {
            this.hbP.get(i).dPX.set(vec3f);
        }
    }

    /* renamed from: d */
    public void mo4903d(int i, Vec3f vec3f) {
        if (this.hbT) {
            this.hbR.mo8680j(i, mo4899bd(vec3f));
        } else {
            this.hbP.get(i).dPY.set(vec3f);
        }
    }

    /* renamed from: wH */
    public Vec3f mo4905wH(int i) {
        if (!this.hbT) {
            return this.hbO.get(i).dPX;
        }
        Vec3f vec3f = new Vec3f();
        mo4890a(vec3f, this.hbQ.mo8684xE(i));
        return vec3f;
    }

    /* renamed from: wI */
    public Vec3f mo4906wI(int i) {
        if (!this.hbT) {
            return this.hbO.get(i).dPY;
        }
        Vec3f vec3f = new Vec3f();
        mo4890a(vec3f, this.hbQ.mo8685xF(i));
        return vec3f;
    }

    /* renamed from: I */
    public void mo4882I(Vec3f vec3f, Vec3f vec3f2) {
        mo4904e(vec3f, vec3f2, 1.0f);
    }

    /* renamed from: e */
    public void mo4904e(Vec3f vec3f, Vec3f vec3f2, float f) {
        this.stack = this.stack.bcD();
        this.stack.bcH().push();
        try {
            Vec3f h = this.stack.bcH().mo4460h(f, f, f);
            this.hbU.sub(vec3f, h);
            this.hbV.add(vec3f2, h);
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            vec3f3.sub(this.hbV, this.hbU);
            this.hbW.set(65535.0f, 65535.0f, 65535.0f);
            C0647JL.m5604h(this.hbW, this.hbW, vec3f3);
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: ao */
    public void mo4892ao(int i, int i2) {
        if (this.hbT) {
            this.hbR.mo8674aB(i, -i2);
        } else {
            this.hbP.get(i).dPZ = i2;
        }
    }

    /* renamed from: c */
    public void mo4901c(int i, Vec3f vec3f, Vec3f vec3f2) {
        if (this.hbT) {
            long bd = mo4899bd(vec3f);
            long bd2 = mo4899bd(vec3f2);
            for (int i2 = 0; i2 < 3; i2++) {
                if (this.hbR.mo8675az(i, i2) > C5317aEr.m14388i(bd, i2)) {
                    this.hbR.mo8691z(i, i2, C5317aEr.m14388i(bd, i2));
                }
                if (this.hbR.mo8673aA(i, i2) < C5317aEr.m14388i(bd2, i2)) {
                    this.hbR.mo8671A(i, i2, C5317aEr.m14388i(bd2, i2));
                }
            }
            return;
        }
        C0647JL.m5605o(this.hbP.get(i).dPX, vec3f);
        C0647JL.m5606p(this.hbP.get(i).dPY, vec3f2);
    }

    /* renamed from: ap */
    public void mo4893ap(int i, int i2) {
        if (this.hbT) {
            this.hbQ.swap(i, i2);
            return;
        }
        this.hbO.set(i, this.hbO.get(i2));
        this.hbO.set(i2, this.hbO.get(i));
    }

    /* renamed from: aq */
    public void mo4894aq(int i, int i2) {
        if (this.hbT) {
            this.hbR.mo8672a(i, this.hbQ, i2);
        } else {
            this.hbP.get(i).mo4839a(this.hbO.get(i2));
        }
    }

    /* renamed from: a */
    public void mo4889a(C5933adJ adj, boolean z, Vec3f vec3f, Vec3f vec3f2) {
        int size;
        this.stack = this.stack.bcD();
        this.stack.bcH().push();
        try {
            this.hbT = z;
            if (this.hbT) {
                mo4882I(vec3f, vec3f2);
                adj.mo12767a(new C1091b(this.hbQ, this), this.hbU, this.hbV);
                size = this.hbQ.size();
                this.hbR.resize(size * 2);
            } else {
                adj.mo12767a(new C1090a(this.hbO), this.stack.bcH().mo4460h(-1.0E30f, -1.0E30f, -1.0E30f), this.stack.bcH().mo4460h(1.0E30f, 1.0E30f, 1.0E30f));
                size = this.hbO.size();
                C3696tt.m39762a(this.hbP, size * 2, C1080Po.class);
            }
            this.hbS = 0;
            mo4895ar(0, size);
            if (this.hbT && this.hbY.size() == 0) {
                aVM avm = new aVM();
                this.hbY.add(avm);
                avm.mo11769a(this.hbR, 0);
                avm.jbF = 0;
                avm.jbG = this.hbR.mo8687xH(0) ? 1 : this.hbR.mo8688xI(0);
            }
            this.hbZ = this.hbY.size();
            this.hbQ.clear();
            this.hbO.clear();
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a */
    public void mo4886a(C5933adJ adj) {
        this.stack = this.stack.bcD();
        this.stack.bcH().push();
        try {
            if (this.hbT) {
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                adj.mo12770x(vec3f, vec3f2);
                mo4882I(vec3f, vec3f2);
                mo4887a(adj, 0, this.hbS, 0);
                for (int i = 0; i < this.hbY.size(); i++) {
                    aVM avm = this.hbY.get(i);
                    avm.mo11769a(this.hbR, avm.jbF);
                }
            } else {
                mo4889a(adj, false, (Vec3f) null, (Vec3f) null);
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a */
    public void mo4888a(C5933adJ adj, Vec3f vec3f, Vec3f vec3f2) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public void mo4887a(C5933adJ adj, int i, int i2, int i3) {
        int i4;
        if (f1388jR || this.hbT) {
            this.stack = this.stack.bcD();
            this.stack.bcH().push();
            try {
                Vec3f[] vec3fArr = {(Vec3f) this.stack.bcH().get(), (Vec3f) this.stack.bcH().get(), (Vec3f) this.stack.bcH().get()};
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                Vec3f scaling = adj.getScaling();
                for (int i5 = i2 - 1; i5 >= i; i5--) {
                    C5317aEr aer = this.hbR;
                    if (aer.mo8687xH(i5)) {
                        int xK = aer.mo8690xK(i5);
                        int xJ = aer.mo8689xJ(i5);
                        if (xK != -1) {
                            adj.mo11094b(this.f1389BD, xK);
                            if (!(f1388jR || this.f1389BD.iXo == C6537aop.PHY_INTEGER || this.f1389BD.iXo == C6537aop.PHY_SHORT)) {
                                throw new AssertionError();
                            }
                        }
                        ByteBuffer byteBuffer = this.f1389BD.iXl;
                        int i6 = xJ * this.f1389BD.iXm;
                        for (int i7 = 2; i7 >= 0; i7--) {
                            if (this.f1389BD.iXo == C6537aop.PHY_SHORT) {
                                i4 = byteBuffer.getShort((i7 * 2) + i6) & 65535;
                            } else {
                                i4 = byteBuffer.getInt((i7 * 4) + i6);
                            }
                            ByteBuffer byteBuffer2 = this.f1389BD.iXi;
                            int i8 = i4 * this.f1389BD.stride;
                            vec3fArr[i7].set(byteBuffer2.getFloat(i8 + 0) * scaling.x, byteBuffer2.getFloat(i8 + 4) * scaling.y, byteBuffer2.getFloat(i8 + 8) * scaling.z);
                        }
                        vec3f.set(1.0E30f, 1.0E30f, 1.0E30f);
                        vec3f2.set(-1.0E30f, -1.0E30f, -1.0E30f);
                        C0647JL.m5605o(vec3f, vec3fArr[0]);
                        C0647JL.m5606p(vec3f2, vec3fArr[0]);
                        C0647JL.m5605o(vec3f, vec3fArr[1]);
                        C0647JL.m5606p(vec3f2, vec3fArr[1]);
                        C0647JL.m5605o(vec3f, vec3fArr[2]);
                        C0647JL.m5606p(vec3f2, vec3fArr[2]);
                        aer.mo8679i(i5, mo4899bd(vec3f));
                        aer.mo8680j(i5, mo4899bd(vec3f2));
                    } else {
                        int i9 = i5 + 1;
                        int xI = this.hbR.mo8687xH(i9) ? i5 + 2 : i5 + 1 + this.hbR.mo8688xI(i9);
                        for (int i10 = 0; i10 < 3; i10++) {
                            aer.mo8691z(i5, i10, this.hbR.mo8675az(i9, i10));
                            if (aer.mo8675az(i5, i10) > this.hbR.mo8675az(xI, i10)) {
                                aer.mo8691z(i5, i10, this.hbR.mo8675az(xI, i10));
                            }
                            aer.mo8671A(i5, i10, this.hbR.mo8673aA(i9, i10));
                            if (aer.mo8673aA(i5, i10) < this.hbR.mo8673aA(xI, i10)) {
                                aer.mo8671A(i5, i10, this.hbR.mo8673aA(xI, i10));
                            }
                        }
                    }
                }
                this.f1389BD.dAi();
            } finally {
                this.stack.bcH().pop();
            }
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ar */
    public void mo4895ar(int i, int i2) {
        this.stack = this.stack.bcD();
        this.stack.bcH().push();
        int i3 = i2 - i;
        try {
            int i4 = this.hbS;
            if (!f1388jR && i3 <= 0) {
                throw new AssertionError();
            } else if (i3 == 1) {
                mo4894aq(this.hbS, i);
                this.hbS++;
            } else {
                int y = mo4907y(i, i2, mo4897at(i, i2));
                int i5 = this.hbS;
                mo4903d(this.hbS, this.stack.bcH().mo4460h(-1.0E30f, -1.0E30f, -1.0E30f));
                mo4900c(this.hbS, this.stack.bcH().mo4460h(1.0E30f, 1.0E30f, 1.0E30f));
                for (int i6 = i; i6 < i2; i6++) {
                    mo4901c(this.hbS, mo4905wH(i6), mo4906wI(i6));
                }
                this.hbS++;
                int i7 = this.hbS;
                mo4895ar(i, y);
                int i8 = this.hbS;
                mo4895ar(y, i2);
                int i9 = this.hbS - i4;
                if (this.hbT && C5317aEr.cXz() * i9 > 2048) {
                    mo4896as(i7, i8);
                }
                mo4892ao(i5, i9);
                this.stack.bcH().pop();
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo4891a(long j, long j2, long j3, long j4) {
        int i = C5317aEr.m14388i(j, 0);
        int i2 = C5317aEr.m14388i(j, 1);
        int i3 = C5317aEr.m14388i(j, 2);
        int i4 = C5317aEr.m14388i(j2, 0);
        int i5 = C5317aEr.m14388i(j2, 1);
        int i6 = C5317aEr.m14388i(j2, 2);
        int i7 = C5317aEr.m14388i(j3, 0);
        int i8 = C5317aEr.m14388i(j3, 1);
        int i9 = C5317aEr.m14388i(j3, 2);
        int i10 = C5317aEr.m14388i(j4, 0);
        int i11 = C5317aEr.m14388i(j4, 1);
        int i12 = C5317aEr.m14388i(j4, 2);
        boolean z = true;
        if (i > i10 || i4 < i7) {
            z = false;
        }
        if (i3 > i12 || i6 < i9) {
            z = false;
        }
        if (i2 > i11 || i5 < i8) {
            return false;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    /* renamed from: as */
    public void mo4896as(int i, int i2) {
        int i3 = 1;
        if (f1388jR || this.hbT) {
            int xI = this.hbR.mo8687xH(i) ? 1 : this.hbR.mo8688xI(i);
            int cXz = C5317aEr.cXz() * xI;
            if (!this.hbR.mo8687xH(i2)) {
                i3 = this.hbR.mo8688xI(i2);
            }
            int cXz2 = C5317aEr.cXz() * i3;
            if (cXz <= 2048) {
                aVM avm = new aVM();
                this.hbY.add(avm);
                avm.mo11769a(this.hbR, i);
                avm.jbF = i;
                avm.jbG = xI;
            }
            if (cXz2 <= 2048) {
                aVM avm2 = new aVM();
                this.hbY.add(avm2);
                avm2.mo11769a(this.hbR, i2);
                avm2.jbF = i2;
                avm2.jbG = i3;
            }
            this.hbZ = this.hbY.size();
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: protected */
    /* renamed from: y */
    public int mo4907y(int i, int i2, int i3) {
        boolean z;
        this.stack = this.stack.bcD();
        this.stack.bcH().push();
        int i4 = i2 - i;
        try {
            Vec3f h = this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f);
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            for (int i5 = i; i5 < i2; i5++) {
                vec3f.add(mo4906wI(i5), mo4905wH(i5));
                vec3f.scale(0.5f);
                h.add(vec3f);
            }
            h.scale(1.0f / ((float) i4));
            float b = C0647JL.m5594b(h, i3);
            int i6 = i;
            for (int i7 = i; i7 < i2; i7++) {
                vec3f.add(mo4906wI(i7), mo4905wH(i7));
                vec3f.scale(0.5f);
                if (C0647JL.m5594b(vec3f, i3) > b) {
                    mo4893ap(i7, i6);
                    i6++;
                }
            }
            int i8 = i4 / 3;
            if (i6 <= i + i8 || i6 >= (i2 + -1) - i8) {
                i6 = i + (i4 >> 1);
            }
            if (i6 == i || i6 == i2) {
                z = true;
            } else {
                z = false;
            }
            if (f1388jR || !z) {
                return i6;
            }
            throw new AssertionError();
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: at */
    public int mo4897at(int i, int i2) {
        this.stack.bcH().push();
        try {
            Vec3f h = this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f);
            Vec3f h2 = this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f);
            int i3 = i2 - i;
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            for (int i4 = i; i4 < i2; i4++) {
                vec3f.add(mo4906wI(i4), mo4905wH(i4));
                vec3f.scale(0.5f);
                h.add(vec3f);
            }
            h.scale(1.0f / ((float) i3));
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            while (i < i2) {
                vec3f.add(mo4906wI(i), mo4905wH(i));
                vec3f.scale(0.5f);
                vec3f2.sub(vec3f, h);
                C0647JL.m5603g(vec3f2, vec3f2, vec3f2);
                h2.add(vec3f2);
                i++;
            }
            h2.scale(1.0f / (((float) i3) - 1.0f));
            return C0647JL.m5588W(h2);
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a */
    public void mo4884a(aLG alg, Vec3f vec3f, Vec3f vec3f2) {
        if (this.hbT) {
            mo4883a(this.hbR, 0, alg, mo4899bd(vec3f), mo4899bd(vec3f2));
            return;
        }
        mo4898b(alg, vec3f, vec3f2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo4898b(aLG alg, Vec3f vec3f, Vec3f vec3f2) {
        if (f1388jR || !this.hbT) {
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            while (i2 < this.hbS) {
                if (f1388jR || i < this.hbS) {
                    int i4 = i + 1;
                    C1080Po po = this.hbP.get(i3);
                    boolean j = aRO.m17758j(vec3f, vec3f2, po.dPX, po.dPY);
                    boolean z = po.dPZ == -1;
                    if (z && j) {
                        alg.mo9823f(po.dQa, po.dQb);
                    }
                    if (j || z) {
                        i3++;
                        i2++;
                        i = i4;
                    } else {
                        int i5 = this.hbP.get(i3).dPZ;
                        i3 += i5;
                        i2 += i5;
                        i = i4;
                    }
                } else {
                    throw new AssertionError();
                }
            }
            if (hbL < i) {
                hbL = i;
                return;
            }
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo4883a(C5317aEr aer, int i, aLG alg, long j, long j2) {
        if (f1388jR || this.hbT) {
            boolean a = mo4891a(j, j2, aer.mo8684xE(i), aer.mo8685xF(i));
            boolean xH = aer.mo8687xH(i);
            if (!a) {
                return;
            }
            if (xH) {
                alg.mo9823f(aer.mo8690xK(i), aer.mo8689xJ(i));
                return;
            }
            int i2 = i + 1;
            mo4883a(aer, i2, alg, j, j2);
            mo4883a(aer, aer.mo8687xH(i2) ? i2 + 1 : i2 + aer.mo8688xI(i2), alg, j, j2);
            return;
        }
        throw new AssertionError();
    }

    /* renamed from: c */
    public void mo4902c(aLG alg, Vec3f vec3f, Vec3f vec3f2) {
        this.stack = this.stack.bcD();
        this.stack.bcH().push();
        try {
            if (this.hbT && this.hbX == C1477Vk.TRAVERSAL_STACKLESS) {
                throw new UnsupportedOperationException();
            }
            Vec3f ac = this.stack.bcH().mo4458ac(vec3f);
            Vec3f ac2 = this.stack.bcH().mo4458ac(vec3f);
            C0647JL.m5605o(ac, vec3f2);
            C0647JL.m5606p(ac2, vec3f2);
            mo4884a(alg, ac, ac2);
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a */
    public void mo4885a(aLG alg, Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
        this.stack = this.stack.bcD();
        this.stack.bcH().push();
        try {
            if (this.hbT && this.hbX == C1477Vk.TRAVERSAL_STACKLESS) {
                throw new UnsupportedOperationException();
            }
            Vec3f ac = this.stack.bcH().mo4458ac(vec3f);
            Vec3f ac2 = this.stack.bcH().mo4458ac(vec3f);
            C0647JL.m5605o(ac, vec3f2);
            C0647JL.m5606p(ac2, vec3f2);
            ac.add(vec3f3);
            ac2.add(vec3f4);
            mo4884a(alg, ac, ac2);
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: bd */
    public long mo4899bd(Vec3f vec3f) {
        if (f1388jR || this.hbT) {
            this.stack = this.stack.bcD();
            this.stack.bcH().push();
            try {
                Vec3f ac = this.stack.bcH().mo4458ac(vec3f);
                C0647JL.m5606p(ac, this.hbU);
                C0647JL.m5605o(ac, this.hbV);
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                vec3f2.sub(ac, this.hbU);
                C0647JL.m5603g(vec3f2, vec3f2, this.hbW);
                int i = ((int) (vec3f2.x + 0.5f)) & 65535;
                long j = ((long) (((int) (vec3f2.y + 0.5f)) & 65535)) << 16;
                long j2 = (((long) (((int) (vec3f2.z + 0.5f)) & 65535)) << 32) | j | ((long) i);
                return j2;
            } finally {
                this.stack.bcH().pop();
            }
        } else {
            throw new AssertionError();
        }
    }

    /* renamed from: a */
    public void mo4890a(Vec3f vec3f, long j) {
        vec3f.x = ((float) ((int) (65535 & j))) / this.hbW.x;
        vec3f.y = ((float) ((int) ((4294901760L & j) >>> 16))) / this.hbW.y;
        vec3f.z = ((float) ((int) ((281470681743360L & j) >>> 32))) / this.hbW.z;
        vec3f.add(this.hbU);
    }

    /* renamed from: a.Pw$a */
    private static class C1090a implements C1245SP {
        private final Vec3f cam = new Vec3f();
        private final Vec3f can = new Vec3f();
        public List<C1080Po> dQC;

        public C1090a(List<C1080Po> list) {
            this.dQC = list;
        }

        /* renamed from: b */
        public void mo819b(Vec3f[] vec3fArr, int i, int i2) {
            C1080Po po = new C1080Po();
            this.cam.set(1.0E30f, 1.0E30f, 1.0E30f);
            this.can.set(-1.0E30f, -1.0E30f, -1.0E30f);
            C0647JL.m5605o(this.cam, vec3fArr[0]);
            C0647JL.m5606p(this.can, vec3fArr[0]);
            C0647JL.m5605o(this.cam, vec3fArr[1]);
            C0647JL.m5606p(this.can, vec3fArr[1]);
            C0647JL.m5605o(this.cam, vec3fArr[2]);
            C0647JL.m5606p(this.can, vec3fArr[2]);
            po.dPX.set(this.cam);
            po.dPY.set(this.can);
            po.dPZ = -1;
            po.dQa = i;
            po.dQb = i2;
            this.dQC.add(po);
        }
    }

    /* renamed from: a.Pw$b */
    /* compiled from: a */
    private static class C1091b implements C1245SP {

        /* renamed from: jR */
        static final /* synthetic */ boolean f1390jR = (!C1089Pw.class.desiredAssertionStatus());
        public C5317aEr hDg;
        public C1089Pw hDh;
        public C0763Kt stack = C0763Kt.bcE();

        public C1091b(C5317aEr aer, C1089Pw pw) {
            this.hDg = aer;
            this.hDh = pw;
        }

        /* renamed from: b */
        public void mo819b(Vec3f[] vec3fArr, int i, int i2) {
            if (!f1390jR && i >= 1024) {
                throw new AssertionError();
            } else if (!f1390jR && i2 >= 2097152) {
                throw new AssertionError();
            } else if (f1390jR || i2 >= 0) {
                this.stack = this.stack.bcD();
                this.stack.bcH().push();
                try {
                    int cXy = this.hDg.cXy();
                    Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                    Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                    vec3f.set(1.0E30f, 1.0E30f, 1.0E30f);
                    vec3f2.set(-1.0E30f, -1.0E30f, -1.0E30f);
                    C0647JL.m5605o(vec3f, vec3fArr[0]);
                    C0647JL.m5606p(vec3f2, vec3fArr[0]);
                    C0647JL.m5605o(vec3f, vec3fArr[1]);
                    C0647JL.m5606p(vec3f2, vec3fArr[1]);
                    C0647JL.m5605o(vec3f, vec3fArr[2]);
                    C0647JL.m5606p(vec3f2, vec3fArr[2]);
                    if (vec3f2.x - vec3f.x < 0.002f) {
                        vec3f2.x += 0.001f;
                        vec3f.x -= 0.001f;
                    }
                    if (vec3f2.y - vec3f.y < 0.002f) {
                        vec3f2.y += 0.001f;
                        vec3f.y -= 0.001f;
                    }
                    if (vec3f2.z - vec3f.z < 0.002f) {
                        vec3f2.z += 0.001f;
                        vec3f.z -= 0.001f;
                    }
                    this.hDg.mo8679i(cXy, this.hDh.mo4899bd(vec3f));
                    this.hDg.mo8680j(cXy, this.hDh.mo4899bd(vec3f2));
                    this.hDg.mo8674aB(cXy, (i << 21) | i2);
                } finally {
                    this.stack.bcH().pop();
                }
            } else {
                throw new AssertionError();
            }
        }
    }
}
