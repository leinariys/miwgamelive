package p001a;

import com.hoplon.geometry.Vec3f;
import logic.abc.C0802Lc;

/* renamed from: a.acZ  reason: case insensitive filesystem */
/* compiled from: a */
public class C5897acZ {
    public final C3978xf fgC;
    public final Vec3f fgE;
    public float dMd;
    public float dMe;
    public C6863avD fgB;
    public C0802Lc fgD;
    public float fgF;
    public float fgG;
    public float fgH;
    public float fgI;
    public boolean fgJ;
    public float fgK;
    public float fgL;
    public float fgM;
    public float fgN;
    public float mass;

    public C5897acZ(float f, C6863avD avd, C0802Lc lc) {
        this(f, avd, lc, C0128Bb.dMU);
    }

    public C5897acZ(float f, C6863avD avd, C0802Lc lc, Vec3f vec3f) {
        this.fgC = new C3978xf();
        this.fgE = new Vec3f();
        this.fgF = 0.0f;
        this.fgG = 0.0f;
        this.dMd = 0.5f;
        this.dMe = 0.0f;
        this.fgH = 0.8f;
        this.fgI = 1.0f;
        this.fgJ = false;
        this.fgK = 0.005f;
        this.fgL = 0.01f;
        this.fgM = 0.01f;
        this.fgN = 0.01f;
        this.mass = f;
        this.fgB = avd;
        this.fgD = lc;
        this.fgE.set(vec3f);
        this.fgC.setIdentity();
    }
}
