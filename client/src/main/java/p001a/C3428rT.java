package p001a;

import javax.swing.*;

/* renamed from: a.rT */
/* compiled from: a */
public abstract class C3428rT<T> extends C6124ags<T> {
    /* renamed from: b */
    public abstract void mo321b(T t);

    /* renamed from: k */
    public final boolean updateInfo(T t) {
        SwingUtilities.invokeLater(new C3429a(t));
        return true;
    }

    /* renamed from: a.rT$a */
    class C3429a implements Runnable {
        private final /* synthetic */ Object beo;

        C3429a(Object obj) {
            this.beo = obj;
        }

        public void run() {
            C3428rT.this.mo321b(this.beo);
        }
    }
}
