package p001a;

import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.item.aUR;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/* renamed from: a.cE */
/* compiled from: a */
public abstract class C2122cE implements TableCellRenderer {
    /* renamed from: a */
    public abstract Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2);

    public Component getTableCellRendererComponent(JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
        Component a = mo17543a((aUR) jTable.getClientProperty(aUR.propertyKey), jTable, obj, z, z2, i, i2);
        IComponentManager e = ComponentManager.getCssHolder(a);
        if (e != null) {
            int i3 = 0;
            if (z) {
                i3 = 2;
            }
            if (z2) {
                i3 |= 128;
            }
            e.mo13063q(130, i3);
        }
        return a;
    }
}
