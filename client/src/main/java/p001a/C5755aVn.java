package p001a;

/* renamed from: a.aVn  reason: case insensitive filesystem */
/* compiled from: a */
public class C5755aVn {
    private long swigCPtr;

    public C5755aVn(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5755aVn() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m19091a(C5755aVn avn) {
        if (avn == null) {
            return 0;
        }
        return avn.swigCPtr;
    }
}
