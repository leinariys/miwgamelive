package p001a;

/* renamed from: a.ajm  reason: case insensitive filesystem */
/* compiled from: a */
public class C6274ajm {
    final Object fQq;
    final Object object;
    final String title;

    public C6274ajm(Object obj, String str, Object obj2) {
        this.fQq = obj;
        this.title = str;
        this.object = obj2;
    }

    /* renamed from: a */
    public boolean mo14175a(C6274ajm ajm) {
        if (this.fQq == null || ajm == null) {
            return false;
        }
        return this.fQq.equals(ajm.fQq);
    }
}
