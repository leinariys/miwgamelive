package p001a;

import game.script.item.ItemTypeCategory;

import javax.swing.tree.DefaultMutableTreeNode;

/* renamed from: a.fg */
/* compiled from: a */
public class C2482fg extends DefaultMutableTreeNode {
    public C2482fg(Object obj) {
        super(obj);
    }

    public String toString() {
        return ((ItemTypeCategory) getUserObject()).mo12246ke().get();
    }
}
