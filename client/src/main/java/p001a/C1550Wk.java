package p001a;

/* renamed from: a.Wk */
/* compiled from: a */
public class C1550Wk {
    /* renamed from: oV */
    public static int m11310oV(int i) {
        int i2 = 1;
        int i3 = 0;
        if (i < 0) {
            throw new IllegalArgumentException();
        }
        while (i2 < i) {
            i2 *= 2;
            i3++;
        }
        return i3;
    }

    /* renamed from: oW */
    public static int m11311oW(int i) {
        int i2 = 1;
        if (i < 0) {
            throw new IllegalArgumentException();
        }
        while (i2 < i) {
            i2 *= 2;
        }
        return i2;
    }
}
