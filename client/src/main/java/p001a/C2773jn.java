package p001a;

import java.util.HashMap;
import java.util.Map;

/* renamed from: a.jn */
/* compiled from: a */
public class C2773jn {

    /* renamed from: OC */
    public static final String f8342OC = "UNDEFINED";
    public static final int aiU = 0;
    public static final int aiV = 1;
    /* renamed from: Ow */
    public static Map<Integer, String> f8343Ow = new HashMap();
    /* renamed from: Ox */
    public static Map<String, Integer> f8344Ox = new HashMap();

    static {
        f8343Ow.put(0, "MOUSE_X");
        f8343Ow.put(1, "MOUSE_Y");
        f8344Ox.put("MOUSE_X", 0);
        f8344Ox.put("MOUSE_Y", 1);
    }

    /* renamed from: ao */
    public static int m34193ao(String str) {
        Integer num = f8344Ox.get(str);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    /* renamed from: bf */
    public static String m34194bf(int i) {
        String str = f8343Ow.get(Integer.valueOf(i));
        return str != null ? str : "UNDEFINED";
    }
}
