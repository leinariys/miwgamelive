package p001a;

/* renamed from: a.uX */
/* compiled from: a */
public class C3750uX extends C5445aJp {
    long start = -1;

    public void start() {
        this.start = ako();
    }

    public void end() {
        set(Math.round(((double) (ako() - this.start)) / 1000000.0d));
    }

    public void restart() {
        if (this.start != -1) {
            end();
        }
        start();
    }

    private long ako() {
        return System.nanoTime();
    }
}
