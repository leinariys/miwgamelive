package p001a;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

/* renamed from: a.rk */
/* compiled from: a */
public class C3507rk extends URLStreamHandler {
    public static String ban = "remoteloader";
    public int len = (ban.length() + 1);

    /* access modifiers changed from: protected */
    public URLConnection openConnection(URL url) {
        String[] split = url.toString().substring(this.len).split(":");
        return new C3508a(url, C6934awW.m26882kA(split[0]), split[1]);
    }

    /* renamed from: a.rk$a */
    class C3508a extends URLConnection {
        private final /* synthetic */ C6934awW dvh;
        private final /* synthetic */ String dvi;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C3508a(URL url, C6934awW aww, String str) {
            super(url);
            this.dvh = aww;
            this.dvi = str;
        }

        public void connect() {
        }

        public InputStream getInputStream() {
            return this.dvh.getResourceAsStream(this.dvi);
        }
    }
}
