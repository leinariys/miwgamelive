package p001a;

import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: a.jr */
/* compiled from: a */
public class C2777jr implements aLA {
    /* access modifiers changed from: private */
    public ArrayList<C3596sj> idk = new ArrayList<>();
    /* access modifiers changed from: private */
    public C3596sj idl = null;
    /* access modifiers changed from: private */
    public Object idm = new Object();

    /* renamed from: a */
    public C3596sj mo9816a(C6217aih aih) {
        return new C2778a(aih);
    }

    /* renamed from: a.jr$a */
    class C2778a implements C3596sj {
        private final /* synthetic */ C6217aih akk;

        C2778a(C6217aih aih) {
            this.akk = aih;
        }

        /* renamed from: dn */
        public void mo17084dn() {
            if (!C2777jr.this.idk.contains(this)) {
                if (C2777jr.this.idl != null) {
                    C2777jr.this.idl.unregister();
                }
                C2777jr.this.idk.add(this);
            }
        }

        /* renamed from: dm */
        public void mo17083dm() {
            synchronized (C2777jr.this.idm) {
                while (C2777jr.this.idl != null) {
                    try {
                        C2777jr.this.idm.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                C2777jr.this.idl = this;
                Iterator it = C2777jr.this.idk.iterator();
                while (it.hasNext()) {
                    ((C3596sj) it.next()).unregister();
                }
                C2777jr.this.idk.clear();
            }
        }

        public void unregister() {
            if (C2777jr.this.idk.contains(this)) {
                this.akk.mo13778dl();
            } else if (C2777jr.this.idl == this) {
                synchronized (C2777jr.this.idm) {
                    this.akk.mo13777dk();
                    C2777jr.this.idl = null;
                    C2777jr.this.idm.notify();
                }
            }
        }
    }
}
