package p001a;

import game.engine.DataGameEvent;
import game.network.message.IReadProto;
import game.network.message.serializable.C2436fS_1;

/* renamed from: a.aPZ */
/* compiled from: a */
public class aPZ extends C2436fS_1 {

    private final DataGameEvent aGA;
    private int aGC;
    private boolean aVO;
    private boolean iDz;

    public aPZ(DataGameEvent jz, IReadProto ie, boolean z) {
        super(ie);
        this.aGC = 255;
        this.aVO = true;
        mo18641E(true);
        this.aGA = jz;
    }

    public aPZ(DataGameEvent jz, IReadProto ie, boolean z, boolean z2) {
        this(jz, ie, z);
        this.aVO = z2;
    }

    public aPZ(DataGameEvent jz, IReadProto ie, boolean z, int i) {
        this(jz, ie, z);
        this.aGC = i;
    }

    /* renamed from: PM */
    public DataGameEvent mo10811PM() {
        return this.aGA;
    }

    /* renamed from: PN */
    public int mo10812PN() {
        return this.aGC;
    }

    /* renamed from: Xd */
    public boolean mo10813Xd() {
        return this.aVO;
    }

    /* renamed from: hd */
    public <T> T mo6366hd(String str) {
        return super.mo6366hd(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C2436fS_1.C2437a mo10814a(C2436fS_1.C2438b bVar, C2436fS_1.C2439c cVar) {
        if (bVar.cnu() == null) {
            long gY = readLong("oid");
            try {
                Object b = this.aGA.mo3365b((Class<?>) (Class) mo6366hd("class"), gY);
                bVar.mo18687w(b);
                bVar.setType(130);
                mo6370pe();
                mo18681t(b);
            } catch (Exception e) {
                throw new CountFailedReadOrWriteException("Error reading game.script " + gY, e);
            }
        }
        return bVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: A */
    public C2436fS_1.C2437a mo7913A(String str, int i) {
        if (!dpa()) {
            return super.mo7913A(str, i);
        }
        if (i == 134) {
            return null;
        }
        return mo18653d(i, str);
    }

    public boolean dpa() {
        return this.iDz;
    }

    /* renamed from: jY */
    public void mo10816jY(boolean z) {
        this.iDz = z;
    }
}
