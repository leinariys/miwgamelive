package p001a;

import logic.thred.LogPrinter;
import taikodom.render.graphics2d.C0559Hm;
import taikodom.render.loader.provider.C0399FY;
import taikodom.render.loader.provider.FilePath;

import java.io.*;
import java.nio.ByteBuffer;

/* renamed from: a.GB */
/* compiled from: a */
public class C0434GB implements FilePath {
    static LogPrinter logger = LogPrinter.m10275K(C0434GB.class);
    public FilePath bjw;
    public FilePath cZk;

    public C0434GB(FilePath ain, FilePath ain2) {
        this.bjw = ain2;
        this.cZk = ain;
    }

    public boolean delete() {
        throw new UnsupportedOperationException("not implemented yet");
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public boolean mo2263e(FilePath ain) {
        try {
            for (FilePath ain2 : ain.mo2250BC()) {
                if (!ain2.isDir()) {
                    ain2.delete();
                } else if (ain2.mo2250BC().length > 0) {
                    mo2263e(ain2);
                } else {
                    ain2.delete();
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean exists() {
        if (this.cZk.exists()) {
            return true;
        }
        return this.bjw.exists();
    }

    /* renamed from: a */
    public boolean mo2257a(C0434GB gb) {
        return this.bjw.concat(gb.getPath()).exists();
    }

    /* renamed from: aC */
    public FilePath concat(String str) {
        return new C0434GB(this.cZk.concat(str), this.bjw.concat(str));
    }

    /* renamed from: BF */
    public FilePath mo2253BF() {
        return this.cZk.mo2253BF();
    }

    /* renamed from: BG */
    public File getFile() {
        if (!this.cZk.exists()) {
            try {
                aRi();
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
        return this.cZk.getFile();
    }

    public String getName() {
        return this.cZk.getName();
    }

    /* renamed from: BB */
    public FilePath mo2249BB() {
        return new C0434GB(this.cZk.mo2249BB(), this.bjw.mo2249BB());
    }

    public String getPath() {
        return this.cZk.getPath();
    }

    public boolean isDir() {
        return this.cZk.isDir() || this.bjw.isDir();
    }

    public long lastModified() {
        return this.cZk.lastModified();
    }

    public long length() {
        return this.cZk.length();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: a.GB[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: BC */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public FilePath[] mo2250BC() {
        /*
            r4 = this;
            a.ain r0 = r4.bjw
            a.ain[] r1 = r0.mo2250BC()
            int r0 = r1.length
            a.GB[] r2 = new p001a.C0434GB[r0]
            r0 = 0
        L_0x000a:
            int r3 = r1.length
            if (r0 < r3) goto L_0x000e
            return r2
        L_0x000e:
            r3 = r1[r0]
            java.lang.String r3 = r3.getName()
            a.ain r3 = r4.mo2259aC(r3)
            r2[r0] = r3
            int r0 = r0 + 1
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C0434GB.mo2250BC():a.ain[]");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: a.GB[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public FilePath[] mo2258a(C0399FY r5) {
        /*
            r4 = this;
            a.ain r0 = r4.bjw
            a.ain[] r1 = r0.mo2258a(r5)
            int r0 = r1.length
            a.GB[] r2 = new p001a.C0434GB[r0]
            r0 = 0
        L_0x000a:
            int r3 = r1.length
            if (r0 < r3) goto L_0x000e
            return r2
        L_0x000e:
            r3 = r1[r0]
            java.lang.String r3 = r3.getName()
            a.ain r3 = r4.mo2259aC(r3)
            r2[r0] = r3
            int r0 = r0 + 1
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C0434GB.mo2258a(a.FY):a.ain[]");
    }

    /* renamed from: BD */
    public void mo2251BD() {
        this.cZk.mo2251BD();
    }

    public InputStream openInputStream() throws FileNotFoundException {
        if (this.cZk.exists() || !this.bjw.isDir()) {
            aRi();
            return this.cZk.openInputStream();
        }
        throw new FileNotFoundException("Cannot open the input stream of a directory");
    }

    private void aRi() throws FileNotFoundException {
        if (!this.cZk.exists()) {
            logger.info("getting from source: " + this.cZk.getPath());
            InputStream openInputStream = this.bjw.openInputStream();
            OutputStream openOutputStream = this.cZk.openOutputStream();
            try {
                C0559Hm.copyStream(openInputStream, openOutputStream);
                openInputStream.close();
                openOutputStream.flush();
                openOutputStream.close();
                openInputStream.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public OutputStream openOutputStream() {
        return this.cZk.openOutputStream();
    }

    /* renamed from: BE */
    public PrintWriter mo2252BE() {
        return this.cZk.mo2252BE();
    }

    /* renamed from: BH */
    public byte[] mo2255BH() {
        if (!this.cZk.exists()) {
            try {
                aRi();
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
        return this.cZk.mo2255BH();
    }

    /* renamed from: d */
    public boolean mo2261d(FilePath ain) {
        return this.cZk.mo2261d(ain);
    }

    /* renamed from: BI */
    public ByteBuffer mo2256BI() {
        return this.cZk.mo2256BI();
    }

    /* renamed from: d */
    public void mo2260d(byte[] bArr) {
        throw new UnsupportedOperationException();
    }
}
