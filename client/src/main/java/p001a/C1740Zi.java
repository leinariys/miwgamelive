package p001a;

import logic.res.scene.C2036bO;
import logic.res.scene.C6791atj;

/* renamed from: a.Zi */
/* compiled from: a */
public interface C1740Zi<Parameter> {
    /* renamed from: a */
    boolean mo7456a(C2036bO bOVar, Parameter parameter);

    /* renamed from: c */
    Parameter mo7457c(C6791atj atj);

    /* renamed from: j */
    Parameter mo7458j(C2036bO bOVar);
}
