package p001a;

/* renamed from: a.aAh  reason: case insensitive filesystem */
/* compiled from: a */
public class C5203aAh {
    private long swigCPtr;

    public C5203aAh(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5203aAh() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m12618a(C5203aAh aah) {
        if (aah == null) {
            return 0;
        }
        return aah.swigCPtr;
    }
}
