package p001a;

import logic.res.scene.C3087nb;

import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: a.JV */
/* compiled from: a */
public class C0667JV {
    private C3087nb gfa;
    private ArrayList<C0668a> gfb;
    private boolean logging;

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo3123b(int i, Object[] objArr) {
        if (this.gfb == null) {
            this.gfb = new ArrayList<>();
        }
        this.gfb.add(new C0668a(i, objArr));
    }

    /* access modifiers changed from: protected */
    public boolean isLogging() {
        return this.logging;
    }

    /* access modifiers changed from: protected */
    public void setLogging(boolean z) {
        this.logging = z;
    }

    /* access modifiers changed from: protected */
    public C3087nb clW() {
        return this.gfa;
    }

    /* renamed from: c */
    public final void mo3125c(C3087nb nbVar) {
        this.gfa = nbVar;
    }

    /* renamed from: fj */
    public final void mo3127fj(boolean z) {
        Iterator<C0668a> it = this.gfb.iterator();
        while (it.hasNext()) {
            C0668a next = it.next();
            try {
                mo3124c(next.dmY, next.dmZ);
            } catch (RuntimeException e) {
                if (!z) {
                    throw e;
                }
                e.printStackTrace();
            }
        }
        this.gfb.clear();
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo3124c(int i, Object[] objArr) {
        throw new RuntimeException("Unknow function " + i);
    }

    /* renamed from: a.JV$a */
    static class C0668a {
        /* access modifiers changed from: private */
        public final int dmY;
        /* access modifiers changed from: private */
        public final Object[] dmZ;

        public C0668a(int i, Object[] objArr) {
            this.dmY = i;
            this.dmZ = objArr;
        }
    }
}
