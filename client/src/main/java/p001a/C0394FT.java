package p001a;

/* renamed from: a.FT */
/* compiled from: a */
public class C0394FT {
    private final float cXa;
    private int cXb;
    private long cXc;
    private float fps;

    public C0394FT() {
        this.cXb = 0;
        this.cXc = 0;
        this.fps = 0.0f;
        this.cXa = 1.0E9f;
    }

    public C0394FT(long j) {
        this.cXb = 0;
        this.cXc = 0;
        this.fps = 0.0f;
        this.cXa = ((float) j) * 1000000.0f;
    }

    public float aQw() {
        return this.fps;
    }

    public void update() {
        long nanoTime = System.nanoTime();
        this.cXb++;
        if (((float) (nanoTime - this.cXc)) > this.cXa) {
            this.fps = (((float) this.cXb) * this.cXa) / ((float) (nanoTime - this.cXc));
            this.cXb = 0;
            this.cXc = nanoTime;
        }
    }
}
