package p001a;

import taikodom.infra.script.ai.behaviours.AimQuality;

/* renamed from: a.mQ */
/* compiled from: a */
public class C2971mQ extends C2355eP {
    private static final float aFI = 0.0f;
    private static final float aFJ = 5.0f;
    private static final float aFK = 50.0f;
    private static final float aFL = 750.0f;
    private static final float aFM = 0.5f;
    private static final float aFN = 2.0f;
    private static final float aFO = 0.5f;
    private static final float aFP = 2.0f;
    private static final float aFQ = 0.5f;
    private static final float aFR = 2.0f;
    private static final long serialVersionUID = -7631666257351133263L;

    public C2971mQ(C2971mQ mQVar) {
        mo18003Ph();
        for (int i = 0; i < this.emI.length; i++) {
            this.emI[i].set(mQVar.emI[i].get());
        }
    }

    public C2971mQ() {
        mo18003Ph();
        this.emI[0].set(AimQuality.FIRE_NEAR_TARGET);
        this.emI[1].set(Float.valueOf(1.0f));
        this.emI[2].set(Float.valueOf(200.0f));
        this.emI[3].set(Float.valueOf(1.0f));
        this.emI[4].set(Float.valueOf(1.0f));
        this.emI[5].set(Float.valueOf(1.0f));
    }

    /* access modifiers changed from: protected */
    /* renamed from: Ph */
    public void mo18003Ph() {
        this.emI = new C2355eP.C2356a[6];
        this.emI[0] = new C2355eP.C2356a(AimQuality.values());
        this.emI[1] = new C2355eP.C2356a(Float.valueOf(aFI), Float.valueOf(5.0f));
        this.emI[2] = new C2355eP.C2356a(Float.valueOf(aFK), Float.valueOf(aFL));
        this.emI[3] = new C2355eP.C2356a(Float.valueOf(0.5f), Float.valueOf(2.0f));
        this.emI[4] = new C2355eP.C2356a(Float.valueOf(0.5f), Float.valueOf(2.0f));
        this.emI[5] = new C2355eP.C2356a(Float.valueOf(0.5f), Float.valueOf(2.0f));
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("\n**************************************************\n");
        stringBuffer.append("Drone\n\n");
        stringBuffer.append("AimQuality: " + this.emI[0].get().toString() + "\n");
        stringBuffer.append("ReactionTime: " + this.emI[1].get() + "  PursuitDistance: " + this.emI[2].get() + "\n");
        stringBuffer.append("Gains -> Speed: " + this.emI[3].get() + "  Yaw: " + this.emI[4].get() + "  Pitch: " + this.emI[5].get() + "\n");
        stringBuffer.append("**************************************************\n");
        return stringBuffer.toString();
    }

    /* renamed from: a */
    public C2971mQ[] mo18004b(C2355eP ePVar) {
        if (!(ePVar instanceof C2971mQ)) {
            return null;
        }
        C2971mQ mQVar = (C2971mQ) ePVar;
        C2971mQ[] mQVarArr = {new C2971mQ(), new C2971mQ()};
        for (int i = 0; i < this.emI.length; i++) {
            if (emH.nextBoolean()) {
                mQVarArr[0].emI[i].set(this.emI[i].get());
                mQVarArr[1].emI[i].set(mQVar.emI[i].get());
            } else {
                mQVarArr[1].emI[i].set(this.emI[i].get());
                mQVarArr[0].emI[i].set(mQVar.emI[i].get());
            }
        }
        return mQVarArr;
    }

    /* renamed from: Pi */
    public AimQuality mo20530Pi() {
        return (AimQuality) this.emI[0].get();
    }

    /* renamed from: By */
    public float mo20529By() {
        return ((Float) this.emI[1].get()).floatValue();
    }

    /* renamed from: Pj */
    public float mo20531Pj() {
        return ((Float) this.emI[2].get()).floatValue();
    }

    /* renamed from: Pk */
    public float mo20532Pk() {
        return ((Float) this.emI[3].get()).floatValue();
    }

    /* renamed from: Pl */
    public float mo20533Pl() {
        return ((Float) this.emI[4].get()).floatValue();
    }

    /* renamed from: Pm */
    public float mo20534Pm() {
        return ((Float) this.emI[5].get()).floatValue();
    }

    /* renamed from: a */
    public void mo20535a(AimQuality aimQuality) {
        this.emI[0].set(aimQuality);
    }

    /* renamed from: aK */
    public void mo20537aK(float f) {
        this.emI[1].set(Float.valueOf(f));
    }

    /* renamed from: co */
    public void mo20538co(float f) {
        this.emI[2].set(Float.valueOf(f));
    }

    /* renamed from: cp */
    public void mo20539cp(float f) {
        this.emI[3].set(Float.valueOf(f));
    }

    /* renamed from: cq */
    public void mo20540cq(float f) {
        this.emI[4].set(Float.valueOf(f));
    }

    /* renamed from: cr */
    public void mo20541cr(float f) {
        this.emI[5].set(Float.valueOf(f));
    }
}
