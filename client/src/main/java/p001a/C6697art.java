package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.script.space.Asteroid;
import logic.thred.PoolThread;

/* renamed from: a.art  reason: case insensitive filesystem */
/* compiled from: a */
public class C6697art extends ayA {
    public static int WAIT_TIME = 10000;

    public C6697art(C1285Sv sv) {
        super(sv);
    }

    /* access modifiers changed from: protected */
    public void aGu() {
        if (this.state == 0) {
            this.runs++;
            mo16918a("Docked", C1285Sv.C1286a.Docked);
            mo16931r("State:" + this.state + " Init", true);
            mo16928jh((long) (WAIT_TIME + ((int) (((double) WAIT_TIME) * Math.random()))));
            this.state++;
        } else if (this.state == 1) {
            mo16931r("State:" + this.state + " Undock: " + mo5504eT().getName(), true);
            if (mo5495al().isDead()) {
                btE();
            }
            btJ().mo16115bS(mo5503dL());
            btG().mo19541a(C2675iR.C2676a.HiSpeedStates);
            btG().mo19543hg(C1285Sv.btK());
            mo16917a(btG());
            mo16918a("FlyTo1", C1285Sv.C1286a.LongFlight);
            this.state++;
        } else if (this.state == 2) {
            mo16931r("State:" + this.state + " Mining asteroid 1", true);
            csg();
            mo16917a(btI());
            mo16918a("Mine1", C1285Sv.C1286a.Minning);
            this.state++;
        } else if (this.state == 3) {
            mo16931r("State:" + this.state, true);
            btG().mo19541a(C2675iR.C2676a.FastMovementsStates);
            btG().mo19543hg(C1285Sv.btM());
            mo16917a(btG());
            mo16918a("FlyTo2", C1285Sv.C1286a.ShortFlight);
            this.state++;
        } else if (this.state == 4) {
            mo16931r("State:" + this.state + " Mining asteroid 2", true);
            csg();
            mo16917a(btI());
            mo16918a("Mine2", C1285Sv.C1286a.Minning);
            this.state++;
        } else if (this.state == 5) {
            mo16931r("State:" + this.state, true);
            btG().mo19541a(C2675iR.C2676a.FastMovementsStates);
            btG().mo19543hg(C1285Sv.btM());
            mo16917a(btG());
            mo16918a("FlyTo3", C1285Sv.C1286a.ShortFlight);
            this.state++;
        } else if (this.state == 6) {
            mo16931r("State:" + this.state + " Mining asteroid 3", true);
            csg();
            mo16917a(btI());
            mo16918a("Mine3", C1285Sv.C1286a.Minning);
            this.state++;
        } else if (this.state == 7) {
            mo16931r("State:" + this.state, true);
            btG().mo19541a(C2675iR.C2676a.HiSpeedStates);
            btG().mo19543hg(C1285Sv.btK());
            mo16917a(btG());
            mo16918a("FlyToSt", C1285Sv.C1286a.LongFlight);
            this.state++;
        } else if (this.state == 8) {
            mo16931r("State:" + this.state + " Dock", true);
            btJ().mo16116bU(mo5503dL());
            mo16918a("Docked", C1285Sv.C1286a.Docked);
            this.state = 0;
        }
    }

    private void csg() {
        Asteroid an = btJ().mo16108an(mo5504eT());
        if (an == null) {
            throw new IllegalStateException("Was not able to find an asteroid");
        }
        Vec3d position = an.getPosition();
        Vec3f vec3f = new Vec3f(btJ().mo16105aY(an) + btJ().mo16105aY(mo5495al()) + 100.0f, 0.0f, 0.0f);
        PoolThread.sleep(400);
        btJ().mo16112b(mo5495al(), position.mo9531q(vec3f));
        PoolThread.sleep(400);
        btJ().mo16124d(mo5495al(), position);
        PoolThread.sleep(400);
    }
}
