package p001a;

import logic.ui.item.Picture;
import org.mozilla1.classfile.C0147Bi;
import org.objectweb.asm.tree.AnnotationNode;
import org.objectweb.asm.tree.MethodNode;
import util.Syst;

/* renamed from: a.Wn */
/* compiled from: a */
public class C1554Wn extends ayO {
    private static /* synthetic */ int[] fQS;
    public final String cUW;
    public String cUQ;
    String fQP;
    private C1070Pe fQO;
    private MethodNode fQQ;
    private MethodVisitor fQR;

    public C1554Wn(C6416amY amy, int i, String str, String str2, String str3, String[] strArr) {
        super((MethodVisitor) null, amy, i, str, str2, str3, strArr);
        if ((i & 8) == 0) {
            this.fQR = amy.ckg().visitMethod(2, String.valueOf(str) + "_x_", str2, str3, strArr);
            MethodNode methodNode = new MethodNode(2, String.valueOf(str) + "_x_", str2, str3, strArr);
            this.fQQ = methodNode;
            this.mv = methodNode;
        } else {
            this.mv = new MethodNode(i, str, str2, str3, strArr);
            this.mv = amy.ckg().visitMethod(i, str, str2, str3, strArr);
        }
        this.cUW = Syst.getMd5(String.valueOf(amy.name.replace(C0147Bi.cla, '.')) + "#" + str + str2);
    }

    static /* synthetic */ int[] cej() {
        int[] iArr = fQS;
        if (iArr == null) {
            iArr = new int[C6380alo.C1937a.values().length];
            try {
                iArr[C6380alo.C1937a.CLIENT.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C6380alo.C1937a.GENERIC.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C6380alo.C1937a.SERVER.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            fQS = iArr;
        }
        return iArr;
    }

    public void visitCode() {
        super.visitCode();
        if ((this.access & 8) != 0 && C2821kb.arS.equals(this.name)) {
            this.mv.visitMethodInsn(184, this.cUO.name, C2821kb.arT, "()V");
        }
    }

    public void visitEnd() {
        if ((this.access & 8) != 0) {
            super.visitEnd();
            return;
        }
        AnnotationVisitor visitAnnotation = this.mv.visitAnnotation(C2821kb.aqu.getDescriptor(), true);
        visitAnnotation.visit("hash", this.cUW);
        visitAnnotation.visit("id", Integer.valueOf(this.index));
        visitAnnotation.visitEnd();
        this.cUQ = C2821kb.arP + this.name + "_0020" + C6423amf.m23961jq(this.desc);
        if ((this.access & 256) != 0) {
            if (this.cUY == null || !this.cUY.containsKey(C2821kb.aqB.getDescriptor())) {
                super.visitCode();
                this.cUO.mo14851e(this.mv);
                this.mv.visitTypeInsn(192, this.cUO.ckm());
                if (this.gVX != null) {
                    int i = 1;
                    for (int i2 = 0; i2 < this.gVX.length; i2++) {
                        visitVarInsn(this.gVX[i2].getOpcode(21), i);
                        i += this.gVX[i2].getSize();
                    }
                }
                this.mv.visitMethodInsn(185, this.cUO.ckm(), this.name, this.desc);
                this.mv.visitInsn(this.gVW.getOpcode(172));
                this.mv.visitMaxs(8, 4);
                super.visitEnd();
            } else {
                this.mv.visitCode();
                this.cUO.mo14851e(this.mv);
                this.fQO = new C1070Pe(String.valueOf(this.cUO.ckn()) + "$" + Syst.m15908t(this.name) + "Listener");
                this.mv.visitLdcInsn(this.fQO.getType());
                m11351d(this.mv);
                C2821kb.arz.mo20041a(this.mv, 185);
                this.mv.visitInsn(177);
                this.mv.visitMaxs(8, 4);
                this.mv.visitEnd();
                this.cUO.mo14844b(this);
            }
        }
        if ((this.access & 1024) != 0) {
            m11349b(this.mv);
        }
        if (this.fQQ != null) {
            boolean z = false;
            if (this.cUY != null) {
                switch (cej()[this.cUO.gci.blO().ordinal()]) {
                    case 1:
                        z = this.cUY.containsKey(C2821kb.aqz.getDescriptor());
                        break;
                    case 2:
                        z = this.cUY.containsKey(C2821kb.aqA.getDescriptor());
                        break;
                }
            }
            if (z) {
                for (AnnotationNode annotationNode : this.fQQ.visibleAnnotations) {
                    annotationNode.accept(this.fQR.visitAnnotation(annotationNode.desc, true));
                }
                m11349b(this.fQR);
            } else {
                this.fQQ.accept(this.fQR);
            }
        }
        ClassVisitor ckg = this.cUO.ckg();
        this.cUO.mo14843a(this);
        ckg.visitField(25, this.cUQ, C2821kb.aqI.getDescriptor(), (String) null, (Object) null).visitEnd();
        MethodVisitor visitMethod = ckg.visitMethod(this.access & -1281, this.name, this.desc, this.signature, this.gVY);
        this.fQQ.accept(new C1555a(visitMethod));
        Picture label = new Picture();
        Picture label2 = new Picture();
        Picture label3 = new Picture();
        Picture label4 = new Picture();
        Picture label5 = new Picture();
        Picture label6 = new Picture();
        Picture[] labelArr = new Picture[4];
        labelArr[1] = label2;
        labelArr[0] = label;
        labelArr[2] = label3;
        labelArr[3] = label4;
        visitMethod.visitCode();
        visitMethod.visitLabel(label5);
        this.cUO.mo14851e(visitMethod);
        visitMethod.visitFieldInsn(178, this.cUO.name, this.cUQ, C2821kb.aqI.getDescriptor());
        C2821kb.arc.mo20041a(visitMethod, 185);
        visitMethod.visitTableSwitchInsn(0, 3, label2, labelArr);
        visitMethod.visitLabel(label);
        if ("isDeleted".equals(this.name)) {
            visitMethod.visitInsn(4);
        } else {
            C1551Wl.m11316b(this.gVW, visitMethod);
        }
        visitMethod.visitInsn(this.gVW.getOpcode(172));
        visitMethod.visitLabel(label3);
        m11350c(visitMethod);
        if (!C1551Wl.m11315a(this.gVW, visitMethod)) {
            if (this.gVW.getSort() == 9) {
                visitMethod.visitTypeInsn(192, this.gVW.getDescriptor());
            } else if (this.gVW.getSort() != 0) {
                visitMethod.visitTypeInsn(192, this.gVW.getInternalName());
            }
        }
        visitMethod.visitInsn(this.gVW.getOpcode(172));
        visitMethod.visitLabel(label4);
        m11350c(visitMethod);
        visitMethod.visitInsn(87);
        visitMethod.visitLabel(label2);
        visitMethod.visitVarInsn(25, 0);
        int length = this.gVX.length;
        int i3 = 0;
        int i4 = 1;
        int i5 = 10;
        int i6 = 1;
        while (i3 < length) {
            int size = i6 + this.gVX[i3].getSize();
            visitMethod.visitVarInsn(this.gVX[i3].getOpcode(21), i4);
            i4 += this.gVX[i3].getSize();
            i3++;
            i5 = this.gVX[i3].getSize() + i5;
            i6 = size;
        }
        visitMethod.visitMethodInsn(183, this.cUO.name, this._name, this.desc);
        visitMethod.visitLabel(label6);
        visitMethod.visitInsn(this.gVW.getOpcode(172));
        visitMethod.visitLocalVariable("this", this.cUO.desc, (String) null, label5, label6, 0);
        int i7 = 1;
        for (int i8 = 0; i8 < length; i8++) {
            visitMethod.visitLocalVariable("arg" + i8, this.gVX[i8].getDescriptor(), (String) null, label5, label6, i7);
            i7 += this.gVX[i8].getSize();
        }
        visitMethod.visitMaxs(i5, i6);
        visitMethod.visitEnd();
        if ((this.access & 256) != 0 && this.cUY != null && this.cUY.containsKey(C2821kb.aqB.getDescriptor())) {
            this.fQP = C2821kb.asc + this.name;
            MethodVisitor visitMethod2 = ckg.visitMethod(2, this.fQP, C2821kb.asd, (String) null, (String[]) null);
            visitMethod2.visitCode();
            visitMethod2.visitVarInsn(25, 1);
            visitMethod2.visitMethodInsn(185, "java/util/Collection", "iterator", "()Ljava/util/Iterator;");
            visitMethod2.visitVarInsn(58, 3);
            Picture label7 = new Picture();
            visitMethod2.visitLabel(label7);
            visitMethod2.visitVarInsn(25, 3);
            visitMethod2.visitMethodInsn(185, "java/util/Iterator", "hasNext", "()Z");
            Picture label8 = new Picture();
            visitMethod2.visitJumpInsn(153, label8);
            visitMethod2.visitVarInsn(25, 3);
            visitMethod2.visitMethodInsn(185, "java/util/Iterator", "next", "()Ljava/lang/Object;");
            visitMethod2.visitTypeInsn(192, this.fQO.getInternalName());
            visitMethod2.visitVarInsn(25, 0);
            int i9 = 6;
            if (this.gVX != null && this.gVX.length > 0) {
                visitMethod2.visitVarInsn(25, 2);
                C2821kb.arA.mo20041a(visitMethod2, 185);
                visitMethod2.visitVarInsn(58, 4);
                i9 = 6 + Math.max(6, mo6627b(visitMethod2, 4));
            }
            visitMethod2.visitMethodInsn(185, this.fQO.getInternalName(), "on" + Syst.m15908t(this.name), "(L" + this.cUO.name + ";" + this.desc.substring(1, this.desc.lastIndexOf(41)) + ")V");
            visitMethod2.visitJumpInsn(167, label7);
            visitMethod2.visitLabel(label8);
            visitMethod2.visitInsn(177);
            visitMethod2.visitMaxs(i9, 5);
            visitMethod2.visitEnd();
        }
    }

    /* renamed from: b */
    private void m11349b(MethodVisitor methodVisitor) {
        methodVisitor.visitCode();
        methodVisitor.visitTypeInsn(187, C2821kb.arJ.getInternalName());
        methodVisitor.visitInsn(89);
        m11351d(methodVisitor);
        C2821kb.arK.mo5379a(methodVisitor);
        methodVisitor.visitInsn(191);
        methodVisitor.visitMaxs(6, (this.gVX.length << 1) + 1);
        methodVisitor.visitEnd();
    }

    /* renamed from: c */
    private void m11350c(MethodVisitor methodVisitor) {
        this.cUO.mo14851e(methodVisitor);
        m11351d(methodVisitor);
        C2821kb.arf.mo20041a(methodVisitor, 185);
    }

    /* renamed from: d */
    private void m11351d(MethodVisitor methodVisitor) {
        int length = this.gVX.length;
        methodVisitor.visitTypeInsn(187, C2821kb.aqP.getInternalName());
        methodVisitor.visitInsn(89);
        methodVisitor.visitVarInsn(25, 0);
        methodVisitor.visitTypeInsn(192, C2821kb.aqE.getInternalName());
        methodVisitor.visitFieldInsn(178, this.cUO.name, this.cUQ, C2821kb.aqI.getDescriptor());
        C6423amf.m23960c(methodVisitor, length);
        methodVisitor.visitTypeInsn(189, "java/lang/Object");
        int i = 1;
        for (int i2 = 0; i2 < length; i2++) {
            methodVisitor.visitInsn(89);
            C6423amf.m23960c(methodVisitor, i2);
            C1551Wl.m11313a(this.gVX[i2], methodVisitor, i);
            methodVisitor.visitInsn(83);
            i += this.gVX[i2].getSize();
        }
        C2821kb.arF.mo5379a(methodVisitor);
    }

    /* renamed from: b */
    public int mo6627b(MethodVisitor methodVisitor, int i) {
        int length = this.gVX.length;
        int i2 = 2;
        for (int i3 = 0; i3 < length; i3++) {
            methodVisitor.visitVarInsn(25, i);
            C6423amf.m23960c(methodVisitor, i3);
            methodVisitor.visitInsn(50);
            Type type = this.gVX[i3];
            i2 += type.getSize();
            if (!C1551Wl.m11315a(type, methodVisitor)) {
                if (type.getSort() == 9) {
                    methodVisitor.visitTypeInsn(192, type.getDescriptor());
                } else {
                    methodVisitor.visitTypeInsn(192, type.getInternalName());
                }
            }
        }
        return i2;
    }

    public String toString() {
        return String.valueOf(this.name) + this.desc;
    }

    public void visitMaxs(int i, int i2) {
        if (this.maxStack > i) {
            i = this.maxStack;
        }
        if (this.maxLocals > i2) {
            i2 = this.maxLocals;
        }
        super.visitMaxs(i, i2);
    }

    /* renamed from: a.Wn$a */
    class C1555a extends C2415fC {
        private final /* synthetic */ MethodVisitor exw;

        C1555a(MethodVisitor methodVisitor) {
            this.exw = methodVisitor;
        }

        public AnnotationVisitor visitAnnotation(String str, boolean z) {
            if (str.equals(C2821kb.aqu.getDescriptor())) {
                return null;
            }
            return this.exw.visitAnnotation(str, z);
        }
    }
}
