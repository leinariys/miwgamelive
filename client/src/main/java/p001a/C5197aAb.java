package p001a;

import java.util.Comparator;

/* renamed from: a.aAb  reason: case insensitive filesystem */
/* compiled from: a */
class C5197aAb implements Comparator<String> {
    final /* synthetic */ C6368alc hcy;

    C5197aAb(C6368alc alc) {
        this.hcy = alc;
    }

    public int compare(String str, String str2) {
        boolean contains = str.contains("km");
        boolean contains2 = str2.contains("km");
        if (contains && !contains2) {
            return 1;
        }
        if (!contains && contains2) {
            return -1;
        }
        return this.hcy.m23789ae(Integer.parseInt(str.split(" ")[0]), Integer.parseInt(str2.split(" ")[0]));
    }
}
