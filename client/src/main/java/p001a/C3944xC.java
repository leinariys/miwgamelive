package p001a;

import game.network.manager.C6546aoy;

@C6546aoy
/* renamed from: a.xC */
/* compiled from: a */
public class C3944xC extends Exception {
    private C3945a bHq;

    public C3944xC(String str, C3945a aVar) {
        super(str);
        this.bHq = aVar;
    }

    public C3945a anW() {
        return this.bHq;
    }

    /* renamed from: a.xC$a */
    public enum C3945a {
        MONEY_NO_BALANCE,
        CRAFT_NO_REQUIRED_BLUEPRINT,
        CRAFT_NO_INGREDIENT,
        CRAFT_NO_RESULT,
        CRAFT_NO_ROOM,
        INSUFICIENT_ITENS,
        GIVE_NO_ROOM
    }
}
