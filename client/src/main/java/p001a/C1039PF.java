package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;

import java.io.Externalizable;

/* renamed from: a.PF */
/* compiled from: a */
public class C1039PF implements C3735uM {

    /* renamed from: dP */
    private static final float f1355dP = 0.017453292f;

    /* renamed from: dQ */
    final Vec3f f1356dQ = new Vec3f(1000.0f, 1000.0f, 1000.0f);
    final Vec3f dRX = new Vec3f(0.0f, 0.0f, 0.0f);
    private float dRY;
    private C1003Om dRZ;
    private C0763Kt stack = C0763Kt.bcE();

    /* renamed from: p */
    public void mo4627p(float f) {
        this.f1356dQ.set(f, f, f);
    }

    /* renamed from: ae */
    public void mo4621ae(Vec3f vec3f) {
        this.f1356dQ.set(vec3f);
    }

    public Vec3f bnO() {
        return this.f1356dQ;
    }

    /* renamed from: hi */
    public void mo4625hi(float f) {
        this.dRY = f;
    }

    public float bnP() {
        return this.dRY;
    }

    private final float max(float f, float f2) {
        return f < f2 ? f2 : f;
    }

    /* access modifiers changed from: package-private */
    public final float min(float f, float f2) {
        return f < f2 ? f : f2;
    }

    private final float clamp(float f, float f2, float f3) {
        if (f3 > f) {
            return f;
        }
        if (f3 < f2) {
            return f2;
        }
        return f3;
    }

    /* renamed from: a */
    private float m8349a(float f, float f2, float f3, float f4, float f5) {
        float clamp = clamp(f5, -f5, f3);
        if (f2 > f5 || f2 < (-f5)) {
            float f6 = f4 * f;
            float clamp2 = clamp(f6, -f6, clamp - f2) / f;
            float f7 = 10.0f * f6;
            float clamp3 = clamp(f7, -f7, (Math.signum(f2) * f5) - f2) / f;
            return Math.abs(clamp3) > Math.abs(clamp2) ? clamp3 : clamp2;
        }
        float f8 = f4 * f;
        return clamp(f8, -f8, clamp - f2) / f;
    }

    /* renamed from: a */
    public void mo3859a(float f, C1003Om om) {
        this.stack.bcI().push();
        this.stack.bcH().push();
        try {
            Vec3d ajr = (Vec3d) this.stack.bcI().get();
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            if (this.dRZ == null) {
                om.bln().set(0.0f, 0.0f, 0.0f);
                om.blp().set(0.0f, 0.0f, 0.0f);
            } else {
                om.bln().set(10.0f, 0.0f, 0.0f);
                om.blp().set(1.0f, 0.0f, 0.0f);
            }
            Vec3f bln = om.bln();
            Vec3f blp = om.blp();
            float f2 = bln.x;
            float f3 = bln.y;
            float f4 = bln.z;
            blp.x = m8349a(f, bln.x, f2 * 0.017453292f, 17.453293f, this.f1356dQ.x * 0.017453292f);
            float f5 = f;
            blp.y = m8349a(f5, bln.y, f3 * 0.017453292f, 17.453293f, this.f1356dQ.y * 0.017453292f);
            float f6 = f;
            blp.z = m8349a(f6, bln.z, f4 * 0.017453292f, 17.453293f, this.f1356dQ.z * 0.017453292f);
            om.mo4490IQ().orientation.transform(om.blp(), om.bls());
        } finally {
            this.stack.bcI().pop();
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a */
    public void mo3861a(Externalizable externalizable) {
    }

    /* renamed from: j */
    public Externalizable mo3862j(int i) {
        return null;
    }

    /* renamed from: a */
    public void mo4620a(C1003Om om) {
        this.dRZ = om;
    }

    public C1003Om bnQ() {
        return this.dRZ;
    }
}
