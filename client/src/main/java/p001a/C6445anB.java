package p001a;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.anB  reason: case insensitive filesystem */
/* compiled from: a */
public class C6445anB {
    private static final String ged = "cookies.txt";
    private static final String gee = "cookies.sqlite";
    private static String geb = System.getenv("APPDATA");
    private static String gec = (String.valueOf(geb) + File.separator + "Mozilla" + File.separator + "Firefox" + File.separator + "Profiles");

    /* renamed from: n */
    public static List<C4077yz> m24036n(String str, String str2, String str3) {
        ArrayList arrayList = new ArrayList();
        List<File> clB = clB();
        if (clB == null) {
            return null;
        }
        for (File next : clB) {
            if (ged.equals(next.getName())) {
                arrayList.addAll(m24035b(next, str, str2, str3));
            } else if (gee.equals(next.getName())) {
                arrayList.addAll(m24034a(next, str, str2, str3));
            }
        }
        return arrayList;
    }

    /* renamed from: a */
    private static List<C4077yz> m24034a(File file, String str, String str2, String str3) {
        ArrayList arrayList = new ArrayList();
        try {
            Class.forName("org.sqlite.JDBC");
            try {
                Connection connection = DriverManager.getConnection("jdbc:sqlite:" + file.getAbsolutePath());
                connection.setReadOnly(true);
                ResultSet executeQuery = connection.createStatement().executeQuery("select host, path, name, value, expiry from moz_cookies where host like '%" + str + "' and path='" + str2 + "' and name='" + str3 + "';");
                while (executeQuery.next()) {
                    arrayList.add(new C4077yz(executeQuery.getString("host"), executeQuery.getString("path"), executeQuery.getString("name"), executeQuery.getString("value"), executeQuery.getString("expiry")));
                }
                executeQuery.close();
                connection.close();
                return arrayList;
            } catch (SQLException e) {
                e.printStackTrace();
                return new ArrayList();
            }
        } catch (ClassNotFoundException e2) {
            e2.printStackTrace();
            return new ArrayList();
        }
    }

    /* renamed from: b */
    private static List<C4077yz> m24035b(File file, String str, String str2, String str3) {
        ArrayList arrayList = new ArrayList();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    return arrayList;
                }
                if (!readLine.startsWith("# ") && !readLine.equals("")) {
                    String[] split = readLine.split("\t");
                    String str4 = split[0];
                    String str5 = split[2];
                    String str6 = split[4];
                    String str7 = split[5];
                    String str8 = split.length >= 7 ? split[6] : "";
                    if (str.equals(str4) && str2.equals(str5) && str3.equals(str7)) {
                        arrayList.add(new C4077yz(str, str2, str3, str8, str6));
                    }
                }
            }
        } catch (IOException e) {
            return new ArrayList();
        }
    }

    private static List<File> clB() {
        ArrayList arrayList = new ArrayList();
        File file = new File(gec);
        if (!file.isDirectory()) {
            return null;
        }
        for (File file2 : file.listFiles()) {
            if (file2.isDirectory()) {
                for (File file3 : file2.listFiles()) {
                    if (ged.equals(file3.getName()) || gee.equals(file3.getName())) {
                        arrayList.add(file3);
                    }
                }
            }
        }
        if (!arrayList.isEmpty()) {
            return arrayList;
        }
        return null;
    }
}
