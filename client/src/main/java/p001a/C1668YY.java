package p001a;

import logic.baa.C1616Xf;
import taikodom.geom.Orientation;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.YY */
/* compiled from: a */
public class C1668YY extends C5342aFq {
    public static final C1668YY eNc = new C1668YY();

    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) {
        if (obj == null) {
            objectOutput.writeBoolean(false);
            return;
        }
        objectOutput.writeBoolean(true);
        Orientation orientation = (Orientation) obj;
        objectOutput.writeFloat(orientation.x);
        objectOutput.writeFloat(orientation.y);
        objectOutput.writeFloat(orientation.z);
        objectOutput.writeFloat(orientation.w);
    }

    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) {
        if (!objectInput.readBoolean()) {
            return null;
        }
        float readFloat = objectInput.readFloat();
        float readFloat2 = objectInput.readFloat();
        float readFloat3 = objectInput.readFloat();
        float readFloat4 = objectInput.readFloat();
        Orientation orientation = new Orientation();
        orientation.set(readFloat, readFloat2, readFloat3, readFloat4);
        return orientation;
    }

    /* renamed from: b */
    public Object mo3591b(C1616Xf xf) {
        return null;
    }
}
