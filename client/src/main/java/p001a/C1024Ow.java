package p001a;

import gnu.trove.THashSet;
import logic.render.QueueItem;
import logic.thred.LogPrinter;
import org.mozilla1.classfile.C0147Bi;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import taikodom.render.graphics2d.C0559Hm;

import java.io.File;
import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.reflect.Method;
import java.security.ProtectionDomain;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/* renamed from: a.Ow */
/* compiled from: a */
public class C1024Ow implements C6380alo, ClassFileTransformer {
    private static ConcurrentLinkedQueue<C1024Ow> dLN = new ConcurrentLinkedQueue<>();
    private static LogPrinter logger = LogPrinter.m10275K(C1024Ow.class);
    private Set<String> dLL = Collections.synchronizedSet(new THashSet());
    private Map<String, C0244Cz> dLM = new ConcurrentHashMap();
    private List<QueueItem<String, byte[]>> dLO = new LinkedList();
    private C6380alo.C1937a dLP = C6380alo.C1937a.GENERIC;
    private ThreadLocal<ClassLoader> dLQ = new ThreadLocal<>();
    private Method dLR;
    private File dLS = null;
    private Map<String, byte[]> dLT = null;

    public C1024Ow() {
        dLN.add(this);
    }

    /* renamed from: g */
    public static C0244Cz m8231g(byte[] bArr) {
        ClassReader classReader = new ClassReader(bArr);
        C6351alL all = new C6351alL();
        classReader.accept(all, 7);
        return all;
    }

    public C6380alo.C1937a blO() {
        return this.dLP;
    }

    /* renamed from: a */
    public void mo4566a(C6380alo.C1937a aVar) {
        this.dLP = aVar;
    }

    public byte[] transform(ClassLoader classLoader, String str, Class<?> cls, ProtectionDomain protectionDomain, byte[] bArr) {
        if (classLoader == null) {
            return null;
        }
        ClassLoader classLoader2 = this.dLQ.get();
        try {
            this.dLQ.set(classLoader);
            if (!m8232h(cls, str)) {
                C0244Cz cz = this.dLM.get(str);
                if (cz == null) {
                    cz = m8231g(bArr);
                    this.dLM.put(str, cz);
                } else if (cz.aBz()) {
                    if (this.dLL.contains(str)) {
                        cz = m8231g(bArr);
                        this.dLM.put(str, cz);
                    }
                }
                if (cz.aBz() && !cz.aBA()) {
                    ClassReader classReader = new ClassReader(bArr);
                    ClassWriter classWriter = new ClassWriter(1);
                    try {
                        classReader.accept(new C6416amY(classWriter, this), 0);
                        byte[] byteArray = classWriter.toByteArray();
                        mo4568b(str, byteArray);
                        this.dLL.add(str);
                        this.dLQ.set(classLoader2);
                        return byteArray;
                    } catch (C5637aQz e) {
                    }
                }
            }
            this.dLQ.set(classLoader2);
            return null;
        } catch (RuntimeException e2) {
            e2.printStackTrace();
            this.dLQ.set(classLoader2);
        } catch (Exception e3) {
            e3.printStackTrace();
            this.dLQ.set(classLoader2);
        } catch (Throwable th) {
            this.dLQ.set(classLoader2);
            throw th;
        }
        return null;
    }

    /* renamed from: b */
    public void mo4569b(String str, byte[] bArr, int i, int i2) {
        if (!(i == 0 && i2 == bArr.length)) {
            byte[] bArr2 = new byte[i2];
            System.arraycopy(bArr, i, bArr2, 0, i2);
            bArr = bArr2;
        }
        try {
            if (this.dLS != null) {
                C0559Hm.m5248a(new File(this.dLS, String.valueOf(str.replace('.', C0147Bi.cla)) + ".class"), bArr);
            } else {
                synchronized (this.dLO) {
                    this.dLO.add(new QueueItem(str, bArr));
                }
            }
        } catch (Exception e) {
            synchronized (this.dLO) {
                this.dLO.add(new QueueItem(str, bArr));
            }
        }
        blP();
    }

    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(Unknown Source)
        	at java.util.ArrayList.get(Unknown Source)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processExcHandler(RegionMaker.java:1043)
        	at jadx.core.dex.visitors.regions.RegionMaker.processTryCatchBlocks(RegionMaker.java:975)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:52)
        */
    private void blP() {
        /*
            r12 = this;
            r10 = 0
            java.util.List<a.Km<java.lang.String, byte[]>> r11 = r12.dLO
            monitor-enter(r11)
        L_0x0004:
            r9 = r10
            r8 = r10
        L_0x0006:
            java.util.List<a.Km<java.lang.String, byte[]>> r1 = r12.dLO     // Catch:{ all -> 0x004c }
            int r1 = r1.size()     // Catch:{ all -> 0x004c }
            if (r9 < r1) goto L_0x0012
            if (r8 > 0) goto L_0x0004
            monitor-exit(r11)     // Catch:{ all -> 0x004c }
            return
        L_0x0012:
            java.util.List<a.Km<java.lang.String, byte[]>> r1 = r12.dLO     // Catch:{ all -> 0x004c }
            r2 = 0
            java.lang.Object r1 = r1.remove(r2)     // Catch:{ all -> 0x004c }
            r0 = r1
            a.Km r0 = (p001a.C0754Km) r0     // Catch:{ all -> 0x004c }
            r7 = r0
            r2 = 0
            java.lang.Object r3 = r7.first()     // Catch:{ ClassNotFoundException -> 0x003d, Exception -> 0x004f }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ ClassNotFoundException -> 0x003d, Exception -> 0x004f }
            java.lang.Object r4 = r7.bcA()     // Catch:{ ClassNotFoundException -> 0x003d, Exception -> 0x004f }
            byte[] r4 = (byte[]) r4     // Catch:{ ClassNotFoundException -> 0x003d, Exception -> 0x004f }
            r5 = 0
            java.lang.Object r1 = r7.bcA()     // Catch:{ ClassNotFoundException -> 0x003d, Exception -> 0x004f }
            byte[] r1 = (byte[]) r1     // Catch:{ ClassNotFoundException -> 0x003d, Exception -> 0x004f }
            int r6 = r1.length     // Catch:{ ClassNotFoundException -> 0x003d, Exception -> 0x004f }
            r1 = r12
            r1.mo4565a((java.lang.ClassLoader) r2, (java.lang.String) r3, (byte[]) r4, (int) r5, (int) r6)     // Catch:{ ClassNotFoundException -> 0x003d, Exception -> 0x004f }
            int r1 = r8 + 1
        L_0x0038:
            int r2 = r9 + 1
            r9 = r2
            r8 = r1
            goto L_0x0006
        L_0x003d:
            r1 = move-exception
            java.util.List<a.Km<java.lang.String, byte[]>> r2 = r12.dLO     // Catch:{ all -> 0x004c }
            monitor-enter(r2)     // Catch:{ all -> 0x004c }
            java.util.List<a.Km<java.lang.String, byte[]>> r1 = r12.dLO     // Catch:{ all -> 0x0049 }
            r1.add(r7)     // Catch:{ all -> 0x0049 }
            monitor-exit(r2)     // Catch:{ all -> 0x0049 }
            r1 = r8
            goto L_0x0038
        L_0x0049:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0049 }
            throw r1     // Catch:{ all -> 0x004c }
        L_0x004c:
            r1 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x004c }
            throw r1
        L_0x004f:
            r1 = move-exception
            java.util.List<a.Km<java.lang.String, byte[]>> r2 = r12.dLO     // Catch:{ all -> 0x004c }
            monitor-enter(r2)     // Catch:{ all -> 0x004c }
            java.util.List<a.Km<java.lang.String, byte[]>> r1 = r12.dLO     // Catch:{ all -> 0x005b }
            r1.add(r7)     // Catch:{ all -> 0x005b }
            monitor-exit(r2)     // Catch:{ all -> 0x005b }
            r1 = r8
            goto L_0x0038
        L_0x005b:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x005b }
            throw r1     // Catch:{ all -> 0x004c }
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C1024Ow.blP():void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Class mo4565a(ClassLoader classLoader, String str, byte[] bArr, int i, int i2) {
        if (classLoader == null) {
            classLoader = ClassLoader.getSystemClassLoader();
        }
        try {
            if (this.dLR == null) {
                Method declaredMethod = Class.forName("java.lang.ClassLoader").getDeclaredMethod("defineClass", new Class[]{String.class, byte[].class, Integer.TYPE, Integer.TYPE});
                declaredMethod.setAccessible(true);
                this.dLR = declaredMethod;
            }
            Class cls = (Class) this.dLR.invoke(classLoader, new Object[]{str, bArr, Integer.valueOf(i), Integer.valueOf(i2)});
            if (this.dLT != null) {
                byte[] bArr2 = new byte[i2];
                System.arraycopy(bArr, i, bArr2, 0, i2);
                this.dLT.put(str, bArr2);
            }
            return cls;
        } catch (Exception e) {
            throw new Exception("Error loading class: " + str, e);
        }
    }

    /* renamed from: h */
    private boolean m8232h(Class<?> cls, String str) {
        return str.startsWith("java/") || str.startsWith("org/") || str.startsWith("sun/") || str.startsWith("net/") || str.startsWith("gnu/") || str.startsWith("javax/") || str.startsWith("com/sun/");
    }

    public void clear() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo4568b(String str, byte[] bArr) {
    }

    /* renamed from: a */
    public void mo4567a(C6416amY amy, ayO ayo, int i, String str, Object... objArr) {
        System.err.println(String.valueOf(amy.name) + ":" + i + " " + String.format(str, objArr));
    }

    /* renamed from: ge */
    public C0244Cz mo4573ge(String str) {
        return mo4575i((Class<?>) null, str);
    }

    /* renamed from: i */
    public C0244Cz mo4575i(Class<?> cls, String str) {
        C0244Cz cz = this.dLM.get(str);
        if (cz != null) {
            return cz;
        }
        if (m8232h(cls, str)) {
            C6351alL all = new C6351alL();
            this.dLM.put(str, all);
            return all;
        }
        try {
            ClassReader classReader = new ClassReader(str);
            C6351alL all2 = new C6351alL();
            classReader.accept(all2, 7);
            this.dLM.put(str, all2);
            return all2;
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(str);
        }
    }

    /* renamed from: h */
    public void mo4574h(File file) {
        this.dLS = file;
    }

    /* renamed from: q */
    public void mo4576q(Map<String, byte[]> map) {
        this.dLT = map;
    }

    public Map<String, byte[]> blQ() {
        return this.dLT;
    }
}
