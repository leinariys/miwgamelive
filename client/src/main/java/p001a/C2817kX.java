package p001a;

/* renamed from: a.kX */
/* compiled from: a */
public final class C2817kX {
    private C2817kX() {
    }

    /* renamed from: cK */
    public static void m34451cK(int i) {
        StackTraceElement[] stackTrace = new Exception().getStackTrace();
        int i2 = 0;
        while (i2 < i && i2 < stackTrace.length) {
            System.out.println("   at " + stackTrace[i2].getClassName() + "." + stackTrace[i2].getMethodName() + " [" + stackTrace[i2].getFileName() + ":" + stackTrace[i2].getLineNumber() + "]");
            i2++;
        }
    }

    /* renamed from: k */
    public static void m34452k(int i, int i2) {
        StackTraceElement[] stackTrace = new Exception().getStackTrace();
        while (i < i2 && i < stackTrace.length) {
            System.out.println("   at " + stackTrace[i].getClassName() + "." + stackTrace[i].getMethodName() + " [" + stackTrace[i].getFileName() + ":" + stackTrace[i].getLineNumber() + "]");
            i++;
        }
    }
}
