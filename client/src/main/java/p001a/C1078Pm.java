package p001a;

import logic.render.IEngineGraphics;
import logic.res.LoaderTrail;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import taikodom.render.loader.RenderAsset;

/* renamed from: a.Pm */
/* compiled from: a */
public class C1078Pm implements aOT<C5907acj> {
    private static final Log logger = LogPrinter.m10275K(LoaderTrail.class);

    /* renamed from: lV */
    private IEngineGraphics f1384lV;

    public C1078Pm(IEngineGraphics abd) {
        this.f1384lV = abd;
    }

    /* renamed from: a */
    public void mo4836b(C5907acj acj) {
        this.f1384lV.mo3049bV(acj.fileName);
    }

    /* renamed from: b */
    public void mo4838c(C5907acj acj) {
        RenderAsset renderAsset;
        try {
            renderAsset = this.f1384lV.mo3047bT(acj.hTN);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            renderAsset = null;
        }
        if (renderAsset != null) {
            try {
                acj.mo12688c(renderAsset);
            } catch (RuntimeException e2) {
                logger.error(e2.getMessage(), e2);
            }
        } else {
            logger.error("Object not found " + acj.hTN);
        }
    }
}
