package p001a;

import logic.baa.C1616Xf;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aOM */
/* compiled from: a */
public class aOM extends C3813vL {
    private static final Float iAd = Float.valueOf(0.0f);

    /* renamed from: d */
    public void mo7919d(ObjectOutput objectOutput, Object obj) {
        objectOutput.writeFloat(((Float) obj).floatValue());
    }

    /* renamed from: f */
    public Object mo7920f(ObjectInput objectInput) {
        return Float.valueOf(objectInput.readFloat());
    }

    /* renamed from: b */
    public Object mo3591b(C1616Xf xf) {
        return iAd;
    }
}
