package p001a;

import game.script.npc.NPCType;
import game.script.npcchat.AbstractSpeech;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: a.QV */
/* compiled from: a */
public class C1122QV {
    private NPCType dXp;
    private String message;
    private List<AbstractSpeech> nodes;

    public C1122QV(String str, AbstractSpeech aht) {
        this.message = str;
        this.nodes = new ArrayList();
        this.nodes.add(aht);
    }

    public C1122QV(String str) {
        this.message = str;
        this.nodes = new ArrayList();
    }

    public C1122QV(String str, AbstractSpeech aht, NPCType aed) {
        this(str, aht);
        this.dXp = aed;
    }

    /* renamed from: d */
    public void mo5009d(AbstractSpeech aht) {
        this.nodes.add(aht);
    }

    /* renamed from: q */
    public void mo5010q(NPCType aed) {
        this.dXp = aed;
    }

    public String toString() {
        ArrayList arrayList = new ArrayList(this.nodes);
        Collections.reverse(arrayList);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.message);
        stringBuffer.append(": ");
        if (this.dXp == null) {
            throw new IllegalStateException("No NPCType has been set, so the toString() method cannot be called");
        }
        stringBuffer.append("NPCChat [");
        stringBuffer.append(this.dXp.getHandle());
        stringBuffer.append("]");
        if (!arrayList.isEmpty()) {
            stringBuffer.append(", ");
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            stringBuffer.append("[");
            stringBuffer.append(((AbstractSpeech) it.next()).getHandle());
            stringBuffer.append("]");
            if (it.hasNext()) {
                stringBuffer.append(" -> ");
            }
        }
        return stringBuffer.toString();
    }
}
