package p001a;

import java.util.ArrayList;

/* renamed from: a.aoW  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C6518aoW<T> {
    private final ArrayList<T> list;
    private T gjR;
    private int pos;
    private int[] stack;
    private int stackCount;

    public C6518aoW() {
        this.list = new ArrayList<>();
        this.stack = new int[512];
        this.stackCount = 0;
        this.pos = 0;
        this.gjR = create();
    }

    public C6518aoW(boolean z) {
        this.list = new ArrayList<>();
        this.stack = new int[512];
        this.stackCount = 0;
        this.pos = 0;
    }

    /* access modifiers changed from: protected */
    public abstract void copy(T t, T t2);

    /* access modifiers changed from: protected */
    public abstract T create();

    public final void push() {
        int[] iArr = this.stack;
        int i = this.stackCount;
        this.stackCount = i + 1;
        iArr[i] = this.pos;
    }

    public final void pop() {
        int[] iArr = this.stack;
        int i = this.stackCount - 1;
        this.stackCount = i;
        this.pos = iArr[i];
    }

    public T get() {
        if (this.pos == this.list.size()) {
            expand();
        }
        ArrayList<T> arrayList = this.list;
        int i = this.pos;
        this.pos = i + 1;
        return arrayList.get(i);
    }

    /* renamed from: aq */
    public final T mo15197aq(T t) {
        copy(this.gjR, t);
        return this.gjR;
    }

    private void expand() {
        this.list.add(create());
    }
}
