package p001a;

import com.hoplon.geometry.Vec3f;
import logic.abc.C1182RU;
import logic.abc.azD;

/* renamed from: a.gd */
/* compiled from: a */
public class C2544gd extends C3808vG {

    /* renamed from: Qk */
    private boolean f7684Qk = false;

    /* renamed from: Ql */
    private aGW f7685Ql;
    private boolean bzz;

    public C2544gd(aGW agw, C1034PA pa, ayY ayy, ayY ayy2, boolean z) {
        super(pa);
        this.f7685Ql = agw;
        this.bzz = z;
        ayY ayy3 = z ? ayy2 : ayy;
        ayy = !z ? ayy2 : ayy;
        if (this.f7685Ql == null && this.f9400Uy.mo588b(ayy3, ayy)) {
            this.f7685Ql = this.f9400Uy.mo594g(ayy3, ayy);
            this.f7684Qk = true;
        }
    }

    public void destroy() {
        if (this.f7684Qk && this.f7685Ql != null) {
            this.f9400Uy.mo583a(this.f7685Ql);
        }
    }

    /* renamed from: a */
    public void mo8931a(ayY ayy, ayY ayy2, C5408aIe aie, C5362aGk agk) {
        if (this.f7685Ql != null) {
            this.stack.bcF();
            try {
                ayY ayy3 = this.bzz ? ayy2 : ayy;
                if (!this.bzz) {
                    ayy = ayy2;
                }
                azD azd = (azD) ayy3.cEZ();
                C1182RU ru = (C1182RU) ayy.cEZ();
                Vec3f bsp = ru.bsp();
                float bsq = ru.bsq();
                C3978xf xfVar = (C3978xf) this.stack.bcJ().get();
                xfVar.mo22952b(ayy3.cFf());
                xfVar.mo22954c(ayy.cFf());
                C3978xf xfVar2 = (C3978xf) this.stack.bcJ().get();
                xfVar2.mo22952b(ayy.cFf());
                xfVar2.mo22954c(ayy3.cFf());
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                vec3f.negate(bsp);
                xfVar.bFF.transform(vec3f);
                Vec3f ac = this.stack.bcH().mo4458ac(this.stack.bcH().mo4458ac(azd.localGetSupportingVertexWithoutMargin(vec3f)));
                xfVar2.mo22946G(ac);
                float dot = (bsp.dot(ac) - bsq) - azd.getMargin();
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                vec3f.scale(dot, bsp);
                vec3f2.sub(ac, vec3f);
                Vec3f ac2 = this.stack.bcH().mo4458ac(vec3f2);
                ayy.cFf().mo22946G(ac2);
                boolean z = dot < this.f7685Ql.dcB();
                agk.mo9117e(this.f7685Ql);
                if (z) {
                    Vec3f ac3 = this.stack.bcH().mo4458ac(bsp);
                    ayy.cFf().bFF.transform(ac3);
                    agk.mo5217c(ac3, this.stack.bcH().mo4458ac(ac2), dot);
                }
                if (this.f7684Qk && this.f7685Ql.dcA() != 0) {
                    agk.dad();
                }
            } finally {
                this.stack.bcG();
            }
        }
    }

    /* renamed from: b */
    public float mo8932b(ayY ayy, ayY ayy2, C5408aIe aie, C5362aGk agk) {
        return 1.0f;
    }

    /* renamed from: a.gd$a */
    public static class C2545a extends C6548apA {
        /* renamed from: a */
        public C3808vG mo8207a(C1034PA pa, ayY ayy, ayY ayy2) {
            if (!this.gmK) {
                return new C2544gd((aGW) null, pa, ayy, ayy2, false);
            }
            return new C2544gd((aGW) null, pa, ayy, ayy2, true);
        }
    }
}
