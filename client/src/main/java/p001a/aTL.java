package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.C0813Lk;
import game.script.missile.MissileWeapon;
import game.script.ship.Ship;
import logic.WrapRunnable;
import logic.aaa.C5623aQl;
import taikodom.addon.hud.HudTargetLockAddon;
import taikodom.render.camera.Camera;

import javax.vecmath.Tuple3d;
import java.util.Collection;

/* renamed from: a.aTL */
/* compiled from: a */
class aTL extends WrapRunnable {
    final /* synthetic */ HudTargetLockAddon iSR;
    private float angle;
    private Ship coW;

    public aTL(HudTargetLockAddon hudTargetLockAddon) {
        this.iSR = hudTargetLockAddon;
    }

    public void run() {
        C1583XH xh;
        Vec3d blk;
        boolean z;
        if (this.iSR.f9935kj == null || this.iSR.f9935kj.getPlayer() == null) {
            cancel();
        } else if (this.iSR.ams != null && (this.iSR.ams instanceof aDA)) {
            Collection<C1583XH> b = this.iSR.f9935kj.aVU().mo13967b(C1583XH.class);
            if (!b.isEmpty()) {
                xh = (C1583XH) b.toArray()[b.size() - 1];
            } else {
                xh = null;
            }
            if (!(xh == null || xh.bhP() == this.iSR.ams)) {
                xh = null;
            }
            if (this.coW == null || this.coW != this.iSR.f9935kj.getPlayer().bQx()) {
                this.coW = this.iSR.f9935kj.getPlayer().bQx();
                if (this.coW == null) {
                    System.err.println("HTLA: Player has no active ship.");
                    return;
                }
                this.angle = (float) Math.cos((double) this.coW.agD());
            }
            Camera adY = this.iSR.f9935kj.ale().adY();
            Vec3f vec3f = new Vec3f();
            adY.getTransform().multiply3x3(this.coW.aip(), vec3f);
            vec3f.normalize();
            Vec3d position = this.coW.getPosition();
            if (this.iSR.gYs != null && this.iSR.gYs.aqh() > 0) {
                if (xh == null) {
                    blk = this.iSR.ams.getPosition();
                } else {
                    blk = xh.bGf().blk();
                }
                Vec3f dfV = blk.mo9517f((Tuple3d) position).dfV();
                dfV.normalize();
                float dot = vec3f.dot(dfV);
                if (((float) this.coW.getPosition().mo9491ax(blk)) <= this.iSR.gYs.mo12940in()) {
                    z = true;
                } else {
                    z = false;
                }
                if (z) {
                    if (!this.iSR.gYp) {
                        this.iSR.gYp = true;
                        this.iSR.f9935kj.aVU().mo13975h(new C5334aFi(true));
                    }
                } else if (this.iSR.gYp) {
                    this.iSR.gYp = false;
                    this.iSR.f9935kj.aVU().mo13975h(new C5334aFi(false));
                }
                if (dot <= this.angle || !z) {
                    if (this.iSR.gYj) {
                        this.iSR.f9935kj.aVU().mo13975h(new C5623aQl(false));
                        this.iSR.gYj = false;
                    }
                } else if (!this.iSR.gYj) {
                    this.iSR.f9935kj.aVU().mo13975h(new C5623aQl(true));
                    this.iSR.gYj = true;
                }
            }
            if ((this.iSR.gYr instanceof MissileWeapon) && this.iSR.gYr.aqh() > 0 && (this.iSR.ams instanceof Ship)) {
                MissileWeapon bp = (MissileWeapon) this.iSR.gYr;
                if (bp.ayD() > 0) {
                    Vec3f dfV2 = this.iSR.ams.getPosition().mo9517f((Tuple3d) position).dfV();
                    dfV2.normalize();
                    float dot2 = vec3f.dot(dfV2);
                    float bx = this.coW.mo1013bx(this.iSR.ams);
                    this.angle = (float) Math.cos((double) (this.coW.agD() * this.iSR.gYi));
                    boolean z2 = bx <= bp.mo12940in() * bp.mo12940in();
                    if (z2) {
                        if (!this.iSR.gYq) {
                            this.iSR.gYq = true;
                            this.iSR.f9935kj.aVU().mo13975h(new C5296aDw(true));
                        }
                    } else if (this.iSR.gYq) {
                        this.iSR.gYq = false;
                        this.iSR.f9935kj.aVU().mo13975h(new C5296aDw(false));
                    }
                    if (dot2 > this.angle && z2) {
                        bp.ayD();
                        if (!this.iSR.gYk) {
                            this.iSR.gYk = true;
                            bp.mo830bR(true);
                            this.iSR.f9935kj.aVU().mo13975h(new C0813Lk(true, bp));
                        }
                    } else if (this.iSR.gYk) {
                        this.iSR.gYk = false;
                        bp.mo830bR(false);
                        this.iSR.f9935kj.aVU().mo13975h(new C0813Lk(false, bp));
                    }
                }
            }
        }
    }
}
