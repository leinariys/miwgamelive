package p001a;

import com.steadystate.css.dom.CSSStyleSheetImpl;
import com.steadystate.css.parser.CSSOMParser;
import logic.res.css.C3331qN;
import logic.res.css.CSSException;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.CSSCharsetRule;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleSheet;

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;

/* renamed from: a.aSL */
/* compiled from: a */
public class aSL implements Serializable, CSSCharsetRule {
    String _encoding = null;

    /* renamed from: qq */
    CSSStyleSheetImpl f3746qq = null;

    /* renamed from: qr */
    CSSRule f3747qr = null;

    public aSL(CSSStyleSheetImpl ss, CSSRule cSSRule, String str) {
        this.f3746qq = ss;
        this.f3747qr = cSSRule;
        this._encoding = str;
    }

    public short getType() {
        return 2;
    }

    public String getCssText() {
        return "@charset \"" + getEncoding() + "\";";
    }

    public void setCssText(String str) {
        if (this.f3746qq == null || !this.f3746qq.isReadOnly()) {
            try {
                aSL f = new CSSOMParser().parseRule(new InputSource((Reader) new StringReader(str)));
                if (f.getType() == 2) {
                    this._encoding = f._encoding;
                    return;
                }
                throw new C3331qN(13, 5);
            } catch (CSSException e) {
                throw new C3331qN(12, 0, e.getMessage());
            } catch (IOException e2) {
                throw new C3331qN(12, 0, e2.getMessage());
            }
        } else {
            throw new C3331qN(7, 2);
        }
    }

    public CSSStyleSheet getParentStyleSheet() {
        return this.f3746qq;
    }

    public CSSRule getParentRule() {
        return this.f3747qr;
    }

    public String getEncoding() {
        return this._encoding;
    }

    public void setEncoding(String str) {
        this._encoding = str;
    }
}
