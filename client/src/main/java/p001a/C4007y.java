package p001a;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.y */
/* compiled from: a */
public class C4007y {

    /* renamed from: bW */
    private final Rectangle f9570bW;
    /* renamed from: bX */
    private final int f9571bX;
    /* renamed from: bU */
    C4007y[] f9568bU = new C4007y[4];
    /* renamed from: bV */
    List<Rectangle> f9569bV = new ArrayList();
    Rectangle[] rects = new Rectangle[4];
    /* renamed from: bY */
    private int f9572bY = 4;

    public C4007y(Rectangle rectangle) {
        this.f9570bW = rectangle;
        int i = ((rectangle.x * 2) + rectangle.width) / 2;
        int i2 = ((rectangle.y * 2) + rectangle.height) / 2;
        int i3 = i - rectangle.x;
        int i4 = rectangle.width - i3;
        int i5 = i2 - rectangle.y;
        int i6 = rectangle.height - i5;
        this.f9571bX = rectangle.width * rectangle.height;
        this.rects[0] = new Rectangle(rectangle.x, rectangle.y, i3, i5);
        this.rects[1] = new Rectangle(i, rectangle.y, i4, i5);
        this.rects[2] = new Rectangle(i, i2, i4, i6);
        this.rects[3] = new Rectangle(rectangle.x, i2, i3, i6);
    }

    /* renamed from: a */
    public void mo23053a(Rectangle rectangle) {
        if (this.f9570bW.intersects(rectangle)) {
            if (rectangle.height * rectangle.width >= this.f9571bX || rectangle.height < this.f9572bY || rectangle.width < this.f9572bY) {
                this.f9569bV.add(rectangle);
                return;
            }
            for (int i = 0; i < 4; i++) {
                if (this.rects[i].intersects(rectangle)) {
                    if (this.f9568bU[i] == null) {
                        this.f9568bU[i] = new C4007y(this.rects[i]);
                    }
                    this.f9568bU[i].mo23053a(rectangle);
                }
            }
        }
    }

    /* renamed from: b */
    public boolean mo23054b(Rectangle rectangle) {
        if (!this.f9570bW.intersects(rectangle)) {
            return false;
        }
        if (this.f9569bV.size() > 0) {
            for (Rectangle intersects : this.f9569bV) {
                if (intersects.intersects(rectangle)) {
                    return true;
                }
            }
        }
        for (int i = 0; i < 4; i++) {
            if (this.f9568bU[i] != null && this.f9568bU[i].mo23054b(rectangle)) {
                return true;
            }
        }
        return false;
    }
}
