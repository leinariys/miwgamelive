package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;
import logic.abc.azD;

import javax.vecmath.Quat4f;
import java.lang.reflect.Array;
import java.util.Arrays;

/* renamed from: a.dL */
/* compiled from: a */
public class C2248dL {

    /* renamed from: Aj */
    private static final float f6471Aj = Float.MAX_VALUE;
    /* renamed from: Ak */
    private static final float f6472Ak = 3.1415927f;
    /* renamed from: Al */
    private static final float f6473Al = 6.2831855f;
    /* renamed from: Am */
    private static final int f6474Am = 128;
    /* renamed from: An */
    private static final int f6475An = 64;
    /* renamed from: Ao */
    private static final int f6476Ao = 63;
    /* renamed from: Ap */
    private static final float f6477Ap = 1.0E-4f;
    /* renamed from: Aq */
    private static final float f6478Aq = 9.999999E-9f;
    /* renamed from: Ar */
    private static final int f6479Ar = 256;
    /* renamed from: As */
    private static final float f6480As = 0.01f;
    /* renamed from: At */
    private static final float f6481At = 0.001f;
    /* renamed from: Ag */
    public static aFF<C2249a.C2251b> f6468Ag = new aFF<>(C2249a.C2251b.class);
    /* renamed from: Ah */
    public static aFF<C2249a.C2250a> f6469Ah = new aFF<>(C2249a.C2250a.class);
    /* renamed from: Ai */
    public static aFF<C2252b.C2253a> f6470Ai = new aFF<>(C2252b.C2253a.class);
    /* renamed from: Au */
    private static C2249a f6482Au = new C2249a();

    /* renamed from: kh */
    public static void m28791kh() {
        f6468Ag.push();
        f6469Ah.push();
        f6470Ai.push();
    }

    /* renamed from: ki */
    public static void m28792ki() {
        f6468Ag.pop();
        f6469Ah.pop();
        f6470Ai.pop();
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public static boolean m28790a(azD azd, C3978xf xfVar, azD azd2, C3978xf xfVar2, float f, C2255d dVar) {
        dVar.f6488Oi[0].set(0.0f, 0.0f, 0.0f);
        dVar.f6488Oi[1].set(0.0f, 0.0f, 0.0f);
        dVar.f6489Oj.set(0.0f, 0.0f, 0.0f);
        dVar.f6490Ok = 0.0f;
        dVar.gRC = C2254c.Separated;
        dVar.f6491Ol = 0;
        dVar.f6492Om = 0;
        f6482Au.mo17750a(xfVar.bFF, xfVar.bFG, azd, xfVar2.bFF, xfVar2.bFG, azd2, f + f6481At);
        try {
            boolean MT = f6482Au.mo17747MT();
            dVar.f6492Om = f6482Au.iterations + 1;
            if (MT) {
                C2252b bVar = new C2252b(f6482Au);
                float aSb = bVar.aSb();
                dVar.f6491Ol = bVar.iterations + 1;
                if (aSb > 0.0f) {
                    dVar.gRC = C2254c.Penetrating;
                    dVar.f6489Oj.set(bVar.f6486Oj);
                    dVar.f6490Ok = aSb;
                    dVar.f6488Oi[0].set(bVar.dcW[0]);
                    dVar.f6488Oi[1].set(bVar.dcW[1]);
                    f6482Au.destroy();
                    return true;
                } else if (bVar.failed) {
                    dVar.gRC = C2254c.EPA_Failed;
                }
            } else if (f6482Au.failed) {
                dVar.gRC = C2254c.GJK_Failed;
            }
            f6482Au.destroy();
            return false;
        } catch (Throwable th) {
            f6482Au.destroy();
            throw th;
        }
    }

    /* renamed from: a.dL$c */
    /* compiled from: a */
    public enum C2254c {
        Separated,
        Penetrating,
        GJK_Failed,
        EPA_Failed
    }

    /* renamed from: a.dL$d */
    /* compiled from: a */
    public static class C2255d {

        /* renamed from: Oi */
        public final Vec3f[] f6488Oi = {new Vec3f(), new Vec3f()};

        /* renamed from: Oj */
        public final Vec3f f6489Oj = new Vec3f();

        /* renamed from: Ok */
        public float f6490Ok;

        /* renamed from: Ol */
        public int f6491Ol;

        /* renamed from: Om */
        public int f6492Om;
        public C2254c gRC;
    }

    /* renamed from: a.dL$a */
    public static class C2249a {
        public final Matrix3fWrap[] aBg;
        public final Vec3f[] aBh;
        public final Vec3f aBk;
        public final azD[] cXA;
        public final C2251b[] cXB;
        public final C2250a[] cXz;
        public final C0763Kt stack;
        public float aBl;
        public boolean failed;
        public int iterations;
        public int order;

        public C2249a() {
            this.stack = C0763Kt.bcE();
            this.cXz = new C2250a[64];
            this.aBg = new Matrix3fWrap[]{new Matrix3fWrap(), new Matrix3fWrap()};
            this.aBh = new Vec3f[]{new Vec3f(), new Vec3f()};
            this.cXA = new azD[2];
            this.cXB = new C2251b[5];
            this.aBk = new Vec3f();
            for (int i = 0; i < this.cXB.length; i++) {
                this.cXB[i] = new C2251b();
            }
        }

        public C2249a(Matrix3fWrap ajd, Vec3f vec3f, azD azd, Matrix3fWrap ajd2, Vec3f vec3f2, azD azd2) {
            this(ajd, vec3f, azd, ajd2, vec3f2, azd2, 0.0f);
        }

        public C2249a(Matrix3fWrap ajd, Vec3f vec3f, azD azd, Matrix3fWrap ajd2, Vec3f vec3f2, azD azd2, float f) {
            this.stack = C0763Kt.bcE();
            this.cXz = new C2250a[64];
            this.aBg = new Matrix3fWrap[]{new Matrix3fWrap(), new Matrix3fWrap()};
            this.aBh = new Vec3f[]{new Vec3f(), new Vec3f()};
            this.cXA = new azD[2];
            this.cXB = new C2251b[5];
            this.aBk = new Vec3f();
            for (int i = 0; i < this.cXB.length; i++) {
                this.cXB[i] = new C2251b();
            }
            mo17750a(ajd, vec3f, azd, ajd2, vec3f2, azd2, f);
        }

        /* renamed from: t */
        public static int m28793t(Vec3f vec3f) {
            return (((((int) (vec3f.x * 15461.0f)) ^ ((int) (vec3f.y * 83003.0f))) ^ ((int) (vec3f.z * 15473.0f))) * 169639) & 63;
        }

        /* renamed from: a */
        public void mo17750a(Matrix3fWrap ajd, Vec3f vec3f, azD azd, Matrix3fWrap ajd2, Vec3f vec3f2, azD azd2, float f) {
            C2248dL.m28791kh();
            this.aBg[0].set(ajd);
            this.aBh[0].set(vec3f);
            this.cXA[0] = azd;
            this.aBg[1].set(ajd2);
            this.aBh[1].set(vec3f2);
            this.cXA[1] = azd2;
            this.aBl = f;
            this.failed = false;
        }

        public void destroy() {
            C2248dL.m28792ki();
        }

        /* renamed from: a */
        public Vec3f mo17749a(Vec3f vec3f, int i) {
            this.stack.bcH().push();
            try {
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                C3427rS.m38372a(vec3f2, vec3f, this.aBg[i]);
                Vec3f ac = this.stack.bcH().mo4458ac(this.cXA[i].localGetSupportingVertex(vec3f2));
                this.aBg[i].transform(ac);
                ac.add(this.aBh[i]);
                return (Vec3f) this.stack.bcH().mo15197aq(ac);
            } finally {
                this.stack.bcH().pop();
            }
        }

        /* renamed from: a */
        public void mo17751a(Vec3f vec3f, C2251b bVar) {
            this.stack.bcH().push();
            try {
                bVar.f6484gw.set(vec3f);
                Vec3f ac = this.stack.bcH().mo4458ac(mo17749a(vec3f, 0));
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                vec3f2.set(vec3f);
                vec3f2.negate();
                bVar.f6483gv.sub(ac, this.stack.bcH().mo4458ac(mo17749a(vec3f2, 1)));
                bVar.f6483gv.scaleAdd(this.aBl, vec3f, bVar.f6483gv);
            } finally {
                this.stack.bcH().pop();
            }
        }

        /* renamed from: MS */
        public boolean mo17746MS() {
            int t = m28793t(this.aBk);
            for (C2250a aVar = this.cXz[t]; aVar != null; aVar = aVar.aMa) {
                if (aVar.aLZ.equals(this.aBk)) {
                    this.order--;
                    return false;
                }
            }
            C2250a aVar2 = C2248dL.f6469Ah.get();
            aVar2.aLZ.set(this.aBk);
            aVar2.aMa = this.cXz[t];
            this.cXz[t] = aVar2;
            Vec3f vec3f = this.aBk;
            C2251b[] bVarArr = this.cXB;
            int i = this.order + 1;
            this.order = i;
            mo17751a(vec3f, bVarArr[i]);
            if (this.aBk.dot(this.cXB[this.order].f6483gv) > 0.0f) {
                return true;
            }
            return false;
        }

        /* JADX INFO: finally extract failed */
        /* renamed from: e */
        public boolean mo17756e(Vec3f vec3f, Vec3f vec3f2) {
            this.stack.bcH().push();
            try {
                if (vec3f2.dot(vec3f) >= 0.0f) {
                    Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                    vec3f3.cross(vec3f2, vec3f);
                    if (vec3f3.lengthSquared() > C2248dL.f6478Aq) {
                        this.aBk.cross(vec3f3, vec3f2);
                    } else {
                        this.stack.bcH().pop();
                        return true;
                    }
                } else {
                    this.order = 0;
                    this.cXB[0].mo17758a(this.cXB[1]);
                    this.aBk.set(vec3f);
                }
                this.stack.bcH().pop();
                return false;
            } catch (Throwable th) {
                this.stack.bcH().pop();
                throw th;
            }
        }

        /* renamed from: c */
        public boolean mo17753c(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
            this.stack.bcH().push();
            try {
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                vec3f4.cross(vec3f2, vec3f3);
                return mo17752b(vec3f, vec3f2, vec3f3, vec3f4);
            } finally {
                this.stack.bcH().pop();
            }
        }

        /* renamed from: b */
        public boolean mo17752b(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
            this.stack.bcH().push();
            try {
                Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                vec3f5.cross(vec3f4, vec3f2);
                Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
                vec3f6.cross(vec3f4, vec3f3);
                if (vec3f5.dot(vec3f) < -1.0E-4f) {
                    this.order = 1;
                    this.cXB[0].mo17758a(this.cXB[1]);
                    this.cXB[1].mo17758a(this.cXB[2]);
                    return mo17756e(vec3f, vec3f2);
                } else if (vec3f6.dot(vec3f) > 1.0E-4f) {
                    this.order = 1;
                    this.cXB[1].mo17758a(this.cXB[2]);
                    boolean e = mo17756e(vec3f, vec3f3);
                    this.stack.bcH().pop();
                    return e;
                } else {
                    float dot = vec3f4.dot(vec3f);
                    if (Math.abs(dot) > 1.0E-4f) {
                        if (dot > 0.0f) {
                            this.aBk.set(vec3f4);
                        } else {
                            this.aBk.negate(vec3f4);
                            C2251b bVar = new C2251b();
                            bVar.mo17758a(this.cXB[0]);
                            this.cXB[0].mo17758a(this.cXB[1]);
                            this.cXB[1].mo17758a(bVar);
                        }
                        this.stack.bcH().pop();
                        return false;
                    }
                    this.stack.bcH().pop();
                    return true;
                }
            } finally {
                this.stack.bcH().pop();
            }
        }

        /* renamed from: c */
        public boolean mo17754c(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
            this.stack.bcH().push();
            try {
                Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
                vec3f6.cross(vec3f2, vec3f3);
                Vec3f vec3f7 = (Vec3f) this.stack.bcH().get();
                vec3f7.cross(vec3f3, vec3f4);
                Vec3f vec3f8 = (Vec3f) this.stack.bcH().get();
                vec3f8.cross(vec3f4, vec3f2);
                if (vec3f6.dot(vec3f) > 1.0E-4f) {
                    vec3f5.set(vec3f6);
                    this.order = 2;
                    this.cXB[0].mo17758a(this.cXB[1]);
                    this.cXB[1].mo17758a(this.cXB[2]);
                    this.cXB[2].mo17758a(this.cXB[3]);
                    return mo17752b(vec3f, vec3f2, vec3f3, vec3f5);
                } else if (vec3f7.dot(vec3f) > 1.0E-4f) {
                    vec3f5.set(vec3f7);
                    this.order = 2;
                    this.cXB[2].mo17758a(this.cXB[3]);
                    boolean b = mo17752b(vec3f, vec3f3, vec3f4, vec3f5);
                    this.stack.bcH().pop();
                    return b;
                } else if (vec3f8.dot(vec3f) > 1.0E-4f) {
                    vec3f5.set(vec3f8);
                    this.order = 2;
                    this.cXB[1].mo17758a(this.cXB[0]);
                    this.cXB[0].mo17758a(this.cXB[2]);
                    this.cXB[2].mo17758a(this.cXB[3]);
                    boolean b2 = mo17752b(vec3f, vec3f4, vec3f2, vec3f5);
                    this.stack.bcH().pop();
                    return b2;
                } else {
                    this.stack.bcH().pop();
                    return true;
                }
            } finally {
                this.stack.bcH().pop();
            }
        }

        /* renamed from: MT */
        public boolean mo17747MT() {
            this.stack.bcH().push();
            try {
                return mo17757u(this.stack.bcH().mo4460h(1.0f, 0.0f, 0.0f));
            } finally {
                this.stack.bcH().pop();
            }
        }

        /* JADX INFO: finally extract failed */
        /* renamed from: u */
        public boolean mo17757u(Vec3f vec3f) {
            boolean c;
            this.stack.bcH().push();
            try {
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                this.iterations = 0;
                this.order = -1;
                this.failed = false;
                this.aBk.set(vec3f);
                this.aBk.normalize();
                Arrays.fill(this.cXz, (Object) null);
                mo17746MS();
                this.aBk.negate(this.cXB[0].f6483gv);
                while (true) {
                    if (this.iterations >= 128) {
                        this.failed = true;
                    } else {
                        float length = this.aBk.length();
                        Vec3f vec3f6 = this.aBk;
                        if (length <= 0.0f) {
                            length = 1.0f;
                        }
                        vec3f6.scale(1.0f / length);
                        if (mo17746MS()) {
                            switch (this.order) {
                                case 1:
                                    vec3f2.negate(this.cXB[1].f6483gv);
                                    vec3f3.sub(this.cXB[0].f6483gv, this.cXB[1].f6483gv);
                                    c = mo17756e(vec3f2, vec3f3);
                                    break;
                                case 2:
                                    vec3f2.negate(this.cXB[2].f6483gv);
                                    vec3f3.sub(this.cXB[1].f6483gv, this.cXB[2].f6483gv);
                                    vec3f4.sub(this.cXB[0].f6483gv, this.cXB[2].f6483gv);
                                    c = mo17753c(vec3f2, vec3f3, vec3f4);
                                    break;
                                case 3:
                                    vec3f2.negate(this.cXB[3].f6483gv);
                                    vec3f3.sub(this.cXB[2].f6483gv, this.cXB[3].f6483gv);
                                    vec3f4.sub(this.cXB[1].f6483gv, this.cXB[3].f6483gv);
                                    vec3f5.sub(this.cXB[0].f6483gv, this.cXB[3].f6483gv);
                                    c = mo17754c(vec3f2, vec3f3, vec3f4, vec3f5);
                                    break;
                                default:
                                    c = false;
                                    break;
                            }
                            if (c) {
                                this.stack.bcH().pop();
                                return true;
                            }
                            this.iterations++;
                        }
                    }
                }
                this.stack.bcH().pop();
                return false;
            } catch (Throwable th) {
                this.stack.bcH().pop();
                throw th;
            }
        }

        /* JADX INFO: finally extract failed */
        /* renamed from: MU */
        public boolean mo17748MU() {
            char c = 0;
            this.stack.bcF();
            this.stack.bcN().push();
            try {
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                switch (this.order) {
                    case 1:
                        Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                        vec3f4.sub(this.cXB[1].f6483gv, this.cXB[0].f6483gv);
                        Vec3f[] vec3fArr = {this.stack.bcH().mo4460h(1.0f, 0.0f, 0.0f), this.stack.bcH().mo4460h(0.0f, 1.0f, 0.0f), this.stack.bcH().mo4460h(0.0f, 0.0f, 1.0f)};
                        vec3fArr[0].cross(vec3f4, vec3fArr[0]);
                        vec3fArr[1].cross(vec3f4, vec3fArr[1]);
                        vec3fArr[2].cross(vec3f4, vec3fArr[2]);
                        float[] fArr = {vec3fArr[0].lengthSquared(), vec3fArr[1].lengthSquared(), vec3fArr[2].lengthSquared()};
                        Quat4f quat4f = (Quat4f) this.stack.bcN().get();
                        vec3f.normalize(vec3f4);
                        C1777aC.m13121a(quat4f, vec3f, 2.0943952f);
                        Matrix3fWrap ajd = (Matrix3fWrap) this.stack.bcK().get();
                        C3427rS.m38370a(ajd, quat4f);
                        C0988OY bcH = this.stack.bcH();
                        if (fArr[0] <= fArr[1]) {
                            c = fArr[1] > fArr[2] ? (char) 1 : 2;
                        } else if (fArr[0] <= fArr[2]) {
                            c = 2;
                        }
                        Vec3f ac = bcH.mo4458ac(vec3fArr[c]);
                        vec3f.normalize(ac);
                        mo17751a(vec3f, this.cXB[4]);
                        ajd.transform(ac);
                        vec3f.normalize(ac);
                        mo17751a(vec3f, this.cXB[2]);
                        ajd.transform(ac);
                        vec3f.normalize(ac);
                        mo17751a(vec3f, this.cXB[3]);
                        ajd.transform(ac);
                        this.order = 4;
                        break;
                    case 2:
                        vec3f2.sub(this.cXB[1].f6483gv, this.cXB[0].f6483gv);
                        vec3f3.sub(this.cXB[2].f6483gv, this.cXB[0].f6483gv);
                        Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                        vec3f5.cross(vec3f2, vec3f3);
                        vec3f5.normalize();
                        mo17751a(vec3f5, this.cXB[3]);
                        vec3f.negate(vec3f5);
                        mo17751a(vec3f, this.cXB[4]);
                        this.order = 4;
                        break;
                    case 3:
                    case 4:
                        break;
                    default:
                        this.stack.bcG();
                        this.stack.bcN().pop();
                        return false;
                }
                this.stack.bcG();
                this.stack.bcN().pop();
                return true;
            } catch (Throwable th) {
                this.stack.bcG();
                this.stack.bcN().pop();
                throw th;
            }
        }

        /* renamed from: a.dL$a$a */
        public static class C2250a {
            public final Vec3f aLZ = new Vec3f();
            public C2250a aMa;
        }

        /* renamed from: a.dL$a$b */
        /* compiled from: a */
        public static class C2251b {

            /* renamed from: gv */
            public final Vec3f f6483gv = new Vec3f();

            /* renamed from: gw */
            public final Vec3f f6484gw = new Vec3f();

            /* renamed from: a */
            public void mo17758a(C2251b bVar) {
                this.f6483gv.set(bVar.f6483gv);
                this.f6484gw.set(bVar.f6484gw);
            }
        }
    }

    /* renamed from: a.dL$b */
    /* compiled from: a */
    public static class C2252b {
        private static final int[][] dcY;
        private static final int[][] dcZ;
        private static final int[][] dda;
        private static final int[][] ddb;
        private static int[] dcX;

        static {
            int[] iArr = new int[5];
            iArr[1] = 1;
            iArr[2] = 2;
            iArr[4] = 1;
            dcX = iArr;
            int[] iArr2 = new int[3];
            iArr2[0] = 2;
            iArr2[1] = 1;
            int[] iArr3 = new int[3];
            iArr3[0] = 3;
            iArr3[2] = 1;
            int[] iArr4 = new int[3];
            iArr4[0] = 3;
            iArr4[1] = 2;
            dcY = new int[][]{iArr2, iArr3, new int[]{3, 1, 2}, iArr4};
            int[] iArr5 = new int[4];
            iArr5[2] = 2;
            iArr5[3] = 1;
            int[] iArr6 = new int[4];
            iArr6[1] = 1;
            iArr6[2] = 1;
            iArr6[3] = 1;
            int[] iArr7 = new int[4];
            iArr7[1] = 2;
            iArr7[2] = 3;
            iArr7[3] = 1;
            int[] iArr8 = new int[4];
            iArr8[0] = 1;
            iArr8[2] = 3;
            iArr8[3] = 2;
            int[] iArr9 = new int[4];
            iArr9[0] = 2;
            iArr9[2] = 1;
            iArr9[3] = 2;
            int[] iArr10 = new int[4];
            iArr10[0] = 3;
            iArr10[2] = 2;
            iArr10[3] = 2;
            dcZ = new int[][]{iArr5, iArr6, iArr7, iArr8, iArr9, iArr10};
            int[] iArr11 = new int[3];
            iArr11[0] = 2;
            iArr11[2] = 4;
            int[] iArr12 = new int[3];
            iArr12[0] = 1;
            iArr12[1] = 4;
            int[] iArr13 = new int[3];
            iArr13[1] = 3;
            iArr13[2] = 1;
            int[] iArr14 = new int[3];
            iArr14[1] = 2;
            iArr14[2] = 3;
            dda = new int[][]{iArr11, new int[]{4, 1, 2}, iArr12, iArr13, iArr14, new int[]{1, 3, 2}};
            int[] iArr15 = new int[4];
            iArr15[2] = 4;
            int[] iArr16 = new int[4];
            iArr16[1] = 1;
            iArr16[2] = 2;
            iArr16[3] = 1;
            int[] iArr17 = new int[4];
            iArr17[1] = 2;
            iArr17[2] = 1;
            iArr17[3] = 2;
            int[] iArr18 = new int[4];
            iArr18[0] = 1;
            iArr18[2] = 2;
            int[] iArr19 = new int[4];
            iArr19[0] = 3;
            iArr19[1] = 1;
            iArr19[2] = 5;
            int[] iArr20 = new int[4];
            iArr20[0] = 3;
            iArr20[2] = 4;
            iArr20[3] = 2;
            ddb = new int[][]{iArr15, iArr16, iArr17, new int[]{1, 1, 5, 2}, iArr18, new int[]{2, 2, 3, 2}, iArr19, iArr20, new int[]{5, 1, 4, 1}};
        }

        /* renamed from: Oj */
        public final Vec3f f6486Oj = new Vec3f();
        public final Vec3f[][] dcV = ((Vec3f[][]) Array.newInstance(Vec3f.class, new int[]{2, 3}));
        public final Vec3f[] dcW = {new Vec3f(), new Vec3f()};
        public final C0763Kt stack = C0763Kt.bcE();
        /* renamed from: Au */
        public C2249a f6485Au;
        /* renamed from: Ok */
        public float f6487Ok;
        public C2253a dcT;
        public int dcU;
        public boolean failed;
        public int iterations;

        public C2252b(C2249a aVar) {
            for (int i = 0; i < this.dcV.length; i++) {
                for (int i2 = 0; i2 < this.dcV[i].length; i2++) {
                    this.dcV[i][i2] = new Vec3f();
                }
            }
            this.f6485Au = aVar;
        }

        /* renamed from: a */
        public Vec3f mo17762a(C2253a aVar) {
            this.stack.bcH().push();
            try {
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                vec3f4.scale(-aVar.bak, aVar.baj);
                float[] qW = this.stack.bcO().mo1166qW(3);
                vec3f2.sub(aVar.bag[0].f6483gv, vec3f4);
                vec3f3.sub(aVar.bag[1].f6483gv, vec3f4);
                vec3f.cross(vec3f2, vec3f3);
                qW[0] = vec3f.length();
                vec3f2.sub(aVar.bag[1].f6483gv, vec3f4);
                vec3f3.sub(aVar.bag[2].f6483gv, vec3f4);
                vec3f.cross(vec3f2, vec3f3);
                qW[1] = vec3f.length();
                vec3f2.sub(aVar.bag[2].f6483gv, vec3f4);
                vec3f3.sub(aVar.bag[0].f6483gv, vec3f4);
                vec3f.cross(vec3f2, vec3f3);
                qW[2] = vec3f.length();
                float f = qW[0] + qW[1] + qW[2];
                Vec3f h = this.stack.bcH().mo4460h(qW[1], qW[2], qW[0]);
                if (f <= 0.0f) {
                    f = 1.0f;
                }
                h.scale(1.0f / f);
                this.stack.bcO().release(qW);
                return (Vec3f) this.stack.bcH().mo15197aq(h);
            } finally {
                this.stack.bcH().pop();
            }
        }

        public C2253a aSa() {
            C2253a aVar = null;
            if (this.dcT != null) {
                C2253a aVar2 = this.dcT;
                float f = Float.MAX_VALUE;
                do {
                    if (aVar2.bak < f) {
                        f = aVar2.bak;
                        aVar = aVar2;
                    }
                    aVar2 = aVar2.bam;
                } while (aVar2 != null);
            }
            return aVar;
        }

        /* renamed from: a */
        public boolean mo17764a(C2253a aVar, C2249a.C2251b bVar, C2249a.C2251b bVar2, C2249a.C2251b bVar3) {
            this.stack.bcH().push();
            try {
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                vec3f.sub(bVar2.f6483gv, bVar.f6483gv);
                vec3f2.sub(bVar3.f6483gv, bVar.f6483gv);
                vec3f4.cross(vec3f, vec3f2);
                float length = vec3f4.length();
                vec3f.cross(bVar.f6483gv, bVar2.f6483gv);
                vec3f2.cross(bVar2.f6483gv, bVar3.f6483gv);
                vec3f3.cross(bVar3.f6483gv, bVar.f6483gv);
                boolean z = vec3f.dot(vec3f4) >= -0.01f && vec3f2.dot(vec3f4) >= -0.01f && vec3f3.dot(vec3f4) >= -0.01f;
                aVar.bag[0] = bVar;
                aVar.bag[1] = bVar2;
                aVar.bag[2] = bVar3;
                aVar.mark = 0;
                aVar.baj.scale(1.0f / (length > 0.0f ? length : Float.MAX_VALUE), vec3f4);
                aVar.bak = Math.max(0.0f, -aVar.baj.dot(bVar.f6483gv));
                return z;
            } finally {
                this.stack.bcH().pop();
            }
        }

        /* renamed from: a */
        public C2253a mo17761a(C2249a.C2251b bVar, C2249a.C2251b bVar2, C2249a.C2251b bVar3) {
            C2253a aVar = C2248dL.f6470Ai.get();
            if (mo17764a(aVar, bVar, bVar2, bVar3)) {
                if (this.dcT != null) {
                    this.dcT.bal = aVar;
                }
                aVar.bal = null;
                aVar.bam = this.dcT;
                this.dcT = aVar;
                this.dcU++;
            } else {
                aVar.bam = null;
                aVar.bal = null;
            }
            return aVar;
        }

        /* renamed from: b */
        public void mo17767b(C2253a aVar) {
            if (aVar.bal != null || aVar.bam != null) {
                this.dcU--;
                if (aVar == this.dcT) {
                    this.dcT = aVar.bam;
                    this.dcT.bal = null;
                } else if (aVar.bam == null) {
                    aVar.bal.bam = null;
                } else {
                    aVar.bal.bam = aVar.bam;
                    aVar.bam.bal = aVar.bal;
                }
                aVar.bam = null;
                aVar.bal = null;
            }
        }

        /* renamed from: a */
        public void mo17763a(C2253a aVar, int i, C2253a aVar2, int i2) {
            aVar.bah[i] = aVar2;
            aVar2.bai[i2] = i;
            aVar2.bah[i2] = aVar;
            aVar.bai[i] = i2;
        }

        /* renamed from: S */
        public C2249a.C2251b mo17759S(Vec3f vec3f) {
            C2249a.C2251b bVar = C2248dL.f6468Ag.get();
            this.f6485Au.mo17751a(vec3f, bVar);
            return bVar;
        }

        /* renamed from: a */
        public int mo17760a(int i, C2249a.C2251b bVar, C2253a aVar, int i2, C2253a[] aVarArr, C2253a[] aVarArr2) {
            if (aVar.mark == i) {
                return 0;
            }
            int i3 = dcX[i2 + 1];
            if (aVar.baj.dot(bVar.f6483gv) + aVar.bak > 0.0f) {
                C2253a a = mo17761a(aVar.bag[i3], aVar.bag[i2], bVar);
                mo17763a(a, 0, aVar, i2);
                if (aVarArr[0] != null) {
                    mo17763a(aVarArr[0], 1, a, 2);
                } else {
                    aVarArr2[0] = a;
                }
                aVarArr[0] = a;
                return 1;
            }
            int i4 = dcX[i2 + 2];
            mo17767b(aVar);
            aVar.mark = i;
            return mo17760a(i, bVar, aVar.bah[i4], aVar.bai[i4], aVarArr, aVarArr2) + 0 + mo17760a(i, bVar, aVar.bah[i3], aVar.bai[i3], aVarArr, aVarArr2);
        }

        public float aSb() {
            return mo17768fu(C2248dL.f6481At);
        }

        /* renamed from: fu */
        public float mo17768fu(float f) {
            C2253a aVar;
            float f2;
            C2253a aSa;
            int[][] iArr;
            int[][] iArr2;
            this.stack.bcH().push();
            C2248dL.m28791kh();
            try {
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                this.f6487Ok = -3.4028235E38f;
                this.f6486Oj.set(0.0f, 0.0f, 0.0f);
                this.dcT = null;
                this.dcU = 0;
                this.iterations = 0;
                this.failed = false;
                if (this.f6485Au.mo17748MU()) {
                    int[][] iArr3 = null;
                    int i = 0;
                    int i2 = 0;
                    int[][] iArr4 = null;
                    int i3 = 0;
                    int i4 = 0;
                    C2249a.C2251b[] bVarArr = new C2249a.C2251b[5];
                    C2253a[] aVarArr = new C2253a[6];
                    switch (this.f6485Au.order) {
                        case 3:
                            iArr = dcY;
                            i = 0;
                            i2 = 4;
                            iArr2 = dcZ;
                            i4 = 6;
                            i3 = 0;
                            break;
                        case 4:
                            iArr = dda;
                            i = 0;
                            i2 = 6;
                            iArr2 = ddb;
                            i4 = 9;
                            i3 = 0;
                            break;
                        default:
                            iArr2 = iArr4;
                            iArr = iArr3;
                            break;
                    }
                    for (int i5 = 0; i5 <= this.f6485Au.order; i5++) {
                        bVarArr[i5] = new C2249a.C2251b();
                        bVarArr[i5].mo17758a(this.f6485Au.cXB[i5]);
                    }
                    int i6 = 0;
                    while (i6 < i2) {
                        aVarArr[i6] = mo17761a(bVarArr[iArr[i][0]], bVarArr[iArr[i][1]], bVarArr[iArr[i][2]]);
                        i6++;
                        i++;
                    }
                    int i7 = 0;
                    while (i7 < i4) {
                        mo17763a(aVarArr[iArr2[i3][0]], iArr2[i3][1], aVarArr[iArr2[i3][2]], iArr2[i3][3]);
                        i7++;
                        i3++;
                    }
                }
                if (this.dcU == 0) {
                    f2 = this.f6487Ok;
                } else {
                    int i8 = 1;
                    C2253a aVar2 = null;
                    while (true) {
                        if (this.iterations < 256 && (aSa = aSa()) != null) {
                            vec3f.negate(aSa.baj);
                            C2249a.C2251b S = mo17759S(vec3f);
                            if (aSa.baj.dot(S.f6483gv) + aSa.bak < (-f)) {
                                C2253a[] aVarArr2 = new C2253a[1];
                                C2253a[] aVarArr3 = new C2253a[1];
                                mo17767b(aSa);
                                i8++;
                                aSa.mark = i8;
                                int i9 = 0;
                                int i10 = 0;
                                while (i9 < 3) {
                                    i9++;
                                    i10 += mo17760a(i8, S, aSa.bah[i9], aSa.bai[i9], aVarArr2, aVarArr3);
                                }
                                if (i10 <= 2) {
                                    aVar = aSa;
                                } else {
                                    mo17763a(aVarArr2[0], 1, aVarArr3[0], 2);
                                    this.iterations++;
                                    aVar2 = aSa;
                                }
                            } else {
                                aVar = aSa;
                            }
                        }
                    }
                    aVar = aVar2;
                    if (aVar != null) {
                        Vec3f ac = this.stack.bcH().mo4458ac(mo17762a(aVar));
                        this.f6486Oj.set(aVar.baj);
                        this.f6487Ok = Math.max(0.0f, aVar.bak);
                        int i11 = 0;
                        while (i11 < 2) {
                            float f3 = i11 != 0 ? -1.0f : 1.0f;
                            for (int i12 = 0; i12 < 3; i12++) {
                                vec3f.scale(f3, aVar.bag[i12].f6484gw);
                                this.dcV[i11][i12].set(this.f6485Au.mo17749a(vec3f, i11));
                            }
                            i11++;
                        }
                        Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
                        Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                        Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                        vec3f2.scale(ac.x, this.dcV[0][0]);
                        vec3f3.scale(ac.y, this.dcV[0][1]);
                        vec3f4.scale(ac.z, this.dcV[0][2]);
                        C0647JL.m5602f(this.dcW[0], vec3f2, vec3f3, vec3f4);
                        vec3f2.scale(ac.x, this.dcV[1][0]);
                        vec3f3.scale(ac.y, this.dcV[1][1]);
                        vec3f4.scale(ac.z, this.dcV[1][2]);
                        C0647JL.m5602f(this.dcW[1], vec3f2, vec3f3, vec3f4);
                    } else {
                        this.failed = true;
                    }
                    f2 = this.f6487Ok;
                    this.stack.bcH().pop();
                    C2248dL.m28792ki();
                }
                return f2;
            } finally {
                this.stack.bcH().pop();
                C2248dL.m28792ki();
            }
        }

        /* renamed from: a.dL$b$a */
        public static class C2253a {
            public final C2249a.C2251b[] bag = new C2249a.C2251b[3];
            public final C2253a[] bah = new C2253a[3];
            public final int[] bai = new int[3];
            public final Vec3f baj = new Vec3f();
            public float bak;
            public C2253a bal;
            public C2253a bam;
            public int mark;
        }
    }
}
