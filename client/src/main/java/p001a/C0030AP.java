package p001a;

/* renamed from: a.AP */
/* compiled from: a */
public class C0030AP {
    private long swigCPtr;

    public C0030AP(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C0030AP() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m384a(C0030AP ap) {
        if (ap == null) {
            return 0;
        }
        return ap.swigCPtr;
    }
}
