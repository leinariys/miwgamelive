package p001a;

import com.hoplon.geometry.Vec3f;

import javax.vecmath.Vector4f;

/* renamed from: a.JL */
/* compiled from: a */
public class C0647JL {
    /* renamed from: W */
    public static int m5588W(Vec3f vec3f) {
        int i = -1;
        float f = -1.0E30f;
        if (vec3f.x > -1.0E30f) {
            i = 0;
            f = vec3f.x;
        }
        if (vec3f.y > f) {
            i = 1;
            f = vec3f.y;
        }
        if (vec3f.z <= f) {
            return i;
        }
        float f2 = vec3f.z;
        return 2;
    }

    /* renamed from: b */
    public static int m5596b(Vector4f vector4f) {
        int i = -1;
        float f = -1.0E30f;
        if (vector4f.x > -1.0E30f) {
            i = 0;
            f = vector4f.x;
        }
        if (vector4f.y > f) {
            i = 1;
            f = vector4f.y;
        }
        if (vector4f.z > f) {
            i = 2;
            f = vector4f.z;
        }
        if (vector4f.w <= f) {
            return i;
        }
        float f2 = vector4f.w;
        return 3;
    }

    /* renamed from: c */
    public static int m5598c(Vector4f vector4f) {
        Vector4f vector4f2 = new Vector4f(vector4f);
        vector4f2.absolute();
        return m5596b(vector4f2);
    }

    /* renamed from: b */
    public static float m5594b(Vec3f vec3f, int i) {
        switch (i) {
            case 0:
                return vec3f.x;
            case 1:
                return vec3f.y;
            case 2:
                return vec3f.z;
            default:
                throw new InternalError();
        }
    }

    /* renamed from: a */
    public static void m5590a(Vec3f vec3f, int i, float f) {
        switch (i) {
            case 0:
                vec3f.x = f;
                return;
            case 1:
                vec3f.y = f;
                return;
            case 2:
                vec3f.z = f;
                return;
            default:
                throw new InternalError();
        }
    }

    /* renamed from: b */
    public static void m5597b(Vec3f vec3f, int i, float f) {
        switch (i) {
            case 0:
                vec3f.x *= f;
                return;
            case 1:
                vec3f.y *= f;
                return;
            case 2:
                vec3f.z *= f;
                return;
            default:
                throw new InternalError();
        }
    }

    /* renamed from: a */
    public static void m5591a(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, float f) {
        float f2 = 1.0f - f;
        vec3f.x = (vec3f2.x * f2) + (vec3f3.x * f);
        vec3f.y = (vec3f2.y * f2) + (vec3f3.y * f);
        vec3f.z = (f2 * vec3f2.z) + (vec3f3.z * f);
    }

    /* renamed from: f */
    public static void m5601f(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        vec3f.x = vec3f2.x + vec3f3.x;
        vec3f.y = vec3f2.y + vec3f3.y;
        vec3f.z = vec3f2.z + vec3f3.z;
    }

    /* renamed from: f */
    public static void m5602f(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
        vec3f.x = vec3f2.x + vec3f3.x + vec3f4.x;
        vec3f.y = vec3f2.y + vec3f3.y + vec3f4.y;
        vec3f.z = vec3f2.z + vec3f3.z + vec3f4.z;
    }

    /* renamed from: a */
    public static void m5592a(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4, Vec3f vec3f5) {
        vec3f.x = vec3f2.x + vec3f3.x + vec3f4.x + vec3f5.x;
        vec3f.y = vec3f2.y + vec3f3.y + vec3f4.y + vec3f5.y;
        vec3f.z = vec3f2.z + vec3f3.z + vec3f4.z + vec3f5.z;
    }

    /* renamed from: g */
    public static void m5603g(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        vec3f.x = vec3f2.x * vec3f3.x;
        vec3f.y = vec3f2.y * vec3f3.y;
        vec3f.z = vec3f2.z * vec3f3.z;
    }

    /* renamed from: h */
    public static void m5604h(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        vec3f.x = vec3f2.x / vec3f3.x;
        vec3f.y = vec3f2.y / vec3f3.y;
        vec3f.z = vec3f2.z / vec3f3.z;
    }

    /* renamed from: o */
    public static void m5605o(Vec3f vec3f, Vec3f vec3f2) {
        vec3f.x = Math.min(vec3f.x, vec3f2.x);
        vec3f.y = Math.min(vec3f.y, vec3f2.y);
        vec3f.z = Math.min(vec3f.z, vec3f2.z);
    }

    /* renamed from: p */
    public static void m5606p(Vec3f vec3f, Vec3f vec3f2) {
        vec3f.x = Math.max(vec3f.x, vec3f2.x);
        vec3f.y = Math.max(vec3f.y, vec3f2.y);
        vec3f.z = Math.max(vec3f.z, vec3f2.z);
    }

    /* renamed from: a */
    public static float m5589a(Vector4f vector4f, Vec3f vec3f) {
        return (vector4f.x * vec3f.x) + (vector4f.y * vec3f.y) + (vector4f.z * vec3f.z);
    }

    /* renamed from: b */
    public static float m5595b(Vector4f vector4f, Vector4f vector4f2) {
        return (vector4f.x * vector4f2.x) + (vector4f.y * vector4f2.y) + (vector4f.z * vector4f2.z);
    }

    /* renamed from: d */
    public static float m5599d(Vector4f vector4f) {
        return (vector4f.x * vector4f.x) + (vector4f.y * vector4f.y) + (vector4f.z * vector4f.z);
    }

    /* renamed from: e */
    public static void m5600e(Vector4f vector4f) {
        float sqrt = (float) (1.0d / Math.sqrt((double) (((vector4f.x * vector4f.x) + (vector4f.y * vector4f.y)) + (vector4f.z * vector4f.z))));
        vector4f.x *= sqrt;
        vector4f.y *= sqrt;
        vector4f.z = sqrt * vector4f.z;
    }

    /* renamed from: a */
    public static void m5593a(Vec3f vec3f, Vector4f vector4f, Vector4f vector4f2) {
        float f = (vector4f.y * vector4f2.z) - (vector4f.z * vector4f2.y);
        float f2 = (vector4f2.x * vector4f.z) - (vector4f2.z * vector4f.x);
        vec3f.z = (vector4f.x * vector4f2.y) - (vector4f.y * vector4f2.x);
        vec3f.x = f;
        vec3f.y = f2;
    }
}
