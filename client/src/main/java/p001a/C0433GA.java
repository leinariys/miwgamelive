package p001a;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/* renamed from: a.GA */
/* compiled from: a */
public class C0433GA extends C0686Jm {
    public C0433GA(C6781atZ atz) {
        super(atz);
    }

    /**
     * @param url file:/C:/Projects/Java/TaikodomGameLIVE/out/production/TaikodomGameLIVE/taikodom/infra/script/InfraScripts
     * @throws IOException
     */
    /* renamed from: d */
    public void mo2248d(URL url) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream(), "utf-8"));
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                String trim = readLine.trim();
                if (!trim.startsWith("#") && trim.length() > 0) {
                    mo3262eD(trim.trim());
                }
            } else {
                return;
            }
        }
    }
}
