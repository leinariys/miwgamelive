package p001a;

import game.geometry.Vec3d;
import logic.baa.C1616Xf;
import logic.res.code.C2759jd;

import java.lang.reflect.Field;

/* renamed from: a.ahr  reason: case insensitive filesystem */
/* compiled from: a */
public class C6175ahr extends C2759jd {


    public C6175ahr(Class<?> cls, Field field, String str, Class<?>[] clsArr) {
        super(cls, field, str, clsArr);
    }

    /* renamed from: z */
    public Object mo2181z(Object obj) {
        Vec3d ajr = (Vec3d) obj;
        return new Vec3d(ajr.x, ajr.y, ajr.z);
    }

    /* renamed from: yl */
    public boolean mo2180yl() {
        return true;
    }

    /* renamed from: b */
    public Object mo2188b(C1616Xf xf) {
        return null;
    }

    /* renamed from: d */
    public Object mo11307d(Object obj, Object obj2) {
        return (obj == null || !obj.equals(obj2)) ? obj2 : obj;
    }
}
