package p001a;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/* renamed from: a.aBG */
/* compiled from: a */
public class aBG {
    private File file;

    /* renamed from: hP */
    private List<C0599IT> f2403hP = new ArrayList();
    private Set<String> includes = new LinkedHashSet();
    private String relativePath;

    /* renamed from: a */
    public void mo7825a(C0599IT it) {
        getItems().add(it);
    }

    public List<C0599IT> getItems() {
        return this.f2403hP;
    }

    /* renamed from: kZ */
    public void mo7831kZ(String str) {
        this.relativePath = str;
    }

    public String cJo() {
        return this.relativePath;
    }

    public File getFile() {
        return this.file;
    }

    public void setFile(File file2) {
        this.file = file2;
    }

    /* renamed from: dS */
    public void mo7828dS(String str) {
        cJp().add(str);
    }

    public Set<String> cJp() {
        return this.includes;
    }
}
