package p001a;

import game.script.item.Shot;
import game.script.simulation.Space;
import logic.bbb.C4029yK;
import logic.thred.C1362Tq;

import java.util.Collection;

/* renamed from: a.Ts */
/* compiled from: a */
public class C1364Ts extends C0520HN {
    private final double eiX = this.cYX.blk().x;
    private final double eiY = this.cYX.blk().y;
    private final double eiZ = this.cYX.blk().z;
    private final float eja;

    public C1364Ts(Space ea, Shot cAVar, C4029yK yKVar, float f) {
        super(ea, cAVar, yKVar);
        this.eja = f * f;
    }

    public void step(float f) {
        if (this.eja < this.cYX.blk().mo9542t(this.eiX, this.eiY, this.eiZ)) {
            mo2568ha().mo1099zx();
        } else {
            super.step(f);
        }
    }

    public boolean aQZ() {
        return false;
    }

    /* renamed from: b */
    public void mo2462b(int i, long j, Collection<C6625aqZ> collection, C6705asB asb) {
    }

    /* renamed from: IM */
    public byte mo2438IM() {
        return 2;
    }

    /* renamed from: a */
    public boolean mo2545a(C0520HN hn) {
        if (!(hn instanceof C1084Ps) || ((Turret) hn.aSp()).aZS() != mo2568ha().cLw()) {
            return mo2568ha().cLw() != hn.mo2568ha();
        }
        return false;
    }

    /* renamed from: a */
    public void mo2543a(C1362Tq tq) {
        super.mo2543a(tq);
    }

    /* renamed from: IS */
    public Object mo2441IS() {
        return C1364Ts.class;
    }
}
