package p001a;

import logic.bbb.C4029yK;
import logic.thred.C1362Tq;
import taikodom.render.camera.Camera;

import java.util.ArrayList;
import java.util.Collection;

/* renamed from: a.ajf  reason: case insensitive filesystem */
/* compiled from: a */
public class C6267ajf extends C0501Gx implements C0461GN {
    ArrayList<C6625aqZ> fOW = new ArrayList<>();
    private Camera camera;
    private C3387qy<C0461GN> ddk;
    private float range = 1000.0f;

    public C6267ajf(C4029yK yKVar, Camera camera2) {
        super(yKVar);
        this.camera = camera2;
    }

    public boolean aQZ() {
        return false;
    }

    /* renamed from: b */
    public void mo2462b(int i, long j, Collection<C6625aqZ> collection, C6705asB asb) {
    }

    public void step(float f) {
        super.step(f);
        if (this.cYY != null) {
            this.cYY.mo3841kO();
        }
        if (this.ddk != null) {
            this.ddk.mo2339kO();
        }
        this.camera.setTransform(mo2472kQ().mo4490IQ());
    }

    /* renamed from: a */
    public void mo2543a(C1362Tq tq) {
    }

    /* renamed from: a */
    public void mo14160a(C3387qy<C0461GN> qyVar) {
        this.ddk = qyVar;
    }

    public Collection<C6625aqZ> aRu() {
        return this.fOW;
    }

    /* renamed from: in */
    public float mo2337in() {
        return this.range;
    }

    /* renamed from: jD */
    public void mo14161jD(float f) {
        this.range = f;
    }

    public boolean isReady() {
        return true;
    }
}
