package p001a;

import com.hoplon.geometry.Color;
import gametoolkit.client.scene.render.ImageFloatImpl;
import gametoolkit.client.scene.render.TextureImpl;
import logic.res.scene.C1710ZI;
import logic.res.scene.C2036bO;
import logic.res.scene.C3038nJ;
import logic.res.scene.C6980axt;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/* renamed from: a.aHE */
/* compiled from: a */
public class aHE implements C3915wj {
    private static final Log logger = LogFactory.getLog(aHE.class);
    private final Color color;
    private final C2036bO dOK;
    private final String gXl;
    private final int hYB;

    public aHE(String str, int i, Color color2, C2036bO bOVar) {
        this.gXl = str;
        this.hYB = i;
        this.color = color2;
        this.dOK = bOVar;
    }

    public void execute() {
        C2036bO bHA = new aUD(this.gXl).mo7458j(this.dOK).bHA();
        if (bHA == null) {
            C5226aBe abe = new C5226aBe("Render object not found: " + this.gXl);
            logger.error(abe);
            throw abe;
        } else if (!(bHA instanceof C6980axt)) {
            C5226aBe abe2 = new C5226aBe("Specified object '" + this.gXl + "' must be an instance of RenderObject instead of '" + bHA.getClass().getSimpleName() + "'");
            logger.error(abe2);
            throw abe2;
        } else {
            C3038nJ YU = ((C6980axt) bHA).mo16837YU();
            if (YU == null) {
                C5226aBe abe3 = new C5226aBe("Render object '" + this.gXl + "' has no material");
                logger.error(abe3);
                throw abe3;
            }
            C1710ZI A = YU.mo20684A(this.hYB);
            if (A == null) {
                C5226aBe abe4 = new C5226aBe("Render object '" + this.gXl + "' has no texture binded in the specified channel '" + this.hYB + "'");
                logger.error(abe4);
                throw abe4;
            }
            ImageFloatImpl imageFloatImpl = new ImageFloatImpl(A.getSizeX(), A.getSizeY(), A.getBpp() / 8);
            int dcx = imageFloatImpl.dcx();
            for (int i = 0; i < dcx; i++) {
                imageFloatImpl.mo11945a(i, this.color);
            }
            TextureImpl textureImpl = new TextureImpl();
            textureImpl.mo7349a(A.getTarget(), A.mo7344Xn(), A.mo7345Xo(), A.mo7341Xk(), A.mo7342Xl(), imageFloatImpl);
            YU.mo20686a(this.hYB, (C1710ZI) textureImpl);
            YU.mo20809PR();
        }
    }
}
