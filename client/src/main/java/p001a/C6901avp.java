package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vector2fWrap;
import game.network.message.serializable.C6400amI;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.simulation.Space;
import logic.bbb.C4029yK;

import java.util.Collection;
import java.util.HashSet;

/* renamed from: a.avp  reason: case insensitive filesystem */
/* compiled from: a */
public class C6901avp extends C1714ZM implements C0461GN {
    private C1255SZ ajg = null;
    private boolean fbN;
    private C5917act gHc = new C5917act();
    private HashSet<C6625aqZ> gHd = new HashSet<>();
    private HashSet<C6625aqZ> gHe = new HashSet<>();
    private Vec3f gHf = new Vec3f(0.0f, 0.0f, -1.0f);
    private float gHg;
    private Vec3f gxO = new Vec3f(0.0f, 0.0f, -1.0f);

    public C6901avp(Space ea, Ship fAVar, C4029yK yKVar) {
        super(ea, fAVar, yKVar);
        this.gHc.mo12728o(fAVar.mo962VF());
        mo2560as(fAVar.mo963VH());
        this.gHc.mo12731q(fAVar.mo1092rb());
        this.gHc.mo12732r(fAVar.mo1091ra());
        this.gHc.mo12724iU(fAVar.agf());
        this.gHc.mo12723iT(fAVar.agd());
        mo2450a((C3735uM) this.gHc);
        this.gHg = fAVar.air();
        if (this.gHg <= 0.0f) {
            this.gHg = 0.2f;
        }
    }

    /* renamed from: b */
    public boolean mo2564b(C0520HN hn) {
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo2448a(float f, C6400amI ami) {
        super.mo2448a(f, ami);
        Vector2fWrap aka = new Vector2fWrap(0.0f, 0.0f);
        float radians = ((float) (Math.toRadians((double) ahA().mo963VH()) * ((double) f))) * this.gHg;
        float angle = this.gHf.angle(this.gxO);
        Vec3f j = this.gxO.mo23506j(this.gHf);
        aka.add((double) j.x, (double) j.y);
        if (angle <= radians) {
            this.gHf.set(this.gxO);
            return;
        }
        j.scale(Math.min(1.0f, Math.max(aka.length() * (radians / angle) * 25.0f, radians)));
        this.gHf.add(j);
        this.gHf.normalize();
    }

    private Ship ahA() {
        return (Ship) mo2568ha();
    }

    public C1255SZ czc() {
        return this.ajg;
    }

    /* renamed from: a */
    public void mo16630a(C1255SZ sz) {
        this.ajg = sz;
    }

    /* renamed from: in */
    public float mo2337in() {
        return ahA().agp();
    }

    /* renamed from: IM */
    public byte mo2438IM() {
        return 4;
    }

    public void step(float f) {
        super.step(f);
        if ((ahA().mo2998hb() instanceof PlayerController) && ahA().bGX() && ahA().ald().mo4089dL() == ahA().agj()) {
            ((PlayerController) ahA().mo2998hb()).step(f);
        }
    }

    public Collection<C6625aqZ> aRu() {
        this.gHe.clear();
        if (ahA().agj() instanceof Player) {
            Player aku = (Player) ahA().agj();
            this.gHe.add(aku);
            if (aku.dxi() != null) {
                for (Player next : aku.dxi().aQS()) {
                    if (next.bQx() != null && next.bQx().azW() == aku.bQx().azW()) {
                        this.gHe.add(next);
                    }
                }
            }
        }
        if (!this.gHe.equals(this.gHd)) {
            wake();
            this.gHd.clear();
            this.gHd.addAll(this.gHe);
        }
        return this.gHd;
    }

    /* renamed from: as */
    public void mo2560as(float f) {
        float afZ = ahA().afZ();
        float agb = ahA().agb();
        wake();
        this.gHc.mo12709ae(new Vec3f(afZ * f, f, agb * f));
    }

    /* renamed from: aX */
    public void mo16631aX(Vec3f vec3f) {
        this.gHf.set(vec3f);
    }

    public Vec3f czd() {
        return this.gHf;
    }

    /* renamed from: E */
    public void mo16629E(Vec3f vec3f) {
        this.gxO.set(vec3f);
        this.gxO.normalize();
        wake();
    }

    public boolean isReady() {
        if (ahA().agj() instanceof Player) {
            return ((Player) ahA().agj()).isReady();
        }
        return true;
    }

    public boolean aXW() {
        return this.fbN;
    }

    /* renamed from: ec */
    public void mo16635ec(boolean z) {
        this.fbN = z;
    }
}
