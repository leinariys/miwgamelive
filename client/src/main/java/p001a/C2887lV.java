package p001a;

import com.hoplon.geometry.Vec3f;
import logic.abc.C1226SE;
import logic.abc.C1468Vc;
import logic.abc.C5804aak;

/* renamed from: a.lV */
/* compiled from: a */
public class C2887lV extends C3808vG {
    private boolean bzz;
    private C5965adp fKt;

    public C2887lV(C1034PA pa, ayY ayy, ayY ayy2, boolean z) {
        super(pa);
        this.bzz = z;
        this.fKt = new C5965adp(this.f9400Uy, ayy, ayy2, z);
    }

    public void destroy() {
        this.fKt.destroy();
    }

    /* renamed from: a */
    public void mo8931a(ayY ayy, ayY ayy2, C5408aIe aie, C5362aGk agk) {
        ayY ayy3 = this.bzz ? ayy2 : ayy;
        if (!this.bzz) {
            ayy = ayy2;
        }
        if (ayy.cEZ().aiG()) {
            C1468Vc vc = (C1468Vc) ayy.cEZ();
            if (ayy3.cEZ().aiF()) {
                float margin = vc.getMargin();
                agk.mo9117e(this.fKt.f4338Ql);
                this.fKt.mo12896a(margin, aie, agk);
                this.fKt.f4338Ql.mo9018j(ayy3, ayy);
                vc.mo2381a(this.fKt, this.fKt.asv(), this.fKt.asw());
                agk.dad();
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: b */
    public float mo8932b(ayY ayy, ayY ayy2, C5408aIe aie, C5362aGk agk) {
        this.stack.bcF();
        try {
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            ayY ayy3 = this.bzz ? ayy2 : ayy;
            if (!this.bzz) {
                ayy = ayy2;
            }
            vec3f.sub(ayy3.cFh().bFG, ayy3.cFf().bFG);
            if (vec3f.lengthSquared() >= ayy3.cFp()) {
                C3978xf d = this.stack.bcJ().mo1157d(ayy.cFf());
                d.inverse();
                C3978xf xfVar = (C3978xf) this.stack.bcJ().get();
                xfVar.mo22948a(d, ayy3.cFf());
                C3978xf xfVar2 = (C3978xf) this.stack.bcJ().get();
                xfVar2.mo22948a(d, ayy3.cFh());
                if (ayy.cEZ().aiG()) {
                    Vec3f ac = this.stack.bcH().mo4458ac(xfVar.bFG);
                    C0647JL.m5605o(ac, xfVar2.bFG);
                    Vec3f ac2 = this.stack.bcH().mo4458ac(xfVar.bFG);
                    C0647JL.m5606p(ac2, xfVar2.bFG);
                    float cFo = ayy3.cFo();
                    vec3f.set(cFo, cFo, cFo);
                    ac.sub(vec3f);
                    ac2.add(vec3f);
                    C2888a aVar = new C2888a(xfVar, xfVar2, ayy3.cFo(), 1.0f);
                    aVar.f8535r = ayy3.cFm();
                    C1468Vc vc = (C1468Vc) ayy.cEZ();
                    if (vc != null) {
                        vc.mo2381a(aVar, ac, ac2);
                    }
                    if (aVar.f8535r < ayy3.cFm()) {
                        ayy3.mo17047ld(aVar.f8535r);
                        float f = aVar.f8535r;
                        this.stack.bcG();
                        return f;
                    }
                }
            }
            this.stack.bcG();
            return 1.0f;
        } catch (Throwable th) {
            this.stack.bcG();
            throw th;
        }
    }

    public void clearCache() {
        this.fKt.clearCache();
    }

    /* renamed from: a.lV$a */
    private static class C2888a implements aTC {
        public final C3978xf aAE = new C3978xf();
        public final C3978xf aAF = new C3978xf();
        public final C3978xf aAG = new C3978xf();
        private final C3978xf aAI = new C3978xf();
        public float aAH;
        /* renamed from: r */
        public float f8535r;

        public C2888a(C3978xf xfVar, C3978xf xfVar2, float f, float f2) {
            this.aAE.mo22947a(xfVar);
            this.aAF.mo22947a(xfVar2);
            this.aAH = f;
            this.f8535r = f2;
            this.aAI.setIdentity();
        }

        /* renamed from: a */
        public void mo820a(Vec3f[] vec3fArr, int i, int i2) {
            C5915acr.C1866a aVar = new C5915acr.C1866a();
            aVar.fkF = this.f8535r;
            if (new C5253aCf(new C5804aak(this.aAH), new C1226SE(vec3fArr[0], vec3fArr[1], vec3fArr[2]), new C0327ES()).mo8215a(this.aAE, this.aAF, this.aAI, this.aAI, aVar) && this.f8535r > aVar.fkF) {
                this.f8535r = aVar.fkF;
            }
        }
    }

    /* renamed from: a.lV$b */
    /* compiled from: a */
    public static class C2889b extends C6548apA {
        /* renamed from: a */
        public C3808vG mo8207a(C1034PA pa, ayY ayy, ayY ayy2) {
            return new C2887lV(pa, ayy, ayy2, false);
        }
    }

    /* renamed from: a.lV$c */
    /* compiled from: a */
    public static class C2890c extends C6548apA {
        /* renamed from: a */
        public C3808vG mo8207a(C1034PA pa, ayY ayy, ayY ayy2) {
            return new C2887lV(pa, ayy, ayy2, true);
        }
    }
}
