package p001a;

import taikodom.render.helpers.RenderVisitor;
import taikodom.render.scene.SceneObject;

/* renamed from: a.pn */
/* compiled from: a */
class C3266pn implements RenderVisitor {
    C3266pn() {
    }

    public boolean visit(SceneObject sceneObject) {
        sceneObject.setDisposeWhenEmpty(true);
        return true;
    }
}
