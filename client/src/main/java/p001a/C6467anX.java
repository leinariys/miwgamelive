package p001a;

import logic.WrapRunnable;
import taikodom.addon.p002fx.CollisionFxAddon;

import java.util.ArrayList;
import java.util.Map;

/* renamed from: a.anX  reason: case insensitive filesystem */
/* compiled from: a */
class C6467anX extends WrapRunnable {
    final /* synthetic */ CollisionFxAddon bth;

    public C6467anX(CollisionFxAddon collisionFxAddon) {
        this.bth = collisionFxAddon;
    }

    public void run() {
        long currentTimeMillis = System.currentTimeMillis();
        ArrayList<CollisionFxAddon.C4401a> arrayList = new ArrayList<>();
        for (Map.Entry entry : this.bth.f9883Rk.entrySet()) {
            if (((Long) entry.getValue()).longValue() + CollisionFxAddon.f9878Rl < currentTimeMillis) {
                arrayList.add((CollisionFxAddon.C4401a) entry.getKey());
            }
        }
        for (CollisionFxAddon.C4401a remove : arrayList) {
            this.bth.f9883Rk.remove(remove);
        }
    }
}
