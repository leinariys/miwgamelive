package p001a;

import game.engine.DataGameEvent;
import game.engine.SocketMessage;
import game.io.IExternalIO;
import game.network.message.ByteMessageReader;
import game.network.message.IReadProto;
import game.network.message.externalizable.C0722KO;
import game.network.message.serializable.C1695Yw;
import logic.data.mbean.C0677Jd;
import logic.data.mbean.C6064afk;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import taikodom.render.loader.provider.FilePath;
import util.Syst;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Arrays;

/* renamed from: a.aJd  reason: case insensitive filesystem */
/* compiled from: a */
public class C5433aJd implements WraperDbFile {
    private static final Log log = LogPrinter.setClass(C5433aJd.class);
    private int ifi;
    private int ifj;
    private int ifk;
    private int ifl;
    private int ifm;
    private int ifn;
    private C1412Uh ifo;

    public boolean bOc() {
        this.ifi++;
        return true;
    }

    /* renamed from: a */
    private void m15797a(C6064afk afk, byte[] bArr) {
        aPZ apz = new aPZ((DataGameEvent) this.ifo, (IReadProto) new ByteMessageReader(bArr), false, false);
        try {
            Class<?> cls = (Class) apz.mo6366hd("class");
            apz.mo6369hg(C0891Mw.DATA);
            if (cls != afk.mo11944yn().getClass()) {
                log.error("EXTREMELY DANGEROUS ERROR: wrong class " + cls + " for " + afk.mo11944yn().bFf().bFY());
            }
            ((IExternalIO) afk).readExternal(apz);
            apz.mo6370pe();
        } catch (ClassNotFoundException e) {
            try {
                new C2683iW().mo19626a((IReadProto) new ByteMessageReader(bArr));
            } catch (Exception e2) {
            }
            throw new CountFailedReadOrWriteException("Error reading object: " + afk.mo11944yn().bFf().bFY(), e);
        } catch (Exception e3) {
            try {
                new C2683iW().mo19626a((IReadProto) new ByteMessageReader(bArr));
            } catch (Exception e4) {
            }
            throw new CountFailedReadOrWriteException((Throwable) e3);
        }
    }

    public void flush() {
        this.ifj++;
    }

    public int dfx() {
        return this.ifi;
    }

    public int dfy() {
        return this.ifk;
    }

    public int dfz() {
        return this.ifj;
    }

    public int dfA() {
        return this.ifm;
    }

    public int dfB() {
        return this.ifn;
    }

    public int dfC() {
        return this.ifl;
    }

    public void init() {
    }

    public boolean bOd() {
        return false;
    }

    /* renamed from: h */
    public void mo5236h(C3582se seVar) {
    }

    public void bOe() {
        this.ifm++;
    }

    public void bOf() {
        this.ifn++;
    }

    /* renamed from: i */
    public void mo5238i(C3582se seVar) {
        C0677Jd jd = (C0677Jd) seVar.cVo();
        byte[] f = m15798f((C6064afk) jd);
        C0677Jd jd2 = (C0677Jd) jd.mo11944yn().mo13W();
        m15797a(jd2, f);
        byte[] f2 = m15798f((C6064afk) jd2);
        if (!Arrays.equals(f, f2)) {
            String str = "no match for serialization-deserialization-serialization test of " + seVar.bFY() + " version " + jd.aBt();
            log.error(str);
            System.out.println(Syst.byteToStringLog(f, 0, f.length, " ", 16));
            System.out.println(Syst.byteToStringLog(f2, 0, f2.length, " ", 16));
            throw new IOException(str);
        }
    }

    /* renamed from: c */
    public void mo5226c(C1695Yw yw) {
        int size = yw.bHG().size();
        while (true) {
            int i = size - 1;
            if (i >= 0) {
                C0722KO ko = yw.bHG().get(i);
                C3582se seVar = (C3582se) ko.bdg();
                C0677Jd bdh = ko.bdh();
                long aBt = bdh.aBt();
                try {
                    if (aBt > seVar.cVe()) {
                        mo5238i(seVar);
                        seVar.mo22001kv(aBt);
                    }
                    try {
                        SocketMessage ala = new SocketMessage();
                        aAD aad = new aAD((DataGameEvent) this.ifo, ala, false, false, 255, false);
                        bdh.mo3159a((ObjectOutput) aad, 48);
                        aad.flush();
                        byte[] byteArray = ala.toByteArray();
                        C3086na naVar = new C3086na((DataGameEvent) this.ifo, new ByteMessageReader(byteArray), false, 255);
                        C0677Jd jd = (C0677Jd) bdh.mo11944yn().mo13W();
                        jd.mo3204i((ObjectInput) naVar);
                        SocketMessage ala2 = new SocketMessage();
                        aAD aad2 = new aAD((DataGameEvent) this.ifo, ala2, false, false, 255, false);
                        jd.mo3159a((ObjectOutput) aad2, 4);
                        aad2.flush();
                        byte[] byteArray2 = ala2.toByteArray();
                        if (!Arrays.equals(byteArray, byteArray2)) {
                            throw new IOException("Different serialization result of transactional changes: \n" + Syst.m15898a(byteArray, 0, byteArray.length, " ", 16) + "\n---\n" + Syst.m15898a(byteArray2, 0, byteArray2.length, " ", 16));
                        }
                        size = i;
                    } catch (Exception e) {
                        throw new CountFailedReadOrWriteException("Error saving transaction. At game.script " + seVar.bFY(), e);
                    }
                } catch (Exception e2) {
                    throw new CountFailedReadOrWriteException("Error saving transaction. At game.script " + seVar.bFY(), e2);
                }
            } else {
                return;
            }
        }
    }

    /* renamed from: f */
    private byte[] m15798f(C6064afk afk) {
        SocketMessage ala = new SocketMessage();
        C3380qs qsVar = new C3380qs((DataGameEvent) this.ifo, ala, false, false);
        qsVar.mo16273g("class", afk.mo11944yn().getClass());
        qsVar.writeString(C0891Mw.DATA);
        ((IExternalIO) afk).writeExternal(qsVar);
        qsVar.mo16277pe();
        qsVar.flush();
        return ala.toByteArray();
    }

    /* renamed from: a */
    public void mo5219a(C1412Uh uh) {
        this.ifo = uh;
    }

    /* renamed from: f */
    public void mo5234f(FilePath ain) {
    }

    public void start() {
        this.ifl++;
    }

    /* renamed from: HY */
    public void mo5218HY() {
        this.ifk++;
    }
}
