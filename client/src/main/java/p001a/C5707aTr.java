package p001a;

import com.hoplon.geometry.Vec3f;
import logic.abc.C1226SE;
import logic.abc.azD;

/* renamed from: a.aTr  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C5707aTr implements aTC {
    public final C3978xf iOS = new C3978xf();
    public final C3978xf iOT = new C3978xf();
    public final C3978xf iOU = new C3978xf();
    public azD iOR;
    /* renamed from: r */
    public float f3836r;

    public C5707aTr(azD azd, C3978xf xfVar, C3978xf xfVar2, C3978xf xfVar3) {
        this.iOR = azd;
        this.iOS.mo22947a(xfVar);
        this.iOT.mo22947a(xfVar2);
        this.iOU.mo22947a(xfVar3);
        this.f3836r = 1.0f;
    }

    /* renamed from: a */
    public abstract float mo11545a(Vec3f vec3f, Vec3f vec3f2, float f, int i, int i2);

    /* renamed from: a */
    public void mo820a(Vec3f[] vec3fArr, int i, int i2) {
        C5253aCf acf = new C5253aCf(this.iOR, new C1226SE(vec3fArr[0], vec3fArr[1], vec3fArr[2]), new C0327ES());
        C5915acr.C1866a aVar = new C5915acr.C1866a();
        aVar.fkF = 1.0f;
        if (acf.mo8215a(this.iOS, this.iOT, this.iOU, this.iOU, aVar) && aVar.f4276Oj.lengthSquared() > 1.0E-4f && aVar.fkF < this.f3836r) {
            this.iOS.bFF.transform(aVar.f4276Oj);
            aVar.f4276Oj.normalize();
            mo11545a(aVar.f4276Oj, aVar.fkE, aVar.fkF, i, i2);
        }
    }
}
