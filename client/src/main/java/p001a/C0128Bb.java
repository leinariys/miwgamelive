package p001a;

import com.hoplon.geometry.Vec3f;

import java.util.*;

/* renamed from: a.Bb */
/* compiled from: a */
public class C0128Bb {
    public static final boolean DEBUG = true;
    public static final float dMA = 57.295776f;
    public static final float dMB = Float.MAX_VALUE;
    public static final Vec3f dMO = new Vec3f();
    public static final Vec3f dMU = new Vec3f(0.0f, 0.0f, 0.0f);
    public static final boolean dMs = false;
    public static final float dMt = 0.04f;
    public static final float dMu = 1.1920929E-7f;
    public static final float dMv = 1.1920929E-7f;
    public static final float dMw = 6.2831855f;
    public static final float dMx = 3.1415927f;
    public static final float dMy = 1.5707964f;
    public static final float dMz = 0.017453292f;
    private static final List<C0130b> dMV = new ArrayList();
    private static final Map<String, Long> dMW = new HashMap();
    public static C5799aaf dMC = null;
    public static C6161ahd dMD = null;
    public static float dME = 0.02f;
    public static float dMF = 2.0f;
    public static boolean dMG = false;
    public static int dMH = 0;
    public static int dMI = 0;
    public static int dMJ = 0;
    public static int dMK = 0;
    public static int dML = 0;
    public static int dMM = 0;
    public static int dMN = 0;
    public static float dMP = 0.0f;
    public static int dMQ = 0;
    public static int dMR = 0;
    public static int dMS = 0;
    public static int dMT = 0;
    public static long dMX = 0;
    public static long dMY = 0;

    /* renamed from: gf */
    public static void m1253gf(String str) {
    }

    public static void blS() {
    }

    public static void blT() {
        ArrayList arrayList = new ArrayList(dMW.entrySet());
        Collections.sort(arrayList, new C0129a());
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            System.out.println(String.valueOf((String) entry.getKey()) + " = " + entry.getValue() + " ms");
        }
    }

    /* renamed from: a.Bb$b */
    /* compiled from: a */
    private static class C0130b {
        public String name;
        public long startTime;

        private C0130b() {
        }
    }

    /* renamed from: a.Bb$a */
    class C0129a implements Comparator<Map.Entry<String, Long>> {
        C0129a() {
        }

        /* renamed from: a */
        public int compare(Map.Entry<String, Long> entry, Map.Entry<String, Long> entry2) {
            return entry.getValue().compareTo(entry2.getValue());
        }
    }
}
