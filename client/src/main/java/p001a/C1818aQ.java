package p001a;

import com.hoplon.geometry.Vec3f;
import logic.abc.C5804aak;
import logic.abc.azD;

/* renamed from: a.aQ */
/* compiled from: a */
public class C1818aQ extends C3808vG {
    private static boolean gAR = false;
    /* renamed from: mM */
    public final C1123QW<C5852abg.C1854a> f3574mM = C0762Ks.m6640D(C5852abg.C1854a.class);
    /* renamed from: Qk */
    public boolean f3572Qk = false;
    /* renamed from: Ql */
    public aGW f3573Ql;
    public boolean gAQ = false;
    private C4090zJ gAP;

    public C1818aQ(aGW agw, C1034PA pa, ayY ayy, ayY ayy2, C6331akr akr, C5434aJe aje) {
        super(pa);
        this.gAP = new C4090zJ((azD) null, (azD) null, akr, aje);
        this.f3573Ql = agw;
    }

    public void destroy() {
        if (this.f3572Qk && this.f3573Ql != null) {
            this.f9400Uy.mo583a(this.f3573Ql);
        }
    }

    /* renamed from: gB */
    public void mo10857gB(boolean z) {
        this.gAQ = z;
    }

    /* renamed from: a */
    public void mo8931a(ayY ayy, ayY ayy2, C5408aIe aie, C5362aGk agk) {
        if (this.f3573Ql == null) {
            this.f3573Ql = this.f9400Uy.mo594g(ayy, ayy2);
            this.f3572Qk = true;
        }
        agk.mo9117e(this.f3573Ql);
        azD azd = (azD) ayy.cEZ();
        azD azd2 = (azD) ayy2.cEZ();
        C5852abg.C1854a aVar = this.f3574mM.get();
        aVar.init();
        this.gAP.mo23283a(azd);
        this.gAP.mo23284b(azd2);
        aVar.fGC = azd.getMargin() + azd2.getMargin() + this.f3573Ql.dcB();
        aVar.fGC *= aVar.fGC;
        aVar.fGA.mo22947a(ayy.cFf());
        aVar.fGB.mo22947a(ayy2.cFf());
        this.gAP.mo12499a(aVar, agk, aie.iaO);
        this.f3574mM.release(aVar);
        if (this.f3572Qk) {
            agk.dad();
        }
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: b */
    public float mo8932b(ayY ayy, ayY ayy2, C5408aIe aie, C5362aGk agk) {
        float f;
        float f2 = 1.0f;
        this.stack.bcH().push();
        try {
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            vec3f.sub(ayy.cFh().bFG, ayy.cFf().bFG);
            float lengthSquared = vec3f.lengthSquared();
            vec3f.sub(ayy2.cFh().bFG, ayy2.cFf().bFG);
            float lengthSquared2 = vec3f.lengthSquared();
            if (lengthSquared < ayy.cFp() && lengthSquared2 < ayy2.cFp()) {
                this.stack.bcH().pop();
                return 1.0f;
            } else if (gAR) {
                this.stack.bcH().pop();
                return 1.0f;
            } else {
                C5804aak aak = new C5804aak(ayy2.cFo());
                C5915acr.C1866a aVar = new C5915acr.C1866a();
                if (new C5422aIs((azD) ayy.cEZ(), aak, new C0327ES()).mo8215a(ayy.cFf(), ayy.cFh(), ayy2.cFf(), ayy2.cFh(), aVar)) {
                    if (ayy.cFm() > aVar.fkF) {
                        ayy.mo17047ld(aVar.fkF);
                    }
                    if (ayy2.cFm() > aVar.fkF) {
                        ayy2.mo17047ld(aVar.fkF);
                    }
                    if (1.0f > aVar.fkF) {
                        f2 = aVar.fkF;
                    }
                }
                C5804aak aak2 = new C5804aak(ayy.cFo());
                C5915acr.C1866a aVar2 = new C5915acr.C1866a();
                if (new C5422aIs(aak2, (azD) ayy2.cEZ(), new C0327ES()).mo8215a(ayy.cFf(), ayy.cFh(), ayy2.cFf(), ayy2.cFh(), aVar2)) {
                    if (ayy.cFm() > aVar2.fkF) {
                        ayy.mo17047ld(aVar2.fkF);
                    }
                    if (ayy2.cFm() > aVar2.fkF) {
                        ayy2.mo17047ld(aVar2.fkF);
                    }
                    if (f2 > aVar2.fkF) {
                        f = aVar2.fkF;
                        this.stack.bcH().pop();
                        return f;
                    }
                }
                f = f2;
                this.stack.bcH().pop();
                return f;
            }
        } catch (Throwable th) {
            this.stack.bcH().pop();
            throw th;
        }
    }

    public aGW cws() {
        return this.f3573Ql;
    }

    /* renamed from: a.aQ$a */
    public static class C1819a extends C6548apA {

        /* renamed from: lK */
        public C5434aJe f3575lK;

        /* renamed from: lL */
        public C6331akr f3576lL;

        public C1819a(C6331akr akr, C5434aJe aje) {
            this.f3576lL = akr;
            this.f3575lK = aje;
        }

        /* renamed from: a */
        public C3808vG mo8207a(C1034PA pa, ayY ayy, ayY ayy2) {
            return new C1818aQ(pa.dRf, pa, ayy, ayy2, this.f3576lL, this.f3575lK);
        }
    }
}
