package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.abg  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5852abg {

    /* renamed from: a */
    void mo12499a(C1854a aVar, C1855b bVar, C1072Pg pg);

    /* renamed from: a.abg$b */
    /* compiled from: a */
    public interface C1855b {
        /* renamed from: b */
        void mo5216b(int i, int i2, int i3, int i4);

        /* renamed from: c */
        void mo5217c(Vec3f vec3f, Vec3f vec3f2, float f);
    }

    /* renamed from: a.abg$a */
    public static class C1854a {
        public final C3978xf fGA = new C3978xf();
        public final C3978xf fGB = new C3978xf();
        public float fGC;

        public C1854a() {
            init();
        }

        public void init() {
            this.fGC = Float.MAX_VALUE;
        }
    }
}
