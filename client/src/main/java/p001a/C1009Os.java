package p001a;

import game.geometry.TransformWrap;
import game.script.avatar.Avatar;
import game.script.avatar.AvatarSector;
import game.script.avatar.ColorWrapper;
import game.script.avatar.TextureGrid;
import game.script.avatar.body.BodyPiece;
import game.script.avatar.cloth.Cloth;
import game.script.resource.Asset;
import gnu.trove.THashMap;
import logic.render.IEngineGraphics;
import logic.res.sound.C0907NJ;
import logic.thred.LogPrinter;
import taikodom.render.RenderView;
import taikodom.render.helpers.RenderTask;
import taikodom.render.loader.FileSceneLoader;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.RAnimation;
import taikodom.render.scene.RModel;
import taikodom.render.scene.Scene;
import taikodom.render.scene.custom.RCustomMesh;
import taikodom.render.textures.BaseTexture;
import taikodom.render.textures.Material;
import taikodom.render.textures.ProceduralTexture;
import taikodom.render.textures.Texture;
import taikodom.render.textures.procedural.Filter;
import taikodom.render.textures.procedural.filter.AddFilter;
import taikodom.render.textures.procedural.filter.DecalFilter;
import taikodom.render.textures.procedural.filter.ReplaceFilter;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.List;
import java.util.*;

/* renamed from: a.Os */
/* compiled from: a */
public class C1009Os {
    /* access modifiers changed from: private */
    public static final LogPrinter logger = LogPrinter.setClass(Avatar.class);
    private static final boolean dKM = false;
    private static final Comparator<Cloth> dKN = new C5463aKh();
    private static int count;
    /* access modifiers changed from: private */
    public final Map<AvatarSector.C0955a, Material> dKP;
    /* access modifiers changed from: private */
    public final THashMap<AvatarSector.C0955a, ProceduralTexture> dKQ;
    /* access modifiers changed from: private */
    public final THashMap<AvatarSector.C0955a, ProceduralTexture> dKR;
    /* access modifiers changed from: private */
    public final Map<AvatarSector, C3845vi> dKS;
    /* access modifiers changed from: private */
    public final aVE dLh;
    /* access modifiers changed from: private */
    public final C0805Lf dLi;
    /* access modifiers changed from: private */
    public final THashMap<Cloth, C0661JS> dLj;
    /* renamed from: lV */
    public final IEngineGraphics f1341lV;
    final TransformWrap dLp;
    private final Filter dKT;
    private final Filter dKU;
    private final Filter dKV;
    private final Filter dKW;
    private final Filter dKX;
    private final Filter dKY;
    private final Filter dKZ;
    private final Filter dLa;
    private final Map<AvatarSector, Filter> dLb;
    private final Filter dLc;
    private final Filter dLd;
    private final Filter dLe;
    private final Filter dLf;
    private final Map<AvatarSector, Filter> dLg;
    /* access modifiers changed from: private */
    public RAnimation dKO;
    /* access modifiers changed from: private */
    public RenderTask dLk;
    /* access modifiers changed from: private */
    public RenderTask dLl;
    /* access modifiers changed from: private */
    public boolean dLm;
    /* access modifiers changed from: private */
    public boolean dLn;
    /* access modifiers changed from: private */
    public RenderTask dLo;
    /* access modifiers changed from: private */
    public Boolean dtj;
    /* access modifiers changed from: private */
    /* access modifiers changed from: private */
    public List<C0907NJ> dtl;
    /* access modifiers changed from: private */
    public RModel rmodel;

    public C1009Os(Avatar ev) {
        this(ev, ev.ala().ald().ale());
    }

    public C1009Os(aVE ave, IEngineGraphics abd) {
        this.dtj = false;
        this.dtl = new ArrayList();
        this.dLp = new TransformWrap();
        this.dLh = ave;
        this.f1341lV = abd;
        this.dKS = new THashMap();
        FileSceneLoader aej = abd.aej();
        C5207aAl aOm = ave.aOm();
        this.dLi = new C0805Lf(aOm, abd);
        this.dLj = new THashMap<>();
        String prefix = aOm.getPrefix();
        for (Cloth next : ave.aOu()) {
            this.dLj.put(next, new C0661JS(next, prefix));
        }
        this.dKP = new THashMap();
        this.dKP.put(AvatarSector.C0955a.HEAD, aej.getDefaultMaterial());
        this.dKP.put(AvatarSector.C0955a.BODY, aej.getDefaultMaterial());
        this.dKP.put(AvatarSector.C0955a.HAIR, aej.getDefaultMaterial());
        this.dKQ = new THashMap<>();
        this.dLb = new THashMap();
        this.dKR = new THashMap<>();
        this.dLg = new THashMap();
        Texture defaultDiffuseTexture = aej.getDefaultDiffuseTexture();
        this.dKT = new ReplaceFilter((BaseTexture) defaultDiffuseTexture, Filter.FullArea, true);
        this.dKV = new DecalFilter((BaseTexture) null, (Rectangle2D.Float) null, aOm.bdV().bjc(), false);
        this.dKW = new DecalFilter((BaseTexture) null, aOm.aFO().bjc(), true, true, false);
        this.dKX = new DecalFilter((BaseTexture) null, aOm.aFQ().bjc(), true, true, false);
        this.dKY = new DecalFilter((BaseTexture) null, Color.WHITE, (Rectangle2D.Float) null, aOm.aGk(), (BaseTexture) null, false, true, false);
        this.dKU = new ReplaceFilter((BaseTexture) defaultDiffuseTexture, Filter.FullArea, true);
        this.dKZ = new ReplaceFilter((BaseTexture) null, Filter.FullArea, false);
        this.dLa = new ReplaceFilter((BaseTexture) null, Filter.FullArea, false);
        Texture defaultNormalTexture = aej.getDefaultNormalTexture();
        this.dLc = new ReplaceFilter(defaultNormalTexture, Filter.FullArea);
        this.dLd = new ReplaceFilter(defaultNormalTexture, Filter.FullArea);
        this.dLe = new ReplaceFilter((BaseTexture) null, Filter.FullArea, false);
        this.dLf = new ReplaceFilter((BaseTexture) null, Filter.FullArea, false);
        count++;
        logger.trace("assembler initialized [count: " + count + "]");
    }

    /* renamed from: a */
    private static boolean m8181a(Filter filter, TextureGrid uk) {
        boolean z;
        BaseTexture baseTexture = null;
        if (uk == null || !(filter instanceof DecalFilter)) {
            filter.setActive(false);
        } else {
            BaseTexture texture = uk.getTexture();
            filter.setMask(texture);
            ((DecalFilter) filter).setSourceArea(uk.byp());
            if (texture != null) {
                z = true;
            } else {
                z = false;
            }
            filter.setActive(z);
            baseTexture = texture;
        }
        if (baseTexture != null) {
            return true;
        }
        return false;
    }

    /* renamed from: aX */
    private static Texture m8182aX(Asset tCVar) {
        if (tCVar == null) {
            return null;
        }
        IEngineGraphics ale = tCVar.ald().getEngineGraphics();
        ale.mo3049bV(tCVar.getFile());
        return (Texture) ale.mo3047bT(tCVar.getHandle());
    }

    /* renamed from: a */
    private static Texture m8175a(IEngineGraphics abd, String str) {
        if (str == null) {
            return null;
        }
        abd.mo3049bV(str);
        return (Texture) abd.mo3047bT(str);
    }

    /* access modifiers changed from: private */
    public Collection<Filter> blv() {
        return this.dLb.values();
    }

    /* access modifiers changed from: private */
    public Collection<Filter> blw() {
        return this.dLg.values();
    }

    /* renamed from: o */
    private Filter m8200o(AvatarSector nw) {
        return this.dLb.get(nw);
    }

    /* renamed from: p */
    private Filter m8202p(AvatarSector nw) {
        return this.dLg.get(nw);
    }

    private Filter blx() {
        return this.dKZ;
    }

    private Filter bly() {
        return this.dLa;
    }

    public RModel aOo() {
        return this.rmodel;
    }

    /* access modifiers changed from: private */
    public void blz() {
        this.f1341lV.mo3007a((RenderTask) new C1012b());
    }

    /* renamed from: c */
    public void mo4544c(C1019i iVar) {
        if (this.dtj.booleanValue()) {
            this.f1341lV.mo3007a((RenderTask) new C1014d(iVar));
        } else if (iVar != null) {
            iVar.mo4552x(this);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo4539b(C0907NJ r4) {
        /*
            r3 = this;
            java.lang.Boolean r1 = r3.dtj
            monitor-enter(r1)
            java.lang.Boolean r0 = r3.dtj     // Catch:{ all -> 0x0023 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x0023 }
            if (r0 == 0) goto L_0x0014
            if (r4 == 0) goto L_0x0012
            taikodom.render.scene.RModel r0 = r3.rmodel     // Catch:{ all -> 0x0023 }
            r4.mo968a(r0)     // Catch:{ all -> 0x0023 }
        L_0x0012:
            monitor-exit(r1)     // Catch:{ all -> 0x0023 }
        L_0x0013:
            return
        L_0x0014:
            java.util.List<a.NJ> r2 = r3.dtl     // Catch:{ all -> 0x0023 }
            monitor-enter(r2)     // Catch:{ all -> 0x0023 }
            java.util.List<a.NJ> r0 = r3.dtl     // Catch:{ all -> 0x0026 }
            r0.add(r4)     // Catch:{ all -> 0x0026 }
            monitor-exit(r2)     // Catch:{ all -> 0x0026 }
            taikodom.render.helpers.RenderTask r0 = r3.dLo     // Catch:{ all -> 0x0023 }
            if (r0 == 0) goto L_0x0029
            monitor-exit(r1)     // Catch:{ all -> 0x0023 }
            goto L_0x0013
        L_0x0023:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0023 }
            throw r0
        L_0x0026:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0026 }
            throw r0     // Catch:{ all -> 0x0023 }
        L_0x0029:
            a.Os$e r0 = new a.Os$e     // Catch:{ all -> 0x0023 }
            r0.<init>()     // Catch:{ all -> 0x0023 }
            r3.dLo = r0     // Catch:{ all -> 0x0023 }
            a.aBD r0 = r3.f1341lV     // Catch:{ all -> 0x0023 }
            taikodom.render.helpers.RenderTask r2 = r3.dLo     // Catch:{ all -> 0x0023 }
            r0.mo3007a((taikodom.render.helpers.RenderTask) r2)     // Catch:{ all -> 0x0023 }
            monitor-exit(r1)     // Catch:{ all -> 0x0023 }
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C1009Os.mo4539b(a.NJ):void");
    }

    /* renamed from: y */
    public void mo4548y(Cloth wj) {
        C0661JS js = (C0661JS) this.dLj.remove(wj);
        if (this.rmodel != null && js != null) {
            this.f1341lV.mo3007a((RenderTask) new C1013c(js));
        }
    }

    /* renamed from: z */
    public void mo4550z(Cloth wj) {
        C0661JS js = (C0661JS) this.dLj.remove(wj);
        if (this.rmodel != null && js != null) {
            for (AvatarSector.C0955a a : AvatarSector.C0955a.values()) {
                this.rmodel.removeSubMesh(js.mo3097a(a));
            }
            js.akR();
        }
    }

    /* renamed from: A */
    public void mo4534A(Cloth wj) {
        C0661JS js = new C0661JS(wj, this.dLh.aOm().getPrefix());
        this.dLj.put(wj, js);
        if (this.rmodel != null && js.akQ()) {
            Asset bEd = wj.bEd();
            this.f1341lV.mo3049bV(bEd.getFile());
            Texture defaultSelfIluminationTexture = this.f1341lV.aej().getDefaultSelfIluminationTexture();
            for (AvatarSector.C0955a aVar : AvatarSector.C0955a.values()) {
                RCustomMesh a = js.mo3097a(aVar);
                if (a != null) {
                    this.rmodel.addSubMesh(a);
                    Material material = (Material) this.f1341lV.mo3047bT(bEd.getHandle());
                    material.invalidateHandle();
                    material.setDiffuseTexture((BaseTexture) this.dKQ.get(aVar));
                    material.setNormalTexture((BaseTexture) this.dKR.get(aVar));
                    material.setSelfIluminationTexture(defaultSelfIluminationTexture);
                    a.setMaterial(material);
                }
            }
            blD();
            blC();
        }
    }

    /* renamed from: B */
    public void mo4535B(Cloth wj) {
        C0661JS js = new C0661JS(wj, this.dLh.aOm().getPrefix());
        this.dLj.put(wj, js);
        if (this.rmodel != null) {
            this.f1341lV.mo3007a((RenderTask) new C1010a(wj, js));
        }
    }

    public void blA() {
        if (this.dtj.booleanValue()) {
            if (this.dLk != null) {
                this.dLm = true;
                return;
            }
            this.dLk = new C1017g();
            this.f1341lV.mo3007a(this.dLk);
        }
    }

    public void blB() {
        for (ProceduralTexture update : this.dKQ.values()) {
            update.update();
        }
        for (ProceduralTexture update2 : this.dKR.values()) {
            update2.update();
        }
    }

    /* renamed from: yf */
    public void mo4549yf() {
        if (this.dtj.booleanValue()) {
            if (this.dLl != null) {
                this.dLn = true;
                return;
            }
            this.dLl = new C1018h();
            this.f1341lV.mo3007a(this.dLl);
        }
    }

    /* renamed from: f */
    public BaseTexture mo4545f(AvatarSector.C0955a aVar) {
        return (BaseTexture) this.dKQ.get(aVar);
    }

    /* renamed from: g */
    public BaseTexture mo4547g(AvatarSector.C0955a aVar) {
        return (BaseTexture) this.dKR.get(aVar);
    }

    /* renamed from: ak */
    public void mo4538ak(Asset tCVar) {
        if (this.rmodel != null) {
            tCVar.ald().getLoaderTrail().mo4995a(tCVar.getFile(), tCVar.getHandle(), (Scene) null, new C1016f(), getClass().getSimpleName());
        }
    }

    /* renamed from: aC */
    public void mo4536aC(Asset tCVar) {
        if (this.rmodel != null) {
            if (this.dKO != null) {
                this.dKO.setFadeTime(0.0f);
                this.dKO.stop();
                this.rmodel.removeAnimation(this.dKO);
            }
            this.f1341lV.mo3049bV(tCVar.getFile());
            this.dKO = (RAnimation) this.f1341lV.mo3047bT(tCVar.getHandle());
            if (this.dKO != null) {
                this.dKO.setFadeTime(0.0f);
                this.rmodel.addAnimation(this.dKO);
                this.dKO.play();
            }
        }
    }

    public void blC() {
        if (this.rmodel != null) {
            this.dLi.akP();
            for (Cloth wj : this.dLh.aOu()) {
                ((C0661JS) this.dLj.get(wj)).akP();
            }
            this.rmodel.setScaling(this.dLh.aOD());
        }
    }

    /* access modifiers changed from: private */
    public void blD() {
        Set<AvatarSector> emptySet;
        boolean z;
        boolean z2;
        boolean z3;
        Color color;
        this.dKS.clear();
        Texture defaultNormalTexture = this.f1341lV.aej().getDefaultNormalTexture();
        C5207aAl aOm = this.dLh.aOm();
        this.dKT.setMask(m8182aX(aOm.bed().crV()));
        m8181a(this.dKV, aOm.bef());
        m8181a(this.dKW, aOm.beg());
        m8181a(this.dKX, aOm.beh());
        if (m8181a(this.dKY, aOm.bdZ())) {
            ColorWrapper bdX = aOm.bdX();
            this.dKY.getAreas().clear();
            this.dKY.getAreas().add(aOm.aGk());
            ((DecalFilter) this.dKY).setSourceArea(aOm.bdZ().byp());
            Filter filter = this.dKY;
            if (bdX != null) {
                color = bdX.getColor();
            } else {
                color = Color.WHITE;
            }
            filter.setColor(color);
            this.dKY.setActive(true);
        } else {
            this.dKY.setActive(false);
        }
        Texture aX = m8182aX(aOm.bdW());
        if (aX != null) {
            this.dLc.setMask(aX);
        } else {
            this.dLc.setMask(defaultNormalTexture);
        }
        for (Filter active : blv()) {
            active.setActive(false);
        }
        for (Filter active2 : blw()) {
            active2.setActive(false);
        }
        Cloth aOw = this.dLh.aOw();
        if (aOw != null) {
            emptySet = aOw.bEh();
        } else {
            emptySet = Collections.emptySet();
        }
        Collection<Cloth> aOu = this.dLh.aOu();
        if (!aOu.isEmpty()) {
            ArrayList<Cloth> arrayList = new ArrayList<>(aOu);
            Collections.sort(arrayList, dKN);
            for (Cloth wj : arrayList) {
                Set<BodyPiece> bEj = wj.bEj();
                C0661JS js = (C0661JS) this.dLj.get(wj);
                for (BodyPiece bpk : bEj) {
                    AvatarSector bpk2 = bpk.bpk();
                    if (this.dKS.get(bpk2) == null && !emptySet.contains(bpk2)) {
                        js.mo3098a(bpk2, true);
                        Filter o = m8200o(bpk2);
                        Texture a = m8175a(this.f1341lV, js.mo3108h(bpk2.biX()));
                        if (a != null) {
                            z3 = true;
                        } else {
                            z3 = false;
                        }
                        o.setActive(z3);
                        o.setMask(a);
                        Filter p = m8202p(bpk2);
                        Texture a2 = m8175a(this.f1341lV, js.mo3109i(bpk2.biX()));
                        p.setActive(true);
                        p.setMask(a2);
                    } else {
                        js.mo3098a(bpk2, false);
                    }
                }
                for (AvatarSector next : wj.bEh()) {
                    if (this.dKS.get(next) == null) {
                        this.dKS.put(next, js);
                    }
                }
            }
        }
        this.dKU.setMask(m8182aX(aOm.bed().crX()));
        this.dKU.setActive(true);
        Texture aX2 = m8182aX(aOm.bdT());
        if (aX2 == null) {
            this.dLd.setMask(defaultNormalTexture);
        } else {
            this.dLd.setMask(aX2);
        }
        Filter filter2 = this.dLd;
        if (aX2 != null) {
            z = true;
        } else {
            z = false;
        }
        filter2.setActive(z);
        for (AvatarSector next2 : aOm.aFI()) {
            C3845vi viVar = this.dKS.get(next2);
            C0805Lf lf = this.dLi;
            if (viVar == null || viVar.mo3097a(next2.biX()) == null) {
                z2 = true;
            } else {
                z2 = false;
            }
            lf.mo3098a(next2, z2);
        }
        if (this.dLh.aOs() != null) {
            blx().setActive(true);
            C0661JS js2 = (C0661JS) this.dLj.get(this.dLh.aOs());
            blx().setMask(m8175a(this.f1341lV, js2.mo3108h(AvatarSector.C0955a.BODY)));
            if (m8175a(this.f1341lV, js2.mo3108h(AvatarSector.C0955a.BODY)) == null) {
                this.dLe.setMask(defaultNormalTexture);
            } else {
                this.dLe.setMask(defaultNormalTexture);
            }
            this.dLe.setActive(true);
        } else {
            blx().setActive(false);
        }
        if (this.dLh.aOw() != null) {
            bly().setActive(true);
            Texture a3 = m8175a(this.f1341lV, ((C0661JS) this.dLj.get(this.dLh.aOw())).mo3108h(AvatarSector.C0955a.BODY));
            bly().setMask(a3);
            if (a3 == null) {
                this.dLf.setMask(defaultNormalTexture);
            } else {
                this.dLf.setMask(a3);
            }
            this.dLf.setActive(true);
        } else {
            bly().setActive(false);
        }
        blB();
    }

    /* access modifiers changed from: private */
    public void blE() {
    }

    /* access modifiers changed from: private */
    public void blF() {
        ProceduralTexture proceduralTexture = new ProceduralTexture(512, 512);
        proceduralTexture.addFilter(this.dKT);
        proceduralTexture.addFilter(this.dKY);
        proceduralTexture.addFilter(this.dKV);
        proceduralTexture.addFilter(this.dKW);
        proceduralTexture.addFilter(this.dKX);
        ProceduralTexture proceduralTexture2 = new ProceduralTexture(512, 512);
        proceduralTexture2.addFilter(this.dKU);
        proceduralTexture2.addFilter(this.dKZ);
        for (AvatarSector next : this.dLh.aFI()) {
            if (next.biX().equals(AvatarSector.C0955a.BODY)) {
                ReplaceFilter replaceFilter = new ReplaceFilter((BaseTexture) null, Color.WHITE, next.bjc(), (BaseTexture) null, false, false, true, false);
                this.dLb.put(next, replaceFilter);
                proceduralTexture2.addFilter(replaceFilter);
            }
        }
        proceduralTexture2.addFilter(this.dLa);
        this.dKQ.put(AvatarSector.C0955a.HEAD, proceduralTexture);
        this.dKQ.put(AvatarSector.C0955a.BODY, proceduralTexture2);
        this.dKQ.put(AvatarSector.C0955a.HAIR, proceduralTexture);
        ProceduralTexture proceduralTexture3 = new ProceduralTexture(512, 512);
        proceduralTexture3.addFilter(this.dLc);
        ProceduralTexture proceduralTexture4 = new ProceduralTexture(512, 512);
        Collection<AvatarSector> aFI = this.dLh.aOm().aFI();
        proceduralTexture4.addFilter(this.dLd);
        proceduralTexture4.addFilter(this.dLe);
        for (AvatarSector next2 : aFI) {
            if (next2.biX().equals(AvatarSector.C0955a.BODY)) {
                ReplaceFilter replaceFilter2 = new ReplaceFilter((BaseTexture) null, Color.WHITE, next2.bjc(), (BaseTexture) null, true, false, false, false);
                this.dLg.put(next2, replaceFilter2);
                proceduralTexture4.addFilter(replaceFilter2);
            }
        }
        proceduralTexture4.addFilter(this.dLf);
        this.dKR.put(AvatarSector.C0955a.HEAD, proceduralTexture3);
        this.dKR.put(AvatarSector.C0955a.BODY, proceduralTexture4);
        this.dKR.put(AvatarSector.C0955a.HAIR, proceduralTexture3);
    }

    /* access modifiers changed from: private */
    public void blG() {
        Material material = (Material) this.f1341lV.mo3047bT("basic_skin_material");
        material.invalidateHandle();
        Texture defaultSelfIluminationTexture = this.f1341lV.aej().getDefaultSelfIluminationTexture();
        material.setSelfIluminationTexture(defaultSelfIluminationTexture);
        material.setNormalTexture((BaseTexture) this.dKR.get(AvatarSector.C0955a.HEAD));
        BaseTexture baseTexture = (BaseTexture) this.dKQ.get(AvatarSector.C0955a.HEAD);
        material.setDiffuseTexture(baseTexture);
        this.f1341lV.mo3049bV("data/avatar/texture/shared/head_alpha.dds");
        ((ProceduralTexture) baseTexture).addFilter(new AddFilter((Texture) this.f1341lV.mo3047bT("data/avatar/texture/shared/head_alpha.dds"), Filter.FullArea, false, false, false, true, true));
        Material material2 = (Material) this.f1341lV.mo3047bT("basic_skin_material");
        material2.invalidateHandle();
        material2.setSelfIluminationTexture(defaultSelfIluminationTexture);
        material2.setNormalTexture((BaseTexture) this.dKR.get(AvatarSector.C0955a.BODY));
        material2.setDiffuseTexture((BaseTexture) this.dKQ.get(AvatarSector.C0955a.BODY));
        Material material3 = (Material) this.f1341lV.mo3047bT("basic_hair_material");
        material3.invalidateHandle();
        material3.setSelfIluminationTexture(material.getSelfIluminationTexture());
        material3.setNormalTexture((BaseTexture) this.dKR.get(AvatarSector.C0955a.HAIR));
        material.setDiffuseTexture((BaseTexture) this.dKQ.get(AvatarSector.C0955a.HAIR));
        this.dKP.put(AvatarSector.C0955a.HEAD, material);
        this.dKP.put(AvatarSector.C0955a.BODY, material2);
        this.dKP.put(AvatarSector.C0955a.HAIR, material3);
        material.setDiffuseTexture(mo4545f(AvatarSector.C0955a.HEAD));
        material.setNormalTexture(mo4547g(AvatarSector.C0955a.HEAD));
        material2.setDiffuseTexture(mo4545f(AvatarSector.C0955a.BODY));
        material2.setNormalTexture(mo4547g(AvatarSector.C0955a.BODY));
        material3.setDiffuseTexture(mo4545f(AvatarSector.C0955a.HAIR));
        material3.setNormalTexture(mo4547g(AvatarSector.C0955a.HAIR));
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        count--;
        logger.trace("assembler finalized [yet to be: " + count + "]");
        mo4544c((C1019i) null);
        super.finalize();
    }

    public C0805Lf blH() {
        return this.dLi;
    }

    /* renamed from: a.Os$i */
    /* compiled from: a */
    public interface C1019i {
        /* renamed from: x */
        void mo4552x(C1009Os os);
    }

    /* renamed from: a.Os$b */
    /* compiled from: a */
    class C1012b implements RenderTask {
        C1012b() {
        }

        public void run(RenderView renderView) {
            synchronized (C1009Os.this.dtl) {
                for (C0907NJ a : C1009Os.this.dtl) {
                    a.mo968a(C1009Os.this.rmodel);
                }
                C1009Os.this.dtl.clear();
            }
        }
    }

    /* renamed from: a.Os$d */
    /* compiled from: a */
    class C1014d implements RenderTask {
        private final /* synthetic */ C1019i ihY;

        C1014d(C1019i iVar) {
            this.ihY = iVar;
        }

        public void run(RenderView renderView) {
            synchronized (C1009Os.this.dtj) {
                if (C1009Os.this.rmodel != null) {
                    for (Filter filter : C1009Os.this.blv()) {
                        filter.setMask((BaseTexture) null);
                        filter.setActive(false);
                    }
                    for (Filter filter2 : C1009Os.this.blw()) {
                        filter2.setMask((BaseTexture) null);
                        filter2.setActive(false);
                    }
                    C1009Os.this.dKS.clear();
                    C1009Os.this.rmodel.clearSubMeshes();
                    C1009Os.this.rmodel.dispose();
                    C1009Os.this.rmodel.releaseReferences();
                    C1009Os.this.rmodel = null;
                    C1009Os.this.dLi.akR();
                    for (C0661JS akR : C1009Os.this.dLj.values()) {
                        akR.akR();
                    }
                    C1009Os.this.dtj = false;
                    C1009Os.logger.trace("Assembler: " + C1009Os.this + " have released the resource.");
                }
            }
            if (this.ihY != null) {
                this.ihY.mo4552x(C1009Os.this);
            }
        }
    }

    /* renamed from: a.Os$e */
    /* compiled from: a */
    class C1015e implements RenderTask {
        C1015e() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run(taikodom.render.RenderView r7) {
            /*
                r6 = this;
                a.Os r0 = p001a.C1009Os.this
                java.lang.Boolean r2 = r0.dtj
                monitor-enter(r2)
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                java.lang.Boolean r0 = r0.dtj     // Catch:{ all -> 0x017e }
                boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x017e }
                if (r0 == 0) goto L_0x001a
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                r0.blz()     // Catch:{ all -> 0x017e }
                monitor-exit(r2)     // Catch:{ all -> 0x017e }
            L_0x0019:
                return
            L_0x001a:
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                a.aVE r0 = r0.dLh     // Catch:{ all -> 0x017e }
                a.aAl r0 = r0.aOm()     // Catch:{ all -> 0x017e }
                a.tC r1 = r0.beb()     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                a.aBD r0 = r0.f1341lV     // Catch:{ all -> 0x017e }
                java.lang.String r3 = r1.getFile()     // Catch:{ all -> 0x017e }
                r0.mo3049bV(r3)     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                a.aBD r0 = r0.f1341lV     // Catch:{ all -> 0x017e }
                java.lang.String r3 = r1.getHandle()     // Catch:{ all -> 0x017e }
                taikodom.render.loader.RenderAsset r0 = r0.mo3047bT(r3)     // Catch:{ all -> 0x017e }
                boolean r3 = r0 instanceof taikodom.render.scene.RModel     // Catch:{ all -> 0x017e }
                if (r3 == 0) goto L_0x022a
                a.Os r1 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                taikodom.render.scene.RModel r0 = (taikodom.render.scene.RModel) r0     // Catch:{ all -> 0x017e }
                r1.rmodel = r0     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                taikodom.render.scene.RModel r0 = r0.rmodel     // Catch:{ all -> 0x017e }
                r0.clearSubMeshes()     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                a.aBD r0 = r0.f1341lV     // Catch:{ all -> 0x017e }
                java.lang.String r1 = "data/avatar/material/basic_materials.pro"
                r0.mo3049bV(r1)     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                r0.blF()     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                r0.blG()     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                r0.blD()     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                a.Lf r0 = r0.dLi     // Catch:{ all -> 0x017e }
                r0.akQ()     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                a.Lf r0 = r0.dLi     // Catch:{ all -> 0x017e }
                r0.akP()     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                a.Lf r0 = r0.dLi     // Catch:{ all -> 0x017e }
                a.Nw$a r1 = p001a.C0954Nw.C0955a.BODY     // Catch:{ all -> 0x017e }
                taikodom.render.scene.custom.RCustomMesh r1 = r0.mo3097a((p001a.C0954Nw.C0955a) r1)     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                java.util.Map r0 = r0.dKP     // Catch:{ all -> 0x017e }
                a.Nw$a r3 = p001a.C0954Nw.C0955a.BODY     // Catch:{ all -> 0x017e }
                java.lang.Object r0 = r0.get(r3)     // Catch:{ all -> 0x017e }
                taikodom.render.textures.Material r0 = (taikodom.render.textures.Material) r0     // Catch:{ all -> 0x017e }
                r1.setMaterial(r0)     // Catch:{ all -> 0x017e }
                r1.buildIndexData()     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                taikodom.render.scene.RModel r0 = r0.rmodel     // Catch:{ all -> 0x017e }
                r1.updateInternalGeometry(r0)     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                taikodom.render.scene.RModel r0 = r0.rmodel     // Catch:{ all -> 0x017e }
                r0.addSubMesh(r1)     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                a.Lf r0 = r0.dLi     // Catch:{ all -> 0x017e }
                a.Nw$a r1 = p001a.C0954Nw.C0955a.HEAD     // Catch:{ all -> 0x017e }
                taikodom.render.scene.custom.RCustomMesh r1 = r0.mo3097a((p001a.C0954Nw.C0955a) r1)     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                java.util.Map r0 = r0.dKP     // Catch:{ all -> 0x017e }
                a.Nw$a r3 = p001a.C0954Nw.C0955a.HEAD     // Catch:{ all -> 0x017e }
                java.lang.Object r0 = r0.get(r3)     // Catch:{ all -> 0x017e }
                taikodom.render.textures.Material r0 = (taikodom.render.textures.Material) r0     // Catch:{ all -> 0x017e }
                r1.setMaterial(r0)     // Catch:{ all -> 0x017e }
                r1.buildIndexData()     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                taikodom.render.scene.RModel r0 = r0.rmodel     // Catch:{ all -> 0x017e }
                r1.updateInternalGeometry(r0)     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                taikodom.render.scene.RModel r0 = r0.rmodel     // Catch:{ all -> 0x017e }
                r0.addSubMesh(r1)     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                a.Lf r0 = r0.dLi     // Catch:{ all -> 0x017e }
                a.Nw$a r1 = p001a.C0954Nw.C0955a.HAIR     // Catch:{ all -> 0x017e }
                taikodom.render.scene.custom.RCustomMesh r1 = r0.mo3097a((p001a.C0954Nw.C0955a) r1)     // Catch:{ all -> 0x017e }
                if (r1 == 0) goto L_0x011b
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                java.util.Map r0 = r0.dKP     // Catch:{ all -> 0x017e }
                a.Nw$a r3 = p001a.C0954Nw.C0955a.HAIR     // Catch:{ all -> 0x017e }
                java.lang.Object r0 = r0.get(r3)     // Catch:{ all -> 0x017e }
                taikodom.render.textures.Material r0 = (taikodom.render.textures.Material) r0     // Catch:{ all -> 0x017e }
                r1.setMaterial(r0)     // Catch:{ all -> 0x017e }
                r1.buildIndexData()     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                taikodom.render.scene.RModel r0 = r0.rmodel     // Catch:{ all -> 0x017e }
                r1.updateInternalGeometry(r0)     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                taikodom.render.scene.RModel r0 = r0.rmodel     // Catch:{ all -> 0x017e }
                r0.addSubMesh(r1)     // Catch:{ all -> 0x017e }
            L_0x011b:
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                a.aVE r0 = r0.dLh     // Catch:{ all -> 0x017e }
                java.util.Collection r0 = r0.aOu()     // Catch:{ all -> 0x017e }
                java.util.Iterator r3 = r0.iterator()     // Catch:{ all -> 0x017e }
            L_0x0129:
                boolean r0 = r3.hasNext()     // Catch:{ all -> 0x017e }
                if (r0 != 0) goto L_0x0181
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                r0.blE()     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                r0.blD()     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                r0.blC()     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                taikodom.render.scene.RModel r0 = r0.rmodel     // Catch:{ all -> 0x017e }
                r1 = 0
                r0.updateInternalGeometry(r1)     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                r1 = 1
                java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ all -> 0x017e }
                r0.dtj = r1     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                r1 = 0
                r0.dLo = r1     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                r0.blz()     // Catch:{ all -> 0x017e }
                a.UR r0 = p001a.C1009Os.logger     // Catch:{ all -> 0x017e }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x017e }
                java.lang.String r3 = "Assembler: "
                r1.<init>(r3)     // Catch:{ all -> 0x017e }
                a.Os r3 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x017e }
                java.lang.String r3 = " have loaded resources."
                java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x017e }
                java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x017e }
                r0.trace(r1)     // Catch:{ all -> 0x017e }
            L_0x017b:
                monitor-exit(r2)     // Catch:{ all -> 0x017e }
                goto L_0x0019
            L_0x017e:
                r0 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x017e }
                throw r0
            L_0x0181:
                java.lang.Object r0 = r3.next()     // Catch:{ all -> 0x017e }
                a.WJ r0 = (p001a.C1516WJ) r0     // Catch:{ all -> 0x017e }
                a.Os r1 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                gnu.trove.THashMap r1 = r1.dLj     // Catch:{ all -> 0x017e }
                java.lang.Object r1 = r1.get(r0)     // Catch:{ all -> 0x017e }
                a.vi r1 = (p001a.C3845vi) r1     // Catch:{ all -> 0x017e }
                r1.akQ()     // Catch:{ all -> 0x017e }
                r1.akP()     // Catch:{ all -> 0x017e }
                a.Nw$a r4 = p001a.C0954Nw.C0955a.BODY     // Catch:{ all -> 0x017e }
                taikodom.render.scene.custom.RCustomMesh r4 = r1.mo3097a((p001a.C0954Nw.C0955a) r4)     // Catch:{ all -> 0x017e }
                if (r4 == 0) goto L_0x0129
                a.Os r1 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                a.aBD r1 = r1.f1341lV     // Catch:{ all -> 0x017e }
                a.tC r5 = r0.bEd()     // Catch:{ all -> 0x017e }
                java.lang.String r5 = r5.getFile()     // Catch:{ all -> 0x017e }
                r1.mo3049bV(r5)     // Catch:{ all -> 0x017e }
                a.Os r1 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                a.aBD r1 = r1.f1341lV     // Catch:{ all -> 0x017e }
                a.tC r0 = r0.bEd()     // Catch:{ all -> 0x017e }
                java.lang.String r0 = r0.getHandle()     // Catch:{ all -> 0x017e }
                taikodom.render.loader.RenderAsset r0 = r1.mo3047bT(r0)     // Catch:{ all -> 0x017e }
                taikodom.render.textures.Material r0 = (taikodom.render.textures.Material) r0     // Catch:{ all -> 0x017e }
                if (r0 == 0) goto L_0x0218
                r0.invalidateHandle()     // Catch:{ all -> 0x017e }
                a.Os r1 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                a.aBD r1 = r1.f1341lV     // Catch:{ all -> 0x017e }
                taikodom.render.loader.FileSceneLoader r1 = r1.aej()     // Catch:{ all -> 0x017e }
                taikodom.render.textures.Texture r1 = r1.getDefaultSelfIluminationTexture()     // Catch:{ all -> 0x017e }
                r0.setSelfIluminationTexture(r1)     // Catch:{ all -> 0x017e }
                a.Os r1 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                gnu.trove.THashMap r1 = r1.dKQ     // Catch:{ all -> 0x017e }
                a.Nw$a r5 = p001a.C0954Nw.C0955a.BODY     // Catch:{ all -> 0x017e }
                java.lang.Object r1 = r1.get(r5)     // Catch:{ all -> 0x017e }
                taikodom.render.textures.BaseTexture r1 = (taikodom.render.textures.BaseTexture) r1     // Catch:{ all -> 0x017e }
                r0.setDiffuseTexture(r1)     // Catch:{ all -> 0x017e }
                a.Os r1 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                gnu.trove.THashMap r1 = r1.dKR     // Catch:{ all -> 0x017e }
                a.Nw$a r5 = p001a.C0954Nw.C0955a.BODY     // Catch:{ all -> 0x017e }
                java.lang.Object r1 = r1.get(r5)     // Catch:{ all -> 0x017e }
                taikodom.render.textures.BaseTexture r1 = (taikodom.render.textures.BaseTexture) r1     // Catch:{ all -> 0x017e }
                r0.setNormalTexture(r1)     // Catch:{ all -> 0x017e }
                r4.setMaterial(r0)     // Catch:{ all -> 0x017e }
            L_0x0201:
                r4.buildIndexData()     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                taikodom.render.scene.RModel r0 = r0.rmodel     // Catch:{ all -> 0x017e }
                r4.updateInternalGeometry(r0)     // Catch:{ all -> 0x017e }
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                taikodom.render.scene.RModel r0 = r0.rmodel     // Catch:{ all -> 0x017e }
                r0.addSubMesh(r4)     // Catch:{ all -> 0x017e }
                goto L_0x0129
            L_0x0218:
                a.Os r0 = p001a.C1009Os.this     // Catch:{ all -> 0x017e }
                java.util.Map r0 = r0.dKP     // Catch:{ all -> 0x017e }
                a.Nw$a r1 = p001a.C0954Nw.C0955a.BODY     // Catch:{ all -> 0x017e }
                java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x017e }
                taikodom.render.textures.Material r0 = (taikodom.render.textures.Material) r0     // Catch:{ all -> 0x017e }
                r4.setMaterial(r0)     // Catch:{ all -> 0x017e }
                goto L_0x0201
            L_0x022a:
                a.UR r0 = p001a.C1009Os.logger     // Catch:{ all -> 0x017e }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x017e }
                java.lang.String r4 = "Cannot find RModel (Skeleton data) file for this appearance! [file: "
                r3.<init>(r4)     // Catch:{ all -> 0x017e }
                java.lang.String r4 = r1.getFile()     // Catch:{ all -> 0x017e }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x017e }
                java.lang.String r4 = " , handle: "
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x017e }
                java.lang.String r1 = r1.getHandle()     // Catch:{ all -> 0x017e }
                java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x017e }
                java.lang.String r3 = "]"
                java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x017e }
                java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x017e }
                r0.error(r1)     // Catch:{ all -> 0x017e }
                goto L_0x017b
            */
            throw new UnsupportedOperationException("Method not decompiled: p001a.C1009Os.C1015e.run(taikodom.render.RenderView):void");
        }
    }

    /* renamed from: a.Os$c */
    /* compiled from: a */
    class C1013c implements RenderTask {
        private final /* synthetic */ C0661JS hwW;

        C1013c(C0661JS js) {
            this.hwW = js;
        }

        public void run(RenderView renderView) {
            for (AvatarSector.C0955a a : AvatarSector.C0955a.values()) {
                C1009Os.this.rmodel.removeSubMesh(this.hwW.mo3097a(a));
            }
            this.hwW.akR();
        }
    }

    /* renamed from: a.Os$a */
    class C1010a implements RenderTask {
        private final /* synthetic */ C0661JS hwW;
        private final /* synthetic */ Cloth ihX;

        C1010a(Cloth wj, C0661JS js) {
            this.ihX = wj;
            this.hwW = js;
        }

        public void run(RenderView renderView) {
            Asset bEd = this.ihX.bEd();
            C1009Os.this.f1341lV.mo3049bV(bEd.getFile());
            this.hwW.mo3099a((C3845vi.C3846a) new C1011a(this.hwW, bEd, C1009Os.this.f1341lV.aej().getDefaultSelfIluminationTexture()));
        }

        /* renamed from: a.Os$a$a */
        class C1011a implements C3845vi.C3846a {
            private final /* synthetic */ C0661JS hwW;
            private final /* synthetic */ Asset hwX;
            private final /* synthetic */ BaseTexture hwY;

            C1011a(C0661JS js, Asset tCVar, BaseTexture baseTexture) {
                this.hwW = js;
                this.hwX = tCVar;
                this.hwY = baseTexture;
            }

            /* renamed from: a */
            public void mo4551a(C3845vi viVar, boolean z) {
                if (z) {
                    for (AvatarSector.C0955a aVar : AvatarSector.C0955a.values()) {
                        RCustomMesh a = this.hwW.mo3097a(aVar);
                        if (a != null) {
                            C1009Os.this.rmodel.addSubMesh(a);
                            Material material = (Material) C1009Os.this.f1341lV.mo3047bT(this.hwX.getHandle());
                            material.invalidateHandle();
                            material.setDiffuseTexture((BaseTexture) C1009Os.this.dKQ.get(aVar));
                            material.setNormalTexture((BaseTexture) C1009Os.this.dKR.get(aVar));
                            material.setSelfIluminationTexture(this.hwY);
                            a.setMaterial(material);
                        }
                    }
                    C1009Os.this.blD();
                    C1009Os.this.blC();
                }
            }
        }
    }

    /* renamed from: a.Os$g */
    /* compiled from: a */
    class C1017g implements RenderTask {
        C1017g() {
        }

        public void run(RenderView renderView) {
            C1009Os.this.dLm = false;
            C1009Os.this.blD();
            if (C1009Os.this.dLm) {
                C1009Os.this.f1341lV.mo3007a(C1009Os.this.dLk);
            } else {
                C1009Os.this.dLk = null;
            }
        }
    }

    /* renamed from: a.Os$h */
    /* compiled from: a */
    class C1018h implements RenderTask {
        C1018h() {
        }

        public void run(RenderView renderView) {
            C1009Os.this.dLn = false;
            C1009Os.this.blC();
            if (C1009Os.this.dLn) {
                C1009Os.this.f1341lV.mo3007a(C1009Os.this.dLl);
            } else {
                C1009Os.this.dLl = null;
            }
        }
    }

    /* renamed from: a.Os$f */
    /* compiled from: a */
    class C1016f implements C0907NJ {
        C1016f() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if ((renderAsset instanceof RAnimation) && C1009Os.this.rmodel != null) {
                if (C1009Os.this.dKO != null) {
                    C1009Os.this.dKO.setFadeTime(0.0f);
                    C1009Os.this.dKO.stop();
                    C1009Os.this.rmodel.removeAnimation(C1009Os.this.dKO);
                }
                C1009Os.this.dKO = (RAnimation) renderAsset;
                C1009Os.this.dKO.setFadeTime(0.0f);
                C1009Os.this.rmodel.addAnimation(C1009Os.this.dKO);
                C1009Os.this.dKO.play();
            }
        }
    }
}
