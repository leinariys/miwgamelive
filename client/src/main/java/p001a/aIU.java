package p001a;

/* renamed from: a.aIU */
/* compiled from: a */
public final class aIU extends aOG {
    public static final C5961adl idz = new C5961adl("MouseMoveEvent");
    private final int aCK;
    private final int aCL;

    /* renamed from: dx */
    private final float f3083dx;

    /* renamed from: dy */
    private final float f3084dy;
    private final int idx;
    private final int idy;
    private final int modifiers;

    /* renamed from: x */
    private final float f3085x;

    /* renamed from: y */
    private final float f3086y;

    public aIU(float f, float f2, float f3, float f4, int i, int i2, int i3, int i4, int i5) {
        this.f3085x = f;
        this.f3086y = f2;
        this.f3083dx = f3;
        this.f3084dy = f4;
        this.aCK = i;
        this.aCL = i2;
        this.idx = i3;
        this.idy = i4;
        this.modifiers = i5;
    }

    public final float getDx() {
        return this.f3083dx;
    }

    public final float getDy() {
        return this.f3084dy;
    }

    public final float getX() {
        return this.f3085x;
    }

    public final float getY() {
        return this.f3086y;
    }

    public final int dfi() {
        return this.idx;
    }

    public final int dfj() {
        return this.idy;
    }

    public final int getScreenX() {
        return this.aCK;
    }

    public final int getScreenY() {
        return this.aCL;
    }

    /* renamed from: Fp */
    public C5961adl mo2005Fp() {
        return idz;
    }

    public int getModifiers() {
        return this.modifiers;
    }
}
