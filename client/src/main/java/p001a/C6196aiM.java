package p001a;

import org.mozilla1.javascript.Context;
import org.mozilla1.javascript.Function;
import org.mozilla1.javascript.Scriptable;
import org.mozilla1.javascript.Wrapper;

/* renamed from: a.aiM  reason: case insensitive filesystem */
/* compiled from: a */
public class C6196aiM extends C6012aek {
    public C6196aiM(Scriptable avf) {
        super(avf);
    }

    public Object call(Context lhVar, Scriptable avf, Scriptable avf2, Object[] objArr) {
        Object obj;
        Object call;
        if (avf2 instanceof Wrapper) {
            obj = ((Wrapper) avf2).unwrap();
        } else {
            obj = avf2;
        }
        synchronized (obj) {
            call = ((Function) this.fnr).call(lhVar, avf, avf2, objArr);
        }
        return call;
    }
}
