package p001a;

import com.hoplon.geometry.Vec3f;
import logic.abc.azD;

/* renamed from: a.zJ */
/* compiled from: a */
public class C4090zJ implements C5852abg {

    /* renamed from: jR */
    static final /* synthetic */ boolean f9644jR = (!C4090zJ.class.desiredAssertionStatus());

    /* renamed from: mN */
    private static final float f9645mN = 1.0E-6f;
    public final C0763Kt stack = C0763Kt.bcE();
    /* renamed from: mO */
    private final Vec3f f9647mO = new Vec3f(0.0f, 0.0f, 1.0f);
    /* renamed from: mT */
    public int f9649mT = -1;
    /* renamed from: mU */
    public int f9650mU;
    /* renamed from: mV */
    public int f9651mV;
    /* renamed from: mW */
    public int f9652mW = 1;
    private C5434aJe caI;
    private azD caJ;
    private azD caK;
    /* renamed from: lL */
    private C6331akr f9646lL;
    /* renamed from: mS */
    private boolean f9648mS = false;

    public C4090zJ(azD azd, azD azd2, C6331akr akr, C5434aJe aje) {
        this.caI = aje;
        this.f9646lL = akr;
        this.caJ = azd;
        this.caK = azd2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:77:0x0409 A[Catch:{ all -> 0x0207 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo12499a(p001a.C5852abg.C1854a r25, p001a.C5852abg.C1855b r26, p001a.C1072Pg r27) {
        /*
            r24 = this;
            r0 = r24
            a.Kt r2 = r0.stack
            r2.bcF()
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0207 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0207 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0207 }
            r0 = r2
            com.hoplon.geometry.Vec3f r0 = (com.hoplon.geometry.Vec3f) r0     // Catch:{ all -> 0x0207 }
            r12 = r0
            r17 = 0
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0207 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0207 }
            r3 = 0
            r4 = 0
            r5 = 0
            com.hoplon.geometry.Vec3f r18 = r2.mo4460h(r3, r4, r5)     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0207 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0207 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0207 }
            r0 = r2
            com.hoplon.geometry.Vec3f r0 = (com.hoplon.geometry.Vec3f) r0     // Catch:{ all -> 0x0207 }
            r13 = r0
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0207 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0207 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0207 }
            r0 = r2
            com.hoplon.geometry.Vec3f r0 = (com.hoplon.geometry.Vec3f) r0     // Catch:{ all -> 0x0207 }
            r14 = r0
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0207 }
            a.Cg r2 = r2.bcJ()     // Catch:{ all -> 0x0207 }
            r0 = r25
            a.xf r3 = r0.fGA     // Catch:{ all -> 0x0207 }
            a.xf r6 = r2.mo1157d((p001a.C3978xf) r3)     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0207 }
            a.Cg r2 = r2.bcJ()     // Catch:{ all -> 0x0207 }
            r0 = r25
            a.xf r3 = r0.fGB     // Catch:{ all -> 0x0207 }
            a.xf r7 = r2.mo1157d((p001a.C3978xf) r3)     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0207 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0207 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0207 }
            r0 = r2
            com.hoplon.geometry.Vec3f r0 = (com.hoplon.geometry.Vec3f) r0     // Catch:{ all -> 0x0207 }
            r15 = r0
            com.hoplon.geometry.Vec3f r2 = r6.bFG     // Catch:{ all -> 0x0207 }
            com.hoplon.geometry.Vec3f r3 = r7.bFG     // Catch:{ all -> 0x0207 }
            r15.add(r2, r3)     // Catch:{ all -> 0x0207 }
            r2 = 1056964608(0x3f000000, float:0.5)
            r15.scale(r2)     // Catch:{ all -> 0x0207 }
            com.hoplon.geometry.Vec3f r2 = r6.bFG     // Catch:{ all -> 0x0207 }
            r2.sub(r15)     // Catch:{ all -> 0x0207 }
            com.hoplon.geometry.Vec3f r2 = r7.bFG     // Catch:{ all -> 0x0207 }
            r2.sub(r15)     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.azD r2 = r0.caJ     // Catch:{ all -> 0x0207 }
            float r3 = r2.getMargin()     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.azD r2 = r0.caK     // Catch:{ all -> 0x0207 }
            float r2 = r2.getMargin()     // Catch:{ all -> 0x0207 }
            int r4 = p001a.C0128Bb.dMJ     // Catch:{ all -> 0x0207 }
            int r4 = r4 + 1
            p001a.C0128Bb.dMJ = r4     // Catch:{ all -> 0x0207 }
            r0 = r24
            boolean r4 = r0.f9648mS     // Catch:{ all -> 0x0207 }
            if (r4 == 0) goto L_0x043c
            r3 = 0
            r2 = 0
            r9 = r2
            r10 = r3
        L_0x00ae:
            r2 = 0
            r0 = r24
            r0.f9650mU = r2     // Catch:{ all -> 0x0207 }
            r11 = 1000(0x3e8, float:1.401E-42)
            r0 = r24
            com.hoplon.geometry.Vec3f r2 = r0.f9647mO     // Catch:{ all -> 0x0207 }
            r3 = 0
            r4 = 1065353216(0x3f800000, float:1.0)
            r5 = 0
            r2.set(r3, r4, r5)     // Catch:{ all -> 0x0207 }
            r16 = 0
            r8 = 0
            r5 = 1
            r2 = 0
            r0 = r24
            r0.f9651mV = r2     // Catch:{ all -> 0x0207 }
            r2 = -1
            r0 = r24
            r0.f9649mT = r2     // Catch:{ all -> 0x0207 }
            r4 = 2139095039(0x7f7fffff, float:3.4028235E38)
            float r19 = r10 + r9
            r0 = r24
            a.akr r2 = r0.f9646lL     // Catch:{ all -> 0x0207 }
            r2.reset()     // Catch:{ all -> 0x0207 }
        L_0x00da:
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0207 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0207 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0207 }
            com.hoplon.geometry.Vec3f r2 = (com.hoplon.geometry.Vec3f) r2     // Catch:{ all -> 0x0207 }
            r0 = r24
            com.hoplon.geometry.Vec3f r3 = r0.f9647mO     // Catch:{ all -> 0x0207 }
            r2.negate(r3)     // Catch:{ all -> 0x0207 }
            r0 = r25
            a.xf r3 = r0.fGA     // Catch:{ all -> 0x0207 }
            a.ajD r3 = r3.bFF     // Catch:{ all -> 0x0207 }
            p001a.C3427rS.m38372a((com.hoplon.geometry.Vec3f) r2, (com.hoplon.geometry.Vec3f) r2, (p001a.C6239ajD) r3)     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.Kt r3 = r0.stack     // Catch:{ all -> 0x0207 }
            a.OY r3 = r3.bcH()     // Catch:{ all -> 0x0207 }
            java.lang.Object r3 = r3.get()     // Catch:{ all -> 0x0207 }
            com.hoplon.geometry.Vec3f r3 = (com.hoplon.geometry.Vec3f) r3     // Catch:{ all -> 0x0207 }
            r0 = r24
            com.hoplon.geometry.Vec3f r0 = r0.f9647mO     // Catch:{ all -> 0x0207 }
            r20 = r0
            r0 = r20
            r3.set(r0)     // Catch:{ all -> 0x0207 }
            r0 = r25
            a.xf r0 = r0.fGB     // Catch:{ all -> 0x0207 }
            r20 = r0
            r0 = r20
            a.ajD r0 = r0.bFF     // Catch:{ all -> 0x0207 }
            r20 = r0
            r0 = r20
            p001a.C3427rS.m38372a((com.hoplon.geometry.Vec3f) r3, (com.hoplon.geometry.Vec3f) r3, (p001a.C6239ajD) r0)     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.Kt r0 = r0.stack     // Catch:{ all -> 0x0207 }
            r20 = r0
            a.OY r20 = r20.bcH()     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.azD r0 = r0.caJ     // Catch:{ all -> 0x0207 }
            r21 = r0
            r0 = r21
            com.hoplon.geometry.Vec3f r2 = r0.localGetSupportingVertexWithoutMargin(r2)     // Catch:{ all -> 0x0207 }
            r0 = r20
            com.hoplon.geometry.Vec3f r2 = r0.mo4458ac(r2)     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.Kt r0 = r0.stack     // Catch:{ all -> 0x0207 }
            r20 = r0
            a.OY r20 = r20.bcH()     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.azD r0 = r0.caK     // Catch:{ all -> 0x0207 }
            r21 = r0
            r0 = r21
            com.hoplon.geometry.Vec3f r3 = r0.localGetSupportingVertexWithoutMargin(r3)     // Catch:{ all -> 0x0207 }
            r0 = r20
            com.hoplon.geometry.Vec3f r3 = r0.mo4458ac(r3)     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.Kt r0 = r0.stack     // Catch:{ all -> 0x0207 }
            r20 = r0
            a.OY r20 = r20.bcH()     // Catch:{ all -> 0x0207 }
            r0 = r20
            com.hoplon.geometry.Vec3f r20 = r0.mo4458ac(r2)     // Catch:{ all -> 0x0207 }
            r0 = r20
            r6.mo22946G(r0)     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0207 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0207 }
            com.hoplon.geometry.Vec3f r3 = r2.mo4458ac(r3)     // Catch:{ all -> 0x0207 }
            r7.mo22946G(r3)     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0207 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0207 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0207 }
            com.hoplon.geometry.Vec3f r2 = (com.hoplon.geometry.Vec3f) r2     // Catch:{ all -> 0x0207 }
            r0 = r20
            r2.sub(r0, r3)     // Catch:{ all -> 0x0207 }
            r0 = r24
            com.hoplon.geometry.Vec3f r0 = r0.f9647mO     // Catch:{ all -> 0x0207 }
            r21 = r0
            r0 = r21
            float r21 = r0.dot(r2)     // Catch:{ all -> 0x0207 }
            r22 = 0
            int r22 = (r21 > r22 ? 1 : (r21 == r22 ? 0 : -1))
            if (r22 <= 0) goto L_0x0210
            float r22 = r21 * r21
            r0 = r25
            float r0 = r0.fGC     // Catch:{ all -> 0x0207 }
            r23 = r0
            float r23 = r23 * r4
            int r22 = (r22 > r23 ? 1 : (r22 == r23 ? 0 : -1))
            if (r22 <= 0) goto L_0x0210
            r3 = 0
            r2 = r4
        L_0x01b3:
            if (r8 == 0) goto L_0x035a
            r0 = r24
            a.akr r4 = r0.f9646lL     // Catch:{ all -> 0x0207 }
            r4.mo1844D(r13, r14)     // Catch:{ all -> 0x0207 }
            r0 = r18
            r0.sub(r13, r14)     // Catch:{ all -> 0x0207 }
            r0 = r24
            com.hoplon.geometry.Vec3f r4 = r0.f9647mO     // Catch:{ all -> 0x0207 }
            float r4 = r4.lengthSquared()     // Catch:{ all -> 0x0207 }
            r5 = 953267991(0x38d1b717, float:1.0E-4)
            int r5 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r5 >= 0) goto L_0x01d5
            r5 = 5
            r0 = r24
            r0.f9651mV = r5     // Catch:{ all -> 0x0207 }
        L_0x01d5:
            r5 = 679477248(0x28800000, float:1.4210855E-14)
            int r5 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r5 <= 0) goto L_0x041b
            r5 = 1065353216(0x3f800000, float:1.0)
            double r0 = (double) r4     // Catch:{ all -> 0x0207 }
            r16 = r0
            double r16 = java.lang.Math.sqrt(r16)     // Catch:{ all -> 0x0207 }
            r0 = r16
            float r4 = (float) r0     // Catch:{ all -> 0x0207 }
            float r4 = r5 / r4
            r0 = r18
            r0.scale(r4)     // Catch:{ all -> 0x0207 }
            double r0 = (double) r2     // Catch:{ all -> 0x0207 }
            r16 = r0
            double r16 = java.lang.Math.sqrt(r16)     // Catch:{ all -> 0x0207 }
            r0 = r16
            float r2 = (float) r0     // Catch:{ all -> 0x0207 }
            boolean r5 = f9644jR     // Catch:{ all -> 0x0207 }
            if (r5 != 0) goto L_0x0336
            r5 = 0
            int r5 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r5 > 0) goto L_0x0336
            java.lang.AssertionError r2 = new java.lang.AssertionError     // Catch:{ all -> 0x0207 }
            r2.<init>()     // Catch:{ all -> 0x0207 }
            throw r2     // Catch:{ all -> 0x0207 }
        L_0x0207:
            r2 = move-exception
            r0 = r24
            a.Kt r3 = r0.stack
            r3.bcG()
            throw r2
        L_0x0210:
            r0 = r24
            a.akr r0 = r0.f9646lL     // Catch:{ all -> 0x0207 }
            r22 = r0
            r0 = r22
            boolean r22 = r0.mo1850aL(r2)     // Catch:{ all -> 0x0207 }
            if (r22 == 0) goto L_0x0227
            r2 = 1
            r0 = r24
            r0.f9651mV = r2     // Catch:{ all -> 0x0207 }
            r8 = 1
            r2 = r4
            r3 = r5
            goto L_0x01b3
        L_0x0227:
            float r21 = r4 - r21
            r22 = 897988541(0x358637bd, float:1.0E-6)
            float r22 = r22 * r4
            int r22 = (r21 > r22 ? 1 : (r21 == r22 ? 0 : -1))
            if (r22 > 0) goto L_0x0241
            r2 = 0
            int r2 = (r21 > r2 ? 1 : (r21 == r2 ? 0 : -1))
            if (r2 > 0) goto L_0x023c
            r2 = 2
            r0 = r24
            r0.f9651mV = r2     // Catch:{ all -> 0x0207 }
        L_0x023c:
            r8 = 1
            r2 = r4
            r3 = r5
            goto L_0x01b3
        L_0x0241:
            r0 = r24
            a.akr r0 = r0.f9646lL     // Catch:{ all -> 0x0207 }
            r21 = r0
            r0 = r21
            r1 = r20
            r0.mo1858p(r2, r1, r3)     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.akr r2 = r0.f9646lL     // Catch:{ all -> 0x0207 }
            r0 = r24
            com.hoplon.geometry.Vec3f r3 = r0.f9647mO     // Catch:{ all -> 0x0207 }
            boolean r2 = r2.mo1849aK(r3)     // Catch:{ all -> 0x0207 }
            if (r2 != 0) goto L_0x0266
            r2 = 3
            r0 = r24
            r0.f9651mV = r2     // Catch:{ all -> 0x0207 }
            r8 = 1
            r2 = r4
            r3 = r5
            goto L_0x01b3
        L_0x0266:
            r0 = r24
            com.hoplon.geometry.Vec3f r2 = r0.f9647mO     // Catch:{ all -> 0x0207 }
            float r2 = r2.lengthSquared()     // Catch:{ all -> 0x0207 }
            float r3 = r4 - r2
            r20 = 872415232(0x34000000, float:1.1920929E-7)
            float r4 = r4 * r20
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 > 0) goto L_0x0288
            r0 = r24
            a.akr r3 = r0.f9646lL     // Catch:{ all -> 0x0207 }
            r0 = r24
            com.hoplon.geometry.Vec3f r4 = r0.f9647mO     // Catch:{ all -> 0x0207 }
            r3.mo1851aM(r4)     // Catch:{ all -> 0x0207 }
            r4 = 1
            r3 = r5
            r8 = r4
            goto L_0x01b3
        L_0x0288:
            r0 = r24
            int r3 = r0.f9650mU     // Catch:{ all -> 0x0207 }
            int r4 = r3 + 1
            r0 = r24
            r0.f9650mU = r4     // Catch:{ all -> 0x0207 }
            if (r3 <= r11) goto L_0x0319
            java.io.PrintStream r3 = java.lang.System.err     // Catch:{ all -> 0x0207 }
            java.lang.String r4 = "btGjkPairDetector maxIter exceeded:%i\n"
            r11 = 1
            java.lang.Object[] r11 = new java.lang.Object[r11]     // Catch:{ all -> 0x0207 }
            r20 = 0
            r0 = r24
            int r0 = r0.f9650mU     // Catch:{ all -> 0x0207 }
            r21 = r0
            java.lang.Integer r21 = java.lang.Integer.valueOf(r21)     // Catch:{ all -> 0x0207 }
            r11[r20] = r21     // Catch:{ all -> 0x0207 }
            r3.printf(r4, r11)     // Catch:{ all -> 0x0207 }
            java.io.PrintStream r3 = java.lang.System.err     // Catch:{ all -> 0x0207 }
            java.lang.String r4 = "sepAxis=(%f,%f,%f), squaredDistance = %f, shapeTypeA=%i,shapeTypeB=%i\n"
            r11 = 6
            java.lang.Object[] r11 = new java.lang.Object[r11]     // Catch:{ all -> 0x0207 }
            r20 = 0
            r0 = r24
            com.hoplon.geometry.Vec3f r0 = r0.f9647mO     // Catch:{ all -> 0x0207 }
            r21 = r0
            r0 = r21
            float r0 = r0.x     // Catch:{ all -> 0x0207 }
            r21 = r0
            java.lang.Float r21 = java.lang.Float.valueOf(r21)     // Catch:{ all -> 0x0207 }
            r11[r20] = r21     // Catch:{ all -> 0x0207 }
            r20 = 1
            r0 = r24
            com.hoplon.geometry.Vec3f r0 = r0.f9647mO     // Catch:{ all -> 0x0207 }
            r21 = r0
            r0 = r21
            float r0 = r0.y     // Catch:{ all -> 0x0207 }
            r21 = r0
            java.lang.Float r21 = java.lang.Float.valueOf(r21)     // Catch:{ all -> 0x0207 }
            r11[r20] = r21     // Catch:{ all -> 0x0207 }
            r20 = 2
            r0 = r24
            com.hoplon.geometry.Vec3f r0 = r0.f9647mO     // Catch:{ all -> 0x0207 }
            r21 = r0
            r0 = r21
            float r0 = r0.z     // Catch:{ all -> 0x0207 }
            r21 = r0
            java.lang.Float r21 = java.lang.Float.valueOf(r21)     // Catch:{ all -> 0x0207 }
            r11[r20] = r21     // Catch:{ all -> 0x0207 }
            r20 = 3
            java.lang.Float r21 = java.lang.Float.valueOf(r2)     // Catch:{ all -> 0x0207 }
            r11[r20] = r21     // Catch:{ all -> 0x0207 }
            r20 = 4
            r0 = r24
            a.azD r0 = r0.caJ     // Catch:{ all -> 0x0207 }
            r21 = r0
            a.tK r21 = r21.arV()     // Catch:{ all -> 0x0207 }
            r11[r20] = r21     // Catch:{ all -> 0x0207 }
            r20 = 5
            r0 = r24
            a.azD r0 = r0.caK     // Catch:{ all -> 0x0207 }
            r21 = r0
            a.tK r21 = r21.arV()     // Catch:{ all -> 0x0207 }
            r11[r20] = r21     // Catch:{ all -> 0x0207 }
            r3.printf(r4, r11)     // Catch:{ all -> 0x0207 }
            r3 = r5
            goto L_0x01b3
        L_0x0319:
            r0 = r24
            a.akr r3 = r0.f9646lL     // Catch:{ all -> 0x0207 }
            boolean r3 = r3.cgj()     // Catch:{ all -> 0x0207 }
            if (r3 == 0) goto L_0x0334
            r3 = 0
        L_0x0324:
            if (r3 != 0) goto L_0x0439
            r0 = r24
            a.akr r3 = r0.f9646lL     // Catch:{ all -> 0x0207 }
            r0 = r24
            com.hoplon.geometry.Vec3f r4 = r0.f9647mO     // Catch:{ all -> 0x0207 }
            r3.mo1851aM(r4)     // Catch:{ all -> 0x0207 }
            r3 = r5
            goto L_0x01b3
        L_0x0334:
            r3 = 1
            goto L_0x0324
        L_0x0336:
            float r5 = r10 / r2
            r0 = r24
            com.hoplon.geometry.Vec3f r8 = r0.f9647mO     // Catch:{ all -> 0x0207 }
            r12.scale(r5, r8)     // Catch:{ all -> 0x0207 }
            r13.sub(r12)     // Catch:{ all -> 0x0207 }
            float r2 = r9 / r2
            r0 = r24
            com.hoplon.geometry.Vec3f r5 = r0.f9647mO     // Catch:{ all -> 0x0207 }
            r12.scale(r2, r5)     // Catch:{ all -> 0x0207 }
            r14.add(r12)     // Catch:{ all -> 0x0207 }
            r2 = 1065353216(0x3f800000, float:1.0)
            float r2 = r2 / r4
            float r17 = r2 - r19
            r16 = 1
            r2 = 1
            r0 = r24
            r0.f9649mT = r2     // Catch:{ all -> 0x0207 }
        L_0x035a:
            r0 = r24
            int r2 = r0.f9652mW     // Catch:{ all -> 0x0207 }
            if (r2 == 0) goto L_0x0422
            r0 = r24
            a.aJe r2 = r0.caI     // Catch:{ all -> 0x0207 }
            if (r2 == 0) goto L_0x0422
            r0 = r24
            int r2 = r0.f9651mV     // Catch:{ all -> 0x0207 }
            if (r2 == 0) goto L_0x0422
            float r2 = r17 + r19
            r4 = 1008981770(0x3c23d70a, float:0.01)
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 >= 0) goto L_0x0422
            r2 = 1
        L_0x0376:
            if (r3 == 0) goto L_0x0434
            if (r16 == 0) goto L_0x037c
            if (r2 == 0) goto L_0x0434
        L_0x037c:
            r0 = r24
            a.aJe r2 = r0.caI     // Catch:{ all -> 0x0207 }
            if (r2 == 0) goto L_0x0434
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0207 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0207 }
            java.lang.Object r9 = r2.get()     // Catch:{ all -> 0x0207 }
            com.hoplon.geometry.Vec3f r9 = (com.hoplon.geometry.Vec3f) r9     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0207 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0207 }
            java.lang.Object r10 = r2.get()     // Catch:{ all -> 0x0207 }
            com.hoplon.geometry.Vec3f r10 = (com.hoplon.geometry.Vec3f) r10     // Catch:{ all -> 0x0207 }
            int r2 = p001a.C0128Bb.dMI     // Catch:{ all -> 0x0207 }
            int r2 = r2 + 1
            p001a.C0128Bb.dMI = r2     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.aJe r2 = r0.caI     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.akr r3 = r0.f9646lL     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.azD r4 = r0.caJ     // Catch:{ all -> 0x0207 }
            r0 = r24
            a.azD r5 = r0.caK     // Catch:{ all -> 0x0207 }
            r0 = r24
            com.hoplon.geometry.Vec3f r8 = r0.f9647mO     // Catch:{ all -> 0x0207 }
            r11 = r27
            boolean r2 = r2.mo9594a(r3, r4, r5, r6, r7, r8, r9, r10, r11)     // Catch:{ all -> 0x0207 }
            if (r2 == 0) goto L_0x042f
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0207 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0207 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0207 }
            com.hoplon.geometry.Vec3f r2 = (com.hoplon.geometry.Vec3f) r2     // Catch:{ all -> 0x0207 }
            r2.sub(r10, r9)     // Catch:{ all -> 0x0207 }
            float r3 = r2.lengthSquared()     // Catch:{ all -> 0x0207 }
            r4 = 679477248(0x28800000, float:1.4210855E-14)
            int r4 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x0425
            r4 = 1065353216(0x3f800000, float:1.0)
            double r6 = (double) r3     // Catch:{ all -> 0x0207 }
            double r6 = java.lang.Math.sqrt(r6)     // Catch:{ all -> 0x0207 }
            float r3 = (float) r6     // Catch:{ all -> 0x0207 }
            float r3 = r4 / r3
            r2.scale(r3)     // Catch:{ all -> 0x0207 }
            r12.sub(r9, r10)     // Catch:{ all -> 0x0207 }
            float r3 = r12.length()     // Catch:{ all -> 0x0207 }
            float r3 = -r3
            if (r16 == 0) goto L_0x03f6
            int r4 = (r3 > r17 ? 1 : (r3 == r17 ? 0 : -1))
            if (r4 >= 0) goto L_0x0434
        L_0x03f6:
            r13.set(r9)     // Catch:{ all -> 0x0207 }
            r14.set(r10)     // Catch:{ all -> 0x0207 }
            r0 = r18
            r0.set(r2)     // Catch:{ all -> 0x0207 }
            r2 = 1
            r4 = 3
            r0 = r24
            r0.f9649mT = r4     // Catch:{ all -> 0x0207 }
        L_0x0407:
            if (r2 == 0) goto L_0x0413
            r12.add(r14, r15)     // Catch:{ all -> 0x0207 }
            r0 = r26
            r1 = r18
            r0.mo5217c(r1, r12, r3)     // Catch:{ all -> 0x0207 }
        L_0x0413:
            r0 = r24
            a.Kt r2 = r0.stack
            r2.bcG()
            return
        L_0x041b:
            r2 = 2
            r0 = r24
            r0.f9649mT = r2     // Catch:{ all -> 0x0207 }
            goto L_0x035a
        L_0x0422:
            r2 = 0
            goto L_0x0376
        L_0x0425:
            r2 = 4
            r0 = r24
            r0.f9649mT = r2     // Catch:{ all -> 0x0207 }
            r2 = r16
            r3 = r17
            goto L_0x0407
        L_0x042f:
            r2 = 5
            r0 = r24
            r0.f9649mT = r2     // Catch:{ all -> 0x0207 }
        L_0x0434:
            r2 = r16
            r3 = r17
            goto L_0x0407
        L_0x0439:
            r4 = r2
            goto L_0x00da
        L_0x043c:
            r9 = r2
            r10 = r3
            goto L_0x00ae
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C4090zJ.mo12499a(a.abg$a, a.abg$b, a.Pg):void");
    }

    /* renamed from: a */
    public void mo23283a(azD azd) {
        this.caJ = azd;
    }

    /* renamed from: b */
    public void mo23284b(azD azd) {
        this.caK = azd;
    }

    /* renamed from: d */
    public void mo23285d(Vec3f vec3f) {
        this.f9647mO.set(vec3f);
    }

    /* renamed from: a */
    public void mo23282a(C5434aJe aje) {
        this.caI = aje;
    }

    /* renamed from: v */
    public void mo23286v(boolean z) {
        this.f9648mS = z;
    }
}
