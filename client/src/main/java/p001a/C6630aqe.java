package p001a;

import game.network.message.serializable.C2991mi;
import logic.baa.C1616Xf;

/* renamed from: a.aqe  reason: case insensitive filesystem */
/* compiled from: a */
public class C6630aqe<T> implements C2991mi<T> {

    private final C0651JP cUJ;
    private final Object value;

    public C6630aqe(C0651JP jp, Object obj) {
        this.cUJ = jp;
        this.value = obj;
    }

    public C0651JP aPk() {
        return this.cUJ;
    }

    public int index() {
        return -1;
    }

    public Object value() {
        return this.value;
    }

    public String toString() {
        return String.valueOf(getClass().getSimpleName()) + " " + this.cUJ + " " + (this.value instanceof C1616Xf ? String.valueOf(this.value.getClass().getName()) + ":" + ((C1616Xf) this.value).bFf().getObjectId().getId() : this.value);
    }
}
