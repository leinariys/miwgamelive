package p001a;

import game.geometry.Vec3d;
import game.script.Character;

/* renamed from: a.Dh */
/* compiled from: a */
public interface C0286Dh {

    /* renamed from: Y */
    boolean mo599Y(float f);

    /* renamed from: d */
    C0287a mo634d(Character acx);

    String getName();

    Vec3d getPosition();

    /* renamed from: a.Dh$a */
    public enum C0287a {
        OK,
        ERROR_DEACTIVATED,
        ERROR_CRUISE_SPEED,
        ERROR_BANNED_FACTION,
        ERROR_CORP_EXCLUSIVE,
        ERROR_CANT_GET_LOOT,
        ERROR_OUT_OF_RANGE,
        ERROR_ALREADY_DOCKED,
        ERROR_OBJECT_IN_USE,
        ERROR_NOT_ENOUGH_SPACE,
        ERROR_UNKNOWN,
        ERROR_NOT_ENOUGH_MONEY,
        CITIZENSHIP_RESTRICTED_ACCESS,
        CITIZENSHIP_ONLY,
        PASS_UNAUTHORIZED,
        NOT_NECESSARY
    }
}
