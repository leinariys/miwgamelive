package p001a;

/* renamed from: a.tK */
/* compiled from: a */
public enum C3655tK {
    BOX_SHAPE_PROXYTYPE,
    TRIANGLE_SHAPE_PROXYTYPE,
    TETRAHEDRAL_SHAPE_PROXYTYPE,
    CONVEX_TRIANGLEMESH_SHAPE_PROXYTYPE,
    CONVEX_HULL_SHAPE_PROXYTYPE,
    IMPLICIT_CONVEX_SHAPES_START_HERE,
    SPHERE_SHAPE_PROXYTYPE,
    MULTI_SPHERE_SHAPE_PROXYTYPE,
    CAPSULE_SHAPE_PROXYTYPE,
    CONE_SHAPE_PROXYTYPE,
    CONVEX_SHAPE_PROXYTYPE,
    CYLINDER_SHAPE_PROXYTYPE,
    UNIFORM_SCALING_SHAPE_PROXYTYPE,
    MINKOWSKI_SUM_SHAPE_PROXYTYPE,
    MINKOWSKI_DIFFERENCE_SHAPE_PROXYTYPE,
    CONCAVE_SHAPES_START_HERE,
    TRIANGLE_MESH_SHAPE_PROXYTYPE,
    FAST_CONCAVE_MESH_PROXYTYPE,
    TERRAIN_SHAPE_PROXYTYPE,
    GIMPACT_SHAPE_PROXYTYPE,
    EMPTY_SHAPE_PROXYTYPE,
    STATIC_PLANE_PROXYTYPE,
    CONCAVE_SHAPES_END_HERE,
    COMPOUND_SHAPE_PROXYTYPE,
    MAX_BROADPHASE_COLLISION_TYPES;

    private static C3655tK[] bsZ;

    static {
        bsZ = values();
    }

    /* renamed from: eM */
    public static C3655tK m39582eM(int i) {
        return bsZ[i];
    }

    public boolean aiE() {
        return ordinal() < IMPLICIT_CONVEX_SHAPES_START_HERE.ordinal();
    }

    public boolean aiF() {
        return ordinal() < CONCAVE_SHAPES_START_HERE.ordinal();
    }

    public boolean aiG() {
        return ordinal() > CONCAVE_SHAPES_START_HERE.ordinal() && ordinal() < CONCAVE_SHAPES_END_HERE.ordinal();
    }

    public boolean isCompound() {
        return ordinal() == COMPOUND_SHAPE_PROXYTYPE.ordinal();
    }

    public boolean isInfinite() {
        return ordinal() == STATIC_PLANE_PROXYTYPE.ordinal();
    }
}
