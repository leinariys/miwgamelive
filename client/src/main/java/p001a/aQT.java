package p001a;

import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import taikodom.render.camera.Camera;

import javax.vecmath.Quat4f;

/* renamed from: a.aQT */
/* compiled from: a */
public class aQT extends C6258ajW {
    private static final float iGd = 0.5f;
    private float currentTime = 0.0f;
    private C0763Kt stack = C0763Kt.bcE();

    public aQT(Camera camera) {
        super(camera);
    }

    public void clearState() {
        this.currentTime = 0.0f;
    }

    /* renamed from: a */
    public void mo3859a(float f, C1003Om om) {
        this.stack.bcI().push();
        this.stack.bcH().push();
        this.stack.bcN().push();
        try {
            Vec3d ajr = (Vec3d) this.stack.bcI().get();
            ajr.mo9484aA(this.fSF.mo2472kQ().mo4490IQ().position);
            Vec3d ajr2 = (Vec3d) this.stack.bcI().get();
            ajr2.mo9484aA(om.mo4490IQ().position);
            Quat4fWrap aoy = (Quat4fWrap) this.stack.bcN().get();
            Quat4fWrap aoy2 = (Quat4fWrap) this.stack.bcN().get();
            this.fSF.mo2472kQ().mo4490IQ().mo17347c((Quat4f) aoy);
            om.mo4490IQ().mo17347c((Quat4f) aoy2);
            double d = (double) (this.currentTime / iGd);
            this.currentTime += f;
            if (this.currentTime > 0.1f) {
                if (this.fSG != null) {
                    this.fSG.hide();
                }
                if (this.currentTime > iGd) {
                    this.currentTime = iGd;
                }
            }
            om.bls().set(0.0f, 0.0f, 0.0f);
            om.blt().set(0.0f, 0.0f, 0.0f);
            om.blp().set(0.0f, 0.0f, 0.0f);
            om.bln().set(0.0f, 0.0f, 0.0f);
            om.blr().set(0.0f, 0.0f, 0.0f);
            om.blq().set(0.0f, 0.0f, 0.0f);
            om.blo().set(0.0f, 0.0f, 0.0f);
            om.blm().set(0.0f, 0.0f, 0.0f);
            om.mo4490IQ().position.interpolate(ajr2, ajr, d);
            aoy2.interpolate(aoy, (float) d);
            om.mo4490IQ().mo17335a(aoy2);
            float length = this.fSF.mo2472kQ().blm().length() / this.fSB;
            if (this.camera != null) {
                this.camera.setFovY((((length * this.fSE) + (this.fSD * (1.0f - length))) * 0.1f) + (this.camera.getFovY() * 0.9f));
            }
        } finally {
            this.stack.bcI().pop();
            this.stack.bcH().pop();
            this.stack.bcN().pop();
        }
    }
}
