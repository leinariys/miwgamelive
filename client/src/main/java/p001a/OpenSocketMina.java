package p001a;

import game.network.build.ManagerMessageTransport;
import game.network.build.TransportThread;
import game.network.channel.*;
import game.network.channel.client.ClientConnect;
import game.network.channel.client.ClientConnectImpl;
import game.network.message.TransportCommand;
import game.network.message.TransportResponse;
import game.network.message.externalizable.AuthorizationExt;
import game.network.message.externalizable.ConnectStatusExt;
import game.network.message.externalizable.VersionExt;
import logic.thred.LogPrinter;
import org.apache.mina.core.service.IoService;
import org.apache.mina.core.session.AttributeKey;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: a.iU */
/* compiled from: a */
public abstract class OpenSocketMina implements TransportThread, NetworkChannel {
    static final AttributeKey anW = new AttributeKey(OpenSocketMina.class, "connection");
    static final Map<Class<?>, Integer> classes = new HashMap();
    static LogPrinter logger = LogPrinter.m10275K(OpenSocketMina.class);
    static AtomicInteger nextId = new AtomicInteger(0);

    static {
        classes.put(VersionExt.class, 0);
        classes.put(AuthorizationExt.class, 1);
        classes.put(ConnectStatusExt.class, 2);
    }

    private final String versionString;
    private ClientConnectImpl anX;
    private ManagerMessageTransport anY;
    private IoService anZ;
    private IoSession ioSession = null;

    public OpenSocketMina(ClientConnect.Attitude aVar, IoService ioService, String str) {
        this.anZ = ioService;
        this.versionString = str;
        try {
            this.anX = new ClientConnectImpl(aVar, nextId.getAndIncrement(), InetAddress.getLocalHost());
            ioService.getFilterChain().addLast("codec", new C3824vT(new C2681b()));
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: a */
    public static C5772aaE m33375a(IoSession ioSession) {
        return (C5772aaE) ioSession.getAttribute(anW);
    }

    /* renamed from: e */
    public static C5772aaE m33376e(ClientConnect bVar) {
        return (C5772aaE) bVar;
    }

    /* renamed from: iX */
    public abstract boolean isRunningThreadSelectableChannel();

    /* renamed from: iY */
    public abstract boolean isListenerPort();

    public String getVersionString() {
        return this.versionString;
    }

    /* renamed from: HV */
    public ManagerMessageTransport mo19614HV() {
        return this.anY;
    }

    /* renamed from: HW */
    public IoSession getIoSession() {
        return this.ioSession;
    }

    /* renamed from: b */
    public void mo19617b(IoSession ioSession) {
        this.ioSession = ioSession;
    }

    public IoService getService() {
        return this.anZ;
    }

    public void close() {
        this.anZ.dispose();
    }

    /* renamed from: jH */
    public ClientConnect getClientConnect() {
        return this.anX;
    }

    /* renamed from: jK */
    public ClientConnect getClientConnectLocal() {
        return this.anX;
    }

    /* renamed from: jI */
    public MessageProcessing getMessageProcessing() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jJ */
    public ProcesTransportMessage getProcessSendMessage() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public Collection<ClientConnect> getClientConnects(ClientConnect.Attitude aVar) {
        throw new UnsupportedOperationException();
    }

    public boolean isUp() {
        return this.anZ != null;
    }

    /* renamed from: c */
    public void authorizationChannelClose(ClientConnect bVar) {
        m33376e(bVar).close();
    }

    /* renamed from: a */
    public void mo13494a(C5931adH adh) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public void creatListenerChannel(Connect omVar) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public void buildListenerChannel(int listenerPort, int[] iArr, int[] iArr2, boolean z) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: af */
    public void removeChannelAndClose(int i) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public TransportThread mo19616a(ManagerMessageTransport zCVar) {
        this.anY = zCVar;
        return this;
    }

    public void dispose() {
    }

    /* renamed from: HX */
    public int getThreadInteger() {
        return 0;
    }

    /* renamed from: a */
    public void sendTransportMessageInSeparateThread(ClientConnect bVar, TransportCommand transportCommand) {
        if (bVar == null) {
            this.ioSession.write(transportCommand);
        } else {
            m33376e(bVar).getSession().write(transportCommand);
        }
    }

    /* renamed from: b */
    public void sendTransportResponse(ClientConnect bVar, TransportResponse transportResponse) {
        m33376e(bVar).getSession().write(transportResponse);
    }

    /* renamed from: b */
    public TransportResponse sendTransportMessageAndWaitForResponse(ClientConnect bVar, TransportCommand transportCommand) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: ak */
    public void setIsServer(boolean z) {
    }

    /* renamed from: HY */
    public void pendingClose() {
    }

    /* renamed from: ja */
    public long metricClientTimeAuthorization() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jf */
    public long metricReadByte() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jg */
    public long metricWriteByte() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jc */
    public long metricCountReadMessage() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jb */
    public long metricCountWriteMessage() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: je */
    public long metricByteLength() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jd */
    public long metricAllOriginalByteLength() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a.iU$a */
    enum C2680a {
        HANDSHAKING,
        UP,
        CLOSED
    }

    /* renamed from: a.iU$b */
    /* compiled from: a */
    class C2681b implements ProtocolCodecFactory {
        C3012mw atv = new C3012mw();
        C1141Ql atw = new C1141Ql();

        C2681b() {
        }

        public ProtocolDecoder getDecoder(IoSession ioSession) {
            return this.atv;
        }

        public ProtocolEncoder getEncoder(IoSession ioSession) {
            return this.atw;
        }
    }
}
