package p001a;

import logic.ui.item.ListJ;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.*;
import java.util.concurrent.Executor;

/* renamed from: a.aF */
/* compiled from: a */
public class C1797aF implements C2976mV {
    static final Log log = LogFactory.getLog(C1797aF.class);

    /* renamed from: kh */
    private Map<C5961adl, List<C3028nC>> f2778kh = new HashMap();

    /* renamed from: ki */
    private List<C2776jq> f2779ki = new LinkedList();

    /* renamed from: a */
    public void mo8720a(C3028nC nCVar, C5961adl adl) {
        List list = this.f2778kh.get(adl);
        if (list == null) {
            list = new ArrayList();
            this.f2778kh.put(adl, list);
        }
        list.add(nCVar);
    }

    /* renamed from: a */
    public void mo8721a(C3028nC nCVar, List<C5961adl> list) {
        for (C5961adl a : list) {
            mo8720a(nCVar, a);
        }
    }

    /* renamed from: b */
    public void mo8724b(C3028nC nCVar, C5961adl adl) {
        List list = this.f2778kh.get(adl);
        if (list != null) {
            list.remove(nCVar);
        }
    }

    /* renamed from: b */
    public void mo8725b(C3028nC nCVar, List<C5961adl> list) {
        for (C5961adl b : list) {
            mo8724b(nCVar, b);
        }
    }

    /* renamed from: a */
    public void mo8719a(C2776jq jqVar) {
        synchronized (this.f2779ki) {
            this.f2779ki.add(jqVar);
        }
    }

    /* renamed from: b */
    public void mo8723b(C2776jq jqVar) {
        List list = this.f2778kh.get(jqVar.mo2005Fp());
        if (list != null) {
            for (C3028nC c : new ArrayList(list)) {
                try {
                    c.mo20657c(jqVar);
                } catch (Exception e) {
                    log.error("Error handling event " + jqVar, e);
                }
            }
        }
    }

    public boolean step(float f) {
        long j;
        if (log.isTraceEnabled()) {
            log.trace("Starting event manager step");
        }
        long j2 = 0;
        ArrayList arrayList = new ArrayList();
        if (this.f2779ki.size() > 0) {
            synchronized (this.f2779ki) {
                arrayList.clear();
                arrayList.addAll(this.f2779ki);
                this.f2779ki.removeAll(arrayList);
            }
            Iterator it = arrayList.iterator();
            while (true) {
                j = j2;
                if (!it.hasNext()) {
                    break;
                }
                C2776jq jqVar = (C2776jq) it.next();
                ListJ list = this.f2778kh.get(jqVar.mo2005Fp());
                if (list != null) {
                    for (C3028nC c : new ArrayList(list)) {
                        try {
                            c.mo20657c(jqVar);
                        } catch (Exception e) {
                            log.error("Error handling event " + jqVar, e);
                        }
                    }
                }
                j2 = 1 + j;
            }
        } else {
            j = 0;
        }
        if (!logger.isTraceEnabled()) {
            return false;
        }
        logger.trace("Done event manager step - processed events: " + Long.toString(j));
        return false;
    }

    /* renamed from: a */
    public boolean mo8722a(float f, Executor executor) {
        long j;
        if (logger.isTraceEnabled()) {
            logger.trace("Starting event manager step");
        }
        long j2 = 0;
        ArrayList<C2776jq> arrayList = new ArrayList<>();
        while (this.f2779ki.size() > 0) {
            synchronized (this.f2779ki) {
                synchronized (this.f2779ki) {
                    arrayList.clear();
                    arrayList.addAll(this.f2779ki);
                    this.f2779ki.removeAll(arrayList);
                }
                j = j2;
                for (C2776jq jqVar : arrayList) {
                    ListJ list = this.f2778kh.get(jqVar.mo2005Fp());
                    if (list != null) {
                        for (C3028nC aVar : new ArrayList(list)) {
                            executor.execute(new C1798a(aVar, jqVar));
                        }
                    }
                    j++;
                }
            }
            j2 = j;
        }
        if (!logger.isTraceEnabled()) {
            return false;
        }
        logger.trace("Done event manager step - processed events: " + Long.toString(j2));
        return false;
    }

    /* renamed from: a.aF$a */
    class C1798a implements Runnable {
        private final /* synthetic */ C3028nC gCP;
        private final /* synthetic */ C2776jq gCQ;

        C1798a(C3028nC nCVar, C2776jq jqVar) {
            this.gCP = nCVar;
            this.gCQ = jqVar;
        }

        public void run() {
            try {
                this.gCP.mo20657c(this.gCQ);
            } catch (Exception e) {
                C1797aF.logger.error("Error handling event " + this.gCQ, e);
            }
        }
    }
}
