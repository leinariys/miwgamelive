package p001a;

import logic.IAddonSettings;
import logic.aaa.C6958awx;
import taikodom.addon.loading.LoadingAddon;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.ach  reason: case insensitive filesystem */
/* compiled from: a */
public class C5905ach {
    private static C5905ach gOW;
    /* access modifiers changed from: private */
    public IAddonSettings gOX;
    /* access modifiers changed from: private */
    public C6124ags<C6958awx> gOY;
    private List<C1864b> gOZ = new ArrayList();

    public C5905ach(IAddonSettings avf) {
        this.gOX = avf;
        this.gOY = new C1863a();
    }

    /* renamed from: b */
    public static C5905ach m20567b(IAddonSettings avf) {
        if (gOW == null) {
            gOW = new C5905ach(avf);
        }
        return gOW;
    }

    public void clear() {
        this.gOZ.clear();
    }

    /* access modifiers changed from: private */
    /* renamed from: he */
    public void m20569he(boolean z) {
        ArrayList<C1864b> arrayList = new ArrayList<>();
        ArrayList arrayList2 = new ArrayList();
        for (C1864b next : this.gOZ) {
            if (z || !next.bXL()) {
                arrayList.add(next);
            } else {
                arrayList2.add(next);
            }
        }
        this.gOZ.clear();
        this.gOZ = arrayList2;
        for (C1864b bXK : arrayList) {
            bXK.bXK().actionPerformed((ActionEvent) null);
        }
        arrayList.clear();
    }

    /* renamed from: a */
    public void mo12679a(ActionListener actionListener) {
        for (C1864b bXK : this.gOZ) {
            if (bXK.bXK().equals(actionListener)) {
                return;
            }
        }
        this.gOZ.add(new C1864b(actionListener, false));
        if (!((LoadingAddon) this.gOX.mo11830U(LoadingAddon.class)).isVisible()) {
            m20569he(false);
        } else if (this.gOZ.size() == 1) {
            this.gOX.aVU().mo13965a(C6958awx.class, this.gOY);
        }
    }

    /* renamed from: b */
    public void mo12680b(ActionListener actionListener) {
        for (C1864b bXK : this.gOZ) {
            if (bXK.bXK().equals(actionListener)) {
                return;
            }
        }
        this.gOZ.add(new C1864b(actionListener, true));
        if (this.gOZ.size() == 1) {
            this.gOX.aVU().mo13965a(C6958awx.class, this.gOY);
        }
    }

    public void cCb() {
        if (this.gOZ.size() > 0) {
            this.gOX.aVU().mo13964a(this.gOY);
        }
        m20569he(true);
    }

    public boolean isActive() {
        return ((LoadingAddon) this.gOX.mo11830U(LoadingAddon.class)).isVisible();
    }

    /* renamed from: a.ach$b */
    /* compiled from: a */
    private class C1864b {
        private final ActionListener fHW;
        private final boolean fHX;

        public C1864b(ActionListener actionListener, boolean z) {
            this.fHW = actionListener;
            this.fHX = z;
        }

        public ActionListener bXK() {
            return this.fHW;
        }

        public boolean bXL() {
            return this.fHX;
        }
    }

    /* renamed from: a.ach$a */
    class C1863a extends C6124ags<C6958awx> {
        C1863a() {
        }

        /* renamed from: b */
        public boolean updateInfo(C6958awx awx) {
            C5905ach.this.gOX.aVU().mo13964a(C5905ach.this.gOY);
            C5905ach.this.m20569he(true);
            return false;
        }
    }
}
