package p001a;

import game.engine.DataGameEvent;
import game.network.message.IReadProto;
import game.network.message.WriterBitsData;

/* renamed from: a.vU */
/* compiled from: a */
public interface C3828vU {
    /* renamed from: a */
    aPZ mo9363a(IReadProto ie, boolean z);

    /* renamed from: a */
    aPZ mo9364a(IReadProto ie, boolean z, int i);

    /* renamed from: a */
    aPZ mo9365a(IReadProto ie, boolean z, boolean z2);

    /* renamed from: a */
    C3380qs mo9366a(WriterBitsData oUVar, boolean z);

    /* renamed from: a */
    C3380qs mo9367a(WriterBitsData oUVar, boolean z, int i, boolean z2);

    /* renamed from: a */
    C3380qs mo9368a(WriterBitsData oUVar, boolean z, boolean z2);

    /* renamed from: a */
    void mo9369a(DataGameEvent jz);
}
