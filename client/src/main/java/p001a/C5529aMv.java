package p001a;

import logic.res.XmlNode;
import logic.thred.LogPrinter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: a.aMv  reason: case insensitive filesystem */
/* compiled from: a */
public class C5529aMv extends C6235aiz {
    public C5529aMv() {
        LogPrinter K = LogPrinter.m10275K(C5529aMv.class);
        K.info("Loading Resources...");
        XmlNode agy = null;
        try {
            LoaderFileXML axx = new LoaderFileXML();
            InputStream resourceAsStream = getClass().getResourceAsStream("resource-directory.xml");
            agy = axx.loadFileXml(resourceAsStream, "UTF-8");
            resourceAsStream.close();
        } catch (FileNotFoundException e) {
            K.error("File not found", e);
        } catch (IOException e2) {
            K.error("Error parsing file", e2);
        }
        K.info("Found " + agy.count() + " resources to load...");
        try {
            for (XmlNode next : agy.getListChildrenTag()) {
                mo13873a(next.getAttribute("name"), next.getAttribute("file"), Class.forName(next.getAttribute("class")));
            }
        } catch (ClassNotFoundException e3) {
            K.error("Error loading resource", e3);
        }
        K.info("Resources loaded");
    }
}
