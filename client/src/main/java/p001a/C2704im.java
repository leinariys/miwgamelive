package p001a;

/* renamed from: a.im */
/* compiled from: a */
public class C2704im extends UnsupportedOperationException {


    public C2704im() {
    }

    public C2704im(String str) {
        super(str);
    }

    public C2704im(Throwable th) {
        super(th);
    }

    public C2704im(String str, Throwable th) {
        super(str, th);
    }
}
