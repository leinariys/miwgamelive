package p001a;

import game.network.message.serializable.C5512aMe;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import taikodom.render.loader.provider.FilePath;

import java.io.IOException;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/* renamed from: a.TO */
/* compiled from: a */
public class C1324TO {
    static final Log log = LogPrinter.setClass(C1324TO.class);
    private static aHL ekX;

    private C1324TO() {
    }

    /* renamed from: a */
    public static Map<Long, C5512aMe> m9960a(FilePath ain, C6280ajs ajs) {
        GZIPInputStream openInputStream;
        Map<Long, C5512aMe> map = null;
        System.getProperty("base-repository", (String) null);
        System.getProperty("live-repository", (String) null);
        if (!ain.exists()) {
            ain = ain.mo2249BB().concat(ain.getName().replaceAll("[.]gz$", ""));
        }
        if (!ain.exists()) {
            log.info("Persisted data file could not be found: " + ain);
        } else {
            log.info("Reading " + ain.getName() + "...");
            if (ain.getName().toLowerCase().endsWith(".gz")) {
                openInputStream = new GZIPInputStream(ain.openInputStream());
            } else {
                openInputStream = ain.openInputStream();
            }
            try {
                ekX = new aHL(ajs, openInputStream);
                map = ekX.aBu();
                if (openInputStream != null) {
                    try {
                        openInputStream.close();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            } catch (Throwable th) {
                if (openInputStream != null) {
                    try {
                        openInputStream.close();
                    } catch (IOException e2) {
                        throw new RuntimeException(e2);
                    }
                }
                throw th;
            }
        }
        return map;
    }

    public static long aBt() {
        return ekX.aBt();
    }
}
