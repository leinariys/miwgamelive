package p001a;

import game.script.associates.Associate;

import javax.swing.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/* renamed from: a.xh */
/* compiled from: a */
public class C3980xh extends TransferHandler {
    public boolean canImport(TransferHandler.TransferSupport transferSupport) {
        if (!transferSupport.isDataFlavorSupported(DataFlavor.stringFlavor)) {
            return false;
        }
        return true;
    }

    public int getSourceActions(JComponent jComponent) {
        return 2;
    }

    /* access modifiers changed from: protected */
    public Transferable createTransferable(JComponent jComponent) {
        return new C5502aLu((Associate) ((JList) jComponent).getSelectedValue());
    }

    public boolean importData(TransferHandler.TransferSupport transferSupport) {
        if (!transferSupport.isDrop()) {
            return false;
        }
        JTextField component = transferSupport.getComponent();
        if (!(component instanceof JTextField)) {
            return false;
        }
        try {
            component.setText((String) transferSupport.getTransferable().getTransferData(DataFlavor.stringFlavor));
        } catch (UnsupportedFlavorException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void exportDone(JComponent jComponent, Transferable transferable, int i) {
    }
}
