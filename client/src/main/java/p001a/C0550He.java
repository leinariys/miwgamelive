package p001a;

import java.lang.reflect.Array;
import java.util.Collection;

/* renamed from: a.He */
/* compiled from: a */
public final class C0550He {
    public static final int daD = Integer.MAX_VALUE;

    /* renamed from: a */
    public static final Object[] m5232a(Collection collection, Class cls) {
        Object[] objArr = (Object[]) Array.newInstance(cls, collection.size());
        int i = 0;
        for (Object obj : collection) {
            objArr[i] = obj;
            i++;
        }
        return objArr;
    }

    /* renamed from: m */
    public static final String m5238m(Object[] objArr) {
        if (objArr == null) {
            return "null";
        }
        StringBuffer stringBuffer = new StringBuffer(objArr.getClass().getComponentType().getName());
        stringBuffer.append("[").append(objArr.length).append("]:{");
        if (objArr.length >= 1) {
            stringBuffer.append(objArr[0]);
            for (int i = 1; i < objArr.length; i++) {
                stringBuffer.append(", ").append(objArr[i]);
            }
        }
        stringBuffer.append("}");
        return stringBuffer.toString();
    }

    /* renamed from: b */
    public static final Object[] m5236b(Object[] objArr, Object obj) {
        Object[] objArr2 = (Object[]) Array.newInstance(obj.getClass(), objArr.length + 1);
        System.arraycopy(objArr, 0, objArr2, 0, objArr.length);
        objArr2[objArr.length] = obj;
        return objArr2;
    }

    /* renamed from: a */
    public static final <T> T[] m5231a(Class<T> cls, T[] tArr, int i) {
        T[] tArr2 = (Object[]) Array.newInstance(cls, i);
        System.arraycopy(tArr, 0, tArr2, 0, Math.min(tArr.length, i));
        return tArr2;
    }

    /* renamed from: c */
    public static final Object[] m5237c(Object[] objArr, Object obj) {
        int i = 0;
        while (i < objArr.length && !objArr[i].equals(obj)) {
            i++;
        }
        if (i >= objArr.length) {
            return objArr;
        }
        Object[] objArr2 = (Object[]) Array.newInstance(obj.getClass(), objArr.length - 1);
        System.arraycopy(objArr, 0, objArr2, 0, i);
        System.arraycopy(objArr, i + 1, objArr2, i, objArr2.length - i);
        return objArr2;
    }

    /* renamed from: a */
    public static final Object[] m5234a(Object[] objArr, Collection collection) {
        int i;
        if (collection.size() == 0) {
            return objArr;
        }
        Object[] objArr2 = (Object[]) Array.newInstance(objArr.getClass().getComponentType(), objArr.length - collection.size());
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i4 < objArr.length) {
            if (collection.contains(objArr[i4])) {
                System.arraycopy(objArr, i2, objArr2, i2 - i3, i4 - i2);
                i2 = i4 + 1;
                i = i3 + 1;
            } else {
                i = i3;
            }
            i4++;
            i3 = i;
        }
        System.arraycopy(objArr, i2, objArr2, i2 - i3, objArr.length - i2);
        return objArr2;
    }

    /* renamed from: a */
    public static <T> T[] m5233a(T[] tArr, int i, int i2) {
        return m5235b(tArr, i, i2);
    }

    /* renamed from: b */
    public static <T> T[] m5235b(T[] tArr, int i, int i2) {
        int length;
        int length2;
        int i3;
        int i4;
        if (i == Integer.MAX_VALUE) {
            length = 0;
        } else {
            length = i < 0 ? tArr.length + i : i;
        }
        if (i2 == Integer.MAX_VALUE) {
            length2 = tArr.length;
        } else {
            length2 = i2 < 0 ? tArr.length + i2 : i2;
        }
        if (length < 0) {
            i3 = 0;
        } else if (length > tArr.length) {
            i3 = tArr.length;
        } else {
            i3 = length;
        }
        if (length2 < 0) {
            length2 = 0;
        } else if (length2 > tArr.length) {
            length2 = tArr.length;
        }
        int i5 = length2 - i3;
        if (i5 < 0) {
            i4 = 0;
        } else {
            i4 = i5;
        }
        T[] tArr2 = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), i4);
        System.arraycopy(tArr, i3, tArr2, 0, i4);
        return tArr2;
    }
}
