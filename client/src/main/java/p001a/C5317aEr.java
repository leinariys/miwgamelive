package p001a;

/* renamed from: a.aEr  reason: case insensitive filesystem */
/* compiled from: a */
public class C5317aEr {
    /* renamed from: jR */
    static final /* synthetic */ boolean f2774jR = (!C5317aEr.class.desiredAssertionStatus());
    private static final int hFv = 4;
    private int[] hFw;
    private int size = 0;

    public C5317aEr() {
        resize(16);
    }

    public static int cXz() {
        return 16;
    }

    /* renamed from: i */
    public static int m14388i(long j, int i) {
        switch (i) {
            case 1:
                return ((int) ((4294901760L & j) >>> 16)) & 65535;
            case 2:
                return ((int) ((281470681743360L & j) >>> 32)) & 65535;
            default:
                return ((int) (65535 & j)) & 65535;
        }
    }

    public int cXy() {
        while (this.size + 1 >= capacity()) {
            resize(capacity() * 2);
        }
        int i = this.size;
        this.size = i + 1;
        return i;
    }

    public int size() {
        return this.size;
    }

    public int capacity() {
        return this.hFw.length / 4;
    }

    public void clear() {
        this.size = 0;
    }

    public void resize(int i) {
        int[] iArr = this.hFw;
        this.hFw = new int[(i * 4)];
        if (iArr != null) {
            System.arraycopy(iArr, 0, this.hFw, 0, Math.min(iArr.length, this.hFw.length));
        }
    }

    /* renamed from: a */
    public void mo8672a(int i, C5317aEr aer, int i2) {
        int[] iArr = this.hFw;
        int[] iArr2 = aer.hFw;
        iArr[(i * 4) + 0] = iArr2[(i2 * 4) + 0];
        iArr[(i * 4) + 1] = iArr2[(i2 * 4) + 1];
        iArr[(i * 4) + 2] = iArr2[(i2 * 4) + 2];
        iArr[(i * 4) + 3] = iArr2[(i2 * 4) + 3];
    }

    public void swap(int i, int i2) {
        int[] iArr = this.hFw;
        int i3 = iArr[(i * 4) + 0];
        int i4 = iArr[(i * 4) + 1];
        int i5 = iArr[(i * 4) + 2];
        int i6 = iArr[(i * 4) + 3];
        iArr[(i * 4) + 0] = iArr[(i2 * 4) + 0];
        iArr[(i * 4) + 1] = iArr[(i2 * 4) + 1];
        iArr[(i * 4) + 2] = iArr[(i2 * 4) + 2];
        iArr[(i * 4) + 3] = iArr[(i2 * 4) + 3];
        iArr[(i2 * 4) + 0] = i3;
        iArr[(i2 * 4) + 1] = i4;
        iArr[(i2 * 4) + 2] = i5;
        iArr[(i2 * 4) + 3] = i6;
    }

    /* renamed from: az */
    public int mo8675az(int i, int i2) {
        switch (i2) {
            case 1:
                return (this.hFw[(i * 4) + 0] >>> 16) & 65535;
            case 2:
                return this.hFw[(i * 4) + 1] & 65535;
            default:
                return this.hFw[(i * 4) + 0] & 65535;
        }
    }

    /* renamed from: xE */
    public long mo8684xE(int i) {
        return (((long) this.hFw[(i * 4) + 0]) & 4294967295L) | ((((long) this.hFw[(i * 4) + 1]) & 65535) << 32);
    }

    /* renamed from: i */
    public void mo8679i(int i, long j) {
        this.hFw[(i * 4) + 0] = (int) j;
        mo8691z(i, 2, (short) ((int) ((281470681743360L & j) >>> 32)));
    }

    /* renamed from: j */
    public void mo8680j(int i, long j) {
        mo8671A(i, 0, (short) ((int) j));
        this.hFw[(i * 4) + 2] = (int) (j >>> 16);
    }

    /* renamed from: z */
    public void mo8691z(int i, int i2, int i3) {
        switch (i2) {
            case 0:
                this.hFw[(i * 4) + 0] = (this.hFw[(i * 4) + 0] & -65536) | (i3 & 65535);
                return;
            case 1:
                this.hFw[(i * 4) + 0] = (this.hFw[(i * 4) + 0] & 65535) | ((i3 & 65535) << 16);
                return;
            case 2:
                this.hFw[(i * 4) + 1] = (this.hFw[(i * 4) + 1] & -65536) | (i3 & 65535);
                return;
            default:
                return;
        }
    }

    /* renamed from: aA */
    public int mo8673aA(int i, int i2) {
        switch (i2) {
            case 1:
                return this.hFw[(i * 4) + 2] & 65535;
            case 2:
                return (this.hFw[(i * 4) + 2] >>> 16) & 65535;
            default:
                return (this.hFw[(i * 4) + 1] >>> 16) & 65535;
        }
    }

    /* renamed from: xF */
    public long mo8685xF(int i) {
        return ((((long) this.hFw[(i * 4) + 1]) & 4294901760L) >>> 16) | ((((long) this.hFw[(i * 4) + 2]) & 4294967295L) << 16);
    }

    /* renamed from: A */
    public void mo8671A(int i, int i2, int i3) {
        switch (i2) {
            case 0:
                this.hFw[(i * 4) + 1] = (this.hFw[(i * 4) + 1] & 65535) | ((i3 & 65535) << 16);
                return;
            case 1:
                this.hFw[(i * 4) + 2] = (this.hFw[(i * 4) + 2] & -65536) | (i3 & 65535);
                return;
            case 2:
                this.hFw[(i * 4) + 2] = (this.hFw[(i * 4) + 2] & 65535) | ((i3 & 65535) << 16);
                return;
            default:
                return;
        }
    }

    /* renamed from: xG */
    public int mo8686xG(int i) {
        return this.hFw[(i * 4) + 3];
    }

    /* renamed from: aB */
    public void mo8674aB(int i, int i2) {
        this.hFw[(i * 4) + 3] = i2;
    }

    /* renamed from: xH */
    public boolean mo8687xH(int i) {
        return mo8686xG(i) >= 0;
    }

    /* renamed from: xI */
    public int mo8688xI(int i) {
        if (f2774jR || !mo8687xH(i)) {
            return -mo8686xG(i);
        }
        throw new AssertionError();
    }

    /* renamed from: xJ */
    public int mo8689xJ(int i) {
        if (f2774jR || mo8687xH(i)) {
            return mo8686xG(i) & 2097151;
        }
        throw new AssertionError();
    }

    /* renamed from: xK */
    public int mo8690xK(int i) {
        if (f2774jR || mo8687xH(i)) {
            return mo8686xG(i) >>> 21;
        }
        throw new AssertionError();
    }
}
