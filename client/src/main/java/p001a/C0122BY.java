package p001a;

import logic.swing.C0454GJ;
import logic.swing.aDX;
import logic.ui.Panel;
import logic.ui.item.C2830kk;
import logic.ui.item.Progress;
import taikodom.addon.neo.lowerbar.NeoLowerBar;

import javax.swing.*;

/* renamed from: a.BY */
/* compiled from: a */
public class C0122BY extends C0758Kq.C0759a {
    final boolean dkt;
    final NeoLowerBar dku;
    final int time;

    public C0122BY(boolean z, int i, String str, NeoLowerBar neoLowerBar) {
        super(str);
        this.dkt = z;
        this.time = i;
        this.dku = neoLowerBar;
    }

    /* renamed from: b */
    public void mo749b(Panel aco) {
        Progress cg = aco.mo4918cg("progress");
        cg.setValue(this.dkt ? 0 : 100);
        aco.setAlpha(0);
        aco.setVisible(true);
        new aDX(aco, "[0..255] dur 250", C2830kk.asS, new C0123a(cg, aco));
    }

    /* renamed from: a.BY$a */
    class C0123a implements C0454GJ {
        private final /* synthetic */ Panel dpC;
        private final /* synthetic */ Progress fOv;

        C0123a(Progress bnVar, Panel aco) {
            this.fOv = bnVar;
            this.dpC = aco;
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            String str;
            C0124a aVar = new C0124a(this.dpC);
            Progress bnVar = this.fOv;
            if (C0122BY.this.dkt) {
                str = "[0..100]";
            } else {
                str = "[100..0]";
            }
            new aDX(bnVar, String.valueOf(str) + " dur " + (C0122BY.this.time - 750), C2830kk.asU, aVar);
        }

        /* renamed from: a.BY$a$a */
        class C0124a implements C0454GJ {
            private final /* synthetic */ Panel dpC;

            C0124a(Panel aco) {
                this.dpC = aco;
            }

            /* renamed from: a */
            public void mo9a(JComponent jComponent) {
                new aDX(this.dpC, "[255..255] dur 500;[255..0] dur 250", C2830kk.asS, new C0125a());
            }

            /* renamed from: a.BY$a$a$a */
            class C0125a implements C0454GJ {
                C0125a() {
                }

                /* renamed from: a */
                public void mo9a(JComponent jComponent) {
                    C0122BY.this.dku.apW().mo3680b(C0122BY.this);
                    C0122BY.this.dku.mo24369bH(true);
                }
            }
        }
    }
}
