package p001a;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/* renamed from: a.apM  reason: case insensitive filesystem */
/* compiled from: a */
public class C6560apM {
    /* renamed from: jR */
    static final /* synthetic */ boolean f5097jR;
    private static final Comparator<aGW> gnF = new aDH();

    static {
        boolean z;
        if (!C6560apM.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        f5097jR = z;
    }

    /* access modifiers changed from: private */
    public final C1415Uk gnB = new C1415Uk();
    private final List<aGW> gnC = new ArrayList();
    private final List<ayY> gnD = new ArrayList();
    private C1960b gnE = new C1960b(this, (C1960b) null);

    /* access modifiers changed from: private */
    /* renamed from: c */
    public static int m24874c(aGW agw) {
        ayY ayy = (ayY) agw.dcy();
        return ayy.cFk() >= 0 ? ayy.cFk() : ((ayY) agw.dcz()).cFk();
    }

    /* renamed from: tA */
    public void mo15402tA(int i) {
        this.gnB.reset(i);
    }

    public C1415Uk cqn() {
        return this.gnB;
    }

    /* renamed from: a */
    public void mo15397a(C1701ZB zb, C2225d dVar) {
        dVar.aDo().mo21056SX().mo4579a(this.gnE);
    }

    /* renamed from: a */
    public void mo15400a(C2225d dVar, C1701ZB zb) {
        mo15402tA(dVar.aDu().size());
        int i = 0;
        for (int i2 = 0; i2 < dVar.aDu().size(); i2++) {
            ayY ayy = dVar.aDu().get(i2);
            ayy.mo17052wx(i);
            ayy.mo17053wy(-1);
            ayy.mo17047ld(1.0f);
            i++;
        }
        mo15397a(zb, dVar);
    }

    /* renamed from: a */
    public void mo15399a(C2225d dVar) {
        int i = 0;
        for (int i2 = 0; i2 < dVar.aDu().size(); i2++) {
            ayY ayy = dVar.aDu().get(i2);
            if (ayy.cEU()) {
                ayy.mo17052wx(this.gnB.mo5909zZ(i));
                ayy.mo17053wy(-1);
            } else {
                ayy.mo17052wx(-1);
                ayy.mo17053wy(-2);
            }
            i++;
        }
    }

    /* renamed from: a */
    public void mo15398a(C1701ZB zb, List<ayY> list, C1959a aVar) {
        int i;
        boolean z;
        boolean z2;
        C0128Bb.m1253gf("islandUnionFindAndHeapSort");
        try {
            cqn().doe();
            int dof = cqn().dof();
            int i2 = 0;
            while (i2 < dof) {
                int i3 = cqn().mo5907zX(i2).f1797id;
                int i4 = i2 + 1;
                while (i4 < dof && cqn().mo5907zX(i4).f1797id == i3) {
                    i4++;
                }
                boolean z3 = true;
                int i5 = i2;
                while (i5 < i4) {
                    ayY ayy = list.get(cqn().mo5907zX(i5).ena);
                    if (!(ayy.cFk() == i3 || ayy.cFk() == -1)) {
                        System.err.println("error in island management\n");
                    }
                    if (f5097jR || ayy.cFk() == i3 || ayy.cFk() == -1) {
                        if (ayy.cFk() == i3) {
                            if (ayy.cFa() == 1) {
                                z3 = false;
                            }
                            if (ayy.cFa() == 4) {
                                z2 = false;
                                i5++;
                                z3 = z2;
                            }
                        }
                        z2 = z3;
                        i5++;
                        z3 = z2;
                    } else {
                        throw new AssertionError();
                    }
                }
                if (z3) {
                    while (i2 < i4) {
                        ayY ayy2 = list.get(cqn().mo5907zX(i2).ena);
                        if (!(ayy2.cFk() == i3 || ayy2.cFk() == -1)) {
                            System.err.println("error in island management\n");
                        }
                        if (f5097jR || ayy2.cFk() == i3 || ayy2.cFk() == -1) {
                            if (ayy2.cFk() == i3) {
                                ayy2.mo17050wv(2);
                            }
                            i2++;
                        } else {
                            throw new AssertionError();
                        }
                    }
                    continue;
                } else {
                    int i6 = i2;
                    while (i6 < i4) {
                        ayY ayy3 = list.get(cqn().mo5907zX(i6).ena);
                        if (!(ayy3.cFk() == i3 || ayy3.cFk() == -1)) {
                            System.err.println("error in island management\n");
                        }
                        if (f5097jR || ayy3.cFk() == i3 || ayy3.cFk() == -1) {
                            if (ayy3.cFk() == i3 && ayy3.cFa() == 2) {
                                ayy3.mo17050wv(3);
                            }
                            i6++;
                        } else {
                            throw new AssertionError();
                        }
                    }
                    continue;
                }
                i2 = i4;
            }
            int bJa = zb.bJa();
            for (int i7 = 0; i7 < bJa; i7++) {
                aGW po = zb.mo595po(i7);
                ayY ayy4 = (ayY) po.dcy();
                ayY ayy5 = (ayY) po.dcz();
                if (!((ayy4 == null || ayy4.cFa() == 2) && (ayy5 == null || ayy5.cFa() == 2))) {
                    if (ayy4.cEW() && ayy4.cFa() != 2) {
                        ayy5.activate();
                    }
                    if (ayy5.cEW() && ayy5.cFa() != 2) {
                        ayy4.activate();
                    }
                    if (zb.mo591c(ayy4, ayy5)) {
                        this.gnC.add(po);
                    }
                }
            }
            int size = this.gnC.size();
            C3696tt.m39764a(this.gnC, gnF);
            int i8 = 0;
            int i9 = 1;
            int i10 = 0;
            while (i10 < dof) {
                int i11 = cqn().mo5907zX(i10).f1797id;
                boolean z4 = false;
                int i12 = i10;
                while (i12 < dof && cqn().mo5907zX(i12).f1797id == i11) {
                    ayY ayy6 = list.get(cqn().mo5907zX(i12).ena);
                    this.gnD.add(ayy6);
                    if (!ayy6.isActive()) {
                        z = true;
                    } else {
                        z = z4;
                    }
                    z4 = z;
                    i12++;
                }
                int i13 = 0;
                int i14 = -1;
                if (i8 < size && m24874c(this.gnC.get(i8)) == i11) {
                    i9 = i8 + 1;
                    while (i9 < size && i11 == m24874c(this.gnC.get(i9))) {
                        i9++;
                    }
                    i13 = i9 - i8;
                    i14 = i8;
                }
                if (!z4) {
                    aVar.mo15403a(this.gnD, this.gnD.size(), this.gnC, i14, i13, i11);
                }
                if (i13 != 0) {
                    i = i9;
                } else {
                    i = i8;
                }
                this.gnD.clear();
                i8 = i;
                i10 = i12;
            }
            this.gnC.clear();
        } finally {
            C0128Bb.blS();
        }
    }

    /* renamed from: a.apM$a */
    public interface C1959a {
        /* renamed from: a */
        void mo15403a(List<ayY> list, int i, List<aGW> list2, int i2, int i3, int i4);
    }

    /* renamed from: a.apM$b */
    /* compiled from: a */
    private class C1960b implements C1026Oy.C1027a<aUJ> {
        private C1960b() {
        }

        /* synthetic */ C1960b(C6560apM apm, C1960b bVar) {
            this();
        }

        /* renamed from: b */
        public boolean execute(aUJ auj) {
            ayY ayy = (ayY) auj.iXz.dxy;
            ayY ayy2 = (ayY) auj.iXA.dxy;
            if (ayy == null || !ayy.cEU() || ayy2 == null || !ayy2.cEU()) {
                return true;
            }
            C6560apM.this.gnB.mo5901aH(ayy.cFk(), ayy2.cFk());
            return true;
        }
    }
}
