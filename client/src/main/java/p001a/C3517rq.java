package p001a;

import java.util.HashMap;
import java.util.Map;

/* renamed from: a.rq */
/* compiled from: a */
public class C3517rq {

    public static final int UNDEFINED = -1;
    public static final int baA = 3;
    public static final int baB = 4;
    public static final int baC = 16;
    public static final int bax = 0;
    public static final int bay = 1;
    public static final int baz = 2;
    /* renamed from: Ow */
    public static Map<Integer, String> f9073Ow = new HashMap();
    /* renamed from: Ox */
    public static Map<String, Integer> f9074Ox = new HashMap();

    static {
        f9073Ow.put(-1, "");
        f9073Ow.put(0, "MOUSE_LEFT");
        f9073Ow.put(1, "MOUSE_MIDDLE");
        f9073Ow.put(2, "MOUSE_RIGHT");
        f9073Ow.put(3, "MOUSE_WHEELUP");
        f9073Ow.put(4, "MOUSE_WHEELDOWN");
        f9074Ox.put("", -1);
        f9074Ox.put("MOUSE_LEFT", 0);
        f9074Ox.put("MOUSE_MIDDLE", 1);
        f9074Ox.put("MOUSE_RIGHT", 2);
        f9074Ox.put("MOUSE_WHEELUP", 3);
        f9074Ox.put("MOUSE_WHEELDOWN", 4);
    }

    /* renamed from: ao */
    public static int m38645ao(String str) {
        Integer num = f9074Ox.get(str);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    /* renamed from: bf */
    public static String m38646bf(int i) {
        String str = f9073Ow.get(Integer.valueOf(i));
        return str != null ? str : "UNDEFINED";
    }
}
