package p001a;

import game.geometry.Vec3d;
import game.geometry.aLH;
import logic.thred.C6615aqP;

import java.util.List;

/* renamed from: a.py */
/* compiled from: a */
public interface C3286py {
    /* renamed from: VA */
    C0250DD mo1222VA();

    /* renamed from: Vy */
    C0250DD mo1223Vy();

    /* renamed from: Vz */
    C0250DD mo1224Vz();

    /* renamed from: a */
    aRR mo1225a(aLH alh, C6615aqP aqp);

    /* renamed from: a */
    void mo1226a(C1037PD pd);

    /* renamed from: a */
    void mo1227a(Vec3d ajr, float f, C6178ahu ahu);

    /* renamed from: a */
    void mo1228a(Vec3d ajr, Vec3d ajr2, C6178ahu ahu, List<C6615aqP> list);

    /* renamed from: a */
    void mo1229a(aLH alh, C6178ahu ahu);

    void dispose();

    /* renamed from: gZ */
    void mo1231gZ();
}
