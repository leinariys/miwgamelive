package p001a;

import logic.ui.item.InternalFrame;
import taikodom.addon.neo.preferences.GUIPrefAddon;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

/* renamed from: a.O */
/* compiled from: a */
public class C0962O implements ComponentListener {
    private final String key;

    public C0962O(String str) {
        this.key = str;
    }

    public void componentHidden(ComponentEvent componentEvent) {
        if (componentEvent.getComponent() instanceof InternalFrame) {
            GUIPrefAddon.m44887a(componentEvent.getComponent(), this.key);
        }
    }

    public void componentShown(ComponentEvent componentEvent) {
        if (componentEvent.getComponent() instanceof InternalFrame) {
            GUIPrefAddon.m44893b(componentEvent.getComponent(), this.key);
        }
    }

    public void componentMoved(ComponentEvent componentEvent) {
    }

    public void componentResized(ComponentEvent componentEvent) {
    }
}
