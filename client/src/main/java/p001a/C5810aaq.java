package p001a;

import java.io.OutputStream;

/* renamed from: a.aaq  reason: case insensitive filesystem */
/* compiled from: a */
public final class C5810aaq extends OutputStream {
    private final OutputStream out;
    private boolean closed;
    private long eTl;
    private long eTm = System.currentTimeMillis();
    private int eTn;
    /* renamed from: pb */
    private int f4116pb;

    public C5810aaq(OutputStream outputStream) {
        this.out = outputStream;
    }

    public float bJV() {
        long currentTimeMillis = System.currentTimeMillis();
        this.eTm = currentTimeMillis;
        float f = ((float) this.eTl) / (((float) (currentTimeMillis - this.eTm)) / 1000.0f);
        this.eTl = 0;
        return f;
    }

    public int bJW() {
        return this.f4116pb;
    }

    public int bJX() {
        return this.eTn;
    }

    public boolean isClosed() {
        return this.closed;
    }

    public void close() {
        this.out.close();
        this.eTl = 0;
        this.closed = true;
    }

    public void flush() {
        this.out.flush();
        this.eTn++;
    }

    public void write(byte[] bArr, int i, int i2) {
        this.out.write(bArr, i, i2);
        this.f4116pb += i2;
        this.eTl += (long) i2;
    }

    public void write(byte[] bArr) {
        this.out.write(bArr);
        this.f4116pb += bArr.length;
        this.eTl += (long) bArr.length;
    }

    public void write(int i) {
        this.out.write(i);
        this.f4116pb++;
        this.eTl++;
    }
}
