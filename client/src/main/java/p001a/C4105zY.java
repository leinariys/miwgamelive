package p001a;

import logic.res.html.C2491fm;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: a.zY */
/* compiled from: a */
public class C4105zY {
    private static Map<String, Method> ccW = new ConcurrentHashMap();
    private static Map<String, C2491fm> ccX = new ConcurrentHashMap();

    /* renamed from: a */
    public static C2491fm m41624a(Class<?> cls, String str, int i) {
        C2491fm fmVar = ccX.get(str);
        if (fmVar != null) {
            return fmVar;
        }
        if (i >= 0) {
            C1066Pb d = m41625d(cls, str);
            d.mo7398pq(i);
            ccX.put(str, d);
            return d;
        }
        for (Field field : cls.getDeclaredFields()) {
            if (C2491fm.class.isAssignableFrom(field.getType())) {
                field.setAccessible(true);
                try {
                    C2491fm fmVar2 = (C2491fm) field.get((Object) null);
                    if (str.equals(fmVar2.mo7396hr())) {
                        return fmVar2;
                    }
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e2) {
                    e2.printStackTrace();
                }
            }
        }
        throw new IllegalArgumentException("Method not found: " + cls + " " + str);
    }

    /* renamed from: d */
    private static C1066Pb m41625d(Class<?> cls, String str) {
        Method remove = ccW.remove(str);
        if (remove == null) {
            for (Method method : cls.getDeclaredMethods()) {
                C0064Am am = (C0064Am) method.getAnnotation(C0064Am.class);
                if (am != null) {
                    ccW.put(am.aul(), method);
                }
            }
            remove = ccW.remove(str);
        }
        if (remove != null) {
            return new C1066Pb(remove, cls, str, false);
        }
        throw new RuntimeException("field not found: " + cls + " - " + str);
    }
}
