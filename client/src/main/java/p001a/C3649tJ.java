package p001a;

import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import game.script.Character;
import game.script.player.Player;
import logic.render.IEngineGraphics;
import logic.res.FileControl;
import logic.res.sound.C0907NJ;
import logic.thred.LogPrinter;
import logic.ui.IBaseUiTegXml;
import org.mozilla1.classfile.C0147Bi;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.addon.C6144ahM;
import taikodom.render.RenderView;
import taikodom.render.SceneView;
import taikodom.render.camera.Camera;
import taikodom.render.helpers.RenderTask;
import taikodom.render.helpers.SceneHelper;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.RBillboard;
import taikodom.render.scene.RModel;
import taikodom.render.scene.SceneObject;
import taikodom.render.textures.Material;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/* renamed from: a.tJ */
/* compiled from: a */
public class C3649tJ {
    /* access modifiers changed from: private */

    public static final String gYC = "data/avatarSnapshots/";
    public static final String gYG = "data/gui/imageset/imageset_avatar/default_avatar";
    private static final String gYD = "data/scene/nod_avt_scene.pro";
    private static final String gYE = "nod_avt_scene";
    private static final int gYF = 64;
    private static final double gnJ = 0.5d;
    private static final double gnM = 0.98d;
    private static final LogPrinter log = LogPrinter.m10275K(C3649tJ.class);
    /* renamed from: P */
    public static Player f9233P = null;
    /* access modifiers changed from: private */
    /* renamed from: lV */
    public static IEngineGraphics f9234lV;
    private static Image gYH = null;

    /* renamed from: cd */
    public static void m39575cd(Player aku) {
        f9233P = aku;
    }

    /* renamed from: d */
    public static void m39576d(IEngineGraphics abd) {
        f9234lV = abd;
    }

    /* renamed from: a */
    public static String m39572a(Character acx, C3650a aVar) {
        String str = gYC + acx.bQO().aOy();
        if (!new FileControl(String.valueOf(f9234lV.getRootPath().mo2253BF().getPath()) + C0147Bi.SEPARATOR + str + ".png").exists()) {
            ((Character) C3582se.m38985a(acx, (C6144ahM<?>) new C3654c(aVar))).bQI();
        } else if (aVar != null) {
            aVar.mo2039d(str, true);
        }
        return str;
    }

    static Image cFy() {
        if (gYH == null) {
            gYH = IBaseUiTegXml.initBaseUItegXML().adz().getImage(gYG);
        }
        return gYH;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public static void m39573a(aVE ave, C3650a aVar) {
        ave.mo2027b((C0907NJ) new C3651b(ave, aVar));
    }

    /* renamed from: a.tJ$a */
    public interface C3650a {
        /* renamed from: d */
        void mo2039d(String str, boolean z);
    }

    /* renamed from: a.tJ$c */
    /* compiled from: a */
    class C3654c implements C6144ahM<aVE> {
        private final /* synthetic */ C3650a eJQ;

        C3654c(C3650a aVar) {
            this.eJQ = aVar;
        }

        /* renamed from: a */
        public void mo1931n(aVE ave) {
            C3649tJ.m39573a(ave, this.eJQ);
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }
    }

    /* renamed from: a.tJ$b */
    /* compiled from: a */
    class C3651b implements C0907NJ {
        private final /* synthetic */ aVE eJP;
        private final /* synthetic */ C3650a eJQ;

        C3651b(aVE ave, C3650a aVar) {
            this.eJP = ave;
            this.eJQ = aVar;
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if (renderAsset instanceof RModel) {
                C3649tJ.f9234lV.mo3007a((RenderTask) new C3652a(renderAsset, this.eJP, this.eJQ));
            }
        }

        /* renamed from: a.tJ$b$a */
        class C3652a implements RenderTask {
            private final /* synthetic */ aVE eJP;
            private final /* synthetic */ C3650a eJQ;
            private final /* synthetic */ RenderAsset gRc;

            C3652a(RenderAsset renderAsset, aVE ave, C3650a aVar) {
                this.gRc = renderAsset;
                this.eJP = ave;
                this.eJQ = aVar;
            }

            public void run(RenderView renderView) {
                try {
                    RModel rModel = (RModel) this.gRc;
                    C3649tJ.f9234lV.mo3049bV(C3649tJ.gYD);
                    SceneView sceneView = new SceneView();
                    sceneView.setClearColorBuffer(true);
                    sceneView.setClearDepthBuffer(true);
                    sceneView.setClearStencilBuffer(true);
                    sceneView.getClearColor().setBlue(1.0f);
                    sceneView.getScene().addChild((SceneObject) C3649tJ.f9234lV.mo3047bT(C3649tJ.gYE));
                    TransformWrap bcVar = new TransformWrap();
                    bcVar.mo17392z(180.0f);
                    rModel.setTransform(bcVar);
                    sceneView.getScene().addChild(rModel);
                    Camera camera = new Camera();
                    camera.setNearPlane(0.01f);
                    camera.setFovY(40.0f);
                    camera.setNearPlane(0.1f);
                    camera.setFarPlane(100.0f);
                    camera.setAspect(1.0f);
                    sceneView.setCamera(camera);
                    RBillboard rBillboard = new RBillboard();
                    Material material = new Material();
                    material.setShader(C3649tJ.f9234lV.aej().getDefaultAlphaTestShader());
                    rBillboard.setMaterial(material);
                    rBillboard.setPrimitiveColor(0.0f, 0.0f, 0.0f, 1.0f);
                    sceneView.getScene().addChild(rBillboard);
                    rBillboard.setSize(1000.0f, 1000.0f);
                    rBillboard.setPosition(0.0f, 0.0f, -10.0f);
                    float[] fArr = {-25.0f, -15.0f, -5.0f, 5.0f, 15.0f, 25.0f};
                    camera.getTransform().mo17392z(fArr[(int) Math.floor(Math.random() * ((double) fArr.length))]);
                    Vec3d ajr = new Vec3d(ScriptRuntime.NaN, ((double) this.eJP.getHeight()) * C3649tJ.gnM, ((double) this.eJP.aOD()) * C3649tJ.gnJ);
                    camera.getTransform().mo17331a(ajr);
                    camera.setPosition(ajr);
                    renderView.render(sceneView, ScriptRuntime.NaN, 0.0f);
                    C3649tJ.f9234lV.mo3007a((RenderTask) new C3653a(sceneView, this.eJP, this.eJQ));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            /* renamed from: a.tJ$b$a$a */
            class C3653a implements RenderTask {
                private final /* synthetic */ aVE eJP;
                private final /* synthetic */ C3650a eJQ;
                private final /* synthetic */ SceneView ghb;

                C3653a(SceneView sceneView, aVE ave, C3650a aVar) {
                    this.ghb = sceneView;
                    this.eJP = ave;
                    this.eJQ = aVar;
                }

                public void run(RenderView renderView) {
                    try {
                        SceneHelper.saveScreenShot(this.ghb, renderView, String.valueOf(C3649tJ.f9234lV.getRootPath().mo2253BF().getPath()) + C0147Bi.SEPARATOR + C3649tJ.gYC, this.eJP.aOy(), "png", 64, 64, true);
                        String str = C3649tJ.gYC + this.eJP.aOy();
                        this.eJP.mo2031d(new ImageIcon(IBaseUiTegXml.initBaseUItegXML().adz().getImage(str)));
                        if (this.eJQ != null) {
                            this.eJQ.mo2039d(String.valueOf(str) + ".png", false);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    this.ghb.getScene().removeAllChildren();
                    if (C3649tJ.f9233P == null || !C3649tJ.f9233P.equals(this.eJP.agj())) {
                        this.eJP.mo2028b((C1009Os.C1019i) null);
                    }
                }
            }
        }
    }
}
