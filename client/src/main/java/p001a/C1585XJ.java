package p001a;

import java.util.HashMap;
import java.util.Map;

/* renamed from: a.XJ */
/* compiled from: a */
public class C1585XJ {
    private String[] dyW;
    private Map<String, String> map = null;

    public C1585XJ(String[] strArr) {
        this.dyW = strArr;
    }

    /* renamed from: f */
    private static Map<String, String> m11462f(String[] strArr) {
        String str;
        HashMap hashMap = new HashMap();
        int length = strArr.length;
        int i = 0;
        String str2 = null;
        while (i < length) {
            String str3 = strArr[i];
            if (str3.startsWith("-")) {
                str = str3.substring(1);
                hashMap.put(str, (Object) null);
            } else {
                hashMap.put(str2, str3);
                str = null;
            }
            i++;
            str2 = str;
        }
        return hashMap;
    }

    public String get(String str) {
        return getMap().get(str);
    }

    public boolean contains(String str) {
        return getMap().containsKey(str);
    }

    public String bgC() {
        return get((String) null);
    }

    public Map<String, String> getMap() {
        if (this.map == null) {
            this.map = m11462f(this.dyW);
        }
        return this.map;
    }
}
