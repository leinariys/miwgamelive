package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.Pg */
/* compiled from: a */
public abstract class C1072Pg {
    public final C0763Kt stack = C0763Kt.bcE();

    /* renamed from: a */
    public abstract void mo4807a(Vec3f vec3f, Vec3f vec3f2, float f, int i, Vec3f vec3f3);

    public abstract int bnd();

    /* renamed from: c */
    public abstract void mo4809c(Vec3f vec3f, String str);

    /* renamed from: gj */
    public abstract void mo4810gj(String str);

    /* renamed from: i */
    public abstract void mo4811i(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3);

    /* renamed from: mt */
    public abstract void mo4813mt(int i);

    /* renamed from: j */
    public void mo4812j(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        this.stack.bcH().push();
        try {
            Vec3f ac = this.stack.bcH().mo4458ac(vec3f2);
            ac.sub(vec3f);
            ac.scale(0.5f);
            Vec3f ac2 = this.stack.bcH().mo4458ac(vec3f2);
            ac2.add(vec3f);
            ac2.scale(0.5f);
            Vec3f h = this.stack.bcH().mo4460h(1.0f, 1.0f, 1.0f);
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            for (int i = 0; i < 4; i++) {
                for (int i2 = 0; i2 < 3; i2++) {
                    vec3f4.set(h.x * ac.x, h.y * ac.y, h.z * ac.z);
                    vec3f4.add(ac2);
                    C0647JL.m5597b(h, i2 % 3, -1.0f);
                    vec3f5.set(h.x * ac.x, h.y * ac.y, h.z * ac.z);
                    vec3f5.add(ac2);
                    mo4811i(vec3f4, vec3f5, vec3f3);
                }
                h.set(-1.0f, -1.0f, -1.0f);
                if (i < 3) {
                    C0647JL.m5597b(h, i, -1.0f);
                }
            }
        } finally {
            this.stack.bcH().pop();
        }
    }
}
