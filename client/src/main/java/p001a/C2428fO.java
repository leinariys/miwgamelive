package p001a;

import game.script.Character;
import logic.baa.C1616Xf;
import logic.data.mbean.C6064afk;
import logic.res.XmlNode;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.thred.LogPrinter;

import java.io.*;
import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: a.fO */
/* compiled from: a */
public final class C2428fO {
    static LogPrinter logger = LogPrinter.m10275K(C2428fO.class);
    /* access modifiers changed from: private */
    public Set<Class<?>> cVt = new HashSet();
    /* access modifiers changed from: private */
    public aIX cVu = new aIX();
    ByteArrayOutputStream cVr = new ByteArrayOutputStream();
    ObjectOutputStream cVs = null;

    public C2428fO(OutputStream outputStream, C6285ajx ajx, long j) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            printWriter.append("<root structureVersion=\"" + 1 + "\" version=\"").append(new StringBuilder().append(j).toString()).append("\">\n");
            printWriter.append("<scriptClasses>\n");
            ajx.mo14225a((C6449anF) new C2429a(printWriter));
            printWriter.append("</scriptClasses>\n");
            ajx.mo14226a((C6568apU) new C2430b(printWriter));
            printWriter.append("</root>\n");
            printWriter.flush();
            if (!this.cVt.isEmpty()) {
                logger.info("Classes using default java serializer:");
                for (Class<?> canonicalName : this.cVt) {
                    logger.info("  " + canonicalName.getCanonicalName());
                }
            }
            this.cVu.dfu();
            logger.info("Xml serialization took: " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: a */
    public static void m31025a(C1616Xf xf, C2042bT bTVar) {
        C5663aRz[] c = xf.mo25c();
        C6064afk cVj = ((C3582se) xf.bFf()).cVj();
        for (C5663aRz arz : c) {
            if (arz.mo11300Ew() != C1020Ot.C1021a.NOT_PERSISTED) {
                bTVar.mo17212b(arz, cVj.mo3216r(arz));
            }
        }
    }

    /* renamed from: a */
    public static C5663aRz m31024a(C1616Xf xf, String str) {
        C5663aRz arz;
        C5663aRz[] c = xf.mo25c();
        int length = c.length;
        int i = 0;
        while (true) {
            if (i < length) {
                arz = c[i];
                if (arz.name().equals(str)) {
                    break;
                }
                i++;
            } else {
                arz = null;
                break;
            }
        }
        if (arz != null) {
            return arz;
        }
        throw new RuntimeException("Field " + str + " not found");
    }

    /* access modifiers changed from: private */
    /* renamed from: w */
    public boolean m31028w(Class<?> cls) {
        return cls.isPrimitive() || cls == Integer.class || cls == Long.class || cls == Short.class || cls == Float.class || cls == Double.class || cls == Byte.class || cls == Character.class || cls == Boolean.class;
    }

    /* renamed from: a.fO$a */
    class C2429a implements C6449anF {

        /* renamed from: Og */
        private final /* synthetic */ PrintWriter f7349Og;

        C2429a(PrintWriter printWriter) {
            this.f7349Og = printWriter;
        }

        /* renamed from: a */
        public void mo14924a(Class<?> cls, C6494any any) {
            XmlNode agy = new XmlNode("scriptClass");
            agy.addAttribute("class", cls.getName());
            agy.addAttribute("version", any.getVersion());
            for (C5663aRz arz : any.mo15889c()) {
                if (arz == null) {
                    C2428fO.logger.warn("Field null at " + any.getName());
                } else {
                    XmlNode agy2 = new XmlNode("field");
                    agy.addChildrenTag(agy2);
                    agy2.addAttribute("name", arz.name());
                    agy2.addAttribute("type", arz.mo11291El().getName());
                    if (arz.mo11295Eq() != null && arz.mo11295Eq().length > 0) {
                        StringBuilder sb = new StringBuilder();
                        for (Class cls2 : arz.mo11295Eq()) {
                            if (sb.length() > 0) {
                                sb.append(",");
                            }
                            sb.append(cls2.getName());
                        }
                        agy2.addAttribute("componentTypes", sb.toString());
                    }
                }
            }
            agy.buildStringFromNode(this.f7349Og);
        }
    }

    /* renamed from: a.fO$b */
    /* compiled from: a */
    class C2430b implements C6568apU {

        /* renamed from: Og */
        private final /* synthetic */ PrintWriter f7351Og;

        C2430b(PrintWriter printWriter) {
            this.f7351Og = printWriter;
        }

        /* renamed from: a */
        public C2834kn mo15427a(C1616Xf xf) {
            XmlNode agy = new XmlNode("scriptObject");
            agy.addAttribute("id", new StringBuilder().append(xf.bFf().getObjectId().getId()).toString());
            agy.addAttribute("class", xf.getClass().getName());
            return new C2431a(agy, xf, this.f7351Og);
        }

        /* renamed from: a.fO$b$a */
        class C2431a implements C2834kn {

            /* renamed from: Og */
            private final /* synthetic */ PrintWriter f7352Og;
            private final /* synthetic */ XmlNode ggY;
            private final /* synthetic */ C1616Xf ggZ;

            C2431a(XmlNode agy, C1616Xf xf, PrintWriter printWriter) {
                this.ggY = agy;
                this.ggZ = xf;
                this.f7352Og = printWriter;
            }

            /* renamed from: c */
            public void mo18638c(C5663aRz arz, Object obj) {
                if (arz.mo11300Ew() != C1020Ot.C1021a.NOT_PERSISTED) {
                    new C2432c(C2428fO.this, this.ggY, this.ggZ, (C2432c) null).mo17212b(arz, obj);
                }
            }

            public void visitEnd() {
                this.ggY.buildStringFromNode(this.f7352Og);
            }
        }
    }

    /* renamed from: a.fO$c */
    /* compiled from: a */
    private final class C2432c implements C2042bT {
        private final XmlNode gzB;

        /* synthetic */ C2432c(C2428fO fOVar, XmlNode agy, C1616Xf xf, C2432c cVar) {
            this(agy, xf);
        }

        private C2432c(XmlNode agy, C1616Xf xf) {
            this.gzB = agy;
        }

        public final void writeFloat(float f) {
            throw new UnsupportedOperationException();
        }

        /* renamed from: b */
        public final void mo17212b(C5663aRz arz, Object obj) {
            if (obj != null) {
                if (obj instanceof Collection) {
                    if (((Collection) obj).isEmpty()) {
                        return;
                    }
                } else if ((obj instanceof Map) && ((Map) obj).isEmpty()) {
                    return;
                }
                XmlNode mz = this.gzB.addChildrenTag(arz.name());
                if (!arz.mo11291El().isPrimitive() && arz.mo11291El() != obj.getClass() && !arz.isCollection() && !arz.mo11292En()) {
                    mz.addAttribute("class", obj.getClass().getName());
                }
                m31033a(mz, arz, obj);
            }
        }

        /* renamed from: a */
        private final void m31033a(XmlNode agy, C5663aRz arz, Object obj) {
            if (obj instanceof C1616Xf) {
                C1616Xf xf = (C1616Xf) obj;
                if (!arz.isCollection() || arz.mo11295Eq()[0] != xf.getClass()) {
                    agy.addAttribute("class", xf.getClass().getName());
                }
                agy.addAttribute("scriptObject", new StringBuilder().append(xf.bFf().getObjectId().getId()).toString());
            } else if (obj instanceof Collection) {
                Collection collection = (Collection) obj;
                if (!collection.isEmpty()) {
                    for (Object next : collection) {
                        XmlNode mz = agy.addChildrenTag("item");
                        if (next == null) {
                            mz.addAttribute("null", "yes");
                        } else {
                            m31033a(mz, arz, next);
                        }
                    }
                }
            } else if (obj instanceof Map) {
                Map map = (Map) obj;
                if (map.isEmpty()) {
                    return;
                }
                if (arz.mo11295Eq().length != 2) {
                    throw new RuntimeException("You must specify the component types for the Map (example: DataMap<Integer,String> instead of just DataMap)");
                }
                for (Map.Entry entry : map.entrySet()) {
                    XmlNode mz2 = agy.addChildrenTag("item");
                    m31033a(mz2.addChildrenTag("key"), arz, entry.getKey());
                    m31033a(mz2.addChildrenTag("value"), arz, entry.getValue());
                }
            } else if (obj == null) {
                agy.addAttribute("null", "yes");
            } else {
                Class<?> cls = obj.getClass();
                if (C2428fO.this.m31028w(cls)) {
                    agy.setText(new StringBuilder().append(obj).toString());
                } else if (m31035aw(cls)) {
                    agy.addAttribute("xObject", "true");
                    C2428fO.this.cVu.mo9361g(obj, agy.getTagName());
                    agy.addAttribute("xId", C2428fO.this.cVu.mo9358aI(obj));
                    for (XmlNode k : C2428fO.this.cVu.dft()) {
                        agy.addChildrenTag(k);
                    }
                } else if (cls.isEnum()) {
                    agy.addAttribute("enum", "yes");
                    agy.addAttribute("class", cls.getName());
                    agy.addAttribute("member", ((Enum) obj).name());
                } else if (cls == Class.class) {
                    agy.addAttribute("isClass", "yes");
                    agy.addAttribute("class", ((Class) obj).getName());
                } else {
                    agy.addAttribute("blob", "yes");
                    agy.setText(m31034as(obj));
                }
            }
        }

        /* renamed from: aw */
        private boolean m31035aw(Class<?> cls) {
            try {
                for (Constructor parameterTypes : cls.getConstructors()) {
                    if (parameterTypes.getParameterTypes().length == 0) {
                        return true;
                    }
                }
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        /* renamed from: as */
        private final String m31034as(Object obj) {
            C2428fO.this.cVt.add(obj.getClass());
            try {
                C2428fO.this.cVs = new ObjectOutputStream(C2428fO.this.cVr);
                C2428fO.this.cVs.writeObject(obj);
                C2428fO.this.cVs.flush();
                String i = m31036i(C2428fO.this.cVr.toByteArray());
                C2428fO.this.cVs.reset();
                C2428fO.this.cVr.reset();
                return i;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        /* renamed from: i */
        private final String m31036i(byte[] bArr) {
            StringBuilder sb = new StringBuilder(bArr.length * 2);
            for (byte b : bArr) {
                if ((b & 255) < 16) {
                    sb.append("0").append(Integer.toHexString(b & 255));
                } else {
                    sb.append(Integer.toHexString(b & 255));
                }
            }
            return sb.toString();
        }
    }
}
