package p001a;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.aOk  reason: case insensitive filesystem */
/* compiled from: a */
public class C5570aOk implements C1450VL {
    public C0285Dg iyC;

    public C5570aOk(C0285Dg dg) {
        this.iyC = dg;
    }

    /* renamed from: a */
    public Object mo6056a(Vec3f vec3f, Vec3f vec3f2, C0403Fc fc) {
        C6238ajC c;
        C2225d.C2228c cVar = new C2225d.C2228c(vec3f, vec3f2);
        this.iyC.mo17709a(vec3f, vec3f2, (C2225d.C2227b) cVar);
        if (!cVar.mo17722gp() || (c = C6238ajC.m22755c(cVar.f6429o)) == null) {
            return null;
        }
        fc.cUK.set(cVar.aku);
        fc.cUL.set(cVar.akt);
        fc.cUL.normalize();
        fc.cUM = cVar.f6428nL;
        return c;
    }
}
