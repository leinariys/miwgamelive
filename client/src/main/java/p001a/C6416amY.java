package p001a;

import gnu.trove.THashMap;
import logic.ui.item.Picture;
import org.mozilla1.classfile.C0147Bi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: a.amY  reason: case insensitive filesystem */
/* compiled from: a */
public class C6416amY extends ClassAdapter implements Opcodes {
    final C6380alo gci;
    public String signature;
    String desc;
    String name;
    String superName;
    private Type cUV;
    private boolean cUX;
    private Map<String, C1811aM> cUY = new THashMap();
    private boolean frF;
    private Map<String, C0404Fd> gce = new THashMap();
    private List<C0404Fd> gcf = new ArrayList();
    private Map<String, C1554Wn> gcg = new THashMap();
    private List<C1554Wn> gch = new ArrayList();
    private boolean gcj;
    private String gck;
    private C6915awD gcl = C6915awD.UNCERTAIN;
    private String gcm;
    private ArrayList<C1554Wn> gcn;
    private String gco;
    private String gcp;
    private String gcq;
    private boolean gcr;
    private boolean gcs;
    private String gct;
    private int version;

    public C6416amY(ClassVisitor classVisitor, C6380alo alo) {
        super(classVisitor);
        this.gci = alo;
    }

    public ClassVisitor ckg() {
        return this.cv;
    }

    public void visit(int i, int i2, String str, String str2, String str3, String[] strArr) {
        String[] strArr2;
        this.version = i;
        this.name = str;
        int lastIndexOf = str.lastIndexOf(47) + 1;
        this.gcp = str.substring(lastIndexOf);
        this.gcq = lastIndexOf > 0 ? str.substring(0, lastIndexOf) : "";
        this.desc = "L" + str + ";";
        this.signature = str2;
        this.gct = String.valueOf(str) + "_Data";
        this.frF = C2821kb.aqo.getInternalName().equals(str3);
        if (isTopLevel()) {
            str3 = C2821kb.aqC.getInternalName();
        }
        this.superName = str3;
        this.cUV = Type.getType("L" + str + ";");
        String[] strArr3 = null;
        if (strArr != null) {
            int i3 = 0;
            while (true) {
                if (i3 >= strArr.length) {
                    break;
                } else if (C2821kb.aqE.getInternalName().equals(strArr[i3])) {
                    strArr2 = strArr;
                    break;
                } else {
                    i3++;
                }
            }
        }
        strArr2 = strArr3;
        if (strArr2 == null) {
            strArr2 = new String[(strArr.length + 1)];
            System.arraycopy(strArr, 0, strArr2, 0, strArr.length);
            strArr2[strArr.length] = C2821kb.aqE.getInternalName();
        }
        C6416amY.super.visit(i, i2, str, str2, this.superName, strArr2);
        C6416amY.super.visitAnnotation(C2821kb.aqt.getDescriptor(), true).visitEnd();
    }

    public AnnotationVisitor visitAnnotation(String str, boolean z) {
        C1811aM aMVar = new C1811aM(str);
        C0925NZ nz = new C0925NZ(C6416amY.super.visitAnnotation(str, z), aMVar);
        if (C2821kb.aqy.getDescriptor().equals(str)) {
            this.cUX = true;
        }
        this.cUY.put(str, aMVar);
        return nz;
    }

    public FieldVisitor visitField(int i, String str, String str2, String str3, Object obj) {
        if (!aBz()) {
            return C6416amY.super.visitField(i, str, str2, str3, obj);
        }
        if ("serialVersionUID".equals(str) && "J".equals(str2) && (i & 8) != 0) {
            this.gcs = true;
        }
        return new C0404Fd(this, i, str, str2, str3, obj);
    }

    private boolean aBz() {
        if (this.gcl == C6915awD.UNCERTAIN) {
            if (this.cUY.containsKey(C2821kb.aqx.getDescriptor())) {
                this.gcl = C6915awD.TRUE;
            } else {
                throw C5637aQz.iEJ;
            }
        }
        return this.gcl == C6915awD.TRUE;
    }

    public MethodVisitor visitMethod(int i, String str, String str2, String str3, String[] strArr) {
        if (!aBz()) {
            return C6416amY.super.visitMethod(i, str, str2, str3, strArr);
        }
        if (C2821kb.arS.equals(str)) {
            this.gcj = true;
        }
        if (C2821kb.arR.equals(str)) {
            if ("()V".equals(str2)) {
                this.gcr = true;
            }
            return new C5958adi(this, i, str, str2, str3, strArr);
        } else if ((i & 1280) == 0 || (!str2.equals(C2821kb.ash) && !str2.equals(C2821kb.asg))) {
            return new C1554Wn(this, i, str, str2, str3, strArr);
        } else {
            return new C5464aKi(this, i, str, str2, str3, strArr);
        }
    }

    public void visitEnd() {
        if (!aBz()) {
            C6416amY.super.visitEnd();
            return;
        }
        if (!this.gcr) {
            MethodVisitor visitMethod = visitMethod(1, C2821kb.arR, "()V", (String) null, (String[]) null);
            visitMethod.visitCode();
            visitMethod.visitVarInsn(25, 0);
            visitMethod.visitMethodInsn(183, this.superName, C2821kb.arR, "()V");
            visitMethod.visitInsn(177);
            visitMethod.visitMaxs(1, 1);
            visitMethod.visitEnd();
        }
        ckl();
        ckj();
        ckk();
        cki();
        ckh();
        C6416amY.super.visitEnd();
    }

    private void ckh() {
        String str;
        ClassWriter classWriter = new ClassWriter(1);
        String str2 = String.valueOf(this.name) + "_Data";
        if (isTopLevel()) {
            str = C2821kb.aqG.getInternalName();
        } else {
            str = String.valueOf(this.superName) + "_Data";
        }
        classWriter.visit(this.version, 1, str2, (String) null, str, (String[]) null);
        for (C0404Fd next : this.gcf) {
            AnnotationVisitor visitAnnotation = classWriter.visitField(1, next.name, next.desc, next.signature, next.value).visitAnnotation(C2821kb.aqu.getDescriptor(), true);
            visitAnnotation.visit("hash", next.cUW);
            visitAnnotation.visit("id", Integer.valueOf(next.index));
            visitAnnotation.visitEnd();
        }
        for (C0404Fd next2 : this.gcf) {
            AnnotationVisitor visitAnnotation2 = classWriter.visitField(1, C2821kb.MICRO + next2.name + "_version", "J", (String) null, 0).visitAnnotation(C2821kb.aqu.getDescriptor(), true);
            visitAnnotation2.visit("hash", next2.cUW);
            visitAnnotation2.visit("id", -2);
            visitAnnotation2.visitEnd();
        }
        for (C0404Fd next3 : this.gcf) {
            AnnotationVisitor visitAnnotation3 = classWriter.visitField(1, C2821kb.MICRO + next3.name + "_flags", "B", (String) null, 0).visitAnnotation(C2821kb.aqu.getDescriptor(), true);
            visitAnnotation3.visit("hash", next3.cUW);
            visitAnnotation3.visit("id", -1);
            visitAnnotation3.visitEnd();
        }
        MethodVisitor visitMethod = classWriter.visitMethod(1, C2821kb.arR, "()V", (String) null, (String[]) null);
        visitMethod.visitCode();
        visitMethod.visitVarInsn(25, 0);
        visitMethod.visitMethodInsn(183, str, C2821kb.arR, "()V");
        visitMethod.visitInsn(177);
        visitMethod.visitMaxs(2, 2);
        visitMethod.visitEnd();
        MethodVisitor visitMethod2 = classWriter.visitMethod(1, C2821kb.arR, C2821kb.aqH.getDescriptor(), (String) null, (String[]) null);
        visitMethod2.visitCode();
        visitMethod2.visitVarInsn(25, 0);
        visitMethod2.visitVarInsn(25, 1);
        visitMethod2.visitMethodInsn(183, str, C2821kb.arR, C2821kb.aqH.getDescriptor());
        visitMethod2.visitInsn(177);
        visitMethod2.visitMaxs(3, 3);
        visitMethod2.visitEnd();
        MethodVisitor visitMethod3 = classWriter.visitMethod(1, C2821kb.arN.getName(), C2821kb.arN.getDescriptor(), (String) null, (String[]) null);
        visitMethod3.visitCode();
        visitMethod3.visitFieldInsn(178, this.name, C2821kb.asf, C2821kb.aqJ.getDescriptor());
        visitMethod3.visitInsn(176);
        visitMethod3.visitMaxs(2, 2);
        visitMethod3.visitEnd();
        MethodVisitor visitMethod4 = classWriter.visitMethod(1, "doReset", "()V", (String) null, (String[]) null);
        visitMethod4.visitCode();
        if (!this.frF) {
            visitMethod4.visitVarInsn(25, 0);
            visitMethod4.visitMethodInsn(183, str, "doReset", "()V");
        }
        for (C0404Fd next4 : this.gcf) {
            visitMethod4.visitVarInsn(25, 0);
            switch (next4.cUV.getSort()) {
                case 1:
                    visitMethod4.visitInsn(3);
                    break;
                case 2:
                    visitMethod4.visitInsn(3);
                    visitMethod4.visitInsn(146);
                    break;
                case 3:
                    visitMethod4.visitInsn(3);
                    visitMethod4.visitInsn(145);
                    break;
                case 4:
                    visitMethod4.visitInsn(3);
                    visitMethod4.visitInsn(147);
                    break;
                case 5:
                    visitMethod4.visitInsn(3);
                    break;
                case 6:
                    visitMethod4.visitInsn(11);
                    break;
                case 7:
                    visitMethod4.visitInsn(9);
                    break;
                case 8:
                    visitMethod4.visitInsn(14);
                    break;
                case 9:
                case 10:
                    visitMethod4.visitInsn(1);
                    break;
                default:
                    throw new IllegalArgumentException(next4.cUV.toString());
            }
            visitMethod4.visitFieldInsn(181, str2, next4.name, next4.desc);
        }
        for (C0404Fd fd : this.gcf) {
            visitMethod4.visitVarInsn(25, 0);
            visitMethod4.visitInsn(9);
            visitMethod4.visitFieldInsn(181, str2, C2821kb.MICRO + fd.name + "_version", "J");
        }
        for (C0404Fd fd2 : this.gcf) {
            visitMethod4.visitVarInsn(25, 0);
            visitMethod4.visitInsn(3);
            visitMethod4.visitFieldInsn(181, str2, C2821kb.MICRO + fd2.name + "_flags", "B");
        }
        visitMethod4.visitInsn(177);
        visitMethod4.visitMaxs(4, 4);
        visitMethod4.visitEnd();
        byte[] byteArray = classWriter.toByteArray();
        this.gci.mo4569b(str2.replace(C0147Bi.cla, '.'), byteArray, 0, byteArray.length);
    }

    private void cki() {
        MethodVisitor visitMethod = ckg().visitMethod(1, C2821kb.aqV.getName(), C2821kb.aqV.getDescriptor(), (String) null, (String[]) null);
        visitMethod.visitCode();
        visitMethod.visitTypeInsn(187, this.gct);
        visitMethod.visitInsn(89);
        visitMethod.visitVarInsn(25, 0);
        visitMethod.visitMethodInsn(183, this.gct, C2821kb.arR, C2821kb.aqH.getDescriptor());
        visitMethod.visitInsn(176);
        visitMethod.visitMaxs(4, 4);
        visitMethod.visitEnd();
    }

    private void ckj() {
        int i;
        MethodVisitor visitMethod = ckg().visitMethod(1, C2821kb.aqX.getName(), C2821kb.aqX.getDescriptor(), (String) null, (String[]) null);
        Picture label = new Picture();
        int size = this.gch.size();
        Picture[] labelArr = new Picture[size];
        int i2 = size;
        while (true) {
            i2--;
            if (i2 < 0) {
                break;
            }
            labelArr[i2] = new Picture();
        }
        visitMethod.visitCode();
        int i3 = 4;
        if (size > 0) {
            visitMethod.visitVarInsn(25, 1);
            visitMethod.visitInsn(89);
            C2821kb.arA.mo20041a(visitMethod, 185);
            visitMethod.visitVarInsn(58, 2);
            C2821kb.arB.mo20041a(visitMethod, 185);
            if (!isTopLevel()) {
                visitMethod.visitFieldInsn(178, this.superName, C2821kb.arU, "I");
                visitMethod.visitInsn(100);
            }
            visitMethod.visitTableSwitchInsn(0, size - 1, label, labelArr);
            for (int i4 = 0; i4 < size; i4++) {
                visitMethod.visitLabel(labelArr[i4]);
                C1554Wn wn = this.gch.get(i4);
                Type type = wn.gVW;
                C1551Wl a = C1551Wl.m11312a(type);
                if (a != null) {
                    visitMethod.visitTypeInsn(187, a.type);
                    visitMethod.visitInsn(89);
                }
                visitMethod.visitVarInsn(25, 0);
                i3 = Math.max(i3, wn.mo6627b(visitMethod, 2) + 4);
                visitMethod.visitMethodInsn(183, this.name, wn._name, wn.desc);
                if (a != null) {
                    visitMethod.visitMethodInsn(183, a.type, C2821kb.arR, a.exr);
                } else if (type.getSort() == 0) {
                    visitMethod.visitInsn(1);
                }
                visitMethod.visitInsn(176);
            }
            visitMethod.visitLabel(label);
            i = i3;
        } else {
            i = 2;
        }
        visitMethod.visitVarInsn(25, 0);
        visitMethod.visitVarInsn(25, 1);
        visitMethod.visitMethodInsn(183, this.superName, C2821kb.aqX.getName(), C2821kb.aqX.getDescriptor());
        visitMethod.visitInsn(176);
        visitMethod.visitMaxs(i, 6);
        visitMethod.visitEnd();
    }

    private void ckk() {
        MethodVisitor visitMethod = ckg().visitMethod(1, C2821kb.aqY.getName(), C2821kb.aqY.getDescriptor(), (String) null, (String[]) null);
        visitMethod.visitCode();
        if (this.gcn != null) {
            Picture label = new Picture();
            int i = Integer.MAX_VALUE;
            int i2 = Integer.MIN_VALUE;
            int size = this.gcn.size();
            int i3 = 0;
            while (i3 < size) {
                int i4 = this.gcn.get(i3).index;
                if (i > i4) {
                    i = i4;
                }
                if (i2 >= i4) {
                    i4 = i2;
                }
                i3++;
                i2 = i4;
            }
            Picture[] labelArr = new Picture[((i2 - i) + 1)];
            int i5 = 0;
            int i6 = i;
            while (i6 <= i2) {
                if (this.gch.get(i6).fQP != null) {
                    labelArr[i5] = new Picture();
                } else {
                    labelArr[i5] = label;
                }
                i6++;
                i5++;
            }
            visitMethod.visitVarInsn(25, 2);
            C2821kb.arB.mo20041a(visitMethod, 185);
            if (!isTopLevel()) {
                visitMethod.visitFieldInsn(178, this.superName, C2821kb.arU, "I");
                visitMethod.visitInsn(100);
            }
            visitMethod.visitTableSwitchInsn(i, i2, label, labelArr);
            int i7 = 0;
            while (i <= i2) {
                C1554Wn wn = this.gch.get(i);
                if (wn.fQP != null) {
                    visitMethod.visitLabel(labelArr[i7]);
                    visitMethod.visitVarInsn(25, 0);
                    visitMethod.visitVarInsn(25, 1);
                    visitMethod.visitVarInsn(25, 2);
                    visitMethod.visitMethodInsn(183, this.name, wn.fQP, C2821kb.aqY.getDescriptor());
                    visitMethod.visitInsn(177);
                }
                i++;
                i7++;
            }
            visitMethod.visitLabel(label);
        }
        if (!isTopLevel()) {
            visitMethod.visitVarInsn(25, 0);
            visitMethod.visitVarInsn(25, 1);
            visitMethod.visitVarInsn(25, 2);
            visitMethod.visitMethodInsn(183, this.superName, C2821kb.aqY.getName(), C2821kb.aqY.getDescriptor());
            visitMethod.visitInsn(177);
        } else {
            visitMethod.visitTypeInsn(187, C2821kb.arJ.getInternalName());
            visitMethod.visitInsn(89);
            visitMethod.visitVarInsn(25, 2);
            C2821kb.arK.mo5379a(visitMethod);
            visitMethod.visitInsn(191);
        }
        visitMethod.visitMaxs(6, 3);
        visitMethod.visitEnd();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public void mo14851e(MethodVisitor methodVisitor) {
        methodVisitor.visitVarInsn(25, 0);
        methodVisitor.visitTypeInsn(192, C2821kb.aqE.getInternalName());
        C2821kb.aqU.mo20041a(methodVisitor, 185);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo14842a(C0404Fd fd) {
        fd.index = this.gcf.size();
        this.gce.put(fd.name, fd);
        this.gcf.add(fd);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo14843a(C1554Wn wn) {
        wn.index = this.gch.size();
        this.gcg.put(this.name, wn);
        this.gch.add(wn);
    }

    /* renamed from: b */
    public void mo14844b(C1554Wn wn) {
        if (this.gcn == null) {
            this.gcn = new ArrayList<>();
        }
        this.gcn.add(wn);
    }

    /* renamed from: jv */
    public C0404Fd mo14853jv(String str) {
        return this.gce.get(str);
    }

    private void ckl() {
        if (!this.gcj) {
            MethodVisitor visitMethod = C6416amY.super.visitMethod(8, C2821kb.arS, "()V", (String) null, (String[]) null);
            visitMethod.visitCode();
            visitMethod.visitMethodInsn(184, this.name, C2821kb.arT, "()V");
            visitMethod.visitInsn(177);
            visitMethod.visitMaxs(1, 1);
            visitMethod.visitEnd();
        }
        if (!this.gcs) {
            C6416amY.super.visitField(25, "serialVersionUID", "J", (String) null, (Object) null).visitEnd();
        }
        C6416amY.super.visitField(25, C2821kb.arU, "I", (String) null, (Object) null).visitEnd();
        C6416amY.super.visitField(25, C2821kb.arX, "I", (String) null, (Object) null).visitEnd();
        C6416amY.super.visitField(9, C2821kb.arQ.getName(), C2821kb.arQ.bqf().getDescriptor(), (String) null, (Object) null).visitEnd();
        MethodVisitor visitMethod2 = C6416amY.super.visitMethod(1, C2821kb.aqT.getName(), C2821kb.aqT.getDescriptor(), (String) null, (String[]) null);
        visitMethod2.visitCode();
        visitMethod2.visitFieldInsn(178, this.name, C2821kb.arQ.getName(), C2821kb.arQ.bqf().getDescriptor());
        visitMethod2.visitInsn(176);
        visitMethod2.visitMaxs(1, 1);
        visitMethod2.visitEnd();
        C6416amY.super.visitField(25, C2821kb.asf, C2821kb.aqJ.getDescriptor(), (String) null, (Object) null).visitEnd();
        C6416amY.super.visitField(25, C2821kb.ase, C2821kb.aqK.getDescriptor(), (String) null, (Object) null).visitEnd();
        MethodVisitor visitMethod3 = C6416amY.super.visitMethod(1, C2821kb.arR, C2821kb.arW, (String) null, (String[]) null);
        visitMethod3.visitCode();
        visitMethod3.visitVarInsn(25, 0);
        visitMethod3.visitVarInsn(25, 1);
        visitMethod3.visitMethodInsn(183, this.superName, C2821kb.arR, C2821kb.arW);
        visitMethod3.visitInsn(177);
        visitMethod3.visitMaxs(2, 2);
        visitMethod3.visitEnd();
        MethodVisitor visitMethod4 = C6416amY.super.visitMethod(1, C2821kb.arN.getName(), C2821kb.arN.getDescriptor(), (String) null, (String[]) null);
        visitMethod4.visitCode();
        visitMethod4.visitFieldInsn(178, this.name, C2821kb.asf, C2821kb.aqJ.getDescriptor());
        visitMethod4.visitInsn(176);
        visitMethod4.visitMaxs(2, 2);
        visitMethod4.visitEnd();
        MethodVisitor visitMethod5 = C6416amY.super.visitMethod(1, C2821kb.arO.getName(), C2821kb.arO.getDescriptor(), (String) null, (String[]) null);
        visitMethod5.visitCode();
        visitMethod5.visitFieldInsn(178, this.name, C2821kb.ase, C2821kb.aqK.getDescriptor());
        visitMethod5.visitInsn(176);
        visitMethod5.visitMaxs(2, 2);
        visitMethod5.visitEnd();
        MethodVisitor visitMethod6 = C6416amY.super.visitMethod(8, C2821kb.arT, "()V", (String) null, (String[]) null);
        visitMethod6.visitCode();
        if (!this.gcs) {
            visitMethod6.visitInsn(4);
            visitMethod6.visitInsn(133);
            visitMethod6.visitFieldInsn(179, this.name, "serialVersionUID", "J");
        }
        C6423amf.m23960c(visitMethod6, this.gcf.size());
        if (!isTopLevel()) {
            visitMethod6.visitFieldInsn(178, this.superName, C2821kb.arX, "I");
            visitMethod6.visitInsn(96);
        }
        visitMethod6.visitFieldInsn(179, this.name, C2821kb.arX, "I");
        C6423amf.m23960c(visitMethod6, this.gch.size());
        if (!isTopLevel()) {
            visitMethod6.visitFieldInsn(178, this.superName, C2821kb.arU, "I");
            visitMethod6.visitInsn(96);
        }
        visitMethod6.visitFieldInsn(179, this.name, C2821kb.arU, "I");
        C6423amf.m23960c(visitMethod6, this.gcf.size());
        if (!isTopLevel()) {
            visitMethod6.visitFieldInsn(178, this.superName, C2821kb.arX, "I");
            visitMethod6.visitInsn(89);
            visitMethod6.visitVarInsn(54, 1);
            visitMethod6.visitInsn(96);
        } else {
            visitMethod6.visitInsn(3);
            visitMethod6.visitVarInsn(54, 1);
        }
        visitMethod6.visitTypeInsn(189, C2821kb.aqF.getInternalName());
        for (C0404Fd next : this.gcf) {
            visitMethod6.visitInsn(89);
            visitMethod6.visitVarInsn(21, 1);
            visitMethod6.visitLdcInsn(this.cUV);
            visitMethod6.visitLdcInsn(next.cUW);
            visitMethod6.visitVarInsn(21, 1);
            C2821kb.arE.mo20041a(visitMethod6, 184);
            visitMethod6.visitInsn(89);
            visitMethod6.visitFieldInsn(179, this.name, next.cUQ, C2821kb.aqF.getDescriptor());
            visitMethod6.visitInsn(83);
            visitMethod6.visitIincInsn(1, 1);
        }
        visitMethod6.visitFieldInsn(179, this.name, C2821kb.asf, C2821kb.aqJ.getDescriptor());
        if (!isTopLevel()) {
            visitMethod6.visitFieldInsn(178, this.superName, C2821kb.asf, C2821kb.aqJ.getDescriptor());
            visitMethod6.visitFieldInsn(178, this.name, C2821kb.asf, C2821kb.aqJ.getDescriptor());
            C2821kb.arb.mo20041a(visitMethod6, 184);
        }
        C6423amf.m23960c(visitMethod6, this.gch.size());
        if (!isTopLevel()) {
            visitMethod6.visitFieldInsn(178, this.superName, C2821kb.arU, "I");
            visitMethod6.visitInsn(89);
            visitMethod6.visitVarInsn(54, 1);
            visitMethod6.visitInsn(96);
        } else {
            visitMethod6.visitInsn(3);
            visitMethod6.visitVarInsn(54, 1);
        }
        visitMethod6.visitTypeInsn(189, C2821kb.aqI.getInternalName());
        for (C1554Wn next2 : this.gch) {
            visitMethod6.visitInsn(89);
            visitMethod6.visitVarInsn(21, 1);
            visitMethod6.visitLdcInsn(this.cUV);
            visitMethod6.visitLdcInsn(next2.cUW);
            visitMethod6.visitVarInsn(21, 1);
            C2821kb.arC.mo20041a(visitMethod6, 184);
            visitMethod6.visitInsn(89);
            visitMethod6.visitFieldInsn(179, this.name, next2.cUQ, C2821kb.aqI.getDescriptor());
            visitMethod6.visitInsn(83);
            visitMethod6.visitIincInsn(1, 1);
        }
        visitMethod6.visitFieldInsn(179, this.name, C2821kb.ase, C2821kb.aqK.getDescriptor());
        if (!isTopLevel()) {
            visitMethod6.visitFieldInsn(178, this.superName, C2821kb.ase, C2821kb.aqK.getDescriptor());
            visitMethod6.visitFieldInsn(178, this.name, C2821kb.ase, C2821kb.aqK.getDescriptor());
            C2821kb.arb.mo20041a(visitMethod6, 184);
        }
        visitMethod6.visitLdcInsn(this.cUV);
        visitMethod6.visitLdcInsn(Type.getObjectType(this.gct));
        visitMethod6.visitFieldInsn(178, this.name, C2821kb.asf, C2821kb.aqJ.getDescriptor());
        visitMethod6.visitFieldInsn(178, this.name, C2821kb.ase, C2821kb.aqK.getDescriptor());
        C2821kb.arD.mo20041a(visitMethod6, 184);
        visitMethod6.visitFieldInsn(179, this.name, C2821kb.arQ.getName(), C2821kb.arQ.bqf().getDescriptor());
        visitMethod6.visitInsn(177);
        visitMethod6.visitMaxs(4, 2);
        visitMethod6.visitEnd();
    }

    public String ckm() {
        if (this.gcm != null) {
            return this.gcm;
        }
        int lastIndexOf = this.name.lastIndexOf(47);
        if (lastIndexOf >= 0) {
            String str = String.valueOf(this.name.substring(0, lastIndexOf + 1)) + "I" + this.name.substring(lastIndexOf + 1) + "Infra";
            this.gcm = str;
            return str;
        }
        String str2 = "I" + this.name + "Infra";
        this.gcm = str2;
        return str2;
    }

    public String ckn() {
        if (this.gco == null) {
            int max = Math.max(this.name.lastIndexOf(47), this.name.lastIndexOf(36)) + 1;
            this.gco = String.valueOf(this.name.substring(0, max)) + 'I' + this.name.substring(max);
        }
        return this.gco;
    }

    public boolean isTopLevel() {
        return this.frF;
    }

    public String cko() {
        String str;
        String str2;
        if (this.gck == null) {
            C1811aM aMVar = this.cUY.get(C2821kb.aqy.getDescriptor());
            if (aMVar != null) {
                str2 = (String) aMVar.mo9962ft().get("typeNamePattern");
                str = (String) aMVar.mo9962ft().get("typeName");
            } else {
                str = null;
                str2 = null;
            }
            if (str2 == null) {
                str2 = C2712iu.f8283Yo;
            }
            if (str == null) {
                str = C2712iu.f8282Yn;
            }
            this.gck = String.valueOf(this.gcq) + this.gcp.replaceAll(str2, str);
        }
        return this.gck;
    }

    public boolean ckp() {
        return this.cUX;
    }

    public List<C1554Wn> ckq() {
        return this.gch;
    }
}
