package p001a;

import game.network.message.serializable.C2991mi;

/* renamed from: a.Fb */
/* compiled from: a */
public class C0402Fb<T> implements C2991mi<T> {

    private final C0651JP cUJ;
    private final int index;

    public C0402Fb(C0651JP jp, int i) {
        this.cUJ = jp;
        this.index = i;
    }

    public C0651JP aPk() {
        return this.cUJ;
    }

    public int index() {
        return this.index;
    }

    public T value() {
        return null;
    }

    public String toString() {
        return String.valueOf(getClass().getSimpleName()) + " " + this.index + " " + this.cUJ;
    }
}
