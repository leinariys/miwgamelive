package p001a;

import logic.swing.C0454GJ;
import logic.swing.C2740jL;

import javax.swing.*;

/* renamed from: a.MV */
/* compiled from: a */
class C0861MV implements C5912aco.C1865a {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ C2762jf.C2763a dCS;

    C0861MV(C2762jf.C2763a aVar) {
        this.dCS = aVar;
    }

    /* renamed from: gZ */
    public void mo4gZ() {
        this.dCS.mo12691a((C5912aco.C1865a) null);
        C2762jf.this.ahE.mo23034dt(true);
        if (C2762jf.this.ahJ > 0) {
            JScrollBar buy = C2762jf.this.ahE.buy();
            this.dCS.mo12692a(buy, "[" + buy.getValue() + ".." + buy.getValue() + "] dur 100;" + "[" + buy.getValue() + "0.." + C2762jf.this.ahJ + "] dur " + "50" + " regular_in", new C0862a(), new C0863b());
            return;
        }
        C2762jf.this.ahK.setVisible(false);
        this.dCS.bOD();
        this.dCS.stop();
    }

    /* renamed from: a.MV$a */
    class C0862a implements C2740jL<JScrollBar> {
        C0862a() {
        }

        /* renamed from: a */
        public void mo7a(JScrollBar jScrollBar, float f) {
            jScrollBar.setValue((int) f);
        }

        /* renamed from: a */
        public float mo5a(JScrollBar jScrollBar) {
            return (float) jScrollBar.getValue();
        }
    }

    /* renamed from: a.MV$b */
    /* compiled from: a */
    class C0863b implements C0454GJ {
        C0863b() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            C2762jf.this.ahK.setVisible(false);
            C0861MV.this.dCS.bOD();
            C0861MV.this.dCS.stop();
        }
    }
}
