package p001a;

import logic.res.LoaderFileXML;
import logic.res.XmlNode;
import org.mozilla1.classfile.C0147Bi;
import taikodom.render.loader.provider.FilePath;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.azH */
/* compiled from: a */
public class azH {
    private final Object root;
    /* renamed from: yh */
    private final C1942an f5670yh;
    private C2017a hcD = null;
    private List<C0273DW> hcE = new ArrayList();
    private String hcF;
    private XmlNode hcG;
    private FilePath hcH;

    public azH(FilePath ain, Object obj, C1942an anVar) throws Exception {
        this.root = obj;
        this.f5670yh = anVar;
        try {
            this.hcH = ain;
            this.hcG = new LoaderFileXML().loadFileXml(ain.openInputStream(), "UTF-8");
            this.hcF = this.hcG.getAttribute("name");
        } catch (IOException e) {
            throw new Exception("Cannot parse descriptor", e);
        }
    }

    public void start() throws Exception {
        String str;
        try {
            this.hcG = new LoaderFileXML().loadFileXml(this.hcH.openInputStream(), "UTF-8");
            List<XmlNode> children = this.hcG.findNodeChildTag("classpath").getListChildrenTag();
            ArrayList arrayList = new ArrayList();
            for (XmlNode next : children) {
                if ("dir".equals(next.getTagName())) {
                    String attribute = next.getAttribute("path");
                    if (!attribute.endsWith(C0147Bi.SEPARATOR)) {
                        str = String.valueOf(attribute) + C0147Bi.SEPARATOR;
                    } else {
                        str = attribute;
                    }
                    try {
                        arrayList.add(this.hcH.mo2249BB().concat(str).getFile().toURL());
                    } catch (MalformedURLException e) {
                        throw new Exception("Cannot add classpath: " + str, e);
                    }
                } else if ("jar".equals(next.getTagName())) {
                    String attribute2 = next.getAttribute("path");
                    try {
                        arrayList.add(this.hcH.mo2249BB().concat(attribute2).getFile().toURL());
                    } catch (MalformedURLException e2) {
                        throw new Exception("Cannot add classpath: " + attribute2, e2);
                    }
                }
            }
            URL[] urlArr = new URL[arrayList.size()];
            arrayList.toArray(urlArr);
            this.hcD = new C2017a(urlArr, getClass().getClassLoader());
            XmlNode mC = this.hcG.findNodeChildTag("libraries");
            if (mC != null) {
                for (XmlNode next2 : mC.getListChildrenTag()) {
                    if ("dir".equals(next2.getTagName())) {
                        String attribute3 = next2.getAttribute("path");
                        if (!attribute3.endsWith(C0147Bi.SEPARATOR)) {
                            attribute3 = String.valueOf(attribute3) + C0147Bi.SEPARATOR;
                        }
                        this.hcD.mo17160hZ(this.hcH.mo2249BB().concat(attribute3).getFile().getAbsolutePath());
                    }
                }
            }
            for (XmlNode attribute4 : this.hcG.findNodeChildAllTag("component")) {
                String attribute5 = attribute4.getAttribute("class");
                try {
                    this.hcE.add((C0273DW) this.hcD.loadClass(attribute5).newInstance());
                } catch (ClassNotFoundException e3) {
                    throw new Exception("Cannot load class: " + attribute5, e3);
                } catch (InstantiationException e4) {
                    throw new Exception("Cannot instanciate class: " + attribute5, e4);
                } catch (IllegalAccessException e5) {
                    throw new Exception("Cannot instanciate class: " + attribute5, e5);
                }
            }
            for (C0273DW a : this.hcE) {
                a.mo1633a(this.root, this);
            }
        } catch (IOException e6) {
            throw new Exception("Cannot parse descriptor", e6);
        }
    }

    public C1942an cHm() {
        return this.f5670yh;
    }

    public String cHn() {
        return this.hcF;
    }

    public void stop() {
        for (C0273DW stop : this.hcE) {
            stop.stop();
        }
        this.hcE.clear();
        this.hcD = null;
    }

    /* renamed from: a.azH$a */
    static class C2017a extends URLClassLoader {
        private List<String> eSu = new ArrayList();

        public C2017a(URL[] urlArr, ClassLoader classLoader) {
            super(urlArr, classLoader);
        }

        /* renamed from: hZ */
        public void mo17160hZ(String str) {
            this.eSu.add(str);
        }

        /* access modifiers changed from: protected */
        public String findLibrary(String str) {
            String findLibrary = super.findLibrary(str);
            if (findLibrary == null) {
                for (String file : this.eSu) {
                    File file2 = new File(file, System.mapLibraryName(str));
                    if (file2.exists()) {
                        return file2.getAbsolutePath();
                    }
                }
            }
            return findLibrary;
        }
    }
}
