package p001a;

import game.network.message.externalizable.C1291Sy;
import game.network.message.externalizable.C2631hn;
import game.network.message.serializable.C5287aDn;
import game.network.message.serializable.C5512aMe;
import game.network.message.serializable.C5985aeJ;
import logic.baa.C1616Xf;
import logic.baa.aOW;
import logic.res.XmlNode;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.thred.LogPrinter;
import logic.ui.item.ListJ;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.lang.reflect.Method;
import java.util.*;

/* renamed from: a.aHL */
/* compiled from: a */
public final class aHL {
    private static LogPrinter logger = LogPrinter.m10275K(aHL.class);

    /* renamed from: yj */
    private static Map<String, C3672tZ> f2918yj = new HashMap();

    static {
        try {
            f2918yj.put("boolean", new C3672tZ(Boolean.class));
            f2918yj.put("int", new C3672tZ(Integer.class));
            f2918yj.put("byte", new C3672tZ(Byte.class));
            f2918yj.put("short", new C3672tZ(Short.class));
            f2918yj.put("long", new C3672tZ(Long.class));
            f2918yj.put("double", new C3672tZ(Double.class));
            f2918yj.put("float", new C3672tZ(Float.class));
            f2918yj.put("java.lang.Boolean", new C3672tZ(Boolean.class));
            f2918yj.put("java.lang.Integer", new C3672tZ(Integer.class));
            f2918yj.put("java.lang.Byte", new C3672tZ(Byte.class));
            f2918yj.put("java.lang.Short", new C3672tZ(Short.class));
            f2918yj.put("java.lang.Long", new C3672tZ(Long.class));
            f2918yj.put("java.lang.Double", new C3672tZ(Double.class));
            f2918yj.put("java.lang.Float", new C3672tZ(Float.class));
        } catch (SecurityException e) {
            throw new Error(e);
        } catch (NoSuchMethodException e2) {
            throw new Error(e2);
        }
    }

    C1260Sc cvx;
    private ListJ<String> ahu;
    private long cmb;
    private Map<Long, C5512aMe> cvp;
    private long cvq;
    private Map<String, C5985aeJ> cvr;
    private Map<String, C5985aeJ> cvs;
    private Map<String, C5985aeJ> cvt;
    private C6361alV cvu;
    private boolean cvv;
    private int cvw;

    public aHL() {
        boolean z;
        this.cvp = new HashMap();
        this.cvq = 0;
        this.cvr = new HashMap();
        this.cvs = new HashMap();
        this.cvt = new HashMap();
        this.ahu = new ArrayList();
        if (System.getProperty("lean-xml-deserializer", (String) null) != null) {
            z = true;
        } else {
            z = false;
        }
        this.cvv = z;
        this.cvx = new C0410Fj(this);
        this.cvu = new C6361alV();
    }

    public aHL(C6280ajs ajs, InputStream inputStream) {
        this();
        mo9180b(inputStream);
        aBr();
        if (System.getProperty("ignore-version") == null) {
            aBs();
        }
        aBq();
        if (System.getProperty("ignore-version") == null) {
            aBp();
        }
        logger.info("Deserialization finished");
    }

    /* renamed from: b */
    public void mo9180b(InputStream inputStream) {
        int i;
        logger.info("Parsing XML");
        C1492Vy vy = new C1492Vy(inputStream, "UTF-8");
        XmlNode bBf = vy.bBf();
        this.cmb = Long.parseLong(bBf.getAttribute("version"));
        if (bBf.getAttribute("structureVersion") != null) {
            i = Integer.parseInt(bBf.getAttribute("structureVersion"));
        } else {
            i = 0;
        }
        if (i <= 0) {
            vy.mo6459dB(false);
        }
        while (true) {
            XmlNode bBg = vy.bBg();
            if (bBg == null) {
                this.cvu.cza();
                return;
            } else if ("scriptClasses".equals(bBg.getTagName())) {
                m15148c(bBg);
            } else {
                long j = 0;
                String str = null;
                try {
                    j = Long.parseLong(bBg.getAttribute("id"));
                    str = bBg.getAttribute("class");
                    m15145a(m15141a(m15149dP(str), j), bBg);
                } catch (Exception e) {
                    throw new RuntimeException("Error reading object " + str + ", " + j, e);
                }
            }
        }
    }

    public void aBp() {
        if (this.cvv) {
            logger.info("Not calling onLoad of version changed scripts because of leanMode");
            return;
        }
        logger.info("Calling onLoad of version changed scripts");
        for (C5512aMe next : this.cvp.values()) {
            C1616Xf yz = next.mo10098yz();
            if (yz instanceof aOW) {
                ((aOW) yz).mo24b(next);
            }
        }
    }

    public void aBq() {
        logger.info("Filling game.script values");
        for (C5512aMe a : this.cvp.values()) {
            m15144a(a);
        }
        for (C5512aMe b : this.cvp.values()) {
            m15147b(b);
        }
    }

    /* renamed from: a */
    private void m15144a(C5512aMe ame) {
        if (!ame.diT()) {
            C1616Xf yz = ame.mo10098yz();
            if (yz.bFf().getObjectId().getId() > this.cvq) {
                this.cvq = yz.bFf().getObjectId().getId();
            }
            for (C5663aRz arz : yz.mo25c()) {
                Object obj = ame.get(arz.name());
                if (obj != null) {
                    Object unwrap = unwrap(obj);
                    if (unwrap == null) {
                        logger.error("Failed at unwrap of: " + obj);
                    } else {
                        try {
                            if (!arz.mo11291El().isPrimitive()) {
                                if (!arz.isCollection() && !arz.mo11292En() && arz.mo11291El().isInstance(unwrap)) {
                                    yz.mo6765g(arz, unwrap);
                                }
                            } else if (ame.diQ().bUD().get(arz.name()).getType().equals(arz.mo11291El().getName())) {
                                yz.mo6765g(arz, unwrap);
                            }
                        } catch (RuntimeException e) {
                            logger.error("error copying values of field: " + yz.getClass().getName() + ":" + yz.bFf().getObjectId().getId() + "." + arz, e);
                            throw e;
                        }
                    }
                }
            }
        }
    }

    /* renamed from: b */
    private void m15147b(C5512aMe ame) {
        if (!ame.diT()) {
            C1616Xf yz = ame.mo10098yz();
            if (yz.bFf().getObjectId().getId() > this.cvq) {
                this.cvq = yz.bFf().getObjectId().getId();
            }
            for (C5663aRz arz : yz.mo25c()) {
                Object obj = ame.get(arz.name());
                if (obj != null) {
                    Object unwrap = unwrap(obj);
                    if (unwrap == null) {
                        logger.error("Failed at unwrap of: " + obj);
                    } else {
                        try {
                            if (!arz.mo11291El().isPrimitive()) {
                                if (arz.isCollection()) {
                                    C2631hn hnVar = (C2631hn) yz.mo6767w(arz);
                                    for (Object unwrap2 : (Collection) unwrap) {
                                        hnVar.mo17726l(unwrap(unwrap2));
                                    }
                                } else if (arz.mo11292En()) {
                                    C1291Sy sy = (C1291Sy) yz.mo6767w(arz);
                                    for (Map.Entry entry : ((Map) unwrap).entrySet()) {
                                        sy.mo5513h(unwrap(entry.getKey()), unwrap(entry.getValue()));
                                    }
                                }
                            }
                        } catch (RuntimeException e) {
                            logger.error("error copying values of field: " + yz.getClass().getName() + ":" + yz.bFf().getObjectId().getId() + "." + arz, e);
                            throw e;
                        }
                    }
                }
            }
        }
    }

    public void aBr() {
        if (this.ahu.size() > 0) {
            for (String error : this.ahu) {
                logger.error(error);
            }
            throw new RuntimeException(this.ahu.toString());
        }
    }

    public void aBs() {
        if (this.cvv) {
            logger.info("Not calling onPreLoadVersion of version changed scripts because of leanMode");
            return;
        }
        logger.info("Calling onPreLoadVersion of versionAwareScripts");
        for (C5512aMe next : this.cvp.values()) {
            try {
                Method bUH = next.bUE().bUH();
                if (bUH != null) {
                    bUH.invoke((Object) null, new Object[]{next});
                }
            } catch (Exception e) {
                throw new RuntimeException("Error at " + next.diQ().getClassName() + ":" + next.mo10072Ej(), e);
            }
        }
    }

    private Object unwrap(Object obj) {
        if (!(obj instanceof C5512aMe)) {
            return obj;
        }
        C5512aMe ame = this.cvp.get(Long.valueOf(((C5512aMe) obj).mo10072Ej()));
        if (ame == null) {
            this.ahu.add("No game.script create for " + ame);
            return null;
        }
        C1616Xf yz = ame.mo10098yz();
        if (yz == null) {
            if (this.cvv) {
                yz = ame.mo10098yz();
            }
            if (yz == null) {
                this.ahu.add("No game.script created for " + ame);
            }
        }
        return yz;
    }

    /* renamed from: a */
    private void m15145a(C5512aMe ame, XmlNode agy) {
        C5985aeJ diQ = ame.diQ();
        if (diQ == null) {
            throw new NullPointerException("No old class definition for " + ame + " at " + agy);
        }
        for (XmlNode next : agy.getListChildrenTag()) {
            String name = next.getTagName();
            C5287aDn adn = diQ.bUD().get(name);
            if (adn != null) {
                try {
                    ame.setField(name, m15142a(adn, next));
                } catch (Exception e) {
                    throw new RuntimeException("Error reading field " + adn.getName() + " " + ame.diQ() + ":" + ame.mo10072Ej() + " type: " + adn.getType(), e);
                }
            } else {
                ame.setField(name, next.toString());
            }
        }
    }

    /* renamed from: yx */
    public long mo9182yx() {
        return this.cvq;
    }

    public long aBt() {
        return this.cmb;
    }

    /* renamed from: c */
    private void m15148c(XmlNode agy) {
        for (XmlNode next : agy.getListChildrenTag()) {
            C5985aeJ aej = new C5985aeJ(next.getAttribute("class"));
            this.cvr.put(aej.getClassName(), aej);
            aej.setVersion(next.getAttribute("version"));
            for (XmlNode next2 : next.getListChildrenTag()) {
                C5287aDn adn = new C5287aDn(next2.getAttribute("name"), next2.getAttribute("type"));
                aej.mo13030a(adn);
                adn.mo8502lU(next2.getAttribute("componentTypes"));
            }
            C5985aeJ aej2 = this.cvt.get(aej.getClassName());
            if (aej2 != null) {
                aej.mo13031a(aej2);
            }
        }
    }

    /* renamed from: a */
    public void mo9171a(C6280ajs ajs) {
        for (Class next : ajs.aXU()) {
            C5985aeJ aej = new C5985aeJ(next.getName());
            C6494any B = ajs.mo3258B(next);
            aej.setVersion(B.getVersion());
            C3437rZ rZVar = (C3437rZ) next.getAnnotation(C3437rZ.class);
            if (rZVar != null) {
                m15146a(aej, rZVar.aal(), rZVar.aak());
            }
            C6793atl atl = (C6793atl) next.getAnnotation(C6793atl.class);
            if (atl != null) {
                String[] value = atl.value();
                for (int i = 0; i < value.length; i += 2) {
                    m15146a(aej, value[i + 0], value[i + 1]);
                }
            }
            for (C5663aRz arz : B.mo15889c()) {
                if (arz != null) {
                    C5287aDn adn = new C5287aDn(arz.name(), arz.mo11291El().getName());
                    adn.mo8492aF(arz.mo11291El());
                    adn.mo8493c(arz.mo11295Eq());
                    aej.mo13030a(adn);
                } else {
                    logger.warn("null field at: " + B.getName());
                }
            }
            this.cvs.put(aej.getClassName(), aej);
        }
    }

    /* renamed from: a */
    private void m15146a(C5985aeJ aej, String str, String str2) {
        if (this.cvt.containsKey(str)) {
            this.ahu.add("Trying to map old class " + str + " to new class " + aej.getClassName() + " but it is already maped to " + this.cvt.get(str).getClassName());
        } else {
            this.cvt.put(str, aej);
        }
    }

    /* renamed from: dP */
    private C5985aeJ m15149dP(String str) {
        C5985aeJ aej = this.cvr.get(str);
        if (aej != null) {
            return aej;
        }
        C5985aeJ aej2 = this.cvs.get(str);
        return aej2 == null ? this.cvt.get(str) : aej2;
    }

    /* renamed from: a */
    private C5512aMe m15141a(C5985aeJ aej, long j) {
        C5512aMe ame = this.cvp.get(Long.valueOf(j));
        if (ame == null) {
            C5512aMe ame2 = new C5512aMe();
            ame2.mo10093lh(j);
            this.cvp.put(Long.valueOf(j), ame2);
            ame2.mo10078b(aej);
            aej.bUF();
            return ame2;
        } else if (ame.diQ() != null) {
            return ame;
        } else {
            ame.mo10078b(aej);
            aej.bUF();
            return ame;
        }
    }

    /* renamed from: a */
    private Object m15142a(C5287aDn adn, XmlNode agy) {
        Object obj;
        AbstractCollection arrayList;
        Object obj2;
        if (adn.isCollection()) {
            ListJ<XmlNode> mF = agy.findNodeChildAllTag("item");
            if (adn.cTE()) {
                arrayList = new HashSet();
            } else {
                arrayList = new ArrayList();
            }
            String str = adn.cTC()[0];
            for (XmlNode next : mF) {
                if (!"yes".equals(next.getAttribute("null"))) {
                    obj2 = m15143a(next, str);
                } else {
                    obj2 = null;
                }
                arrayList.add(obj2);
            }
            return arrayList;
        } else if (!adn.mo8491En()) {
            return m15143a(agy, adn.getType());
        } else {
            ListJ<XmlNode> mF2 = agy.findNodeChildAllTag("item");
            HashMap hashMap = new HashMap();
            if (adn.cTC().length != 2) {
                throw new RuntimeException("You must specify the component types for the Map (example: DataMap<Integer,String> instead of just DataMap)");
            }
            String str2 = adn.cTC()[0];
            String str3 = adn.cTC()[1];
            for (XmlNode next2 : mF2) {
                Object a = m15143a(next2.findNodeChildTag("key"), str2);
                XmlNode mC = next2.findNodeChildTag("value");
                if (!"yes".equals(mC.getAttribute("null"))) {
                    obj = m15143a(mC, str3);
                } else {
                    obj = null;
                }
                hashMap.put(a, obj);
            }
            return hashMap;
        }
    }

    /* renamed from: a */
    private Object m15143a(XmlNode agy, String str) {
        if (agy.getAttribute("scriptObject") != null) {
            return m15152el(Long.parseLong(agy.getAttribute("scriptObject")));
        }
        if (m15151dR(str)) {
            String content = agy.getContent();
            if (content != null) {
                return f2918yj.get(str).mo22256O(content);
            }
            return null;
        } else if (agy.getAttribute("blob") != null) {
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(m15150dQ(agy.getContent())));
            Object readObject = objectInputStream.readObject();
            objectInputStream.close();
            return readObject;
        } else if (agy.getAttribute("enum") != null) {
            try {
                Class<?> cls = Class.forName(agy.getAttribute("class"));
                String attribute = agy.getAttribute("member");
                for (Object obj : cls.getEnumConstants()) {
                    if (((Enum) obj).name().equals(attribute)) {
                        return obj;
                    }
                }
                throw new IllegalStateException("Enum member not found: " + cls.getName() + "." + attribute);
            } catch (ClassNotFoundException e) {
                logger.error("Enum not found: " + agy.getAttribute("class"));
                return agy.getAttribute("member");
            }
        } else if ("yes".equals(agy.getAttribute("isClass"))) {
            return Class.forName(agy.getAttribute("class"));
        } else {
            if (agy.getAttribute("xObject") != null) {
                return this.cvu.mo14688g(agy);
            }
            return null;
        }
    }

    /* renamed from: el */
    private Object m15152el(long j) {
        C5512aMe ame = this.cvp.get(Long.valueOf(j));
        if (ame != null) {
            return ame;
        }
        C5512aMe ame2 = new C5512aMe();
        ame2.mo10093lh(j);
        this.cvp.put(Long.valueOf(j), ame2);
        return ame2;
    }

    /* renamed from: dQ */
    private byte[] m15150dQ(String str) {
        int length = str.length() / 2;
        byte[] bArr = new byte[length];
        for (int i = 0; i < length; i++) {
            bArr[i] = (byte) Integer.parseInt(str.substring(i * 2, (i * 2) + 2), 16);
        }
        return bArr;
    }

    /* renamed from: dR */
    private boolean m15151dR(String str) {
        return f2918yj.containsKey(str);
    }

    public Map<Long, C5512aMe> aBu() {
        return this.cvp;
    }

    /* renamed from: j */
    public void mo9181j(Map<Long, C5512aMe> map) {
        this.cvp = map;
    }

    public void aBv() {
        if (this.cvv) {
            throw new IllegalStateException("Should not be in lean mode");
        }
        logger.info("Calling onMergerDatabase of scripts");
        for (C5512aMe next : this.cvp.values()) {
            C1616Xf yz = next.mo10098yz();
            if (yz instanceof C0943Nq) {
                ((C0943Nq) yz).mo625b(next);
            }
        }
    }

    public void aBw() {
        if (this.cvv) {
            throw new IllegalStateException("Should not be in lean mode");
        }
        logger.info("Calling onPreMergeDatabase");
        for (C5512aMe next : this.cvp.values()) {
            try {
                Method bUI = next.bUE().bUI();
                if (bUI != null) {
                    bUI.invoke((Object) null, new Object[]{this.cvx, next});
                }
            } catch (Exception e) {
                throw new RuntimeException("Error at " + next.diQ().getClassName() + ":" + next.mo10072Ej(), e);
            }
        }
        Iterator it = new ArrayList(this.cvp.values()).iterator();
        while (it.hasNext()) {
            C5512aMe ame = (C5512aMe) it.next();
            if (ame.diS()) {
                this.cvp.remove(Long.valueOf(ame.mo10072Ej()));
            }
        }
    }
}
