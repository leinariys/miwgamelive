package p001a;

import com.hoplon.geometry.Vec3f;
import game.engine.IEngineGame;
import game.geometry.Vec3d;
import game.geometry.Vector2fWrap;
import game.script.resource.Asset;
import logic.render.IEngineGraphics;
import logic.res.sound.C0907NJ;
import taikodom.render.RenderView;
import taikodom.render.helpers.RenderTask;
import taikodom.render.helpers.RenderVisitor;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.SSoundGroup;
import taikodom.render.scene.SSoundSource;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

/* renamed from: a.pi */
/* compiled from: a */
public class C3257pi implements C0907NJ {
    private static final RenderVisitor caE = new C3266pn();
    private static final float caF = 0.95f;
    private static final RenderVisitor caG = new C3263pk();
    private static int[] caH = {30000, 10000, 3000};
    /* access modifiers changed from: private */

    /* renamed from: Uz */
    public SceneObject f8846Uz;
    private SceneObject bxn;
    private Vec3d caC;
    private boolean caD = false;
    private Vector2fWrap distances;

    public SceneObject asx() {
        return this.f8846Uz;
    }

    /* renamed from: a */
    public void mo968a(RenderAsset renderAsset) {
        IEngineGame ald = C5916acs.getSingolton();
        if (renderAsset != null && ald.getEngineGraphics() != null) {
            Scene adZ = ald.getEngineGraphics().adZ();
            this.f8846Uz = (SceneObject) renderAsset;
            if ((this.f8846Uz instanceof SSoundGroup) && this.caD) {
                ((SSoundGroup) this.f8846Uz).setMirrorX(true);
            }
            if (this.distances != null) {
                if (this.f8846Uz instanceof SSoundGroup) {
                    ((SSoundGroup) this.f8846Uz).setDistances(this.distances);
                } else if (this.f8846Uz instanceof SSoundSource) {
                    ((SSoundSource) this.f8846Uz).setDistances(this.distances);
                }
            }
            if (this.bxn != null) {
                this.bxn.addChild(this.f8846Uz);
            } else {
                adZ.addChild(this.f8846Uz);
            }
            this.f8846Uz.setPosition(this.caC);
            this.f8846Uz.visitPreOrder(caG);
            this.f8846Uz.visitPreOrder(caE);
        }
    }

    /* renamed from: a */
    public boolean mo21215a(Vec3d ajr, C3261d dVar) {
        IEngineGraphics ale = C5916acs.getSingolton().getEngineGraphics();
        if (ale != null && Vec3d.m15653n(ale.aea().getCamera().getGlobalTransform().position, ajr) <= ((double) caH[dVar.ordinal()])) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public boolean mo21216a(Vec3d ajr, SceneObject sceneObject, C3261d dVar) {
        if (sceneObject == null) {
            return mo21215a(ajr, dVar);
        }
        IEngineGraphics ale = C5916acs.getSingolton().getEngineGraphics();
        if (ale == null) {
            return false;
        }
        Vec3d ajr2 = ale.aea().getCamera().getGlobalTransform().position;
        Vec3d ajr3 = new Vec3d();
        sceneObject.getGlobalTransform().mo17343b(ajr, ajr3);
        if (Vec3d.m15653n(ajr2, ajr3) <= ((double) caH[dVar.ordinal()])) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public void mo21211a(Asset tCVar, Vec3d ajr, Vec3f vec3f, C3261d dVar) {
        mo21212a(tCVar, ajr, vec3f, (SceneObject) null, dVar);
    }

    /* renamed from: a */
    public void mo21212a(Asset tCVar, Vec3d ajr, Vec3f vec3f, SceneObject sceneObject, C3261d dVar) {
        this.bxn = sceneObject;
        if (tCVar != null && mo21216a(ajr, sceneObject, dVar)) {
            IEngineGame ald = C5916acs.getSingolton();
            if (ald.getEngineGraphics() != null) {
                Scene adZ = ald.getEngineGraphics().adZ();
                this.f8846Uz = (SceneObject) ald.getLoaderTrail().getAsset(tCVar.getHandle());
                this.caC = ajr;
                if (this.f8846Uz != null) {
                    ald.getEngineGraphics().mo3007a((RenderTask) new C3260c());
                    return;
                }
                ald.getLoaderTrail().mo4995a(tCVar.getFile(), tCVar.getHandle(), adZ, this, "EffectView:spawnEffect");
            }
        }
    }

    /* renamed from: a */
    public void mo21209a(Asset tCVar, Vec3d ajr, C3261d dVar) {
        if (mo21215a(ajr, dVar)) {
            m37274a(tCVar, ajr, (SceneObject) null, false);
        }
    }

    /* renamed from: a */
    public void mo21210a(Asset tCVar, Vec3d ajr, C3261d dVar, float f) {
        if (mo21215a(ajr, dVar)) {
            if (f > 0.0f) {
                this.distances = new Vector2fWrap(Math.max(2.0f * f, (float) (caH[dVar.ordinal()] / 10)), Math.max(20.0f * f, (float) caH[dVar.ordinal()]));
            }
            m37274a(tCVar, ajr, (SceneObject) null, false);
        }
    }

    /* renamed from: a */
    public void mo21213a(Asset tCVar, Vec3d ajr, SceneObject sceneObject, C3261d dVar) {
        if (mo21216a(ajr, sceneObject, dVar)) {
            m37274a(tCVar, new Vec3d(ajr), sceneObject, false);
        }
    }

    /* renamed from: a */
    public void mo21214a(Asset tCVar, Vec3d ajr, SceneObject sceneObject, C3261d dVar, boolean z) {
        if (mo21216a(ajr, sceneObject, dVar)) {
            m37274a(tCVar, new Vec3d(ajr), sceneObject, z);
        }
    }

    /* renamed from: a */
    private void m37274a(Asset tCVar, Vec3d ajr, SceneObject sceneObject, boolean z) {
        if (tCVar != null) {
            this.bxn = sceneObject;
            this.caD = z;
            IEngineGame ald = C5916acs.getSingolton();
            if (ald.getEngineGraphics() != null) {
                this.caC = ajr;
                Scene adZ = ald.getEngineGraphics().adZ();
                this.f8846Uz = (SceneObject) ald.getLoaderTrail().getAsset(tCVar.getHandle());
                if (this.f8846Uz != null) {
                    ald.getEngineGraphics().mo3007a((RenderTask) new C3258a());
                    return;
                }
                ald.getLoaderTrail().mo4995a(tCVar.getFile(), tCVar.getHandle(), adZ, this, "EffectView:spawnEffect");
            }
        }
    }

    public void asy() {
        if (this.f8846Uz != null) {
            C5916acs.getSingolton().getEngineGraphics().mo3007a((RenderTask) new C3259b());
        }
    }

    public boolean isDisposed() {
        return this.f8846Uz == null || this.f8846Uz.isDisposed();
    }

    /* renamed from: a.pi$d */
    /* compiled from: a */
    public enum C3261d {
        FAR_AWAY,
        MID_RANGE,
        NEAR_ENOUGH
    }

    /* renamed from: a.pi$c */
    /* compiled from: a */
    class C3260c implements RenderTask {
        C3260c() {
        }

        public void run(RenderView renderView) {
            C3257pi.this.mo968a((RenderAsset) C3257pi.this.f8846Uz);
        }
    }

    /* renamed from: a.pi$a */
    class C3258a implements RenderTask {
        C3258a() {
        }

        public void run(RenderView renderView) {
            C3257pi.this.mo968a((RenderAsset) C3257pi.this.f8846Uz);
        }
    }

    /* renamed from: a.pi$b */
    /* compiled from: a */
    class C3259b implements RenderTask {
        C3259b() {
        }

        public void run(RenderView renderView) {
            if (C3257pi.this.f8846Uz != null) {
                C3257pi.this.f8846Uz.dispose();
            }
            C3257pi.this.f8846Uz = null;
        }
    }
}
