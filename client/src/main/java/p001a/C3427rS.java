package p001a;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;

import javax.vecmath.Quat4f;

/* renamed from: a.rS */
/* compiled from: a */
public class C3427rS {

    /* renamed from: jR */
    static final /* synthetic */ boolean f9033jR = (!C3427rS.class.desiredAssertionStatus());

    /* renamed from: a */
    public static void m38369a(Matrix3fWrap ajd, Matrix3fWrap ajd2, Vec3f vec3f) {
        ajd.m00 = ajd2.m00 * vec3f.x;
        ajd.m01 = ajd2.m01 * vec3f.y;
        ajd.m02 = ajd2.m02 * vec3f.z;
        ajd.m10 = ajd2.m10 * vec3f.x;
        ajd.m11 = ajd2.m11 * vec3f.y;
        ajd.m12 = ajd2.m12 * vec3f.z;
        ajd.m20 = ajd2.m20 * vec3f.x;
        ajd.m21 = ajd2.m21 * vec3f.y;
        ajd.m22 = ajd2.m22 * vec3f.z;
    }

    /* renamed from: b */
    public static void m38374b(Matrix3fWrap ajd) {
        ajd.m00 = Math.abs(ajd.m00);
        ajd.m01 = Math.abs(ajd.m01);
        ajd.m02 = Math.abs(ajd.m02);
        ajd.m10 = Math.abs(ajd.m10);
        ajd.m11 = Math.abs(ajd.m11);
        ajd.m12 = Math.abs(ajd.m12);
        ajd.m20 = Math.abs(ajd.m20);
        ajd.m21 = Math.abs(ajd.m21);
        ajd.m22 = Math.abs(ajd.m22);
    }

    /* renamed from: a */
    public static void m38371a(Matrix3fWrap ajd, float[] fArr) {
        ajd.m00 = fArr[0];
        ajd.m01 = fArr[4];
        ajd.m02 = fArr[8];
        ajd.m10 = fArr[1];
        ajd.m11 = fArr[5];
        ajd.m12 = fArr[9];
        ajd.m20 = fArr[2];
        ajd.m21 = fArr[6];
        ajd.m22 = fArr[10];
    }

    /* renamed from: b */
    public static void m38376b(Matrix3fWrap ajd, float[] fArr) {
        fArr[0] = ajd.m00;
        fArr[1] = ajd.m10;
        fArr[2] = ajd.m20;
        fArr[3] = 0.0f;
        fArr[4] = ajd.m01;
        fArr[5] = ajd.m11;
        fArr[6] = ajd.m21;
        fArr[7] = 0.0f;
        fArr[8] = ajd.m02;
        fArr[9] = ajd.m12;
        fArr[10] = ajd.m22;
        fArr[11] = 0.0f;
    }

    /* renamed from: a */
    public static void m38368a(Matrix3fWrap ajd, float f, float f2, float f3) {
        float cos = (float) Math.cos((double) f);
        float cos2 = (float) Math.cos((double) f2);
        float cos3 = (float) Math.cos((double) f3);
        float sin = (float) Math.sin((double) f);
        float sin2 = (float) Math.sin((double) f2);
        float sin3 = (float) Math.sin((double) f3);
        float f4 = cos * cos3;
        float f5 = cos * sin3;
        float f6 = sin * cos3;
        float f7 = sin * sin3;
        ajd.setRow(0, cos3 * cos2, (sin2 * f6) - f5, (sin2 * f4) + f7);
        ajd.setRow(1, sin3 * cos2, f4 + (f7 * sin2), (f5 * sin2) - f6);
        ajd.setRow(2, -sin2, sin * cos2, cos * cos2);
    }

    /* renamed from: b */
    private static float m38373b(Matrix3fWrap ajd, Vec3f vec3f) {
        return (ajd.m00 * vec3f.x) + (ajd.m10 * vec3f.y) + (ajd.m20 * vec3f.z);
    }

    /* renamed from: c */
    private static float m38377c(Matrix3fWrap ajd, Vec3f vec3f) {
        return (ajd.m01 * vec3f.x) + (ajd.m11 * vec3f.y) + (ajd.m21 * vec3f.z);
    }

    /* renamed from: d */
    private static float m38379d(Matrix3fWrap ajd, Vec3f vec3f) {
        return (ajd.m02 * vec3f.x) + (ajd.m12 * vec3f.y) + (ajd.m22 * vec3f.z);
    }

    /* renamed from: a */
    public static void m38372a(Vec3f vec3f, Vec3f vec3f2, Matrix3fWrap ajd) {
        float b = m38373b(ajd, vec3f2);
        float c = m38377c(ajd, vec3f2);
        float d = m38379d(ajd, vec3f2);
        vec3f.x = b;
        vec3f.y = c;
        vec3f.z = d;
    }

    /* renamed from: a */
    public static void m38370a(Matrix3fWrap ajd, Quat4f quat4f) {
        float f = (quat4f.x * quat4f.x) + (quat4f.y * quat4f.y) + (quat4f.z * quat4f.z) + (quat4f.w * quat4f.w);
        if (f9033jR || f != 0.0f) {
            float f2 = 2.0f / f;
            float f3 = quat4f.x * f2;
            float f4 = quat4f.y * f2;
            float f5 = f2 * quat4f.z;
            float f6 = quat4f.w * f3;
            float f7 = quat4f.w * f4;
            float f8 = quat4f.w * f5;
            float f9 = f3 * quat4f.x;
            float f10 = quat4f.x * f4;
            float f11 = quat4f.x * f5;
            float f12 = f4 * quat4f.y;
            float f13 = quat4f.y * f5;
            float f14 = f5 * quat4f.z;
            ajd.m00 = 1.0f - (f12 + f14);
            ajd.m01 = f10 - f8;
            ajd.m02 = f11 + f7;
            ajd.m10 = f8 + f10;
            ajd.m11 = 1.0f - (f14 + f9);
            ajd.m12 = f13 - f6;
            ajd.m20 = f11 - f7;
            ajd.m21 = f13 + f6;
            ajd.m22 = 1.0f - (f9 + f12);
            return;
        }
        throw new AssertionError();
    }

    /* renamed from: b */
    public static void m38375b(Matrix3fWrap ajd, Quat4f quat4f) {
        C0763Kt bcE = C0763Kt.bcE();
        float f = ajd.m22 + ajd.m00 + ajd.m11;
        float[] qW = bcE.bcO().mo1166qW(4);
        if (f > 0.0f) {
            float sqrt = (float) Math.sqrt((double) (f + 1.0f));
            qW[3] = sqrt * 0.5f;
            float f2 = 0.5f / sqrt;
            qW[0] = (ajd.m21 - ajd.m12) * f2;
            qW[1] = (ajd.m02 - ajd.m20) * f2;
            qW[2] = f2 * (ajd.m10 - ajd.m01);
        } else {
            int i = ajd.m00 < ajd.m11 ? ajd.m11 < ajd.m22 ? 2 : 1 : ajd.m00 < ajd.m22 ? 2 : 0;
            int i2 = (i + 1) % 3;
            int i3 = (i + 2) % 3;
            float sqrt2 = (float) Math.sqrt((double) (((ajd.getElement(i, i) - ajd.getElement(i2, i2)) - ajd.getElement(i3, i3)) + 1.0f));
            qW[i] = sqrt2 * 0.5f;
            float f3 = 0.5f / sqrt2;
            qW[3] = (ajd.getElement(i3, i2) - ajd.getElement(i2, i3)) * f3;
            qW[i2] = (ajd.getElement(i2, i) + ajd.getElement(i, i2)) * f3;
            qW[i3] = (ajd.getElement(i, i3) + ajd.getElement(i3, i)) * f3;
        }
        quat4f.set(qW[0], qW[1], qW[2], qW[3]);
        bcE.bcO().release(qW);
    }

    /* renamed from: a */
    private static float m38367a(Matrix3fWrap ajd, int i, int i2, int i3, int i4) {
        return (ajd.getElement(i, i2) * ajd.getElement(i3, i4)) - (ajd.getElement(i, i4) * ajd.getElement(i3, i2));
    }

    /* renamed from: c */
    public static void m38378c(Matrix3fWrap ajd) {
        float a = m38367a(ajd, 1, 1, 2, 2);
        float a2 = m38367a(ajd, 1, 2, 2, 0);
        float a3 = m38367a(ajd, 1, 0, 2, 1);
        float f = (ajd.m00 * a) + (ajd.m01 * a2) + (ajd.m02 * a3);
        if (f9033jR || f != 0.0f) {
            float f2 = 1.0f / f;
            float a4 = m38367a(ajd, 0, 2, 2, 1) * f2;
            float a5 = m38367a(ajd, 0, 1, 1, 2) * f2;
            float a6 = m38367a(ajd, 0, 0, 2, 2) * f2;
            float a7 = m38367a(ajd, 0, 2, 1, 0) * f2;
            float a8 = m38367a(ajd, 0, 1, 2, 0) * f2;
            ajd.m00 = a * f2;
            ajd.m01 = a4;
            ajd.m02 = a5;
            ajd.m10 = a2 * f2;
            ajd.m11 = a6;
            ajd.m12 = a7;
            ajd.m20 = a3 * f2;
            ajd.m21 = a8;
            ajd.m22 = f2 * m38367a(ajd, 0, 0, 1, 1);
            return;
        }
        throw new AssertionError();
    }
}
