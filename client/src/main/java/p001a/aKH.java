package p001a;

import logic.WrapRunnable;
import taikodom.addon.hud.monitor.TargetInfoAddon;

/* renamed from: a.aKH */
/* compiled from: a */
class aKH extends WrapRunnable {
    final /* synthetic */ TargetInfoAddon baw;

    public aKH(TargetInfoAddon targetInfoAddon) {
        this.baw = targetInfoAddon;
    }

    public void run() {
        if (!this.baw.aoL.isVisible() || this.baw.f9949kj.getEngineGame() == null || this.baw.f9949kj.getEngineGame().getTaikodom() == null || this.baw.f9949kj.getEngineGame().getTaikodom().getPlayer() == null || this.baw.bOC == null) {
            cancel();
            return;
        }
        this.baw.arm();
        this.baw.arr();
    }
}
