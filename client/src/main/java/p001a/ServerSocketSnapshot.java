package p001a;

import game.network.channel.*;
import game.network.channel.client.ClientConnect;
import game.network.channel.client.ClientConnectBuilder;
import game.network.message.ByteMessage;

import java.io.EOFException;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: a.mC */
/* compiled from: a */
public class ServerSocketSnapshot implements MessageProcessing, ProcesTransportMessage, NetworkChannel {
    /* renamed from: yP */
    private final ClientConnect clientConnect;
    ByteMessage aDz;
    private C2672iO aDx;
    private BlockingQueue<ByteMessage> aDy;

    public ServerSocketSnapshot() {
        this.aDy = new LinkedBlockingQueue();
        this.aDz = new ByteMessage(new byte[1]);
        this.clientConnect = ClientConnectBuilder.build(ClientConnect.Attitude.BROTHER, (InetAddress) null);
    }

    public ServerSocketSnapshot(ClientConnect.Attitude aVar) {
        this.aDy = new LinkedBlockingQueue();
        this.aDz = new ByteMessage(new byte[1]);
        this.clientConnect = ClientConnectBuilder.build(aVar, (InetAddress) null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo20453a(C2672iO iOVar) {
        this.aDx = iOVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo20454c(ClientConnect bVar, byte[] bArr, int i, int i2) {
        this.aDy.add(new ByteMessage(bVar, bArr, i, i2));
    }

    /* renamed from: jH */
    public ClientConnect getClientConnect() {
        return this.clientConnect;
    }

    /* renamed from: jI */
    public MessageProcessing getMessageProcessing() {
        return this;
    }

    /* renamed from: jJ */
    public ProcesTransportMessage getProcessSendMessage() {
        return this;
    }

    /* renamed from: iX */
    public boolean isRunningThreadSelectableChannel() {
        return false;
    }

    /* renamed from: a */
    public void creatListenerChannel(Connect omVar) {
        throw new UnsupportedOperationException();
    }

    public void stopListening() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: Nz */
    public boolean mo20452Nz() {
        return false;
    }

    public void close() {
        if (this.aDx != null) {
            this.aDx.mo19479b(this);
        }
        this.aDy.add(this.aDz);
    }

    /* renamed from: jG */
    public ByteMessage getTransportMessage() {
        try {
            ByteMessage take = this.aDy.take();
            if (take != this.aDz) {
                return take;
            }
            throw new EOFException();
        } catch (InterruptedException e) {
            throw new C2954a(e);
        }
    }

    /* renamed from: b */
    public void sendTransportMessage(byte[] bArr, int i, int i2) {
        if (this.aDx != null) {
            this.aDx.mo19478b(getClientConnect(), bArr, i, i2);
        }
    }

    /* renamed from: a */
    public void sendTransportMessage(ClientConnect bVar, byte[] bArr, int i, int i2) {
        if (bVar == getClientConnect()) {
            mo20454c(bVar, bArr, i, i2);
        } else if (this.aDx != null) {
            this.aDx.mo19476a(getClientConnect(), bVar, bArr, i, i2);
        }
    }

    public boolean isUp() {
        return true;
    }

    /* renamed from: a */
    public Collection<ClientConnect> getClientConnects(ClientConnect.Attitude aVar) {
        HashSet hashSet = new HashSet();
        if (this.aDx == null) {
            return Collections.emptySet();
        }
        for (ClientConnect next : this.aDx.aed.keySet()) {
            if (next.getAttitude().equals(aVar)) {
                hashSet.add(next);
            }
        }
        hashSet.remove(getClientConnect());
        return hashSet;
    }

    /* renamed from: iY */
    public boolean isListenerPort() {
        return true;
    }

    /* renamed from: jK */
    public ClientConnect getClientConnectLocal() {
        return getClientConnect();
    }

    /* renamed from: c */
    public void authorizationChannelClose(ClientConnect bVar) {
    }

    /* renamed from: a */
    public void mo13494a(C5931adH adh) {
    }

    /* renamed from: a */
    public void buildListenerChannel(int listenerPort, int[] iArr, int[] iArr2, boolean z) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: af */
    public void removeChannelAndClose(int i) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: ja */
    public long metricClientTimeAuthorization() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jf */
    public long metricReadByte() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jg */
    public long metricWriteByte() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jc */
    public long metricCountReadMessage() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jb */
    public long metricCountWriteMessage() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: je */
    public long metricByteLength() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jd */
    public long metricAllOriginalByteLength() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jh */
    public InetAddress getIpRemoteHost() {
        return null;
    }

    /* renamed from: a.mC$a */
    public static class C2954a extends IOException {


        public C2954a(InterruptedException interruptedException) {
            super(interruptedException.getMessage());
        }
    }
}
