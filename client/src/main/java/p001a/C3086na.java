package p001a;

import game.engine.DataGameEvent;
import game.network.GameScriptClasses;
import game.network.exception.C2571gy;
import game.network.manager.aHN;
import logic.baa.C1616Xf;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

/* renamed from: a.na */
/* compiled from: a */
public class C3086na extends ObjectInputStream implements aHN {
    public final DataGameEvent aGA;
    private boolean aGB;
    private int aGC;

    public C3086na(DataGameEvent jz, InputStream inputStream) {
        super(inputStream);
        this.aGB = false;
        this.aGC = 255;
        this.aGA = jz;
        enableResolveObject(true);
    }

    public C3086na(DataGameEvent jz, InputStream inputStream, boolean z, int i) {
        this(jz, inputStream);
        this.aGB = z;
        this.aGC = i;
    }

    /* access modifiers changed from: protected */
    public void readStreamHeader() {
    }

    /* access modifiers changed from: protected */
    public Object resolveObject(Object obj) {
        if (obj instanceof GameScriptClasses) {
            return this.aGA.mo3345a((GameScriptClasses) obj);
        }
        return super.resolveObject(obj);
    }

    /* renamed from: PL */
    public C1616Xf mo9183PL() {
        int a = this.aGA.bxy().mo12007a(this);
        if (a == -1) {
            return null;
        }
        long readLong = readLong();
        try {
            return this.aGA.mo3421c(a, readLong);
        } catch (Exception e) {
            throw new C2571gy(e, "Error deserializing game.script: " + readLong);
        }
    }

    /* access modifiers changed from: protected */
    public ObjectStreamClass readClassDescriptor() {
        C6656arE bxy = this.aGA.bxy();
        int a = bxy.mo12007a(this);
        if (a == -1) {
            return super.readClassDescriptor();
        }
        if (a == -2) {
            String readUTF = readUTF();
            if (this.aGB) {
                readUTF = bxy.mo12014jK(readUTF);
            }
            return ObjectStreamClass.lookup(Class.forName(readUTF));
        }
        ObjectStreamClass objectStreamClassFromMagicNumber = bxy.getObjectStreamClassFromMagicNumber(a);
        if (objectStreamClassFromMagicNumber != null) {
            return objectStreamClassFromMagicNumber;
        }
        throw new ClassNotFoundException("failed fetching class descriptor for magic number " + a + " [" + bxy.mo12017tS(a) + "]");
    }

    /* renamed from: PM */
    public DataGameEvent mo9184PM() {
        return this.aGA;
    }

    /* renamed from: PN */
    public int mo9185PN() {
        return this.aGC;
    }
}
