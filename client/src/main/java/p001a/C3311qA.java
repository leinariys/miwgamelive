package p001a;

/* renamed from: a.qA */
/* compiled from: a */
abstract class C3311qA {
    public final C6365alZ aVW;
    final int aVU;
    private final boolean aVV;
    public int aVX = -1;
    public double aVY;
    public int aVZ;

    public C3311qA(boolean z, double d, C6365alZ alz) {
        int i = -1;
        this.aVV = z;
        this.aVU = z ? 1 : i;
        this.aVY = d;
        this.aVW = alz;
    }

    /* renamed from: a */
    public abstract boolean mo728a(C6365alZ alz);

    /* renamed from: Xi */
    public final int mo21297Xi() {
        return this.aVZ + this.aVU;
    }

    /* renamed from: Xj */
    public final boolean mo21298Xj() {
        return this.aVV;
    }

    public String toString() {
        return "[" + (mo21298Xj() ? "S" : "E") + this.aVW.f3320id + " " + this.aVX + " s" + this.aVZ + " P" + this.aVY + "]";
    }
}
