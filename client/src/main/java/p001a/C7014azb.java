package p001a;

import com.hoplon.geometry.Vec3f;

import java.util.List;

/* renamed from: a.azb  reason: case insensitive filesystem */
/* compiled from: a */
public class C7014azb extends C0285Dg {
    public final Vec3f fdB = new Vec3f(0.0f, 0.0f, -10.0f);
    public boolean fdE;
    public C2849lA fdy;

    public C7014azb(C1701ZB zb, C3737uO uOVar, C2849lA lAVar, C6760atE ate) {
        super(zb, uOVar, ate);
        this.fdy = lAVar;
        this.fdE = false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: jc */
    public void mo17225jc(float f) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.czT.size()) {
                C6238ajC c = C6238ajC.m22755c((ayY) this.czT.get(i2));
                if (c != null && !c.cEV() && c.isActive()) {
                    c.bPy();
                    c.mo13934jF(f);
                    c.mo13933jE(f);
                    c.mo13898a(f, c.cFh());
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: jb */
    public void mo17224jb(float f) {
        this.stack.bcJ().push();
        try {
            C3978xf xfVar = (C3978xf) this.stack.bcJ().get();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.czT.size()) {
                    C6238ajC c = C6238ajC.m22755c((ayY) this.czT.get(i2));
                    if (c != null && c.isActive() && !c.cEV()) {
                        c.mo13898a(f, xfVar);
                        c.mo13930h(xfVar);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        } finally {
            this.stack.bcJ().pop();
        }
    }

    /* renamed from: a */
    public int mo1649a(float f, int i, float f2) {
        mo17225jc(f);
        C5408aIe aDq = aDq();
        aDq.aGM = f;
        aDq.dTw = 0;
        aDq.iaO = aDs();
        aDm();
        int bJa = this.czU.bJa();
        if (bJa != 0) {
            List<aGW> bJb = ((C0093BE) this.czU).bJb();
            C5718aUc auc = new C5718aUc();
            auc.aGM = f;
            this.fdy.mo20163n(0, bJa);
            this.fdy.mo20161a((List<ayY>) null, 0, bJb, 0, bJa, (List<C1083Pr>) null, 0, 0, auc, this.f6423Ux, this.czU);
            this.fdy.mo20162a(auc, this.f6423Ux);
        }
        mo17224jb(f);
        aDr();
        bPz();
        aDV();
        return 1;
    }

    public void aDV() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.czT.size()) {
                C6238ajC c = C6238ajC.m22755c((ayY) this.czT.get(i2));
                if (c != null) {
                    c.aDV();
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* renamed from: K */
    public void mo1647K(Vec3f vec3f) {
        this.fdB.set(vec3f);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.czT.size()) {
                C6238ajC c = C6238ajC.m22755c((ayY) this.czT.get(i2));
                if (c != null) {
                    c.mo13893K(vec3f);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    public void mo1653a(C6238ajC ajc) {
        ajc.mo13893K(this.fdB);
        if (ajc.cEZ() != null) {
            mo17706a((ayY) ajc);
        }
    }

    /* renamed from: b */
    public void mo1662b(C6238ajC ajc) {
        mo17719b(ajc);
    }

    public void aDr() {
        this.stack.bcF();
        try {
            C3978xf xfVar = (C3978xf) this.stack.bcJ().get();
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.czT.size()) {
                    ayY ayy = (ayY) this.czT.get(i2);
                    C6238ajC c = C6238ajC.m22755c(ayy);
                    if (c != null && c.isActive() && !c.cEV()) {
                        ayy.cEZ().getAabb(ayy.cFf(), vec3f, vec3f2);
                        aDn().mo7768a(c.cFg(), vec3f, vec3f2, this.czU);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        } finally {
            this.stack.bcG();
        }
    }

    public void bPz() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.czT.size()) {
                C6238ajC c = C6238ajC.m22755c((ayY) this.czT.get(i2));
                if (!(c == null || c.cey() == null || c.cFa() == 2)) {
                    c.cey().mo2961f(c.cFf());
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    public void mo1654a(C2849lA lAVar) {
        this.fdE = false;
        this.fdy = lAVar;
    }

    public C2849lA aDS() {
        return this.fdy;
    }

    public void aDR() {
    }

    public aVG aDU() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
