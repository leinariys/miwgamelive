package p001a;

import java.io.IOException;

/* renamed from: a.asP  reason: case insensitive filesystem */
/* compiled from: a */
public class C6719asP extends IOException {
    private final String name;

    public C6719asP(String str) {
        this.name = str;
    }

    public String getName() {
        return this.name;
    }
}
