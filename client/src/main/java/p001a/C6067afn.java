package p001a;

import game.network.WhoAmI;
import game.network.channel.client.ClientConnect;
import game.network.message.externalizable.C5694aTe;
import game.network.message.externalizable.C6302akO;
import logic.res.code.DataGameEventImpl;

/* renamed from: a.afn  reason: case insensitive filesystem */
/* compiled from: a */
class C6067afn extends axB<C5694aTe> {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ DataGameEventImpl heo;

    C6067afn(DataGameEventImpl arVar) {
        this.heo = arVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C6302akO mo2392b(ClientConnect bVar, C5694aTe ate) {
        if (this.heo.getWhoAmI() != WhoAmI.CLIENT) {
            return null;
        }
        if (this.heo.bGw()) {
            this.heo.mo3438f((Runnable) new C1882a(ate));
            return null;
        }
        this.heo.ald().getEventManager().mo13975h(ate.dvz());
        return null;
    }

    /* renamed from: a.afn$a */
    class C1882a implements Runnable {
        private final /* synthetic */ C5694aTe fuh;

        C1882a(C5694aTe ate) {
            this.fuh = ate;
        }

        public void run() {
            if (this.fuh.getStr() == null || this.fuh.getStr().length() <= 0) {
                C6067afn.this.heo.ald().getEventManager().mo13975h(this.fuh.dvz());
            } else {
                C6067afn.this.heo.ald().getEventManager().mo13972d(this.fuh.getStr(), this.fuh.dvz());
            }
        }
    }
}
