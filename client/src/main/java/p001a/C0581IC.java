package p001a;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.IC */
/* compiled from: a */
public final class C0581IC {
    public static final C0581IC dfJ = new C0581IC("GrannyStandardMainSection", grannyJNI.GrannyStandardMainSection_get());
    public static final C0581IC dfK = new C0581IC("GrannyStandardRigidVertexSection", grannyJNI.GrannyStandardRigidVertexSection_get());
    public static final C0581IC dfL = new C0581IC("GrannyStandardRigidIndexSection", grannyJNI.GrannyStandardRigidIndexSection_get());
    public static final C0581IC dfM = new C0581IC("GrannyStandardDeformableVertexSection", grannyJNI.GrannyStandardDeformableVertexSection_get());
    public static final C0581IC dfN = new C0581IC("GrannyStandardDeformableIndexSection", grannyJNI.GrannyStandardDeformableIndexSection_get());
    public static final C0581IC dfO = new C0581IC("GrannyStandardTextureSection", grannyJNI.GrannyStandardTextureSection_get());
    public static final C0581IC dfP = new C0581IC("GrannyStandardDiscardableSection", grannyJNI.GrannyStandardDiscardableSection_get());
    public static final C0581IC dfQ = new C0581IC("GrannyStandardUnloadedSection", grannyJNI.GrannyStandardUnloadedSection_get());
    public static final C0581IC dfR = new C0581IC("GrannyStandardSectionCount");
    private static C0581IC[] dfS = {dfJ, dfK, dfL, dfM, dfN, dfO, dfP, dfQ, dfR};

    /* renamed from: pF */
    private static int f699pF = 0;

    /* renamed from: pG */
    private final int f700pG;

    /* renamed from: pH */
    private final String f701pH;

    private C0581IC(String str) {
        this.f701pH = str;
        int i = f699pF;
        f699pF = i + 1;
        this.f700pG = i;
    }

    private C0581IC(String str, int i) {
        this.f701pH = str;
        this.f700pG = i;
        f699pF = i + 1;
    }

    private C0581IC(String str, C0581IC ic) {
        this.f701pH = str;
        this.f700pG = ic.f700pG;
        f699pF = this.f700pG + 1;
    }

    /* renamed from: kU */
    public static C0581IC m5324kU(int i) {
        if (i < dfS.length && i >= 0 && dfS[i].f700pG == i) {
            return dfS[i];
        }
        for (int i2 = 0; i2 < dfS.length; i2++) {
            if (dfS[i2].f700pG == i) {
                return dfS[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C0581IC.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f700pG;
    }

    public String toString() {
        return this.f701pH;
    }
}
