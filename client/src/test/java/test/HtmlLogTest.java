package test;

import logic.res.code.DataGameEventImpl;
import logic.res.html.C2690id;
import logic.res.html.ThreadTimestamp;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

/* renamed from: b */
public class HtmlLogTest {
    public static void main(String[] strArr) throws IOException {
        int i = 1;
        C2690id idVar = new C2690id();
        DataGameEventImpl.bxD();
        if (strArr.length == 0) {
            strArr = new String[]{"../utaikodom.game/calls.html", "../utaikodom.game/client-calls.log", "../utaikodom.game/server-calls.log"};
        }
        while (true) {
            int i2 = i;
            if (i2 >= strArr.length) {
                idVar.mo19759aB(strArr[0]);
                System.out.println("file written");
                return;
            }
            String str = strArr[i2];
            try {
                System.out.println("loading " + str);
                ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(str));
                idVar.mo19758a((List<ThreadTimestamp>) objectInputStream.readObject(), (String) objectInputStream.readObject());
                System.out.println(" done");
            } catch (Exception e) {
                System.err.println(e.toString());
            }
            i = i2 + 1;
        }
    }
}
