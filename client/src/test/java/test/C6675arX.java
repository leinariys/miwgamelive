package test;

import game.engine.DataGameEvent;
import game.engine.SocketMessage;
import game.network.WhoAmI;
import game.network.manager.C0387FM;
import game.network.manager.ContainerBinaryData;
import game.network.message.ByteMessageReader;
import game.script.Taikodom;
import logic.data.mbean.C0677Jd;
import logic.data.mbean.C6064afk;
import logic.data.mbean.RunTheUnsafe;
import logic.res.FileControl;
import logic.res.code.C5663aRz;
import logic.res.code.DataGameEventImpl;
import logic.res.code.ResourceDeobfuscation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import p001a.C3582se;
import p001a.ServerSocketSnapshot;
import p001a.WraperDbFile;
import p001a.WraperDbFileImpl;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.arX  reason: case insensitive filesystem */
/* compiled from: a */
public class C6675arX {


    public static void main(String[] strArr) {
        C6675arX arx = new C6675arX();
        arx.ctv();
        try {
            arx.ctw();
            arx.ctx();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e2) {
            e2.printStackTrace();
        }
    }
    @Before
    public void ctv() {
        FileControl avr = new FileControl("./data/environment.db");
        FileControl avr2 = new FileControl("./data/environment.lg");
        if (avr.exists()) {
            FileControl avr3 = new FileControl("./data/test/");
            FileControl avr4 = new FileControl("./data/test/environment.db");
            if (!avr3.exists()) {
                avr3.mo2251BD();
            }
            avr4.mo2260d(avr.mo2255BH());
        }
        if (avr2.exists()) {
            new FileControl("./data/test/environment.lg").mo2260d(avr2.mo2255BH());
        }
    }

    @Test
    public void ctw() {
        DataGameEvent cty = cty();
        for (C3582se next : cty.bGA()) {
            C6064afk dq = next.mo5608dq();
            if (dq instanceof C0677Jd) {
                SocketMessage ala = new SocketMessage();
                C0387FM fm = new C0387FM(cty, ala, false, true, 0, false);
                C0677Jd jd = (C0677Jd) dq;
                jd.mo3186b((ObjectOutput) fm, 0);
                fm.flush();
                ContainerBinaryData aqq = new ContainerBinaryData(cty, new ByteMessageReader(ala.toByteArray()), false, true, 0);
                C0677Jd jd2 = (C0677Jd) next.mo6901yn().mo13W();
                jd2.mo3158a((ObjectInput) aqq, 0);
                for (C5663aRz arz : jd.mo11944yn().mo25c()) {
                    if (!arz.mo7393ho() && !arz.mo7389hk()) {
                        Assert.assertEquals(jd.mo3199g(arz), jd2.mo3199g(arz));
                    }
                }
            }
        }
    }

    @Test
    public void ctx() {
        DataGameEvent cty = cty();
        for (C3582se next : cty.bGA()) {
            C6064afk dq = next.mo5608dq();
            if (dq instanceof C0677Jd) {
                SocketMessage ala = new SocketMessage();
                C0387FM fm = new C0387FM(cty, ala, false, true, 0, false);
                C0677Jd jd = (C0677Jd) dq;
                jd.mo3186b((ObjectOutput) fm, 0);
                fm.flush();
                C0677Jd jd2 = (C0677Jd) dq.mo11944yn().mo13W();
                jd2.mo3154a((RunTheUnsafe) dq);
                jd2.setMode(1);
                jd2.mo3146A(false);
                for (C5663aRz arz : jd.mo309c()) {
                    switch (arz.mo11290ED()) {
                        case 'B':
                            jd2.mo3195e(arz, 255);
                            break;
                        case 'C':
                            jd2.mo3195e(arz, 'c');
                            break;
                        case 'D':
                            jd2.mo3195e(arz, Double.valueOf(1.0d));
                            break;
                        case 'F':
                            jd2.mo3195e(arz, Float.valueOf(1.0f));
                            break;
                        case 'I':
                            jd2.mo3195e(arz, 1);
                            break;
                        case 'J':
                            jd2.mo3195e(arz, 1L);
                            break;
                        case 'S':
                            jd2.mo3195e(arz, new Short("1"));
                            break;
                        case 'Z':
                            jd2.mo3195e(arz, true);
                            break;
                    }
                }
                jd2.mo3160a(dq, dq.aBt() + 1);
                jd2.mo3159a((ObjectOutput) fm, 38);
                fm.flush();
                ContainerBinaryData aqq = new ContainerBinaryData(cty, new ByteMessageReader(ala.toByteArray()), false, true, 0);
                C0677Jd jd3 = (C0677Jd) next.mo6901yn().mo13W();
                jd3.mo3158a((ObjectInput) aqq, 0);
                C0677Jd jd4 = (C0677Jd) jd3.mo11944yn().mo13W();
                jd4.mo3146A(false);
                jd4.mo3204i((ObjectInput) aqq);
                for (C5663aRz arz2 : jd2.mo11944yn().mo25c()) {
                    if (!arz2.mo7393ho() && !arz2.mo7389hk() && arz2.mo11290ED() != '[' && arz2.mo11290ED() != 'L') {
                        Assert.assertEquals(jd2.mo3199g(arz2), jd4.mo3199g(arz2));
                    }
                }
            }
        }
    }

    private DataGameEvent cty() {
        ResourceDeobfuscation sh = new ResourceDeobfuscation();
        sh.setWhoAmI(WhoAmI.SERVER);
        sh.setServerSocket(new ServerSocketSnapshot());
        sh.setClassObject(Taikodom.class);
        sh.mo13373ew(true);
        sh.mo13371eu(true);
        sh.mo13372ev(true);
        sh.setResursData(new FileControl("data/test/"));
        WraperDbFileImpl abr = new WraperDbFileImpl();
        abr.setDbname("environment");
        sh.setWraperDbFile((WraperDbFile) abr);
        return new DataGameEventImpl(sh);
    }
}
