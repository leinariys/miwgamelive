package test;

import game.engine.DataGameEvent;
import game.network.WhoAmI;
import game.network.build.ManagerMessageTransportImpl;
import game.network.message.ByteMessageReader;
import game.network.thread.TransportThreadImpl;
import game.script.Taikodom;
import logic.res.FileControl;
import logic.res.code.DataGameEventImpl;
import logic.res.code.ResourceDeobfuscation;
import org.junit.Test;
import p001a.ByteMessageReaderLogger;
import p001a.ServerSocketSnapshot;
import p001a.WraperDbFile;
import p001a.WraperDbFileImpl;

/* renamed from: a.ajG  reason: case insensitive filesystem */
/* compiled from: a */
public class ManagerMessageTransportTest {
    public static void main(String[] strArr) {
        new ManagerMessageTransportTest().ceF();
    }

    @Test
    public void ceF() {
        DataGameEventImpl.bxC();
        DataGameEvent ja = m22794ja("environment");
        ByteMessageReaderLogger amu = new ByteMessageReaderLogger(mo13962jb("14 05 93 a4 20 03 77 22 6b 3a 00 03 04 02 08 3503 1f 51 36 01 1c d5 5a 43 71 01 ".replaceAll(" ", "")));
        System.out.println(amu.toString());
        try {
            System.out.println(ManagerMessageTransportImpl.messageTransformation(ja, TransportThreadImpl.buildMessageTransport((ByteMessageReader) amu)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* renamed from: ja */
    private DataGameEvent m22794ja(String str) {
        ResourceDeobfuscation sh = new ResourceDeobfuscation();
        sh.setWhoAmI(WhoAmI.SERVER);
        sh.setServerSocket(new ServerSocketSnapshot());
        sh.setClassObject(Taikodom.class);
        sh.mo13373ew(true);
        sh.mo13371eu(true);
        sh.mo13372ev(true);
        sh.setResursData(new FileControl("data/"));
        WraperDbFileImpl abr = new WraperDbFileImpl();
        abr.setDbname(str);
        sh.setWraperDbFile((WraperDbFile) abr);
        return new DataGameEventImpl(sh);
    }

    /* access modifiers changed from: protected */
    /* renamed from: jb */
    public byte[] mo13962jb(String str) {
        byte[] bArr = new byte[(str.length() / 2)];
        int i = 0;
        int i2 = 0;
        while (i2 < str.length()) {
            bArr[i] = (byte) Integer.parseInt(str.substring(i2, i2 + 2), 16);
            i2 += 2;
            i++;
        }
        return bArr;
    }
}
