package test;

import p001a.C1024Ow;
import p001a.C6380alo;
import taikodom.render.graphics2d.C0559Hm;

import java.io.File;
import java.io.FilenameFilter;
import java.security.ProtectionDomain;
import java.util.List;

/* renamed from: test.a.Kj */
/* compiled from: test.a */
public class C0750Kj {
    C1024Ow dpx = new C1024Ow();

    public static void main(String[] strArr) {
        System.out.println("Input: " + strArr[0]);
        System.out.println("Output: " + strArr[1]);
        new C0750Kj().m6607a(strArr[0], strArr[1], bcz());
        System.out.println("Done!");
    }

    public static C6380alo.C1937a bcz() {
        String property = System.getProperty("instrumentation.target");
        if (property != null) {
            return C6380alo.C1937a.valueOf(property.toUpperCase());
        }
        throw new IllegalArgumentException("Missing mandatory property: instrumentation.target");
    }

    /* renamed from: test.a */
    private void m6607a(String str, String str2, C6380alo.C1937a aVar) {
        File file = new File(str);
        File file2 = new File(str2);
        this.dpx.mo4566a(aVar);
        this.dpx.mo4574h(file2);
        m6608b(file, file2);
    }

    /* renamed from: b */
    private void m6608b(File file, File file2) {
        int i;
        m6609d(file2);
        List<File> b = C0559Hm.m5256b(file, (FilenameFilter) new C0751a());
        int length = file.getAbsolutePath().length();
        if (!file.getAbsolutePath().endsWith(File.separator)) {
            i = length + 1;
        } else {
            i = length;
        }
        m6609d(file2);
        C0559Hm.m5252a(file2, true);
        System.out.println("Instrumenting " + file + ", generating at " + file2);
        for (File next : b) {
            String substring = next.getAbsolutePath().substring(i);
            File file3 = new File(file2, substring);
            if (substring.endsWith(".class")) {
                m6606a(substring.replace(File.separatorChar, '.').substring(0, substring.length() - ".class".length()), next, file3);
            } else {
                m6605a(next, file3);
            }
        }
    }

    /* renamed from: d */
    private void m6609d(File file) {
        System.out.println("Cleaning up " + file);
        C0559Hm.m5252a(file, true);
    }

    /* renamed from: test.a */
    private void m6606a(String str, File file, File file2) {
        if (!file2.getParentFile().exists()) {
            file2.getParentFile().mkdirs();
        }
        byte[] transform = this.dpx.transform(getClass().getClassLoader(), str, (Class<?>) null, (ProtectionDomain) null, C0559Hm.m5264q(file));
        if (transform != null) {
            System.out.println("Instrumenting " + str);
            C0559Hm.m5248a(file2, transform);
            return;
        }
        m6605a(file, file2);
    }

    /* renamed from: test.a */
    private void m6605a(File file, File file2) {
        System.out.println("Copying " + file + " to " + file2);
        C0559Hm.m5250a(file, file2, true);
    }

    /* renamed from: test.a.Kj$test.a */
    class C0751a implements FilenameFilter {
        C0751a() {
        }

        public boolean accept(File file, String str) {
            return true;
        }
    }
}
