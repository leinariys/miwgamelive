package test;

import game.script.ai.npc.evolving.EvolvingDroneAIController;
import p001a.C2971mQ;
import p001a.C3255ph;
import p001a.C6712asI;

import java.util.HashMap;
import java.util.List;

/* renamed from: e */
/* compiled from: test.a */
public class C4146e {
    public static void main(String[] strArr) {
        EvolvingDroneAIController.bkT();
        HashMap<C2971mQ, C6712asI> hashMap = EvolvingDroneAIController.dIt;
        for (C2971mQ put : hashMap.keySet()) {
            hashMap.put(put, new C6712asI());
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= 50) {
                break;
            }
            List<T> H = C3255ph.m37270H(hashMap);
            hashMap.clear();
            System.out.println("New Gen:");
            System.out.println();
            for (T t : H) {
                hashMap.put(t, new C6712asI());
                System.out.println(t);
            }
            System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
            i = i2 + 1;
        }
        List<T> H2 = C3255ph.m37270H(hashMap);
        System.out.println("LastGen:");
        for (T println : H2) {
            System.out.println(println);
        }
    }
}
