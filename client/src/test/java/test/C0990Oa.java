package test;

import game.script.Character;
import util.Syst;

import javax.swing.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/* renamed from: test.a.Oa */
/* compiled from: test.a */
public class C0990Oa {
    public static void main(String[] strArr) {
        Class<Spring> cls = Spring.class;
        for (Field field : cls.getFields()) {
            if (!Modifier.isStatic(field.getModifiers())) {
                String name = field.getName();
                String a = m8092a(field.getType(), name, "c." + name);
                if (a != null) {
                    System.out.println("\t\tc." + name + " = " + a + ";");
                } else {
                    System.out.println("\t\t// TODO: " + cls.getName() + " " + className(field.getType()) + " " + name);
                }
            }
        }
        for (Method method : cls.getMethods()) {
            String name2 = method.getName();
            if (method.getParameterTypes() != null && method.getParameterTypes().length == 1 && name2.startsWith("set")) {
                String substring = name2.substring(3);
                if (Character.isUpperCase(substring.charAt(0))) {
                    String u = Syst.m15909u(substring);
                    String a2 = m8092a(method.getParameterTypes()[0], u, "c." + method.getName().replaceAll("^s", "g") + "()");
                    if (a2 != null) {
                        System.out.println("\t\tc." + method.getName() + "( " + a2 + ");");
                    } else {
                        System.out.println("\t\t// TODO: " + cls.getName() + " " + className(method.getParameterTypes()[0]) + " " + u);
                    }
                }
            }
        }
    }

    /* renamed from: test.a */
    public static String m8092a(Class<?> cls, String str, String str2) {
        if (cls == String.class) {
            return "getStringProp(sd, \"" + str + "\", " + str2 + ")";
        }
        if (cls == Integer.TYPE) {
            return "getIntProp(sd, \"" + str + "\", " + str2 + ")";
        }
        if (cls == Float.TYPE) {
            return "getNumberProp(sd, \"" + str + "\", " + str2 + ")";
        }
        if (cls == Boolean.TYPE) {
            return "getBooleanProp(sd, \"" + str + "\", " + str2 + ")";
        }
        if (cls == Double.TYPE) {
            return "getNumberProp(sd, \"" + str + "\", (float)" + str2 + ")";
        }
        if (cls == double[].class) {
            return "getDoubleArrayProp(sd, \"" + str + "\", " + str2 + ")";
        }
        if (cls == int[].class) {
            return "getIntArrayProp(sd, \"" + str + "\", " + str2 + ")";
        }
        if (cls.isEnum()) {
            return "(" + cls.getName() + ") getIntArrayProp(sd, " + cls.getName() + ".class, \"" + str + "\", " + str2 + ")";
        }
        return null;
    }

    public static String className(Class<?> cls) {
        if (cls.isArray()) {
            return String.valueOf(className(cls.getComponentType())) + "[]";
        }
        if (cls.isPrimitive()) {
            if (cls == Double.TYPE) {
                return "double";
            }
            if (cls == Integer.TYPE) {
                return "int";
            }
            if (cls == Float.TYPE) {
                return "float";
            }
        }
        return cls.getName();
    }
}
