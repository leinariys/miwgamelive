package test;

import logic.render.QueueItem;
import logic.swing.BasicLookAndFeelSpaceEngine;
import logic.swing.IComponentUi;
import logic.ui.item.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.je */
/* compiled from: a */
public class C2760je {
    private static Field ahw;

    static {
        try {
            ahw = JComponent.class.getDeclaredField("ui");
            ahw.setAccessible(true);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e2) {
            e2.printStackTrace();
        }
    }

    List<QueueItem<String, Exception>> ahu = new ArrayList();
    List<AssertionError> ahv = new ArrayList();

    /* renamed from: c */
    public static ComponentUI m34085c(Component component) {
        try {
            if (component instanceof JComponent) {
                return (ComponentUI) ahw.get(component);
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    @Before
    public void setUp() throws UnsupportedLookAndFeelException {
        this.ahu.clear();
        this.ahv.clear();
        UIManager.setLookAndFeel(new BasicLookAndFeelSpaceEngine());
    }

    @After
    public void tearDown() {
        if (this.ahu.size() > 0) {
            throw new RuntimeException(String.valueOf(this.ahu.toString()) + " " + this.ahv.toString(), (Throwable) this.ahu.get(0).getLast());
        }
        if (this.ahv.size() > 0) {
            for (AssertionError println : this.ahv) {
                System.out.println(println);
            }
            Assert.fail(this.ahv.toString());
        }
        this.ahu.clear();
        this.ahv.clear();
    }

    /* renamed from: EJ */
    public List<Component> mo19973EJ() {
        ArrayList arrayList = new ArrayList();
        JLabel jLabel = new JLabel();
        arrayList.add(jLabel);
        jLabel.setText("label");
        JButton jButton = new JButton();
        arrayList.add(jButton);
        jButton.setText("button");
        arrayList.add(new JSplitPane());
        arrayList.add(new JPanel());
        arrayList.add(new JTable(new Object[][]{new Object[]{"teste1", "teste1"}, new Object[]{"teste1", "teste1"}}, new Object[]{"col 1", "col 2"}));
        arrayList.add(new JComboBox());
        arrayList.add(new JPopupMenu());
        arrayList.add(new JProgressBar());
        JInternalFrame jInternalFrame = new JInternalFrame();
        arrayList.add(jInternalFrame);
        jInternalFrame.setTitle("internalFrame");
        InternalFrame nxVar = new InternalFrame();
        arrayList.add(nxVar);
        nxVar.setTitle("taikodomWindow");
        JCheckBox jCheckBox = new JCheckBox();
        arrayList.add(jCheckBox);
        jCheckBox.setText("checkBox");
        arrayList.add(new JScrollPane());
        arrayList.add(new JScrollBar());
        JTextField jTextField = new JTextField();
        arrayList.add(jTextField);
        jTextField.setText("textField");
        JTextArea jTextArea = new JTextArea();
        arrayList.add(jTextArea);
        jTextArea.setText("textArea");
        arrayList.add(new VBox());
        arrayList.add(new HBox());
        arrayList.add(new StackJ());
        arrayList.add(new FBox());
        arrayList.add(new GBox());
        JPasswordField jPasswordField = new JPasswordField();
        arrayList.add(jPasswordField);
        jPasswordField.setText("passwordField");
        JFormattedTextField jFormattedTextField = new JFormattedTextField();
        arrayList.add(jFormattedTextField);
        jFormattedTextField.setText("formattedTextField");
        return arrayList;
    }

    @Test
    /* renamed from: EK */
    public void mo19974EK() throws UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel(new BasicLookAndFeelSpaceEngine());
        for (Component next : mo19973EJ()) {
            ComponentUI c = m34085c(next);
            if (c == null) {
                Assert.fail("No ui for " + next.getClass());
            }
            if (!(c instanceof IComponentUi)) {
                Assert.fail(c.getClass() + " does not implements TaikodomUI");
            }
        }
    }

    @Test
    /* renamed from: EL */
    public void mo19975EL() {
        BufferedImage bufferedImage = new BufferedImage(128, 64, 2);
        Graphics graphics = bufferedImage.getGraphics();
        File file = new File("test-output");
        file.mkdirs();
        C2761a aVar = new C2761a();
        aVar.setFont(new Font("SansSerif", 0, 12));
        for (Component next : mo19973EJ()) {
            try {
                aVar.add(next);
                graphics.setColor(new Color(255, 255, 255, 255));
                graphics.fillRect(0, 0, 128, 64);
                next.setSize(new Dimension(128, 64));
                next.paint(graphics);
                ImageIO.write(bufferedImage, "png", new FileOutputStream(new File(file, next.getClass() + ".png")));
            } catch (Exception e) {
                this.ahu.add(new QueueItem("Error painting " + next.getClass().getName() + " " + e.getMessage(), e));
            }
        }
    }

    @Test
    /* renamed from: EM */
    public void mo19976EM() {
        for (Component next : mo19973EJ()) {
            try {
                Dimension preferredSize = next.getPreferredSize();
                Assert.assertNotNull("Null preferred size for " + next.getClass(), preferredSize);
                if (!(next instanceof C2867lF)) {
                    Assert.assertFalse("Zero preferred size for " + next.getClass(), preferredSize.height == 0 && preferredSize.width == 0);
                }
            } catch (Exception e) {
                this.ahu.add(new QueueItem("Error calculating preferred size of " + next.getClass().getName() + " " + e.getMessage(), e));
            } catch (AssertionError e2) {
                this.ahv.add(e2);
            }
        }
    }

    @Test
    /* renamed from: EN */
    public void mo19977EN() {
        for (Component next : mo19973EJ()) {
            try {
                Assert.assertNotNull("Null minimum size for " + next.getClass(), next.getMinimumSize());
            } catch (Exception e) {
                this.ahu.add(new QueueItem("Error calculating minimum size of " + next.getClass().getName() + " " + e.getMessage(), e));
            } catch (AssertionError e2) {
                this.ahv.add(e2);
            }
        }
    }

    @Test
    /* renamed from: EO */
    public void mo19978EO() {
        for (Component next : mo19973EJ()) {
            try {
                Assert.assertNotNull("Null maximum size for " + next.getClass(), next.getMinimumSize());
            } catch (Exception e) {
                this.ahu.add(new QueueItem("Error calculating maximum size of " + next.getClass().getName() + " " + e.getMessage(), e));
            } catch (AssertionError e2) {
                this.ahv.add(e2);
            }
        }
    }

    /* renamed from: a.je$a */
    class C2761a extends JPanel {
        C2761a() {
        }

        public boolean isVisible() {
            return true;
        }
    }
}
