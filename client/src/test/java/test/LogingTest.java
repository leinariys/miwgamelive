package test;

import game.network.WhoAmI;
import game.network.exception.ClientAlreadyConnectedException;
import game.script.BotUtils;
import game.script.Taikodom;
import logic.res.FileControl;
import logic.res.LoaderTrail;
import logic.res.code.*;
import logic.thred.LogPrinter;
import logic.thred.WatchDog;
import taikodom.addon.neo.preferences.GUIPrefAddon;
import taikodom.render.loader.provider.FilePath;

import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.Executor;

/* renamed from: a.asr  reason: case insensitive filesystem */
/* compiled from: a */
public class LogingTest {
    public static void main(String[] strArr) {
        m25779a(new FileControl("."), new FileControl(System.getProperty("gametoolkit.client.render.rootpath", ".")));
    }

    /* renamed from: a */
    public static void m25779a(FilePath ain, FilePath ain2) {
        int i = 0;
        WatchDog akc = new WatchDog();
        try {
            akc.setPrintWriter(LogPrinter.createLogFile("bot-watchdog", "log"));
            akc.start();
            ResourceDeobfuscation sh = new ResourceDeobfuscation();
            LoaderTrail glVar = new LoaderTrail(new C3638tD());
            glVar.mo19096a((C1625Xn) new C1991b());
            glVar.setExecutor(new C1990a());
            SoftTimer abg = new SoftTimer();
            int i2 = 1;
            while (true) {
                try {
                    i = i2;
                    C6236ajA aja = new C6236ajA("teste" + i, "teste" + i, glVar, abg, ain, ain2);
                    sh.setServerSocket(aja.aPT());
                    sh.setWhoAmI(WhoAmI.CLIENT);
                    sh.setClassObject(Taikodom.class);
                    sh.mo13372ev(false);
                    sh.mo13373ew(false);
                    DataGameEventImpl arVar = new DataGameEventImpl(sh);
                    aja.mo13882a(arVar);
                    BotUtils aMa = arVar.ala().aMa();
                    int parseInt = Integer.parseInt(System.getProperty("bots.count", "30"));
                    String str = "botx" + C0395FU.m3225e((long) aMa.drj(), 2) + GUIPrefAddon.C4817c.X;
                    aMa.mo11178h(str, 0, parseInt);
                    ArrayList arrayList = new ArrayList();
                    FilePath aC = ain.concat("bots/flying-crazy.js");
                    arrayList.add(aja);
                    aja.mo13883a((Reader) new InputStreamReader(aC.openInputStream()));
                    for (int i3 = 1; i3 < parseInt; i3++) {
                        String str2 = String.valueOf(str) + C0395FU.m3225e((long) i3, 3);
                        System.out.println("creating connection for bot : " + str2);
                        try {
                            C6236ajA aja2 = new C6236ajA(arVar, str2, str2, glVar, abg, ain, ain2);
                            arrayList.add(aja2);
                            aja2.mo13883a((Reader) new InputStreamReader(aC.openInputStream()));
                        } catch (ClientAlreadyConnectedException e) {
                            System.out.println("bot account " + str2 + " already logged in");
                        }
                    }
                    System.gc();
                    System.gc();
                    System.gc();
                    long lastModified = aC.lastModified();
                    C6145ahN ahn = new C6145ahN(10.0f);
                    while (!arrayList.isEmpty()) {
                        long caT = ahn.caT();
                        long lastModified2 = aC.lastModified();
                        boolean z = lastModified != lastModified2;
                        aja.cAG();
                        abg.mo7965jB(caT);
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            C6236ajA aja3 = (C6236ajA) it.next();
                            if (z) {
                                aja3.mo13889kf();
                                aja3.mo13883a((Reader) new InputStreamReader(aC.openInputStream()));
                            }
                            if (!aja3.step(((float) caT) / 1000.0f)) {
                                System.out.println("---------------------------------> removing");
                                it.remove();
                            }
                        }
                        glVar.mo19099gZ();
                        glVar.mo5008vu();
                        lastModified = lastModified2;
                    }
                    return;
                } catch (ClientAlreadyConnectedException e2) {
                    i2 = i + 1;
                    if (i > 30) {
                        throw new RuntimeException("We are out of test accounts...., Sir.");
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (UnsupportedEncodingException | FileNotFoundException e3) {
            throw new RuntimeException(e3);
        }
    }

    /* renamed from: a.asr$b */
    /* compiled from: a */
    static class C1991b implements C1625Xn {
        C1991b() {
        }

        public void brK() throws InterruptedException {
            throw new InterruptedException();
        }

        public boolean brL() {
            return false;
        }
    }

    /* renamed from: a.asr$a */
    static class C1990a implements Executor {
        C1990a() {
        }

        public void execute(Runnable runnable) {
            runnable.run();
        }
    }
}
