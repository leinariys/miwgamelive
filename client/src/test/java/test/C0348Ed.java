package test;

import com.hoplon.geometry.Vec3f;
import logic.bbb.C5947adX;
import p001a.C1422Up;
import p001a.C3381qt;
import p001a.C4092zL;
import p001a.C5780aaM;

import javax.vecmath.Vector3f;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.Ed */
/* compiled from: a */
public class C0348Ed {
    private static final int cRb = 256;
    private static final float cRc = 0.001f;
    private final C5780aaM aAn;
    private final Vec3f aAq;
    private final C3381qt aVP;
    private final int[][] cRd;
    private final int[][] cRe;
    QuickHull3DTest cRm;
    /* renamed from: Ok */
    private float f504Ok;
    private List<C4092zL> cRf;
    private C4092zL cRg;
    private Vec3f cRh;
    private Vec3f cRi;
    private boolean cRj;
    private C4092zL cRk;
    private C4092zL cRl;
    private C1422Up cRn;

    public C0348Ed(C5947adX adx, QuickHull3DTest hlVar) {
        this(adx);
        this.cRm = hlVar;
        hlVar = hlVar == null ? new QuickHull3DTest() : hlVar;
        aNK();
        hlVar.setVisible(true);
    }

    public C0348Ed(C5947adX adx) {
        int[] iArr = new int[3];
        iArr[1] = 1;
        iArr[2] = 2;
        int[] iArr2 = new int[3];
        iArr2[1] = 2;
        iArr2[2] = 3;
        int[] iArr3 = new int[3];
        iArr3[1] = 3;
        iArr3[2] = 1;
        this.cRd = new int[][]{iArr, new int[]{1, 3, 2}, iArr2, iArr3};
        int[] iArr4 = new int[4];
        iArr4[1] = 3;
        iArr4[3] = 2;
        int[] iArr5 = new int[4];
        iArr5[1] = 1;
        iArr5[2] = 1;
        iArr5[3] = 2;
        int[] iArr6 = new int[4];
        iArr6[1] = 2;
        iArr6[2] = 2;
        int[] iArr7 = new int[4];
        iArr7[0] = 1;
        iArr7[1] = 3;
        iArr7[3] = 1;
        int[] iArr8 = new int[4];
        iArr8[0] = 3;
        iArr8[1] = 2;
        iArr8[3] = 2;
        this.cRe = new int[][]{iArr4, iArr5, iArr6, iArr7, new int[]{2, 1, 1, 1}, iArr8};
        this.cRg = null;
        this.cRh = new Vec3f();
        this.cRi = new Vec3f();
        this.cRj = false;
        this.aAq = new Vec3f();
        this.cRk = null;
        this.cRl = null;
        this.f504Ok = Float.NEGATIVE_INFINITY;
        this.aVP = adx.bTp();
        if (this.aVP.getSize() != 4) {
            throw new IllegalStateException("Simplex must be a tetrahedron for EPA (no support for other cases)");
        }
        this.aAn = adx.mo12880ME();
        aNH();
    }

    public boolean aNB() {
        this.cRn = new C1422Up();
        int i = 0;
        while (true) {
            if (i >= 256) {
                break;
            }
            C4092zL aNI = aNI();
            if (aNI != null) {
                this.aAq.set(aNI.mo23296vO());
                this.aAq.negate();
                this.cRn.setDirection(this.aAq);
                this.aAn.getFarthest(this.cRn.getDirection(), this.cRn.bxH());
                float dot = aNI.mo23296vO().dot(this.cRn.bxH()) + aNI.asz();
                this.cRg = aNI;
                m2993M(aNI.mo23296vO());
                if (!m2992L(this.cRn.bxH()) && dot > cRc) {
                    aNI.mo23290bK(true);
                    m2995a(aNI);
                    this.cRk = null;
                    this.cRl = null;
                    int i2 = 0;
                    for (int i3 = 0; i3 < 3; i3++) {
                        i2 += m2994a(aNI.mo23291fM(i3), aNI.mo23292fN(i3), this.cRn);
                    }
                    this.cRm.mo19363yj();
                    aNK();
                    m2993M((Vec3f) null);
                    if (i2 <= 2) {
                        break;
                    }
                    C4092zL.m41527a(this.cRl, 2, this.cRk, 1);
                    i++;
                } else {
                    this.cRm.mo19363yj();
                    aNK();
                    m2993M((Vec3f) null);
                    this.cRj = true;
                    this.f504Ok = Math.max(0.0f, this.cRg.asz());
                    aNJ();
                }
            }
        }
        return this.cRj;
    }

    public float aNC() {
        return this.f504Ok;
    }

    public C4092zL aND() {
        return this.cRg;
    }

    public Vec3f aNE() {
        return this.cRh;
    }

    public Vec3f aNF() {
        return this.cRi;
    }

    public boolean aNG() {
        return this.cRj;
    }

    private void aNH() {
        this.cRf = new ArrayList();
        for (int i = 0; i < this.cRd.length; i++) {
            this.cRf.add(new C4092zL(new C1422Up(this.aVP.mo21529oJ(this.cRd[i][0])), new C1422Up(this.aVP.mo21529oJ(this.cRd[i][1])), new C1422Up(this.aVP.mo21529oJ(this.cRd[i][2]))));
        }
        for (int i2 = 0; i2 < this.cRe.length; i2++) {
            C4092zL.m41527a(this.cRf.get(this.cRe[i2][0]), this.cRe[i2][2], this.cRf.get(this.cRe[i2][1]), this.cRe[i2][3]);
        }
    }

    /* renamed from: L */
    private boolean m2992L(Vec3f vec3f) {
        for (C4092zL next : this.cRf) {
            int i = 0;
            while (true) {
                if (i < 3) {
                    if (vec3f.equals(next.mo23293fO(i))) {
                        return true;
                    }
                    i++;
                }
            }
        }
        return false;
    }

    private C4092zL aNI() {
        float f = Float.POSITIVE_INFINITY;
        C4092zL zLVar = null;
        for (C4092zL next : this.cRf) {
            if (next.asA() && next.asz() < f) {
                f = next.asz();
                zLVar = next;
            }
        }
        return zLVar;
    }

    /* renamed from: a */
    private int m2994a(C4092zL zLVar, int i, C1422Up up) {
        if (zLVar.isObsolete()) {
            return 0;
        }
        int i2 = (i + 1) % 3;
        if (zLVar.mo23296vO().dot(up.bxH()) + zLVar.asz() > 0.0f) {
            zLVar.mo23290bK(true);
            m2995a(zLVar);
            System.out.println("GOT A VISIBLE FACE\n\tW " + up.bxH() + "\n\tcurrentFace.getNormal() " + zLVar.mo23296vO() + "\n\tdot = " + zLVar.mo23296vO().dot(up.bxH()) + "\n\tcurrentFace.getPlaneDistance() = " + zLVar.asz() + "\n");
            m2993M(zLVar.mo23296vO());
            aNK();
            this.cRm.mo19353a(zLVar.mo23293fO(0).bxH(), zLVar.mo23293fO(1).bxH(), zLVar.mo23293fO(2).bxH(), 1.0f, 0.0f, 1.0f);
            int i3 = (i + 2) % 3;
            return m2994a(zLVar.mo23291fM(i3), zLVar.mo23292fN(i3), up) + m2994a(zLVar.mo23291fM(i2), zLVar.mo23292fN(i2), up) + 0;
        }
        C4092zL zLVar2 = new C4092zL(zLVar.mo23293fO(i), zLVar.mo23293fO(i2), new C1422Up(up));
        m2996b(zLVar2);
        this.cRf.add(zLVar2);
        C4092zL.m41527a(zLVar2, 0, zLVar, i);
        if (this.cRl == null) {
            this.cRk = zLVar2;
        } else {
            C4092zL.m41527a(zLVar2, 1, this.cRl, 2);
        }
        this.cRl = zLVar2;
        this.cRf.add(zLVar2);
        return 1;
    }

    /* renamed from: a */
    private void m2995a(C4092zL zLVar) {
        this.cRf.remove(zLVar);
    }

    private void aNJ() {
        Vec3f vec3f = new Vec3f((Vector3f) this.cRg.mo23296vO());
        vec3f.mo23511mT(this.cRg.asz());
        float length = this.cRg.mo23293fO(0).bxH().mo23506j(vec3f).mo23476b(this.cRg.mo23293fO(1).bxH().mo23506j(vec3f)).length();
        float length2 = this.cRg.mo23293fO(1).bxH().mo23506j(vec3f).mo23476b(this.cRg.mo23293fO(2).bxH().mo23506j(vec3f)).length();
        float length3 = this.cRg.mo23293fO(2).bxH().mo23506j(vec3f).mo23476b(this.cRg.mo23293fO(0).bxH().mo23506j(vec3f)).length();
        float f = length + length2 + length3;
        if (f <= 0.0f) {
            f = 1.0f;
        }
        vec3f.set(length / f, length2 / f, length3 / f);
        Vec3f[][] vec3fArr = (Vec3f[][]) Array.newInstance(Vec3f.class, new int[]{2, 3});
        for (int i = 0; i < 3; i++) {
            vec3fArr[0][i] = new Vec3f();
            this.aAn.mo12184u(this.cRg.mo23293fO(i).getDirection(), vec3fArr[0][i]);
        }
        for (int i2 = 0; i2 < 3; i2++) {
            vec3fArr[1][i2] = new Vec3f();
            this.aAn.mo12185v(this.cRg.mo23293fO(i2).getDirection().mo23511mT(1.0f), vec3fArr[1][i2]);
        }
        this.cRh.set(vec3fArr[0][0].mo23510mS(vec3f.x).mo23503i(vec3fArr[0][1].mo23510mS(vec3f.y)).mo23503i(vec3fArr[0][2].mo23510mS(vec3f.z)));
        this.cRi.set(vec3fArr[1][0].mo23510mS(vec3f.x).mo23503i(vec3fArr[1][1].mo23510mS(vec3f.y)).mo23503i(vec3fArr[1][2].mo23510mS(vec3f.z)));
    }

    /* renamed from: b */
    private void m2996b(C4092zL zLVar) {
        if (zLVar != null) {
            this.cRm.mo19356b(zLVar.mo23293fO(0).bxH(), zLVar.mo23293fO(1).bxH(), zLVar.mo23293fO(2).bxH());
        }
    }

    private void aNK() {
        for (C4092zL next : this.cRf) {
            this.cRm.mo19352a(next.mo23293fO(0).bxH(), next.mo23293fO(1).bxH(), next.mo23293fO(2).bxH());
        }
    }

    /* renamed from: M */
    private void m2993M(Vec3f vec3f) {
        this.cRm.mo19361yc().clear();
        if (this.cRn != null) {
            this.cRm.mo19361yc().add(new Vec3f((Vector3f) this.cRn.bxH()));
        }
        if (vec3f != null) {
            this.cRm.mo19361yc().add(vec3f);
        }
    }
}
