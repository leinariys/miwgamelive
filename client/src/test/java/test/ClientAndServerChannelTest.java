package test;

import game.engine.DataGameEvent;
import game.network.WhoAmI;
import game.network.build.BuilderSocketStatic;
import game.network.build.IServerConnect;
import game.network.channel.Connect;
import game.network.channel.NetworkChannel;
import game.network.channel.client.ClientConnect;
import logic.baa.C1616Xf;
import logic.baa.aDJ;
import logic.res.html.C6663arL;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import p001a.*;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: a.AG */
/* compiled from: a */
public abstract class ClientAndServerChannelTest<T> extends Assert {
    public NetworkChannel gOH;
    public DataGameEvent gOI;
    public ArrayList<DataGameEvent> gOO = new ArrayList<>();
    public ArrayList<DataGameEvent> gOP = new ArrayList<>();
    public AtomicInteger gOQ = new AtomicInteger((int) ((15226.0d + (Math.random() * 1000.0d)) + 100.0d));
    private long fXt;
    private long gOJ;
    private long gOK;
    private C2672iO gOL;
    private ArrayList<NetworkChannel> gOM = new ArrayList<>();
    private ArrayList<NetworkChannel> gON = new ArrayList<>();
    private int gOR = ((this.gOQ.incrementAndGet() % 32767) + IServerConnect.PORT);
    private int gOS = ((this.gOQ.incrementAndGet() % 32767) + IServerConnect.PORT);
    private long startTime;

    /* renamed from: a */
    public static void m187a(aDJ adj, aDJ adj2) {
        if (adj != adj2) {
            if (adj != null && adj2 == null) {
                assertSame(adj, adj2);
            }
            if (adj == null && adj2 != null) {
                assertSame(adj, adj2);
            }
            assertTrue(adj.bFf().getObjectId().getId() == adj2.bFf().getObjectId().getId());
        }
    }

    /* access modifiers changed from: protected */
    public T cBC() {
        return mo169vK(0).bGz().mo6901yn();
    }

    /* access modifiers changed from: protected */
    /* renamed from: vI */
    public T mo167vI(int i) {
        return mo169vK(i).bGz().mo6901yn();
    }

    /* access modifiers changed from: protected */
    /* renamed from: vJ */
    public T mo168vJ(int i) {
        return mo170vL(i).bGz().mo6901yn();
    }

    /* access modifiers changed from: protected */
    public T cBD() {
        return mo168vJ(0);
    }

    /* access modifiers changed from: protected */
    public boolean cBE() {
        return false;
    }

    /* access modifiers changed from: protected */
    public T cBF() {
        return this.gOI.bGz().mo6901yn();
    }

    public DataGameEvent cBG() {
        return this.gOI;
    }

    public DataGameEvent cBH() {
        return this.gOO.get(0);
    }

    /* renamed from: vK */
    public DataGameEvent mo169vK(int i) {
        return this.gOO.get(i);
    }

    /* renamed from: vL */
    public DataGameEvent mo170vL(int i) {
        return this.gOP.get(i);
    }

    public DataGameEvent cBI() {
        return mo170vL(0);
    }

    @Before
    public void setUp() {
        this.gOJ = System.nanoTime();
        if (cBK()) {
            cBP();
        }
        if (cBJ()) {
            cBM();
        }
        this.startTime = System.nanoTime();
    }

    /* access modifiers changed from: protected */
    public boolean cBJ() {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean cBK() {
        return true;
    }

    @After
    public void tearDown() {
        this.fXt = System.nanoTime();
        long currentTimeMillis = System.currentTimeMillis();
        try {
            Iterator<DataGameEvent> it = this.gOO.iterator();
            while (it.hasNext()) {
                it.next().mo3335HY();
            }
            this.gOK = System.nanoTime();
            System.out.println("Test time: " + m188kT(1.0E-6f * ((float) (this.fXt - this.startTime))) + "ms  Setup: " + m188kT(1.0E-6f * ((float) (this.startTime - this.gOJ))) + "ms  Finalization: " + m188kT(1.0E-6f * ((float) (this.gOK - this.fXt))) + "ms  Total: " + m188kT(1.0E-6f * ((float) (this.gOK - this.gOJ))) + "ms");
            Iterator<NetworkChannel> it2 = this.gOM.iterator();
            while (it2.hasNext()) {
                NetworkChannel next = it2.next();
                long jf = next.metricReadByte();
                float je = (float) next.metricByteLength();
                float jc = (float) next.metricCountReadMessage();
                long jg = next.metricWriteByte();
                long jd = next.metricAllOriginalByteLength();
                long jb = next.metricCountWriteMessage();
                long ja = currentTimeMillis - next.metricClientTimeAuthorization();
                System.out.println("Client Network Statistics\n [Recv: " + jf + " / " + je + " = " + m188kT((100.0f * ((float) jf)) / je) + "%  " + jc + " msgs " + m188kT((1000.0f * jc) / ((float) ja)) + " m/s " + m188kT((((float) ja) * 1000.0f) / jc) + "us/m (" + m188kT(((float) jf) / jc) + " / " + m188kT(je / jc) + ")]   \n [Sent: " + jg + " / " + jd + " = " + m188kT((100.0f * ((float) jg)) / ((float) jd)) + "%  " + jb + " msgs " + m188kT((((float) jb) * 1000.0f) / ((float) ja)) + " m/s " + m188kT((((float) ja) * 1000.0f) / ((float) jb)) + "us/m (" + m188kT(((float) jg) / ((float) jb)) + " / " + m188kT(((float) jd) / ((float) jb)) + ")] up: " + ja + " ms");
            }
        } finally {
            if (this.gOI != null) {
                this.gOI.mo3335HY();
                cBL();
            }
        }
    }

    /* renamed from: kT */
    private float m188kT(float f) {
        return ((float) Math.round(f * 100.0f)) / 100.0f;
    }

    /* access modifiers changed from: protected */
    public void cBL() {
        cBS();
        if (cBT()) {
        }
    }

    /* access modifiers changed from: protected */
    public DataGameEvent cBM() {
        NetworkChannel cBU = cBU();
        C0056Ae ae = new C0056Ae();
        ae.setWhoAmI(WhoAmI.CLIENT);
        ae.setServerSocket(cBU);
        ae.setClassObject(cBY());
        DataGameEvent jz = new DataGameEvent(ae);
        jz.mo3427dI(cBE());
        this.gOO.add(jz);
        return jz;
    }

    /* access modifiers changed from: protected */
    public DataGameEvent cBN() {
        this.gOI.bGU().aPT().buildListenerChannel(this.gOS, (int[]) null, (int[]) null, false);
        NetworkChannel cBV = cBV();
        C6094agO cBO = cBO();
        cBO.setWhoAmI(WhoAmI.CLIENT);
        cBO.mo13375ey(true);
        cBO.setServerSocket(cBV);
        cBO.setClassObject(cBY());
        DataGameEvent a = mo139a(cBO);
        a.mo3427dI(cBE());
        this.gOP.add(a);
        a.bGV();
        return a;
    }

    /* access modifiers changed from: protected */
    public C6094agO cBO() {
        return new C0056Ae();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public DataGameEvent mo139a(C6094agO ago) {
        return new DataGameEvent(ago);
    }

    /* access modifiers changed from: protected */
    public void cBP() {
        this.gOH = cBX();
        C0056Ae ae = new C0056Ae();
        ae.setWhoAmI(WhoAmI.SERVER);
        ae.setServerSocket(this.gOH);
        ae.setClassObject(cBY());
        ae.mo13372ev(cBR());
        ae.setWraperDbFile((WraperDbFile) new C5837abR());
        this.gOI = new DataGameEvent(ae);
        this.gOI.mo3427dI(cBE());
        this.gOH.creatListenerChannel(cBQ());
    }

    /* access modifiers changed from: protected */
    public Connect cBQ() {
        return new C0017a();
    }

    /* access modifiers changed from: protected */
    public boolean cBR() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean cBS() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean cBT() {
        return true;
    }

    /* access modifiers changed from: protected */
    public NetworkChannel cBU() {
        return mo141az("teste" + (this.gOM.size() + 1), "teste" + (this.gOM.size() + 1));
    }

    /* access modifiers changed from: protected */
    /* renamed from: az */
    public NetworkChannel mo141az(String str, String str2) {
        ServerSocketSnapshot mCVar;
        if (cBW()) {
            ServerSocketSnapshot mCVar2 = new ServerSocketSnapshot();
            if (this.gOL == null) {
                this.gOL = new C2672iO();
            }
            this.gOL.mo19477a(mCVar2);
            this.gOM.add(mCVar2);
            mCVar = mCVar2;
        } else {
            try {
                NetworkChannel a = BuilderSocketStatic.m21641a(IServerConnect.DEFAULT_HOST, this.gOR, str, str2, false, "testServer" + hashCode());
                this.gOM.add(a);
                mCVar = a;
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e2) {
                throw new RuntimeException(e2);
            }
        }
        return mCVar;
    }

    /* access modifiers changed from: protected */
    public NetworkChannel cBV() {
        ServerSocketSnapshot mCVar;
        if (cBW()) {
            ServerSocketSnapshot mCVar2 = new ServerSocketSnapshot();
            if (this.gOL == null) {
                this.gOL = new C2672iO();
            }
            this.gOL.mo19477a(mCVar2);
            this.gON.add(mCVar2);
            mCVar = mCVar2;
        } else {
            try {
                NetworkChannel d = BuilderSocketStatic.m21643d(IServerConnect.DEFAULT_HOST, this.gOS, "testServer" + hashCode());
                this.gON.add(d);
                mCVar = d;
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e2) {
                throw new RuntimeException(e2);
            }
        }
        return mCVar;
    }

    /* access modifiers changed from: protected */
    public boolean cBW() {
        return false;
    }

    /* access modifiers changed from: protected */
    public NetworkChannel cBX() {
        if (!cBW()) {
            return BuilderSocketStatic.buildServerSocket(this.gOR, "testServer" + hashCode());
        }
        ServerSocketSnapshot mCVar = new ServerSocketSnapshot();
        if (this.gOL == null) {
            this.gOL = new C2672iO();
        }
        this.gOL.mo19477a(mCVar);
        return mCVar;
    }

    /* access modifiers changed from: protected */
    public Class<?> cBY() {
        return (Class) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public <T extends aDJ> T mo140a(DataGameEvent jz, T t) {
        C6663arL bFf = ((C1616Xf) t).bFf();
        return bFf.mo6866PM() == jz ? t : (aDJ) jz.mo3421c(jz.bxy().getMagicNumber(t.getClass()), bFf.getObjectId().getId());
    }

    /* renamed from: a.AG$a */
    class C0017a implements Connect {
        C0017a() {
        }

        /* renamed from: a */
        public void chechUser(ClientConnect bVar, String userName, String pass, boolean isForcingNewUser) {
        }

        /* renamed from: d */
        public void userDisconnected(ClientConnect bVar) {
            ClientAndServerChannelTest.this.gOI.mo3449i(bVar);
        }
    }
}
