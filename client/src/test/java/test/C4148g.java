package test;

import game.engine.DataGameEvent;
import game.network.WhoAmI;
import game.script.Taikodom;
import logic.res.FileControl;
import org.junit.Assert;
import p001a.*;

/* renamed from: g */
/* compiled from: test.a */
public class C4148g {
    /* renamed from: v */
    private static DataGameEvent m41960v(String str, boolean z) {
        if (z) {
            m41959nD("data/" + str + ".db");
            m41959nD("data/" + str + ".lg");
        }
        C0056Ae ae = new C0056Ae();
        ae.setWhoAmI(WhoAmI.SERVER);
        ae.setServerSocket(new ServerSocketSnapshot());
        ae.setClassObject(Taikodom.class);
        ae.mo13373ew(true);
        ae.mo13371eu(true);
        ae.mo13372ev(true);
        ae.setResursData(new FileControl("data/"));
        WraperDbFileImpl abr = new WraperDbFileImpl(false);
        abr.setDbname(str);
        ae.setWraperDbFile((WraperDbFile) abr);
        return new DataGameEvent(ae);
    }

    /* renamed from: nD */
    private static void m41959nD(String str) {
        FileControl avr = new FileControl(str);
        if (avr.exists()) {
            Assert.assertTrue(avr.delete());
        }
    }

    /* renamed from: e */
    public static void m41958e(DataGameEvent jz) {
        long j = -1;
        int i = -1;
        int i2 = 0;
        while (true) {
            int i3 = i;
            if (i2 < 100) {
                j = System.nanoTime();
                jz.ayN();
                i = 0;
                for (C3582se next : jz.bGA()) {
                    try {
                        if (next.bFT()) {
                            next.mo5608dq();
                        }
                        i++;
                    } finally {
                        if (j != j) {
                            jz.mo3441gd(System.nanoTime() - j);
                        }
                    }
                }
                jz.mo3442ge(System.nanoTime() - j);
                System.out.println("Total : " + i);
                if (i3 == i) {
                    break;
                }
                i2++;
            } else {
                break;
            }
        }
        System.out.println("Done loading database");
    }

    private static void dwg() {
        DataGameEvent v = m41960v("environment", false);
        System.out.println("Loading the reacheable objects into main memory");
        m41958e(v);
        System.out.println("Creating new repository");
        WraperDbFileImpl abr = new WraperDbFileImpl(false);
        abr.mo5234f(new FileControl("data/"));
        abr.setDbname("fixed-environment");
        abr.mo5219a((C1412Uh) v);
        int i = 0;
        long nanoTime = System.nanoTime();
        long nanoTime2 = System.nanoTime();
        try {
            abr.init();
            System.out.println("Iterating over the current objects");
            v.bGt();
            int i2 = 0;
            for (C3582se next : v.bGA()) {
                if (next.mo6901yn().mo11T().mo15115Ew() != C1020Ot.C1021a.NOT_PERSISTED) {
                    abr.mo7898a(next, next.cVo());
                    i++;
                    int i3 = i2 + 1;
                    if (i3 > 100000) {
                        abr.commit();
                        i3 = 0;
                    }
                    long nanoTime3 = System.nanoTime() - nanoTime;
                    if (((double) (System.nanoTime() - nanoTime2)) * 1.0E-9d > 10.0d) {
                        System.out.println("Working... Persisted " + i + " objects so far in " + (((double) nanoTime3) * 1.0E-9d) + " seconds => " + (((double) i) / (((double) nanoTime3) * 1.0E-9d)) + " objects per second.");
                        nanoTime2 = System.nanoTime();
                        i2 = i3;
                    } else {
                        i2 = i3;
                    }
                }
            }
            abr.commit();
            abr.mo5218HY();
            v.bGu();
            long nanoTime4 = System.nanoTime() - nanoTime;
            System.out.println("Done! Persisted " + i + " objects. Took " + (((double) nanoTime4) * 1.0E-9d) + " seconds => " + (((double) i) / (((double) nanoTime4) * 1.0E-9d)) + " objects per second.");
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Throwable th) {
            v.bGu();
            throw th;
        }
    }

    public static void main(String[] strArr) {
        System.setProperty("FIX_COLLECTIONS", "TRUE");
        dwg();
    }
}
