package test;

import com.hoplon.taikodom.common.TaikodomVersion;
import game.network.WhoAmI;
import game.network.build.BuilderSocketStatic;
import game.network.build.IServerConnect;
import game.network.channel.NetworkChannel;
import game.network.exception.IOExceptionServer;
import game.script.Taikodom;
import game.script.offload.OffloadNode;
import game.script.offload.OffloadUtils;
import game.script.simulation.Space;
import logic.res.FileControl;
import logic.res.code.DataGameEventImpl;
import logic.res.code.ResourceDeobfuscation;
import logic.thred.C6101agV;
import logic.thred.LogPrinter;
import logic.thred.PoolThread;
import logic.thred.WatchDog;
import taikodom.render.loader.provider.FilePath;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;

/* renamed from: a.ul */
/* compiled from: a */
public class C3765ul {
    /* access modifiers changed from: private */
    public static final LogPrinter log = LogPrinter.m10275K(C3765ul.class);
    static long iFa = 0;
    static WatchDog watchDog;
    private static int iFb;
    private static long iFc;
    private static C6101agV iFd;
    private static String iFe;

    public static void main(String[] strArr) throws IOException {
        FileControl avr = new FileControl(System.getProperty("physics.resources.rootpath", "."));
        FileControl avr2 = new FileControl(".");
        HashMap hashMap = new HashMap();
        for (String str : strArr) {
            if (str.startsWith("-")) {
                String[] split = str.split("=");
                if (split.length == 1) {
                    hashMap.put(str, "true");
                } else {
                    hashMap.put(split[0], split[1]);
                }
            }
        }
        m40104a((FilePath) avr2, (FilePath) avr, (HashMap<String, String>) hashMap);
    }

    /* renamed from: a */
    public static void m40104a(FilePath ain, FilePath ain2, HashMap<String, String> hashMap) throws IOException {
        iFe = OffloadUtils.m17613hY();
        DataGameEventImpl.bxC();
        C2606hU.setRootPathResource(ain2);
        watchDog = new WatchDog();
        watchDog.setPrintWriter(LogPrinter.createLogFile("offload-watchdog", "log"));
        watchDog.start();
        iFd = new C6101agV();
        log.debug("step6");
        iFd.setPrintWriter(LogPrinter.createLogFile("collision-watchdog", "log"));
        iFd.start();
        NetworkChannel nq = m40106nq(System.getProperty("main.server", IServerConnect.DEFAULT_HOST));
        ResourceDeobfuscation sh = new ResourceDeobfuscation();
        sh.setServerSocket(nq);
        sh.setWhoAmI(WhoAmI.CLIENT);
        sh.mo13375ey(true);
        sh.setClassObject(Taikodom.class);
        sh.mo13372ev(false);
        sh.mo13373ew(false);
        DataGameEventImpl arVar = new DataGameEventImpl(sh);
        Taikodom dn = (Taikodom) arVar.bGz().mo6901yn();
        arVar.bGV();
        System.setProperty("console.port", "5459");
        new C0493Gq(ain.concat("components"), dn);
        OffloadNode M = dn.aLY().mo11046M((Map<String, String>) hashMap);
        M.mo17693E(iFe);
        while (true) {
            m40105a(arVar, M, (Executor) new C3766a());
        }
    }

    /* renamed from: a */
    private static void m40105a(DataGameEventImpl arVar, OffloadNode cxVar, Executor executor) {
        try {
            long currentTimeMillis = System.currentTimeMillis();
            iFb++;
            for (Space ea : cxVar.mo17699hQ()) {
                long currentTimeMillis2 = System.currentTimeMillis();
                ea.mo1913gZ();
                ea.hja = (System.currentTimeMillis() - currentTimeMillis2) + ea.hja;
                if (currentTimeMillis - iFa >= 1000) {
                    cxVar.mo17694b(ea, (long) (((double) ea.hja) / ((double) iFb)));
                    ea.hja = 0;
                }
            }
            long currentTimeMillis3 = (10 + currentTimeMillis) - System.currentTimeMillis();
            if (currentTimeMillis3 > 0) {
                PoolThread.sleep(Math.min(100, currentTimeMillis3));
            }
            long currentTimeMillis4 = System.currentTimeMillis();
            arVar.mo3366b(0.1f, executor);
            iFc = (System.currentTimeMillis() - currentTimeMillis4) + iFc;
            if (currentTimeMillis - iFa >= 1000) {
                cxVar.mo17704q((long) (((double) iFc) / ((double) iFb)));
                iFb = 0;
                iFa = currentTimeMillis;
                iFc = 0;
            }
        } catch (RuntimeException e) {
            log.error("Error in OffloadServer main thread", e);
            log.error("Error in OffloadServer main thread. Cause", e.getCause());
            throw e;
        }
    }

    /* renamed from: nq */
    private static NetworkChannel m40106nq(String str) throws IOException {
        while (true) {
            if (str != null) {
                return BuilderSocketStatic.m21643d(str, IServerConnect.PORT, TaikodomVersion.VERSION);
            }
            try {
                throw new IOException("Main server not set");
            } catch (IOExceptionServer e) {
                log.info("Ignoring exception, please start the server, retrying in 1 second", e);
                PoolThread.sleep(1000);
            }
        }
    }

    /* renamed from: a.ul$a */
    static class C3766a implements Executor {
        C3766a() {
        }

        public void execute(Runnable runnable) {
            try {
                runnable.run();
            } catch (ThreadDeath e) {
                throw e;
            } catch (Throwable th) {
                C3765ul.log.warn("exception ignored", th);
            }
        }
    }
}
