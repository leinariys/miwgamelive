package test;

import com.steadystate.css.parser.CSSOMParser;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.*;

import java.io.FileReader;
import java.io.Reader;

/* renamed from: test.a.hB */
/* compiled from: test.a */
public class C2575hB {
    public C2575hB() {
        try {
            CSSStyleSheet c = new CSSOMParser().parseStyleSheet(new InputSource((Reader) new FileReader("stylesheets/page_test.css")));
            System.out.println(c.toString());
            CSSRuleList cssRules = c.getCssRules();
            for (int i = 0; i < cssRules.getLength(); i++) {
                CSSStyleRule item = cssRules.item(i);
                if (item.getType() == 1) {
                    CSSStyleDeclaration style = item.getStyle();
                    for (int i2 = 0; i2 < style.getLength(); i2++) {
                        CSSPrimitiveValue propertyCSSValue = style.getPropertyCSSValue(style.item(i2));
                        if (propertyCSSValue.getCssValueType() == 1) {
                            CSSPrimitiveValue cSSPrimitiveValue = propertyCSSValue;
                            System.out.println(">> " + cSSPrimitiveValue.toString());
                            if (cSSPrimitiveValue.getPrimitiveType() == 23) {
                                System.out.println("CSS_COUNTER(" + cSSPrimitiveValue.toString() + ")");
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("Exception: " + e.getMessage());
            e.printStackTrace(System.err);
        }
    }

    public static void main(String[] strArr) {
        new C2575hB();
    }
}
