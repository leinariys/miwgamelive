package test;

import logic.res.code.ResourceDeobfuscation;
import p001a.C2650hz;

import java.io.IOException;

/* renamed from: a.acx  reason: case insensitive filesystem */
/* compiled from: a */
public class C5921acx extends C2650hz {
    public static void main(String[] strArr) throws IOException {
        new C5921acx().mo19438a(new ResourceDeobfuscation(), "data/base-environment-snapshot.xml.gz", "data/environment-snapshot.xml.gz", "data/live-environment-snapshot.xml.gz");
    }
}
