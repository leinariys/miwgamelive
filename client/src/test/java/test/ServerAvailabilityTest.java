package test;

import com.hoplon.taikodom.common.TaikodomVersion;
import game.network.WhoAmI;
import game.network.build.BuilderSocketStatic;
import game.network.build.IServerConnect;
import game.network.channel.NetworkChannel;
import game.script.Taikodom;
import logic.res.code.DataGameEventImpl;
import logic.res.code.ResourceDeobfuscation;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.C5973adx;
import taikodom.game.main.ClientMain;

import java.io.*;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

/* renamed from: test.a.fZ */
/* compiled from: test.a */
public class ServerAvailabilityTest {

    /* renamed from: OE */
    private static final String f7399OE = "ServerAvailability-";
    private static final Log logger = LogPrinter.setClass(ClientMain.class);
    /* renamed from: OF */
    private static SimpleDateFormat f7400OF = new SimpleDateFormat("yyyy-MM-dd");
    /* renamed from: OG */
    private static SimpleDateFormat f7401OG = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");

    public static void main(String[] strArr) {
        long j;
        long j2;
        boolean z;
        NetworkChannel ahg;
        long j3;
        boolean z2;
        if (strArr.length != 3) {
            logger.error("Wrong number of parameters");
            return;
        }
        Properties uI = m31179uI();
        String[] split = uI.getProperty("host", IServerConnect.DEFAULT_HOST).split(",");
        boolean z3 = false;
        boolean z4 = false;
        long currentTimeMillis = System.currentTimeMillis();
        Date date = null;
        long j4 = 0;
        long currentTimeMillis2 = System.currentTimeMillis();
        while (true) {
            long currentTimeMillis3 = System.currentTimeMillis();
            if ((currentTimeMillis3 - currentTimeMillis) / 1000 >= ((long) new Integer(strArr[1]).intValue())) {
                date = new Date(currentTimeMillis3);
                int parseInt = Integer.parseInt(uI.getProperty("port", String.valueOf(IServerConnect.PORT_LOGIN)));
                int length = split.length;
                int i = 0;
                NetworkChannel ahg2 = null;
                boolean z5 = z3;
                while (i < length) {
                    String str = split[i];
                    try {
                        ahg = BuilderSocketStatic.m21641a(str, parseInt, uI.getProperty("user", "teste1"), uI.getProperty("pass", "teste1"), false, TaikodomVersion.VERSION);
                        z = z5;
                    } catch (Exception e) {
                        logger.error(e);
                        z = false;
                        ahg = ahg2;
                    }
                    if (ahg != null) {
                        z = true;
                        ResourceDeobfuscation sh = new ResourceDeobfuscation();
                        sh.setServerSocket(ahg);
                        sh.setWhoAmI(WhoAmI.CLIENT);
                        sh.setClassObject(Taikodom.class);
                        sh.mo13372ev(false);
                        sh.mo13373ew(false);
                        DataGameEventImpl arVar = new DataGameEventImpl(sh);
                        arVar.mo3428dJ(true);
                        arVar.mo3427dI(false);
                        arVar.ala().aLW().bIW();
                        z2 = true;
                        arVar.mo3335HY();
                        try {
                            ahg.close();
                            ahg = null;
                        } catch (IOException e2) {
                            logger.error(e2);
                        }
                        j3 = System.currentTimeMillis() - currentTimeMillis3;
                    } else {
                        j3 = j4;
                        z2 = z4;
                    }
                    m31178a(date, str, parseInt, z, z2, j3);
                    j4 = 0;
                    z5 = false;
                    z4 = false;
                    i++;
                    ahg2 = ahg;
                    currentTimeMillis = currentTimeMillis3;
                }
                logger.info("*** Terminated ***");
                j = currentTimeMillis;
                z3 = z5;
            } else {
                j = currentTimeMillis;
            }
            if ((currentTimeMillis3 - currentTimeMillis2) / 1000 >= ((long) new Integer(strArr[2]).intValue())) {
                try {
                    InetAddress localHost = InetAddress.getLocalHost();
                    String str2 = "<<unknown>>";
                    if (localHost != null) {
                        str2 = localHost.getHostName();
                    }
                    C5973adx adx = new C5973adx(strArr[0]);
                    HashMap hashMap = new HashMap();
                    hashMap.put("file", new File(f7399OE + f7400OF.format(date) + ".log"));
                    hashMap.put("client_hostname", str2);
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(adx.mo12985t(hashMap)));
                    String readLine = bufferedReader.readLine();
                    bufferedReader.close();
                    if ("SUCCESS".equals(readLine)) {
                        try {
                            logger.info("*** Data saved to database ***");
                            m31177a(date);
                            currentTimeMillis2 = currentTimeMillis3;
                            currentTimeMillis = j;
                        } catch (IOException e3) {
                            e = e3;
                            j2 = currentTimeMillis3;
                            logger.error(e);
                            currentTimeMillis2 = j2;
                            currentTimeMillis = j;
                        }
                    }
                } catch (IOException e4) {
                    e = e4;
                    j2 = currentTimeMillis2;
                    logger.error(e);
                    currentTimeMillis2 = j2;
                    currentTimeMillis = j;
                }
            }
            currentTimeMillis = j;
        }
    }

    /* renamed from: test.a */
    private static void m31177a(Date date) {
        File file = new File(f7399OE + f7400OF.format(new Date(date.getTime() - 86400000)) + ".log");
        if (file.exists()) {
            file.delete();
        }
        logger.info("*** File deleted ***");
    }

    /* renamed from: uI */
    private static Properties m31179uI() {
        Properties properties = new Properties();
        File file = new File("config/metrics/metrics.properties");
        if (file.exists()) {
            try {
                properties.load(new FileInputStream(file));
            } catch (FileNotFoundException e) {
                logger.error(e);
            } catch (IOException e2) {
                logger.error(e2);
            }
        }
        return properties;
    }

    /* renamed from: test.a */
    private static void m31178a(Date date, String str, int i, boolean z, boolean z2, long j) {
        try {
            PrintWriter printWriter = new PrintWriter(new FileOutputStream(f7399OE + f7400OF.format(date) + ".log", true));
            printWriter.append(f7401OG.format(date) + ";" + str + ";" + i + ";" + z + ";" + z2 + ";" + j + ";\n");
            printWriter.close();
            logger.info("*** File updated ***");
        } catch (FileNotFoundException e) {
            logger.error(e);
        }
    }
}
