package test;

import game.engine.DataGameEvent;
import game.network.WhoAmI;
import game.network.build.BuilderSocketStatic;
import game.network.channel.Connect;
import game.network.channel.NetworkChannel;
import game.network.channel.client.ClientConnect;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import p001a.C0056Ae;
import p001a.C2282db;
import p001a.C2672iO;
import p001a.ServerSocketSnapshot;

import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: a.dP */
/* compiled from: a */
public abstract class C2261dP<T> extends Assert {
    public ArrayList<NetworkChannel> izL = new ArrayList<>();
    public ArrayList<DataGameEvent> izM = new ArrayList<>();
    int izQ = 0;
    private long fXt;
    private long gOJ;
    private long gOK;
    private ArrayList<NetworkChannel> izN = new ArrayList<>();
    private ArrayList<DataGameEvent> izO = new ArrayList<>();
    private C2672iO izP;
    private long startTime;

    /* access modifiers changed from: protected */
    public abstract Class<?> cBY();

    /* access modifiers changed from: protected */
    /* renamed from: vI */
    public T mo17781vI(int i) {
        return this.izO.get(i).bGz().mo6901yn();
    }

    /* access modifiers changed from: protected */
    public boolean cBE() {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: zP */
    public T mo17783zP(int i) {
        return this.izM.get(i).bGz().mo6901yn();
    }

    /* renamed from: zQ */
    public DataGameEvent mo17784zQ(int i) {
        return this.izM.get(i);
    }

    /* renamed from: vK */
    public DataGameEvent mo17782vK(int i) {
        return this.izO.get(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: zR */
    public NetworkChannel mo17785zR(int i) {
        return this.izN.get(i);
    }

    @Before
    public void dnL() {
        this.gOJ = System.nanoTime();
        int dnN = dnN();
        while (true) {
            dnN--;
            if (dnN < 0) {
                break;
            }
            cBP();
        }
        int dnM = dnM();
        while (true) {
            dnM--;
            if (dnM < 0) {
                this.startTime = System.nanoTime();
                return;
            }
            dnP();
        }
    }

    /* access modifiers changed from: protected */
    public int dnM() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public int dnN() {
        return 1;
    }

    @After
    public void dnO() {
        this.fXt = System.nanoTime();
        System.currentTimeMillis();
        try {
            if (this.izO != null) {
                Iterator<DataGameEvent> it = this.izO.iterator();
                while (it.hasNext()) {
                    it.next().mo3335HY();
                }
            }
            Iterator<DataGameEvent> it2 = this.izM.iterator();
            while (it2.hasNext()) {
                it2.next().mo3335HY();
                cBL();
            }
            this.gOK = System.nanoTime();
            System.out.println("Test time: " + m28834kT(((float) (this.fXt - this.startTime)) * 1.0E-6f) + "ms  Setup: " + m28834kT(((float) (this.startTime - this.gOJ)) * 1.0E-6f) + "ms  Finalization: " + m28834kT(((float) (this.gOK - this.fXt)) * 1.0E-6f) + "ms  Total: " + m28834kT(((float) (this.gOK - this.gOJ)) * 1.0E-6f) + "ms");
            Iterator<NetworkChannel> it3 = this.izN.iterator();
            while (it3.hasNext()) {
                NetworkChannel next = it3.next();
            }
        } catch (Throwable th) {
            Throwable th2 = th;
            Iterator<DataGameEvent> it4 = this.izM.iterator();
            while (it4.hasNext()) {
                it4.next().mo3335HY();
                cBL();
            }
            throw th2;
        }
    }

    /* renamed from: kT */
    private float m28834kT(float f) {
        return ((float) Math.round(f * 100.0f)) / 100.0f;
    }

    private void cBL() {
        Iterator<DataGameEvent> it = this.izM.iterator();
        while (it.hasNext()) {
            DataGameEvent next = it.next();
        }
    }

    private void dnP() {
        try {
            NetworkChannel cBU = cBU();
            C0056Ae ae = new C0056Ae();
            ae.setWhoAmI(WhoAmI.CLIENT);
            ae.setServerSocket(cBU);
            ae.setClassObject(cBY());
            DataGameEvent jz = new DataGameEvent(ae);
            jz.mo3427dI(cBE());
            this.izO.add(jz);
            this.izN.add(cBU);
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    private void cBP() {
        try {
            NetworkChannel cBX = cBX();
            C0056Ae ae = new C0056Ae();
            ae.setWhoAmI(WhoAmI.SERVER);
            ae.setServerSocket(cBX);
            ae.setClassObject(cBY());
            DataGameEvent jz = new DataGameEvent(ae);
            this.izL.get(this.izL.size() - 1).creatListenerChannel((Connect) new C2262a(jz));
            this.izM.add(jz);
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw e;
        }
    }

    private NetworkChannel cBU() {
        this.izQ %= this.izL.size();
        NetworkChannel ahg = this.izL.get(this.izQ);
        this.izQ++;
        return null;
    }

    private NetworkChannel cBX() {
        C2282db dbVar = new C2282db();
        ServerSocketSnapshot mCVar = new ServerSocketSnapshot();
        if (this.izP == null) {
            this.izP = new C2672iO();
        }
        this.izP.mo19477a(mCVar);
        NetworkChannel b = BuilderSocketStatic.buildServerSocket(this.izL.size() + 8000, "testServer" + hashCode());
        this.izL.add(b);
        dbVar.mo17868b(mCVar);
        dbVar.mo17868b(b);
        return dbVar;
    }

    /* renamed from: a.dP$a */
    class C2262a implements Connect {

        /* renamed from: AF */
        private final /* synthetic */ DataGameEvent f6505AF;

        C2262a(DataGameEvent jz) {
            this.f6505AF = jz;
        }

        /* renamed from: a */
        public void chechUser(ClientConnect bVar, String userName, String pass, boolean isForcingNewUser) {
        }

        /* renamed from: d */
        public void userDisconnected(ClientConnect bVar) {
            this.f6505AF.mo3449i(bVar);
        }
    }
}
