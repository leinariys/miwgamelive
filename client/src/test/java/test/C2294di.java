package test;

import com.steadystate.css.parser.CSSOMParser;
import com.steadystate.css.parser.HandlerBase;
import com.steadystate.css.parser.LexicalUnitImpl;
import com.steadystate.css.sac.DocumentHandler;
import logic.res.css.ParserFactory;
import logic.res.css.SACMediaList;
import org.w3c.css.sac.InputSource;
import org.w3c.css.sac.LexicalUnit;
import org.w3c.css.sac.Parser;
import org.w3c.css.sac.SelectorList;

import java.io.FileReader;
import java.io.Reader;

/* renamed from: test.a.di */
/* compiled from: test.a */
public class C2294di extends HandlerBase {

    /* renamed from: yv */
    private static final String f6583yv = "com.steadystate.css.parser.SACParser";

    /* renamed from: yw */
    private int f6584yw = 0;

    /* renamed from: yx */
    private int f6585yx = 0;

    public C2294di() {
        try {
            CSSOMParser.setProperty("org.w3c.css.sac.parser", f6583yv);
            Parser azZ = new ParserFactory().makeParser();
            azZ.setDocumentHandler((DocumentHandler) this);
            azZ.parseStyleSheet(new InputSource((Reader) new FileReader("c:\\working\\css2parser\\stylesheets\\page_test.css")));
        } catch (Exception e) {
            System.err.println("Exception: " + e.getMessage());
        }
    }

    public static void main(String[] strArr) {
        new C2294di();
    }

    /* renamed from: test.a */
    public void startDocument(InputSource fh) {
        System.out.println("startDocument");
    }

    /* renamed from: b */
    public void endDocument(InputSource fh) {
        System.out.println("endDocument");
    }

    public void comment(String str) {
    }

    /* renamed from: Q */
    public void ignorableAtRule(String str) {
        System.out.println(str);
    }

    /* renamed from: d */
    public void mo6346d(String str, String str2) {
    }

    /* renamed from: test.a */
    public void ignorableAtRule(String str, SACMediaList atf, String str2) {
        System.out.print("@import url(" + str + ")");
        if (atf.getLength() > 0) {
            System.out.println(" " + atf.toString() + ";");
        } else {
            System.out.println(";");
        }
    }

    /* renamed from: test.a */
    public void startMedia(SACMediaList atf) {
        System.out.println(String.valueOf(m29168jx()) + "@media " + atf.toString() + " {");
        m29169jy();
    }

    /* renamed from: b */
    public void endMedia(SACMediaList atf) {
        m29170jz();
        System.out.println(String.valueOf(m29168jx()) + "}");
    }

    /* renamed from: e */
    public void mo6347e(String str, String str2) {
        System.out.print(String.valueOf(m29168jx()) + "@page");
        if (str != null) {
            System.out.print(" " + str);
        }
        if (str2 != null) {
            System.out.println(" " + str2);
        }
        System.out.println(" {");
        this.f6584yw = 0;
        m29169jy();
    }

    /* renamed from: f */
    public void endPage(String str, String str2) {
        System.out.println();
        m29170jz();
        System.out.println(String.valueOf(m29168jx()) + "}");
    }

    /* renamed from: jv */
    public void mo6350jv() {
        System.out.println(String.valueOf(m29168jx()) + "@font-face {");
        this.f6584yw = 0;
        m29169jy();
    }

    /* renamed from: jw */
    public void endFontFace() {
        System.out.println();
        m29170jz();
        System.out.println(String.valueOf(m29168jx()) + "}");
    }

    /* renamed from: test.a */
    public void mo6338a(SelectorList ahq) {
        System.out.println(String.valueOf(m29168jx()) + ahq.toString() + " {");
        this.f6584yw = 0;
        m29169jy();
    }

    /* renamed from: b */
    public void endSelector(SelectorList ahq) {
        System.out.println();
        m29170jz();
        System.out.println(String.valueOf(m29168jx()) + "}");
    }

    /* renamed from: test.a */
    public void mo6341a(String str, LexicalUnit ald, boolean z) {
        int i = this.f6584yw;
        this.f6584yw = i + 1;
        if (i > 0) {
            System.out.println(";");
        }
        System.out.print(String.valueOf(m29168jx()) + str + ":");
        for (LexicalUnit ald2 = ald; ald2 != null; ald2 = ald2.getNextLexicalUnit()) {
            System.out.print(" " + ((LexicalUnitImpl) ald2).toDebugString());
        }
        if (z) {
            System.out.print(" !important");
        }
    }

    /* renamed from: jx */
    private String m29168jx() {
        StringBuffer stringBuffer = new StringBuffer(16);
        for (int i = 0; i < this.f6585yx; i++) {
            stringBuffer.append(" ");
        }
        return stringBuffer.toString();
    }

    /* renamed from: jy */
    private void m29169jy() {
        this.f6585yx += 4;
    }

    /* renamed from: jz */
    private void m29170jz() {
        this.f6585yx -= 4;
    }
}
