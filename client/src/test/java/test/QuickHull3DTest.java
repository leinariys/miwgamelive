package test;

import com.hoplon.geometry.Vec3f;
import com.sun.opengl.util.Animator;
import game.geometry.Matrix4fWrap;
import game.geometry.TransformWrap;
import logic.bbb.C1188RY;
import org.mozilla1.javascript.ScriptRuntime;
import quickhull3d.Point3d;
import quickhull3d.QuickHull3D;
import taikodom.render.RenderView;
import taikodom.render.camera.Camera;
import taikodom.render.enums.GeometryType;
import taikodom.render.scene.ObjectFactory;
import taikodom.render.scene.RExpandableMesh;
import taikodom.render.scene.RenderObject;
import taikodom.render.scene.Scene;
import taikodom.render.textures.Material;

import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.hl */
/* compiled from: a */
public class QuickHull3DTest extends Frame implements GLEventListener {

    /* renamed from: TT */
    public static final Vec3f f8032TT = new Vec3f(0.0f, 1.0f, 0.0f);
    /* renamed from: TV */
    public static final Vec3f f8034TV = new Vec3f(0.0f, 0.5f, 0.5f);
    /* renamed from: TW */
    public static final Vec3f f8035TW = new Vec3f(0.0f, 0.0f, 1.0f);
    /* renamed from: TX */
    public static final Vec3f f8036TX = new Vec3f(1.0f, 0.0f, 0.0f);
    /* renamed from: TU */
    private static final float f8033TU = 1.0f;
    private static final long serialVersionUID = -4185867234214293845L;
    /* renamed from: Uc */
    private final List<Vec3f> f8041Uc = new ArrayList();
    /* renamed from: Ud */
    private final List<Vec3f> f8042Ud = new ArrayList();
    /* access modifiers changed from: private */
    /* renamed from: Ue */
    private final List<Vec3f> f8043Ue = new ArrayList();
    /* access modifiers changed from: private */
    /* renamed from: Uf */
    private final List<Vec3f> f8044Uf = new ArrayList();
    /* renamed from: Ug */
    private final List<RenderObject> f8045Ug = new ArrayList();
    /* renamed from: Ua */
    public boolean f8039Ua = true;
    /* renamed from: Ub */
    public boolean f8040Ub = true;
    /* renamed from: Ui */
    public float f8047Ui = 0.0f;
    /* renamed from: TY */
    private QuickHull3D f8037TY;
    /* renamed from: TZ */
    private Material f8038TZ;
    /* access modifiers changed from: private */
    /* renamed from: Uh */
    private Vec3f f8046Uh = new Vec3f();
    private long before = System.nanoTime();

    /* renamed from: cW */
    private boolean f8048cW = true;
    private Camera camera;
    private GLCanvas canvas;

    /* renamed from: rv */
    private RenderView f8049rv;
    private Scene scene;

    public QuickHull3DTest() {
        super("EPA Debugger");
        setLayout(new BorderLayout());
        setSize(720, 640);
        setLocation(40, 40);
        setVisible(true);
        setupJOGL();
        addWindowListener(new C2629b());
        this.canvas.addKeyListener(new C2628a());
    }

    public static void main(String[] strArr) {
        new QuickHull3DTest().setVisible(true);
    }

    /* renamed from: a */
    public void mo19350a(Matrix4fWrap ajk, float f, float f2, float f3) {
        float f4 = f / 2.0f;
        float f5 = f2 / 2.0f;
        float f6 = f3 / 2.0f;
        for (Tuple3f c : new Vec3f[]{new Vec3f(f4, -f5, -f6), new Vec3f(f4, -f5, f6), new Vec3f(-f4, -f5, f6), new Vec3f(-f4, -f5, -f6), new Vec3f(f4, f5, -f6), new Vec3f(f4, f5, f6), new Vec3f(-f4, f5, f6), new Vec3f(-f4, f5, -f6), new Vec3f(-f4, -f5, -f6), new Vec3f(f4, -f5, -f6), new Vec3f(f4, f5, -f6), new Vec3f(-f4, f5, -f6), new Vec3f(-f4, -f5, f6), new Vec3f(f4, -f5, f6), new Vec3f(f4, f5, f6), new Vec3f(-f4, f5, f6)}) {
            this.f8042Ud.add(ajk.mo14004c(c));
        }
    }

    /* renamed from: a */
    public void mo19351a(Matrix4fWrap ajk, C1188RY ry, float f, float f2, float f3) {
        RExpandableMesh rExpandableMesh = new RExpandableMesh();
        for (Vec3f vec3f : ry.bsr()) {
            rExpandableMesh.addVertexWithColor(vec3f.x, vec3f.y, vec3f.z, f, f2, f3, 1.0f);
        }
        for (int addIndex : ry.bss()) {
            rExpandableMesh.addIndex(addIndex);
        }
        TransformWrap bcVar = new TransformWrap();
        bcVar.set(ajk);
        rExpandableMesh.setTransform(bcVar);
        this.f8045Ug.add(rExpandableMesh);
    }

    public void display(GLAutoDrawable gLAutoDrawable) {
        update();
        if (this.f8048cW) {
            Matrix4fWrap ajk = new Matrix4fWrap();
            Vec3f vec3f = new Vec3f(0.0f, 5.0f, 40.0f);
            ajk.rotateY(this.f8047Ui);
            this.camera.getTransform().set3x3(ajk);
            this.camera.getTransform().multiply3x3(vec3f, vec3f);
            this.camera.getTransform().setTranslation(vec3f);
            this.camera.calculateProjectionMatrix();
        }
        try {
            long nanoTime = System.nanoTime();
            this.f8049rv.render(this.scene, this.camera, (double) System.currentTimeMillis(), 1.0E-9f * ((float) (nanoTime - this.before)));
            this.before = nanoTime;
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    public void displayChanged(GLAutoDrawable gLAutoDrawable, boolean z, boolean z2) {
        this.f8049rv.setViewport(0, 0, gLAutoDrawable.getWidth(), gLAutoDrawable.getHeight());
    }

    /* renamed from: a */
    private void m32925a(RExpandableMesh rExpandableMesh, Vec3f vec3f, int i, float f, float f2, float f3) {
        rExpandableMesh.addVertexWithColor(vec3f.x - 1.0f, vec3f.y, vec3f.z, f, f2, f3, 1.0f);
        rExpandableMesh.addVertexWithColor(vec3f.x + 1.0f, vec3f.y, vec3f.z, f, f2, f3, 1.0f);
        rExpandableMesh.addIndex(i);
        rExpandableMesh.addIndex(i + 1);
        rExpandableMesh.addVertexWithColor(vec3f.x, vec3f.y - 1.0f, vec3f.z, f, f2, f3, 1.0f);
        rExpandableMesh.addVertexWithColor(vec3f.x, vec3f.y + 1.0f, vec3f.z, f, f2, f3, 1.0f);
        rExpandableMesh.addIndex(i + 2);
        rExpandableMesh.addIndex(i + 3);
        rExpandableMesh.addVertexWithColor(vec3f.x, vec3f.y, vec3f.z - 1.0f, f, f2, f3, 1.0f);
        rExpandableMesh.addVertexWithColor(vec3f.x, vec3f.y, vec3f.z + 1.0f, f, f2, f3, 1.0f);
        rExpandableMesh.addIndex(i + 4);
        rExpandableMesh.addIndex(i + 5);
    }

    /* renamed from: a */
    private void m32927a(RExpandableMesh rExpandableMesh, List<Vec3f> list, int i) {
        Vec3f vec3f = list.get(i);
        Vec3f vec3f2 = list.get(i + 1);
        Vec3f vec3f3 = list.get(i + 2);
        Vec3f vec3f4 = list.get(i + 3);
        rExpandableMesh.addVertexWithColor(vec3f.x, vec3f.y, vec3f.z, f8034TV.x, f8034TV.y, f8034TV.z, 1.0f);
        rExpandableMesh.addVertexWithColor(vec3f2.x, vec3f2.y, vec3f2.z, f8034TV.x, f8034TV.y, f8034TV.z, 1.0f);
        rExpandableMesh.addVertexWithColor(vec3f3.x, vec3f3.y, vec3f3.z, f8034TV.x, f8034TV.y, f8034TV.z, 1.0f);
        rExpandableMesh.addVertexWithColor(vec3f4.x, vec3f4.y, vec3f4.z, f8034TV.x, f8034TV.y, f8034TV.z, 1.0f);
        rExpandableMesh.addIndex(i);
        rExpandableMesh.addIndex(i + 1);
        rExpandableMesh.addIndex(i + 2);
        rExpandableMesh.addIndex(i + 3);
    }

    /* renamed from: a */
    private void m32926a(RExpandableMesh rExpandableMesh, Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, int i, float f, float f2, float f3) {
        rExpandableMesh.addVertexWithColor(vec3f.x, vec3f.y, vec3f.z, f, f2, f3, 1.0f);
        rExpandableMesh.addVertexWithColor(vec3f2.x, vec3f2.y, vec3f2.z, f, f2, f3, 1.0f);
        rExpandableMesh.addVertexWithColor(vec3f3.x, vec3f3.y, vec3f3.z, f, f2, f3, 1.0f);
        rExpandableMesh.addIndex(i);
        rExpandableMesh.addIndex(i + 1);
        rExpandableMesh.addIndex(i + 2);
    }

    /* renamed from: yc */
    public List<Vec3f> mo19361yc() {
        return this.f8041Uc;
    }

    /* renamed from: yd */
    public List<Vec3f> mo19362yd() {
        return this.f8042Ud;
    }

    public void init(GLAutoDrawable gLAutoDrawable) {
        try {
            this.f8049rv = new RenderView(gLAutoDrawable);
            this.scene = new Scene();
            System.loadLibrary("granny2");
            System.loadLibrary("utaikodom.render");
            this.f8038TZ = ObjectFactory.getDefault().createDefaultMaterial();
            this.camera = new Camera();
            this.camera.getTransform().setTranslation(ScriptRuntime.NaN, ScriptRuntime.NaN, 20.0d);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    public void reshape(GLAutoDrawable gLAutoDrawable, int i, int i2, int i3, int i4) {
        if (this.camera != null) {
            this.camera.setAspect(((float) i3) / ((float) i4));
        }
        this.f8049rv.setViewport(0, 0, i3, i4);
    }

    /* renamed from: a */
    public void mo19355a(QuickHull3D quickHull3D) {
        this.f8037TY = quickHull3D;
    }

    private void setupJOGL() {
        GLCapabilities gLCapabilities = new GLCapabilities();
        gLCapabilities.setDoubleBuffered(true);
        gLCapabilities.setHardwareAccelerated(true);
        gLCapabilities.setSampleBuffers(true);
        gLCapabilities.setHardwareAccelerated(true);
        gLCapabilities.setNumSamples(8);
        this.canvas = new GLCanvas(gLCapabilities);
        this.canvas.enableInputMethods(true);
        this.canvas.addGLEventListener(this);
        add(this.canvas, "Center");
        Animator animator = new Animator(this.canvas);
        animator.setRunAsFastAsPossible(false);
        animator.start();
    }

    private void update() {
        this.scene.removeAllChildren();
        if (this.f8039Ua) {
            m32931ye();
        }
        m32934yh();
        m32935yi();
        m32933yg();
        if (this.f8040Ub) {
            m32932yf();
        }
    }

    /* renamed from: ye */
    private void m32931ye() {
        if (this.f8037TY != null) {
            Point3d[] vertices = this.f8037TY.getVertices();
            int[][] faces = this.f8037TY.getFaces();
            RExpandableMesh rExpandableMesh = new RExpandableMesh();
            rExpandableMesh.setMaterial(this.f8038TZ);
            if (this.f8037TY.getFaces()[0].length == 4) {
                rExpandableMesh.getMesh().setGeometryType(GeometryType.QUADS);
            } else {
                rExpandableMesh.getMesh().setGeometryType(GeometryType.TRIANGLES);
            }
            for (Point3d point3d : vertices) {
                rExpandableMesh.addVertexWithColor((float) point3d.x, (float) point3d.y, (float) point3d.z, 1.0f, 1.0f, 0.0f, 1.0f);
            }
            for (int[] iArr : faces) {
                for (int addIndex : faces[r2]) {
                    rExpandableMesh.addIndex(addIndex);
                }
            }
            this.scene.addChild(rExpandableMesh);
        }
    }

    /* renamed from: yf */
    private void m32932yf() {
        if (this.f8045Ug.size() > 0) {
            for (RenderObject next : this.f8045Ug) {
                next.setMaterial(this.f8038TZ);
                this.scene.addChild(next);
            }
        }
    }

    /* renamed from: yg */
    private void m32933yg() {
        if (this.f8041Uc.size() > 0) {
            RExpandableMesh rExpandableMesh = new RExpandableMesh();
            rExpandableMesh.setMaterial(this.f8038TZ);
            int i = 0;
            for (Vec3f a : this.f8041Uc) {
                m32925a(rExpandableMesh, a, i, f8032TT.x, f8032TT.y, f8032TT.z);
                i += 6;
            }
            m32925a(rExpandableMesh, this.f8046Uh, i, 1.0f, 0.0f, 1.0f);
            rExpandableMesh.getMesh().setGeometryType(GeometryType.LINES);
            this.scene.addChild(rExpandableMesh);
        }
    }

    /* renamed from: yh */
    private void m32934yh() {
        int size = this.f8042Ud.size();
        if (size > 0) {
            RExpandableMesh rExpandableMesh = new RExpandableMesh();
            rExpandableMesh.setMaterial(this.f8038TZ);
            for (int i = 0; i < size; i += 4) {
                m32927a(rExpandableMesh, this.f8042Ud, i);
            }
            rExpandableMesh.getMesh().setGeometryType(GeometryType.QUADS);
            this.scene.addChild(rExpandableMesh);
        }
    }

    /* renamed from: yi */
    private void m32935yi() {
        int size = this.f8043Ue.size();
        if (size > 0) {
            RExpandableMesh rExpandableMesh = new RExpandableMesh();
            rExpandableMesh.setMaterial(this.f8038TZ);
            for (int i = 0; i < size; i += 3) {
                Vec3f vec3f = this.f8044Uf.get(i);
                float f = vec3f.x;
                float f2 = vec3f.y;
                float f3 = vec3f.z;
                m32926a(rExpandableMesh, this.f8043Ue.get(i), this.f8043Ue.get(i + 1), this.f8043Ue.get(i + 2), i, f, f2, f3);
            }
            this.scene.addChild(rExpandableMesh);
        }
    }

    /* renamed from: a */
    public void mo19354a(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
        this.f8043Ue.add(vec3f);
        this.f8043Ue.add(vec3f2);
        this.f8043Ue.add(vec3f3);
        this.f8044Uf.add(vec3f4);
        this.f8044Uf.add(new Vec3f());
        this.f8044Uf.add(new Vec3f());
    }

    /* renamed from: a */
    public void mo19353a(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, float f, float f2, float f3) {
        mo19354a(new Vec3f((Vector3f) vec3f), new Vec3f((Vector3f) vec3f2), new Vec3f((Vector3f) vec3f3), new Vec3f(f, f2, f3));
    }

    /* renamed from: a */
    public void mo19352a(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        mo19354a(new Vec3f((Vector3f) vec3f), new Vec3f((Vector3f) vec3f2), new Vec3f((Vector3f) vec3f3), f8035TW);
    }

    /* renamed from: b */
    public void mo19356b(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        mo19354a(new Vec3f((Vector3f) vec3f), new Vec3f((Vector3f) vec3f2), new Vec3f((Vector3f) vec3f3), f8036TX);
    }

    /* renamed from: yj */
    public void mo19363yj() {
        this.f8043Ue.clear();
        this.f8044Uf.clear();
    }

    /* renamed from: a.hl$b */
    /* compiled from: a */
    class C2629b extends WindowAdapter {
        C2629b() {
        }

        public void windowClosed(WindowEvent windowEvent) {
            System.exit(0);
        }

        public void windowClosing(WindowEvent windowEvent) {
            System.exit(0);
        }
    }

    /* renamed from: a.hl$a */
    class C2628a implements KeyListener {
        C2628a() {
        }

        public void keyPressed(KeyEvent keyEvent) {
            if (keyEvent.getKeyChar() == 'x') {
                QuickHull3DTest.this.f8047Ui = (QuickHull3DTest.this.f8047Ui + 2.0f) % 360.0f;
            }
            if (keyEvent.getKeyChar() == 'z') {
                QuickHull3DTest.this.f8047Ui = (QuickHull3DTest.this.f8047Ui - 2.0f) % 360.0f;
            }
        }

        public void keyReleased(KeyEvent keyEvent) {
            boolean z = false;
            if (keyEvent.getKeyChar() == 'c') {
                QuickHull3DTest.this.f8039Ua = !QuickHull3DTest.this.f8039Ua;
            }
            if (keyEvent.getKeyChar() == 'v') {
                QuickHull3DTest hlVar = QuickHull3DTest.this;
                if (!QuickHull3DTest.this.f8040Ub) {
                    z = true;
                }
                hlVar.f8040Ub = z;
            }
        }

        public void keyTyped(KeyEvent keyEvent) {
        }
    }
}
