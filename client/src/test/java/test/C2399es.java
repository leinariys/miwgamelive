package test;

import org.apache.ecs.xml.XML;
import org.mozilla1.classfile.C0147Bi;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import p001a.C0599IT;
import p001a.C3095nj;
import p001a.aBG;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.util.*;

/* renamed from: test.a.es */
/* compiled from: test.a */
public class C2399es {
    List<aBG> gpv = new ArrayList();
    Map<String, String> gpw = new HashMap();

    public static void main(String[] strArr) {
        File file = new File(".");
        File file2 = new File("./converted");
        int i = 0;
        while (i < strArr.length) {
            String str = strArr[i];
            if ("-outdir".equals(str)) {
                i++;
                file2 = new File(strArr[i]);
            } else if ("-basedir".equals(str)) {
                i++;
                file = new File(strArr[i]);
            } else if ("-help".equals(str) || "--help".equals(str)) {
                System.out.println("Usage  proconverter  -rootdir <rootdir>  -outdir <outdir> ");
            }
            i++;
        }
        C2399es esVar = new C2399es();
        esVar.m30082c(file, file);
        esVar.m30083j(file2);
    }

    /* renamed from: j */
    private void m30083j(File file) {
        String str;
        for (aBG next : this.gpv) {
            for (C0599IT next2 : next.getItems()) {
                String str2 = this.gpw.get(next2.getName());
                if (str2 != null) {
                    System.out.println("WARNING: reapeated item " + next2.getName() + " at " + str2 + " and " + next.cJo());
                } else {
                    this.gpw.put(next2.getName(), next.cJo());
                }
            }
        }
        String[] strArr = {"mark", "markOut", "lockUpperLeft", "lockUpperRight", "lockLowerLeft", "lockLowerRight", "child", "transform", "texture"};
        for (aBG next3 : this.gpv) {
            for (C0599IT next4 : next3.getItems()) {
                for (String property : strArr) {
                    String property2 = next4.getProperty(property);
                    if (property2 != null) {
                        for (String str3 : property2.split(" ")) {
                            String str4 = this.gpw.get(str3);
                            if (str4 == null) {
                                System.out.println("Warning: referenced item not found: " + next4);
                            } else if (!str4.equals(next3.cJo())) {
                                next3.mo7828dS(str4);
                            }
                        }
                    }
                }
            }
        }
        HashMap hashMap = new HashMap();
        hashMap.put("billboard", "RBillboard");
        hashMap.put("trail", "RTrail");
        hashMap.put("group", "RGroup");
        hashMap.put("mesh", "RMesh");
        hashMap.put("planet", "RPlanet");
        hashMap.put("light", "RLight");
        hashMap.put("panorama", "RPanorama");
        hashMap.put("spacedust", "RSpacedust");
        hashMap.put("explosion", "RExplosion");
        HashMap hashMap2 = new HashMap();
        hashMap2.put("num_atoms", "numBillboards");
        hashMap2.put("texture", "material");
        hashMap2.put("max_distance", "regionSize");
        HashSet hashSet = new HashSet();
        hashSet.add("classtype");
        hashSet.add("name");
        hashSet.add("bb2_max_size");
        hashSet.add("bb2_percent");
        hashSet.add("bb1_percent");
        for (aBG next5 : this.gpv) {
            XML xml = new XML("root");
            if (next5.cJp().size() > 0) {
                XML xml2 = new XML("includes");
                xml.addElement(xml2);
                for (String next6 : next5.cJp()) {
                    if (next6 != null) {
                        XML xml3 = new XML("include");
                        xml2.addElement(xml3);
                        xml3.addAttribute("file", next6);
                    }
                }
            }
            XML xml4 = new XML("objects");
            xml.addElement(xml4);
            for (C0599IT next7 : next5.getItems()) {
                String str5 = (String) hashMap.get(next7.aXg());
                if (str5 == null) {
                    System.out.println("ERROR: tag mapping not available for classtype: " + next7.aXg());
                } else {
                    XML xml5 = new XML(str5);
                    xml4.addElement(xml5);
                    xml5.addAttribute("name", next7.getName());
                    for (String next8 : next7.getProperties().keySet()) {
                        if (!hashSet.contains(next8)) {
                            if (hashMap2.containsKey(next8)) {
                                str = (String) hashMap2.get(next8);
                            } else {
                                str = next8;
                            }
                            xml5.addAttribute(str, next7.getProperty(next8));
                        }
                    }
                }
            }
            File file2 = new File(file, next5.cJo());
            String jE = m30084jE(xml.toString());
            try {
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file2));
                bufferedWriter.write(jE);
                bufferedWriter.flush();
                bufferedWriter.close();
            } catch (IOException e) {
                System.out.println("ERROR: error writing " + file2);
            }
        }
    }

    /* renamed from: jE */
    private String m30084jE(String str) {
        try {
            SAXParser newSAXParser = SAXParserFactory.newInstance().newSAXParser();
            StringWriter stringWriter = new StringWriter(str.length());
            newSAXParser.parse(new InputSource(new StringReader(str)), new C3095nj(stringWriter));
            return stringWriter.toString();
        } catch (IOException | ParserConfigurationException | SAXException e) {
            return str;
        }
    }

    /* renamed from: jF */
    private XML m30085jF(String str) {
        return null;
    }

    /* renamed from: c */
    private void m30082c(File file, File file2) {
        File[] listFiles = file2.listFiles(new C2400a());
        String absolutePath = file.getAbsolutePath();
        for (File file3 : listFiles) {
            try {
                aBG k = mo18228k(file3);
                k.mo7831kZ(file3.getAbsolutePath().substring(absolutePath.length()).replace(File.separatorChar, C0147Bi.cla));
                this.gpv.add(k);
            } catch (IOException e) {
                System.out.println("Error: problems loading file: " + file3);
            }
        }
        for (File c : file2.listFiles(new C2401b())) {
            m30082c(file, c);
        }
    }

    /* renamed from: k */
    public aBG mo18228k(File file) {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        aBG abg = new aBG();
        abg.setFile(file);
        C0599IT it = null;
        int i = 0;
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                break;
            }
            i++;
            String trim = readLine.trim();
            if (!trim.startsWith("#") && trim.length() != 0) {
                if (trim.startsWith("[")) {
                    if (!trim.endsWith("]")) {
                        System.err.println("WARNING: Badly formatted config section on " + file + ";" + Integer.toString(i));
                        trim.concat("]");
                    }
                    String substring = trim.substring(1, trim.length() - 1);
                    if (it != null) {
                        abg.mo7825a(it);
                    }
                    it = new C0599IT();
                    it.setProperty("name", substring);
                } else {
                    int indexOf = trim.indexOf("=");
                    if (indexOf < 0) {
                        System.err.println("WARNING: Missing '='" + file + ":" + Integer.toString(i));
                    } else {
                        String substring2 = trim.substring(0, indexOf);
                        if (substring2.length() == 0) {
                            System.err.println("WARNING: Zero-length key on " + file + ":" + Integer.toString(i));
                        } else {
                            String substring3 = trim.substring(indexOf + 1);
                            if (it == null) {
                                System.err.println("WARNING: Found an entry before test.a section on " + file + ":" + Integer.toString(i));
                            } else {
                                it.setProperty(substring2, substring3);
                            }
                        }
                    }
                }
            }
        }
        if (it != null) {
            abg.mo7825a(it);
        }
        return abg;
    }

    /* renamed from: test.a.es$test.a */
    class C2400a implements FilenameFilter {
        C2400a() {
        }

        public boolean accept(File file, String str) {
            return str.endsWith(".pro");
        }
    }

    /* renamed from: test.a.es$b */
    /* compiled from: test.a */
    class C2401b implements FileFilter {
        C2401b() {
        }

        public boolean accept(File file) {
            return file.isDirectory();
        }
    }
}
