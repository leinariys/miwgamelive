package test;

import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.DrawContext;
import taikodom.render.RenderView;
import taikodom.render.graphics2d.RGraphics2;
import taikodom.render.textures.RenderTarget;
import taikodom.render.textures.SimpleTexture;

import javax.media.opengl.GL;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.vecmath.Vector2f;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

/* renamed from: a.cO */
/* compiled from: a */
public class GLCanvasTestAdditional {
    /* access modifiers changed from: private */
    public static final Logger logger = Logger.getLogger(GLCanvasTestAdditional.class.getName());
    private static final int fKF = 4;
    private static final int fKx = 300;

    private static int fKy = 0;
    /* access modifiers changed from: private */
    public final C2165k fKN = new C2165k(this, (C2165k) null);
    /* access modifiers changed from: private */
    public final Frame frame = new C2161g();
    /* access modifiers changed from: private */
    public JDesktopPane desktop;
    /* access modifiers changed from: private */
    public RGraphics2 fKu;
    /* access modifiers changed from: private */
    public boolean fKv = false;
    /* access modifiers changed from: private */
    public boolean initialized;
    private int clickCount = 0;
    private Component fKA;
    private int fKB;
    private int fKC = 0;
    private int fKD = 0;
    private long fKE = 0;
    private Vector2f fKG = new Vector2f();
    private boolean fKH = true;
    private boolean fKI = false;
    private int fKJ = -1;
    private int fKK = -1;
    private int fKL = -1;
    private int fKM = -1;
    private Component fKO;
    private long fKP;
    private long fKQ;
    private Component fKz;
    private int height;
    private RenderTarget renderTarget;
    private SimpleTexture texture;
    private int width;

    public GLCanvasTestAdditional(String str) {
        this.frame.setFocusableWindowState(false);
        Frame frame = this.frame;
        this.frame.setUndecorated(true);
        m28329d((Container) frame);
        this.desktop = new C2160f();
        new C2166l((C2166l) null).m28351b(this.desktop);
        this.desktop.setBackground(new Color(0, 0, 0, 0));
        this.desktop.setFocusable(true);
        this.desktop.addMouseListener(new C2163i());
        if (System.getProperty("os.name").toLowerCase().indexOf("mac") < 0) {
            JInternalFrame jInternalFrame = new JInternalFrame();
            jInternalFrame.setUI(new C2162h(jInternalFrame));
            jInternalFrame.setOpaque(false);
            jInternalFrame.setBackground((Color) null);
            jInternalFrame.getContentPane().setLayout(new BorderLayout());
            jInternalFrame.getContentPane().add(this.desktop, "Center");
            jInternalFrame.setVisible(true);
            jInternalFrame.setBorder((Border) null);
            frame.add(jInternalFrame);
        } else {
            frame.add(this.desktop, "Center");
        }
        this.frame.setSize(100, 100);
        this.frame.validate();
        RepaintManager.currentManager(this.desktop).setDoubleBufferingEnabled(false);
    }

    /* renamed from: d */
    private static void m28329d(Container container) {
        if (container != null) {
            container.setBackground((Color) null);
            if (container instanceof JComponent) {
                ((JComponent) container).setOpaque(false);
            }
            m28329d(container.getParent());
        }
    }

    /* renamed from: g */
    private static int calculation(int i, boolean z) {
        int i2 = 1;
        while (i2 < i) {
            i2 <<= 1;
        }
        return i2;
    }

    public boolean caG() {
        return this.fKv;
    }

    /* renamed from: eT */
    public void mo17585eT(boolean z) {
        this.fKv = z;
        this.frame.setVisible(z);
        this.frame.repaint();
    }

    /* renamed from: c */
    public void setSize(int width, int height, boolean z) {
        if (this.initialized) {
            throw new IllegalStateException("may be called only once");
        }
        this.width = calculation(width, z);
        this.height = calculation(height, z);
    }

    /* access modifiers changed from: protected */
    public void caH() {
    }

    /* renamed from: a */
    public void mo17572a(char c, int i, boolean z) {
        try {
            SwingUtilities.invokeAndWait(new C2156b(i, z, c));
        } catch (InterruptedException e) {
            logger.logp(Level.SEVERE, getClass().toString(), "onKey(character, keyCode, pressed)", "Exception", e);
        } catch (InvocationTargetException e2) {
            logger.logp(Level.SEVERE, getClass().toString(), "onKey(character, keyCode, pressed)", "Exception", e2);
        }
    }

    /* renamed from: a */
    public void mo17574a(int i, boolean z, int i2, int i3) {
        mo17573a(i2, i3, this.fKG);
        try {
            SwingUtilities.invokeAndWait(new C2157c((int) this.fKG.x, (int) this.fKG.y, z, i));
        } catch (InterruptedException e) {
            logger.logp(Level.SEVERE, getClass().toString(), "onButton(swingButton, pressed, x, y)", "Exception", e);
        } catch (InvocationTargetException e2) {
            logger.logp(Level.SEVERE, getClass().toString(), "onButton(swingButton, pressed, x, y)", "Exception", e2);
        }
    }

    /* renamed from: r */
    public void mo17590r(int i, int i2, int i3) {
        mo17573a(i2, i3, this.fKG);
        try {
            SwingUtilities.invokeAndWait(new C2158d(i, (int) this.fKG.x, (int) this.fKG.y));
        } catch (InterruptedException e) {
            logger.logp(Level.SEVERE, getClass().toString(), "onWheel(wheelDelta, x, y)", "Exception", e);
        } catch (InvocationTargetException e2) {
            logger.logp(Level.SEVERE, getClass().toString(), "onWheel(wheelDelta, x, y)", "Exception", e2);
        }
    }

    /* renamed from: c */
    public void mo17577c(int i, int i2, int i3, int i4) {
        mo17573a(i3, i4, this.fKG);
        try {
            SwingUtilities.invokeAndWait(new C2159e((int) this.fKG.x, (int) this.fKG.y));
        } catch (InterruptedException e) {
            logger.logp(Level.SEVERE, getClass().toString(), "onMove(xDelta, yDelta, newX, newY)", "Exception", e);
        } catch (InvocationTargetException e2) {
            logger.logp(Level.SEVERE, getClass().toString(), "onMove(xDelta, yDelta, newX, newY)", "Exception", e2);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m28319a(int i, boolean z, char c) {
        char c2;
        if (i != 0) {
            JDesktopPane focusOwner = (JDesktopPane) getFocusOwner();
            if (focusOwner == null) {
                focusOwner = this.desktop;
            }
            if (c == 0) {
                c2 = 65535;
            } else {
                c2 = c;
            }
            if (focusOwner != null) {
                if (z) {
                    m28324a(focusOwner, new KeyEvent(focusOwner, 401, System.currentTimeMillis(), m28333qT(-1), i, c2));
                    if (c2 != 65535) {
                        m28324a(focusOwner, new KeyEvent(focusOwner, 400, System.currentTimeMillis(), m28333qT(-1), 0, c2));
                    }
                }
                if (!z) {
                    m28324a(focusOwner, new KeyEvent(focusOwner, 402, System.currentTimeMillis(), m28333qT(-1), i, c2));
                }
            }
        }
    }

    /* renamed from: a */
    private void m28324a(Component component, AWTEvent aWTEvent) {
        if (caJ() != null && !SwingUtilities.isDescendingFrom(component, caJ())) {
            return;
        }
        if (!SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("not in swing thread!");
        }
        component.dispatchEvent(aWTEvent);
    }

    /* access modifiers changed from: private */
    /* renamed from: s */
    public void m28336s(int i, int i2, int i3) {
        Component a;
        if (this.fKz != null) {
            a = this.fKz;
        } else {
            a = m28317a(i2, i3, (Component) this.desktop, false);
        }
        if (a == null) {
            a = this.desktop;
        }
        Point convertPoint = convertPoint(this.desktop, i2, i3, (Container) a);
        m28324a(a, new MouseWheelEvent(a, 507, System.currentTimeMillis(), m28333qT(-1), convertPoint.x, convertPoint.y, 1, false, 0, Math.abs(i), i > 0 ? -1 : 1));
    }

    private Point convertPoint(Component component, int i, int i2, Container container) {
        if (this.fKH) {
            try {
                return SwingUtilities.convertPoint(component, i, i2, container);
            } catch (InternalError e) {
                this.fKH = false;
            }
        }
        if (container != null) {
            while (container != component) {
                i -= container.getX();
                i2 -= container.getY();
                if (container.getParent() == null) {
                    break;
                }
                container = container.getParent();
            }
        }
        return new Point(i, i2);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m28318a(int i, int i2, boolean z, int i3) {
        int i4;
        int i5;
        int i6;
        Component a = m28317a(i, i2, (Component) this.desktop, false);
        if (i3 > 0) {
            if (z) {
                i6 = 501;
            } else {
                i6 = 502;
            }
            i5 = i6;
        } else {
            if (m28334qU(0) == 0) {
                i4 = 503;
            } else {
                i4 = 506;
            }
            i5 = i4;
        }
        long currentTimeMillis = System.currentTimeMillis();
        if (this.fKz != a) {
            while (this.fKz != null && (a == null || !SwingUtilities.isDescendingFrom(a, this.fKz))) {
                m28323a(this.fKz, m28333qT(i3), convertPoint(this.desktop, i, i2, (Container) this.fKz));
                this.fKz = this.fKz.getParent();
            }
            Point convertPoint = convertPoint(this.desktop, i, i2, (Container) this.fKz);
            if (this.fKz == null) {
                this.fKz = this.desktop;
            }
            m28325a(a, this.fKz, m28333qT(i3), convertPoint);
            this.fKz = a;
            this.fKC = Integer.MIN_VALUE;
            this.fKD = Integer.MIN_VALUE;
            this.fKE = 0;
        }
        if (a != null) {
            boolean z2 = false;
            if (i3 > 0) {
                if (z) {
                    this.fKA = a;
                    this.fKB = i3;
                    this.fKC = i;
                    this.fKD = i2;
                    mo17587n(m28317a(i, i2, (Component) this.desktop, true));
                } else if (this.fKB == i3 && this.fKA != null) {
                    a = this.fKA;
                    this.fKA = null;
                    if (Math.abs(this.fKC - i) <= 4 && Math.abs(this.fKD - i2) < 4) {
                        if (this.fKE + 300 > currentTimeMillis) {
                            this.clickCount++;
                        } else {
                            this.clickCount = 1;
                        }
                        z2 = true;
                        this.fKE = currentTimeMillis;
                    }
                    this.fKC = Integer.MIN_VALUE;
                    this.fKD = Integer.MIN_VALUE;
                }
            } else if (this.fKA != null) {
                a = this.fKA;
            }
            Point convertPoint2 = convertPoint(this.desktop, i, i2, (Container) a);
            m28324a(a, new MouseEvent(a, i5, currentTimeMillis, m28333qT(i3), convertPoint2.x, convertPoint2.y, this.clickCount, i3 == 2 && z, i3 >= 0 ? i3 : 0));
            if (z2) {
                Component a2 = m28317a(i, i2, (Component) this.desktop, true);
                Point convertPoint3 = convertPoint(this.desktop, i, i2, (Container) a2);
                m28324a(a2, new MouseEvent(a2, 500, currentTimeMillis, m28333qT(i3), convertPoint3.x, convertPoint3.y, this.clickCount, false, i3));
            }
        } else if (z) {
            mo17587n((Component) null);
        }
    }

    /* renamed from: n */
    public void mo17587n(Component component) {
        if (component == null || component.isFocusable()) {
            for (Component component2 = component; component2 != null; component2 = component2.getParent()) {
                if (component2 instanceof JInternalFrame) {
                    try {
                        ((JInternalFrame) component2).setSelected(true);
                    } catch (PropertyVetoException e) {
                        logger.logp(Level.SEVERE, getClass().toString(), "setFocusOwner(Component comp)", "Exception", e);
                    }
                }
            }
            this.frame.setFocusableWindowState(true);
            Component focusOwner = getFocusOwner();
            if (component == this.desktop) {
                component = null;
            }
            if (focusOwner != component) {
                if (focusOwner != null) {
                    m28324a(focusOwner, new FocusEvent(focusOwner, 1005, false, component));
                }
                KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
                if (component != null) {
                    m28324a(component, new FocusEvent(component, 1004, false, focusOwner));
                }
            }
            this.frame.setFocusableWindowState(false);
        }
        this.fKI = component == null;
    }

    /* renamed from: qT */
    private int m28333qT(int i) {
        return 0 | m28334qU(i);
    }

    /* renamed from: qU */
    private int m28334qU(int i) {
        return 0;
    }

    /* renamed from: a */
    public void mo17573a(int i, int i2, Vector2f vector2f) {
        if (this.fKJ == i && this.fKL == i2) {
            vector2f.x = (float) this.fKK;
            vector2f.y = (float) this.fKM;
        } else {
            this.fKJ = i;
            this.fKL = i2;
        }
        this.fKM = i2;
        this.fKK = i;
        vector2f.set((float) i, (float) i2);
    }

    /* renamed from: ac */
    public Component mo17576ac(int i, int i2) {
        JDesktopPane a = (JDesktopPane) m28317a(i, i2, (Component) this.desktop, true);
        if (a != this.desktop) {
            return a;
        }
        return null;
    }

    /* renamed from: a */
    private Component m28317a(int i, int i2, Component component, boolean z) {
        Component component2;
        Component component3;
        if (!z || !(component instanceof JRootPane)) {
            component2 = component;
        } else {
            component2 = ((JRootPane) component).getContentPane();
        }
        if (!component2.contains(i, i2)) {
            component3 = null;
        } else {
            synchronized (component2.getTreeLock()) {
                if (component2 instanceof Container) {
                    Container container = (Container) component2;
                    int componentCount = container.getComponentCount();
                    int i3 = 0;
                    while (true) {
                        if (i3 < componentCount) {
                            Component component4 = container.getComponent(i3);
                            if (component4 != null && component4.isVisible() && component4.contains(i - component4.getX(), i2 - component4.getY())) {
                                component3 = component4;
                                break;
                            }
                            i3++;
                        } else {
                            break;
                        }
                    }
                }
                component3 = component2;
            }
        }
        if (component3 != null) {
            if ((component2 instanceof JTabbedPane) && component3 != component2) {
                component3 = ((JTabbedPane) component2).getSelectedComponent();
            }
            i -= component3.getX();
            i2 -= component3.getY();
        }
        if (component3 == component2 || component3 == null) {
            return component3;
        }
        return m28317a(i, i2, component3, z);
    }

    /* renamed from: a */
    private void m28325a(Component component, Component component2, int i, Point point) {
        if (component != null && component != component2) {
            m28325a((Component) component.getParent(), component2, i, point);
            Point convertPoint = convertPoint(component2, point.x, point.y, (Container) component);
            Component component3 = component;
            m28324a(component3, new MouseEvent(component, 504, System.currentTimeMillis(), i, convertPoint.x, convertPoint.y, 0, false, 0));
        }
    }

    /* renamed from: a */
    private void m28323a(Component component, int i, Point point) {
        m28324a(component, new MouseEvent(component, 505, System.currentTimeMillis(), i, point.x, point.y, 1, false, 0));
    }

    /* renamed from: a */
    public void mo17575a(Renderer renderer) {
    }

    public JDesktopPane caI() {
        return this.desktop;
    }

    public Component getFocusOwner() {
        if (!this.fKI) {
            return this.frame.getFocusOwner();
        }
        return null;
    }

    /* renamed from: qV */
    private int m28335qV(int i) {
        switch (i) {
            case 0:
                return 1;
            case 1:
                return 2;
            case 2:
                return 3;
            default:
                return 0;
        }
    }

    public Component caJ() {
        return this.fKO;
    }

    /* renamed from: o */
    public void mo17588o(Component component) {
        this.fKO = component;
    }

    public void dispose() {
        if (this.desktop != null) {
            try {
                SwingUtilities.invokeAndWait(new C2155a());
            } catch (InterruptedException e) {
                logger.logp(Level.SEVERE, getClass().toString(), "dispose()", "Exception", e);
            } catch (InvocationTargetException e2) {
                logger.logp(Level.SEVERE, getClass().toString(), "dispose()", "Exception", e2);
            }
            this.desktop = null;
            fKy--;
            if (fKy == 0) {
                PopupFactory.setSharedInstance(new PopupFactory());
            }
        }
    }

    /* renamed from: r */
    public void mo17589r(float f, float f2) {
        throw new UnsupportedOperationException("resizing JMEDesktop not yet implemented!");
    }

    public void draw(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        RenderView renderView = drawContext.getRenderView();
        gl.glMatrixMode(5888);
        gl.glLoadIdentity();
        gl.glMatrixMode(5889);
        gl.glLoadIdentity();
        gl.glViewport(0, 0, this.width, renderView.getViewport().height);
        gl.glDisable(2929);
        gl.glOrtho(ScriptRuntime.NaN, (double) renderView.getViewport().width, (double) renderView.getViewport().height, ScriptRuntime.NaN, -1.0d, 1.0d);
        gl.glMatrixMode(5888);
        gl.glCullFace(0);
        gl.glBlendFunc(1, 771);
        gl.glDisableClientState(32884);
        gl.glDisableClientState(32888);
        gl.glDisableClientState(32886);
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        if (this.fKu == null) {
            this.fKu = new RGraphics2(drawContext);
        }
        this.fKu.reset();
        this.desktop.paint(this.fKu.create());
        this.fKu.getGuiScene().draw();
    }

    /* renamed from: a.cO$m */
    /* compiled from: a */
    private static class C2168m extends Popup {
        private static final Integer dIb = Integer.MAX_VALUE;
        private final JComponent dIc;
        JPanel dve = new JPanel(new BorderLayout());

        public C2168m(JComponent jComponent) {
            this.dIc = jComponent;
            new C2166l((C2166l) null).m28351b(this.dve);
        }

        /* renamed from: a */
        public void mo17609a(Component component, Component component2, int i, int i2) {
            this.dve.setVisible(false);
            this.dIc.add(this.dve, dIb);
            this.dve.removeAll();
            this.dve.add(component2, "Center");
            if (component2 instanceof JComponent) {
                ((JComponent) component2).setDoubleBuffered(false);
            }
            this.dve.setSize(this.dve.getPreferredSize());
            int min = Math.min(i2, this.dIc.getHeight() - this.dve.getHeight());
            this.dve.setLocation(Math.min(i, this.dIc.getWidth() - this.dve.getWidth()), min);
            component2.invalidate();
            this.dve.validate();
        }

        public void show() {
            this.dve.setVisible(true);
        }

        public void hide() {
            Rectangle bounds = this.dve.getBounds();
            this.dIc.remove(this.dve);
            this.dIc.repaint(bounds);
        }
    }

    /* renamed from: a.cO$j */
    /* compiled from: a */
    private static class C2164j extends PopupFactory {
        private final PopupFactory aUu = new PopupFactory();

        private C2164j() {
        }

        public Popup getPopup(Component component, Component component2, int i, int i2) {

            while (!(component instanceof JDesktopPane)) {
                component = component.getParent();
                if (component == null) {
                    //Не удалось создать всплывающее окно jME, всплывающее окно по умолчанию создано - рабочий стол не найден в иерархии компонентов
                    GLCanvasTestAdditional.logger.warning("jME Popup creation failed, default popup created - desktop not found in component hierarchy of " + component);
                    return this.aUu.getPopup(component, component2, i, i2);
                }
            }
            Popup mVar = new C2168m((JComponent) component);
            ((C2168m) mVar).mo17609a(component, component2, i, i2);
            return mVar;
        }
    }

    /* renamed from: a.cO$l */
    /* compiled from: a */
    private static class C2166l implements ContainerListener {
        private C2166l() {
        }

        /* synthetic */ C2166l(C2166l lVar) {
            this();
        }

        public void componentAdded(ContainerEvent containerEvent) {
            m28353j(containerEvent.getChild());
        }

        /* renamed from: j */
        private void m28353j(Component component) {
            if (component instanceof Container) {
                Container container = (Container) component;
                m28351b(container);
                container.addContainerListener(this);
            }
            if (component instanceof JScrollPane) {
                m28350a(((JScrollPane) component).getViewport());
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public void m28351b(Container container) {
            container.addContainerListener(this);
            for (int i = 0; i < container.getComponentCount(); i++) {
                m28353j(container.getComponent(i));
            }
        }

        /* renamed from: c */
        private void m28352c(Container container) {
            container.removeContainerListener(this);
            for (int i = 0; i < container.getComponentCount(); i++) {
                m28354k(container.getComponent(i));
            }
        }

        /* renamed from: a */
        private void m28350a(JViewport jViewport) {
            int i = 0;
            while (i < jViewport.getChangeListeners().length) {
                if (!(jViewport.getChangeListeners()[i] instanceof C2167a)) {
                    i++;
                } else {
                    return;
                }
            }
            jViewport.addChangeListener(new C2167a(jViewport));
        }

        public void componentRemoved(ContainerEvent containerEvent) {
            m28354k(containerEvent.getChild());
        }

        /* renamed from: k */
        private void m28354k(Component component) {
            if (component instanceof Container) {
                m28352c((Container) component);
            }
        }

        /* renamed from: a.cO$l$a */
        private static class C2167a implements ChangeListener {
            private final Component component;

            public C2167a(Component component2) {
                this.component = component2;
            }

            public void stateChanged(ChangeEvent changeEvent) {
                this.component.repaint();
            }
        }
    }

    /* renamed from: a.cO$g */
    /* compiled from: a */
    class C2161g extends Frame {


        C2161g() {
        }

        public boolean isShowing() {
            return true;
        }

        public boolean isVisible() {
            if (GLCanvasTestAdditional.this.frame.isFocusableWindow() && new Throwable().getStackTrace()[1].getMethodName().startsWith("requestFocus")) {
                return false;
            }
            if (GLCanvasTestAdditional.this.initialized || super.isVisible()) {
                return true;
            }
            return false;
        }

        public Graphics getGraphics() {
            if (GLCanvasTestAdditional.this.fKv) {
                return super.getGraphics();
            }
            if (GLCanvasTestAdditional.this.fKu == null) {
                return super.getGraphics();
            }
            return GLCanvasTestAdditional.this.fKu.create();
        }

        public boolean isFocused() {
            return true;
        }
    }

    /* renamed from: a.cO$f */
    /* compiled from: a */
    class C2160f extends JDesktopPane {


        C2160f() {
        }

        public void paint(Graphics graphics) {
            if (!GLCanvasTestAdditional.this.caG()) {
                graphics.clearRect(0, 0, getWidth(), getHeight());
            }
            super.paint(graphics);
        }

        public boolean isOptimizedDrawingEnabled() {
            return false;
        }
    }

    /* renamed from: a.cO$i */
    /* compiled from: a */
    class C2163i extends MouseAdapter {
        C2163i() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            GLCanvasTestAdditional.this.desktop.requestFocusInWindow();
        }
    }

    /* renamed from: a.cO$h */
    /* compiled from: a */
    class C2162h extends BasicInternalFrameUI {
        C2162h(JInternalFrame jInternalFrame) {
            super(jInternalFrame);
        }

        /* access modifiers changed from: protected */
        public void installComponents() {
        }
    }

    /* renamed from: a.cO$b */
    /* compiled from: a */
    class C2156b implements Runnable {

        /* renamed from: xh */
        private final /* synthetic */ int f6100xh;

        /* renamed from: xi */
        private final /* synthetic */ boolean f6101xi;

        /* renamed from: xj */
        private final /* synthetic */ char f6102xj;

        C2156b(int i, boolean z, char c) {
            this.f6100xh = i;
            this.f6101xi = z;
            this.f6102xj = c;
        }

        public void run() {
            GLCanvasTestAdditional.this.m28319a(this.f6100xh, this.f6101xi, this.f6102xj);
        }
    }

    /* renamed from: a.cO$c */
    /* compiled from: a */
    class C2157c implements Runnable {

        /* renamed from: xi */
        private final /* synthetic */ boolean f6104xi;

        /* renamed from: xk */
        private final /* synthetic */ int f6105xk;

        /* renamed from: xl */
        private final /* synthetic */ int f6106xl;

        /* renamed from: xm */
        private final /* synthetic */ int f6107xm;

        C2157c(int i, int i2, boolean z, int i3) {
            this.f6105xk = i;
            this.f6106xl = i2;
            this.f6104xi = z;
            this.f6107xm = i3;
        }

        public void run() {
            GLCanvasTestAdditional.this.m28318a(this.f6105xk, this.f6106xl, this.f6104xi, this.f6107xm);
        }
    }

    /* renamed from: a.cO$d */
    /* compiled from: a */
    class C2158d implements Runnable {

        /* renamed from: xk */
        private final /* synthetic */ int f6109xk;

        /* renamed from: xl */
        private final /* synthetic */ int f6110xl;

        /* renamed from: xn */
        private final /* synthetic */ int f6111xn;

        C2158d(int i, int i2, int i3) {
            this.f6111xn = i;
            this.f6109xk = i2;
            this.f6110xl = i3;
        }

        public void run() {
            GLCanvasTestAdditional.this.m28336s(this.f6111xn, this.f6109xk, this.f6110xl);
        }
    }

    /* renamed from: a.cO$e */
    /* compiled from: a */
    class C2159e implements Runnable {

        /* renamed from: xk */
        private final /* synthetic */ int f6113xk;

        /* renamed from: xl */
        private final /* synthetic */ int f6114xl;

        C2159e(int i, int i2) {
            this.f6113xk = i;
            this.f6114xl = i2;
        }

        public void run() {
            GLCanvasTestAdditional.this.m28318a(this.f6113xk, this.f6114xl, false, 0);
        }
    }

    /* renamed from: a.cO$k */
    /* compiled from: a */
    private class C2165k implements Runnable {
        private boolean cWM;

        private C2165k() {
            this.cWM = false;
        }

        /* synthetic */ C2165k(GLCanvasTestAdditional cOVar, C2165k kVar) {
            this();
        }

        public void run() {
            synchronized (GLCanvasTestAdditional.this.fKN) {
                notifyAll();
                if (this.cWM) {
                    try {
                        this.cWM = false;
                        GLCanvasTestAdditional.this.fKN.wait(200);
                    } catch (InterruptedException e) {
                        GLCanvasTestAdditional.logger.logp(Level.SEVERE, getClass().toString(), "run()", "Exception", e);
                    }
                }
            }
        }
    }

    /* renamed from: a.cO$a */
    class C2155a implements Runnable {
        C2155a() {
        }

        public void run() {
            GLCanvasTestAdditional.this.desktop.removeAll();
            GLCanvasTestAdditional.this.frame.dispose();
        }
    }

    /* renamed from: a.cO$n */
    /* compiled from: a */
 /*   private final class C2169n implements Runnable {
        private Rectangle ddg;
        private C6394amC ioA;

        private C2169n(Rectangle rectangle, C6394amC amc) {
            this.ddg = rectangle;
            this.ioA = amc;
        }

        public void run() {
            C2154cO.this.fKu.reset();
            C2154cO.this.fKu.getContext().startRendering();
            try {
                C2154cO.this.desktop.paint(C2154cO.this.fKu.create());
                Rectangle bounds = C2154cO.this.desktop.getBounds();
                this.ddg.x = bounds.x;
                this.ddg.y = bounds.y;
                this.ddg.width = bounds.width;
                this.ddg.height = bounds.height;
            } finally {
                C2154cO.this.fKu.getContext().stopRendering();
            }
        }
    }*/
}
