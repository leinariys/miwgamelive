package test;

import com.sun.opengl.util.FPSAnimator;
import logic.render.RepaintManagerWraper;
import taikodom.render.DrawContext;
import taikodom.render.RenderView;
import taikodom.render.gl.GLWrapper;
import taikodom.render.textures.DDSLoader;

import javax.media.opengl.*;
import javax.swing.*;
import java.awt.*;

/* renamed from: a.no */
/* compiled from: a */
public class GLCanvasTest {
    public static void main(String[] strArr) {
        JFrame jFrame = new JFrame();//Создать окно
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        /**
         * GLCapabilities указывает неизменный набор возможностей OpenGL.
         */
        GLCapabilities gLCapabilities = new GLCapabilities();
        gLCapabilities.setDoubleBuffered(true);
        gLCapabilities.setHardwareAccelerated(true);
        gLCapabilities.setDepthBits(24);
        gLCapabilities.setStencilBits(8);
        gLCapabilities.setRedBits(8);
        gLCapabilities.setBlueBits(8);
        gLCapabilities.setGreenBits(8);
        gLCapabilities.setAlphaBits(8);

        GLCanvas gLCanvas = new GLCanvas(gLCapabilities);
        jFrame.setBounds(100, 100, 1050, 800);
        jFrame.setLayout(new BorderLayout());
        gLCanvas.addGLEventListener(new GLEventListenerWrap(gLCanvas));
        jFrame.add(gLCanvas);
        jFrame.setVisible(true);
    }

    /* renamed from: a.no$a */
    static class GLEventListenerWrap implements GLEventListener {

        private final /* synthetic */ GLCanvas glCanvas;
        /* renamed from: dc */
        private DrawContext f8748dc;
        private GLCanvasTestAdditional gvG;

        GLEventListenerWrap(GLCanvas gLCanvas) {
            this.glCanvas = gLCanvas;
        }

        /**
         * Он вызывается объектом интерфейса GLAutoDrawable сразу после инициализации GLContext OpenGL.
         *
         * @param gLAutoDrawable Поверхность для рисования OpenGL и примитивы рисования(линии треугольники).
         */

        public void init(GLAutoDrawable gLAutoDrawable) {
            this.f8748dc = new RenderView((GLAutoDrawable) this.glCanvas).getDrawContext();
            this.f8748dc.setGL(new GLWrapper(this.glCanvas.getGL()));
            this.f8748dc.setCurrentFrame(10);
            RepaintManager.setCurrentManager(new RepaintManagerWraper());//Менеджер перерисовки
            new FPSAnimator(this.glCanvas, 200, true).start();
            this.gvG = new GLCanvasTestAdditional("Test");
            this.gvG.caI().setSize(100, 100);
            JButton jButton = new JButton("bla");
            this.gvG.caI().add(jButton);
            jButton.setBounds(10, 10, 50, 50);//Перемещает и изменяет размер этого компонента.
            this.gvG.setSize(800, 600, false);
        }

        /**
         * этот метод содержит логику, используемую для рисования графических элементов с использованием OpenGL API.
         *
         * @param gLAutoDrawable Поверхность для рисования OpenGL и примитивы рисования(линии треугольники).
         */
        public void display(GLAutoDrawable gLAutoDrawable) {
            /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
             * седержит методы для рисования примитивов (линии треугольники)*/
            GL gl = this.f8748dc.getGl();

            gl.glBegin(GL.GL_TRIANGLES);//Этот метод запускает процесс рисования
            gl.glColor3f(1.0f, 0.0f, 0.0f);   // Red
            gl.glVertex3f(0.5f, 0.7f, 0.0f);    // Top
            gl.glColor3f(0.0f, 1.0f, 0.0f);     // green
            gl.glVertex3f(-0.2f, -0.50f, 0.0f); // Bottom Left
            gl.glColor3f(0.0f, 0.0f, 1.0f);     // blue
            gl.glVertex3f(0.5f, -0.5f, 0.0f);   // Bottom Right
            gl.glEnd();

            gl.glClearColor(0.0f, 0.0f, 0.5f, 0.0f);//фон
            gl.glClear(DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ);//очистить буферы до заданных значений
            if (this.f8748dc != null) {
                this.gvG.draw(this.f8748dc);
            }
        }

        /**
         * Он вызывается объектом интерфейса GLAutoDrawable во время первой перерисовки после изменения размера компонента.
         * Он также вызывается всякий раз, когда положение компонента в окне изменяется.
         *
         * @param glAutoDrawable Поверхность для рисования для команд OpenGL.
         * @param x
         * @param y
         * @param width
         * @param height
         */
        @Override
        public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {

        }

        /**
         * @param glAutoDrawable Поверхность для рисования для команд OpenGL.
         * @param b
         * @param b1
         */
        @Override
        public void displayChanged(GLAutoDrawable glAutoDrawable, boolean b, boolean b1) {

        }
    }
}
