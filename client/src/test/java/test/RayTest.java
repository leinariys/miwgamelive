package test;

import com.hoplon.geometry.Vec3f;
import com.sun.opengl.util.Animator;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import logic.thred.LogPrinter;
import logic.thred.WatchDog;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.RenderGui;
import taikodom.render.RenderView;
import taikodom.render.SceneView;
import taikodom.render.StepContext;
import taikodom.render.camera.Camera;
import taikodom.render.camera.ViewCameraControl;
import taikodom.render.enums.BlendType;
import taikodom.render.gui.GAim;
import taikodom.render.gui.GuiScene;
import taikodom.render.loader.FileSceneLoader;
import taikodom.render.scene.*;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.ChannelInfo;
import taikodom.render.textures.Material;
import taikodom.render.textures.Texture;

import java.awt.*;
import java.awt.event.*;
import java.io.PrintWriter;

/* compiled from: a */
public class RayTest extends Frame implements MouseListener, MouseMotionListener, MouseWheelListener, GLEventListener {

    static RayTest demo = null;
    final SceneView sceneView = new SceneView();
    /* access modifiers changed from: private */
    public Vec3f Dir = new Vec3f();
    /* access modifiers changed from: private */
    public SceneObject barracuda;
    /* access modifiers changed from: private */
    public Vec3d craftPosition;
    /* access modifiers changed from: private */
    public Vec3f craftRotation;
    /* access modifiers changed from: private */
    public double craftSpeed = ScriptRuntime.NaN;
    /* access modifiers changed from: private */
    public RRay ray;
    /* access modifiers changed from: private */
    public Vec3d tempVect = new Vec3d();
    GAim aim;
    int displayCount = 0;
    Texture dustTex;
    int fps;
    boolean isRendering = false;
    boolean lockedTarget;
    int mousex;
    int mousey;
    int secondCounter;
    StepContext stepContext;
    long timeToDisplay = 0;
    Texture trailTex;
    private long before = System.nanoTime();
    private SceneObject bull;
    private Camera camera;
    private ViewCameraControl cameraControl = new ViewCameraControl();
    private Quat4fWrap cameraOrientation;
    private Vec3d cameraPosition;
    private Vec3f craftVelocity;
    private Quat4fWrap finalCameraOrientation;
    private GuiScene guiScene = new GuiScene();
    private FileSceneLoader loader;
    private RenderGui renderGui = new RenderGui();
    private String resourceDir;
    /* renamed from: rv */
    private RenderView f10376rv;
    private Scene scene;
    private long startTime = System.nanoTime();
    private WatchDog watchDog;

    public RayTest() {
        super("Basic Aim Demo");
        setLayout(new BorderLayout());
        setSize(720, 640);
        setLocation(40, 40);
        setAlwaysOnTop(true);
        setVisible(true);
        setupJOGL();
        addWindowListener(new C5156a());
    }

    public static void main(String[] strArr) {
        demo = new RayTest();
        demo.resourceDir = "../taikodom.client.resource/";
        if (strArr.length > 0) {
            demo.resourceDir = strArr[0];
        }
        demo.setVisible(true);
    }

    public void init(GLAutoDrawable gLAutoDrawable) {
        try {
            this.f10376rv = new RenderView(gLAutoDrawable.getContext());
            this.f10376rv.setViewport(0, 0, gLAutoDrawable.getWidth(), gLAutoDrawable.getHeight());
            gLAutoDrawable.setGL(new DebugGL(gLAutoDrawable.getGL()));
            this.f10376rv.getDrawContext().setCurrentFrame(1000);
            this.f10376rv.setGL(gLAutoDrawable.getGL());
            this.renderGui.getGuiContext().setDc(this.f10376rv.getDrawContext());
            this.f10376rv.getDrawContext().setRenderView(this.f10376rv);
            this.loader = new FileSceneLoader(this.resourceDir);

            System.loadLibrary("granny2");
            System.loadLibrary("mss32");
            System.loadLibrary("binkw32");
            System.loadLibrary("utaikodom.render");

            this.loader.loadFile("data/shaders/light_shader.pro");
            this.loader.loadFile("data/models/fig_spa_bullfrog.pro");
            this.loader.loadFile("data/models/fig_spa_barracuda.pro");
            this.loader.loadFile("data/scene/nod_spa_mars.pro");
            this.loader.loadFile("data/trails.pro");
            this.loader.loadFile("data/effects.pro");
            this.loader.loadFile("data/models/amm_min_contramed1.pro");
            this.loader.loadFile("data/hud.pro");
            this.loader.loadFile("data/projectiles.pro");
            this.loader.loadMissing();

            Shader shader = new Shader();
            shader.setName("Default billboard shader");
            ShaderPass shaderPass = new ShaderPass();
            shaderPass.setName("Default billboard pass");
            ChannelInfo channelInfo = new ChannelInfo();
            channelInfo.setTexChannel(8);
            shaderPass.setChannelTextureSet(0, channelInfo);
            shaderPass.setBlendEnabled(true);
            shaderPass.setAlphaTestEnabled(true);
            shaderPass.setDepthMask(false);
            shaderPass.setDstBlend(BlendType.ONE);
            shaderPass.setSrcBlend(BlendType.SRC_ALPHA);
            shaderPass.setCullFaceEnabled(false);
            shader.addPass(shaderPass);
            Material material = new Material();
            material.setShader(shader);
            material.setDiffuseTexture((Texture) this.loader.instantiateAsset("data/gfx/misc/rail_gun.dds"));
            this.camera = this.sceneView.getCamera();
            this.camera.setFarPlane(400000.0f);
            this.camera.setNearPlane(1.0f);
            this.camera.setFovY(75.0f);
            this.camera.setAspect(1.3333f);
            this.camera.getTransform().setTranslation(20.0d, 20.0d, 20.0d);
            this.scene = this.sceneView.getScene();
            this.craftRotation = new Vec3f();
            this.craftPosition = new Vec3d();
            this.cameraPosition = new Vec3d();
            this.craftVelocity = new Vec3f();
            this.cameraOrientation = new Quat4fWrap();
            this.finalCameraOrientation = new Quat4fWrap();
            this.cameraControl.setCamera(this.camera);
            this.barracuda = (SceneObject) this.loader.instantiateAsset("fig_spa_bullfrog");
            this.bull = (SceneObject) this.loader.instantiateAsset("fig_spa_barracuda");
            RTrail rTrail = (RTrail) this.loader.instantiateAsset("trail_green");
            rTrail.setLifeTime(1000.0f);
            rTrail.setTrailWidth(8.0f);
            this.bull.addChild(rTrail);
            this.aim = (GAim) this.loader.instantiateAsset("hud_target_aim");
            this.ray = new RRay();
            RBillboard rBillboard = new RBillboard();
            rBillboard.setMaterial(material);
            rBillboard.setSize(10.5f, 0.0f);
            this.ray.setRay(rBillboard, 3000.0f);
            this.scene.addChild((SceneObject) this.loader.instantiateAsset("nod_spa_mars"));
            this.scene.addChild(this.bull);
            this.scene.addChild(this.barracuda);
            this.bull.addChild(this.ray);
            RTrail rTrail2 = (RTrail) this.loader.instantiateAsset("trail_blue");
            rTrail2.setPosition((double) ScriptRuntime.NaN, (double) ScriptRuntime.NaN, -5.0d);
            this.bull.addChild(rTrail2);
            this.barracuda.setPosition(0.0f, 0.0f, -100.0f);
            this.aim.setSceneView(this.sceneView);
            this.aim.setShip(this.bull, this.sceneView);
            this.guiScene.addChild(this.aim);
            System.nanoTime();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    public void display(GLAutoDrawable gLAutoDrawable) {
        try {
            long currentTimeMillis = System.currentTimeMillis();
            if (this.watchDog == null) {
                this.watchDog = new WatchDog();
                PrintWriter M = LogPrinter.createLogFile("client-watchdog", "log");
                M.println("Starting RTrail test");
                this.watchDog.setPrintWriter(M);
                this.watchDog.setSlipBeforeRun(20);
                this.watchDog.mo14453K(2.0E10d);
                this.watchDog.start();
            }
            this.f10376rv.getDrawContext().setViewport(0, 0, getSize().width, getSize().height);
            this.f10376rv.getRenderInfo().setDrawAABBs(true);
            long j = currentTimeMillis - this.startTime;
            this.craftRotation.x -= ((float) (this.mousex - (demo.getWidth() / 2))) * 0.001f;
            this.craftRotation.y -= ((float) (this.mousey - (demo.getHeight() / 2))) * 0.001f;
            this.finalCameraOrientation.mo15244h(((double) this.craftRotation.y) * 0.017453292519943295d, ((double) this.craftRotation.x) * 0.017453292519943295d, (double) this.craftRotation.z);
            this.Dir.set(0.0f, 0.0f, 1.0f);
            this.bull.getTransform().multiply3x3(this.Dir, this.Dir);
            this.bull.getTransform().setOrientation(this.finalCameraOrientation);
            this.craftVelocity.set(((float) this.craftSpeed) * this.Dir.x, ((float) this.craftSpeed) * this.Dir.y, ((float) this.craftSpeed) * this.Dir.z);
            this.craftPosition.add(this.craftVelocity);
            this.cameraPosition.set(ScriptRuntime.NaN, 7.0d, 20.0d);
            this.bull.getTransform().multiply3x4(this.cameraPosition, this.cameraPosition);
            this.bull.setPosition(this.craftPosition);
            this.bull.setVelocity(this.craftVelocity);
            this.camera.getTransform().setIdentity();
            this.cameraOrientation.mo15207a(this.cameraOrientation, this.finalCameraOrientation, 0.05f);
            this.camera.setOrientation(this.cameraOrientation);
            this.camera.getTransform().setTranslation(this.cameraPosition);
            this.camera.calculateProjectionMatrix();
            if (this.stepContext == null) {
                this.stepContext = new StepContext();
            }
            this.stepContext.setCamera(this.camera);
            this.stepContext.setDeltaTime((float) (currentTimeMillis - this.before));
            this.scene.step(this.stepContext);
            this.guiScene.step(this.stepContext);
            this.timeToDisplay += currentTimeMillis - this.before;
            this.f10376rv.render(this.sceneView, (double) (((float) j) * 1.0E-9f), ((float) (currentTimeMillis - this.before)) * 1.0E-9f);
            this.renderGui.render(this.guiScene);
            this.before = currentTimeMillis;
            if (this.timeToDisplay > 1000000000) {
                this.timeToDisplay = 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    private void setupJOGL() {
        GLCapabilities gLCapabilities = new GLCapabilities();
        gLCapabilities.setDoubleBuffered(true);
        gLCapabilities.setHardwareAccelerated(true);
        GLCanvas gLCanvas = new GLCanvas(gLCapabilities);
        gLCanvas.enableInputMethods(true);
        gLCanvas.addGLEventListener(this);
        gLCanvas.addMouseMotionListener(this);
        gLCanvas.addMouseListener(this);
        gLCanvas.addMouseWheelListener(this);
        add(gLCanvas, "Center");
        gLCanvas.addMouseMotionListener(new C5157b());
        gLCanvas.addMouseListener(new C5158c());
        gLCanvas.addKeyListener(new C5159d());
        new Animator(gLCanvas).start();
    }

    public void reshape(GLAutoDrawable gLAutoDrawable, int i, int i2, int i3, int i4) {
        if (this.camera != null) {
            this.camera.setAspect(((float) i3) / ((float) i4));
        }
        this.sceneView.setViewport(0, 0, i3, i4);
        this.renderGui.resize(i, i2, i3, i4);
    }

    public void displayChanged(GLAutoDrawable gLAutoDrawable, boolean z, boolean z2) {
        this.f10376rv.setViewport(0, 0, gLAutoDrawable.getWidth(), gLAutoDrawable.getHeight());
    }

    public void mouseMoved(MouseEvent mouseEvent) {
    }

    public void mouseClicked(MouseEvent mouseEvent) {
    }

    public void mouseEntered(MouseEvent mouseEvent) {
    }

    public void mouseExited(MouseEvent mouseEvent) {
    }

    public void mousePressed(MouseEvent mouseEvent) {
    }

    public void mouseReleased(MouseEvent mouseEvent) {
    }

    public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
        if (mouseWheelEvent.getScrollType() == 0) {
            int wheelRotation = mouseWheelEvent.getWheelRotation();
            if (wheelRotation > 0) {
                this.cameraControl.zoomOut((float) wheelRotation);
            } else {
                this.cameraControl.zoomIn((float) (-wheelRotation));
            }
        }
    }

    public void mouseDragged(MouseEvent mouseEvent) {
    }

    /* renamed from: test.RayTest$a */
    class C5156a extends WindowAdapter {
        C5156a() {
        }

        public void windowClosing(WindowEvent windowEvent) {
            System.exit(0);
        }

        public void windowClosed(WindowEvent windowEvent) {
            System.exit(0);
        }
    }

    /* renamed from: test.RayTest$b */
    /* compiled from: a */
    class C5157b implements MouseMotionListener {
        C5157b() {
        }

        public void mouseDragged(MouseEvent mouseEvent) {
        }

        public void mouseMoved(MouseEvent mouseEvent) {
            RayTest.this.mousex = mouseEvent.getX();
            RayTest.this.mousey = mouseEvent.getY();
        }
    }

    /* renamed from: test.RayTest$c */
    /* compiled from: a */
    class C5158c implements MouseListener {
        C5158c() {
        }

        public void mouseClicked(MouseEvent mouseEvent) {
            RayTest.this.tempVect.mo9497bG(RayTest.this.Dir);
            RayTest.this.tempVect.scale(-50.0d);
            RayTest.this.tempVect.add(RayTest.this.craftPosition);
            RayTest.this.ray.trigger(RayTest.this.tempVect, RayTest.this.Dir, 100.0f);
        }

        public void mouseEntered(MouseEvent mouseEvent) {
        }

        public void mouseExited(MouseEvent mouseEvent) {
        }

        public void mousePressed(MouseEvent mouseEvent) {
        }

        public void mouseReleased(MouseEvent mouseEvent) {
        }
    }

    /* renamed from: test.RayTest$d */
    /* compiled from: a */
    class C5159d implements KeyListener {
        C5159d() {
        }

        public void keyPressed(KeyEvent keyEvent) {
            if (keyEvent.getKeyCode() == 87) {
                RayTest rayTest = RayTest.this;
                rayTest.craftSpeed = rayTest.craftSpeed - 0.009999999776482582d;
            }
            if (keyEvent.getKeyCode() == 83) {
                RayTest rayTest2 = RayTest.this;
                rayTest2.craftSpeed = rayTest2.craftSpeed + 0.009999999776482582d;
            }
            if (keyEvent.getKeyCode() == 65) {
                RayTest.this.craftRotation.x += 1.0f;
            }
            if (keyEvent.getKeyCode() == 68) {
                RayTest.this.craftRotation.x -= 1.0f;
            }
            if (keyEvent.getKeyCode() == 49) {
                RayTest.this.lockedTarget = !RayTest.this.lockedTarget;
                if (RayTest.this.lockedTarget) {
                    System.out.println("Target locked");
                    RayTest.this.aim.setTarget(RayTest.this.barracuda);
                    return;
                }
                System.out.println("Target unocked");
                RayTest.this.aim.setTarget((SceneObject) null);
            }
        }

        public void keyReleased(KeyEvent keyEvent) {
        }

        public void keyTyped(KeyEvent keyEvent) {
        }
    }
}
