package test;

import logic.res.html.MessageContainer;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import org.mozilla1.classfile.C0147Bi;
import p001a.C5395aHr;
import p001a.C5533aMz;
import p001a.aAH;
import p001a.aEI;
import taikodom.render.graphics2d.C0559Hm;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.util.List;
import java.util.Map;
import java.util.jar.JarInputStream;
import java.util.zip.ZipEntry;

/* renamed from: test.a.Ub */
/* compiled from: test.a */
public class C1404Ub {
    private static final int eZF = 1048576;
    private static final Log logger = LogPrinter.m10275K(C1404Ub.class);
    int eZI = 0;
    int eZJ = 0;
    List<aEI> entries = MessageContainer.init();
    private List<C5395aHr> eZG = MessageContainer.init();
    private Map<String, aEI> eZH = MessageContainer.newMap();

    public static void main(String[] strArr) {
        File file;
        File file2 = null;
        List dgK = MessageContainer.init();
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            String str = strArr[i];
            if (file2 == null) {
                file = new File(str);
            } else if (str.indexOf("*") != -1) {
                logger.debug(str);
                List<File> a = C0559Hm.m5246a(".", (FilenameFilter) new C1405a(str.replaceAll("[*]", "[^/\\\\]+")));
                for (File add : a) {
                    logger.debug(a);
                    dgK.add(add);
                }
                file = file2;
            } else {
                dgK.add(new File(str));
                file = file2;
            }
            i++;
            file2 = file;
        }
        new C5533aMz(new aAH(new StringBuilder().append(file2).toString())).init();
        logger.debug("Done.");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: test.a */
    public void mo5894a(File[] fileArr) {
        JarInputStream jarInputStream;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        JarInputStream jarInputStream2 = null;
        int length = fileArr.length;
        int i = 0;
        while (i < length) {
            File file = fileArr[i];
            logger.debug("loading " + file);
            try {
                jarInputStream = new JarInputStream(new FileInputStream(file));
                while (true) {
                    try {
                        ZipEntry nextEntry = jarInputStream.getNextEntry();
                        if (nextEntry == null) {
                            try {
                                break;
                            } catch (Exception e) {
                            }
                        } else {
                            C0559Hm.copyStream(jarInputStream, byteArrayOutputStream);
                            int i2 = this.eZI;
                            this.eZI = i2 + 1;
                            aEI aei = new aEI(i2, file.getName(), nextEntry.getName(), byteArrayOutputStream.toByteArray());
                            this.eZH.put(nextEntry.getName(), aei);
                            this.entries.add(aei);
                            byteArrayOutputStream.reset();
                        }
                    } catch (Throwable th) {
                        th = th;
                    }
                }
                jarInputStream.close();
                i++;
                jarInputStream2 = jarInputStream;
            } catch (Throwable th2) {
                th = th2;
                jarInputStream = jarInputStream2;
                try {
                    jarInputStream.close();
                } catch (Exception e2) {
                }
                throw th;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void bNO() {
        int i = this.eZJ;
        this.eZJ = i + 1;
        C5395aHr ahr = new C5395aHr(i);
        String str = null;
        int i2 = 0;
        for (aEI next : this.entries) {
            if (i2 > eZF || (str != null && !next.hHL.equals(str))) {
                this.eZG.add(ahr);
                int i3 = this.eZJ;
                this.eZJ = i3 + 1;
                ahr = new C5395aHr(i3);
                i2 = 0;
            }
            ahr.entries.add(next);
            i2 += next.data.length;
            str = next.hHL;
        }
        this.entries.clear();
        if (i2 > 0) {
            this.eZG.add(ahr);
        }
    }

    /* renamed from: test.a.Ub$test.a */
    class C1405a implements FilenameFilter {
        private final /* synthetic */ String emN;

        C1405a(String str) {
            this.emN = str;
        }

        public boolean accept(File file, String str) {
            return new File(file + C0147Bi.SEPARATOR, str).getPath().matches(this.emN);
        }
    }
}
