package test;

import logic.swing.BasicLookAndFeelSpaceEngine;
import logic.ui.BaseUiTegXml;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.Oh */
/* compiled from: a */
public class C0997Oh {
    @Before
    public void setUp() throws UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel(new BasicLookAndFeelSpaceEngine());
    }

    @Test
    public void blc() {
        boolean z;
        JSplitPane e = (JSplitPane) new BaseUiTegXml().createJComponentAndAddInRootPane((Object) this, "<split style='width:300px; height:200px'><vbox name='c1'/><vbox name='c2'/></split>");
        Assert.assertTrue(e instanceof JSplitPane);
        JSplitPane jSplitPane = e;
        e.setBounds(0, 0, 100, 100);
        e.doLayout();
        Component component = jSplitPane.getComponent(0);
        Component component2 = jSplitPane.getComponent(1);
        Assert.assertEquals((long) component.getHeight(), (long) component2.getHeight());
        if (component.getWidth() + component.getX() <= component2.getX()) {
            z = true;
        } else {
            z = false;
        }
        Assert.assertTrue(z);
    }
}
