/*Вывести список системных переменных Mysql сервера. Переменные могут быть глобальные ( GLOBAL ) и сессионные ( SESSION ).*/
SHOW GLOBAL VARIABLES;
SHOW SESSION VARIABLES;

/*Вывести список существующих баз данных*/
SHOW DATABASES;

/*Если не указана конструкция FROM, выведет список таблиц в текущей базе данных.*/
SHOW TABLES;

/*Проверка таблицы на предмет ошибок в различных режимах.*/
CHECK TABLE PROCESS_LOG;

/*Оптимизация таблиц.*/
OPTIMIZE TABLE PROCESS_LOG;

/*Просмотр существующих привилегий пользователя mysql. */
SHOW GRANTS FOR 'u100399292_proxy'@'%';

/*Выводит информацию по указанной таблице, имена колонок, типы данных и т.д.*/
SHOW COLUMNS FROM collection_proxy;
DESCRIBE PROXY_LIST;

/*Выводит размер баз данных*/
SELECT table_schema "DATABASE_NAME", sum(data_length + index_length) / 1024 / 1024 "DATABASE_SIZE_IN_MB"
FROM information_schema.TABLES
GROUP BY table_schema;

/*Выводит размер всех таблиц в БД*/
SELECT table_schema as                                        `DATABASE_NAME`,
    table_name AS                                        `TABLE_NAME`,
    round(((data_length + index_length) / 1024 / 1024), 4) `TABLE_SIZE_IN_MB`
FROM information_schema.TABLES
ORDER BY (data_length + index_length) DESC;








