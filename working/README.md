# README #

### Декомпилировать ###

java -jar procyon-decompiler-0.6.0.jar -jar taikodom.client.jar -o out

java -jar procyon-decompiler-0.6.0.jar a.DN

### Описание ###

<p align="center"><img src ="./readme/logo.png" alt="Watchdog logo" /></p>

### Запуск оригинального КЛИЕНТА **из консоли** с логированием в файл

```shell
set PATH=libs;
libs\jre\bin\javaw -Xmx800m -noverify -XX:+ShowMessageBoxOnError -Dfile.encoding=UTF-8 -Dnio.savemessages=true -Xbootclasspath:libs\jre\lib\rt.jar;libs\jre\lib\jsse.jar;libs\jre\lib\jce.jar;libs\jre\lib\charsets.jar;libs\jre\lib\resources.jar -cp .;res;libs;taikodom.client.jar -Djava.ext.dirs=libs\jre\lib\ext;jars -Djava.library.path=libs;Debug\client;Debug\client\deps;Debug\common;Debug\common\deps; taikodom.game.main.ClientMain > log/logger-Client.log

```

-Dnio.savemessages=true - сохранение передаваемых макетов сообщений в файл \messages

### Запуск оригинального КЛИЕНТА **через исполняемые файлы**

exec.exe -launch taikodom-game.exe -launch

###### Принцип работы

Вызов taikodom.game.main.ClientMain

#### Переменные среды для сервера

```shell
set nio.savemessages=true
```

### Запуск оригинального СЕРВЕРА **из консоли** с логированием в файл

```shell
set PATH=libs;
libs\jre\bin\javaw -Xmx800m -noverify -XX:+ShowMessageBoxOnError -Dfile.encoding=UTF-8 -Xbootclasspath:libs\jre\lib\rt.jar;libs\jre\lib\jsse.jar;libs\jre\lib\jce.jar;libs\jre\lib\charsets.jar;libs\jre\lib\resources.jar -cp .;res;libs;taikodom.client.jar -Djava.ext.dirs=libs\jre\lib\ext;jars -Djava.library.path=libs;Debug\client;Debug\client\deps;Debug\common;Debug\common\deps; taikodom.game.main.ServerMain -onlySnapshot > log/logger-Server.log
```

###### Аргументы для сервера

-preloadClasses -onlySnapshot -testCache testComand

#### Переменные среды для сервера

```shell
set fill-hollow-hack=true
set glitchLog.enable=true
set watchdog.enable=true
set RUNNING_CLIENT_BOT=true
set bitverse.ai.disable=true
```

### Запуск оригинальных MAIN CLASS **из консоли** с логированием в файл

```shell
set PATH=libs;
libs\jre\bin\javaw -Xmx800m -noverify -XX:+ShowMessageBoxOnError -Dfile.encoding=UTF-8 -Xbootclasspath:libs\jre\lib\rt.jar;libs\jre\lib\jsse.jar;libs\jre\lib\jce.jar;libs\jre\lib\charsets.jar;libs\jre\lib\resources.jar -cp .;res;libs;taikodom.client.jar -Djava.ext.dirs=libs\jre\lib\ext;jars -Djava.library.path=libs;Debug\client;Debug\client\deps;Debug\common;Debug\common\deps; a.Hd > log/logger-Server.log
```

### Алгоритм работы **UpdaterExec.exe**

* set PATH=libs;
* libs\\jre\\bin\\javaw -XX:+ShowMessageBoxOnError -Dfile.encoding=UTF-8 -Xbootclasspath:
  libs\\jre\\lib\\rt.jar;libs\\jre\\lib\\jsse.jar;libs\\jre\\lib\\jce.jar;libs\\jre\\lib\\charsets.jar -cp
  .;res;libs;taikodom.client.jar -Djava.ext.dirs=libs\\jre\\lib\\ext;jars
  -Djava.library.path=libs;Debug\\client;Debug\\client\\deps;Debug\\common;Debug\\common\\deps;
  com.hoplon.gametoolkit.updater.client.Updater
* Потом запускает treemv.exe

### Алгоритм работы **launcher.exe**, **taikodom.exe**

* Ищет updater_new.exe
* Удаляет
* remove("updater.exe");
* rename("updater_new.exe", "updater.exe");
* %s = sprintf(v5, "pt");
* %s = sprintf(v5, "en");
* Запускает updater.exe %s

### Алгоритм работы **taikodom-game.exe**

```shell
"-Xmx256m";
"-XX:+ShowMessageBoxOnError";
"-Dfile.encoding=UTF-8";
"-Xbootclasspath:libs\\jre\\lib\\rt.jar;libs\\jre\\lib\\jsse.jar;libs\\jre\\lib\\jce.jar;libs\\jre\\lib\\charse"
"ts.jar;libs\\jre\\lib\\resources.jar;";
"-Djava.class.path=./;./taikodom.client.jar;./res;./libs;";
"-Djava.ext.dirs=libs\\jre\\lib\\ext;jars;";
"-Djava.library.path=libs;Debug\\client;Debug\\client\\deps;Debug\\common;Debug\\common\\deps;";
"-Xverify:none";
"-XX:+UseConcMarkSweepGC";
"-verbose:gc";
"-XX:+PrintGCDetails";
"-Xloggc:gc.log";
```

### Написание клиента

https://habr.com/post/103830/

* Различают 3-и вида загрузчиков в Java.
  * базовый загрузчик (bootstrap), getClassLoader() = null
  * системный загрузчик (System Classloader), getClassLoader() = sun.misc.Launcher$AppClassLoader
  * загрузчик расширений (Extension Classloader). getClassLoader() = sun.misc.Launcher$ExtClassLoader

Для того, чтобы получить загрузчик, которым был загружен класс А, необходимо воспользоваться методом
A.class.getClassLoader().

* (bootstrap) Управлять с помощью ключа -Xbootclasspath, который позволяет переопределять наборы базовых классов.
* (System Classloader) Управлять с помощью ключа -classpath или системной опцией java.class.path.
* (Extension Classloader)Управлять с помощью системной опции java.ext.dirs.

-Djava.library.path

### Команда для Декомпиляция

```shell
C:\Users\Lein\Downloads\Taikodom-live>java -jar deobfuscator.jar --config detect.yml
[main] INFO com.javadeobfuscator.deobfuscator.Deobfuscator - Loading classpath
[main] INFO com.javadeobfuscator.deobfuscator.Deobfuscator - Loading input
[main] INFO com.javadeobfuscator.deobfuscator.Deobfuscator - Detecting known obfuscators
[main] INFO com.javadeobfuscator.deobfuscator.Deobfuscator -
[main] INFO com.javadeobfuscator.deobfuscator.Deobfuscator - RuleSourceFileAttribute: Some obfuscators don't remove the SourceFile attribute by default. This information can be recovered, and is very useful
[main] INFO com.javadeobfuscator.deobfuscator.Deobfuscator -    Found possible SourceFile attribute on a/rP$b: a.java
[main] INFO com.javadeobfuscator.deobfuscator.Deobfuscator - Recommend transformers:
[main] INFO com.javadeobfuscator.deobfuscator.Deobfuscator -    com.javadeobfuscator.deobfuscator.transformers.normalizer.SourceFileClassNormalizer
```

### Адреса и порты

###### Домены на которые идут запросы

* taikodom.com.br
  * :80 - Сайт
    * Событие, запуск клиента
      * GET /server_status_en.txt
      * GET /release_notes_en.txt
    * регистация
      * POST /users/game_create_user
        - locale
        - external_agent
        - name
        - email
        - password
        - birthdate
        - gender
        - newsletter
          - ответ при ошибке - Server is offline. Please try again later.
    * лицензия EULA
      * GET /end_user_license_in_game?locale=en
    * условия использования в игре
      * GET /terms_of_use_in_game?locale=en
      * GET /credits_in_game
    * забыл пароль, открытие ссылки в браузере
      * GET /auth/forgotten
    * поддержка, открытие ссылки в браузере
      * GET /support
    * открытие ссылки в браузере
      * GET /game_billing
      * GET /signup
    * Не понятно
      * GET /end_user_license_in_game_v
      * GET /terms_of_use_in_game_v
      * GET /help_in_game?locale=en
      * GET /users/game_resend_activation_email
      * GET /users/game_get_email_from_user
* live.server2.taikodom.com.br
  * :15225
    * Событие, ввод пороля
      * Ошибка Server down for maintenance. We'll soon be back. Thanks for your comprehension.
* updater.taikodom.com
  * :80 - Сайт

### Заметки по классам

Vec3d  
Tuple3d одинаковые реализации

Менеджер разметки - Layout

Plaf Look and Feel
установить новый
UIManager.setLookAndFeel


Оформление поменялось SwingUtilities.updateComponentTreeUI

Обновить заголовок окна LookAndFeelDecorated

UI Delegate

Контейнеры Общего назначения JPanel JScrollPane JToolBar JSplitPane JTabbedPane Специального назначения JInternakFrame
JLayeredPane Компоненты

C6223ain - работа с файлами написать тесты

### Формат сообщений SocketMessage

| Клинет                       |        Событие             | Сервер                       |
|------------------------------|:--------------------------:|------------------------------|
| Начало события кукопожатие   |        Соедениение         | Начало события кукопожатие   |
| StepLoging.HANDSHAKE1        |                            | StepLoging.HANDSHAKE1        |
| Отправляет HANDSHAKE1        |                            | Отправляет HANDSHAKE1        |
| sendHandshake1               |                            | sendHandshake1               |
| --------------------------   | -------------------------  | --------------------------   |
| processingHandshake1         |   Обработать HANDSHAKE1    | processingHandshake1         |
| Если нет ошибки              |                            | Если нет ошибки              |
| codeErrorConnectStatusNotOk  |                            | codeErrorConnectStatusNotOk  |
| codeErrorVersion             |                            | codeErrorVersion             |
| то                           |                            | то                           |
| ставит статус HANDSHAKE2     |                            | ставит статус HANDSHAKE2     |
| отправляет HANDSHAKE2        |                            | отправляет HANDSHAKE2        |
| buildMessageHandshake2       |                            | buildMessageHandshake2       |
| --------------------------   | -------------------------- | --------------------------   |
| processingHandshake2         |   Обработать HANDSHAKE2    | processingHandshake2         |
| Проверяет что это HANDSHAKE2 |                            | Проверяет что это HANDSHAKE2 |
| и нет кода ошибки            |                            | и нет кода ошибки            |
| Если this.authentication     |                            | если isAuthentication        |
| то отправляем AUTHENTICATION |                            | ставим статус AUTHENTICATION |
| username,password            |                            | иначе UP                     |
| и isForcingNewUser           |                            | (Ничего не отправляем)       |
| Иначе ставит статус          |                            |                              |
| StepLoging.UP                |                            |                              |
| --------------------------   | -------------------------- | --------------------------   |
|                              | Обработать AUTHENTICATION  | processingAuthentication     |
|                              |                            | ставит статус AUTHENTICATING |
|                              |                            | Проверяем пользователя       |
|                              |                            | managerGameEvent.chechUser   |
|                              |                            | Если все хорошо              |
|                              |                            | ставим статусStepLoging.UP   |
|                              |                            | добавляем в список           |
|                              |                            | this.listAuthorizationSocket |
|                              |                            | отправляем AUTHENTICATION    |
|                              |                            | иначе код ошибки             |
| --------------------------   | -------------------------- | --------------------------   |
| processingAuthentication     | Обработать AUTHENTICATION  |                              |
| Если статус Status.OK        |                            |                              |
| processingUp                 |                            |                              |
| ставим статус StepLoging.UP  |                            |                              |
|                              |                            |                              |

Отправка сервером и клиентом при подключении канала:
HANDSHAKE1 = 0
Биты:

* [0] HandlerClientState.StepLoging.HANDSHAKE1 = 0
* [1] Connect.Status.OK = 0
* [UTF] versionString

HANDSHAKE2 = 1
Биты:

* [0] HandlerClientState.StepLoging.HANDSHAKE2 = 1
* [1] Connect.Status.OK = 0 | codeErrorVersion = -2 | codeErrorConnectStatusNotOk = -1

Отправляется клиентом:
AUTHENTICATION = 2
Биты:
* [UTF] username
* [UTF] password
* [Boolean] isForcingNewUser

Отправляется сервером:
AUTHENTICATION = 2
Биты:
* [0] HandlerClientState.StepLoging.AUTHENTICATION = 2
* [1] Connect.Status.OK = 0 | Код ошибки
* [3] Авторизация успешна - 1 | Если код ошибки -1 | Если код ошибки кол-во пользователей на сервере

AUTHENTICATING = 3
UP = 4
CLOSING = 5
CLOSED = 6

HandlerClientState.StepLoging. Connect.Status

### Если загрузку выставить через арт мани 100%

Произойдёт ошибка

42 2022-05-12 04:03:43.465 [INFO ] aqx: Opening channel 42 2022-05-12 04:03:43.466 [INFO ] aqx: Trying to connect to
localhost:15225 42 2022-05-12 04:03:43.469 [INFO ] aqx: Channel opened 42 2022-05-12 04:03:43.490 [INFO ] agX: Channel
added: java.nio.channels.SocketChannel[connected local=/127.0.0.1:56852 remote=localhost/127.0.0.1:15225]
42 2022-05-12 04:03:43.490 [INFO ] aqx: Sending: 15 bytes HANDSHAKE1 42 2022-05-12 04:03:43.494 [INFO ] aqx: Waiting
connection. 46 2022-05-12 04:03:43.495 [INFO ] agX: Listening 46 2022-05-12 04:03:43.502 [INFO ] aqx: Received: 15 bytes
HANDSHAKE1 46 2022-05-12 04:03:43.505 [INFO ] aqx: Sending: 3 bytes HANDSHAKE2 46 2022-05-12 04:03:43.508 [INFO ] aqx:
Received: 3 bytes HANDSHAKE2 46 2022-05-12 04:03:43.508 [INFO ] aqx: Sending: 9 bytes AUTHENTICATION 46 2022-05-12 04:
03:43.509 [INFO ] aqx: Received: 5 bytes AUTHENTICATION 42 2022-05-12 04:03:43.521 [INFO ] ClientMain: Getting
environment 42 2022-05-12 04:03:43.530 [INFO ] Jz: Initing Environment. 42 2022-05-12 04:03:43.720 [INFO ] Jz: Setup
transport manager. 42 2022-05-12 04:03:43.739 [INFO ] Jz: Resolving rt. 49 2022-05-12 04:03:43.739 [DEBUG] ea$a:
waiting... 50 2022-05-12 04:03:43.739 [DEBUG] ea$c: waiting... 50 2022-05-12 04:03:43.743 [DEBUG] ea$c: waiting... 9
2022-05-12 04:06:41.708 [WARN ] aiD: Exception while executing a task    
java.lang.NullPointerException at a.aou.bhe(a.java:95)     // EngineGame.bhe at taikodom.addon.connect.ConnectAddon.bMt(
a.java:150)
at taikodom.addon.connect.ConnectAddon.bMt(a.java:150)
at taikodom.addon.connect.ConnectAddon.b(a.java:146)
at taikodom.addon.connect.ConnectAddon$b.run(a.java:596)
at gametoolkit.timer.module.SoftTimer$InternalTimer.run(a.java:292)
at a.aiD$a.run(a.java:36)
at a.aiD$b.run(a.java:62)
at java.util.concurrent.Executors$RunnableAdapter.call(Unknown Source)
at java.util.concurrent.FutureTask$Sync.innerRun(Unknown Source)
at java.util.concurrent.FutureTask.run(Unknown Source)
at java.util.concurrent.ThreadPoolExecutor$Worker.runTask(Unknown Source)
at java.util.concurrent.ThreadPoolExecutor$Worker.run(Unknown Source)
at java.lang.Thread.run(Unknown Source)










