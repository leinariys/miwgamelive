package game.network.metric;

import game.network.channel.client.HandlerClientState;
import game.network.channel.server.OpenSocketNioServer;

import javax.management.*;
import java.util.concurrent.atomic.AtomicLong;

/* compiled from: a */
public class MetricNetworkTrafficImpl implements MetricNetworkTraffic, DynamicMBean {
    final /* synthetic */ OpenSocketNioServer serverSocketChannel;
    public AtomicLong receivedLength = new AtomicLong();
    public AtomicLong receivedCount = new AtomicLong();
    public AtomicLong sendingLength = new AtomicLong();
    public AtomicLong sendingCount = new AtomicLong();
    long lastUpdateTime = System.currentTimeMillis();
    private float incomingBytesPerSecond;
    private float incomingMessagesPerSecond;
    private float outgoingBytesPerSecond;
    private float outgoingMessagesPerSecond;

    public MetricNetworkTrafficImpl(OpenSocketNioServer qr) {
        this.serverSocketChannel = qr;
    }

    public int getNumberOfConnections() {
        return this.serverSocketChannel.getNumberOfConnections();
    }

    public float getIncomingBytesPerSecond() {
        calculation();
        return this.incomingBytesPerSecond;
    }

    public float getIncomingMessagesPerSecond() {
        calculation();
        return this.incomingMessagesPerSecond;
    }

    public float getOutgoingBytesPerSecond() {
        calculation();
        return this.outgoingBytesPerSecond;
    }

    public float getOutgoingMessagesPerSecond() {
        calculation();
        return this.outgoingMessagesPerSecond;
    }

    public float[] getServerOutgoingMessagesStats() {
        float sqrt;
        float f;
        int bgz;
        HandlerClientState[] handlerClientStates = this.serverSocketChannel.getHandlerClientStates();
        int i = Integer.MAX_VALUE;
        int length = handlerClientStates.length;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i2 < length) {
            HandlerClientState lz = handlerClientStates[i2];
            if (!lz.isChild()) {
                bgz = i;
            } else {
                bgz = lz.getSizeMessageQueue();
                i4 += bgz;
                if (bgz > i5) {
                    i5 = bgz;
                }
                if (bgz >= i) {
                    bgz = i;
                }
                i3++;
            }
            i2++;
            i = bgz;
        }
        if (i3 == 0) {
            sqrt = 0.0f;
            f = 0.0f;
            i = 0;
        } else {
            float f2 = ((float) i4) / ((float) i3);
            int i6 = 0;
            for (HandlerClientState lz2 : handlerClientStates) {
                if (lz2.isChild()) {
                    int bgz2 = lz2.getSizeMessageQueue();
                    i6 = (int) (((float) i6) + ((((float) bgz2) - f2) * (((float) bgz2) - f2)));
                }
            }
            sqrt = (float) Math.sqrt(((double) i6) / ((double) i3));
            f = f2;
        }
        return new float[]{(float) i3, f, sqrt, (float) i, (float) i5};
    }

    private void calculation() {
        long currentTimeMillis = System.currentTimeMillis();
        long periodTime = currentTimeMillis - this.lastUpdateTime;
        if (periodTime >= 1000) {
            this.incomingBytesPerSecond = converting(this.receivedLength, periodTime);
            this.incomingMessagesPerSecond = converting(this.receivedCount, periodTime);
            this.outgoingBytesPerSecond = converting(this.sendingLength, periodTime);
            this.outgoingMessagesPerSecond = converting(this.sendingCount, periodTime);
            this.lastUpdateTime = currentTimeMillis;
        }
    }

    /* renamed from: a */
    private float converting(AtomicLong atomicLong, long j) {
        return (((float) atomicLong.getAndSet(0)) * 1000.0f) / ((float) j);
    }

    @Override
    public Object getAttribute(String attribute) throws AttributeNotFoundException, MBeanException, ReflectionException {
        return null;
    }

    @Override
    public void setAttribute(Attribute attribute) throws AttributeNotFoundException, InvalidAttributeValueException, MBeanException, ReflectionException {

    }

    @Override
    public AttributeList getAttributes(String[] attributes) {
        return null;
    }

    @Override
    public AttributeList setAttributes(AttributeList attributes) {
        return null;
    }

    @Override
    public Object invoke(String actionName, Object[] params, String[] signature) throws MBeanException, ReflectionException {
        return null;
    }

    @Override
    public MBeanInfo getMBeanInfo() {
        return null;
    }
}
