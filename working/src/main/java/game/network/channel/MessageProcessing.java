package game.network.channel;

import game.network.message.ByteMessage;

import java.io.EOFException;
import java.io.IOException;

/* renamed from: a.UV */
/* compiled from: a */
public interface MessageProcessing {
    void close() throws IOException;

    /* renamed from: jG */
    ByteMessage getTransportMessage() throws InterruptedException, EOFException, ClassNotFoundException;
}
