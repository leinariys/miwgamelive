package game.network.channel.server;

import game.engine.SocketMessage;
import game.network.channel.Connect;
import game.network.channel.InteractionSocket;
import game.network.channel.client.ClientConnect;
import game.network.channel.client.HandlerClientState;
import game.network.exception.ChechUserException;
import game.network.message.ByteMessageCompres;
import game.network.message.ByteMessageReader;
import game.network.metric.MetricNetworkTrafficImpl;
import logic.thred.LogPrinter;
import logic.thred.PoolThread;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.io.EOFException;
import java.io.IOException;
import java.io.UTFDataFormatException;
import java.lang.management.ManagementFactory;
import java.net.InetSocketAddress;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: a.Qr */
/* compiled from: a */
public class OpenSocketNioServer extends InteractionSocket implements ChechKeepAlive {
    /* access modifiers changed from: private */
    static LogPrinter logger = LogPrinter.m10275K(OpenSocketNioServer.class);
    private final ThreadPoolExecutor poolExecutor;
    /* renamed from: AB */
    public Connect managerGameEvent;
    private int listenerPort;
    private ClientConnect.Attitude attitudeRole;
    private long lostChechAliveTime;
    private Map<Integer, HandlerServerState> listenerChannel;
    private MetricNetworkTrafficImpl networkTraffic;

    public OpenSocketNioServer() {
        this(6667, (String) null);
    }

    public OpenSocketNioServer(int listenerPort, String version) {
        this.attitudeRole = ClientConnect.Attitude.CHILD;
        this.listenerChannel = new HashMap();
        this.listenerPort = listenerPort;
        this.versionString = version;
        this.selectableChannelThread.setChechKeepAlive(this);
        this.poolExecutor = PoolThread.newFixedThreadPool(1, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), "", false);
        //registerNetworkTraffic();
    }

    /* renamed from: a */
    public HandlerServerState initChannelAndAdd(ClientConnect.Attitude attitude, ServerSocketChannel serverSocketChannel, int[] ip1, int[] ip2, boolean authentication) throws ClosedChannelException {
        HandlerServerState handlerServerState = new HandlerServerState(serverSocketChannel, attitude, this, ip1, ip2, authentication);
        this.selectableChannelThread.сhannelRegister(handlerServerState);
        return handlerServerState;
    }

    /* renamed from: a */
    public void buildListenerChannel(int listenerPort, int[] ip1, int[] ip2) throws IOException {
        buildListenerChannel(listenerPort, ip1, ip2, true);
    }

    /* renamed from: a */
    public void buildListenerChannel(int listenerPort, int[] ip1, int[] ip2, boolean authentication) throws IOException {
        int[] ip3;
        ServerSocketChannel open = ServerSocketChannel.open();
        open.configureBlocking(false);
        open.socket().bind(new InetSocketAddress(listenerPort));
        if (ip1 != null) {
            ip3 = ip2 == null ? new int[]{255, 255, 255, 255} : ip2;
        } else {
            ip3 = null;
        }
        this.listenerChannel.put(Integer.valueOf(listenerPort), initChannelAndAdd(this.attitudeRole, open, ip1, ip3, authentication));
    }

    /* renamed from: af */
    public void removeChannelAndClose(int listenerPort) throws IOException {
        HandlerServerState remove = this.listenerChannel.remove(Integer.valueOf(listenerPort));
        if (remove != null) {
            remove.serverSocketChannel.close();
        }
    }

    public Map<Integer, HandlerServerState> getListenerChannel() {
        return this.listenerChannel;
    }

    /* renamed from: a */
    public void creatListenerChannel(Connect managerGameEvent) {
        this.managerGameEvent = managerGameEvent;
        try {
            buildListenerChannel(this.listenerPort, (int[]) null, (int[]) null);
            start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void processingAuthentication(HandlerClientState lz, ByteMessageCompres jb) throws UTFDataFormatException, EOFException {
        ByteMessageReader amf = new ByteMessageReader(jb.getMessage(), jb.getOffset(), jb.getLength());
        String userName = amf.readUTF();
        String password = amf.readUTF();
        boolean isForcingNewUser = amf.readBoolean();
        lz.setCurrentStepLoging(HandlerClientState.StepLoging.AUTHENTICATING);
        this.poolExecutor.execute(new AuthenticationRunnable(lz, userName, password, isForcingNewUser));
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void signalDisconnected(HandlerClientState lz) {
        try {
            super.signalDisconnected(lz);
        } finally {
            if (!this.poolExecutor.isShutdown()) {
                try {
                    this.poolExecutor.execute(new DisconnectedRunnable(lz));
                } catch (RejectedExecutionException e2) {
                    logger.warn(e2.getMessage());
                }
            }
        }
    }

    public void close() {
        super.close();
        this.poolExecutor.shutdown();
    }

    public void checkAliveChannel() {
        HandlerClientState clientState = null;
        if (System.currentTimeMillis() - this.lostChechAliveTime > 10000) {
            this.lostChechAliveTime = System.currentTimeMillis();
            for (SelectionKey next : this.selectableChannelThread.getSelectorKeys()) {
                try {
                    if (next.attachment() instanceof HandlerClientState) {//Клиентские соединения
                        clientState = (HandlerClientState) next.attachment();
                        long currentTimeMillis = System.currentTimeMillis() - clientState.getLastTimeBufferReader();
                        if (((double) currentTimeMillis) > 600000.0d) {//10 minutes

                            next.channel().close();
                            clientState.stepDisconnected();
                        } else if (((double) currentTimeMillis) > 60000.0d && ((double) (System.currentTimeMillis() - clientState.getTimeAddToDispatchQueue())) > 50000.0d) {
                            clientState.insertMessageSendQueue(this.messageKeepAlive);
                        }
                    } else {
                        continue;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Throwable th) {
                    clientState.stepDisconnected();
                    throw th;
                }
            }
        }
    }

    /* renamed from: a */
    public void receivedByteMessage(HandlerClientState clientState, ByteMessageCompres messageCompres) throws IOException, InterruptedException {
        if (clientState.getCurrentStepLoging() == HandlerClientState.StepLoging.UP && this.networkTraffic != null) {
            this.networkTraffic.receivedLength.addAndGet((long) messageCompres.getLength());
            this.networkTraffic.receivedCount.incrementAndGet();
        }
        super.receivedByteMessage(clientState, messageCompres);
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void saveMessageToFileBeforeSending(HandlerClientState lz, ByteMessageCompres message) throws IOException {
        if (this.networkTraffic != null) {
            this.networkTraffic.sendingLength.addAndGet((long) message.getLength());
            this.networkTraffic.sendingCount.incrementAndGet();
        }
        super.saveMessageToFileBeforeSending(lz, message);
    }

    private void registerNetworkTraffic() {
        MBeanServer server = ManagementFactory.getPlatformMBeanServer();
        if (server != null) {
            this.networkTraffic = new MetricNetworkTrafficImpl(this);
            try {
                server.registerMBean(this.networkTraffic, new ObjectName("Bitverse:name=NetworkTraffic"));
            } catch (InstanceAlreadyExistsException e) {
                System.err.println(e.getMessage());
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* renamed from: a.Qr$b */
    /* compiled from: a */
    class AuthenticationRunnable implements Runnable {
        private final /* synthetic */ HandlerClientState handlerClientState;
        private final /* synthetic */ String userName;
        private final /* synthetic */ String pass;
        private final /* synthetic */ boolean isForcingNewUser;

        AuthenticationRunnable(HandlerClientState lz, String userName, String pass, boolean isForcingNewUser) {
            this.handlerClientState = lz;
            this.userName = userName;
            this.pass = pass;
            this.isForcingNewUser = isForcingNewUser;
        }

        public void run() {
            int i;
            try {
                Connect.Status aVar = Connect.Status.ERROR;
                try {
                    OpenSocketNioServer.this.managerGameEvent.chechUser(this.handlerClientState.getClientConnect(), this.userName, this.pass, this.isForcingNewUser);
                    this.handlerClientState.setCurrentStepLoging(HandlerClientState.StepLoging.UP);
                    OpenSocketNioServer.this.addAuthorizationSocket(this.handlerClientState);
                    SocketMessage message = new SocketMessage();
                    message.writeInt(HandlerClientState.StepLoging.AUTHENTICATION.ordinal());
                    message.writeInt(Connect.Status.OK.ordinal());
                    message.writeInt(-1);
                    OpenSocketNioServer.this.saveMessageToFileBeforeSending(this.handlerClientState, new ByteMessageCompres(message.toByteArray()));
                } catch (ChechUserException e) {
                    Connect.Status coo = e.getStatus();
                    if (coo == Connect.Status.SERVER_FULL) {
                        i = e.getCount();
                    } else {
                        i = -1;
                    }
                    SocketMessage message = new SocketMessage();
                    message.writeInt(HandlerClientState.StepLoging.AUTHENTICATION.ordinal());
                    message.writeInt(coo.ordinal());
                    message.writeInt(i);
                    OpenSocketNioServer.this.saveMessageToFileBeforeSending(this.handlerClientState, new ByteMessageCompres(message.toByteArray()));
                    this.handlerClientState.setStepClosing();
                }
            } catch (Exception e2) {
                OpenSocketNioServer.logger.error("Severe error authenticating: ", e2);
                this.handlerClientState.setStepClosing();
            }
        }
    }

    /* renamed from: a.Qr$a */
    class DisconnectedRunnable implements Runnable {
        private final /* synthetic */ HandlerClientState handlerClientState;

        DisconnectedRunnable(HandlerClientState lz) {
            this.handlerClientState = lz;
        }

        public void run() {
            OpenSocketNioServer.this.managerGameEvent.userDisconnected(this.handlerClientState.getClientConnect());
        }
    }
}
