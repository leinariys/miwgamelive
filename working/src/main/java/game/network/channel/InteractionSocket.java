package game.network.channel;

import game.engine.SocketMessage;
import game.network.channel.client.ClientConnect;
import game.network.channel.client.ClientConnectBuilder;
import game.network.channel.client.ClientConnectImpl;
import game.network.channel.client.HandlerClientState;
import game.network.exception.CreatListenerChannelException;
import game.network.message.ByteMessage;
import game.network.message.ByteMessageCompres;
import game.network.message.ByteMessageReader;
import game.network.thread.SelectableChannelThread;
import gnu.trove.THashMap;
import logic.thred.LogPrinter;
import taikodom.render.textures.DDSLoader;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.channels.SelectableChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: a.dm */
/* compiled from: a */
public class InteractionSocket implements MessageProcessing, ProcesTransportMessage, NetworkChannel {
    static LogPrinter log = LogPrinter.m10275K(InteractionSocket.class);

    /* renamed from: yI */
    private static final int codeErrorVersion = -2;
    /* renamed from: yJ */
    private static final int codeErrorConnectStatusNotOk = -1;
    /* renamed from: yK */
    private static final boolean NIO_SAVEMESSAGES = "true".equalsIgnoreCase(System.getProperty("nio.savemessages"));
    /* renamed from: yY */
    /*  private static /* synthetic *//* int[] f6607yY;*/
    /* renamed from: yT */
    public final ByteMessageCompres messageKeepAlive;
    /* renamed from: yU */
    public final ByteMessageCompres messageKeepAliveReply;
    /* renamed from: yL */
    private final ArrayBlockingQueue<ByteMessage> transportMessage = new ArrayBlockingQueue<>(DDSLoader.DDSD_MIPMAPCOUNT);
    /* renamed from: yP */
    private final ClientConnect clientConnect = ClientConnectBuilder.build(ClientConnect.Attitude.BROTHER, (InetAddress) null);
    public String versionString;
    /* renamed from: yQ */
    public SelectableChannelThread selectableChannelThread = new SelectableChannelThread();
    /* renamed from: yX */
    public InetAddress ipRemoteHost;
    /* renamed from: yM */
    private ReentrantLock reentrantLock = new ReentrantLock();
    /* renamed from: yN */
    private Map<ClientConnect, HandlerClientState> listAuthorizationSocket = new THashMap();
    /* renamed from: yO */
    private HandlerClientState[] handlerClientStates = new HandlerClientState[0];
    /* renamed from: yR */
    private int codeConnectStatusOkSend;
    /* renamed from: yS */
    private int codeConnectStatusOkCheck;
    /* renamed from: yV */
    private AtomicLong iterationSaveMessage = new AtomicLong();
    /* renamed from: yW */
    private boolean compression = true;

    public InteractionSocket() {
        try {
            ipRemoteHost = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        SocketMessage ala = new SocketMessage();
        ala.writeByte(0);
        ala.writeByte(0);
        this.messageKeepAlive = new ByteMessageCompres(ala);
        this.messageKeepAlive.setSpecial(true);
        SocketMessage ala2 = new SocketMessage();
        ala2.writeByte(0);
        ala2.writeByte(1);
        this.messageKeepAliveReply = new ByteMessageCompres(ala2);
        this.messageKeepAliveReply.setSpecial(true);
    }

    /* renamed from: a */
    public void addAuthorizationSocket(HandlerClientState lz) {
        this.reentrantLock.lock();
        try {
            THashMap tHashMap = new THashMap(this.listAuthorizationSocket);
            tHashMap.put(lz.getClientConnect(), lz);
            this.handlerClientStates = (HandlerClientState[]) tHashMap.values().toArray(new HandlerClientState[this.listAuthorizationSocket.size()]);
            this.listAuthorizationSocket = tHashMap;
        } finally {
            this.reentrantLock.unlock();
        }
    }

    /* renamed from: b */
    public void signalDisconnected(HandlerClientState lz) {
        log.info("Disconnected " + lz.getClientConnect());
        this.reentrantLock.lock();
        try {
            THashMap tHashMap = new THashMap(this.listAuthorizationSocket);
            tHashMap.remove(lz.getClientConnect());
            this.handlerClientStates = (HandlerClientState[]) tHashMap.values().toArray(new HandlerClientState[this.listAuthorizationSocket.size()]);
            this.listAuthorizationSocket = tHashMap;
        } finally {
            this.reentrantLock.unlock();
        }
    }

    public int getNumberOfConnections() {
        return this.handlerClientStates.length;
    }

    /* renamed from: b */
    public void sendTransportMessage(byte[] bArr, int i, int i2) throws IOException {
        HandlerClientState[] lzArr = this.handlerClientStates;
        ByteMessageCompres jb = new ByteMessageCompres(bArr, i, i2);
        for (HandlerClientState e : lzArr) {
            saveMessageToFileBeforeSending(e, jb);
        }
    }

    /* renamed from: a */
    public void sendTransportMessage(ClientConnect bVar, byte[] bArr, int i, int i2) throws IOException {
        if (bVar == this.clientConnect) {
            try {
                this.transportMessage.put(new ByteMessage(bVar, bArr, i, i2));
            } catch (InterruptedException e) {
                throw new RuntimeException("Interrupted while trying to loopback", e);
            }
        } else {
            ClientConnectImpl haVar = (ClientConnectImpl) bVar;
            HandlerClientState lz = (HandlerClientState) haVar.getHandlerClientState();
            if (lz == null) {
                HandlerClientState lz2 = this.listAuthorizationSocket.get(bVar);
                if (lz2 != null) {
                    haVar.setHandlerClientState(lz2);
                    saveMessageToFileBeforeSending(lz2, new ByteMessageCompres(bArr, i, i2));
                    return;
                }
                System.err.println("address not found: " + bVar);
                if (bVar == null) {
                    throw new IllegalArgumentException("Null address");
                }
                return;
            }
            saveMessageToFileBeforeSending(lz, new ByteMessageCompres(bArr, i, i2));
        }
    }

    /* renamed from: jG */
    public ByteMessage getTransportMessage() throws InterruptedException {
        return this.transportMessage.take();
    }

    public void close() {
        this.selectableChannelThread.close();
    }

    /* renamed from: jH */
    public ClientConnect getClientConnect() {
        return this.clientConnect;
    }

    /* renamed from: a */
    public Collection<ClientConnect> getClientConnects(ClientConnect.Attitude aVar) {
        HandlerClientState[] lzArr = this.handlerClientStates;
        ArrayList arrayList = new ArrayList();
        for (HandlerClientState lz : lzArr) {
            if (lz.getClientConnect().getAttitude() == aVar) {
                arrayList.add(lz.getClientConnect());
            }
        }
        return arrayList;
    }

    public boolean isUp() {
        return this.selectableChannelThread.isRunning();
    }

    public void start() {
        this.selectableChannelThread.start();
    }

    /* renamed from: a */
    public void receivedByteMessage(HandlerClientState clientState, ByteMessageCompres messageCompres) throws InterruptedException, IOException {
        if (messageCompres.isSpecial()) {
            byte[] message = messageCompres.getMessage();
            int offset = messageCompres.getOffset();
            if (message[offset] == 0 && message[offset + 1] == 0) {
                //Ответ на Специальное сообщение  messageKeepAlive
                saveMessageToFileBeforeSending(clientState, this.messageKeepAliveReply);
            } else if (message[offset + 0] != 0 || message[offset + 1] != 1) {
                throw new IllegalArgumentException("Invalid special message: " + message[0] + "  " + message[1]);
            }
        } else if (clientState.getCurrentStepLoging() == HandlerClientState.StepLoging.UP) {
            this.transportMessage.put(messageCompres);
        } else {
            switch (clientState.getCurrentStepLoging().ordinal()) {
                case 0://HANDSHAKE1
                    processingHandshake1(clientState, messageCompres);
                    return;
                case 1://HANDSHAKE2
                    processingHandshake2(clientState, messageCompres);
                    return;
                case 2://AUTHENTICATION
                    processingAuthentication(clientState, messageCompres);
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void sendHandshake1(HandlerClientState lz) throws IOException {
        SocketMessage message = new SocketMessage();
        message.writeInt(HandlerClientState.StepLoging.HANDSHAKE1.ordinal());
        message.writeInt(this.codeConnectStatusOkSend);
        message.writeUTF(this.versionString);
        saveMessageToFileBeforeSending(lz, new ByteMessageCompres(message.toByteArray()));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ByteMessageCompres buildMessageHandshake2(HandlerClientState.StepLoging aVar, int i) {
        SocketMessage ala = new SocketMessage();
        ala.writeInt(aVar.ordinal());
        ala.writeInt(i);
        return new ByteMessageCompres(ala.toByteArray());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int checkMessageHandshake2(HandlerClientState.StepLoging aVar, ByteMessageCompres jb) throws EOFException {
        ByteMessageReader amf = new ByteMessageReader(jb.getMessage(), jb.getOffset(), jb.getLength());
        int readInt = amf.readInt();
        if (readInt == aVar.ordinal()) {
            return amf.readInt();
        }
        Thread.dumpStack();
        throw new IllegalStateException("Read state " + readInt + " expected " + jb);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void processingHandshake1(HandlerClientState lz, ByteMessageCompres messageCompres) throws IOException {
        ByteMessageReader messageRead = new ByteMessageReader(messageCompres.getMessage(), messageCompres.getOffset(), messageCompres.getLength());
        int code = 0;
        int readInt = messageRead.readInt();
        if (readInt != HandlerClientState.StepLoging.HANDSHAKE1.ordinal()) {
            throw new IllegalStateException("Read state " + readInt + " expected " + HandlerClientState.StepLoging.HANDSHAKE1.ordinal());
        }
        if (messageRead.readInt() != this.codeConnectStatusOkCheck) {
            code = codeErrorConnectStatusNotOk;
        }
        String readUTF = messageRead.readUTF();
        if (!this.versionString.equals(readUTF)) {
            setStatusInvalidHandshake(readUTF);
            code = codeErrorVersion;
        }
        if (code != 0) {
            lz.setCurrentStepLoging(HandlerClientState.StepLoging.CLOSING);
        } else {
            lz.setCurrentStepLoging(HandlerClientState.StepLoging.HANDSHAKE2);
        }
        saveMessageToFileBeforeSending(lz, buildMessageHandshake2(HandlerClientState.StepLoging.HANDSHAKE2, code));
    }

    /* access modifiers changed from: protected */
    /* renamed from: R */
    public void setStatusInvalidHandshake(String str) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void processingHandshake2(HandlerClientState lz, ByteMessageCompres byteMessage) throws EOFException {
        HandlerClientState.StepLoging stepLoging;
        if (checkMessageHandshake2(HandlerClientState.StepLoging.HANDSHAKE2, byteMessage) == Connect.Status.OK.ordinal()) {
            if (lz.isAuthentication()) {
                stepLoging = HandlerClientState.StepLoging.AUTHENTICATION;
            } else {
                stepLoging = HandlerClientState.StepLoging.UP;
            }
            lz.setCurrentStepLoging(stepLoging);
            return;
        }
        lz.setStepClosing();
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void processingAuthentication(HandlerClientState lz, ByteMessageCompres jb) throws UTFDataFormatException, EOFException {
        throw new CreatListenerChannelException();
    }


    /* renamed from: jI */
    public MessageProcessing getMessageProcessing() {
        return this;
    }

    /* renamed from: jJ */
    public ProcesTransportMessage getProcessSendMessage() {
        return this;
    }

    /* renamed from: iX */
    public boolean isRunningThreadSelectableChannel() {
        return this.selectableChannelThread.isRunning();
    }

    /* renamed from: a */
    public void creatListenerChannel(Connect omVar) {
        throw new CreatListenerChannelException();
    }

    /* renamed from: jK */
    public ClientConnect getClientConnectLocal() {
        return this.clientConnect;
    }

    /* renamed from: jh */
    public InetAddress getIpRemoteHost() {
        return this.ipRemoteHost;
    }

    /* renamed from: iY */
    public boolean isListenerPort() {
        return true;
    }

    /* renamed from: c */
    public void authorizationChannelClose(ClientConnect bVar) {
        this.reentrantLock.lock();
        try {
            HandlerClientState lz = this.listAuthorizationSocket.get(bVar);
            if (lz != null) {
                SelectableChannel channel = lz.getChannel();
                if (channel != null) {
                    channel.close();
                }
            }
        } catch (IOException e) {
            log.warn("Ignoring", e);
        } catch (Throwable th) {
            this.reentrantLock.unlock();
            throw th;
        }
        this.reentrantLock.unlock();
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void saveMessageToFileBeforeSending(HandlerClientState lz, ByteMessageCompres message) throws IOException {
        if (NIO_SAVEMESSAGES) {
            this.iterationSaveMessage.incrementAndGet();
            new File("messages").mkdir();
            FileOutputStream fileOutputStream = new FileOutputStream("messages/" + getClass().getName() + "-" + String.format("%06d", new Object[]{Long.valueOf(this.iterationSaveMessage.get())}) + ".msg");
            fileOutputStream.write(message.getMessage(), message.getOffset(), message.getLength());
            fileOutputStream.flush();
            fileOutputStream.close();
        }
        lz.compressBeforeSending(message);
    }

    /* renamed from: a */
    /*
    public void mo13494a(C5931adH adh) {
    }*/

    /* renamed from: a */
    public void buildListenerChannel(int listenerPort, int[] iArr, int[] iArr2, boolean z) throws IOException {
        throw new UnsupportedOperationException();
    }

    /* renamed from: af */
    public void removeChannelAndClose(int i) throws IOException {
        throw new UnsupportedOperationException();
    }

    /* renamed from: ja */
    public long metricClientTimeAuthorization() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jf */
    public long metricReadByte() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jg */
    public long metricWriteByte() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jc */
    public long metricCountReadMessage() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jb */
    public long metricCountWriteMessage() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: je */
    public long metricByteLength() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jd */
    public long metricAllOriginalByteLength() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: jL */
    public boolean isCompression() {
        return this.compression;
    }

    public void setCompression(boolean z) {
        this.compression = z;
    }

    /* renamed from: jM */
    public HandlerClientState[] getHandlerClientStates() {
        return this.handlerClientStates;
    }
}
