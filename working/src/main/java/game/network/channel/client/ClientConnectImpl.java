package game.network.channel.client;

import java.net.InetAddress;

/* renamed from: a.ha */
/* compiled from: a */
public class ClientConnectImpl implements ClientConnect {

    /* renamed from: Ti */
    private static final int bitShift = 28;
    /* renamed from: Tn */
    private final InetAddress inetAddress;
    /* renamed from: id */
    private final int id;
    /* renamed from: Tj */
    private transient Attitude attitude;
    /* renamed from: Tk */
    private transient Object handlerClientState;
    /* renamed from: Tl */
    private transient long connectTime;
    /* renamed from: Tm */
    private transient long ping;
    private String info;
    private transient String str;

    public ClientConnectImpl(Attitude attitude, int i, InetAddress inetAddress) {
        this.inetAddress = inetAddress;
        this.id = (268435455 & i) | (attitude.ordinal() << bitShift);
        this.attitude = attitude;
    }

    public int hashCode() {
        return this.id;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (this.id != ((ClientConnectImpl) obj).id) {
            return false;
        }
        return true;
    }

    public String toString() {
        if (this.str != null) {
            return this.str;
        }
        String str2 = String.valueOf(Integer.toString(this.id)) + (this.info != null ? " " + this.info : "");
        this.str = str2;
        return str2;
    }

    /* renamed from: xQ */
    public Attitude getAttitude() {
        if (this.attitude != null) {
            return this.attitude;
        }
        this.attitude = Attitude.values()[this.id >> bitShift];
        return null;
    }

    /* renamed from: xR */
    public Object getHandlerClientState() {
        return this.handlerClientState;
    }

    /* renamed from: x */
    public void setHandlerClientState(Object obj) {
        this.handlerClientState = obj;
    }

    /* renamed from: xS */
    public void setConnectTime() {
        this.connectTime = System.currentTimeMillis();
    }

    /* renamed from: xT */
    public long getConnectTime() {
        return this.connectTime;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    /* renamed from: xU */
    public long getPing() {
        return this.ping;
    }

    /* renamed from: bj */
    public void setPing(long ping) {
        this.ping = ping;
    }

    /* renamed from: xV */
   /* public long getPings() {
        return this.ping;
    }*/

    public InetAddress getInetAddress() {
        return this.inetAddress;
    }

    public int getId() {
        return this.id;
    }
}
