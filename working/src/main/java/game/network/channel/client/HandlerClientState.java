package game.network.channel.client;

import game.network.channel.InteractionSocket;
import game.network.message.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.zip.Deflater;

/* renamed from: a.LZ */
/* compiled from: a */
public class HandlerClientState extends HandlerClient {
    /* private static final boolean dyB = false;*/
    private static final int edgeCompression = 1024;
    private static AtomicInteger nextId = new AtomicInteger();
    private final InteractionSocket interactionSocket;
    private final boolean authentication;
    private final LinkedBlockingQueue<ByteMessageCompres> queueMessageToSend;
    private final ClientConnect clientConnect;
    private final int port;
    private final ChannelBufferReader bufferReader;
    private final ChannelBufferWriter bufferWriter;
    long lastTimeBufferReader;
    long timeAddToDispatchQueue;
    ReentrantLock reentrantLock;
    ArrayList<ByteMessageCompres> processedMessages;
    private Deflater zipDeflater;
    private byte[] bufferCompressedData;
    private StepLoging currentStepLoging;
    private boolean isFirstDisconnect;
    private boolean isReadWriteInteres;
    //private NIOBandwidthMeter dyQ;
    private AtomicBoolean isConnectionDown;
    /* renamed from: id */
    private int id;

    /* renamed from: pa */
    private int sizeBlock;

    HandlerClientState(ClientConnect bVar, InteractionSocket dmVar, SocketChannel channel, int port) {
        this(bVar, dmVar, channel, port, true);
    }

    public HandlerClientState(ClientConnect clientConnect, InteractionSocket interactionSocket, SocketChannel channel, int port, boolean authentication) {
        super(channel);
        this.queueMessageToSend = new LinkedBlockingQueue<>();
        this.zipDeflater = new Deflater(-1, true);
        this.bufferCompressedData = new byte[262144];
        this.lastTimeBufferReader = System.currentTimeMillis();
        this.timeAddToDispatchQueue = System.currentTimeMillis();
        this.reentrantLock = new ReentrantLock();
        this.currentStepLoging = StepLoging.HANDSHAKE1;
        this.isFirstDisconnect = true;
        this.id = nextId.incrementAndGet();
        this.isConnectionDown = new AtomicBoolean();
        this.processedMessages = new ArrayList<>();
        this.port = port;
        this.authentication = authentication;
        this.sizeBlock = 67108863;//67108864  = 2^26 = 8 Mbit       8388608 =  2^23 = 1 Mbit
        if (interactionSocket.isCompression()) {
            this.bufferReader = new ChannelReaderCompression(2048, this.sizeBlock);
            this.bufferWriter = new ChannelWriterCompression(2048, this.sizeBlock);
        } else {
            this.bufferReader = new ChannelReaderNotCompression(2048, this.sizeBlock);
            this.bufferWriter = new ChannelWriterNotCompression(2048, this.sizeBlock);
        }
        this.clientConnect = clientConnect;
        this.interactionSocket = interactionSocket;
        //this.dyQ = new NIOBandwidthMeter(this);
    }

    public boolean isAuthentication() {
        return this.authentication;
    }

    /* access modifiers changed from: package-private */
    public int prochentResultingKey() {
        return 5;
    }

    public StepLoging getCurrentStepLoging() {
        return this.currentStepLoging;
    }

    /* renamed from: a */
    public void setCurrentStepLoging(StepLoging currentStepLoging) {
        this.currentStepLoging = currentStepLoging;
    }

    /* renamed from: b */
    public void insertMessageSendQueue(ByteMessageCompres message) {
        if (this.currentStepLoging != StepLoging.CLOSED && this.currentStepLoging != StepLoging.CLOSING) {
            this.timeAddToDispatchQueue = System.currentTimeMillis();
            if (!this.queueMessageToSend.offer(message)) {
                return;
            }
            if (this.reentrantLock.tryLock()) {
                try {
                    if (!this.isReadWriteInteres && haveSomethingToWrite()) {
                        addSelectionKeyToThread();
                    }
                } finally {
                    this.reentrantLock.unlock();
                }
            } else if (this.queueMessageToSend.size() < 10) {
                addSelectionKeyToThread();
            }
        }
    }

    /* renamed from: c */
    public void compressBeforeSending(ByteMessageCompres message) throws IOException {
        if (this.currentStepLoging != StepLoging.CLOSED && this.currentStepLoging != StepLoging.CLOSING) {
            if (this.interactionSocket.isCompression() && this.clientConnect.getAttitude() == ClientConnect.Attitude.CHILD && message.getLength() > edgeCompression) {
                ByteMessageCompres messageZip = new ByteMessageCompres(toZip(message));
                messageZip.setCompressed(true);
                messageZip.setOriginalLength(message.getLength());
                message = messageZip;
            }
            this.timeAddToDispatchQueue = System.currentTimeMillis();
            try {
                addToDispatchQueue(message);
            } catch (InterruptedException e) {
                throw new IOException(e.getMessage());
            }
        }
    }

    /* renamed from: d */
    private byte[] toZip(ByteMessageCompres message) {
        this.zipDeflater.setInput(message.getMessage(), message.getOffset(), message.getLength());
        this.zipDeflater.finish();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (!this.zipDeflater.finished()) {
            byteArrayOutputStream.write(this.bufferCompressedData, 0, this.zipDeflater.deflate(this.bufferCompressedData));
        }
        this.zipDeflater.reset();
        return byteArrayOutputStream.toByteArray();
    }

    /* renamed from: e */
    private void addToDispatchQueue(ByteMessageCompres message) throws InterruptedException {
        this.queueMessageToSend.put(message);
        addSelectionKeyToThread();
    }

    /* renamed from: f */
    private void insertMessageIntoSendQueue(ByteMessageCompres jb) throws InterruptedException, IOException {
        if (!this.queueMessageToSend.offer(jb)) {
            this.queueMessageToSend.put(jb);
        } else if (this.reentrantLock.tryLock()) {
            try {
                if (!this.isReadWriteInteres) {
                    processedQueueMessages(getSelectionKey(), false, true);
                    if (haveSomethingToWrite()) {
                        addSelectionKeyToThread();
                    }
                }
            } finally {
                this.reentrantLock.unlock();
            }
        } else if (this.queueMessageToSend.size() < 10) {
            addSelectionKeyToThread();
        }
    }

    public void stepDisconnected() {
        if (this.isConnectionDown.compareAndSet(false, true)) {
            this.currentStepLoging = StepLoging.CLOSED;
            this.processedMessages.clear();
            this.queueMessageToSend.clear();
            if (this.isFirstDisconnect) {
                this.isFirstDisconnect = false;
                this.interactionSocket.signalDisconnected(this);
            }
            //this.dyQ.cqZ();
            //this.dyQ = null;
        }
    }

    /* renamed from: b */
    public void isReadyKeyChannelForOperations(SelectionKey selectionKey) throws IOException, InterruptedException {
        if (selectionKey.isValid()) {
            if (selectionKey.isReadable() && getCurrentStepLoging() != StepLoging.CLOSING) {
                if (this.bufferReader.readerChannelToBuffer(selectionKey.channel()) == -1) {
                    try {
                        selectionKey.channel().close();
                        selectionKey.cancel();
                        return;
                    } finally {
                        stepDisconnected();
                    }
                } else {
                    this.lastTimeBufferReader = System.currentTimeMillis();
                }
            }
            while (this.bufferReader.isNotEmpty()) {
                getClientConnect().setConnectTime();
                ByteMessageCompres ayg = this.bufferReader.getMessage();
                ayg.setFromClientConnect(getClientConnect());
                this.interactionSocket.receivedByteMessage(this, ayg);
            }
            eventIsOpConnect(selectionKey);
        } else if (this.isFirstDisconnect) {
            this.isFirstDisconnect = false;
            this.interactionSocket.signalDisconnected(this);
        }
    }

    /* renamed from: a */
    public void eventIsOpConnect(SelectionKey selectionKey) {
        boolean isOpConnect = true;
        if (this.reentrantLock.tryLock()) {
            try {//SelectionKey.OP_CONNECT
                if (selectionKey.isConnectable() && !selectionKey.isWritable()) {
                    isOpConnect = false;
                }
                processedQueueMessages(selectionKey, true, isOpConnect);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                this.reentrantLock.unlock();
            }
        }
    }

    /* renamed from: a */
    private void processedQueueMessages(SelectionKey selectionKey, boolean z, boolean isWritChannel) throws IOException {
        if ((isWritChannel || this.bufferWriter.isBufferReadyToBeRead()) && this.bufferWriter.isBufferReadyToBeRead()) {
            this.bufferWriter.writeBufferToChannel(getChannel());
        }
        if (!this.bufferWriter.isBufferReadyToBeRead()) {
            this.queueMessageToSend.drainTo(this.processedMessages, 100 - this.processedMessages.size());
            if (this.processedMessages.size() > 0) {
                Iterator<ByteMessageCompres> it = this.processedMessages.iterator();
                while (it.hasNext()) {
                    if (!this.bufferWriter.writeMessageToBuffer(it.next())) {
                        break;
                    }
                    it.remove();
                }
                this.bufferWriter.prepareForReading();
                if (this.bufferWriter.isBufferReadyToBeRead() && isWritChannel) {
                    this.bufferWriter.writeBufferToChannel(getChannel());
                }
            }
        }
        if (!z) {
            return;
        }
        if (haveSomethingToWrite()) {
            this.isReadWriteInteres = true;
            selectionKey.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);//SelectionKey.OP_READ + SelectionKey.OP_WRITE
        } else if (getCurrentStepLoging() == StepLoging.CLOSING) {
            try {
                getChannel().close();
            } finally {
                stepDisconnected();
            }
        } else {
            this.isReadWriteInteres = false;
            selectionKey.interestOps(SelectionKey.OP_READ);//SelectionKey.OP_READ
        }
    }

    private boolean haveSomethingToWrite() {
        return this.bufferWriter.isBufferReadyToBeRead() || this.processedMessages.size() > 0 || this.queueMessageToSend.size() > 0;
    }

    public ClientConnect getClientConnect() {
        return this.clientConnect;
    }

    public void sendHandshake1() throws IOException {
        this.interactionSocket.sendHandshake1(this);
    }

    public void setStepClosing() {
        setCurrentStepLoging(StepLoging.CLOSING);
    }

    /* renamed from: gH */
    public int getCountWriteMessage() {
        return this.bufferWriter.getCountWriteMessage();
    }

    /* renamed from: gI */
    public int getAllOriginalByteLength() {
        return this.bufferWriter.getAllOriginalByteLength();
    }

    /* renamed from: gG */
    public int getMetricWriteByte() {
        return this.bufferWriter.getMetricWriteByte();
    }

    public int getCountReadMessage() {
        return this.bufferReader.getCountReadMessage();
    }

    public int getByteLength() {
        return this.bufferReader.getByteLength();
    }

    public int getMetricReadByte() {
        return this.bufferReader.getMetricReadByte();
    }

    /* renamed from: gJ */
    public int getMetricМessagesEmpty() {
        return this.bufferWriter.getMetricМessagesEmpty();
    }

    /* renamed from: gK */
    public int getMetricMessageFilled() {
        return this.bufferWriter.getMetricMessageFilled();
    }

    public int getId() {
        return this.id;
    }

    public int getPort() {
        return this.port;
    }

    public long getLastTimeBufferReader() {
        return this.lastTimeBufferReader;
    }

    public long getTimeAddToDispatchQueue() {
        return this.timeAddToDispatchQueue;
    }

    public int getSizeMessageQueue() {
        return this.queueMessageToSend.size();
    }

    public boolean isChild() {
        return this.clientConnect.getAttitude() == ClientConnect.Attitude.CHILD;
    }

    /* renamed from: a.LZ$a */
    public enum StepLoging {
        HANDSHAKE1,
        HANDSHAKE2,
        AUTHENTICATION,
        AUTHENTICATING,
        UP,
        CLOSING,
        CLOSED
    }
}
