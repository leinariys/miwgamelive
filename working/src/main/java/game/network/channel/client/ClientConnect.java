package game.network.channel.client;

import java.io.Serializable;
import java.net.InetAddress;

/* renamed from: a.b */
/* compiled from: a */
public interface ClientConnect extends Serializable {

    /* renamed from: bj */
    void setPing(long j);

    int getId();

    InetAddress getInetAddress();

    /* renamed from: xQ */
    Attitude getAttitude();

    /* renamed from: xS */
    void setConnectTime();

    /* renamed from: xT */
    long getConnectTime();

    /* renamed from: xU */
    long getPing();

    /* renamed from: xV */
    /* long getPings();*/

    /* renamed from: a.b$a */
    public enum Attitude {
        BROTHER,
        CHILD,
        PARENT
    }
}
