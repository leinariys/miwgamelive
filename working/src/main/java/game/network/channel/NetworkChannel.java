package game.network.channel;


import game.network.channel.client.ClientConnect;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Collection;

/* renamed from: a.ahG  reason: case insensitive filesystem */
/* compiled from: a */
public interface NetworkChannel {
    /* renamed from: a */
    Collection<ClientConnect> getClientConnects(ClientConnect.Attitude aVar);

    /* renamed from: a */
    void buildListenerChannel(int listenerPort, int[] iArr, int[] iArr2, boolean z) throws IOException;

    /* renamed from: a *//*
    void mo13494a(C5931adH adh);
*/
    /* renamed from: a */
    void creatListenerChannel(Connect omVar);

    /* renamed from: af */
    void removeChannelAndClose(int i) throws IOException;

    /* renamed from: c */
    void authorizationChannelClose(ClientConnect bVar);

    void close();

    /* renamed from: iX */
    boolean isRunningThreadSelectableChannel();

    /* renamed from: iY */
    boolean isListenerPort();

    boolean isUp();

    /* renamed from: jH */
    ClientConnect getClientConnect();

    /* renamed from: jI */
    MessageProcessing getMessageProcessing();

    /* renamed from: jJ */
    ProcesTransportMessage getProcessSendMessage();

    /* renamed from: jK */
    ClientConnect getClientConnectLocal();

    /* renamed from: ja */
    long metricClientTimeAuthorization();

    /* renamed from: jb */
    long metricCountWriteMessage();

    /* renamed from: jc */
    long metricCountReadMessage();

    /* renamed from: jd */
    long metricAllOriginalByteLength();

    /* renamed from: je */
    long metricByteLength();

    /* renamed from: jf */
    long metricReadByte();

    /* renamed from: jg */
    long metricWriteByte();

    /* renamed from: jh */
    InetAddress getIpRemoteHost();
}
