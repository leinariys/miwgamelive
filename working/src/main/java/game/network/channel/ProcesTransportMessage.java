package game.network.channel;

import game.network.channel.client.ClientConnect;

import java.io.IOException;

/* renamed from: a.aGP */
/* compiled from: a */
public interface ProcesTransportMessage {
    /* renamed from: a */
    void sendTransportMessage(ClientConnect bVar, byte[] bArr, int i, int i2) throws IOException;

    /* renamed from: b */
    void sendTransportMessage(byte[] bArr, int i, int i2) throws IOException;

    void close() throws IOException;
}
