package game.network.message;

import java.io.Closeable;
import java.io.DataOutput;
import java.io.Flushable;

/* renamed from: a.oU */
/* compiled from: a */
public interface WriterBitsData extends WriterBits, Closeable, DataOutput, Flushable {
}
