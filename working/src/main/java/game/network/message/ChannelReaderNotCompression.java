package game.network.message;


import logic.thred.LogPrinter;
import taikodom.render.textures.DDSLoader;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.Channel;
import java.util.LinkedList;

/* renamed from: a.aud  reason: case insensitive filesystem */
/* compiled from: a */
public class ChannelReaderNotCompression extends ChannelBufferReader {
    /* renamed from: pa */
    private final int sizeBlock;
    private ByteBuffer buffer;
    private int startPosition;
    private int lengthCopied;
    private LinkedList<ByteMessageCompres> messages;
    private byte[] bytes;
    private int metricReadByte;
    private int countReadMessage;
    /* renamed from: oZ */
    private int anInt;
    /* renamed from: pd */
    private int byteLength;
    private boolean special;

    public ChannelReaderNotCompression(int i, int sizeBlock) {
        this.buffer = ByteBuffer.allocate(1024);
        this.anInt = 48151623;
        this.startPosition = -1;
        this.lengthCopied = -1;
        this.messages = new LinkedList<>();
        this.bytes = new byte[1024];
        this.sizeBlock = sizeBlock;
        if (sizeBlock >= 536870911) {
            throw new IllegalArgumentException("Max block sise too big " + sizeBlock);
        }
    }

    public ChannelReaderNotCompression() {
        this.buffer = ByteBuffer.allocate(1024);
        this.anInt = 48151623;
        this.startPosition = -1;
        this.lengthCopied = -1;
        this.messages = new LinkedList<>();
        this.bytes = new byte[1024];
        this.sizeBlock = DDSLoader.DDSD_DEPTH;
    }

    public ByteMessageCompres getMessage() {
        return this.messages.removeFirst();
    }

    public boolean isNotEmpty() {
        return this.messages.size() > 0;
    }

    /* renamed from: b */
    public int readerChannelToBuffer(Channel channel) throws IOException {
        int read = ((ByteChannel) channel).read(this.buffer);
        if (read == -1) {
            return -1;
        }
        this.metricReadByte += read;
        if (this.buffer.position() >= this.buffer.capacity() - 1) {
            ByteBuffer allocate = ByteBuffer.allocate(this.buffer.capacity() << 1);
            this.buffer.flip();
            allocate.put(this.buffer);
            this.buffer = allocate;
            read += ((ByteChannel) channel).read(this.buffer);
        }
        if (read <= 0) {
            return read;
        }
        decryptionByteXorSalt(read);
        do {
            if (this.lengthCopied == -1) {
                readHeader();
            }
            if (this.lengthCopied == -1) {
                return read;
            }
            if (this.lengthCopied > this.sizeBlock) {
                throw new IllegalArgumentException("Message too big: " + this.lengthCopied);
            } else if (this.buffer.position() < this.lengthCopied + this.startPosition) {
                return read;
            } else {
                readerBufferToMessage();
            }
        } while (this.buffer.position() > 0);
        return read;
    }

    private void readerBufferToMessage() {
        byte[] array = this.buffer.array();
        byte[] bArr = new byte[this.lengthCopied];
        System.arraycopy(array, this.startPosition, bArr, 0, this.lengthCopied);
        ByteMessageCompres jb = new ByteMessageCompres(bArr, 0, bArr.length);
        jb.setSpecial(this.special);
        this.messages.add(jb);
        if (this.buffer.position() > this.lengthCopied + this.startPosition) {
            System.arraycopy(array, this.lengthCopied + this.startPosition, array, 0, this.buffer.position() - (this.lengthCopied + this.startPosition));
            this.buffer.position(this.buffer.position() - (this.lengthCopied + this.startPosition));
        } else {
            this.buffer.clear();
        }
        this.countReadMessage++;
        this.byteLength += bArr.length;
        this.lengthCopied = -1;
        this.startPosition = -1;
    }

    private void readHeader() {
        int i = 0;
        byte b = this.buffer.get(0);
        if (b != 0) {
            this.special = false;
        } else if (this.buffer.position() >= 2) {
            b = this.buffer.get(1);
            this.special = true;
            i = 1;
        } else {
            return;
        }
        switch ((b & 96) >> 5) {
            case 0:
                this.startPosition = i + 1;
                this.lengthCopied = b & 31;
                return;
            case 1:
                this.startPosition = i + 2;
                if (this.buffer.position() > 1) {
                    this.lengthCopied = (this.buffer.get(i + 1) & 255) | ((b & 31) << 8);
                    return;
                }
                return;
            case 2:
                this.startPosition = i + 3;
                if (this.buffer.position() > 2) {
                    this.lengthCopied = (this.buffer.get(i + 2) & 255) | ((this.buffer.get(i + 1) & 255) << 8) | ((b & 31) << LogPrinter.eqN);
                    return;
                }
                return;
            case 3:
                this.startPosition = i + 4;
                if (this.buffer.position() > 4) { //24    C6215aif.idH
                    this.lengthCopied = (this.buffer.get(i + 3) & 255) | ((this.buffer.get(i + 1) & 255) << LogPrinter.eqN) | ((b & 31) << 24) | ((this.buffer.get(i + 2) & 255) << 8);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* renamed from: gx */
    private void decryptionByteXorSalt(int i) {
        byte[] array = this.buffer.array();
        int position = this.buffer.position();
        for (int i2 = position - i; i2 < position; i2++) {
            int i3 = this.anInt;
            byte b = (byte) (((array[i2] & 255) ^ (this.anInt & 255)) & 255);
            array[i2] = b;
            this.anInt = WriteEncryptionOutputStream.calculationSaltKey(i3, b);
        }
    }

    public int getMetricReadByte() {
        return this.metricReadByte;
    }

    public int getCountReadMessage() {
        return this.countReadMessage;
    }

    public int getByteLength() {
        return this.byteLength;
    }
}
