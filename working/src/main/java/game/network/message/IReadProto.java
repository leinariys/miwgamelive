package game.network.message;

import game.network.IReadBits;

import java.io.Closeable;
import java.io.DataInput;

/* renamed from: a.Ie */
/* compiled from: a */
public interface IReadProto extends IReadBits, Closeable, DataInput {
}
