package game.network.message;

import game.network.Transport;

import java.io.EOFException;
import java.io.Serializable;

/* compiled from: a */
public class TransportResponse extends Transport implements Serializable {

    public TransportResponse() {
    }

    public TransportResponse(ByteMessageReader amf) throws EOFException {
        super(amf);
    }

    public int getType() {
        return TRANSPORT_RESPONSE;
    }
}
