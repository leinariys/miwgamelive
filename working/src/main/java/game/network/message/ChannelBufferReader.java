package game.network.message;


import java.io.IOException;
import java.nio.channels.Channel;

/* renamed from: a.TT */
/* compiled from: a */
public abstract class ChannelBufferReader {

    public abstract ByteMessageCompres getMessage();

    /* renamed from: b */
    public abstract int readerChannelToBuffer(Channel channel) throws IOException;

    public abstract boolean isNotEmpty();

    public abstract int getMetricReadByte();

    public abstract int getCountReadMessage();

    public abstract int getByteLength();

}
