package game.network.thread;

import game.network.channel.HandlerChannel;
import game.network.channel.MessageHandler;
import game.network.channel.server.ChechKeepAlive;
import gnu.trove.THashSet;
import gnu.trove.TObjectProcedure;
import logic.thred.LogPrinter;

import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: a.agX  reason: case insensitive filesystem */
/* compiled from: a */
public class SelectableChannelThread {
    static LogPrinter logger = LogPrinter.m10275K(SelectableChannelThread.class);
    private final TObjectProcedure<SelectionKey> messageHandler = new MessageHandler(this);
    /* access modifiers changed from: private */
    public Selector selector;
    private THashSet<SelectionKey> receivedMessages = new THashSet<>();
    private THashSet<SelectionKey> messagesToBeAnswered = new THashSet<>();
    /**
     * Очередь каналов на добавление в потоке прослушивания соединений
     */
    private LinkedBlockingQueue<Runnable> queueAddChannel = new LinkedBlockingQueue<>();
    private ChechKeepAlive chechKeepAlive;
    private int poolCount;
    private int forcedCount;
    private int uselessWakeupCount;
    private ReentrantLock reentrantLock = new ReentrantLock();
    private boolean running = false;
    private Thread thread;

    public SelectableChannelThread() {
        try {
            selector = Selector.open();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isRunning() {
        return this.running;
    }

    public void close() {
        if (this.running) {
            this.running = false;
            this.selector.wakeup();
            this.thread.interrupt();
            try {
                this.thread.join(1000);
            } catch (InterruptedException e) {
            }
        }
        try {
            synchronized (this.selector) {
                for (SelectionKey channel : this.selector.keys()) {
                    try {
                        channel.channel().close();
                    } catch (Throwable th) {
                        logger.error(th.getMessage());
                    }
                }
            }
        } catch (Exception e2) {
        }
        try {
            this.selector.close();
        } catch (Exception e3) {
        }
    }

    public void start() {
        this.thread = new Thread(new ListeningRunnable(), "NIO Pooler Thread");
        this.thread.setDaemon(true);
        this.running = true;
        this.thread.start();
    }

    /* access modifiers changed from: protected */
    public void runListening() throws IOException {
        logger.info("Listening");
        while (this.running) {
            this.poolCount++;
            this.selector.select(50);
            while (this.queueAddChannel.size() > 0) {
                try {
                    this.queueAddChannel.take().run();
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
            this.reentrantLock.lock();
            try {
                if (this.receivedMessages.size() > 0) {
                    THashSet<SelectionKey> tHashSet = this.receivedMessages;
                    this.receivedMessages = this.messagesToBeAnswered;
                    this.messagesToBeAnswered = tHashSet;
                    this.receivedMessages.clear();
                }
                this.reentrantLock.unlock();
                if (this.messagesToBeAnswered.size() > 0) {
                    this.forcedCount += this.messagesToBeAnswered.size();
                    this.messagesToBeAnswered.forEach(this.messageHandler);
                    this.messagesToBeAnswered.clear();
                }
                Set<SelectionKey> selectedKeys = this.selector.selectedKeys();
                if (selectedKeys.size() > 0) {
                    Iterator<SelectionKey> it = selectedKeys.iterator();
                    while (it.hasNext()) {
                        SelectionKey key = it.next();
                        try {
                            ((HandlerChannel) key.attachment()).isReadyKeyChannelForOperations(key);
                        } catch (Throwable th2) {
                            exceptionHandling(th2, key);
                        }
                        it.remove();
                    }
                } else {
                    this.uselessWakeupCount++;
                }
                if (this.chechKeepAlive != null) {
                    this.chechKeepAlive.checkAliveChannel();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } catch (Throwable th3) {
                this.reentrantLock.unlock();
                throw th3;
            }
        }
        this.running = false;
        logger.info("poolCount          = " + this.poolCount);
        logger.info("forcedCount        = " + this.forcedCount);
        logger.info("uselessWakeupCount = " + this.uselessWakeupCount);
    }

    /* renamed from: a */
    public void сhannelRegister(HandlerChannel handlerChannel) throws ClosedChannelException {
        //Добавление первого канала(сам сервер) или добавление каналов в потоке NIO Pooler Thread
        if (!this.running || Thread.currentThread() == this.thread) {
            SelectableChannel channel = handlerChannel.getChannel();
            SelectionKey selectionKey = channel.register(this.selector, handlerChannel.prochentResultingKey());
            logger.info("Channel added: " + selectionKey.channel());
            selectionKey.attach(handlerChannel);
            handlerChannel.initHandlerChannel(selectionKey, this);
            try {
                handlerChannel.sendHandshake1();
            } catch (IOException e) {
                e.printStackTrace();
                try {
                    channel.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                    selectionKey.cancel();
                }
            }
        } else {//Добавление в очередь на добавление в потоке NIO Pooler Thread нового канала
            this.queueAddChannel.add(new ChannelRegisterRunnable(handlerChannel));
            this.selector.wakeup();
        }
    }


    /* access modifiers changed from: private */
    /* renamed from: a */
    public void exceptionHandling(Throwable th, SelectionKey selectionKey) {
        if (th instanceof IOException) {
            logger.debug("Connection closed. Please do not worry about the following exception.", th);
            logger.warn("Connection closed due to exception: " + selectionKey);
        } else {
            logger.error("Connection closed. Please do not worry about the following exception.", th);
        }
        try {
            if (selectionKey.isValid()) {
                selectionKey.channel().close();
            }
        } catch (Throwable th2) {
            Throwable th3 = th2;
            try {
                ((HandlerChannel) selectionKey.attachment()).stepDisconnected();
            } catch (Throwable th4) {
                th4.printStackTrace();
            }
            try {
                throw th3;
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
        try {
            ((HandlerChannel) selectionKey.attachment()).stepDisconnected();
        } catch (Throwable th5) {
            th5.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public void signalReadyReadAndWrite(SelectionKey selectionKey) {
        if (Thread.currentThread() != this.thread) {
            this.reentrantLock.lock();
            try {
                if (this.receivedMessages.add(selectionKey) && this.receivedMessages.size() == 1) {
                    this.selector.wakeup();
                }
            } finally {
                this.reentrantLock.unlock();
            }
        } else {
            selectionKey.interestOps(5);//SelectionKey.OP_READ + SelectionKey.OP_WRITE
        }
    }

    public ChechKeepAlive getChechKeepAlive() {
        return this.chechKeepAlive;
    }

    /* renamed from: a */
    public void setChechKeepAlive(ChechKeepAlive abe) {
        this.chechKeepAlive = abe;
    }

    public Set<SelectionKey> getSelectorKeys() {
        return this.selector.keys();
    }

    /* renamed from: a.agX$a */
    class ListeningRunnable implements Runnable {
        ListeningRunnable() {
        }

        public void run() {
            try {
                SelectableChannelThread.this.runListening();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a.agX$b */
    /* compiled from: a */
    class ChannelRegisterRunnable implements Runnable {
        private final HandlerChannel channel;

        ChannelRegisterRunnable(HandlerChannel channel) {
            this.channel = channel;
        }

        public void run() {
            synchronized (SelectableChannelThread.this.selector) {
                try {
                    SelectableChannelThread.this.сhannelRegister(this.channel);
                } catch (ClosedChannelException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
