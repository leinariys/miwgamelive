package game.network.build;

import game.network.channel.client.ClientConnect;
import game.network.message.TransportCommand;
import game.network.message.TransportResponse;

/* renamed from: a.QF */
/* compiled from: a */
public interface TransportThread {
    /* renamed from: HX */
    int getThreadInteger();

    /* renamed from: HY */
    void pendingClose() throws InterruptedException;

    /* renamed from: a */
    void sendTransportMessageInSeparateThread(ClientConnect bVar, TransportCommand transportCommand);

    /* renamed from: ak */
    void setIsServer(boolean z);

    /* renamed from: b */
    TransportResponse sendTransportMessageAndWaitForResponse(ClientConnect bVar, TransportCommand transportCommand);

    /* renamed from: b */
    void sendTransportResponse(ClientConnect bVar, TransportResponse transportResponse);

    void dispose();
}
