package game.network.exception;

import game.network.channel.Connect;

import java.io.IOException;

/* renamed from: a.aoN  reason: case insensitive filesystem */
/* compiled from: a */
public class ChechUserException extends IOException {

    private final Connect.Status status;
    private final int count;

    public ChechUserException(Connect.Status aVar) {
        this.status = aVar;
        this.count = 0;
    }

    public ChechUserException(Connect.Status status, int count) {
        this.status = status;
        this.count = count;
    }

    public Connect.Status getStatus() {
        return this.status;
    }

    public int getCount() {
        return this.count;
    }
}
