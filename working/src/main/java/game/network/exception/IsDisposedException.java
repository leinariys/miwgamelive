package game.network.exception;

/* renamed from: a.aDV */
/* compiled from: a */
public class IsDisposedException extends RuntimeException {


    public IsDisposedException() {
    }

    public IsDisposedException(String str) {
        super(str);
    }

    public IsDisposedException(Throwable th) {
        super(th);
    }

    public IsDisposedException(String str, Throwable th) {
        super(str, th);
    }
}
