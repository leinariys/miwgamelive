package game.server;

import lombok.extern.slf4j.Slf4j;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.Undefined;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

@Slf4j
/* renamed from: a.Hd */
/* compiled from: a */
public class ServerConsoleConnect {
    private static ClassLoader classLoader = ServerConsoleConnect.class.getClassLoader();
    public final int PORT = 5458;
    /* access modifiers changed from: private */
    public IServerConsole serverConsole;
    /* access modifiers changed from: private */
    public Socket sock;
    /* access modifiers changed from: private */
    public boolean started = false;
    private String filePathJs = "serverconsole.js";
    /* renamed from: TR */
    private Scriptable scriptable;
    private ThreadLocal<Context> threadLocal = new ThreadLocal<Context>() {
        @Override
        public Context initialValue() {
            return ServerConsoleConnect.optimization();
        }
    };

    public ServerConsoleConnect() {
        try {
            this.sock = new Socket("localhost", PORT);
        } catch (UnknownHostException e) {
            log.error("Проблемы при подключении к сокету: Неизвестный хост");
        } catch (IOException e2) {
            log.error("Проблемы при подключении к сокету");
        }
    }

    /**
     * Уровни от 0 до 9 указывают, что файлы классов могут быть созданы.
     * Более высокие уровни оптимизации приносят компромисс между производительностью
     * времени компиляции и производительностью во время выполнения.
     *
     * @return
     */
    /* access modifiers changed from: private */
    public static Context optimization() {
        Context context = Context.enter();
        context.setOptimizationLevel(0);
        return context;
    }

    /* renamed from: BL */
    public void initScriptable() {
        if (this.scriptable == null) {
            //Context хранит информацию о среде выполнения скрипта.
            Context contextJs = this.threadLocal.get();
            //Инициализирует стандартные объекты ( Object, Functionи т. Д.).
            // Это необходимо сделать до того, как скрипты смогут быть выполнены
            this.scriptable = contextJs.initStandardObjects();
            try {

                InputStream inputStream = classLoader.getResourceAsStream(filePathJs);
                if (inputStream == null) {
                    throw new RuntimeException(" ");
                }
                Object result = contextJs.evaluateReader(this.scriptable, new InputStreamReader(inputStream), "<ServerConsole>", 1, null);
                System.out.println(contextJs.toString(result));
            } catch (Exception e) {
                e.printStackTrace();
                Context.exit();
            }

            this.scriptable.put("_sconsole_", this.scriptable, (Object) this);
            this.scriptable.put("out", this.scriptable, (Object) System.out);
        }
    }

    /* renamed from: ev */
    public Object runJs(String inputMes) {
        initScriptable();
        Object result = this.threadLocal.get().evaluateString(this.scriptable, inputMes, "<ServerConsole>", 1, (Object) null);
        if (!(result instanceof Undefined)) {
            return result;
        }
        return null;
    }

    /* renamed from: cP */
    public void writeSocketUTF8(String inputMes) throws IOException {
        OutputStream outputStream = this.sock.getOutputStream();
        outputStream.write((String.valueOf(inputMes) + "\n").getBytes("UTF-8"));
        outputStream.flush();
        receivedMessage();
    }

    public void receivedMessage() {
        if (!this.started) {
            new ThreadServerConsole().start();
        }
    }

    public void clear() {
        if (this.serverConsole != null) {
            this.serverConsole.clearConsole();
        }
    }

    /* renamed from: a */
    public void setServerConsole(IServerConsole serverConsole) {
        this.serverConsole = serverConsole;
    }

    /* renamed from: a.Hd$a */
    class ThreadServerConsole extends Thread {

        public void run() {
            try {
                ServerConsoleConnect.this.started = true;
                while (ServerConsoleConnect.this.started) {
                    StringBuffer stringBuffer = new StringBuffer();
                    //Отправка на сервер
                    while (ServerConsoleConnect.this.sock.getInputStream().available() > 0) {
                        stringBuffer.append((char) ServerConsoleConnect.this.sock.getInputStream().read());
                    }
                    //Получение ответа с сервера
                    if (stringBuffer.length() > 0 && ServerConsoleConnect.this.serverConsole != null) {
                        ServerConsoleConnect.this.serverConsole.setMessageConsole(stringBuffer.toString());
                    }
                    Thread.sleep(100);
                }
            } catch (Exception e) {
                e.printStackTrace();
                ServerConsoleConnect.this.started = false;
            }
        }
    }
}
