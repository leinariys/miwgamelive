package game.geometry;

/* renamed from: a.te */
/* compiled from: a */
public interface IGeometryF {
    /* renamed from: a */
    float dot(Vec3fWrap iw);

    float getX();

    float getY();

    float getZ();

    float get(int i);

    float length();

    float lengthSquared();
}
