package game.geometry;

import game.io.IExternalIO;
import game.io.IReadExternal;
import game.io.IWriteExternal;

import javax.vecmath.Tuple2d;
import javax.vecmath.Tuple2f;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector2f;
import java.io.IOException;

/* renamed from: a.aKa  reason: case insensitive filesystem */
/* compiled from: a */
public class Vector2fWrap extends Vector2f implements IExternalIO {
    public Vector2fWrap() {
    }

    public Vector2fWrap(float[] fArr) {
        super(fArr);
    }

    public Vector2fWrap(Vector2f vector2f) {
        super(vector2f);
    }

    public Vector2fWrap(Vector2d vector2d) {
        super(vector2d);
    }

    public Vector2fWrap(Tuple2f tuple2f) {
        super(tuple2f);
    }

    public Vector2fWrap(Tuple2d tuple2d) {
        super(tuple2d);
    }

    public Vector2fWrap(float f, float f2) {
        super(f, f2);
    }

    /* renamed from: mU */
    public Vector2fWrap mo9759mU(float f) {
        Vector2fWrap aka = new Vector2fWrap();
        aka.x = this.x * f;
        aka.y = this.y * f;
        return aka;
    }

    public void add(double d, double d2) {
        this.x = (float) (((double) this.x) + d);
        this.y = (float) (((double) this.y) + d2);
    }

    /* renamed from: f */
    public Vector2fWrap mo9757f(double d, double d2) {
        this.x = (float) (((double) this.x) + d);
        this.y = (float) (((double) this.y) + d2);
        return this;
    }

    /* renamed from: a */
    public Vector2fWrap mo9755a(Tuple2f tuple2f) {
        this.x -= tuple2f.x;
        this.y -= tuple2f.y;
        return this;
    }

    /* renamed from: g */
    public Vector2fWrap mo9758g(double d, double d2) {
        Vector2fWrap aka = new Vector2fWrap((Vector2f) this);
        aka.x = (float) (((double) aka.x) + d);
        aka.y = (float) (((double) aka.y) + d2);
        return aka;
    }

    public void readExternal(IReadExternal vm) throws IOException {
        this.x = vm.readFloat("x");
        this.y = vm.readFloat("y");
    }

    public void writeExternal(IWriteExternal att) throws IOException {
        att.writeFloat("x", this.x);
        att.writeFloat("y", this.y);
    }
}
