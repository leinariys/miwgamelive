package game.geometry;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.aTw  reason: case insensitive filesystem */
/* compiled from: a */
public class C5712aTw {
    private static final float iPG = 0.001f;

    /* renamed from: Oj */
    private Vec3f f3838Oj = new Vec3f();
    private float distance = 0.0f;

    public void normalize() {
        float lengthSquared = this.f3838Oj.lengthSquared();
        if (lengthSquared > 0.0f) {
            float sqrt = 1.0f / ((float) Math.sqrt((double) lengthSquared));
            this.f3838Oj.x *= sqrt;
            this.f3838Oj.y *= sqrt;
            this.f3838Oj.z *= sqrt;
            this.distance = sqrt * this.distance;
        }
    }

    /* renamed from: bV */
    public C1827a mo11569bV(Vec3f vec3f) {
        float f = (vec3f.x * this.f3838Oj.x) + (vec3f.y * this.f3838Oj.y) + (vec3f.z * this.f3838Oj.z) + this.distance;
        if (f > iPG) {
            return C1827a.POINT_IN_FRONT_OF_PLANE;
        }
        if (f < -0.001f) {
            return C1827a.POINT_BEHIND_PLANE;
        }
        return C1827a.POINT_ON_PLANE;
    }

    /* renamed from: t */
    public void mo11575t(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        float f = vec3f2.x - vec3f.x;
        float f2 = vec3f2.z - vec3f.z;
        float f3 = vec3f2.z - vec3f.z;
        float f4 = vec3f3.x - vec3f.x;
        float f5 = vec3f3.z - vec3f.z;
        float f6 = vec3f3.z - vec3f.z;
        this.f3838Oj.x = (f2 * f6) - (f3 * f5);
        this.f3838Oj.y = (f3 * f4) - (f6 * f);
        this.f3838Oj.z = (f * f5) - (f2 * f4);
        this.f3838Oj.normalize();
        this.distance = -((this.f3838Oj.x * vec3f.x) + (this.f3838Oj.y * vec3f.y) + (this.f3838Oj.z * vec3f.z));
    }

    /* renamed from: b */
    public void mo11568b(C5712aTw atw) {
        this.f3838Oj.set(atw.f3838Oj);
        this.distance = atw.distance;
    }

    /* renamed from: a */
    public void mo11566a(C0107BM bm) {
        this.f3838Oj.set(bm.getNormal().dfV());
        this.distance = (float) bm.dnz();
    }

    /* renamed from: b */
    public void mo11567b(float f, float f2, float f3) {
        this.f3838Oj.set(f, f2, f3);
    }

    /* renamed from: l */
    public void mo11572l(Vec3f vec3f) {
        this.f3838Oj.set(vec3f);
    }

    /* renamed from: vO */
    public Vec3f mo11576vO() {
        return this.f3838Oj;
    }

    public float getDistance() {
        return this.distance;
    }

    public void setDistance(float f) {
        this.distance = f;
    }

    /* renamed from: x */
    public void mo11577x(float f, float f2, float f3, float f4) {
        this.f3838Oj.x = f;
        this.f3838Oj.y = f2;
        this.f3838Oj.z = f3;
        this.distance = f4;
    }

    /* renamed from: bW */
    public float mo11570bW(Vec3f vec3f) {
        return (vec3f.x * this.f3838Oj.x) + (vec3f.y * this.f3838Oj.y) + (vec3f.z * this.f3838Oj.z) + this.distance;
    }

    /* renamed from: a.aTw$a */
    public enum C1827a {
        POINT_ON_PLANE,
        POINT_IN_FRONT_OF_PLANE,
        POINT_BEHIND_PLANE
    }
}
