package game.geometry;

import org.mozilla.javascript.ScriptRuntime;

/* renamed from: a.BM */
/* compiled from: a */
public class C0107BM {
    private static final double izm = 0.001d;
    private Vec3d izn = new Vec3d();
    private double izo = ScriptRuntime.NaN;

    public void normalize() {
        double lengthSquared = this.izn.lengthSquared();
        if (lengthSquared > ScriptRuntime.NaN) {
            double sqrt = 1.0d / Math.sqrt(lengthSquared);
            this.izn.x *= sqrt;
            this.izn.y *= sqrt;
            this.izn.z *= sqrt;
            this.izo = sqrt * this.izo;
        }
    }

    /* renamed from: aM */
    public C0108a mo716aM(Vec3d ajr) {
        double d = (ajr.x * this.izn.x) + (ajr.y * this.izn.y) + (ajr.z * this.izn.z) + this.izo;
        if (d > izm) {
            return C0108a.POINT_IN_FRONT_OF_PLANE;
        }
        if (d < -0.001d) {
            return C0108a.POINT_BEHIND_PLANE;
        }
        return C0108a.POINT_ON_PLANE;
    }

    /* renamed from: f */
    public void mo722f(Vec3d ajr, Vec3d ajr2, Vec3d ajr3) {
        double d = ajr2.x - ajr.x;
        double d2 = ajr2.z - ajr.z;
        double d3 = ajr2.z - ajr.z;
        double d4 = ajr3.x - ajr.x;
        double d5 = ajr3.z - ajr.z;
        double d6 = ajr3.z - ajr.z;
        this.izn.x = (d2 * d6) - (d3 * d5);
        this.izn.y = (d3 * d4) - (d6 * d);
        this.izn.z = (d * d5) - (d2 * d4);
        this.izn.normalize();
        this.izo = -((this.izn.x * ajr.x) + (this.izn.y * ajr.y) + (this.izn.z * ajr.z));
    }

    /* renamed from: a */
    public void mo715a(C0107BM bm) {
        this.izn.mo9484aA(bm.izn);
        this.izo = bm.izo;
    }

    /* renamed from: b */
    public void mo719b(C5712aTw atw) {
        this.izn.mo9484aA(atw.mo11576vO().dfR());
        this.izo = (double) atw.getDistance();
    }

    public void setNormal(double d, double d2, double d3) {
        this.izn.set(d, d2, d3);
    }

    public Vec3d getNormal() {
        return this.izn;
    }

    public void setNormal(Vec3d ajr) {
        this.izn.mo9484aA(ajr);
    }

    /* renamed from: ac */
    public void mo718ac(double d) {
        this.izo = d;
    }

    public double dnz() {
        return this.izo;
    }

    /* renamed from: c */
    public void mo720c(double d, double d2, double d3, double d4) {
        this.izn.x = d;
        this.izn.y = d2;
        this.izn.z = d3;
        this.izo = d4;
    }

    /* renamed from: aN */
    public double mo717aN(Vec3d ajr) {
        return (ajr.x * this.izn.x) + (ajr.y * this.izn.y) + (ajr.z * this.izn.z) + this.izo;
    }

    public String toString() {
        return "Normal: " + this.izn + ", d:" + this.izo;
    }

    /* renamed from: a.BM$a */
    public enum C0108a {
        POINT_ON_PLANE,
        POINT_IN_FRONT_OF_PLANE,
        POINT_BEHIND_PLANE
    }
}
