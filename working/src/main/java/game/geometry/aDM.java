package game.geometry;

import javax.vecmath.Tuple3d;

/* renamed from: a.aDM */
/* compiled from: a */
public class aDM {
    private final Vec3d endPos;
    private final Vec3d rayDelta;
    private final Vec3d startPos;
    private boolean intersected = false;
    private double paramIntersection = Double.POSITIVE_INFINITY;
    private C0107BM plane = new C0107BM();

    public aDM(Vec3d ajr, Vec3d ajr2) {
        this.startPos = new Vec3d(ajr);
        this.endPos = new Vec3d(ajr2);
        this.rayDelta = ajr2.mo9517f((Tuple3d) ajr);
    }

    public void set(aDM adm) {
        setIntersected(adm.intersected);
        setParamIntersection(adm.paramIntersection);
        setPlane(adm.plane);
    }

    public void set(C2567gu guVar) {
        setIntersected(guVar.isIntersected());
        setParamIntersection((double) guVar.mo19142vM());
        this.plane.mo719b(guVar.mo19143vN());
    }

    public final boolean isIntersected() {
        return this.intersected;
    }

    public final void setIntersected(boolean z) {
        this.intersected = z;
    }

    public final double getParamIntersection() {
        return this.paramIntersection;
    }

    public final void setParamIntersection(double d) {
        this.paramIntersection = d;
    }

    public final C0107BM getPlane() {
        return this.plane;
    }

    public final void setPlane(C0107BM bm) {
        this.plane.mo715a(bm);
    }

    public void setNormal(double d, double d2, double d3) {
        this.plane.setNormal(d, d2, d3);
    }

    public Vec3d getNormal() {
        return this.plane.getNormal();
    }

    public void setNormal(Vec3d ajr) {
        this.plane.setNormal(ajr);
    }

    public final Vec3d getStartPos() {
        return this.startPos;
    }

    public final Vec3d getEndPos() {
        return this.endPos;
    }

    public final Vec3d getRayDelta() {
        return this.rayDelta;
    }

    public void updateRay(Vec3d ajr, Vec3d ajr2) {
        this.startPos.mo9484aA(ajr);
        this.endPos.mo9484aA(ajr2);
        this.rayDelta.mo9484aA(ajr2.mo9517f((Tuple3d) ajr));
    }
}
