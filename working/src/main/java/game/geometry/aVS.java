package game.geometry;

/* renamed from: a.aVS */
/* compiled from: a */
public class aVS implements C6600aqA {
    public final Vec3d center = new Vec3d();
    public double jcB;

    /* renamed from: zO */
    public Vec3d mo11775zO() {
        return this.center;
    }

    public double dCJ() {
        return this.jcB;
    }

    /* renamed from: h */
    public void mo11774h(Vec3d ajr) {
        this.center.mo9484aA(ajr);
    }

    /* renamed from: ag */
    public void mo11771ag(double d) {
        this.jcB = d;
    }

    /* renamed from: a */
    public void mo4861a(Matrix3fWrap ajd, Vec3d ajr, Vec3d ajr2) {
        double d = this.jcB;
        Vec3d ajr3 = this.center;
        double d2 = (((double) ajd.m00) * ajr3.x) + (((double) ajd.m01) * ajr3.y) + (((double) ajd.m02) * ajr3.z);
        double d3 = (((double) ajd.m10) * ajr3.y) + (((double) ajd.m11) * ajr3.y) + (((double) ajd.m12) * ajr3.z);
        double d4 = (ajr3.z * ((double) ajd.m22)) + (((double) ajd.m20) * ajr3.z) + (((double) ajd.m21) * ajr3.y);
        ajr.x = d2 + d;
        ajr.y = d3 + d;
        ajr.z = d4 + d;
        ajr2.x = d2 - d;
        ajr2.y = d3 - d;
        ajr2.z = d4 - d;
    }

    /* renamed from: H */
    public void mo11770H(double d, double d2, double d3) {
        this.center.set(d, d2, d3);
    }

    /* renamed from: b */
    public void mo11772b(aVS avs) {
        this.center.mo9484aA(avs.center);
        this.jcB = avs.jcB;
    }
}
