package game.geometry;

import com.hoplon.geometry.Vec3f;
import game.io.IExternalIO;
import game.io.IReadExternal;
import game.io.IWriteExternal;
import org.mozilla.javascript.ScriptRuntime;

import javax.vecmath.Quat4f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;
import java.io.IOException;

/* renamed from: a.aoY  reason: case insensitive filesystem */
/* compiled from: a */
public class Quat4fWrap extends Quat4f implements IExternalIO {
    public static final float ANGLE_PRECISION = 5.0E-5f;
    public static final float TRACE_PRECISION = 1.0E-5f;
    private static final double PI_180 = 0.017453292519943295d;

    public Quat4fWrap() {
        this.w = 1.0f;
        this.z = 0.0f;
        this.y = 0.0f;
        this.x = 0.0f;
    }

    public Quat4fWrap(Quat4fWrap aoy) {
        super(aoy);
    }

    public Quat4fWrap(float[] fArr) {
        super(fArr);
    }

    public Quat4fWrap(float f, float f2, float f3, float f4) {
        super(f, f2, f3, f4);
    }

    public Quat4fWrap(Vec3f vec3f, double d) {
        double d2 = d / 2.0d;
        double sin = Math.sin(d2);
        double cos = Math.cos(d2);
        Vec3f vec3f2 = new Vec3f((Vector3f) vec3f);
        vec3f2.normalize();
        this.w = (float) cos;
        this.x = (float) (((double) vec3f2.x) * sin);
        this.y = (float) (((double) vec3f2.y) * sin);
        this.z = (float) (((double) vec3f2.z) * sin);
    }

    public Quat4fWrap(double d, double d2, double d3) {
        mo15244h(d, d2, d3);
    }

    public Quat4fWrap(Matrix4fWrap ajk) {
        set(ajk);
    }

    public Quat4fWrap(Matrix3fWrap ajd) {
        mo15236e(ajd);
    }

    /* renamed from: c */
    public static Quat4fWrap m24428c(Vec3f vec3f, double d) {
        return new Quat4fWrap(vec3f, d);
    }

    /* renamed from: d */
    public static Quat4fWrap m24430d(Vec3f vec3f, double d) {
        return new Quat4fWrap(vec3f, PI_180 * d);
    }

    /* renamed from: d */
    public static Quat4fWrap m24429d(double d, double d2, double d3) {
        return new Quat4fWrap(d, d2, d3);
    }

    /* renamed from: c */
    public static Quat4fWrap m24427c(double d, double d2, double d3) {
        return new Quat4fWrap(d * PI_180, d2 * PI_180, PI_180 * d3);
    }

    /* renamed from: a */
    public static Quat4fWrap m24423a(Quat4f quat4f, Quat4f quat4f2) {
        return new Quat4fWrap().mo15213b(quat4f, quat4f2);
    }

    /* renamed from: a */
    public static void m24424a(Quat4fWrap aoy, Vec3f vec3f, Vec3f vec3f2) {
        float f = vec3f.x;
        float f2 = vec3f.y;
        float f3 = vec3f.z;
        if (isZero(f) && isZero(f2) && isZero(f3)) {
            vec3f2.x = f;
            vec3f2.y = f2;
            vec3f2.z = f3;
        }
        float f4 = aoy.x;
        float f5 = aoy.z;
        float f6 = aoy.y;
        float f7 = aoy.w;
        float f8 = (((-f4) * f) - (f6 * f2)) - (f5 * f3);
        float f9 = ((f7 * f) + (f6 * f3)) - (f5 * f2);
        float f10 = ((f7 * f2) + (f5 * f)) - (f4 * f3);
        float f11 = ((f7 * f3) + (f4 * f2)) - (f6 * f);
        float f12 = 1.0f / ((((f7 * f7) + (f4 * f4)) + (f6 * f6)) + (f5 * f5));
        if (((double) Math.abs(((f8 * f7) + (f9 * f4) + (f10 * f6) + (f11 * f5)) * f12)) > 0.1d) {
            vec3f2.x = f;
            vec3f2.y = f2;
            vec3f2.z = f3;
            return;
        }
        vec3f2.x = ((((f9 * f7) - (f8 * f4)) - (f10 * f5)) + (f11 * f6)) * f12;
        vec3f2.y = ((((f10 * f7) - (f8 * f6)) - (f11 * f4)) + (f9 * f5)) * f12;
        vec3f2.z = ((((f11 * f7) - (f8 * f5)) - (f9 * f6)) + (f10 * f4)) * f12;
    }

    /* renamed from: b */
    public static void m24425b(Quat4fWrap aoy, Vec3d ajr, Vec3d ajr2) {
        double d = ajr.x;
        double d2 = ajr.y;
        double d3 = ajr.z;
        if (m24421T(d) && m24421T(d2) && m24421T(d3)) {
            ajr2.x = d;
            ajr2.y = d2;
            ajr2.z = d3;
        }
        float f = aoy.x;
        float f2 = aoy.z;
        float f3 = aoy.y;
        float f4 = aoy.w;
        double d4 = ((((double) (-f)) * d) - (((double) f3) * d2)) - (((double) f2) * d3);
        double d5 = ((((double) f4) * d) + (((double) f3) * d3)) - (((double) f2) * d2);
        double d6 = ((((double) f4) * d2) + (((double) f2) * d)) - (((double) f) * d3);
        double d7 = ((((double) f4) * d3) + (((double) f) * d2)) - (((double) f3) * d);
        float f5 = 1.0f / ((((f4 * f4) + (f * f)) + (f3 * f3)) + (f2 * f2));
        if (Math.abs(((((double) f4) * d4) + (((double) f) * d5) + (((double) f3) * d6) + (((double) f2) * d7)) * ((double) f5)) > 0.1d) {
            ajr2.x = d;
            ajr2.y = d2;
            ajr2.z = d3;
            return;
        }
        ajr2.x = ((((((double) f4) * d5) - (((double) f) * d4)) - (((double) f2) * d6)) + (((double) f3) * d7)) * ((double) f5);
        ajr2.y = ((((((double) f4) * d6) - (((double) f3) * d4)) - (((double) f) * d7)) + (((double) f2) * d5)) * ((double) f5);
        ajr2.z = ((((((double) f4) * d7) - (((double) f2) * d4)) - (((double) f3) * d5)) + (((double) f) * d6)) * ((double) f5);
    }

    /* renamed from: b */
    private static void m24426b(Quat4fWrap aoy, Vec3f vec3f, Vec3f vec3f2) {
        float f = vec3f.x;
        float f2 = vec3f.y;
        float f3 = vec3f.z;
        if (isZero(f) && isZero(f2) && isZero(f3)) {
            vec3f2.x = f;
            vec3f2.y = f2;
            vec3f2.z = f3;
        }
        float f4 = aoy.w;
        float f5 = -aoy.x;
        float f6 = -aoy.y;
        float f7 = -aoy.z;
        float f8 = (((-f5) * f) - (f6 * f2)) - (f7 * f3);
        float f9 = ((f4 * f) + (f6 * f3)) - (f7 * f2);
        float f10 = ((f4 * f2) + (f7 * f)) - (f5 * f3);
        float f11 = ((f4 * f3) + (f5 * f2)) - (f6 * f);
        float f12 = 1.0f / ((((f4 * f4) + (f5 * f5)) + (f6 * f6)) + (f7 * f7));
        if (((double) Math.abs(((f8 * f4) + (f9 * f5) + (f10 * f6) + (f11 * f7)) * f12)) > 0.1d) {
            vec3f2.x = f;
            vec3f2.y = f2;
            vec3f2.z = f3;
            return;
        }
        vec3f2.x = ((((f9 * f4) - (f8 * f5)) - (f10 * f7)) + (f11 * f6)) * f12;
        vec3f2.y = ((((f10 * f4) - (f8 * f6)) - (f11 * f5)) + (f9 * f7)) * f12;
        vec3f2.z = ((((f11 * f4) - (f8 * f7)) - (f9 * f6)) + (f10 * f5)) * f12;
    }

    /* renamed from: a */
    public static Quat4fWrap m24422a(Quat4fWrap aoy, Quat4fWrap aoy2, float f, Quat4fWrap aoy3) {
        Quat4fWrap aoy4;
        float sin;
        Quat4fWrap zb = aoy.mo15254zb();
        Quat4fWrap zb2 = aoy2.mo15254zb();
        double d = (double) ((zb.w * zb2.w) + (zb.x * zb2.x) + (zb.y * zb2.y) + (zb.z * zb2.z));
        if (d < ScriptRuntime.NaN) {
            aoy4 = new Quat4fWrap(-zb2.x, -zb2.y, -zb2.z, -zb2.w);
            d = -d;
        } else {
            aoy4 = zb2;
        }
        if (d > 0.9998999834060669d) {
            sin = 1.0f - f;
        } else {
            float sqrt = (float) Math.sqrt(1.0d - (d * d));
            float atan2 = (float) Math.atan2((double) sqrt, d);
            float f2 = 1.0f / sqrt;
            sin = (float) (Math.sin((double) ((1.0f - f) * atan2)) * ((double) f2));
            f = (float) (Math.sin((double) (atan2 * f)) * ((double) f2));
        }
        aoy3.set((zb.x * sin) + (aoy4.x * f), (zb.y * sin) + (aoy4.y * f), (zb.z * sin) + (aoy4.z * f), (sin * zb.w) + (aoy4.w * f));
        aoy3.normalize();
        return aoy3;
    }

    public static boolean isZero(float f) {
        return f < TRACE_PRECISION && f > -TRACE_PRECISION;
    }

    /* renamed from: T */
    static boolean m24421T(double d) {
        return d < 9.999999747378752E-6d && d > -9.999999747378752E-6d;
    }

    /* renamed from: h */
    public void mo15244h(double d, double d2, double d3) {
        double sin = Math.sin(d2 / 2.0d);
        double cos = Math.cos(d2 / 2.0d);
        double sin2 = Math.sin(d3 / 2.0d);
        double cos2 = Math.cos(d3 / 2.0d);
        double sin3 = Math.sin(d / 2.0d);
        double cos3 = Math.cos(d / 2.0d);
        double d4 = cos * cos2;
        double d5 = sin * sin2;
        this.x = (float) ((sin3 * d4) - (cos3 * d5));
        this.y = (float) ((cos3 * sin * cos2) + (sin3 * cos * sin2));
        this.z = (float) (((cos * cos3) * sin2) - ((sin * sin3) * cos2));
        this.w = (float) ((cos3 * d4) + (sin3 * d5));
    }

    /* renamed from: i */
    public void mo15246i(double d, double d2, double d3) {
        mo15244h(d * PI_180, d2 * PI_180, d3 * PI_180);
    }

    public void set(Matrix4fWrap ajk) {
        float f = ajk.m00 + 1.0f + ajk.m11 + ajk.m22;
        if (f > TRACE_PRECISION) {
            float sqrt = ((float) Math.sqrt((double) f)) * 2.0f;
            this.x = (ajk.m21 - ajk.m12) / sqrt;
            this.y = (ajk.m02 - ajk.m20) / sqrt;
            this.z = (ajk.m10 - ajk.m01) / sqrt;
            this.w = sqrt * 0.25f;
        } else if (ajk.m00 > ajk.m11 && ajk.m00 > ajk.m22) {
            float sqrt2 = ((float) Math.sqrt(((((double) ajk.m00) + 1.0d) - ((double) ajk.m11)) - ((double) ajk.m22))) * 2.0f;
            this.x = 0.25f * sqrt2;
            this.y = (ajk.m10 + ajk.m01) / sqrt2;
            this.z = (ajk.m02 + ajk.m20) / sqrt2;
            this.w = (ajk.m12 - ajk.m21) / sqrt2;
        } else if (ajk.m11 > ajk.m22) {
            float sqrt3 = ((float) Math.sqrt(((((double) ajk.m11) + 1.0d) - ((double) ajk.m00)) - ((double) ajk.m22))) * 2.0f;
            this.x = (ajk.m10 + ajk.m01) / sqrt3;
            this.y = 0.25f * sqrt3;
            this.z = (ajk.m21 + ajk.m12) / sqrt3;
            this.w = (ajk.m02 - ajk.m20) / sqrt3;
        } else {
            float sqrt4 = ((float) Math.sqrt(((((double) ajk.m22) + 1.0d) - ((double) ajk.m00)) - ((double) ajk.m11))) * 2.0f;
            this.x = (ajk.m02 + ajk.m20) / sqrt4;
            this.y = (ajk.m21 + ajk.m12) / sqrt4;
            this.z = 0.25f * sqrt4;
            this.w = (ajk.m01 - ajk.m10) / sqrt4;
        }
        double bhp = bhp();
        if (!isZero(1.0f - ((float) bhp))) {
            double sqrt5 = Math.sqrt(bhp);
            this.w = (float) (((double) this.w) / sqrt5);
            this.x = (float) (((double) this.x) / sqrt5);
            this.y = (float) (((double) this.y) / sqrt5);
            this.z = (float) (((double) this.z) / sqrt5);
        }
    }

    /* renamed from: e */
    public void mo15236e(Matrix3fWrap ajd) {
        float f = ajd.m00 + 1.0f + ajd.m11 + ajd.m22;
        if (f > TRACE_PRECISION) {
            float sqrt = ((float) Math.sqrt((double) f)) * 2.0f;
            this.x = (ajd.m21 - ajd.m12) / sqrt;
            this.y = (ajd.m02 - ajd.m20) / sqrt;
            this.z = (ajd.m10 - ajd.m01) / sqrt;
            this.w = sqrt * 0.25f;
        } else if (ajd.m00 > ajd.m11 && ajd.m00 > ajd.m22) {
            float sqrt2 = ((float) Math.sqrt(((((double) ajd.m00) + 1.0d) - ((double) ajd.m11)) - ((double) ajd.m22))) * 2.0f;
            this.x = 0.25f * sqrt2;
            this.y = (ajd.m10 + ajd.m01) / sqrt2;
            this.z = (ajd.m02 + ajd.m20) / sqrt2;
            this.w = (ajd.m12 - ajd.m21) / sqrt2;
        } else if (ajd.m11 > ajd.m22) {
            float sqrt3 = ((float) Math.sqrt(((((double) ajd.m11) + 1.0d) - ((double) ajd.m00)) - ((double) ajd.m22))) * 2.0f;
            this.x = (ajd.m10 + ajd.m01) / sqrt3;
            this.y = 0.25f * sqrt3;
            this.z = (ajd.m21 + ajd.m12) / sqrt3;
            this.w = (ajd.m02 - ajd.m20) / sqrt3;
        } else {
            float sqrt4 = ((float) Math.sqrt(((((double) ajd.m22) + 1.0d) - ((double) ajd.m00)) - ((double) ajd.m11))) * 2.0f;
            this.x = (ajd.m02 + ajd.m20) / sqrt4;
            this.y = (ajd.m21 + ajd.m12) / sqrt4;
            this.z = 0.25f * sqrt4;
            this.w = (ajd.m01 - ajd.m10) / sqrt4;
        }
        double bhp = bhp();
        if (!isZero(1.0f - ((float) bhp))) {
            double sqrt5 = Math.sqrt(bhp);
            this.w = (float) (((double) this.w) / sqrt5);
            this.x = (float) (((double) this.x) / sqrt5);
            this.y = (float) (((double) this.y) / sqrt5);
            this.z = (float) (((double) this.z) / sqrt5);
        }
    }

    /* renamed from: q */
    public void mo15251q(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        float f = vec3f.x + 1.0f + vec3f2.y + vec3f3.z;
        if (f > TRACE_PRECISION) {
            float sqrt = ((float) Math.sqrt((double) f)) * 2.0f;
            this.x = (vec3f2.z - vec3f3.y) / sqrt;
            this.y = (vec3f3.x - vec3f.z) / sqrt;
            this.z = (vec3f.y - vec3f2.x) / sqrt;
            this.w = sqrt * 0.25f;
        } else if (vec3f.x > vec3f2.y && vec3f.x > vec3f3.z) {
            float sqrt2 = ((float) Math.sqrt(((((double) vec3f.x) + 1.0d) - ((double) vec3f2.y)) - ((double) vec3f3.z))) * 2.0f;
            this.x = 0.25f * sqrt2;
            this.y = (vec3f.y + vec3f2.x) / sqrt2;
            this.z = (vec3f3.x + vec3f.z) / sqrt2;
            this.w = (vec3f3.y - vec3f2.z) / sqrt2;
        } else if (vec3f2.y > vec3f3.z) {
            float sqrt3 = ((float) Math.sqrt(((((double) vec3f2.y) + 1.0d) - ((double) vec3f.x)) - ((double) vec3f3.z))) * 2.0f;
            this.x = (vec3f.y + vec3f2.x) / sqrt3;
            this.y = 0.25f * sqrt3;
            this.z = (vec3f2.z + vec3f3.y) / sqrt3;
            this.w = (vec3f3.x - vec3f.z) / sqrt3;
        } else {
            float sqrt4 = ((float) Math.sqrt(((((double) vec3f3.z) + 1.0d) - ((double) vec3f.x)) - ((double) vec3f2.y))) * 2.0f;
            this.x = (vec3f3.x + vec3f.z) / sqrt4;
            this.y = (vec3f2.z + vec3f3.y) / sqrt4;
            this.z = 0.25f * sqrt4;
            this.w = (vec3f2.x - vec3f.y) / sqrt4;
        }
        double bhp = bhp();
        if (!isZero(1.0f - ((float) bhp))) {
            double sqrt5 = Math.sqrt(bhp);
            this.w = (float) (((double) this.w) / sqrt5);
            this.x = (float) (((double) this.x) / sqrt5);
            this.y = (float) (((double) this.y) / sqrt5);
            this.z = (float) (((double) this.z) / sqrt5);
        }
    }

    public double bho() {
        return Math.sqrt((double) ((this.w * this.w) + (this.x * this.x) + (this.y * this.y) + (this.z * this.z)));
    }

    public double bhp() {
        return (double) ((this.w * this.w) + (this.x * this.x) + (this.y * this.y) + (this.z * this.z));
    }

    /* renamed from: zd */
    public Quat4fWrap mo15256zd() {
        return new Quat4fWrap(-this.x, -this.y, -this.z, this.w);
    }

    public Quat4fWrap coq() {
        conjugate();
        return this;
    }

    /* renamed from: zc */
    public Quat4fWrap mo15255zc() {
        double bho = bho();
        return new Quat4fWrap(-((float) (((double) this.x) / bho)), -((float) (((double) this.y) / bho)), -((float) (((double) this.z) / bho)), (float) (((double) this.w) / bho));
    }

    public Quat4fWrap cor() {
        double bho = bho();
        set(-((float) (((double) this.x) / bho)), -((float) (((double) this.y) / bho)), -((float) (((double) this.z) / bho)), (float) (((double) this.w) / bho));
        return this;
    }

    /* renamed from: zb */
    public Quat4fWrap mo15254zb() {
        return new Quat4fWrap(this).cos();
    }

    public Quat4fWrap cos() {
        normalize();
        return this;
    }

    public boolean isNormalized() {
        if (isZero(1.0f - ((float) bhp()))) {
            return true;
        }
        return false;
    }

    /* renamed from: d */
    public Quat4fWrap mo15233d(Quat4f quat4f) {
        return new Quat4fWrap().mo15213b(this, quat4f);
    }

    /* renamed from: b */
    public Quat4fWrap mo15213b(Quat4f quat4f, Quat4f quat4f2) {
        mul(quat4f, quat4f2);
        return this;
    }

    /* renamed from: e */
    public Quat4fWrap mo15235e(Quat4f quat4f) {
        mul(quat4f);
        return this;
    }

    public float bht() {
        float f = 0.0f;
        float f2 = (this.x * this.x) + (this.y * this.y) + (this.z * this.z) + (this.w * this.w);
        if (f2 > 0.0f) {
            f = (1.0f / ((float) Math.sqrt((double) f2))) * this.w;
        }
        return ((float) Math.acos((double) f)) * 2.0f;
    }

    public float bhu() {
        return (float) (((double) bht()) / PI_180);
    }

    public Vec3f cot() {
        Vec3f vec3f = new Vec3f();
        float f = (this.x * this.x) + (this.y * this.y) + (this.z * this.z) + (this.w * this.w);
        if (f > 0.0f) {
            float sqrt = 1.0f / ((float) Math.sqrt((double) f));
            float f2 = this.x * sqrt;
            float f3 = this.y * sqrt;
            float f4 = this.z * sqrt;
            float f5 = sqrt * this.w;
            float sqrt2 = (float) Math.sqrt((double) (1.0f - (f5 * f5)));
            if (Math.abs(sqrt2) < ANGLE_PRECISION) {
                vec3f.x = 1.0f;
                vec3f.z = 0.0f;
                vec3f.y = 0.0f;
            } else {
                vec3f.x = f2 / sqrt2;
                vec3f.y = f3 / sqrt2;
                vec3f.z = f4 / sqrt2;
            }
        }
        return vec3f;
    }

    public float[] bhw() {
        float[] fArr = new float[3];
        double d = (double) ((this.x * this.y) + (this.z * this.w));
        if (d > 0.4999500000012631d) {
            fArr[1] = (float) (2.0d * Math.atan2((double) this.x, (double) this.w));
            fArr[2] = 1.5707964f;
            fArr[0] = 0.0f;
        } else if (d < -0.4999500000012631d) {
            fArr[1] = (float) (-2.0d * Math.atan2((double) this.x, (double) this.w));
            fArr[2] = -1.5707964f;
            fArr[0] = 0.0f;
        } else {
            double d2 = (double) (this.x * this.x);
            double d3 = (double) (this.y * this.y);
            double d4 = (double) (this.z * this.z);
            fArr[0] = (float) Math.atan2((double) (((2.0f * this.x) * this.w) - ((2.0f * this.y) * this.z)), (1.0d - (d2 * 2.0d)) - (2.0d * d4));
            fArr[1] = (float) Math.atan2((double) (((2.0f * this.y) * this.w) - ((2.0f * this.x) * this.z)), (1.0d - (d3 * 2.0d)) - (d4 * 2.0d));
            fArr[2] = (float) Math.asin(d * 2.0d);
        }
        return fArr;
    }

    /* renamed from: e */
    public void mo15238e(Tuple3f tuple3f) {
        double d = (double) ((this.x * this.y) + (this.z * this.w));
        if (d > 0.4999500000012631d) {
            tuple3f.y = (float) (2.0d * Math.atan2((double) this.x, (double) this.w));
            tuple3f.z = 1.5707964f;
            tuple3f.x = 0.0f;
        } else if (d < -0.4999500000012631d) {
            tuple3f.y = (float) (-2.0d * Math.atan2((double) this.x, (double) this.w));
            tuple3f.z = -1.5707964f;
            tuple3f.x = 0.0f;
        } else {
            double d2 = (double) (this.z * this.z);
            tuple3f.x = (float) Math.atan2((double) (((2.0f * this.x) * this.w) - ((2.0f * this.y) * this.z)), (1.0d - (((double) (this.x * this.x)) * 2.0d)) - (2.0d * d2));
            tuple3f.y = (float) Math.atan2((double) (((2.0f * this.y) * this.w) - ((2.0f * this.x) * this.z)), (1.0d - (((double) (this.y * this.y)) * 2.0d)) - (d2 * 2.0d));
            tuple3f.z = (float) Math.asin(d * 2.0d);
        }
    }

    public float[] bhx() {
        float[] bhw = bhw();
        bhw[0] = (float) (((double) bhw[0]) / PI_180);
        bhw[1] = (float) (((double) bhw[1]) / PI_180);
        bhw[2] = (float) (((double) bhw[2]) / PI_180);
        return bhw;
    }

    /* renamed from: d */
    public Quat4fWrap mo15232d(Quat4fWrap aoy) {
        return new Quat4fWrap(this.x + aoy.x, this.y + aoy.y, this.z + aoy.z, this.w + aoy.w);
    }

    public Quat4fWrap cou() {
        return new Quat4fWrap(-this.x, -this.y, -this.z, -this.w);
    }

    public Quat4fWrap cov() {
        negate();
        return this;
    }

    /* renamed from: e */
    public boolean mo15239e(Quat4fWrap aoy) {
        return mo15254zb().equals(aoy.mo15254zb());
    }

    public Matrix4fWrap cow() {
        Matrix4fWrap ajk = new Matrix4fWrap();
        Quat4fWrap zb = mo15254zb();
        float f = zb.x * zb.x;
        float f2 = zb.x * zb.y;
        float f3 = zb.x * zb.z;
        float f4 = zb.w * zb.x;
        float f5 = zb.y * zb.y;
        float f6 = zb.y * zb.z;
        float f7 = zb.y * zb.w;
        float f8 = zb.z * zb.z;
        float f9 = zb.w * zb.z;
        ajk.m00 = 1.0f - ((f5 + f8) * 2.0f);
        ajk.m01 = (f2 - f9) * 2.0f;
        ajk.m02 = (f3 + f7) * 2.0f;
        ajk.m10 = (f9 + f2) * 2.0f;
        ajk.m11 = 1.0f - ((f + f8) * 2.0f);
        ajk.m12 = (f6 - f4) * 2.0f;
        ajk.m20 = (f3 - f7) * 2.0f;
        ajk.m21 = (f6 + f4) * 2.0f;
        ajk.m22 = 1.0f - ((f + f5) * 2.0f);
        ajk.m32 = 0.0f;
        ajk.m31 = 0.0f;
        ajk.m30 = 0.0f;
        ajk.m23 = 0.0f;
        ajk.m13 = 0.0f;
        ajk.m03 = 0.0f;
        ajk.m33 = 1.0f;
        return ajk;
    }

    /* renamed from: d */
    public void mo15234d(Matrix4fWrap ajk) {
        float f = this.w;
        float f2 = this.x;
        float f3 = this.y;
        float f4 = this.z;
        double d = (double) ((f * f) + (f2 * f2) + (f3 * f3) + (f4 * f4));
        if (!isZero(1.0f - ((float) d))) {
            double sqrt = 1.0d / Math.sqrt(d);
            f = (float) (((double) f) * sqrt);
            f2 = (float) (((double) f2) * sqrt);
            f3 = (float) (((double) f3) * sqrt);
            f4 = (float) (sqrt * ((double) f4));
        }
        float f5 = f2 * f2;
        float f6 = f2 * f3;
        float f7 = f2 * f4;
        float f8 = f2 * f;
        float f9 = f3 * f3;
        float f10 = f3 * f4;
        float f11 = f3 * f;
        float f12 = f4 * f4;
        float f13 = f4 * f;
        ajk.m00 = 1.0f - ((f9 + f12) * 2.0f);
        ajk.m01 = (f6 - f13) * 2.0f;
        ajk.m02 = (f7 + f11) * 2.0f;
        ajk.m10 = (f13 + f6) * 2.0f;
        ajk.m11 = 1.0f - ((f5 + f12) * 2.0f);
        ajk.m12 = (f10 - f8) * 2.0f;
        ajk.m20 = (f7 - f11) * 2.0f;
        ajk.m21 = (f10 + f8) * 2.0f;
        ajk.m22 = 1.0f - ((f5 + f9) * 2.0f);
        ajk.m32 = 0.0f;
        ajk.m31 = 0.0f;
        ajk.m30 = 0.0f;
        ajk.m23 = 0.0f;
        ajk.m13 = 0.0f;
        ajk.m03 = 0.0f;
        ajk.m33 = 1.0f;
    }

    /* renamed from: e */
    public void mo15237e(Matrix4fWrap ajk) {
        float f = this.w;
        float f2 = this.x;
        float f3 = this.y;
        float f4 = this.z;
        double d = (double) ((f * f) + (f2 * f2) + (f3 * f3) + (f4 * f4));
        if (!isZero(1.0f - ((float) d))) {
            double sqrt = 1.0d / Math.sqrt(d);
            f = (float) (((double) f) * sqrt);
            f2 = (float) (((double) f2) * sqrt);
            f3 = (float) (((double) f3) * sqrt);
            f4 = (float) (sqrt * ((double) f4));
        }
        float f5 = f2 * f2;
        float f6 = f2 * f3;
        float f7 = f2 * f4;
        float f8 = f2 * f;
        float f9 = f3 * f3;
        float f10 = f3 * f4;
        float f11 = f3 * f;
        float f12 = f4 * f4;
        float f13 = f4 * f;
        ajk.m00 = 1.0f - ((f9 + f12) * 2.0f);
        ajk.m01 = (f6 - f13) * 2.0f;
        ajk.m02 = (f7 + f11) * 2.0f;
        ajk.m10 = (f13 + f6) * 2.0f;
        ajk.m11 = 1.0f - ((f5 + f12) * 2.0f);
        ajk.m12 = (f10 - f8) * 2.0f;
        ajk.m20 = (f7 - f11) * 2.0f;
        ajk.m21 = (f10 + f8) * 2.0f;
        ajk.m22 = 1.0f - ((f5 + f9) * 2.0f);
    }

    /* renamed from: aT */
    public Vec3f mo15209aT(Vec3f vec3f) {
        Vec3f vec3f2 = new Vec3f();
        m24424a(this, vec3f, vec3f2);
        return vec3f2;
    }

    /* renamed from: aU */
    public Vec3f mo15210aU(Vec3f vec3f) {
        Vec3f vec3f2 = new Vec3f();
        m24426b(this, vec3f, vec3f2);
        return vec3f2;
    }

    /* renamed from: E */
    public final void mo15203E(Vec3f vec3f, Vec3f vec3f2) {
        m24426b(this, vec3f, vec3f2);
    }

    /* renamed from: F */
    public void mo15204F(Vec3f vec3f, Vec3f vec3f2) {
        m24424a(this, vec3f, vec3f2);
    }

    /* renamed from: aV */
    public Vec3f mo15211aV(Vec3f vec3f) {
        Vec3f vec3f2 = new Vec3f();
        mo15205G(vec3f, vec3f2);
        return vec3f2;
    }

    /* renamed from: G */
    public void mo15205G(Vec3f vec3f, Vec3f vec3f2) {
        float f = this.w;
        float f2 = this.x;
        float f3 = this.y;
        float f4 = this.z;
        float f5 = f * f2;
        float f6 = f * f3;
        float f7 = f * f4;
        float f8 = (-f2) * f2;
        float f9 = f2 * f3;
        float f10 = f2 * f4;
        float f11 = (-f3) * f3;
        float f12 = f3 * f4;
        float f13 = vec3f.x;
        float f14 = vec3f.y;
        float f15 = vec3f.z;
        vec3f2.x = (2.0f * (((f11 + f12) * f13) + ((f7 + f9) * f14) + ((f10 - f6) * f15))) + f13;
        vec3f2.y = ((((f9 - f7) * f13) + (((f4 * (-f4)) + f8) * f14) + ((f5 + f12) * f15)) * 2.0f) + f14;
        vec3f2.z = ((((f10 + f6) * f13) + ((f12 - f5) * f14) + ((f8 + f11) * f15)) * 2.0f) + f15;
    }

    /* renamed from: a */
    public void mo15206a(Quat4fWrap aoy, float f) {
        m24422a(this, aoy, f, this);
    }

    /* renamed from: a */
    public void mo15207a(Quat4fWrap aoy, Quat4fWrap aoy2, float f) {
        float sin;
        float f2 = aoy2.w;
        float f3 = aoy2.x;
        float f4 = aoy2.y;
        float f5 = aoy2.z;
        float f6 = aoy.w;
        float f7 = aoy.x;
        float f8 = aoy.y;
        float f9 = aoy.z;
        double d = (double) ((f6 * f2) + (f7 * f3) + (f8 * f4) + (f9 * f5));
        if (d < ScriptRuntime.NaN) {
            f2 = -f2;
            f3 = -f3;
            f4 = -f4;
            f5 = -f5;
            d = -d;
        }
        if (d > 0.9998999834060669d) {
            sin = 1.0f - f;
        } else {
            float sqrt = (float) Math.sqrt(1.0d - (d * d));
            float atan2 = (float) Math.atan2((double) sqrt, d);
            float f10 = 1.0f / sqrt;
            sin = (float) (Math.sin((double) ((1.0f - f) * atan2)) * ((double) f10));
            f = (float) (((double) f10) * Math.sin((double) (atan2 * f)));
        }
        this.w = (f6 * sin) + (f2 * f);
        this.x = (f7 * sin) + (f3 * f);
        this.y = (f8 * sin) + (f4 * f);
        this.z = (sin * f9) + (f5 * f);
    }

    public void cox() {
        double bhp = bhp();
        if (!isZero(1.0f - ((float) bhp))) {
            double sqrt = Math.sqrt(bhp);
            this.w = (float) (((double) this.w) / sqrt);
            this.x = (float) (((double) this.x) / sqrt);
            this.y = (float) (((double) this.y) / sqrt);
            this.z = (float) (((double) this.z) / sqrt);
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Quat4fWrap)) {
            return false;
        }
        Quat4fWrap aoy = (Quat4fWrap) obj;
        if (!isZero(this.w - aoy.w) || !isZero(this.x - aoy.x) || !isZero(this.y - aoy.y) || !isZero(this.z - aoy.z)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "[ (" + this.w + ") + (" + this.x + ")*i + (" + this.y + ")*j + (" + this.z + ")*k ]";
    }

    public String bhA() {
        String valueOf;
        String valueOf2;
        String valueOf3;
        String str = "0.0";
        if (isZero(this.w)) {
            valueOf = str;
        } else {
            valueOf = String.valueOf(this.w);
        }
        if (isZero(this.x)) {
            valueOf2 = str;
        } else {
            valueOf2 = String.valueOf(this.x);
        }
        if (isZero(this.y)) {
            valueOf3 = str;
        } else {
            valueOf3 = String.valueOf(this.y);
        }
        if (!isZero(this.z)) {
            str = String.valueOf(this.z);
        }
        return "[ (" + valueOf + ") + (" + valueOf2 + ")*i + (" + valueOf3 + ")*j + (" + str + ")*k ]";
    }

    public String bhB() {
        return "[ Axis: " + cot() + " Angle: " + bhu() + "° ]";
    }

    public String bhC() {
        float[] bhx = bhx();
        return "[ Roll: " + bhx[2] + " Pitch: " + bhx[0] + " Yaw: " + bhx[1] + " ]";
    }

    public int hashCode() {
        return (int) ((((((this.w * 31.0f) + this.x) * 31.0f) + this.y) * 31.0f) + this.z);
    }

    public boolean anA() {
        float f = this.w + this.x + this.y + this.z;
        return !Float.isNaN(f) && !Float.isInfinite(f);
    }

    /* renamed from: f */
    public void mo15242f(Quat4fWrap aoy) {
        this.x = aoy.x;
        this.y = aoy.y;
        this.z = aoy.z;
        this.w = aoy.w;
    }

    /* renamed from: p */
    public Quat4fWrap mo15250p(float f, float f2, float f3, float f4) {
        this.x = f;
        this.y = f2;
        this.z = f3;
        this.w = f4;
        return this;
    }

    /* renamed from: a */
    public boolean mo15208a(Quat4fWrap aoy, double d) {
        return ((double) Math.abs(aoy.x - this.x)) <= d && ((double) Math.abs(aoy.y - this.y)) <= d && ((double) Math.abs(aoy.z - this.z)) <= d && ((double) Math.abs(aoy.w - this.w)) <= d;
    }

    /* renamed from: g */
    public void mo15243g(Vec3d ajr, Vec3d ajr2) {
        Vec3f aB = ajr.mo9485aB(ajr2);
        aB.normalize();
        Vec3f vec3f = new Vec3f(0.0f, 1.0f, 0.0f);
        mo15204F(vec3f, vec3f);
        Matrix4fWrap ajk = new Matrix4fWrap();
        if (Math.abs(vec3f.dot(aB)) >= 0.9f) {
            vec3f.set(1.0f, 0.0f, 0.0f);
            mo15204F(vec3f, vec3f);
            Vec3f b = aB.mo23476b(vec3f);
            b.normalize();
            vec3f.cross(b, aB);
            vec3f.normalize();
            ajk.mo13988aD(vec3f);
            ajk.mo13989aE(b);
            ajk.mo13990aF(aB);
        } else {
            Vec3f b2 = vec3f.mo23476b(aB);
            b2.normalize();
            vec3f.cross(aB, b2);
            vec3f.normalize();
            ajk.mo13988aD(b2);
            ajk.mo13989aE(vec3f);
            ajk.mo13990aF(aB);
        }
        set(ajk);
    }

    /* renamed from: f */
    public Quat4fWrap mo15241f(Matrix3fWrap ajd) {
        mo15236e(ajd);
        return this;
    }

    /* renamed from: kb */
    public Quat4fWrap mo15248kb(float f) {
        Quat4fWrap aoy = new Quat4fWrap(this);
        aoy.scale(f);
        return aoy;
    }

    /* renamed from: kc */
    public Quat4fWrap mo15249kc(float f) {
        scale(f);
        return this;
    }

    public Quat4fWrap coy() {
        return new Quat4fWrap(this);
    }

    public void readExternal(IReadExternal vm) throws IOException {
        this.x = vm.readFloat("x");
        this.y = vm.readFloat("y");
        this.z = vm.readFloat("z");
        this.w = vm.readFloat("w");
    }

    public void writeExternal(IWriteExternal att) throws IOException {
        att.writeFloat("x", this.x);
        att.writeFloat("y", this.y);
        att.writeFloat("z", this.z);
        att.writeFloat("w", this.w);
    }
}
