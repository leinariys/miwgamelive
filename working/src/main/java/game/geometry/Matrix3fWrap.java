package game.geometry;

import com.hoplon.geometry.Vec3f;
import game.io.IExternalIO;
import game.io.IReadExternal;
import game.io.IWriteExternal;

import javax.vecmath.*;
import java.io.IOException;

/* renamed from: a.ajD  reason: case insensitive filesystem */
/* compiled from: a */
public class Matrix3fWrap extends Matrix3f implements IExternalIO {
    public Matrix3fWrap() {
        setIdentity();
    }

    public Matrix3fWrap(float[] fArr) {
        super(fArr[0], fArr[3], fArr[6], fArr[1], fArr[4], fArr[7], fArr[2], fArr[5], fArr[8]);
    }

    public Matrix3fWrap(Matrix3d matrix3d) {
        super(matrix3d);
    }

    public Matrix3fWrap(Matrix3fWrap ajd) {
        super(ajd);
    }

    public Matrix3fWrap(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9) {
        super(f, f4, f7, f2, f5, f8, f3, f6, f9);
    }

    /* renamed from: c */
    public void mo13949c(float[] fArr) {
        this.m00 = fArr[0];
        this.m01 = fArr[1];
        this.m02 = fArr[2];
        this.m10 = fArr[3];
        this.m11 = fArr[4];
        this.m12 = fArr[5];
        this.m20 = fArr[6];
        this.m21 = fArr[7];
        this.m22 = fArr[8];
    }

    public float[] ceB() {
        return new float[]{this.m00, this.m01, this.m02, this.m10, this.m11, this.m12, this.m20, this.m21, this.m22};
    }

    public void transform(Tuple3d tuple3d) {
        double d = tuple3d.x;
        double d2 = tuple3d.y;
        double d3 = tuple3d.z;
        tuple3d.x = (((double) this.m00) * d) + (((double) this.m01) * d2) + (((double) this.m02) * d3);
        tuple3d.y = (((double) this.m10) * d) + (((double) this.m11) * d2) + (((double) this.m12) * d3);
        tuple3d.z = (d * ((double) this.m20)) + (d2 * ((double) this.m21)) + (((double) this.m22) * d3);
    }

    public final void rotateX(float f) {
        float sin = (float) Math.sin((double) f);
        float cos = (float) Math.cos((double) f);
        float f2 = this.m10;
        float f3 = this.m11;
        float f4 = this.m12;
        float f5 = this.m20;
        float f6 = this.m21;
        float f7 = this.m22;
        this.m10 = (cos * f2) - (sin * f5);
        this.m11 = (cos * f3) - (sin * f6);
        this.m12 = (cos * f4) - (sin * f7);
        this.m20 = (f2 * sin) + (f5 * cos);
        this.m21 = (sin * f3) + (cos * f6);
        this.m22 = (sin * f4) + (cos * f7);
    }

    public final void rotateY(float f) {
        float sin = (float) Math.sin((double) f);
        float cos = (float) Math.cos((double) f);
        float f2 = this.m00;
        float f3 = this.m01;
        float f4 = this.m02;
        float f5 = this.m20;
        float f6 = this.m21;
        float f7 = this.m22;
        this.m00 = (cos * f2) + (sin * f5);
        this.m01 = (cos * f3) + (sin * f6);
        this.m02 = (cos * f4) + (sin * f7);
        this.m20 = (f2 * (-sin)) + (f5 * cos);
        this.m21 = ((-sin) * f3) + (cos * f6);
        this.m22 = ((-sin) * f4) + (cos * f7);
    }

    public final void rotateZ(float f) {
        float sin = (float) Math.sin((double) f);
        float cos = (float) Math.cos((double) f);
        float f2 = this.m00;
        float f3 = this.m01;
        float f4 = this.m02;
        float f5 = this.m10;
        float f6 = this.m11;
        float f7 = this.m12;
        this.m00 = (cos * f2) - (sin * f5);
        this.m01 = (cos * f3) - (sin * f6);
        this.m02 = (cos * f4) - (sin * f7);
        this.m10 = (f2 * sin) + (f5 * cos);
        this.m11 = (sin * f3) + (cos * f6);
        this.m12 = (sin * f4) + (cos * f7);
    }

    /* renamed from: a */
    public void mo13945a(Vector3d vector3d, Vector3d vector3d2) {
        float f = 1.0f / (((this.m00 * this.m00) + (this.m10 * this.m10)) + (this.m20 * this.m20));
        float f2 = 1.0f / (((this.m01 * this.m01) + (this.m11 * this.m11)) + (this.m21 * this.m21));
        float f3 = 1.0f / (((this.m02 * this.m02) + (this.m12 * this.m12)) + (this.m22 * this.m22));
        float f4 = this.m00 * f;
        float f5 = this.m10 * f2;
        float f6 = this.m20 * f3;
        float f7 = this.m01 * f;
        float f8 = this.m11 * f2;
        float f9 = this.m21 * f3;
        float f10 = f * this.m02;
        float f11 = f2 * this.m12;
        double d = vector3d.x;
        double d2 = vector3d.y;
        double d3 = vector3d.z;
        vector3d2.x = (((double) f6) * d3) + (((double) f4) * d) + (((double) f5) * d2);
        vector3d2.y = (((double) f7) * d) + (((double) f8) * d2) + (((double) f9) * d3);
        vector3d2.z = (((double) f11) * d2) + (((double) f10) * d) + (((double) (f3 * this.m22)) * d3);
    }

    /* renamed from: a */
    public void mo13947a(Vector3f vector3f, Vector3f vector3f2) {
        float f = 1.0f / (((this.m00 * this.m00) + (this.m10 * this.m10)) + (this.m20 * this.m20));
        float f2 = 1.0f / (((this.m01 * this.m01) + (this.m11 * this.m11)) + (this.m21 * this.m21));
        float f3 = 1.0f / (((this.m02 * this.m02) + (this.m12 * this.m12)) + (this.m22 * this.m22));
        float f4 = this.m00 * f;
        float f5 = this.m10 * f2;
        float f6 = this.m20 * f3;
        float f7 = this.m01 * f;
        float f8 = this.m11 * f2;
        float f9 = this.m21 * f3;
        float f10 = f * this.m02;
        float f11 = f2 * this.m12;
        float f12 = f3 * this.m22;
        float f13 = vector3f.x;
        float f14 = vector3f.y;
        float f15 = vector3f.z;
        vector3f2.x = (f4 * f13) + (f5 * f14) + (f15 * f6);
        vector3f2.y = (f13 * f7) + (f14 * f8) + (f15 * f9);
        vector3f2.z = (f10 * f13) + (f11 * f14) + (f15 * f12);
    }

    /* renamed from: a */
    public void mo13946a(Vector3f vector3f) {
        mo13947a(vector3f, vector3f);
    }

    /* renamed from: a */
    public void mo13944a(Vector3d vector3d) {
        mo13945a(vector3d, vector3d);
    }

    public void set(Matrix4fWrap ajk) {
        this.m00 = ajk.m00;
        this.m01 = ajk.m01;
        this.m02 = ajk.m02;
        this.m10 = ajk.m10;
        this.m11 = ajk.m11;
        this.m12 = ajk.m12;
        this.m20 = ajk.m20;
        this.m21 = ajk.m21;
        this.m22 = ajk.m22;
    }

    public boolean anA() {
        float f = this.m00 + this.m01 + this.m02 + this.m10 + this.m11 + this.m12 + this.m20 + this.m21 + this.m22;
        return !Float.isNaN(f) && !Float.isInfinite(f);
    }

    public Vec3f ceC() {
        return new Vec3f(this.m00, this.m10, this.m20);
    }

    public Vec3f ceD() {
        return new Vec3f(this.m01, this.m11, this.m21);
    }

    public Vec3f ceE() {
        return new Vec3f(this.m02, this.m12, this.m22);
    }

    public void readExternal(IReadExternal vm) throws IOException {
        this.m00 = vm.readFloat("m00");
        this.m01 = vm.readFloat("m01");
        this.m02 = vm.readFloat("m02");
        this.m10 = vm.readFloat("m10");
        this.m11 = vm.readFloat("m11");
        this.m12 = vm.readFloat("m12");
        this.m20 = vm.readFloat("m20");
        this.m21 = vm.readFloat("m21");
        this.m22 = vm.readFloat("m22");
    }

    public void writeExternal(IWriteExternal att) throws IOException {
        att.writeFloat("m00", this.m00);
        att.writeFloat("m01", this.m01);
        att.writeFloat("m02", this.m02);
        att.writeFloat("m10", this.m10);
        att.writeFloat("m11", this.m11);
        att.writeFloat("m12", this.m12);
        att.writeFloat("m20", this.m20);
        att.writeFloat("m21", this.m21);
        att.writeFloat("m22", this.m22);
    }
}
