package game.engine;

import taikodom.render.textures.DDSLoader;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.arC  reason: case insensitive filesystem */
/* compiled from: a */
public class FloatCastom extends FloatBit {
    public static final int gsF = 32;
    public static final int gsG = 255;
    public static final int gsH = 127;
    public static final int gsI = 23;
    private static final int gbL = -126;
    private final int evX;
    private final int evY;
    private final int evZ;
    private final int ewa;
    private final int ewb;
    private final int ewf;
    private final boolean ewi;
    private final int ewm;
    private final int gsJ;
    private final int gsK;
    private final int gsL;
    private final float gsM;
    private final int gsN;
    private final int gsO;
    private final int gsP;
    private final float maxValue;
    private final float minValue;
    private final boolean signed;
    public String name;

    public FloatCastom(boolean z, int i, int i2, boolean z2, int i3) {
        this((String) null, z, i, i2, z2, i3);
    }

    public FloatCastom(String str, boolean z, int i, int i2, boolean z2, int i3) {
        int i4;
        int i5;
        int i6;
        float intBitsToFloat;
        int i7 = 0;
        this.name = str == null ? getClass().getSimpleName() : str;
        this.signed = z;
        this.evX = i;
        this.evY = i2;
        this.ewi = z2;
        this.gsJ = (1 << i2) - 1;
        this.gsK = (1 << i) - 1;
        this.ewb = i3;
        int i8 = this.gsK - i3;
        if (z2) {
            i4 = 2;
        } else {
            i4 = 1;
        }
        this.ewa = -(i8 - i4);
        this.ewf = ((-this.ewa) - 127) + 1;
        if (z) {
            i5 = 1;
        } else {
            i5 = 0;
        }
        this.evZ = i5 + i + i2;
        if (i2 < 23) {
            i6 = 1 << ((23 - i2) - 1);
        } else {
            i6 = 0;
        }
        this.gsP = i6;
        this.gsN = 23 - i2;
        this.gsO = 32 - this.evZ;
        this.gsL = 1 << (this.evZ - 1);
        this.ewm = (this.evZ >> 3) + ((this.evZ & 7) != 0 ? 1 : i7);
        this.minValue = Float.intBitsToFloat((this.ewa + 127) << 23);
        this.maxValue = Float.intBitsToFloat(((i3 + 127) << 23) | (this.gsJ << (23 - i2)));
        if (23 <= i2) {
            intBitsToFloat = 0.0f;
        } else {
            intBitsToFloat = Float.intBitsToFloat(1065353216 | ((1 << (23 - i2)) - 1)) - 1.0f;
        }
        this.gsM = intBitsToFloat;
    }

    /* renamed from: tR */
    public static int m25356tR(int i) {
        int i2 = 0;
        int i3 = i;
        for (int i4 = 16; i4 > 0; i4 >>= 1) {
            if ((i3 >>> i4) != 0) {
                i3 >>>= i4;
                i2 |= i4;
            }
        }
        return i2 + i3;
    }

    /* renamed from: rb */
    public static int m25355rb(int i) {
        int i2;
        int i3 = 0;
        if ((i >>> 16) != 0) {
            i2 = i >>> 16;
            i3 = 16;
        } else {
            i2 = i;
        }
        if ((i2 >>> 8) != 0) {
            i2 >>>= 8;
            i3 += 8;
        }
        if ((i2 >>> 4) != 0) {
            i2 >>>= 4;
            i3 += 4;
        }
        if ((i2 >>> 2) != 0) {
            i2 >>>= 2;
            i3 += 2;
        }
        if ((i2 >>> 1) != 0) {
            i2 >>>= 1;
            i3++;
        }
        return i3 + i2;
    }

    /* renamed from: Kc */
    public int mo8273Kc() {
        return this.evZ;
    }

    /* renamed from: Ka */
    public int mo8271Ka() {
        return this.ewb;
    }

    /* renamed from: Kb */
    public int mo8272Kb() {
        return this.ewa;
    }

    /* renamed from: Ke */
    public int mo8275Ke() {
        return this.evX;
    }

    /* renamed from: Kd */
    public int mo8274Kd() {
        return this.evY;
    }

    /* renamed from: Kf */
    public float mo8276Kf() {
        return this.maxValue;
    }

    /* renamed from: Kg */
    public float mo8277Kg() {
        return this.minValue;
    }

    /* renamed from: aT */
    public int mo8282aT(float f) {
        int i;
        int i2;
        int floatToIntBits = Float.floatToIntBits(f);
        int i3 = floatToIntBits & 8388607;
        int i4 = (floatToIntBits >> 23) & 255;
        if (i4 != 255) {
            if ((this.gsP & i3) != 0) {
                floatToIntBits += this.gsP;
                i4 = (floatToIntBits >> 23) & 255;
                i3 = floatToIntBits & 8388607;
            }
            if (i4 != 0) {
                i4 += this.ewf;
                if (i4 <= 0) {
                    if (i4 > (-this.evY)) {
                        i2 = (i3 | DDSLoader.DDSD_DEPTH) >>> (1 - i4);
                    } else {
                        i2 = 0;
                    }
                    i = 0;
                    i3 = i2;
                } else {
                    if (i4 >= this.gsK && (i4 != this.gsK || this.ewi)) {
                        i = this.ewb + (-this.ewa) + 1;
                        i3 = 8388607;
                    }
                    i = i4;
                }
            } else {
                if (this.ewa > -126) {
                    if (this.ewa + 126 < this.evY) {
                        i = i4;
                        i3 >>= this.ewa + 126;
                    } else {
                        i = i4;
                        i3 = 0;
                    }
                }
                i = i4;
            }
        } else {
            if (!this.ewi && (i4 = i4 + this.ewf) <= 0 && i4 > -126) {
                i = 0;
                i3 = (i3 | DDSLoader.DDSD_DEPTH) >> (1 - i4);
            }
            i = i4;
        }
        if (this.signed) {
            return ((floatToIntBits >> this.gsO) & this.gsL) | ((this.gsK & i) << this.evY) | (i3 >> this.gsN);
        }
        return ((this.gsK & i) << this.evY) | (i3 >> this.gsN);
    }

    /* renamed from: cF */
    public float mo8283cF(int i) {
        int i2 = this.gsK & (i >> this.evY);
        int i3 = this.gsJ & i;
        if (i2 == this.gsK) {
            i2 = this.ewi ? 255 : i2 - this.ewf;
        } else if (i2 != 0) {
            i2 -= this.ewf;
        } else if (this.ewa > -126 && i3 != 0) {
            int rb = (this.evY - m25355rb(i3)) + 1;
            int i4 = this.ewa + 126;
            if (i4 > rb) {
                i2 = (this.ewa - rb) + 127;
                i3 = (i3 << rb) & this.gsJ;
            } else {
                i3 = (i3 << i4) & this.gsJ;
            }
        }
        if (this.signed) {
            return Float.intBitsToFloat((i3 << this.gsN) | (i2 << 23) | ((i << this.gsO) & Integer.MIN_VALUE));
        }
        return Float.intBitsToFloat((i3 << this.gsN) | (i2 << 23));
    }

    /* renamed from: a */
    public void mo8281a(ObjectOutput objectOutput, float f) throws IOException {
        switch (this.ewm) {
            case 1:
                objectOutput.writeByte(mo8282aT(f));
                break;
            case 2:
                objectOutput.writeShort(mo8282aT(f));
                break;
            case 3:
                objectOutput.writeByte(mo8282aT(f) >> 16);
                objectOutput.writeShort(mo8282aT(f));
                break;
            case 4:
                objectOutput.writeInt(mo8282aT(f));
                break;
        }
        throw new IllegalArgumentException();
    }

    /* renamed from: d */
    public float mo8284d(ObjectInput objectInput) throws IOException {
        switch (this.ewm) {
            case 1:
                return mo8283cF(objectInput.readUnsignedByte() & 255);
            case 2:
                return mo8283cF(objectInput.readUnsignedShort());
            case 3:
                return mo8283cF((objectInput.readUnsignedByte() << 16) | objectInput.readUnsignedShort());
            case 4:
                return mo8283cF(objectInput.readInt());
            default:
                throw new IllegalArgumentException();
        }
    }

    /* renamed from: Kh */
    public float mo8278Kh() {
        return this.gsM;
    }

    /* renamed from: Ki */
    public boolean mo8279Ki() {
        return this.signed;
    }

    public String getName() {
        return this.name;
    }

    /* renamed from: Kj */
    public boolean mo8280Kj() {
        return this.ewi;
    }
}
