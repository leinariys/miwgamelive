package game.io;

import java.io.IOException;

/* renamed from: a.afA  reason: case insensitive filesystem */
/* compiled from: a */
public interface IExternalIO {
    void readExternal(IReadExternal vm) throws IOException;

    void writeExternal(IWriteExternal att) throws IOException;
}
