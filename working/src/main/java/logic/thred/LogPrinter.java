package logic.thred;

import com.hoplon.commons.TaikodomLogger$TaikodomLoggerConsole;
import org.apache.commons.logging.Log;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: a.UR */
/* compiled from: a */
public class LogPrinter implements Log {
    public static final String SEPARATOR = "/";
    public static final byte eqK = 2;
    public static final byte eqL = 4;
    public static final byte eqM = 8;
    public static final byte eqN = 16;
    public static final byte eqO = 32;
    public static final byte eqP = 64;
    public static final byte eqQ = 126;
    public static final byte eqR = 120;
    private static final SimpleDateFormat dBH = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private static final SimpleDateFormat eqV = new SimpleDateFormat("yyyyMMdd-HHmmss-SSS");
    public static byte eqS = eqR;
    public static byte eqT = 0;
    public static boolean eqU = false;
    /* access modifiers changed from: private */
    public static Map<Class<?>, LogPrinter> eqW;
    private static Properties eqX;
    private static boolean eqY = true;
    private static String eqZ = null;
    private PrintWriter eqC = new PrintWriter(System.out, true);
    private String eqD = "[TRACE]";
    private String eqE = "[DEBUG]";
    private String eqF = "[INFO ]";
    private String eqG = "[WARN ]";
    private String eqH = "[ERROR]";
    private String eqI = "[FATAL]";
    private byte eqJ;
    private String era;

    private LogPrinter(Class<?> cls) {
        String substring;
        int lastIndexOf = cls.getName().lastIndexOf(46);
        if (lastIndexOf == -1) {
            substring = cls.getName();
        } else {
            substring = cls.getName().substring(lastIndexOf + 1);
        }
        this.era = substring;
        byte gM = m10279gM(this.era);
        if (gM == 0) {
            mo5853b(eqT);
        } else {
            mo5853b(gM);
        }
    }

    /**
     * Класс подписывается на логирование
     *
     * @param paramClass
     * @return
     */
    /* renamed from: K */
    public static LogPrinter m10275K(Class<?> paramClass) {
        return setClass(paramClass);
    }

    /* renamed from: L */
    public static LogPrinter setClass(Class<?> cls) {
        if (eqY) {
            eqY = false;
            byt();
        }
        if (eqW == null) {
            aqT();
            eqW = new ConcurrentHashMap();
        }
        LogPrinter ur = eqW.get(cls);
        if (ur != null) {
            return ur;
        }
        LogPrinter ur2 = new LogPrinter(cls);
        eqW.put(cls, ur2);
        return ur2;
    }

    private static void byt() {
        eqX = new Properties();
        File file = new File("taikodomlogger.properties");
        if (!file.exists()) {
            file = new File("res/client/config", "taikodomlogger.properties");
        }
        if (!file.exists()) {
            System.out.println("TaikodomLogger: no taikodomlogger.properties found, using default: info,warn,error,fatal");
        } else {
            try {
                eqX.load(new FileInputStream(file));
            } catch (FileNotFoundException e) {
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        byte gM = m10279gM("levels");
        if (gM == 0) {
            gM = eqR;
        }
        eqT = gM;
        String property = System.getProperty("TaikodomLogger.DISABLE");
        eqU = property != null && property.equalsIgnoreCase("true");
    }

    /* renamed from: gM */
    private static byte m10279gM(String str) {
        String property;
        byte b = 0;
        if (eqX == null || (property = eqX.getProperty(str, (String) null)) == null) {
            return 0;
        }
        String[] split = property.split(",");
        int i = 0;
        while (true) {
            byte b2 = b;
            if (i >= split.length) {
                return b2;
            }
            String str2 = split[i];
            if (str2.equals("trace")) {
                b = (byte) (b2 | 2);
            } else if (str2.equals("debug")) {
                b = (byte) (b2 | 4);
            } else if (str2.equals("info")) {
                b = (byte) (b2 | 8);
            } else if (str2.equals("warn")) {
                b = (byte) (b2 | eqN);
            } else if (str2.equals("error")) {
                b = (byte) (b2 | eqO);
            } else {
                b = str2.equals("fatal") ? (byte) (b2 | eqP) : b2;
            }
            i++;
        }
    }

    private static synchronized void aqT() {
        synchronized (LogPrinter.class) {
            MBeanServer platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
            if (platformMBeanServer != null) {
                TaikodomLogger$TaikodomLoggerConsole taikodomLogger$TaikodomLoggerConsole = new TaikodomLogger$TaikodomLoggerConsole();
                int i = 0;
                while (true) {
                    if (i >= 10) {
                        break;
                    }
                    try {
                        platformMBeanServer.registerMBean(taikodomLogger$TaikodomLoggerConsole, new ObjectName("Bitverse:name=Logger" + (i == 0 ? "" : Integer.valueOf(i))));
                    } catch (InstanceAlreadyExistsException e) {
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    i++;
                }
            }
        }
    }

    /* renamed from: M */
    public static PrintWriter createLogFile(String str, String str2) throws FileNotFoundException, UnsupportedEncodingException {
        String str3 = String.valueOf(System.getProperty("log-dir", "log")) + SEPARATOR + str + "-" + eqV.format(new Date(System.currentTimeMillis())) + "." + str2;
        new File(str3).getParentFile().mkdirs();
        System.out.println("Opening log file: " + str3);

        return new PrintWriter(new OutputStreamWriter(new FileOutputStream(str3, true), "UTF-8"), true);

    }

    /* renamed from: gN */
    public static void m10280gN(String str) {
        eqZ = str;
    }

    /* renamed from: b */
    public void mo5853b(byte b) {
        this.eqJ = b;
    }

    /* renamed from: c */
    public void mo5854c(byte b) {
        this.eqJ = (byte) (this.eqJ | b);
    }

    /* renamed from: d */
    public void mo5855d(byte b) {
        this.eqJ = (byte) (this.eqJ & (b ^ -1));
    }

    /* renamed from: r */
    private void m10281r(String str, Object obj) {
        if (!eqU) {
            if (obj instanceof Throwable) {
                m10278a(str, obj.toString(), (Throwable) obj);
                return;
            }
            this.eqC.println(format(str, obj));
            if (eqZ != null && obj != null && obj.toString().indexOf(eqZ) != -1) {
                Thread.dumpStack();
            }
        }
    }

    /* renamed from: a */
    private void m10278a(String str, Object obj, Throwable th) {
        if (!eqU) {
            this.eqC.println(format(str, obj));
            th.printStackTrace(this.eqC);
        }
    }

    private String format(String str, Object obj) {
        return String.valueOf(Thread.currentThread().getId()) + " " + dBH.format(new Date(System.currentTimeMillis())) + " " + str + " " + this.era + ": " + obj;
    }

    public void debug(Object obj) {
        if (isDebugEnabled()) {
            m10281r(this.eqE, obj);
        }
    }

    public void debug(Object obj, Throwable th) {
        if (isDebugEnabled()) {
            m10278a(this.eqE, obj, th);
        }
    }

    public void error(Object obj) {
        if (isErrorEnabled()) {
            m10281r(this.eqH, obj);
        }
    }

    public void error(Object obj, Throwable th) {
        if (isErrorEnabled()) {
            m10278a(this.eqH, obj, th);
        }
    }

    public void fatal(Object obj) {
        if (isFatalEnabled()) {
            m10281r(this.eqI, obj);
        }
    }

    public void fatal(Object obj, Throwable th) {
        if (isFatalEnabled()) {
            m10278a(this.eqI, obj, th);
        }
    }

    public void info(Object obj) {
        if (isInfoEnabled()) {
            m10281r(this.eqF, obj);
        }
    }

    public void info(Object obj, Throwable th) {
        if (isInfoEnabled()) {
            m10278a(this.eqF, obj, th);
        }
    }

    public void trace(Object obj) {
        if (isTraceEnabled()) {
            m10281r(this.eqD, obj);
        }
    }

    public void trace(Object obj, Throwable th) {
        if (isTraceEnabled()) {
            m10278a(this.eqD, obj, th);
        }
    }

    public void warn(Object obj) {
        if (isWarnEnabled()) {
            m10281r(this.eqG, obj);
        }
    }

    public void warn(Object obj, Throwable th) {
        if (isWarnEnabled()) {
            m10278a(this.eqG, obj, th);
        }
    }

    public boolean isDebugEnabled() {
        return !eqU && (this.eqJ & 4) == 4;
    }

    public boolean isErrorEnabled() {
        return !eqU && (this.eqJ & eqO) == 32;
    }

    public boolean isFatalEnabled() {
        return !eqU && (this.eqJ & eqP) == 64;
    }

    public boolean isInfoEnabled() {
        return !eqU && (this.eqJ & 8) == 8;
    }

    public boolean isTraceEnabled() {
        return !eqU && (this.eqJ & 2) == 2;
    }

    public boolean isWarnEnabled() {
        return !eqU && (this.eqJ & eqN) == 16;
    }
}
