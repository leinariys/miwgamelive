package logic.obfuscation.read;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/* renamed from: a.lk */
/* compiled from: a */
public class ObfuscationReader {

    /* renamed from: in */
    private BufferedReader reader;

    public ObfuscationReader(InputStream inputStream) {
        this.reader = new BufferedReader(new InputStreamReader(inputStream));
    }

    public Map<String, String> getMap() throws IOException {
        HashMap hashMap = new HashMap();
        while (true) {
            String readLine = this.reader.readLine();
            if (readLine == null) {
                return hashMap;
            }
            if (!readLine.startsWith(" ")) {
                StringTokenizer stringTokenizer = new StringTokenizer(readLine, " ");
                String nextToken = stringTokenizer.nextToken();
                stringTokenizer.nextToken();
                String nextToken2 = stringTokenizer.nextToken();
                hashMap.put(nextToken, nextToken2.substring(0, nextToken2.length() - 1));
            }
        }
    }
}
