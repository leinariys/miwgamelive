package logic;

import java.io.File;
import java.util.Collection;

/* renamed from: a.aVW */
/* compiled from: a */
public interface IWrapFileXmlOrJar {
    /* renamed from: a */
    <T extends IAddonSettings> void mo11803a(IAddonExecutor<T> ams);

    void initAddonWaitPlayer();

    /* renamed from: b */
    void add(String str, String[] strArr);

    void reLoadingAddon();

    <T extends IAddonSettings> Collection<IAddonExecutor<T>> dCN();

    void dispose();

    File getFile();
}
