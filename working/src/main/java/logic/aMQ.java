package logic;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

/* renamed from: a.aMQ */
/* compiled from: a */
public class aMQ<K, V> {
    private ConcurrentHashMap<K, Collection<V>> hCq = new ConcurrentHashMap<>();

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x000a, code lost:
        r1 = djH();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void put(K paramK, V paramV) {
        Object localObject = (Collection) this.hCq.get(paramK);
        if (localObject == null) {
            localObject = djH();
            Collection localCollection = (Collection) this.hCq.putIfAbsent(paramK, (Collection<V>) localObject);
            if (localCollection != null) {
                localObject = localCollection;
            }
        }
        ((Collection) localObject).add(paramV);
    }

    /* access modifiers changed from: protected */
    public Collection<V> djH() {
        return new CopyOnWriteArraySet();
    }

    /* renamed from: aQ */
    public void mo10039aQ(V v) {
        for (Collection<V> remove : this.hCq.values()) {
            remove.remove(v);
        }
    }

    /* renamed from: k */
    public void mo10044k(K k, V v) {
        Collection collection = this.hCq.get(k);
        if (collection != null) {
            collection.remove(v);
        }
    }

    /* renamed from: aR */
    public Collection<V> mo10040aR(K k) {
        return this.hCq.get(k);
    }

    public void clear() {
        this.hCq.clear();
    }

    public Set<Map.Entry<K, Collection<V>>> entrySet() {
        return this.hCq.entrySet();
    }
}
