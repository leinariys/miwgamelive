package logic.render.granny;

/* renamed from: a.Xl */
/* compiled from: a */
public final class C1622Xl {
    public static final C1622Xl eIw = new C1622Xl("GrannyDontAllowUncopiedTail");
    public static final C1622Xl eIx = new C1622Xl("GrannyAllowUncopiedTail");
    private static C1622Xl[] eIy = {eIw, eIx};

    /* renamed from: pF */
    private static int f2133pF = 0;

    /* renamed from: pG */
    private final int f2134pG;

    /* renamed from: pH */
    private final String f2135pH;

    private C1622Xl(String str) {
        this.f2135pH = str;
        int i = f2133pF;
        f2133pF = i + 1;
        this.f2134pG = i;
    }

    private C1622Xl(String str, int i) {
        this.f2135pH = str;
        this.f2134pG = i;
        f2133pF = i + 1;
    }

    private C1622Xl(String str, C1622Xl xl) {
        this.f2135pH = str;
        this.f2134pG = xl.f2134pG;
        f2133pF = this.f2134pG + 1;
    }

    /* renamed from: pb */
    public static C1622Xl m11583pb(int i) {
        if (i < eIy.length && i >= 0 && eIy[i].f2134pG == i) {
            return eIy[i];
        }
        for (int i2 = 0; i2 < eIy.length; i2++) {
            if (eIy[i2].f2134pG == i) {
                return eIy[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C1622Xl.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f2134pG;
    }

    public String toString() {
        return this.f2135pH;
    }
}
