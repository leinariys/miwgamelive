package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aMK */
/* compiled from: a */
public class aMK {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aMK(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aMK() {
        this(grannyJNI.new_granny_morph_target(), true);
    }

    /* renamed from: b */
    public static long m16345b(aMK amk) {
        if (amk == null) {
            return 0;
        }
        return amk.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_morph_target(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: ng */
    public void mo10024ng(String str) {
        grannyJNI.granny_morph_target_ScalarName_set(this.swigCPtr, str);
    }

    public String djy() {
        return grannyJNI.granny_morph_target_ScalarName_get(this.swigCPtr);
    }

    /* renamed from: c */
    public void mo10018c(C0251DE de) {
        grannyJNI.granny_morph_target_VertexData_set(this.swigCPtr, C0251DE.m1941a(de));
    }

    public C0251DE djz() {
        long granny_morph_target_VertexData_get = grannyJNI.granny_morph_target_VertexData_get(this.swigCPtr);
        if (granny_morph_target_VertexData_get == 0) {
            return null;
        }
        return new C0251DE(granny_morph_target_VertexData_get, false);
    }

    /* renamed from: yM */
    public void mo10025yM(int i) {
        grannyJNI.granny_morph_target_DataIsDeltas_set(this.swigCPtr, i);
    }

    public int djA() {
        return grannyJNI.granny_morph_target_DataIsDeltas_get(this.swigCPtr);
    }

    /* renamed from: yN */
    public aMK mo10026yN(int i) {
        long granny_morph_target_get = grannyJNI.granny_morph_target_get(this.swigCPtr, i);
        if (granny_morph_target_get == 0) {
            return null;
        }
        return new aMK(granny_morph_target_get, false);
    }
}
