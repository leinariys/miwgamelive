package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.Nm */
/* compiled from: a */
public class C0938Nm {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0938Nm(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0938Nm() {
        this(grannyJNI.new_granny_system_clock(), true);
    }

    /* renamed from: b */
    public static long m7718b(C0938Nm nm) {
        if (nm == null) {
            return 0;
        }
        return nm.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_system_clock(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: d */
    public void mo4264d(C6135ahD ahd) {
        grannyJNI.granny_system_clock_Data_set(this.swigCPtr, C6135ahD.m22189i(ahd));
    }

    public C6135ahD biu() {
        long granny_system_clock_Data_get = grannyJNI.granny_system_clock_Data_get(this.swigCPtr);
        if (granny_system_clock_Data_get == 0) {
            return null;
        }
        return new C6135ahD(granny_system_clock_Data_get, false);
    }

    /* renamed from: lS */
    public C0938Nm mo4267lS(int i) {
        long granny_system_clock_get = grannyJNI.granny_system_clock_get(this.swigCPtr, i);
        if (granny_system_clock_get == 0) {
            return null;
        }
        return new C0938Nm(granny_system_clock_get, false);
    }
}
