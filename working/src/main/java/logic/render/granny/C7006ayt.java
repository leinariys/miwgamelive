package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.ayt  reason: case insensitive filesystem */
/* compiled from: a */
public class C7006ayt {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C7006ayt(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C7006ayt() {
        this(grannyJNI.new_granny_material_binding(), true);
    }

    /* renamed from: b */
    public static long m27495b(C7006ayt ayt) {
        if (ayt == null) {
            return 0;
        }
        return ayt.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_material_binding(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo17086b(C0201CS cs) {
        grannyJNI.granny_material_binding_Material_set(this.swigCPtr, C0201CS.m1752a(cs));
    }

    public C0201CS cDE() {
        long granny_material_binding_Material_get = grannyJNI.granny_material_binding_Material_get(this.swigCPtr);
        if (granny_material_binding_Material_get == 0) {
            return null;
        }
        return new C0201CS(granny_material_binding_Material_get, false);
    }

    /* renamed from: wa */
    public C7006ayt mo17090wa(int i) {
        long granny_material_binding_get = grannyJNI.granny_material_binding_get(this.swigCPtr, i);
        if (granny_material_binding_get == 0) {
            return null;
        }
        return new C7006ayt(granny_material_binding_get, false);
    }
}
