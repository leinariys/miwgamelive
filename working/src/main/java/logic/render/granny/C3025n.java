package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.n */
/* compiled from: a */
public class C3025n {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3025n(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3025n() {
        this(grannyJNI.new_granny_texture_mip_level(), true);
    }

    /* renamed from: a */
    public static long m35921a(C3025n nVar) {
        if (nVar == null) {
            return 0;
        }
        return nVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_texture_mip_level(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo20649b(int i) {
        grannyJNI.granny_texture_mip_level_Stride_set(this.swigCPtr, i);
    }

    /* renamed from: e */
    public int mo20653e() {
        return grannyJNI.granny_texture_mip_level_Stride_get(this.swigCPtr);
    }

    /* renamed from: c */
    public void mo20650c(int i) {
        grannyJNI.granny_texture_mip_level_PixelByteCount_set(this.swigCPtr, i);
    }

    /* renamed from: f */
    public int mo20654f() {
        return grannyJNI.granny_texture_mip_level_PixelByteCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo20648a(C5501aLt alt) {
        grannyJNI.granny_texture_mip_level_PixelBytes_set(this.swigCPtr, C5501aLt.m16286h(alt));
    }

    /* renamed from: g */
    public C5501aLt mo20656g() {
        long granny_texture_mip_level_PixelBytes_get = grannyJNI.granny_texture_mip_level_PixelBytes_get(this.swigCPtr);
        if (granny_texture_mip_level_PixelBytes_get == 0) {
            return null;
        }
        return new C5501aLt(granny_texture_mip_level_PixelBytes_get, false);
    }

    /* renamed from: d */
    public C3025n mo20651d(int i) {
        long granny_texture_mip_level_get = grannyJNI.granny_texture_mip_level_get(this.swigCPtr, i);
        if (granny_texture_mip_level_get == 0) {
            return null;
        }
        return new C3025n(granny_texture_mip_level_get, false);
    }
}
