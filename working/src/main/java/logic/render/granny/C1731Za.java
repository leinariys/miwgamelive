package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.Za */
/* compiled from: a */
public class C1731Za {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1731Za(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1731Za() {
        this(grannyJNI.new_granny_data_type_definition(), true);
    }

    /* renamed from: bi */
    public static long m12101bi(C1731Za za) {
        if (za == null) {
            return 0;
        }
        return za.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_data_type_definition(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: c */
    public void mo7441c(C3278pt ptVar) {
        grannyJNI.granny_data_type_definition_Type_set(this.swigCPtr, ptVar.swigValue());
    }

    public C3278pt bHY() {
        return C3278pt.m37325dK(grannyJNI.granny_data_type_definition_Type_get(this.swigCPtr));
    }

    public String getName() {
        return grannyJNI.granny_data_type_definition_Name_get(this.swigCPtr);
    }

    public void setName(String str) {
        grannyJNI.granny_data_type_definition_Name_set(this.swigCPtr, str);
    }

    /* renamed from: bj */
    public void mo7440bj(C1731Za za) {
        grannyJNI.granny_data_type_definition_ReferenceType_set(this.swigCPtr, m12101bi(za));
    }

    public C1731Za bHZ() {
        long granny_data_type_definition_ReferenceType_get = grannyJNI.granny_data_type_definition_ReferenceType_get(this.swigCPtr);
        if (granny_data_type_definition_ReferenceType_get == 0) {
            return null;
        }
        return new C1731Za(granny_data_type_definition_ReferenceType_get, false);
    }

    /* renamed from: pi */
    public void mo7447pi(int i) {
        grannyJNI.granny_data_type_definition_ArrayWidth_set(this.swigCPtr, i);
    }

    public int bIa() {
        return grannyJNI.granny_data_type_definition_ArrayWidth_get(this.swigCPtr);
    }

    /* renamed from: k */
    public void mo7446k(C1845ab abVar) {
        grannyJNI.granny_data_type_definition_Extra_set(this.swigCPtr, C1845ab.m19847a(abVar));
    }

    public C1845ab bIb() {
        long granny_data_type_definition_Extra_get = grannyJNI.granny_data_type_definition_Extra_get(this.swigCPtr);
        if (granny_data_type_definition_Extra_get == 0) {
            return null;
        }
        return new C1845ab(granny_data_type_definition_Extra_get, false);
    }

    /* renamed from: gh */
    public void mo7445gh(long j) {
        grannyJNI.granny_data_type_definition_Ignored__Ignored_set(this.swigCPtr, j);
    }

    public long bIc() {
        return grannyJNI.granny_data_type_definition_Ignored__Ignored_get(this.swigCPtr);
    }

    public C1731Za bId() {
        long granny_data_type_definition_next = grannyJNI.granny_data_type_definition_next(this.swigCPtr);
        if (granny_data_type_definition_next == 0) {
            return null;
        }
        return new C1731Za(granny_data_type_definition_next, false);
    }

    /* renamed from: pj */
    public C1731Za mo7448pj(int i) {
        long granny_data_type_definition_get = grannyJNI.granny_data_type_definition_get(this.swigCPtr, i);
        if (granny_data_type_definition_get == 0) {
            return null;
        }
        return new C1731Za(granny_data_type_definition_get, false);
    }

    /* renamed from: a */
    public void mo7433a(int i, C1731Za za) {
        grannyJNI.granny_data_type_definition_set(this.swigCPtr, i, m12101bi(za));
    }
}
