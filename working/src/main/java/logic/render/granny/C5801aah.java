package logic.render.granny;

/* renamed from: a.aah  reason: case insensitive filesystem */
/* compiled from: a */
public class C5801aah {
    private long swigCPtr;

    public C5801aah(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5801aah() {
        this.swigCPtr = 0;
    }

    /* renamed from: x */
    public static long m19565x(C5801aah aah) {
        if (aah == null) {
            return 0;
        }
        return aah.swigCPtr;
    }
}
