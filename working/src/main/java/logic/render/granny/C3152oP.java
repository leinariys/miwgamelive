package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.oP */
/* compiled from: a */
public class C3152oP {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3152oP(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3152oP() {
        this(grannyJNI.new_granny_tri_material_group(), true);
    }

    /* renamed from: a */
    public static long m36708a(C3152oP oPVar) {
        if (oPVar == null) {
            return 0;
        }
        return oPVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_tri_material_group(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: dC */
    public void mo20983dC(int i) {
        grannyJNI.granny_tri_material_group_MaterialIndex_set(this.swigCPtr, i);
    }

    /* renamed from: Uz */
    public int mo20982Uz() {
        return grannyJNI.granny_tri_material_group_MaterialIndex_get(this.swigCPtr);
    }

    /* renamed from: dD */
    public void mo20984dD(int i) {
        grannyJNI.granny_tri_material_group_TriFirst_set(this.swigCPtr, i);
    }

    /* renamed from: UA */
    public int mo20980UA() {
        return grannyJNI.granny_tri_material_group_TriFirst_get(this.swigCPtr);
    }

    /* renamed from: dE */
    public void mo20985dE(int i) {
        grannyJNI.granny_tri_material_group_TriCount_set(this.swigCPtr, i);
    }

    /* renamed from: UB */
    public int mo20981UB() {
        return grannyJNI.granny_tri_material_group_TriCount_get(this.swigCPtr);
    }

    /* renamed from: dF */
    public C3152oP mo20986dF(int i) {
        long granny_tri_material_group_get = grannyJNI.granny_tri_material_group_get(this.swigCPtr, i);
        if (granny_tri_material_group_get == 0) {
            return null;
        }
        return new C3152oP(granny_tri_material_group_get, false);
    }
}
