package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.MZ */
/* compiled from: a */
public class C0867MZ {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0867MZ(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0867MZ() {
        this(grannyJNI.new_granny_bone_binding(), true);
    }

    /* renamed from: a */
    public static long m7062a(C0867MZ mz) {
        if (mz == null) {
            return 0;
        }
        return mz.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_bone_binding(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: eO */
    public void mo3980eO(String str) {
        grannyJNI.granny_bone_binding_BoneName_set(this.swigCPtr, str);
    }

    public String getBoneName() {
        return grannyJNI.granny_bone_binding_BoneName_get(this.swigCPtr);
    }

    /* renamed from: G */
    public void mo3973G(C3159oW oWVar) {
        grannyJNI.granny_bone_binding_OBBMin_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW bih() {
        long granny_bone_binding_OBBMin_get = grannyJNI.granny_bone_binding_OBBMin_get(this.swigCPtr);
        if (granny_bone_binding_OBBMin_get == 0) {
            return null;
        }
        return new C3159oW(granny_bone_binding_OBBMin_get, false);
    }

    /* renamed from: H */
    public void mo3974H(C3159oW oWVar) {
        grannyJNI.granny_bone_binding_OBBMax_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW bii() {
        long granny_bone_binding_OBBMax_get = grannyJNI.granny_bone_binding_OBBMax_get(this.swigCPtr);
        if (granny_bone_binding_OBBMax_get == 0) {
            return null;
        }
        return new C3159oW(granny_bone_binding_OBBMax_get, false);
    }

    /* renamed from: lN */
    public void mo3984lN(int i) {
        grannyJNI.granny_bone_binding_TriangleCount_set(this.swigCPtr, i);
    }

    public int bij() {
        return grannyJNI.granny_bone_binding_TriangleCount_get(this.swigCPtr);
    }

    /* renamed from: h */
    public void mo3983h(C1845ab abVar) {
        grannyJNI.granny_bone_binding_TriangleIndices_set(this.swigCPtr, C1845ab.m19847a(abVar));
    }

    public C1845ab bik() {
        long granny_bone_binding_TriangleIndices_get = grannyJNI.granny_bone_binding_TriangleIndices_get(this.swigCPtr);
        if (granny_bone_binding_TriangleIndices_get == 0) {
            return null;
        }
        return new C1845ab(granny_bone_binding_TriangleIndices_get, false);
    }

    /* renamed from: lO */
    public C0867MZ mo3985lO(int i) {
        long granny_bone_binding_get = grannyJNI.granny_bone_binding_get(this.swigCPtr, i);
        if (granny_bone_binding_get == 0) {
            return null;
        }
        return new C0867MZ(granny_bone_binding_get, false);
    }
}
