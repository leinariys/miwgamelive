package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aMh  reason: case insensitive filesystem */
/* compiled from: a */
public class C5515aMh {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5515aMh(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5515aMh() {
        this(grannyJNI.new_granny_transform(), true);
    }

    /* renamed from: f */
    public static long m16438f(C5515aMh amh) {
        if (amh == null) {
            return 0;
        }
        return amh.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_transform(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public long getFlags() {
        return grannyJNI.granny_transform_Flags_get(this.swigCPtr);
    }

    public void setFlags(long j) {
        grannyJNI.granny_transform_Flags_set(this.swigCPtr, j);
    }

    /* renamed from: b */
    public void mo10135b(C3159oW oWVar) {
        grannyJNI.granny_transform_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo10142s() {
        long granny_transform_Position_get = grannyJNI.granny_transform_Position_get(this.swigCPtr);
        if (granny_transform_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_transform_Position_get, false);
    }

    /* renamed from: a */
    public void mo10134a(C3159oW oWVar) {
        grannyJNI.granny_transform_Orientation_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: q */
    public C3159oW mo10141q() {
        long granny_transform_Orientation_get = grannyJNI.granny_transform_Orientation_get(this.swigCPtr);
        if (granny_transform_Orientation_get == 0) {
            return null;
        }
        return new C3159oW(granny_transform_Orientation_get, false);
    }

    /* renamed from: c */
    public void mo10136c(C5448aJs ajs) {
        grannyJNI.granny_transform_ScaleShear_set(this.swigCPtr, C5448aJs.m15872b(ajs));
    }

    public C5448aJs dja() {
        long granny_transform_ScaleShear_get = grannyJNI.granny_transform_ScaleShear_get(this.swigCPtr);
        if (granny_transform_ScaleShear_get == 0) {
            return null;
        }
        return new C5448aJs(granny_transform_ScaleShear_get, false);
    }

    /* renamed from: yH */
    public C5515aMh mo10144yH(int i) {
        long granny_transform_get = grannyJNI.granny_transform_get(this.swigCPtr, i);
        if (granny_transform_get == 0) {
            return null;
        }
        return new C5515aMh(granny_transform_get, false);
    }
}
