package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.akQ  reason: case insensitive filesystem */
/* compiled from: a */
public final class C6304akQ {
    public static final C6304akQ fUt = new C6304akQ("GrannyDeformPosition", grannyJNI.GrannyDeformPosition_get());
    public static final C6304akQ fUu = new C6304akQ("GrannyDeformPositionNormal");
    public static final C6304akQ fUv = new C6304akQ("GrannyDeformPositionNormalTangent");
    public static final C6304akQ fUw = new C6304akQ("GrannyDeformPositionNormalTangentBinormal");
    private static C6304akQ[] fUx = {fUt, fUu, fUv, fUw};

    /* renamed from: pF */
    private static int f4764pF = 0;

    /* renamed from: pG */
    private final int f4765pG;

    /* renamed from: pH */
    private final String f4766pH;

    private C6304akQ(String str) {
        this.f4766pH = str;
        int i = f4764pF;
        f4764pF = i + 1;
        this.f4765pG = i;
    }

    private C6304akQ(String str, int i) {
        this.f4766pH = str;
        this.f4765pG = i;
        f4764pF = i + 1;
    }

    private C6304akQ(String str, C6304akQ akq) {
        this.f4766pH = str;
        this.f4765pG = akq.f4765pG;
        f4764pF = this.f4765pG + 1;
    }

    /* renamed from: sm */
    public static C6304akQ m23174sm(int i) {
        if (i < fUx.length && i >= 0 && fUx[i].f4765pG == i) {
            return fUx[i];
        }
        for (int i2 = 0; i2 < fUx.length; i2++) {
            if (fUx[i2].f4765pG == i) {
                return fUx[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C6304akQ.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f4765pG;
    }

    public String toString() {
        return this.f4766pH;
    }
}
