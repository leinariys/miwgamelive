package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aSM */
/* compiled from: a */
public class aSM {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aSM(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aSM() {
        this(grannyJNI.new_granny_variant(), true);
    }

    /* renamed from: c */
    public static long m18172c(aSM asm) {
        if (asm == null) {
            return 0;
        }
        return asm.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_variant(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: bo */
    public void mo11361bo(C1731Za za) {
        grannyJNI.granny_variant_Type_set(this.swigCPtr, C1731Za.m12101bi(za));
    }

    public C1731Za duP() {
        long granny_variant_Type_get = grannyJNI.granny_variant_Type_get(this.swigCPtr);
        if (granny_variant_Type_get == 0) {
            return null;
        }
        return new C1731Za(granny_variant_Type_get, false);
    }

    /* renamed from: i */
    public void mo11366i(C5501aLt alt) {
        grannyJNI.granny_variant_Object_set(this.swigCPtr, C5501aLt.m16286h(alt));
    }

    public C5501aLt duQ() {
        long granny_variant_Object_get = grannyJNI.granny_variant_Object_get(this.swigCPtr);
        if (granny_variant_Object_get == 0) {
            return null;
        }
        return new C5501aLt(granny_variant_Object_get, false);
    }

    /* renamed from: AJ */
    public aSM mo11360AJ(int i) {
        long granny_variant_get = grannyJNI.granny_variant_get(this.swigCPtr, i);
        if (granny_variant_get == 0) {
            return null;
        }
        return new aSM(granny_variant_get, false);
    }
}
