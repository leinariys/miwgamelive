package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.ex */
/* compiled from: a */
public class C2406ex {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2406ex(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C2406ex() {
        this(grannyJNI.new_granny_texture_image(), true);
    }

    /* renamed from: a */
    public static long m30145a(C2406ex exVar) {
        if (exVar == null) {
            return 0;
        }
        return exVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_texture_image(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: aB */
    public void mo18237aB(int i) {
        grannyJNI.granny_texture_image_MIPLevelCount_set(this.swigCPtr, i);
    }

    /* renamed from: lQ */
    public int mo18242lQ() {
        return grannyJNI.granny_texture_image_MIPLevelCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo18239b(C3025n nVar) {
        grannyJNI.granny_texture_image_MIPLevels_set(this.swigCPtr, C3025n.m35921a(nVar));
    }

    /* renamed from: lR */
    public C3025n mo18243lR() {
        long granny_texture_image_MIPLevels_get = grannyJNI.granny_texture_image_MIPLevels_get(this.swigCPtr);
        if (granny_texture_image_MIPLevels_get == 0) {
            return null;
        }
        return new C3025n(granny_texture_image_MIPLevels_get, false);
    }

    /* renamed from: aC */
    public C2406ex mo18238aC(int i) {
        long granny_texture_image_get = grannyJNI.granny_texture_image_get(this.swigCPtr, i);
        if (granny_texture_image_get == 0) {
            return null;
        }
        return new C2406ex(granny_texture_image_get, false);
    }
}
