package logic.render.granny;

import logic.render.granny.dell.*;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.dS */
/* compiled from: a */
public class C2267dS {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2267dS(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C2267dS() {
        this(grannyJNI.new_granny_file_info(), true);
    }

    /* renamed from: a */
    public static long m28965a(C2267dS dSVar) {
        if (dSVar == null) {
            return 0;
        }
        return dSVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_file_info(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo17818a(C3222pQ pQVar) {
        grannyJNI.granny_file_info_ArtToolInfo_set(this.swigCPtr, C3222pQ.m37096b(pQVar));
    }

    /* renamed from: kr */
    public C3222pQ mo17847kr() {
        long granny_file_info_ArtToolInfo_get = grannyJNI.granny_file_info_ArtToolInfo_get(this.swigCPtr);
        if (granny_file_info_ArtToolInfo_get == 0) {
            return null;
        }
        return new C3222pQ(granny_file_info_ArtToolInfo_get, false);
    }

    /* renamed from: a */
    public void mo17817a(C3167od odVar) {
        grannyJNI.granny_file_info_ExporterInfo_set(this.swigCPtr, C3167od.m36747b(odVar));
    }

    /* renamed from: ks */
    public C3167od mo17848ks() {
        long granny_file_info_ExporterInfo_get = grannyJNI.granny_file_info_ExporterInfo_get(this.swigCPtr);
        if (granny_file_info_ExporterInfo_get == 0) {
            return null;
        }
        return new C3167od(granny_file_info_ExporterInfo_get, false);
    }

    /* renamed from: T */
    public void mo17809T(String str) {
        grannyJNI.granny_file_info_FromFileName_set(this.swigCPtr, str);
    }

    /* renamed from: kt */
    public String mo17849kt() {
        return grannyJNI.granny_file_info_FromFileName_get(this.swigCPtr);
    }

    /* renamed from: ag */
    public void mo17821ag(int i) {
        grannyJNI.granny_file_info_TextureCount_set(this.swigCPtr, i);
    }

    /* renamed from: ku */
    public int mo17850ku() {
        return grannyJNI.granny_file_info_TextureCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo17813a(C6456anM anm) {
        grannyJNI.granny_file_info_Textures_set(this.swigCPtr, C6456anM.m24139c(anm));
    }

    /* renamed from: kv */
    public C6456anM mo17851kv() {
        long granny_file_info_Textures_get = grannyJNI.granny_file_info_Textures_get(this.swigCPtr);
        if (granny_file_info_Textures_get == 0) {
            return null;
        }
        return new C6456anM(granny_file_info_Textures_get, false);
    }

    /* renamed from: ah */
    public void mo17822ah(int i) {
        grannyJNI.granny_file_info_MaterialCount_set(this.swigCPtr, i);
    }

    /* renamed from: kw */
    public int mo17852kw() {
        return grannyJNI.granny_file_info_MaterialCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo17811a(aBS abs) {
        grannyJNI.granny_file_info_Materials_set(this.swigCPtr, aBS.m12939c(abs));
    }

    /* renamed from: kx */
    public aBS mo17853kx() {
        long granny_file_info_Materials_get = grannyJNI.granny_file_info_Materials_get(this.swigCPtr);
        if (granny_file_info_Materials_get == 0) {
            return null;
        }
        return new aBS(granny_file_info_Materials_get, false);
    }

    /* renamed from: ai */
    public void mo17823ai(int i) {
        grannyJNI.granny_file_info_SkeletonCount_set(this.swigCPtr, i);
    }

    /* renamed from: ky */
    public int mo17854ky() {
        return grannyJNI.granny_file_info_SkeletonCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo17820a(C4082zD zDVar) {
        grannyJNI.granny_file_info_Skeletons_set(this.swigCPtr, C4082zD.m41509b(zDVar));
    }

    /* renamed from: kz */
    public C4082zD mo17855kz() {
        long granny_file_info_Skeletons_get = grannyJNI.granny_file_info_Skeletons_get(this.swigCPtr);
        if (granny_file_info_Skeletons_get == 0) {
            return null;
        }
        return new C4082zD(granny_file_info_Skeletons_get, false);
    }

    /* renamed from: aj */
    public void mo17824aj(int i) {
        grannyJNI.granny_file_info_VertexDataCount_set(this.swigCPtr, i);
    }

    /* renamed from: kA */
    public int mo17834kA() {
        return grannyJNI.granny_file_info_VertexDataCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo17810a(C0438GF gf) {
        grannyJNI.granny_file_info_VertexDatas_set(this.swigCPtr, C0438GF.m3342b(gf));
    }

    /* renamed from: kB */
    public C0438GF mo17835kB() {
        long granny_file_info_VertexDatas_get = grannyJNI.granny_file_info_VertexDatas_get(this.swigCPtr);
        if (granny_file_info_VertexDatas_get == 0) {
            return null;
        }
        return new C0438GF(granny_file_info_VertexDatas_get, false);
    }

    /* renamed from: ak */
    public void mo17825ak(int i) {
        grannyJNI.granny_file_info_TriTopologyCount_set(this.swigCPtr, i);
    }

    /* renamed from: kC */
    public int mo17836kC() {
        return grannyJNI.granny_file_info_TriTopologyCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo17814a(C6658arG arg) {
        grannyJNI.granny_file_info_TriTopologies_set(this.swigCPtr, C6658arG.m25380c(arg));
    }

    /* renamed from: kD */
    public C6658arG mo17837kD() {
        long granny_file_info_TriTopologies_get = grannyJNI.granny_file_info_TriTopologies_get(this.swigCPtr);
        if (granny_file_info_TriTopologies_get == 0) {
            return null;
        }
        return new C6658arG(granny_file_info_TriTopologies_get, false);
    }

    /* renamed from: al */
    public void mo17826al(int i) {
        grannyJNI.granny_file_info_MeshCount_set(this.swigCPtr, i);
    }

    /* renamed from: kE */
    public int mo17838kE() {
        return grannyJNI.granny_file_info_MeshCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo17816a(C2591hG hGVar) {
        grannyJNI.granny_file_info_Meshes_set(this.swigCPtr, C2591hG.m32536b(hGVar));
    }

    /* renamed from: kF */
    public C2591hG mo17839kF() {
        long granny_file_info_Meshes_get = grannyJNI.granny_file_info_Meshes_get(this.swigCPtr);
        if (granny_file_info_Meshes_get == 0) {
            return null;
        }
        return new C2591hG(granny_file_info_Meshes_get, false);
    }

    /* renamed from: am */
    public void mo17827am(int i) {
        grannyJNI.granny_file_info_ModelCount_set(this.swigCPtr, i);
    }

    /* renamed from: kG */
    public int mo17840kG() {
        return grannyJNI.granny_file_info_ModelCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo17819a(C3521ru ruVar) {
        grannyJNI.granny_file_info_Models_set(this.swigCPtr, C3521ru.m38665b(ruVar));
    }

    /* renamed from: kH */
    public C3521ru mo17841kH() {
        long granny_file_info_Models_get = grannyJNI.granny_file_info_Models_get(this.swigCPtr);
        if (granny_file_info_Models_get == 0) {
            return null;
        }
        return new C3521ru(granny_file_info_Models_get, false);
    }

    /* renamed from: an */
    public void mo17828an(int i) {
        grannyJNI.granny_file_info_TrackGroupCount_set(this.swigCPtr, i);
    }

    /* renamed from: kI */
    public int mo17842kI() {
        return grannyJNI.granny_file_info_TrackGroupCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo17831b(C2016az azVar) {
        grannyJNI.granny_file_info_TrackGroups_set(this.swigCPtr, C2016az.m27559a(azVar));
    }

    /* renamed from: kJ */
    public C2016az mo17843kJ() {
        long granny_file_info_TrackGroups_get = grannyJNI.granny_file_info_TrackGroups_get(this.swigCPtr);
        if (granny_file_info_TrackGroups_get == 0) {
            return null;
        }
        return new C2016az(granny_file_info_TrackGroups_get, false);
    }

    /* renamed from: ao */
    public void mo17829ao(int i) {
        grannyJNI.granny_file_info_AnimationCount_set(this.swigCPtr, i);
    }

    /* renamed from: kK */
    public int mo17844kK() {
        return grannyJNI.granny_file_info_AnimationCount_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo17815a(C6917awF awf) {
        grannyJNI.granny_file_info_Animations_set(this.swigCPtr, C6917awF.m26850c(awf));
    }

    /* renamed from: kL */
    public C6917awF mo17845kL() {
        long granny_file_info_Animations_get = grannyJNI.granny_file_info_Animations_get(this.swigCPtr);
        if (granny_file_info_Animations_get == 0) {
            return null;
        }
        return new C6917awF(granny_file_info_Animations_get, false);
    }

    /* renamed from: a */
    public void mo17812a(aSM asm) {
        grannyJNI.granny_file_info_ExtendedData_set(this.swigCPtr, aSM.m18172c(asm));
    }

    /* renamed from: kM */
    public aSM mo17846kM() {
        long granny_file_info_ExtendedData_get = grannyJNI.granny_file_info_ExtendedData_get(this.swigCPtr);
        if (granny_file_info_ExtendedData_get == 0) {
            return null;
        }
        return new aSM(granny_file_info_ExtendedData_get, false);
    }

    /* renamed from: ap */
    public C2267dS mo17830ap(int i) {
        long granny_file_info_get = grannyJNI.granny_file_info_get(this.swigCPtr, i);
        if (granny_file_info_get == 0) {
            return null;
        }
        return new C2267dS(granny_file_info_get, false);
    }
}
