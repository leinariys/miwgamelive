package logic.render.granny;

/* renamed from: a.avo  reason: case insensitive filesystem */
/* compiled from: a */
public class C6900avo {
    private long swigCPtr;

    public C6900avo(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6900avo() {
        this.swigCPtr = 0;
    }

    /* renamed from: d */
    public static long m26731d(C6900avo avo) {
        if (avo == null) {
            return 0;
        }
        return avo.swigCPtr;
    }
}
