package logic.render.granny;

/* renamed from: a.ahD  reason: case insensitive filesystem */
/* compiled from: a */
public class C6135ahD {
    private long swigCPtr;

    public C6135ahD(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6135ahD() {
        this.swigCPtr = 0;
    }

    /* renamed from: i */
    public static long m22189i(C6135ahD ahd) {
        if (ahd == null) {
            return 0;
        }
        return ahd.swigCPtr;
    }
}
