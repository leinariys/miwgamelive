package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.eV */
/* compiled from: a */
public class C2362eV {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2362eV(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C2362eV() {
        this(grannyJNI.new_granny_pt32_vertex(), true);
    }

    /* renamed from: a */
    public static long m29548a(C2362eV eVVar) {
        if (eVVar == null) {
            return 0;
        }
        return eVVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pt32_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo18017b(C3159oW oWVar) {
        grannyJNI.granny_pt32_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo18022s() {
        long granny_pt32_vertex_Position_get = grannyJNI.granny_pt32_vertex_Position_get(this.swigCPtr);
        if (granny_pt32_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pt32_vertex_Position_get, false);
    }

    /* renamed from: j */
    public void mo18020j(C3159oW oWVar) {
        grannyJNI.granny_pt32_vertex_UV_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: mv */
    public C3159oW mo18021mv() {
        long granny_pt32_vertex_UV_get = grannyJNI.granny_pt32_vertex_UV_get(this.swigCPtr);
        if (granny_pt32_vertex_UV_get == 0) {
            return null;
        }
        return new C3159oW(granny_pt32_vertex_UV_get, false);
    }

    /* renamed from: aG */
    public C2362eV mo18016aG(int i) {
        long granny_pt32_vertex_get = grannyJNI.granny_pt32_vertex_get(this.swigCPtr, i);
        if (granny_pt32_vertex_get == 0) {
            return null;
        }
        return new C2362eV(granny_pt32_vertex_get, false);
    }
}
