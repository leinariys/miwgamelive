package logic.render.granny.dell;

import logic.render.granny.aSM;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.od */
/* compiled from: a */
public class C3167od {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3167od(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3167od() {
        this(grannyJNI.new_granny_exporter_info(), true);
    }

    /* renamed from: b */
    public static long m36747b(C3167od odVar) {
        if (odVar == null) {
            return 0;
        }
        return odVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_exporter_info(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: aX */
    public void mo21012aX(String str) {
        grannyJNI.granny_exporter_info_ExporterName_set(this.swigCPtr, str);
    }

    /* renamed from: Sj */
    public String mo21006Sj() {
        return grannyJNI.granny_exporter_info_ExporterName_get(this.swigCPtr);
    }

    /* renamed from: dn */
    public void mo21014dn(int i) {
        grannyJNI.granny_exporter_info_ExporterMajorRevision_set(this.swigCPtr, i);
    }

    /* renamed from: Sk */
    public int mo21007Sk() {
        return grannyJNI.granny_exporter_info_ExporterMajorRevision_get(this.swigCPtr);
    }

    /* renamed from: do */
    public void mo21015do(int i) {
        grannyJNI.granny_exporter_info_ExporterMinorRevision_set(this.swigCPtr, i);
    }

    /* renamed from: Sl */
    public int mo21008Sl() {
        return grannyJNI.granny_exporter_info_ExporterMinorRevision_get(this.swigCPtr);
    }

    /* renamed from: dp */
    public void mo21016dp(int i) {
        grannyJNI.granny_exporter_info_ExporterCustomization_set(this.swigCPtr, i);
    }

    /* renamed from: Sm */
    public int mo21009Sm() {
        return grannyJNI.granny_exporter_info_ExporterCustomization_get(this.swigCPtr);
    }

    /* renamed from: dq */
    public void mo21017dq(int i) {
        grannyJNI.granny_exporter_info_ExporterBuildNumber_set(this.swigCPtr, i);
    }

    /* renamed from: Sn */
    public int mo21010Sn() {
        return grannyJNI.granny_exporter_info_ExporterBuildNumber_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo21011a(aSM asm) {
        grannyJNI.granny_exporter_info_ExtendedData_set(this.swigCPtr, aSM.m18172c(asm));
    }

    /* renamed from: kM */
    public aSM mo21020kM() {
        long granny_exporter_info_ExtendedData_get = grannyJNI.granny_exporter_info_ExtendedData_get(this.swigCPtr);
        if (granny_exporter_info_ExtendedData_get == 0) {
            return null;
        }
        return new aSM(granny_exporter_info_ExtendedData_get, false);
    }

    /* renamed from: dr */
    public C3167od mo21018dr(int i) {
        long granny_exporter_info_get = grannyJNI.granny_exporter_info_get(this.swigCPtr, i);
        if (granny_exporter_info_get == 0) {
            return null;
        }
        return new C3167od(granny_exporter_info_get, false);
    }
}
