package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.apT  reason: case insensitive filesystem */
/* compiled from: a */
public class C6567apT {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6567apT(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6567apT() {
        this(grannyJNI.new_granny_curve_data_da_k32f_c32f(), true);
    }

    /* renamed from: a */
    public static long m24891a(C6567apT apt) {
        if (apt == null) {
            return 0;
        }
        return apt.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve_data_da_k32f_c32f(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo15416a(C6341alB alb) {
        grannyJNI.granny_curve_data_da_k32f_c32f_CurveDataHeader_set(this.swigCPtr, C6341alB.m23687b(alb));
    }

    /* renamed from: Qa */
    public C6341alB mo15414Qa() {
        long granny_curve_data_da_k32f_c32f_CurveDataHeader_get = grannyJNI.granny_curve_data_da_k32f_c32f_CurveDataHeader_get(this.swigCPtr);
        if (granny_curve_data_da_k32f_c32f_CurveDataHeader_get == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_da_k32f_c32f_CurveDataHeader_get, false);
    }

    /* renamed from: e */
    public void mo15422e(short s) {
        grannyJNI.granny_curve_data_da_k32f_c32f_Padding_set(this.swigCPtr, s);
    }

    /* renamed from: Qb */
    public short mo15415Qb() {
        return grannyJNI.granny_curve_data_da_k32f_c32f_Padding_get(this.swigCPtr);
    }

    /* renamed from: tB */
    public void mo15425tB(int i) {
        grannyJNI.granny_curve_data_da_k32f_c32f_KnotCount_set(this.swigCPtr, i);
    }

    public int cqx() {
        return grannyJNI.granny_curve_data_da_k32f_c32f_KnotCount_get(this.swigCPtr);
    }

    /* renamed from: M */
    public void mo15413M(C3159oW oWVar) {
        grannyJNI.granny_curve_data_da_k32f_c32f_Knots_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW cqy() {
        long granny_curve_data_da_k32f_c32f_Knots_get = grannyJNI.granny_curve_data_da_k32f_c32f_Knots_get(this.swigCPtr);
        if (granny_curve_data_da_k32f_c32f_Knots_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_da_k32f_c32f_Knots_get, false);
    }

    /* renamed from: qv */
    public void mo15424qv(int i) {
        grannyJNI.granny_curve_data_da_k32f_c32f_ControlCount_set(this.swigCPtr, i);
    }

    public int bVw() {
        return grannyJNI.granny_curve_data_da_k32f_c32f_ControlCount_get(this.swigCPtr);
    }

    /* renamed from: K */
    public void mo15412K(C3159oW oWVar) {
        grannyJNI.granny_curve_data_da_k32f_c32f_Controls_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW bEk() {
        long granny_curve_data_da_k32f_c32f_Controls_get = grannyJNI.granny_curve_data_da_k32f_c32f_Controls_get(this.swigCPtr);
        if (granny_curve_data_da_k32f_c32f_Controls_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_da_k32f_c32f_Controls_get, false);
    }

    /* renamed from: tC */
    public C6567apT mo15426tC(int i) {
        long granny_curve_data_da_k32f_c32f_get = grannyJNI.granny_curve_data_da_k32f_c32f_get(this.swigCPtr, i);
        if (granny_curve_data_da_k32f_c32f_get == 0) {
            return null;
        }
        return new C6567apT(granny_curve_data_da_k32f_c32f_get, false);
    }
}
