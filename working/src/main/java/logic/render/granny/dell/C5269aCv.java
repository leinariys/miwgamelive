package logic.render.granny.dell;

/* renamed from: a.aCv  reason: case insensitive filesystem */
/* compiled from: a */
public class C5269aCv {
    private long swigCPtr;

    public C5269aCv(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5269aCv() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m13385b(C5269aCv acv) {
        if (acv == null) {
            return 0;
        }
        return acv.swigCPtr;
    }
}
