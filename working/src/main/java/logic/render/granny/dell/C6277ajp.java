package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.ajp  reason: case insensitive filesystem */
/* compiled from: a */
public class C6277ajp {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6277ajp(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6277ajp() {
        this(grannyJNI.new_granny_fixed_allocator_block(), true);
    }

    /* renamed from: a */
    public static long m23053a(C6277ajp ajp) {
        if (ajp == null) {
            return 0;
        }
        return ajp.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_fixed_allocator_block(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: ry */
    public void mo14196ry(int i) {
        grannyJNI.granny_fixed_allocator_block_UnitCount_set(this.swigCPtr, i);
    }

    public int cdK() {
        return grannyJNI.granny_fixed_allocator_block_UnitCount_get(this.swigCPtr);
    }

    /* renamed from: h */
    public void mo14195h(C2382ef efVar) {
        grannyJNI.granny_fixed_allocator_block_Units_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    public C2382ef cdL() {
        long granny_fixed_allocator_block_Units_get = grannyJNI.granny_fixed_allocator_block_Units_get(this.swigCPtr);
        if (granny_fixed_allocator_block_Units_get == 0) {
            return null;
        }
        return new C2382ef(granny_fixed_allocator_block_Units_get, false);
    }

    /* renamed from: a */
    public void mo14185a(C6372alg alg) {
        grannyJNI.granny_fixed_allocator_block_FirstFreeUnit_set(this.swigCPtr, C6372alg.m23797b(alg));
    }

    public C6372alg cdM() {
        long granny_fixed_allocator_block_FirstFreeUnit_get = grannyJNI.granny_fixed_allocator_block_FirstFreeUnit_get(this.swigCPtr);
        if (granny_fixed_allocator_block_FirstFreeUnit_get == 0) {
            return null;
        }
        return new C6372alg(granny_fixed_allocator_block_FirstFreeUnit_get, false);
    }

    /* renamed from: b */
    public void mo14186b(C6277ajp ajp) {
        grannyJNI.granny_fixed_allocator_block_Next_set(this.swigCPtr, m23053a(ajp));
    }

    public C6277ajp cdN() {
        long granny_fixed_allocator_block_Next_get = grannyJNI.granny_fixed_allocator_block_Next_get(this.swigCPtr);
        if (granny_fixed_allocator_block_Next_get == 0) {
            return null;
        }
        return new C6277ajp(granny_fixed_allocator_block_Next_get, false);
    }

    /* renamed from: c */
    public void mo14187c(C6277ajp ajp) {
        grannyJNI.granny_fixed_allocator_block_Previous_set(this.swigCPtr, m23053a(ajp));
    }

    public C6277ajp cdO() {
        long granny_fixed_allocator_block_Previous_get = grannyJNI.granny_fixed_allocator_block_Previous_get(this.swigCPtr);
        if (granny_fixed_allocator_block_Previous_get == 0) {
            return null;
        }
        return new C6277ajp(granny_fixed_allocator_block_Previous_get, false);
    }

    /* renamed from: rz */
    public C6277ajp mo14197rz(int i) {
        long granny_fixed_allocator_block_get = grannyJNI.granny_fixed_allocator_block_get(this.swigCPtr, i);
        if (granny_fixed_allocator_block_get == 0) {
            return null;
        }
        return new C6277ajp(granny_fixed_allocator_block_get, false);
    }
}
