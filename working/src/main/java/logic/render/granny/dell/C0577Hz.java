package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.Hz */
/* compiled from: a */
public class C0577Hz {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0577Hz(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0577Hz() {
        this(grannyJNI.new_granny_grn_pointer_fixup(), true);
    }

    /* renamed from: a */
    public static long m5296a(C0577Hz hz) {
        if (hz == null) {
            return 0;
        }
        return hz.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_grn_pointer_fixup(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: eQ */
    public void mo2715eQ(long j) {
        grannyJNI.granny_grn_pointer_fixup_FromOffset_set(this.swigCPtr, j);
    }

    public long aRL() {
        return grannyJNI.granny_grn_pointer_fixup_FromOffset_get(this.swigCPtr);
    }

    /* renamed from: c */
    public void mo2713c(C0031AQ aq) {
        grannyJNI.granny_grn_pointer_fixup_To_set(this.swigCPtr, C0031AQ.m385a(aq));
    }

    public C0031AQ aRM() {
        long granny_grn_pointer_fixup_To_get = grannyJNI.granny_grn_pointer_fixup_To_get(this.swigCPtr);
        if (granny_grn_pointer_fixup_To_get == 0) {
            return null;
        }
        return new C0031AQ(granny_grn_pointer_fixup_To_get, false);
    }

    /* renamed from: hs */
    public C0577Hz mo2717hs(int i) {
        long granny_grn_pointer_fixup_get = grannyJNI.granny_grn_pointer_fixup_get(this.swigCPtr, i);
        if (granny_grn_pointer_fixup_get == 0) {
            return null;
        }
        return new C0577Hz(granny_grn_pointer_fixup_get, false);
    }
}
