package logic.render.granny.dell;

/* renamed from: a.qS */
/* compiled from: a */
public final class C3338qS {
    public static final C3338qS aWT = new C3338qS("GrannyNotImplementedLogMessage");
    public static final C3338qS aWU = new C3338qS("GrannyApplicationLogMessage");
    public static final C3338qS aWV = new C3338qS("GrannyWin32SubsystemLogMessage");
    public static final C3338qS aWW = new C3338qS("GrannyWin64SubsystemLogMessage");
    public static final C3338qS aWX = new C3338qS("GrannyMacOSSubsystemLogMessage");
    public static final C3338qS aWY = new C3338qS("GrannyANSISubsystemLogMessage");
    public static final C3338qS aWZ = new C3338qS("GrannyGamecubeSubsystemLogMessage");
    public static final C3338qS aXA = new C3338qS("GrannyDataTypeLogMessage");
    public static final C3338qS aXB = new C3338qS("GrannyNameMapLogMessage");
    public static final C3338qS aXC = new C3338qS("GrannyMaterialLogMessage");
    public static final C3338qS aXD = new C3338qS("GrannyModelLogMessage");
    public static final C3338qS aXE = new C3338qS("GrannyStackAllocatorLogMessage");
    public static final C3338qS aXF = new C3338qS("GrannyFixedAllocatorLogMessage");
    public static final C3338qS aXG = new C3338qS("GrannySceneLogMessage");
    public static final C3338qS aXH = new C3338qS("GrannyTrackMaskLogMessage");
    public static final C3338qS aXI = new C3338qS("GrannyLocalPoseLogMessage");
    public static final C3338qS aXJ = new C3338qS("GrannyWorldPoseLogMessage");
    public static final C3338qS aXK = new C3338qS("GrannyNameLibraryLogMessage");
    public static final C3338qS aXL = new C3338qS("GrannyControlLogMessage");
    public static final C3338qS aXM = new C3338qS("GrannyMeshBindingLogMessage");
    public static final C3338qS aXN = new C3338qS("GrannyMathLogMessage");
    public static final C3338qS aXO = new C3338qS("GrannyVersionLogMessage");
    public static final C3338qS aXP = new C3338qS("GrannyMemoryLogMessage");
    public static final C3338qS aXQ = new C3338qS("GrannyDeformerLogMessage");
    public static final C3338qS aXR = new C3338qS("GrannyVoxelLogMessage");
    public static final C3338qS aXS = new C3338qS("GrannyBitmapLogMessage");
    public static final C3338qS aXT = new C3338qS("GrannyIKLogMessage");
    public static final C3338qS aXU = new C3338qS("GrannyCurveLogMessage");
    public static final C3338qS aXV = new C3338qS("GrannyTrackGroupLogMessage");
    public static final C3338qS aXW = new C3338qS("GrannyThreadSafetyLogMessage");
    public static final C3338qS aXX = new C3338qS("GrannyQuantizeLogMessage");
    public static final C3338qS aXY = new C3338qS("GrannyBlendDagLogMessage");
    public static final C3338qS aXZ = new C3338qS("GrannyOnePastLastMessageOrigin");
    public static final C3338qS aXa = new C3338qS("GrannyPS2SubsystemLogMessage");
    public static final C3338qS aXb = new C3338qS("GrannyPSPSubsystemLogMessage");
    public static final C3338qS aXc = new C3338qS("GrannyPS3SubsystemLogMessage");
    public static final C3338qS aXd = new C3338qS("GrannyXboxSubsystemLogMessage");
    public static final C3338qS aXe = new C3338qS("GrannyXenonSubsystemLogMessage");
    public static final C3338qS aXf = new C3338qS("GrannyMAXSubsystemLogMessage");
    public static final C3338qS aXg = new C3338qS("GrannyMayaSubsystemLogMessage");
    public static final C3338qS aXh = new C3338qS("GrannyXSISubsystemLogMessage");
    public static final C3338qS aXi = new C3338qS("GrannyLightwaveSubsystemLogMessage");
    public static final C3338qS aXj = new C3338qS("GrannyFileWritingLogMessage");
    public static final C3338qS aXk = new C3338qS("GrannyFileReadingLogMessage");
    public static final C3338qS aXl = new C3338qS("GrannyExporterLogMessage");
    public static final C3338qS aXm = new C3338qS("GrannyCompressorLogMessage");
    public static final C3338qS aXn = new C3338qS("GrannyStringLogMessage");
    public static final C3338qS aXo = new C3338qS("GrannyStringTableLogMessage");
    public static final C3338qS aXp = new C3338qS("GrannyVertexLayoutLogMessage");
    public static final C3338qS aXq = new C3338qS("GrannyMeshLogMessage");
    public static final C3338qS aXr = new C3338qS("GrannyPropertyLogMessage");
    public static final C3338qS aXs = new C3338qS("GrannySkeletonLogMessage");
    public static final C3338qS aXt = new C3338qS("GrannyAnimationLogMessage");
    public static final C3338qS aXu = new C3338qS("GrannySetupGraphLogMessage");
    public static final C3338qS aXv = new C3338qS("GrannyTextureLogMessage");
    public static final C3338qS aXw = new C3338qS("GrannyBSplineLogMessage");
    public static final C3338qS aXx = new C3338qS("GrannyHashLogMessage");
    public static final C3338qS aXy = new C3338qS("GrannyLinkerLogMessage");
    public static final C3338qS aXz = new C3338qS("GrannyInstantiatorLogMessage");
    private static C3338qS[] aYa = {aWT, aWU, aWV, aWW, aWX, aWY, aWZ, aXa, aXb, aXc, aXd, aXe, aXf, aXg, aXh, aXi, aXj, aXk, aXl, aXm, aXn, aXo, aXp, aXq, aXr, aXs, aXt, aXu, aXv, aXw, aXx, aXy, aXz, aXA, aXB, aXC, aXD, aXE, aXF, aXG, aXH, aXI, aXJ, aXK, aXL, aXM, aXN, aXO, aXP, aXQ, aXR, aXS, aXT, aXU, aXV, aXW, aXX, aXY, aXZ};

    /* renamed from: pF */
    private static int f8907pF = 0;

    /* renamed from: pG */
    private final int f8908pG;

    /* renamed from: pH */
    private final String f8909pH;

    private C3338qS(String str) {
        this.f8909pH = str;
        int i = f8907pF;
        f8907pF = i + 1;
        this.f8908pG = i;
    }

    private C3338qS(String str, int i) {
        this.f8909pH = str;
        this.f8908pG = i;
        f8907pF = i + 1;
    }

    private C3338qS(String str, C3338qS qSVar) {
        this.f8909pH = str;
        this.f8908pG = qSVar.f8908pG;
        f8907pF = this.f8908pG + 1;
    }

    /* renamed from: eg */
    public static C3338qS m37522eg(int i) {
        if (i < aYa.length && i >= 0 && aYa[i].f8908pG == i) {
            return aYa[i];
        }
        for (int i2 = 0; i2 < aYa.length; i2++) {
            if (aYa[i2].f8908pG == i) {
                return aYa[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C3338qS.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f8908pG;
    }

    public String toString() {
        return this.f8909pH;
    }
}
