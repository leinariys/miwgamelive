package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aEy  reason: case insensitive filesystem */
/* compiled from: a */
public class C5324aEy {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5324aEy(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5324aEy() {
        this(grannyJNI.new_granny_text_track_entry(), true);
    }

    /* renamed from: b */
    public static long m14469b(C5324aEy aey) {
        if (aey == null) {
            return 0;
        }
        return aey.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_text_track_entry(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: mw */
    public void mo8715mw(float f) {
        grannyJNI.granny_text_track_entry_TimeStamp_set(this.swigCPtr, f);
    }

    public float cXL() {
        return grannyJNI.granny_text_track_entry_TimeStamp_get(this.swigCPtr);
    }

    public String getText() {
        return grannyJNI.granny_text_track_entry_Text_get(this.swigCPtr);
    }

    public void setText(String str) {
        grannyJNI.granny_text_track_entry_Text_set(this.swigCPtr, str);
    }

    /* renamed from: xL */
    public C5324aEy mo8717xL(int i) {
        long granny_text_track_entry_get = grannyJNI.granny_text_track_entry_get(this.swigCPtr, i);
        if (granny_text_track_entry_get == 0) {
            return null;
        }
        return new C5324aEy(granny_text_track_entry_get, false);
    }
}
