package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.Iv */
/* compiled from: a */
public class C0629Iv {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0629Iv(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0629Iv() {
        this(grannyJNI.new_granny_curve_data_d3i1_k8u_c8u(), true);
    }

    /* renamed from: a */
    public static long m5494a(C0629Iv iv) {
        if (iv == null) {
            return 0;
        }
        return iv.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve_data_d3i1_k8u_c8u(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo2901a(C6341alB alb) {
        grannyJNI.granny_curve_data_d3i1_k8u_c8u_CurveDataHeader_set(this.swigCPtr, C6341alB.m23687b(alb));
    }

    /* renamed from: Qa */
    public C6341alB mo2896Qa() {
        long granny_curve_data_d3i1_k8u_c8u_CurveDataHeader_get = grannyJNI.granny_curve_data_d3i1_k8u_c8u_CurveDataHeader_get(this.swigCPtr);
        if (granny_curve_data_d3i1_k8u_c8u_CurveDataHeader_get == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_d3i1_k8u_c8u_CurveDataHeader_get, false);
    }

    /* renamed from: dT */
    public void mo2904dT(int i) {
        grannyJNI.granny_curve_data_d3i1_k8u_c8u_OneOverKnotScaleTrunc_set(this.swigCPtr, i);
    }

    /* renamed from: WK */
    public int mo2900WK() {
        return grannyJNI.granny_curve_data_d3i1_k8u_c8u_OneOverKnotScaleTrunc_get(this.swigCPtr);
    }

    /* renamed from: n */
    public void mo2909n(C3159oW oWVar) {
        grannyJNI.granny_curve_data_d3i1_k8u_c8u_ControlScales_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: Qc */
    public C3159oW mo2897Qc() {
        long granny_curve_data_d3i1_k8u_c8u_ControlScales_get = grannyJNI.granny_curve_data_d3i1_k8u_c8u_ControlScales_get(this.swigCPtr);
        if (granny_curve_data_d3i1_k8u_c8u_ControlScales_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_d3i1_k8u_c8u_ControlScales_get, false);
    }

    /* renamed from: o */
    public void mo2910o(C3159oW oWVar) {
        grannyJNI.granny_curve_data_d3i1_k8u_c8u_ControlOffsets_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: Qd */
    public C3159oW mo2898Qd() {
        long granny_curve_data_d3i1_k8u_c8u_ControlOffsets_get = grannyJNI.granny_curve_data_d3i1_k8u_c8u_ControlOffsets_get(this.swigCPtr);
        if (granny_curve_data_d3i1_k8u_c8u_ControlOffsets_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_d3i1_k8u_c8u_ControlOffsets_get, false);
    }

    /* renamed from: di */
    public void mo2906di(int i) {
        grannyJNI.granny_curve_data_d3i1_k8u_c8u_KnotControlCount_set(this.swigCPtr, i);
    }

    /* renamed from: Qe */
    public int mo2899Qe() {
        return grannyJNI.granny_curve_data_d3i1_k8u_c8u_KnotControlCount_get(this.swigCPtr);
    }

    /* renamed from: d */
    public void mo2903d(C2382ef efVar) {
        grannyJNI.granny_curve_data_d3i1_k8u_c8u_KnotsControls_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    public C2382ef ano() {
        long granny_curve_data_d3i1_k8u_c8u_KnotsControls_get = grannyJNI.granny_curve_data_d3i1_k8u_c8u_KnotsControls_get(this.swigCPtr);
        if (granny_curve_data_d3i1_k8u_c8u_KnotsControls_get == 0) {
            return null;
        }
        return new C2382ef(granny_curve_data_d3i1_k8u_c8u_KnotsControls_get, false);
    }

    /* renamed from: kI */
    public C0629Iv mo2908kI(int i) {
        long granny_curve_data_d3i1_k8u_c8u_get = grannyJNI.granny_curve_data_d3i1_k8u_c8u_get(this.swigCPtr, i);
        if (granny_curve_data_d3i1_k8u_c8u_get == 0) {
            return null;
        }
        return new C0629Iv(granny_curve_data_d3i1_k8u_c8u_get, false);
    }
}
