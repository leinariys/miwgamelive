package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aa */
/* compiled from: a */
public class C1834aa {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1834aa(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1834aa() {
        this(grannyJNI.new_granny_pwn323_vertex(), true);
    }

    /* renamed from: a */
    public static long m19362a(C1834aa aaVar) {
        if (aaVar == null) {
            return 0;
        }
        return aaVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pwn323_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo12132b(C3159oW oWVar) {
        grannyJNI.granny_pwn323_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo12140s() {
        long granny_pwn323_vertex_Position_get = grannyJNI.granny_pwn323_vertex_Position_get(this.swigCPtr);
        if (granny_pwn323_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwn323_vertex_Position_get, false);
    }

    /* renamed from: a */
    public void mo12130a(C2382ef efVar) {
        grannyJNI.granny_pwn323_vertex_BoneWeights_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cP */
    public C2382ef mo12133cP() {
        long granny_pwn323_vertex_BoneWeights_get = grannyJNI.granny_pwn323_vertex_BoneWeights_get(this.swigCPtr);
        if (granny_pwn323_vertex_BoneWeights_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwn323_vertex_BoneWeights_get, false);
    }

    /* renamed from: b */
    public void mo12131b(C2382ef efVar) {
        grannyJNI.granny_pwn323_vertex_BoneIndices_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cQ */
    public C2382ef mo12134cQ() {
        long granny_pwn323_vertex_BoneIndices_get = grannyJNI.granny_pwn323_vertex_BoneIndices_get(this.swigCPtr);
        if (granny_pwn323_vertex_BoneIndices_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwn323_vertex_BoneIndices_get, false);
    }

    /* renamed from: h */
    public void mo12138h(C3159oW oWVar) {
        grannyJNI.granny_pwn323_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo12135cR() {
        long granny_pwn323_vertex_Normal_get = grannyJNI.granny_pwn323_vertex_Normal_get(this.swigCPtr);
        if (granny_pwn323_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwn323_vertex_Normal_get, false);
    }

    /* renamed from: l */
    public C1834aa mo12139l(int i) {
        long granny_pwn323_vertex_get = grannyJNI.granny_pwn323_vertex_get(this.swigCPtr, i);
        if (granny_pwn323_vertex_get == 0) {
            return null;
        }
        return new C1834aa(granny_pwn323_vertex_get, false);
    }
}
