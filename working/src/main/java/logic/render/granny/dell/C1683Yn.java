package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.Yn */
/* compiled from: a */
public class C1683Yn {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1683Yn(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1683Yn() {
        this(grannyJNI.new_granny_pwnt3232_vertex(), true);
    }

    /* renamed from: a */
    public static long m11938a(C1683Yn yn) {
        if (yn == null) {
            return 0;
        }
        return yn.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pwnt3232_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo7255b(C3159oW oWVar) {
        grannyJNI.granny_pwnt3232_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo7265s() {
        long granny_pwnt3232_vertex_Position_get = grannyJNI.granny_pwnt3232_vertex_Position_get(this.swigCPtr);
        if (granny_pwnt3232_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwnt3232_vertex_Position_get, false);
    }

    /* renamed from: a */
    public void mo7253a(C2382ef efVar) {
        grannyJNI.granny_pwnt3232_vertex_BoneWeights_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cP */
    public C2382ef mo7256cP() {
        long granny_pwnt3232_vertex_BoneWeights_get = grannyJNI.granny_pwnt3232_vertex_BoneWeights_get(this.swigCPtr);
        if (granny_pwnt3232_vertex_BoneWeights_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwnt3232_vertex_BoneWeights_get, false);
    }

    /* renamed from: b */
    public void mo7254b(C2382ef efVar) {
        grannyJNI.granny_pwnt3232_vertex_BoneIndices_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cQ */
    public C2382ef mo7257cQ() {
        long granny_pwnt3232_vertex_BoneIndices_get = grannyJNI.granny_pwnt3232_vertex_BoneIndices_get(this.swigCPtr);
        if (granny_pwnt3232_vertex_BoneIndices_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwnt3232_vertex_BoneIndices_get, false);
    }

    /* renamed from: h */
    public void mo7261h(C3159oW oWVar) {
        grannyJNI.granny_pwnt3232_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo7258cR() {
        long granny_pwnt3232_vertex_Normal_get = grannyJNI.granny_pwnt3232_vertex_Normal_get(this.swigCPtr);
        if (granny_pwnt3232_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwnt3232_vertex_Normal_get, false);
    }

    /* renamed from: j */
    public void mo7262j(C3159oW oWVar) {
        grannyJNI.granny_pwnt3232_vertex_UV_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: mv */
    public C3159oW mo7263mv() {
        long granny_pwnt3232_vertex_UV_get = grannyJNI.granny_pwnt3232_vertex_UV_get(this.swigCPtr);
        if (granny_pwnt3232_vertex_UV_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwnt3232_vertex_UV_get, false);
    }

    /* renamed from: ph */
    public C1683Yn mo7264ph(int i) {
        long granny_pwnt3232_vertex_get = grannyJNI.granny_pwnt3232_vertex_get(this.swigCPtr, i);
        if (granny_pwnt3232_vertex_get == 0) {
            return null;
        }
        return new C1683Yn(granny_pwnt3232_vertex_get, false);
    }
}
