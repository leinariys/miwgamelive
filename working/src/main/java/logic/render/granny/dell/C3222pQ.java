package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import logic.render.granny.aSM;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.pQ */
/* compiled from: a */
public class C3222pQ {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3222pQ(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3222pQ() {
        this(grannyJNI.new_granny_art_tool_info(), true);
    }

    /* renamed from: b */
    public static long m37096b(C3222pQ pQVar) {
        if (pQVar == null) {
            return 0;
        }
        return pQVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_art_tool_info(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: bd */
    public void mo21150bd(String str) {
        grannyJNI.granny_art_tool_info_FromArtToolName_set(this.swigCPtr, str);
    }

    /* renamed from: Wv */
    public String mo21144Wv() {
        return grannyJNI.granny_art_tool_info_FromArtToolName_get(this.swigCPtr);
    }

    /* renamed from: dM */
    public void mo21152dM(int i) {
        grannyJNI.granny_art_tool_info_ArtToolMajorRevision_set(this.swigCPtr, i);
    }

    /* renamed from: Ww */
    public int mo21145Ww() {
        return grannyJNI.granny_art_tool_info_ArtToolMajorRevision_get(this.swigCPtr);
    }

    /* renamed from: dN */
    public void mo21153dN(int i) {
        grannyJNI.granny_art_tool_info_ArtToolMinorRevision_set(this.swigCPtr, i);
    }

    /* renamed from: Wx */
    public int mo21146Wx() {
        return grannyJNI.granny_art_tool_info_ArtToolMinorRevision_get(this.swigCPtr);
    }

    /* renamed from: cP */
    public void mo21151cP(float f) {
        grannyJNI.granny_art_tool_info_UnitsPerMeter_set(this.swigCPtr, f);
    }

    /* renamed from: Wy */
    public float mo21147Wy() {
        return grannyJNI.granny_art_tool_info_UnitsPerMeter_get(this.swigCPtr);
    }

    /* renamed from: r */
    public void mo21158r(C3159oW oWVar) {
        grannyJNI.granny_art_tool_info_Origin_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: Wz */
    public C3159oW mo21148Wz() {
        long granny_art_tool_info_Origin_get = grannyJNI.granny_art_tool_info_Origin_get(this.swigCPtr);
        if (granny_art_tool_info_Origin_get == 0) {
            return null;
        }
        return new C3159oW(granny_art_tool_info_Origin_get, false);
    }

    /* renamed from: s */
    public void mo21159s(C3159oW oWVar) {
        grannyJNI.granny_art_tool_info_RightVector_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: WA */
    public C3159oW mo21141WA() {
        long granny_art_tool_info_RightVector_get = grannyJNI.granny_art_tool_info_RightVector_get(this.swigCPtr);
        if (granny_art_tool_info_RightVector_get == 0) {
            return null;
        }
        return new C3159oW(granny_art_tool_info_RightVector_get, false);
    }

    /* renamed from: t */
    public void mo21160t(C3159oW oWVar) {
        grannyJNI.granny_art_tool_info_UpVector_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: WB */
    public C3159oW mo21142WB() {
        long granny_art_tool_info_UpVector_get = grannyJNI.granny_art_tool_info_UpVector_get(this.swigCPtr);
        if (granny_art_tool_info_UpVector_get == 0) {
            return null;
        }
        return new C3159oW(granny_art_tool_info_UpVector_get, false);
    }

    /* renamed from: u */
    public void mo21161u(C3159oW oWVar) {
        grannyJNI.granny_art_tool_info_BackVector_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: WC */
    public C3159oW mo21143WC() {
        long granny_art_tool_info_BackVector_get = grannyJNI.granny_art_tool_info_BackVector_get(this.swigCPtr);
        if (granny_art_tool_info_BackVector_get == 0) {
            return null;
        }
        return new C3159oW(granny_art_tool_info_BackVector_get, false);
    }

    /* renamed from: a */
    public void mo21149a(aSM asm) {
        grannyJNI.granny_art_tool_info_ExtendedData_set(this.swigCPtr, aSM.m18172c(asm));
    }

    /* renamed from: kM */
    public aSM mo21157kM() {
        long granny_art_tool_info_ExtendedData_get = grannyJNI.granny_art_tool_info_ExtendedData_get(this.swigCPtr);
        if (granny_art_tool_info_ExtendedData_get == 0) {
            return null;
        }
        return new aSM(granny_art_tool_info_ExtendedData_get, false);
    }

    /* renamed from: dO */
    public C3222pQ mo21154dO(int i) {
        long granny_art_tool_info_get = grannyJNI.granny_art_tool_info_get(this.swigCPtr, i);
        if (granny_art_tool_info_get == 0) {
            return null;
        }
        return new C3222pQ(granny_art_tool_info_get, false);
    }
}
