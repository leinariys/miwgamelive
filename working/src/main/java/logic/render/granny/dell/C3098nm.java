package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.nm */
/* compiled from: a */
public class C3098nm {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3098nm(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3098nm() {
        this(grannyJNI.new_granny_curve_data_d3i1_k32f_c32f(), true);
    }

    /* renamed from: a */
    public static long m36489a(C3098nm nmVar) {
        if (nmVar == null) {
            return 0;
        }
        return nmVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve_data_d3i1_k32f_c32f(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo20848a(C6341alB alb) {
        grannyJNI.granny_curve_data_d3i1_k32f_c32f_CurveDataHeader_set(this.swigCPtr, C6341alB.m23687b(alb));
    }

    /* renamed from: Qa */
    public C6341alB mo20842Qa() {
        long granny_curve_data_d3i1_k32f_c32f_CurveDataHeader_get = grannyJNI.granny_curve_data_d3i1_k32f_c32f_CurveDataHeader_get(this.swigCPtr);
        if (granny_curve_data_d3i1_k32f_c32f_CurveDataHeader_get == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_d3i1_k32f_c32f_CurveDataHeader_get, false);
    }

    /* renamed from: e */
    public void mo20852e(short s) {
        grannyJNI.granny_curve_data_d3i1_k32f_c32f_Padding_set(this.swigCPtr, s);
    }

    /* renamed from: Qb */
    public short mo20843Qb() {
        return grannyJNI.granny_curve_data_d3i1_k32f_c32f_Padding_get(this.swigCPtr);
    }

    /* renamed from: n */
    public void mo20854n(C3159oW oWVar) {
        grannyJNI.granny_curve_data_d3i1_k32f_c32f_ControlScales_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: Qc */
    public C3159oW mo20844Qc() {
        long granny_curve_data_d3i1_k32f_c32f_ControlScales_get = grannyJNI.granny_curve_data_d3i1_k32f_c32f_ControlScales_get(this.swigCPtr);
        if (granny_curve_data_d3i1_k32f_c32f_ControlScales_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_d3i1_k32f_c32f_ControlScales_get, false);
    }

    /* renamed from: o */
    public void mo20855o(C3159oW oWVar) {
        grannyJNI.granny_curve_data_d3i1_k32f_c32f_ControlOffsets_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: Qd */
    public C3159oW mo20845Qd() {
        long granny_curve_data_d3i1_k32f_c32f_ControlOffsets_get = grannyJNI.granny_curve_data_d3i1_k32f_c32f_ControlOffsets_get(this.swigCPtr);
        if (granny_curve_data_d3i1_k32f_c32f_ControlOffsets_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_d3i1_k32f_c32f_ControlOffsets_get, false);
    }

    /* renamed from: di */
    public void mo20850di(int i) {
        grannyJNI.granny_curve_data_d3i1_k32f_c32f_KnotControlCount_set(this.swigCPtr, i);
    }

    /* renamed from: Qe */
    public int mo20846Qe() {
        return grannyJNI.granny_curve_data_d3i1_k32f_c32f_KnotControlCount_get(this.swigCPtr);
    }

    /* renamed from: p */
    public void mo20856p(C3159oW oWVar) {
        grannyJNI.granny_curve_data_d3i1_k32f_c32f_KnotsControls_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: Qf */
    public C3159oW mo20847Qf() {
        long granny_curve_data_d3i1_k32f_c32f_KnotsControls_get = grannyJNI.granny_curve_data_d3i1_k32f_c32f_KnotsControls_get(this.swigCPtr);
        if (granny_curve_data_d3i1_k32f_c32f_KnotsControls_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_d3i1_k32f_c32f_KnotsControls_get, false);
    }

    /* renamed from: dj */
    public C3098nm mo20851dj(int i) {
        long granny_curve_data_d3i1_k32f_c32f_get = grannyJNI.granny_curve_data_d3i1_k32f_c32f_get(this.swigCPtr, i);
        if (granny_curve_data_d3i1_k32f_c32f_get == 0) {
            return null;
        }
        return new C3098nm(granny_curve_data_d3i1_k32f_c32f_get, false);
    }
}
