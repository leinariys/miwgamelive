package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.atK  reason: case insensitive filesystem */
/* compiled from: a */
public class C6766atK {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6766atK(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6766atK() {
        this(grannyJNI.new_granny_vector_track(), true);
    }

    /* renamed from: b */
    public static long m25835b(C6766atK atk) {
        if (atk == null) {
            return 0;
        }
        return atk.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_vector_track(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getName() {
        return grannyJNI.granny_vector_track_Name_get(this.swigCPtr);
    }

    public void setName(String str) {
        grannyJNI.granny_vector_track_Name_set(this.swigCPtr, str);
    }

    /* renamed from: iU */
    public void mo16065iU(long j) {
        grannyJNI.granny_vector_track_TrackKey_set(this.swigCPtr, j);
    }

    public long cwq() {
        return grannyJNI.granny_vector_track_TrackKey_get(this.swigCPtr);
    }

    /* renamed from: uB */
    public void mo16068uB(int i) {
        grannyJNI.granny_vector_track_Dimension_set(this.swigCPtr, i);
    }

    public int getDimension() {
        return grannyJNI.granny_vector_track_Dimension_get(this.swigCPtr);
    }

    /* renamed from: u */
    public void mo16067u(C3914wi wiVar) {
        grannyJNI.granny_vector_track_ValueCurve_set(this.swigCPtr, C3914wi.m40719a(wiVar));
    }

    public C3914wi cwr() {
        long granny_vector_track_ValueCurve_get = grannyJNI.granny_vector_track_ValueCurve_get(this.swigCPtr);
        if (granny_vector_track_ValueCurve_get == 0) {
            return null;
        }
        return new C3914wi(granny_vector_track_ValueCurve_get, false);
    }

    /* renamed from: uC */
    public C6766atK mo16069uC(int i) {
        long granny_vector_track_get = grannyJNI.granny_vector_track_get(this.swigCPtr, i);
        if (granny_vector_track_get == 0) {
            return null;
        }
        return new C6766atK(granny_vector_track_get, false);
    }
}
