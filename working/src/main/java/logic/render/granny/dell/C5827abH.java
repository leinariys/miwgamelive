package logic.render.granny.dell;

import logic.render.granny.C6135ahD;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.abH  reason: case insensitive filesystem */
/* compiled from: a */
public class C5827abH {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5827abH(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5827abH() {
        this(grannyJNI.new_granny_grn_file_header(), true);
    }

    /* renamed from: b */
    public static long m19928b(C5827abH abh) {
        if (abh == null) {
            return 0;
        }
        return abh.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_grn_file_header(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: eU */
    public void mo12446eU(long j) {
        grannyJNI.granny_grn_file_header_Version_set(this.swigCPtr, j);
    }

    public long aBt() {
        return grannyJNI.granny_grn_file_header_Version_get(this.swigCPtr);
    }

    /* renamed from: gp */
    public void mo12449gp(long j) {
        grannyJNI.granny_grn_file_header_TotalSize_set(this.swigCPtr, j);
    }

    public long bNP() {
        return grannyJNI.granny_grn_file_header_TotalSize_get(this.swigCPtr);
    }

    /* renamed from: gq */
    public void mo12450gq(long j) {
        grannyJNI.granny_grn_file_header_CRC_set(this.swigCPtr, j);
    }

    public long bNQ() {
        return grannyJNI.granny_grn_file_header_CRC_get(this.swigCPtr);
    }

    /* renamed from: gr */
    public void mo12451gr(long j) {
        grannyJNI.granny_grn_file_header_SectionArrayOffset_set(this.swigCPtr, j);
    }

    public long bNR() {
        return grannyJNI.granny_grn_file_header_SectionArrayOffset_get(this.swigCPtr);
    }

    /* renamed from: gs */
    public void mo12452gs(long j) {
        grannyJNI.granny_grn_file_header_SectionArrayCount_set(this.swigCPtr, j);
    }

    public long bNS() {
        return grannyJNI.granny_grn_file_header_SectionArrayCount_get(this.swigCPtr);
    }

    /* renamed from: d */
    public void mo12442d(C0031AQ aq) {
        grannyJNI.granny_grn_file_header_RootObjectTypeDefinition_set(this.swigCPtr, C0031AQ.m385a(aq));
    }

    public C0031AQ bNT() {
        long granny_grn_file_header_RootObjectTypeDefinition_get = grannyJNI.granny_grn_file_header_RootObjectTypeDefinition_get(this.swigCPtr);
        if (granny_grn_file_header_RootObjectTypeDefinition_get == 0) {
            return null;
        }
        return new C0031AQ(granny_grn_file_header_RootObjectTypeDefinition_get, false);
    }

    /* renamed from: e */
    public void mo12444e(C0031AQ aq) {
        grannyJNI.granny_grn_file_header_RootObject_set(this.swigCPtr, C0031AQ.m385a(aq));
    }

    public C0031AQ bNU() {
        long granny_grn_file_header_RootObject_get = grannyJNI.granny_grn_file_header_RootObject_get(this.swigCPtr);
        if (granny_grn_file_header_RootObject_get == 0) {
            return null;
        }
        return new C0031AQ(granny_grn_file_header_RootObject_get, false);
    }

    /* renamed from: gt */
    public void mo12453gt(long j) {
        grannyJNI.granny_grn_file_header_TypeTag_set(this.swigCPtr, j);
    }

    public long bNV() {
        return grannyJNI.granny_grn_file_header_TypeTag_get(this.swigCPtr);
    }

    /* renamed from: e */
    public void mo12445e(C6135ahD ahd) {
        grannyJNI.granny_grn_file_header_ExtraTags_set(this.swigCPtr, C6135ahD.m22189i(ahd));
    }

    public C6135ahD bNW() {
        long granny_grn_file_header_ExtraTags_get = grannyJNI.granny_grn_file_header_ExtraTags_get(this.swigCPtr);
        if (granny_grn_file_header_ExtraTags_get == 0) {
            return null;
        }
        return new C6135ahD(granny_grn_file_header_ExtraTags_get, false);
    }

    /* renamed from: gu */
    public void mo12454gu(long j) {
        grannyJNI.granny_grn_file_header_StringDatabaseCRC_set(this.swigCPtr, j);
    }

    public long bNX() {
        return grannyJNI.granny_grn_file_header_StringDatabaseCRC_get(this.swigCPtr);
    }

    /* renamed from: f */
    public void mo12447f(C6135ahD ahd) {
        grannyJNI.granny_grn_file_header_ReservedUnused_set(this.swigCPtr, C6135ahD.m22189i(ahd));
    }

    public C6135ahD bNY() {
        long granny_grn_file_header_ReservedUnused_get = grannyJNI.granny_grn_file_header_ReservedUnused_get(this.swigCPtr);
        if (granny_grn_file_header_ReservedUnused_get == 0) {
            return null;
        }
        return new C6135ahD(granny_grn_file_header_ReservedUnused_get, false);
    }

    /* renamed from: pN */
    public C5827abH mo12455pN(int i) {
        long granny_grn_file_header_get = grannyJNI.granny_grn_file_header_get(this.swigCPtr, i);
        if (granny_grn_file_header_get == 0) {
            return null;
        }
        return new C5827abH(granny_grn_file_header_get, false);
    }
}
