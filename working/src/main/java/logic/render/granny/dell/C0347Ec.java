package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.Ec */
/* compiled from: a */
public class C0347Ec {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0347Ec(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0347Ec() {
        this(grannyJNI.new_granny_grn_mixed_marshalling_fixup(), true);
    }

    /* renamed from: a */
    public static long m2989a(C0347Ec ec) {
        if (ec == null) {
            return 0;
        }
        return ec.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_grn_mixed_marshalling_fixup(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public long getCount() {
        return grannyJNI.granny_grn_mixed_marshalling_fixup_Count_get(this.swigCPtr);
    }

    public void setCount(long j) {
        grannyJNI.granny_grn_mixed_marshalling_fixup_Count_set(this.swigCPtr, j);
    }

    public long getOffset() {
        return grannyJNI.granny_grn_mixed_marshalling_fixup_Offset_get(this.swigCPtr);
    }

    public void setOffset(long j) {
        grannyJNI.granny_grn_mixed_marshalling_fixup_Offset_set(this.swigCPtr, j);
    }

    /* renamed from: b */
    public void mo1938b(C0031AQ aq) {
        grannyJNI.granny_grn_mixed_marshalling_fixup_Type_set(this.swigCPtr, C0031AQ.m385a(aq));
    }

    public C0031AQ aNA() {
        long granny_grn_mixed_marshalling_fixup_Type_get = grannyJNI.granny_grn_mixed_marshalling_fixup_Type_get(this.swigCPtr);
        if (granny_grn_mixed_marshalling_fixup_Type_get == 0) {
            return null;
        }
        return new C0031AQ(granny_grn_mixed_marshalling_fixup_Type_get, false);
    }

    /* renamed from: gV */
    public C0347Ec mo1941gV(int i) {
        long granny_grn_mixed_marshalling_fixup_get = grannyJNI.granny_grn_mixed_marshalling_fixup_get(this.swigCPtr, i);
        if (granny_grn_mixed_marshalling_fixup_get == 0) {
            return null;
        }
        return new C0347Ec(granny_grn_mixed_marshalling_fixup_get, false);
    }
}
