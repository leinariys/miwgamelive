package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.jA */
/* compiled from: a */
public class C2720jA {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2720jA(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C2720jA() {
        this(grannyJNI.new_granny_periodic_loop(), true);
    }

    /* renamed from: a */
    public static long m33633a(C2720jA jAVar) {
        if (jAVar == null) {
            return 0;
        }
        return jAVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_periodic_loop(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public float getRadius() {
        return grannyJNI.granny_periodic_loop_Radius_get(this.swigCPtr);
    }

    public void setRadius(float f) {
        grannyJNI.granny_periodic_loop_Radius_set(this.swigCPtr, f);
    }

    /* renamed from: aN */
    public void mo19821aN(float f) {
        grannyJNI.granny_periodic_loop_dAngle_set(this.swigCPtr, f);
    }

    /* renamed from: Ge */
    public float mo19816Ge() {
        return grannyJNI.granny_periodic_loop_dAngle_get(this.swigCPtr);
    }

    /* renamed from: aO */
    public void mo19822aO(float f) {
        grannyJNI.granny_periodic_loop_dZ_set(this.swigCPtr, f);
    }

    /* renamed from: Gf */
    public float mo19817Gf() {
        return grannyJNI.granny_periodic_loop_dZ_get(this.swigCPtr);
    }

    /* renamed from: k */
    public void mo19827k(C3159oW oWVar) {
        grannyJNI.granny_periodic_loop_BasisX_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: Gg */
    public C3159oW mo19818Gg() {
        long granny_periodic_loop_BasisX_get = grannyJNI.granny_periodic_loop_BasisX_get(this.swigCPtr);
        if (granny_periodic_loop_BasisX_get == 0) {
            return null;
        }
        return new C3159oW(granny_periodic_loop_BasisX_get, false);
    }

    /* renamed from: l */
    public void mo19828l(C3159oW oWVar) {
        grannyJNI.granny_periodic_loop_BasisY_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: Gh */
    public C3159oW mo19819Gh() {
        long granny_periodic_loop_BasisY_get = grannyJNI.granny_periodic_loop_BasisY_get(this.swigCPtr);
        if (granny_periodic_loop_BasisY_get == 0) {
            return null;
        }
        return new C3159oW(granny_periodic_loop_BasisY_get, false);
    }

    /* renamed from: m */
    public void mo19829m(C3159oW oWVar) {
        grannyJNI.granny_periodic_loop_Axis_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: Gi */
    public C3159oW mo19820Gi() {
        long granny_periodic_loop_Axis_get = grannyJNI.granny_periodic_loop_Axis_get(this.swigCPtr);
        if (granny_periodic_loop_Axis_get == 0) {
            return null;
        }
        return new C3159oW(granny_periodic_loop_Axis_get, false);
    }

    /* renamed from: ch */
    public C2720jA mo19823ch(int i) {
        long granny_periodic_loop_get = grannyJNI.granny_periodic_loop_get(this.swigCPtr, i);
        if (granny_periodic_loop_get == 0) {
            return null;
        }
        return new C2720jA(granny_periodic_loop_get, false);
    }
}
