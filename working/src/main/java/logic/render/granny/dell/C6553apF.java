package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.apF  reason: case insensitive filesystem */
/* compiled from: a */
public class C6553apF {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6553apF(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6553apF() {
        this(grannyJNI.new_granny_curve_data_d9i3_k8u_c8u(), true);
    }

    /* renamed from: a */
    public static long m24792a(C6553apF apf) {
        if (apf == null) {
            return 0;
        }
        return apf.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve_data_d9i3_k8u_c8u(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo15358a(C6341alB alb) {
        grannyJNI.granny_curve_data_d9i3_k8u_c8u_CurveDataHeader_set(this.swigCPtr, C6341alB.m23687b(alb));
    }

    /* renamed from: Qa */
    public C6341alB mo15353Qa() {
        long granny_curve_data_d9i3_k8u_c8u_CurveDataHeader_get = grannyJNI.granny_curve_data_d9i3_k8u_c8u_CurveDataHeader_get(this.swigCPtr);
        if (granny_curve_data_d9i3_k8u_c8u_CurveDataHeader_get == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_d9i3_k8u_c8u_CurveDataHeader_get, false);
    }

    /* renamed from: dT */
    public void mo15361dT(int i) {
        grannyJNI.granny_curve_data_d9i3_k8u_c8u_OneOverKnotScaleTrunc_set(this.swigCPtr, i);
    }

    /* renamed from: WK */
    public int mo15357WK() {
        return grannyJNI.granny_curve_data_d9i3_k8u_c8u_OneOverKnotScaleTrunc_get(this.swigCPtr);
    }

    /* renamed from: n */
    public void mo15365n(C3159oW oWVar) {
        grannyJNI.granny_curve_data_d9i3_k8u_c8u_ControlScales_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: Qc */
    public C3159oW mo15354Qc() {
        long granny_curve_data_d9i3_k8u_c8u_ControlScales_get = grannyJNI.granny_curve_data_d9i3_k8u_c8u_ControlScales_get(this.swigCPtr);
        if (granny_curve_data_d9i3_k8u_c8u_ControlScales_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_d9i3_k8u_c8u_ControlScales_get, false);
    }

    /* renamed from: o */
    public void mo15366o(C3159oW oWVar) {
        grannyJNI.granny_curve_data_d9i3_k8u_c8u_ControlOffsets_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: Qd */
    public C3159oW mo15355Qd() {
        long granny_curve_data_d9i3_k8u_c8u_ControlOffsets_get = grannyJNI.granny_curve_data_d9i3_k8u_c8u_ControlOffsets_get(this.swigCPtr);
        if (granny_curve_data_d9i3_k8u_c8u_ControlOffsets_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_d9i3_k8u_c8u_ControlOffsets_get, false);
    }

    /* renamed from: di */
    public void mo15363di(int i) {
        grannyJNI.granny_curve_data_d9i3_k8u_c8u_KnotControlCount_set(this.swigCPtr, i);
    }

    /* renamed from: Qe */
    public int mo15356Qe() {
        return grannyJNI.granny_curve_data_d9i3_k8u_c8u_KnotControlCount_get(this.swigCPtr);
    }

    /* renamed from: d */
    public void mo15360d(C2382ef efVar) {
        grannyJNI.granny_curve_data_d9i3_k8u_c8u_KnotsControls_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    public C2382ef ano() {
        long granny_curve_data_d9i3_k8u_c8u_KnotsControls_get = grannyJNI.granny_curve_data_d9i3_k8u_c8u_KnotsControls_get(this.swigCPtr);
        if (granny_curve_data_d9i3_k8u_c8u_KnotsControls_get == 0) {
            return null;
        }
        return new C2382ef(granny_curve_data_d9i3_k8u_c8u_KnotsControls_get, false);
    }

    /* renamed from: tx */
    public C6553apF mo15367tx(int i) {
        long granny_curve_data_d9i3_k8u_c8u_get = grannyJNI.granny_curve_data_d9i3_k8u_c8u_get(this.swigCPtr, i);
        if (granny_curve_data_d9i3_k8u_c8u_get == 0) {
            return null;
        }
        return new C6553apF(granny_curve_data_d9i3_k8u_c8u_get, false);
    }
}
