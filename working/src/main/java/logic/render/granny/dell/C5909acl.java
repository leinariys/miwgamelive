package logic.render.granny.dell;

/* renamed from: a.acl  reason: case insensitive filesystem */
/* compiled from: a */
public class C5909acl {
    private long swigCPtr;

    public C5909acl(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5909acl() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m20580b(C5909acl acl) {
        if (acl == null) {
            return 0;
        }
        return acl.swigCPtr;
    }
}
