package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aba  reason: case insensitive filesystem */
/* compiled from: a */
public class C5846aba {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5846aba(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5846aba() {
        this(grannyJNI.new_granny_pngb3333_vertex(), true);
    }

    /* renamed from: a */
    public static long m20017a(C5846aba aba) {
        if (aba == null) {
            return 0;
        }
        return aba.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pngb3333_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo12481b(C3159oW oWVar) {
        grannyJNI.granny_pngb3333_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo12489s() {
        long granny_pngb3333_vertex_Position_get = grannyJNI.granny_pngb3333_vertex_Position_get(this.swigCPtr);
        if (granny_pngb3333_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngb3333_vertex_Position_get, false);
    }

    /* renamed from: h */
    public void mo12486h(C3159oW oWVar) {
        grannyJNI.granny_pngb3333_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo12482cR() {
        long granny_pngb3333_vertex_Normal_get = grannyJNI.granny_pngb3333_vertex_Normal_get(this.swigCPtr);
        if (granny_pngb3333_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngb3333_vertex_Normal_get, false);
    }

    /* renamed from: i */
    public void mo12487i(C3159oW oWVar) {
        grannyJNI.granny_pngb3333_vertex_Tangent_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: gD */
    public C3159oW mo12485gD() {
        long granny_pngb3333_vertex_Tangent_get = grannyJNI.granny_pngb3333_vertex_Tangent_get(this.swigCPtr);
        if (granny_pngb3333_vertex_Tangent_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngb3333_vertex_Tangent_get, false);
    }

    /* renamed from: w */
    public void mo12490w(C3159oW oWVar) {
        grannyJNI.granny_pngb3333_vertex_Binormal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW amC() {
        long granny_pngb3333_vertex_Binormal_get = grannyJNI.granny_pngb3333_vertex_Binormal_get(this.swigCPtr);
        if (granny_pngb3333_vertex_Binormal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngb3333_vertex_Binormal_get, false);
    }

    /* renamed from: pB */
    public C5846aba mo12488pB(int i) {
        long granny_pngb3333_vertex_get = grannyJNI.granny_pngb3333_vertex_get(this.swigCPtr, i);
        if (granny_pngb3333_vertex_get == 0) {
            return null;
        }
        return new C5846aba(granny_pngb3333_vertex_get, false);
    }
}
