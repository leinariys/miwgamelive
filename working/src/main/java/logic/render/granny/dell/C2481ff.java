package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.ff */
/* compiled from: a */
public class C2481ff {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2481ff(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C2481ff() {
        this(grannyJNI.new_granny_file_location(), true);
    }

    /* renamed from: a */
    public static long m31245a(C2481ff ffVar) {
        if (ffVar == null) {
            return 0;
        }
        return ffVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_file_location(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: w */
    public void mo18776w(long j) {
        grannyJNI.granny_file_location_SectionIndex_set(this.swigCPtr, j);
    }

    /* renamed from: pb */
    public long mo18773pb() {
        return grannyJNI.granny_file_location_SectionIndex_get(this.swigCPtr);
    }

    /* renamed from: x */
    public void mo18777x(long j) {
        grannyJNI.granny_file_location_BufferIndex_set(this.swigCPtr, j);
    }

    /* renamed from: pc */
    public long mo18774pc() {
        return grannyJNI.granny_file_location_BufferIndex_get(this.swigCPtr);
    }

    public long getOffset() {
        return grannyJNI.granny_file_location_Offset_get(this.swigCPtr);
    }

    public void setOffset(long j) {
        grannyJNI.granny_file_location_Offset_set(this.swigCPtr, j);
    }

    /* renamed from: aI */
    public C2481ff mo18769aI(int i) {
        long granny_file_location_get = grannyJNI.granny_file_location_get(this.swigCPtr, i);
        if (granny_file_location_get == 0) {
            return null;
        }
        return new C2481ff(granny_file_location_get, false);
    }
}
