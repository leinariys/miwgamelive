package logic.render.granny.dell;

import logic.render.granny.C2062bl;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.QI */
/* compiled from: a */
public class C1105QI {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1105QI(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1105QI() {
        this(grannyJNI.new_granny_curve_data_d3_k16u_c16u(), true);
    }

    /* renamed from: a */
    public static long m8741a(C1105QI qi) {
        if (qi == null) {
            return 0;
        }
        return qi.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve_data_d3_k16u_c16u(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo4951a(C6341alB alb) {
        grannyJNI.granny_curve_data_d3_k16u_c16u_CurveDataHeader_set(this.swigCPtr, C6341alB.m23687b(alb));
    }

    /* renamed from: Qa */
    public C6341alB mo4945Qa() {
        long granny_curve_data_d3_k16u_c16u_CurveDataHeader_get = grannyJNI.granny_curve_data_d3_k16u_c16u_CurveDataHeader_get(this.swigCPtr);
        if (granny_curve_data_d3_k16u_c16u_CurveDataHeader_get == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_d3_k16u_c16u_CurveDataHeader_get, false);
    }

    /* renamed from: dT */
    public void mo4953dT(int i) {
        grannyJNI.granny_curve_data_d3_k16u_c16u_OneOverKnotScaleTrunc_set(this.swigCPtr, i);
    }

    /* renamed from: WK */
    public int mo4950WK() {
        return grannyJNI.granny_curve_data_d3_k16u_c16u_OneOverKnotScaleTrunc_get(this.swigCPtr);
    }

    /* renamed from: n */
    public void mo4958n(C3159oW oWVar) {
        grannyJNI.granny_curve_data_d3_k16u_c16u_ControlScales_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: Qc */
    public C3159oW mo4946Qc() {
        long granny_curve_data_d3_k16u_c16u_ControlScales_get = grannyJNI.granny_curve_data_d3_k16u_c16u_ControlScales_get(this.swigCPtr);
        if (granny_curve_data_d3_k16u_c16u_ControlScales_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_d3_k16u_c16u_ControlScales_get, false);
    }

    /* renamed from: o */
    public void mo4959o(C3159oW oWVar) {
        grannyJNI.granny_curve_data_d3_k16u_c16u_ControlOffsets_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: Qd */
    public C3159oW mo4947Qd() {
        long granny_curve_data_d3_k16u_c16u_ControlOffsets_get = grannyJNI.granny_curve_data_d3_k16u_c16u_ControlOffsets_get(this.swigCPtr);
        if (granny_curve_data_d3_k16u_c16u_ControlOffsets_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_d3_k16u_c16u_ControlOffsets_get, false);
    }

    /* renamed from: di */
    public void mo4955di(int i) {
        grannyJNI.granny_curve_data_d3_k16u_c16u_KnotControlCount_set(this.swigCPtr, i);
    }

    /* renamed from: Qe */
    public int mo4948Qe() {
        return grannyJNI.granny_curve_data_d3_k16u_c16u_KnotControlCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo4952b(C2062bl blVar) {
        grannyJNI.granny_curve_data_d3_k16u_c16u_KnotsControls_set(this.swigCPtr, C2062bl.m27965a(blVar));
    }

    /* renamed from: Ua */
    public C2062bl mo4949Ua() {
        long granny_curve_data_d3_k16u_c16u_KnotsControls_get = grannyJNI.granny_curve_data_d3_k16u_c16u_KnotsControls_get(this.swigCPtr);
        if (granny_curve_data_d3_k16u_c16u_KnotsControls_get == 0) {
            return null;
        }
        return new C2062bl(granny_curve_data_d3_k16u_c16u_KnotsControls_get, false);
    }

    /* renamed from: mW */
    public C1105QI mo4957mW(int i) {
        long granny_curve_data_d3_k16u_c16u_get = grannyJNI.granny_curve_data_d3_k16u_c16u_get(this.swigCPtr, i);
        if (granny_curve_data_d3_k16u_c16u_get == 0) {
            return null;
        }
        return new C1105QI(granny_curve_data_d3_k16u_c16u_get, false);
    }
}
