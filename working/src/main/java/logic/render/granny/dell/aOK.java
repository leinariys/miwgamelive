package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aOK */
/* compiled from: a */
public class aOK {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aOK(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aOK() {
        this(grannyJNI.new_granny_pngbt33333_vertex(), true);
    }

    /* renamed from: a */
    public static long m16808a(aOK aok) {
        if (aok == null) {
            return 0;
        }
        return aok.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pngbt33333_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo10549b(C3159oW oWVar) {
        grannyJNI.granny_pngbt33333_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo10556s() {
        long granny_pngbt33333_vertex_Position_get = grannyJNI.granny_pngbt33333_vertex_Position_get(this.swigCPtr);
        if (granny_pngbt33333_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngbt33333_vertex_Position_get, false);
    }

    /* renamed from: h */
    public void mo10554h(C3159oW oWVar) {
        grannyJNI.granny_pngbt33333_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo10550cR() {
        long granny_pngbt33333_vertex_Normal_get = grannyJNI.granny_pngbt33333_vertex_Normal_get(this.swigCPtr);
        if (granny_pngbt33333_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngbt33333_vertex_Normal_get, false);
    }

    /* renamed from: i */
    public void mo10555i(C3159oW oWVar) {
        grannyJNI.granny_pngbt33333_vertex_Tangent_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: gD */
    public C3159oW mo10553gD() {
        long granny_pngbt33333_vertex_Tangent_get = grannyJNI.granny_pngbt33333_vertex_Tangent_get(this.swigCPtr);
        if (granny_pngbt33333_vertex_Tangent_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngbt33333_vertex_Tangent_get, false);
    }

    /* renamed from: w */
    public void mo10558w(C3159oW oWVar) {
        grannyJNI.granny_pngbt33333_vertex_Binormal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW amC() {
        long granny_pngbt33333_vertex_Binormal_get = grannyJNI.granny_pngbt33333_vertex_Binormal_get(this.swigCPtr);
        if (granny_pngbt33333_vertex_Binormal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngbt33333_vertex_Binormal_get, false);
    }

    /* renamed from: v */
    public void mo10557v(C3159oW oWVar) {
        grannyJNI.granny_pngbt33333_vertex_UVW_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW alh() {
        long granny_pngbt33333_vertex_UVW_get = grannyJNI.granny_pngbt33333_vertex_UVW_get(this.swigCPtr);
        if (granny_pngbt33333_vertex_UVW_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngbt33333_vertex_UVW_get, false);
    }

    /* renamed from: zS */
    public aOK mo10559zS(int i) {
        long granny_pngbt33333_vertex_get = grannyJNI.granny_pngbt33333_vertex_get(this.swigCPtr, i);
        if (granny_pngbt33333_vertex_get == 0) {
            return null;
        }
        return new aOK(granny_pngbt33333_vertex_get, false);
    }
}
