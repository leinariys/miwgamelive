package logic.render.granny.dell;

/* renamed from: a.agi  reason: case insensitive filesystem */
/* compiled from: a */
public class C6114agi {
    private long swigCPtr;

    public C6114agi(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6114agi() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m22105b(C6114agi agi) {
        if (agi == null) {
            return 0;
        }
        return agi.swigCPtr;
    }
}
