package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aUp  reason: case insensitive filesystem */
/* compiled from: a */
public class C5731aUp {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5731aUp(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5731aUp() {
        this(grannyJNI.new_granny_file_writer(), true);
    }

    /* renamed from: b */
    public static long m18737b(C5731aUp aup) {
        if (aup == null) {
            return 0;
        }
        return aup.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_file_writer(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getSourceFileName() {
        return grannyJNI.granny_file_writer_SourceFileName_get(this.swigCPtr);
    }

    public void setSourceFileName(String str) {
        grannyJNI.granny_file_writer_SourceFileName_set(this.swigCPtr, str);
    }

    /* renamed from: O */
    public void mo11697O(int i) {
        grannyJNI.granny_file_writer_SourceLineNumber_set(this.swigCPtr, i);
    }

    /* renamed from: fV */
    public int mo11711fV() {
        return grannyJNI.granny_file_writer_SourceLineNumber_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo11702b(C3386qx qxVar) {
        grannyJNI.granny_file_writer_DeleteFileWriterCallback_set(this.swigCPtr, C3386qx.m37893a(qxVar));
    }

    public C3386qx dAc() {
        long granny_file_writer_DeleteFileWriterCallback_get = grannyJNI.granny_file_writer_DeleteFileWriterCallback_get(this.swigCPtr);
        if (granny_file_writer_DeleteFileWriterCallback_get == 0) {
            return null;
        }
        return new C3386qx(granny_file_writer_DeleteFileWriterCallback_get, false);
    }

    /* renamed from: b */
    public void mo11698b(C6358alS als) {
        grannyJNI.granny_file_writer_SeekWriterCallback_set(this.swigCPtr, C6358alS.m23715a(als));
    }

    public C6358alS dAd() {
        long granny_file_writer_SeekWriterCallback_get = grannyJNI.granny_file_writer_SeekWriterCallback_get(this.swigCPtr);
        if (granny_file_writer_SeekWriterCallback_get == 0) {
            return null;
        }
        return new C6358alS(granny_file_writer_SeekWriterCallback_get, false);
    }

    /* renamed from: b */
    public void mo11699b(C6487anr anr) {
        grannyJNI.granny_file_writer_WriteCallback_set(this.swigCPtr, C6487anr.m24338a(anr));
    }

    public C6487anr dAe() {
        long granny_file_writer_WriteCallback_get = grannyJNI.granny_file_writer_WriteCallback_get(this.swigCPtr);
        if (granny_file_writer_WriteCallback_get == 0) {
            return null;
        }
        return new C6487anr(granny_file_writer_WriteCallback_get, false);
    }

    /* renamed from: b */
    public void mo11700b(C6586apm apm) {
        grannyJNI.granny_file_writer_BeginCRCCallback_set(this.swigCPtr, C6586apm.m25008a(apm));
    }

    public C6586apm dAf() {
        long granny_file_writer_BeginCRCCallback_get = grannyJNI.granny_file_writer_BeginCRCCallback_get(this.swigCPtr);
        if (granny_file_writer_BeginCRCCallback_get == 0) {
            return null;
        }
        return new C6586apm(granny_file_writer_BeginCRCCallback_get, false);
    }

    /* renamed from: b */
    public void mo11701b(C2510gA gAVar) {
        grannyJNI.granny_file_writer_EndCRCCallback_set(this.swigCPtr, C2510gA.m31649a(gAVar));
    }

    public C2510gA dAg() {
        long granny_file_writer_EndCRCCallback_get = grannyJNI.granny_file_writer_EndCRCCallback_get(this.swigCPtr);
        if (granny_file_writer_EndCRCCallback_get == 0) {
            return null;
        }
        return new C2510gA(granny_file_writer_EndCRCCallback_get, false);
    }

    /* renamed from: ku */
    public void mo11715ku(boolean z) {
        grannyJNI.granny_file_writer_CRCing_set(this.swigCPtr, z);
    }

    public boolean dAh() {
        return grannyJNI.granny_file_writer_CRCing_get(this.swigCPtr);
    }

    /* renamed from: gq */
    public void mo11714gq(long j) {
        grannyJNI.granny_file_writer_CRC_set(this.swigCPtr, j);
    }

    public long bNQ() {
        return grannyJNI.granny_file_writer_CRC_get(this.swigCPtr);
    }

    /* renamed from: AW */
    public C5731aUp mo11696AW(int i) {
        long granny_file_writer_get = grannyJNI.granny_file_writer_get(this.swigCPtr, i);
        if (granny_file_writer_get == 0) {
            return null;
        }
        return new C5731aUp(granny_file_writer_get, false);
    }
}
