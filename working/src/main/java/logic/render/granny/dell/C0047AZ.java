package logic.render.granny.dell;

import logic.render.granny.C2062bl;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.AZ */
/* compiled from: a */
public class C0047AZ {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0047AZ(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0047AZ() {
        this(grannyJNI.new_granny_curve_data_da_k16u_c16u(), true);
    }

    /* renamed from: a */
    public static long m424a(C0047AZ az) {
        if (az == null) {
            return 0;
        }
        return az.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve_data_da_k16u_c16u(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo327a(C6341alB alb) {
        grannyJNI.granny_curve_data_da_k16u_c16u_CurveDataHeader_set(this.swigCPtr, C6341alB.m23687b(alb));
    }

    /* renamed from: Qa */
    public C6341alB mo323Qa() {
        long granny_curve_data_da_k16u_c16u_CurveDataHeader_get = grannyJNI.granny_curve_data_da_k16u_c16u_CurveDataHeader_get(this.swigCPtr);
        if (granny_curve_data_da_k16u_c16u_CurveDataHeader_get == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_da_k16u_c16u_CurveDataHeader_get, false);
    }

    /* renamed from: dT */
    public void mo331dT(int i) {
        grannyJNI.granny_curve_data_da_k16u_c16u_OneOverKnotScaleTrunc_set(this.swigCPtr, i);
    }

    /* renamed from: WK */
    public int mo326WK() {
        return grannyJNI.granny_curve_data_da_k16u_c16u_OneOverKnotScaleTrunc_get(this.swigCPtr);
    }

    /* renamed from: gv */
    public void mo335gv(int i) {
        grannyJNI.granny_curve_data_da_k16u_c16u_ControlScaleOffsetCount_set(this.swigCPtr, i);
    }

    public int aye() {
        return grannyJNI.granny_curve_data_da_k16u_c16u_ControlScaleOffsetCount_get(this.swigCPtr);
    }

    /* renamed from: x */
    public void mo337x(C3159oW oWVar) {
        grannyJNI.granny_curve_data_da_k16u_c16u_ControlScaleOffsets_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW ayf() {
        long granny_curve_data_da_k16u_c16u_ControlScaleOffsets_get = grannyJNI.granny_curve_data_da_k16u_c16u_ControlScaleOffsets_get(this.swigCPtr);
        if (granny_curve_data_da_k16u_c16u_ControlScaleOffsets_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_da_k16u_c16u_ControlScaleOffsets_get, false);
    }

    /* renamed from: di */
    public void mo333di(int i) {
        grannyJNI.granny_curve_data_da_k16u_c16u_KnotControlCount_set(this.swigCPtr, i);
    }

    /* renamed from: Qe */
    public int mo324Qe() {
        return grannyJNI.granny_curve_data_da_k16u_c16u_KnotControlCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo330b(C2062bl blVar) {
        grannyJNI.granny_curve_data_da_k16u_c16u_KnotsControls_set(this.swigCPtr, C2062bl.m27965a(blVar));
    }

    /* renamed from: Ua */
    public C2062bl mo325Ua() {
        long granny_curve_data_da_k16u_c16u_KnotsControls_get = grannyJNI.granny_curve_data_da_k16u_c16u_KnotsControls_get(this.swigCPtr);
        if (granny_curve_data_da_k16u_c16u_KnotsControls_get == 0) {
            return null;
        }
        return new C2062bl(granny_curve_data_da_k16u_c16u_KnotsControls_get, false);
    }

    /* renamed from: gw */
    public C0047AZ mo336gw(int i) {
        long granny_curve_data_da_k16u_c16u_get = grannyJNI.granny_curve_data_da_k16u_c16u_get(this.swigCPtr, i);
        if (granny_curve_data_da_k16u_c16u_get == 0) {
            return null;
        }
        return new C0047AZ(granny_curve_data_da_k16u_c16u_get, false);
    }
}
