package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.wy */
/* compiled from: a */
public class C3939wy {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3939wy(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3939wy() {
        this(grannyJNI.new_granny_pngbt33332_vertex(), true);
    }

    /* renamed from: a */
    public static long m40764a(C3939wy wyVar) {
        if (wyVar == null) {
            return 0;
        }
        return wyVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pngbt33332_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo22835b(C3159oW oWVar) {
        grannyJNI.granny_pngbt33332_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo22845s() {
        long granny_pngbt33332_vertex_Position_get = grannyJNI.granny_pngbt33332_vertex_Position_get(this.swigCPtr);
        if (granny_pngbt33332_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngbt33332_vertex_Position_get, false);
    }

    /* renamed from: h */
    public void mo22841h(C3159oW oWVar) {
        grannyJNI.granny_pngbt33332_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo22836cR() {
        long granny_pngbt33332_vertex_Normal_get = grannyJNI.granny_pngbt33332_vertex_Normal_get(this.swigCPtr);
        if (granny_pngbt33332_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngbt33332_vertex_Normal_get, false);
    }

    /* renamed from: i */
    public void mo22842i(C3159oW oWVar) {
        grannyJNI.granny_pngbt33332_vertex_Tangent_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: gD */
    public C3159oW mo22840gD() {
        long granny_pngbt33332_vertex_Tangent_get = grannyJNI.granny_pngbt33332_vertex_Tangent_get(this.swigCPtr);
        if (granny_pngbt33332_vertex_Tangent_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngbt33332_vertex_Tangent_get, false);
    }

    /* renamed from: w */
    public void mo22846w(C3159oW oWVar) {
        grannyJNI.granny_pngbt33332_vertex_Binormal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW amC() {
        long granny_pngbt33332_vertex_Binormal_get = grannyJNI.granny_pngbt33332_vertex_Binormal_get(this.swigCPtr);
        if (granny_pngbt33332_vertex_Binormal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngbt33332_vertex_Binormal_get, false);
    }

    /* renamed from: j */
    public void mo22843j(C3159oW oWVar) {
        grannyJNI.granny_pngbt33332_vertex_UV_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: mv */
    public C3159oW mo22844mv() {
        long granny_pngbt33332_vertex_UV_get = grannyJNI.granny_pngbt33332_vertex_UV_get(this.swigCPtr);
        if (granny_pngbt33332_vertex_UV_get == 0) {
            return null;
        }
        return new C3159oW(granny_pngbt33332_vertex_UV_get, false);
    }

    /* renamed from: ft */
    public C3939wy mo22839ft(int i) {
        long granny_pngbt33332_vertex_get = grannyJNI.granny_pngbt33332_vertex_get(this.swigCPtr, i);
        if (granny_pngbt33332_vertex_get == 0) {
            return null;
        }
        return new C3939wy(granny_pngbt33332_vertex_get, false);
    }
}
