package logic.render.granny.dell;

import logic.render.granny.C6135ahD;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.ady  reason: case insensitive filesystem */
/* compiled from: a */
public class C5974ady {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5974ady(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5974ady() {
        this(grannyJNI.new_granny_grn_file_magic_value(), true);
    }

    /* renamed from: g */
    public static long m21102g(C5974ady ady) {
        if (ady == null) {
            return 0;
        }
        return ady.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_grn_file_magic_value(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: g */
    public void mo12995g(C6135ahD ahd) {
        grannyJNI.granny_grn_file_magic_value_MagicValue_set(this.swigCPtr, C6135ahD.m22189i(ahd));
    }

    public C6135ahD bRo() {
        long granny_grn_file_magic_value_MagicValue_get = grannyJNI.granny_grn_file_magic_value_MagicValue_get(this.swigCPtr);
        if (granny_grn_file_magic_value_MagicValue_get == 0) {
            return null;
        }
        return new C6135ahD(granny_grn_file_magic_value_MagicValue_get, false);
    }

    /* renamed from: gO */
    public void mo12996gO(long j) {
        grannyJNI.granny_grn_file_magic_value_HeaderSize_set(this.swigCPtr, j);
    }

    public long bRp() {
        return grannyJNI.granny_grn_file_magic_value_HeaderSize_get(this.swigCPtr);
    }

    /* renamed from: gP */
    public void mo12997gP(long j) {
        grannyJNI.granny_grn_file_magic_value_HeaderFormat_set(this.swigCPtr, j);
    }

    public long bRq() {
        return grannyJNI.granny_grn_file_magic_value_HeaderFormat_get(this.swigCPtr);
    }

    /* renamed from: h */
    public void mo12998h(C6135ahD ahd) {
        grannyJNI.granny_grn_file_magic_value_Reserved_set(this.swigCPtr, C6135ahD.m22189i(ahd));
    }

    public C6135ahD bRr() {
        long granny_grn_file_magic_value_Reserved_get = grannyJNI.granny_grn_file_magic_value_Reserved_get(this.swigCPtr);
        if (granny_grn_file_magic_value_Reserved_get == 0) {
            return null;
        }
        return new C6135ahD(granny_grn_file_magic_value_Reserved_get, false);
    }

    /* renamed from: pS */
    public C5974ady mo12999pS(int i) {
        long granny_grn_file_magic_value_get = grannyJNI.granny_grn_file_magic_value_get(this.swigCPtr, i);
        if (granny_grn_file_magic_value_get == 0) {
            return null;
        }
        return new C5974ady(granny_grn_file_magic_value_get, false);
    }
}
