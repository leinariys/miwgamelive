package logic.render.granny.dell;

/* renamed from: a.amj  reason: case insensitive filesystem */
/* compiled from: a */
public final class C6427amj {
    public static final C6427amj fZQ = new C6427amj("GrannyIgnoredLogMessage");
    public static final C6427amj fZR = new C6427amj("GrannyNoteLogMessage");
    public static final C6427amj fZS = new C6427amj("GrannyWarningLogMessage");
    public static final C6427amj fZT = new C6427amj("GrannyErrorLogMessage");
    public static final C6427amj fZU = new C6427amj("GrannyOnePastLastMessageType");
    private static C6427amj[] fZV = {fZQ, fZR, fZS, fZT, fZU};

    /* renamed from: pF */
    private static int f4925pF = 0;

    /* renamed from: pG */
    private final int f4926pG;

    /* renamed from: pH */
    private final String f4927pH;

    private C6427amj(String str) {
        this.f4927pH = str;
        int i = f4925pF;
        f4925pF = i + 1;
        this.f4926pG = i;
    }

    private C6427amj(String str, int i) {
        this.f4927pH = str;
        this.f4926pG = i;
        f4925pF = i + 1;
    }

    private C6427amj(String str, C6427amj amj) {
        this.f4927pH = str;
        this.f4926pG = amj.f4926pG;
        f4925pF = this.f4926pG + 1;
    }

    /* renamed from: sA */
    public static C6427amj m23977sA(int i) {
        if (i < fZV.length && i >= 0 && fZV[i].f4926pG == i) {
            return fZV[i];
        }
        for (int i2 = 0; i2 < fZV.length; i2++) {
            if (fZV[i2].f4926pG == i) {
                return fZV[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C6427amj.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f4926pG;
    }

    public String toString() {
        return this.f4927pH;
    }
}
