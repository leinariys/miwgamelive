package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.Qw */
/* compiled from: a */
public class C1155Qw {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1155Qw(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1155Qw() {
        this(grannyJNI.new_granny_pwnt3132_vertex(), true);
    }

    /* renamed from: a */
    public static long m9043a(C1155Qw qw) {
        if (qw == null) {
            return 0;
        }
        return qw.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pwnt3132_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo5107b(C3159oW oWVar) {
        grannyJNI.granny_pwnt3132_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo5117s() {
        long granny_pwnt3132_vertex_Position_get = grannyJNI.granny_pwnt3132_vertex_Position_get(this.swigCPtr);
        if (granny_pwnt3132_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwnt3132_vertex_Position_get, false);
    }

    /* renamed from: n */
    public void mo5116n(long j) {
        grannyJNI.granny_pwnt3132_vertex_BoneIndex_set(this.swigCPtr, j);
    }

    /* renamed from: gC */
    public long mo5111gC() {
        return grannyJNI.granny_pwnt3132_vertex_BoneIndex_get(this.swigCPtr);
    }

    /* renamed from: h */
    public void mo5112h(C3159oW oWVar) {
        grannyJNI.granny_pwnt3132_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo5108cR() {
        long granny_pwnt3132_vertex_Normal_get = grannyJNI.granny_pwnt3132_vertex_Normal_get(this.swigCPtr);
        if (granny_pwnt3132_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwnt3132_vertex_Normal_get, false);
    }

    /* renamed from: j */
    public void mo5113j(C3159oW oWVar) {
        grannyJNI.granny_pwnt3132_vertex_UV_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: mv */
    public C3159oW mo5115mv() {
        long granny_pwnt3132_vertex_UV_get = grannyJNI.granny_pwnt3132_vertex_UV_get(this.swigCPtr);
        if (granny_pwnt3132_vertex_UV_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwnt3132_vertex_UV_get, false);
    }

    /* renamed from: mR */
    public C1155Qw mo5114mR(int i) {
        long granny_pwnt3132_vertex_get = grannyJNI.granny_pwnt3132_vertex_get(this.swigCPtr, i);
        if (granny_pwnt3132_vertex_get == 0) {
            return null;
        }
        return new C1155Qw(granny_pwnt3132_vertex_get, false);
    }
}
