package logic.render.granny.dell;

/* renamed from: a.aoG  reason: case insensitive filesystem */
/* compiled from: a */
public class C6502aoG {
    private long swigCPtr;

    public C6502aoG(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6502aoG() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m24370b(C6502aoG aog) {
        if (aog == null) {
            return 0;
        }
        return aog.swigCPtr;
    }
}
