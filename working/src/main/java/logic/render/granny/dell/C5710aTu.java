package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aTu  reason: case insensitive filesystem */
/* compiled from: a */
public class C5710aTu {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5710aTu(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5710aTu() {
        this(grannyJNI.new_granny_pwnt3432_vertex(), true);
    }

    /* renamed from: a */
    public static long m18431a(C5710aTu atu) {
        if (atu == null) {
            return 0;
        }
        return atu.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pwnt3432_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo11550b(C3159oW oWVar) {
        grannyJNI.granny_pwnt3432_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo11559s() {
        long granny_pwnt3432_vertex_Position_get = grannyJNI.granny_pwnt3432_vertex_Position_get(this.swigCPtr);
        if (granny_pwnt3432_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwnt3432_vertex_Position_get, false);
    }

    /* renamed from: a */
    public void mo11548a(C2382ef efVar) {
        grannyJNI.granny_pwnt3432_vertex_BoneWeights_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cP */
    public C2382ef mo11551cP() {
        long granny_pwnt3432_vertex_BoneWeights_get = grannyJNI.granny_pwnt3432_vertex_BoneWeights_get(this.swigCPtr);
        if (granny_pwnt3432_vertex_BoneWeights_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwnt3432_vertex_BoneWeights_get, false);
    }

    /* renamed from: b */
    public void mo11549b(C2382ef efVar) {
        grannyJNI.granny_pwnt3432_vertex_BoneIndices_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cQ */
    public C2382ef mo11552cQ() {
        long granny_pwnt3432_vertex_BoneIndices_get = grannyJNI.granny_pwnt3432_vertex_BoneIndices_get(this.swigCPtr);
        if (granny_pwnt3432_vertex_BoneIndices_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwnt3432_vertex_BoneIndices_get, false);
    }

    /* renamed from: h */
    public void mo11556h(C3159oW oWVar) {
        grannyJNI.granny_pwnt3432_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo11553cR() {
        long granny_pwnt3432_vertex_Normal_get = grannyJNI.granny_pwnt3432_vertex_Normal_get(this.swigCPtr);
        if (granny_pwnt3432_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwnt3432_vertex_Normal_get, false);
    }

    /* renamed from: j */
    public void mo11557j(C3159oW oWVar) {
        grannyJNI.granny_pwnt3432_vertex_UV_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: mv */
    public C3159oW mo11558mv() {
        long granny_pwnt3432_vertex_UV_get = grannyJNI.granny_pwnt3432_vertex_UV_get(this.swigCPtr);
        if (granny_pwnt3432_vertex_UV_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwnt3432_vertex_UV_get, false);
    }

    /* renamed from: AO */
    public C5710aTu mo11547AO(int i) {
        long granny_pwnt3432_vertex_get = grannyJNI.granny_pwnt3432_vertex_get(this.swigCPtr, i);
        if (granny_pwnt3432_vertex_get == 0) {
            return null;
        }
        return new C5710aTu(granny_pwnt3432_vertex_get, false);
    }
}
