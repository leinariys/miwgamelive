package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aNT */
/* compiled from: a */
public class aNT {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aNT(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aNT() {
        this(grannyJNI.new_granny_stat_hud_model_controls(), true);
    }

    /* renamed from: b */
    public static long m16576b(aNT ant) {
        if (ant == null) {
            return 0;
        }
        return ant.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_stat_hud_model_controls(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: yR */
    public void mo10267yR(int i) {
        grannyJNI.granny_stat_hud_model_controls_TotalControlCount_set(this.swigCPtr, i);
    }

    public int dkG() {
        return grannyJNI.granny_stat_hud_model_controls_TotalControlCount_get(this.swigCPtr);
    }

    /* renamed from: yS */
    public void mo10268yS(int i) {
        grannyJNI.granny_stat_hud_model_controls_ActiveControlCount_set(this.swigCPtr, i);
    }

    public int dkH() {
        return grannyJNI.granny_stat_hud_model_controls_ActiveControlCount_get(this.swigCPtr);
    }

    /* renamed from: yT */
    public void mo10269yT(int i) {
        grannyJNI.granny_stat_hud_model_controls_ActiveAndWeightedControlCount_set(this.swigCPtr, i);
    }

    public int dkI() {
        return grannyJNI.granny_stat_hud_model_controls_ActiveAndWeightedControlCount_get(this.swigCPtr);
    }

    /* renamed from: yU */
    public void mo10270yU(int i) {
        grannyJNI.m45856xb80ef8e2(this.swigCPtr, i);
    }

    public int dkJ() {
        return grannyJNI.m45855xb80ecbd6(this.swigCPtr);
    }

    /* renamed from: yV */
    public void mo10271yV(int i) {
        grannyJNI.granny_stat_hud_model_controls_CompletableControlCount_set(this.swigCPtr, i);
    }

    public int dkK() {
        return grannyJNI.granny_stat_hud_model_controls_CompletableControlCount_get(this.swigCPtr);
    }

    /* renamed from: yW */
    public void mo10272yW(int i) {
        grannyJNI.granny_stat_hud_model_controls_CompletedControlCount_set(this.swigCPtr, i);
    }

    public int dkL() {
        return grannyJNI.granny_stat_hud_model_controls_CompletedControlCount_get(this.swigCPtr);
    }

    /* renamed from: yX */
    public void mo10273yX(int i) {
        grannyJNI.granny_stat_hud_model_controls_UnusedControlCount_set(this.swigCPtr, i);
    }

    public int dkM() {
        return grannyJNI.granny_stat_hud_model_controls_UnusedControlCount_get(this.swigCPtr);
    }

    /* renamed from: np */
    public void mo10263np(float f) {
        grannyJNI.granny_stat_hud_model_controls_MinClockTime_set(this.swigCPtr, f);
    }

    public float dkN() {
        return grannyJNI.granny_stat_hud_model_controls_MinClockTime_get(this.swigCPtr);
    }

    /* renamed from: nq */
    public void mo10264nq(float f) {
        grannyJNI.granny_stat_hud_model_controls_MaxClockTime_set(this.swigCPtr, f);
    }

    public float dkO() {
        return grannyJNI.granny_stat_hud_model_controls_MaxClockTime_get(this.swigCPtr);
    }

    /* renamed from: nr */
    public void mo10265nr(float f) {
        grannyJNI.granny_stat_hud_model_controls_MinCompletionTime_set(this.swigCPtr, f);
    }

    public float dkP() {
        return grannyJNI.granny_stat_hud_model_controls_MinCompletionTime_get(this.swigCPtr);
    }

    /* renamed from: ns */
    public void mo10266ns(float f) {
        grannyJNI.granny_stat_hud_model_controls_MaxCompletionTime_set(this.swigCPtr, f);
    }

    public float dkQ() {
        return grannyJNI.granny_stat_hud_model_controls_MaxCompletionTime_get(this.swigCPtr);
    }

    /* renamed from: yY */
    public aNT mo10274yY(int i) {
        long granny_stat_hud_model_controls_get = grannyJNI.granny_stat_hud_model_controls_get(this.swigCPtr, i);
        if (granny_stat_hud_model_controls_get == 0) {
            return null;
        }
        return new aNT(granny_stat_hud_model_controls_get, false);
    }
}
