package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.bK */
/* compiled from: a */
public class C2032bK {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2032bK(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C2032bK() {
        this(grannyJNI.new_granny_pwng3133_vertex(), true);
    }

    /* renamed from: a */
    public static long m27772a(C2032bK bKVar) {
        if (bKVar == null) {
            return 0;
        }
        return bKVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pwng3133_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo17254b(C3159oW oWVar) {
        grannyJNI.granny_pwng3133_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo17263s() {
        long granny_pwng3133_vertex_Position_get = grannyJNI.granny_pwng3133_vertex_Position_get(this.swigCPtr);
        if (granny_pwng3133_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwng3133_vertex_Position_get, false);
    }

    /* renamed from: n */
    public void mo17262n(long j) {
        grannyJNI.granny_pwng3133_vertex_BoneIndex_set(this.swigCPtr, j);
    }

    /* renamed from: gC */
    public long mo17258gC() {
        return grannyJNI.granny_pwng3133_vertex_BoneIndex_get(this.swigCPtr);
    }

    /* renamed from: h */
    public void mo17260h(C3159oW oWVar) {
        grannyJNI.granny_pwng3133_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo17255cR() {
        long granny_pwng3133_vertex_Normal_get = grannyJNI.granny_pwng3133_vertex_Normal_get(this.swigCPtr);
        if (granny_pwng3133_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwng3133_vertex_Normal_get, false);
    }

    /* renamed from: i */
    public void mo17261i(C3159oW oWVar) {
        grannyJNI.granny_pwng3133_vertex_Tangent_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: gD */
    public C3159oW mo17259gD() {
        long granny_pwng3133_vertex_Tangent_get = grannyJNI.granny_pwng3133_vertex_Tangent_get(this.swigCPtr);
        if (granny_pwng3133_vertex_Tangent_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwng3133_vertex_Tangent_get, false);
    }

    /* renamed from: X */
    public C2032bK mo17253X(int i) {
        long granny_pwng3133_vertex_get = grannyJNI.granny_pwng3133_vertex_get(this.swigCPtr, i);
        if (granny_pwng3133_vertex_get == 0) {
            return null;
        }
        return new C2032bK(granny_pwng3133_vertex_get, false);
    }
}
