package logic.render.granny.dell;

/* renamed from: a.aqu  reason: case insensitive filesystem */
/* compiled from: a */
public final class C6646aqu {
    public static final C6646aqu gqA = new C6646aqu("GrannyS3TCBGRA8888MappedAlpha");
    public static final C6646aqu gqB = new C6646aqu("GrannyS3TCBGRA8888InterpolatedAlpha");
    public static final C6646aqu gqC = new C6646aqu("GrannyOnePastLastS3TCTextureFormat");
    public static final C6646aqu gqy = new C6646aqu("GrannyS3TCBGR565");
    public static final C6646aqu gqz = new C6646aqu("GrannyS3TCBGRA5551");
    private static C6646aqu[] gqD = {gqy, gqz, gqA, gqB, gqC};
    /* renamed from: pF */
    private static int f5209pF = 0;

    /* renamed from: pG */
    private final int f5210pG;

    /* renamed from: pH */
    private final String f5211pH;

    private C6646aqu(String str) {
        this.f5211pH = str;
        int i = f5209pF;
        f5209pF = i + 1;
        this.f5210pG = i;
    }

    private C6646aqu(String str, int i) {
        this.f5211pH = str;
        this.f5210pG = i;
        f5209pF = i + 1;
    }

    private C6646aqu(String str, C6646aqu aqu) {
        this.f5211pH = str;
        this.f5210pG = aqu.f5210pG;
        f5209pF = this.f5210pG + 1;
    }

    /* renamed from: tH */
    public static C6646aqu m25309tH(int i) {
        if (i < gqD.length && i >= 0 && gqD[i].f5210pG == i) {
            return gqD[i];
        }
        for (int i2 = 0; i2 < gqD.length; i2++) {
            if (gqD[i2].f5210pG == i) {
                return gqD[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C6646aqu.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f5210pG;
    }

    public String toString() {
        return this.f5211pH;
    }
}
