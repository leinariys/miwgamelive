package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.WO */
/* compiled from: a */
public class C1522WO {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1522WO(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1522WO() {
        this(grannyJNI.new_granny_curve_data_d4_constant32f(), true);
    }

    /* renamed from: a */
    public static long m11150a(C1522WO wo) {
        if (wo == null) {
            return 0;
        }
        return wo.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve_data_d4_constant32f(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo6542a(C6341alB alb) {
        grannyJNI.granny_curve_data_d4_constant32f_CurveDataHeader_set(this.swigCPtr, C6341alB.m23687b(alb));
    }

    /* renamed from: Qa */
    public C6341alB mo6540Qa() {
        long granny_curve_data_d4_constant32f_CurveDataHeader_get = grannyJNI.granny_curve_data_d4_constant32f_CurveDataHeader_get(this.swigCPtr);
        if (granny_curve_data_d4_constant32f_CurveDataHeader_get == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_d4_constant32f_CurveDataHeader_get, false);
    }

    /* renamed from: e */
    public void mo6545e(short s) {
        grannyJNI.granny_curve_data_d4_constant32f_Padding_set(this.swigCPtr, s);
    }

    /* renamed from: Qb */
    public short mo6541Qb() {
        return grannyJNI.granny_curve_data_d4_constant32f_Padding_get(this.swigCPtr);
    }

    /* renamed from: K */
    public void mo6539K(C3159oW oWVar) {
        grannyJNI.granny_curve_data_d4_constant32f_Controls_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW bEk() {
        long granny_curve_data_d4_constant32f_Controls_get = grannyJNI.granny_curve_data_d4_constant32f_Controls_get(this.swigCPtr);
        if (granny_curve_data_d4_constant32f_Controls_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_d4_constant32f_Controls_get, false);
    }

    /* renamed from: pa */
    public C1522WO mo6547pa(int i) {
        long granny_curve_data_d4_constant32f_get = grannyJNI.granny_curve_data_d4_constant32f_get(this.swigCPtr, i);
        if (granny_curve_data_d4_constant32f_get == 0) {
            return null;
        }
        return new C1522WO(granny_curve_data_d4_constant32f_get, false);
    }
}
