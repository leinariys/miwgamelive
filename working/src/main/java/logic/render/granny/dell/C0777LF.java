package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.LF */
/* compiled from: a */
public class C0777LF {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0777LF(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0777LF() {
        this(grannyJNI.new_granny_transform_track(), true);
    }

    /* renamed from: f */
    public static long m6656f(C0777LF lf) {
        if (lf == null) {
            return 0;
        }
        return lf.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_transform_track(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getName() {
        return grannyJNI.granny_transform_track_Name_get(this.swigCPtr);
    }

    public void setName(String str) {
        grannyJNI.granny_transform_track_Name_set(this.swigCPtr, str);
    }

    /* renamed from: lF */
    public void mo3719lF(int i) {
        grannyJNI.granny_transform_track_Flags_set(this.swigCPtr, i);
    }

    public int getFlags() {
        return grannyJNI.granny_transform_track_Flags_get(this.swigCPtr);
    }

    /* renamed from: r */
    public void mo3721r(C3914wi wiVar) {
        grannyJNI.granny_transform_track_OrientationCurve_set(this.swigCPtr, C3914wi.m40719a(wiVar));
    }

    public C3914wi bfT() {
        long granny_transform_track_OrientationCurve_get = grannyJNI.granny_transform_track_OrientationCurve_get(this.swigCPtr);
        if (granny_transform_track_OrientationCurve_get == 0) {
            return null;
        }
        return new C3914wi(granny_transform_track_OrientationCurve_get, false);
    }

    /* renamed from: s */
    public void mo3722s(C3914wi wiVar) {
        grannyJNI.granny_transform_track_PositionCurve_set(this.swigCPtr, C3914wi.m40719a(wiVar));
    }

    public C3914wi bfU() {
        long granny_transform_track_PositionCurve_get = grannyJNI.granny_transform_track_PositionCurve_get(this.swigCPtr);
        if (granny_transform_track_PositionCurve_get == 0) {
            return null;
        }
        return new C3914wi(granny_transform_track_PositionCurve_get, false);
    }

    /* renamed from: t */
    public void mo3724t(C3914wi wiVar) {
        grannyJNI.granny_transform_track_ScaleShearCurve_set(this.swigCPtr, C3914wi.m40719a(wiVar));
    }

    public C3914wi bfV() {
        long granny_transform_track_ScaleShearCurve_get = grannyJNI.granny_transform_track_ScaleShearCurve_get(this.swigCPtr);
        if (granny_transform_track_ScaleShearCurve_get == 0) {
            return null;
        }
        return new C3914wi(granny_transform_track_ScaleShearCurve_get, false);
    }

    /* renamed from: lG */
    public C0777LF mo3720lG(int i) {
        long granny_transform_track_get = grannyJNI.granny_transform_track_get(this.swigCPtr, i);
        if (granny_transform_track_get == 0) {
            return null;
        }
        return new C0777LF(granny_transform_track_get, false);
    }
}
