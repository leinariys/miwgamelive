package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.vX */
/* compiled from: a */
public class C3832vX {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3832vX(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3832vX() {
        this(grannyJNI.new_granny_pnt333_vertex(), true);
    }

    /* renamed from: a */
    public static long m40277a(C3832vX vXVar) {
        if (vXVar == null) {
            return 0;
        }
        return vXVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pnt333_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo22600b(C3159oW oWVar) {
        grannyJNI.granny_pnt333_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo22606s() {
        long granny_pnt333_vertex_Position_get = grannyJNI.granny_pnt333_vertex_Position_get(this.swigCPtr);
        if (granny_pnt333_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pnt333_vertex_Position_get, false);
    }

    /* renamed from: h */
    public void mo22605h(C3159oW oWVar) {
        grannyJNI.granny_pnt333_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo22601cR() {
        long granny_pnt333_vertex_Normal_get = grannyJNI.granny_pnt333_vertex_Normal_get(this.swigCPtr);
        if (granny_pnt333_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pnt333_vertex_Normal_get, false);
    }

    /* renamed from: v */
    public void mo22607v(C3159oW oWVar) {
        grannyJNI.granny_pnt333_vertex_UVW_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW alh() {
        long granny_pnt333_vertex_UVW_get = grannyJNI.granny_pnt333_vertex_UVW_get(this.swigCPtr);
        if (granny_pnt333_vertex_UVW_get == 0) {
            return null;
        }
        return new C3159oW(granny_pnt333_vertex_UVW_get, false);
    }

    /* renamed from: fa */
    public C3832vX mo22603fa(int i) {
        long granny_pnt333_vertex_get = grannyJNI.granny_pnt333_vertex_get(this.swigCPtr, i);
        if (granny_pnt333_vertex_get == 0) {
            return null;
        }
        return new C3832vX(granny_pnt333_vertex_get, false);
    }
}
