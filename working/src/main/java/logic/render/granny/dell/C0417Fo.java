package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.Fo */
/* compiled from: a */
public class C0417Fo {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0417Fo(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0417Fo() {
        this(grannyJNI.new_granny_curve_data_da_k8u_c8u(), true);
    }

    /* renamed from: a */
    public static long m3279a(C0417Fo fo) {
        if (fo == null) {
            return 0;
        }
        return fo.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve_data_da_k8u_c8u(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo2208a(C6341alB alb) {
        grannyJNI.granny_curve_data_da_k8u_c8u_CurveDataHeader_set(this.swigCPtr, C6341alB.m23687b(alb));
    }

    /* renamed from: Qa */
    public C6341alB mo2205Qa() {
        long granny_curve_data_da_k8u_c8u_CurveDataHeader_get = grannyJNI.granny_curve_data_da_k8u_c8u_CurveDataHeader_get(this.swigCPtr);
        if (granny_curve_data_da_k8u_c8u_CurveDataHeader_get == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_da_k8u_c8u_CurveDataHeader_get, false);
    }

    /* renamed from: dT */
    public void mo2213dT(int i) {
        grannyJNI.granny_curve_data_da_k8u_c8u_OneOverKnotScaleTrunc_set(this.swigCPtr, i);
    }

    /* renamed from: WK */
    public int mo2207WK() {
        return grannyJNI.granny_curve_data_da_k8u_c8u_OneOverKnotScaleTrunc_get(this.swigCPtr);
    }

    /* renamed from: gv */
    public void mo2217gv(int i) {
        grannyJNI.granny_curve_data_da_k8u_c8u_ControlScaleOffsetCount_set(this.swigCPtr, i);
    }

    public int aye() {
        return grannyJNI.granny_curve_data_da_k8u_c8u_ControlScaleOffsetCount_get(this.swigCPtr);
    }

    /* renamed from: x */
    public void mo2219x(C3159oW oWVar) {
        grannyJNI.granny_curve_data_da_k8u_c8u_ControlScaleOffsets_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW ayf() {
        long granny_curve_data_da_k8u_c8u_ControlScaleOffsets_get = grannyJNI.granny_curve_data_da_k8u_c8u_ControlScaleOffsets_get(this.swigCPtr);
        if (granny_curve_data_da_k8u_c8u_ControlScaleOffsets_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_da_k8u_c8u_ControlScaleOffsets_get, false);
    }

    /* renamed from: di */
    public void mo2215di(int i) {
        grannyJNI.granny_curve_data_da_k8u_c8u_KnotControlCount_set(this.swigCPtr, i);
    }

    /* renamed from: Qe */
    public int mo2206Qe() {
        return grannyJNI.granny_curve_data_da_k8u_c8u_KnotControlCount_get(this.swigCPtr);
    }

    /* renamed from: d */
    public void mo2212d(C2382ef efVar) {
        grannyJNI.granny_curve_data_da_k8u_c8u_KnotsControls_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    public C2382ef ano() {
        long granny_curve_data_da_k8u_c8u_KnotsControls_get = grannyJNI.granny_curve_data_da_k8u_c8u_KnotsControls_get(this.swigCPtr);
        if (granny_curve_data_da_k8u_c8u_KnotsControls_get == 0) {
            return null;
        }
        return new C2382ef(granny_curve_data_da_k8u_c8u_KnotsControls_get, false);
    }

    /* renamed from: hf */
    public C0417Fo mo2218hf(int i) {
        long granny_curve_data_da_k8u_c8u_get = grannyJNI.granny_curve_data_da_k8u_c8u_get(this.swigCPtr, i);
        if (granny_curve_data_da_k8u_c8u_get == 0) {
            return null;
        }
        return new C0417Fo(granny_curve_data_da_k8u_c8u_get, false);
    }
}
