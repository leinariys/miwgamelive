package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aOc  reason: case insensitive filesystem */
/* compiled from: a */
public class C5562aOc {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5562aOc(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5562aOc() {
        this(grannyJNI.new_granny_old_curve(), true);
    }

    /* renamed from: a */
    public static long m16918a(C5562aOc aoc) {
        if (aoc == null) {
            return 0;
        }
        return aoc.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_old_curve(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: zL */
    public void mo10631zL(int i) {
        grannyJNI.granny_old_curve_Degree_set(this.swigCPtr, i);
    }

    public int dmC() {
        return grannyJNI.granny_old_curve_Degree_get(this.swigCPtr);
    }

    /* renamed from: tB */
    public void mo10630tB(int i) {
        grannyJNI.granny_old_curve_KnotCount_set(this.swigCPtr, i);
    }

    public int cqx() {
        return grannyJNI.granny_old_curve_KnotCount_get(this.swigCPtr);
    }

    /* renamed from: M */
    public void mo10621M(C3159oW oWVar) {
        grannyJNI.granny_old_curve_Knots_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW cqy() {
        long granny_old_curve_Knots_get = grannyJNI.granny_old_curve_Knots_get(this.swigCPtr);
        if (granny_old_curve_Knots_get == 0) {
            return null;
        }
        return new C3159oW(granny_old_curve_Knots_get, false);
    }

    /* renamed from: qv */
    public void mo10629qv(int i) {
        grannyJNI.granny_old_curve_ControlCount_set(this.swigCPtr, i);
    }

    public int bVw() {
        return grannyJNI.granny_old_curve_ControlCount_get(this.swigCPtr);
    }

    /* renamed from: K */
    public void mo10620K(C3159oW oWVar) {
        grannyJNI.granny_old_curve_Controls_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW bEk() {
        long granny_old_curve_Controls_get = grannyJNI.granny_old_curve_Controls_get(this.swigCPtr);
        if (granny_old_curve_Controls_get == 0) {
            return null;
        }
        return new C3159oW(granny_old_curve_Controls_get, false);
    }

    /* renamed from: zM */
    public C5562aOc mo10632zM(int i) {
        long granny_old_curve_get = grannyJNI.granny_old_curve_get(this.swigCPtr, i);
        if (granny_old_curve_get == 0) {
            return null;
        }
        return new C5562aOc(granny_old_curve_get, false);
    }
}
