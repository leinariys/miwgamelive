package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.Mj */
/* compiled from: a */
public class C0878Mj {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0878Mj(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0878Mj() {
        this(grannyJNI.new_granny_curve_data_d3_k8u_c8u(), true);
    }

    /* renamed from: a */
    public static long m7148a(C0878Mj mj) {
        if (mj == null) {
            return 0;
        }
        return mj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve_data_d3_k8u_c8u(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo4046a(C6341alB alb) {
        grannyJNI.granny_curve_data_d3_k8u_c8u_CurveDataHeader_set(this.swigCPtr, C6341alB.m23687b(alb));
    }

    /* renamed from: Qa */
    public C6341alB mo4041Qa() {
        long granny_curve_data_d3_k8u_c8u_CurveDataHeader_get = grannyJNI.granny_curve_data_d3_k8u_c8u_CurveDataHeader_get(this.swigCPtr);
        if (granny_curve_data_d3_k8u_c8u_CurveDataHeader_get == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_d3_k8u_c8u_CurveDataHeader_get, false);
    }

    /* renamed from: dT */
    public void mo4049dT(int i) {
        grannyJNI.granny_curve_data_d3_k8u_c8u_OneOverKnotScaleTrunc_set(this.swigCPtr, i);
    }

    /* renamed from: WK */
    public int mo4045WK() {
        return grannyJNI.granny_curve_data_d3_k8u_c8u_OneOverKnotScaleTrunc_get(this.swigCPtr);
    }

    /* renamed from: n */
    public void mo4054n(C3159oW oWVar) {
        grannyJNI.granny_curve_data_d3_k8u_c8u_ControlScales_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: Qc */
    public C3159oW mo4042Qc() {
        long granny_curve_data_d3_k8u_c8u_ControlScales_get = grannyJNI.granny_curve_data_d3_k8u_c8u_ControlScales_get(this.swigCPtr);
        if (granny_curve_data_d3_k8u_c8u_ControlScales_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_d3_k8u_c8u_ControlScales_get, false);
    }

    /* renamed from: o */
    public void mo4055o(C3159oW oWVar) {
        grannyJNI.granny_curve_data_d3_k8u_c8u_ControlOffsets_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: Qd */
    public C3159oW mo4043Qd() {
        long granny_curve_data_d3_k8u_c8u_ControlOffsets_get = grannyJNI.granny_curve_data_d3_k8u_c8u_ControlOffsets_get(this.swigCPtr);
        if (granny_curve_data_d3_k8u_c8u_ControlOffsets_get == 0) {
            return null;
        }
        return new C3159oW(granny_curve_data_d3_k8u_c8u_ControlOffsets_get, false);
    }

    /* renamed from: di */
    public void mo4051di(int i) {
        grannyJNI.granny_curve_data_d3_k8u_c8u_KnotControlCount_set(this.swigCPtr, i);
    }

    /* renamed from: Qe */
    public int mo4044Qe() {
        return grannyJNI.granny_curve_data_d3_k8u_c8u_KnotControlCount_get(this.swigCPtr);
    }

    /* renamed from: d */
    public void mo4048d(C2382ef efVar) {
        grannyJNI.granny_curve_data_d3_k8u_c8u_KnotsControls_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    public C2382ef ano() {
        long granny_curve_data_d3_k8u_c8u_KnotsControls_get = grannyJNI.granny_curve_data_d3_k8u_c8u_KnotsControls_get(this.swigCPtr);
        if (granny_curve_data_d3_k8u_c8u_KnotsControls_get == 0) {
            return null;
        }
        return new C2382ef(granny_curve_data_d3_k8u_c8u_KnotsControls_get, false);
    }

    /* renamed from: lL */
    public C0878Mj mo4053lL(int i) {
        long granny_curve_data_d3_k8u_c8u_get = grannyJNI.granny_curve_data_d3_k8u_c8u_get(this.swigCPtr, i);
        if (granny_curve_data_d3_k8u_c8u_get == 0) {
            return null;
        }
        return new C0878Mj(granny_curve_data_d3_k8u_c8u_get, false);
    }
}
