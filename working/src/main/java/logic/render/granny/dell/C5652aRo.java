package logic.render.granny.dell;

/* renamed from: a.aRo  reason: case insensitive filesystem */
/* compiled from: a */
public class C5652aRo {
    private long swigCPtr;

    public C5652aRo(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5652aRo() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m17936b(C5652aRo aro) {
        if (aro == null) {
            return 0;
        }
        return aro.swigCPtr;
    }
}
