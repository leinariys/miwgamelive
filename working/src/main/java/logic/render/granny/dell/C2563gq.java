package logic.render.granny.dell;

import logic.render.granny.C3242pY;
import logic.render.granny.C5975adz;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.gq */
/* compiled from: a */
public class C2563gq {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2563gq(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C2563gq() {
        this(grannyJNI.new_granny_animation_binding_identifier(), true);
    }

    /* renamed from: a */
    public static long m32392a(C2563gq gqVar) {
        if (gqVar == null) {
            return 0;
        }
        return gqVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_animation_binding_identifier(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo19112a(C3242pY pYVar) {
        grannyJNI.granny_animation_binding_identifier_Animation_set(this.swigCPtr, C3242pY.m37170b(pYVar));
    }

    /* renamed from: vz */
    public C3242pY mo19127vz() {
        long granny_animation_binding_identifier_Animation_get = grannyJNI.granny_animation_binding_identifier_Animation_get(this.swigCPtr);
        if (granny_animation_binding_identifier_Animation_get == 0) {
            return null;
        }
        return new C3242pY(granny_animation_binding_identifier_Animation_get, false);
    }

    /* renamed from: bi */
    public void mo19116bi(int i) {
        grannyJNI.granny_animation_binding_identifier_SourceTrackGroupIndex_set(this.swigCPtr, i);
    }

    /* renamed from: vA */
    public int mo19121vA() {
        return grannyJNI.granny_animation_binding_identifier_SourceTrackGroupIndex_get(this.swigCPtr);
    }

    /* renamed from: at */
    public void mo19113at(String str) {
        grannyJNI.granny_animation_binding_identifier_TrackPattern_set(this.swigCPtr, str);
    }

    /* renamed from: vB */
    public String mo19122vB() {
        return grannyJNI.granny_animation_binding_identifier_TrackPattern_get(this.swigCPtr);
    }

    /* renamed from: au */
    public void mo19114au(String str) {
        grannyJNI.granny_animation_binding_identifier_BonePattern_set(this.swigCPtr, str);
    }

    /* renamed from: vC */
    public String mo19123vC() {
        return grannyJNI.granny_animation_binding_identifier_BonePattern_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo19111a(C5975adz adz) {
        grannyJNI.granny_animation_binding_identifier_OnModel_set(this.swigCPtr, C5975adz.m21108e(adz));
    }

    /* renamed from: vD */
    public C5975adz mo19124vD() {
        long granny_animation_binding_identifier_OnModel_get = grannyJNI.granny_animation_binding_identifier_OnModel_get(this.swigCPtr);
        if (granny_animation_binding_identifier_OnModel_get == 0) {
            return null;
        }
        return new C5975adz(granny_animation_binding_identifier_OnModel_get, false);
    }

    /* renamed from: b */
    public void mo19115b(C5975adz adz) {
        grannyJNI.granny_animation_binding_identifier_FromBasis_set(this.swigCPtr, C5975adz.m21108e(adz));
    }

    /* renamed from: vE */
    public C5975adz mo19125vE() {
        long granny_animation_binding_identifier_FromBasis_get = grannyJNI.granny_animation_binding_identifier_FromBasis_get(this.swigCPtr);
        if (granny_animation_binding_identifier_FromBasis_get == 0) {
            return null;
        }
        return new C5975adz(granny_animation_binding_identifier_FromBasis_get, false);
    }

    /* renamed from: c */
    public void mo19118c(C5975adz adz) {
        grannyJNI.granny_animation_binding_identifier_ToBasis_set(this.swigCPtr, C5975adz.m21108e(adz));
    }

    /* renamed from: vF */
    public C5975adz mo19126vF() {
        long granny_animation_binding_identifier_ToBasis_get = grannyJNI.granny_animation_binding_identifier_ToBasis_get(this.swigCPtr);
        if (granny_animation_binding_identifier_ToBasis_get == 0) {
            return null;
        }
        return new C5975adz(granny_animation_binding_identifier_ToBasis_get, false);
    }

    /* renamed from: bj */
    public C2563gq mo19117bj(int i) {
        long granny_animation_binding_identifier_get = grannyJNI.granny_animation_binding_identifier_get(this.swigCPtr, i);
        if (granny_animation_binding_identifier_get == 0) {
            return null;
        }
        return new C2563gq(granny_animation_binding_identifier_get, false);
    }
}
