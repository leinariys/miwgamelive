package logic.render.granny.dell;

import logic.render.granny.C2062bl;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.qf */
/* compiled from: a */
public class C3359qf {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3359qf(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3359qf() {
        this(grannyJNI.new_granny_curve_data_d9i1_k16u_c16u(), true);
    }

    /* renamed from: a */
    public static long m37754a(C3359qf qfVar) {
        if (qfVar == null) {
            return 0;
        }
        return qfVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve_data_d9i1_k16u_c16u(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo21430a(C6341alB alb) {
        grannyJNI.granny_curve_data_d9i1_k16u_c16u_CurveDataHeader_set(this.swigCPtr, C6341alB.m23687b(alb));
    }

    /* renamed from: Qa */
    public C6341alB mo21424Qa() {
        long granny_curve_data_d9i1_k16u_c16u_CurveDataHeader_get = grannyJNI.granny_curve_data_d9i1_k16u_c16u_CurveDataHeader_get(this.swigCPtr);
        if (granny_curve_data_d9i1_k16u_c16u_CurveDataHeader_get == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_d9i1_k16u_c16u_CurveDataHeader_get, false);
    }

    /* renamed from: dT */
    public void mo21434dT(int i) {
        grannyJNI.granny_curve_data_d9i1_k16u_c16u_OneOverKnotScaleTrunc_set(this.swigCPtr, i);
    }

    /* renamed from: WK */
    public int mo21427WK() {
        return grannyJNI.granny_curve_data_d9i1_k16u_c16u_OneOverKnotScaleTrunc_get(this.swigCPtr);
    }

    /* renamed from: cU */
    public void mo21432cU(float f) {
        grannyJNI.granny_curve_data_d9i1_k16u_c16u_ControlScale_set(this.swigCPtr, f);
    }

    /* renamed from: WL */
    public float mo21428WL() {
        return grannyJNI.granny_curve_data_d9i1_k16u_c16u_ControlScale_get(this.swigCPtr);
    }

    /* renamed from: cV */
    public void mo21433cV(float f) {
        grannyJNI.granny_curve_data_d9i1_k16u_c16u_ControlOffset_set(this.swigCPtr, f);
    }

    /* renamed from: WM */
    public float mo21429WM() {
        return grannyJNI.granny_curve_data_d9i1_k16u_c16u_ControlOffset_get(this.swigCPtr);
    }

    /* renamed from: di */
    public void mo21437di(int i) {
        grannyJNI.granny_curve_data_d9i1_k16u_c16u_KnotControlCount_set(this.swigCPtr, i);
    }

    /* renamed from: Qe */
    public int mo21425Qe() {
        return grannyJNI.granny_curve_data_d9i1_k16u_c16u_KnotControlCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo21431b(C2062bl blVar) {
        grannyJNI.granny_curve_data_d9i1_k16u_c16u_KnotsControls_set(this.swigCPtr, C2062bl.m27965a(blVar));
    }

    /* renamed from: Ua */
    public C2062bl mo21426Ua() {
        long granny_curve_data_d9i1_k16u_c16u_KnotsControls_get = grannyJNI.granny_curve_data_d9i1_k16u_c16u_KnotsControls_get(this.swigCPtr);
        if (granny_curve_data_d9i1_k16u_c16u_KnotsControls_get == 0) {
            return null;
        }
        return new C2062bl(granny_curve_data_d9i1_k16u_c16u_KnotsControls_get, false);
    }

    /* renamed from: dU */
    public C3359qf mo21435dU(int i) {
        long granny_curve_data_d9i1_k16u_c16u_get = grannyJNI.granny_curve_data_d9i1_k16u_c16u_get(this.swigCPtr, i);
        if (granny_curve_data_d9i1_k16u_c16u_get == 0) {
            return null;
        }
        return new C3359qf(granny_curve_data_d9i1_k16u_c16u_get, false);
    }
}
