package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.Pj */
/* compiled from: a */
public class C1075Pj {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1075Pj(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1075Pj() {
        this(grannyJNI.new_granny_stat_hud_model_instances(), true);
    }

    /* renamed from: a */
    public static long m8601a(C1075Pj pj) {
        if (pj == null) {
            return 0;
        }
        return pj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_stat_hud_model_instances(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: mu */
    public void mo4825mu(int i) {
        grannyJNI.granny_stat_hud_model_instances_TotalInstanceCount_set(this.swigCPtr, i);
    }

    public int bne() {
        return grannyJNI.granny_stat_hud_model_instances_TotalInstanceCount_get(this.swigCPtr);
    }

    /* renamed from: mv */
    public void mo4826mv(int i) {
        grannyJNI.granny_stat_hud_model_instances_TotalInstancedBoneCount_set(this.swigCPtr, i);
    }

    public int bnf() {
        return grannyJNI.granny_stat_hud_model_instances_TotalInstancedBoneCount_get(this.swigCPtr);
    }

    /* renamed from: mw */
    public void mo4827mw(int i) {
        grannyJNI.granny_stat_hud_model_instances_TotalUsedModelCount_set(this.swigCPtr, i);
    }

    public int bng() {
        return grannyJNI.granny_stat_hud_model_instances_TotalUsedModelCount_get(this.swigCPtr);
    }

    /* renamed from: mx */
    public void mo4828mx(int i) {
        grannyJNI.granny_stat_hud_model_instances_MaxInstanceCount_set(this.swigCPtr, i);
    }

    public int bnh() {
        return grannyJNI.granny_stat_hud_model_instances_MaxInstanceCount_get(this.swigCPtr);
    }

    /* renamed from: my */
    public void mo4829my(int i) {
        grannyJNI.granny_stat_hud_model_instances_MaxBoneCount_set(this.swigCPtr, i);
    }

    public int bni() {
        return grannyJNI.granny_stat_hud_model_instances_MaxBoneCount_get(this.swigCPtr);
    }

    /* renamed from: mz */
    public void mo4830mz(int i) {
        grannyJNI.granny_stat_hud_model_instances_MaxInstanceControlCount_set(this.swigCPtr, i);
    }

    public int bnj() {
        return grannyJNI.granny_stat_hud_model_instances_MaxInstanceControlCount_get(this.swigCPtr);
    }

    /* renamed from: mA */
    public C1075Pj mo4824mA(int i) {
        long granny_stat_hud_model_instances_get = grannyJNI.granny_stat_hud_model_instances_get(this.swigCPtr, i);
        if (granny_stat_hud_model_instances_get == 0) {
            return null;
        }
        return new C1075Pj(granny_stat_hud_model_instances_get, false);
    }
}
