package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.oQ */
/* compiled from: a */
public class C3153oQ {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3153oQ(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3153oQ() {
        this(grannyJNI.new_granny_pnt332_vertex(), true);
    }

    /* renamed from: a */
    public static long m36716a(C3153oQ oQVar) {
        if (oQVar == null) {
            return 0;
        }
        return oQVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pnt332_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo20989b(C3159oW oWVar) {
        grannyJNI.granny_pnt332_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo20997s() {
        long granny_pnt332_vertex_Position_get = grannyJNI.granny_pnt332_vertex_Position_get(this.swigCPtr);
        if (granny_pnt332_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pnt332_vertex_Position_get, false);
    }

    /* renamed from: h */
    public void mo20994h(C3159oW oWVar) {
        grannyJNI.granny_pnt332_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo20990cR() {
        long granny_pnt332_vertex_Normal_get = grannyJNI.granny_pnt332_vertex_Normal_get(this.swigCPtr);
        if (granny_pnt332_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pnt332_vertex_Normal_get, false);
    }

    /* renamed from: j */
    public void mo20995j(C3159oW oWVar) {
        grannyJNI.granny_pnt332_vertex_UV_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: mv */
    public C3159oW mo20996mv() {
        long granny_pnt332_vertex_UV_get = grannyJNI.granny_pnt332_vertex_UV_get(this.swigCPtr);
        if (granny_pnt332_vertex_UV_get == 0) {
            return null;
        }
        return new C3159oW(granny_pnt332_vertex_UV_get, false);
    }

    /* renamed from: dG */
    public C3153oQ mo20991dG(int i) {
        long granny_pnt332_vertex_get = grannyJNI.granny_pnt332_vertex_get(this.swigCPtr, i);
        if (granny_pnt332_vertex_get == 0) {
            return null;
        }
        return new C3153oQ(granny_pnt332_vertex_get, false);
    }
}
