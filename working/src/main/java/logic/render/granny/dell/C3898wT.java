package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.wT */
/* compiled from: a */
public class C3898wT {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3898wT(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3898wT() {
        this(grannyJNI.new_granny_curve_data_d4n_k8u_c7u(), true);
    }

    /* renamed from: a */
    public static long m40637a(C3898wT wTVar) {
        if (wTVar == null) {
            return 0;
        }
        return wTVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve_data_d4n_k8u_c7u(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo22759a(C6341alB alb) {
        grannyJNI.granny_curve_data_d4n_k8u_c7u_CurveDataHeader_set(this.swigCPtr, C6341alB.m23687b(alb));
    }

    /* renamed from: Qa */
    public C6341alB mo22755Qa() {
        long granny_curve_data_d4n_k8u_c7u_CurveDataHeader_get = grannyJNI.granny_curve_data_d4n_k8u_c7u_CurveDataHeader_get(this.swigCPtr);
        if (granny_curve_data_d4n_k8u_c7u_CurveDataHeader_get == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_d4n_k8u_c7u_CurveDataHeader_get, false);
    }

    /* renamed from: dA */
    public void mo22763dA(int i) {
        grannyJNI.granny_curve_data_d4n_k8u_c7u_ScaleOffsetTableEntries_set(this.swigCPtr, i);
    }

    /* renamed from: TY */
    public int mo22757TY() {
        return grannyJNI.granny_curve_data_d4n_k8u_c7u_ScaleOffsetTableEntries_get(this.swigCPtr);
    }

    /* renamed from: cL */
    public void mo22761cL(float f) {
        grannyJNI.granny_curve_data_d4n_k8u_c7u_OneOverKnotScale_set(this.swigCPtr, f);
    }

    /* renamed from: TZ */
    public float mo22758TZ() {
        return grannyJNI.granny_curve_data_d4n_k8u_c7u_OneOverKnotScale_get(this.swigCPtr);
    }

    /* renamed from: di */
    public void mo22765di(int i) {
        grannyJNI.granny_curve_data_d4n_k8u_c7u_KnotControlCount_set(this.swigCPtr, i);
    }

    /* renamed from: Qe */
    public int mo22756Qe() {
        return grannyJNI.granny_curve_data_d4n_k8u_c7u_KnotControlCount_get(this.swigCPtr);
    }

    /* renamed from: d */
    public void mo22762d(C2382ef efVar) {
        grannyJNI.granny_curve_data_d4n_k8u_c7u_KnotsControls_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    public C2382ef ano() {
        long granny_curve_data_d4n_k8u_c7u_KnotsControls_get = grannyJNI.granny_curve_data_d4n_k8u_c7u_KnotsControls_get(this.swigCPtr);
        if (granny_curve_data_d4n_k8u_c7u_KnotsControls_get == 0) {
            return null;
        }
        return new C2382ef(granny_curve_data_d4n_k8u_c7u_KnotsControls_get, false);
    }

    /* renamed from: fu */
    public C3898wT mo22767fu(int i) {
        long granny_curve_data_d4n_k8u_c7u_get = grannyJNI.granny_curve_data_d4n_k8u_c7u_get(this.swigCPtr, i);
        if (granny_curve_data_d4n_k8u_c7u_get == 0) {
            return null;
        }
        return new C3898wT(granny_curve_data_d4n_k8u_c7u_get, false);
    }
}
