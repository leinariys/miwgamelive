package logic.render.granny.dell;

import logic.render.granny.C5501aLt;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.apj  reason: case insensitive filesystem */
/* compiled from: a */
public class C6583apj {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6583apj(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6583apj() {
        this(grannyJNI.new_granny_animation_binding(), true);
    }

    /* renamed from: f */
    public static long m24993f(C6583apj apj) {
        if (apj == null) {
            return 0;
        }
        return apj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_animation_binding(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: c */
    public void mo15469c(C2563gq gqVar) {
        grannyJNI.granny_animation_binding_ID_set(this.swigCPtr, C2563gq.m32392a(gqVar));
    }

    public C2563gq cpq() {
        long granny_animation_binding_ID_get = grannyJNI.granny_animation_binding_ID_get(this.swigCPtr);
        if (granny_animation_binding_ID_get == 0) {
            return null;
        }
        return new C2563gq(granny_animation_binding_ID_get, false);
    }

    /* renamed from: f */
    public void mo15480f(C5501aLt alt) {
        grannyJNI.granny_animation_binding_RebasingMemory_set(this.swigCPtr, C5501aLt.m16286h(alt));
    }

    public C5501aLt cpr() {
        long granny_animation_binding_RebasingMemory_get = grannyJNI.granny_animation_binding_RebasingMemory_get(this.swigCPtr);
        if (granny_animation_binding_RebasingMemory_get == 0) {
            return null;
        }
        return new C5501aLt(granny_animation_binding_RebasingMemory_get, false);
    }

    /* renamed from: tr */
    public void mo15486tr(int i) {
        grannyJNI.granny_animation_binding_TrackBindingCount_set(this.swigCPtr, i);
    }

    public int cps() {
        return grannyJNI.granny_animation_binding_TrackBindingCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo15468b(C0476Gb gb) {
        grannyJNI.granny_animation_binding_TrackBindings_set(this.swigCPtr, C0476Gb.m3443a(gb));
    }

    public C0476Gb cpt() {
        long granny_animation_binding_TrackBindings_get = grannyJNI.granny_animation_binding_TrackBindings_get(this.swigCPtr);
        if (granny_animation_binding_TrackBindings_get == 0) {
            return null;
        }
        return new C0476Gb(granny_animation_binding_TrackBindings_get, false);
    }

    /* renamed from: g */
    public void mo15482g(C6583apj apj) {
        grannyJNI.granny_animation_binding_Left_set(this.swigCPtr, m24993f(apj));
    }

    public C6583apj cpu() {
        long granny_animation_binding_Left_get = grannyJNI.granny_animation_binding_Left_get(this.swigCPtr);
        if (granny_animation_binding_Left_get == 0) {
            return null;
        }
        return new C6583apj(granny_animation_binding_Left_get, false);
    }

    /* renamed from: h */
    public void mo15483h(C6583apj apj) {
        grannyJNI.granny_animation_binding_Right_set(this.swigCPtr, m24993f(apj));
    }

    public C6583apj cpv() {
        long granny_animation_binding_Right_get = grannyJNI.granny_animation_binding_Right_get(this.swigCPtr);
        if (granny_animation_binding_Right_get == 0) {
            return null;
        }
        return new C6583apj(granny_animation_binding_Right_get, false);
    }

    /* renamed from: ts */
    public void mo15487ts(int i) {
        grannyJNI.granny_animation_binding_UsedBy_set(this.swigCPtr, i);
    }

    public int cpw() {
        return grannyJNI.granny_animation_binding_UsedBy_get(this.swigCPtr);
    }

    /* renamed from: i */
    public void mo15484i(C6583apj apj) {
        grannyJNI.granny_animation_binding_PreviousUnused_set(this.swigCPtr, m24993f(apj));
    }

    public C6583apj cpx() {
        long granny_animation_binding_PreviousUnused_get = grannyJNI.granny_animation_binding_PreviousUnused_get(this.swigCPtr);
        if (granny_animation_binding_PreviousUnused_get == 0) {
            return null;
        }
        return new C6583apj(granny_animation_binding_PreviousUnused_get, false);
    }

    /* renamed from: j */
    public void mo15485j(C6583apj apj) {
        grannyJNI.granny_animation_binding_NextUnused_set(this.swigCPtr, m24993f(apj));
    }

    public C6583apj cpy() {
        long granny_animation_binding_NextUnused_get = grannyJNI.granny_animation_binding_NextUnused_get(this.swigCPtr);
        if (granny_animation_binding_NextUnused_get == 0) {
            return null;
        }
        return new C6583apj(granny_animation_binding_NextUnused_get, false);
    }

    /* renamed from: tt */
    public C6583apj mo15488tt(int i) {
        long granny_animation_binding_get = grannyJNI.granny_animation_binding_get(this.swigCPtr, i);
        if (granny_animation_binding_get == 0) {
            return null;
        }
        return new C6583apj(granny_animation_binding_get, false);
    }
}
