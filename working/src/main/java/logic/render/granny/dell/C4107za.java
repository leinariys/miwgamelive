package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.za */
/* compiled from: a */
public class C4107za {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C4107za(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C4107za() {
        this(grannyJNI.new_granny_pwngb32333_vertex(), true);
    }

    /* renamed from: a */
    public static long m41632a(C4107za zaVar) {
        if (zaVar == null) {
            return 0;
        }
        return zaVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pwngb32333_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo23342b(C3159oW oWVar) {
        grannyJNI.granny_pwngb32333_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo23352s() {
        long granny_pwngb32333_vertex_Position_get = grannyJNI.granny_pwngb32333_vertex_Position_get(this.swigCPtr);
        if (granny_pwngb32333_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngb32333_vertex_Position_get, false);
    }

    /* renamed from: a */
    public void mo23339a(C2382ef efVar) {
        grannyJNI.granny_pwngb32333_vertex_BoneWeights_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cP */
    public C2382ef mo23343cP() {
        long granny_pwngb32333_vertex_BoneWeights_get = grannyJNI.granny_pwngb32333_vertex_BoneWeights_get(this.swigCPtr);
        if (granny_pwngb32333_vertex_BoneWeights_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwngb32333_vertex_BoneWeights_get, false);
    }

    /* renamed from: b */
    public void mo23341b(C2382ef efVar) {
        grannyJNI.granny_pwngb32333_vertex_BoneIndices_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cQ */
    public C2382ef mo23344cQ() {
        long granny_pwngb32333_vertex_BoneIndices_get = grannyJNI.granny_pwngb32333_vertex_BoneIndices_get(this.swigCPtr);
        if (granny_pwngb32333_vertex_BoneIndices_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwngb32333_vertex_BoneIndices_get, false);
    }

    /* renamed from: h */
    public void mo23350h(C3159oW oWVar) {
        grannyJNI.granny_pwngb32333_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo23345cR() {
        long granny_pwngb32333_vertex_Normal_get = grannyJNI.granny_pwngb32333_vertex_Normal_get(this.swigCPtr);
        if (granny_pwngb32333_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngb32333_vertex_Normal_get, false);
    }

    /* renamed from: i */
    public void mo23351i(C3159oW oWVar) {
        grannyJNI.granny_pwngb32333_vertex_Tangent_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: gD */
    public C3159oW mo23349gD() {
        long granny_pwngb32333_vertex_Tangent_get = grannyJNI.granny_pwngb32333_vertex_Tangent_get(this.swigCPtr);
        if (granny_pwngb32333_vertex_Tangent_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngb32333_vertex_Tangent_get, false);
    }

    /* renamed from: w */
    public void mo23353w(C3159oW oWVar) {
        grannyJNI.granny_pwngb32333_vertex_Binormal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW amC() {
        long granny_pwngb32333_vertex_Binormal_get = grannyJNI.granny_pwngb32333_vertex_Binormal_get(this.swigCPtr);
        if (granny_pwngb32333_vertex_Binormal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngb32333_vertex_Binormal_get, false);
    }

    /* renamed from: fL */
    public C4107za mo23347fL(int i) {
        long granny_pwngb32333_vertex_get = grannyJNI.granny_pwngb32333_vertex_get(this.swigCPtr, i);
        if (granny_pwngb32333_vertex_get == 0) {
            return null;
        }
        return new C4107za(granny_pwngb32333_vertex_get, false);
    }
}
