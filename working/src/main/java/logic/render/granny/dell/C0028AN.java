package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.AN */
/* compiled from: a */
public class C0028AN {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0028AN(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0028AN() {
        this(grannyJNI.new_granny_stack_allocator(), true);
    }

    /* renamed from: a */
    public static long m373a(C0028AN an) {
        if (an == null) {
            return 0;
        }
        return an.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_stack_allocator(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: gn */
    public void mo264gn(int i) {
        grannyJNI.granny_stack_allocator_UnitSize_set(this.swigCPtr, i);
    }

    public int axS() {
        return grannyJNI.granny_stack_allocator_UnitSize_get(this.swigCPtr);
    }

    /* renamed from: go */
    public void mo265go(int i) {
        grannyJNI.granny_stack_allocator_UnitsPerBlock_set(this.swigCPtr, i);
    }

    public int axT() {
        return grannyJNI.granny_stack_allocator_UnitsPerBlock_get(this.swigCPtr);
    }

    /* renamed from: gp */
    public void mo266gp(int i) {
        grannyJNI.granny_stack_allocator_TotalUsedUnitCount_set(this.swigCPtr, i);
    }

    public int axU() {
        return grannyJNI.granny_stack_allocator_TotalUsedUnitCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo261b(C0007AA aa) {
        grannyJNI.granny_stack_allocator_LastBlock_set(this.swigCPtr, C0007AA.m7a(aa));
    }

    public C0007AA axV() {
        long granny_stack_allocator_LastBlock_get = grannyJNI.granny_stack_allocator_LastBlock_get(this.swigCPtr);
        if (granny_stack_allocator_LastBlock_get == 0) {
            return null;
        }
        return new C0007AA(granny_stack_allocator_LastBlock_get, false);
    }

    /* renamed from: gq */
    public void mo267gq(int i) {
        grannyJNI.granny_stack_allocator_MaxUnits_set(this.swigCPtr, i);
    }

    public int axW() {
        return grannyJNI.granny_stack_allocator_MaxUnits_get(this.swigCPtr);
    }

    /* renamed from: gr */
    public void mo268gr(int i) {
        grannyJNI.granny_stack_allocator_ActiveBlocks_set(this.swigCPtr, i);
    }

    public int axX() {
        return grannyJNI.granny_stack_allocator_ActiveBlocks_get(this.swigCPtr);
    }

    /* renamed from: gs */
    public void mo269gs(int i) {
        grannyJNI.granny_stack_allocator_MaxActiveBlocks_set(this.swigCPtr, i);
    }

    public int axY() {
        return grannyJNI.granny_stack_allocator_MaxActiveBlocks_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo252a(C6508aoM aom) {
        grannyJNI.granny_stack_allocator_BlockDirectory_set(this.swigCPtr, C6508aoM.m24414c(aom));
    }

    public C6508aoM axZ() {
        long granny_stack_allocator_BlockDirectory_get = grannyJNI.granny_stack_allocator_BlockDirectory_get(this.swigCPtr);
        if (granny_stack_allocator_BlockDirectory_get == 0) {
            return null;
        }
        return new C6508aoM(granny_stack_allocator_BlockDirectory_get, false);
    }

    /* renamed from: gt */
    public C0028AN mo270gt(int i) {
        long granny_stack_allocator_get = grannyJNI.granny_stack_allocator_get(this.swigCPtr, i);
        if (granny_stack_allocator_get == 0) {
            return null;
        }
        return new C0028AN(granny_stack_allocator_get, false);
    }
}
