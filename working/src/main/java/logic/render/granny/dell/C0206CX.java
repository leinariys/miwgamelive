package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.CX */
/* compiled from: a */
public class C0206CX {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0206CX(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0206CX() {
        this(grannyJNI.new_granny_triangle_intersection(), true);
    }

    /* renamed from: a */
    public static long m1802a(C0206CX cx) {
        if (cx == null) {
            return 0;
        }
        return cx.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_triangle_intersection(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public float getT() {
        return grannyJNI.granny_triangle_intersection_T_get(this.swigCPtr);
    }

    public void setT(float f) {
        grannyJNI.granny_triangle_intersection_T_set(this.swigCPtr, f);
    }

    /* renamed from: ez */
    public void mo1129ez(float f) {
        grannyJNI.granny_triangle_intersection_TriangleU_set(this.swigCPtr, f);
    }

    public float aDD() {
        return grannyJNI.granny_triangle_intersection_TriangleU_get(this.swigCPtr);
    }

    /* renamed from: eA */
    public void mo1128eA(float f) {
        grannyJNI.granny_triangle_intersection_TriangleV_set(this.swigCPtr, f);
    }

    public float aDE() {
        return grannyJNI.granny_triangle_intersection_TriangleV_get(this.swigCPtr);
    }

    /* renamed from: y */
    public void mo1134y(C3159oW oWVar) {
        grannyJNI.granny_triangle_intersection_EdgeU_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW aDF() {
        long granny_triangle_intersection_EdgeU_get = grannyJNI.granny_triangle_intersection_EdgeU_get(this.swigCPtr);
        if (granny_triangle_intersection_EdgeU_get == 0) {
            return null;
        }
        return new C3159oW(granny_triangle_intersection_EdgeU_get, false);
    }

    /* renamed from: z */
    public void mo1135z(C3159oW oWVar) {
        grannyJNI.granny_triangle_intersection_EdgeV_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW aDG() {
        long granny_triangle_intersection_EdgeV_get = grannyJNI.granny_triangle_intersection_EdgeV_get(this.swigCPtr);
        if (granny_triangle_intersection_EdgeV_get == 0) {
            return null;
        }
        return new C3159oW(granny_triangle_intersection_EdgeV_get, false);
    }

    /* renamed from: bS */
    public void mo1126bS(boolean z) {
        grannyJNI.granny_triangle_intersection_Backfacing_set(this.swigCPtr, z);
    }

    public boolean aDH() {
        return grannyJNI.granny_triangle_intersection_Backfacing_get(this.swigCPtr);
    }

    /* renamed from: gE */
    public C0206CX mo1131gE(int i) {
        long granny_triangle_intersection_get = grannyJNI.granny_triangle_intersection_get(this.swigCPtr, i);
        if (granny_triangle_intersection_get == 0) {
            return null;
        }
        return new C0206CX(granny_triangle_intersection_get, false);
    }
}
