package logic.render.granny.dell;

import logic.render.granny.C2382ef;
import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.RR */
/* compiled from: a */
public class C1178RR {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1178RR(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1178RR() {
        this(grannyJNI.new_granny_pwng3233_vertex(), true);
    }

    /* renamed from: a */
    public static long m9166a(C1178RR rr) {
        if (rr == null) {
            return 0;
        }
        return rr.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pwng3233_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo5196b(C3159oW oWVar) {
        grannyJNI.granny_pwng3233_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo5206s() {
        long granny_pwng3233_vertex_Position_get = grannyJNI.granny_pwng3233_vertex_Position_get(this.swigCPtr);
        if (granny_pwng3233_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwng3233_vertex_Position_get, false);
    }

    /* renamed from: a */
    public void mo5194a(C2382ef efVar) {
        grannyJNI.granny_pwng3233_vertex_BoneWeights_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cP */
    public C2382ef mo5197cP() {
        long granny_pwng3233_vertex_BoneWeights_get = grannyJNI.granny_pwng3233_vertex_BoneWeights_get(this.swigCPtr);
        if (granny_pwng3233_vertex_BoneWeights_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwng3233_vertex_BoneWeights_get, false);
    }

    /* renamed from: b */
    public void mo5195b(C2382ef efVar) {
        grannyJNI.granny_pwng3233_vertex_BoneIndices_set(this.swigCPtr, C2382ef.m29966c(efVar));
    }

    /* renamed from: cQ */
    public C2382ef mo5198cQ() {
        long granny_pwng3233_vertex_BoneIndices_get = grannyJNI.granny_pwng3233_vertex_BoneIndices_get(this.swigCPtr);
        if (granny_pwng3233_vertex_BoneIndices_get == 0) {
            return null;
        }
        return new C2382ef(granny_pwng3233_vertex_BoneIndices_get, false);
    }

    /* renamed from: h */
    public void mo5203h(C3159oW oWVar) {
        grannyJNI.granny_pwng3233_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo5199cR() {
        long granny_pwng3233_vertex_Normal_get = grannyJNI.granny_pwng3233_vertex_Normal_get(this.swigCPtr);
        if (granny_pwng3233_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwng3233_vertex_Normal_get, false);
    }

    /* renamed from: i */
    public void mo5204i(C3159oW oWVar) {
        grannyJNI.granny_pwng3233_vertex_Tangent_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: gD */
    public C3159oW mo5202gD() {
        long granny_pwng3233_vertex_Tangent_get = grannyJNI.granny_pwng3233_vertex_Tangent_get(this.swigCPtr);
        if (granny_pwng3233_vertex_Tangent_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwng3233_vertex_Tangent_get, false);
    }

    /* renamed from: ne */
    public C1178RR mo5205ne(int i) {
        long granny_pwng3233_vertex_get = grannyJNI.granny_pwng3233_vertex_get(this.swigCPtr, i);
        if (granny_pwng3233_vertex_get == 0) {
            return null;
        }
        return new C1178RR(granny_pwng3233_vertex_get, false);
    }
}
