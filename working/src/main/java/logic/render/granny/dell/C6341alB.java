package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.alB  reason: case insensitive filesystem */
/* compiled from: a */
public class C6341alB {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6341alB(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6341alB() {
        this(grannyJNI.new_granny_curve_data_header(), true);
    }

    /* renamed from: b */
    public static long m23687b(C6341alB alb) {
        if (alb == null) {
            return 0;
        }
        return alb.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve_data_header(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: u */
    public void mo14639u(short s) {
        grannyJNI.granny_curve_data_header_Format_set(this.swigCPtr, s);
    }

    public short cig() {
        return grannyJNI.granny_curve_data_header_Format_get(this.swigCPtr);
    }

    /* renamed from: v */
    public void mo14640v(short s) {
        grannyJNI.granny_curve_data_header_Degree_set(this.swigCPtr, s);
    }

    public short cih() {
        return grannyJNI.granny_curve_data_header_Degree_get(this.swigCPtr);
    }

    /* renamed from: st */
    public C6341alB mo14638st(int i) {
        long granny_curve_data_header_get = grannyJNI.granny_curve_data_header_get(this.swigCPtr, i);
        if (granny_curve_data_header_get == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_header_get, false);
    }
}
