package logic.render.granny.dell;

import logic.render.granny.C2062bl;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.ov */
/* compiled from: a */
public class C3190ov {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3190ov(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3190ov() {
        this(grannyJNI.new_granny_curve_data_d4n_k16u_c15u(), true);
    }

    /* renamed from: a */
    public static long m36910a(C3190ov ovVar) {
        if (ovVar == null) {
            return 0;
        }
        return ovVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_curve_data_d4n_k16u_c15u(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo21086a(C6341alB alb) {
        grannyJNI.granny_curve_data_d4n_k16u_c15u_CurveDataHeader_set(this.swigCPtr, C6341alB.m23687b(alb));
    }

    /* renamed from: Qa */
    public C6341alB mo21081Qa() {
        long granny_curve_data_d4n_k16u_c15u_CurveDataHeader_get = grannyJNI.granny_curve_data_d4n_k16u_c15u_CurveDataHeader_get(this.swigCPtr);
        if (granny_curve_data_d4n_k16u_c15u_CurveDataHeader_get == 0) {
            return null;
        }
        return new C6341alB(granny_curve_data_d4n_k16u_c15u_CurveDataHeader_get, false);
    }

    /* renamed from: dA */
    public void mo21089dA(int i) {
        grannyJNI.granny_curve_data_d4n_k16u_c15u_ScaleOffsetTableEntries_set(this.swigCPtr, i);
    }

    /* renamed from: TY */
    public int mo21083TY() {
        return grannyJNI.granny_curve_data_d4n_k16u_c15u_ScaleOffsetTableEntries_get(this.swigCPtr);
    }

    /* renamed from: cL */
    public void mo21088cL(float f) {
        grannyJNI.granny_curve_data_d4n_k16u_c15u_OneOverKnotScale_set(this.swigCPtr, f);
    }

    /* renamed from: TZ */
    public float mo21084TZ() {
        return grannyJNI.granny_curve_data_d4n_k16u_c15u_OneOverKnotScale_get(this.swigCPtr);
    }

    /* renamed from: di */
    public void mo21092di(int i) {
        grannyJNI.granny_curve_data_d4n_k16u_c15u_KnotControlCount_set(this.swigCPtr, i);
    }

    /* renamed from: Qe */
    public int mo21082Qe() {
        return grannyJNI.granny_curve_data_d4n_k16u_c15u_KnotControlCount_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo21087b(C2062bl blVar) {
        grannyJNI.granny_curve_data_d4n_k16u_c15u_KnotsControls_set(this.swigCPtr, C2062bl.m27965a(blVar));
    }

    /* renamed from: Ua */
    public C2062bl mo21085Ua() {
        long granny_curve_data_d4n_k16u_c15u_KnotsControls_get = grannyJNI.granny_curve_data_d4n_k16u_c15u_KnotsControls_get(this.swigCPtr);
        if (granny_curve_data_d4n_k16u_c15u_KnotsControls_get == 0) {
            return null;
        }
        return new C2062bl(granny_curve_data_d4n_k16u_c15u_KnotsControls_get, false);
    }

    /* renamed from: dB */
    public C3190ov mo21090dB(int i) {
        long granny_curve_data_d4n_k16u_c15u_get = grannyJNI.granny_curve_data_d4n_k16u_c15u_get(this.swigCPtr, i);
        if (granny_curve_data_d4n_k16u_c15u_get == 0) {
            return null;
        }
        return new C3190ov(granny_curve_data_d4n_k16u_c15u_get, false);
    }
}
