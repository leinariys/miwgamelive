package logic.render.granny.dell;

import logic.render.granny.C1731Za;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aAi  reason: case insensitive filesystem */
/* compiled from: a */
public class C5204aAi {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5204aAi(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5204aAi() {
        this(grannyJNI.new_granny_defined_type(), true);
    }

    /* renamed from: b */
    public static long m12619b(C5204aAi aai) {
        if (aai == null) {
            return 0;
        }
        return aai.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_defined_type(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: wK */
    public void mo7676wK(int i) {
        grannyJNI.granny_defined_type_UIID_set(this.swigCPtr, i);
    }

    public int cHp() {
        return grannyJNI.granny_defined_type_UIID_get(this.swigCPtr);
    }

    public String getName() {
        return grannyJNI.granny_defined_type_Name_get(this.swigCPtr);
    }

    public void setName(String str) {
        grannyJNI.granny_defined_type_Name_set(this.swigCPtr, str);
    }

    /* renamed from: bm */
    public void mo7669bm(C1731Za za) {
        grannyJNI.granny_defined_type_Definition_set(this.swigCPtr, C1731Za.m12101bi(za));
    }

    public C1731Za cHq() {
        long granny_defined_type_Definition_get = grannyJNI.granny_defined_type_Definition_get(this.swigCPtr);
        if (granny_defined_type_Definition_get == 0) {
            return null;
        }
        return new C1731Za(granny_defined_type_Definition_get, false);
    }

    /* renamed from: wL */
    public C5204aAi mo7677wL(int i) {
        long granny_defined_type_get = grannyJNI.granny_defined_type_get(this.swigCPtr, i);
        if (granny_defined_type_get == 0) {
            return null;
        }
        return new C5204aAi(granny_defined_type_get, false);
    }
}
