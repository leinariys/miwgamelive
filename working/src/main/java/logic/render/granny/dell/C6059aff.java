package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aff  reason: case insensitive filesystem */
/* compiled from: a */
public class C6059aff {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6059aff(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6059aff() {
        this(grannyJNI.new_granny_unbound_weight(), true);
    }

    /* renamed from: a */
    public static long m21578a(C6059aff aff) {
        if (aff == null) {
            return 0;
        }
        return aff.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_unbound_weight(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getName() {
        return grannyJNI.granny_unbound_weight_Name_get(this.swigCPtr);
    }

    public void setName(String str) {
        grannyJNI.granny_unbound_weight_Name_set(this.swigCPtr, str);
    }

    public float getWeight() {
        return grannyJNI.granny_unbound_weight_Weight_get(this.swigCPtr);
    }

    public void setWeight(float f) {
        grannyJNI.granny_unbound_weight_Weight_set(this.swigCPtr, f);
    }

    /* renamed from: qu */
    public C6059aff mo13276qu(int i) {
        long granny_unbound_weight_get = grannyJNI.granny_unbound_weight_get(this.swigCPtr, i);
        if (granny_unbound_weight_get == 0) {
            return null;
        }
        return new C6059aff(granny_unbound_weight_get, false);
    }
}
