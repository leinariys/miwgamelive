package logic.render.granny.dell;

/* renamed from: a.oZ */
/* compiled from: a */
public final class C3163oZ {
    public static final C3163oZ aQK = new C3163oZ("GrannyDagNodeType_Leaf_AnimationBlend");
    public static final C3163oZ aQL = new C3163oZ("GrannyDagNodeType_Leaf_LocalPose");
    public static final C3163oZ aQM = new C3163oZ("GrannyDagNodeType_Leaf_Callback");
    public static final C3163oZ aQN = new C3163oZ("GrannyDagNodeType_OnePastLastLeafType");
    public static final C3163oZ aQO = new C3163oZ("GrannyDagNodeType_Node_Crossfade");
    public static final C3163oZ aQP = new C3163oZ("GrannyDagNodeType_Node_WeightedBlend");
    public static final C3163oZ aQQ = new C3163oZ("GrannyDagNodeType_OnePastLast");
    private static C3163oZ[] aQR = {aQK, aQL, aQM, aQN, aQO, aQP, aQQ};

    /* renamed from: pF */
    private static int f8772pF = 0;

    /* renamed from: pG */
    private final int f8773pG;

    /* renamed from: pH */
    private final String f8774pH;

    private C3163oZ(String str) {
        this.f8774pH = str;
        int i = f8772pF;
        f8772pF = i + 1;
        this.f8773pG = i;
    }

    private C3163oZ(String str, int i) {
        this.f8774pH = str;
        this.f8773pG = i;
        f8772pF = i + 1;
    }

    private C3163oZ(String str, C3163oZ oZVar) {
        this.f8774pH = str;
        this.f8773pG = oZVar.f8773pG;
        f8772pF = this.f8773pG + 1;
    }

    /* renamed from: dI */
    public static C3163oZ m36745dI(int i) {
        if (i < aQR.length && i >= 0 && aQR[i].f8773pG == i) {
            return aQR[i];
        }
        for (int i2 = 0; i2 < aQR.length; i2++) {
            if (aQR[i2].f8773pG == i) {
                return aQR[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C3163oZ.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f8773pG;
    }

    public String toString() {
        return this.f8774pH;
    }
}
