package logic.render.granny.dell;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aZ */
/* compiled from: a */
public class C1833aZ {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1833aZ(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1833aZ() {
        this(grannyJNI.new_granny_stat_hud_alloc_point(), true);
    }

    /* renamed from: a */
    public static long m19350a(C1833aZ aZVar) {
        if (aZVar == null) {
            return 0;
        }
        return aZVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_stat_hud_alloc_point(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: A */
    public void mo12117A(String str) {
        grannyJNI.granny_stat_hud_alloc_point_SourceFilename_set(this.swigCPtr, str);
    }

    /* renamed from: fU */
    public String mo12124fU() {
        return grannyJNI.granny_stat_hud_alloc_point_SourceFilename_get(this.swigCPtr);
    }

    /* renamed from: O */
    public void mo12118O(int i) {
        grannyJNI.granny_stat_hud_alloc_point_SourceLineNumber_set(this.swigCPtr, i);
    }

    /* renamed from: fV */
    public int mo12125fV() {
        return grannyJNI.granny_stat_hud_alloc_point_SourceLineNumber_get(this.swigCPtr);
    }

    /* renamed from: P */
    public void mo12119P(int i) {
        grannyJNI.granny_stat_hud_alloc_point_AllocationCount_set(this.swigCPtr, i);
    }

    /* renamed from: fW */
    public int mo12126fW() {
        return grannyJNI.granny_stat_hud_alloc_point_AllocationCount_get(this.swigCPtr);
    }

    /* renamed from: Q */
    public void mo12120Q(int i) {
        grannyJNI.granny_stat_hud_alloc_point_BytesRequested_set(this.swigCPtr, i);
    }

    /* renamed from: fX */
    public int mo12127fX() {
        return grannyJNI.granny_stat_hud_alloc_point_BytesRequested_get(this.swigCPtr);
    }

    /* renamed from: R */
    public void mo12121R(int i) {
        grannyJNI.granny_stat_hud_alloc_point_BytesAllocated_set(this.swigCPtr, i);
    }

    /* renamed from: fY */
    public int mo12128fY() {
        return grannyJNI.granny_stat_hud_alloc_point_BytesAllocated_get(this.swigCPtr);
    }

    /* renamed from: S */
    public C1833aZ mo12122S(int i) {
        long granny_stat_hud_alloc_point_get = grannyJNI.granny_stat_hud_alloc_point_get(this.swigCPtr, i);
        if (granny_stat_hud_alloc_point_get == 0) {
            return null;
        }
        return new C1833aZ(granny_stat_hud_alloc_point_get, false);
    }
}
