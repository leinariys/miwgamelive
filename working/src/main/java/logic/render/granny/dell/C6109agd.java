package logic.render.granny.dell;

import logic.render.granny.C3159oW;
import utaikodom.render.granny.grannyJNI;

/* renamed from: a.agd  reason: case insensitive filesystem */
/* compiled from: a */
public class C6109agd {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6109agd(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6109agd() {
        this(grannyJNI.new_granny_pwngb31333_vertex(), true);
    }

    /* renamed from: a */
    public static long m22091a(C6109agd agd) {
        if (agd == null) {
            return 0;
        }
        return agd.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_pwngb31333_vertex(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo13441b(C3159oW oWVar) {
        grannyJNI.granny_pwngb31333_vertex_Position_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: s */
    public C3159oW mo13451s() {
        long granny_pwngb31333_vertex_Position_get = grannyJNI.granny_pwngb31333_vertex_Position_get(this.swigCPtr);
        if (granny_pwngb31333_vertex_Position_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngb31333_vertex_Position_get, false);
    }

    /* renamed from: n */
    public void mo13449n(long j) {
        grannyJNI.granny_pwngb31333_vertex_BoneIndex_set(this.swigCPtr, j);
    }

    /* renamed from: gC */
    public long mo13445gC() {
        return grannyJNI.granny_pwngb31333_vertex_BoneIndex_get(this.swigCPtr);
    }

    /* renamed from: h */
    public void mo13447h(C3159oW oWVar) {
        grannyJNI.granny_pwngb31333_vertex_Normal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: cR */
    public C3159oW mo13442cR() {
        long granny_pwngb31333_vertex_Normal_get = grannyJNI.granny_pwngb31333_vertex_Normal_get(this.swigCPtr);
        if (granny_pwngb31333_vertex_Normal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngb31333_vertex_Normal_get, false);
    }

    /* renamed from: i */
    public void mo13448i(C3159oW oWVar) {
        grannyJNI.granny_pwngb31333_vertex_Tangent_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    /* renamed from: gD */
    public C3159oW mo13446gD() {
        long granny_pwngb31333_vertex_Tangent_get = grannyJNI.granny_pwngb31333_vertex_Tangent_get(this.swigCPtr);
        if (granny_pwngb31333_vertex_Tangent_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngb31333_vertex_Tangent_get, false);
    }

    /* renamed from: w */
    public void mo13452w(C3159oW oWVar) {
        grannyJNI.granny_pwngb31333_vertex_Binormal_set(this.swigCPtr, C3159oW.m36741q(oWVar));
    }

    public C3159oW amC() {
        long granny_pwngb31333_vertex_Binormal_get = grannyJNI.granny_pwngb31333_vertex_Binormal_get(this.swigCPtr);
        if (granny_pwngb31333_vertex_Binormal_get == 0) {
            return null;
        }
        return new C3159oW(granny_pwngb31333_vertex_Binormal_get, false);
    }

    /* renamed from: qE */
    public C6109agd mo13450qE(int i) {
        long granny_pwngb31333_vertex_get = grannyJNI.granny_pwngb31333_vertex_get(this.swigCPtr, i);
        if (granny_pwngb31333_vertex_get == 0) {
            return null;
        }
        return new C6109agd(granny_pwngb31333_vertex_get, false);
    }
}
