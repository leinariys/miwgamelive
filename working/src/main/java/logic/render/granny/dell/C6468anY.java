package logic.render.granny.dell;

/* renamed from: a.anY  reason: case insensitive filesystem */
/* compiled from: a */
public final class C6468anY {
    public static final C6468anY gfQ = new C6468anY("GrannyNoAccumulation");
    public static final C6468anY gfR = new C6468anY("GrannyConstantExtractionAccumulation");
    public static final C6468anY gfS = new C6468anY("GrannyVariableDeltaAccumulation");
    private static C6468anY[] gfT = {gfQ, gfR, gfS};

    /* renamed from: pF */
    private static int f4974pF = 0;

    /* renamed from: pG */
    private final int f4975pG;

    /* renamed from: pH */
    private final String f4976pH;

    private C6468anY(String str) {
        this.f4976pH = str;
        int i = f4974pF;
        f4974pF = i + 1;
        this.f4975pG = i;
    }

    private C6468anY(String str, int i) {
        this.f4976pH = str;
        this.f4975pG = i;
        f4974pF = i + 1;
    }

    private C6468anY(String str, C6468anY any) {
        this.f4976pH = str;
        this.f4975pG = any.f4975pG;
        f4974pF = this.f4975pG + 1;
    }

    /* renamed from: sX */
    public static C6468anY m24201sX(int i) {
        if (i < gfT.length && i >= 0 && gfT[i].f4975pG == i) {
            return gfT[i];
        }
        for (int i2 = 0; i2 < gfT.length; i2++) {
            if (gfT[i2].f4975pG == i) {
                return gfT[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C6468anY.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f4975pG;
    }

    public String toString() {
        return this.f4976pH;
    }
}
