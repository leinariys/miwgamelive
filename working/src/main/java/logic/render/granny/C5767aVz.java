package logic.render.granny;

import utaikodom.render.granny.grannyJNI;

/* renamed from: a.aVz  reason: case insensitive filesystem */
/* compiled from: a */
public class C5767aVz {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5767aVz(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5767aVz() {
        this(grannyJNI.new_granny_bone(), true);
    }

    /* renamed from: b */
    public static long m19225b(C5767aVz avz) {
        if (avz == null) {
            return 0;
        }
        return avz.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            grannyJNI.delete_granny_bone(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getName() {
        return grannyJNI.granny_bone_Name_get(this.swigCPtr);
    }

    public void setName(String str) {
        grannyJNI.granny_bone_Name_set(this.swigCPtr, str);
    }

    /* renamed from: Bf */
    public void mo11988Bf(int i) {
        grannyJNI.granny_bone_ParentIndex_set(this.swigCPtr, i);
    }

    public int dBV() {
        return grannyJNI.granny_bone_ParentIndex_get(this.swigCPtr);
    }

    /* renamed from: g */
    public void mo11999g(C5515aMh amh) {
        grannyJNI.granny_bone_LocalTransform_set(this.swigCPtr, C5515aMh.m16438f(amh));
    }

    public C5515aMh dBW() {
        long granny_bone_LocalTransform_get = grannyJNI.granny_bone_LocalTransform_get(this.swigCPtr);
        if (granny_bone_LocalTransform_get == 0) {
            return null;
        }
        return new C5515aMh(granny_bone_LocalTransform_get, false);
    }

    /* renamed from: f */
    public void mo11996f(C6799atr atr) {
        grannyJNI.granny_bone_InverseWorld4x4_set(this.swigCPtr, C6799atr.m26063e(atr));
    }

    public C6799atr dBX() {
        long granny_bone_InverseWorld4x4_get = grannyJNI.granny_bone_InverseWorld4x4_get(this.swigCPtr);
        if (granny_bone_InverseWorld4x4_get == 0) {
            return null;
        }
        return new C6799atr(granny_bone_InverseWorld4x4_get, false);
    }

    /* renamed from: fl */
    public void mo11998fl(float f) {
        grannyJNI.granny_bone_LODError_set(this.swigCPtr, f);
    }

    public float aQD() {
        return grannyJNI.granny_bone_LODError_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo11990a(aSM asm) {
        grannyJNI.granny_bone_ExtendedData_set(this.swigCPtr, aSM.m18172c(asm));
    }

    /* renamed from: kM */
    public aSM mo12001kM() {
        long granny_bone_ExtendedData_get = grannyJNI.granny_bone_ExtendedData_get(this.swigCPtr);
        if (granny_bone_ExtendedData_get == 0) {
            return null;
        }
        return new aSM(granny_bone_ExtendedData_get, false);
    }

    /* renamed from: Bg */
    public C5767aVz mo11989Bg(int i) {
        long granny_bone_get = grannyJNI.granny_bone_get(this.swigCPtr, i);
        if (granny_bone_get == 0) {
            return null;
        }
        return new C5767aVz(granny_bone_get, false);
    }
}
