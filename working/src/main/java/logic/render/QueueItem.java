package logic.render;

/* renamed from: a.Km */
/* compiled from: a */
public class QueueItem<F, L> {
    F first;
    L last;

    public QueueItem(F first, L last) {
        this.first = first;
        this.last = last;
    }

    public F getFirst() {
        return this.first;
    }

    /* renamed from: S */
    public void setFirst(F first) {
        this.first = first;
    }

    public L getLast() {
        return this.last;
    }

    /* renamed from: T */
    public void setLast(L last) {
        this.last = last;
    }

    public String toString() {
        return "[" + this.first + "," + this.last + "]";
    }
}
