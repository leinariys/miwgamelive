package logic.render;

import logic.render.granny.AdapterGrannyJNI;
import logic.render.granny.C6900avo;

/* renamed from: a.aCF */
/* compiled from: a */
public class aCF {
    public static String position = "Position";
    public static String normal = "Normal";
    public static String tangent = "Tangent";
    public static String binormal = "Binormal";
    public static String tangentBinormalCross = "TangentBinormalCross";
    public static String boneWeights = "BoneWeights";
    public static String boneIndices = "BoneIndices";
    public static String diffuseColor = "DiffuseColor";
    public static String specularColor = "SpecularColor";
    public static String textureCoordinates = "TextureCoordinates";
    public static String vertexMorphCurve = "VertexMorphCurve";

    /* renamed from: b */
    public static String[] m13142b(C6900avo avo, int i) {
        String[] strArr = new String[i];
        for (int i2 = 0; i2 < i; i2++) {
            strArr[i2] = AdapterGrannyJNI.m3941a(avo, i2);
        }
        return strArr;
    }
}
