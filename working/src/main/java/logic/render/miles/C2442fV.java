package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.fV */
/* compiled from: a */
public class C2442fV {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2442fV(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C2442fV() {
        this(MilesBridgeJNI.new_DPINFO_TYPE(), true);
    }

    /* renamed from: a */
    public static long m31084a(C2442fV fVVar) {
        if (fVVar == null) {
            return 0;
        }
        return fVVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_DPINFO_TYPE(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo18691a(C2687ia iaVar) {
        MilesBridgeJNI.DPINFO_TYPE_MSS_mixer_flush_set(this.swigCPtr, C2687ia.m33424b(iaVar));
    }

    /* renamed from: uD */
    public C2687ia mo18695uD() {
        long DPINFO_TYPE_MSS_mixer_flush_get = MilesBridgeJNI.DPINFO_TYPE_MSS_mixer_flush_get(this.swigCPtr);
        if (DPINFO_TYPE_MSS_mixer_flush_get == 0) {
            return null;
        }
        return new C2687ia(DPINFO_TYPE_MSS_mixer_flush_get, false);
    }

    /* renamed from: a */
    public void mo18690a(axT axt) {
        MilesBridgeJNI.DPINFO_TYPE_MSS_mixer_copy_set(this.swigCPtr, axT.m27083b(axt));
    }

    /* renamed from: uE */
    public axT mo18696uE() {
        long DPINFO_TYPE_MSS_mixer_copy_get = MilesBridgeJNI.DPINFO_TYPE_MSS_mixer_copy_get(this.swigCPtr);
        if (DPINFO_TYPE_MSS_mixer_copy_get == 0) {
            return null;
        }
        return new axT(DPINFO_TYPE_MSS_mixer_copy_get, false);
    }

    /* renamed from: b */
    public void mo18692b(C2357eQ eQVar) {
        MilesBridgeJNI.DPINFO_TYPE_MSS_mixer_mc_copy_set(this.swigCPtr, C2357eQ.m29536a(eQVar));
    }

    /* renamed from: uF */
    public C2357eQ mo18697uF() {
        long DPINFO_TYPE_MSS_mixer_mc_copy_get = MilesBridgeJNI.DPINFO_TYPE_MSS_mixer_mc_copy_get(this.swigCPtr);
        if (DPINFO_TYPE_MSS_mixer_mc_copy_get == 0) {
            return null;
        }
        return new C2357eQ(DPINFO_TYPE_MSS_mixer_mc_copy_get, false);
    }

    /* renamed from: a */
    public void mo18689a(C5412aIi aii) {
        MilesBridgeJNI.DPINFO_TYPE_MSS_mixer_adpcm_decode_set(this.swigCPtr, C5412aIi.m15456b(aii));
    }

    /* renamed from: uG */
    public C5412aIi mo18698uG() {
        long DPINFO_TYPE_MSS_mixer_adpcm_decode_get = MilesBridgeJNI.DPINFO_TYPE_MSS_mixer_adpcm_decode_get(this.swigCPtr);
        if (DPINFO_TYPE_MSS_mixer_adpcm_decode_get == 0) {
            return null;
        }
        return new C5412aIi(DPINFO_TYPE_MSS_mixer_adpcm_decode_get, false);
    }
}
