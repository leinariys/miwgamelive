package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aSc  reason: case insensitive filesystem */
/* compiled from: a */
public class C5666aSc {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5666aSc(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5666aSc() {
        this(MilesBridgeJNI.new_REVERB_INFO(), true);
    }

    /* renamed from: b */
    public static long m18218b(C5666aSc asc) {
        if (asc == null) {
            return 0;
        }
        return asc.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_REVERB_INFO(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo11391b(aNR anr) {
        MilesBridgeJNI.REVERB_INFO_u_set(this.swigCPtr, aNR.m16565a(anr));
    }

    public aNR dua() {
        long REVERB_INFO_u_get = MilesBridgeJNI.REVERB_INFO_u_get(this.swigCPtr);
        if (REVERB_INFO_u_get == 0) {
            return null;
        }
        return new aNR(REVERB_INFO_u_get, false);
    }

    /* renamed from: b */
    public void mo11390b(C1758Zy zy) {
        MilesBridgeJNI.REVERB_INFO_c_set(this.swigCPtr, C1758Zy.m12259a(zy));
    }

    public C1758Zy dub() {
        long REVERB_INFO_c_get = MilesBridgeJNI.REVERB_INFO_c_get(this.swigCPtr);
        if (REVERB_INFO_c_get == 0) {
            return null;
        }
        return new C1758Zy(REVERB_INFO_c_get, false);
    }
}
