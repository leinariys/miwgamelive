package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aCS */
/* compiled from: a */
public class aCS {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aCS(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aCS() {
        this(MilesBridgeJNI.new_SMPATTRIBS(), true);
    }

    /* renamed from: a */
    public static long m13229a(aCS acs) {
        if (acs == null) {
            return 0;
        }
        return acs.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_SMPATTRIBS(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: lO */
    public void mo8093lO(float f) {
        MilesBridgeJNI.SMPATTRIBS_cone_inner_angle_set(this.swigCPtr, f);
    }

    public float cQz() {
        return MilesBridgeJNI.SMPATTRIBS_cone_inner_angle_get(this.swigCPtr);
    }

    /* renamed from: lP */
    public void mo8094lP(float f) {
        MilesBridgeJNI.SMPATTRIBS_cone_outer_angle_set(this.swigCPtr, f);
    }

    public float cQA() {
        return MilesBridgeJNI.SMPATTRIBS_cone_outer_angle_get(this.swigCPtr);
    }

    /* renamed from: lQ */
    public void mo8095lQ(float f) {
        MilesBridgeJNI.SMPATTRIBS_cone_outer_volume_set(this.swigCPtr, f);
    }

    public float cQB() {
        return MilesBridgeJNI.SMPATTRIBS_cone_outer_volume_get(this.swigCPtr);
    }

    /* renamed from: lR */
    public void mo8096lR(float f) {
        MilesBridgeJNI.SMPATTRIBS_max_distance_set(this.swigCPtr, f);
    }

    public float cQC() {
        return MilesBridgeJNI.SMPATTRIBS_max_distance_get(this.swigCPtr);
    }

    /* renamed from: lS */
    public void mo8097lS(float f) {
        MilesBridgeJNI.SMPATTRIBS_min_distance_set(this.swigCPtr, f);
    }

    public float cQD() {
        return MilesBridgeJNI.SMPATTRIBS_min_distance_get(this.swigCPtr);
    }

    /* renamed from: xl */
    public void mo8120xl(int i) {
        MilesBridgeJNI.SMPATTRIBS_auto_3D_wet_atten_set(this.swigCPtr, i);
    }

    public int cQE() {
        return MilesBridgeJNI.SMPATTRIBS_auto_3D_wet_atten_get(this.swigCPtr);
    }

    /* renamed from: ox */
    public void mo8119ox(int i) {
        MilesBridgeJNI.SMPATTRIBS_is_3D_set(this.swigCPtr, i);
    }

    public int bzs() {
        return MilesBridgeJNI.SMPATTRIBS_is_3D_get(this.swigCPtr);
    }

    /* renamed from: lT */
    public void mo8098lT(float f) {
        MilesBridgeJNI.SMPATTRIBS_X_pos_set(this.swigCPtr, f);
    }

    public float cQF() {
        return MilesBridgeJNI.SMPATTRIBS_X_pos_get(this.swigCPtr);
    }

    /* renamed from: lU */
    public void mo8099lU(float f) {
        MilesBridgeJNI.SMPATTRIBS_Y_pos_set(this.swigCPtr, f);
    }

    public float cQG() {
        return MilesBridgeJNI.SMPATTRIBS_Y_pos_get(this.swigCPtr);
    }

    /* renamed from: lV */
    public void mo8100lV(float f) {
        MilesBridgeJNI.SMPATTRIBS_Z_pos_set(this.swigCPtr, f);
    }

    public float cQH() {
        return MilesBridgeJNI.SMPATTRIBS_Z_pos_get(this.swigCPtr);
    }

    /* renamed from: lW */
    public void mo8101lW(float f) {
        MilesBridgeJNI.SMPATTRIBS_X_face_set(this.swigCPtr, f);
    }

    public float cQI() {
        return MilesBridgeJNI.SMPATTRIBS_X_face_get(this.swigCPtr);
    }

    /* renamed from: lX */
    public void mo8102lX(float f) {
        MilesBridgeJNI.SMPATTRIBS_Y_face_set(this.swigCPtr, f);
    }

    public float cQJ() {
        return MilesBridgeJNI.SMPATTRIBS_Y_face_get(this.swigCPtr);
    }

    /* renamed from: lY */
    public void mo8103lY(float f) {
        MilesBridgeJNI.SMPATTRIBS_Z_face_set(this.swigCPtr, f);
    }

    public float cQK() {
        return MilesBridgeJNI.SMPATTRIBS_Z_face_get(this.swigCPtr);
    }

    /* renamed from: lZ */
    public void mo8104lZ(float f) {
        MilesBridgeJNI.SMPATTRIBS_X_up_set(this.swigCPtr, f);
    }

    public float cQL() {
        return MilesBridgeJNI.SMPATTRIBS_X_up_get(this.swigCPtr);
    }

    /* renamed from: ma */
    public void mo8105ma(float f) {
        MilesBridgeJNI.SMPATTRIBS_Y_up_set(this.swigCPtr, f);
    }

    public float cQM() {
        return MilesBridgeJNI.SMPATTRIBS_Y_up_get(this.swigCPtr);
    }

    /* renamed from: mb */
    public void mo8106mb(float f) {
        MilesBridgeJNI.SMPATTRIBS_Z_up_set(this.swigCPtr, f);
    }

    public float cQN() {
        return MilesBridgeJNI.SMPATTRIBS_Z_up_get(this.swigCPtr);
    }

    /* renamed from: mc */
    public void mo8107mc(float f) {
        MilesBridgeJNI.SMPATTRIBS_X_meters_per_ms_set(this.swigCPtr, f);
    }

    public float cQO() {
        return MilesBridgeJNI.SMPATTRIBS_X_meters_per_ms_get(this.swigCPtr);
    }

    /* renamed from: md */
    public void mo8108md(float f) {
        MilesBridgeJNI.SMPATTRIBS_Y_meters_per_ms_set(this.swigCPtr, f);
    }

    public float cQP() {
        return MilesBridgeJNI.SMPATTRIBS_Y_meters_per_ms_get(this.swigCPtr);
    }

    /* renamed from: me */
    public void mo8109me(float f) {
        MilesBridgeJNI.SMPATTRIBS_Z_meters_per_ms_set(this.swigCPtr, f);
    }

    public float cQQ() {
        return MilesBridgeJNI.SMPATTRIBS_Z_meters_per_ms_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo8032b(C3196oz ozVar) {
        MilesBridgeJNI.SMPATTRIBS_channel_levels_set(this.swigCPtr, C3196oz.m36970a(ozVar));
    }

    public C3196oz cQR() {
        long SMPATTRIBS_channel_levels_get = MilesBridgeJNI.SMPATTRIBS_channel_levels_get(this.swigCPtr);
        if (SMPATTRIBS_channel_levels_get == 0) {
            return null;
        }
        return new C3196oz(SMPATTRIBS_channel_levels_get, false);
    }

    /* renamed from: xm */
    public void mo8121xm(int i) {
        MilesBridgeJNI.SMPATTRIBS_n_channel_levels_set(this.swigCPtr, i);
    }

    public int cQS() {
        return MilesBridgeJNI.SMPATTRIBS_n_channel_levels_get(this.swigCPtr);
    }

    /* renamed from: il */
    public void mo8090il(float f) {
        MilesBridgeJNI.SMPATTRIBS_exclusion_set(this.swigCPtr, f);
    }

    public float byW() {
        return MilesBridgeJNI.SMPATTRIBS_exclusion_get(this.swigCPtr);
    }

    /* renamed from: ij */
    public void mo8088ij(float f) {
        MilesBridgeJNI.SMPATTRIBS_obstruction_set(this.swigCPtr, f);
    }

    public float byU() {
        return MilesBridgeJNI.SMPATTRIBS_obstruction_get(this.swigCPtr);
    }

    /* renamed from: ik */
    public void mo8089ik(float f) {
        MilesBridgeJNI.SMPATTRIBS_occlusion_set(this.swigCPtr, f);
    }

    public float byV() {
        return MilesBridgeJNI.SMPATTRIBS_occlusion_get(this.swigCPtr);
    }

    /* renamed from: ok */
    public void mo8117ok(int i) {
        MilesBridgeJNI.SMPATTRIBS_loop_count_set(this.swigCPtr, i);
    }

    public int byF() {
        return MilesBridgeJNI.SMPATTRIBS_loop_count_get(this.swigCPtr);
    }

    /* renamed from: xn */
    public void mo8122xn(int i) {
        MilesBridgeJNI.SMPATTRIBS_loop_start_offset_set(this.swigCPtr, i);
    }

    public int cQT() {
        return MilesBridgeJNI.SMPATTRIBS_loop_start_offset_get(this.swigCPtr);
    }

    /* renamed from: xo */
    public void mo8123xo(int i) {
        MilesBridgeJNI.SMPATTRIBS_loop_end_offset_set(this.swigCPtr, i);
    }

    public int cQU() {
        return MilesBridgeJNI.SMPATTRIBS_loop_end_offset_get(this.swigCPtr);
    }

    /* renamed from: mf */
    public void mo8110mf(float f) {
        MilesBridgeJNI.SMPATTRIBS_LPF_cutoff_set(this.swigCPtr, f);
    }

    public float cQV() {
        return MilesBridgeJNI.SMPATTRIBS_LPF_cutoff_get(this.swigCPtr);
    }

    /* renamed from: or */
    public void mo8118or(int i) {
        MilesBridgeJNI.SMPATTRIBS_playback_rate_set(this.swigCPtr, i);
    }

    public int byN() {
        return MilesBridgeJNI.SMPATTRIBS_playback_rate_get(this.swigCPtr);
    }

    /* renamed from: jM */
    public void mo8091jM(long j) {
        MilesBridgeJNI.SMPATTRIBS_position_set(this.swigCPtr, j);
    }

    public long getPosition() {
        return MilesBridgeJNI.SMPATTRIBS_position_get(this.swigCPtr);
    }

    /* renamed from: ii */
    public void mo8087ii(float f) {
        MilesBridgeJNI.SMPATTRIBS_dry_level_set(this.swigCPtr, f);
    }

    public float byT() {
        return MilesBridgeJNI.SMPATTRIBS_dry_level_get(this.swigCPtr);
    }

    /* renamed from: ih */
    public void mo8086ih(float f) {
        MilesBridgeJNI.SMPATTRIBS_wet_level_set(this.swigCPtr, f);
    }

    public float byS() {
        return MilesBridgeJNI.SMPATTRIBS_wet_level_get(this.swigCPtr);
    }

    /* renamed from: jN */
    public void mo8092jN(long j) {
        MilesBridgeJNI.SMPATTRIBS_playback_status_set(this.swigCPtr, j);
    }

    public long cQW() {
        return MilesBridgeJNI.SMPATTRIBS_playback_status_get(this.swigCPtr);
    }

    /* renamed from: mg */
    public void mo8111mg(float f) {
        MilesBridgeJNI.SMPATTRIBS_FL_51_level_set(this.swigCPtr, f);
    }

    public float cQX() {
        return MilesBridgeJNI.SMPATTRIBS_FL_51_level_get(this.swigCPtr);
    }

    /* renamed from: mh */
    public void mo8112mh(float f) {
        MilesBridgeJNI.SMPATTRIBS_FR_51_level_set(this.swigCPtr, f);
    }

    public float cQY() {
        return MilesBridgeJNI.SMPATTRIBS_FR_51_level_get(this.swigCPtr);
    }

    /* renamed from: mi */
    public void mo8113mi(float f) {
        MilesBridgeJNI.SMPATTRIBS_BL_51_level_set(this.swigCPtr, f);
    }

    public float cQZ() {
        return MilesBridgeJNI.SMPATTRIBS_BL_51_level_get(this.swigCPtr);
    }

    /* renamed from: mj */
    public void mo8114mj(float f) {
        MilesBridgeJNI.SMPATTRIBS_BR_51_level_set(this.swigCPtr, f);
    }

    public float cRa() {
        return MilesBridgeJNI.SMPATTRIBS_BR_51_level_get(this.swigCPtr);
    }

    /* renamed from: mk */
    public void mo8115mk(float f) {
        MilesBridgeJNI.SMPATTRIBS_C_51_level_set(this.swigCPtr, f);
    }

    public float cRb() {
        return MilesBridgeJNI.SMPATTRIBS_C_51_level_get(this.swigCPtr);
    }

    /* renamed from: ml */
    public void mo8116ml(float f) {
        MilesBridgeJNI.SMPATTRIBS_LFE_51_level_set(this.swigCPtr, f);
    }

    public float cRc() {
        return MilesBridgeJNI.SMPATTRIBS_LFE_51_level_get(this.swigCPtr);
    }

    /* renamed from: if */
    public void mo8084if(float f) {
        MilesBridgeJNI.SMPATTRIBS_left_volume_set(this.swigCPtr, f);
    }

    public float byQ() {
        return MilesBridgeJNI.SMPATTRIBS_left_volume_get(this.swigCPtr);
    }

    /* renamed from: ig */
    public void mo8085ig(float f) {
        MilesBridgeJNI.SMPATTRIBS_right_volume_set(this.swigCPtr, f);
    }

    public float byR() {
        return MilesBridgeJNI.SMPATTRIBS_right_volume_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo8031a(aTY aty) {
        MilesBridgeJNI.SMPATTRIBS_filter_stages_set(this.swigCPtr, aTY.m18358b(aty));
    }

    public aTY cRd() {
        long SMPATTRIBS_filter_stages_get = MilesBridgeJNI.SMPATTRIBS_filter_stages_get(this.swigCPtr);
        if (SMPATTRIBS_filter_stages_get == 0) {
            return null;
        }
        return new aTY(SMPATTRIBS_filter_stages_get, false);
    }

    /* renamed from: c */
    public void mo8043c(C5758aVq avq) {
        MilesBridgeJNI.SMPATTRIBS_filter_providers_set(this.swigCPtr, C5758aVq.m19143h(avq));
    }

    public C5758aVq cRe() {
        long SMPATTRIBS_filter_providers_get = MilesBridgeJNI.SMPATTRIBS_filter_providers_get(this.swigCPtr);
        if (SMPATTRIBS_filter_providers_get == 0) {
            return null;
        }
        return new C5758aVq(SMPATTRIBS_filter_providers_get, false);
    }

    /* renamed from: d */
    public void mo8079d(C5758aVq avq) {
        MilesBridgeJNI.SMPATTRIBS_filter_property_tokens_set(this.swigCPtr, C5758aVq.m19143h(avq));
    }

    public C5758aVq cRf() {
        long SMPATTRIBS_filter_property_tokens_get = MilesBridgeJNI.SMPATTRIBS_filter_property_tokens_get(this.swigCPtr);
        if (SMPATTRIBS_filter_property_tokens_get == 0) {
            return null;
        }
        return new C5758aVq(SMPATTRIBS_filter_property_tokens_get, false);
    }

    /* renamed from: e */
    public void mo8081e(C5758aVq avq) {
        MilesBridgeJNI.SMPATTRIBS_filter_property_values_set(this.swigCPtr, C5758aVq.m19143h(avq));
    }

    public C5758aVq cRg() {
        long SMPATTRIBS_filter_property_values_get = MilesBridgeJNI.SMPATTRIBS_filter_property_values_get(this.swigCPtr);
        if (SMPATTRIBS_filter_property_values_get == 0) {
            return null;
        }
        return new C5758aVq(SMPATTRIBS_filter_property_values_get, false);
    }

    /* renamed from: xp */
    public void mo8124xp(int i) {
        MilesBridgeJNI.SMPATTRIBS_n_filter_properties_set(this.swigCPtr, i);
    }

    public int cRh() {
        return MilesBridgeJNI.SMPATTRIBS_n_filter_properties_get(this.swigCPtr);
    }
}
