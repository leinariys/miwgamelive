package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.avH  reason: case insensitive filesystem */
/* compiled from: a */
public class C6867avH {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6867avH(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6867avH() {
        this(MilesBridgeJNI.new_MP3_INFO(), true);
    }

    /* renamed from: b */
    public static long m26487b(C6867avH avh) {
        if (avh == null) {
            return 0;
        }
        return avh.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_MP3_INFO(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: f */
    public void mo16475f(C6626aqa aqa) {
        MilesBridgeJNI.MP3_INFO_MP3_file_image_set(this.swigCPtr, C6626aqa.m25149e(aqa));
    }

    public C6626aqa czA() {
        long MP3_INFO_MP3_file_image_get = MilesBridgeJNI.MP3_INFO_MP3_file_image_get(this.swigCPtr);
        if (MP3_INFO_MP3_file_image_get == 0) {
            return null;
        }
        return new C6626aqa(MP3_INFO_MP3_file_image_get, false);
    }

    /* renamed from: uQ */
    public void mo16488uQ(int i) {
        MilesBridgeJNI.MP3_INFO_MP3_image_size_set(this.swigCPtr, i);
    }

    public int czB() {
        return MilesBridgeJNI.MP3_INFO_MP3_image_size_get(this.swigCPtr);
    }

    /* renamed from: g */
    public void mo16477g(C6626aqa aqa) {
        MilesBridgeJNI.MP3_INFO_ID3v2_set(this.swigCPtr, C6626aqa.m25149e(aqa));
    }

    public C6626aqa czC() {
        long MP3_INFO_ID3v2_get = MilesBridgeJNI.MP3_INFO_ID3v2_get(this.swigCPtr);
        if (MP3_INFO_ID3v2_get == 0) {
            return null;
        }
        return new C6626aqa(MP3_INFO_ID3v2_get, false);
    }

    /* renamed from: uR */
    public void mo16489uR(int i) {
        MilesBridgeJNI.MP3_INFO_ID3v2_size_set(this.swigCPtr, i);
    }

    public int czD() {
        return MilesBridgeJNI.MP3_INFO_ID3v2_size_get(this.swigCPtr);
    }

    /* renamed from: h */
    public void mo16480h(C6626aqa aqa) {
        MilesBridgeJNI.MP3_INFO_ID3v1_set(this.swigCPtr, C6626aqa.m25149e(aqa));
    }

    public C6626aqa czE() {
        long MP3_INFO_ID3v1_get = MilesBridgeJNI.MP3_INFO_ID3v1_get(this.swigCPtr);
        if (MP3_INFO_ID3v1_get == 0) {
            return null;
        }
        return new C6626aqa(MP3_INFO_ID3v1_get, false);
    }

    /* renamed from: i */
    public void mo16481i(C6626aqa aqa) {
        MilesBridgeJNI.MP3_INFO_start_MP3_data_set(this.swigCPtr, C6626aqa.m25149e(aqa));
    }

    public C6626aqa czF() {
        long MP3_INFO_start_MP3_data_get = MilesBridgeJNI.MP3_INFO_start_MP3_data_get(this.swigCPtr);
        if (MP3_INFO_start_MP3_data_get == 0) {
            return null;
        }
        return new C6626aqa(MP3_INFO_start_MP3_data_get, false);
    }

    /* renamed from: j */
    public void mo16483j(C6626aqa aqa) {
        MilesBridgeJNI.MP3_INFO_end_MP3_data_set(this.swigCPtr, C6626aqa.m25149e(aqa));
    }

    public C6626aqa czG() {
        long MP3_INFO_end_MP3_data_get = MilesBridgeJNI.MP3_INFO_end_MP3_data_get(this.swigCPtr);
        if (MP3_INFO_end_MP3_data_get == 0) {
            return null;
        }
        return new C6626aqa(MP3_INFO_end_MP3_data_get, false);
    }

    /* renamed from: uS */
    public void mo16490uS(int i) {
        MilesBridgeJNI.MP3_INFO_sample_rate_set(this.swigCPtr, i);
    }

    public int czH() {
        return MilesBridgeJNI.MP3_INFO_sample_rate_get(this.swigCPtr);
    }

    /* renamed from: uT */
    public void mo16491uT(int i) {
        MilesBridgeJNI.MP3_INFO_bit_rate_set(this.swigCPtr, i);
    }

    public int czI() {
        return MilesBridgeJNI.MP3_INFO_bit_rate_get(this.swigCPtr);
    }

    /* renamed from: uU */
    public void mo16492uU(int i) {
        MilesBridgeJNI.MP3_INFO_channels_per_sample_set(this.swigCPtr, i);
    }

    public int czJ() {
        return MilesBridgeJNI.MP3_INFO_channels_per_sample_get(this.swigCPtr);
    }

    /* renamed from: uV */
    public void mo16493uV(int i) {
        MilesBridgeJNI.MP3_INFO_samples_per_frame_set(this.swigCPtr, i);
    }

    public int czK() {
        return MilesBridgeJNI.MP3_INFO_samples_per_frame_get(this.swigCPtr);
    }

    /* renamed from: uW */
    public void mo16494uW(int i) {
        MilesBridgeJNI.MP3_INFO_byte_offset_set(this.swigCPtr, i);
    }

    public int czL() {
        return MilesBridgeJNI.MP3_INFO_byte_offset_get(this.swigCPtr);
    }

    /* renamed from: uX */
    public void mo16495uX(int i) {
        MilesBridgeJNI.MP3_INFO_next_frame_expected_set(this.swigCPtr, i);
    }

    public int czM() {
        return MilesBridgeJNI.MP3_INFO_next_frame_expected_get(this.swigCPtr);
    }

    /* renamed from: uY */
    public void mo16496uY(int i) {
        MilesBridgeJNI.MP3_INFO_average_frame_size_set(this.swigCPtr, i);
    }

    public int czN() {
        return MilesBridgeJNI.MP3_INFO_average_frame_size_get(this.swigCPtr);
    }

    /* renamed from: uZ */
    public void mo16497uZ(int i) {
        MilesBridgeJNI.MP3_INFO_data_size_set(this.swigCPtr, i);
    }

    public int czO() {
        return MilesBridgeJNI.MP3_INFO_data_size_get(this.swigCPtr);
    }

    /* renamed from: va */
    public void mo16505va(int i) {
        MilesBridgeJNI.MP3_INFO_header_size_set(this.swigCPtr, i);
    }

    public int czP() {
        return MilesBridgeJNI.MP3_INFO_header_size_get(this.swigCPtr);
    }

    /* renamed from: vb */
    public void mo16506vb(int i) {
        MilesBridgeJNI.MP3_INFO_side_info_size_set(this.swigCPtr, i);
    }

    public int czQ() {
        return MilesBridgeJNI.MP3_INFO_side_info_size_get(this.swigCPtr);
    }

    /* renamed from: vc */
    public void mo16507vc(int i) {
        MilesBridgeJNI.MP3_INFO_ngr_set(this.swigCPtr, i);
    }

    public int czR() {
        return MilesBridgeJNI.MP3_INFO_ngr_get(this.swigCPtr);
    }

    /* renamed from: vd */
    public void mo16508vd(int i) {
        MilesBridgeJNI.MP3_INFO_main_data_begin_set(this.swigCPtr, i);
    }

    public int czS() {
        return MilesBridgeJNI.MP3_INFO_main_data_begin_get(this.swigCPtr);
    }

    /* renamed from: ve */
    public void mo16509ve(int i) {
        MilesBridgeJNI.MP3_INFO_hpos_set(this.swigCPtr, i);
    }

    public int czT() {
        return MilesBridgeJNI.MP3_INFO_hpos_get(this.swigCPtr);
    }

    /* renamed from: vf */
    public void mo16510vf(int i) {
        MilesBridgeJNI.MP3_INFO_MPEG1_set(this.swigCPtr, i);
    }

    public int czU() {
        return MilesBridgeJNI.MP3_INFO_MPEG1_get(this.swigCPtr);
    }

    /* renamed from: vg */
    public void mo16511vg(int i) {
        MilesBridgeJNI.MP3_INFO_MPEG25_set(this.swigCPtr, i);
    }

    public int czV() {
        return MilesBridgeJNI.MP3_INFO_MPEG25_get(this.swigCPtr);
    }

    public int getLayer() {
        return MilesBridgeJNI.MP3_INFO_layer_get(this.swigCPtr);
    }

    public void setLayer(int i) {
        MilesBridgeJNI.MP3_INFO_layer_set(this.swigCPtr, i);
    }

    /* renamed from: vh */
    public void mo16512vh(int i) {
        MilesBridgeJNI.MP3_INFO_protection_bit_set(this.swigCPtr, i);
    }

    public int czW() {
        return MilesBridgeJNI.MP3_INFO_protection_bit_get(this.swigCPtr);
    }

    /* renamed from: vi */
    public void mo16513vi(int i) {
        MilesBridgeJNI.MP3_INFO_bitrate_index_set(this.swigCPtr, i);
    }

    public int czX() {
        return MilesBridgeJNI.MP3_INFO_bitrate_index_get(this.swigCPtr);
    }

    /* renamed from: vj */
    public void mo16514vj(int i) {
        MilesBridgeJNI.MP3_INFO_sampling_frequency_set(this.swigCPtr, i);
    }

    public int czY() {
        return MilesBridgeJNI.MP3_INFO_sampling_frequency_get(this.swigCPtr);
    }

    /* renamed from: vk */
    public void mo16515vk(int i) {
        MilesBridgeJNI.MP3_INFO_padding_bit_set(this.swigCPtr, i);
    }

    public int czZ() {
        return MilesBridgeJNI.MP3_INFO_padding_bit_get(this.swigCPtr);
    }

    /* renamed from: vl */
    public void mo16516vl(int i) {
        MilesBridgeJNI.MP3_INFO_private_bit_set(this.swigCPtr, i);
    }

    public int cAa() {
        return MilesBridgeJNI.MP3_INFO_private_bit_get(this.swigCPtr);
    }

    public int getMode() {
        return MilesBridgeJNI.MP3_INFO_mode_get(this.swigCPtr);
    }

    public void setMode(int i) {
        MilesBridgeJNI.MP3_INFO_mode_set(this.swigCPtr, i);
    }

    /* renamed from: vm */
    public void mo16517vm(int i) {
        MilesBridgeJNI.MP3_INFO_mode_extension_set(this.swigCPtr, i);
    }

    public int cAb() {
        return MilesBridgeJNI.MP3_INFO_mode_extension_get(this.swigCPtr);
    }

    /* renamed from: vn */
    public void mo16518vn(int i) {
        MilesBridgeJNI.MP3_INFO_copyright_set(this.swigCPtr, i);
    }

    public int cAc() {
        return MilesBridgeJNI.MP3_INFO_copyright_get(this.swigCPtr);
    }

    /* renamed from: vo */
    public void mo16519vo(int i) {
        MilesBridgeJNI.MP3_INFO_original_set(this.swigCPtr, i);
    }

    public int cAd() {
        return MilesBridgeJNI.MP3_INFO_original_get(this.swigCPtr);
    }

    /* renamed from: vp */
    public void mo16520vp(int i) {
        MilesBridgeJNI.MP3_INFO_emphasis_set(this.swigCPtr, i);
    }

    public int cAe() {
        return MilesBridgeJNI.MP3_INFO_emphasis_get(this.swigCPtr);
    }

    /* renamed from: vq */
    public void mo16521vq(int i) {
        MilesBridgeJNI.MP3_INFO_Xing_valid_set(this.swigCPtr, i);
    }

    public int cAf() {
        return MilesBridgeJNI.MP3_INFO_Xing_valid_get(this.swigCPtr);
    }

    /* renamed from: vr */
    public void mo16522vr(int i) {
        MilesBridgeJNI.MP3_INFO_Info_valid_set(this.swigCPtr, i);
    }

    public int cAg() {
        return MilesBridgeJNI.MP3_INFO_Info_valid_get(this.swigCPtr);
    }

    /* renamed from: iY */
    public void mo16482iY(long j) {
        MilesBridgeJNI.MP3_INFO_header_flags_set(this.swigCPtr, j);
    }

    public long cAh() {
        return MilesBridgeJNI.MP3_INFO_header_flags_get(this.swigCPtr);
    }

    /* renamed from: vs */
    public void mo16523vs(int i) {
        MilesBridgeJNI.MP3_INFO_frame_count_set(this.swigCPtr, i);
    }

    public int cAi() {
        return MilesBridgeJNI.MP3_INFO_frame_count_get(this.swigCPtr);
    }

    /* renamed from: vt */
    public void mo16524vt(int i) {
        MilesBridgeJNI.MP3_INFO_byte_count_set(this.swigCPtr, i);
    }

    public int cAj() {
        return MilesBridgeJNI.MP3_INFO_byte_count_get(this.swigCPtr);
    }

    /* renamed from: vu */
    public void mo16525vu(int i) {
        MilesBridgeJNI.MP3_INFO_VBR_scale_set(this.swigCPtr, i);
    }

    public int cAk() {
        return MilesBridgeJNI.MP3_INFO_VBR_scale_get(this.swigCPtr);
    }

    /* renamed from: k */
    public void mo16484k(C6626aqa aqa) {
        MilesBridgeJNI.MP3_INFO_TOC_set(this.swigCPtr, C6626aqa.m25149e(aqa));
    }

    public C6626aqa cAl() {
        long MP3_INFO_TOC_get = MilesBridgeJNI.MP3_INFO_TOC_get(this.swigCPtr);
        if (MP3_INFO_TOC_get == 0) {
            return null;
        }
        return new C6626aqa(MP3_INFO_TOC_get, false);
    }

    /* renamed from: vv */
    public void mo16526vv(int i) {
        MilesBridgeJNI.MP3_INFO_enc_delay_set(this.swigCPtr, i);
    }

    public int cAm() {
        return MilesBridgeJNI.MP3_INFO_enc_delay_get(this.swigCPtr);
    }

    /* renamed from: vw */
    public void mo16527vw(int i) {
        MilesBridgeJNI.MP3_INFO_enc_padding_set(this.swigCPtr, i);
    }

    public int cAn() {
        return MilesBridgeJNI.MP3_INFO_enc_padding_get(this.swigCPtr);
    }

    /* renamed from: l */
    public void mo16485l(C6626aqa aqa) {
        MilesBridgeJNI.MP3_INFO_ptr_set(this.swigCPtr, C6626aqa.m25149e(aqa));
    }

    public C6626aqa cAo() {
        long MP3_INFO_ptr_get = MilesBridgeJNI.MP3_INFO_ptr_get(this.swigCPtr);
        if (MP3_INFO_ptr_get == 0) {
            return null;
        }
        return new C6626aqa(MP3_INFO_ptr_get, false);
    }

    /* renamed from: vx */
    public void mo16528vx(int i) {
        MilesBridgeJNI.MP3_INFO_bytes_left_set(this.swigCPtr, i);
    }

    public int cAp() {
        return MilesBridgeJNI.MP3_INFO_bytes_left_get(this.swigCPtr);
    }

    /* renamed from: vy */
    public void mo16529vy(int i) {
        MilesBridgeJNI.MP3_INFO_check_valid_set(this.swigCPtr, i);
    }

    public int cAq() {
        return MilesBridgeJNI.MP3_INFO_check_valid_get(this.swigCPtr);
    }

    /* renamed from: vz */
    public void mo16530vz(int i) {
        MilesBridgeJNI.MP3_INFO_check_MPEG1_set(this.swigCPtr, i);
    }

    public int cAr() {
        return MilesBridgeJNI.MP3_INFO_check_MPEG1_get(this.swigCPtr);
    }

    /* renamed from: vA */
    public void mo16498vA(int i) {
        MilesBridgeJNI.MP3_INFO_check_MPEG25_set(this.swigCPtr, i);
    }

    public int cAs() {
        return MilesBridgeJNI.MP3_INFO_check_MPEG25_get(this.swigCPtr);
    }

    /* renamed from: vB */
    public void mo16499vB(int i) {
        MilesBridgeJNI.MP3_INFO_check_layer_set(this.swigCPtr, i);
    }

    public int cAt() {
        return MilesBridgeJNI.MP3_INFO_check_layer_get(this.swigCPtr);
    }

    /* renamed from: vC */
    public void mo16500vC(int i) {
        MilesBridgeJNI.MP3_INFO_check_protection_bit_set(this.swigCPtr, i);
    }

    public int cAu() {
        return MilesBridgeJNI.MP3_INFO_check_protection_bit_get(this.swigCPtr);
    }

    /* renamed from: vD */
    public void mo16501vD(int i) {
        MilesBridgeJNI.MP3_INFO_check_sampling_frequency_set(this.swigCPtr, i);
    }

    public int cAv() {
        return MilesBridgeJNI.MP3_INFO_check_sampling_frequency_get(this.swigCPtr);
    }

    /* renamed from: vE */
    public void mo16502vE(int i) {
        MilesBridgeJNI.MP3_INFO_check_mode_set(this.swigCPtr, i);
    }

    public int cAw() {
        return MilesBridgeJNI.MP3_INFO_check_mode_get(this.swigCPtr);
    }

    /* renamed from: vF */
    public void mo16503vF(int i) {
        MilesBridgeJNI.MP3_INFO_check_copyright_set(this.swigCPtr, i);
    }

    public int cAx() {
        return MilesBridgeJNI.MP3_INFO_check_copyright_get(this.swigCPtr);
    }

    /* renamed from: vG */
    public void mo16504vG(int i) {
        MilesBridgeJNI.MP3_INFO_check_original_set(this.swigCPtr, i);
    }

    public int cAy() {
        return MilesBridgeJNI.MP3_INFO_check_original_get(this.swigCPtr);
    }
}
