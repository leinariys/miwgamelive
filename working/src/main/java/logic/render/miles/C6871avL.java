package logic.render.miles;

/* renamed from: a.avL  reason: case insensitive filesystem */
/* compiled from: a */
public class C6871avL {
    private long swigCPtr;

    public C6871avL(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6871avL() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m26575b(C6871avL avl) {
        if (avl == null) {
            return 0;
        }
        return avl.swigCPtr;
    }
}
