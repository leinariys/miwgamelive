package logic.render.miles;

/* renamed from: a.aok  reason: case insensitive filesystem */
/* compiled from: a */
public class C6532aok {
    private long swigCPtr;

    public C6532aok(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6532aok() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m24679b(C6532aok aok) {
        if (aok == null) {
            return 0;
        }
        return aok.swigCPtr;
    }
}
