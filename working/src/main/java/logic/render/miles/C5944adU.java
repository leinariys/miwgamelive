package logic.render.miles;

/* renamed from: a.adU  reason: case insensitive filesystem */
/* compiled from: a */
public class C5944adU {
    private long swigCPtr;

    public C5944adU(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5944adU() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m20899b(C5944adU adu) {
        if (adu == null) {
            return 0;
        }
        return adu.swigCPtr;
    }
}
