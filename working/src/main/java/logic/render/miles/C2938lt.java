package logic.render.miles;

/* renamed from: a.lt */
/* compiled from: a */
public final class C2938lt {
    public static final C2938lt axA = new C2938lt("PS3_AUDIO_PORT");
    public static final C2938lt axB = new C2938lt("PS3_AUDIO_ADDRESS");
    public static final C2938lt axC = new C2938lt("PS3_AUDIO_LENGTH");
    public static final C2938lt axD = new C2938lt("PS3_AUDIO_POSITION");
    public static final C2938lt axE = new C2938lt("XB_TIMER_THREAD");
    public static final C2938lt axF = new C2938lt("XB_STREAM_THREAD");
    public static final C2938lt axG = new C2938lt("XB_LPDS");
    public static final C2938lt axH = new C2938lt("XB_LPDSB");
    public static final C2938lt axI = new C2938lt("XB360_TIMER_THREAD");
    public static final C2938lt axJ = new C2938lt("XB360_STREAM_THREAD");
    public static final C2938lt axK = new C2938lt("XB360_LPXAB");
    public static final C2938lt axq = new C2938lt("WIN32_TIMER_THREAD");
    public static final C2938lt axr = new C2938lt("WIN32_STREAM_THREAD");
    public static final C2938lt axs = new C2938lt("WIN32_HWAVEOUT");
    public static final C2938lt axt = new C2938lt("WIN32_HWAVEIN");
    public static final C2938lt axu = new C2938lt("WIN32_LPDS");
    public static final C2938lt axv = new C2938lt("WIN32_LPDSB");
    public static final C2938lt axw = new C2938lt("WIN32_HWND");
    public static final C2938lt axx = new C2938lt("WIN32_POSITION_ERR");
    public static final C2938lt axy = new C2938lt("PS3_TIMER_THREAD");
    public static final C2938lt axz = new C2938lt("PS3_STREAM_THREAD");
    private static C2938lt[] axL = {axq, axr, axs, axt, axu, axv, axw, axx, axy, axz, axA, axB, axC, axD, axE, axF, axG, axH, axI, axJ, axK};
    /* renamed from: pF */
    private static int f8589pF = 0;

    /* renamed from: pG */
    private final int f8590pG;

    /* renamed from: pH */
    private final String f8591pH;

    private C2938lt(String str) {
        this.f8591pH = str;
        int i = f8589pF;
        f8589pF = i + 1;
        this.f8590pG = i;
    }

    private C2938lt(String str, int i) {
        this.f8591pH = str;
        this.f8590pG = i;
        f8589pF = i + 1;
    }

    private C2938lt(String str, C2938lt ltVar) {
        this.f8591pH = str;
        this.f8590pG = ltVar.f8590pG;
        f8589pF = this.f8590pG + 1;
    }

    /* renamed from: cL */
    public static C2938lt m35339cL(int i) {
        if (i < axL.length && i >= 0 && axL[i].f8590pG == i) {
            return axL[i];
        }
        for (int i2 = 0; i2 < axL.length; i2++) {
            if (axL[i2].f8590pG == i) {
                return axL[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C2938lt.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f8590pG;
    }

    public String toString() {
        return this.f8591pH;
    }
}
