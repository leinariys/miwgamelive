package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aeF  reason: case insensitive filesystem */
/* compiled from: a */
public final class C5981aeF {
    public static final C5981aeF fpX = new C5981aeF("MSS_MC_INVALID", MilesBridgeJNI.MSS_MC_INVALID_get());
    public static final C5981aeF fpY = new C5981aeF("MSS_MC_MONO", MilesBridgeJNI.MSS_MC_MONO_get());
    public static final C5981aeF fpZ = new C5981aeF("MSS_MC_STEREO", MilesBridgeJNI.MSS_MC_STEREO_get());
    public static final C5981aeF fqa = new C5981aeF("MSS_MC_USE_SYSTEM_CONFIG", MilesBridgeJNI.MSS_MC_USE_SYSTEM_CONFIG_get());
    public static final C5981aeF fqb = new C5981aeF("MSS_MC_HEADPHONES", MilesBridgeJNI.MSS_MC_HEADPHONES_get());
    public static final C5981aeF fqc = new C5981aeF("MSS_MC_DOLBY_SURROUND", MilesBridgeJNI.MSS_MC_DOLBY_SURROUND_get());
    public static final C5981aeF fqd = new C5981aeF("MSS_MC_SRS_CIRCLE_SURROUND", MilesBridgeJNI.MSS_MC_SRS_CIRCLE_SURROUND_get());
    public static final C5981aeF fqe = new C5981aeF("MSS_MC_40_DTS", MilesBridgeJNI.MSS_MC_40_DTS_get());
    public static final C5981aeF fqf = new C5981aeF("MSS_MC_40_DISCRETE", MilesBridgeJNI.MSS_MC_40_DISCRETE_get());
    public static final C5981aeF fqg = new C5981aeF("MSS_MC_51_DTS", MilesBridgeJNI.MSS_MC_51_DTS_get());
    public static final C5981aeF fqh = new C5981aeF("MSS_MC_51_DISCRETE", MilesBridgeJNI.MSS_MC_51_DISCRETE_get());
    public static final C5981aeF fqi = new C5981aeF("MSS_MC_61_DISCRETE", MilesBridgeJNI.MSS_MC_61_DISCRETE_get());
    public static final C5981aeF fqj = new C5981aeF("MSS_MC_71_DISCRETE", MilesBridgeJNI.MSS_MC_71_DISCRETE_get());
    public static final C5981aeF fqk = new C5981aeF("MSS_MC_81_DISCRETE", MilesBridgeJNI.MSS_MC_81_DISCRETE_get());
    public static final C5981aeF fql = new C5981aeF("MSS_MC_DIRECTSOUND3D", MilesBridgeJNI.MSS_MC_DIRECTSOUND3D_get());
    public static final C5981aeF fqm = new C5981aeF("MSS_MC_EAX2", MilesBridgeJNI.MSS_MC_EAX2_get());
    public static final C5981aeF fqn = new C5981aeF("MSS_MC_EAX3", MilesBridgeJNI.MSS_MC_EAX3_get());
    public static final C5981aeF fqo = new C5981aeF("MSS_MC_EAX4", MilesBridgeJNI.MSS_MC_EAX4_get());
    private static C5981aeF[] fqp = {fpX, fpY, fpZ, fqa, fqb, fqc, fqd, fqe, fqf, fqg, fqh, fqi, fqj, fqk, fql, fqm, fqn, fqo};

    /* renamed from: pF */
    private static int f4383pF = 0;

    /* renamed from: pG */
    private final int f4384pG;

    /* renamed from: pH */
    private final String f4385pH;

    private C5981aeF(String str) {
        this.f4385pH = str;
        int i = f4383pF;
        f4383pF = i + 1;
        this.f4384pG = i;
    }

    private C5981aeF(String str, int i) {
        this.f4385pH = str;
        this.f4384pG = i;
        f4383pF = i + 1;
    }

    private C5981aeF(String str, C5981aeF aef) {
        this.f4385pH = str;
        this.f4384pG = aef.f4384pG;
        f4383pF = this.f4384pG + 1;
    }

    /* renamed from: qn */
    public static C5981aeF m21164qn(int i) {
        if (i < fqp.length && i >= 0 && fqp[i].f4384pG == i) {
            return fqp[i];
        }
        for (int i2 = 0; i2 < fqp.length; i2++) {
            if (fqp[i2].f4384pG == i) {
                return fqp[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C5981aeF.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f4384pG;
    }

    public String toString() {
        return this.f4385pH;
    }
}
