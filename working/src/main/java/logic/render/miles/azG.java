package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.azG */
/* compiled from: a */
public class azG {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public azG(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public azG() {
        this(MilesBridgeJNI.new_SPINFO(), true);
    }

    /* renamed from: b */
    public static long m27595b(azG azg) {
        if (azg == null) {
            return 0;
        }
        return azg.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_SPINFO(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: wE */
    public void mo17154wE(int i) {
        MilesBridgeJNI.SPINFO_active_set(this.swigCPtr, i);
    }

    public int cGq() {
        return MilesBridgeJNI.SPINFO_active_get(this.swigCPtr);
    }

    /* renamed from: hv */
    public void mo17153hv(long j) {
        MilesBridgeJNI.SPINFO_provider_set(this.swigCPtr, j);
    }

    public long cbo() {
        return MilesBridgeJNI.SPINFO_provider_get(this.swigCPtr);
    }

    public C0815Lm cGr() {
        long SPINFO_TYPE_get = MilesBridgeJNI.SPINFO_TYPE_get(this.swigCPtr);
        if (SPINFO_TYPE_get == 0) {
            return null;
        }
        return new C0815Lm(SPINFO_TYPE_get, false);
    }
}
