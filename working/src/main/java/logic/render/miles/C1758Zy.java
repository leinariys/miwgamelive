package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.Zy */
/* compiled from: a */
public class C1758Zy {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1758Zy(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1758Zy() {
        this(MilesBridgeJNI.new_REVERB_CONSTANT_INFO(), true);
    }

    /* renamed from: a */
    public static long m12259a(C1758Zy zy) {
        if (zy == null) {
            return 0;
        }
        return zy.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_REVERB_CONSTANT_INFO(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: c */
    public void mo7528c(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_start0_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC bIt() {
        long REVERB_CONSTANT_INFO_start0_get = MilesBridgeJNI.REVERB_CONSTANT_INFO_start0_get(this.swigCPtr);
        if (REVERB_CONSTANT_INFO_start0_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_CONSTANT_INFO_start0_get, false);
    }

    /* renamed from: d */
    public void mo7529d(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_start1_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC bIu() {
        long REVERB_CONSTANT_INFO_start1_get = MilesBridgeJNI.REVERB_CONSTANT_INFO_start1_get(this.swigCPtr);
        if (REVERB_CONSTANT_INFO_start1_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_CONSTANT_INFO_start1_get, false);
    }

    /* renamed from: e */
    public void mo7531e(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_start2_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC bIv() {
        long REVERB_CONSTANT_INFO_start2_get = MilesBridgeJNI.REVERB_CONSTANT_INFO_start2_get(this.swigCPtr);
        if (REVERB_CONSTANT_INFO_start2_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_CONSTANT_INFO_start2_get, false);
    }

    /* renamed from: f */
    public void mo7532f(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_start3_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC bIw() {
        long REVERB_CONSTANT_INFO_start3_get = MilesBridgeJNI.REVERB_CONSTANT_INFO_start3_get(this.swigCPtr);
        if (REVERB_CONSTANT_INFO_start3_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_CONSTANT_INFO_start3_get, false);
    }

    /* renamed from: g */
    public void mo7534g(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_start4_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC bIx() {
        long REVERB_CONSTANT_INFO_start4_get = MilesBridgeJNI.REVERB_CONSTANT_INFO_start4_get(this.swigCPtr);
        if (REVERB_CONSTANT_INFO_start4_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_CONSTANT_INFO_start4_get, false);
    }

    /* renamed from: h */
    public void mo7536h(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_start5_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC bIy() {
        long REVERB_CONSTANT_INFO_start5_get = MilesBridgeJNI.REVERB_CONSTANT_INFO_start5_get(this.swigCPtr);
        if (REVERB_CONSTANT_INFO_start5_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_CONSTANT_INFO_start5_get, false);
    }

    /* renamed from: i */
    public void mo7537i(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_end0_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC bIz() {
        long REVERB_CONSTANT_INFO_end0_get = MilesBridgeJNI.REVERB_CONSTANT_INFO_end0_get(this.swigCPtr);
        if (REVERB_CONSTANT_INFO_end0_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_CONSTANT_INFO_end0_get, false);
    }

    /* renamed from: j */
    public void mo7546j(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_end1_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC bIA() {
        long REVERB_CONSTANT_INFO_end1_get = MilesBridgeJNI.REVERB_CONSTANT_INFO_end1_get(this.swigCPtr);
        if (REVERB_CONSTANT_INFO_end1_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_CONSTANT_INFO_end1_get, false);
    }

    /* renamed from: k */
    public void mo7547k(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_end2_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC bIB() {
        long REVERB_CONSTANT_INFO_end2_get = MilesBridgeJNI.REVERB_CONSTANT_INFO_end2_get(this.swigCPtr);
        if (REVERB_CONSTANT_INFO_end2_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_CONSTANT_INFO_end2_get, false);
    }

    /* renamed from: l */
    public void mo7548l(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_end3_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC bIC() {
        long REVERB_CONSTANT_INFO_end3_get = MilesBridgeJNI.REVERB_CONSTANT_INFO_end3_get(this.swigCPtr);
        if (REVERB_CONSTANT_INFO_end3_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_CONSTANT_INFO_end3_get, false);
    }

    /* renamed from: m */
    public void mo7549m(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_end4_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC bID() {
        long REVERB_CONSTANT_INFO_end4_get = MilesBridgeJNI.REVERB_CONSTANT_INFO_end4_get(this.swigCPtr);
        if (REVERB_CONSTANT_INFO_end4_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_CONSTANT_INFO_end4_get, false);
    }

    /* renamed from: n */
    public void mo7550n(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_end5_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC bIE() {
        long REVERB_CONSTANT_INFO_end5_get = MilesBridgeJNI.REVERB_CONSTANT_INFO_end5_get(this.swigCPtr);
        if (REVERB_CONSTANT_INFO_end5_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_CONSTANT_INFO_end5_get, false);
    }

    /* renamed from: iK */
    public void mo7538iK(float f) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_C0_set(this.swigCPtr, f);
    }

    public float bIF() {
        return MilesBridgeJNI.REVERB_CONSTANT_INFO_C0_get(this.swigCPtr);
    }

    /* renamed from: iL */
    public void mo7539iL(float f) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_C1_set(this.swigCPtr, f);
    }

    public float bIG() {
        return MilesBridgeJNI.REVERB_CONSTANT_INFO_C1_get(this.swigCPtr);
    }

    /* renamed from: iM */
    public void mo7540iM(float f) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_C2_set(this.swigCPtr, f);
    }

    public float bIH() {
        return MilesBridgeJNI.REVERB_CONSTANT_INFO_C2_get(this.swigCPtr);
    }

    /* renamed from: iN */
    public void mo7541iN(float f) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_C3_set(this.swigCPtr, f);
    }

    public float bII() {
        return MilesBridgeJNI.REVERB_CONSTANT_INFO_C3_get(this.swigCPtr);
    }

    /* renamed from: iO */
    public void mo7542iO(float f) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_C4_set(this.swigCPtr, f);
    }

    public float bIJ() {
        return MilesBridgeJNI.REVERB_CONSTANT_INFO_C4_get(this.swigCPtr);
    }

    /* renamed from: iP */
    public void mo7543iP(float f) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_C5_set(this.swigCPtr, f);
    }

    public float bIK() {
        return MilesBridgeJNI.REVERB_CONSTANT_INFO_C5_get(this.swigCPtr);
    }

    public float getA() {
        return MilesBridgeJNI.REVERB_CONSTANT_INFO_A_get(this.swigCPtr);
    }

    public void setA(float f) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_A_set(this.swigCPtr, f);
    }

    /* renamed from: iQ */
    public void mo7544iQ(float f) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_B0_set(this.swigCPtr, f);
    }

    public float bIL() {
        return MilesBridgeJNI.REVERB_CONSTANT_INFO_B0_get(this.swigCPtr);
    }

    /* renamed from: iR */
    public void mo7545iR(float f) {
        MilesBridgeJNI.REVERB_CONSTANT_INFO_B1_set(this.swigCPtr, f);
    }

    public float bIM() {
        return MilesBridgeJNI.REVERB_CONSTANT_INFO_B1_get(this.swigCPtr);
    }
}
