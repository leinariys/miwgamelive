package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.sH */
/* compiled from: a */
public final class C3536sH {
    public static final C3536sH biq = new C3536sH("RIB_FUNCTION", MilesBridgeJNI.RIB_FUNCTION_get());
    public static final C3536sH bir = new C3536sH("RIB_PROPERTY");
    private static C3536sH[] bis = {biq, bir};

    /* renamed from: pF */
    private static int f9090pF = 0;

    /* renamed from: pG */
    private final int f9091pG;

    /* renamed from: pH */
    private final String f9092pH;

    private C3536sH(String str) {
        this.f9092pH = str;
        int i = f9090pF;
        f9090pF = i + 1;
        this.f9091pG = i;
    }

    private C3536sH(String str, int i) {
        this.f9092pH = str;
        this.f9091pG = i;
        f9090pF = i + 1;
    }

    private C3536sH(String str, C3536sH sHVar) {
        this.f9092pH = str;
        this.f9091pG = sHVar.f9091pG;
        f9090pF = this.f9091pG + 1;
    }

    /* renamed from: eB */
    public static C3536sH m38709eB(int i) {
        if (i < bis.length && i >= 0 && bis[i].f9091pG == i) {
            return bis[i];
        }
        for (int i2 = 0; i2 < bis.length; i2++) {
            if (bis[i2].f9091pG == i) {
                return bis[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C3536sH.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f9091pG;
    }

    public String toString() {
        return this.f9092pH;
    }
}
