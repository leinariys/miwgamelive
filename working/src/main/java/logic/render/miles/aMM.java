package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aMM */
/* compiled from: a */
public class aMM {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aMM(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aMM() {
        this(MilesBridgeJNI.new_DPINFO(), true);
    }

    /* renamed from: a */
    public static long m16363a(aMM amm) {
        if (amm == null) {
            return 0;
        }
        return amm.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_DPINFO(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: wE */
    public void mo10036wE(int i) {
        MilesBridgeJNI.DPINFO_active_set(this.swigCPtr, i);
    }

    public int cGq() {
        return MilesBridgeJNI.DPINFO_active_get(this.swigCPtr);
    }

    /* renamed from: hv */
    public void mo10035hv(long j) {
        MilesBridgeJNI.DPINFO_provider_set(this.swigCPtr, j);
    }

    public long cbo() {
        return MilesBridgeJNI.DPINFO_provider_get(this.swigCPtr);
    }

    public C2442fV djF() {
        long DPINFO_TYPE_get = MilesBridgeJNI.DPINFO_TYPE_get(this.swigCPtr);
        if (DPINFO_TYPE_get == 0) {
            return null;
        }
        return new C2442fV(DPINFO_TYPE_get, false);
    }
}
