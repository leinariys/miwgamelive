package logic.render.miles;

/* renamed from: a.aNr  reason: case insensitive filesystem */
/* compiled from: a */
public class C5551aNr {
    private long swigCPtr;

    public C5551aNr(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5551aNr() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m16779a(C5551aNr anr) {
        if (anr == null) {
            return 0;
        }
        return anr.swigCPtr;
    }
}
