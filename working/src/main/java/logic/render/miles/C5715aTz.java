package logic.render.miles;

/* renamed from: a.aTz  reason: case insensitive filesystem */
/* compiled from: a */
public class C5715aTz {
    private long swigCPtr;

    public C5715aTz(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5715aTz() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m18490b(C5715aTz atz) {
        if (atz == null) {
            return 0;
        }
        return atz.swigCPtr;
    }
}
