package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.ajR  reason: case insensitive filesystem */
/* compiled from: a */
public class C6253ajR {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6253ajR(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6253ajR() {
        this(MilesBridgeJNI.new_MPCMWAVEFORMAT(), true);
    }

    /* renamed from: a */
    public static long m22894a(C6253ajR ajr) {
        if (ajr == null) {
            return 0;
        }
        return ajr.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_MPCMWAVEFORMAT(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo14078b(C0774LC lc) {
        MilesBridgeJNI.MPCMWAVEFORMAT_wf_set(this.swigCPtr, C0774LC.m6650a(lc));
    }

    public C0774LC cfF() {
        long MPCMWAVEFORMAT_wf_get = MilesBridgeJNI.MPCMWAVEFORMAT_wf_get(this.swigCPtr);
        if (MPCMWAVEFORMAT_wf_get == 0) {
            return null;
        }
        return new C0774LC(MPCMWAVEFORMAT_wf_get, false);
    }

    /* renamed from: rJ */
    public void mo14083rJ(int i) {
        MilesBridgeJNI.MPCMWAVEFORMAT_wBitsPerSample_set(this.swigCPtr, i);
    }

    public int cfG() {
        return MilesBridgeJNI.MPCMWAVEFORMAT_wBitsPerSample_get(this.swigCPtr);
    }
}
