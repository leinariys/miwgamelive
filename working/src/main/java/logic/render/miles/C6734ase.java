package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.ase  reason: case insensitive filesystem */
/* compiled from: a */
public class C6734ase {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6734ase(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6734ase() {
        this(MilesBridgeJNI.new_WAVE_ENTRY(), true);
    }

    /* renamed from: b */
    public static long m25715b(C6734ase ase) {
        if (ase == null) {
            return 0;
        }
        return ase.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_WAVE_ENTRY(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: um */
    public void mo15958um(int i) {
        MilesBridgeJNI.WAVE_ENTRY_bank_set(this.swigCPtr, i);
    }

    public int getBank() {
        return MilesBridgeJNI.WAVE_ENTRY_bank_get(this.swigCPtr);
    }

    /* renamed from: un */
    public void mo15959un(int i) {
        MilesBridgeJNI.WAVE_ENTRY_patch_set(this.swigCPtr, i);
    }

    public int getPatch() {
        return MilesBridgeJNI.WAVE_ENTRY_patch_get(this.swigCPtr);
    }

    /* renamed from: uo */
    public void mo15960uo(int i) {
        MilesBridgeJNI.WAVE_ENTRY_root_key_set(this.swigCPtr, i);
    }

    public int ctD() {
        return MilesBridgeJNI.WAVE_ENTRY_root_key_get(this.swigCPtr);
    }

    /* renamed from: iH */
    public void mo15953iH(long j) {
        MilesBridgeJNI.WAVE_ENTRY_file_offset_set(this.swigCPtr, j);
    }

    public long ctE() {
        return MilesBridgeJNI.WAVE_ENTRY_file_offset_get(this.swigCPtr);
    }

    public long getSize() {
        return MilesBridgeJNI.WAVE_ENTRY_size_get(this.swigCPtr);
    }

    public void setSize(long j) {
        MilesBridgeJNI.WAVE_ENTRY_size_set(this.swigCPtr, j);
    }

    public int getFormat() {
        return MilesBridgeJNI.WAVE_ENTRY_format_get(this.swigCPtr);
    }

    public void setFormat(int i) {
        MilesBridgeJNI.WAVE_ENTRY_format_set(this.swigCPtr, i);
    }

    public long getFlags() {
        return MilesBridgeJNI.WAVE_ENTRY_flags_get(this.swigCPtr);
    }

    public void setFlags(long j) {
        MilesBridgeJNI.WAVE_ENTRY_flags_set(this.swigCPtr, j);
    }

    /* renamed from: or */
    public void mo15954or(int i) {
        MilesBridgeJNI.WAVE_ENTRY_playback_rate_set(this.swigCPtr, i);
    }

    public int byN() {
        return MilesBridgeJNI.WAVE_ENTRY_playback_rate_get(this.swigCPtr);
    }
}
