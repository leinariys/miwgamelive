package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aMG */
/* compiled from: a */
public final class aMG {
    public static final aMG ipP = new aMG("DP_FLUSH", MilesBridgeJNI.DP_FLUSH_get());
    public static final aMG ipQ = new aMG("DP_DEFAULT_FILTER");
    public static final aMG ipR = new aMG("DP_DEFAULT_MERGE");
    public static final aMG ipS = new aMG("DP_COPY");
    public static final aMG ipT = new aMG("DP_MC_COPY");
    public static final aMG ipU = new aMG("DP_ADPCM_DECODE");
    public static final aMG ipV = new aMG("N_DIGDRV_STAGES");
    public static final aMG ipW = new aMG("DIGDRV_ALL_STAGES");
    private static aMG[] ipX = {ipP, ipQ, ipR, ipS, ipT, ipU, ipV, ipW};

    /* renamed from: pF */
    private static int f3340pF = 0;

    /* renamed from: pG */
    private final int f3341pG;

    /* renamed from: pH */
    private final String f3342pH;

    private aMG(String str) {
        this.f3342pH = str;
        int i = f3340pF;
        f3340pF = i + 1;
        this.f3341pG = i;
    }

    private aMG(String str, int i) {
        this.f3342pH = str;
        this.f3341pG = i;
        f3340pF = i + 1;
    }

    private aMG(String str, aMG amg) {
        this.f3342pH = str;
        this.f3341pG = amg.f3341pG;
        f3340pF = this.f3341pG + 1;
    }

    /* renamed from: yL */
    public static aMG m16330yL(int i) {
        if (i < ipX.length && i >= 0 && ipX[i].f3341pG == i) {
            return ipX[i];
        }
        for (int i2 = 0; i2 < ipX.length; i2++) {
            if (ipX[i2].f3341pG == i) {
                return ipX[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + aMG.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f3341pG;
    }

    public String toString() {
        return this.f3342pH;
    }
}
