package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.adN  reason: case insensitive filesystem */
/* compiled from: a */
public class C5937adN {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5937adN(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5937adN() {
        this(MilesBridgeJNI.new_AILDLSINFO(), true);
    }

    /* renamed from: a */
    public static long m20815a(C5937adN adn) {
        if (adn == null) {
            return 0;
        }
        return adn.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_AILDLSINFO(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getDescription() {
        return MilesBridgeJNI.AILDLSINFO_Description_get(this.swigCPtr);
    }

    public void setDescription(String str) {
        MilesBridgeJNI.AILDLSINFO_Description_set(this.swigCPtr, str);
    }

    /* renamed from: pV */
    public void mo12825pV(int i) {
        MilesBridgeJNI.AILDLSINFO_MaxDLSMemory_set(this.swigCPtr, i);
    }

    public int bSk() {
        return MilesBridgeJNI.AILDLSINFO_MaxDLSMemory_get(this.swigCPtr);
    }

    /* renamed from: pW */
    public void mo12826pW(int i) {
        MilesBridgeJNI.AILDLSINFO_CurrentDLSMemory_set(this.swigCPtr, i);
    }

    public int bSl() {
        return MilesBridgeJNI.AILDLSINFO_CurrentDLSMemory_get(this.swigCPtr);
    }

    /* renamed from: pX */
    public void mo12827pX(int i) {
        MilesBridgeJNI.AILDLSINFO_LargestSize_set(this.swigCPtr, i);
    }

    public int bSm() {
        return MilesBridgeJNI.AILDLSINFO_LargestSize_get(this.swigCPtr);
    }

    /* renamed from: pY */
    public void mo12828pY(int i) {
        MilesBridgeJNI.AILDLSINFO_GMAvailable_set(this.swigCPtr, i);
    }

    public int bSn() {
        return MilesBridgeJNI.AILDLSINFO_GMAvailable_get(this.swigCPtr);
    }

    /* renamed from: pZ */
    public void mo12829pZ(int i) {
        MilesBridgeJNI.AILDLSINFO_GMBankSize_set(this.swigCPtr, i);
    }

    public int bSo() {
        return MilesBridgeJNI.AILDLSINFO_GMBankSize_get(this.swigCPtr);
    }
}
