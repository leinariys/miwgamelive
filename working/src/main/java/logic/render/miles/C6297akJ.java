package logic.render.miles;


import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.akJ  reason: case insensitive filesystem */
/* compiled from: a */
public class C6297akJ {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6297akJ(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6297akJ() {
        this(MilesBridgeJNI.new_MDI_DRIVER(), true);
    }

    /* renamed from: j */
    public static long m23148j(C6297akJ akj) {
        if (akj == null) {
            return 0;
        }
        return akj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_MDI_DRIVER(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getTag() {
        return MilesBridgeJNI.MDI_DRIVER_tag_get(this.swigCPtr);
    }

    public void setTag(String str) {
        MilesBridgeJNI.MDI_DRIVER_tag_set(this.swigCPtr, str);
    }

    /* renamed from: sg */
    public void mo14304sg(int i) {
        MilesBridgeJNI.MDI_DRIVER_timer_set(this.swigCPtr, i);
    }

    public int cgS() {
        return MilesBridgeJNI.MDI_DRIVER_timer_get(this.swigCPtr);
    }

    /* renamed from: sh */
    public void mo14305sh(int i) {
        MilesBridgeJNI.MDI_DRIVER_interval_time_set(this.swigCPtr, i);
    }

    public int cgT() {
        return MilesBridgeJNI.MDI_DRIVER_interval_time_get(this.swigCPtr);
    }

    /* renamed from: si */
    public void mo14306si(int i) {
        MilesBridgeJNI.MDI_DRIVER_disable_set(this.swigCPtr, i);
    }

    public int cgU() {
        return MilesBridgeJNI.MDI_DRIVER_disable_get(this.swigCPtr);
    }

    /* renamed from: j */
    public void mo14298j(C6481anl anl) {
        MilesBridgeJNI.MDI_DRIVER_sequences_set(this.swigCPtr, C6481anl.m24294k(anl));
    }

    public C6481anl cgV() {
        long MDI_DRIVER_sequences_get = MilesBridgeJNI.MDI_DRIVER_sequences_get(this.swigCPtr);
        if (MDI_DRIVER_sequences_get == 0) {
            return null;
        }
        return new C6481anl(MDI_DRIVER_sequences_get, false);
    }

    /* renamed from: sj */
    public void mo14307sj(int i) {
        MilesBridgeJNI.MDI_DRIVER_n_sequences_set(this.swigCPtr, i);
    }

    public int cgW() {
        return MilesBridgeJNI.MDI_DRIVER_n_sequences_get(this.swigCPtr);
    }

    /* renamed from: P */
    public void mo14260P(C2480fe feVar) {
        MilesBridgeJNI.MDI_DRIVER_lock_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe cgX() {
        long MDI_DRIVER_lock_get = MilesBridgeJNI.MDI_DRIVER_lock_get(this.swigCPtr);
        if (MDI_DRIVER_lock_get == 0) {
            return null;
        }
        return new C2480fe(MDI_DRIVER_lock_get, false);
    }

    /* renamed from: b */
    public void mo14267b(C2811kS kSVar) {
        MilesBridgeJNI.MDI_DRIVER_locker_set(this.swigCPtr, C2811kS.m34432a(kSVar));
    }

    public C2811kS cgY() {
        long MDI_DRIVER_locker_get = MilesBridgeJNI.MDI_DRIVER_locker_get(this.swigCPtr);
        if (MDI_DRIVER_locker_get == 0) {
            return null;
        }
        return new C2811kS(MDI_DRIVER_locker_get, false);
    }

    /* renamed from: c */
    public void mo14270c(C2811kS kSVar) {
        MilesBridgeJNI.MDI_DRIVER_owner_set(this.swigCPtr, C2811kS.m34432a(kSVar));
    }

    public C2811kS cgZ() {
        long MDI_DRIVER_owner_get = MilesBridgeJNI.MDI_DRIVER_owner_get(this.swigCPtr);
        if (MDI_DRIVER_owner_get == 0) {
            return null;
        }
        return new C2811kS(MDI_DRIVER_owner_get, false);
    }

    /* renamed from: d */
    public void mo14293d(C2811kS kSVar) {
        MilesBridgeJNI.MDI_DRIVER_user_set(this.swigCPtr, C2811kS.m34432a(kSVar));
    }

    public C2811kS cha() {
        long MDI_DRIVER_user_get = MilesBridgeJNI.MDI_DRIVER_user_get(this.swigCPtr);
        if (MDI_DRIVER_user_get == 0) {
            return null;
        }
        return new C2811kS(MDI_DRIVER_user_get, false);
    }

    /* renamed from: Q */
    public void mo14261Q(C2480fe feVar) {
        MilesBridgeJNI.MDI_DRIVER_state_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe chb() {
        long MDI_DRIVER_state_get = MilesBridgeJNI.MDI_DRIVER_state_get(this.swigCPtr);
        if (MDI_DRIVER_state_get == 0) {
            return null;
        }
        return new C2480fe(MDI_DRIVER_state_get, false);
    }

    /* renamed from: R */
    public void mo14262R(C2480fe feVar) {
        MilesBridgeJNI.MDI_DRIVER_notes_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe chc() {
        long MDI_DRIVER_notes_get = MilesBridgeJNI.MDI_DRIVER_notes_get(this.swigCPtr);
        if (MDI_DRIVER_notes_get == 0) {
            return null;
        }
        return new C2480fe(MDI_DRIVER_notes_get, false);
    }

    /* renamed from: b */
    public void mo14265b(C5550aNq anq) {
        MilesBridgeJNI.MDI_DRIVER_event_trap_set(this.swigCPtr, C5550aNq.m16778c(anq));
    }

    public C5550aNq chd() {
        long MDI_DRIVER_event_trap_get = MilesBridgeJNI.MDI_DRIVER_event_trap_get(this.swigCPtr);
        if (MDI_DRIVER_event_trap_get == 0) {
            return null;
        }
        return new C5550aNq(MDI_DRIVER_event_trap_get, false);
    }

    /* renamed from: c */
    public void mo14269c(C5944adU adu) {
        MilesBridgeJNI.MDI_DRIVER_timbre_trap_set(this.swigCPtr, C5944adU.m20899b(adu));
    }

    public C5944adU che() {
        long MDI_DRIVER_timbre_trap_get = MilesBridgeJNI.MDI_DRIVER_timbre_trap_get(this.swigCPtr);
        if (MDI_DRIVER_timbre_trap_get == 0) {
            return null;
        }
        return new C5944adU(MDI_DRIVER_timbre_trap_get, false);
    }

    /* renamed from: sk */
    public void mo14308sk(int i) {
        MilesBridgeJNI.MDI_DRIVER_master_volume_set(this.swigCPtr, i);
    }

    public int chf() {
        return MilesBridgeJNI.MDI_DRIVER_master_volume_get(this.swigCPtr);
    }

    /* renamed from: o */
    public void mo14300o(C2480fe feVar) {
        MilesBridgeJNI.MDI_DRIVER_system_data_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bzk() {
        long MDI_DRIVER_system_data_get = MilesBridgeJNI.MDI_DRIVER_system_data_get(this.swigCPtr);
        if (MDI_DRIVER_system_data_get == 0) {
            return null;
        }
        return new C2480fe(MDI_DRIVER_system_data_get, false);
    }

    /* renamed from: sl */
    public void mo14309sl(int i) {
        MilesBridgeJNI.MDI_DRIVER_released_set(this.swigCPtr, i);
    }

    public int chg() {
        return MilesBridgeJNI.MDI_DRIVER_released_get(this.swigCPtr);
    }

    /* renamed from: hM */
    public void mo14297hM(long j) {
        MilesBridgeJNI.MDI_DRIVER_deviceid_set(this.swigCPtr, j);
    }

    public long chh() {
        return MilesBridgeJNI.MDI_DRIVER_deviceid_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo14266b(C6626aqa aqa) {
        MilesBridgeJNI.MDI_DRIVER_sysdata_set(this.swigCPtr, C6626aqa.m25149e(aqa));
    }

    public C6626aqa chi() {
        long MDI_DRIVER_sysdata_get = MilesBridgeJNI.MDI_DRIVER_sysdata_get(this.swigCPtr);
        if (MDI_DRIVER_sysdata_get == 0) {
            return null;
        }
        return new C6626aqa(MDI_DRIVER_sysdata_get, false);
    }

    /* renamed from: a */
    public void mo14263a(aLF alf) {
        MilesBridgeJNI.MDI_DRIVER_mhdr_set(this.swigCPtr, aLF.m16103b(alf));
    }

    public aLF chj() {
        long MDI_DRIVER_mhdr_get = MilesBridgeJNI.MDI_DRIVER_mhdr_get(this.swigCPtr);
        if (MDI_DRIVER_mhdr_get == 0) {
            return null;
        }
        return new aLF(MDI_DRIVER_mhdr_get, false);
    }

    /* renamed from: k */
    public void mo14299k(C6297akJ akj) {
        MilesBridgeJNI.MDI_DRIVER_next_set(this.swigCPtr, m23148j(akj));
    }

    public C6297akJ chk() {
        long MDI_DRIVER_next_get = MilesBridgeJNI.MDI_DRIVER_next_get(this.swigCPtr);
        if (MDI_DRIVER_next_get == 0) {
            return null;
        }
        return new C6297akJ(MDI_DRIVER_next_get, false);
    }

    /* renamed from: rH */
    public void mo14301rH(int i) {
        MilesBridgeJNI.MDI_DRIVER_callingCT_set(this.swigCPtr, i);
    }

    public int cfD() {
        return MilesBridgeJNI.MDI_DRIVER_callingCT_get(this.swigCPtr);
    }

    /* renamed from: rI */
    public void mo14302rI(int i) {
        MilesBridgeJNI.MDI_DRIVER_callingDS_set(this.swigCPtr, i);
    }

    public int cfE() {
        return MilesBridgeJNI.MDI_DRIVER_callingDS_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo14264a(C6637aql aql) {
        MilesBridgeJNI.MDI_DRIVER_hMidiOut_set(this.swigCPtr, C6637aql.m25207b(aql));
    }

    public C6637aql chl() {
        long MDI_DRIVER_hMidiOut_get = MilesBridgeJNI.MDI_DRIVER_hMidiOut_get(this.swigCPtr);
        if (MDI_DRIVER_hMidiOut_get == 0) {
            return null;
        }
        return new C6637aql(MDI_DRIVER_hMidiOut_get, false);
    }
}
