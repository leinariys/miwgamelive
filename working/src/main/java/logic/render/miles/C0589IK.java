package logic.render.miles;


import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.IK */
/* compiled from: a */
public class C0589IK {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0589IK(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0589IK() {
        this(MilesBridgeJNI.new_WAVE_SYNTH(), true);
    }

    /* renamed from: a */
    public static long m5329a(C0589IK ik) {
        if (ik == null) {
            return 0;
        }
        return ik.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_WAVE_SYNTH(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo2767a(C6297akJ akj) {
        MilesBridgeJNI.WAVE_SYNTH_mdi_set(this.swigCPtr, C6297akJ.m23148j(akj));
    }

    public C6297akJ aoB() {
        long WAVE_SYNTH_mdi_get = MilesBridgeJNI.WAVE_SYNTH_mdi_get(this.swigCPtr);
        if (WAVE_SYNTH_mdi_get == 0) {
            return null;
        }
        return new C6297akJ(WAVE_SYNTH_mdi_get, false);
    }

    /* renamed from: a */
    public void mo2764a(aNZ anz) {
        MilesBridgeJNI.WAVE_SYNTH_dig_set(this.swigCPtr, aNZ.m16621q(anz));
    }

    public aNZ getDig() {
        long WAVE_SYNTH_dig_get = MilesBridgeJNI.WAVE_SYNTH_dig_get(this.swigCPtr);
        if (WAVE_SYNTH_dig_get == 0) {
            return null;
        }
        return new aNZ(WAVE_SYNTH_dig_get, false);
    }

    /* renamed from: a */
    public void mo2768a(C6734ase ase) {
        MilesBridgeJNI.WAVE_SYNTH_library_set(this.swigCPtr, C6734ase.m25715b(ase));
    }

    public C6734ase aWJ() {
        long WAVE_SYNTH_library_get = MilesBridgeJNI.WAVE_SYNTH_library_get(this.swigCPtr);
        if (WAVE_SYNTH_library_get == 0) {
            return null;
        }
        return new C6734ase(WAVE_SYNTH_library_get, false);
    }

    /* renamed from: a */
    public void mo2765a(C5550aNq anq) {
        MilesBridgeJNI.WAVE_SYNTH_prev_event_fn_set(this.swigCPtr, C5550aNq.m16778c(anq));
    }

    public C5550aNq aWK() {
        long WAVE_SYNTH_prev_event_fn_get = MilesBridgeJNI.WAVE_SYNTH_prev_event_fn_get(this.swigCPtr);
        if (WAVE_SYNTH_prev_event_fn_get == 0) {
            return null;
        }
        return new C5550aNq(WAVE_SYNTH_prev_event_fn_get, false);
    }

    /* renamed from: a */
    public void mo2766a(C5944adU adu) {
        MilesBridgeJNI.WAVE_SYNTH_prev_timb_fn_set(this.swigCPtr, C5944adU.m20899b(adu));
    }

    public C5944adU aWL() {
        long WAVE_SYNTH_prev_timb_fn_get = MilesBridgeJNI.WAVE_SYNTH_prev_timb_fn_get(this.swigCPtr);
        if (WAVE_SYNTH_prev_timb_fn_get == 0) {
            return null;
        }
        return new C5944adU(WAVE_SYNTH_prev_timb_fn_get, false);
    }

    /* renamed from: a */
    public void mo2762a(C1481Vn vn) {
        MilesBridgeJNI.WAVE_SYNTH_controls_set(this.swigCPtr, C1481Vn.m10855b(vn));
    }

    public C1481Vn aWM() {
        long WAVE_SYNTH_controls_get = MilesBridgeJNI.WAVE_SYNTH_controls_get(this.swigCPtr);
        if (WAVE_SYNTH_controls_get == 0) {
            return null;
        }
        return new C1481Vn(WAVE_SYNTH_controls_get, false);
    }

    /* renamed from: a */
    public void mo2769a(ayS ays) {
        MilesBridgeJNI.WAVE_SYNTH_wave_set(this.swigCPtr, ayS.m27395b(ays));
    }

    public ayS aWN() {
        long WAVE_SYNTH_wave_get = MilesBridgeJNI.WAVE_SYNTH_wave_get(this.swigCPtr);
        if (WAVE_SYNTH_wave_get == 0) {
            return null;
        }
        return new ayS(WAVE_SYNTH_wave_get, false);
    }

    /* renamed from: a */
    public void mo2763a(C1527WT wt) {
        MilesBridgeJNI.WAVE_SYNTH_S_set(this.swigCPtr, C1527WT.m11210b(wt));
    }

    public C1527WT aWO() {
        long WAVE_SYNTH_S_get = MilesBridgeJNI.WAVE_SYNTH_S_get(this.swigCPtr);
        if (WAVE_SYNTH_S_get == 0) {
            return null;
        }
        return new C1527WT(WAVE_SYNTH_S_get, false);
    }

    /* renamed from: kV */
    public void mo2794kV(int i) {
        MilesBridgeJNI.WAVE_SYNTH_n_voices_set(this.swigCPtr, i);
    }

    public int aWP() {
        return MilesBridgeJNI.WAVE_SYNTH_n_voices_get(this.swigCPtr);
    }

    /* renamed from: h */
    public void mo2790h(C2480fe feVar) {
        MilesBridgeJNI.WAVE_SYNTH_chan_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe aWQ() {
        long WAVE_SYNTH_chan_get = MilesBridgeJNI.WAVE_SYNTH_chan_get(this.swigCPtr);
        if (WAVE_SYNTH_chan_get == 0) {
            return null;
        }
        return new C2480fe(WAVE_SYNTH_chan_get, false);
    }

    /* renamed from: i */
    public void mo2791i(C2480fe feVar) {
        MilesBridgeJNI.WAVE_SYNTH_note_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe aWR() {
        long WAVE_SYNTH_note_get = MilesBridgeJNI.WAVE_SYNTH_note_get(this.swigCPtr);
        if (WAVE_SYNTH_note_get == 0) {
            return null;
        }
        return new C2480fe(WAVE_SYNTH_note_get, false);
    }

    /* renamed from: j */
    public void mo2792j(C2480fe feVar) {
        MilesBridgeJNI.WAVE_SYNTH_root_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe aWS() {
        long WAVE_SYNTH_root_get = MilesBridgeJNI.WAVE_SYNTH_root_get(this.swigCPtr);
        if (WAVE_SYNTH_root_get == 0) {
            return null;
        }
        return new C2480fe(WAVE_SYNTH_root_get, false);
    }

    /* renamed from: k */
    public void mo2793k(C2480fe feVar) {
        MilesBridgeJNI.WAVE_SYNTH_rate_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe aWT() {
        long WAVE_SYNTH_rate_get = MilesBridgeJNI.WAVE_SYNTH_rate_get(this.swigCPtr);
        if (WAVE_SYNTH_rate_get == 0) {
            return null;
        }
        return new C2480fe(WAVE_SYNTH_rate_get, false);
    }

    /* renamed from: l */
    public void mo2795l(C2480fe feVar) {
        MilesBridgeJNI.WAVE_SYNTH_vel_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe aWU() {
        long WAVE_SYNTH_vel_get = MilesBridgeJNI.WAVE_SYNTH_vel_get(this.swigCPtr);
        if (WAVE_SYNTH_vel_get == 0) {
            return null;
        }
        return new C2480fe(WAVE_SYNTH_vel_get, false);
    }

    /* renamed from: b */
    public void mo2785b(C5758aVq avq) {
        MilesBridgeJNI.WAVE_SYNTH_time_set(this.swigCPtr, C5758aVq.m19143h(avq));
    }

    public C5758aVq aWV() {
        long WAVE_SYNTH_time_get = MilesBridgeJNI.WAVE_SYNTH_time_get(this.swigCPtr);
        if (WAVE_SYNTH_time_get == 0) {
            return null;
        }
        return new C5758aVq(WAVE_SYNTH_time_get, false);
    }

    /* renamed from: eT */
    public void mo2787eT(long j) {
        MilesBridgeJNI.WAVE_SYNTH_event_set(this.swigCPtr, j);
    }

    public long aWW() {
        return MilesBridgeJNI.WAVE_SYNTH_event_get(this.swigCPtr);
    }
}
