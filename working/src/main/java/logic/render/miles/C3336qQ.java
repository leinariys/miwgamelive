package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.qQ */
/* compiled from: a */
public class C3336qQ {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3336qQ(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3336qQ() {
        this(MilesBridgeJNI.new_DLSFILEID(), true);
    }

    /* renamed from: a */
    public static long m37519a(C3336qQ qQVar) {
        if (qQVar == null) {
            return 0;
        }
        return qQVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_DLSFILEID(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public int getId() {
        return MilesBridgeJNI.DLSFILEID_id_get(this.swigCPtr);
    }

    public void setId(int i) {
        MilesBridgeJNI.DLSFILEID_id_set(this.swigCPtr, i);
    }

    /* renamed from: b */
    public void mo21349b(C3336qQ qQVar) {
        MilesBridgeJNI.DLSFILEID_next_set(this.swigCPtr, m37519a(qQVar));
    }

    /* renamed from: XI */
    public C3336qQ mo21348XI() {
        long DLSFILEID_next_get = MilesBridgeJNI.DLSFILEID_next_get(this.swigCPtr);
        if (DLSFILEID_next_get == 0) {
            return null;
        }
        return new C3336qQ(DLSFILEID_next_get, false);
    }
}
