package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.anl  reason: case insensitive filesystem */
/* compiled from: a */
public class C6481anl {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6481anl(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6481anl() {
        this(MilesBridgeJNI.new_SEQUENCE(), true);
    }

    /* renamed from: k */
    public static long m24294k(C6481anl anl) {
        if (anl == null) {
            return 0;
        }
        return anl.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_SEQUENCE(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getTag() {
        return MilesBridgeJNI.SEQUENCE_tag_get(this.swigCPtr);
    }

    public void setTag(String str) {
        MilesBridgeJNI.SEQUENCE_tag_set(this.swigCPtr, str);
    }

    /* renamed from: l */
    public void mo15090l(C6297akJ akj) {
        MilesBridgeJNI.SEQUENCE_driver_set(this.swigCPtr, C6297akJ.m23148j(akj));
    }

    public C6297akJ ckO() {
        long SEQUENCE_driver_get = MilesBridgeJNI.SEQUENCE_driver_get(this.swigCPtr);
        if (SEQUENCE_driver_get == 0) {
            return null;
        }
        return new C6297akJ(SEQUENCE_driver_get, false);
    }

    /* renamed from: hK */
    public void mo15089hK(long j) {
        MilesBridgeJNI.SEQUENCE_status_set(this.swigCPtr, j);
    }

    public long cfz() {
        return MilesBridgeJNI.SEQUENCE_status_get(this.swigCPtr);
    }

    /* renamed from: d */
    public void mo15079d(aBO abo) {
        MilesBridgeJNI.SEQUENCE_TIMB_set(this.swigCPtr, aBO.m12884g(abo));
    }

    public aBO ckP() {
        long SEQUENCE_TIMB_get = MilesBridgeJNI.SEQUENCE_TIMB_get(this.swigCPtr);
        if (SEQUENCE_TIMB_get == 0) {
            return null;
        }
        return new aBO(SEQUENCE_TIMB_get, false);
    }

    /* renamed from: e */
    public void mo15083e(aBO abo) {
        MilesBridgeJNI.SEQUENCE_RBRN_set(this.swigCPtr, aBO.m12884g(abo));
    }

    public aBO ckQ() {
        long SEQUENCE_RBRN_get = MilesBridgeJNI.SEQUENCE_RBRN_get(this.swigCPtr);
        if (SEQUENCE_RBRN_get == 0) {
            return null;
        }
        return new aBO(SEQUENCE_RBRN_get, false);
    }

    /* renamed from: f */
    public void mo15084f(aBO abo) {
        MilesBridgeJNI.SEQUENCE_EVNT_set(this.swigCPtr, aBO.m12884g(abo));
    }

    public aBO ckR() {
        long SEQUENCE_EVNT_get = MilesBridgeJNI.SEQUENCE_EVNT_get(this.swigCPtr);
        if (SEQUENCE_EVNT_get == 0) {
            return null;
        }
        return new aBO(SEQUENCE_EVNT_get, false);
    }

    /* renamed from: c */
    public void mo15043c(C6626aqa aqa) {
        MilesBridgeJNI.SEQUENCE_EVNT_ptr_set(this.swigCPtr, C6626aqa.m25149e(aqa));
    }

    public C6626aqa ckS() {
        long SEQUENCE_EVNT_ptr_get = MilesBridgeJNI.SEQUENCE_EVNT_ptr_get(this.swigCPtr);
        if (SEQUENCE_EVNT_ptr_get == 0) {
            return null;
        }
        return new C6626aqa(SEQUENCE_EVNT_ptr_get, false);
    }

    /* renamed from: d */
    public void mo15080d(C6626aqa aqa) {
        MilesBridgeJNI.SEQUENCE_ICA_set(this.swigCPtr, C6626aqa.m25149e(aqa));
    }

    public C6626aqa ckT() {
        long SEQUENCE_ICA_get = MilesBridgeJNI.SEQUENCE_ICA_get(this.swigCPtr);
        if (SEQUENCE_ICA_get == 0) {
            return null;
        }
        return new C6626aqa(SEQUENCE_ICA_get, false);
    }

    /* renamed from: a */
    public void mo15037a(C5687aSx asx) {
        MilesBridgeJNI.SEQUENCE_prefix_callback_set(this.swigCPtr, C5687aSx.m18319b(asx));
    }

    public C5687aSx ckU() {
        long SEQUENCE_prefix_callback_get = MilesBridgeJNI.SEQUENCE_prefix_callback_get(this.swigCPtr);
        if (SEQUENCE_prefix_callback_get == 0) {
            return null;
        }
        return new C5687aSx(SEQUENCE_prefix_callback_get, false);
    }

    /* renamed from: a */
    public void mo15036a(C5332aFg afg) {
        MilesBridgeJNI.SEQUENCE_trigger_callback_set(this.swigCPtr, C5332aFg.m14623b(afg));
    }

    public C5332aFg ckV() {
        long SEQUENCE_trigger_callback_get = MilesBridgeJNI.SEQUENCE_trigger_callback_get(this.swigCPtr);
        if (SEQUENCE_trigger_callback_get == 0) {
            return null;
        }
        return new C5332aFg(SEQUENCE_trigger_callback_get, false);
    }

    /* renamed from: b */
    public void mo15039b(C3656tL tLVar) {
        MilesBridgeJNI.SEQUENCE_beat_callback_set(this.swigCPtr, C3656tL.m39583a(tLVar));
    }

    public C3656tL ckW() {
        long SEQUENCE_beat_callback_get = MilesBridgeJNI.SEQUENCE_beat_callback_get(this.swigCPtr);
        if (SEQUENCE_beat_callback_get == 0) {
            return null;
        }
        return new C3656tL(SEQUENCE_beat_callback_get, false);
    }

    /* renamed from: b */
    public void mo15038b(C6362alW alw) {
        MilesBridgeJNI.SEQUENCE_EOS_set(this.swigCPtr, C6362alW.m23767a(alw));
    }

    public C6362alW ckX() {
        long SEQUENCE_EOS_get = MilesBridgeJNI.SEQUENCE_EOS_get(this.swigCPtr);
        if (SEQUENCE_EOS_get == 0) {
            return null;
        }
        return new C6362alW(SEQUENCE_EOS_get, false);
    }

    /* renamed from: ok */
    public void mo15092ok(int i) {
        MilesBridgeJNI.SEQUENCE_loop_count_set(this.swigCPtr, i);
    }

    public int byF() {
        return MilesBridgeJNI.SEQUENCE_loop_count_get(this.swigCPtr);
    }

    /* renamed from: sG */
    public void mo15093sG(int i) {
        MilesBridgeJNI.SEQUENCE_interval_count_set(this.swigCPtr, i);
    }

    public int ckY() {
        return MilesBridgeJNI.SEQUENCE_interval_count_get(this.swigCPtr);
    }

    /* renamed from: sH */
    public void mo15094sH(int i) {
        MilesBridgeJNI.SEQUENCE_interval_num_set(this.swigCPtr, i);
    }

    public int ckZ() {
        return MilesBridgeJNI.SEQUENCE_interval_num_get(this.swigCPtr);
    }

    public int getVolume() {
        return MilesBridgeJNI.SEQUENCE_volume_get(this.swigCPtr);
    }

    public void setVolume(int i) {
        MilesBridgeJNI.SEQUENCE_volume_set(this.swigCPtr, i);
    }

    /* renamed from: sI */
    public void mo15095sI(int i) {
        MilesBridgeJNI.SEQUENCE_volume_target_set(this.swigCPtr, i);
    }

    public int cla() {
        return MilesBridgeJNI.SEQUENCE_volume_target_get(this.swigCPtr);
    }

    /* renamed from: sJ */
    public void mo15096sJ(int i) {
        MilesBridgeJNI.SEQUENCE_volume_accum_set(this.swigCPtr, i);
    }

    public int clb() {
        return MilesBridgeJNI.SEQUENCE_volume_accum_get(this.swigCPtr);
    }

    /* renamed from: sK */
    public void mo15097sK(int i) {
        MilesBridgeJNI.SEQUENCE_volume_period_set(this.swigCPtr, i);
    }

    public int clc() {
        return MilesBridgeJNI.SEQUENCE_volume_period_get(this.swigCPtr);
    }

    /* renamed from: sL */
    public void mo15098sL(int i) {
        MilesBridgeJNI.SEQUENCE_tempo_percent_set(this.swigCPtr, i);
    }

    public int cld() {
        return MilesBridgeJNI.SEQUENCE_tempo_percent_get(this.swigCPtr);
    }

    /* renamed from: sM */
    public void mo15099sM(int i) {
        MilesBridgeJNI.SEQUENCE_tempo_target_set(this.swigCPtr, i);
    }

    public int cle() {
        return MilesBridgeJNI.SEQUENCE_tempo_target_get(this.swigCPtr);
    }

    /* renamed from: sN */
    public void mo15100sN(int i) {
        MilesBridgeJNI.SEQUENCE_tempo_accum_set(this.swigCPtr, i);
    }

    public int clf() {
        return MilesBridgeJNI.SEQUENCE_tempo_accum_get(this.swigCPtr);
    }

    /* renamed from: sO */
    public void mo15101sO(int i) {
        MilesBridgeJNI.SEQUENCE_tempo_period_set(this.swigCPtr, i);
    }

    public int clg() {
        return MilesBridgeJNI.SEQUENCE_tempo_period_get(this.swigCPtr);
    }

    /* renamed from: sP */
    public void mo15102sP(int i) {
        MilesBridgeJNI.SEQUENCE_tempo_error_set(this.swigCPtr, i);
    }

    public int clh() {
        return MilesBridgeJNI.SEQUENCE_tempo_error_get(this.swigCPtr);
    }

    /* renamed from: sQ */
    public void mo15103sQ(int i) {
        MilesBridgeJNI.SEQUENCE_beat_count_set(this.swigCPtr, i);
    }

    public int cli() {
        return MilesBridgeJNI.SEQUENCE_beat_count_get(this.swigCPtr);
    }

    /* renamed from: sR */
    public void mo15104sR(int i) {
        MilesBridgeJNI.SEQUENCE_measure_count_set(this.swigCPtr, i);
    }

    public int clj() {
        return MilesBridgeJNI.SEQUENCE_measure_count_get(this.swigCPtr);
    }

    /* renamed from: sS */
    public void mo15105sS(int i) {
        MilesBridgeJNI.SEQUENCE_time_numerator_set(this.swigCPtr, i);
    }

    public int clk() {
        return MilesBridgeJNI.SEQUENCE_time_numerator_get(this.swigCPtr);
    }

    /* renamed from: sT */
    public void mo15106sT(int i) {
        MilesBridgeJNI.SEQUENCE_time_fraction_set(this.swigCPtr, i);
    }

    public int cll() {
        return MilesBridgeJNI.SEQUENCE_time_fraction_get(this.swigCPtr);
    }

    /* renamed from: sU */
    public void mo15107sU(int i) {
        MilesBridgeJNI.SEQUENCE_beat_fraction_set(this.swigCPtr, i);
    }

    public int clm() {
        return MilesBridgeJNI.SEQUENCE_beat_fraction_get(this.swigCPtr);
    }

    /* renamed from: sV */
    public void mo15108sV(int i) {
        MilesBridgeJNI.SEQUENCE_time_per_beat_set(this.swigCPtr, i);
    }

    public int cln() {
        return MilesBridgeJNI.SEQUENCE_time_per_beat_get(this.swigCPtr);
    }

    /* renamed from: d */
    public void mo15081d(C2224cz czVar) {
        MilesBridgeJNI.SEQUENCE_FOR_ptrs_set(this.swigCPtr, C2224cz.m28711a(czVar));
    }

    public C2224cz clo() {
        long SEQUENCE_FOR_ptrs_get = MilesBridgeJNI.SEQUENCE_FOR_ptrs_get(this.swigCPtr);
        if (SEQUENCE_FOR_ptrs_get == 0) {
            return null;
        }
        return new C2224cz(SEQUENCE_FOR_ptrs_get, false);
    }

    /* renamed from: S */
    public void mo15031S(C2480fe feVar) {
        MilesBridgeJNI.SEQUENCE_FOR_loop_count_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe clp() {
        long SEQUENCE_FOR_loop_count_get = MilesBridgeJNI.SEQUENCE_FOR_loop_count_get(this.swigCPtr);
        if (SEQUENCE_FOR_loop_count_get == 0) {
            return null;
        }
        return new C2480fe(SEQUENCE_FOR_loop_count_get, false);
    }

    /* renamed from: T */
    public void mo15032T(C2480fe feVar) {
        MilesBridgeJNI.SEQUENCE_chan_map_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe clq() {
        long SEQUENCE_chan_map_get = MilesBridgeJNI.SEQUENCE_chan_map_get(this.swigCPtr);
        if (SEQUENCE_chan_map_get == 0) {
            return null;
        }
        return new C2480fe(SEQUENCE_chan_map_get, false);
    }

    /* renamed from: c */
    public void mo15042c(C1481Vn vn) {
        MilesBridgeJNI.SEQUENCE_shadow_set(this.swigCPtr, C1481Vn.m10855b(vn));
    }

    public C1481Vn clr() {
        long SEQUENCE_shadow_get = MilesBridgeJNI.SEQUENCE_shadow_get(this.swigCPtr);
        if (SEQUENCE_shadow_get == 0) {
            return null;
        }
        return new C1481Vn(SEQUENCE_shadow_get, false);
    }

    /* renamed from: sW */
    public void mo15109sW(int i) {
        MilesBridgeJNI.SEQUENCE_note_count_set(this.swigCPtr, i);
    }

    public int cls() {
        return MilesBridgeJNI.SEQUENCE_note_count_get(this.swigCPtr);
    }

    /* renamed from: U */
    public void mo15033U(C2480fe feVar) {
        MilesBridgeJNI.SEQUENCE_note_chan_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe clt() {
        long SEQUENCE_note_chan_get = MilesBridgeJNI.SEQUENCE_note_chan_get(this.swigCPtr);
        if (SEQUENCE_note_chan_get == 0) {
            return null;
        }
        return new C2480fe(SEQUENCE_note_chan_get, false);
    }

    /* renamed from: V */
    public void mo15034V(C2480fe feVar) {
        MilesBridgeJNI.SEQUENCE_note_num_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe clu() {
        long SEQUENCE_note_num_get = MilesBridgeJNI.SEQUENCE_note_num_get(this.swigCPtr);
        if (SEQUENCE_note_num_get == 0) {
            return null;
        }
        return new C2480fe(SEQUENCE_note_num_get, false);
    }

    /* renamed from: W */
    public void mo15035W(C2480fe feVar) {
        MilesBridgeJNI.SEQUENCE_note_time_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe clv() {
        long SEQUENCE_note_time_get = MilesBridgeJNI.SEQUENCE_note_time_get(this.swigCPtr);
        if (SEQUENCE_note_time_get == 0) {
            return null;
        }
        return new C2480fe(SEQUENCE_note_time_get, false);
    }

    /* renamed from: g */
    public void mo15086g(C2480fe feVar) {
        MilesBridgeJNI.SEQUENCE_user_data_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    /* renamed from: Bb */
    public C2480fe mo15030Bb() {
        long SEQUENCE_user_data_get = MilesBridgeJNI.SEQUENCE_user_data_get(this.swigCPtr);
        if (SEQUENCE_user_data_get == 0) {
            return null;
        }
        return new C2480fe(SEQUENCE_user_data_get, false);
    }

    /* renamed from: o */
    public void mo15091o(C2480fe feVar) {
        MilesBridgeJNI.SEQUENCE_system_data_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bzk() {
        long SEQUENCE_system_data_get = MilesBridgeJNI.SEQUENCE_system_data_get(this.swigCPtr);
        if (SEQUENCE_system_data_get == 0) {
            return null;
        }
        return new C2480fe(SEQUENCE_system_data_get, false);
    }
}
