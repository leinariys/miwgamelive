package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.LC */
/* compiled from: a */
public class C0774LC {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0774LC(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0774LC() {
        this(MilesBridgeJNI.new_MWAVEFORMAT(), true);
    }

    /* renamed from: a */
    public static long m6650a(C0774LC lc) {
        if (lc == null) {
            return 0;
        }
        return lc.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_MWAVEFORMAT(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: lC */
    public void mo3709lC(int i) {
        MilesBridgeJNI.MWAVEFORMAT_wFormatTag_set(this.swigCPtr, i);
    }

    public int bfO() {
        return MilesBridgeJNI.MWAVEFORMAT_wFormatTag_get(this.swigCPtr);
    }

    /* renamed from: lD */
    public void mo3710lD(int i) {
        MilesBridgeJNI.MWAVEFORMAT_nChannels_set(this.swigCPtr, i);
    }

    public int bfP() {
        return MilesBridgeJNI.MWAVEFORMAT_nChannels_get(this.swigCPtr);
    }

    /* renamed from: eX */
    public void mo3706eX(long j) {
        MilesBridgeJNI.MWAVEFORMAT_nSamplesPerSec_set(this.swigCPtr, j);
    }

    public long bfQ() {
        return MilesBridgeJNI.MWAVEFORMAT_nSamplesPerSec_get(this.swigCPtr);
    }

    /* renamed from: eY */
    public void mo3707eY(long j) {
        MilesBridgeJNI.MWAVEFORMAT_nAvgBytesPerSec_set(this.swigCPtr, j);
    }

    public long bfR() {
        return MilesBridgeJNI.MWAVEFORMAT_nAvgBytesPerSec_get(this.swigCPtr);
    }

    /* renamed from: lE */
    public void mo3711lE(int i) {
        MilesBridgeJNI.MWAVEFORMAT_nBlockAlign_set(this.swigCPtr, i);
    }

    public int bfS() {
        return MilesBridgeJNI.MWAVEFORMAT_nBlockAlign_get(this.swigCPtr);
    }
}
