package logic.render.miles;

/* renamed from: a.ami  reason: case insensitive filesystem */
/* compiled from: a */
public class C6426ami {
    private long swigCPtr;

    public C6426ami(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6426ami() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m23976a(C6426ami ami) {
        if (ami == null) {
            return 0;
        }
        return ami.swigCPtr;
    }
}
