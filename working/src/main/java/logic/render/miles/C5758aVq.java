package logic.render.miles;

/* renamed from: a.aVq  reason: case insensitive filesystem */
/* compiled from: a */
public class C5758aVq {
    private long swigCPtr;

    public C5758aVq(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5758aVq() {
        this.swigCPtr = 0;
    }

    /* renamed from: h */
    public static long m19143h(C5758aVq avq) {
        if (avq == null) {
            return 0;
        }
        return avq.swigCPtr;
    }
}
