package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aDx  reason: case insensitive filesystem */
/* compiled from: a */
public final class C5297aDx {
    public static final C5297aDx hBO = new C5297aDx("SP_ASI_DECODER", MilesBridgeJNI.SP_ASI_DECODER_get());
    public static final C5297aDx hBP = new C5297aDx("SP_FILTER");
    public static final C5297aDx hBQ = new C5297aDx("SP_FILTER_0", MilesBridgeJNI.SP_FILTER_0_get());
    public static final C5297aDx hBR = new C5297aDx("SP_FILTER_1");
    public static final C5297aDx hBS = new C5297aDx("SP_FILTER_2");
    public static final C5297aDx hBT = new C5297aDx("SP_FILTER_3");
    public static final C5297aDx hBU = new C5297aDx("SP_FILTER_4");
    public static final C5297aDx hBV = new C5297aDx("SP_FILTER_5");
    public static final C5297aDx hBW = new C5297aDx("SP_FILTER_6");
    public static final C5297aDx hBX = new C5297aDx("SP_FILTER_7");
    public static final C5297aDx hBY = new C5297aDx("SP_MERGE");
    public static final C5297aDx hBZ = new C5297aDx("N_SAMPLE_STAGES");
    public static final C5297aDx hCa = new C5297aDx("SP_OUTPUT", MilesBridgeJNI.SP_OUTPUT_get());
    public static final C5297aDx hCb = new C5297aDx("SAMPLE_ALL_STAGES");
    private static C5297aDx[] hCc = {hBO, hBP, hBQ, hBR, hBS, hBT, hBU, hBV, hBW, hBX, hBY, hBZ, hCa, hCb};

    /* renamed from: pF */
    private static int f2606pF = 0;

    /* renamed from: pG */
    private final int f2607pG;

    /* renamed from: pH */
    private final String f2608pH;

    private C5297aDx(String str) {
        this.f2608pH = str;
        int i = f2606pF;
        f2606pF = i + 1;
        this.f2607pG = i;
    }

    private C5297aDx(String str, int i) {
        this.f2608pH = str;
        this.f2607pG = i;
        f2606pF = i + 1;
    }

    private C5297aDx(String str, C5297aDx adx) {
        this.f2608pH = str;
        this.f2607pG = adx.f2607pG;
        f2606pF = this.f2607pG + 1;
    }

    /* renamed from: xC */
    public static C5297aDx m13923xC(int i) {
        if (i < hCc.length && i >= 0 && hCc[i].f2607pG == i) {
            return hCc[i];
        }
        for (int i2 = 0; i2 < hCc.length; i2++) {
            if (hCc[i2].f2607pG == i) {
                return hCc[i2];
            }
        }
        throw new IllegalArgumentException("No enum " + C5297aDx.class + " with value " + i);
    }

    public final int swigValue() {
        return this.f2607pG;
    }

    public String toString() {
        return this.f2608pH;
    }
}
