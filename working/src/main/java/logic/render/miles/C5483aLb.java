package logic.render.miles;

/* renamed from: a.aLb  reason: case insensitive filesystem */
/* compiled from: a */
public class C5483aLb {
    private long swigCPtr;

    public C5483aLb(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5483aLb() {
        this.swigCPtr = 0;
    }

    /* renamed from: d */
    public static long m16216d(C5483aLb alb) {
        if (alb == null) {
            return 0;
        }
        return alb.swigCPtr;
    }
}
