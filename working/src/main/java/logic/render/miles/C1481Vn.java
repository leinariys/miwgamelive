package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.Vn */
/* compiled from: a */
public class C1481Vn {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1481Vn(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1481Vn() {
        this(MilesBridgeJNI.new_CTRL_LOG(), true);
    }

    /* renamed from: b */
    public static long m10855b(C1481Vn vn) {
        if (vn == null) {
            return 0;
        }
        return vn.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_CTRL_LOG(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: q */
    public void mo6408q(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_program_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAl() {
        long CTRL_LOG_program_get = MilesBridgeJNI.CTRL_LOG_program_get(this.swigCPtr);
        if (CTRL_LOG_program_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_program_get, false);
    }

    /* renamed from: r */
    public void mo6409r(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_pitch_l_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAm() {
        long CTRL_LOG_pitch_l_get = MilesBridgeJNI.CTRL_LOG_pitch_l_get(this.swigCPtr);
        if (CTRL_LOG_pitch_l_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_pitch_l_get, false);
    }

    /* renamed from: s */
    public void mo6410s(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_pitch_h_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAn() {
        long CTRL_LOG_pitch_h_get = MilesBridgeJNI.CTRL_LOG_pitch_h_get(this.swigCPtr);
        if (CTRL_LOG_pitch_h_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_pitch_h_get, false);
    }

    /* renamed from: t */
    public void mo6411t(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_c_lock_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAo() {
        long CTRL_LOG_c_lock_get = MilesBridgeJNI.CTRL_LOG_c_lock_get(this.swigCPtr);
        if (CTRL_LOG_c_lock_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_c_lock_get, false);
    }

    /* renamed from: u */
    public void mo6412u(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_c_prot_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAp() {
        long CTRL_LOG_c_prot_get = MilesBridgeJNI.CTRL_LOG_c_prot_get(this.swigCPtr);
        if (CTRL_LOG_c_prot_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_c_prot_get, false);
    }

    /* renamed from: v */
    public void mo6413v(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_c_mute_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAq() {
        long CTRL_LOG_c_mute_get = MilesBridgeJNI.CTRL_LOG_c_mute_get(this.swigCPtr);
        if (CTRL_LOG_c_mute_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_c_mute_get, false);
    }

    /* renamed from: w */
    public void mo6414w(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_c_v_prot_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAr() {
        long CTRL_LOG_c_v_prot_get = MilesBridgeJNI.CTRL_LOG_c_v_prot_get(this.swigCPtr);
        if (CTRL_LOG_c_v_prot_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_c_v_prot_get, false);
    }

    /* renamed from: x */
    public void mo6415x(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_bank_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAs() {
        long CTRL_LOG_bank_get = MilesBridgeJNI.CTRL_LOG_bank_get(this.swigCPtr);
        if (CTRL_LOG_bank_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_bank_get, false);
    }

    /* renamed from: y */
    public void mo6416y(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_gm_bank_l_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAt() {
        long CTRL_LOG_gm_bank_l_get = MilesBridgeJNI.CTRL_LOG_gm_bank_l_get(this.swigCPtr);
        if (CTRL_LOG_gm_bank_l_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_gm_bank_l_get, false);
    }

    /* renamed from: z */
    public void mo6417z(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_gm_bank_m_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAu() {
        long CTRL_LOG_gm_bank_m_get = MilesBridgeJNI.CTRL_LOG_gm_bank_m_get(this.swigCPtr);
        if (CTRL_LOG_gm_bank_m_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_gm_bank_m_get, false);
    }

    /* renamed from: A */
    public void mo6372A(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_indirect_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAv() {
        long CTRL_LOG_indirect_get = MilesBridgeJNI.CTRL_LOG_indirect_get(this.swigCPtr);
        if (CTRL_LOG_indirect_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_indirect_get, false);
    }

    /* renamed from: B */
    public void mo6373B(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_callback_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAw() {
        long CTRL_LOG_callback_get = MilesBridgeJNI.CTRL_LOG_callback_get(this.swigCPtr);
        if (CTRL_LOG_callback_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_callback_get, false);
    }

    /* renamed from: C */
    public void mo6374C(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_mod_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAx() {
        long CTRL_LOG_mod_get = MilesBridgeJNI.CTRL_LOG_mod_get(this.swigCPtr);
        if (CTRL_LOG_mod_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_mod_get, false);
    }

    /* renamed from: D */
    public void mo6375D(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_vol_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAy() {
        long CTRL_LOG_vol_get = MilesBridgeJNI.CTRL_LOG_vol_get(this.swigCPtr);
        if (CTRL_LOG_vol_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_vol_get, false);
    }

    /* renamed from: E */
    public void mo6376E(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_pan_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAz() {
        long CTRL_LOG_pan_get = MilesBridgeJNI.CTRL_LOG_pan_get(this.swigCPtr);
        if (CTRL_LOG_pan_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_pan_get, false);
    }

    /* renamed from: F */
    public void mo6377F(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_exp_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAA() {
        long CTRL_LOG_exp_get = MilesBridgeJNI.CTRL_LOG_exp_get(this.swigCPtr);
        if (CTRL_LOG_exp_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_exp_get, false);
    }

    /* renamed from: G */
    public void mo6378G(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_sus_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAB() {
        long CTRL_LOG_sus_get = MilesBridgeJNI.CTRL_LOG_sus_get(this.swigCPtr);
        if (CTRL_LOG_sus_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_sus_get, false);
    }

    /* renamed from: H */
    public void mo6379H(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_reverb_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAC() {
        long CTRL_LOG_reverb_get = MilesBridgeJNI.CTRL_LOG_reverb_get(this.swigCPtr);
        if (CTRL_LOG_reverb_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_reverb_get, false);
    }

    /* renamed from: I */
    public void mo6380I(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_chorus_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAD() {
        long CTRL_LOG_chorus_get = MilesBridgeJNI.CTRL_LOG_chorus_get(this.swigCPtr);
        if (CTRL_LOG_chorus_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_chorus_get, false);
    }

    /* renamed from: J */
    public void mo6381J(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_bend_range_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAE() {
        long CTRL_LOG_bend_range_get = MilesBridgeJNI.CTRL_LOG_bend_range_get(this.swigCPtr);
        if (CTRL_LOG_bend_range_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_bend_range_get, false);
    }

    /* renamed from: K */
    public void mo6382K(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_RPN_L_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAF() {
        long CTRL_LOG_RPN_L_get = MilesBridgeJNI.CTRL_LOG_RPN_L_get(this.swigCPtr);
        if (CTRL_LOG_RPN_L_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_RPN_L_get, false);
    }

    /* renamed from: L */
    public void mo6383L(C2480fe feVar) {
        MilesBridgeJNI.CTRL_LOG_RPN_M_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bAG() {
        long CTRL_LOG_RPN_M_get = MilesBridgeJNI.CTRL_LOG_RPN_M_get(this.swigCPtr);
        if (CTRL_LOG_RPN_M_get == 0) {
            return null;
        }
        return new C2480fe(CTRL_LOG_RPN_M_get, false);
    }
}
