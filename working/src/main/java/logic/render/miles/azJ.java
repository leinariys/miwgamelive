package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.azJ */
/* compiled from: a */
public class azJ {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public azJ(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public azJ() {
        this(MilesBridgeJNI.new_FLTSTAGE(), true);
    }

    /* renamed from: b */
    public static long m27599b(azJ azj) {
        if (azj == null) {
            return 0;
        }
        return azj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_FLTSTAGE(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: c */
    public void mo17161c(C6153ahV ahv) {
        MilesBridgeJNI.FLTSTAGE_provider_set(this.swigCPtr, C6153ahV.m22222a(ahv));
    }

    public C6153ahV cGt() {
        long FLTSTAGE_provider_get = MilesBridgeJNI.FLTSTAGE_provider_get(this.swigCPtr);
        if (FLTSTAGE_provider_get == 0) {
            return null;
        }
        return new C6153ahV(FLTSTAGE_provider_get, false);
    }

    /* renamed from: wF */
    public void mo17166wF(int i) {
        MilesBridgeJNI.FLTSTAGE_sample_state_set(this.swigCPtr, i);
    }

    public int cGu() {
        return MilesBridgeJNI.FLTSTAGE_sample_state_get(this.swigCPtr);
    }
}
