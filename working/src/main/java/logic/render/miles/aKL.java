package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aKL */
/* compiled from: a */
public class aKL {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aKL(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aKL() {
        this(MilesBridgeJNI.new_MSS_BB(), true);
    }

    /* renamed from: a */
    public static long m15944a(aKL akl) {
        if (akl == null) {
            return 0;
        }
        return akl.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_MSS_BB(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: X */
    public void mo9701X(C2480fe feVar) {
        MilesBridgeJNI.MSS_BB_buffer_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe dgw() {
        long MSS_BB_buffer_get = MilesBridgeJNI.MSS_BB_buffer_get(this.swigCPtr);
        if (MSS_BB_buffer_get == 0) {
            return null;
        }
        return new C2480fe(MSS_BB_buffer_get, false);
    }

    public int getBytes() {
        return MilesBridgeJNI.MSS_BB_bytes_get(this.swigCPtr);
    }

    public void setBytes(int i) {
        MilesBridgeJNI.MSS_BB_bytes_set(this.swigCPtr, i);
    }

    /* renamed from: bU */
    public void mo9702bU(int i) {
        MilesBridgeJNI.MSS_BB_chans_set(this.swigCPtr, i);
    }

    /* renamed from: DI */
    public int mo9700DI() {
        return MilesBridgeJNI.MSS_BB_chans_get(this.swigCPtr);
    }

    /* renamed from: yu */
    public void mo9709yu(int i) {
        MilesBridgeJNI.MSS_BB_speaker_offset_set(this.swigCPtr, i);
    }

    public int dgx() {
        return MilesBridgeJNI.MSS_BB_speaker_offset_get(this.swigCPtr);
    }
}
