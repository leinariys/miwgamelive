package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aMJ */
/* compiled from: a */
public class aMJ {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aMJ(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aMJ() {
        this(MilesBridgeJNI.new_MSSVECTOR3D(), true);
    }

    /* renamed from: m */
    public static long m16344m(aMJ amj) {
        if (amj == null) {
            return 0;
        }
        return amj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_MSSVECTOR3D(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public float getX() {
        return MilesBridgeJNI.MSSVECTOR3D_x_get(this.swigCPtr);
    }

    public void setX(float f) {
        MilesBridgeJNI.MSSVECTOR3D_x_set(this.swigCPtr, f);
    }

    public float getY() {
        return MilesBridgeJNI.MSSVECTOR3D_y_get(this.swigCPtr);
    }

    public void setY(float f) {
        MilesBridgeJNI.MSSVECTOR3D_y_set(this.swigCPtr, f);
    }

    public float getZ() {
        return MilesBridgeJNI.MSSVECTOR3D_z_get(this.swigCPtr);
    }

    public void setZ(float f) {
        MilesBridgeJNI.MSSVECTOR3D_z_set(this.swigCPtr, f);
    }
}
