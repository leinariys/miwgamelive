package logic.render.miles;


import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.DT */
/* compiled from: a */
public class C0270DT {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0270DT(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0270DT() {
        this(MilesBridgeJNI.new_ASISTAGE(), true);
    }

    /* renamed from: b */
    public static long m2424b(C0270DT dt) {
        if (dt == null) {
            return 0;
        }
        return dt.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_ASISTAGE(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo1564a(C0453GI gi) {
        MilesBridgeJNI.ASISTAGE_ASI_stream_open_set(this.swigCPtr, C0453GI.m3385b(gi));
    }

    public C0453GI aMQ() {
        long ASISTAGE_ASI_stream_open_get = MilesBridgeJNI.ASISTAGE_ASI_stream_open_get(this.swigCPtr);
        if (ASISTAGE_ASI_stream_open_get == 0) {
            return null;
        }
        return new C0453GI(ASISTAGE_ASI_stream_open_get, false);
    }

    /* renamed from: a */
    public void mo1566a(C1119QS qs) {
        MilesBridgeJNI.ASISTAGE_ASI_stream_process_set(this.swigCPtr, C1119QS.m8886b(qs));
    }

    public C1119QS aMR() {
        long ASISTAGE_ASI_stream_process_get = MilesBridgeJNI.ASISTAGE_ASI_stream_process_get(this.swigCPtr);
        if (ASISTAGE_ASI_stream_process_get == 0) {
            return null;
        }
        return new C1119QS(ASISTAGE_ASI_stream_process_get, false);
    }

    /* renamed from: b */
    public void mo1596b(C0020AJ aj) {
        MilesBridgeJNI.ASISTAGE_ASI_stream_seek_set(this.swigCPtr, C0020AJ.m225a(aj));
    }

    public C0020AJ aMS() {
        long ASISTAGE_ASI_stream_seek_get = MilesBridgeJNI.ASISTAGE_ASI_stream_seek_get(this.swigCPtr);
        if (ASISTAGE_ASI_stream_seek_get == 0) {
            return null;
        }
        return new C0020AJ(ASISTAGE_ASI_stream_seek_get, false);
    }

    /* renamed from: a */
    public void mo1565a(C1103QG qg) {
        MilesBridgeJNI.ASISTAGE_ASI_stream_close_set(this.swigCPtr, C1103QG.m8740b(qg));
    }

    public C1103QG aMT() {
        long ASISTAGE_ASI_stream_close_get = MilesBridgeJNI.ASISTAGE_ASI_stream_close_get(this.swigCPtr);
        if (ASISTAGE_ASI_stream_close_get == 0) {
            return null;
        }
        return new C1103QG(ASISTAGE_ASI_stream_close_get, false);
    }

    /* renamed from: a */
    public void mo1567a(C5483aLb alb) {
        MilesBridgeJNI.ASISTAGE_ASI_stream_property_set(this.swigCPtr, C5483aLb.m16216d(alb));
    }

    public C5483aLb aMU() {
        long ASISTAGE_ASI_stream_property_get = MilesBridgeJNI.ASISTAGE_ASI_stream_property_get(this.swigCPtr);
        if (ASISTAGE_ASI_stream_property_get == 0) {
            return null;
        }
        return new C5483aLb(ASISTAGE_ASI_stream_property_get, false);
    }

    /* renamed from: ep */
    public void mo1609ep(long j) {
        MilesBridgeJNI.ASISTAGE_INPUT_BIT_RATE_set(this.swigCPtr, j);
    }

    public long aMV() {
        return MilesBridgeJNI.ASISTAGE_INPUT_BIT_RATE_get(this.swigCPtr);
    }

    /* renamed from: eq */
    public void mo1610eq(long j) {
        MilesBridgeJNI.ASISTAGE_INPUT_SAMPLE_RATE_set(this.swigCPtr, j);
    }

    public long aMW() {
        return MilesBridgeJNI.ASISTAGE_INPUT_SAMPLE_RATE_get(this.swigCPtr);
    }

    /* renamed from: er */
    public void mo1611er(long j) {
        MilesBridgeJNI.ASISTAGE_INPUT_BITS_set(this.swigCPtr, j);
    }

    public long aMX() {
        return MilesBridgeJNI.ASISTAGE_INPUT_BITS_get(this.swigCPtr);
    }

    /* renamed from: es */
    public void mo1612es(long j) {
        MilesBridgeJNI.ASISTAGE_INPUT_CHANNELS_set(this.swigCPtr, j);
    }

    public long aMY() {
        return MilesBridgeJNI.ASISTAGE_INPUT_CHANNELS_get(this.swigCPtr);
    }

    /* renamed from: et */
    public void mo1613et(long j) {
        MilesBridgeJNI.ASISTAGE_OUTPUT_BIT_RATE_set(this.swigCPtr, j);
    }

    public long aMZ() {
        return MilesBridgeJNI.ASISTAGE_OUTPUT_BIT_RATE_get(this.swigCPtr);
    }

    /* renamed from: eu */
    public void mo1614eu(long j) {
        MilesBridgeJNI.ASISTAGE_OUTPUT_SAMPLE_RATE_set(this.swigCPtr, j);
    }

    public long aNa() {
        return MilesBridgeJNI.ASISTAGE_OUTPUT_SAMPLE_RATE_get(this.swigCPtr);
    }

    /* renamed from: ev */
    public void mo1615ev(long j) {
        MilesBridgeJNI.ASISTAGE_OUTPUT_BITS_set(this.swigCPtr, j);
    }

    public long aNb() {
        return MilesBridgeJNI.ASISTAGE_OUTPUT_BITS_get(this.swigCPtr);
    }

    /* renamed from: ew */
    public void mo1616ew(long j) {
        MilesBridgeJNI.ASISTAGE_OUTPUT_CHANNELS_set(this.swigCPtr, j);
    }

    public long aNc() {
        return MilesBridgeJNI.ASISTAGE_OUTPUT_CHANNELS_get(this.swigCPtr);
    }

    /* renamed from: ex */
    public void mo1617ex(long j) {
        MilesBridgeJNI.ASISTAGE_OUTPUT_CHANNEL_MASK_set(this.swigCPtr, j);
    }

    public long aNd() {
        return MilesBridgeJNI.ASISTAGE_OUTPUT_CHANNEL_MASK_get(this.swigCPtr);
    }

    /* renamed from: ey */
    public void mo1618ey(long j) {
        MilesBridgeJNI.ASISTAGE_OUTPUT_RESERVOIR_set(this.swigCPtr, j);
    }

    public long aNe() {
        return MilesBridgeJNI.ASISTAGE_OUTPUT_RESERVOIR_get(this.swigCPtr);
    }

    /* renamed from: ez */
    public void mo1619ez(long j) {
        MilesBridgeJNI.ASISTAGE_POSITION_set(this.swigCPtr, j);
    }

    public long aNf() {
        return MilesBridgeJNI.ASISTAGE_POSITION_get(this.swigCPtr);
    }

    /* renamed from: eA */
    public void mo1598eA(long j) {
        MilesBridgeJNI.ASISTAGE_PERCENT_DONE_set(this.swigCPtr, j);
    }

    public long aNg() {
        return MilesBridgeJNI.ASISTAGE_PERCENT_DONE_get(this.swigCPtr);
    }

    /* renamed from: eB */
    public void mo1599eB(long j) {
        MilesBridgeJNI.ASISTAGE_MIN_INPUT_BLOCK_SIZE_set(this.swigCPtr, j);
    }

    public long aNh() {
        return MilesBridgeJNI.ASISTAGE_MIN_INPUT_BLOCK_SIZE_get(this.swigCPtr);
    }

    /* renamed from: eC */
    public void mo1600eC(long j) {
        MilesBridgeJNI.ASISTAGE_RAW_RATE_set(this.swigCPtr, j);
    }

    public long aNi() {
        return MilesBridgeJNI.ASISTAGE_RAW_RATE_get(this.swigCPtr);
    }

    /* renamed from: eD */
    public void mo1601eD(long j) {
        MilesBridgeJNI.ASISTAGE_RAW_BITS_set(this.swigCPtr, j);
    }

    public long aNj() {
        return MilesBridgeJNI.ASISTAGE_RAW_BITS_get(this.swigCPtr);
    }

    /* renamed from: eE */
    public void mo1602eE(long j) {
        MilesBridgeJNI.ASISTAGE_RAW_CHANNELS_set(this.swigCPtr, j);
    }

    public long aNk() {
        return MilesBridgeJNI.ASISTAGE_RAW_CHANNELS_get(this.swigCPtr);
    }

    /* renamed from: eF */
    public void mo1603eF(long j) {
        MilesBridgeJNI.ASISTAGE_REQUESTED_RATE_set(this.swigCPtr, j);
    }

    public long aNl() {
        return MilesBridgeJNI.ASISTAGE_REQUESTED_RATE_get(this.swigCPtr);
    }

    /* renamed from: eG */
    public void mo1604eG(long j) {
        MilesBridgeJNI.ASISTAGE_REQUESTED_BITS_set(this.swigCPtr, j);
    }

    public long aNm() {
        return MilesBridgeJNI.ASISTAGE_REQUESTED_BITS_get(this.swigCPtr);
    }

    /* renamed from: eH */
    public void mo1605eH(long j) {
        MilesBridgeJNI.ASISTAGE_REQUESTED_CHANS_set(this.swigCPtr, j);
    }

    public long aNn() {
        return MilesBridgeJNI.ASISTAGE_REQUESTED_CHANS_get(this.swigCPtr);
    }

    /* renamed from: eI */
    public void mo1606eI(long j) {
        MilesBridgeJNI.ASISTAGE_STREAM_SEEK_POS_set(this.swigCPtr, j);
    }

    public long aNo() {
        return MilesBridgeJNI.ASISTAGE_STREAM_SEEK_POS_get(this.swigCPtr);
    }

    /* renamed from: eJ */
    public void mo1607eJ(long j) {
        MilesBridgeJNI.ASISTAGE_DATA_START_OFFSET_set(this.swigCPtr, j);
    }

    public long aNp() {
        return MilesBridgeJNI.ASISTAGE_DATA_START_OFFSET_get(this.swigCPtr);
    }

    /* renamed from: eK */
    public void mo1608eK(long j) {
        MilesBridgeJNI.ASISTAGE_DATA_LEN_set(this.swigCPtr, j);
    }

    public long aNq() {
        return MilesBridgeJNI.ASISTAGE_DATA_LEN_get(this.swigCPtr);
    }

    /* renamed from: gU */
    public void mo1621gU(int i) {
        MilesBridgeJNI.ASISTAGE_stream_set(this.swigCPtr, i);
    }

    public int aNr() {
        return MilesBridgeJNI.ASISTAGE_stream_get(this.swigCPtr);
    }
}
