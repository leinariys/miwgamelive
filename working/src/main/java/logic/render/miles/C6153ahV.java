package logic.render.miles;


import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.ahV  reason: case insensitive filesystem */
/* compiled from: a */
public class C6153ahV {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6153ahV(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6153ahV() {
        this(MilesBridgeJNI.new_FLTPROVIDER(), true);
    }

    /* renamed from: a */
    public static long m22222a(C6153ahV ahv) {
        if (ahv == null) {
            return 0;
        }
        return ahv.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_FLTPROVIDER(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo13524b(C0384FJ fj) {
        MilesBridgeJNI.FLTPROVIDER_PROVIDER_property_set(this.swigCPtr, C0384FJ.m3162a(fj));
    }

    public C0384FJ caW() {
        long FLTPROVIDER_PROVIDER_property_get = MilesBridgeJNI.FLTPROVIDER_PROVIDER_property_get(this.swigCPtr);
        if (FLTPROVIDER_PROVIDER_property_get == 0) {
            return null;
        }
        return new C0384FJ(FLTPROVIDER_PROVIDER_property_get, false);
    }

    /* renamed from: b */
    public void mo13528b(C6008aeg aeg) {
        MilesBridgeJNI.FLTPROVIDER_startup_set(this.swigCPtr, C6008aeg.m21333a(aeg));
    }

    public C6008aeg caX() {
        long FLTPROVIDER_startup_get = MilesBridgeJNI.FLTPROVIDER_startup_get(this.swigCPtr);
        if (FLTPROVIDER_startup_get == 0) {
            return null;
        }
        return new C6008aeg(FLTPROVIDER_startup_get, false);
    }

    /* renamed from: b */
    public void mo13532b(C4093zM zMVar) {
        MilesBridgeJNI.FLTPROVIDER_error_set(this.swigCPtr, C4093zM.m41534a(zMVar));
    }

    public C4093zM caY() {
        long FLTPROVIDER_error_get = MilesBridgeJNI.FLTPROVIDER_error_get(this.swigCPtr);
        if (FLTPROVIDER_error_get == 0) {
            return null;
        }
        return new C4093zM(FLTPROVIDER_error_get, false);
    }

    /* renamed from: c */
    public void mo13535c(C6008aeg aeg) {
        MilesBridgeJNI.FLTPROVIDER_shutdown_set(this.swigCPtr, C6008aeg.m21333a(aeg));
    }

    public C6008aeg caZ() {
        long FLTPROVIDER_shutdown_get = MilesBridgeJNI.FLTPROVIDER_shutdown_get(this.swigCPtr);
        if (FLTPROVIDER_shutdown_get == 0) {
            return null;
        }
        return new C6008aeg(FLTPROVIDER_shutdown_get, false);
    }

    /* renamed from: b */
    public void mo13529b(C6136ahE ahe) {
        MilesBridgeJNI.FLTPROVIDER_open_driver_set(this.swigCPtr, C6136ahE.m22190a(ahe));
    }

    public C6136ahE cba() {
        long FLTPROVIDER_open_driver_get = MilesBridgeJNI.FLTPROVIDER_open_driver_get(this.swigCPtr);
        if (FLTPROVIDER_open_driver_get == 0) {
            return null;
        }
        return new C6136ahE(FLTPROVIDER_open_driver_get, false);
    }

    /* renamed from: c */
    public void mo13533c(C1103QG qg) {
        MilesBridgeJNI.FLTPROVIDER_close_driver_set(this.swigCPtr, C1103QG.m8740b(qg));
    }

    public C1103QG cbb() {
        long FLTPROVIDER_close_driver_get = MilesBridgeJNI.FLTPROVIDER_close_driver_get(this.swigCPtr);
        if (FLTPROVIDER_close_driver_get == 0) {
            return null;
        }
        return new C1103QG(FLTPROVIDER_close_driver_get, false);
    }

    /* renamed from: a */
    public void mo13521a(C6291akD akd) {
        MilesBridgeJNI.FLTPROVIDER_premix_process_set(this.swigCPtr, C6291akD.m23118b(akd));
    }

    public C6291akD cbc() {
        long FLTPROVIDER_premix_process_get = MilesBridgeJNI.FLTPROVIDER_premix_process_get(this.swigCPtr);
        if (FLTPROVIDER_premix_process_get == 0) {
            return null;
        }
        return new C6291akD(FLTPROVIDER_premix_process_get, false);
    }

    /* renamed from: a */
    public void mo13522a(C6857aux aux) {
        MilesBridgeJNI.FLTPROVIDER_postmix_process_set(this.swigCPtr, C6857aux.m26473b(aux));
    }

    public C6857aux cbd() {
        long FLTPROVIDER_postmix_process_get = MilesBridgeJNI.FLTPROVIDER_postmix_process_get(this.swigCPtr);
        if (FLTPROVIDER_postmix_process_get == 0) {
            return null;
        }
        return new C6857aux(FLTPROVIDER_postmix_process_get, false);
    }

    /* renamed from: b */
    public void mo13531b(C2408ez ezVar) {
        MilesBridgeJNI.FLTPROVIDER_open_sample_set(this.swigCPtr, C2408ez.m30179a(ezVar));
    }

    public C2408ez cbe() {
        long FLTPROVIDER_open_sample_get = MilesBridgeJNI.FLTPROVIDER_open_sample_get(this.swigCPtr);
        if (FLTPROVIDER_open_sample_get == 0) {
            return null;
        }
        return new C2408ez(FLTPROVIDER_open_sample_get, false);
    }

    /* renamed from: d */
    public void mo13559d(C1103QG qg) {
        MilesBridgeJNI.FLTPROVIDER_close_sample_set(this.swigCPtr, C1103QG.m8740b(qg));
    }

    public C1103QG cbf() {
        long FLTPROVIDER_close_sample_get = MilesBridgeJNI.FLTPROVIDER_close_sample_get(this.swigCPtr);
        if (FLTPROVIDER_close_sample_get == 0) {
            return null;
        }
        return new C1103QG(FLTPROVIDER_close_sample_get, false);
    }

    /* renamed from: b */
    public void mo13526b(C1702ZC zc) {
        MilesBridgeJNI.FLTPROVIDER_sample_process_set(this.swigCPtr, C1702ZC.m11981a(zc));
    }

    public C1702ZC cbg() {
        long FLTPROVIDER_sample_process_get = MilesBridgeJNI.FLTPROVIDER_sample_process_get(this.swigCPtr);
        if (FLTPROVIDER_sample_process_get == 0) {
            return null;
        }
        return new C1702ZC(FLTPROVIDER_sample_process_get, false);
    }

    /* renamed from: b */
    public void mo13527b(C5483aLb alb) {
        MilesBridgeJNI.FLTPROVIDER_sample_property_set(this.swigCPtr, C5483aLb.m16216d(alb));
    }

    public C5483aLb cbh() {
        long FLTPROVIDER_sample_property_get = MilesBridgeJNI.FLTPROVIDER_sample_property_get(this.swigCPtr);
        if (FLTPROVIDER_sample_property_get == 0) {
            return null;
        }
        return new C5483aLb(FLTPROVIDER_sample_property_get, false);
    }

    /* renamed from: c */
    public void mo13536c(C2408ez ezVar) {
        MilesBridgeJNI.FLTPROVIDER_assign_sample_voice_set(this.swigCPtr, C2408ez.m30179a(ezVar));
    }

    public C2408ez cbi() {
        long FLTPROVIDER_assign_sample_voice_get = MilesBridgeJNI.FLTPROVIDER_assign_sample_voice_get(this.swigCPtr);
        if (FLTPROVIDER_assign_sample_voice_get == 0) {
            return null;
        }
        return new C2408ez(FLTPROVIDER_assign_sample_voice_get, false);
    }

    /* renamed from: a */
    public void mo13523a(C6871avL avl) {
        MilesBridgeJNI.FLTPROVIDER_release_sample_voice_set(this.swigCPtr, C6871avL.m26575b(avl));
    }

    public C6871avL cbj() {
        long FLTPROVIDER_release_sample_voice_get = MilesBridgeJNI.FLTPROVIDER_release_sample_voice_get(this.swigCPtr);
        if (FLTPROVIDER_release_sample_voice_get == 0) {
            return null;
        }
        return new C6871avL(FLTPROVIDER_release_sample_voice_get, false);
    }

    /* renamed from: d */
    public void mo13560d(C2408ez ezVar) {
        MilesBridgeJNI.FLTPROVIDER_start_sample_voice_set(this.swigCPtr, C2408ez.m30179a(ezVar));
    }

    public C2408ez cbk() {
        long FLTPROVIDER_start_sample_voice_get = MilesBridgeJNI.FLTPROVIDER_start_sample_voice_get(this.swigCPtr);
        if (FLTPROVIDER_start_sample_voice_get == 0) {
            return null;
        }
        return new C2408ez(FLTPROVIDER_start_sample_voice_get, false);
    }

    /* renamed from: c */
    public void mo13534c(C5483aLb alb) {
        MilesBridgeJNI.FLTPROVIDER_driver_property_set(this.swigCPtr, C5483aLb.m16216d(alb));
    }

    public C5483aLb cbl() {
        long FLTPROVIDER_driver_property_get = MilesBridgeJNI.FLTPROVIDER_driver_property_get(this.swigCPtr);
        if (FLTPROVIDER_driver_property_get == 0) {
            return null;
        }
        return new C5483aLb(FLTPROVIDER_driver_property_get, false);
    }

    /* renamed from: e */
    public void mo13562e(C1103QG qg) {
        MilesBridgeJNI.FLTPROVIDER_force_update_set(this.swigCPtr, C1103QG.m8740b(qg));
    }

    public C1103QG cbm() {
        long FLTPROVIDER_force_update_get = MilesBridgeJNI.FLTPROVIDER_force_update_get(this.swigCPtr);
        if (FLTPROVIDER_force_update_get == 0) {
            return null;
        }
        return new C1103QG(FLTPROVIDER_force_update_get, false);
    }

    /* renamed from: b */
    public void mo13525b(C1698Yz yz) {
        MilesBridgeJNI.FLTPROVIDER_output_sample_property_set(this.swigCPtr, C1698Yz.m11968a(yz));
    }

    public C1698Yz cbn() {
        long FLTPROVIDER_output_sample_property_get = MilesBridgeJNI.FLTPROVIDER_output_sample_property_get(this.swigCPtr);
        if (FLTPROVIDER_output_sample_property_get == 0) {
            return null;
        }
        return new C1698Yz(FLTPROVIDER_output_sample_property_get, false);
    }

    /* renamed from: a */
    public void mo13520a(aNZ anz) {
        MilesBridgeJNI.FLTPROVIDER_dig_set(this.swigCPtr, aNZ.m16621q(anz));
    }

    public aNZ getDig() {
        long FLTPROVIDER_dig_get = MilesBridgeJNI.FLTPROVIDER_dig_get(this.swigCPtr);
        if (FLTPROVIDER_dig_get == 0) {
            return null;
        }
        return new aNZ(FLTPROVIDER_dig_get, false);
    }

    /* renamed from: hv */
    public void mo13565hv(long j) {
        MilesBridgeJNI.FLTPROVIDER_provider_set(this.swigCPtr, j);
    }

    public long cbo() {
        return MilesBridgeJNI.FLTPROVIDER_provider_get(this.swigCPtr);
    }

    /* renamed from: qY */
    public void mo13566qY(int i) {
        MilesBridgeJNI.FLTPROVIDER_driver_state_set(this.swigCPtr, i);
    }

    public int cbp() {
        return MilesBridgeJNI.FLTPROVIDER_driver_state_get(this.swigCPtr);
    }

    /* renamed from: qZ */
    public void mo13567qZ(int i) {
        MilesBridgeJNI.FLTPROVIDER_provider_flags_set(this.swigCPtr, i);
    }

    public int cbq() {
        return MilesBridgeJNI.FLTPROVIDER_provider_flags_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo13530b(C6153ahV ahv) {
        MilesBridgeJNI.FLTPROVIDER_next_set(this.swigCPtr, m22222a(ahv));
    }

    public C6153ahV cbr() {
        long FLTPROVIDER_next_get = MilesBridgeJNI.FLTPROVIDER_next_get(this.swigCPtr);
        if (FLTPROVIDER_next_get == 0) {
            return null;
        }
        return new C6153ahV(FLTPROVIDER_next_get, false);
    }
}
