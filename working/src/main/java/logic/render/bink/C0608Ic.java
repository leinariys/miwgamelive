package logic.render.bink;

/* renamed from: a.Ic */
/* compiled from: a */
public class C0608Ic {
    private long swigCPtr;

    public C0608Ic(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C0608Ic() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m5422b(C0608Ic ic) {
        if (ic == null) {
            return 0;
        }
        return ic.swigCPtr;
    }
}
