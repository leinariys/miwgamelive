package logic.render.bink;

/* renamed from: a.aBs  reason: case insensitive filesystem */
/* compiled from: a */
public class C5240aBs {
    private long swigCPtr;

    public C5240aBs(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5240aBs() {
        this.swigCPtr = 0;
    }

    /* renamed from: c */
    public static long m13099c(C5240aBs abs) {
        if (abs == null) {
            return 0;
        }
        return abs.swigCPtr;
    }
}
