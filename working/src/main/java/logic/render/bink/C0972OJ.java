package logic.render.bink;


import utaikodom.render.bink.BinkBridgeJNI;

import java.nio.Buffer;

/* renamed from: a.OJ */
/* compiled from: a */
public class C0972OJ {
    public static int wBinkSoundUseDirectSound(int i) {
        return BinkBridgeJNI.wBinkSoundUseDirectSound(i);
    }

    public static C1263Sf bmC() {
        long BinkLogoAddress = BinkBridgeJNI.BinkLogoAddress();
        if (BinkLogoAddress == 0) {
            return null;
        }
        return new C1263Sf(BinkLogoAddress, false);
    }

    public static void BinkSetError(String str) {
        BinkBridgeJNI.BinkSetError(str);
    }

    public static String BinkGetError() {
        return BinkBridgeJNI.BinkGetError();
    }

    /* renamed from: d */
    public static C2427fN m7939d(String str, long j) {
        long BinkOpen = BinkBridgeJNI.BinkOpen(str, j);
        if (BinkOpen == 0) {
            return null;
        }
        return new C2427fN(BinkOpen, false);
    }

    /* renamed from: a */
    public static void m7922a(C2427fN fNVar, C2816kW kWVar) {
        BinkBridgeJNI.BinkGetFrameBuffersInfo(C2427fN.m30833a(fNVar), fNVar, C2816kW.m34436c(kWVar), kWVar);
    }

    /* renamed from: b */
    public static void m7930b(C2427fN fNVar, C2816kW kWVar) {
        BinkBridgeJNI.BinkRegisterFrameBuffers(C2427fN.m30833a(fNVar), fNVar, C2816kW.m34436c(kWVar), kWVar);
    }

    /* renamed from: b */
    public static int m7926b(C2427fN fNVar) {
        return BinkBridgeJNI.BinkDoFrame(C2427fN.m30833a(fNVar), fNVar);
    }

    /* renamed from: c */
    public static void m7934c(C2427fN fNVar) {
        BinkBridgeJNI.BinkNextFrame(C2427fN.m30833a(fNVar), fNVar);
    }

    /* renamed from: d */
    public static int m7936d(C2427fN fNVar) {
        return BinkBridgeJNI.BinkWait(C2427fN.m30833a(fNVar), fNVar);
    }

    /* renamed from: e */
    public static void m7943e(C2427fN fNVar) {
        BinkBridgeJNI.BinkClose(C2427fN.m30833a(fNVar), fNVar);
    }

    /* renamed from: a */
    public static int m7904a(C2427fN fNVar, int i) {
        return BinkBridgeJNI.BinkPause(C2427fN.m30833a(fNVar), fNVar, i);
    }

    /* renamed from: a */
    public static int m7907a(C2427fN fNVar, Buffer buffer, int i, long j, long j2, long j3, long j4) {
        return BinkBridgeJNI.BinkCopyToBuffer(C2427fN.m30833a(fNVar), fNVar, buffer, i, j, j2, j3, j4);
    }

    /* renamed from: a */
    public static int m7908a(C2427fN fNVar, Buffer buffer, int i, long j, long j2, long j3, long j4, long j5, long j6, long j7, long j8) {
        return BinkBridgeJNI.BinkCopyToBufferRect(C2427fN.m30833a(fNVar), fNVar, buffer, i, j, j2, j3, j4, j5, j6, j7, j8);
    }

    /* renamed from: a */
    public static int m7905a(C2427fN fNVar, long j) {
        return BinkBridgeJNI.BinkGetRects(C2427fN.m30833a(fNVar), fNVar, j);
    }

    /* renamed from: a */
    public static void m7917a(C2427fN fNVar, long j, int i) {
        BinkBridgeJNI.BinkGoto(C2427fN.m30833a(fNVar), fNVar, j, i);
    }

    /* renamed from: b */
    public static long m7929b(C2427fN fNVar, long j, int i) {
        return BinkBridgeJNI.BinkGetKeyFrame(C2427fN.m30833a(fNVar), fNVar, j, i);
    }

    /* renamed from: b */
    public static int m7927b(C2427fN fNVar, int i) {
        return BinkBridgeJNI.BinkSetVideoOnOff(C2427fN.m30833a(fNVar), fNVar, i);
    }

    /* renamed from: c */
    public static int m7932c(C2427fN fNVar, int i) {
        return BinkBridgeJNI.BinkSetSoundOnOff(C2427fN.m30833a(fNVar), fNVar, i);
    }

    /* renamed from: c */
    public static void m7935c(C2427fN fNVar, long j, int i) {
        BinkBridgeJNI.BinkSetVolume(C2427fN.m30833a(fNVar), fNVar, j, i);
    }

    /* renamed from: d */
    public static void m7941d(C2427fN fNVar, long j, int i) {
        BinkBridgeJNI.BinkSetPan(C2427fN.m30833a(fNVar), fNVar, j, i);
    }

    /* renamed from: a */
    public static void m7918a(C2427fN fNVar, long j, C2091bv bvVar, long j2) {
        BinkBridgeJNI.BinkSetMixBins(C2427fN.m30833a(fNVar), fNVar, j, C2091bv.m28102a(bvVar), j2);
    }

    /* renamed from: a */
    public static void m7919a(C2427fN fNVar, long j, C2091bv bvVar, C6573apZ apz, long j2) {
        BinkBridgeJNI.BinkSetMixBinVolumes(C2427fN.m30833a(fNVar), fNVar, j, C2091bv.m28102a(bvVar), C6573apZ.m24928d(apz), j2);
    }

    /* renamed from: f */
    public static void m7945f(C2427fN fNVar) {
        BinkBridgeJNI.BinkService(C2427fN.m30833a(fNVar), fNVar);
    }

    /* renamed from: g */
    public static int m7949g(C2427fN fNVar) {
        return BinkBridgeJNI.BinkShouldSkip(C2427fN.m30833a(fNVar), fNVar);
    }

    /* renamed from: q */
    public static void m7950q(C1263Sf sf) {
        BinkBridgeJNI.BinkGetPalette(C1263Sf.m9484u(sf));
    }

    /* renamed from: b */
    public static int m7928b(C2427fN fNVar, long j) {
        return BinkBridgeJNI.BinkControlBackgroundIO(C2427fN.m30833a(fNVar), fNVar, j);
    }

    /* renamed from: a */
    public static int m7896a(int i, C1263Sf sf) {
        return BinkBridgeJNI.BinkStartAsyncThread(i, C1263Sf.m9484u(sf));
    }

    /* renamed from: a */
    public static int m7906a(C2427fN fNVar, long j, long j2) {
        return BinkBridgeJNI.BinkDoFrameAsync(C2427fN.m30833a(fNVar), fNVar, j, j2);
    }

    /* renamed from: d */
    public static int m7937d(C2427fN fNVar, int i) {
        return BinkBridgeJNI.BinkDoFrameAsyncWait(C2427fN.m30833a(fNVar), fNVar, i);
    }

    public static int BinkRequestStopAsyncThread(int i) {
        return BinkBridgeJNI.BinkRequestStopAsyncThread(i);
    }

    public static int BinkWaitStopAsyncThread(int i) {
        return BinkBridgeJNI.BinkWaitStopAsyncThread(i);
    }

    /* renamed from: c */
    public static C6500aoE m7933c(C2427fN fNVar, long j) {
        long BinkOpenTrack = BinkBridgeJNI.BinkOpenTrack(C2427fN.m30833a(fNVar), fNVar, j);
        if (BinkOpenTrack == 0) {
            return null;
        }
        return new C6500aoE(BinkOpenTrack, false);
    }

    /* renamed from: a */
    public static void m7913a(C6500aoE aoe) {
        BinkBridgeJNI.BinkCloseTrack(C6500aoE.m24354b(aoe), aoe);
    }

    /* renamed from: a */
    public static long m7909a(C6500aoE aoe, Buffer buffer) {
        return BinkBridgeJNI.BinkGetTrackData(C6500aoE.m24354b(aoe), aoe, buffer);
    }

    /* renamed from: d */
    public static long m7938d(C2427fN fNVar, long j) {
        return BinkBridgeJNI.BinkGetTrackType(C2427fN.m30833a(fNVar), fNVar, j);
    }

    /* renamed from: e */
    public static long m7942e(C2427fN fNVar, long j) {
        return BinkBridgeJNI.BinkGetTrackMaxSize(C2427fN.m30833a(fNVar), fNVar, j);
    }

    /* renamed from: f */
    public static long m7944f(C2427fN fNVar, long j) {
        return BinkBridgeJNI.BinkGetTrackID(C2427fN.m30833a(fNVar), fNVar, j);
    }

    /* renamed from: a */
    public static void m7920a(C2427fN fNVar, aCW acw) {
        BinkBridgeJNI.BinkGetSummary(C2427fN.m30833a(fNVar), fNVar, aCW.m13300a(acw), acw);
    }

    /* renamed from: a */
    public static void m7921a(C2427fN fNVar, C6084agE age, long j) {
        BinkBridgeJNI.BinkGetRealtime(C2427fN.m30833a(fNVar), fNVar, C6084agE.m21940a(age), age, j);
    }

    /* renamed from: a */
    public static void m7911a(long j, C2091bv bvVar) {
        BinkBridgeJNI.BinkSetSoundTrack(j, C2091bv.m28102a(bvVar));
    }

    /* renamed from: a */
    public static void m7912a(aIO aio) {
        BinkBridgeJNI.BinkSetIO(aIO.m15403b(aio));
    }

    public static void BinkSetFrameRate(long j, long j2) {
        BinkBridgeJNI.BinkSetFrameRate(j, j2);
    }

    public static void BinkSetSimulate(long j) {
        BinkBridgeJNI.BinkSetSimulate(j);
    }

    public static void BinkSetIOSize(long j) {
        BinkBridgeJNI.BinkSetIOSize(j);
    }

    /* renamed from: a */
    public static int m7899a(C1687Yr yr, long j) {
        return BinkBridgeJNI.BinkSetSoundSystem(C1687Yr.m11952a(yr), j);
    }

    public static int BinkControlPlatformFeatures(int i, int i2) {
        return BinkBridgeJNI.BinkControlPlatformFeatures(i, i2);
    }

    /* renamed from: fl */
    public static C0464GQ m7946fl(long j) {
        long BinkOpenDirectSound = BinkBridgeJNI.BinkOpenDirectSound(j);
        if (BinkOpenDirectSound == 0) {
            return null;
        }
        return new C0464GQ(BinkOpenDirectSound, false);
    }

    /* renamed from: fm */
    public static C0464GQ m7947fm(long j) {
        long BinkOpenWaveOut = BinkBridgeJNI.BinkOpenWaveOut(j);
        if (BinkOpenWaveOut == 0) {
            return null;
        }
        return new C0464GQ(BinkOpenWaveOut, false);
    }

    /* renamed from: fn */
    public static C0464GQ m7948fn(long j) {
        long BinkOpenMiles = BinkBridgeJNI.BinkOpenMiles(j);
        if (BinkOpenMiles == 0) {
            return null;
        }
        return new C0464GQ(BinkOpenMiles, false);
    }

    /* renamed from: r */
    public static int m7951r(C1263Sf sf) {
        return BinkBridgeJNI.BinkDX8SurfaceType(C1263Sf.m9484u(sf));
    }

    /* renamed from: s */
    public static int m7952s(C1263Sf sf) {
        return BinkBridgeJNI.BinkDX9SurfaceType(C1263Sf.m9484u(sf));
    }

    /* renamed from: a */
    public static C6673arV m7910a(C1263Sf sf, long j, long j2, long j3) {
        long BinkBufferOpen = BinkBridgeJNI.BinkBufferOpen(C1263Sf.m9484u(sf), j, j2, j3);
        if (BinkBufferOpen == 0) {
            return null;
        }
        return new C6673arV(BinkBufferOpen, false);
    }

    /* renamed from: a */
    public static int m7903a(C6673arV arv, C1263Sf sf) {
        return BinkBridgeJNI.BinkBufferSetHWND(C6673arV.m25460e(arv), arv, C1263Sf.m9484u(sf));
    }

    /* renamed from: t */
    public static int m7953t(C1263Sf sf) {
        return BinkBridgeJNI.BinkDDSurfaceType(C1263Sf.m9484u(sf));
    }

    /* renamed from: a */
    public static int m7898a(C1263Sf sf, C1263Sf sf2) {
        return BinkBridgeJNI.BinkIsSoftwareCursor(C1263Sf.m9484u(sf), C1263Sf.m9484u(sf2));
    }

    /* renamed from: a */
    public static int m7897a(C1263Sf sf, int i, int i2, int i3, int i4) {
        return BinkBridgeJNI.BinkCheckCursor(C1263Sf.m9484u(sf), i, i2, i3, i4);
    }

    /* renamed from: b */
    public static int m7924b(C1263Sf sf, C1263Sf sf2) {
        return BinkBridgeJNI.BinkBufferSetDirectDraw(C1263Sf.m9484u(sf), C1263Sf.m9484u(sf2));
    }

    /* renamed from: a */
    public static void m7914a(C6673arV arv) {
        BinkBridgeJNI.BinkBufferClose(C6673arV.m25460e(arv), arv);
    }

    /* renamed from: b */
    public static int m7925b(C6673arV arv) {
        return BinkBridgeJNI.BinkBufferLock(C6673arV.m25460e(arv), arv);
    }

    /* renamed from: c */
    public static int m7931c(C6673arV arv) {
        return BinkBridgeJNI.BinkBufferUnlock(C6673arV.m25460e(arv), arv);
    }

    public static void BinkBufferSetResolution(int i, int i2, int i3) {
        BinkBridgeJNI.BinkBufferSetResolution(i, i2, i3);
    }

    /* renamed from: a */
    public static void m7915a(C6673arV arv, C6573apZ apz, C6573apZ apz2) {
        BinkBridgeJNI.BinkBufferCheckWinPos(C6673arV.m25460e(arv), arv, C6573apZ.m24928d(apz), C6573apZ.m24928d(apz2));
    }

    /* renamed from: a */
    public static int m7900a(C6673arV arv, int i, int i2) {
        return BinkBridgeJNI.BinkBufferSetOffset(C6673arV.m25460e(arv), arv, i, i2);
    }

    /* renamed from: a */
    public static void m7916a(C6673arV arv, C6679arb arb, long j) {
        BinkBridgeJNI.BinkBufferBlit(C6673arV.m25460e(arv), arv, C6679arb.m25511b(arb), arb, j);
    }

    /* renamed from: a */
    public static int m7902a(C6673arV arv, long j, long j2) {
        return BinkBridgeJNI.BinkBufferSetScale(C6673arV.m25460e(arv), arv, j, j2);
    }

    /* renamed from: d */
    public static String m7940d(C6673arV arv) {
        return BinkBridgeJNI.BinkBufferGetDescription(C6673arV.m25460e(arv), arv);
    }

    public static String BinkBufferGetError() {
        return BinkBridgeJNI.BinkBufferGetError();
    }

    public static void BinkRestoreCursor(int i) {
        BinkBridgeJNI.BinkRestoreCursor(i);
    }

    /* renamed from: a */
    public static int m7901a(C6673arV arv, long j) {
        return BinkBridgeJNI.BinkBufferClear(C6673arV.m25460e(arv), arv, j);
    }

    /* renamed from: a */
    public static void m7923a(C2518gG gGVar, C3956xM xMVar) {
        BinkBridgeJNI.BinkSetMemory(C2518gG.m31893a(gGVar), C3956xM.m40907a(xMVar));
    }
}
