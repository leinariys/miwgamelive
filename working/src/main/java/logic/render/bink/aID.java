package logic.render.bink;

/* renamed from: a.aID */
/* compiled from: a */
public class aID {
    private long swigCPtr;

    public aID(long j, boolean z) {
        this.swigCPtr = j;
    }

    public aID() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m15359b(aID aid) {
        if (aid == null) {
            return 0;
        }
        return aid.swigCPtr;
    }
}
