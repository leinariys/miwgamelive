package logic.render.bink;

import utaikodom.render.bink.BinkBridgeJNI;

/* renamed from: a.arV  reason: case insensitive filesystem */
/* compiled from: a */
public class C6673arV {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6673arV(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6673arV() {
        this(BinkBridgeJNI.new_BINKBUFFER(), true);
    }

    /* renamed from: e */
    public static long m25460e(C6673arV arv) {
        if (arv == null) {
            return 0;
        }
        return arv.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            BinkBridgeJNI.delete_BINKBUFFER(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: S */
    public void mo15749S(long j) {
        BinkBridgeJNI.BINKBUFFER_Width_set(this.swigCPtr, this, j);
    }

    /* renamed from: sN */
    public long mo15806sN() {
        return BinkBridgeJNI.BINKBUFFER_Width_get(this.swigCPtr, this);
    }

    /* renamed from: T */
    public void mo15750T(long j) {
        BinkBridgeJNI.BINKBUFFER_Height_set(this.swigCPtr, this, j);
    }

    /* renamed from: sO */
    public long mo15807sO() {
        return BinkBridgeJNI.BINKBUFFER_Height_get(this.swigCPtr, this);
    }

    /* renamed from: iu */
    public void mo15800iu(long j) {
        BinkBridgeJNI.BINKBUFFER_WindowWidth_set(this.swigCPtr, this, j);
    }

    public long csH() {
        return BinkBridgeJNI.BINKBUFFER_WindowWidth_get(this.swigCPtr, this);
    }

    /* renamed from: iv */
    public void mo15801iv(long j) {
        BinkBridgeJNI.BINKBUFFER_WindowHeight_set(this.swigCPtr, this, j);
    }

    public long csI() {
        return BinkBridgeJNI.BINKBUFFER_WindowHeight_get(this.swigCPtr, this);
    }

    /* renamed from: iw */
    public void mo15802iw(long j) {
        BinkBridgeJNI.BINKBUFFER_SurfaceType_set(this.swigCPtr, this, j);
    }

    public long csJ() {
        return BinkBridgeJNI.BINKBUFFER_SurfaceType_get(this.swigCPtr, this);
    }

    /* renamed from: g */
    public void mo15791g(C1263Sf sf) {
        BinkBridgeJNI.BINKBUFFER_Buffer_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    /* renamed from: JY */
    public C1263Sf mo15748JY() {
        long BINKBUFFER_Buffer_get = BinkBridgeJNI.BINKBUFFER_Buffer_get(this.swigCPtr, this);
        if (BINKBUFFER_Buffer_get == 0) {
            return null;
        }
        return new C1263Sf(BINKBUFFER_Buffer_get, false);
    }

    /* renamed from: tT */
    public void mo15808tT(int i) {
        BinkBridgeJNI.BINKBUFFER_BufferPitch_set(this.swigCPtr, this, i);
    }

    public int csK() {
        return BinkBridgeJNI.BINKBUFFER_BufferPitch_get(this.swigCPtr, this);
    }

    /* renamed from: tU */
    public void mo15809tU(int i) {
        BinkBridgeJNI.BINKBUFFER_ClientOffsetX_set(this.swigCPtr, this, i);
    }

    public int csL() {
        return BinkBridgeJNI.BINKBUFFER_ClientOffsetX_get(this.swigCPtr, this);
    }

    /* renamed from: tV */
    public void mo15810tV(int i) {
        BinkBridgeJNI.BINKBUFFER_ClientOffsetY_set(this.swigCPtr, this, i);
    }

    public int csM() {
        return BinkBridgeJNI.BINKBUFFER_ClientOffsetY_get(this.swigCPtr, this);
    }

    /* renamed from: ix */
    public void mo15803ix(long j) {
        BinkBridgeJNI.BINKBUFFER_ScreenWidth_set(this.swigCPtr, this, j);
    }

    public long csN() {
        return BinkBridgeJNI.BINKBUFFER_ScreenWidth_get(this.swigCPtr, this);
    }

    /* renamed from: iy */
    public void mo15804iy(long j) {
        BinkBridgeJNI.BINKBUFFER_ScreenHeight_set(this.swigCPtr, this, j);
    }

    public long csO() {
        return BinkBridgeJNI.BINKBUFFER_ScreenHeight_get(this.swigCPtr, this);
    }

    /* renamed from: iz */
    public void mo15805iz(long j) {
        BinkBridgeJNI.BINKBUFFER_ScreenDepth_set(this.swigCPtr, this, j);
    }

    public long csP() {
        return BinkBridgeJNI.BINKBUFFER_ScreenDepth_get(this.swigCPtr, this);
    }

    /* renamed from: iA */
    public void mo15793iA(long j) {
        BinkBridgeJNI.BINKBUFFER_ExtraWindowWidth_set(this.swigCPtr, this, j);
    }

    public long csQ() {
        return BinkBridgeJNI.BINKBUFFER_ExtraWindowWidth_get(this.swigCPtr, this);
    }

    /* renamed from: iB */
    public void mo15794iB(long j) {
        BinkBridgeJNI.BINKBUFFER_ExtraWindowHeight_set(this.swigCPtr, this, j);
    }

    public long csR() {
        return BinkBridgeJNI.BINKBUFFER_ExtraWindowHeight_get(this.swigCPtr, this);
    }

    /* renamed from: iC */
    public void mo15795iC(long j) {
        BinkBridgeJNI.BINKBUFFER_ScaleFlags_set(this.swigCPtr, this, j);
    }

    public long csS() {
        return BinkBridgeJNI.BINKBUFFER_ScaleFlags_get(this.swigCPtr, this);
    }

    /* renamed from: iD */
    public void mo15796iD(long j) {
        BinkBridgeJNI.BINKBUFFER_StretchWidth_set(this.swigCPtr, this, j);
    }

    public long csT() {
        return BinkBridgeJNI.BINKBUFFER_StretchWidth_get(this.swigCPtr, this);
    }

    /* renamed from: iE */
    public void mo15797iE(long j) {
        BinkBridgeJNI.BINKBUFFER_StretchHeight_set(this.swigCPtr, this, j);
    }

    public long csU() {
        return BinkBridgeJNI.BINKBUFFER_StretchHeight_get(this.swigCPtr, this);
    }

    /* renamed from: tW */
    public void mo15811tW(int i) {
        BinkBridgeJNI.BINKBUFFER_surface_set(this.swigCPtr, this, i);
    }

    public int csV() {
        return BinkBridgeJNI.BINKBUFFER_surface_get(this.swigCPtr, this);
    }

    /* renamed from: v */
    public void mo15825v(C1263Sf sf) {
        BinkBridgeJNI.BINKBUFFER_ddsurface_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    public C1263Sf csW() {
        long BINKBUFFER_ddsurface_get = BinkBridgeJNI.BINKBUFFER_ddsurface_get(this.swigCPtr, this);
        if (BINKBUFFER_ddsurface_get == 0) {
            return null;
        }
        return new C1263Sf(BINKBUFFER_ddsurface_get, false);
    }

    /* renamed from: w */
    public void mo15826w(C1263Sf sf) {
        BinkBridgeJNI.BINKBUFFER_ddclipper_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    public C1263Sf csX() {
        long BINKBUFFER_ddclipper_get = BinkBridgeJNI.BINKBUFFER_ddclipper_get(this.swigCPtr, this);
        if (BINKBUFFER_ddclipper_get == 0) {
            return null;
        }
        return new C1263Sf(BINKBUFFER_ddclipper_get, false);
    }

    /* renamed from: tX */
    public void mo15812tX(int i) {
        BinkBridgeJNI.BINKBUFFER_destx_set(this.swigCPtr, this, i);
    }

    public int csY() {
        return BinkBridgeJNI.BINKBUFFER_destx_get(this.swigCPtr, this);
    }

    /* renamed from: tY */
    public void mo15813tY(int i) {
        BinkBridgeJNI.BINKBUFFER_desty_set(this.swigCPtr, this, i);
    }

    public int csZ() {
        return BinkBridgeJNI.BINKBUFFER_desty_get(this.swigCPtr, this);
    }

    /* renamed from: tZ */
    public void mo15814tZ(int i) {
        BinkBridgeJNI.BINKBUFFER_wndx_set(this.swigCPtr, this, i);
    }

    public int cta() {
        return BinkBridgeJNI.BINKBUFFER_wndx_get(this.swigCPtr, this);
    }

    /* renamed from: ua */
    public void mo15815ua(int i) {
        BinkBridgeJNI.BINKBUFFER_wndy_set(this.swigCPtr, this, i);
    }

    public int ctb() {
        return BinkBridgeJNI.BINKBUFFER_wndy_get(this.swigCPtr, this);
    }

    /* renamed from: x */
    public void mo15827x(C1263Sf sf) {
        BinkBridgeJNI.BINKBUFFER_wnd_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    public C1263Sf ctc() {
        long BINKBUFFER_wnd_get = BinkBridgeJNI.BINKBUFFER_wnd_get(this.swigCPtr, this);
        if (BINKBUFFER_wnd_get == 0) {
            return null;
        }
        return new C1263Sf(BINKBUFFER_wnd_get, false);
    }

    /* renamed from: ub */
    public void mo15816ub(int i) {
        BinkBridgeJNI.BINKBUFFER_minimized_set(this.swigCPtr, this, i);
    }

    public int ctd() {
        return BinkBridgeJNI.BINKBUFFER_minimized_get(this.swigCPtr, this);
    }

    /* renamed from: uc */
    public void mo15817uc(int i) {
        BinkBridgeJNI.BINKBUFFER_ddoverlay_set(this.swigCPtr, this, i);
    }

    public int cte() {
        return BinkBridgeJNI.BINKBUFFER_ddoverlay_get(this.swigCPtr, this);
    }

    /* renamed from: ud */
    public void mo15818ud(int i) {
        BinkBridgeJNI.BINKBUFFER_ddoffscreen_set(this.swigCPtr, this, i);
    }

    public int ctf() {
        return BinkBridgeJNI.BINKBUFFER_ddoffscreen_get(this.swigCPtr, this);
    }

    /* renamed from: ue */
    public void mo15819ue(int i) {
        BinkBridgeJNI.BINKBUFFER_lastovershow_set(this.swigCPtr, this, i);
    }

    public int ctg() {
        return BinkBridgeJNI.BINKBUFFER_lastovershow_get(this.swigCPtr, this);
    }

    /* renamed from: uf */
    public void mo15820uf(int i) {
        BinkBridgeJNI.BINKBUFFER_issoftcur_set(this.swigCPtr, this, i);
    }

    public int cth() {
        return BinkBridgeJNI.BINKBUFFER_issoftcur_get(this.swigCPtr, this);
    }

    /* renamed from: iF */
    public void mo15798iF(long j) {
        BinkBridgeJNI.BINKBUFFER_cursorcount_set(this.swigCPtr, this, j);
    }

    public long cti() {
        return BinkBridgeJNI.BINKBUFFER_cursorcount_get(this.swigCPtr, this);
    }

    /* renamed from: y */
    public void mo15828y(C1263Sf sf) {
        BinkBridgeJNI.BINKBUFFER_buffertop_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    public C1263Sf ctj() {
        long BINKBUFFER_buffertop_get = BinkBridgeJNI.BINKBUFFER_buffertop_get(this.swigCPtr, this);
        if (BINKBUFFER_buffertop_get == 0) {
            return null;
        }
        return new C1263Sf(BINKBUFFER_buffertop_get, false);
    }

    /* renamed from: iG */
    public void mo15799iG(long j) {
        BinkBridgeJNI.BINKBUFFER_type_set(this.swigCPtr, this, j);
    }

    public long getType() {
        return BinkBridgeJNI.BINKBUFFER_type_get(this.swigCPtr, this);
    }

    /* renamed from: ug */
    public void mo15821ug(int i) {
        BinkBridgeJNI.BINKBUFFER_noclipping_set(this.swigCPtr, this, i);
    }

    public int ctk() {
        return BinkBridgeJNI.BINKBUFFER_noclipping_get(this.swigCPtr, this);
    }

    /* renamed from: uh */
    public void mo15822uh(int i) {
        BinkBridgeJNI.BINKBUFFER_loadeddd_set(this.swigCPtr, this, i);
    }

    public int ctl() {
        return BinkBridgeJNI.BINKBUFFER_loadeddd_get(this.swigCPtr, this);
    }

    /* renamed from: ui */
    public void mo15823ui(int i) {
        BinkBridgeJNI.BINKBUFFER_loadedwin_set(this.swigCPtr, this, i);
    }

    public int ctm() {
        return BinkBridgeJNI.BINKBUFFER_loadedwin_get(this.swigCPtr, this);
    }

    /* renamed from: z */
    public void mo15829z(C1263Sf sf) {
        BinkBridgeJNI.BINKBUFFER_dibh_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    public C1263Sf ctn() {
        long BINKBUFFER_dibh_get = BinkBridgeJNI.BINKBUFFER_dibh_get(this.swigCPtr, this);
        if (BINKBUFFER_dibh_get == 0) {
            return null;
        }
        return new C1263Sf(BINKBUFFER_dibh_get, false);
    }

    /* renamed from: A */
    public void mo15744A(C1263Sf sf) {
        BinkBridgeJNI.BINKBUFFER_dibbuffer_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    public C1263Sf cto() {
        long BINKBUFFER_dibbuffer_get = BinkBridgeJNI.BINKBUFFER_dibbuffer_get(this.swigCPtr, this);
        if (BINKBUFFER_dibbuffer_get == 0) {
            return null;
        }
        return new C1263Sf(BINKBUFFER_dibbuffer_get, false);
    }

    /* renamed from: uj */
    public void mo15824uj(int i) {
        BinkBridgeJNI.BINKBUFFER_dibpitch_set(this.swigCPtr, this, i);
    }

    public int ctp() {
        return BinkBridgeJNI.BINKBUFFER_dibpitch_get(this.swigCPtr, this);
    }

    /* renamed from: B */
    public void mo15745B(C1263Sf sf) {
        BinkBridgeJNI.BINKBUFFER_dibinfo_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    public C1263Sf ctq() {
        long BINKBUFFER_dibinfo_get = BinkBridgeJNI.BINKBUFFER_dibinfo_get(this.swigCPtr, this);
        if (BINKBUFFER_dibinfo_get == 0) {
            return null;
        }
        return new C1263Sf(BINKBUFFER_dibinfo_get, false);
    }

    /* renamed from: C */
    public void mo15746C(C1263Sf sf) {
        BinkBridgeJNI.BINKBUFFER_dibdc_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    public C1263Sf ctr() {
        long BINKBUFFER_dibdc_get = BinkBridgeJNI.BINKBUFFER_dibdc_get(this.swigCPtr, this);
        if (BINKBUFFER_dibdc_get == 0) {
            return null;
        }
        return new C1263Sf(BINKBUFFER_dibdc_get, false);
    }

    /* renamed from: D */
    public void mo15747D(C1263Sf sf) {
        BinkBridgeJNI.BINKBUFFER_diboldbitmap_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    public C1263Sf cts() {
        long BINKBUFFER_diboldbitmap_get = BinkBridgeJNI.BINKBUFFER_diboldbitmap_get(this.swigCPtr, this);
        if (BINKBUFFER_diboldbitmap_get == 0) {
            return null;
        }
        return new C1263Sf(BINKBUFFER_diboldbitmap_get, false);
    }
}
