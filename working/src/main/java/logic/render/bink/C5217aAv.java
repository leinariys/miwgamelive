package logic.render.bink;

/* renamed from: a.aAv  reason: case insensitive filesystem */
/* compiled from: a */
public class C5217aAv {
    private long swigCPtr;

    public C5217aAv(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5217aAv() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m12687a(C5217aAv aav) {
        if (aav == null) {
            return 0;
        }
        return aav.swigCPtr;
    }
}
