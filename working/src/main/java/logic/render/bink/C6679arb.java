package logic.render.bink;

import utaikodom.render.bink.BinkBridgeJNI;

/* renamed from: a.arb  reason: case insensitive filesystem */
/* compiled from: a */
public class C6679arb {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6679arb(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6679arb() {
        this(BinkBridgeJNI.new_BINKRECT(), true);
    }

    /* renamed from: b */
    public static long m25511b(C6679arb arb) {
        if (arb == null) {
            return 0;
        }
        return arb.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            BinkBridgeJNI.delete_BINKRECT(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: tM */
    public void mo15844tM(int i) {
        BinkBridgeJNI.BINKRECT_Left_set(this.swigCPtr, this, i);
    }

    public int crQ() {
        return BinkBridgeJNI.BINKRECT_Left_get(this.swigCPtr, this);
    }

    public void setTop(int i) {
        BinkBridgeJNI.BINKRECT_Top_set(this.swigCPtr, this, i);
    }

    public int crR() {
        return BinkBridgeJNI.BINKRECT_Top_get(this.swigCPtr, this);
    }

    public int getWidth() {
        return BinkBridgeJNI.BINKRECT_Width_get(this.swigCPtr, this);
    }

    public void setWidth(int i) {
        BinkBridgeJNI.BINKRECT_Width_set(this.swigCPtr, this, i);
    }

    public int getHeight() {
        return BinkBridgeJNI.BINKRECT_Height_get(this.swigCPtr, this);
    }

    public void setHeight(int i) {
        BinkBridgeJNI.BINKRECT_Height_set(this.swigCPtr, this, i);
    }
}
