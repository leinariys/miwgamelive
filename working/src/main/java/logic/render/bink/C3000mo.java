package logic.render.bink;

import utaikodom.render.bink.BinkBridgeJNI;

/* renamed from: a.mo */
/* compiled from: a */
public class C3000mo {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3000mo(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3000mo() {
        this(BinkBridgeJNI.new_BUNDLEPOINTERS(), true);
    }

    /* renamed from: b */
    public static long m35839b(C3000mo moVar) {
        if (moVar == null) {
            return 0;
        }
        return moVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            BinkBridgeJNI.delete_BUNDLEPOINTERS(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: h */
    public void mo20603h(C1263Sf sf) {
        BinkBridgeJNI.BUNDLEPOINTERS_typeptr_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    /* renamed from: Nj */
    public C1263Sf mo20592Nj() {
        long BUNDLEPOINTERS_typeptr_get = BinkBridgeJNI.BUNDLEPOINTERS_typeptr_get(this.swigCPtr, this);
        if (BUNDLEPOINTERS_typeptr_get == 0) {
            return null;
        }
        return new C1263Sf(BUNDLEPOINTERS_typeptr_get, false);
    }

    /* renamed from: i */
    public void mo20604i(C1263Sf sf) {
        BinkBridgeJNI.BUNDLEPOINTERS_type16ptr_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    /* renamed from: Nk */
    public C1263Sf mo20593Nk() {
        long BUNDLEPOINTERS_type16ptr_get = BinkBridgeJNI.BUNDLEPOINTERS_type16ptr_get(this.swigCPtr, this);
        if (BUNDLEPOINTERS_type16ptr_get == 0) {
            return null;
        }
        return new C1263Sf(BUNDLEPOINTERS_type16ptr_get, false);
    }

    /* renamed from: j */
    public void mo20605j(C1263Sf sf) {
        BinkBridgeJNI.BUNDLEPOINTERS_colorptr_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    /* renamed from: Nl */
    public C1263Sf mo20594Nl() {
        long BUNDLEPOINTERS_colorptr_get = BinkBridgeJNI.BUNDLEPOINTERS_colorptr_get(this.swigCPtr, this);
        if (BUNDLEPOINTERS_colorptr_get == 0) {
            return null;
        }
        return new C1263Sf(BUNDLEPOINTERS_colorptr_get, false);
    }

    /* renamed from: k */
    public void mo20606k(C1263Sf sf) {
        BinkBridgeJNI.BUNDLEPOINTERS_bits2ptr_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    /* renamed from: Nm */
    public C1263Sf mo20595Nm() {
        long BUNDLEPOINTERS_bits2ptr_get = BinkBridgeJNI.BUNDLEPOINTERS_bits2ptr_get(this.swigCPtr, this);
        if (BUNDLEPOINTERS_bits2ptr_get == 0) {
            return null;
        }
        return new C1263Sf(BUNDLEPOINTERS_bits2ptr_get, false);
    }

    /* renamed from: l */
    public void mo20607l(C1263Sf sf) {
        BinkBridgeJNI.BUNDLEPOINTERS_motionXptr_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    /* renamed from: Nn */
    public C1263Sf mo20596Nn() {
        long BUNDLEPOINTERS_motionXptr_get = BinkBridgeJNI.BUNDLEPOINTERS_motionXptr_get(this.swigCPtr, this);
        if (BUNDLEPOINTERS_motionXptr_get == 0) {
            return null;
        }
        return new C1263Sf(BUNDLEPOINTERS_motionXptr_get, false);
    }

    /* renamed from: m */
    public void mo20608m(C1263Sf sf) {
        BinkBridgeJNI.BUNDLEPOINTERS_motionYptr_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    /* renamed from: No */
    public C1263Sf mo20597No() {
        long BUNDLEPOINTERS_motionYptr_get = BinkBridgeJNI.BUNDLEPOINTERS_motionYptr_get(this.swigCPtr, this);
        if (BUNDLEPOINTERS_motionYptr_get == 0) {
            return null;
        }
        return new C1263Sf(BUNDLEPOINTERS_motionYptr_get, false);
    }

    /* renamed from: n */
    public void mo20609n(C1263Sf sf) {
        BinkBridgeJNI.BUNDLEPOINTERS_dctptr_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    /* renamed from: Np */
    public C1263Sf mo20598Np() {
        long BUNDLEPOINTERS_dctptr_get = BinkBridgeJNI.BUNDLEPOINTERS_dctptr_get(this.swigCPtr, this);
        if (BUNDLEPOINTERS_dctptr_get == 0) {
            return null;
        }
        return new C1263Sf(BUNDLEPOINTERS_dctptr_get, false);
    }

    /* renamed from: o */
    public void mo20610o(C1263Sf sf) {
        BinkBridgeJNI.BUNDLEPOINTERS_mdctptr_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    /* renamed from: Nq */
    public C1263Sf mo20599Nq() {
        long BUNDLEPOINTERS_mdctptr_get = BinkBridgeJNI.BUNDLEPOINTERS_mdctptr_get(this.swigCPtr, this);
        if (BUNDLEPOINTERS_mdctptr_get == 0) {
            return null;
        }
        return new C1263Sf(BUNDLEPOINTERS_mdctptr_get, false);
    }

    /* renamed from: p */
    public void mo20611p(C1263Sf sf) {
        BinkBridgeJNI.BUNDLEPOINTERS_patptr_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    /* renamed from: Nr */
    public C1263Sf mo20600Nr() {
        long BUNDLEPOINTERS_patptr_get = BinkBridgeJNI.BUNDLEPOINTERS_patptr_get(this.swigCPtr, this);
        if (BUNDLEPOINTERS_patptr_get == 0) {
            return null;
        }
        return new C1263Sf(BUNDLEPOINTERS_patptr_get, false);
    }
}
