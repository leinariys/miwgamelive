package logic.render.bink;


import utaikodom.render.bink.BinkBridgeJNI;

/* renamed from: a.iS */
/* compiled from: a */
public class C2677iS {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2677iS(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C2677iS() {
        this(BinkBridgeJNI.new_BINKSND(), true);
    }

    /* renamed from: b */
    public static long m33304b(C2677iS iSVar) {
        if (iSVar == null) {
            return 0;
        }
        return iSVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            BinkBridgeJNI.delete_BINKSND(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo19587b(aTU atu) {
        BinkBridgeJNI.BINKSND_sndwritepos_set(this.swigCPtr, this, aTU.m18355h(atu));
    }

    /* renamed from: Dx */
    public aTU mo19570Dx() {
        long BINKSND_sndwritepos_get = BinkBridgeJNI.BINKSND_sndwritepos_get(this.swigCPtr, this);
        if (BINKSND_sndwritepos_get == 0) {
            return null;
        }
        return new aTU(BINKSND_sndwritepos_get, false);
    }

    /* renamed from: bA */
    public void mo19588bA(long j) {
        BinkBridgeJNI.BINKSND_audiodecompsize_set(this.swigCPtr, this, j);
    }

    /* renamed from: Dy */
    public long mo19571Dy() {
        return BinkBridgeJNI.BINKSND_audiodecompsize_get(this.swigCPtr, this);
    }

    /* renamed from: bB */
    public void mo19589bB(long j) {
        BinkBridgeJNI.BINKSND_sndbufsize_set(this.swigCPtr, this, j);
    }

    /* renamed from: Dz */
    public long mo19572Dz() {
        return BinkBridgeJNI.BINKSND_sndbufsize_get(this.swigCPtr, this);
    }

    /* renamed from: c */
    public void mo19607c(aTU atu) {
        BinkBridgeJNI.BINKSND_sndbuf_set(this.swigCPtr, this, aTU.m18355h(atu));
    }

    /* renamed from: DA */
    public aTU mo19544DA() {
        long BINKSND_sndbuf_get = BinkBridgeJNI.BINKSND_sndbuf_get(this.swigCPtr, this);
        if (BINKSND_sndbuf_get == 0) {
            return null;
        }
        return new aTU(BINKSND_sndbuf_get, false);
    }

    /* renamed from: d */
    public void mo19608d(aTU atu) {
        BinkBridgeJNI.BINKSND_sndend_set(this.swigCPtr, this, aTU.m18355h(atu));
    }

    /* renamed from: DB */
    public aTU mo19545DB() {
        long BINKSND_sndend_get = BinkBridgeJNI.BINKSND_sndend_get(this.swigCPtr, this);
        if (BINKSND_sndend_get == 0) {
            return null;
        }
        return new aTU(BINKSND_sndend_get, false);
    }

    /* renamed from: bC */
    public void mo19590bC(long j) {
        BinkBridgeJNI.BINKSND_sndcomp_set(this.swigCPtr, this, j);
    }

    /* renamed from: DC */
    public long mo19546DC() {
        return BinkBridgeJNI.BINKSND_sndcomp_get(this.swigCPtr, this);
    }

    /* renamed from: e */
    public void mo19610e(aTU atu) {
        BinkBridgeJNI.BINKSND_sndreadpos_set(this.swigCPtr, this, aTU.m18355h(atu));
    }

    /* renamed from: DD */
    public aTU mo19547DD() {
        long BINKSND_sndreadpos_get = BinkBridgeJNI.BINKSND_sndreadpos_get(this.swigCPtr, this);
        if (BINKSND_sndreadpos_get == 0) {
            return null;
        }
        return new aTU(BINKSND_sndreadpos_get, false);
    }

    /* renamed from: bD */
    public void mo19591bD(long j) {
        BinkBridgeJNI.BINKSND_padding_set(this.swigCPtr, this, j);
    }

    /* renamed from: DE */
    public long mo19548DE() {
        return BinkBridgeJNI.BINKSND_padding_get(this.swigCPtr, this);
    }

    /* renamed from: bE */
    public void mo19592bE(long j) {
        BinkBridgeJNI.BINKSND_orig_freq_set(this.swigCPtr, this, j);
    }

    /* renamed from: DF */
    public long mo19549DF() {
        return BinkBridgeJNI.BINKSND_orig_freq_get(this.swigCPtr, this);
    }

    /* renamed from: bF */
    public void mo19593bF(long j) {
        BinkBridgeJNI.BINKSND_freq_set(this.swigCPtr, this, j);
    }

    /* renamed from: DG */
    public long mo19550DG() {
        return BinkBridgeJNI.BINKSND_freq_get(this.swigCPtr, this);
    }

    /* renamed from: bT */
    public void mo19601bT(int i) {
        BinkBridgeJNI.BINKSND_bits_set(this.swigCPtr, this, i);
    }

    /* renamed from: DH */
    public int mo19551DH() {
        return BinkBridgeJNI.BINKSND_bits_get(this.swigCPtr, this);
    }

    /* renamed from: bU */
    public void mo19602bU(int i) {
        BinkBridgeJNI.BINKSND_chans_set(this.swigCPtr, this, i);
    }

    /* renamed from: DI */
    public int mo19552DI() {
        return BinkBridgeJNI.BINKSND_chans_get(this.swigCPtr, this);
    }

    /* renamed from: bV */
    public void mo19603bV(int i) {
        BinkBridgeJNI.BINKSND_BestSizeIn16_set(this.swigCPtr, this, i);
    }

    /* renamed from: DJ */
    public int mo19553DJ() {
        return BinkBridgeJNI.BINKSND_BestSizeIn16_get(this.swigCPtr, this);
    }

    /* renamed from: bG */
    public void mo19594bG(long j) {
        BinkBridgeJNI.BINKSND_BestSizeMask_set(this.swigCPtr, this, j);
    }

    /* renamed from: DK */
    public long mo19554DK() {
        return BinkBridgeJNI.BINKSND_BestSizeMask_get(this.swigCPtr, this);
    }

    /* renamed from: bW */
    public void mo19604bW(int i) {
        BinkBridgeJNI.BINKSND_OnOff_set(this.swigCPtr, this, i);
    }

    /* renamed from: DL */
    public int mo19555DL() {
        return BinkBridgeJNI.BINKSND_OnOff_get(this.swigCPtr, this);
    }

    /* renamed from: bH */
    public void mo19595bH(long j) {
        BinkBridgeJNI.BINKSND_Latency_set(this.swigCPtr, this, j);
    }

    public long getLatency() {
        return BinkBridgeJNI.BINKSND_Latency_get(this.swigCPtr, this);
    }

    /* renamed from: bI */
    public void mo19596bI(long j) {
        BinkBridgeJNI.BINKSND_VideoScale_set(this.swigCPtr, this, j);
    }

    /* renamed from: DM */
    public long mo19556DM() {
        return BinkBridgeJNI.BINKSND_VideoScale_get(this.swigCPtr, this);
    }

    /* renamed from: bJ */
    public void mo19597bJ(long j) {
        BinkBridgeJNI.BINKSND_sndendframe_set(this.swigCPtr, this, j);
    }

    /* renamed from: DN */
    public long mo19557DN() {
        return BinkBridgeJNI.BINKSND_sndendframe_get(this.swigCPtr, this);
    }

    /* renamed from: bK */
    public void mo19598bK(long j) {
        BinkBridgeJNI.BINKSND_sndpad_set(this.swigCPtr, this, j);
    }

    /* renamed from: DO */
    public long mo19558DO() {
        return BinkBridgeJNI.BINKSND_sndpad_get(this.swigCPtr, this);
    }

    /* renamed from: bX */
    public void mo19605bX(int i) {
        BinkBridgeJNI.BINKSND_sndprime_set(this.swigCPtr, this, i);
    }

    /* renamed from: DP */
    public int mo19559DP() {
        return BinkBridgeJNI.BINKSND_sndprime_get(this.swigCPtr, this);
    }

    /* renamed from: bY */
    public void mo19606bY(int i) {
        BinkBridgeJNI.BINKSND_NoThreadService_set(this.swigCPtr, this, i);
    }

    /* renamed from: DQ */
    public int mo19560DQ() {
        return BinkBridgeJNI.BINKSND_NoThreadService_get(this.swigCPtr, this);
    }

    /* renamed from: bL */
    public void mo19599bL(long j) {
        BinkBridgeJNI.BINKSND_SoundDroppedOut_set(this.swigCPtr, this, j);
    }

    /* renamed from: DR */
    public long mo19561DR() {
        return BinkBridgeJNI.BINKSND_SoundDroppedOut_get(this.swigCPtr, this);
    }

    /* renamed from: bM */
    public void mo19600bM(long j) {
        BinkBridgeJNI.BINKSND_sndconvert8_set(this.swigCPtr, this, j);
    }

    /* renamed from: DS */
    public long mo19562DS() {
        return BinkBridgeJNI.BINKSND_sndconvert8_get(this.swigCPtr, this);
    }

    /* renamed from: f */
    public void mo19611f(aTU atu) {
        BinkBridgeJNI.BINKSND_snddata_set(this.swigCPtr, this, aTU.m18355h(atu));
    }

    /* renamed from: DT */
    public aTU mo19563DT() {
        long BINKSND_snddata_get = BinkBridgeJNI.BINKSND_snddata_get(this.swigCPtr, this);
        if (BINKSND_snddata_get == 0) {
            return null;
        }
        return new aTU(BINKSND_snddata_get, false);
    }

    /* renamed from: a */
    public void mo19581a(aID aid) {
        BinkBridgeJNI.BINKSND_Ready_set(this.swigCPtr, this, aID.m15359b(aid));
    }

    /* renamed from: DU */
    public aID mo19564DU() {
        long BINKSND_Ready_get = BinkBridgeJNI.BINKSND_Ready_get(this.swigCPtr, this);
        if (BINKSND_Ready_get == 0) {
            return null;
        }
        return new aID(BINKSND_Ready_get, false);
    }

    /* renamed from: a */
    public void mo19578a(C0608Ic ic) {
        BinkBridgeJNI.BINKSND_Lock_set(this.swigCPtr, this, C0608Ic.m5422b(ic));
    }

    /* renamed from: DV */
    public C0608Ic mo19565DV() {
        long BINKSND_Lock_get = BinkBridgeJNI.BINKSND_Lock_get(this.swigCPtr, this);
        if (BINKSND_Lock_get == 0) {
            return null;
        }
        return new C0608Ic(BINKSND_Lock_get, false);
    }

    /* renamed from: a */
    public void mo19583a(C2812kT kTVar) {
        BinkBridgeJNI.BINKSND_Unlock_set(this.swigCPtr, this, C2812kT.m34433b(kTVar));
    }

    /* renamed from: DW */
    public C2812kT mo19566DW() {
        long BINKSND_Unlock_get = BinkBridgeJNI.BINKSND_Unlock_get(this.swigCPtr, this);
        if (BINKSND_Unlock_get == 0) {
            return null;
        }
        return new C2812kT(BINKSND_Unlock_get, false);
    }

    /* renamed from: a */
    public void mo19580a(aHV ahv) {
        BinkBridgeJNI.BINKSND_Volume_set(this.swigCPtr, this, aHV.m15221c(ahv));
    }

    /* renamed from: DX */
    public aHV mo19567DX() {
        long BINKSND_Volume_get = BinkBridgeJNI.BINKSND_Volume_get(this.swigCPtr, this);
        if (BINKSND_Volume_get == 0) {
            return null;
        }
        return new aHV(BINKSND_Volume_get, false);
    }

    /* renamed from: b */
    public void mo19586b(aHV ahv) {
        BinkBridgeJNI.BINKSND_Pan_set(this.swigCPtr, this, aHV.m15221c(ahv));
    }

    /* renamed from: DY */
    public aHV mo19568DY() {
        long BINKSND_Pan_get = BinkBridgeJNI.BINKSND_Pan_get(this.swigCPtr, this);
        if (BINKSND_Pan_get == 0) {
            return null;
        }
        return new aHV(BINKSND_Pan_get, false);
    }

    /* renamed from: a */
    public void mo19579a(C5240aBs abs) {
        BinkBridgeJNI.BINKSND_Pause_set(this.swigCPtr, this, C5240aBs.m13099c(abs));
    }

    /* renamed from: DZ */
    public C5240aBs mo19569DZ() {
        long BINKSND_Pause_get = BinkBridgeJNI.BINKSND_Pause_get(this.swigCPtr, this);
        if (BINKSND_Pause_get == 0) {
            return null;
        }
        return new C5240aBs(BINKSND_Pause_get, false);
    }

    /* renamed from: b */
    public void mo19585b(C5240aBs abs) {
        BinkBridgeJNI.BINKSND_SetOnOff_set(this.swigCPtr, this, C5240aBs.m13099c(abs));
    }

    /* renamed from: Ea */
    public C5240aBs mo19573Ea() {
        long BINKSND_SetOnOff_get = BinkBridgeJNI.BINKSND_SetOnOff_get(this.swigCPtr, this);
        if (BINKSND_SetOnOff_get == 0) {
            return null;
        }
        return new C5240aBs(BINKSND_SetOnOff_get, false);
    }

    /* renamed from: a */
    public void mo19582a(C2754jY jYVar) {
        BinkBridgeJNI.BINKSND_Close_set(this.swigCPtr, this, C2754jY.m34039b(jYVar));
    }

    /* renamed from: Eb */
    public C2754jY mo19574Eb() {
        long BINKSND_Close_get = BinkBridgeJNI.BINKSND_Close_get(this.swigCPtr, this);
        if (BINKSND_Close_get == 0) {
            return null;
        }
        return new C2754jY(BINKSND_Close_get, false);
    }

    /* renamed from: a */
    public void mo19584a(C3094ni niVar) {
        BinkBridgeJNI.BINKSND_MixBins_set(this.swigCPtr, this, C3094ni.m36486b(niVar));
    }

    /* renamed from: Ec */
    public C3094ni mo19575Ec() {
        long BINKSND_MixBins_get = BinkBridgeJNI.BINKSND_MixBins_get(this.swigCPtr, this);
        if (BINKSND_MixBins_get == 0) {
            return null;
        }
        return new C3094ni(BINKSND_MixBins_get, false);
    }

    /* renamed from: a */
    public void mo19577a(C0362Ep ep) {
        BinkBridgeJNI.BINKSND_MixBinVols_set(this.swigCPtr, this, C0362Ep.m3030b(ep));
    }

    /* renamed from: Ed */
    public C0362Ep mo19576Ed() {
        long BINKSND_MixBinVols_get = BinkBridgeJNI.BINKSND_MixBinVols_get(this.swigCPtr, this);
        if (BINKSND_MixBinVols_get == 0) {
            return null;
        }
        return new C0362Ep(BINKSND_MixBinVols_get, false);
    }
}
