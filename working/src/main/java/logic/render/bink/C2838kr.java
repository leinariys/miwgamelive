package logic.render.bink;

import utaikodom.render.bink.BinkBridgeJNI;

/* renamed from: a.kr */
/* compiled from: a */
public class C2838kr {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2838kr(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C2838kr() {
        this(BinkBridgeJNI.new_BINKPLANE(), true);
    }

    /* renamed from: a */
    public static long m34578a(C2838kr krVar) {
        if (krVar == null) {
            return 0;
        }
        return krVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            BinkBridgeJNI.delete_BINKPLANE(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: cE */
    public void mo20147cE(int i) {
        BinkBridgeJNI.BINKPLANE_Allocate_set(this.swigCPtr, this, i);
    }

    /* renamed from: JX */
    public int mo20144JX() {
        return BinkBridgeJNI.BINKPLANE_Allocate_get(this.swigCPtr, this);
    }

    /* renamed from: g */
    public void mo20151g(C1263Sf sf) {
        BinkBridgeJNI.BINKPLANE_Buffer_set(this.swigCPtr, this, C1263Sf.m9484u(sf));
    }

    /* renamed from: JY */
    public C1263Sf mo20145JY() {
        long BINKPLANE_Buffer_get = BinkBridgeJNI.BINKPLANE_Buffer_get(this.swigCPtr, this);
        if (BINKPLANE_Buffer_get == 0) {
            return null;
        }
        return new C1263Sf(BINKPLANE_Buffer_get, false);
    }

    /* renamed from: cd */
    public void mo20148cd(long j) {
        BinkBridgeJNI.BINKPLANE_BufferPitch_set(this.swigCPtr, this, j);
    }

    /* renamed from: JZ */
    public long mo20146JZ() {
        return BinkBridgeJNI.BINKPLANE_BufferPitch_get(this.swigCPtr, this);
    }
}
