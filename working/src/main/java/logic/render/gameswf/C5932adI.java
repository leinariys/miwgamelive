package logic.render.gameswf;

/* renamed from: a.adI  reason: case insensitive filesystem */
/* compiled from: a */
public class C5932adI {
    private long swigCPtr;

    public C5932adI(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5932adI() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m20729a(C5932adI adi) {
        if (adi == null) {
            return 0;
        }
        return adi.swigCPtr;
    }
}
