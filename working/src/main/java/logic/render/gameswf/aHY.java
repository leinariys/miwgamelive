package logic.render.gameswf;

import utaikodom.render.gameswf.GameSWFBridgeJNI;

/* renamed from: a.aHY */
/* compiled from: a */
public class aHY {
    public static C3840ve ddX() {
        long j = GameSWFBridgeJNI.get_current_root();
        if (j == 0) {
            return null;
        }
        return new C3840ve(j, false);
    }

    /* renamed from: b */
    public static void m15232b(C3840ve veVar) {
        GameSWFBridgeJNI.set_current_root(C3840ve.m40338a(veVar), veVar);
    }

    /* renamed from: b */
    public static void m15230b(axK axk) {
        GameSWFBridgeJNI.register_log_callback(axK.m27035a(axk));
    }

    public static boolean get_verbose_parse() {
        return GameSWFBridgeJNI.get_verbose_parse();
    }

    public static void set_verbose_parse(boolean z) {
        GameSWFBridgeJNI.set_verbose_parse(z);
    }

    public static boolean get_verbose_debug() {
        return GameSWFBridgeJNI.get_verbose_debug();
    }

    public static boolean get_verbose_action() {
        return GameSWFBridgeJNI.get_verbose_action();
    }

    public static void set_verbose_action(boolean z) {
        GameSWFBridgeJNI.set_verbose_action(z);
    }

    /* renamed from: b */
    public static void m15231b(C2453fa faVar) {
        GameSWFBridgeJNI.set_render_handler(C2453fa.m31180a(faVar), faVar);
    }

    /* renamed from: b */
    public static void m15226b(C1040PG pg) {
        GameSWFBridgeJNI.set_sound_handler(C1040PG.m8357a(pg), pg);
    }

    public static C1040PG ddY() {
        long j = GameSWFBridgeJNI.get_sound_handler();
        if (j == 0) {
            return null;
        }
        return new C1040PG(j, false);
    }

    /* renamed from: b */
    public static void m15229b(C5768aaA aaa) {
        GameSWFBridgeJNI.register_file_opener_callback(C5768aaA.m19372a(aaa));
    }

    /* renamed from: b */
    public static void m15227b(C1323TN tn) {
        GameSWFBridgeJNI.register_fscommand_callback(C1323TN.m9959a(tn));
    }

    public static float get_curve_max_pixel_error() {
        return GameSWFBridgeJNI.get_curve_max_pixel_error();
    }

    public static void set_curve_max_pixel_error(float f) {
        GameSWFBridgeJNI.set_curve_max_pixel_error(f);
    }

    public static C2453fa ddZ() {
        long create_render_handler_ogl = GameSWFBridgeJNI.create_render_handler_ogl();
        if (create_render_handler_ogl == 0) {
            return null;
        }
        return new C2453fa(create_render_handler_ogl, false);
    }

    public static C1040PG dea() {
        long create_sound_handler_sdl = GameSWFBridgeJNI.create_sound_handler_sdl();
        if (create_sound_handler_sdl == 0) {
            return null;
        }
        return new C1040PG(create_sound_handler_sdl, false);
    }

    /* renamed from: mL */
    public static C1831aX m15234mL(String str) {
        long create_movie = GameSWFBridgeJNI.create_movie(str);
        if (create_movie == 0) {
            return null;
        }
        return new C1831aX(create_movie, false);
    }

    public static void clear_gameswf() {
        GameSWFBridgeJNI.clear_gameswf();
    }

    public static void clear_movie() {
        GameSWFBridgeJNI.clear_movie();
    }

    public static void register_default_file_opener_callback() {
        GameSWFBridgeJNI.register_default_file_opener_callback();
    }

    public static void register_memory_file_opener_callback() {
        GameSWFBridgeJNI.register_memory_file_opener_callback();
    }

    public static void clear_log_callback() {
        GameSWFBridgeJNI.clear_log_callback();
    }

    /* renamed from: B */
    public static C1040PG m15225B(int i, int i2, int i3) {
        long create_sound_handler_miles = GameSWFBridgeJNI.create_sound_handler_miles(i, i2, i3);
        if (create_sound_handler_miles == 0) {
            return null;
        }
        return new C1040PG(create_sound_handler_miles, false);
    }

    /* renamed from: b */
    public static void m15228b(C1831aX aXVar) {
        GameSWFBridgeJNI.drop_ref__SWIG_0(C1831aX.m19335a(aXVar), aXVar);
    }

    /* renamed from: c */
    public static void m15233c(C3840ve veVar) {
        GameSWFBridgeJNI.drop_ref__SWIG_1(C3840ve.m40338a(veVar), veVar);
    }
}
