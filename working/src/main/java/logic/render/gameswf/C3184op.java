package logic.render.gameswf;

/* renamed from: a.op */
/* compiled from: a */
public class C3184op {
    private long swigCPtr;

    public C3184op(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C3184op() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m36869a(C3184op opVar) {
        if (opVar == null) {
            return 0;
        }
        return opVar.swigCPtr;
    }
}
