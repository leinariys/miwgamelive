package logic.render.gameswf;

import utaikodom.render.gameswf.GameSWFBridgeJNI;

/* renamed from: a.fa */
/* compiled from: a */
public class C2453fa {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2453fa(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    /* renamed from: a */
    public static long m31180a(C2453fa faVar) {
        if (faVar == null) {
            return 0;
        }
        return faVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            GameSWFBridgeJNI.delete_render_handler(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public C1414Uj adF() {
        long render_handler_create_bitmap_info_empty = GameSWFBridgeJNI.render_handler_create_bitmap_info_empty(this.swigCPtr, this);
        if (render_handler_create_bitmap_info_empty == 0) {
            return null;
        }
        return new C1414Uj(render_handler_create_bitmap_info_empty, false);
    }

    /* renamed from: a */
    public C1414Uj mo18711a(int i, int i2, C0048Aa aa) {
        long render_handler_create_bitmap_info_alpha = GameSWFBridgeJNI.render_handler_create_bitmap_info_alpha(this.swigCPtr, this, i, i2, C0048Aa.m436a(aa));
        if (render_handler_create_bitmap_info_alpha == 0) {
            return null;
        }
        return new C1414Uj(render_handler_create_bitmap_info_alpha, false);
    }

    public C6024aew adG() {
        long render_handler_create_video_handler = GameSWFBridgeJNI.render_handler_create_video_handler(this.swigCPtr, this);
        if (render_handler_create_video_handler == 0) {
            return null;
        }
        return new C6024aew(render_handler_create_video_handler, false);
    }

    /* renamed from: a */
    public void mo18715a(C5734aUs aus) {
        GameSWFBridgeJNI.render_handler_set_matrix(this.swigCPtr, this, C5734aUs.m18752b(aus));
    }

    /* renamed from: a */
    public void mo18713a(aIZ aiz) {
        GameSWFBridgeJNI.render_handler_set_cxform(this.swigCPtr, this, aIZ.m15424b(aiz));
    }

    /* renamed from: a */
    public void mo18714a(aJY ajy, int i) {
        GameSWFBridgeJNI.render_handler_draw_mesh_strip(this.swigCPtr, this, aJY.m15787b(ajy), i);
    }

    /* renamed from: b */
    public void mo18723b(aJY ajy, int i) {
        GameSWFBridgeJNI.render_handler_draw_triangle_list(this.swigCPtr, this, aJY.m15787b(ajy), i);
    }

    /* renamed from: c */
    public void mo18725c(aJY ajy, int i) {
        GameSWFBridgeJNI.render_handler_draw_line_strip(this.swigCPtr, this, aJY.m15787b(ajy), i);
    }

    /* renamed from: eE */
    public void mo18728eE(int i) {
        GameSWFBridgeJNI.render_handler_fill_style_disable(this.swigCPtr, this, i);
    }

    /* renamed from: a */
    public void mo18712a(int i, C1414Uj uj, C5734aUs aus, C2454a aVar) {
        GameSWFBridgeJNI.render_handler_fill_style_bitmap(this.swigCPtr, this, i, C1414Uj.m10332a(uj), C5734aUs.m18752b(aus), aVar.swigValue());
    }

    public void adH() {
        GameSWFBridgeJNI.render_handler_line_style_disable(this.swigCPtr, this);
    }

    /* renamed from: dl */
    public void mo18727dl(float f) {
        GameSWFBridgeJNI.render_handler_line_style_width(this.swigCPtr, this, f);
    }

    /* renamed from: bq */
    public void mo18724bq(boolean z) {
        GameSWFBridgeJNI.render_handler_set_antialiased(this.swigCPtr, this, z);
    }

    public void adI() {
        GameSWFBridgeJNI.render_handler_begin_submit_mask(this.swigCPtr, this);
    }

    public void adJ() {
        GameSWFBridgeJNI.render_handler_end_submit_mask(this.swigCPtr, this);
    }

    public void adK() {
        GameSWFBridgeJNI.render_handler_disable_mask(this.swigCPtr, this);
    }

    /* renamed from: a */
    public void mo18716a(C2455b bVar) {
        GameSWFBridgeJNI.render_handler_set_cursor(this.swigCPtr, this, bVar.swigValue());
    }

    /* renamed from: a.fa$a */
    public static final class C2454a {

        /* renamed from: Jc */
        public static final C2454a f7402Jc = new C2454a("WRAP_REPEAT");

        /* renamed from: Jd */
        public static final C2454a f7403Jd = new C2454a("WRAP_CLAMP");

        /* renamed from: Je */
        private static C2454a[] f7404Je = {f7402Jc, f7403Jd};

        /* renamed from: pF */
        private static int f7405pF = 0;

        /* renamed from: pG */
        private final int f7406pG;

        /* renamed from: pH */
        private final String f7407pH;

        private C2454a(String str) {
            this.f7407pH = str;
            int i = f7405pF;
            f7405pF = i + 1;
            this.f7406pG = i;
        }

        private C2454a(String str, int i) {
            this.f7407pH = str;
            this.f7406pG = i;
            f7405pF = i + 1;
        }

        private C2454a(String str, C2454a aVar) {
            this.f7407pH = str;
            this.f7406pG = aVar.f7406pG;
            f7405pF = this.f7406pG + 1;
        }

        /* renamed from: aH */
        public static C2454a m31192aH(int i) {
            if (i < f7404Je.length && i >= 0 && f7404Je[i].f7406pG == i) {
                return f7404Je[i];
            }
            for (int i2 = 0; i2 < f7404Je.length; i2++) {
                if (f7404Je[i2].f7406pG == i) {
                    return f7404Je[i2];
                }
            }
            throw new IllegalArgumentException("No enum " + C2454a.class + " with value " + i);
        }

        public final int swigValue() {
            return this.f7406pG;
        }

        public String toString() {
            return this.f7407pH;
        }
    }

    /* renamed from: a.fa$b */
    /* compiled from: a */
    public static final class C2455b {
        public static final C2455b ata = new C2455b("SYSTEM_CURSOR");
        public static final C2455b atb = new C2455b("ACTIVE_CURSOR");
        private static C2455b[] atc = {ata, atb};

        /* renamed from: pF */
        private static int f7408pF = 0;

        /* renamed from: pG */
        private final int f7409pG;

        /* renamed from: pH */
        private final String f7410pH;

        private C2455b(String str) {
            this.f7410pH = str;
            int i = f7408pF;
            f7408pF = i + 1;
            this.f7409pG = i;
        }

        private C2455b(String str, int i) {
            this.f7410pH = str;
            this.f7409pG = i;
            f7408pF = i + 1;
        }

        private C2455b(String str, C2455b bVar) {
            this.f7410pH = str;
            this.f7409pG = bVar.f7409pG;
            f7408pF = this.f7409pG + 1;
        }

        /* renamed from: cD */
        public static C2455b m31193cD(int i) {
            if (i < atc.length && i >= 0 && atc[i].f7409pG == i) {
                return atc[i];
            }
            for (int i2 = 0; i2 < atc.length; i2++) {
                if (atc[i2].f7409pG == i) {
                    return atc[i2];
                }
            }
            throw new IllegalArgumentException("No enum " + C2455b.class + " with value " + i);
        }

        public final int swigValue() {
            return this.f7409pG;
        }

        public String toString() {
            return this.f7410pH;
        }
    }
}
