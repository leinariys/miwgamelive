package logic.render.gameswf;

import utaikodom.render.gameswf.GameSWFBridgeJNI;

/* renamed from: a.PG */
/* compiled from: a */
public class C1040PG {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1040PG(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    /* renamed from: a */
    public static long m8357a(C1040PG pg) {
        if (pg == null) {
            return 0;
        }
        return pg.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            GameSWFBridgeJNI.delete_sound_handler(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public int mo4630a(aJY ajy, int i, int i2, C1041a aVar, int i3, boolean z) {
        return GameSWFBridgeJNI.sound_handler_create_sound(this.swigCPtr, this, aJY.m15787b(ajy), i, i2, aVar.swigValue(), i3, z);
    }

    /* renamed from: a */
    public void mo4631a(int i, aJY ajy, int i2) {
        GameSWFBridgeJNI.sound_handler_append_sound(this.swigCPtr, this, i, aJY.m15787b(ajy), i2);
    }

    /* renamed from: O */
    public void mo4628O(int i, int i2) {
        GameSWFBridgeJNI.sound_handler_play_sound(this.swigCPtr, this, i, i2);
    }

    /* renamed from: P */
    public void mo4629P(int i, int i2) {
        GameSWFBridgeJNI.sound_handler_set_volume(this.swigCPtr, this, i, i2);
    }

    /* renamed from: mD */
    public void mo4640mD(int i) {
        GameSWFBridgeJNI.sound_handler_stop_sound(this.swigCPtr, this, i);
    }

    public void bnR() {
        GameSWFBridgeJNI.sound_handler_stop_all_sounds(this.swigCPtr, this);
    }

    /* renamed from: mE */
    public void mo4641mE(int i) {
        GameSWFBridgeJNI.sound_handler_delete_sound(this.swigCPtr, this, i);
    }

    /* renamed from: a */
    public void mo4633a(aVO avo, C3532sD sDVar) {
        GameSWFBridgeJNI.sound_handler_attach_aux_streamer(this.swigCPtr, this, aVO.m18804a(avo), C3532sD.m38683a(sDVar));
    }

    /* renamed from: b */
    public void mo4635b(C3532sD sDVar) {
        GameSWFBridgeJNI.sound_handler_detach_aux_streamer(this.swigCPtr, this, C3532sD.m38683a(sDVar));
    }

    /* renamed from: a */
    public void mo4634a(C3184op opVar, C1483Vp vp, C0048Aa aa, int i, int i2, int i3) {
        GameSWFBridgeJNI.sound_handler_cvt(this.swigCPtr, this, C3184op.m36869a(opVar), C1483Vp.m10893a(vp), C0048Aa.m436a(aa), i, i2, i3);
    }

    public boolean bnS() {
        return GameSWFBridgeJNI.sound_handler_is_open(this.swigCPtr, this);
    }

    /* renamed from: a */
    public void mo4632a(int i, boolean z) {
        GameSWFBridgeJNI.sound_handler_pause(this.swigCPtr, this, i, z);
    }

    /* renamed from: a.PG$a */
    public static final class C1041a {
        public static final C1041a iyU = new C1041a("FORMAT_RAW", GameSWFBridgeJNI.sound_handler_FORMAT_RAW_get());
        public static final C1041a iyV = new C1041a("FORMAT_ADPCM", GameSWFBridgeJNI.sound_handler_FORMAT_ADPCM_get());
        public static final C1041a iyW = new C1041a("FORMAT_MP3", GameSWFBridgeJNI.sound_handler_FORMAT_MP3_get());
        public static final C1041a iyX = new C1041a("FORMAT_UNCOMPRESSED", GameSWFBridgeJNI.sound_handler_FORMAT_UNCOMPRESSED_get());
        public static final C1041a iyY = new C1041a("FORMAT_NELLYMOSER", GameSWFBridgeJNI.sound_handler_FORMAT_NELLYMOSER_get());
        public static final C1041a iyZ = new C1041a("FORMAT_NATIVE16", GameSWFBridgeJNI.sound_handler_FORMAT_NATIVE16_get());
        private static C1041a[] iza = {iyU, iyV, iyW, iyX, iyY, iyZ};

        /* renamed from: pF */
        private static int f1357pF = 0;

        /* renamed from: pG */
        private final int f1358pG;

        /* renamed from: pH */
        private final String f1359pH;

        private C1041a(String str) {
            this.f1359pH = str;
            int i = f1357pF;
            f1357pF = i + 1;
            this.f1358pG = i;
        }

        private C1041a(String str, int i) {
            this.f1359pH = str;
            this.f1358pG = i;
            f1357pF = i + 1;
        }

        private C1041a(String str, C1041a aVar) {
            this.f1359pH = str;
            this.f1358pG = aVar.f1358pG;
            f1357pF = this.f1358pG + 1;
        }

        /* renamed from: zO */
        public static C1041a m8368zO(int i) {
            if (i < iza.length && i >= 0 && iza[i].f1358pG == i) {
                return iza[i];
            }
            for (int i2 = 0; i2 < iza.length; i2++) {
                if (iza[i2].f1358pG == i) {
                    return iza[i2];
                }
            }
            throw new IllegalArgumentException("No enum " + C1041a.class + " with value " + i);
        }

        public final int swigValue() {
            return this.f1358pG;
        }

        public String toString() {
            return this.f1359pH;
        }
    }
}
