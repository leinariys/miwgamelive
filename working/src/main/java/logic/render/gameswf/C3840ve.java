package logic.render.gameswf;

import utaikodom.render.gameswf.GameSWFBridgeJNI;

/* renamed from: a.ve */
/* compiled from: a */
public class C3840ve {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3840ve(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    /* renamed from: a */
    public static long m40338a(C3840ve veVar) {
        if (veVar == null) {
            return 0;
        }
        return veVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            GameSWFBridgeJNI.delete_movie_interface(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public C3597sk akt() {
        long movie_interface_get_movie_definition = GameSWFBridgeJNI.movie_interface_get_movie_definition(this.swigCPtr, this);
        if (movie_interface_get_movie_definition == 0) {
            return null;
        }
        return new C3597sk(movie_interface_get_movie_definition, false);
    }

    public int aku() {
        return GameSWFBridgeJNI.movie_interface_get_current_frame(this.swigCPtr, this);
    }

    public boolean akv() {
        return GameSWFBridgeJNI.movie_interface_has_looped(this.swigCPtr, this);
    }

    /* renamed from: dR */
    public void mo22644dR(float f) {
        GameSWFBridgeJNI.movie_interface_advance(this.swigCPtr, this, f);
    }

    /* renamed from: eW */
    public void mo22649eW(int i) {
        GameSWFBridgeJNI.movie_interface_goto_frame(this.swigCPtr, this, i);
    }

    /* renamed from: co */
    public boolean mo22642co(String str) {
        return GameSWFBridgeJNI.movie_interface_goto_labeled_frame(this.swigCPtr, this, str);
    }

    public void display() {
        GameSWFBridgeJNI.movie_interface_display(this.swigCPtr, this);
    }

    /* renamed from: a */
    public void mo22624a(C3841a aVar) {
        GameSWFBridgeJNI.movie_interface_set_play_state(this.swigCPtr, this, aVar.swigValue());
    }

    public C3841a akw() {
        return C3841a.m40354Ar(GameSWFBridgeJNI.movie_interface_get_play_state(this.swigCPtr, this));
    }

    /* renamed from: b */
    public void mo22640b(C2394en enVar) {
        GameSWFBridgeJNI.movie_interface_set_background_color(this.swigCPtr, this, C2394en.m29987a(enVar));
    }

    /* renamed from: dS */
    public void mo22645dS(float f) {
        GameSWFBridgeJNI.movie_interface_set_background_alpha(this.swigCPtr, this, f);
    }

    public float akx() {
        return GameSWFBridgeJNI.movie_interface_get_background_alpha(this.swigCPtr, this);
    }

    /* renamed from: a */
    public void mo22622a(int i, int i2, int i3, int i4) {
        GameSWFBridgeJNI.movie_interface_set_display_viewport(this.swigCPtr, this, i, i2, i3, i4);
    }

    /* renamed from: e */
    public void mo22648e(int i, int i2, int i3) {
        GameSWFBridgeJNI.movie_interface_notify_mouse_state(this.swigCPtr, this, i, i2, i3);
    }

    /* renamed from: r */
    public void mo22651r(String str, String str2) {
        GameSWFBridgeJNI.movie_interface_set_variable__SWIG_0(this.swigCPtr, this, str, str2);
    }

    /* renamed from: a */
    public void mo22625a(String str, C0419Fq fq) {
        GameSWFBridgeJNI.movie_interface_set_variable__SWIG_1(this.swigCPtr, this, str, C0419Fq.m3290a(fq));
    }

    /* renamed from: cp */
    public String mo22643cp(String str) {
        return GameSWFBridgeJNI.movie_interface_get_variable(this.swigCPtr, this, str);
    }

    /* renamed from: bx */
    public void mo22641bx(boolean z) {
        GameSWFBridgeJNI.movie_interface_set_visible(this.swigCPtr, this, z);
    }

    public boolean aky() {
        return GameSWFBridgeJNI.movie_interface_get_visible(this.swigCPtr, this);
    }

    public aJY akz() {
        long movie_interface_get_userdata = GameSWFBridgeJNI.movie_interface_get_userdata(this.swigCPtr, this);
        if (movie_interface_get_userdata == 0) {
            return null;
        }
        return new aJY(movie_interface_get_userdata, false);
    }

    /* renamed from: a */
    public void mo22623a(aJY ajy) {
        GameSWFBridgeJNI.movie_interface_set_userdata(this.swigCPtr, this, aJY.m15787b(ajy));
    }

    /* renamed from: a */
    public void mo22626a(String str, C6072afs afs, aJY ajy) {
        GameSWFBridgeJNI.movie_interface_attach_display_callback(this.swigCPtr, this, str, C6072afs.m21648a(afs), aJY.m15787b(ajy));
    }

    public C5932adI akA() {
        long movie_interface_get_root_movie = GameSWFBridgeJNI.movie_interface_get_root_movie(this.swigCPtr, this);
        if (movie_interface_get_root_movie == 0) {
            return null;
        }
        return new C5932adI(movie_interface_get_root_movie, false);
    }

    /* renamed from: b */
    public void mo22639b(C2214cp cpVar, boolean z) {
        GameSWFBridgeJNI.movie_interface_notify_key_event(this.swigCPtr, this, C2214cp.m28640a(cpVar), z);
    }

    public int akB() {
        return GameSWFBridgeJNI.movie_interface_get_movie_version(this.swigCPtr, this);
    }

    public int akC() {
        return GameSWFBridgeJNI.movie_interface_get_movie_width(this.swigCPtr, this);
    }

    public int akD() {
        return GameSWFBridgeJNI.movie_interface_get_movie_height(this.swigCPtr, this);
    }

    public float akE() {
        return GameSWFBridgeJNI.movie_interface_get_movie_fps(this.swigCPtr, this);
    }

    /* renamed from: a.ve$a */
    public static final class C3841a {
        public static final C3841a iGr = new C3841a("PLAY");
        public static final C3841a iGs = new C3841a("STOP");
        private static C3841a[] iGt = {iGr, iGs};

        /* renamed from: pF */
        private static int f9433pF = 0;

        /* renamed from: pG */
        private final int f9434pG;

        /* renamed from: pH */
        private final String f9435pH;

        private C3841a(String str) {
            this.f9435pH = str;
            int i = f9433pF;
            f9433pF = i + 1;
            this.f9434pG = i;
        }

        private C3841a(String str, int i) {
            this.f9435pH = str;
            this.f9434pG = i;
            f9433pF = i + 1;
        }

        private C3841a(String str, C3841a aVar) {
            this.f9435pH = str;
            this.f9434pG = aVar.f9434pG;
            f9433pF = this.f9434pG + 1;
        }

        /* renamed from: Ar */
        public static C3841a m40354Ar(int i) {
            if (i < iGt.length && i >= 0 && iGt[i].f9434pG == i) {
                return iGt[i];
            }
            for (int i2 = 0; i2 < iGt.length; i2++) {
                if (iGt[i2].f9434pG == i) {
                    return iGt[i2];
                }
            }
            throw new IllegalArgumentException("No enum " + C3841a.class + " with value " + i);
        }

        public final int swigValue() {
            return this.f9434pG;
        }

        public String toString() {
            return this.f9435pH;
        }
    }
}
