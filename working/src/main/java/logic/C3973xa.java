package logic;

import java.util.Collection;
import java.util.Map;

/* renamed from: a.xa */
/* compiled from: a */
public class C3973xa extends C1753Zu implements C6245ajJ {
    aVH<Object> bFA = new C0364Er(this);
    C6245ajJ bFz;

    public C3973xa(C6245ajJ ajj) {
        this.bFz = ajj;
    }

    /* renamed from: l */
    public void mo7502l(String str, Object obj) {
        super.mo7502l(str, obj);
    }

    /* renamed from: m */
    public void mo7503m(String str, Object obj) {
        super.mo7503m(str, obj);
    }

    /* renamed from: d */
    public void mo13972d(String str, Object obj) {
        this.bFz.mo13972d(str, obj);
    }

    /* renamed from: h */
    public void mo13975h(Object obj) {
        this.bFz.mo13975h(obj);
    }

    public void publish(Object obj) {
        this.bFz.publish(obj);
    }

    /* renamed from: a */
    public void mo2869a(String str, Object obj) {
        this.bFz.mo2869a(str, obj);
    }

    @Override
    public <T> Collection<T> mo13967b(Class<T> cls) {
        return null;
    }

    @Override
    public <T> void mo13968b(Class<T> cls, aVH<T> avh) {

    }

    @Override
    public <T> void mo13969b(String str, Class<?> cls, aVH<T> avh) {

    }

    /* renamed from: g */
    public void mo13974g(Object obj) {
        this.bFz.mo13974g(obj);
    }

    /* renamed from: b */
    public void mo13970b(String str, Object obj) {
        this.bFz.mo13970b(str, obj);
    }

    /* renamed from: a */
    public <T> void mo7499a(String str, Class<T> cls, aVH<T> avh) {
        super.mo7499a(str, cls, avh);
        this.bFz.mo13966a(str, cls, this.bFA);
    }

    /* renamed from: a */
    public <T> void mo7497a(aVH<T> avh) {
        for (Map.Entry entry : this.eNC.entrySet()) {
            if (((Collection) entry.getValue()).remove(avh) && ((Collection) entry.getValue()).isEmpty()) {
                this.bFz.mo13969b((String) ((C1753Zu.C1754a) entry.getKey()).getKey(), ((C1753Zu.C1754a) entry.getKey()).getClazz(), this.bFA);
            }
        }
    }

    /* renamed from: b */
    public <T> void mo7501b(String str, Class<T> cls, aVH<T> avh) {
        super.mo7501b(str, cls, avh);
        Collection aR = this.eNC.mo10040aR(new C1753Zu.C1754a(str, cls));
        if (aR == null || aR.size() == 0) {
            this.bFz.mo13969b(str, cls, this.bFA);
        }
    }

    public void dispose() {
        this.eNC.clear();
        this.bFz.mo13964a(this.bFA);
    }

    /* renamed from: a */
    public <T> Collection<T> mo13963a(String str, Class<T> cls) {
        return this.bFz.mo13963a(str, cls);
    }

    @Override
    public <T> void mo13964a(aVH<T> avh) {

    }

    @Override
    public <T> void mo13965a(Class<T> cls, aVH<T> avh) {

    }

    @Override
    public <T> void mo13966a(String str, Class<?> cls, aVH<T> avh) {

    }

    /* renamed from: e */
    public <T> Collection<T> mo13973e(String str, T t) {
        return this.bFz.mo13973e(str, t);
    }

    /* renamed from: j */
    public <T> Collection<T> mo13977j(T t) {
        return mo13973e("", t);
    }

    /* renamed from: i */
    public void mo13976i(Object obj) {
        this.bFz.mo13976i(obj);
    }

    /* renamed from: c */
    public void mo13971c(String str, Object obj) {
        this.bFz.mo13971c(str, obj);
    }
}
