package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicViewportUI;
import java.awt.*;

/* renamed from: a.QE */
/* compiled from: a */
public class ViewportUI extends BasicViewportUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f1418Rp;

    public ViewportUI(JViewport jViewport) {
        this.f1418Rp = new ComponentManager("viewport", jViewport);
        jViewport.setOpaque(false);
        jViewport.setScrollMode(0);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new ViewportUI((JViewport) jComponent);
    }

    public void installUI(JComponent jComponent) {
        ViewportUI.super.installUI(jComponent);
        this.f1418Rp.mo13045Vk();
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        paint(graphics, jComponent);
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13057b(jComponent, ViewportUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13052a(jComponent, ViewportUI.super.getMaximumSize(jComponent));
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return this.f1418Rp.mo13059c(jComponent, ViewportUI.super.getPreferredSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f1418Rp;
    }
}
