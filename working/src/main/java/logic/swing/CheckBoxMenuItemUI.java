package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicMenuItemUI;
import java.awt.*;

/* renamed from: a.abN  reason: case insensitive filesystem */
/* compiled from: a */
public class CheckBoxMenuItemUI extends BasicMenuItemUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f4134Rp;

    public CheckBoxMenuItemUI(JMenuItem jMenuItem) {
        this.f4134Rp = new ComponentManager("menuitem", jMenuItem);
        jMenuItem.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new CheckBoxMenuItemUI((JMenuItem) jComponent);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9084a((IComponentUi) this, graphics, (AbstractButton) jComponent);
        paint(graphics, jComponent);
    }

    /* access modifiers changed from: protected */
    public void paintBackground(Graphics graphics, JMenuItem jMenuItem, Color color) {
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13059c(jComponent, CheckBoxMenuItemUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13057b(jComponent, CheckBoxMenuItemUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13052a(jComponent, CheckBoxMenuItemUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f4134Rp;
    }
}
