package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicColorChooserUI;
import java.awt.*;

/* renamed from: a.aPW */
/* compiled from: a */
public class ColorChooserUI extends BasicColorChooserUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f3530Rp;

    public ColorChooserUI(JColorChooser jColorChooser) {
        this.f3530Rp = new ComponentManager("colorchooser", jColorChooser);
        jColorChooser.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new ColorChooserUI((JColorChooser) jComponent);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        paint(graphics, jComponent);
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        ColorChooserUI.super.paint(graphics, jComponent);
        AdapterUiCss.m9099a((ComponentUI) this, graphics, jComponent);
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        return getComponentManager().mo13059c(jComponent, ColorChooserUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        return getComponentManager().mo13057b(jComponent, ColorChooserUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        return getComponentManager().mo13052a(jComponent, ColorChooserUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f3530Rp;
    }
}
