package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicMenuUI;
import java.awt.*;

/* renamed from: a.Bt */
/* compiled from: a */
public class MenuUI extends BasicMenuUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f260Rp;

    public MenuUI(JMenu jMenu) {
        this.f260Rp = new C1401UY("menu", jMenu);
        jMenu.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new MenuUI((JMenu) jComponent);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9084a((IComponentUi) this, graphics, (AbstractButton) jComponent);
        paint(graphics, jComponent);
    }

    /* access modifiers changed from: protected */
    public void paintBackground(Graphics graphics, JMenuItem jMenuItem, Color color) {
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13059c(jComponent, MenuUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13057b(jComponent, MenuUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13052a(jComponent, MenuUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f260Rp;
    }
}
