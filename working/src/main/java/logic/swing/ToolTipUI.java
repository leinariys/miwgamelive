package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.PropertiesUiFromCss;
import sun.swing.SwingUtilities2;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicToolTipUI;
import javax.swing.text.View;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;

/* renamed from: a.aiR  reason: case insensitive filesystem */
/* compiled from: a */
public class ToolTipUI extends BasicToolTipUI implements IComponentUi {
    private static final Color cds = new Color(0, 0, 0, 0);
    /* access modifiers changed from: private */
    public JToolTip iNi = null;
    /* access modifiers changed from: private */
    public JComponent iNj;
    /* access modifiers changed from: private */
    public Timer iNk = new Timer(500, new C1908b());
    /* renamed from: Rp */
    private ComponentManager f4639Rp = new ComponentManager("tooltip", this.iNi);

    public ToolTipUI(JToolTip jToolTip) {
        this.iNi = jToolTip;
        this.iNi.setOpaque(false);
        this.iNi.setBorder(new BorderWrapper());
        this.iNi.addPropertyChangeListener("component", new C1907a());
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new ToolTipUI((JToolTip) jComponent);
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f4639Rp;
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        Border border;
        String str;
        Border border2 = jComponent.getBorder();
        if (!(border2 instanceof BorderWrapper)) {
            Border lu = new BorderWrapper();
            jComponent.setBorder(lu);
            border = lu;
        } else {
            border = border2;
        }
        PropertiesUiFromCss Vp = this.f4639Rp.mo13047Vp();
        AdapterUiCss.m9093a(graphics, (Component) jComponent, Vp);
        graphics.setFont(Vp.getFont());
        if (jComponent.getComponentCount() <= 0) {
            FontMetrics fontMetrics = SwingUtilities2.getFontMetrics(jComponent, graphics, Vp.getFont());
            graphics.setColor(cds);
            graphics.setColor(Vp.getColor());
            String tipText = ((JToolTip) jComponent).getTipText();
            if (tipText == null) {
                str = "";
            } else {
                str = tipText;
            }
            Insets borderInsets = border.getBorderInsets(jComponent);
            Dimension size = jComponent.getSize();
            Rectangle rectangle = new Rectangle(borderInsets.left, borderInsets.top, size.width - (borderInsets.left + borderInsets.right), size.height - (borderInsets.bottom + borderInsets.top));
            View view = (View) jComponent.getClientProperty("html");
            if (view != null) {
                graphics.setFont(Vp.getFont());
                graphics.setColor(Vp.getColor());
                view.paint(graphics, rectangle);
                return;
            }
            graphics.setFont(Vp.getFont());
            graphics.setColor(Vp.getShadowColor());
            SwingUtilities2.drawString(jComponent, graphics, str, (int) (((float) rectangle.x) + Vp.atO()), ((int) (((float) rectangle.y) + Vp.atP())) + fontMetrics.getAscent());
            graphics.setColor(Vp.getColor());
            SwingUtilities2.drawString(jComponent, graphics, str, rectangle.x, rectangle.y + fontMetrics.getAscent());
        }
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        if (jComponent.getComponentCount() > 0) {
            return null;
        }
        PropertiesUiFromCss Vp = this.f4639Rp.mo13047Vp();
        FontMetrics fontMetrics = jComponent.getFontMetrics(Vp.getFont());
        BorderWrapper border = (BorderWrapper) jComponent.getBorder();
        if (!(border instanceof BorderWrapper)) {
            border = new BorderWrapper();
            jComponent.setBorder(border);
        }
        Insets borderInsets = border.getBorderInsets(jComponent);
        Dimension dimension = new Dimension(borderInsets.left + borderInsets.right, borderInsets.bottom + borderInsets.top);
        String tipText = ((JToolTip) jComponent).getTipText();
        if (tipText != null && !tipText.equals("")) {
            View view = (View) jComponent.getClientProperty("html");
            if (view != null) {
                dimension.width = (int) (((float) dimension.width) + ((float) ((int) view.getPreferredSpan(0))) + Vp.atO());
                dimension.height = (int) (((float) ((int) view.getPreferredSpan(1))) + Vp.atP() + ((float) dimension.height));
            } else {
                dimension.width = (int) (((float) dimension.width) + ((float) SwingUtilities2.stringWidth(jComponent, fontMetrics, tipText)) + Vp.atO());
                dimension.height = (int) (((float) dimension.height) + Vp.atP() + ((float) fontMetrics.getHeight()));
            }
        }
        return dimension;
    }

    /* renamed from: a.aiR$b */
    /* compiled from: a */
    class C1908b implements ActionListener {
        C1908b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (!ToolTipUI.this.iNi.isVisible()) {
                ToolTipUI.this.iNk.stop();
                ToolTipUI.this.iNj = null;
            } else if (ToolTipUI.this.iNj == null || !ToolTipUI.this.iNj.isShowing()) {
                ToolTipUI.this.iNk.stop();
                ToolTipUI.this.iNi.setVisible(false);
                ToolTipUI.this.iNj = null;
            }
        }
    }

    /* renamed from: a.aiR$a */
    class C1907a implements PropertyChangeListener {
        C1907a() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:2:0x0009, code lost:
            r0 = (javax.swing.JComponent) r5.getNewValue();
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void propertyChange(java.beans.PropertyChangeEvent r5) {
            /*
                r4 = this;
                java.lang.Object r0 = r5.getNewValue()
                boolean r0 = r0 instanceof javax.swing.JComponent
                if (r0 != 0) goto L_0x0009
            L_0x0008:
                return
            L_0x0009:
                java.lang.Object r0 = r5.getNewValue()
                javax.swing.JComponent r0 = (javax.swing.JComponent) r0
                a.wz r1 = p001a.C3940wz.m40778d(r0)
                if (r1 == 0) goto L_0x0008
                java.awt.Component r1 = r1.mo2675b(r0)
                if (r1 == 0) goto L_0x0073
                a.aiR r2 = logic.swing.C6201aiR.this
                javax.swing.JToolTip r2 = r2.iNi
                java.awt.BorderLayout r3 = new java.awt.BorderLayout
                r3.<init>()
                r2.setLayout(r3)
                a.aiR r2 = logic.swing.C6201aiR.this
                javax.swing.JToolTip r2 = r2.iNi
                r2.add(r1)
                a.aiR r2 = logic.swing.C6201aiR.this
                r2.iNj = r0
                a.aiR r0 = logic.swing.C6201aiR.this
                javax.swing.Timer r0 = r0.iNk
                r0.restart()
                a.aeK r0 = p001a.C3280pv.m37354e(r1)
                java.lang.String r1 = "tooltipClass"
                java.lang.String r0 = r0.getAttribute(r1)
                if (r0 == 0) goto L_0x005b
                a.aiR r1 = logic.swing.C6201aiR.this
                javax.swing.JToolTip r1 = r1.iNi
                a.aeK r1 = p001a.C3280pv.m37354e(r1)
                java.lang.String r2 = "class"
                r1.setAttribute(r2, r0)
            L_0x005b:
                a.aiR r0 = logic.swing.C6201aiR.this
                javax.swing.JToolTip r0 = r0.iNi
                java.awt.Container r0 = r0.getParent()
                if (r0 == 0) goto L_0x0008
                a.aeK r0 = p001a.C3280pv.m37354e(r0)
                java.lang.String r1 = "class"
                java.lang.String r2 = "tooltipParent"
                r0.setAttribute(r1, r2)
                goto L_0x0008
            L_0x0073:
                a.aiR r0 = logic.swing.C6201aiR.this
                javax.swing.JToolTip r0 = r0.iNi
                r1 = 0
                r0.setVisible(r1)
                goto L_0x0008
            */
            throw new UnsupportedOperationException("Method not decompiled: logic.swing.C6201aiR.C1907a.propertyChange(java.beans.PropertyChangeEvent):void");
        }
    }
}
