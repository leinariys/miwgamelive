package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.IComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicListUI;
import java.awt.*;

/* renamed from: a.GW */
/* compiled from: a */
public class ListUI extends BasicListUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager componentManager;

    public ListUI(JList jList) {
        this.componentManager = new ComponentManager("list", jList);
        jList.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new ListUI((JList) jComponent);
    }

    /* access modifiers changed from: protected */
    public void paintCell(Graphics graphics, int i, Rectangle rectangle, ListCellRenderer listCellRenderer, ListModel listModel, ListSelectionModel listSelectionModel, int i2) {
        Object elementAt = listModel.getElementAt(i);
        boolean z = this.list.hasFocus() && i == i2;
        boolean isSelectedIndex = listSelectionModel.isSelectedIndex(i);
        Component listCellRendererComponent = listCellRenderer.getListCellRendererComponent(this.list, elementAt, i, isSelectedIndex, z);
        IComponentManager e = ComponentManager.getCssHolder(listCellRendererComponent);
        if (e != null) {
            int i3 = 0;
            if (isSelectedIndex) {
                i3 = 2;
            }
            if (z) {
                i3 |= 128;
            }
            e.mo13063q(130, i3);
        }
        this.rendererPane.paintComponent(graphics, listCellRendererComponent, this.list, rectangle.x, rectangle.y, rectangle.width, rectangle.height, true);
        e.mo13048Vq();
    }

    public void installUI(JComponent jComponent) {
        ListUI.super.installUI(jComponent);
        ((JList) jComponent).setCellRenderer(new DefaultListCellRenderer());
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        paint(graphics, jComponent);
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13059c(jComponent, ListUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13057b(jComponent, ListUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13052a(jComponent, ListUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.componentManager;
    }
}
