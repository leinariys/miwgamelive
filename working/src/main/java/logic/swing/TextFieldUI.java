package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTextFieldUI;
import javax.swing.text.JTextComponent;
import java.awt.*;

/* renamed from: a.aat  reason: case insensitive filesystem */
/* compiled from: a */
public class TextFieldUI extends BasicTextFieldUI implements IComponentUi {

    private final JTextField editor;
    /* renamed from: Rp */
    private ComponentManager f4120Rp;
    private final C0356Ek bHX = new C0356Ek(this.f4120Rp);

    public TextFieldUI(JTextField jTextField) {
        this.editor = jTextField;
        this.f4120Rp = new C6233aix("textfield", jTextField);
        jTextField.setOpaque(false);
        jTextField.setSelectionColor(new Color(12, 92, 111));
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new TextFieldUI((JTextField) jComponent);
    }

    public void installUI(JComponent jComponent) {
        this.f4120Rp.mo13045Vk();
        TextFieldUI.super.installUI(jComponent);
    }

    /* access modifiers changed from: protected */
    public void installListeners() {
        TextFieldUI.super.installListeners();
        this.editor.addKeyListener(this.bHX);
        this.editor.addMouseListener(this.bHX);
    }

    /* access modifiers changed from: protected */
    public void uninstallListeners() {
        TextFieldUI.super.uninstallListeners();
        this.editor.removeKeyListener(this.bHX);
        this.editor.removeMouseListener(this.bHX);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9087a((IComponentUi) this, graphics, (JTextComponent) jComponent);
        TextFieldUI.super.paint(graphics, jComponent);
    }

    /* access modifiers changed from: protected */
    public void paintBackground(Graphics graphics) {
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return getComponentManager().mo13057b(jComponent, TextFieldUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return getComponentManager().mo13052a(jComponent, TextFieldUI.super.getMaximumSize(jComponent));
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return this.f4120Rp.mo13059c(jComponent, TextFieldUI.super.getPreferredSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f4120Rp;
    }
}
