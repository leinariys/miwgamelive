package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicMenuBarUI;
import java.awt.*;

/* renamed from: a.hN */
/* compiled from: a */
public class MenuBarUI extends BasicMenuBarUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f7872Rp;

    public MenuBarUI(JMenuBar jMenuBar) {
        this.f7872Rp = new ComponentManager("menubar", jMenuBar);
        jMenuBar.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new MenuBarUI((JMenuBar) jComponent);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        paint(graphics, jComponent);
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13059c(jComponent, MenuBarUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13057b(jComponent, MenuBarUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13052a(jComponent, MenuBarUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f7872Rp;
    }
}
