package logic.swing;

import logic.ui.C1276Sr;
import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.PropertiesUiFromCss;
import sun.swing.SwingUtilities2;

import javax.swing.*;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import java.awt.*;
import java.beans.PropertyChangeListener;

/* renamed from: a.el */
/* compiled from: a */
public class C2391el extends BasicInternalFrameTitlePane implements C0650JO, C1276Sr {

    /* renamed from: BM */
    public IComponentManager f7036BM = new ComponentManager("window-title", this);
    /* access modifiers changed from: private */
    /* renamed from: BL */
    public JPanel f7035BL;
    public Icon paletteCloseIcon;
    public int paletteTitleHeight;
    public JLabel titleLabel = new JLabel();

    public C2391el(JInternalFrame jInternalFrame) {
        super(jInternalFrame);
        this.titleLabel.setName("title");
        ComponentManager.getCssHolder(this.titleLabel).setAttribute("name", "title");
        add(this.titleLabel);
        setBorder(new BorderWrapper());
    }

    /* access modifiers changed from: protected */
    public void installDefaults() {
        C2391el.super.installDefaults();
        setFont(UIManager.getFont("InternalFrame.titleFont"));
        this.paletteTitleHeight = UIManager.getInt("InternalFrame.paletteTitleHeight");
        this.paletteCloseIcon = UIManager.getIcon("InternalFrame.paletteCloseIcon");
    }

    /* access modifiers changed from: protected */
    public void createButtons() {
        this.f7035BL = new JPanel();
        this.f7035BL.setLayout(new FlowLayout(2, 0, 0));
        this.f7035BL.setAlignmentY(1.0f);
        this.f7035BL.setName("button-panel");
        ComponentManager.getCssHolder(this.f7035BL).setAttribute("name", "button-panel");
        this.iconButton = new JButton();
        this.iconButton.addActionListener(this.iconifyAction);
        this.iconButton.setFocusable(false);
        this.iconButton.setAlignmentY(1.0f);
        ComponentManager.getCssHolder(this.iconButton).setAttribute("name", "iconify");
        this.iconButton.setName("iconify");
        this.maxButton = new JButton();
        this.maxButton.setFocusable(false);
        this.maxButton.addActionListener(this.maximizeAction);
        this.maxButton.setAlignmentY(1.0f);
        this.closeButton = new JButton();
        this.closeButton.setFocusable(false);
        this.closeButton.setAlignmentY(1.0f);
        this.closeButton.addActionListener(this.closeAction);
        ComponentManager.getCssHolder(this.closeButton).setAttribute("name", "close");
        this.closeButton.setName("close");
        setButtonIcons();
    }

    /* access modifiers changed from: protected */
    public void addSubComponents() {
        add(this.f7035BL);
        this.f7035BL.add(this.iconButton);
        this.f7035BL.add(this.closeButton);
    }

    /* access modifiers changed from: protected */
    public void assembleSystemMenu() {
    }

    /* access modifiers changed from: protected */
    public void addSystemMenuItems(JMenu jMenu) {
    }

    /* access modifiers changed from: protected */
    public void showSystemMenu() {
    }

    /* access modifiers changed from: protected */
    public PropertyChangeListener createPropertyChangeListener() {
        return null;
    }

    /* access modifiers changed from: protected */
    public LayoutManager createLayout() {
        return new C2392a();
    }

    public void paintComponent(Graphics graphics) {
        PropertiesUiFromCss Vp = this.f7036BM.mo13047Vp();
        this.titleLabel.setText(this.frame.getTitle());
        AdapterUiCss.m9093a(graphics, (Component) this, Vp);
    }

    /* renamed from: la */
    public IComponentManager mo2964la() {
        return this.f7036BM;
    }

    public String getElementName() {
        return "window-title";
    }

    /* renamed from: a.el$a */
    class C2392a extends TitlePaneLayout {

        public void addLayoutComponent(String str, Component component) {
        }

        public void removeLayoutComponent(Component component) {
        }

        public Dimension preferredLayoutSize(Container container) {
            return minimumLayoutSize(container);
        }

        public Dimension minimumLayoutSize(Container container) {
            int i;
            int i2;
            int i3;
            int i4 = 30;
            if (C2391el.this.frame.isClosable() && C2391el.this.closeButton != null) {
                i4 = 30 + C2391el.this.closeButton.getPreferredSize().width;
            }
            FontMetrics fontMetrics = C2391el.this.frame.getFontMetrics(C2391el.this.getFont());
            String title = C2391el.this.frame.getTitle();
            if (title != null) {
                i = SwingUtilities2.stringWidth(C2391el.this.frame, fontMetrics, title);
            } else {
                i = 0;
            }
            if (title != null) {
                i2 = title.length();
            } else {
                i2 = 0;
            }
            if (i2 > 2) {
                int stringWidth = SwingUtilities2.stringWidth(C2391el.this.frame, fontMetrics, String.valueOf(C2391el.this.frame.getTitle().substring(0, 2)) + "...");
                if (i >= stringWidth) {
                    i = stringWidth;
                }
                i3 = i4 + i;
            } else {
                i3 = i4 + i;
            }
            int height = fontMetrics.getHeight();
            PropertiesUiFromCss Vp = C2391el.this.f7036BM.mo13047Vp();
            if (Vp != null) {
                height = (int) (((float) height) + Vp.atP());
            }
            int max = Math.max(Math.max(height, -1), C2391el.this.closeButton.getHeight());
            int width = container.getWidth();
            int height2 = container.getHeight();
            if (Vp == null) {
                return new Dimension(0, 0);
            }
            return new Dimension(Vp.atL().mo5756jp((float) width) + Vp.atK().mo5756jp((float) width) + i3, max + Vp.atM().mo5756jp((float) height2) + Vp.atJ().mo5756jp((float) height2));
        }

        public void layoutContainer(Container container) {
            int width = C2391el.this.getWidth();
            int height = C2391el.this.getHeight();
            C2391el.this.closeButton.setVisible(C2391el.this.frame.isClosable());
            C2391el.this.iconButton.setVisible(C2391el.this.frame.isIconifiable());
            C2391el.this.maxButton.setVisible(C2391el.this.frame.isMaximizable());
            int i = C2391el.this.f7035BL.getPreferredSize().width;
            C2391el.this.f7035BL.setBounds(width - i, 0, i, height);
            C2391el.this.titleLabel.setBounds(0, 0, width, height);
        }
    }
}
