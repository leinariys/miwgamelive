package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.IComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTableHeaderUI;
import javax.swing.table.JTableHeader;
import java.awt.*;

/* renamed from: a.EG */
/* compiled from: a */
public class TableHeaderUI extends BasicTableHeaderUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f478Rp;

    private TableHeaderUI(JTableHeader jTableHeader) {
        this.f478Rp = new ComponentManager("table-header", jTableHeader);
        jTableHeader.setOpaque(false);
        jTableHeader.setBackground(new Color(0, 0, 0, 0));
    }

    public static ComponentUI createUI(JComponent jComponent) {
        jComponent.setBorder(new BorderWrapper());
        jComponent.setBackground((Color) null);
        return new TableHeaderUI((JTableHeader) jComponent);
    }

    public void installUI(JComponent jComponent) {
        this.header = (JTableHeader) jComponent;
        this.rendererPane = new C6621aqV("cell");
        this.header.add(this.rendererPane);
        installListeners();
        installKeyboardActions();
    }

    /* renamed from: wz */
    public IComponentManager getComponentManager() {
        return this.f478Rp;
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13059c(jComponent, TableHeaderUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13057b(jComponent, TableHeaderUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13052a(jComponent, TableHeaderUI.super.getMaximumSize(jComponent));
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        paint(graphics, jComponent);
    }
}
