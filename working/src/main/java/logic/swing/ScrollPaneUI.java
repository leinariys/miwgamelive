package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicScrollPaneUI;
import java.awt.*;

/* renamed from: a.gQ */
/* compiled from: a */
public class ScrollPaneUI extends BasicScrollPaneUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f7570Rp;

    public ScrollPaneUI(JScrollPane jScrollPane) {
        this.f7570Rp = new ComponentManager("scrollable", jScrollPane);
        jScrollPane.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new ScrollPaneUI((JScrollPane) jComponent);
    }

    public void installUI(JComponent jComponent) {
        ScrollPaneUI.super.installUI(jComponent);
        JScrollPane jScrollPane = (JScrollPane) jComponent;
        jScrollPane.setViewportBorder(new BorderWrapperAndComponent(jScrollPane.getViewport()));
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        paint(graphics, jComponent);
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13059c(jComponent, ScrollPaneUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13057b(jComponent, ScrollPaneUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13052a(jComponent, ScrollPaneUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f7570Rp;
    }
}
