package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.IBaseUiTegXml;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/* renamed from: a.os */
/* compiled from: a */
public class C3187os extends ComponentAdapter {
    private static final IBaseUiTegXml aPy = IBaseUiTegXml.initBaseUItegXML();

    /* renamed from: Rp */
    private final ComponentManager f8794Rp;

    public C3187os(ComponentManager pvVar) {
        this.f8794Rp = pvVar;
    }

    public void componentShown(ComponentEvent componentEvent) {
        aPy.mo13724cx(this.f8794Rp.mo13049Vr().auf());
    }

    public void componentHidden(ComponentEvent componentEvent) {
        aPy.mo13724cx(this.f8794Rp.mo13049Vr().auh());
    }
}
