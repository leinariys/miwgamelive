package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.PropertiesUiFromCss;
import sun.swing.SwingUtilities2;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicButtonListener;
import javax.swing.plaf.basic.BasicButtonUI;
import java.awt.*;

/* renamed from: a.aPp  reason: case insensitive filesystem */
/* compiled from: a */
public class ButtonUI extends BasicButtonUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f3558Rp;

    public ButtonUI(JButton jButton) {
        jButton.setRolloverEnabled(true);
        jButton.setOpaque(false);
        this.f3558Rp = new C1401UY("button", jButton);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new ButtonUI((JButton) jComponent);
    }

    /* access modifiers changed from: protected */
    public BasicButtonListener createButtonListener(AbstractButton abstractButton) {
        return new C5371aGt(abstractButton, this.f3558Rp);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9084a((IComponentUi) this, graphics, (AbstractButton) jComponent);
        paint(graphics, jComponent);
    }

    /* access modifiers changed from: protected */
    public void paintText(Graphics graphics, JComponent jComponent, Rectangle rectangle, String str) {
        FontMetrics fontMetrics = SwingUtilities2.getFontMetrics(jComponent, graphics);
        int displayedMnemonicIndex = ((AbstractButton) jComponent).getDisplayedMnemonicIndex();
        PropertiesUiFromCss f = PropertiesUiFromCss.m461f(jComponent);
        if (f == null) {
            ButtonUI.super.paintText(graphics, jComponent, rectangle, str);
            return;
        }
        graphics.setColor(f.getColor());
        SwingUtilities2.drawStringUnderlineCharAt(jComponent, graphics, str, displayedMnemonicIndex, rectangle.x + getTextShiftOffset(), rectangle.y + fontMetrics.getAscent() + getTextShiftOffset());
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13059c(jComponent, ButtonUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13057b(jComponent, ButtonUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13052a(jComponent, ButtonUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f3558Rp;
    }
}
