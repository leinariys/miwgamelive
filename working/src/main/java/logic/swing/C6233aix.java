package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.text.JTextComponent;

/* renamed from: a.aix  reason: case insensitive filesystem */
/* compiled from: a */
public final class C6233aix extends ComponentManager {
    public C6233aix(String str, JTextComponent jTextComponent) {
        super(str, jTextComponent);
    }

    /* renamed from: Vi */
    public int mo5877Vi() {
        int Vi = super.mo5877Vi();
        if (!((JTextComponent) getCurrentComponent()).isEditable()) {
            return Vi | 64;
        }
        return Vi;
    }
}
