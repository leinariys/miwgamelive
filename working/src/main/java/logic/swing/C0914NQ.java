package logic.swing;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.NQ */
/* compiled from: a */
public class C0914NQ extends Popup {
    private static final Integer dIb = Integer.MAX_VALUE;
    private final JComponent dIc;
    JPanel dve = new JPanel(new BorderLayout());

    public C0914NQ(JComponent jComponent) {
        this.dIc = jComponent;
    }

    /* renamed from: a */
    public void mo4235a(Component component, Component component2, int i, int i2) {
        this.dve.setVisible(false);
        this.dIc.add(this.dve, dIb);
        this.dve.removeAll();
        this.dve.add(component2, "Center");
        if (component2 instanceof JComponent) {
            ((JComponent) component2).setDoubleBuffered(false);
        }
        this.dve.setSize(this.dve.getPreferredSize());
        Point locationOnScreen = this.dIc.getLocationOnScreen();
        int min = Math.min(i2 - locationOnScreen.y, this.dIc.getHeight() - this.dve.getHeight());
        this.dve.setLocation(Math.min(i - locationOnScreen.x, this.dIc.getWidth() - this.dve.getWidth()), min);
        component2.invalidate();
        this.dve.validate();
        if (component instanceof Container) {
            ((Container) component).setComponentZOrder(this.dve, 0);
        }
    }

    public void show() {
        this.dve.setVisible(true);
    }

    public void hide() {
        Rectangle bounds = this.dve.getBounds();
        this.dIc.remove(this.dve);
        this.dIc.repaint(bounds);
    }
}
