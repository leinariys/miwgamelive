package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicToolBarSeparatorUI;
import java.awt.*;

/* renamed from: a.aEJ */
/* compiled from: a */
public class ToolBarSeparatorUI extends BasicToolBarSeparatorUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f2668Rp;

    public ToolBarSeparatorUI(JComponent jComponent) {
        this.f2668Rp = new ComponentManager("toolbarseparator", jComponent);
        jComponent.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new ToolBarSeparatorUI(jComponent);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        paint(graphics, jComponent);
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13059c(jComponent, ToolBarSeparatorUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13057b(jComponent, ToolBarSeparatorUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13052a(jComponent, ToolBarSeparatorUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f2668Rp;
    }
}
