package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicPasswordFieldUI;
import javax.swing.text.JTextComponent;
import java.awt.*;

/* renamed from: a.auE  reason: case insensitive filesystem */
/* compiled from: a */
public class PasswordFieldUI extends BasicPasswordFieldUI implements IComponentUi {

    private final JPasswordField gEj;
    /* renamed from: Rp */
    private ComponentManager f5360Rp;
    private final C0356Ek bHX = new C0356Ek(this.f5360Rp);

    public PasswordFieldUI(JPasswordField jPasswordField) {
        this.gEj = jPasswordField;
        this.f5360Rp = new C6233aix("textfield", jPasswordField);
        jPasswordField.setSelectionColor(new Color(12, 92, 111));
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new PasswordFieldUI((JPasswordField) jComponent);
    }

    /* access modifiers changed from: protected */
    public void installListeners() {
        PasswordFieldUI.super.installListeners();
        this.gEj.addKeyListener(this.bHX);
        this.gEj.addMouseListener(this.bHX);
    }

    /* access modifiers changed from: protected */
    public void uninstallListeners() {
        PasswordFieldUI.super.uninstallListeners();
        this.gEj.removeKeyListener(this.bHX);
        this.gEj.removeMouseListener(this.bHX);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9087a((IComponentUi) this, graphics, (JTextComponent) jComponent);
        PasswordFieldUI.super.paint(graphics, jComponent);
    }

    /* access modifiers changed from: protected */
    public void paintBackground(Graphics graphics) {
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return getComponentManager().mo13057b(jComponent, PasswordFieldUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return getComponentManager().mo13052a(jComponent, PasswordFieldUI.super.getMaximumSize(jComponent));
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return this.f5360Rp.mo13059c(jComponent, PasswordFieldUI.super.getPreferredSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f5360Rp;
    }
}
