package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import java.awt.*;
import java.awt.event.MouseEvent;

/* renamed from: a.atH  reason: case insensitive filesystem */
/* compiled from: a */
public class InternalFrameUI extends BasicInternalFrameUI implements IComponentUi {

    private final JInternalFrame hUF;
    /* renamed from: Rp */
    private ComponentManager f5309Rp;

    public InternalFrameUI(JInternalFrame jInternalFrame) {
        super(jInternalFrame);
        this.hUF = jInternalFrame;
        jInternalFrame.getContentPane().setBackground(new Color(0, 0, 0, 0));
        jInternalFrame.setOpaque(false);
        this.f5309Rp = new ComponentManager("window", jInternalFrame);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new InternalFrameUI((JInternalFrame) jComponent);
    }

    /* access modifiers changed from: protected */
    public void installListeners() {
        InternalFrameUI.super.installListeners();
        this.hUF.addComponentListener(new C3187os(this.f5309Rp));
    }

    /* access modifiers changed from: protected */
    public void uninstallListeners() {
        InternalFrameUI.super.uninstallListeners();
        this.hUF.removeComponentListener(new C3187os(this.f5309Rp));
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        paint(graphics, jComponent);
    }

    /* access modifiers changed from: protected */
    public void installKeyboardActions() {
        InternalFrameUI.super.installKeyboardActions();
        ActionMap uIActionMap = SwingUtilities.getUIActionMap(this.frame);
        if (uIActionMap != null) {
            uIActionMap.remove("showSystemMenu");
        }
    }

    /* access modifiers changed from: protected */
    public void uninstallComponents() {
        this.titlePane = null;
        InternalFrameUI.super.uninstallComponents();
    }

    /* access modifiers changed from: protected */
    public JComponent createNorthPane(JInternalFrame jInternalFrame) {
        return new C2391el(jInternalFrame);
    }

    /* access modifiers changed from: protected */
    public MouseInputAdapter createBorderListener(JInternalFrame jInternalFrame) {
        return new C1993a(this, (C1993a) null);
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f5309Rp;
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        return getComponentManager().mo13059c(jComponent, InternalFrameUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        return getComponentManager().mo13057b(jComponent, InternalFrameUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        return getComponentManager().mo13052a(jComponent, InternalFrameUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: a.atH$a */
    private class C1993a extends BorderListener {

        C1993a(InternalFrameUI ath, C1993a aVar) {

        }

        /* access modifiers changed from: package-private */
        public Rectangle cwp() {
            Icon frameIcon = InternalFrameUI.this.frame.getFrameIcon();
            if (frameIcon != null) {
                return new Rectangle(5, (InternalFrameUI.this.titlePane.getHeight() / 2) - (frameIcon.getIconHeight() / 2), frameIcon.getIconWidth(), frameIcon.getIconHeight());
            }
            return null;
        }

        public void mouseClicked(MouseEvent mouseEvent) {
            if (mouseEvent.getClickCount() != 2 || mouseEvent.getSource() != InternalFrameUI.this.getNorthPane() || !InternalFrameUI.this.frame.isClosable() || InternalFrameUI.this.frame.isIcon()) {
                super.mouseClicked(mouseEvent);
                return;
            }
            Rectangle cwp = cwp();
            if (cwp == null || !cwp.contains(mouseEvent.getX(), mouseEvent.getY())) {
                super.mouseClicked(mouseEvent);
            } else {
                InternalFrameUI.this.frame.doDefaultCloseAction();
            }
        }
    }
}
