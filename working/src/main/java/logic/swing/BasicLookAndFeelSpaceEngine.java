package logic.swing;

import logic.ui.IComponentManager;

import javax.print.attribute.standard.MediaSize;
import javax.swing.*;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.plaf.basic.BasicLookAndFeel;
import java.awt.*;

/**
 * Элементы интерфейса
 */
/* renamed from: a.aTx  reason: case insensitive filesystem */
/* compiled from: a */
public class BasicLookAndFeelSpaceEngine extends BasicLookAndFeel {

    private UIDefaultsUI uiDefaults;

    //определяются шрифты, цвета, горячие клавиши, бордеры и прочие мелочи
    /* access modifiers changed from: protected */
    public void initComponentDefaults(UIDefaults uIDefaults) {
        BasicLookAndFeelSpaceEngine.super.initComponentDefaults(uIDefaults);
        UIDefaults.LazyInputMap lazyInputMap = new UIDefaults.LazyInputMap(new Object[]{"ctrl C", "copy-to-clipboard", "ctrl V", "paste-from-clipboard", "ctrl X", "cut-to-clipboard", "COPY", "copy-to-clipboard", "PASTE", "paste-from-clipboard", "CUT", "cut-to-clipboard", "control INSERT", "copy-to-clipboard", "shift INSERT", "paste-from-clipboard", "shift DELETE", "cut-to-clipboard", "shift LEFT", "selection-backward", "shift KP_LEFT", "selection-backward", "shift RIGHT", "selection-forward", "shift KP_RIGHT", "selection-forward", "ctrl LEFT", "caret-previous-word", "ctrl KP_LEFT", "caret-previous-word", "ctrl RIGHT", "caret-next-word", "ctrl KP_RIGHT", "caret-next-word", "ctrl shift LEFT", "selection-previous-word", "ctrl shift KP_LEFT", "selection-previous-word", "ctrl shift RIGHT", "selection-next-word", "ctrl shift KP_RIGHT", "selection-next-word", "ctrl A", "select-all", "HOME", "caret-begin-line", "END", "caret-end-line", "shift HOME", "selection-begin-line", "shift END", "selection-end-line", "BACK_SPACE", "delete-previous", "shift BACK_SPACE", "delete-previous", "ctrl H", "delete-previous", "DELETE", "delete-next", "ctrl DELETE", "delete-next-word", "ctrl BACK_SPACE", "delete-previous-word", "RIGHT", "caret-forward", "LEFT", "caret-backward", "KP_RIGHT", "caret-forward", "KP_LEFT", "caret-backward", "ENTER", "notify-field-accept", "ctrl BACK_SLASH", "unselect", "control shift O", "toggle-componentOrientation"});
        UIDefaults.LazyInputMap lazyInputMap2 = new UIDefaults.LazyInputMap(new Object[]{"ctrl C", "copy-to-clipboard", "ctrl V", "paste-from-clipboard", "ctrl X", "cut-to-clipboard", "COPY", "copy-to-clipboard", "PASTE", "paste-from-clipboard", "CUT", "cut-to-clipboard", "control INSERT", "copy-to-clipboard", "shift INSERT", "paste-from-clipboard", "shift DELETE", "cut-to-clipboard", "shift LEFT", "selection-backward", "shift KP_LEFT", "selection-backward", "shift RIGHT", "selection-forward", "shift KP_RIGHT", "selection-forward", "ctrl LEFT", "caret-begin-line", "ctrl KP_LEFT", "caret-begin-line", "ctrl RIGHT", "caret-end-line", "ctrl KP_RIGHT", "caret-end-line", "ctrl shift LEFT", "selection-begin-line", "ctrl shift KP_LEFT", "selection-begin-line", "ctrl shift RIGHT", "selection-end-line", "ctrl shift KP_RIGHT", "selection-end-line", "ctrl A", "select-all", "HOME", "caret-begin-line", "END", "caret-end-line", "shift HOME", "selection-begin-line", "shift END", "selection-end-line", "BACK_SPACE", "delete-previous", "shift BACK_SPACE", "delete-previous", "ctrl H", "delete-previous", "DELETE", "delete-next", "RIGHT", "caret-forward", "LEFT", "caret-backward", "KP_RIGHT", "caret-forward", "KP_LEFT", "caret-backward", "ENTER", "notify-field-accept", "ctrl BACK_SLASH", "unselect", "control shift O", "toggle-componentOrientation"});
        UIDefaults.LazyInputMap lazyInputMap3 = new UIDefaults.LazyInputMap(new Object[]{"ctrl C", "copy-to-clipboard", "ctrl V", "paste-from-clipboard", "ctrl X", "cut-to-clipboard", "COPY", "copy-to-clipboard", "PASTE", "paste-from-clipboard", "CUT", "cut-to-clipboard", "control INSERT", "copy-to-clipboard", "shift INSERT", "paste-from-clipboard", "shift DELETE", "cut-to-clipboard", "shift LEFT", "selection-backward", "shift KP_LEFT", "selection-backward", "shift RIGHT", "selection-forward", "shift KP_RIGHT", "selection-forward", "ctrl LEFT", "caret-previous-word", "ctrl KP_LEFT", "caret-previous-word", "ctrl RIGHT", "caret-next-word", "ctrl KP_RIGHT", "caret-next-word", "ctrl shift LEFT", "selection-previous-word", "ctrl shift KP_LEFT", "selection-previous-word", "ctrl shift RIGHT", "selection-next-word", "ctrl shift KP_RIGHT", "selection-next-word", "ctrl A", "select-all", "HOME", "caret-begin-line", "END", "caret-end-line", "shift HOME", "selection-begin-line", "shift END", "selection-end-line", "UP", "caret-up", "KP_UP", "caret-up", "DOWN", "caret-down", "KP_DOWN", "caret-down", "PAGE_UP", "page-up", "PAGE_DOWN", "page-down", "shift PAGE_UP", "selection-page-up", "shift PAGE_DOWN", "selection-page-down", "ctrl shift PAGE_UP", "selection-page-left", "ctrl shift PAGE_DOWN", "selection-page-right", "shift UP", "selection-up", "shift KP_UP", "selection-up", "shift DOWN", "selection-down", "shift KP_DOWN", "selection-down", "ENTER", "insert-break", "BACK_SPACE", "delete-previous", "shift BACK_SPACE", "delete-previous", "ctrl H", "delete-previous", "DELETE", "delete-next", "ctrl DELETE", "delete-next-word", "ctrl BACK_SPACE", "delete-previous-word", "RIGHT", "caret-forward", "LEFT", "caret-backward", "KP_RIGHT", "caret-forward", "KP_LEFT", "caret-backward", "TAB", "insert-tab", "ctrl BACK_SLASH", "unselect", "ctrl HOME", "caret-begin", "ctrl END", "caret-end", "ctrl shift HOME", "selection-begin", "ctrl shift END", "selection-end", "ctrl T", "next-link-action", "ctrl shift T", "previous-link-action", "ctrl SPACE", "activate-link-action", "control shift O", "toggle-componentOrientation"});
        Color color = new Color(0, 0, 0, 0);
        BorderWrapper borderWrapper = new BorderWrapper();

        Object[] objArr = new Object[184];
        objArr[0] = "Button.background";
        objArr[1] = uIDefaults.get("control");
        objArr[2] = "Button.border";
        objArr[3] = borderWrapper;
        objArr[4] = "Button.focusInputMap";
        objArr[5] = new UIDefaults.LazyInputMap(new Object[]{"SPACE", IComponentManager.pressed, "released SPACE", "released", "ENTER", IComponentManager.pressed, "released ENTER", "released"});
        objArr[6] = "Button.foreground";
        objArr[7] = uIDefaults.get("controlText");
        objArr[8] = "Button.margin";
        objArr[9] = new InsetsUIResource(0, 0, 0, 0);
        objArr[10] = "Button.select";
        objArr[11] = uIDefaults.get("controlLightShadow");
        objArr[12] = "CheckBox.border";
        objArr[13] = borderWrapper;
        objArr[14] = "CheckBoxMenuItem.border";
        objArr[15] = borderWrapper;
        objArr[16] = "CheckBoxMenuItem.borderPainted";
        objArr[17] = Boolean.TRUE;
        objArr[18] = "ComboBox.ancestorInputMap";
        objArr[19] = new UIDefaults.LazyInputMap(new Object[]{"ESCAPE", "hidePopup", "PAGE_UP", "pageUpPassThrough", "PAGE_DOWN", "pageDownPassThrough", "HOME", "homePassThrough", "END", "endPassThrough", "ENTER", "enterPressed"});
        objArr[20] = "ComboBox.background";
        objArr[21] = color;
        objArr[22] = "ComboBox.buttonBackground";
        objArr[23] = "";
        objArr[24] = "ComboBox.buttonDarkShadow";
        objArr[25] = "";
        objArr[26] = "ComboBox.buttonHighlight";
        objArr[27] = "";
        objArr[28] = "ComboBox.buttonShadow";
        objArr[29] = "";
        objArr[30] = "ComboBox.disabledBackground";
        objArr[31] = "";
        objArr[32] = "ComboBox.disabledForeground";
        objArr[33] = "";
        objArr[34] = "ComboBox.font";
        objArr[35] = "";
        objArr[36] = "ComboBox.foreground";
        objArr[37] = color;
        objArr[38] = "ComboBox.selectionBackground";
        objArr[39] = "";
        objArr[40] = "ComboBox.selectionForeground";
        objArr[41] = "";
        objArr[42] = "ComboBox.timeFactor";
        objArr[43] = new Long(1000);
        objArr[44] = "DesktopIcon.border";
        objArr[45] = borderWrapper;
        objArr[46] = "EditorPane.border";
        objArr[47] = borderWrapper;
        objArr[48] = "EditorPane.focusInputMap";
        objArr[49] = lazyInputMap3;
        objArr[50] = "FormattedTextField.border";
        objArr[51] = borderWrapper;
        objArr[52] = "InternalFrame.border";
        objArr[53] = borderWrapper;
        objArr[54] = "InternalFrame.closeIcon";
        objArr[56] = "InternalFrame.iconifyIcon";
        objArr[58] = "InternalFrame.maximizeIcon";
        objArr[60] = "InternalFrame.minimizeIcon";
        objArr[62] = "Label.border";
        objArr[63] = borderWrapper;
        objArr[64] = "List.border";
        objArr[65] = borderWrapper;
        objArr[66] = "Menu.border";
        objArr[67] = borderWrapper;
        objArr[68] = "Menu.borderPainted";
        objArr[69] = Boolean.TRUE;
        objArr[70] = "MenuBar.border";
        objArr[71] = borderWrapper;
        objArr[72] = "MenuItem.border";
        objArr[73] = borderWrapper;
        objArr[74] = "MenuItem.borderPainted";
        objArr[75] = Boolean.TRUE;
        objArr[76] = "OptionPane.border";
        objArr[77] = borderWrapper;
        objArr[78] = "Panel.border";
        objArr[79] = borderWrapper;
        objArr[80] = "PasswordField.background";
        objArr[81] = color;
        objArr[82] = "PasswordField.border";
        objArr[83] = borderWrapper;
        objArr[84] = "PasswordField.focusInputMap";
        objArr[85] = lazyInputMap2;
        objArr[86] = "PasswordField.foreground";
        objArr[87] = color;
        objArr[88] = "PopupMenu.border";
        objArr[89] = borderWrapper;
        objArr[90] = "ProgressBar.border";
        objArr[91] = borderWrapper;
        objArr[92] = "RootPane.border";
        objArr[93] = borderWrapper;
        objArr[94] = "RadioButton.border";
        objArr[95] = borderWrapper;
        objArr[96] = "RadioButtonMenuItem.border";
        objArr[97] = borderWrapper;
        objArr[98] = "RadioButtonMenuItem.borderPainted";
        objArr[99] = Boolean.TRUE;
        objArr[100] = "ScrollBar.background";
        objArr[101] = color;
        objArr[102] = "ScrollBar.border";
        objArr[103] = borderWrapper;
        objArr[104] = "ScrollPane.border";
        objArr[105] = borderWrapper;
        objArr[106] = "ScrollPane.viewportBorder";
        objArr[107] = borderWrapper;
        objArr[108] = "ScrollPane.borderPainted";
        objArr[109] = Boolean.TRUE;
        objArr[110] = "Slider.border";
        objArr[111] = borderWrapper;
        objArr[112] = "Slider.borderPainted";
        objArr[113] = Boolean.TRUE;
        objArr[114] = "Spinner.border";
        objArr[115] = borderWrapper;
        objArr[116] = "Spinner.borderPainted";
        objArr[117] = Boolean.TRUE;
        objArr[118] = "Spinner.editorBorderPainted";
        objArr[119] = Boolean.TRUE;
        objArr[120] = "SplitPane.border";
        objArr[121] = borderWrapper;
        objArr[122] = "SplitPane.borderPainted";
        objArr[123] = Boolean.TRUE;
        objArr[124] = "SplitPaneDivider.border";
        objArr[125] = borderWrapper;
        objArr[126] = "SplitPaneDivider.borderPainted";
        objArr[127] = Boolean.TRUE;
        objArr[128] = "Table.border";
        objArr[129] = borderWrapper;
        objArr[130] = "Table.scrollPaneBorder";
        objArr[131] = borderWrapper;
        objArr[132] = "TableHeader.cellBorder";
        objArr[133] = borderWrapper;
        objArr[134] = "TableHeader.focusCellBorder";
        objArr[135] = borderWrapper;
        objArr[136] = "TextArea.border";
        objArr[137] = borderWrapper;
        objArr[138] = "TextArea.focusInputMap";
        objArr[139] = lazyInputMap3;
        objArr[140] = "TextField.border";
        objArr[141] = borderWrapper;
        objArr[142] = "TextField.focusInputMap";
        objArr[143] = lazyInputMap;
        objArr[144] = "TextPane.border";
        objArr[145] = borderWrapper;
        objArr[146] = "TextPane.focusInputMap";
        objArr[147] = lazyInputMap3;
        objArr[148] = "TitledBorder.border";
        objArr[149] = borderWrapper;
        objArr[150] = "ToggleButton.background";
        objArr[151] = uIDefaults.get("control");
        objArr[152] = "ToggleButton.border";
        objArr[153] = borderWrapper;
        objArr[154] = "ToggleButton.borderPainted";
        objArr[155] = Boolean.TRUE;
        objArr[156] = "ToggleButton.focusInputMap";
        objArr[157] = new UIDefaults.LazyInputMap(new Object[]{"SPACE", IComponentManager.pressed, "released SPACE", "released", "ENTER", IComponentManager.pressed, "released ENTER", "released"});
        objArr[158] = "ToggleButton.foreground";
        objArr[159] = uIDefaults.get("controlText");
        objArr[160] = "ToggleButton.margin";
        objArr[161] = new InsetsUIResource(0, 0, 0, 0);
        objArr[162] = "ToggleButton.select";
        objArr[163] = uIDefaults.get("controlLightShadow");
        objArr[164] = "ToolBar.border";
        objArr[165] = borderWrapper;
        objArr[166] = "ToolTip.border";
        objArr[167] = borderWrapper;
        objArr[168] = "Tree.border";
        objArr[169] = borderWrapper;
        objArr[170] = "Tree.selectionBorderColor";
        objArr[171] = borderWrapper;
        objArr[172] = "Tree.dropLineColor";
        objArr[173] = color;
        objArr[174] = "Tree.editorBorder";
        objArr[175] = borderWrapper;
        objArr[176] = "Table.focusSelectedCellHighlightBorder";
        objArr[177] = borderWrapper;
        objArr[178] = "Table.focusCellHighlightBorder";
        objArr[179] = borderWrapper;
        objArr[180] = "Table.focusCellForeground";
        objArr[181] = color;
        objArr[182] = "Table.focusCellBackground";
        objArr[183] = color;
        uIDefaults.putDefaults(objArr);
    }

    /* access modifiers changed from: protected */
    public void initClassDefaults(UIDefaults uIDefaults) {
        BasicLookAndFeelSpaceEngine.super.initClassDefaults(uIDefaults);
        UIManager.getDefaults().put("InternalFrame.iconButtonToolTip", "");
        uIDefaults.putDefaults(new Object[]{

                // Label
                "LabelUI", LabelUI.class.getName(),
                "ToolTipUI", ToolTipUI.class.getName(),

                // Button
                "ButtonUI", ButtonUI.class.getName(),
                "ToggleButtonUI", ToggleButtonUI.class.getName(),
                "CheckBoxUI", CheckBoxUI.class.getName(),
                "RadioButtonUI", MediaSize.NA.class.getName(),

                // Menu
                "MenuBarUI", MenuBarUI.class.getName(),
                "MenuUI", MenuUI.class.getName(),
                "PopupMenuUI", PopupMenuUI.class.getName(),
                //"MenuItemUI", ,
                "CheckBoxMenuItemUI", CheckBoxMenuItemUI.class.getName(),
                "RadioButtonMenuItemUI", RadioButtonMenuItemUI.class.getName(),
                "PopupMenuSeparatorUI", PopupMenuSeparatorUI.class.getName(),

                // Separator
                "SeparatorUI", SeparatorUI.class.getName(),

                // Scroll
                "ScrollBarUI", ScrollBarUI.class.getName(),
                "ScrollPaneUI", ScrollPaneUI.class.getName(),

                // Text
                "TextFieldUI", TextFieldUI.class.getName(),
                "PasswordFieldUI", PasswordFieldUI.class.getName(),
                "FormattedTextFieldUI", FormattedTextFieldUI.class.getName(),
                "TextAreaUI", TextAreaUI.class.getName(),
                "EditorPaneUI", EditorPaneUI.class.getName(),
                "TextPaneUI", TextPaneUI.class.getName(),

                // Toolbar
                "ToolBarUI", ToolBarUI.class.getName(),
                "ToolBarSeparatorUI", ToolBarSeparatorUI.class.getName(),

                // Table
                "TableUI", TableUI.class.getName(),
                "TableHeaderUI", TableHeaderUI.class.getName(),
                // Chooser
                "ColorChooserUI", ColorChooserUI.class.getName(),
                //"FileChooserUI", ,

                // Container
                "PanelUI", PanelUI.class.getName(),
                "ViewportUI", ViewportUI.class.getName(),
                "RootPaneUI", RootPaneUI.class.getName(),
                "TabbedPaneUI", TabbedPaneUI.class.getName(),
                "SplitPaneUI", SplitPaneUI.class.getName(),

                // Complex components
                "ProgressBarUI", ProgressBarUI.class.getName(),
                "SliderUI", SliderUI.class.getName(),
                "SpinnerUI", SpinnerUI.class.getName(),
                "TreeUI", TreeUI.class.getName(),
                "ListUI", ListUI.class.getName(),
                "ComboBoxUI", ComboBoxUI.class.getName(),

                // Desktop pane
                "DesktopPaneUI", DesktopPaneUI.class.getName(),
                "DesktopIconUI", DesktopIconUI.class.getName(),
                "InternalFrameUI", InternalFrameUI.class.getName(),

                // Option pane
                "OptionPaneUI", OptionPaneUI.class.getName(),


        });
    }

    public String getDescription() {
        return "Taikodom Look And Feel";
    }

    public String getID() {
        return "taikodom";
    }

    public String getName() {
        return "Taikodom Look And Feel";
    }

    public boolean isNativeLookAndFeel() {
        return true;
    }

    public boolean isSupportedLookAndFeel() {
        return true;
    }

    public UIDefaults getDefaults() {
        if (this.uiDefaults == null) {
            this.uiDefaults = new UIDefaultsUI(610, 0.75f);
            initClassDefaults(this.uiDefaults);//для «подмены» UI класса
            initSystemColorDefaults(this.uiDefaults);// стандартные цвета различных элементых
            initComponentDefaults(this.uiDefaults);//определяются шрифты, цвета, горячие клавиши, бордеры и прочие мелочи
        }
        return this.uiDefaults;
    }
}
