package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.IComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTreeUI;
import java.awt.*;

/* renamed from: a.aJf  reason: case insensitive filesystem */
/* compiled from: a */
public class TreeUI extends BasicTreeUI implements IComponentUi {

    /* renamed from: BM */
    private IComponentManager f3215BM;
    private IComponentManager iyc;
    private IComponentManager iyd;

    public TreeUI(JTree jTree) {
        jTree.setBorder(new BorderWrapper());
        jTree.setOpaque(false);
        this.f3215BM = new ComponentManager("tree", jTree);
        this.iyc = new ComponentManager("button-expand", new C1805a(jTree));
        this.iyd = new ComponentManager("button-collapse", new C1806b(jTree));
        setHashColor(new Color(12, 92, 111, 255));
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new TreeUI((JTree) jComponent);
    }

    /* renamed from: wz */
    public IComponentManager getComponentManager() {
        return this.f3215BM;
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        return getComponentManager().mo13059c(jComponent, TreeUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        return getComponentManager().mo13057b(jComponent, TreeUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        return getComponentManager().mo13052a(jComponent, TreeUI.super.getMaximumSize(jComponent));
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        TreeUI.super.paint(graphics, jComponent);
    }

    public Icon getExpandedIcon() {
        return this.iyd.mo13047Vp().getIcon();
    }

    public Icon getCollapsedIcon() {
        return this.iyc.mo13047Vp().getIcon();
    }

    /* renamed from: a.aJf$a */
    class C1805a extends JButton {
        private static final long serialVersionUID = -9130234963400596028L;
        private final /* synthetic */ JTree ifq;

        C1805a(JTree jTree) {
            this.ifq = jTree;
        }

        public Container getParent() {
            return this.ifq;
        }
    }

    /* renamed from: a.aJf$b */
    /* compiled from: a */
    class C1806b extends JButton {
        private static final long serialVersionUID = -7782141436422310723L;
        private final /* synthetic */ JTree ifq;

        C1806b(JTree jTree) {
            this.ifq = jTree;
        }

        public Container getParent() {
            return this.ifq;
        }
    }
}
