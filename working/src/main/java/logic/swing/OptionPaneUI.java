package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicOptionPaneUI;
import java.awt.*;

/* renamed from: a.YS */
/* compiled from: a */
public class OptionPaneUI extends BasicOptionPaneUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f2173Rp;

    public OptionPaneUI(JOptionPane jOptionPane) {
        this.f2173Rp = new ComponentManager("dialog", jOptionPane);
        jOptionPane.setBorder(new BorderWrapper());
        jOptionPane.setBackground((Color) null);
        jOptionPane.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new OptionPaneUI((JOptionPane) jComponent);
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f2173Rp;
    }

    public void installUI(JComponent jComponent) {
        OptionPaneUI.super.installUI(jComponent);
        this.f2173Rp.mo13045Vk();
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        return getComponentManager().mo13059c(jComponent, OptionPaneUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        return getComponentManager().mo13057b(jComponent, OptionPaneUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        return getComponentManager().mo13052a(jComponent, OptionPaneUI.super.getMaximumSize(jComponent));
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        OptionPaneUI.super.paint(graphics, jComponent);
        AdapterUiCss.m9099a((ComponentUI) this, graphics, jComponent);
    }
}
