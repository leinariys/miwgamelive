package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.IBaseUiTegXml;

import javax.swing.*;
import javax.swing.plaf.basic.BasicButtonListener;
import java.awt.event.MouseEvent;

/* renamed from: a.aGt  reason: case insensitive filesystem */
/* compiled from: a */
public class C5371aGt extends BasicButtonListener {
    private static final IBaseUiTegXml aPy = IBaseUiTegXml.initBaseUItegXML();

    /* renamed from: Rp */
    private final ComponentManager f2890Rp;

    public C5371aGt(AbstractButton abstractButton, ComponentManager pvVar) {
        super(abstractButton);
        this.f2890Rp = pvVar;
    }

    public void mouseClicked(MouseEvent mouseEvent) {
        C5371aGt.super.mouseClicked(mouseEvent);
        aPy.mo13724cx(this.f2890Rp.mo13049Vr().atX());
    }

    public void mouseEntered(MouseEvent mouseEvent) {
        C5371aGt.super.mouseEntered(mouseEvent);
        aPy.mo13728e(this.f2890Rp.mo13049Vr().atU(), false);
    }

    public void mousePressed(MouseEvent mouseEvent) {
        C5371aGt.super.mousePressed(mouseEvent);
        aPy.mo13724cx(this.f2890Rp.mo13049Vr().atV());
    }

    public void mouseReleased(MouseEvent mouseEvent) {
        C5371aGt.super.mouseReleased(mouseEvent);
        aPy.mo13724cx(this.f2890Rp.mo13049Vr().atW());
    }
}
