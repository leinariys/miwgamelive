package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.IComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicSliderUI;
import java.awt.*;

/* renamed from: a.amF  reason: case insensitive filesystem */
/* compiled from: a */
public class SliderUI extends BasicSliderUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f4904Rp;

    public SliderUI(JSlider jSlider) {
        super(jSlider);
        this.f4904Rp = new ComponentManager("slider", jSlider);
        jSlider.setBackground((Color) null);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new SliderUI((JSlider) jComponent);
    }

    /* renamed from: wz */
    public IComponentManager getComponentManager() {
        return this.f4904Rp;
    }

    /* access modifiers changed from: protected */
    public Dimension getThumbSize() {
        return SliderUI.super.getThumbSize();
    }

    /* access modifiers changed from: protected */
    public void calculateTrackRect() {
        SliderUI.super.calculateTrackRect();
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        SliderUI.super.paint(graphics, jComponent);
    }

    public void paintTrack(Graphics graphics) {
        SliderUI.super.paintTrack(graphics);
    }

    public void paintThumb(Graphics graphics) {
        SliderUI.super.paintThumb(graphics);
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        return this.f4904Rp.mo13059c(jComponent, SliderUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        return this.f4904Rp.mo13057b(jComponent, SliderUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        return this.f4904Rp.mo13052a(jComponent, SliderUI.super.getMaximumSize(jComponent));
    }
}
