package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicToolBarUI;
import java.awt.*;

/* renamed from: a.aSo  reason: case insensitive filesystem */
/* compiled from: a */
public class ToolBarUI extends BasicToolBarUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f3776Rp;

    public ToolBarUI(JToolBar jToolBar) {
        this.f3776Rp = new ComponentManager("toolbar", jToolBar);
        jToolBar.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new ToolBarUI((JToolBar) jComponent);
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f3776Rp;
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        return getComponentManager().mo13059c(jComponent, ToolBarUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        return getComponentManager().mo13057b(jComponent, ToolBarUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        return getComponentManager().mo13052a(jComponent, ToolBarUI.super.getMaximumSize(jComponent));
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        ToolBarUI.super.paint(graphics, jComponent);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        ToolBarUI.super.paint(graphics, jComponent);
    }
}
