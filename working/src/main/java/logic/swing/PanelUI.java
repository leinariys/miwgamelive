package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.item.C2867lF;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicPanelUI;
import java.awt.*;

/* renamed from: a.aSf  reason: case insensitive filesystem */
/* compiled from: a */
public class PanelUI extends BasicPanelUI implements IComponentUi {

    /* renamed from: Rp */
    public ComponentManager f3775Rp;

    public PanelUI() {
    }

    public PanelUI(JPanel jPanel) {
        this.f3775Rp = new ComponentManager("panel", jPanel);
        jPanel.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new PanelUI((JPanel) jComponent);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        PanelUI.super.paint(graphics, jComponent);
    }

    public void installUI(JComponent jComponent) {
        PanelUI.super.installUI(jComponent);
        this.f3775Rp.mo13045Vk();
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        Dimension preferredSize = PanelUI.super.getPreferredSize(jComponent);
        if (preferredSize == null && (jComponent instanceof C2867lF)) {
            Container parent = jComponent.getParent();
            preferredSize = parent != null ? ((C2867lF) jComponent).getPreferredSize(parent.getWidth(), parent.getHeight()) : ((C2867lF) jComponent).getPreferredSize(0, 0);
        }
        return getComponentManager().mo13059c(jComponent, preferredSize);
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        Dimension minimumSize = PanelUI.super.getMinimumSize(jComponent);
        if (minimumSize == null && (jComponent instanceof C2867lF)) {
            Container parent = jComponent.getParent();
            minimumSize = parent != null ? ((C2867lF) jComponent).getMinimumSize(parent.getWidth(), parent.getHeight()) : ((C2867lF) jComponent).getMinimumSize(0, 0);
        }
        return getComponentManager().mo13057b(jComponent, minimumSize);
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13052a(jComponent, PanelUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f3775Rp;
    }

    public boolean contains(JComponent jComponent, int i, int i2) {
        if (!ComponentManager.getCssHolder(jComponent).mo13050Vs()) {
            return false;
        }
        return PanelUI.super.contains(jComponent, i, i2);
    }
}
