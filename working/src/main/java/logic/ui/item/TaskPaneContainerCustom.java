package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.BaseItemFactory;
import logic.ui.C6342alC;
import lombok.extern.slf4j.Slf4j;

import java.awt.*;

/* renamed from: a.aU */
/* compiled from: a */
@Slf4j
public class TaskPaneContainerCustom extends BaseItemFactory<TaskPaneContainer> {

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public TaskPaneContainer creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new TaskPaneContainer();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo361b(C6342alC alc, TaskPaneContainer xxVar, XmlNode agy) {
        super.createLayout(alc, xxVar, agy);
        Integer a = checkValueAttribute(agy, "divisorOrder", log);
        if (a != null) {
            xxVar.mo23037nm(a.intValue());
        }
    }
}
