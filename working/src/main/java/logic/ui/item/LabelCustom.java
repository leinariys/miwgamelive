package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.C6342alC;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.Fk */
/* compiled from: a */
public final class LabelCustom extends BaseItem<JLabel> {
    /* renamed from: j */
    public JLabel creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new aOA();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void createLayout(C6342alC alc, JLabel jLabel, XmlNode agy) {
        super.createLayout(alc, jLabel, agy);
        String attribute = agy.getAttribute("text");
        if (attribute != null) {
            jLabel.setText(attribute);
        }
        XmlNode c = agy.findNodeChild(0, "text", (String) null, (String) null);
        if (c != null) {
            jLabel.setText(c.getText());
        }
        String attribute2 = agy.getAttribute("horizontalTextPosition");
        if (attribute2 != null) {
            String lowerCase = attribute2.toLowerCase();
            if (ButtonCustomAbstract.biH.containsKey(lowerCase)) {
                jLabel.setHorizontalTextPosition(ButtonCustomAbstract.biH.get(lowerCase));
            }
        }
        String attribute3 = agy.getAttribute("verticalTextPosition");
        if (attribute3 != null) {
            String lowerCase2 = attribute3.toLowerCase();
            if (ButtonCustomAbstract.biI.containsKey(lowerCase2)) {
                jLabel.setVerticalTextPosition(ButtonCustomAbstract.biI.get(lowerCase2));
            }
        }
    }
}
