package logic.ui.item;

import logic.ui.C3810vI;
import logic.ui.PropertiesUiFromCss;
import logic.ui.aRU;

import javax.swing.plaf.ComponentUI;
import java.awt.*;

/* renamed from: a.aRM */
/* compiled from: a */
public class GBox extends C2867lF {

    public int cols = 1;

    /* access modifiers changed from: protected */
    /* renamed from: aI */
    public Component mo11121aI(int i, int i2) {
        int i3 = (this.cols * i2) + i;
        if (i3 < getComponentCount()) {
            return getComponent(i3);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Dimension mo11122b(int i, int i2) {
        int componentCount = getComponentCount();
        int rows = getRows();
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i3 < rows) {
            int i6 = 0;
            int i7 = 0;
            for (int i8 = 0; i8 < this.cols; i8++) {
                Component aI = mo11121aI(i8, i3);
                if (aI != null && aI.isVisible()) {
                    Dimension d = mo20189d(aI, i / componentCount, i2);
                    i7 += d.width;
                    i6 = Math.max(i6, d.height);
                }
            }
            i3++;
            i4 = i6 + i4;
            i5 = Math.max(i5, i7);
        }
        return new Dimension(i5, i4);
    }

    private int getRows() {
        int componentCount = getComponentCount();
        return (componentCount % this.cols > 0 ? 1 : 0) + (componentCount / this.cols);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Dimension mo11120a(int i, int i2) {
        int componentCount = getComponentCount();
        int rows = getRows();
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i3 < rows) {
            int i6 = 0;
            int i7 = 0;
            for (int i8 = 0; i8 < this.cols; i8++) {
                Component aI = mo11121aI(i8, i3);
                if (aI != null && aI.isVisible()) {
                    Dimension c = mo20188c(aI, i / componentCount, i2);
                    i7 += c.width;
                    i6 = Math.max(i6, c.height);
                }
            }
            i3++;
            i4 = i6 + i4;
            i5 = Math.max(i5, i7);
        }
        return new Dimension(i5, i4);
    }

    public void layout() {
        int i;
        int i2;
        int i3;
        PropertiesUiFromCss a = PropertiesUiFromCss.m456a((ComponentUI) getUI());
        aRU aty = a.aty();
        C3810vI atR = a.atR();
        Insets amu = amu();
        Dimension size = getSize();
        Dimension dimension = new Dimension((size.width - amu.right) - amu.left, (size.height - amu.top) - amu.bottom);
        int rows = getRows();
        int columns = getColumns();
        C2868a aVar = new C2868a(dimension, columns, rows);
        Dimension dimension2 = new Dimension(0, 0);
        for (int i4 = 0; i4 < rows; i4++) {
            for (int i5 = 0; i5 < columns; i5++) {
                Component aI = mo11121aI(i5, i4);
                if (aI == null || !aI.isVisible()) {
                    aVar.azY[i5][i4] = dimension2;
                } else {
                    aVar.mo20196a(i5, i4, dimension, aI);
                }
            }
        }
        aVar.mo20195Mq();
        float f = aVar.azW.ghL - aVar.azW.ghM;
        float f2 = aVar.azW.ghL / ((float) columns);
        for (int i6 = 0; i6 < columns && f > 0.0f; i6++) {
            if (i6 < aVar.azW.ghH.length) {
                int i7 = aVar.azW.ghH[i6];
                aVar.azW.ghH[i6] = (int) Math.max((float) aVar.azW.ghH[i6], f2);
                f -= (float) (aVar.azW.ghH[i6] - i7);
            }
        }
        float f3 = aVar.azX.ghL - aVar.azX.ghM;
        float f4 = aVar.azX.ghL / ((float) columns);
        for (int i8 = 0; i8 < rows && f3 > 0.0f; i8++) {
            if (i8 < aVar.azX.ghH.length) {
                int i9 = aVar.azX.ghH[i8];
                aVar.azX.ghH[i8] = (int) Math.max((float) aVar.azX.ghH[i8], f4);
                f3 -= (float) (aVar.azX.ghH[i8] - i9);
            }
        }
        int i10 = 0;
        int i11 = amu.top;
        while (i10 < rows) {
            int i12 = amu.left;
            int i13 = 0;
            while (i13 < columns) {
                Component aI2 = mo11121aI(i13, i10);
                if (aI2 == null) {
                    i3 = i12;
                } else {
                    Dimension a2 = m17750a(aVar.azY[i13][i10], aVar.azW.ghH[i13], aVar.azX.ghH[i10]);
                    Point a3 = mo20182a(aI2, dimension.width, dimension.height);
                    int t = i12 + aty.mo11143t(aVar.azW.ghH[i13], a2.width);
                    int t2 = i11 + atR.mo16934t(aVar.azX.ghH[i10], a2.height);
                    if (aty == aRU.FILL) {
                        i = aVar.azW.ghH[i13];
                    } else {
                        i = a2.width;
                    }
                    if (atR == C3810vI.FILL) {
                        i2 = aVar.azX.ghH[i10];
                    } else {
                        i2 = a2.height;
                    }
                    aI2.setBounds(t + a3.x, a3.y + t2, i, i2);
                    i3 = aVar.azW.ghH[i13] + i12;
                }
                i13++;
                i12 = i3;
            }
            int i14 = i11 + aVar.azX.ghH[i10];
            i10++;
            i11 = i14;
        }
    }

    /* renamed from: a */
    private Dimension m17750a(Dimension dimension, int i, int i2) {
        return new Dimension(Math.min(dimension.width, i), Math.min(dimension.height, i2));
    }

    public int getColumns() {
        return this.cols;
    }

    public void setColumns(int i) {
        this.cols = i;
    }

    public String getElementName() {
        return "gbox";
    }
}
