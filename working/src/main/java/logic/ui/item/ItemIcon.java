package logic.ui.item;

import logic.res.ILoaderImageInterface;
import logic.ui.C1276Sr;
import logic.ui.IBaseUiTegXml;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/* renamed from: a.nI */
/* compiled from: a */
public class ItemIcon extends JPanel implements C1276Sr, Icon {

    /* renamed from: Tb */
    private static final String f8694Tb = "res://imageset_items/";

    /* renamed from: Tc */
    private static final String f8695Tc = "res://data/gui/imageset/imageset_items/";
    private static final String dLA = "ico_fra_medium_medium";
    private static final String dLB = "ico_fra_medium_big";
    private static final String dLC = "ico_fra_medium_red";
    private static final int dLD = 0;
    private static final int dLE = 1;
    private static final String dLx = "ico_fra_medium_enabled";
    private static final String dLy = "ico_fra_medium_over";
    private static final String dLz = "ico_fra_medium_small";

    /* access modifiers changed from: private */
    public int dLK = 0;
    /* renamed from: Td */
    private Image f8696Td;
    /* renamed from: Te */
    private String f8697Te;
    /* renamed from: Th */
    private ILoaderImageInterface f8698Th = IBaseUiTegXml.initBaseUItegXML().adz();
    private Image[] dLF;
    private Image dLG;
    private Image dLH;
    private Image dLI;
    private C3037b dLJ;
    private Object data;
    private boolean disabled;
    private Dimension size;

    public ItemIcon() {
        installListeners();
        m35949xN();
    }

    /* renamed from: xN */
    private void m35949xN() {
        this.dLF = new Image[2];
        this.dLF[0] = this.f8698Th.getImage("res://imageset_items/ico_fra_medium_enabled");
        this.dLF[1] = this.f8698Th.getImage("res://imageset_items/ico_fra_medium_over");
        this.dLG = null;
        this.dLJ = C3037b.NONE;
        this.dLH = this.f8698Th.getImage("res://imageset_items/ico_fra_medium_red");
        this.size = new Dimension();
        this.size.width = this.dLF[1].getWidth(this);
        this.size.height = this.dLF[1].getHeight(this);
    }

    public int getIconHeight() {
        return this.size.height;
    }

    public int getIconWidth() {
        return this.size.width;
    }

    private void installListeners() {
        addMouseListener(new C3036a());
    }

    public void paintIcon(Component component, Graphics graphics, int i, int i2) {
        if (this.dLF != null) {
            int i3 = this.size.width;
            int i4 = this.size.height;
            if (this.f8696Td != null) {
                Graphics graphics2 = graphics;
                graphics2.drawImage(this.f8696Td, i + 3, i2 + 3, (i + i3) - 3, (i2 + i4) - 3, 0, 0, this.f8696Td.getWidth(component), this.f8696Td.getHeight(component), component);
            }
            Graphics graphics3 = graphics;
            int i5 = i;
            int i6 = i2;
            graphics3.drawImage(this.dLF[this.dLK], i5, i6, i + i3, i2 + i4, 0, 0, this.dLF[this.dLK].getWidth(component), this.dLF[this.dLK].getHeight(component), component);
            if (this.dLG != null) {
                Graphics graphics4 = graphics;
                int i7 = i;
                int i8 = i2;
                graphics4.drawImage(this.dLG, i7, i8, i + i3, i2 + i4, 0, 0, this.dLG.getWidth(component), this.dLG.getHeight(component), component);
            }
            if (this.dLH != null && this.disabled) {
                Graphics graphics5 = graphics;
                int i9 = i;
                int i10 = i2;
                graphics5.drawImage(this.dLH, i9, i10, i + i3, i2 + i4, 0, 0, this.dLH.getWidth(component), this.dLH.getHeight(component), component);
            }
            if (this.dLI != null) {
                Graphics graphics6 = graphics;
                int i11 = i;
                int i12 = i2;
                graphics6.drawImage(this.dLI, i11, i12, i + i3, i2 + i4, 0, 0, this.dLI.getWidth(component), this.dLI.getHeight(component), component);
            }
            if (this.f8697Te != null) {
                this.f8697Te.length();
            }
        }
    }

    public void paint(Graphics graphics) {
        paintIcon(this, graphics, 0, 0);
    }

    public void paintAll(Graphics graphics) {
        paintIcon(this, graphics, 0, 0);
    }

    /* renamed from: aw */
    public void mo20664aw(String str) {
        try {
            setImage(this.f8698Th.getImage(f8695Tc + str));
        } catch (Exception e) {
        }
    }

    public void setImage(Image image) {
        this.f8696Td = image;
    }

    /* renamed from: ax */
    public void mo20665ax(String str) {
        this.f8697Te = str;
    }

    public String blM() {
        return this.f8697Te;
    }

    public boolean isDisabled() {
        return this.disabled;
    }

    public void setDisabled(boolean z) {
        this.disabled = z;
    }

    /* renamed from: a */
    public void mo20663a(C3037b bVar) {
        this.dLJ = bVar;
        if (this.dLJ == C3037b.NONE) {
            this.dLG = null;
        } else if (this.dLJ == C3037b.SMALL) {
            this.dLG = this.f8698Th.getImage("res://imageset_items/ico_fra_medium_small");
        } else if (this.dLJ == C3037b.MEDIUM) {
            this.dLG = this.f8698Th.getImage("res://imageset_items/ico_fra_medium_medium");
        } else if (this.dLJ == C3037b.BIG) {
            this.dLG = this.f8698Th.getImage("res://imageset_items/ico_fra_medium_big");
        }
    }

    public C3037b blN() {
        return this.dLJ;
    }

    /* renamed from: bo */
    public void mo20668bo(int i) {
        if (i > 0) {
            mo20665ax(String.valueOf(i));
        } else {
            mo20665ax((String) null);
        }
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object obj) {
        this.data = obj;
    }

    public Dimension getPreferredSize() {
        return this.size;
    }

    public String getElementName() {
        return "item-icon";
    }

    /* renamed from: a.nI$b */
    /* compiled from: a */
    public enum C3037b {
        NONE,
        SMALL,
        MEDIUM,
        BIG
    }

    /* renamed from: a.nI$a */
    class C3036a implements MouseListener {
        C3036a() {
        }

        public void mouseClicked(MouseEvent mouseEvent) {
        }

        public void mouseEntered(MouseEvent mouseEvent) {
            ItemIcon.this.dLK = 1;
        }

        public void mouseExited(MouseEvent mouseEvent) {
            ItemIcon.this.dLK = 0;
        }

        public void mousePressed(MouseEvent mouseEvent) {
        }

        public void mouseReleased(MouseEvent mouseEvent) {
        }
    }
}
