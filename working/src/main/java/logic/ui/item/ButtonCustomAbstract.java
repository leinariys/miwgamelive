package logic.ui.item;

import gnu.trove.TObjectIntHashMap;
import logic.res.XmlNode;
import logic.ui.C6342alC;
import logic.ui.IComponentManager;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.sO */
/* compiled from: a */
public abstract class ButtonCustomAbstract<C extends AbstractButton> extends BaseItem<C> {
    static TObjectIntHashMap<String> biH = new TObjectIntHashMap<>();
    static TObjectIntHashMap<String> biI = new TObjectIntHashMap<>();

    static {
        biH.put("left", 2);
        biH.put("center", 0);
        biH.put("right", 4);
        biH.put("leading", 10);
        biH.put("trailing", 11);
        biI.put("top", 1);
        biI.put("center", 0);
        biI.put("bottom", 3);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public abstract C creatComponentCustom(C6342alC alc, Container container, XmlNode agy);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void createLayout(C6342alC alc, C c, XmlNode agy) {
        super.createLayout(alc, c, agy);
        if ("true".equals(agy.getAttribute(IComponentManager.selected))) {
            c.setSelected(true);
        }
        String attribute = agy.getAttribute("horizontalTextPosition");
        if (attribute != null) {
            String lowerCase = attribute.toLowerCase();
            if (biH.containsKey(lowerCase)) {
                c.setHorizontalTextPosition(biH.get(lowerCase));
            }
        }
        String attribute2 = agy.getAttribute("verticalTextPosition");
        if (attribute2 != null) {
            String lowerCase2 = attribute2.toLowerCase();
            if (biI.containsKey(lowerCase2)) {
                c.setVerticalTextPosition(biI.get(lowerCase2));
            }
        }
        String attribute3 = agy.getAttribute("text");
        if (attribute3 != null) {
            c.setText(attribute3);
        }
        XmlNode c2 = agy.findNodeChild(0, "text", (String) null, (String) null);
        if (c2 != null) {
            c.setText(c2.getText());
        }
        if (!"true".equals(agy.getAttribute("focusable"))) {
            c.setFocusable(false);
        }
    }
}
