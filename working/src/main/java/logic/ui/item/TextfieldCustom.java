package logic.ui.item;

import gnu.trove.TObjectIntHashMap;
import logic.res.XmlNode;
import logic.ui.C6342alC;
import lombok.extern.slf4j.Slf4j;

import java.awt.*;

/* renamed from: a.aJV */
/* compiled from: a */
@Slf4j
public final class TextfieldCustom extends TextComponent<TextField> {
    private static TObjectIntHashMap<String> ihO = new TObjectIntHashMap<>();

    static {
        ihO.put("LEFT", 2);
        ihO.put("CENTER", 0);
        ihO.put("RIGHT", 4);
        ihO.put("LEADING", 10);
        ihO.put("TRAILING", 11);
    }

    /* renamed from: R */
    public TextField creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new TextField();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo7340a(C6342alC alc, TextField ahw, XmlNode agy) {
        super.createLayout(alc, ahw, agy);
        Integer a = checkValueAttribute(agy, "cols", log);
        if (a != null) {
            ahw.setColumns(a.intValue());
        }
        String attribute = agy.getAttribute("horizontalAlignment");
        if (attribute != null) {
            ahw.setHorizontalAlignment(ihO.get(attribute.toUpperCase()));
        }
    }
}
