package logic.ui.item;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: a.g */
/* compiled from: a */
public class FBox extends C2867lF {


    /* renamed from: B */
    private int f7544B;

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Dimension mo11120a(int i, int i2) {
        Insets amu = amu();
        Dimension size = getSize();
        Dimension dimension = new Dimension((size.width - amu.right) - amu.left, (size.height - amu.top) - amu.bottom);
        int componentCount = getComponentCount();
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        for (int i8 = 0; i8 < componentCount; i8++) {
            Component component = getComponent(i8);
            if (component.isVisible()) {
                Dimension c = mo20188c(component, i, i2);
                i7 = Math.max(i7, c.width);
                if (c.width + i6 > dimension.width) {
                    i5 = Math.max(i5, i6);
                    i4 += i3 + this.f7544B;
                    i3 = 0;
                    i6 = 0;
                }
                i6 += c.width + this.f7544B;
                i3 = Math.max(c.height, i3);
            }
        }
        Math.max(i5, i6);
        return new Dimension(i7, i3 + i4);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Dimension mo11122b(int i, int i2) {
        int componentCount = getComponentCount();
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        for (int i7 = 0; i7 < componentCount; i7++) {
            Component component = getComponent(i7);
            if (component.isVisible()) {
                Dimension d = mo20189d(component, i, i2);
                if (d.width + i6 > i) {
                    i5 = Math.max(i5, i6);
                    i4 += i3 + this.f7544B;
                    i3 = 0;
                    i6 = 0;
                }
                i6 += d.width + this.f7544B;
                i3 = Math.max(d.height, i3);
            }
        }
        return new Dimension(Math.max(i5, i6), i3 + i4);
    }

    public void layout() {
        Insets amu = amu();
        Dimension size = getSize();
        Dimension dimension = new Dimension((size.width - amu.right) - amu.left, (size.height - amu.top) - amu.bottom);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        int i = 0;
        int i2 = dimension.width;
        int i3 = 0;
        int i4 = 0;
        int componentCount = getComponentCount();
        for (int i5 = 0; i5 < componentCount; i5++) {
            Component component = getComponent(i5);
            if (component.isVisible()) {
                Dimension d = mo20189d(component, dimension.width, dimension.height);
                if (d.width + i > i2) {
                    arrayList3.add(Integer.valueOf(i3));
                    i4 = Math.max(i4, arrayList2.size());
                    arrayList.add(arrayList2);
                    arrayList2 = new ArrayList();
                    i = 0;
                    i3 = 0;
                }
                i += d.width + this.f7544B;
                i3 = Math.max(i3, d.height);
                arrayList2.add(component);
            }
        }
        if (arrayList2.size() > 0) {
            arrayList.add(arrayList2);
            arrayList3.add(Integer.valueOf(i3));
        }
        int i6 = 0;
        Iterator it = arrayList.iterator();
        while (true) {
            int i7 = i6;
            if (it.hasNext()) {
                int i8 = 0;
                for (Object component2 : (List) it.next()) {
                    Dimension d2 = mo20189d((Component) component2, dimension.width, dimension.height);
                    ((Component) component2).setBounds(amu.left + i8, amu.top + i7, d2.width, d2.height);
                    i8 = d2.width + this.f7544B + i8;
                }
                i6 = ((Integer) arrayList3.remove(0)).intValue() + this.f7544B + i7;
            } else {
                return;
            }
        }
    }

    public int getGap() {
        return this.f7544B;
    }

    public void setGap(int i) {
        this.f7544B = i;
        layout();
    }

    public String getElementName() {
        return "fbox";
    }
}
