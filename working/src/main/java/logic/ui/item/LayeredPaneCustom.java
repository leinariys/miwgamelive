package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.BaseItemFactory;
import logic.ui.C6342alC;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.XP */
/* compiled from: a */
public class LayeredPaneCustom extends BaseItemFactory<JLayeredPane> {
    /* access modifiers changed from: protected */
    /* renamed from: t */
    public JLayeredPane creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new JLayeredPane();
    }
}
