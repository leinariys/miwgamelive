package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.BaseItemFactory;
import logic.ui.C6342alC;

import java.awt.*;
import java.util.StringTokenizer;

/* renamed from: a.aAP */
/* compiled from: a */
public class WindowCustom extends BaseItemFactory<InternalFrame> {

    /* renamed from: jR */
    static final /* synthetic */ boolean f2320jR = (!WindowCustom.class.desiredAssertionStatus());

    /* renamed from: N */
    public InternalFrame creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new InternalFrame();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo361b(C6342alC alc, InternalFrame nxVar, XmlNode agy) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4 = false;
        super.createLayout(alc, nxVar, agy);
        if ("true".equals(agy.getAttribute("transparent"))) {
            nxVar.cpU();
            nxVar.setClosable(false);
            nxVar.setIconifiable(false);
            nxVar.setMaximizable(false);
            nxVar.setResizable(false);
            nxVar.mo20875fV(false);
        } else {
            if ("false".equals(agy.getAttribute("resizable"))) {
                z = false;
            } else {
                z = true;
            }
            nxVar.setResizable(z);
            if ("false".equals(agy.getAttribute("alwaysOnTop"))) {
                z2 = false;
            } else {
                z2 = true;
            }
            nxVar.setAlwaysOnTop(z2);
            if ("false".equals(agy.getAttribute("has-title"))) {
                nxVar.cpU();
            } else {
                nxVar.setClosable(!"false".equals(agy.getAttribute("closable")));
                nxVar.setIconifiable("true".equals(agy.getAttribute("iconifiable")));
                nxVar.setMaximizable("true".equals(agy.getAttribute("maximizable")));
                if ("false".equals(agy.getAttribute("movable"))) {
                    z3 = false;
                } else {
                    z3 = true;
                }
                nxVar.mo20875fV(z3);
                if (!"false".equals(agy.getAttribute("focusable"))) {
                    z4 = true;
                }
                nxVar.setFocusable(z4);
                String attribute = agy.getAttribute("title");
                if (attribute != null) {
                    nxVar.setTitle(attribute);
                }
            }
        }
        if ("true".equals(agy.getAttribute("modal"))) {
            nxVar.setModal(true);
        }
        String attribute2 = agy.getAttribute("size");
        if (attribute2 != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(attribute2, ",");
            if (f2320jR || stringTokenizer.countTokens() == 2) {
                nxVar.setSize(Integer.parseInt(stringTokenizer.nextToken().trim()), Integer.parseInt(stringTokenizer.nextToken().trim()));
                return;
            }
            throw new AssertionError();
        }
    }
}
