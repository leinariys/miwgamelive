package logic.ui.item;

import javax.swing.*;
import javax.swing.text.Document;
import javax.swing.text.html.StyleSheet;
import java.net.URL;

/* renamed from: a.aFp  reason: case insensitive filesystem */
/* compiled from: a */
public class EditorPane extends JEditorPane implements C1513WG {

    private C1997av hJn;

    public EditorPane() {
        this(true);
    }

    public EditorPane(boolean z) {
        this.hJn = new C1997av();
        setEditorKit(this.hJn);
        setContentType("text/html");
        if (z) {
            mo8862e(EditorPane.class.getResource("default.css"));
        }
        setEditable(false);
    }

    /* renamed from: e */
    public void mo8862e(URL url) {
        try {
            this.hJn.getStyleSheet().importStyleSheet(url);
        } catch (Exception e) {
            if (!url.getFile().equals("default.css")) {
                mo8862e(EditorPane.class.getResource("default.css"));
            } else {
                e.printStackTrace();
            }
        }
    }

    public StyleSheet getStyleSheet() {
        return this.hJn.getStyleSheet();
    }

    public void setStyleSheet(StyleSheet styleSheet) {
        if (styleSheet == null) {
            this.hJn.setStyleSheet((StyleSheet) null);
            return;
        }
        Document document = getDocument();
        this.hJn = new C1997av();
        this.hJn.setStyleSheet(styleSheet);
        setEditorKit(this.hJn);
        setDocument(document);
    }

    public void setText(String str) {
        if (!str.startsWith("<html>")) {
            str = "<html><div>" + str + "</div></html>";
        }
        EditorPane.super.setText(str);
    }

    public String getElementName() {
        return "html";
    }

    /* renamed from: h */
    public void mo44h(int i) {
    }
}
