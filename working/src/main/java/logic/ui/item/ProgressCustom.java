package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.C6342alC;
import lombok.extern.slf4j.Slf4j;

import java.awt.*;

/* renamed from: a.arp  reason: case insensitive filesystem */
/* compiled from: a */
@Slf4j
public class ProgressCustom extends BaseItem<Progress> {

    /* renamed from: J */
    public Progress creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new Progress();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void createLayout(C6342alC alc, Progress bnVar, XmlNode agy) {
        super.createLayout(alc, bnVar, agy);
        Integer a = checkValueAttribute(agy, "min", log);
        if (a != null) {
            bnVar.setMinimum(a.intValue());
        }
        Integer a2 = checkValueAttribute(agy, "max", log);
        if (a2 != null) {
            bnVar.setMaximum(a2.intValue());
        }
        Integer a3 = checkValueAttribute(agy, "value", log);
        if (a3 != null) {
            bnVar.setValue(a3.intValue());
        }
        if ("true".equals(agy.getAttribute("segmented"))) {
            bnVar.mo17417w(true);
            Integer a4 = checkValueAttribute(agy, "cell-distance", log);
            if (a4 != null) {
                bnVar.mo17402W(a4.intValue());
            }
            Integer a5 = checkValueAttribute(agy, "num-cells", log);
            if (a5 != null) {
                bnVar.mo17401V(a5.intValue());
                bnVar.mo17418x(false);
            }
            Integer a6 = checkValueAttribute(agy, "cell-width", log);
            if (a6 != null) {
                bnVar.mo17400U(a6.intValue());
                bnVar.mo17418x(true);
            }
        }
    }
}
