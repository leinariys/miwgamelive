package logic.ui.item;

import logic.ui.C1276Sr;

import javax.swing.*;
import javax.swing.border.Border;

/* renamed from: a.bn */
/* compiled from: a */
public class Progress extends JProgressBar implements C1276Sr {

    private int cellWidth = 10;

    /* renamed from: nU */
    private C2065a f5894nU;

    /* renamed from: nV */
    private transient float f5895nV;

    /* renamed from: nW */
    private boolean f5896nW = true;

    /* renamed from: nX */
    private int f5897nX = 10;

    /* renamed from: nY */
    private int f5898nY = 5;

    /* renamed from: nZ */
    private boolean f5899nZ = false;

    public Progress() {
        setBorder((Border) null);
        setStringPainted(true);
    }

    /* renamed from: E */
    public void mo17398E(float f) {
        Progress.super.setMaximum(Math.round(f));
    }

    /* renamed from: F */
    public void mo17399F(float f) {
        Progress.super.setMinimum(Math.round(f));
    }

    public String getElementName() {
        return "progress";
    }

    public String getString() {
        if (this.f5894nU == null) {
            return Progress.super.getString();
        }
        try {
            return this.f5894nU.getText();
        } catch (RuntimeException e) {
            e.printStackTrace();
            return Progress.super.getString();
        }
    }

    public int getValue() {
        if (this.f5894nU == null) {
            return Progress.super.getValue();
        }
        try {
            float value = this.f5894nU.getValue();
            if (this.f5895nV != value) {
                repaint(100);
                this.f5895nV = value;
            }
            return (int) this.f5895nV;
        } catch (RuntimeException e) {
            e.printStackTrace();
            return Progress.super.getValue();
        }
    }

    public void setValue(float f) {
        Progress.super.setValue(Math.round(f));
    }

    public void setValue(int i) {
        setValue((float) i);
    }

    public int getMinimum() {
        if (this.f5894nU == null) {
            return Progress.super.getMinimum();
        }
        try {
            return (int) this.f5894nU.getMinimum();
        } catch (RuntimeException e) {
            e.printStackTrace();
            return Progress.super.getMinimum();
        }
    }

    public void setMinimum(int i) {
        mo17399F((float) i);
    }

    public int getMaximum() {
        if (this.f5894nU == null) {
            return Progress.super.getMaximum();
        }
        try {
            return (int) this.f5894nU.getMaximum();
        } catch (RuntimeException e) {
            e.printStackTrace();
            return Progress.super.getMaximum();
        }
    }

    public void setMaximum(int i) {
        mo17398E((float) i);
    }

    /* renamed from: gu */
    public boolean mo17408gu() {
        return this.f5899nZ;
    }

    /* renamed from: w */
    public void mo17417w(boolean z) {
        this.f5899nZ = z;
    }

    /* renamed from: a */
    public void mo17403a(C2065a aVar) {
        this.f5894nU = aVar;
    }

    /* renamed from: gv */
    public boolean mo17409gv() {
        return this.f5896nW;
    }

    /* renamed from: x */
    public void mo17418x(boolean z) {
        this.f5896nW = z;
    }

    /* renamed from: gw */
    public int mo17410gw() {
        return this.cellWidth;
    }

    /* renamed from: U */
    public void mo17400U(int i) {
        this.cellWidth = i;
    }

    /* renamed from: gx */
    public int mo17411gx() {
        return this.f5897nX;
    }

    /* renamed from: V */
    public void mo17401V(int i) {
        this.f5897nX = i;
    }

    /* renamed from: gy */
    public int mo17412gy() {
        return this.f5898nY;
    }

    /* renamed from: W */
    public void mo17402W(int i) {
        this.f5898nY = i;
    }

    /* renamed from: a.bn$a */
    public interface C2065a {
        float getMaximum();

        float getMinimum();

        String getText();

        float getValue();
    }
}
