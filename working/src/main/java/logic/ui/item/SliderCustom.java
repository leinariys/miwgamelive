package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.C6342alC;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.afQ  reason: case insensitive filesystem */
/* compiled from: a */
@Slf4j
public class SliderCustom extends BaseItem<JSlider> {

    /* access modifiers changed from: protected */
    /* renamed from: x */
    public JSlider creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new JSlider();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void createLayout(C6342alC alc, JSlider jSlider, XmlNode agy) {
        int i = 1;
        super.createLayout(alc, jSlider, agy);
        Integer a = checkValueAttribute(agy, "min", log);
        if (a != null) {
            jSlider.setMinimum(a.intValue());
        }
        Integer a2 = checkValueAttribute(agy, "max", log);
        if (a2 != null) {
            jSlider.setMaximum(a2.intValue());
        }
        Integer a3 = checkValueAttribute(agy, "value", log);
        if (a3 != null) {
            jSlider.setValue(a3.intValue());
        }
        Integer a4 = checkValueAttribute(agy, "major-ticks", log);
        if (a4 != null) {
            jSlider.setMajorTickSpacing(a4.intValue());
        }
        Integer a5 = checkValueAttribute(agy, "minor-ticks", log);
        if (a5 != null) {
            jSlider.setMinorTickSpacing(a5.intValue());
        }
        if ("true".equals(agy.getAttribute("snap-to-ticks"))) {
            jSlider.setSnapToTicks(true);
        }
        if ("true".equals(agy.getAttribute("show-ticks"))) {
            jSlider.setPaintTicks(true);
        }
        if (!"vertical".equals(agy.getAttribute("orientation"))) {
            i = 0;
        }
        jSlider.setOrientation(i);
    }
}
