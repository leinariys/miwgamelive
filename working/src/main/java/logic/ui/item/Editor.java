package logic.ui.item;

import logic.ui.PropertiesUiFromCss;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/* renamed from: a.L */
/* compiled from: a */
public class Editor extends JEditorPane implements C1513WG {


    /* renamed from: dO */
    private KeyAdapter f1015dO;

    /* renamed from: a */
    private static String m6646a(Editor l, String str) {
        int i;
        boolean z;
        if (str == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        Font font = PropertiesUiFromCss.m461f(l).getFont();
        Color color = PropertiesUiFromCss.m461f(l).getColor();
        sb.append("<style type=\"text/css\">body { font-size: " + font.getSize() + "px; font-family: " + font.getFamily() + "; color:rgb(" + color.getRed() + "," + color.getGreen() + "," + color.getBlue() + "); }</style><p>");
        char[] charArray = str.toCharArray();
        int i2 = 0;
        boolean z2 = true;
        while (true) {
            if (i2 >= charArray.length) {
                break;
            }
            char c = charArray[i2];
            if (c == '=') {
                i = i2 + 1;
                if (i >= charArray.length) {
                    sb.append(c);
                    break;
                } else if (charArray[i] == '=') {
                    if (z2) {
                        sb.append("<br><br><b>");
                    } else {
                        sb.append("</b>");
                        sb.append("<br>");
                    }
                    if (z2) {
                        z = false;
                    } else {
                        z = true;
                    }
                    z2 = z;
                }
            } else {
                sb.append(c);
                i = i2;
            }
            i2 = i + 1;
        }
        sb.append("<br><br>");
        sb.append("</p>");
        sb.append("</html>");
        return sb.toString();
    }

    /* renamed from: c */
    public void mo3695c(String str) {
        Editor.super.setText(m6646a(this, str));
    }

    public void setText(String str) {
        if ("text/html".equals(getContentType())) {
            mo3695c(str);
        } else {
            Editor.super.setText(str);
        }
    }

    /* renamed from: d */
    public void mo3696d(String str) {
        if (str != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("<html>");
            Font font = PropertiesUiFromCss.m461f(this).getFont();
            Color color = PropertiesUiFromCss.m461f(this).getColor();
            sb.append("<font style=\"color:rgb(" + color.getRed() + "," + color.getGreen() + "," + color.getBlue() + "); size:" + font.getSize() + "px;\" face='" + font.getFamily() + "'>");
            sb.append(str);
            sb.append("<br><br>");
            sb.append("</font>");
            sb.append("</html>");
            Editor.super.setText(sb.toString());
        }
    }

    /* renamed from: h */
    public void mo44h(int i) {
        if (this.f1015dO != null) {
            removeKeyListener(this.f1015dO);
        } else {
            this.f1015dO = new C0771a(i);
        }
        addKeyListener(this.f1015dO);
    }

    public String getElementName() {
        return "editor";
    }

    /* renamed from: a.L$a */
    class C0771a extends KeyAdapter {
        private final /* synthetic */ int bbg;

        C0771a(int i) {
            this.bbg = i;
        }

        public void keyTyped(KeyEvent keyEvent) {
            if ((Editor.this.getText().length() ^ this.bbg) == 0) {
                keyEvent.consume();
            }
        }
    }
}
