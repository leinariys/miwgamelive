package logic.ui.item;

import gnu.trove.TObjectIntHashMap;
import logic.res.XmlNode;
import logic.ui.BaseItemFactory;
import logic.ui.C6342alC;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.util.ArrayList;

/* renamed from: a.ue */
/* compiled from: a */
public class TableCustom extends BaseItemFactory<Table> {
    private static TObjectIntHashMap<String> modes = new TObjectIntHashMap<>();

    static {
        modes.put("ALL_COLUMNS", 4);
        modes.put("LAST_COLUMN", 3);
        modes.put("NEXT_COLUMN", 1);
        modes.put("OFF", 0);
        modes.put("SUBSEQUENT_COLUMNS", 2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: l */
    public Table creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new Table(alc, agy);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void creatAllComponentCustom(C6342alC alc, Table pzVar, XmlNode agy) {
        mo1631a(alc, (JComponent) pzVar, agy);
        boolean z = false;
        for (XmlNode next : agy.getListChildrenTag()) {
            if (!z && "header".equals(next.getTagName())) {
                pzVar.mo21259d(m40080b(next));
                z = true;
            }
        }
        if (!z) {
            pzVar.setTableHeader((JTableHeader) null);
        }
    }

    /* renamed from: b */
    private Object[] m40080b(XmlNode agy) {
        ArrayList arrayList = new ArrayList();
        for (XmlNode next : agy.getListChildrenTag()) {
            if ("title".equals(next.getTagName())) {
                arrayList.add(next.getAttribute("text"));
            }
        }
        return arrayList.toArray();
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo361b(C6342alC alc, Table pzVar, XmlNode agy) {
        super.createLayout(alc, pzVar, agy);
        String attribute = agy.getAttribute("rowHeight");
        if (attribute != null) {
            pzVar.setRowHeight(Integer.parseInt(attribute));
        }
        String attribute2 = agy.getAttribute("autoResizeMode");
        if (attribute2 != null) {
            pzVar.setAutoResizeMode(modes.get(attribute2.replace('-', '_').toUpperCase()));
        }
        if ("true".equals(agy.getAttribute("singleLine"))) {
            pzVar.setSelectionMode(0);
            pzVar.setRowSelectionAllowed(true);
            pzVar.setColumnSelectionAllowed(false);
        }
    }

    /* renamed from: a.ue$a */
    public static final class C3758a extends BaseItem<JLabel> {
        /* access modifiers changed from: protected */
        /* renamed from: j */
        public JLabel creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
            return new JLabel();
        }
    }
}
