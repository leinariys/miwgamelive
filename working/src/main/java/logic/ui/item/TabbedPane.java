package logic.ui.item;

import logic.swing.BorderWrapper;
import logic.ui.C2698il;
import logic.ui.Panel;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.QB */
/* compiled from: a */
public class TabbedPane extends JTabbedPane implements C2698il {

    private C2699a bJg = new C2699a(this);

    public TabbedPane() {
        setBorder(new BorderWrapper());
        setBackground((Color) null);
    }

    public Dimension getMinimumSize() {
        return new Dimension(0, 0);
    }

    public Dimension getMaximumSize() {
        return new Dimension(800, 800);
    }

    public Dimension getPreferredSize() {
        return new Dimension(200, 200);
    }

    public void destroy() {
        if (getParent() != null) {
            getParent().remove(this);
        }
    }

    /* renamed from: cb */
    public JButton mo4913cb(String str) {
        JButton cb;
        for (Component jButton : getComponents()) {
            if ((jButton instanceof JButton) && str.equals(jButton.getName())) {
                return (JButton) jButton;
            }
            if ((jButton instanceof C2698il) && (cb = ((C2698il) jButton).mo4913cb(str)) != null) {
                return cb;
            }
        }
        return null;
    }

    /* renamed from: cd */
    public Component mo4915cd(String str) {
        Component cd;
        for (Component ilVar : getComponents()) {
            if (str.equals(ilVar.getName())) {
                return ilVar;
            }
            if ((ilVar instanceof C2698il) && (cd = ((C2698il) ilVar).mo4915cd(str)) != null) {
                return cd;
            }
        }
        return null;
    }

    /* renamed from: cf */
    public JLabel mo4917cf(String str) {
        JLabel cf;
        for (Component jLabel : getComponents()) {
            if ((jLabel instanceof JLabel) && str.equals(jLabel.getName())) {
                return (JLabel) jLabel;
            }
            if ((jLabel instanceof C2698il) && (cf = ((C2698il) jLabel).mo4917cf(str)) != null) {
                return cf;
            }
        }
        return null;
    }

    /* renamed from: cg */
    public Progress mo4918cg(String str) {
        Progress cg;
        for (Component bnVar : getComponents()) {
            if ((bnVar instanceof Progress) && str.equals(bnVar.getName())) {
                return (Progress) bnVar;
            }
            if ((bnVar instanceof C2698il) && (cg = ((C2698il) bnVar).mo4918cg(str)) != null) {
                return cg;
            }
        }
        return null;
    }

    /* renamed from: ci */
    public TextField mo4920ci(String str) {
        TextField ci;
        for (Component ahw : getComponents()) {
            if ((ahw instanceof TextField) && str.equals(ahw.getName())) {
                return (TextField) ahw;
            }
            if ((ahw instanceof C2698il) && (ci = ((C2698il) ahw).mo4920ci(str)) != null) {
                return ci;
            }
        }
        return null;
    }

    /* renamed from: ce */
    public C2698il mo4916ce(String str) {
        for (Component ilVar : getComponents()) {
            if (ilVar instanceof C2698il) {
                if (str.equals(ilVar.getName())) {
                    return (C2698il) ilVar;
                }
                C2698il ce = ((C2698il) ilVar).mo4916ce(str);
                if (ce != null) {
                    return ce;
                }
            }
        }
        return null;
    }

    /* renamed from: ch */
    public Repeater<?> mo4919ch(String str) {
        Repeater<?> ch;
        for (Component ilVar : getComponents()) {
            if ((ilVar instanceof C2698il) && (ch = ((C2698il) ilVar).mo4919ch(str)) != null) {
                return ch;
            }
        }
        return null;
    }

    /* renamed from: cc */
    public JComboBox mo4914cc(String str) {
        JComboBox cc;
        for (Component jComboBox : getComponents()) {
            if ((jComboBox instanceof JComboBox) && str.equals(jComboBox.getName())) {
                return (JComboBox) jComboBox;
            }
            if ((jComboBox instanceof C2698il) && (cc = ((C2698il) jComboBox).mo4914cc(str)) != null) {
                return cc;
            }
        }
        return null;
    }

    public void pack() {
        setSize(getPreferredSize());
        validate();
    }

    public void setEnabled(boolean z) {
        for (Component enabled : getComponents()) {
            enabled.setEnabled(z);
        }
        TabbedPane.super.setEnabled(z);
    }

    public void setFocusable(boolean z) {
        for (Component focusable : getComponents()) {
            focusable.setFocusable(z);
        }
        TabbedPane.super.setFocusable(z);
    }

    public String getElementName() {
        return "tabpane";
    }

    /* renamed from: Kk */
    public void mo4911Kk() {
        this.bJg.mo19774Kk();
    }

    /* renamed from: Kl */
    public void mo4912Kl() {
        this.bJg.mo19775Kl();
    }

    /* renamed from: a.QB$a */
    public static class C1098a extends Panel {

        public C1098a() {
            setLayout(new GridLayout());
            setBorder(new BorderWrapper());
        }

        public String getElementName() {
            return "tab";
        }
    }
}
