package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.C6342alC;
import lombok.extern.slf4j.Slf4j;

import java.awt.*;

/* renamed from: a.aEM */
/* compiled from: a */
@Slf4j
public class PictureCustom extends BaseItem<Picture> {

    /* access modifiers changed from: protected */
    /* renamed from: Q */
    public Picture creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new Picture(alc.getBaseUiTegXml().adz());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void createLayout(C6342alC alc, Picture axm, XmlNode agy) {
        int i = -1;
        super.createLayout(alc, axm, agy);
        Integer a = checkValueAttribute(agy, "width", log);
        Integer valueOf = Integer.valueOf(a == null ? -1 : a.intValue());
        Integer a2 = checkValueAttribute(agy, "height", log);
        if (a2 != null) {
            i = a2.intValue();
        }
        axm.setImageSize(valueOf.intValue(), Integer.valueOf(i).intValue());
        String attribute = agy.getAttribute("src");
        if (attribute != null) {
            axm.mo16825aw(attribute);
        }
        String attribute2 = agy.getAttribute("text");
        if (attribute2 != null) {
            axm.setText(attribute2);
        }
    }
}
