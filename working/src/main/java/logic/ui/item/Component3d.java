package logic.ui.item;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import logic.ui.IBaseUiTegXml;
import logic.ui.Panel;
import org.mozilla.javascript.ScriptRuntime;
import taikodom.render.RenderView;
import taikodom.render.SceneView;
import taikodom.render.StepContext;
import taikodom.render.TexBackedImage;
import taikodom.render.camera.OrbitalCamera;
import taikodom.render.enums.LightType;
import taikodom.render.helpers.RenderTask;
import taikodom.render.scene.RLight;
import taikodom.render.scene.RSceneAreaInfo;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;
import taikodom.render.textures.BaseTexture;
import taikodom.render.textures.DDSLoader;
import taikodom.render.textures.RenderTarget;

import javax.media.opengl.GL;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/* renamed from: a.ay */
/* compiled from: a */
public class Component3d extends Panel {
    private static final int gsT = 50;
    private static final long serialVersionUID = -873433802107588581L;
    /* access modifiers changed from: private */
    public int gsU = getWidth();
    /* access modifiers changed from: private */
    public int gsV = getHeight();
    /* access modifiers changed from: private */
    public BaseTexture gsZ;
    /* access modifiers changed from: private */
    public TexBackedImage gta;
    /* access modifiers changed from: private */
    public RenderTarget renderTarget;
    /* access modifiers changed from: private */
    public SceneView sceneView = new SceneView();
    /* access modifiers changed from: private */
    public Timer timer;
    private boolean aRG = false;
    private long cYd = System.currentTimeMillis();
    private float gsW = 0.033f;
    private float gsX;
    private OrbitalCamera gsY;
    private boolean gtb = false;
    private RenderTask gte = new RenderTask() {
        public void run(RenderView renderView) {
            if (Component3d.this.sceneView != null) {
                Component3d.this.sceneView.getScene().removeAllChildren();
                Component3d.this.sceneView = null;
            }
            if (Component3d.this.renderTarget != null) {
                Component3d.this.renderTarget.releaseReferences(renderView.getDrawContext().getGl());
                Component3d.this.renderTarget = null;
                Component3d.this.gsZ = null;
                Component3d.this.gta = null;
            }
            if (Component3d.this.timer != null) {
                Component3d.this.timer.stop();
                Component3d.this.timer = null;
            }
        }
    };
    private long startTime = System.currentTimeMillis();
    private StepContext stepContext;
    private RenderTask gtc = new RenderTask() {
        public void run(RenderView renderView) {
            Component3d.this.m27301b(renderView);
        }
    };
    private RenderTask gtd = new RenderTask() {
        public void run(RenderView var1) {
            if (Component3d.this.getWidth() >= 0 && Component3d.this.getHeight() >= 0) {
                if (Component3d.this.renderTarget != null) {
                    Component3d.this.renderTarget.releaseReferences(var1.getDrawContext().getGl());
                    Component3d.this.renderTarget = null;
                    Component3d.this.gsZ = null;
                    Component3d.this.gta = null;
                }

                Component3d.this.gsU = Component3d.this.getWidth();
                Component3d.this.gsV = Component3d.this.getHeight();
                Component3d.this.m27303c(var1);
            }
        }
    };

    public Component3d() {
        this.sceneView.getCamera().setAspect(((float) this.gsU) / ((float) this.gsV));
        this.sceneView.setViewport(0, 0, this.gsU, this.gsV);
        this.sceneView.setAllowImpostors(false);
        this.sceneView.setClearColorBuffer(true);
        this.sceneView.setClearDepthBuffer(true);
        this.sceneView.setClearStencilBuffer(true);
        this.sceneView.disable();
        this.stepContext = new StepContext();
    }

    public boolean csl() {
        return this.aRG;
    }

    /* renamed from: gf */
    public void mo16909gf(boolean z) {
        this.aRG = z;
    }

    public void setVisible(boolean z) {
        super.setVisible(z);
        if (z) {
            mo16910kA(this.gsW);
        } else if (this.timer != null) {
            this.timer.stop();
            this.timer = null;
        }
    }

    /* renamed from: kA */
    public void mo16910kA(float f) {
        this.gsW = f;
        if (this.timer != null) {
            this.timer.stop();
        }
        this.timer = new Timer((int) (((float) 2000) * f), new C2014b());
        this.timer.start();
    }

    /* access modifiers changed from: private */
    public void csm() {
        IBaseUiTegXml.initBaseUItegXML().mo13709a(this.gtc);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m27301b(RenderView renderView) {
        if (this.renderTarget == null && getWidth() > 0 && getHeight() > 0) {
            m27303c(renderView);
        }
        GL gl = renderView.getDrawContext().getGl();
        gl.glPushAttrib(DDSLoader.DDSD_LINEARSIZE);
        if (this.sceneView != null && this.sceneView.isEnabled()) {
            long currentTimeMillis = System.currentTimeMillis();
            this.stepContext.reset();
            float f = ((float) (currentTimeMillis - this.cYd)) * 0.001f;
            if (this.gsY != null) {
                this.gsY.setYaw(this.gsY.getYaw() + (this.gsX * f));
                this.gsY.step(this.stepContext);
                this.stepContext.setCamera(this.gsY);
            }
            this.stepContext.setDeltaTime(f);
            this.sceneView.getScene().step(this.stepContext);
            csn();
            this.sceneView.getCamera().setAspect(((float) getWidth()) / ((float) getHeight()));
            this.sceneView.setViewport(0, 0, this.gsU, this.gsV);
            renderView.render(this.sceneView, (double) (((float) (currentTimeMillis - this.startTime)) * 0.001f), f);
            this.cYd = currentTimeMillis;
            gl.glPopAttrib();
            this.aRG = true;
        }
    }

    private void csn() {
        if (this.gsU <= 0 || this.gsV <= 0) {
            this.gsU = getWidth();
            this.gsV = getHeight();
            this.sceneView.getCamera().setAspect(((float) this.gsU) / ((float) this.gsV));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m27303c(RenderView renderView) {
        if (this.renderTarget == null) {
            this.renderTarget = new RenderTarget();
            csn();
            this.renderTarget.create(this.gsU, this.gsV, true, renderView.getDrawContext(), false);
            this.renderTarget.bind(renderView.getDrawContext());
            renderView.getDrawContext().getGl().glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            renderView.getDrawContext().getGl().glClear(DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ);
            this.renderTarget.copyBufferToTexture(renderView.getDrawContext());
            this.renderTarget.unbind(renderView.getDrawContext());
            this.gsZ = this.renderTarget.getTexture();
            this.sceneView.setRenderTarget(this.renderTarget);
            this.sceneView.getSceneViewQuality().setShaderQuality(renderView.getDrawContext().getMaxShaderQuality());
            if (this.gsZ.getTarget() == 34037) {
                this.gta = new TexBackedImage(this.gsU, this.gsV, (float) this.gsZ.getSizeX(), (float) this.gsZ.getSizeY(), this.gsZ);
            } else {
                this.gta = new TexBackedImage(this.gsU, this.gsV, ((float) this.gsU) / ((float) this.gsZ.getSizeX()), ((float) this.gsV) / ((float) this.gsZ.getSizeY()), this.gsZ);
            }
            this.sceneView.enable();
            m27301b(renderView);
        }
    }

    public SceneView getSceneView() {
        return this.sceneView;
    }

    public void destroy() {
        if (this.timer != null) {
            this.timer.stop();
            this.timer = null;
        }
        IBaseUiTegXml.initBaseUItegXML().mo13709a(this.gte);
    }

    public Scene getScene() {
        return this.sceneView.getScene();
    }

    public String getElementName() {
        return "component3d";
    }

    /* access modifiers changed from: protected */
    public void paintComponent(Graphics graphics) {
        Insets insets = getInsets();
        int width = getWidth();
        int height = getHeight();
        if (this.timer != null && !this.timer.isRunning()) {
            this.timer.start();
        }
        if (this.gta != null && this.aRG) {
            graphics.drawImage(this.gta, insets.left, height - insets.bottom, width - insets.right, insets.top, 0, 0, this.gta.getWidth(), this.gta.getHeight(), this);
        }
    }

    /* renamed from: n */
    public void mo16911n(SceneObject sceneObject) {
        if (sceneObject != null) {
            float length = sceneObject.computeLocalSpaceAABB().length();
            float diq = sceneObject.getAABB().diq() / 2.0f;
            this.sceneView.getCamera().setPosition((double) ScriptRuntime.NaN, (double) ScriptRuntime.NaN, this.sceneView.getDistanceForWidthSize((double) length, (float) Math.min(getWidth(), getHeight())) + ((double) diq));
        }
    }

    private void cso() {
        if (getWidth() >= 0 && getHeight() >= 0) {
            if (this.gtb || Math.abs(getWidth() - this.gsU) >= 50 || Math.abs(getHeight() - this.gsV) >= 50) {
                this.gsU = getWidth();
                this.gsV = getHeight();
                this.sceneView.setViewport(0, 0, this.gsU, this.gsV);
                this.sceneView.getCamera().setAspect(((float) this.gsU) / ((float) this.gsV));
                this.sceneView.getCamera().setNearPlane(0.1f);
                this.sceneView.getCamera().setFarPlane(99999.0f);
                IBaseUiTegXml.initBaseUItegXML().mo13709a(this.gtd);
                this.gtb = false;
            }
        }
    }

    public void setSize(int i, int i2) {
        super.setSize(i, i2);
        cso();
    }

    public void setSize(Dimension dimension) {
        setSize(dimension.width, dimension.height);
    }

    public void setBounds(int i, int i2, int i3, int i4) {
        super.setBounds(i, i2, i3, i4);
        cso();
    }

    public void setBounds(Rectangle rectangle) {
        super.setBounds(rectangle);
        cso();
    }

    public void reshape(int i, int i2, int i3, int i4) {
        super.reshape(i, i2, i3, i4);
        cso();
    }

    public void resize(Dimension dimension) {
        super.resize(dimension);
        cso();
    }

    public void resize(int i, int i2) {
        super.resize(i, i2);
        cso();
    }

    /* renamed from: a */
    public void mo16900a(SceneObject sceneObject, float f) {
        m27297a(sceneObject, f, C2013a.X);
    }

    /* renamed from: b */
    public void mo16901b(SceneObject sceneObject, float f) {
        m27297a(sceneObject, f, C2013a.Y);
    }

    /* renamed from: c */
    public void mo16902c(SceneObject sceneObject, float f) {
        m27297a(sceneObject, f, C2013a.Z);
    }

    /* renamed from: a */
    private void m27297a(SceneObject sceneObject, float f, C2013a aVar) {
        if (sceneObject != null) {
            this.gsY = new OrbitalCamera();
            this.gsY.setTarget(sceneObject);
            this.gsY.setAspect(((float) this.gsU) / ((float) this.gsV));
            this.sceneView.setViewport(0, 0, this.gsU, this.gsV);
            double distanceForWidthSize = this.sceneView.getDistanceForWidthSize((double) sceneObject.computeLocalSpaceAABB().length(), (float) Math.min(getWidth(), getHeight()));
            if (aVar == C2013a.X) {
                this.gsY.setDistance(new Vec3d(((double) (sceneObject.getAABB().dio() / 2.0f)) + distanceForWidthSize, ScriptRuntime.NaN, ScriptRuntime.NaN));
            } else if (aVar == C2013a.Y) {
                this.gsY.setDistance(new Vec3d(ScriptRuntime.NaN, distanceForWidthSize + ((double) (sceneObject.getAABB().dip() / 2.0f)), ScriptRuntime.NaN));
            } else {
                this.gsY.setDistance(new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ((double) (sceneObject.getAABB().diq() / 2.0f)) + distanceForWidthSize));
            }
            mo16911n(sceneObject);
            this.sceneView.setCamera(this.gsY);
            this.gsX = f;
        }
    }

    public void csp() {
        RLight rLight = new RLight();
        rLight.setMainLight(true);
        rLight.setType(LightType.DIRECTIONAL_LIGHT);
        getScene().addChild(rLight);
        rLight.setDirection(new Vec3f(0.0f, 0.0f, 1.0f));
        RSceneAreaInfo rSceneAreaInfo = new RSceneAreaInfo();
        rSceneAreaInfo.setFogStart(10000.0f);
        rSceneAreaInfo.setFogEnd(100000.0f);
        rSceneAreaInfo.setAmbientColor(new Color(0.3f, 0.3f, 0.3f, 1.0f));
        rSceneAreaInfo.setFogColor(new Color(0.0f, 0.0f, 0.0f, 0.0f));
        rSceneAreaInfo.setName("Areainfo for a 3D GUI component");
        rSceneAreaInfo.setSize(new Vec3d(10000.0d, 10000.0d, 10000.0d));
    }

    public OrbitalCamera csq() {
        return this.gsY;
    }

    public void csr() {
        this.gtb = true;
        cso();
    }

    /* renamed from: a.ay$a */
    private enum C2013a {
        X,
        Y,
        Z
    }

    /* renamed from: a.ay$b */
    /* compiled from: a */
    class C2014b implements ActionListener {
        C2014b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            Component3d.this.csm();
            Component3d.this.repaint();
            if (!Component3d.this.isShowing()) {
                Component3d.this.timer.stop();
            }
        }
    }
}
