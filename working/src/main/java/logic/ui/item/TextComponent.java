package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.C6342alC;
import lombok.extern.slf4j.Slf4j;

import javax.swing.text.JTextComponent;

/* renamed from: a.ZH */
/* compiled from: a */
@Slf4j
public abstract class TextComponent<T extends JTextComponent> extends BaseItem<T> {

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void createLayout(C6342alC alc, T t, XmlNode agy) {
        Integer a;
        super.createLayout(alc, t, agy);
        if ("false".equalsIgnoreCase(agy.getAttribute("editable"))) {
            t.setEditable(false);
        }
        String attribute = agy.getAttribute("text");
        if (attribute != null) {
            t.setText(attribute);
        }
        XmlNode c = agy.findNodeChild(0, "text", (String) null, (String) null);
        if (c != null) {
            t.setText(c.getText());
        }
        if ((t instanceof C1513WG) && (a = checkValueAttribute(agy, "max-char", log)) != null) {
            ((C1513WG) t).mo44h(a.intValue());
        }
    }
}
