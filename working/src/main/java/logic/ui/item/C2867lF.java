package logic.ui.item;

import logic.swing.C1366Tu;
import logic.ui.C3810vI;
import logic.ui.Panel;
import logic.ui.PropertiesUiFromCss;
import logic.ui.aRU;
import util.Maths;

import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import java.awt.*;
import java.lang.reflect.Array;

/* renamed from: a.lF */
/* compiled from: a */
public abstract class C2867lF extends Panel {

    public boolean bCM;

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract Dimension mo11120a(int i, int i2);

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public abstract Dimension mo11122b(int i, int i2);

    public Dimension preferredLayoutSize(Container container) {
        int i;
        int i2;
        if (container.getParent() != null) {
            Dimension preferredSize = container.getParent().getPreferredSize();
            int i3 = preferredSize.width;
            i = preferredSize.height;
            i2 = i3;
        } else {
            i = 0;
            i2 = 0;
        }
        PropertiesUiFromCss a = PropertiesUiFromCss.m456a((ComponentUI) getUI());
        if (a != null) {
            if (a.atS() != C1366Tu.fEC) {
                i2 = a.atS().mo5756jp((float) i2);
            }
            if (a.atx() != C1366Tu.fEC) {
                i = a.atx().mo5756jp((float) i);
            }
        }
        return getPreferredSize(i2, i);
    }

    public Dimension getPreferredSize(int i, int i2) {
        Dimension amt = amt();
        Dimension minimumSize = getMinimumSize(i, i2);
        PropertiesUiFromCss a = PropertiesUiFromCss.m456a((ComponentUI) getUI());
        if (a == null) {
            return minimumSize;
        }
        int i3 = minimumSize.width;
        int i4 = minimumSize.height;
        if (a.atS() != C1366Tu.fEC) {
            i3 = a.atS().mo5756jp((float) i);
            if (a.atx() != C1366Tu.fEC) {
                i4 = a.atx().mo5756jp((float) i2);
            } else {
                i4 = mo11122b(i3 - amt.width, i2 - amt.height).height + amt.height;
            }
        } else if (a.atx() != C1366Tu.fEC) {
            i4 = a.atx().mo5756jp((float) i2);
            i3 = mo11122b(i - amt.width, i4 - amt.width).width + amt.width;
        }
        Dimension y = m34765y(i, i2);
        return new Dimension(Math.max(Math.min(i3, y.width), minimumSize.width), Math.max(Math.min(i4, y.height), minimumSize.height));
    }

    private Dimension amt() {
        if (getBorder() == null) {
            return new Dimension(0, 0);
        }
        Insets borderInsets = getBorder().getBorderInsets(this);
        return new Dimension(borderInsets.left + borderInsets.right, borderInsets.bottom + borderInsets.top);
    }

    /* renamed from: y */
    private Dimension m34765y(int i, int i2) {
        return new Dimension(10000, 10000);
    }

    public Dimension minimumSize() {
        return getMinimumSize(0, 0);
    }

    public Dimension getMinimumSize(int i, int i2) {
        Dimension amt = amt();
        Dimension a = mo11120a(i - amt.width, i2 - amt.height);
        a.width += amt.width;
        a.height = amt.height + a.height;
        PropertiesUiFromCss a2 = PropertiesUiFromCss.m456a((ComponentUI) getUI());
        if (a2 == null) {
            return a;
        }
        int i3 = a.width;
        int i4 = a.height;
        if (a2.atI() != C1366Tu.fEC) {
            i3 = a2.atI().mo5756jp((float) i);
        }
        if (a2.atH() != C1366Tu.fEC) {
            i4 = a2.atH().mo5756jp((float) i2);
        }
        return new Dimension(i3, i4);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Point mo20182a(Component component, int i, int i2) {
        int i3;
        int i4;
        PropertiesUiFromCss g = PropertiesUiFromCss.m462g(component);
        if (g != null) {
            int jp = g.atz().mo5756jp((float) i);
            i3 = g.atQ().mo5756jp((float) i2);
            i4 = jp;
        } else {
            i3 = 0;
            i4 = 0;
        }
        return new Point(i4, i3);
    }

    /* access modifiers changed from: protected */
    public Insets amu() {
        Border border = getBorder();
        if (border != null) {
            return border.getBorderInsets(this);
        }
        return new Insets(0, 0, 0, 0);
    }

    /* access modifiers changed from: protected */
    public int amv() {
        if (this.bCM) {
            return getComponentCount();
        }
        int i = 0;
        int componentCount = getComponentCount();
        while (true) {
            componentCount--;
            if (componentCount < 0) {
                return i;
            }
            if (getComponent(componentCount).isVisible()) {
                i++;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo20183a(Component component, aRU aru, C3810vI vIVar, int i, int i2, int i3, int i4, int i5, int i6) {
        component.setBounds(aru.mo11143t(i3, i5) + i, vIVar.mo16934t(i4, i6) + i2, aru.mo7766u(i3, i5), vIVar.mo17141u(i4, i6));
    }

    public void setBounds(Rectangle rectangle) {
        Dimension minimumSize = getMinimumSize(0, 0);
        super.setBounds(new Rectangle(rectangle.x, rectangle.y, Math.max(rectangle.width, minimumSize.width), Math.max(rectangle.height, minimumSize.height)));
    }

    public boolean amw() {
        return this.bCM;
    }

    /* renamed from: bB */
    public void mo20187bB(boolean z) {
        this.bCM = z;
    }

    public Dimension preferredSize() {
        Container parent = getParent();
        if (parent != null) {
            return getPreferredSize(parent.getWidth(), parent.getHeight());
        }
        return getPreferredSize(0, 0);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public Dimension m34764b(Component component, int i, int i2) {
        if (component instanceof C2867lF) {
            return ((C2867lF) component).m34765y(i, i2);
        }
        return component.getMaximumSize();
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public Dimension mo20188c(Component component, int i, int i2) {
        if (component instanceof C2867lF) {
            return ((C2867lF) component).getMinimumSize(i, i2);
        }
        return component.getMinimumSize();
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public Dimension mo20189d(Component component, int i, int i2) {
        if (component instanceof C2867lF) {
            return ((C2867lF) component).getPreferredSize(i, i2);
        }
        return component.getPreferredSize();
    }

    /* renamed from: a.lF$d */
    /* compiled from: a */
    public static class C2871d {
        public Dimension iyP;
        public Dimension iyQ;
        public Dimension iyR;

        public C2871d() {
        }
    }

    /* renamed from: a.lF$c */
    /* compiled from: a */
    public static class C2870c {
        public int[] ghH;
        public float[] ghI;
        public float[] ghJ;
        public float[] ghK;
        public float ghL;
        public float ghM;

        public C2870c(float f, int i) {
            this.ghL = f;
            this.ghH = new int[i];
            this.ghI = new float[i];
            this.ghJ = new float[i];
            this.ghK = new float[i];
        }

        /* renamed from: Mq */
        public void mo20199Mq() {
            int i = 0;
            for (float f : this.ghJ) {
                i = (int) (((float) i) + f);
            }
            float f2 = this.ghL - ((float) i);
            int r1 = 5;
            if (f2 > 0.0f) {
                for (int i2 = 0; i2 < r1; i2++) {
                    float f3 = this.ghJ[i2];
                    float round = (float) Math.round(Maths.clamp(f3, Math.min(f3 + f2, this.ghK[i2]), this.ghI[i2]));
                    f2 -= round - f3;
                    this.ghH[i2] = (int) round;
                }
            } else if (f2 <= 0.0f) {
                for (int j = this.ghJ.length - 1; j >= 0; j--) {
                    //while (true) {
                    //r1--;
                    //if (r1 < 0) {
                    //    break;
                    //}
                    float f4 = this.ghJ[j];
                    float round2 = (float) Math.round(Maths.clamp(f4, Math.min(f4 + f2, this.ghK[j]), this.ghI[j]));
                    f2 -= round2 - f4;
                    this.ghH[j] = (int) round2;
                    //}
                }
            }
            this.ghM = this.ghL - f2;
        }
    }

    /* renamed from: a.lF$a */
    public class C2868a {
        C2870c azW;
        C2870c azX;
        Dimension[][] azY;

        public C2868a(Dimension dimension, int i, int i2) {
            this.azW = new C2870c((float) dimension.width, i);
            this.azX = new C2870c((float) dimension.height, i2);
            this.azY = (Dimension[][]) Array.newInstance(Dimension.class, new int[]{i, i2});
        }

        /* renamed from: a */
        public void mo20196a(int i, int i2, Dimension dimension, Component component) {
            Dimension[] dimensionArr = this.azY[i];
            Dimension d = C2867lF.this.mo20189d(component, dimension.width, dimension.height);
            dimensionArr[i2] = d;
            Dimension c = C2867lF.this.mo20188c(component, dimension.width, dimension.height);
            Dimension a = C2867lF.this.m34764b(component, dimension.width, dimension.height);
            this.azW.ghK[i] = Math.max((float) a.width, this.azW.ghK[i]);
            this.azW.ghJ[i] = Math.max((float) c.width, this.azW.ghJ[i]);
            this.azW.ghI[i] = Math.max((float) d.width, this.azW.ghI[i]);
            this.azX.ghK[i2] = Math.max((float) a.height, this.azX.ghK[i2]);
            this.azX.ghJ[i2] = Math.max((float) c.height, this.azX.ghJ[i2]);
            this.azX.ghI[i2] = Math.max((float) d.height, this.azX.ghI[i2]);
        }

        /* renamed from: Mq */
        public void mo20195Mq() {
            this.azW.mo20199Mq();
            this.azX.mo20199Mq();
        }
    }

    /* renamed from: a.lF$b */
    /* compiled from: a */
    public class C2869b extends C2870c {
        public Dimension[] bjC;
        public boolean[] bjD;

        public C2869b(float f, int i) {
            super(f, i);
            this.bjC = new Dimension[i];
            this.bjD = new boolean[i];
        }

        /* renamed from: a */
        public void mo20197a(int i, Component component, int i2, int i3) {
            if (C2867lF.this.bCM || component.isVisible()) {
                this.bjD[i] = true;
                float[] fArr = this.ghI;
                Dimension[] dimensionArr = this.bjC;
                Dimension d = C2867lF.this.mo20189d(component, i2, i3);
                dimensionArr[i] = d;
                fArr[i] = (float) d.height;
                this.ghJ[i] = (float) C2867lF.this.mo20188c(component, i2, i3).height;
                this.ghK[i] = (float) C2867lF.this.m34764b(component, i2, i3).height;
            }
        }

        /* renamed from: b */
        public void mo20198b(int i, Component component, int i2, int i3) {
            if (C2867lF.this.bCM || component.isVisible()) {
                this.bjD[i] = true;
                float[] fArr = this.ghI;
                Dimension[] dimensionArr = this.bjC;
                Dimension d = C2867lF.this.mo20189d(component, i2, i3);
                dimensionArr[i] = d;
                fArr[i] = (float) d.width;
                this.ghJ[i] = (float) C2867lF.this.mo20188c(component, i2, i3).width;
                this.ghK[i] = (float) C2867lF.this.m34764b(component, i2, i3).width;
            }
        }
    }
}
