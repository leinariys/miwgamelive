package logic.ui.item;

import logic.ui.ComponentManager;
import logic.ui.IBaseUiTegXml;
import logic.ui.IComponentManager;
import logic.ui.Panel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.xx */
/* compiled from: a */
public class TaskPaneContainer extends Panel {

    /* access modifiers changed from: private */
    public boolean efB;
    /* access modifiers changed from: private */
    public boolean efC;
    /* access modifiers changed from: private */
    public Scrollable efw = (Scrollable) IBaseUiTegXml.initBaseUItegXML().createJComponent(TaskPaneContainer.class, "taskpane_container.xml");
    /* access modifiers changed from: private */
    public JLayeredPane efx = this.efw.mo4915cd("content");
    private List<Integer> efA = new ArrayList();
    private List<Container> efD = new ArrayList();
    private C4004c efE;
    private C4002a efy;
    private C4003b efz;

    public TaskPaneContainer() {
        super(new BorderLayout());
        mo23034dt(true);
        super.addImpl(this.efw, "Center", -1);
        this.efy = new C4002a(this, (C4002a) null);
        this.efB = false;
        this.efC = false;
    }

    /* renamed from: l */
    public int mo23036l(Component component) {
        if (this.efA == null || this.efx == null) {
            return -1;
        }
        Integer valueOf = Integer.valueOf(this.efx.getComponentZOrder(component));
        if (valueOf.intValue() < 0) {
            return -1;
        }
        if (valueOf.intValue() >= this.efA.size()) {
            valueOf = Integer.valueOf(this.efA.size() - 1);
        }
        if (valueOf.intValue() < 0) {
            return -1;
        }
        Integer num = this.efA.get(valueOf.intValue());
        return num == null ? -1 : num.intValue();
    }

    /* access modifiers changed from: private */
    public int buq() {
        return this.efD.size();
    }

    /* access modifiers changed from: private */
    public int bur() {
        int i = 0;
        for (int i2 = 0; i2 < this.efD.size(); i2++) {
            i += this.efD.get(i2).getHeight();
        }
        return i;
    }

    /* renamed from: dt */
    public void mo23034dt(boolean z) {
        if (z) {
            if (this.efE == null) {
                this.efE = new C4004c(this, (C4004c) null);
            }
            this.efx.setLayout(this.efE);
        } else {
            this.efx.setLayout((LayoutManager) null);
        }
        this.efw.revalidate();
    }

    public boolean bus() {
        return this.efx.getLayout() != null;
    }

    /* access modifiers changed from: protected */
    public void addImpl(Component component, Object obj, int i) {
        int i2;
        int i3;
        if (component instanceof C0567Hq) {
            Component[] components = this.efx.getComponents();
            int length = components.length;
            int i4 = 0;
            while (true) {
                if (i4 >= length) {
                    i2 = -1;
                    break;
                }
                Component component2 = components[i4];
                if (mo23036l(component2) >= i) {
                    i2 = this.efx.getComponentZOrder(component2);
                    break;
                }
                i4++;
            }
            if (i2 != -1 || buu() < i) {
                i3 = i2;
            } else {
                i3 = buv();
            }
            this.efx.add(component, obj, i3);
            if (component instanceof TaskPane) {
                TaskPane bpVar = (TaskPane) component;
                if (bpVar.isCollapsed()) {
                    bpVar.collapse();
                }
            }
            if (i == -1) {
                i = this.efx.getComponentCount();
            }
            if (i3 == -1) {
                this.efA.add(Integer.valueOf(i));
            } else {
                this.efA.add(i3, Integer.valueOf(i));
            }
            if (i < 0) {
                this.efD.add((Container) component);
                return;
            }
            return;
        }
        super.addImpl(component, obj, i);
    }

    public void remove(int i) {
        if (i >= 0 && i < this.efA.size()) {
            if (this.efA.get(i).intValue() < 0) {
                this.efD.remove(this.efx.getComponent(i));
            }
            this.efA.remove(i);
            super.remove(i);
        }
    }

    public void remove(Component component) {
        int componentZOrder = this.efx.getComponentZOrder(component);
        if (componentZOrder != -1) {
            if (this.efA.get(componentZOrder).intValue() < 0) {
                this.efD.remove(component);
            }
            this.efA.remove(Integer.valueOf(componentZOrder));
            super.remove(component);
        }
    }

    public String getElementName() {
        return "taskpanecontainer";
    }

    /* renamed from: b */
    public void mo23026b(TaskPane bpVar) {
        this.efy.mo23040b(bpVar);
    }

    /* renamed from: c */
    public void mo23033c(TaskPane bpVar) {
        this.efy.mo23041c(bpVar);
    }

    /* renamed from: du */
    public void mo23035du(boolean z) {
        this.efB = z;
    }

    /* renamed from: a */
    public void mo23024a(TaskPane bpVar, boolean z) {
        if (bpVar != null && this.efx.getComponentZOrder(bpVar) >= 0) {
            bpVar.setVisible(z);
            validate();
        }
    }

    private C4003b but() {
        C4003b bVar = new C4003b(this, (C4003b) null);
        bVar.setName("divisor");
        ComponentManager.getCssHolder(bVar).setAttribute("name", "divisor");
        return bVar;
    }

    /* renamed from: nm */
    public void mo23037nm(int i) {
        if (this.efz == null) {
            this.efz = but();
            add((Component) this.efz, i);
        }
    }

    public int buu() {
        return mo23036l(this.efz);
    }

    public int buv() {
        return this.efx.getComponentZOrder(this.efz);
    }

    public Component[] buw() {
        return this.efx.getComponents();
    }

    public int bux() {
        return this.efx.getComponentCount();
    }

    /* renamed from: nn */
    public Component mo23038nn(int i) {
        return this.efx.getComponent(i);
    }

    public JScrollBar buy() {
        return this.efw.getVerticalScrollBar();
    }

    /* renamed from: EP */
    public JComponent mo23023EP() {
        return this.efz;
    }

    /* renamed from: a.xx$c */
    /* compiled from: a */
    private class C4004c implements LayoutManager2 {
        private C4004c() {
        }

        /* synthetic */ C4004c(TaskPaneContainer xxVar, C4004c cVar) {
            this();
        }

        public void addLayoutComponent(String str, Component component) {
        }

        public void layoutContainer(Container container) {
            if (container.getComponentCount() > 0 && !TaskPaneContainer.this.efC) {
                Insets insets = container.getInsets();
                int i = insets.top;
                int width = container.getWidth();
                for (Component component : container.getComponents()) {
                    if (component.isVisible()) {
                        int height = component.getHeight();
                        component.setBounds(insets.left, i, width, height);
                        i += height;
                    }
                }
            }
        }

        public Dimension minimumLayoutSize(Container container) {
            Insets insets = container.getInsets();
            int i = insets.top;
            if (container.getComponentCount() <= 0) {
                return new Dimension(insets.left + insets.right, insets.bottom + insets.top);
            }
            for (Component component : container.getComponents()) {
                if (component.isVisible()) {
                    i += component.getHeight();
                }
            }
            return new Dimension(container.getMinimumSize().width, i + insets.bottom);
        }

        public Dimension preferredLayoutSize(Container container) {
            Insets insets = container.getInsets();
            int i = insets.top;
            if (container.getComponentCount() <= 0) {
                return new Dimension(insets.left + insets.right, insets.top + insets.bottom);
            }
            for (Component component : container.getComponents()) {
                if (component.isVisible()) {
                    i += component.getSize().height;
                }
            }
            return new Dimension(0, i + insets.bottom);
        }

        public void removeLayoutComponent(Component component) {
        }

        public void addLayoutComponent(Component component, Object obj) {
            component.setSize(component.getPreferredSize());
            component.setMaximumSize(new Dimension(component.getParent().getWidth(), component.getMaximumSize().height));
        }

        public float getLayoutAlignmentX(Container container) {
            return 0.0f;
        }

        public float getLayoutAlignmentY(Container container) {
            return 0.0f;
        }

        public void invalidateLayout(Container container) {
        }

        public Dimension maximumLayoutSize(Container container) {
            Insets insets = container.getInsets();
            int i = insets.top;
            if (container.getComponentCount() <= 0) {
                return new Dimension(insets.left + insets.right, insets.bottom + insets.top);
            }
            for (Component component : container.getComponents()) {
                if (component.isVisible()) {
                    i += component.getMaximumSize().height;
                }
            }
            return new Dimension(container.getMaximumSize().width, i + insets.bottom);
        }
    }

    /* renamed from: a.xx$a */
    private class C4002a {
        private int bGo;
        private int bGp;

        private C4002a() {
            this.bGo = -1;
        }

        /* synthetic */ C4002a(TaskPaneContainer xxVar, C4002a aVar) {
            this();
        }

        /* renamed from: b */
        public void mo23040b(TaskPane bpVar) {
            Point mousePosition = TaskPaneContainer.this.getMousePosition();
            if (!TaskPaneContainer.this.efB && mousePosition != null) {
                if (this.bGo < 0) {
                    TaskPaneContainer.this.efC = true;
                    this.bGo = TaskPaneContainer.this.efw.getViewport().getViewPosition().y;
                    this.bGp = TaskPaneContainer.this.efx.getComponentZOrder(bpVar);
                    IComponentManager e = ComponentManager.getCssHolder(bpVar);
                    e.setAlpha(0.5f);
                    IBaseUiTegXml.initBaseUItegXML().mo13724cx(e.mo13049Vr().aue());
                    TaskPaneContainer.this.efx.setLayer(bpVar, JLayeredPane.DRAG_LAYER.intValue());
                }
                if (mousePosition.y >= TaskPaneContainer.this.bur()) {
                    bpVar.setLocation(0, (mousePosition.y + this.bGo) - (bpVar.djo().getHeight() / 2));
                }
            }
        }

        /* renamed from: c */
        public void mo23041c(TaskPane bpVar) {
            IComponentManager e = ComponentManager.getCssHolder(bpVar);
            Component component = null;
            Rectangle bounds = bpVar.getBounds();
            Component[] components = TaskPaneContainer.this.efx.getComponents();
            int length = components.length;
            int i = 0;
            int i2 = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                Component component2 = components[i];
                if (!bpVar.equals(component2) && component2.isVisible()) {
                    i2 += component2.getHeight();
                    if (bounds.intersects(component2.getBounds())) {
                        if (TaskPaneContainer.this.mo23036l(component2) < 0) {
                            this.bGp = TaskPaneContainer.this.efx.getComponentZOrder(component2);
                        } else {
                            component = component2;
                        }
                    }
                }
                i++;
            }
            TaskPaneContainer.this.efx.setLayer(bpVar, JLayeredPane.DEFAULT_LAYER.intValue());
            this.bGo = -1;
            TaskPaneContainer.this.efC = false;
            e.mo13049Vr().setColorMultiplier(new Color(255, 255, 255, 255));
            if (component == null) {
                int y = bpVar.getY();
                if (y <= 0) {
                    TaskPaneContainer.this.efx.setComponentZOrder(bpVar, TaskPaneContainer.this.buq());
                } else if (y >= i2) {
                    TaskPaneContainer.this.efx.setComponentZOrder(bpVar, TaskPaneContainer.this.efx.getComponentCount() - 1);
                } else {
                    TaskPaneContainer.this.efx.setComponentZOrder(bpVar, this.bGp);
                }
                m41146a(e);
                TaskPaneContainer.this.validate();
                return;
            }
            Rectangle bounds2 = component.getBounds();
            TaskPaneContainer.this.efx.remove(bpVar);
            TaskPaneContainer.this.efx.add(bpVar, (bounds.intersection(bounds2).y < (bounds2.height / 2) + bounds2.y ? 0 : 1) + TaskPaneContainer.this.efx.getComponentZOrder(component));
            bpVar.setBounds(bounds);
            m41146a(e);
            TaskPaneContainer.this.validate();
        }

        /* renamed from: a */
        private void m41146a(IComponentManager aek) {
            IBaseUiTegXml.initBaseUItegXML().mo13724cx(aek.mo13049Vr().auc());
        }
    }

    /* renamed from: a.xx$b */
    /* compiled from: a */
    private class C4003b extends Picture implements C0567Hq {


        private C4003b() {
        }

        /* synthetic */ C4003b(TaskPaneContainer xxVar, C4003b bVar) {
            this();
        }

        public boolean isPinned() {
            return true;
        }

        /* renamed from: aA */
        public boolean mo878aA() {
            return true;
        }
    }
}
