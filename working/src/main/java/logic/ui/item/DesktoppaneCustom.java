package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.BaseItemFactory;
import logic.ui.C6342alC;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.aez  reason: case insensitive filesystem */
/* compiled from: a */
public class DesktoppaneCustom extends BaseItemFactory<JDesktopPane> {
    /* access modifiers changed from: protected */
    /* renamed from: v */
    public JDesktopPane creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new JDesktopPane();
    }
}
