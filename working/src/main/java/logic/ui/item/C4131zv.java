package logic.ui.item;

import logic.swing.C2740jL;
import logic.ui.ComponentManager;
import logic.ui.PropertiesUiFromCss;

import java.awt.*;

/* renamed from: a.zv */
/* compiled from: a */
public class C4131zv implements C2740jL<Component> {
    C4131zv() {
    }

    /* renamed from: a */
    public void mo7a(Component component, float f) {
        PropertiesUiFromCss Vr = ((ComponentManager) ComponentManager.getCssHolder(component)).mo13049Vr();
        if (Vr != null) {
            int i = (int) f;
            Vr.setColorMultiplier(new Color(i, i, i, i));
            component.repaint();
        }
    }

    /* renamed from: a */
    public float mo5a(Component component) {
        Color colorMultiplier;
        PropertiesUiFromCss Vr = ((ComponentManager) ComponentManager.getCssHolder(component)).mo13049Vr();
        if (Vr == null || (colorMultiplier = Vr.getColorMultiplier()) == null) {
            return 255.0f;
        }
        return (float) colorMultiplier.getAlpha();
    }
}
