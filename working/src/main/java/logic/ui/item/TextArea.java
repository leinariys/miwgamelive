package logic.ui.item;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicTextUI;
import javax.swing.text.Caret;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/* renamed from: a.AE */
/* compiled from: a */
public class TextArea extends JTextArea implements C1513WG {

    private boolean ciu;

    /* renamed from: dO */
    private KeyAdapter f10dO;

    /* renamed from: bO */
    public void mo42bO(boolean z) {
        if (z) {
            setCaret(new BasicTextUI.BasicCaret());
        } else {
            setCaret(new C0012a(this, (C0012a) null));
        }
        this.ciu = z;
    }

    public boolean awn() {
        return this.ciu;
    }

    public String getElementName() {
        return "textarea";
    }

    /* renamed from: h */
    public void mo44h(int i) {
        if (this.f10dO != null) {
            removeKeyListener(this.f10dO);
        } else {
            this.f10dO = new C0013b(i);
        }
        addKeyListener(this.f10dO);
    }

    /* renamed from: a.AE$a */
    private final class C0012a implements Caret {
        private C0012a() {
        }

        /* synthetic */ C0012a(TextArea ae, C0012a aVar) {
            this();
        }

        public void addChangeListener(ChangeListener changeListener) {
        }

        public void deinstall(JTextComponent jTextComponent) {
        }

        public int getBlinkRate() {
            return 0;
        }

        public void setBlinkRate(int i) {
        }

        public int getDot() {
            return 0;
        }

        public void setDot(int i) {
        }

        public Point getMagicCaretPosition() {
            return null;
        }

        public void setMagicCaretPosition(Point point) {
        }

        public int getMark() {
            return 0;
        }

        public void install(JTextComponent jTextComponent) {
        }

        public boolean isSelectionVisible() {
            return false;
        }

        public void setSelectionVisible(boolean z) {
        }

        public boolean isVisible() {
            return false;
        }

        public void setVisible(boolean z) {
        }

        public void moveDot(int i) {
        }

        public void paint(Graphics graphics) {
        }

        public void removeChangeListener(ChangeListener changeListener) {
        }
    }

    /* renamed from: a.AE$b */
    /* compiled from: a */
    class C0013b extends KeyAdapter {
        private final /* synthetic */ int bbg;

        C0013b(int i) {
            this.bbg = i;
        }

        public void keyTyped(KeyEvent keyEvent) {
            if ((TextArea.this.getText().length() ^ this.bbg) == 0) {
                keyEvent.consume();
            }
        }
    }
}
