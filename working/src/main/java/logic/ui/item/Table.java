package logic.ui.item;

import logic.res.XmlNode;
import logic.swing.C3940wz;
import logic.ui.C1276Sr;
import logic.ui.C6342alC;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.MouseEvent;

/* renamed from: a.pz */
/* compiled from: a */
public class Table extends JTable implements C1276Sr {

    private boolean cuC = false;
    private C3288a cuD;

    public Table(C6342alC alc, XmlNode agy) {
        setBackground((Color) null);
        this.cuD = new C3288a();
        setModel(this.cuD);
        setDefaultRenderer(Picture.class, new C3289b(this, (C3289b) null));
        setDefaultRenderer(JLabel.class, new C3290c(this, (C3290c) null));
    }

    /* renamed from: d */
    public void mo21259d(Object... objArr) {
        this.cuD.setColumnIdentifiers(objArr);
    }

    public void addRow(Object... objArr) {
        this.cuD.addRow(objArr);
        getParent().validate();
    }

    public void clear() {
        this.cuD.mo21261VB();
    }

    /* access modifiers changed from: protected */
    public JTableHeader createDefaultTableHeader() {
        return new C2289df(this.columnModel);
    }

    public String getElementName() {
        return "table";
    }

    public void aBh() {
        this.cuC = false;
    }

    public void aBi() {
        this.cuC = true;
    }

    public void changeSelection(int i, int i2, boolean z, boolean z2) {
        if (!this.cuC) {
            Table.super.changeSelection(i, i2, z, z2);
        }
    }

    public String getToolTipText(MouseEvent mouseEvent) {
        int rowAtPoint;
        Object valueAt;
        if (C3940wz.m40778d(this) == null || (rowAtPoint = rowAtPoint(mouseEvent.getPoint())) == -1 || (valueAt = getValueAt(rowAtPoint, 0)) == null) {
            return Table.super.getToolTipText(mouseEvent);
        }
        return valueAt.toString();
    }

    /* renamed from: a.pz$a */
    private final class C3288a extends DefaultTableModel {


        public C3288a() {
        }

        public Class<?> getColumnClass(int i) {
            return getValueAt(0, i).getClass();
        }

        public boolean isCellEditable(int i, int i2) {
            return getColumnClass(i2).equals(Boolean.class);
        }

        /* renamed from: VB */
        public void mo21261VB() {
            int rowCount = getRowCount();
            if (rowCount > 0) {
                Table.this.removeRowSelectionInterval(0, rowCount - 1);
            }
        }
    }

    /* renamed from: a.pz$b */
    /* compiled from: a */
    private final class C3289b implements TableCellRenderer {
        private C3289b() {
        }

        /* synthetic */ C3289b(Table pzVar, C3289b bVar) {
            this();
        }

        public Component getTableCellRendererComponent(JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            if (obj instanceof Picture) {
                return (Picture) obj;
            }
            return null;
        }
    }

    /* renamed from: a.pz$c */
    /* compiled from: a */
    private final class C3290c implements TableCellRenderer {
        private C3290c() {
        }

        /* synthetic */ C3290c(Table pzVar, C3290c cVar) {
            this();
        }

        public Component getTableCellRendererComponent(JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            if (obj instanceof JLabel) {
                return (JLabel) obj;
            }
            return null;
        }
    }
}
