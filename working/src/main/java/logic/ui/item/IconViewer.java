package logic.ui.item;

import logic.res.ILoaderImageInterface;
import taikodom.render.graphics2d.TexPolygon;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.nF */
/* compiled from: a */
public class IconViewer extends Picture {
    private static final int duN = 8;

    /* access modifiers changed from: private */
    public TexPolygon duO;
    private int duP;

    public IconViewer(ILoaderImageInterface azb) {
        this(azb, 8);
    }

    public IconViewer(ILoaderImageInterface azb, int i) {
        super(azb);
        this.duP = i;
    }

    /* renamed from: lB */
    public void mo20659lB(int i) {
        this.duP = i;
    }

    public void setIcon(Icon icon) {
        if (icon instanceof ImageIcon) {
            mo16826b(((ImageIcon) icon).getImage());
        } else {
            super.setIcon(icon);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo16826b(Image image) {
        this.duO = new TexPolygon(image);
        this.duO.createOctagon(this.duP);
        super.setIcon(new C3032a(image));
    }

    /* renamed from: a.nF$a */
    class C3032a extends ImageIcon {


        C3032a(Image image) {
            super(image);
        }

        public synchronized void paintIcon(Component component, Graphics graphics, int i, int i2) {
            graphics.create(i, i2, component.getWidth(), component.getHeight()).fillPolygon(IconViewer.this.duO);
        }
    }
}
