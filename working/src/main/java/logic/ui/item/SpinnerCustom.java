package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.C6342alC;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.aAC */
/* compiled from: a */
@Slf4j
public class SpinnerCustom extends BaseItem<JSpinner> {

    /* access modifiers changed from: protected */
    /* renamed from: M */
    public JSpinner creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new JSpinner();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void createLayout(C6342alC alc, JSpinner jSpinner, XmlNode agy) {
        super.createLayout(alc, jSpinner, agy);
        SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel();
        spinnerNumberModel.setMinimum(checkValueAttribute(agy, "min", log));
        spinnerNumberModel.setMaximum(checkValueAttribute(agy, "max", log));
        Integer a = checkValueAttribute(agy, "step", log);
        spinnerNumberModel.setStepSize(Integer.valueOf(a != null ? a.intValue() : 1));
        Integer a2 = checkValueAttribute(agy, "value", log);
        if (a2 == null) {
            a2 = spinnerNumberModel.getMinimum() != null ? (Integer) spinnerNumberModel.getMinimum() : 0;
        }
        spinnerNumberModel.setValue(a2);
        jSpinner.setModel(spinnerNumberModel);
    }
}
