package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.BaseItemFactory;
import logic.ui.C6342alC;
import lombok.extern.slf4j.Slf4j;

import java.awt.*;

/* renamed from: a.agq  reason: case insensitive filesystem */
/* compiled from: a */
@Slf4j
public class ScrollableCustom extends BaseItemFactory<Scrollable> {


    /* renamed from: y */
    public Scrollable creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new Scrollable();
    }

    /* renamed from: z */
    public Scrollable creatUi(C6342alC alc, Container container, XmlNode agy) {
        Scrollable A = super.creatUi(alc, container, agy);
        String attribute = agy.getAttribute("show-horizontal");
        if ("never".equals(attribute)) {
            A.setHorizontalScrollBarPolicy(31);
        } else if ("always".equals(attribute)) {
            A.setHorizontalScrollBarPolicy(32);
        } else if ("as-needed".equals(attribute)) {
            A.setHorizontalScrollBarPolicy(30);
        }
        String attribute2 = agy.getAttribute("show-vertical");
        if ("never".equals(attribute2)) {
            A.setVerticalScrollBarPolicy(21);
        } else if ("always".equals(attribute2)) {
            A.setVerticalScrollBarPolicy(22);
        } else if ("as-needed".equals(attribute2)) {
            A.setVerticalScrollBarPolicy(20);
        }
        return A;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void creatAllComponentCustom(C6342alC alc, Scrollable xXVar, XmlNode agy) {
        if (agy.getListChildrenTag().size() > 0) {
            XmlNode agy2 = agy.getListChildrenTag().get(0);
            IBaseItem gC = alc.getClassBaseUi(agy2.getTagName());
            if (gC instanceof RepeaterCustom) {
                log.error("Container for Repeater must be a Box.");
            }
            if (gC != null) {
                Component d = gC.creatUi(alc, xXVar, agy2);
                xXVar.setViewportView(d);
                if (d instanceof Table) {
                    xXVar.setColumnHeader(new C2289df.C2291b());
                    return;
                }
                return;
            }
            log.error("There is no component named '" + agy2.getTagName() + "'");
        }
    }
}
