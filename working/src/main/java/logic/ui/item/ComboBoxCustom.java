package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.C6342alC;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.aRd  reason: case insensitive filesystem */
/* compiled from: a */
public final class ComboBoxCustom extends BaseItem<JComboBox> {
    /* renamed from: V */
    public JComboBox creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new JComboBox();
    }

    /* renamed from: W */
    public JComboBox creatUi(C6342alC alc, Container container, XmlNode agy) {
        JComboBox d = super.creatUi(alc, container, agy);
        mo1631a(alc, (JComponent) d, agy);
        return d;
    }
}
