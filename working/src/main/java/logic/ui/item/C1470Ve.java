package logic.ui.item;

import logic.ui.IBaseUiTegXml;

import javax.swing.text.Element;
import javax.swing.text.html.HTML;
import javax.swing.text.html.ImageView;
import java.awt.*;

/* renamed from: a.Ve */
/* compiled from: a */
public class C1470Ve extends ImageView {
    public C1470Ve(Element element) {
        super(element);
    }

    public Image getImage() {
        String str = (String) getElement().getAttributes().getAttribute(HTML.Attribute.SRC);
        if (str == null) {
            return null;
        }
        try {
            return IBaseUiTegXml.initBaseUItegXML().adz().getImage(str);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
