package logic.ui.item;


import logic.res.XmlNode;
import logic.ui.C6342alC;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.kK */
/* compiled from: a */
@Slf4j
public class SplitCustom extends BaseItem<SplitPane> {

    /* renamed from: n */
    public SplitPane creatUi(C6342alC alc, Container container, XmlNode agy) {
        SplitPane d = super.creatUi(alc, container, agy);
        m34343a(alc, container, d, agy);
        return d;
    }

    /* renamed from: a */
    private void m34343a(C6342alC alc, Container container, JSplitPane jSplitPane, XmlNode agy) {
        int i = 0;
        for (XmlNode next : agy.getListChildrenTag()) {
            IBaseItem gC = alc.getClassBaseUi(next.getTagName().toLowerCase());
            if (gC != null) {
                Component d = gC.creatUi(alc, container, next);
                switch (i) {
                    case 0:
                        jSplitPane.setLeftComponent(d);
                        i++;
                        break;
                    case 1:
                        jSplitPane.setRightComponent(d);
                        i++;
                        break;
                    default:
                        log.error("Split Pane only supports 2 insides panels");
                        break;
                }
            } else {
                log.error("There is no component named '" + next.getTagName() + "'");
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public SplitPane creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new SplitPane();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void createLayout(C6342alC alc, SplitPane ahz, XmlNode agy) {
        int i;
        super.createLayout(alc, ahz, agy);
        if ("horizontal".equals(agy.getAttribute("orientation"))) {
            i = 1;
        } else {
            i = 0;
        }
        ahz.setOrientation(i);
        Integer a = checkValueAttribute(agy, "dividerLocation", log);
        if (a != null) {
            ahz.setDividerLocation(a.intValue());
        }
        ahz.setOneTouchExpandable("true".equals(agy.getAttribute("oneTouchExpandable")));
        ahz.setContinuousLayout("true".equals(agy.getAttribute("continuousLayout")));
    }

    /* renamed from: a.kK$a */
    public static final class C2802a extends BaseItem<JLabel> {
        /* access modifiers changed from: protected */
        /* renamed from: j */
        public JLabel creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
            return new JLabel();
        }
    }
}
