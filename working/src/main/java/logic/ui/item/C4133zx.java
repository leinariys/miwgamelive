package logic.ui.item;

import logic.swing.C2740jL;
import logic.swing.LabelUI;
import logic.ui.ComponentManager;
import logic.ui.IBaseUiTegXml;

import javax.swing.*;

/* renamed from: a.zx */
/* compiled from: a */
public class C4133zx implements C2740jL<JLabel> {
    private float bZy = 0.55f;
    private long bZz = 0;

    C4133zx() {
    }

    /* renamed from: a */
    public void mo7a(JLabel jLabel, float f) {
        ((LabelUI) jLabel.getUI()).mo15940uk(Math.round(f));
        if (Math.random() <= ((double) this.bZy) && this.bZz == 0) {
            this.bZz = System.currentTimeMillis();
            String auk = ComponentManager.getCssHolder(jLabel).mo13049Vr().auk();
            if (auk != null) {
                IBaseUiTegXml.initBaseUItegXML().mo13724cx(auk);
            }
        }
        if (this.bZz != 0 && System.currentTimeMillis() > this.bZz + 75) {
            this.bZz = 0;
        }
        jLabel.repaint();
    }

    /* renamed from: c */
    public float mo5a(JLabel jLabel) {
        return (float) ((LabelUI) jLabel.getUI()).ctz();
    }
}
