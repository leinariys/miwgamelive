package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.C6342alC;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.hT */
/* compiled from: a */
public class ToolBarCustom extends BaseItem<JToolBar> {

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public JToolBar creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new JToolBar();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void createLayout(C6342alC alc, JToolBar jToolBar, XmlNode agy) {
        super.createLayout(alc, jToolBar, agy);
        if ("vertical".equals(agy.getAttribute("orientation"))) {
            jToolBar.setOrientation(1);
        }
    }

    /* renamed from: a.hT$a */
    public static final class C2605a extends BaseItem<JLabel> {
        /* access modifiers changed from: protected */
        /* renamed from: j */
        public JLabel creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
            return new JLabel();
        }
    }
}
