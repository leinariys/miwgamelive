package logic.ui.item;

import logic.ui.C2698il;
import logic.ui.IBaseUiTegXml;
import logic.ui.Panel;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.HS */
/* compiled from: a */
public class LabeledIcon extends Panel {

    private static /* synthetic */ int[] eLR = null;
    private C2698il eLQ = (C2698il) IBaseUiTegXml.initBaseUItegXML().createJComponent(LabeledIcon.class, "labeledIcon.xml");
    private Picture eLK = this.eLQ.mo4915cd("upper-label-w");
    private Picture eLL = this.eLQ.mo4915cd("upper-label-e");
    private Picture eLM = this.eLQ.mo4915cd("lower-label-w");
    private Picture eLN = this.eLQ.mo4915cd("lower-label-e");
    private IconViewer eLO = this.eLQ.mo4915cd("image-label");
    private C2698il eLP = this.eLQ.mo4916ce("overlay");

    public LabeledIcon() {
        setLayout(new GridLayout());
        add((Panel) this.eLQ);
    }

    static /* synthetic */ int[] bHs() {
        int[] iArr = eLR;
        if (iArr == null) {
            iArr = new int[C0530a.values().length];
            try {
                iArr[C0530a.NORTH_EAST.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C0530a.NORTH_WEST.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C0530a.SOUTH_EAST.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[C0530a.SOUTH_WEST.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            eLR = iArr;
        }
        return iArr;
    }

    /* renamed from: a */
    public Picture mo2601a(C0530a aVar) {
        switch (bHs()[aVar.ordinal()]) {
            case 1:
                return this.eLK;
            case 2:
                return this.eLL;
            case 3:
                return this.eLM;
            case 4:
                return this.eLN;
            default:
                return null;
        }
    }

    /* renamed from: a */
    public void mo2602a(C0530a aVar, String str) {
        Picture a = mo2601a(aVar);
        a.setText(str);
        if (str != null && str.length() > 0) {
            a.setSize(a.getPreferredSize());
            a.setVisible(true);
        } else if (a.getIcon() == null || a.getIcon().getIconHeight() <= 0 || a.getIcon().getIconWidth() <= 0) {
            a.setSize(0, 0);
            a.setVisible(false);
        }
    }

    /* renamed from: b */
    public void mo2607b(C0530a aVar, String str) {
        mo2601a(aVar).mo16825aw(str);
    }

    /* renamed from: a */
    public void mo2603a(C0530a aVar, Icon icon) {
        Picture a = mo2601a(aVar);
        a.setIcon(icon);
        if (icon != null && icon.getIconHeight() > 0 && icon.getIconWidth() > 0) {
            a.setSize(a.getPreferredSize());
            a.setVisible(true);
        } else if (a.getText() == null || a.getText().length() <= 0) {
            a.setSize(0, 0);
            a.setVisible(false);
        }
    }

    public void setBackgroundImage(String str) {
        this.eLO.mo16825aw(str);
    }

    /* renamed from: a */
    public void mo2605a(Icon icon) {
        this.eLO.setIcon(icon);
        if (icon != null) {
            this.eLO.setSize(icon.getIconWidth(), icon.getIconHeight());
            setPreferredSize(getPreferredSize());
            ((Panel) this.eLQ).setPreferredSize(this.eLO.getPreferredSize());
        }
    }

    public IconViewer bHq() {
        return this.eLO;
    }

    /* renamed from: b */
    public String mo2606b(C0530a aVar) {
        return mo2601a(aVar).getText();
    }

    public void bHr() {
    }

    /* renamed from: a */
    public void mo2604a(C0531b bVar) {
        for (C0530a aVar : C0530a.values()) {
            mo2602a(aVar, bVar.mo2612b(aVar));
            mo2603a(aVar, bVar.mo2613c(aVar));
        }
        mo2605a(bVar.deh());
    }

    public String getElementName() {
        return "labeled-icon";
    }

    /* renamed from: lB */
    public void mo2610lB(int i) {
        if (this.eLO != null) {
            this.eLO.mo20659lB(i);
        }
    }

    /* renamed from: a.HS$a */
    public enum C0530a {
        NORTH_WEST,
        NORTH_EAST,
        SOUTH_WEST,
        SOUTH_EAST
    }

    /* renamed from: a.HS$b */
    /* compiled from: a */
    public static abstract class C0531b {
        /* renamed from: b */
        public String mo2612b(C0530a aVar) {
            return null;
        }

        /* renamed from: c */
        public Icon mo2613c(C0530a aVar) {
            return null;
        }

        public Icon deh() {
            return null;
        }
    }
}
