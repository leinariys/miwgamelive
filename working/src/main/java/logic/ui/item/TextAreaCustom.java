package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.C6342alC;
import lombok.extern.slf4j.Slf4j;

import java.awt.*;

/* renamed from: a.aMN */
/* compiled from: a */
@Slf4j
public final class TextAreaCustom extends TextComponent<TextArea> {
    /* renamed from: S */
    public TextArea creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new TextArea();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo7340a(C6342alC alc, TextArea ae, XmlNode agy) {
        super.createLayout(alc, ae, agy);
        if ("true".equalsIgnoreCase(agy.getAttribute("multiline"))) {
            ae.setWrapStyleWord(true);
            ae.setLineWrap(true);
        }
        if ("false".equals(agy.getAttribute("selectable"))) {
            ae.mo42bO(false);
        }
        Integer a = checkValueAttribute(agy, "rows", log);
        if (a != null) {
            ae.setRows(a.intValue());
        }
        Integer a2 = checkValueAttribute(agy, "cols", log);
        if (a2 != null) {
            ae.setColumns(a2.intValue());
        }
    }
}
