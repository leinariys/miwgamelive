package logic.ui.item;

import java.awt.*;
import java.util.Map;

/* renamed from: a.akP  reason: case insensitive filesystem */
/* compiled from: a */
public class C6303akP {
    private final Component component;
    private final String name;
    private Map<String, String> aSx;

    public C6303akP(String str, Component component2) {
        this.name = str == null ? component2.getName() : str;
        this.component = component2;
    }

    public String getName() {
        return this.name;
    }

    public Component getComponent() {
        return this.component;
    }

    public void setAttributes(Map<String, String> map) {
        this.aSx = map;
    }

    public Object getAttribute(String str) {
        if (this.aSx == null) {
            return null;
        }
        return this.aSx.get(str);
    }
}
