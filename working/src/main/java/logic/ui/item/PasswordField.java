package logic.ui.item;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/* renamed from: a.anH  reason: case insensitive filesystem */
/* compiled from: a */
public class PasswordField extends JPasswordField implements C1513WG {

    /* renamed from: dO */
    private KeyAdapter f4936dO;

    /* renamed from: h */
    public void mo44h(int i) {
        if (this.f4936dO != null) {
            removeKeyListener(this.f4936dO);
        } else {
            this.f4936dO = new C1945a(i);
        }
        addKeyListener(this.f4936dO);
    }

    public String getElementName() {
        return "textfield";
    }

    /* renamed from: a.anH$a */
    class C1945a extends KeyAdapter {
        private final /* synthetic */ int bbg;

        C1945a(int i) {
            this.bbg = i;
        }

        public void keyTyped(KeyEvent keyEvent) {
            if ((PasswordField.this.getText().length() ^ this.bbg) == 0) {
                keyEvent.consume();
            }
        }
    }
}
