package logic.ui;

import logic.ui.item.IBaseItem;
import logic.ui.layout.IBaseLayout;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.Tz */
/* compiled from: a */
public class MapKeyValue implements C6342alC {
    /* renamed from: ejE */
    public C6342alC listTagAsClass;
    /**
     * Пример file:/C:/.../taikodom/addon/debugtools/taikodomversion/taikodomversion.xml
     */
    private URL base;
    /**
     * Список Базовые элементы интерфейса
     * Класс BaseUItegXML
     */
    /* renamed from: ejB */
    private Map<String, IBaseItem> listBaseUiXML;
    /* renamed from: ejC */
    private BaseUiTegXml baseUiTegXml;
    /**
     * Список Базовые элементы интерфейса Layout
     * Класс BaseUItegXML
     */
    /* renamed from: ejD */
    private Map<String, IBaseLayout> listBaseLayoutUiXML;

    public MapKeyValue(BaseUiTegXml aiw) {
        this.baseUiTegXml = aiw;
    }

    public MapKeyValue(BaseUiTegXml aiw, URL url) {
        this(aiw);
        this.base = url;
    }

    public MapKeyValue(C6342alC alc, URL url) {
        this.listTagAsClass = alc;
        this.base = url;
    }

    /* renamed from: a */
    public void add(String str, IBaseItem jc) {
        if (this.listBaseUiXML == null) {
            this.listBaseUiXML = new HashMap();
        }
        this.listBaseUiXML.put(str, jc);
    }

    /* renamed from: a */
    public void add(String str, IBaseLayout arc) {
        if (this.listBaseLayoutUiXML == null) {
            this.listBaseLayoutUiXML = new HashMap();
        }
        this.listBaseLayoutUiXML.put(str, arc);
    }

    /* renamed from: bvo */
    public URL getPathFile() {
        if (this.base != null) {
            return this.base;
        }
        if (this.listTagAsClass != null) {
            return this.listTagAsClass.getPathFile();
        }
        return null;
    }

    /**
     * Получить класс по имени тега в xml (Базовые элементы интерфейса)
     *
     * @param tegName Пример window
     * @return Пример all.WindowCustom
     */
    /* renamed from: gC */
    public IBaseItem getClassBaseUi(String tegName) {
        IBaseItem jc;
        if (this.listBaseUiXML != null && (jc = this.listBaseUiXML.get(tegName)) != null) {
            return jc;//класс  соответствующий имени тега в xml
        }
        if (this.listTagAsClass != null) {
            return this.listTagAsClass.getClassBaseUi(tegName);
        }
        return null;
    }

    /* renamed from: gD */
    public IBaseLayout getClassLayoutUi(String str) {
        IBaseLayout arc;
        if (this.listBaseLayoutUiXML != null && (arc = this.listBaseLayoutUiXML.get(str)) != null) {
            return arc;
        }
        if (this.listTagAsClass != null) {
            return this.listTagAsClass.getClassLayoutUi(str);
        }
        return null;
    }

    /* renamed from: aVW */
    public IBaseUiTegXml getBaseUiTegXml() {
        if (this.baseUiTegXml != null) {
            return this.baseUiTegXml;
        }
        if (this.listTagAsClass != null) {
            return this.listTagAsClass.getBaseUiTegXml();
        }
        return null;
    }
}
