package logic.ui;

import logic.res.ILoaderImageInterface;
import logic.res.css.C1206Ro;
import logic.res.css.C6868avI;
import logic.swing.*;
import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;
import org.w3c.dom.css.CSSValueList;
import taikodom.render.TDesktop;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Свойства UI компонента заполненое из CSS
 */
/* renamed from: a.Aj */
/* compiled from: a */
public class PropertiesUiFromCss {
    private static final Color cds = new Color(0, 0, 0, 0);
    static Map<String, C1366Tu> cdt = new HashMap();
    static Map<String, C3991xo> cdu = new HashMap();
    static Map<String, aRU> cdv = new HashMap();
    static Map<String, C3810vI> cdw = new HashMap();
    static Map<String, ComponentOrientation> cdx = new HashMap();

    static {
        cdw.put("super", C3810vI.TOP);
        cdw.put("top", C3810vI.TOP);
        cdw.put("bottom", C3810vI.BOTTOM);
        cdw.put("middle", C3810vI.CENTER);
        cdw.put("center", C3810vI.CENTER);
        cdw.put("fill", C3810vI.FILL);
        cdx.put("ltr", ComponentOrientation.LEFT_TO_RIGHT);
        cdx.put("rtl", ComponentOrientation.RIGHT_TO_LEFT);
        cdt.put("left", new C1366Tu(0.0f, C1366Tu.C1367a.PERCENT));
        cdt.put("top", new C1366Tu(0.0f, C1366Tu.C1367a.PERCENT));
        cdt.put("center", new C1366Tu(50.0f, C1366Tu.C1367a.PERCENT));
        cdt.put("bottom", new C1366Tu(100.0f, C1366Tu.C1367a.PERCENT));
        cdt.put("right", new C1366Tu(100.0f, C1366Tu.C1367a.PERCENT));
        cdu.put("stretch", C3991xo.STRETCH);
        cdu.put("repeat", C3991xo.REPEAT);
        cdu.put("repeat-x", C3991xo.REPEAT_X);
        cdu.put("repeat-x-stretch-y", C3991xo.REPEAT_X_STRETCH_Y);
        cdu.put("repeat-y", C3991xo.REPEAT_Y);
        cdu.put("repeat-y-stretch-x", C3991xo.REPEAT_Y_STRETCH_X);
        cdu.put("no-repeat", C3991xo.NO_REPEAT);
        cdv.put("middle", aRU.CENTER);
        cdv.put("center", aRU.CENTER);
        cdv.put("left", aRU.LEFT);
        cdv.put("right", aRU.RIGHT);
        cdv.put("fill", aRU.FILL);
    }

    public final IComponentManager cdV;
    /* renamed from: Td */
    public Image iconImage;
    public Image[] backgroundImage;
    public C2741jM[] backgroundPosition;
    public C3991xo[] backgroundRepeat;
    public Boolean blurBehind;
    public Image glowImage;
    public Image borderBottomImage;
    public C3991xo borderBottomImageRepeat;
    public Image borderBottomLeftImage;
    public Image borderBottomRightImage;
    public Color borderColorMultiplier;
    public Image borderLeftImage;
    public C3991xo borderLeftImageRepeat;
    public Image borderRightImage;
    public C3991xo borderRightImageRepeat;
    public C2741jM borderSpacing;
    public String borderStyle;
    public Image borderTopImage;
    public C3991xo borderTopImageRepeat;
    public Image borderTopLeftImage;
    public Image borderTopRightImage;
    public Cursor cursor;
    public C1366Tu height;
    public aRU textAlign;
    public Icon cdY;
    public C1366Tu left;
    public Color[] backgroundColor;
    public Image backgroundFill;
    public C3810vI verticalAlign;
    public C1366Tu width;
    public Image borderLeftFill;
    public C1366Tu marginBottom;
    public C1366Tu marginLeft;
    public C1366Tu marginRight;
    public C1366Tu marginTop;
    public C1366Tu maxHeight;
    public C1366Tu maxWidth;
    public C1366Tu minHeight;
    public Dimension minimumSize;
    public C1366Tu minWidth;
    public C1366Tu paddingBottom;
    public C1366Tu paddingLeft;
    public C1366Tu paddingRight;
    public C1366Tu paddingTop;
    public Image borderRightFill;
    public C1366Tu fontShadowX;
    public C1366Tu fontShadowY;
    public C1366Tu top;
    public Color color;
    public Color colorMultiplier;
    public Font font;
    public int hash;
    public Color shadowColor;
    private String dropSucessSound;
    private String dropFailSound;
    private String dragStartSound;
    private String openSound;
    private String closeSound;
    private String collapseSound;
    private String expandSound;
    private String scrollSound;
    private String typeAnimationSound;
    private ComponentOrientation direction;
    private String mouseOverSound;
    private String mousePressSound;
    private String mouseReleaseSound;
    private String mouseClickSound;
    private String keyPressSound;
    private String keyReleaseSound;
    private String keyTypedSound;
    private String actionSound;

    public PropertiesUiFromCss(IComponentManager aek) {
        this.cdV = aek;
        setCssToUI(new C5898aca(aek), true);
    }

    public PropertiesUiFromCss(IComponentManager aek, CSSStyleDeclaration cSSStyleDeclaration) {
        this.cdV = aek;
        if (cSSStyleDeclaration != null) {
            setCssToUI(new C5898aca(aek, cSSStyleDeclaration), false);
        }
    }

    /* renamed from: f */
    public static PropertiesUiFromCss m461f(Component component) {
        IComponentManager e = ComponentManager.getCssHolder(component);
        if (e != null) {
            return e.mo13047Vp();
        }
        return null;
    }

    /* renamed from: g */
    public static PropertiesUiFromCss m462g(Component component) {
        IComponentManager e = ComponentManager.getCssHolder(component);
        if (e != null) {
            return e.mo13047Vp();
        }
        return null;
    }

    /* renamed from: a */
    public static PropertiesUiFromCss m456a(ComponentUI componentUI) {
        if (componentUI instanceof IComponentUi) {
            return ((IComponentUi) componentUI).getComponentManager().mo13047Vp();
        }
        return null;
    }

    /* renamed from: h */
    public static TDesktop m463h(Component component) {
        Component component2 = component;
        while (!(component2 instanceof TDesktop)) {
            if (component2 == null) {
                return null;
            }
            component2 = component2.getParent();
        }
        return (TDesktop) component2;
    }

    /* renamed from: fS */
    public Color mo427fS(int i) {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.backgroundColor != null && Vr.backgroundColor.length > i) {
            return Vr.backgroundColor[i];
        }
        if (this.backgroundColor == null || this.backgroundColor.length <= i) {
            return cds;
        }
        return this.backgroundColor[i];
    }

    public Image atg() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.backgroundFill == null) {
            return this.backgroundFill;
        }
        return Vr.backgroundFill;
    }

    /* renamed from: fT */
    public Image mo428fT(int i) {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.backgroundImage != null && Vr.backgroundImage.length > i) {
            return Vr.backgroundImage[i];
        }
        if (this.backgroundImage == null || this.backgroundImage.length <= i) {
            return null;
        }
        return this.backgroundImage[i];
    }

    /* renamed from: fU */
    public C2741jM mo429fU(int i) {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.backgroundPosition != null && Vr.backgroundPosition.length > i) {
            return Vr.backgroundPosition[i];
        }
        if (this.backgroundPosition == null || this.backgroundPosition.length <= i) {
            return C2741jM.apf;
        }
        return this.backgroundPosition[i];
    }

    /* renamed from: fV */
    public C3991xo mo430fV(int i) {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.backgroundRepeat != null && Vr.backgroundRepeat.length > i) {
            return Vr.backgroundRepeat[i];
        }
        if (this.backgroundRepeat == null || this.backgroundRepeat.length <= i) {
            return C3991xo.STRETCH;
        }
        return this.backgroundRepeat[i];
    }

    public Boolean ath() {
        return this.blurBehind;
    }

    public Image ati() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderBottomImage == null) {
            return this.borderBottomImage;
        }
        return Vr.borderBottomImage;
    }

    public Image atj() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.glowImage == null) {
            return this.glowImage;
        }
        return Vr.glowImage;
    }

    public C3991xo atk() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderBottomImageRepeat == null) {
            return this.borderBottomImageRepeat;
        }
        return Vr.borderBottomImageRepeat;
    }

    public Image atl() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderBottomLeftImage == null) {
            return this.borderBottomLeftImage;
        }
        return Vr.borderBottomLeftImage;
    }

    public Image atm() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderBottomRightImage == null) {
            return this.borderBottomRightImage;
        }
        return Vr.borderBottomRightImage;
    }

    public Color atn() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderColorMultiplier == null) {
            return this.borderColorMultiplier;
        }
        return Vr.borderColorMultiplier;
    }

    public Image ato() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderLeftImage == null) {
            return this.borderLeftImage;
        }
        return Vr.borderLeftImage;
    }

    public C3991xo atp() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderLeftImageRepeat == null) {
            return this.borderLeftImageRepeat;
        }
        return Vr.borderLeftImageRepeat;
    }

    public Image atq() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderRightImage == null) {
            return this.borderRightImage;
        }
        return Vr.borderRightImage;
    }

    public C3991xo atr() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderRightImageRepeat == null) {
            return this.borderRightImageRepeat;
        }
        return Vr.borderRightImageRepeat;
    }

    public C2741jM ats() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderSpacing == null) {
            return this.borderSpacing;
        }
        return Vr.borderSpacing;
    }

    public String getBorderStyle() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderStyle == null) {
            return this.borderStyle;
        }
        return Vr.borderStyle;
    }

    public Image att() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderTopImage == null) {
            return this.borderTopImage;
        }
        return Vr.borderTopImage;
    }

    public C3991xo atu() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderTopImageRepeat == null) {
            return this.borderTopImageRepeat;
        }
        return Vr.borderTopImageRepeat;
    }

    public Image atv() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderTopLeftImage == null) {
            return this.borderTopLeftImage;
        }
        return Vr.borderTopLeftImage;
    }

    public Image atw() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderTopRightImage == null) {
            return this.borderTopRightImage;
        }
        return Vr.borderTopRightImage;
    }

    public Color getColor() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.color != null) {
            return Vr.color;
        }
        if (this.color != null) {
            return this.color;
        }
        IComponentManager Vo = this.cdV.mo13046Vo();
        if (Vo != null) {
            return Vo.mo13047Vp().getColor();
        }
        return Color.white;
    }

    public Color getColorMultiplier() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.colorMultiplier == null) {
            return this.colorMultiplier;
        }
        return Vr.colorMultiplier;
    }

    public void setColorMultiplier(Color color2) {
        this.colorMultiplier = color2;
        this.hash = 0;
    }

    public Cursor getCursor() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.cursor == null) {
            return this.cursor;
        }
        return Vr.cursor;
    }

    public Font getFont() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.font == null) {
            return this.font;
        }
        return Vr.font;
    }

    public C1366Tu atx() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.height == null) {
            return this.height;
        }
        return Vr.height;
    }

    public aRU aty() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.textAlign == null) {
            return this.textAlign;
        }
        return Vr.textAlign;
    }

    public Icon getIcon() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.iconImage != null) {
            return Vr.getIcon();
        }
        if (this.iconImage != null && this.cdY == null) {
            this.cdY = new C0281Dc(new ImageIcon(this.iconImage));
        }
        return this.cdY;
    }

    public Image getIconImage() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.iconImage == null) {
            return this.iconImage;
        }
        return Vr.iconImage;
    }

    public C1366Tu atz() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.left == null) {
            return this.left;
        }
        return Vr.left;
    }

    public Image atA() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderLeftFill == null) {
            return this.borderLeftFill;
        }
        return Vr.borderLeftFill;
    }

    public C1366Tu atB() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.marginBottom == null) {
            return this.marginBottom;
        }
        return Vr.marginBottom;
    }

    public C1366Tu atC() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.marginLeft == null) {
            return this.marginLeft;
        }
        return Vr.marginLeft;
    }

    public C1366Tu atD() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.marginRight == null) {
            return this.marginRight;
        }
        return Vr.marginRight;
    }

    public C1366Tu atE() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.marginTop == null) {
            return this.marginTop;
        }
        return Vr.marginTop;
    }

    public C1366Tu atF() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.maxHeight == null) {
            return this.maxHeight;
        }
        return Vr.maxHeight;
    }

    public C1366Tu atG() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.maxWidth == null) {
            return this.maxWidth;
        }
        return Vr.maxWidth;
    }

    public C1366Tu atH() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.minHeight == null) {
            return this.minHeight;
        }
        return Vr.minHeight;
    }

    public Dimension getMinimumSize() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.minimumSize == null) {
            return this.minimumSize;
        }
        return Vr.minimumSize;
    }

    public C1366Tu atI() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.minWidth == null) {
            return this.minWidth;
        }
        return Vr.minWidth;
    }

    public C1366Tu atJ() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.paddingBottom == null) {
            return this.paddingBottom;
        }
        return Vr.paddingBottom;
    }

    public C1366Tu atK() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.paddingLeft == null) {
            return this.paddingLeft;
        }
        return Vr.paddingLeft;
    }

    public C1366Tu atL() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.paddingRight == null) {
            return this.paddingRight;
        }
        return Vr.paddingRight;
    }

    public C1366Tu atM() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.paddingTop == null) {
            return this.paddingTop;
        }
        return Vr.paddingTop;
    }

    public Image atN() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderRightFill == null) {
            return this.borderRightFill;
        }
        return Vr.borderRightFill;
    }

    public Color getShadowColor() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.shadowColor != null) {
            return Vr.shadowColor;
        }
        if (this.shadowColor != null) {
            return this.shadowColor;
        }
        IComponentManager Vo = this.cdV.mo13046Vo();
        if (Vo != null) {
            return Vo.mo13047Vp().getShadowColor();
        }
        return Color.black;
    }

    public float atO() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.fontShadowX != null) {
            return Vr.atO();
        }
        if (this.fontShadowX != null) {
            return this.fontShadowX.value;
        }
        IComponentManager Vo = this.cdV.mo13046Vo();
        if (Vo != null) {
            return Vo.mo13047Vp().atO();
        }
        return 0.0f;
    }

    public float atP() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.fontShadowY != null) {
            return Vr.atP();
        }
        if (this.fontShadowY != null) {
            return this.fontShadowY.value;
        }
        IComponentManager Vo = this.cdV.mo13046Vo();
        if (Vo != null) {
            return Vo.mo13047Vp().atP();
        }
        return 0.0f;
    }

    public C1366Tu atQ() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.top == null) {
            return this.top;
        }
        return Vr.top;
    }

    public C3810vI atR() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.verticalAlign == null) {
            return this.verticalAlign;
        }
        return Vr.verticalAlign;
    }

    public C1366Tu atS() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.width == null) {
            return this.width;
        }
        return Vr.width;
    }

    public int hashCode() {
        int i = this.hash;
        if (i == 0) {
            i = ((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((2120326402 ^ AdapterUiCss.hashCode(this.cdV)) * 31) ^ AdapterUiCss.hashCode(this.backgroundColor)) * 31) ^ AdapterUiCss.hashCode(this.backgroundFill)) * 31) ^ AdapterUiCss.hashCode(this.backgroundImage)) * 31) ^ AdapterUiCss.hashCode(this.backgroundPosition)) * 31) ^ AdapterUiCss.hashCode(this.backgroundRepeat)) * 31) ^ AdapterUiCss.hashCode(this.blurBehind)) * 31) ^ AdapterUiCss.hashCode(this.borderColorMultiplier)) * 31) ^ AdapterUiCss.hashCode(this.borderSpacing)) * 31) ^ AdapterUiCss.hashCode(this.borderStyle)) * 31) ^ AdapterUiCss.hashCode(this.borderBottomImage)) * 31) ^ AdapterUiCss.hashCode(this.borderBottomLeftImage)) * 31) ^ AdapterUiCss.hashCode(this.borderBottomRightImage)) * 31) ^ AdapterUiCss.hashCode(this.color)) * 31) ^ AdapterUiCss.hashCode(this.colorMultiplier)) * 31) ^ AdapterUiCss.hashCode(this.font)) * 31) ^ AdapterUiCss.hashCode(this.height)) * 31) ^ AdapterUiCss.hashCode(this.textAlign)) * 31) ^ AdapterUiCss.hashCode(this.iconImage)) * 31) ^ AdapterUiCss.hashCode(this.cdY)) * 31) ^ AdapterUiCss.hashCode(this.left)) * 31) ^ AdapterUiCss.hashCode(this.borderLeftFill)) * 31) ^ AdapterUiCss.hashCode(this.borderLeftImage)) * 31) ^ AdapterUiCss.hashCode(this.marginBottom)) * 31) ^ AdapterUiCss.hashCode(this.marginLeft)) * 31) ^ AdapterUiCss.hashCode(this.marginRight)) * 31) ^ AdapterUiCss.hashCode(this.marginTop)) * 31) ^ AdapterUiCss.hashCode(this.maxHeight)) * 31) ^ AdapterUiCss.hashCode(this.maxWidth)) * 31) ^ AdapterUiCss.hashCode(this.minHeight)) * 31) ^ AdapterUiCss.hashCode(this.minimumSize)) * 31) ^ AdapterUiCss.hashCode(this.minWidth)) * 31) ^ AdapterUiCss.hashCode(this.paddingBottom)) * 31) ^ AdapterUiCss.hashCode(this.paddingLeft)) * 31) ^ AdapterUiCss.hashCode(this.paddingRight)) * 31) ^ AdapterUiCss.hashCode(this.paddingTop)) * 31) ^ AdapterUiCss.hashCode(this.borderRightFill)) * 31) ^ AdapterUiCss.hashCode(this.borderRightImage)) * 31) ^ AdapterUiCss.hashCode(this.shadowColor)) * 31) ^ AdapterUiCss.hashCode(this.fontShadowX)) * 31) ^ AdapterUiCss.hashCode(this.fontShadowY)) * 31) ^ AdapterUiCss.hashCode(this.top)) * 31) ^ AdapterUiCss.hashCode(this.borderTopImage)) * 31) ^ AdapterUiCss.hashCode(this.borderTopLeftImage)) * 31) ^ AdapterUiCss.hashCode(this.borderTopRightImage)) * 31) ^ AdapterUiCss.hashCode(this.verticalAlign)) * 31) ^ AdapterUiCss.hashCode(this.width);
            if (i == 0) {
                i = 1;
            }
            this.hash = i;
        }
        return i;
    }

    //Присваение параметров из стиля css системе событий
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0516  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x0527 A[SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setCssToUI(C5898aca paramaca, boolean paramBoolean) {
        Object localObject1;
        CSSValue localCSSValue1 = paramaca.mo3817a(this.cdV, "vertical-align", false);
        if (localCSSValue1 != null) {
            localObject1 = localCSSValue1.getCssText().toLowerCase();
            this.verticalAlign = ((C3810vI) cdw.get(localObject1));
        }
        if ((this.verticalAlign == null) && (paramBoolean)) {
            this.verticalAlign = C3810vI.CENTER;
        }
        localObject1 = IBaseUiTegXml.initBaseUItegXML().adz();

        String str1 = "";
        localCSSValue1 = paramaca.mo3817a(this.cdV, "text-align", false);
        if (localCSSValue1 != null) {
            str1 = localCSSValue1.getCssText().toLowerCase();
            this.textAlign = ((aRU) cdv.get(str1));
        }
        if ((this.textAlign == null) && (paramBoolean)) {
            this.textAlign = aRU.LEFT;
        }
        str1 = paramaca.mo16549e(this.cdV, "cursor", false);
        if (str1 != null) {
            C1366Tu localObject2 = paramaca.mo16534a(this.cdV, "cursor-top", false, C1366Tu.fEE);
            C1366Tu localObject3 = paramaca.mo16534a(this.cdV, "cursor-left", false, C1366Tu.fEE);
            this.cursor = ((ILoaderImageInterface) localObject1).getNull(str1, new Point((int) (localObject3).value, (int) (localObject2).value));
        }
        localCSSValue1 = paramaca.mo3817a(this.cdV, "direction", false);
        if (localCSSValue1 != null) {
            Object localObject2 = localCSSValue1.getCssText().toLowerCase();
            this.direction = ((ComponentOrientation) cdx.get(localObject2));
        }
        if ((this.direction == null) && (paramBoolean)) {
            this.direction = ComponentOrientation.UNKNOWN;
        }

        this.borderLeftImageRepeat = m457a(paramaca.mo3817a(this.cdV, "border-left-image-repeat", false), paramBoolean ? C3991xo.STRETCH_Y : null);
        this.borderRightImageRepeat = m457a(paramaca.mo3817a(this.cdV, "border-right-image-repeat", false), paramBoolean ? C3991xo.STRETCH_Y : null);
        this.borderBottomImageRepeat = m457a(paramaca.mo3817a(this.cdV, "border-bottom-image-repeat", false), paramBoolean ? C3991xo.STRETCH_X : null);
        this.borderTopImageRepeat = m457a(paramaca.mo3817a(this.cdV, "border-top-image-repeat", false), paramBoolean ? C3991xo.STRETCH_X : null);
        this.borderColorMultiplier = paramaca.mo16554g(this.cdV, "border-color-multiplier", false);
        this.color = paramaca.mo16535a(this.cdV, false);
        Object localObject2 = paramaca.mo16543d(this.cdV);
        if (localObject2 != null) {
            this.font = IBaseUiTegXml.initBaseUItegXML().getFont(
                    ((C1206Ro) localObject2).getFontName(),
                    ((C1206Ro) localObject2).getFontStyle(),
                    ((C1206Ro) localObject2).getFontSize());
        }
        localCSSValue1 = paramaca.mo3817a(this.cdV, "background-color", false);
        int i;
        int j;
        if (localCSSValue1 != null) {
            if (localCSSValue1.getCssValueType() == 2) {
                CSSValueList localObject3 = (CSSValueList) localCSSValue1;
                i = ((CSSValueList) localObject3).getLength();
                Color[] localObject4 = new Color[i];
                for (j = 0; j < i; j++) {
                    localObject4[j] = paramaca.mo16544d(((CSSValueList) localObject3).item(j));
                }
                this.backgroundColor = ((Color[]) localObject4);
            } else {
                this.backgroundColor = new Color[]{paramaca.mo16544d(localCSSValue1)};
            }
        }
        this.mouseOverSound = paramaca.mo16549e(this.cdV, "mouse-over-sound", false);
        this.mousePressSound = paramaca.mo16549e(this.cdV, "mouse-press-sound", false);
        this.mouseReleaseSound = paramaca.mo16549e(this.cdV, "mouse-release-sound", false);
        this.keyPressSound = paramaca.mo16549e(this.cdV, "key-press-sound", false);
        this.keyReleaseSound = paramaca.mo16549e(this.cdV, "key-release-sound", false);
        this.keyTypedSound = paramaca.mo16549e(this.cdV, "key-typed-sound", false);
        this.mouseClickSound = paramaca.mo16549e(this.cdV, "mouse-click-sound", false);
        this.actionSound = paramaca.mo16549e(this.cdV, "action-sound", false);
        this.dropSucessSound = paramaca.mo16549e(this.cdV, "drop-sucess-sound", false);
        this.dropFailSound = paramaca.mo16549e(this.cdV, "drop-fail-sound", false);
        this.dragStartSound = paramaca.mo16549e(this.cdV, "drag-start-sound", false);
        this.openSound = paramaca.mo16549e(this.cdV, "open-sound", false);
        this.closeSound = paramaca.mo16549e(this.cdV, "close-sound", false);
        this.collapseSound = paramaca.mo16549e(this.cdV, "collapse-sound", false);
        this.expandSound = paramaca.mo16549e(this.cdV, "expand-sound", false);
        this.scrollSound = paramaca.mo16549e(this.cdV, "scroll-sound", false);
        this.typeAnimationSound = paramaca.mo16549e(this.cdV, "type-animation-sound", false);
        this.borderStyle = paramaca.mo16549e(this.cdV, "border-style", false);
        this.borderTopLeftImage = ((ILoaderImageInterface) localObject1).getImage(paramaca.mo16549e(this.cdV, "border-top-left-image", false));
        this.borderTopRightImage = ((ILoaderImageInterface) localObject1).getImage(paramaca.mo16549e(this.cdV, "border-top-right-image", false));
        this.borderTopImage = ((ILoaderImageInterface) localObject1).getImage(paramaca.mo16549e(this.cdV, "border-top-image", false));


        localCSSValue1 = paramaca.mo3817a(this.cdV, "background-image", false);
        if (localCSSValue1 != null) {
            if (localCSSValue1.getCssValueType() == 2) {
                CSSValueList localObject3 = (CSSValueList) localCSSValue1;
                i = ((CSSValueList) localObject3).getLength();
                Image[] localObject4 = new Image[i];
                for (j = 0; j < i; j++) {
                    localObject4[j] = ((ILoaderImageInterface) localObject1).getImage(m458a(((CSSValueList) localObject3).item(j)));
                }
                this.backgroundImage = ((Image[]) localObject4);
            } else {
                this.backgroundImage = new Image[]{((ILoaderImageInterface) localObject1).getImage(m458a(localCSSValue1))};
            }
        }


        this.backgroundRepeat = m459a(paramaca.mo3817a(this.cdV, "background-repeat", false), null);
        this.glowImage = ((ILoaderImageInterface) localObject1).getImage(paramaca.mo16549e(this.cdV, "glow-image", false));
        this.borderLeftImage = ((ILoaderImageInterface) localObject1).getImage(paramaca.mo16549e(this.cdV, "border-left-image", false));
        this.borderRightImage = ((ILoaderImageInterface) localObject1).getImage(paramaca.mo16549e(this.cdV, "border-right-image", false));
        this.borderBottomImage = ((ILoaderImageInterface) localObject1).getImage(paramaca.mo16549e(this.cdV, "border-bottom-image", false));
        this.borderBottomLeftImage = ((ILoaderImageInterface) localObject1).getImage(paramaca.mo16549e(this.cdV, "border-bottom-left-image", false));
        this.borderBottomRightImage = ((ILoaderImageInterface) localObject1).getImage(paramaca.mo16549e(this.cdV, "border-bottom-right-image", false));
        this.backgroundFill = ((ILoaderImageInterface) localObject1).getImage(paramaca.mo16549e(this.cdV, "background-fill", false));
        this.borderLeftFill = ((ILoaderImageInterface) localObject1).getImage(paramaca.mo16549e(this.cdV, "border-left-fill", false));
        this.borderRightFill = ((ILoaderImageInterface) localObject1).getImage(paramaca.mo16549e(this.cdV, "border-right-fill", false));

        Object localObject3 = paramaca.mo16549e(this.cdV, "blur-behind", false);
        if (localObject3 != null) {
            this.blurBehind = Boolean.valueOf("true".equals(localObject3));
        }
        this.iconImage = ((ILoaderImageInterface) localObject1).getImage(paramaca.mo16549e(this.cdV, "icon", false));

        C1366Tu localC1366Tu1 = paramBoolean ? C1366Tu.fEC : null;
        this.marginTop = paramaca.mo16534a(this.cdV, "margin-top", false, localC1366Tu1);
        this.marginBottom = paramaca.mo16534a(this.cdV, "margin-bottom", false, localC1366Tu1);
        this.marginLeft = paramaca.mo16534a(this.cdV, "margin-left", false, localC1366Tu1);
        this.marginRight = paramaca.mo16534a(this.cdV, "margin-right", false, localC1366Tu1);
        this.paddingTop = paramaca.mo16534a(this.cdV, "padding-top", false, localC1366Tu1);
        this.paddingBottom = paramaca.mo16534a(this.cdV, "padding-bottom", false, localC1366Tu1);
        this.paddingLeft = paramaca.mo16534a(this.cdV, "padding-left", false, localC1366Tu1);
        this.paddingRight = paramaca.mo16534a(this.cdV, "padding-right", false, localC1366Tu1);
        this.colorMultiplier = paramaca.mo16554g(this.cdV, "color-multiplier", false);
        this.fontShadowX = paramaca.mo16534a(this.cdV, "font-shadow-x", false, localC1366Tu1);
        this.fontShadowY = paramaca.mo16534a(this.cdV, "font-shadow-y", false, localC1366Tu1);
        this.maxHeight = paramaca.mo16534a(this.cdV, "max-height", false, localC1366Tu1);
        this.maxWidth = paramaca.mo16534a(this.cdV, "max-width", false, localC1366Tu1);
        this.borderSpacing = paramaca.mo16547e(paramaca.mo3817a(this.cdV, "border-spacing", false));


        CSSValue localCSSValue2 = paramaca.mo3817a(this.cdV, "background-position", false);
        if (localCSSValue2 != null) {
            Object localObject5 = null;
            Object localObject6 = null;
            ArrayList localArrayList = new ArrayList();
            int n = 1;
            if (localCSSValue2.getCssValueType() == 2) {
                n = ((CSSValueList) localCSSValue2).getLength();
            }
            for (int i1 = 0; i1 < n; i1++) {
                CSSValue localCSSValue3;
                if (localCSSValue2.getCssValueType() == 2) {
                    localCSSValue3 = ((CSSValueList) localCSSValue2).item(i1);
                } else {
                    localCSSValue3 = localCSSValue2;
                }
                Object localObject7;
                if (localCSSValue3.getCssValueType() == 1) {
                    localObject7 = (CSSPrimitiveValue) localCSSValue3;
                    String str2 = ((CSSPrimitiveValue) localObject7).getCssText().toLowerCase();
                    C1366Tu localC1366Tu3 = (C1366Tu) cdt.get(str2);
                    if (localC1366Tu3 != null) {
                        if (("left".equals(str2)) || ("right".equals(str2))) {
                            localObject5 = localC1366Tu3;
                            if (localObject6 == null) {
                                localObject6 = new C1366Tu(50.0F, C1366Tu.C1367a.PERCENT);
                            }
                        }
                        if ("center".equals(str2)) {
                            if (localObject5 == null) {
                                localObject5 = localC1366Tu3;
                            }
                            if (localObject6 == null) {
                                localObject6 = localC1366Tu3;
                            }
                        }
                        if (("bottom".equals(str2)) || ("top".equals(str2))) {
                            localObject6 = localC1366Tu3;
                            if (localObject5 == null) {
                                localObject5 = new C1366Tu(50.0F, C1366Tu.C1367a.PERCENT);
                            }
                        }
                    } else if (i1 == 0) {
                        localObject5 = C6868avI.m26540c(localCSSValue3);
                    } else if (i1 == 1) {
                        localObject6 = C6868avI.m26540c(localCSSValue3);
                    }
                }
                if (((i1 & 0x1) != 0) || (i1 == n - 1)) {
                    localObject7 = new C2741jM(
                            localObject5 == null ? C1366Tu.fEE : (C1366Tu) localObject5,
                            localObject6 == null ? C1366Tu.fEE : (C1366Tu) localObject6);
                    localArrayList.add(localObject7);
                }
            }
            this.backgroundPosition = ((C2741jM[]) localArrayList.toArray(new C2741jM[localArrayList.size()]));
        }
        this.minHeight = paramaca.mo16534a(this.cdV, "min-height", false, localC1366Tu1);
        this.minWidth = paramaca.mo16534a(this.cdV, "min-width", false, localC1366Tu1);
        /*
        if ((this.backgroundColor != null) && (this.minWidth != null) && (this.backgroundColor != C1366Tu.fEC) && (this.minWidth != C1366Tu.fEC))
        {
            int k = (int)((C1366Tu)this.backgroundColor).value;
            int m = (int)this.minWidth.value;
            this.cei = new Dimension(m, k);
        }
        */
        this.height = paramaca.mo16534a(this.cdV, "height", false, localC1366Tu1);
        this.width = paramaca.mo16534a(this.cdV, "width", false, localC1366Tu1);
        this.top = paramaca.mo16534a(this.cdV, "top", false, localC1366Tu1);
        this.left = paramaca.mo16534a(this.cdV, "left", false, localC1366Tu1);

    }

    /* renamed from: a */
    private String m458a(CSSValue cSSValue) {
        if (cSSValue != null && cSSValue.getCssValueType() == 1) {
            return ((CSSPrimitiveValue) cSSValue).getStringValue();
        }
        return null;
    }

    /* renamed from: b */
    private C2741jM m460b(CSSValue cSSValue) {
        CSSValue cSSValue2;
        C1366Tu tu;
        C1366Tu tu2 = null;
        C1366Tu tu3 = null;
        for (int i = 0; i < 2; i++) {
            if (cSSValue.getCssValueType() != 2) {
                if (i > 0) {
                    break;
                }
                cSSValue2 = cSSValue;
            } else {
                cSSValue2 = ((CSSValueList) cSSValue).item(i);
            }
            if (cSSValue2.getCssValueType() == 1) {
                String lowerCase = ((CSSPrimitiveValue) cSSValue2).getCssText().toLowerCase();
                C1366Tu tu4 = cdt.get(lowerCase);
                if (tu4 != null) {
                    if (!"left".equals(lowerCase) && !"right".equals(lowerCase)) {
                        tu = tu2;
                    } else if (tu2 == null) {
                        tu = new C1366Tu(50.0f, C1366Tu.C1367a.PERCENT);
                        tu3 = tu4;
                    } else {
                        tu = tu2;
                        tu3 = tu4;
                    }
                    if ("center".equals(lowerCase)) {
                        if (tu3 == null) {
                            tu3 = tu4;
                        }
                        if (tu == null) {
                            tu = tu4;
                        }
                    }
                    if (!"bottom".equals(lowerCase) && !"top".equals(lowerCase)) {
                        tu2 = tu;
                    } else if (tu3 == null) {
                        tu3 = new C1366Tu(50.0f, C1366Tu.C1367a.PERCENT);
                        tu2 = tu4;
                    } else {
                        tu2 = tu4;
                    }
                } else if (i == 0) {
                    tu3 = C6868avI.m26540c(cSSValue2);
                } else if (i == 1) {
                    tu2 = C6868avI.m26540c(cSSValue2);
                }
            }
        }
        if (tu3 == null) {
            tu3 = C1366Tu.fEE;
        }
        if (tu2 == null) {
            tu2 = C1366Tu.fEE;
        }
        return new C2741jM(tu3, tu2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:1:0x0002, code lost:
        r0 = cdu.get(r3.getCssText().toLowerCase());
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private C3991xo m457a(CSSValue r3, C3991xo r4) {
        if (r3 != null) {
            String var3 = r3.getCssText().toLowerCase();
            C3991xo var4 = (C3991xo) cdu.get(var3);
            return var4 == null ? r4 : var4;
        } else {
            return r4;
        }
    }

    /* renamed from: a */
    private C3991xo[] m459a(CSSValue cSSValue, C3991xo[] xoVarArr) {
        if (cSSValue == null) {
            return xoVarArr;
        }
        if (cSSValue.getCssValueType() == 2) {
            CSSValueList cSSValueList = (CSSValueList) cSSValue;
            int length = cSSValueList.getLength();
            C3991xo[] xoVarArr2 = new C3991xo[length];
            for (int i = 0; i < length; i++) {
                xoVarArr2[i] = m457a(cSSValueList.item(i), C3991xo.STRETCH);
            }
            return xoVarArr2;
        }
        return new C3991xo[]{m457a(cSSValue, C3991xo.STRETCH)};
    }

    /* renamed from: a */
    public void mo369a(Image image) {
        if (this.backgroundImage == null) {
            this.backgroundImage = new Image[]{image};
            return;
        }
        this.backgroundImage[0] = image;
    }

    public int atT() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.backgroundImage != null) {
            return Vr.backgroundImage.length;
        }
        if (this.backgroundImage != null) {
            return this.backgroundImage.length;
        }
        return 0;
    }

    public ComponentOrientation getComponentOrientation() {
        return this.direction;
    }

    public String atU() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.mouseOverSound == null) {
            return this.mouseOverSound;
        }
        return Vp.mouseOverSound;
    }

    public String atV() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.mousePressSound == null) {
            return this.mousePressSound;
        }
        return Vp.mousePressSound;
    }

    public String atW() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.mouseReleaseSound == null) {
            return this.mouseReleaseSound;
        }
        return Vp.mouseReleaseSound;
    }

    public String atX() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.mouseClickSound == null) {
            return this.mouseClickSound;
        }
        return Vp.mouseClickSound;
    }

    public String atY() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.keyReleaseSound == null) {
            return this.keyReleaseSound;
        }
        return Vp.keyReleaseSound;
    }

    public String atZ() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.keyPressSound == null) {
            return this.keyPressSound;
        }
        return Vp.keyPressSound;
    }

    public String aua() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.keyTypedSound == null) {
            return this.keyTypedSound;
        }
        return Vp.keyTypedSound;
    }

    public String aub() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.actionSound == null) {
            return this.actionSound;
        }
        return Vp.actionSound;
    }

    public String auc() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.dropSucessSound == null) {
            return this.dropSucessSound;
        }
        return Vp.dropSucessSound;
    }

    public String aud() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.dropFailSound == null) {
            return this.dropFailSound;
        }
        return Vp.dropFailSound;
    }

    public String aue() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.dragStartSound == null) {
            return this.dragStartSound;
        }
        return Vp.dragStartSound;
    }

    public String auf() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.openSound == null) {
            return this.openSound;
        }
        return Vp.openSound;
    }

    public String aug() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.expandSound == null) {
            return this.expandSound;
        }
        return Vp.expandSound;
    }

    public String auh() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.closeSound == null) {
            return this.closeSound;
        }
        return Vp.closeSound;
    }

    public String aui() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.collapseSound == null) {
            return this.collapseSound;
        }
        return Vp.collapseSound;
    }

    public String auj() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.scrollSound == null) {
            return this.scrollSound;
        }
        return Vp.scrollSound;
    }

    public String auk() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.typeAnimationSound == null) {
            return this.typeAnimationSound;
        }
        return Vp.typeAnimationSound;
    }
}
