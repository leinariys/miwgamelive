package logic.ui;

import org.w3c.dom.css.CSSStyleDeclaration;

/* renamed from: a.RC */
/* compiled from: a */
public interface C1162RC extends C6738asi {
    /* renamed from: Vh */
    CSSStyleDeclaration mo5149Vh();

    /* renamed from: Vl */
    C6179ahv mo5150Vl();

    String getAttribute(String str);

    String getTextContent();

    boolean hasAttribute(String str);

    void setAttribute(String str, String str2);
}
