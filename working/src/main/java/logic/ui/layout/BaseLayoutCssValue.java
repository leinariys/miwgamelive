package logic.ui.layout;

import gnu.trove.TObjectIntHashMap;
import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;
import org.w3c.dom.css.CSSValueList;

import java.awt.*;

/* renamed from: a.aoF  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class BaseLayoutCssValue implements IBaseLayout {
    /* renamed from: iw */
    public Object mo8427iw(String str) {
        return str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public float mo15145a(CSSStyleDeclaration cSSStyleDeclaration, String str, float f) {
        return mo15146a(cSSStyleDeclaration.getPropertyCSSValue(str), f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int mo15148a(CSSStyleDeclaration cSSStyleDeclaration, String str, int i) {
        return (int) mo15146a(cSSStyleDeclaration.getPropertyCSSValue(str), (float) i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int mo15147a(CSSStyleDeclaration cSSStyleDeclaration, TObjectIntHashMap<String> tObjectIntHashMap, String str, int i) {
        String propertyValue = cSSStyleDeclaration.getPropertyValue(str);
        if (propertyValue != null) {
            String trim = propertyValue.replace('-', '_').toUpperCase().trim();
            if (tObjectIntHashMap.containsKey(trim)) {
                return tObjectIntHashMap.get(trim);
            }
        }
        return (int) mo15145a(cSSStyleDeclaration, str, (float) i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public double[] mo15150a(CSSStyleDeclaration cSSStyleDeclaration, String str, double[] dArr) {
        CSSValueList propertyCSSValue = (CSSValueList) cSSStyleDeclaration.getPropertyCSSValue(str);
        if (propertyCSSValue == null) {
            return dArr;
        }
        if (propertyCSSValue.getCssValueType() == 2) {
            CSSValueList cSSValueList = propertyCSSValue;
            double[] dArr2 = new double[cSSValueList.getLength()];
            for (int i = 0; i < cSSValueList.getLength(); i++) {
                dArr2[i] = (double) mo15146a(cSSValueList.item(i), 0.0f);
            }
            return dArr2;
        } else if (propertyCSSValue.getCssValueType() != 1) {
            return dArr;
        } else {
            return new double[]{(double) ((CSSPrimitiveValue) propertyCSSValue).getFloatValue((short) 0)};
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int[] mo15151a(CSSStyleDeclaration cSSStyleDeclaration, String str, int[] iArr) {
        CSSValueList propertyCSSValue = (CSSValueList) cSSStyleDeclaration.getPropertyCSSValue(str);
        if (propertyCSSValue == null) {
            return iArr;
        }
        if (propertyCSSValue.getCssValueType() == 2) {
            CSSValueList cSSValueList = propertyCSSValue;
            int[] iArr2 = new int[cSSValueList.getLength()];
            for (int i = 0; i < cSSValueList.getLength(); i++) {
                iArr2[i] = (int) mo15146a(cSSValueList.item(i), 0.0f);
            }
            return iArr2;
        } else if (propertyCSSValue.getCssValueType() != 1) {
            return iArr;
        } else {
            return new int[]{(int) ((CSSPrimitiveValue) propertyCSSValue).getFloatValue((short) 0)};
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public float mo15146a(CSSValue cSSValue, float f) {
        if (cSSValue == null || cSSValue.getCssValueType() != 1) {
            return f;
        }
        return ((CSSPrimitiveValue) cSSValue).getFloatValue((short) 0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Insets mo15149a(CSSStyleDeclaration cSSStyleDeclaration, String str, Insets insets) {
        CSSValueList propertyCSSValue = (CSSValueList) cSSStyleDeclaration.getPropertyCSSValue(str);
        if (propertyCSSValue instanceof CSSValueList) {
            Insets insets2 = new Insets(0, 0, 0, 0);
            CSSValueList cSSValueList = propertyCSSValue;
            if (cSSValueList.getLength() >= 4) {
                insets2.top = (int) mo15146a(cSSValueList.item(0), 0.0f);
                insets2.left = (int) mo15146a(cSSValueList.item(1), 0.0f);
                insets2.bottom = (int) mo15146a(cSSValueList.item(2), 0.0f);
                insets2.right = (int) mo15146a(cSSValueList.item(3), 0.0f);
                return insets2;
            } else if (cSSValueList.getLength() >= 2) {
                int a = (int) mo15146a(cSSValueList.item(0), 0.0f);
                insets2.bottom = a;
                insets2.top = a;
                int a2 = (int) mo15146a(cSSValueList.item(1), 0.0f);
                insets2.right = a2;
                insets2.left = a2;
                return insets2;
            } else if (cSSValueList.getLength() == 1) {
                int a3 = (int) mo15146a(cSSValueList.item(0), 0.0f);
                insets2.right = a3;
                insets2.left = a3;
                insets2.bottom = a3;
                insets2.top = a3;
                return insets2;
            } else {
                int a4 = mo15148a(cSSStyleDeclaration, str, 0);
                insets2.right = a4;
                insets2.left = a4;
                insets2.bottom = a4;
                insets2.top = a4;
                return insets2;
            }
        } else if (propertyCSSValue == null) {
            return insets;
        } else {
            Insets insets3 = new Insets(0, 0, 0, 0);
            int a5 = mo15148a(cSSStyleDeclaration, str, 0);
            insets3.right = a5;
            insets3.left = a5;
            insets3.bottom = a5;
            insets3.top = a5;
            return insets3;
        }
    }
}
