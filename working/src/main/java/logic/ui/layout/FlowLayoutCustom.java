package logic.ui.layout;


import com.steadystate.css.parser.CSSOMParser;
import gnu.trove.TObjectIntHashMap;
import lombok.extern.slf4j.Slf4j;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.CSSStyleDeclaration;

import java.awt.*;
import java.io.Reader;
import java.io.StringReader;

/* renamed from: a.eN */
/* compiled from: a */
@Slf4j
public class FlowLayoutCustom extends BaseLayoutCssValue {
    /* renamed from: oY */
    private static final TObjectIntHashMap<String> alignment = new TObjectIntHashMap<>();

    static {
        alignment.put("CENTER", 1);
        alignment.put("TRAILING", 4);
        alignment.put("LEADING", 3);
        alignment.put("LEFT", 0);
        alignment.put("RIGHT", 2);
    }

    /* renamed from: a */
    public LayoutManager createLayout(Container container, String str) {
        FlowLayout flowLayout = new FlowLayout();
        if (str != null) {
            String trim = str.trim();
            if (!trim.isEmpty()) {
                try {
                    CSSStyleDeclaration d = new CSSOMParser().parseStyleDeclaration(new InputSource((Reader) new StringReader(trim)));
                    flowLayout.setAlignment(mo15147a(d, alignment, "alignment", flowLayout.getAlignment()));
                    flowLayout.setHgap(mo15148a(d, "hgap", flowLayout.getHgap()));
                    flowLayout.setAlignOnBaseline("true".equals(d.getPropertyCSSValue("alignOnBaseline")));
                    flowLayout.setVgap(mo15148a(d, "vgap", flowLayout.getVgap()));
                } catch (Exception e) {
                    log.warn(String.valueOf(e));
                }
            }
        }
        return flowLayout;
    }
}
