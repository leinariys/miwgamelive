package logic.ui.layout;


import com.steadystate.css.parser.CSSOMParser;
import gnu.trove.TObjectIntHashMap;
import lombok.extern.slf4j.Slf4j;
import org.w3c.css.sac.InputSource;

import javax.swing.*;
import java.awt.*;
import java.io.Reader;
import java.io.StringReader;

/* renamed from: a.bI */
/* compiled from: a */
@Slf4j
public class BoxLayoutCustom extends BaseLayoutCssValue {
    /* renamed from: oY */
    private static final TObjectIntHashMap<String> location = new TObjectIntHashMap<>();

    static {
        location.put("X_AXIS", 0);
        location.put("Y_AXIS", 1);
        location.put("LINE_AXIS", 2);
        location.put("PAGE_AXIS", 3);
    }

    /* renamed from: a */
    public LayoutManager createLayout(Container container, String str) {
        int i = 0;
        if (str != null) {
            String trim = str.trim();
            if (!trim.isEmpty()) {
                try {
                    i = mo15147a(new CSSOMParser().parseStyleDeclaration(new InputSource((Reader) new StringReader(trim))), location, "axis", 0);
                } catch (Exception e) {
                    log.warn(String.valueOf(e));
                }
            }
        }
        return new BoxLayout(container, i);
    }
}
