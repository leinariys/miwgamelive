package logic.ui.layout;


import com.steadystate.css.parser.CSSOMParser;
import lombok.extern.slf4j.Slf4j;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.CSSStyleDeclaration;

import java.awt.*;
import java.io.Reader;
import java.io.StringReader;

/* renamed from: a.aDh  reason: case insensitive filesystem */
/* compiled from: a */
@Slf4j
public class GridLayoutCustom extends BaseLayoutCssValue {

    /* renamed from: iw */
    public Object mo8427iw(String str) {
        return str;
    }

    /* renamed from: a */
    public LayoutManager createLayout(Container container, String str) {
        GridLayout gridLayout = new GridLayout();
        if (str != null) {
            String trim = str.trim();
            if (!trim.isEmpty()) {
                try {
                    CSSStyleDeclaration d = new CSSOMParser().parseStyleDeclaration(new InputSource((Reader) new StringReader(trim)));
                    gridLayout.setHgap((int) mo15145a(d, "hgap", (float) gridLayout.getHgap()));
                    gridLayout.setVgap((int) mo15145a(d, "vgap", (float) gridLayout.getVgap()));
                    gridLayout.setColumns((int) mo15145a(d, "columns", (float) gridLayout.getColumns()));
                    gridLayout.setRows((int) mo15145a(d, "rows", (float) gridLayout.getRows()));
                } catch (Exception e) {
                    log.warn(String.valueOf(e));
                }
            }
        }
        return gridLayout;
    }
}
