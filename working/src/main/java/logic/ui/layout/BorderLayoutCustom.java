package logic.ui.layout;


import com.steadystate.css.parser.CSSOMParser;
import lombok.extern.slf4j.Slf4j;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.CSSStyleDeclaration;

import java.awt.*;
import java.io.Reader;
import java.io.StringReader;

/* renamed from: a.adA  reason: case insensitive filesystem */
/* compiled from: a */
@Slf4j
public class BorderLayoutCustom extends BaseLayoutCssValue {
    /* renamed from: a */
    public LayoutManager createLayout(Container container, String str) {
        BorderLayout borderLayout = new BorderLayout();
        if (str != null) {
            String trim = str.trim();
            if (!trim.isEmpty()) {
                try {
                    CSSStyleDeclaration d = new CSSOMParser().parseStyleDeclaration(new InputSource((Reader) new StringReader(trim)));
                    borderLayout.setHgap(mo15148a(d, "hgap", borderLayout.getHgap()));
                    borderLayout.setVgap(mo15148a(d, "vgap", borderLayout.getVgap()));
                } catch (Exception e) {
                    log.warn(String.valueOf(e));
                }
            }
        }
        return borderLayout;
    }
}
