package logic.ui;

import gnu.trove.THashMap;
import logic.res.css.C5576aOq;
import logic.res.css.C6868avI;
import logic.res.css.CSSStyleDeclarationImpl;
import logic.res.css.aKV;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: a.aca  reason: case insensitive filesystem */
/* compiled from: a */
public class C5898aca extends C6868avI {
    public Map<String, CSSValue> cache;
    public List<C6868avI> list;
    private IComponentManager fao;

    public C5898aca(IComponentManager aek, CSSStyleDeclaration cSSStyleDeclaration) {
        this.fao = aek;
        C1862a aVar = new C1862a(this, (C1862a) null);
        this.cache = new THashMap();
        if (this.fao != null && cSSStyleDeclaration != null) {
            aVar.mo9738c(cSSStyleDeclaration);
        }
    }

    public C5898aca(IComponentManager aek) {
        this.fao = aek;
        this.cache = new THashMap();
        this.list = new ArrayList(2);
        C1862a aVar = new C1862a(this, (C1862a) null);
        for (C6738asi asi = aek; asi != null; asi = asi.mo15974Vm()) {
            C6868avI Vj = ((IComponentManager) asi).mo13044Vj();
            if (Vj != null && !this.list.contains(Vj)) {
                this.list.add(Vj);
                Vj.mo3818a((C1162RC) this.fao, (aKV) aVar, false);
            }
        }
        C6868avI anv = IBaseUiTegXml.initBaseUItegXML().getCssNodeCore();
        if (anv != null && !this.list.contains(anv)) {
            this.list.add(anv);
            anv.mo3818a((C1162RC) this.fao, (aKV) aVar, false);
        }
    }

    /* renamed from: a */
    public CSSValue mo3817a(C1162RC rc, String str, boolean z) {
        CSSValue cSSValue = null;
        if (rc == this.fao) {
            cSSValue = this.cache.get(str);
        } else if (this.list != null) {
            for (C6868avI a : this.list) {
                cSSValue = a.mo3817a(rc, str, z);
                if (cSSValue != null) {
                    return cSSValue;
                }
            }
        }
        if (!z) {
            return cSSValue;
        }
        if ((cSSValue == null || "".equals(cSSValue.getCssText())) && rc.mo15974Vm() != null) {
            return mo3817a((C1162RC) rc.mo15974Vm(), str, z);
        }
        return cSSValue;
    }

    /* renamed from: a */
    public boolean mo3818a(C1162RC rc, aKV akv, boolean z) {
        return false;
    }

    public IComponentManager bOk() {
        return this.fao;
    }

    /* renamed from: b */
    public void mo12662b(IComponentManager aek) {
        this.fao = aek;
    }

    /* renamed from: a.aca$a */
    private final class C1862a implements aKV {
        private C1862a() {
        }

        /* synthetic */ C1862a(C5898aca aca, C1862a aVar) {
            this();
        }

        /* renamed from: c */
        public boolean mo9738c(CSSStyleDeclaration cSSStyleDeclaration) {
            if (cSSStyleDeclaration != null) {
                CSSStyleDeclarationImpl ahx = (CSSStyleDeclarationImpl) cSSStyleDeclaration;
                int length = cSSStyleDeclaration.getLength();
                while (true) {
                    length--;
                    if (length < 0) {
                        break;
                    }
                    C5576aOq yh = ahx.mo9298yh(length);
                    if (!(yh == null || yh.dnu() == null || C5898aca.this.cache.containsKey(yh.getName()))) {
                        C5898aca.this.cache.put(yh.getName(), yh.dnu());
                    }
                }
            }
            return true;
        }
    }
}
