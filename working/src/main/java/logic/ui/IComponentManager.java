package logic.ui;

import logic.res.css.C6868avI;
import org.w3c.dom.css.CSSStyleDeclaration;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

/* renamed from: a.aeK  reason: case insensitive filesystem */
/* compiled from: a */
public interface IComponentManager extends C1162RC {
    public static final String armed = "armed";
    public static final String pressed = "pressed";
    public static final String hover = "hover";
    public static final String over = "over";
    public static final String selected = "selected";
    public static final String disabled = "disabled";
    public static final String readOnly = "read-only";
    public static final Object cssHolder = "css.holder";
    public static final String expanded = "expanded";
    public static final String enabled = "enabled";
    public static final String rollover = "rollover";
    public static final String focused = "focused";

    /* renamed from: Vh */
    CSSStyleDeclaration mo5149Vh();

    /* renamed from: Vj */
    C6868avI mo13044Vj();

    /* renamed from: Vk */
    void mo13045Vk();

    /* renamed from: Vo */
    IComponentManager mo13046Vo();

    /* renamed from: Vp */
    PropertiesUiFromCss mo13047Vp();

    /* renamed from: Vq */
    void mo13048Vq();

    /* renamed from: Vr */
    PropertiesUiFromCss mo13049Vr();

    /* renamed from: Vs */
    boolean mo13050Vs();

    /* renamed from: Vt */
    boolean mo13051Vt();

    /* renamed from: a */
    Dimension mo13052a(JComponent jComponent, Dimension dimension);

    /* renamed from: a */
    void mo13053a(ComponentCheck aws);

    /* renamed from: a */
    void mo13054a(C6868avI avi);

    /* renamed from: aO */
    void mo13055aO(boolean z);

    /* renamed from: aP */
    void mo13056aP(boolean z);

    /* renamed from: b */
    Dimension mo13057b(JComponent jComponent, Dimension dimension);

    /* renamed from: ba */
    boolean mo13058ba(String str);

    /* renamed from: c */
    Dimension mo13059c(JComponent jComponent, Dimension dimension);

    float getAlpha();

    void setAlpha(float f);

    String getAttribute(String str);

    Color getColor();

    void setColor(Color color);

    Component getCurrentComponent();

    boolean hasAttribute(String str);

    /* renamed from: q */
    void mo13063q(int i, int i2);

    void setAttribute(String str, String str2);

    void setAttributes(Map<String, String> map);

    void setStyle(String str);
}
