package logic.ui;

import java.awt.*;

/* renamed from: a.Sr */
/* compiled from: a */
public interface C1276Sr {
    Rectangle getBounds();

    void setBounds(Rectangle rectangle);

    String getElementName();

    int getHeight();

    Point getLocation();

    void setLocation(Point point);

    Container getParent();

    Dimension getPreferredSize();

    Dimension getSize();

    void setSize(Dimension dimension);

    int getWidth();

    int getX();

    int getY();

    void invalidate();

    boolean isEnabled();

    void setEnabled(boolean z);

    boolean isVisible();

    void setVisible(boolean z);

    void setBounds(int i, int i2, int i3, int i4);

    void setLocation(int i, int i2);

    void setSize(int i, int i2);

    void validate();
}
