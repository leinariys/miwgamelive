package logic.ui;

import logic.res.XmlNode;
import logic.ui.item.BaseItem;
import logic.ui.item.IBaseItem;
import logic.ui.item.InternalFrame;
import logic.ui.item.Repeater;
import logic.ui.layout.IBaseLayout;
import lombok.extern.slf4j.Slf4j;

import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Базовые элементы интерфейса
 *
 * @param <ContainerType>
 */
/* renamed from: a.aol  reason: case insensitive filesystem */
/* compiled from: a */
@Slf4j
public abstract class BaseItemFactory<ContainerType extends Container> extends BaseItem<ContainerType> {

    /* renamed from: A */
    public ContainerType creatUi(C6342alC alc, Container container, XmlNode agy) {
        ContainerType con = super.creatUi(alc, container, agy);
        creatAllComponentCustom(alc, con, agy);
        return con;
    }

    /* access modifiers changed from: protected */
    /* renamed from: m */
    public void creatAllComponentCustom(C6342alC alc, ContainerType c, XmlNode agy) {
        for (XmlNode next : agy.getListChildrenTag()) {
            IBaseItem gC = alc.getClassBaseUi(next.getTagName().toLowerCase());
            if (gC != null) {
                Component d = gC.creatUi(alc, c, next);
                if (!(d instanceof Repeater)) {
                    createLayoutData(alc, c, d, next);
                } else if (c instanceof Panel) {
                    ((Panel) c).mo8234a((Repeater<?>) d);
                } else if (c instanceof Repeater) {
                    ((Repeater) c).mo22251a((Repeater<?>) d);
                } else if (c instanceof InternalFrame) {
                    ((InternalFrame) c).mo20865a((Repeater<?>) d);
                } else {
                    log.error("Container for Repeater must be a Panel, Repeater or Window.");
                }
            } else if ("import".equals(next.getTagName())) {
                try {
                    URL url = new URL(alc.getPathFile(), next.getAttribute("src"));
                    createLayoutData(alc, c, alc.getBaseUiTegXml().createJComponent((C6342alC) new MapKeyValue(alc, url), url), (XmlNode) null);
                } catch (MalformedURLException e) {
                    throw new RuntimeException(e);
                }
            } else {
                log.error("There is no component named '" + next.getTagName() + "'");
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void createLayout(C6342alC alc, ContainerType c, XmlNode agy) {
        String str = null;
        super.createLayout(alc, c, agy);
        String attribute = agy.getAttribute("layout");
        if (attribute != null) {
            if ("null".equals(attribute)) {
                c.setLayout((LayoutManager) null);
                return;
            }
            String trim = attribute.trim();
            int length = trim.length();
            int i = 0;
            while (true) {
                if (i >= length) {
                    i = -1;
                    break;
                }
                char charAt = trim.charAt(i);
                if (charAt == '{' || charAt == ' ' || charAt == '(') {
                    break;
                }
                i++;
            }
            if (i != -1) {
                str = trim.substring(i);
                trim = trim.substring(0, i).trim();
            }
            IBaseLayout gD = alc.getClassLayoutUi(trim);
            if (gD != null) {
                c.setLayout(gD.createLayout(c, str));
                IComponentManager e = ComponentManager.getCssHolder(c);
                if (e != null) {
                    e.setAttribute("layoutManager", trim);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void createLayoutData(C6342alC alc, ContainerType container, Component component, XmlNode agy) {
        IComponentManager e;
        IBaseLayout gD;
        LayoutManager layout = container.getLayout();
        if (agy == null || layout == null) {
            container.add(component);
            component.setFocusable(container.isFocusable());
            component.setEnabled(container.isEnabled());
            return;
        }
        String attribute = agy.getAttribute("layoutData");
        if (attribute == null || (e = ComponentManager.getCssHolder(container)) == null || (gD = alc.getClassLayoutUi(e.getAttribute("layoutManager"))) == null) {
            container.add(component, attribute);
            if (!container.isFocusable()) {
                component.setFocusable(false);
            }
            if (!container.isEnabled()) {
                component.setEnabled(false);
                return;
            }
            return;
        }
        container.add(component, gD.mo8427iw(attribute));
    }
}
