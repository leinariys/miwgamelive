package logic.ui;

import logic.swing.C3940wz;
import logic.ui.item.Progress;
import logic.ui.item.Repeater;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: a.aCo  reason: case insensitive filesystem */
/* compiled from: a */
public class Panel extends JPanel implements C2698il {

    public List<Repeater<?>> buZ = new ArrayList();
    private C2698il.C2699a bJg;
    private Rectangle hrp;

    public Panel() {
        setBackground((Color) null);
        this.bJg = new C2698il.C2699a(this);
    }

    public Panel(LayoutManager layoutManager) {
        super(layoutManager);
        setBackground((Color) null);
    }

    public void paint(Graphics graphics) {
        if (this.hrp != null) {
            graphics.setClip(this.hrp.x, this.hrp.y, this.hrp.width, this.hrp.height);
        }
        Panel.super.paint(graphics);
        if (this.bJg != null) {
            this.bJg.paint(graphics);
        }
    }

    /* renamed from: c */
    public void mo8237c(Rectangle rectangle) {
        this.hrp = rectangle;
    }

    public void center() {
        if (getParent() != null) {
            setLocation((getParent().getWidth() - getWidth()) / 2, (getParent().getHeight() - getHeight()) / 2);
        }
    }

    /* renamed from: d */
    public void mo8241d(int i, int i2, int i3, int i4) {
        if (this.hrp == null) {
            this.hrp = new Rectangle(i, i2, i3, i4);
            return;
        }
        this.hrp.x = i;
        this.hrp.y = i2;
        this.hrp.width = i3;
        this.hrp.height = i4;
    }

    public Rectangle cPL() {
        return this.hrp;
    }

    public void destroy() {
        if (getParent() != null) {
            getParent().remove(this);
        }
    }

    /* renamed from: cg */
    public Progress mo4918cg(String str) {
        Progress cg;
        for (Component bnVar : getComponents()) {
            if ((bnVar instanceof Progress) && str.equals(bnVar.getName())) {
                return (Progress) bnVar;
            }
            if ((bnVar instanceof C2698il) && (cg = ((C2698il) bnVar).mo4918cg(str)) != null) {
                return cg;
            }
        }
        return null;
    }

    /* renamed from: cb */
    public JButton mo4913cb(String str) {
        JButton cb;
        for (Component jButton : getComponents()) {
            if ((jButton instanceof JButton) && str.equals(jButton.getName())) {
                return (JButton) jButton;
            }
            if ((jButton instanceof C2698il) && (cb = ((C2698il) jButton).mo4913cb(str)) != null) {
                return cb;
            }
        }
        return null;
    }

    /* renamed from: cd */
    public <T extends Component> T mo4915cd(String str) {
        T cd;
        Iterator<Repeater<?>> it = this.buZ.iterator();
        while (it.hasNext()) {
            T t = (T) it.next();
            if (str.equals(t.getName())) {
                return t;
            }
        }
        for (Component t2 : getComponents()) {
            if (str.equals(t2.getName())) {
                return (T) t2;
            }
            if ((t2 instanceof C2698il) && (cd = ((C2698il) t2).mo4915cd(str)) != null) {
                return cd;
            }
        }
        return null;
    }

    /* renamed from: ci */
    public logic.ui.item.TextField mo4920ci(String str) {
        logic.ui.item.TextField ci;
        for (Component ahw : getComponents()) {
            if ((ahw instanceof TextField) && str.equals(ahw.getName())) {
                return (logic.ui.item.TextField) ahw;
            }
            if ((ahw instanceof C2698il) && (ci = ((C2698il) ahw).mo4920ci(str)) != null) {
                return ci;
            }
        }
        return null;
    }

    /* renamed from: cf */
    public JLabel mo4917cf(String str) {
        JLabel cf;
        for (Component jLabel : getComponents()) {
            if ((jLabel instanceof JLabel) && str.equals(jLabel.getName())) {
                return (JLabel) jLabel;
            }
            if ((jLabel instanceof C2698il) && (cf = ((C2698il) jLabel).mo4917cf(str)) != null) {
                return cf;
            }
        }
        return null;
    }

    /* renamed from: ce */
    public C2698il mo4916ce(String str) {
        for (Component ilVar : getComponents()) {
            if (ilVar instanceof C2698il) {
                if (str.equals(ilVar.getName())) {
                    return (C2698il) ilVar;
                }
                C2698il ce = ((C2698il) ilVar).mo4916ce(str);
                if (ce != null) {
                    return ce;
                }
            }
        }
        return null;
    }

    /* renamed from: ch */
    public Repeater<?> mo4919ch(String str) {
        Repeater<?> ch;
        if (str == null) {
            return null;
        }
        for (Repeater<?> next : this.buZ) {
            if (str.equals(next.getName())) {
                return next;
            }
        }
        for (Component ilVar : getComponents()) {
            if ((ilVar instanceof C2698il) && (ch = ((C2698il) ilVar).mo4919ch(str)) != null) {
                return ch;
            }
        }
        return null;
    }

    /* renamed from: a */
    public void mo8234a(Repeater<?> tYVar) {
        this.buZ.add(tYVar);
    }

    /* renamed from: cc */
    public JComboBox mo4914cc(String str) {
        JComboBox cc;
        for (Component jComboBox : getComponents()) {
            if ((jComboBox instanceof JComboBox) && str.equals(jComboBox.getName())) {
                return (JComboBox) jComboBox;
            }
            if ((jComboBox instanceof C2698il) && (cc = ((C2698il) jComboBox).mo4914cc(str)) != null) {
                return cc;
            }
        }
        return null;
    }

    public void pack() {
        try {
            setSize(getPreferredSize());
            validate();
        } catch (NullPointerException e) {
        }
    }

    public Component add(Component component) {
        return component instanceof Repeater ? component : Panel.super.add(component);
    }

    public String getElementName() {
        return "panel";
    }

    public void setEnabled(boolean z) {
        for (Component enabled : getComponents()) {
            enabled.setEnabled(z);
        }
        Panel.super.setEnabled(z);
    }

    public void setFocusable(boolean z) {
        for (Component focusable : getComponents()) {
            focusable.setFocusable(z);
        }
        Panel.super.setFocusable(z);
    }

    public void validate() {
        Panel.super.validate();
        if (!isValid()) {
            synchronized (getTreeLock()) {
                if (!isValid()) {
                    validateTree();
                }
            }
        }
    }

    public void removeNotify() {
        if (getParent() != null) {
            getParent().repaint(getX(), getY(), getWidth(), getHeight());
        }
        Panel.super.removeNotify();
    }

    public void remove(int i) {
        Component component = getComponent(i);
        if (component != null) {
            repaint(component.getX(), component.getY(), component.getWidth(), component.getHeight());
        }
        Panel.super.remove(i);
    }

    public int getAlpha() {
        Color colorMultiplier;
        PropertiesUiFromCss g = PropertiesUiFromCss.m462g(this);
        if (g == null || (colorMultiplier = g.getColorMultiplier()) == null) {
            return 255;
        }
        return colorMultiplier.getAlpha();
    }

    public void setAlpha(int i) {
        PropertiesUiFromCss g = PropertiesUiFromCss.m462g(this);
        if (g != null) {
            g.setColorMultiplier(new Color(i, i, i, i));
        }
    }

    public void validateTree() {
        try {
            Panel.super.validateTree();
        } catch (NullPointerException e) {
        }
    }

    public C3940wz cpV() {
        return (C3940wz) getClientProperty(C3940wz.bDU);
    }

    /* renamed from: a */
    public void mo8235a(C3940wz wzVar) {
        putClientProperty(C3940wz.bDU, wzVar);
    }

    /* renamed from: Kk */
    public void mo4911Kk() {
        this.bJg.mo19774Kk();
    }

    /* renamed from: Kl */
    public void mo4912Kl() {
        this.bJg.mo19775Kl();
    }
}
