package logic;

import taikodom.render.loader.provider.FilePath;

/* renamed from: a.aWb */
/* compiled from: a */
public interface IWrapEngGameAddonManager {
    /* renamed from: a */
    IAddonSettings initAddonSetting(IAddonManager addonManager, IWrapFileXmlOrJar fileXmlOrJar, IAddonExecutor<IAddonSettings> addonExecutor);

    C6245ajJ getEventManager();

    FilePath getRootPathRender();

    FilePath getRootPath();

    /* renamed from: b */
    void setAddonManager(IAddonManager axz);

    boolean isPlayerNotNull();

    Object getRoot();

    int getScreenHeight();

    int getScreenWidth();
}
