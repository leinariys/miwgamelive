package logic.res.css;

import org.w3c.css.sac.*;

/* renamed from: a.ZE */
/* compiled from: a */
public interface C1704ZE {
    /* renamed from: a */
    C0546Hb mo7324a(Selector awz, SimpleSelector kw);

    /* renamed from: a */
    ConditionalSelector mo7325a(SimpleSelector kw, Condition aib);

    /* renamed from: a */
    C1613Xc mo7326a(short s, Selector awz, SimpleSelector kw);

    /* renamed from: a */
    NegativeSelector mo7327a(SimpleSelector kw);

    SimpleSelector act();

    SimpleSelector acu();

    /* renamed from: b */
    C0546Hb mo7330b(Selector awz, SimpleSelector kw);

    /* renamed from: bK */
    CharacterDataSelector mo7331bK(String str);

    /* renamed from: bL */
    CharacterDataSelector mo7332bL(String str);

    /* renamed from: bM */
    CharacterDataSelector mo7333bM(String str);

    /* renamed from: m */
    ElementSelector mo7334m(String str, String str2);

    /* renamed from: n */
    ProcessingInstructionSelector mo7335n(String str, String str2);

    /* renamed from: o */
    ElementSelector mo7336o(String str, String str2);
}
