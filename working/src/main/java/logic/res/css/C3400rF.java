package logic.res.css;

import org.w3c.css.sac.AttributeCondition;

import java.io.Serializable;

/* renamed from: a.rF */
/* compiled from: a */
public class C3400rF implements AttributeCondition, Serializable {

    /* renamed from: LN */
    private String f8974LN;

    public C3400rF(String str) {
        this.f8974LN = str;
    }

    /* renamed from: rg */
    public short getConditionType() {
        return 9;
    }

    public String getNamespaceURI() {
        return null;
    }

    public String getLocalName() {
        return null;
    }

    public boolean getSpecified() {
        return true;
    }

    public String getValue() {
        return this.f8974LN;
    }

    public String toString() {
        return "." + getValue();
    }
}
