package logic.res.css;

import com.steadystate.css.dom.CSSStyleSheetImpl;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleSheet;
import org.w3c.dom.css.CSSUnknownRule;

import java.io.Serializable;

/* renamed from: a.aTF */
/* compiled from: a */
public class CSSUnknownRuleImpl implements Serializable, CSSUnknownRule {
    String text_ = null;

    /* renamed from: qq */
    CSSStyleSheetImpl parentStyleSheet = null;

    /* renamed from: qr */
    CSSRule parentRule = null;

    public CSSUnknownRuleImpl(CSSStyleSheetImpl parentStyleSheet, CSSRule parentRule, String str) {
        this.parentStyleSheet = parentStyleSheet;
        this.parentRule = parentRule;
        this.text_ = str;
    }

    public short getType() {
        return UNKNOWN_RULE;
    }

    public String getCssText() {
        return this.text_;
    }

    public void setCssText(String str) {
    }

    public CSSStyleSheet getParentStyleSheet() {
        return this.parentStyleSheet;
    }

    public CSSRule getParentRule() {
        return this.parentRule;
    }

    public String toString() {
        return getCssText();
    }
}
