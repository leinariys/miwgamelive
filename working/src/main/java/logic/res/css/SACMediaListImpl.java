package logic.res.css;

import java.util.Vector;

/* renamed from: a.asq  reason: case insensitive filesystem */
/* compiled from: a */
public class SACMediaListImpl implements SACMediaList {
    private Vector mediaQueries_ = new Vector(10, 10);

    public int getLength() {
        return this.mediaQueries_.size();
    }

    public String item(int i) {
        return (String) this.mediaQueries_.elementAt(i);
    }

    public void add(String str) {
        this.mediaQueries_.addElement(str);
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        int length = getLength();
        for (int i = 0; i < length; i++) {
            stringBuffer.append(item(i));
            if (i < length - 1) {
                stringBuffer.append(", ");
            }
        }
        return stringBuffer.toString();
    }
}
