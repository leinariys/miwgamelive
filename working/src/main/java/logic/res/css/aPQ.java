package logic.res.css;

import org.w3c.css.sac.ElementSelector;

import java.io.Serializable;

/* renamed from: a.aPQ */
/* compiled from: a */
public class aPQ implements ElementSelector, Serializable {
    private String _localName;

    public aPQ(String str) {
        this._localName = str;
    }

    public short getSelectorType() {
        return 9;
    }

    public String getNamespaceURI() {
        return null;
    }

    public String getLocalName() {
        return this._localName;
    }
}
