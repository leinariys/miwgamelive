package logic.res.css;


import com.steadystate.css.parser.CSSOMParser;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleDeclaration;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;

/* renamed from: a.Xy */
/* compiled from: a */
public class LoaderCss {
    /* renamed from: b */
    public C6868avI loadCSS(URL pathFile) throws IOException {
        CssNode akf = new CssNode();
        new C0325EQ(akf);
        if (pathFile == null) {
            throw new NullPointerException("Null url at CssLoaderImpl.loadCSS()");
        }
        akf.loadFileCSS(pathFile.openStream());
        return akf;
    }

    /* renamed from: a */
    public C6868avI mo6902a(Object obj, File file) {
        try {
            return loadCSS(file.toURI().toURL());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Разбор адреса ресурса ядра CSS стилей
     *
     * @param obj      null или  file:/C://addon/debugtools/taikodomversion/taikodomversion.xml
     * @param pathFile res://data/styles/core.css или taikodomversion.css
     * @return
     */
    /* renamed from: a */
    public C6868avI loadFileCSS(Object obj, String pathFile) {
        try {
            if (obj instanceof File) {
                return loadCSS(new File(((File) obj).getParent(), pathFile).toURI().toURL());
            }
            if (pathFile.indexOf("://") >= 0) {
                return loadCSS(new URL(pathFile));
            }
            if (obj instanceof Class) {
                return loadCSS(((Class) obj).getResource(pathFile));
            }
            if (obj instanceof URL) {
                return loadCSS(new URL((URL) obj, pathFile));
            }
            return loadCSS(obj.getClass().getResource(pathFile));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: aY */
    public CSSStyleDeclaration mo6904aY(String str) {
        try {
            CSSStyleDeclaration d = new CSSOMParser().parseStyleDeclaration(new InputSource(new StringReader("{" + str + "}")));
        if (d == null) {
            return d;
        }
        C0325EQ.m2829a(d);
        return d;
        } catch (IOException e) {
            return new CSSStyleDeclarationImpl((CSSRule) null);
        }
    }
}
