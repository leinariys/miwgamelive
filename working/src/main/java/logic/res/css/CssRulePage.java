package logic.res.css;

import com.steadystate.css.dom.CSSStyleSheetImpl;
import com.steadystate.css.parser.CSSOMParser;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.CSSPageRule;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSStyleSheet;

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;

/* renamed from: a.aie  reason: case insensitive filesystem */
/* compiled from: a */
public class CssRulePage implements Serializable, CSSPageRule {
    private String fMO = null;
    private String fMP = null;
    private CSSStyleDeclaration fMQ = null;

    /* renamed from: qq */
    private CSSStyleSheetImpl f4659qq = null;

    /* renamed from: qr */
    private CSSRule f4660qr = null;

    public CssRulePage(CSSStyleSheetImpl ss, CSSRule cSSRule, String str, String str2) {
        this.f4659qq = ss;
        this.f4660qr = cSSRule;
        this.fMO = str;
        this.fMP = str2;
    }

    public short getType() {
        return 6;
    }

    public String getCssText() {
        String selectorText = getSelectorText();
        return "@page " + selectorText + (selectorText.length() > 0 ? " " : "") + getStyle().getCssText();
    }

    public void setCssText(String str) {
        if (this.f4659qq == null || !this.f4659qq.isReadOnly()) {
            try {
                CssRulePage f = (CssRulePage) new CSSOMParser().parseRule(new InputSource((Reader) new StringReader(str)));
                if (f.getType() == 6) {
                    this.fMO = f.fMO;
                    this.fMP = f.fMP;
                    this.fMQ = f.fMQ;
                    return;
                }
                throw new C3331qN(13, 9);
            } catch (CSSException | IOException e) {
                throw new C3331qN((short) 12, 0, e.getMessage());
            }
        } else {
            throw new C3331qN(7, 2);
        }
    }

    public CSSStyleSheet getParentStyleSheet() {
        return this.f4659qq;
    }

    public CSSRule getParentRule() {
        return this.f4660qr;
    }

    public String getSelectorText() {
        return String.valueOf(this.fMO != null ? this.fMO : "") + (this.fMP != null ? ":" + this.fMP : "");
    }

    public void setSelectorText(String str) {
    }

    public CSSStyleDeclaration getStyle() {
        return this.fMQ;
    }

    /* access modifiers changed from: protected */
    /* renamed from: iN */
    public void mo13772iN(String str) {
        this.fMO = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: iO */
    public void mo13773iO(String str) {
        this.fMP = str;
    }

    /* renamed from: a */
    public void mo13765a(CSSStyleDeclarationImpl ahx) {
        this.fMQ = ahx;
    }
}
