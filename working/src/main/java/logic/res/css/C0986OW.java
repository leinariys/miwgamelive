package logic.res.css;

import com.steadystate.css.parser.CharStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/* renamed from: a.OW */
/* compiled from: a */
public final class C0986OW implements CharStream {
    public static final boolean staticFlag = false;
    public int bufpos;
    int bufsize;
    int dPk;
    int dPl;
    private int[] bufcolumn;
    private char[] buffer;
    private int[] bufline;
    private int column;
    private int inBuf;
    private Reader inputStream;
    private int line;
    private int maxNextCharInd;
    private boolean prevCharIsCR;
    private boolean prevCharIsLF;

    public C0986OW(Reader reader, int i, int i2, int i3) {
        this.bufpos = -1;
        this.column = 0;
        this.line = 1;
        this.prevCharIsCR = false;
        this.prevCharIsLF = false;
        this.maxNextCharInd = 0;
        this.inBuf = 0;
        this.inputStream = reader;
        this.line = i;
        this.column = i2 - 1;
        this.bufsize = i3;
        this.dPk = i3;
        this.buffer = new char[i3];
        this.bufline = new int[i3];
        this.bufcolumn = new int[i3];
    }

    public C0986OW(Reader reader, int i, int i2) {
        this(reader, i, i2, 4096);
    }

    public C0986OW(InputStream inputStream2, int i, int i2, int i3) {
        this((Reader) new InputStreamReader(inputStream2), i, i2, 4096);
    }

    public C0986OW(InputStream inputStream2, int i, int i2) {
        this(inputStream2, i, i2, 4096);
    }

    private final void ExpandBuff(boolean z) {
        char[] cArr = new char[(this.bufsize + 2048)];
        int[] iArr = new int[(this.bufsize + 2048)];
        int[] iArr2 = new int[(this.bufsize + 2048)];
        if (z) {
            try {
                System.arraycopy(this.buffer, this.dPl, cArr, 0, this.bufsize - this.dPl);
                System.arraycopy(this.buffer, 0, cArr, this.bufsize - this.dPl, this.bufpos);
                this.buffer = cArr;
                System.arraycopy(this.bufline, this.dPl, iArr, 0, this.bufsize - this.dPl);
                System.arraycopy(this.bufline, 0, iArr, this.bufsize - this.dPl, this.bufpos);
                this.bufline = iArr;
                System.arraycopy(this.bufcolumn, this.dPl, iArr2, 0, this.bufsize - this.dPl);
                System.arraycopy(this.bufcolumn, 0, iArr2, this.bufsize - this.dPl, this.bufpos);
                this.bufcolumn = iArr2;
                int i = this.bufpos + (this.bufsize - this.dPl);
                this.bufpos = i;
                this.maxNextCharInd = i;
            } catch (Throwable th) {
                throw new Error(th.getMessage());
            }
        } else {
            System.arraycopy(this.buffer, this.dPl, cArr, 0, this.bufsize - this.dPl);
            this.buffer = cArr;
            System.arraycopy(this.bufline, this.dPl, iArr, 0, this.bufsize - this.dPl);
            this.bufline = iArr;
            System.arraycopy(this.bufcolumn, this.dPl, iArr2, 0, this.bufsize - this.dPl);
            this.bufcolumn = iArr2;
            int i2 = this.bufpos - this.dPl;
            this.bufpos = i2;
            this.maxNextCharInd = i2;
        }
        this.bufsize += 2048;
        this.dPk = this.bufsize;
        this.dPl = 0;
    }

    private final void FillBuff() throws IOException {
        if (this.maxNextCharInd == this.dPk) {
            if (this.dPk == this.bufsize) {
                if (this.dPl > 2048) {
                    this.maxNextCharInd = 0;
                    this.bufpos = 0;
                    this.dPk = this.dPl;
                } else if (this.dPl < 0) {
                    this.maxNextCharInd = 0;
                    this.bufpos = 0;
                } else {
                    ExpandBuff(false);
                }
            } else if (this.dPk > this.dPl) {
                this.dPk = this.bufsize;
            } else if (this.dPl - this.dPk < 2048) {
                ExpandBuff(true);
            } else {
                this.dPk = this.dPl;
            }
        }
        try {
            int read = this.inputStream.read(this.buffer, this.maxNextCharInd, this.dPk - this.maxNextCharInd);
            if (read == -1) {
                this.inputStream.close();
                throw new IOException();
            } else {
                this.maxNextCharInd = read + this.maxNextCharInd;
            }
        } catch (IOException e) {
            this.bufpos--;
            backup(0);
            if (this.dPl == -1) {
                this.dPl = this.bufpos;
            }
            throw e;
        }
    }

    public final char BeginToken() throws IOException {
        this.dPl = -1;
        char readChar = readChar();
        this.dPl = this.bufpos;
        return readChar;
    }

    private final void UpdateLineColumn(char c) {
        this.column++;
        if (this.prevCharIsLF) {
            this.prevCharIsLF = false;
            int i = this.line;
            this.column = 1;
            this.line = i + 1;
        } else if (this.prevCharIsCR) {
            this.prevCharIsCR = false;
            if (c == 10) {
                this.prevCharIsLF = true;
            } else {
                int i2 = this.line;
                this.column = 1;
                this.line = i2 + 1;
            }
        }
        switch (c) {
            case 9:
                this.column--;
                this.column += 8 - (this.column & 7);
                break;
            case 10:
                this.prevCharIsLF = true;
                break;
            case 13:
                this.prevCharIsCR = true;
                break;
        }
        this.bufline[this.bufpos] = this.line;
        this.bufcolumn[this.bufpos] = this.column;
    }

    public final char readChar() throws IOException {
        int i;
        if (this.inBuf > 0) {
            this.inBuf--;
            char[] cArr = this.buffer;
            if (this.bufpos == this.bufsize - 1) {
                i = 0;
                this.bufpos = 0;
            } else {
                i = this.bufpos + 1;
                this.bufpos = i;
            }
            return (char) (cArr[i] & 255);
        }
        int i2 = this.bufpos + 1;
        this.bufpos = i2;
        if (i2 >= this.maxNextCharInd) {
            FillBuff();
        }
        char c = (char) (this.buffer[this.bufpos] & 255);
        UpdateLineColumn(c);
        return c;
    }

    public final int getColumn() {
        return this.bufcolumn[this.bufpos];
    }

    public final int getLine() {
        return this.bufline[this.bufpos];
    }

    public final int getEndColumn() {
        return this.bufcolumn[this.bufpos];
    }

    public final int getEndLine() {
        return this.bufline[this.bufpos];
    }

    public final int getBeginColumn() {
        return this.bufcolumn[this.dPl];
    }

    public final int getBeginLine() {
        return this.bufline[this.dPl];
    }

    public final void backup(int i) {
        this.inBuf += i;
        int i2 = this.bufpos - i;
        this.bufpos = i2;
        if (i2 < 0) {
            this.bufpos += this.bufsize;
        }
    }

    public void ReInit(Reader reader, int i, int i2, int i3) {
        this.inputStream = reader;
        this.line = i;
        this.column = i2 - 1;
        if (this.buffer == null || i3 != this.buffer.length) {
            this.bufsize = i3;
            this.dPk = i3;
            this.buffer = new char[i3];
            this.bufline = new int[i3];
            this.bufcolumn = new int[i3];
        }
        this.prevCharIsCR = false;
        this.prevCharIsLF = false;
        this.maxNextCharInd = 0;
        this.inBuf = 0;
        this.dPl = 0;
        this.bufpos = -1;
    }

    public void ReInit(Reader reader, int i, int i2) {
        ReInit(reader, i, i2, 4096);
    }

    public void ReInit(InputStream inputStream2, int i, int i2, int i3) {
        ReInit((Reader) new InputStreamReader(inputStream2), i, i2, 4096);
    }

    public void ReInit(InputStream inputStream2, int i, int i2) {
        ReInit(inputStream2, i, i2, 4096);
    }

    public final String GetImage() {
        if (this.bufpos >= this.dPl) {
            return new String(this.buffer, this.dPl, (this.bufpos - this.dPl) + 1);
        }
        return String.valueOf(new String(this.buffer, this.dPl, this.bufsize - this.dPl)) + new String(this.buffer, 0, this.bufpos + 1);
    }

    public final char[] GetSuffix(int i) {
        char[] cArr = new char[i];
        if (this.bufpos + 1 >= i) {
            System.arraycopy(this.buffer, (this.bufpos - i) + 1, cArr, 0, i);
        } else {
            System.arraycopy(this.buffer, this.bufsize - ((i - this.bufpos) - 1), cArr, 0, (i - this.bufpos) - 1);
            System.arraycopy(this.buffer, 0, cArr, (i - this.bufpos) - 1, this.bufpos + 1);
        }
        return cArr;
    }

    public void Done() {
        this.buffer = null;
        this.bufline = null;
        this.bufcolumn = null;
    }

    public void adjustBeginLineColumn(int i, int i2) {
        int i3;
        int i4 = 0;
        int i5 = this.dPl;
        if (this.bufpos >= this.dPl) {
            i3 = (this.bufpos - this.dPl) + this.inBuf + 1;
        } else {
            i3 = (this.bufsize - this.dPl) + this.bufpos + 1 + this.inBuf;
        }
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        while (i4 < i3) {
            int[] iArr = this.bufline;
            i7 = i5 % this.bufsize;
            int i9 = iArr[i7];
            int[] iArr2 = this.bufline;
            i5++;
            int i10 = i5 % this.bufsize;
            if (i9 != iArr2[i10]) {
                break;
            }
            this.bufline[i7] = i;
            int i11 = (this.bufcolumn[i10] + i6) - this.bufcolumn[i7];
            this.bufcolumn[i7] = i6 + i2;
            i8 = i4 + 1;
            i6 = i11;
        }
        if (i4 < i3) {
            int i12 = i + 1;
            this.bufline[i7] = i;
            this.bufcolumn[i7] = i6 + i2;
            int i13 = i5;
            while (true) {
                int i14 = i4 + 1;
                if (i4 >= i3) {
                    break;
                }
                int[] iArr3 = this.bufline;
                i7 = i13 % this.bufsize;
                i13++;
                if (iArr3[i7] != this.bufline[i13 % this.bufsize]) {
                    this.bufline[i7] = i12;
                    i4 = i14;
                    i12++;
                } else {
                    this.bufline[i7] = i12;
                    i4 = i14;
                }
            }
        }
        int i15 = i7;
        this.line = this.bufline[i15];
        this.column = this.bufcolumn[i15];
    }
}
