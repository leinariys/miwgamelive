package logic.res.css;

import org.w3c.dom.css.CSSValue;

import java.io.Serializable;

/* renamed from: a.aOq  reason: case insensitive filesystem */
/* compiled from: a */
public class C5576aOq implements Serializable {
    private String _name;
    private CSSValue iyS;
    private boolean iyT;

    public C5576aOq(String str, CSSValue cSSValue, boolean z) {
        this._name = str.intern();
        this.iyS = cSSValue;
        this.iyT = z;
    }

    public String getName() {
        return this._name;
    }

    public CSSValue dnu() {
        return this.iyS;
    }

    public boolean dnv() {
        return this.iyT;
    }

    /* renamed from: f */
    public void mo10680f(CSSValue cSSValue) {
        this.iyS = cSSValue;
    }

    /* renamed from: jU */
    public void mo10682jU(boolean z) {
        this.iyT = z;
    }

    public String toString() {
        return String.valueOf(this._name) + ": " + this.iyS.toString() + (this.iyT ? " !important" : "");
    }
}
