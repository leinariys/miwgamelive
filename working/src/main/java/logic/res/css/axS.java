package logic.res.css;

import java.util.HashMap;
import java.util.Map;

/* renamed from: a.axS */
/* compiled from: a */
public enum axS {
    INHERIT,
    STATIC,
    ABSOLUTE,
    FIXED,
    RELATIVE;

    static Map<String, axS> gRs = null;

    static {
        gRs = new HashMap();
        gRs.put("inherit", INHERIT);
        gRs.put("static", STATIC);
        gRs.put("absolute", ABSOLUTE);
        gRs.put("fixed", FIXED);
        gRs.put("relative", RELATIVE);
    }

    /* renamed from: kE */
    public static axS m27082kE(String str) {
        return gRs.get(str);
    }
}
