package logic.res.css;

import org.w3c.css.sac.AttributeCondition;

import java.io.Serializable;

/* renamed from: a.UC */
/* compiled from: a */
public class C1378UC implements AttributeCondition, Serializable {

    /* renamed from: LN */
    private String f1733LN;

    public C1378UC(String str) {
        this.f1733LN = str;
    }

    /* renamed from: rg */
    public short getConditionType() {
        return 10;
    }

    public String getNamespaceURI() {
        return null;
    }

    public String getLocalName() {
        return null;
    }

    public boolean getSpecified() {
        return true;
    }

    public String getValue() {
        return this.f1733LN;
    }

    public String toString() {
        return ":" + getValue();
    }
}
