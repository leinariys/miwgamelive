package logic.res.css;

import org.w3c.css.sac.Selector;
import org.w3c.css.sac.SimpleSelector;

import java.io.Serializable;

/* renamed from: a.ava  reason: case insensitive filesystem */
/* compiled from: a */
public class C6886ava implements C1613Xc, Serializable {
    private short gFB;
    private Selector gFC;
    private SimpleSelector gFD;

    public C6886ava(short s, Selector awz, SimpleSelector kw) {
        this.gFB = s;
        this.gFC = awz;
        this.gFD = kw;
    }

    public short getNodeType() {
        return this.gFB;
    }

    public short getSelectorType() {
        return 12;
    }

    public Selector bFd() {
        return this.gFC;
    }

    public SimpleSelector bFe() {
        return this.gFD;
    }

    public String toString() {
        return String.valueOf(this.gFC.toString()) + " + " + this.gFD.toString();
    }
}
