package logic.res.css;

import org.w3c.css.sac.LangCondition;

import java.io.Serializable;

/* renamed from: a.OU */
/* compiled from: a */
public class C0984OU implements LangCondition, Serializable {
    private String _lang;

    public C0984OU(String str) {
        this._lang = str;
    }

    /* renamed from: rg */
    public short getConditionType() {
        return 6;
    }

    public String getLang() {
        return this._lang;
    }

    public String toString() {
        return ":lang(" + getLang() + ")";
    }
}
