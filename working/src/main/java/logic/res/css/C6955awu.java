package logic.res.css;

import org.w3c.css.sac.AttributeCondition;

import java.io.Serializable;

/* renamed from: a.awu  reason: case insensitive filesystem */
/* compiled from: a */
public class C6955awu implements AttributeCondition, Serializable {

    /* renamed from: LN */
    private String f5532LN;
    private String _localName;

    public C6955awu(String str, String str2) {
        this._localName = str;
        this.f5532LN = str2;
    }

    /* renamed from: rg */
    public short getConditionType() {
        return 8;
    }

    public String getNamespaceURI() {
        return null;
    }

    public String getLocalName() {
        return this._localName;
    }

    public boolean getSpecified() {
        return true;
    }

    public String getValue() {
        return this.f5532LN;
    }

    public String toString() {
        return "[" + getLocalName() + "|=\"" + getValue() + "\"]";
    }
}
