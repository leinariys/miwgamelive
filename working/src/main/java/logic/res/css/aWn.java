package logic.res.css;

import org.w3c.css.sac.AttributeCondition;

import java.io.Serializable;

/* renamed from: a.aWn */
/* compiled from: a */
public class aWn implements AttributeCondition, Serializable {

    /* renamed from: LN */
    private String f4021LN;
    private String _localName;

    public aWn(String str, String str2) {
        this._localName = str;
        this.f4021LN = str2;
    }

    /* renamed from: rg */
    public short getConditionType() {
        return 4;
    }

    public String getNamespaceURI() {
        return null;
    }

    public String getLocalName() {
        return this._localName;
    }

    public boolean getSpecified() {
        return true;
    }

    public String getValue() {
        return this.f4021LN;
    }

    public String toString() {
        if (getValue() != null) {
            return "[" + getLocalName() + "=\"" + getValue() + "\"]";
        }
        return "[" + getLocalName() + "]";
    }
}
