package logic.res.css;

import com.steadystate.css.parser.Token;

/* renamed from: a.aNQ */
/* compiled from: a */
public class ParseException extends Exception {
    public int[][] expectedTokenSequences;
    public Token currentToken;
    public String[] tokenImage;
    public String EOL = System.getProperty("line.separator", "\n");
    public boolean specialConstructor = false;

    public ParseException(Token zVVar, int[][] iArr, String[] strArr) {
        super("");
        this.currentToken = zVVar;
        this.expectedTokenSequences = iArr;
        this.tokenImage = strArr;
    }

    public ParseException() {
    }

    public ParseException(String str) {
        super(str);
    }

    public String getMessage() {
        String str;
        if (!this.specialConstructor) {
            return super.getMessage();
        }
        String str2 = "";
        int i = 0;
        for (int i2 = 0; i2 < this.expectedTokenSequences.length; i2++) {
            if (i < this.expectedTokenSequences[i2].length) {
                i = this.expectedTokenSequences[i2].length;
            }
            for (int i3 = 0; i3 < this.expectedTokenSequences[i2].length; i3++) {
                str2 = String.valueOf(str2) + this.tokenImage[this.expectedTokenSequences[i2][i3]] + " ";
            }
            if (this.expectedTokenSequences[i2][this.expectedTokenSequences[i2].length - 1] != 0) {
                str2 = String.valueOf(str2) + "...";
            }
            str2 = String.valueOf(str2) + this.EOL + "    ";
        }
        String str3 = "Encountered \"";
        Token zVVar = this.currentToken.next;
        int i4 = 0;
        while (true) {
            if (i4 >= i) {
                break;
            }
            if (i4 != 0) {
                str3 = String.valueOf(str3) + " ";
            }
            if (zVVar.kind == 0) {
                str3 = String.valueOf(str3) + this.tokenImage[0];
                break;
            }
            str3 = String.valueOf(str3) + add_escapes(zVVar.image);
            zVVar = zVVar.next;
            i4++;
        }
        String str4 = String.valueOf(String.valueOf(str3) + "\" at line " + this.currentToken.next.beginLine + ", column " + this.currentToken.next.beginColumn) + "." + this.EOL;
        if (this.expectedTokenSequences.length == 1) {
            str = String.valueOf(str4) + "Was expecting:" + this.EOL + "    ";
        } else {
            str = String.valueOf(str4) + "Was expecting one of:" + this.EOL + "    ";
        }
        return String.valueOf(str) + str2;
    }

    /* access modifiers changed from: protected */
    public String add_escapes(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            switch (str.charAt(i)) {
                case 0:
                    break;
                case 8:
                    stringBuffer.append("\\b");
                    break;
                case 9:
                    stringBuffer.append("\\t");
                    break;
                case 10:
                    stringBuffer.append("\\n");
                    break;
                case 12:
                    stringBuffer.append("\\f");
                    break;
                case 13:
                    stringBuffer.append("\\r");
                    break;
                case '\"':
                    stringBuffer.append("\\\"");
                    break;
                case '\'':
                    stringBuffer.append("\\'");
                    break;
                case '\\':
                    stringBuffer.append("\\\\");
                    break;
                default:
                    char charAt = str.charAt(i);
                    if (charAt >= ' ' && charAt <= '~') {
                        stringBuffer.append(charAt);
                        break;
                    } else {
                        String str2 = "0000" + Integer.toString(charAt, 16);
                        stringBuffer.append("\\u" + str2.substring(str2.length() - 4, str2.length()));
                        break;
                    }
            }
        }
        return stringBuffer.toString();
    }
}
