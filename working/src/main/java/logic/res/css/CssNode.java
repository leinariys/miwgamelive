package logic.res.css;

import logic.ui.C1162RC;
import logic.ui.C6738asi;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSStyleSheet;
import org.w3c.dom.css.CSSValue;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.aKf  reason: case insensitive filesystem */
/* compiled from: a */
public class CssNode extends C6868avI {

    /**
     * Список заголовков селекторов name и не только
     */
    /* renamed from: buT */
    private List<CssSelector> listTitleSelector;
    /**
     * Список CSS ролей = Css файлов
     */
    /* renamed from: ihT */
    private List<CSSStyleSheet> listCssRule;
    /* renamed from: ihU */
    private List<C6738asi> ihU;
    /* renamed from: ihV */
    private C0325EQ ihV;

    public CssNode() {
        this.listCssRule = new ArrayList();
        this.ihU = new ArrayList();
        this.listTitleSelector = new ArrayList();
        this.ihV = new C0325EQ(this);
    }

    public CssNode(C0325EQ eq) {
        this.listCssRule = new ArrayList();
        this.ihU = new ArrayList();
        this.listTitleSelector = new ArrayList();
        this.ihV = eq;
        eq.mo1841a(this);
    }

    public void loadFileCSS(InputStream inputStream) throws UnsupportedEncodingException {
        this.listCssRule.clear();
        this.ihV.parse(new InputStreamReader(inputStream, "utf-8"));
    }

    public void loadFileCSS(Reader reader) {
        this.ihV.parse(reader);
    }

    /* renamed from: a */
    public void mo9782a(C1162RC rc) {
        this.ihV.mo1840a(rc);
    }

    /* renamed from: a */
    public CSSValue mo3817a(C1162RC rc, String str, boolean z) {
        CSSStyleDeclaration a = new C3669tX(this.listTitleSelector).mo22245a((C6738asi) rc, str, z);
        if (a == null) {
            return null;
        }
        CSSValue propertyCSSValue = a.getPropertyCSSValue(str);
        if (propertyCSSValue != null) {
            return propertyCSSValue;
        }
        if (!z || rc.mo15974Vm() == null) {
            return null;
        }
        return mo3817a((C1162RC) rc.mo15974Vm(), str, z);
    }

    /**
     * Добавить в список - селектор и его параметры
     *
     * @param cSSStyleSheet
     */
    /* renamed from: b */
    public void add(CSSStyleSheet cSSStyleSheet) {
        this.listCssRule.add(cSSStyleSheet);
    }

    /* renamed from: a */
    public void mo9783a(C6738asi asi) {
        this.ihU.add(asi);
    }

    /* renamed from: b */
    public boolean mo9786b(C6738asi asi) {
        return this.ihU.contains(asi);
    }

    /**
     * Добавить в список заголовки селекторов
     *
     * @param lDVar
     */
    /* renamed from: a */
    public void add(CssSelector lDVar) {
        this.listTitleSelector.add(lDVar);
    }

    /* renamed from: a */
    public boolean mo3818a(C1162RC rc, aKV akv, boolean z) {
        return new C3669tX(this.listTitleSelector).mo22246a((C6738asi) rc, akv, z);
    }
}
