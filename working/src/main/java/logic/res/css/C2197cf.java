package logic.res.css;

import com.steadystate.css.dom.CSSStyleSheetImpl;
import com.steadystate.css.parser.CSSOMParser;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.CSSImportRule;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleSheet;
import org.w3c.dom.stylesheets.MediaList;

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;

/* renamed from: a.cf */
/* compiled from: a */
public class C2197cf implements Serializable, CSSImportRule {

    /* renamed from: qq */
    CSSStyleSheetImpl f6200qq = null;

    /* renamed from: qr */
    CSSRule f6201qr = null;

    /* renamed from: qs */
    String f6202qs = null;

    /* renamed from: qt */
    MediaList f6203qt = null;

    public C2197cf(CSSStyleSheetImpl ss, CSSRule cSSRule, String str, MediaList mediaList) {
        this.f6200qq = ss;
        this.f6201qr = cSSRule;
        this.f6202qs = str;
        this.f6203qt = mediaList;
    }

    public short getType() {
        return 3;
    }

    public String getCssText() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("@import url(").append(getHref()).append(")");
        if (getMedia().getLength() > 0) {
            stringBuffer.append(" ").append(getMedia().toString());
        }
        stringBuffer.append(";");
        return stringBuffer.toString();
    }

    public void setCssText(String str) {
        if (this.f6200qq == null || !this.f6200qq.isReadOnly()) {
            try {
                C2197cf f = (C2197cf) new CSSOMParser().parseRule(new InputSource((Reader) new StringReader(str)));
                if (f.getType() == 3) {
                    this.f6202qs = f.f6202qs;
                    this.f6203qt = f.f6203qt;
                    return;
                }
                throw new C3331qN(13, 6);
            } catch (CSSException e) {
                throw new C3331qN((short) 12, 0, e.getMessage());
            } catch (IOException e) {
                throw new C3331qN((short) 12, 0, e.getMessage());
            }
        } else {
            throw new C3331qN(7, 2);
        }
    }

    public CSSStyleSheet getParentStyleSheet() {
        return this.f6200qq;
    }

    public CSSRule getParentRule() {
        return this.f6201qr;
    }

    public String getHref() {
        return this.f6202qs;
    }

    public MediaList getMedia() {
        return this.f6203qt;
    }

    public CSSStyleSheet getStyleSheet() {
        return null;
    }

    public String toString() {
        return getCssText();
    }
}
