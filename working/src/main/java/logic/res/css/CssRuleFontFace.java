package logic.res.css;

import com.steadystate.css.dom.CSSStyleSheetImpl;
import com.steadystate.css.parser.CSSOMParser;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.CSSFontFaceRule;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSStyleSheet;

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;

/* renamed from: a.FW */
/* compiled from: a */
public class CssRuleFontFace implements Serializable, CSSFontFaceRule {
    private CSSStyleDeclarationImpl cXh = null;

    /* renamed from: qq */
    private CSSStyleSheetImpl f578qq = null;

    /* renamed from: qr */
    private CSSRule f579qr = null;

    public CssRuleFontFace(CSSStyleSheetImpl ss, CSSRule cSSRule) {
        this.f578qq = ss;
        this.f579qr = cSSRule;
    }

    public short getType() {
        return 5;
    }

    public String getCssText() {
        return "@font-face " + getStyle().getCssText();
    }

    public void setCssText(String str) {
        if (this.f578qq == null || !this.f578qq.isReadOnly()) {
            try {
                CssRuleFontFace f = (CssRuleFontFace) new CSSOMParser().parseRule(new InputSource((Reader) new StringReader(str)));
                if (f.getType() == 5) {
                    this.cXh = f.cXh;
                    return;
                }
                throw new C3331qN(13, 8);
            } catch (CSSException e) {
                throw new C3331qN((short) 12, 0, e.getMessage());
            } catch (IOException e) {
                throw new C3331qN((short) 12, 0, e.getMessage());
            }
        } else {
            throw new C3331qN(7, 2);
        }
    }

    public CSSStyleSheet getParentStyleSheet() {
        return this.f578qq;
    }

    public CSSRule getParentRule() {
        return this.f579qr;
    }

    public CSSStyleDeclaration getStyle() {
        return this.cXh;
    }

    /* renamed from: a */
    public void mo2156a(CSSStyleDeclarationImpl ahx) {
        this.cXh = ahx;
    }
}
