package logic.res.css;

/* renamed from: a.Ro */
/* compiled from: a */
public class C1206Ro {
    private final String dZJ;
    private final int fontSize;
    private final int fontStyle;

    public C1206Ro(String str, int i, int i2) {
        this.dZJ = str;
        this.fontStyle = i;
        this.fontSize = i2;
    }

    public String getFontName() {
        return this.dZJ;
    }

    public int getFontStyle() {
        return this.fontStyle;
    }

    public int getFontSize() {
        return this.fontSize;
    }
}
