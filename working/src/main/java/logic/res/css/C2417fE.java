package logic.res.css;

import org.w3c.css.sac.AttributeCondition;

import java.io.Serializable;

/* renamed from: a.fE */
/* compiled from: a */
public class C2417fE implements AttributeCondition, Serializable {

    /* renamed from: LN */
    private String f7190LN;

    public C2417fE(String str) {
        this.f7190LN = str;
    }

    /* renamed from: rg */
    public short getConditionType() {
        return 5;
    }

    public String getNamespaceURI() {
        return null;
    }

    public String getLocalName() {
        return null;
    }

    public boolean getSpecified() {
        return true;
    }

    public String getValue() {
        return this.f7190LN;
    }

    public String toString() {
        return "#" + getValue();
    }
}
