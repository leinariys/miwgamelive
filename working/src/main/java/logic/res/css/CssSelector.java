package logic.res.css;

import org.w3c.css.sac.SelectorList;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSStyleSheet;

/**
 * Обёртка CSS селектора
 */
/* renamed from: a.lD */
/* compiled from: a */
public class CssSelector {
    public CSSStyleSheet cssStyleSheet;
    public SelectorList selectorList;//Количество селекторов
    public CSSRule cssRule;
    /**
     * Перечисление всех свойств css селектора
     */
    public CSSStyleDeclaration cssStyleDeclaration;

    public String toString() {
        return this.selectorList.toString();
    }
}
