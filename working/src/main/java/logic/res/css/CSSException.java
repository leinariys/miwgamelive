package logic.res.css;

/* renamed from: a.cM */
/* compiled from: a */
public class CSSException extends RuntimeException {

    /* renamed from: xa */
    public static short SAC_UNSPECIFIED_ERR = 0;

    /* renamed from: xb */
    public static short SAC_NOT_SUPPORTED_ERR = 1;

    /* renamed from: xc */
    public static short SAC_SYNTAX_ERR = 2;
    public short code;

    /* renamed from: s */
    public String f6092s;

    /* renamed from: xd */
    public Exception f6093xd;

    public CSSException() {
    }

    public CSSException(String str) {
        this.code = SAC_UNSPECIFIED_ERR;
        this.f6092s = str;
    }

    public CSSException(Exception exc) {
        this.code = SAC_UNSPECIFIED_ERR;
        this.f6093xd = exc;
    }

    public CSSException(short s) {
        this.code = s;
    }

    public CSSException(short s, String str, Exception exc) {
        this.code = s;
        this.f6092s = str;
        this.f6093xd = exc;
    }

    public String getMessage() {
        if (this.f6092s != null) {
            return this.f6092s;
        }
        if (this.f6093xd != null) {
            return this.f6093xd.getMessage();
        }
        return null;
    }

    /* renamed from: iN */
    public short getCode() {
        return this.code;
    }

    public Exception getException() {
        return this.f6093xd;
    }
}
