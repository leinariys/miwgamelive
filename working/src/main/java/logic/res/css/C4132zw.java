package logic.res.css;

import org.w3c.dom.stylesheets.MediaList;

import java.io.Serializable;
import java.util.Vector;

/* renamed from: a.zw */
/* compiled from: a */
public class C4132zw implements Serializable, MediaList {
    private Vector bZx = new Vector();

    public C4132zw(SACMediaList atf) {
        for (int i = 0; i < atf.getLength(); i++) {
            this.bZx.addElement(atf.item(i));
        }
    }

    public String getMediaText() {
        StringBuffer stringBuffer = new StringBuffer("");
        for (int i = 0; i < this.bZx.size(); i++) {
            stringBuffer.append(this.bZx.elementAt(i).toString());
            if (i < this.bZx.size() - 1) {
                stringBuffer.append(", ");
            }
        }
        return stringBuffer.toString();
    }

    public void setMediaText(String str) {
    }

    public int getLength() {
        return this.bZx.size();
    }

    public String item(int i) {
        if (i < this.bZx.size()) {
            return (String) this.bZx.elementAt(i);
        }
        return null;
    }

    public void deleteMedium(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.bZx.size()) {
                throw new C3331qN(8, 18);
            } else if (((String) this.bZx.elementAt(i2)).equalsIgnoreCase(str)) {
                this.bZx.removeElementAt(i2);
                return;
            } else {
                i = i2 + 1;
            }
        }
    }

    public void appendMedium(String str) {
        this.bZx.addElement(str);
    }

    public String toString() {
        return getMediaText();
    }
}
