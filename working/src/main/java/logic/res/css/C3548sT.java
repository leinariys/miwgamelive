package logic.res.css;


import org.w3c.css.sac.*;

/* renamed from: a.sT */
/* compiled from: a */
public class C3548sT implements C1704ZE {
    /* renamed from: a */
    public ConditionalSelector mo7325a(SimpleSelector kw, Condition aib) {
        return new C3426rR(kw, aib);
    }

    public SimpleSelector act() {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    public SimpleSelector acu() {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    /* renamed from: a */
    public NegativeSelector mo7327a(SimpleSelector kw) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    /* renamed from: m */
    public ElementSelector mo7334m(String str, String str2) {
        if (str == null) {
            return new C0294Do(str2);
        }
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    /* renamed from: bK */
    public CharacterDataSelector mo7331bK(String str) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    /* renamed from: bL */
    public CharacterDataSelector mo7332bL(String str) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    /* renamed from: n */
    public ProcessingInstructionSelector mo7335n(String str, String str2) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    /* renamed from: bM */
    public CharacterDataSelector mo7333bM(String str) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    /* renamed from: o */
    public ElementSelector mo7336o(String str, String str2) {
        if (str == null) {
            return new aPQ(str2);
        }
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    /* renamed from: a */
    public C0546Hb mo7324a(Selector awz, SimpleSelector kw) {
        return new axP(awz, kw);
    }

    /* renamed from: b */
    public C0546Hb mo7330b(Selector awz, SimpleSelector kw) {
        return new C0159Bq(awz, kw);
    }

    /* renamed from: a */
    public C1613Xc mo7326a(short s, Selector awz, SimpleSelector kw) {
        return new C6886ava(s, awz, kw);
    }
}
