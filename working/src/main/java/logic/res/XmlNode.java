package logic.res;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

/* renamed from: a.aGY */
/* compiled from: a */
@Slf4j
public class XmlNode implements Cloneable {
    private static final String EMPTY_STRING = "";

    /**
     * Имя тега
     */
    @Setter
    @Getter
    private String tagName = EMPTY_STRING;

    /**
     * Текстовое содержимое тега
     */
    /* renamed from: Nk */
    @Getter
    private String text = EMPTY_STRING;
    /**
     * Атрибуты тега ключ - значение
     */
    /* renamed from: aSx */
    @Getter
    private Map<String, String> listAttribute = new HashMap<String, String>();
    /**
     * Список тегов потомков
     */
    /* renamed from: children */
    @Getter
    private List<XmlNode> listChildrenTag = new ArrayList<XmlNode>();
    /**
     * Флаг для сигнала вхождения в потомков
     */
    /* renamed from: djr */
    private boolean flagBool = false;
    /**
     * UserData
     */
    /* renamed from: eNl */
    private Object userData;


    public XmlNode() {
    }

    public XmlNode(String tagName) {
        this.tagName = tagName;
    }

    /**
     * Создать тег с текстом
     *
     * @param tagName      Имя тега
     * @param text         текст тега
     * @param arrAttribute атрибуты тега
     */
    public XmlNode(String tagName, String text, String... arrAttribute) {
        this.tagName = tagName;
        setText(text);
        if (arrAttribute != null) {
            for (int i = 0; i + 1 < arrAttribute.length; i += 2) {
                addAttribute(arrAttribute[i], arrAttribute[i + 1]);
            }
        }
    }

    /**
     * Проверка на допустимость символов, кодирование не прошедших
     *
     * @param name
     * @return вернят допустимые символы , другие заменить на их коды
     */
    /* renamed from: mA */
    public static String checkAZaz09code(String name) {
        StringBuilder stringBuilder = new StringBuilder();
        int length = name.equals(EMPTY_STRING) ? -1 : name.length();
        for (int i = 0; i < length; i++) {
            char charAt = name.charAt(i);
            if ((charAt < 'a' || charAt > 'z') && ((charAt < 'A' || charAt > 'Z') && (charAt < '0' || charAt > '9'))) {
                stringBuilder.append(String.format("_%02x", (int) charAt));
            } else {
                stringBuilder.append(charAt);
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Проверка на допустимость символов, декодирование не прошедших
     *
     * @param name
     * @return вернят допустимые символы, другие заменить на символы
     */
    /* renamed from: mB */
    public static String checkAZaz09deCode(String name) {
        StringBuilder stringBuilder = new StringBuilder();
        int length = name.equals(EMPTY_STRING) ? -1 : name.length();
        int i = 0;
        while (i < length) {
            char charAt = name.charAt(i);
            if ((charAt >= 'a' && charAt <= 'z') || ((charAt >= 'A' && charAt <= 'Z') || (charAt >= '0' && charAt <= '9'))) {
                stringBuilder.append(charAt);
            } else if (charAt == '_') {
                stringBuilder.append((char) Integer.parseInt(name.substring(i + 1, i + 3), 16));
                i += 2;
            } else {
                System.out.println("WARNING: character '" + charAt + "' illegal for html, ignoring it");
            }
            i++;
        }
        return stringBuilder.toString();
    }

    /**
     * Получить Флаг для сигнала вхождения в потомков
     *
     * @return
     */
    /* renamed from: dcD */
    public void getFlagBool() {
        this.flagBool = true;
    }

    /* renamed from: dcE */
    public boolean setFlagBool() {
        return this.flagBool;
    }

    public XmlNode clone() {
        XmlNode cloneXmlNode = new XmlNode(this.tagName);
        cloneXmlNode.text = this.text;
        if (!this.listAttribute.isEmpty()) {
            cloneXmlNode.listAttribute.putAll(this.listAttribute);
        }

        if (!this.listChildrenTag.isEmpty()) {
            for (XmlNode dcF : this.listChildrenTag) {
                cloneXmlNode.listChildrenTag.add(dcF.clone());
            }
        }
        cloneXmlNode.userData = this.userData;
        return cloneXmlNode;
    }

    /**
     * Задать текстовое содержимое тега
     *
     * @param text
     * @return
     */
    /* renamed from: my */
    public XmlNode setText(String text) {
        if (!text.equals(EMPTY_STRING) || text.trim().length() != 0) {
            this.text = text;
        } else {
            this.text = EMPTY_STRING;
        }
        return this;
    }

    /**
     * Добавить потомка с переданным именем тега
     *
     * @param tegName имя тега
     * @return
     */
    /* renamed from: mz */
    public XmlNode addChildrenTag(String tegName) {
        XmlNode node = new XmlNode(tegName);
        this.listChildrenTag.add(node);
        return node;
    }

    /**
     * Добавить потомка с заданым тегом
     *
     * @param child Контейнер Тега
     * @return
     */
    /* renamed from: k */
    public XmlNode addChildrenTag(XmlNode child) {
        this.listChildrenTag.add(child);
        return child;
    }

    /**
     * Добавить атрибут тега
     *
     * @param key   Имя атрибута
     * @param value Значение атрибута
     * @return
     */
    /* renamed from: aO */
    public XmlNode addAttribute(String key, String value) {
        this.listAttribute.put(key, value);
        return this;
    }

    /**
     * Получить строковое содержимое тега
     *
     * @return
     */
    public String getXmlStringNode() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintWriter printWriter = new PrintWriter(byteArrayOutputStream);
        buildStringFromNode(printWriter);
        printWriter.flush();
        return new String(byteArrayOutputStream.toByteArray());
    }

    /**
     * Получить строковое содержимое тега
     *
     * @param stringBuilder
     */
    /* renamed from: d */
    public void buildStringFromNode(PrintWriter stringBuilder) {
        buildStringFromNode(EMPTY_STRING, stringBuilder);
    }

    /**
     * Получить строковое содержимое тега
     *
     * @param startLine
     * @param stringBuilder
     */
    /* renamed from: b */
    private void buildStringFromNode(String startLine, PrintWriter stringBuilder) {
        stringBuilder.append(startLine);
        String name;
        if (!this.tagName.equals(EMPTY_STRING)) {
            name = checkAZaz09code(this.tagName);
            stringBuilder.append("<").append(name);
            Set<Map.Entry<String, String>> entrySet = this.listAttribute.entrySet();

            for (Map.Entry<String, String> next : entrySet) {
                if (entrySet.size() > 1) {
                    stringBuilder.append("\n" + startLine + "  ");
                }
                stringBuilder.append(" ").append(next.getKey()).append("=").append('\"').append(next.getValue()).append('\"');
            }
        } else {
            name = EMPTY_STRING;
        }

        if (!this.listChildrenTag.isEmpty()) {
            stringBuilder.append(">").append("\n");
            for (XmlNode xmlNodeChild : this.listChildrenTag) {
                xmlNodeChild.buildStringFromNode(String.valueOf(startLine) + "   ", stringBuilder);
                stringBuilder.append("\n");
            }
            if (this.text != null) {
                stringBuilder.append(startLine).append("   ").append(this.text).append("\n");
            }
            stringBuilder.append(startLine);
        } else if (this.text == null || this.text.trim().length() == 0) {
            stringBuilder.append(" />");
            return;
        } else {
            stringBuilder.append(">").append(this.text);
        }
        if (this.tagName != null) {
            stringBuilder.append("</").append(name).append(">");
        }
    }

    /* renamed from: aP */
    public XmlNode findNodeChildAttribut(String attributName, String attributValue) {
        return findNodeChild((String) null, attributName, attributValue);
    }

    /* renamed from: mC */
    public XmlNode findNodeChildTag(String tagName) {
        return findNodeChild(0, tagName, (String) null, (String) null);
    }

    /* renamed from: o */
    public XmlNode findNodeChild(String tagName, String attributName, String attributValue) {
        return findNodeChild(0, tagName, attributName, attributValue);
    }

    /* renamed from: aQ */
    public XmlNode findNodeChildAttributDepth(String attributName, String attributValue) {
        return findNodeChildTagDepth((String) null, attributName, attributValue);
    }

    /* renamed from: mD */
    public XmlNode findNodeChildTagDepth(String str) {
        return findNodeChild(Integer.MAX_VALUE, str, (String) null, (String) null);
    }

    /* renamed from: p */
    public XmlNode findNodeChildTagDepth(String str, String str2, String str3) {
        XmlNode c = findNodeChild(Integer.MAX_VALUE, str, str2, str3);
        if (c == null) {
            log.error("element " + str + " with " + str2 + " of value " + str3 + " not found!");
        }
        return c;
    }

    /* renamed from: c */
    public XmlNode findNodeChild(int depth, String tagName, String attributName, String attributValue) {
        if (this.listChildrenTag.isEmpty()) {
            return null;
        }
        for (XmlNode xmlNodeChild : this.listChildrenTag) {
            boolean isName = tagName == null || tagName.equals(xmlNodeChild.getTagName());
            boolean isAttribute = attributName == null || attributValue.equals(xmlNodeChild.getAttribute(attributName));
            if (isName && isAttribute) {
                return xmlNodeChild;
            }

            if (depth > 0) {
                XmlNode xmlNodeChildChild = xmlNodeChild.findNodeChild(depth - 1, tagName, attributName, attributValue);
                if (xmlNodeChildChild != null) {
                    return xmlNodeChildChild;
                }

            }
        }
        return null;
    }

    /* renamed from: mE */
    public List<XmlNode> findNodeChildAllTagDepth(String tagName) {
        return findNodeChildAllDepth(tagName, (String) null, (String) null);
    }

    /* renamed from: aR */
    public List<XmlNode> findNodeChildAllAttributDepth(String attributName, String attributValue) {
        return findNodeChildAllDepth((String) null, attributName, attributValue);
    }


    /* renamed from: q */
    public List<XmlNode> findNodeChildAllDepth(String tagName, String attributName, String attributValue) {
        return findNodeChildAll(Integer.MAX_VALUE, tagName, attributName, attributValue);
    }

    /* renamed from: mF */
    public List<XmlNode> findNodeChildAllTag(String tagName) {
        return findNodeChildAll(0, tagName, (String) null, (String) null);
    }

    /* renamed from: aS */
    public List<XmlNode> findNodeChildAllAttribut(String attributName, String attributValue) {
        return findNodeChildAll(0, (String) null, attributName, attributValue);
    }

    /* renamed from: g */
    public List<XmlNode> findNodeChildAll(int depth, String tagName) {
        return findNodeChildAll(depth, tagName, (String) null, (String) null);
    }

    /* renamed from: d */
    public List<XmlNode> findNodeChildAll(int depth, String tagName, String attributName, String attributValue) {
        LinkedList foundNode = new LinkedList();
        findNodeChildAll(depth, foundNode, tagName, attributName, attributValue);
        return foundNode;
    }

    /**
     * Поиск нод всех
     *
     * @param depth         глубина
     * @param foundNode     найденные ноды
     * @param tagName       имя тега
     * @param attributName  имя атрибута
     * @param attributValue значение атрибута
     */
    /* renamed from: a */
    public void findNodeChildAll(int depth, List<XmlNode> foundNode, String tagName, String attributName, String attributValue) {

        if (!this.listChildrenTag.isEmpty()) {
            for (XmlNode nodeChild : this.listChildrenTag) {
                boolean isName = tagName == null || tagName.equals(nodeChild.getTagName());
                boolean isAttribute = attributName == null || attributValue.equals(nodeChild.getAttribute(attributName));
                if (isName && isAttribute) {
                    foundNode.add(nodeChild);
                }
                if (depth > 0) {
                    nodeChild.findNodeChildAll(depth - 1, foundNode, tagName, attributName, attributValue);
                }
            }
        }
    }

    public String getAttribute(String str) {
        return getAttribute(str, (String) null);
    }

    public String getAttribute(String attributName, String defaultValue) {
        String attribut = this.listAttribute.get(attributName);
        return attribut == null ? defaultValue : attribut;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<").append(this.tagName);
        for (Map.Entry next : this.listAttribute.entrySet()) {
            stringBuilder.append(' ').append((String) next.getKey()).append("=").append('\"').append((String) next.getValue()).append('\"');
        }

        if (!this.listChildrenTag.isEmpty()) {
            stringBuilder.append(">").append("...");
        } else if (this.text.equals(EMPTY_STRING)) {
            stringBuilder.append(" />");
            return stringBuilder.toString();
        } else {
            stringBuilder.append(">...");
        }
        stringBuilder.append("</").append(this.tagName).append(">");
        return stringBuilder.toString();
    }

    public String toStringNode() {
        if (this.listChildrenTag.isEmpty()) {
            return EMPTY_STRING;
        }
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        for (XmlNode node : this.listChildrenTag) {
            node.buildStringFromNode(printWriter);
        }
        printWriter.flush();
        return stringWriter.toString();
    }

    public Object getUserData() {
        return this.userData;
    }

    public void setUserData(Object obj) {
        this.userData = obj;
    }

    /* renamed from: mG */
    public void mo9052mG(String tagName) {
        if (!this.listChildrenTag.isEmpty()) {
            XmlNode temp = null;
            for (XmlNode node : this.listChildrenTag) {
                if (tagName.equals(node.getTagName())) {
                    temp = node;
                    break;
                }
            }
            if (temp != null) {
                this.listChildrenTag.remove(temp);
            }

        }
    }

    public int count() {
        if (this.listChildrenTag.isEmpty()) {
            return 0;
        }
        int size = this.listChildrenTag.size();
        Iterator<XmlNode> it = this.listChildrenTag.iterator();
        while (true) {
            int i = size;
            if (!it.hasNext()) {
                return i;
            }
            size = it.next().count() + i;
        }
    }


    public Map<String, String> getAttributes() {
        if (this.listAttribute != null) {
            return Collections.unmodifiableMap(this.listAttribute);
        }
        return Collections.EMPTY_MAP;
    }

    public void clear() {
        this.listChildrenTag.clear();
    }
}
