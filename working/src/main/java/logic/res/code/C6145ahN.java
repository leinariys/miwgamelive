package logic.res.code;

import logic.thred.PoolThread;

/* renamed from: a.ahN  reason: case insensitive filesystem */
/* compiled from: a */
public class C6145ahN {
    private long current;
    private long step;
    private long last;

    public C6145ahN(float f) {
        setFps(f);
    }

    /* access modifiers changed from: package-private */
    public void setFps(float f) {
        this.step = (long) (1000.0f / f);
    }

    /* access modifiers changed from: package-private */
    public long caT() {
        this.current = System.currentTimeMillis();
        long j = (this.current - this.last) - this.step;
        this.last = this.current;
        long j2 = this.step - j;
        if (j2 > 0) {
            PoolThread.sleep(j2);
        }
        if (j > (-this.step)) {
            return this.step + j;
        }
        return 0;
    }
}
