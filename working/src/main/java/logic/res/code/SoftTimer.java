package logic.res.code;

import gametoolkit.timer.module.InternalTimer;
import gametoolkit.timer.module.SoftTimerConsole;
import logic.WrapRunnable;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.util.*;
import java.util.concurrent.Executor;

/* renamed from: a.aBg  reason: case insensitive filesystem */
/* compiled from: a */
public class SoftTimer {
    /**
     * Возможные задачи
     */
    public Map<WrapRunnable, InternalTimer> listTask;
    /**
     * задачи на выполнение
     */
    private List<InternalTimer> tasksPerform;

    public SoftTimer() {
        this(false);
    }

    public SoftTimer(boolean z) {
        this.listTask = new HashMap();
        this.tasksPerform = new ArrayList();
        if (z) {
            mBeanServerRegistered();
        }
    }

    private void mBeanServerRegistered() {
        MBeanServer platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
        if (platformMBeanServer != null) {
            SoftTimerConsole softTimerConsole = new SoftTimerConsole(this);
            try {
                ObjectName objectName = new ObjectName("Bitverse:name=SoftTimer");
                if (!platformMBeanServer.isRegistered(objectName)) {
                    platformMBeanServer.registerMBean(softTimerConsole, objectName);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: jB */
    public void mo7965jB(long j) {
        synchronized (this.listTask) {
            Iterator<InternalTimer> it = this.listTask.values().iterator();
            while (it.hasNext()) {
                InternalTimer next = it.next();
                if (next.isCanceled()) {
                    it.remove();
                } else if (next.isNextRun(j)) {
                    this.tasksPerform.add(next);
                } else if (next.isCanceled()) {
                    it.remove();
                }
            }
        }
        if (!this.tasksPerform.isEmpty()) {
            Iterator<InternalTimer> it2 = this.tasksPerform.iterator();
            while (it2.hasNext()) {
                InternalTimer next2 = it2.next();
                next2.run();
                if (next2.isCanceled()) {
                    it2.remove();
                }
            }
            this.tasksPerform.clear();
        }
    }

    /* renamed from: a */
    public void step(long millis, Executor executor) {
        //Перевод задачи на выполнение
        synchronized (this.listTask) {
            Iterator<InternalTimer> it = this.listTask.values().iterator();
            while (it.hasNext()) {
                InternalTimer next = it.next();
                if (next.isCanceled()) {
                    it.remove();
                } else if (next.isNextRun(millis)) {
                    this.tasksPerform.add(next);
                } else if (next.isCanceled()) {
                    it.remove();
                }
            }
        }

        //запуск задачи для выполнения
        if (!this.tasksPerform.isEmpty()) {
            Iterator<InternalTimer> it2 = this.tasksPerform.iterator();
            while (it2.hasNext()) {
                InternalTimer next2 = it2.next();
                if (next2.isCanceled()) {
                    it2.remove();
                } else {
                    executor.execute(next2);
                }
            }
            this.tasksPerform.clear();
        }
    }

    /* renamed from: a mo7963a */
    public void removeTask(WrapRunnable aen) {
        synchronized (this.listTask) {
            this.listTask.remove(aen);
        }
    }

    /* renamed from: b  mo7964b */
    public WrapRunnable addTask(String str, WrapRunnable runnableTask, long interval) {
        return addTask(str, runnableTask, interval, false);
    }

    /* renamed from: a  mo7961a */
    public WrapRunnable addTask(String nameTask, WrapRunnable runnableTask, long interval, boolean z) {
        runnableTask.setReset(false);
        runnableTask.setCanceled(false);
        synchronized (this.listTask) {
            this.listTask.put(runnableTask, new InternalTimer(this, nameTask, runnableTask, interval, z, true));
        }
        return runnableTask;
    }

    /* renamed from: a */
    public WrapRunnable mo7960a(String nameTask, WrapRunnable runnableTask, long interval) {
        runnableTask.setReset(false);
        runnableTask.setCanceled(false);
        synchronized (this.listTask) {
            this.listTask.put(runnableTask, new InternalTimer(this, nameTask, runnableTask, interval, false, false));
        }
        return runnableTask;
    }
}
