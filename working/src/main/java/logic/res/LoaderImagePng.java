package logic.res;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.kf */
/* compiled from: a */
public class LoaderImagePng implements ILoaderImageInterface {
    private Map<String, Image> asx = new HashMap();

    public Image getImage(String pathFile) {
        if (pathFile == null) {
            return null;
        }
        if ("none".equals(pathFile)) {
            return null;
        }
        if (pathFile.startsWith("res://")) {
            pathFile = pathFile.substring(5);
        }

        if (pathFile.startsWith("/imageset_") && pathFile.indexOf("material") > 0) {//Я добавил
            pathFile = "/data/gui/imageset" + pathFile.replaceAll("material/", "");
        }


        String str2 = String.valueOf(pathFile) + ".png";
        Image image = this.asx.get(str2);
        if (image != null) {
            return image;
        }
        try {
            BufferedImage read = ImageIO.read(ILoaderImageInterface.class.getResourceAsStream(str2));
            this.asx.put(str2, read);
            return read;
        } catch (Exception e) {
            System.err.println("Error reading image " + str2);
            return null;
        }
    }

    /* renamed from: a */
    public Cursor getNull(String str, Point point) {
        return null;
    }
}
