package logic.res.sound;

import taikodom.render.scene.SoundObject;

/* renamed from: a.Ma */
/* compiled from: a */
public interface ISoundPlayer {
    /* renamed from: A */
    void mo2911A(String str, String str2);

    /* renamed from: C */
    SoundObject mo2913C(String str, String str2);

    /* renamed from: cx */
    SoundObject mo2915cx(String str);

    /* renamed from: e */
    SoundObject mo2916e(String str, boolean z);
}
