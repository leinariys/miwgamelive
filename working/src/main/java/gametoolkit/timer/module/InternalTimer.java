package gametoolkit.timer.module;

import logic.WrapRunnable;
import logic.res.code.SoftTimer;

import java.security.InvalidParameterException;

/* compiled from: a */
public class InternalTimer implements InternalTimerMBean, Runnable {

    final /* synthetic */ SoftTimer softTimer;
    /* renamed from: Df */
    private final boolean isLoop;
    private final long interval;
    private final WrapRunnable runnableTask;
    private final boolean ghz;
    /* access modifiers changed from: private */
    public String nameTask;
    private long totalMillis = 0;
    private boolean running = false;

    public InternalTimer(SoftTimer softTimer, String nameTask, WrapRunnable runnableTask, long interval, boolean z, boolean loop) {
        this.softTimer = softTimer;
        if (interval <= 0) {
            throw new InvalidParameterException("interval must be > 0");
        }
        this.nameTask = nameTask;
        this.runnableTask = runnableTask;
        this.interval = interval;
        this.ghz = z;
        this.isLoop = loop;
    }

    public boolean isRunning() {
        return this.running;
    }

    public long getAccumulator() {
        return this.totalMillis;
    }

    public long getInterval() {
        return this.interval;
    }

    /**
     * Задача для выполнения?
     *
     * @param millis
     * @return
     */
    public boolean isNextRun(long millis) {
        if (this.runnableTask.isCanceled()) {
            return false;
        }
        if (this.runnableTask.isReset()) {
            this.totalMillis = 0;
            this.runnableTask.setReset(false);
            return false;
        }
        this.totalMillis += millis;
        if (this.totalMillis >= this.interval) {
            return true;
        }
        return false;
    }

    public void run() {
        while (this.totalMillis >= this.interval) {
            this.totalMillis -= this.interval;
            if (!this.ghz && this.totalMillis >= this.interval) {
                this.totalMillis %= this.interval;
            }
            try {
                this.running = true;
                this.runnableTask.run();
                if (!this.isLoop) {
                    return;
                }
            } finally {
                this.running = false;
                if (!this.isLoop) {
                    this.runnableTask.cancel();
                }
            }
        }
    }

    public boolean isCanceled() {
        return this.runnableTask.isCanceled();
    }
}
