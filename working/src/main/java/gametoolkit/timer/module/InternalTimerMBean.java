package gametoolkit.timer.module;

/* compiled from: a */
public interface InternalTimerMBean {
    long getAccumulator();

    long getInterval();

    boolean isRunning();
}
