package gametoolkit.timer.module;

import logic.res.code.SoftTimer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: a */
public class SoftTimerConsole implements ISoftTimerConsoleMBean {
    final /* synthetic */ SoftTimer softTimer;

    public SoftTimerConsole(SoftTimer softTimer) {
        this.softTimer = softTimer;
    }

    public int getTimersCount() {
        return this.softTimer.listTask.size();
    }

    public List<String> getTimers() {
        ArrayList arrayList = new ArrayList();

        synchronized (this.softTimer.listTask) {
            int i = 0;
            for (InternalTimer softTimer : this.softTimer.listTask.values()) {
                Object[] objArr = new Object[5];
                objArr[0] = softTimer.nameTask;
                objArr[1] = Integer.valueOf(i);
                objArr[2] = Long.valueOf(softTimer.getAccumulator());
                objArr[3] = Long.valueOf(softTimer.getInterval());
                objArr[4] = softTimer.isRunning() ? "running" : "not running";
                arrayList.add(String.format("%s: id:%d acc:%d interval:%d %s", objArr));
                i++;
            }
        }

        Collections.sort(arrayList);
        return arrayList;
    }
}
