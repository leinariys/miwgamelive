package taikodom.render;

import game.geometry.*;
import org.mozilla.javascript.ScriptRuntime;

import java.lang.reflect.Array;

/* renamed from: a.vv */
/* compiled from: a */
public class C3862vv {
    static double[] bzs = new double[16];
    public final TransformWrap tempTransform = new TransformWrap();
    private C0107BM[] bzp = new C0107BM[6];
    private int[][] bzq = ((int[][]) Array.newInstance(Integer.TYPE, new int[]{6, 6}));
    private double[] bzr = new double[6];

    public C3862vv() {
        for (int i = 0; i < 6; i++) {
            this.bzp[i] = new C0107BM();
        }
    }

    /* renamed from: a */
    public void mo1158a(TransformWrap bcVar, Matrix4fWrap ajk) {
        bcVar.mo17336a(this.tempTransform);
        Matrix3fWrap ajd = this.tempTransform.orientation;
        Vec3d ajr = this.tempTransform.position;
        bzs[0] = (double) ((ajd.m00 * ajk.m00) + (ajd.m10 * ajk.m01) + (ajd.m20 * ajk.m02));
        bzs[1] = (double) ((ajd.m00 * ajk.m10) + (ajd.m10 * ajk.m11) + (ajd.m20 * ajk.m12));
        bzs[2] = (double) ((ajd.m00 * ajk.m20) + (ajd.m10 * ajk.m21) + (ajd.m20 * ajk.m22));
        bzs[3] = (double) ((ajd.m00 * ajk.m30) + (ajd.m10 * ajk.m31) + (ajd.m20 * ajk.m32));
        bzs[4] = (double) ((ajd.m01 * ajk.m00) + (ajd.m11 * ajk.m01) + (ajd.m21 * ajk.m02));
        bzs[5] = (double) ((ajd.m01 * ajk.m10) + (ajd.m11 * ajk.m11) + (ajd.m21 * ajk.m12));
        bzs[6] = (double) ((ajd.m01 * ajk.m20) + (ajd.m11 * ajk.m21) + (ajd.m21 * ajk.m22));
        bzs[7] = (double) ((ajd.m01 * ajk.m30) + (ajd.m11 * ajk.m31) + (ajd.m21 * ajk.m32));
        bzs[8] = (double) ((ajd.m02 * ajk.m00) + (ajd.m12 * ajk.m01) + (ajd.m22 * ajk.m02));
        bzs[9] = (double) ((ajd.m02 * ajk.m10) + (ajd.m12 * ajk.m11) + (ajd.m22 * ajk.m12));
        bzs[10] = (double) ((ajd.m02 * ajk.m20) + (ajd.m12 * ajk.m21) + (ajd.m22 * ajk.m22));
        bzs[11] = (double) ((ajd.m22 * ajk.m32) + (ajd.m02 * ajk.m30) + (ajd.m12 * ajk.m31));
        bzs[12] = (ajr.x * ((double) ajk.m00)) + (ajr.y * ((double) ajk.m01)) + (ajr.z * ((double) ajk.m02)) + ((double) ajk.m03);
        bzs[13] = (ajr.x * ((double) ajk.m10)) + (ajr.y * ((double) ajk.m11)) + (ajr.z * ((double) ajk.m12)) + ((double) ajk.m13);
        bzs[14] = (ajr.x * ((double) ajk.m20)) + (ajr.y * ((double) ajk.m21)) + (ajr.z * ((double) ajk.m22)) + ((double) ajk.m23);
        bzs[15] = (ajr.x * ((double) ajk.m30)) + (ajr.y * ((double) ajk.m31)) + (ajr.z * ((double) ajk.m32)) + ((double) ajk.m33);
        this.bzp[C3863a.FRUSTUM_RIGHT_PLANE.ordinal()].mo720c(bzs[3] - bzs[0], bzs[7] - bzs[4], bzs[11] - bzs[8], bzs[15] - bzs[12]);
        this.bzp[C3863a.FRUSTUM_LEFT_PLANE.ordinal()].mo720c(bzs[3] + bzs[0], bzs[7] + bzs[4], bzs[11] + bzs[8], bzs[15] + bzs[12]);
        this.bzp[C3863a.FRUSTUM_BOTTOM_PLANE.ordinal()].mo720c(bzs[3] + bzs[1], bzs[7] + bzs[5], bzs[11] + bzs[9], bzs[15] + bzs[13]);
        this.bzp[C3863a.FRUSTUM_TOP_PLANE.ordinal()].mo720c(bzs[3] - bzs[1], bzs[7] - bzs[5], bzs[11] - bzs[9], bzs[15] - bzs[13]);
        this.bzp[C3863a.FRUSTUM_FAR_PLANE.ordinal()].mo720c(bzs[3] - bzs[2], bzs[7] - bzs[6], bzs[11] - bzs[10], bzs[15] - bzs[14]);
        this.bzp[C3863a.FRUSTUM_NEAR_PLANE.ordinal()].mo720c(bzs[3] + bzs[2], bzs[7] + bzs[6], bzs[11] + bzs[10], bzs[15] + bzs[14]);
        for (int i = 0; i < 6; i++) {
            this.bzp[i].normalize();
            this.bzq[i][0] = 3;
            this.bzq[i][3] = 0;
            this.bzq[i][1] = 4;
            this.bzq[i][4] = 1;
            this.bzq[i][2] = 5;
            this.bzq[i][5] = 2;
            if (this.bzp[i].getNormal().x <= ScriptRuntime.NaN) {
                this.bzq[i][0] = 0;
                this.bzq[i][3] = 3;
            }
            if (this.bzp[i].getNormal().y <= ScriptRuntime.NaN) {
                this.bzq[i][1] = 1;
                this.bzq[i][4] = 4;
            }
            if (this.bzp[i].getNormal().z <= ScriptRuntime.NaN) {
                this.bzq[i][2] = 2;
                this.bzq[i][5] = 5;
            }
        }
    }

    /* renamed from: a */
    public boolean mo1159a(C1085Pt pt) {
        for (int i = 0; i < 6; i++) {
            if (this.bzp[i].getNormal().mo9494b((IGeometryF) pt.bny()) + this.bzp[i].dnz() < ((double) (-pt.getRadius()))) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    public boolean mo1161a(aVS avs) {
        for (int i = 0; i < 6; i++) {
            if (this.bzp[i].getNormal().mo9493az(avs.mo11775zO()) + this.bzp[i].dnz() < (-avs.dCJ())) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    public boolean mo1160a(aLH alh) {
        Vec3d dim = alh.dim();
        this.bzr[0] = dim.x;
        this.bzr[1] = dim.y;
        this.bzr[2] = dim.z;
        Vec3d din = alh.din();
        this.bzr[3] = din.x;
        this.bzr[4] = din.y;
        this.bzr[5] = din.z;
        for (int i = 0; i < 6; i++) {
            Vec3d normal = this.bzp[i].getNormal();
            if ((normal.x * this.bzr[this.bzq[i][0]]) + (normal.y * this.bzr[this.bzq[i][1]]) + (normal.z * this.bzr[this.bzq[i][2]]) + this.bzp[i].dnz() < ScriptRuntime.NaN) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: b */
    public int mo1162b(aLH alh) {
        boolean z = false;
        Vec3d dim = alh.dim();
        this.bzr[0] = dim.x;
        this.bzr[1] = dim.y;
        this.bzr[2] = dim.z;
        Vec3d din = alh.din();
        this.bzr[3] = din.x;
        this.bzr[4] = din.y;
        this.bzr[5] = din.z;
        for (int i = 0; i < 6; i++) {
            Vec3d normal = this.bzp[i].getNormal();
            if ((normal.x * this.bzr[this.bzq[i][0]]) + (normal.y * this.bzr[this.bzq[i][1]]) + (normal.z * this.bzr[this.bzq[i][2]]) + this.bzp[i].dnz() < ScriptRuntime.NaN) {
                return 0;
            }
            if ((normal.z * this.bzr[this.bzq[i][5]]) + (normal.x * this.bzr[this.bzq[i][3]]) + (normal.y * this.bzr[this.bzq[i][4]]) + this.bzp[i].dnz() <= ScriptRuntime.NaN) {
                z = true;
            }
        }
        if (z) {
            return 1;
        }
        return 2;
    }

    /* renamed from: v */
    public boolean mo1163v(Vec3d ajr) {
        for (int i = 0; i < 6; i++) {
            if (this.bzp[i].mo716aM(ajr) == C0107BM.C0108a.POINT_BEHIND_PLANE) {
                return false;
            }
        }
        return true;
    }

    public C0107BM[] akT() {
        return this.bzp;
    }

    public String toString() {
        String str = "";
        for (int i = 0; i < 6; i++) {
            str = String.valueOf(str) + C3863a.values()[i] + ":" + this.bzp[i] + "\n";
        }
        return str;
    }

    /* renamed from: a.vv$a */
    public enum C3863a {
        FRUSTUM_LEFT_PLANE,
        FRUSTUM_RIGHT_PLANE,
        FRUSTUM_TOP_PLANE,
        FRUSTUM_BOTTOM_PLANE,
        FRUSTUM_NEAR_PLANE,
        FRUSTUM_FAR_PLANE
    }
}
