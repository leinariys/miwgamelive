package taikodom.render.shaders.parameters;

import taikodom.render.DrawContext;

/* compiled from: a */
public abstract class GlobalShaderParameter {
    private int layer;
    private String name;

    public GlobalShaderParameter(int i, String str) {
        this.layer = i;
        setName(str);
    }

    public abstract void bind(int i, DrawContext drawContext);

    public int getLayer() {
        return this.layer;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }
}
