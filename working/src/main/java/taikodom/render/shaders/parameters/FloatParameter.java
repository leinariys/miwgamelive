package taikodom.render.shaders.parameters;

import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public class FloatParameter extends ShaderParameter {
    public float value;

    public FloatParameter() {
    }

    public FloatParameter(FloatParameter floatParameter) {
        super(floatParameter);
        this.value = floatParameter.value;
    }

    public RenderAsset cloneAsset() {
        return new FloatParameter(this);
    }

    public float getValue() {
        return this.value;
    }

    public void setValue(float f) {
        this.value = f;
    }

    public boolean bind(DrawContext drawContext) {
        if (drawContext.debugDraw()) {
            drawContext.logBind(this);
        }
        if (this.locationId == -1) {
            return false;
        }
        drawContext.getGl().glUniform1f(this.locationId, this.value);
        return true;
    }

    /* access modifiers changed from: protected */
    public void doAssign(ShaderParameter shaderParameter) {
        setValue(((FloatParameter) shaderParameter).getValue());
    }
}
