package taikodom.render.shaders.parameters;

import game.geometry.Vector2fWrap;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public class Vec2fParameter extends ShaderParameter {
    private final Vector2fWrap value = new Vector2fWrap();

    public Vec2fParameter() {
    }

    public Vec2fParameter(Vec2fParameter vec2fParameter) {
        super(vec2fParameter);
        this.value.set(vec2fParameter.value);
    }

    public RenderAsset cloneAsset() {
        return new Vec2fParameter(this);
    }

    public Vector2fWrap getValue() {
        return this.value;
    }

    public void setValue(Vector2fWrap aka) {
        this.value.set(aka);
    }

    public void setValue(float f, float f2) {
        this.value.set(f, f2);
    }

    public boolean bind(DrawContext drawContext) {
        if (drawContext.debugDraw()) {
            drawContext.logBind(this);
        }
        if (this.locationId == -1) {
            return false;
        }
        drawContext.getGl().glUniform2f(this.locationId, this.value.x, this.value.y);
        return true;
    }

    /* access modifiers changed from: protected */
    public void doAssign(ShaderParameter shaderParameter) {
        setValue(((Vec2fParameter) shaderParameter).getValue());
    }
}
