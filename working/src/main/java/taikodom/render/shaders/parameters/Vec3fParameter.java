package taikodom.render.shaders.parameters;

import com.hoplon.geometry.Vec3f;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public class Vec3fParameter extends ShaderParameter {
    private final Vec3f value = new Vec3f();

    public Vec3fParameter() {
    }

    public Vec3fParameter(Vec3fParameter vec3fParameter) {
        super(vec3fParameter);
        this.value.set(vec3fParameter.value);
    }

    public RenderAsset cloneAsset() {
        return new Vec3fParameter(this);
    }

    public Vec3f getValue() {
        return this.value;
    }

    public void setValue(Vec3f vec3f) {
        this.value.set(vec3f);
    }

    public void setValue(float f, float f2, float f3) {
        this.value.set(f, f2, f3);
    }

    public boolean bind(DrawContext drawContext) {
        if (drawContext.debugDraw()) {
            drawContext.logBind(this);
        }
        if (this.locationId == -1) {
            return false;
        }
        try {
            drawContext.getGl().glUniform3f(this.locationId, this.value.x, this.value.y, this.value.z);
            return true;
        } catch (Exception e) {
            throw new RuntimeException("Error trying to bind parameter: " + getName() + " at location: " + this.locationId, e);
        }
    }

    /* access modifiers changed from: protected */
    public void doAssign(ShaderParameter shaderParameter) {
        setValue(((Vec3fParameter) shaderParameter).getValue());
    }
}
