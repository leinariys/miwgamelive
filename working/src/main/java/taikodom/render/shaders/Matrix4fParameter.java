package taikodom.render.shaders;

import game.geometry.Matrix4fWrap;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;
import taikodom.render.shaders.parameters.ShaderParameter;

/* compiled from: a */
public class Matrix4fParameter extends ShaderParameter {
    private final Matrix4fWrap value = new Matrix4fWrap();

    public Matrix4fParameter() {
    }

    public Matrix4fParameter(Matrix4fParameter matrix4fParameter) {
        super(matrix4fParameter);
        this.value.set(matrix4fParameter.value);
    }

    public RenderAsset cloneAsset() {
        return new Matrix4fParameter(this);
    }

    public Matrix4fWrap getValue() {
        return this.value;
    }

    public void setValue(Matrix4fWrap ajk) {
        this.value.set(ajk);
    }

    public boolean bind(DrawContext drawContext) {
        if (drawContext.debugDraw()) {
            drawContext.logBind(this);
        }
        if (this.locationId == -1) {
            return false;
        }
        drawContext.getGl().glUniformMatrix4fv(this.locationId, 1, false, this.value.mo13998b(drawContext.floatBuffer60Temp0));
        return true;
    }

    /* access modifiers changed from: protected */
    public void doAssign(ShaderParameter shaderParameter) {
        setValue(((Matrix4fParameter) shaderParameter).getValue());
    }
}
