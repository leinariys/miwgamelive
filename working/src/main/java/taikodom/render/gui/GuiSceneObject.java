package taikodom.render.gui;

import com.hoplon.geometry.Color;
import game.geometry.TransformWrap;
import game.geometry.Vector2fWrap;
import taikodom.render.GuiContext;
import taikodom.render.SceneView;
import taikodom.render.StepContext;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public abstract class GuiSceneObject extends RenderAsset {
    public final TransformWrap globalTransform;
    public final Vector2fWrap position;
    public final Color primitiveColor;
    public final TransformWrap transform;
    public boolean disposed;
    public boolean render;
    public SceneView sceneView;

    public GuiSceneObject() {
        this.position = new Vector2fWrap();
        this.primitiveColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.render = true;
        this.disposed = false;
        this.globalTransform = new TransformWrap();
        this.transform = new TransformWrap();
    }

    public GuiSceneObject(GuiSceneObject guiSceneObject) {
        this.position = new Vector2fWrap();
        this.primitiveColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.render = true;
        this.disposed = false;
        this.position.set(guiSceneObject.position);
        this.globalTransform = new TransformWrap();
        this.globalTransform.mo17344b(guiSceneObject.globalTransform);
        this.transform = new TransformWrap();
        this.transform.mo17344b(guiSceneObject.globalTransform);
    }

    public abstract void render(GuiContext guiContext);

    public int step(StepContext stepContext) {
        return 0;
    }

    public Color getPrimitiveColor() {
        return this.primitiveColor;
    }

    public void setPrimitiveColor(Color color) {
        this.primitiveColor.set(color);
    }

    public void setPrimitiveColor(float f, float f2, float f3, float f4) {
        this.primitiveColor.set(f, f2, f3, f4);
    }

    public boolean isRender() {
        return this.render;
    }

    public void setRender(boolean z) {
        this.render = z;
    }

    public boolean isDisposed() {
        return this.disposed;
    }

    public void dispose() {
        this.disposed = true;
    }

    public Vector2fWrap getPosition() {
        return this.position;
    }

    public void setPosition(Vector2fWrap aka) {
        this.position.set(aka);
    }

    public SceneView getSceneView() {
        return this.sceneView;
    }

    public void setSceneView(SceneView sceneView2) {
        this.sceneView = sceneView2;
    }
}
