package taikodom.render.gui;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import org.mozilla.javascript.ScriptRuntime;
import taikodom.render.GuiContext;
import taikodom.render.SceneView;
import taikodom.render.StepContext;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Billboard;
import taikodom.render.scene.SceneObject;
import taikodom.render.textures.Material;

/* compiled from: a */
public class GAim extends GuiRenderObject {
    private static Billboard billBoard = new Billboard();
    private int imageHeight;
    private int imageWidth;
    private boolean onScreen;
    private float projectionDistance;
    private Vec3d screenPos;
    private SceneObject shipObject;
    private SceneView shipScene;
    private Vec3f shipZ;
    private SceneObject targetObject;
    private Vec3d tempVect;
    private Vec3f zTransform;

    public GAim() {
        this.projectionDistance = 1.0E12f;
        this.onScreen = false;
        this.screenPos = new Vec3d();
        this.tempVect = new Vec3d();
        this.shipZ = new Vec3f();
        this.zTransform = new Vec3f(0.0f, 0.0f, 1.0f);
    }

    public GAim(GAim gAim) {
        super(gAim);
        this.projectionDistance = 1.0E12f;
        this.onScreen = false;
        this.material = new Material(gAim.material);
        this.screenPos = new Vec3d();
        this.tempVect = new Vec3d();
        this.shipZ = new Vec3f();
        this.zTransform = new Vec3f(0.0f, 0.0f, 1.0f);
    }

    public void setShip(SceneObject sceneObject, SceneView sceneView) {
        this.shipObject = sceneObject;
        this.shipScene = sceneView;
    }

    public SceneObject getShip() {
        return this.shipObject;
    }

    public SceneObject getTarget() {
        return this.targetObject;
    }

    public void setTarget(SceneObject sceneObject) {
        this.targetObject = sceneObject;
    }

    public void setProjectionDistance(float f) {
        this.projectionDistance = f;
    }

    public int step(StepContext stepContext) {
        if (this.shipObject == null || this.shipScene == null) {
            return 0;
        }
        this.shipObject.getGlobalTransform().orientation.transform(this.zTransform, this.shipZ);
        if (this.shipObject.isDisposed()) {
            dispose();
            return 0;
        }
        if (this.imageWidth < 1 || this.imageHeight < 1) {
            this.imageWidth = (int) Math.abs(this.material.getDiffuseTexture().getTransformedWidth() * this.material.getXScaling());
            this.imageHeight = (int) Math.abs(this.material.getDiffuseTexture().getTransformedWidth() * this.material.getXScaling());
        }
        this.globalTransform.setIdentity();
        if (this.targetObject != null) {
            double n = Vec3d.m15653n(this.targetObject.getPosition(), this.shipObject.getPosition());
            this.tempVect.x = ((double) this.shipZ.x) * n;
            this.tempVect.y = ((double) this.shipZ.y) * n;
            this.tempVect.z = n * ((double) this.shipZ.z);
            this.screenPos.x = this.tempVect.x + this.shipObject.getPosition().x;
            this.screenPos.y = this.tempVect.y + this.shipObject.getPosition().y;
            this.screenPos.z = this.tempVect.z + this.shipObject.getPosition().z;
        } else {
            this.shipZ.x *= this.projectionDistance;
            this.shipZ.y *= this.projectionDistance;
            this.shipZ.z *= this.projectionDistance;
            this.screenPos.mo9497bG(this.shipZ);
            this.screenPos.add(this.shipObject.getPosition());
        }
        this.shipScene.getWindowCoords(this.screenPos, this.screenPos);
        if (this.screenPos.z > 1.0d || this.screenPos.z < ScriptRuntime.NaN) {
            this.onScreen = false;
        } else {
            this.onScreen = true;
        }
        this.transform.setIdentity();
        this.transform.setTranslation((double) ((int) Math.round(this.screenPos.x)), (double) ((int) Math.round(this.screenPos.y)), ScriptRuntime.NaN);
        this.position.set((float) ((int) Math.round(this.screenPos.x)), (float) ((int) Math.round(this.screenPos.y)));
        this.globalTransform.mo17350e(this.transform);
        this.globalTransform.mo17324B((float) this.imageHeight);
        this.globalTransform.mo17325C((float) this.imageWidth);
        return super.step(stepContext);
    }

    public void render(GuiContext guiContext) {
        if (this.render && this.onScreen) {
            guiContext.addPrimitive(this.material.getShader(), this.material, billBoard, this.globalTransform, this.primitiveColor);
        }
    }

    public RenderAsset cloneAsset() {
        return new GAim(this);
    }

    public void validate(SceneLoader sceneLoader) {
        this.material.setShader(sceneLoader.getDefaultBillboardShader());
        super.validate(sceneLoader);
    }

    public void setZTransform(Vec3f vec3f) {
        this.zTransform.set(vec3f);
    }
}
