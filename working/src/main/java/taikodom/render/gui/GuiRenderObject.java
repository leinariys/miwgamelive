package taikodom.render.gui;

import taikodom.render.GuiContext;
import taikodom.render.loader.RenderAsset;
import taikodom.render.textures.Material;

/* compiled from: a */
public class GuiRenderObject extends GuiSceneObject {
    public Material material;

    public GuiRenderObject() {
        this.material = new Material();
    }

    public GuiRenderObject(GuiRenderObject guiRenderObject) {
        this.material = (Material) smartClone(guiRenderObject.material);
    }

    /* access modifiers changed from: protected */
    public <T> T smartClone(T t) {
        if (t instanceof RenderAsset) {
            return (T) ((RenderAsset) t).cloneAsset();
        }
        return t;
    }

    public void render(GuiContext guiContext) {
    }

    public RenderAsset cloneAsset() {
        return null;
    }

    public Material getMaterial() {
        return this.material;
    }

    public void setMaterial(Material material2) {
        this.material = material2;
    }
}
