package taikodom.render.gui;

import gnu.trove.THashSet;
import taikodom.render.GuiContext;
import taikodom.render.StepContext;

import java.util.Set;

/* compiled from: a */
public class GuiScene {
    private String name = null;
    private Set<GuiSceneObject> objects = new THashSet();

    public void addChild(GuiSceneObject guiSceneObject) {
        if (guiSceneObject == null) {
            throw new NullPointerException("Cannot add a null SceneObject");
        }
        synchronized (this.objects) {
            this.objects.add(guiSceneObject);
        }
    }

    public void removeAllChildren() {
        synchronized (this.objects) {
            this.objects.clear();
        }
    }

    public void removeChild(GuiSceneObject guiSceneObject) {
        synchronized (this.objects) {
            this.objects.remove(guiSceneObject);
        }
    }

    public void render(GuiContext guiContext) {
        synchronized (this.objects) {
            for (GuiSceneObject render : this.objects) {
                render.render(guiContext);
            }
        }
    }

    public void step(StepContext stepContext) {
        stepContext.incObjectSteps((long) this.objects.size());
        synchronized (this.objects) {
            for (GuiSceneObject step : this.objects) {
                step.step(stepContext);
            }
        }
    }

    public Set<GuiSceneObject> getChildren() {
        return this.objects;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String toString() {
        return "Scene: " + this.name;
    }

    public void addStaticChild(GuiSceneObject guiSceneObject) {
        if (guiSceneObject == null) {
            throw new NullPointerException("Cannot add a null SceneObject");
        }
        synchronized (this.objects) {
            this.objects.add(guiSceneObject);
        }
    }
}
