package taikodom.render.enums;

/* compiled from: a */
public enum FBOAttachTarget {
    TEXTURE_2D(3553),
    TEXTURE_RECTANGLE(34037),
    CUBE_POS_X(34069),
    CUBE_NEG_X(34070),
    CUBE_POS_Y(34071),
    CUBE_NEG_Y(34072),
    CUBE_POS_Z(34073),
    CUBE_NEG_Z(34074);

    private final int glEquivalent;

    private FBOAttachTarget(int i) {
        this.glEquivalent = i;
    }

    public int glEquivalent() {
        return this.glEquivalent;
    }
}
