package taikodom.render.enums;

/* compiled from: a */
public enum ShapeTypes {
    SHAPE_DISC,
    SHAPE_SPHERE,
    SHAPE_RECTANGLE,
    SHAPE_CUBE,
    SHAPE_SPIRAL
}
