package taikodom.render.enums;

/* compiled from: a */
public enum DepthFuncType {
    NEVER(512),
    ALWAYS(519),
    LESS(513),
    LEQUAL(515),
    EQUAL(514),
    GEQUAL(518),
    GREATER(516),
    NOT_EQUAL(517);

    private final int glEquivalent;

    private DepthFuncType(int i) {
        this.glEquivalent = i;
    }

    public int glEquivalent() {
        return this.glEquivalent;
    }
}
