package taikodom.render.enums;

/* compiled from: a */
public enum LightType {
    NONE,
    POINT_LIGHT,
    DIRECTIONAL_LIGHT,
    SPOT_LIGHT
}
