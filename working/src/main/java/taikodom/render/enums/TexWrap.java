package taikodom.render.enums;

/* compiled from: a */
public enum TexWrap {
    CLAMP(10496),
    CLAMP_TO_EDGE(33071),
    CLAMP_TO_BORDER(33069),
    REPEAT(10497),
    MIRRORED_REPEAT(33648);

    private final int glEquivalent;

    private TexWrap(int i) {
        this.glEquivalent = i;
    }

    public int glEquivalent() {
        return this.glEquivalent;
    }
}
