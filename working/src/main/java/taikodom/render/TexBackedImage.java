package taikodom.render;

import taikodom.render.textures.BaseTexture;

import java.awt.*;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;

/* compiled from: a */
public class TexBackedImage extends Image {
    private int height;
    private BaseTexture texture;
    private int width;
    private float xmul;
    private float ymul;

    public TexBackedImage() {
        this.width = 0;
        this.height = 0;
        this.xmul = 1.0f;
        this.ymul = 1.0f;
    }

    public TexBackedImage(int i, int i2, BaseTexture baseTexture) {
        this.height = i;
        this.width = i2;
        this.texture = baseTexture;
        this.xmul = 1.0f;
        this.ymul = 1.0f;
    }

    public TexBackedImage(int i, int i2, float f, float f2, BaseTexture baseTexture) {
        this.height = i;
        this.width = i2;
        this.xmul = f;
        this.ymul = f2;
        this.texture = baseTexture;
    }

    public Graphics getGraphics() {
        return null;
    }

    public int getHeight(ImageObserver imageObserver) {
        return this.height;
    }

    public Object getProperty(String str, ImageObserver imageObserver) {
        return null;
    }

    public ImageProducer getSource() {
        return null;
    }

    public int getWidth(ImageObserver imageObserver) {
        return this.width;
    }

    public BaseTexture getTexture() {
        return this.texture;
    }

    public void setTexture(BaseTexture baseTexture) {
        this.texture = baseTexture;
    }

    public void flush() {
    }

    public float getXmul() {
        return this.xmul;
    }

    public float getYmul() {
        return this.ymul;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int i) {
        this.height = i;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int i) {
        this.width = i;
    }
}
