package taikodom.render.impostor;

import com.hoplon.geometry.Vec3f;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import taikodom.render.DrawContext;
import taikodom.render.RenderContext;
import taikodom.render.RenderView;
import taikodom.render.SceneView;
import taikodom.render.camera.Camera;
import taikodom.render.enums.BlendType;
import taikodom.render.enums.DepthFuncType;
import taikodom.render.scene.SceneObject;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.ChannelInfo;
import taikodom.render.textures.DDSLoader;
import taikodom.render.textures.PBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.GLContext;
import java.util.ArrayList;
import java.util.List;

/* compiled from: a */
public class ImpostorManager {
    public static final int MAX_IMPOSTORS_GENERATED_FRAME = 10;
    public static final int MAX_IMPOSTOR_ATLAS_SIZE = 512;
    public static final int MAX_IMPOSTOR_RESOLUTION = 64;
    public static final int MIN_IMPOSTOR_RESOLUTION = 2;
    private static final float INTERVAL_TO_CHECK_SEC = 30.0f;
    private float currentElapsedTimeSec = 0.0f;
    private Camera impostorCamera;
    private List<ImpostorGroup> impostorGroups = new ArrayList();
    private RenderView impostorRenderView;
    private SceneView impostorSceneView;
    private Shader impostorShader = new Shader();
    private List<SceneObject> impostorsToUpdate = new ArrayList();
    private PBuffer pbuffer;
    private List<DynamicTextureAtlas> releasedAtlas = new ArrayList();

    public ImpostorManager(GLContext gLContext) {
        this.impostorShader.setName("Impostor shader");
        ShaderPass shaderPass = new ShaderPass();
        shaderPass.setName("Default Impostor pass1");
        ChannelInfo channelInfo = new ChannelInfo();
        channelInfo.setTexChannel(8);
        shaderPass.setChannelTextureSet(0, channelInfo);
        shaderPass.setColorArrayEnabled(true);
        shaderPass.setAlphaTestEnabled(true);
        shaderPass.setCullFaceEnabled(true);
        shaderPass.setAlphaRef(0.95f);
        shaderPass.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
        shaderPass.setSrcBlend(BlendType.ONE);
        shaderPass.setBlendEnabled(true);
        ShaderPass shaderPass2 = new ShaderPass();
        shaderPass2.setName("Default Impostor pass2");
        shaderPass2.setChannelTextureSet(0, channelInfo);
        shaderPass2.setBlendEnabled(true);
        shaderPass2.setColorArrayEnabled(true);
        shaderPass2.setAlphaTestEnabled(true);
        shaderPass2.setDepthFunc(DepthFuncType.LESS);
        shaderPass2.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
        shaderPass2.setSrcBlend(BlendType.ONE);
        shaderPass2.setCullFaceEnabled(true);
        this.impostorShader.addPass(shaderPass);
        this.impostorShader.addPass(shaderPass2);
        this.impostorRenderView = new RenderView(gLContext);
        this.impostorRenderView.getDrawContext().setCurrentFrame(1000);
        this.impostorRenderView.getRenderContext().setCullOutSmallObjects(false);
        this.impostorRenderView.getRenderInfo().setClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        this.impostorSceneView = new SceneView();
        this.impostorSceneView.setClearColorBuffer(true);
        this.impostorCamera = this.impostorSceneView.getCamera();
    }

    public ImpostorGroup getImpostorGroupForSize(int i, RenderContext renderContext) {
        for (ImpostorGroup next : this.impostorGroups) {
            if (next.getTexAtlas().hasFreeSlot(i)) {
                return next;
            }
        }
        ImpostorGroup impostorGroup = new ImpostorGroup();
        DynamicTextureAtlas dynamicTextureAtlas = new DynamicTextureAtlas(512, 64, 2, renderContext.getDc());
        dynamicTextureAtlas.getAtlasMaterial().setShader(this.impostorShader);
        impostorGroup.setTexAtlas(dynamicTextureAtlas);
        this.impostorGroups.add(impostorGroup);
        return impostorGroup;
    }

    public void addImpostor(SceneObject sceneObject) {
        this.impostorsToUpdate.add(sceneObject);
    }

    public void updateImpostors(RenderContext renderContext, SceneView sceneView) {
        this.currentElapsedTimeSec += renderContext.getDeltaTime();
        if (this.currentElapsedTimeSec > 30.0f) {
            this.currentElapsedTimeSec = 0.0f;
            for (ImpostorGroup next : this.impostorGroups) {
                next.removeInvalidImpostors(renderContext);
                if (next.getImpostorsCount() == 0) {
                    next.setRecalculateCenter(true);
                }
            }
            for (DynamicTextureAtlas releaseReferences : this.releasedAtlas) {
                releaseReferences.releaseReferences();
            }
            this.releasedAtlas.clear();
        }
        this.impostorSceneView.setSceneViewQuality(sceneView.getSceneViewQuality());
        this.impostorRenderView.getRenderContext().setSceneViewQuality(sceneView.getSceneViewQuality());
        this.impostorRenderView.getDrawContext().setMaxShaderQuality(sceneView.getSceneViewQuality().getShaderQuality());
        int size = this.impostorsToUpdate.size();
        if (size > 0) {
            DrawContext dc = renderContext.getDc();
            if (this.pbuffer == null) {
                this.pbuffer = dc.getAuxPBuffer();
                this.pbuffer.activate(dc);
                dc.getGl().glClear(DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ);
                this.pbuffer.deactivate(dc);
                dc.getGlContext().makeCurrent();
            }
            Camera camera = renderContext.getCamera();
            this.impostorCamera.setFarPlane(camera.getFarPlane());
            this.impostorCamera.setAspect(camera.getAspect());
            this.impostorCamera.setNearPlane(camera.getNearPlane());
            this.impostorCamera.setFovY(camera.getFovY());
            this.impostorCamera.setTransform(camera.getTransform());
            this.impostorCamera.calculateProjectionMatrix();
            TransformWrap transform = this.impostorCamera.getTransform();
            sceneView.setCamera(this.impostorCamera);
            renderContext.setCamera(this.impostorCamera);
            Vec3f vec3f = new Vec3f();
            Vec3f vec3f2 = new Vec3f();
            Vec3f vec3f3 = new Vec3f();
            Vec3d ajr = new Vec3d();
            Vec3d ajr2 = new Vec3d();
            this.pbuffer.activate(dc);
            GL gl = dc.getGl();
            RenderContext renderContext2 = this.impostorRenderView.getRenderContext();
            renderContext2.getLights().clear();
            renderContext2.getDc().setMainLight(dc.getMainLight());
            renderContext2.setGpuOffset(renderContext.getGpuOffset());
            renderContext2.setMainLight(dc.getMainLight());
            for (int i = 0; i < renderContext.getLights().size(); i++) {
                renderContext2.addLight(renderContext.getLights().get(i).light);
            }
            for (int i2 = 0; i2 < size; i2++) {
                SceneObject sceneObject = this.impostorsToUpdate.get(i2);
                Impostor impostor = sceneObject.getImpostor();
                if (i2 >= 10) {
                    if (!impostor.isTransition()) {
                        sceneObject.render(renderContext, false);
                    }
                    impostor.clearData();
                } else {
                    ajr.add(sceneObject.getAabbWorldSpace().din(), sceneObject.getAabbWorldSpace().dim());
                    ajr.scale(0.5d);
                    vec3f.sub(camera.getPosition(), ajr);
                    vec3f.normalize();
                    vec3f2.set(0.0f, 1.0f, 0.0f);
                    vec3f3.cross(vec3f2, vec3f);
                    vec3f3.normalize();
                    vec3f2.cross(vec3f, vec3f3);
                    transform.setX(vec3f3);
                    transform.setY(vec3f2);
                    transform.setZ(vec3f);
                    this.impostorCamera.setTransform(transform);
                    if (impostor.createImpostor(renderContext, sceneObject, this)) {
                        dc.incNumImpostorsUpdated();
                        DynamicTextureAtlas.MaterialSlot materialSlot = impostor.slot;
                        this.impostorRenderView.setViewport(-((sceneView.getViewport().width / 2) - (materialSlot.textureSize / 2)), -((sceneView.getViewport().height / 2) - (materialSlot.textureSize / 2)), sceneView.getViewport().width, sceneView.getViewport().height);
                        renderContext2.getRecords().clear();
                        renderContext2.setCamera(this.impostorCamera);
                        this.impostorRenderView.setupCamera(sceneView.getScene(), this.impostorCamera);
                        sceneObject.render(renderContext2, false);
                        this.impostorRenderView.renderEntries(this.impostorCamera);
                        impostor.getImpostorGroup().getTexAtlas().getAtlasTexture().copyFromCurrentBuffer(gl, materialSlot.f10356x, materialSlot.f10357y, 0, 0, materialSlot.textureSize, materialSlot.textureSize);
                        sceneView.getWindowCoords(ajr, ajr2);
                    }
                }
            }
            this.pbuffer.deactivate(dc);
            dc.getGlContext().makeCurrent();
            sceneView.setCamera(camera);
            renderContext.setCamera(camera);
            this.impostorsToUpdate.clear();
        }
        for (ImpostorGroup render : this.impostorGroups) {
            render.render(renderContext, true);
        }
    }

    public List<ImpostorGroup> getImpostorGroups() {
        return this.impostorGroups;
    }

    public void clear() {
        for (ImpostorGroup next : this.impostorGroups) {
            this.releasedAtlas.add(next.getTexAtlas());
            next.releaseReferences();
        }
        this.impostorGroups.clear();
    }
}
