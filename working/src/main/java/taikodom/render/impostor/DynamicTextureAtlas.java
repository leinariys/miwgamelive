package taikodom.render.impostor;

import com.hoplon.geometry.Color;
import game.geometry.Vector2fWrap;
import taikodom.render.DrawContext;
import taikodom.render.textures.Material;
import taikodom.render.textures.SimpleTexture;
import util.Maths;

import java.util.ArrayList;

/* compiled from: a */
public class DynamicTextureAtlas {
    Material atlasMaterial;
    SimpleTexture atlasTexture;
    ArrayList<MaterialSlot>[] freeSlots;
    ArrayList<MaterialSlot> usedSlots = new ArrayList<>();
    private int atlasSize;
    private int maxTextureSize;
    private int minTextureSize;

    public DynamicTextureAtlas(int i, int i2, int i3, DrawContext drawContext) {
        this.atlasSize = i;
        this.minTextureSize = i3;
        this.maxTextureSize = i2;
        init(drawContext);
    }

    private void init(DrawContext drawContext) {
        this.atlasTexture = new SimpleTexture();
        this.atlasTexture.createEmptyRGBA(this.atlasSize, this.atlasSize, 32);
        this.atlasTexture.fillWithColor(new Color(0.0f, 0.0f, 0.0f, 0.0f));
        int rb = (Maths.m22676rb(this.maxTextureSize) - Maths.m22676rb(this.minTextureSize)) + 1;
        this.freeSlots = new ArrayList[rb];
        int i = this.maxTextureSize;
        int i2 = 0;
        int i3 = this.atlasSize / 2;
        int i4 = 0;
        while (i2 < rb) {
            this.freeSlots[i2] = new ArrayList<>();
            int i5 = this.atlasSize / i;
            int i6 = i3 / i;
            for (int i7 = 0; i7 < i5; i7++) {
                int i8 = 0;
                while (true) {
                    int i9 = i8;
                    if (i9 >= i6) {
                        break;
                    }
                    int i10 = i4 + (i9 * i);
                    int i11 = i7 * i;
                    Vector2fWrap aka = new Vector2fWrap(((float) i10) / ((float) this.atlasSize), ((float) i11) / ((float) this.atlasSize));
                    this.freeSlots[i2].add(new MaterialSlot(i10, i11, aka, aka.mo9758g((double) (((float) i) / ((float) this.atlasSize)), (double) (((float) i) / ((float) this.atlasSize))), i));
                    i8 = i9 + 1;
                }
            }
            int i12 = i4 + i3;
            i /= 2;
            i2++;
            i3 /= 2;
            i4 = i12;
        }
        this.atlasMaterial = new Material();
        this.atlasMaterial.setDiffuseTexture(this.atlasTexture);
        this.atlasMaterial.setName("Atlas material " + this.atlasSize);
    }

    private int listForResolution(int i) {
        return Maths.m22676rb(this.maxTextureSize / i);
    }

    public boolean hasFreeSlot(int i) {
        return this.freeSlots[listForResolution(i)].size() > 0;
    }

    public MaterialSlot getFreeSlot(int i) {
        int listForResolution = listForResolution(i);
        if (this.freeSlots[listForResolution].size() <= 0) {
            return null;
        }
        MaterialSlot remove = this.freeSlots[listForResolution].remove(this.freeSlots[listForResolution].size() - 1);
        this.usedSlots.add(remove);
        return remove;
    }

    public void pushBackSlot(MaterialSlot materialSlot) {
        if (!this.usedSlots.remove(materialSlot)) {
            throw new RuntimeException("Releasing a material to an wrong atlas!");
        }
        this.freeSlots[listForResolution(materialSlot.textureSize)].add(materialSlot);
    }

    public int getAtlasSize() {
        return this.atlasSize;
    }

    public SimpleTexture getAtlasTexture() {
        return this.atlasTexture;
    }

    public Material getAtlasMaterial() {
        return this.atlasMaterial;
    }

    public void releaseReferences() {
        if (this.freeSlots != null) {
            for (ArrayList<MaterialSlot> clear : this.freeSlots) {
                clear.clear();
            }
            this.freeSlots = null;
        }
        if (this.usedSlots != null) {
            this.usedSlots.clear();
            this.usedSlots = null;
        }
        if (this.atlasTexture != null) {
            this.atlasTexture.releaseReferences();
            this.atlasTexture = null;
        }
        if (this.atlasMaterial != null) {
            this.atlasMaterial.releaseReferences();
            this.atlasMaterial = null;
        }
    }

    /* compiled from: a */
    public class MaterialSlot {
        public final Vector2fWrap maxCoordinates = new Vector2fWrap();
        public final Vector2fWrap minCoordinates = new Vector2fWrap();
        public int textureSize;

        /* renamed from: x */
        public int f10356x;

        /* renamed from: y */
        public int f10357y;

        public MaterialSlot(int i, int i2, Vector2fWrap aka, Vector2fWrap aka2, int i3) {
            this.f10356x = i;
            this.f10357y = i2;
            this.minCoordinates.set(aka);
            this.maxCoordinates.set(aka2);
            this.textureSize = i3;
        }
    }
}
