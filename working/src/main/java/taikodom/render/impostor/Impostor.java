package taikodom.render.impostor;

import com.hoplon.geometry.Vec3f;
import game.geometry.IGeometryF;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import org.mozilla.javascript.ScriptRuntime;
import taikodom.render.RenderContext;
import taikodom.render.camera.Camera;
import taikodom.render.scene.SceneObject;
import util.Maths;

import javax.vecmath.Vector3f;

/* compiled from: a */
public class Impostor {
    private static final float IMPOSTOR_TOLERANCE = 0.96f;
    private static final float RESOLUTION_FACTOR = 0.15f;
    public final Vec3d position = new Vec3d();
    public final Vec3d vert0 = new Vec3d();
    public final Vec3d vert1 = new Vec3d();
    public final Vec3d vert2 = new Vec3d();
    public final Vec3d vert3 = new Vec3d();
    private final Vec3f impostorVector = new Vec3f();
    public float currentColor;
    public double impostorDistance;
    public ImpostorGroup impostorGroup;
    public int impostorRes;
    public int indexStart;
    public float objectSize;
    public int requestRes;
    public DynamicTextureAtlas.MaterialSlot slot;
    public boolean valid;
    private double currentDistanceToCamera;
    private double impostorDistanceRelation;
    private float impostorSizeX;
    private float impostorSizeY;
    private boolean transition;

    public double getImpostorDistance() {
        return this.impostorDistance;
    }

    public void setImpostorDistance(double d) {
        this.impostorDistance = d;
    }

    public DynamicTextureAtlas.MaterialSlot getSlot() {
        return this.slot;
    }

    public void setSlot(DynamicTextureAtlas.MaterialSlot materialSlot) {
        this.slot = materialSlot;
    }

    public int getIndexStart() {
        return this.indexStart;
    }

    public void setIndexStart(int i) {
        this.indexStart = i;
    }

    public float getCurrentColor() {
        return this.currentColor;
    }

    public void setCurrentColor(float f) {
        if (this.currentColor != f) {
            this.currentColor = f;
            if (this.impostorGroup != null) {
                this.impostorGroup.regenerateMeshColor(this);
            }
        }
    }

    public int getRequestRes() {
        return this.requestRes;
    }

    public void setRequestRes(int i) {
        this.requestRes = i;
    }

    public boolean isValid() {
        return this.valid;
    }

    public void setValid(boolean z) {
        this.valid = z;
    }

    public boolean updateImpostor(RenderContext renderContext, SceneObject sceneObject) {
        int max;
        renderContext.incNumImpostorsUpdated();
        Vec3d ajr = renderContext.vec3dTemp0;
        Vec3f vec3f = renderContext.vec3fTemp0;
        Vec3f vec3f2 = renderContext.vec3fTemp1;
        Vec3f vec3f3 = renderContext.vec3fTemp2;
        Vec3f vec3f4 = renderContext.vec3fTemp3;
        sceneObject.getAabbWorldSpace().mo9843aH(ajr);
        TransformWrap bcVar = renderContext.transformTemp0;
        Camera camera = renderContext.getCamera();
        bcVar.mo17344b(camera.getTransform());
        vec3f.sub(camera.getGlobalTransform().position, ajr);
        vec3f.normalize();
        sceneObject.getGlobalTransform().orientation.mo13947a((Vector3f) vec3f, (Vector3f) vec3f4);
        if (this.impostorVector.dot(vec3f4) < ((((9.0f + (((float) this.impostorRes) / 64.0f)) * 0.1f * RESOLUTION_FACTOR) + 0.85f) * IMPOSTOR_TOLERANCE) + (((float) (this.impostorDistance / ((double) ((float) Math.sqrt(sceneObject.getCurrentSquaredDistanceToCamera()))))) * 0.04000002f)) {
            renderContext.addImpostor(sceneObject);
            return true;
        }
        vec3f2.set(0.0f, 1.0f, 0.0f);
        vec3f3.cross(vec3f2, vec3f);
        vec3f3.normalize();
        vec3f2.cross(vec3f, vec3f3);
        camera.getTransform().setX(vec3f3);
        camera.getTransform().setY(vec3f2);
        camera.getTransform().setZ(vec3f);
        camera.updateInternalGeometry((SceneObject) null);
        int ra = Maths.doubling((int) Math.ceil((double) renderContext.getSizeOnScreen(ajr, sceneObject.getAabbWSLenght())));
        camera.setTransform(bcVar);
        if (ra <= 2 || ra <= 2 || (max = Math.max(ra, ra)) == this.impostorRes) {
            return false;
        }
        this.requestRes = max;
        renderContext.addImpostor(sceneObject);
        return true;
    }

    public boolean createImpostor(RenderContext renderContext, SceneObject sceneObject, ImpostorManager impostorManager) {
        this.valid = false;
        Camera camera = renderContext.getCamera();
        if (this.requestRes == 0) {
            sceneObject.getAabbWorldSpace().mo9843aH(renderContext.vec3dTemp0);
            int ra = Maths.doubling((int) Math.ceil((double) renderContext.getSizeOnScreen(renderContext.vec3dTemp0, sceneObject.getAabbWSLenght())));
            if (ra <= 2 || ra <= 2) {
                return false;
            }
            this.requestRes = Math.max(ra, ra);
        }
        if (this.requestRes > 64) {
            this.impostorDistance = renderContext.getDistanceForSize(sceneObject.getAabbWSLenght(), 53.333332f);
            this.requestRes = 0;
            if (this.impostorGroup == null) {
                return false;
            }
            this.impostorGroup.removeImpostor(this);
            this.impostorGroup = null;
            this.slot = null;
            return false;
        } else if (this.requestRes <= 2) {
            return false;
        } else {
            this.impostorRes = this.requestRes;
            this.requestRes = 0;
            float f = (float) this.impostorRes;
            float f2 = (float) this.impostorRes;
            if (this.slot == null) {
                this.impostorGroup = impostorManager.getImpostorGroupForSize(this.impostorRes, renderContext);
                this.slot = this.impostorGroup.getMaterialSlot(this.impostorRes);
                if (this.slot == null) {
                    this.impostorGroup = null;
                    return false;
                }
                this.impostorGroup.addImpostor(this);
            } else if (this.slot.textureSize != this.impostorRes) {
                this.impostorGroup.removeImpostor(this);
                this.impostorGroup = impostorManager.getImpostorGroupForSize(this.impostorRes, renderContext);
                this.slot = this.impostorGroup.getMaterialSlot(this.impostorRes);
                if (this.slot == null) {
                    this.impostorGroup = null;
                    return false;
                }
                this.impostorGroup.addImpostor(this);
            }
            sceneObject.getAabbWorldSpace().mo9843aH(this.position);
            this.currentDistanceToCamera = this.position.mo9491ax(camera.getGlobalTransform().position);
            float tanHalfFovY = camera.getTanHalfFovY();
            this.impostorSizeX = ((float) ((((double) (camera.getAspect() * tanHalfFovY)) * this.currentDistanceToCamera) * ((double) f))) / ((float) renderContext.getDc().getViewport().width);
            this.impostorSizeY = ((float) (((double) f2) * (this.currentDistanceToCamera * ((double) tanHalfFovY)))) / ((float) renderContext.getDc().getViewport().height);
            Vec3f vec3f = renderContext.vec3fTemp0;
            camera.getGlobalTransform().getZ(vec3f);
            sceneObject.getGlobalTransform().orientation.mo13947a((Vector3f) vec3f, (Vector3f) this.impostorVector);
            this.objectSize = (float) sceneObject.getAabbWSLenght();
            this.position.sub(vec3f.mo23511mT((-this.objectSize) * 0.5f));
            this.impostorDistanceRelation = (this.currentDistanceToCamera - ((double) (this.objectSize * 0.5f))) / this.currentDistanceToCamera;
            camera.getGlobalTransform().getZ(renderContext.vec3fTemp2);
            renderContext.vec3dTemp2.sub(camera.getGlobalTransform().position, this.position);
            this.currentDistanceToCamera = renderContext.vec3dTemp2.mo9494b((IGeometryF) renderContext.vec3fTemp2);
            this.impostorSizeX = (float) (((double) this.impostorSizeX) * this.impostorDistanceRelation);
            this.impostorSizeY = (float) (((double) this.impostorSizeY) * this.impostorDistanceRelation);
            this.vert0.set((double) (-this.impostorSizeX), (double) (-this.impostorSizeY), ScriptRuntime.NaN);
            this.vert1.set((double) this.impostorSizeX, (double) (-this.impostorSizeY), ScriptRuntime.NaN);
            this.vert2.set((double) this.impostorSizeX, (double) this.impostorSizeY, ScriptRuntime.NaN);
            this.vert3.set((double) (-this.impostorSizeX), (double) this.impostorSizeY, ScriptRuntime.NaN);
            camera.getGlobalTransform().orientation.transform(this.vert0);
            camera.getGlobalTransform().orientation.transform(this.vert1);
            camera.getGlobalTransform().orientation.transform(this.vert2);
            camera.getGlobalTransform().orientation.transform(this.vert3);
            this.vert0.add(this.position);
            this.vert1.add(this.position);
            this.vert2.add(this.position);
            this.vert3.add(this.position);
            this.impostorGroup.setDirty(true);
            this.valid = true;
            this.transition = false;
            return true;
        }
    }

    public Vec3d getVert0() {
        return this.vert0;
    }

    public Vec3d getVert1() {
        return this.vert1;
    }

    public Vec3d getVert2() {
        return this.vert2;
    }

    public Vec3d getVert3() {
        return this.vert3;
    }

    public ImpostorGroup getImpostorGroup() {
        return this.impostorGroup;
    }

    public void clearData() {
        if (this.impostorGroup != null) {
            this.impostorGroup.removeImpostor(this);
            this.impostorGroup = null;
        }
        this.slot = null;
        this.valid = false;
        this.transition = false;
    }

    public double getCurrentDistanceToCameraPlane() {
        return this.currentDistanceToCamera;
    }

    public boolean isTransition() {
        return this.transition;
    }

    public void setTransition(boolean z) {
        this.transition = z;
    }
}
