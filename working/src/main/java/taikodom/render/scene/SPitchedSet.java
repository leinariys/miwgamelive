package taikodom.render.scene;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vector2fWrap;
import taikodom.render.PitchEntry;
import taikodom.render.StepContext;

import java.util.Iterator;
import java.util.Vector;

/* compiled from: a */
public class SPitchedSet extends SoundObject {
    public final Vector2fWrap distances;
    public final Vec3f position;
    public float currentControllerValue;
    public float gain;
    public boolean isPaused;
    public Vector<PitchEntry> pitchEntries;

    public SPitchedSet() {
        this.pitchEntries = new Vector<>(20);
        this.distances = new Vector2fWrap();
        this.position = new Vec3f();
        this.isPaused = false;
        this.gain = 1.0f;
        this.distances.x = 10.0f;
        this.distances.y = 1000.0f;
        this.currentControllerValue = 0.0f;
    }

    public SPitchedSet(SPitchedSet sPitchedSet) {
        super(sPitchedSet);
        this.pitchEntries = new Vector<>(20);
        this.distances = new Vector2fWrap();
        this.position = new Vec3f();
        this.isPaused = sPitchedSet.isPaused;
        this.gain = sPitchedSet.gain;
        this.distances.set(sPitchedSet.distances);
        this.currentControllerValue = sPitchedSet.currentControllerValue;
        for (int i = 0; i < sPitchedSet.pitchEntryCount(); i++) {
            this.pitchEntries.add(sPitchedSet.getPitchEntry(i).cloneAsset());
        }
    }

    public void releaseReferences() {
        super.releaseReferences();
        stop();
        Iterator<PitchEntry> it = this.pitchEntries.iterator();
        while (it.hasNext()) {
            it.next().releaseReferences();
        }
        this.pitchEntries.clear();
    }

    public void disposed() {
        stop();
        this.disposed = true;
    }

    public float getGain() {
        return this.gain;
    }

    public void setGain(float f) {
        boolean z;
        if (this.gain != 0.0f || f <= 0.0f) {
            z = false;
        } else {
            z = true;
        }
        this.gain = f;
        for (int i = 0; i < this.pitchEntries.size(); i++) {
            this.pitchEntries.get(i).setParentGain(this.gain);
        }
        if (z) {
            play();
        }
    }

    public Vector2fWrap getDistances() {
        return this.distances;
    }

    public void setDistances(Vector2fWrap aka) {
        this.distances.set(aka);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.pitchEntries.size()) {
                if (this.pitchEntries.get(i2).getSoundSource() != null) {
                    this.pitchEntries.get(i2).getSoundSource().setMinMaxDistance(this.distances.x, this.distances.y);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public boolean play() {
        this.isPaused = false;
        for (int i = 0; i < this.pitchEntries.size(); i++) {
            SoundSource soundSource = this.pitchEntries.get(i).getSoundSource();
            if (soundSource != null) {
                soundSource.getBuffer().forceSyncLoading();
                soundSource.create();
                soundSource.setMinMaxDistance(this.distances.x, this.distances.y);
                this.transform.getTranslation(this.position);
                soundSource.setPosition(this.position);
                soundSource.setVelocity(this.velocity);
                soundSource.setGain(0.0f);
                soundSource.play(0);
            }
        }
        return true;
    }

    public boolean stop() {
        int i = 0;
        this.isPaused = false;
        boolean z = true;
        while (true) {
            int i2 = i;
            if (i2 >= this.pitchEntries.size()) {
                return z;
            }
            if (this.pitchEntries.get(i2).getSoundSource() != null) {
                z = this.pitchEntries.get(i2).getSoundSource().releaseSample();
            }
            i = i2 + 1;
        }
    }

    public boolean isPlaying() {
        for (int i = 0; i < this.pitchEntries.size(); i++) {
            if (this.pitchEntries.get(i).getSoundSource() != null && this.pitchEntries.get(i).getSoundSource().isPlaying()) {
                return true;
            }
        }
        return false;
    }

    public boolean isValid() {
        for (int i = 0; i < this.pitchEntries.size(); i++) {
            if (this.pitchEntries.get(i).getSoundSource() != null && !this.pitchEntries.get(i).getSoundSource().isValid()) {
                return false;
            }
        }
        return true;
    }

    public boolean isAlive() {
        return isPlaying() || this.isPaused;
    }

    public int step(StepContext stepContext) {
        float gain2;
        if (this.currentControllerValue > 1.0f) {
            gain2 = getGain() * ((this.currentControllerValue * 2.0f) - 1.0f);
        } else {
            gain2 = (getGain() * (this.currentControllerValue + 1.0f)) / 2.0f;
        }
        for (int i = 0; i < this.pitchEntries.size(); i++) {
            this.pitchEntries.get(i).setParentGain(gain2);
            this.pitchEntries.get(i).update(this.currentControllerValue, stepContext);
            SoundSource soundSource = this.pitchEntries.get(i).getSoundSource();
            if (soundSource != null) {
                this.position.sub(this.globalTransform.position, stepContext.getCamera().getGlobalTransform().position);
                soundSource.setPosition(this.position);
                soundSource.setVelocity(this.velocity);
            }
        }
        return 0;
    }

    public void addPitchEntry(PitchEntry pitchEntry) {
        if (pitchEntry != null) {
            if (this.pitchEntries.size() > 1) {
                this.pitchEntries.get(this.pitchEntries.size() - 1).setLast(false);
            }
            this.pitchEntries.add(pitchEntry);
            if (this.pitchEntries.size() == 1) {
                pitchEntry.setFirst(true);
            } else {
                pitchEntry.setLast(true);
            }
        }
    }

    public int pitchEntryCount() {
        return this.pitchEntries.size();
    }

    public PitchEntry getPitchEntry(int i) {
        return this.pitchEntries.get(i);
    }

    public void setPitchEntry(int i, PitchEntry pitchEntry) {
        if (this.pitchEntries.size() > i) {
            this.pitchEntries.set(i, pitchEntry);
        } else {
            this.pitchEntries.add(pitchEntry);
        }
        if (i == 0) {
            pitchEntry.setFirst(true);
        }
        pitchEntry.setLast(true);
        for (int i2 = 0; i2 < this.pitchEntries.size() - 1; i2++) {
            this.pitchEntries.get(i2).setLast(false);
        }
    }

    public void removePitchEntry(int i) {
        this.pitchEntries.remove(this.pitchEntries.get(i));
    }

    public void removePitchEntry(PitchEntry pitchEntry) {
        this.pitchEntries.remove(pitchEntry);
    }

    public float getControllerValue() {
        return this.currentControllerValue;
    }

    public void setControllerValue(float f) {
        this.currentControllerValue = f;
    }

    public void onRemove() {
        super.onRemove();
        stop();
    }

    public SPitchedSet cloneAsset() {
        return new SPitchedSet(this);
    }
}
