package taikodom.render.scene;

import com.hoplon.geometry.Vec3f;
import com.sun.opengl.util.texture.TextureData;
import game.geometry.Vec3d;
import lombok.extern.slf4j.Slf4j;
import org.mozilla.javascript.ScriptRuntime;
import taikodom.render.RenderContext;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.enums.GeometryType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.Mesh;
import taikodom.render.primitives.OcclusionQuery;
import taikodom.render.textures.Texture;

import java.lang.reflect.Array;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

/* compiled from: a */
@Slf4j
public class RTerrain extends RenderObject {
    private static final int PATCH_COUNT = 32;
    private static final int PATCH_SIZE = 33;
    C5155b[][] patches;
    private Texture heightmap = null;
    private boolean inited = false;
    private Vec3f scale = new Vec3f(1.0f, 1.0f, 1.0f);

    public RTerrain() {
    }

    public RTerrain(RTerrain rTerrain) {
        super(rTerrain);
        this.patches = rTerrain.patches;
        this.inited = rTerrain.inited;
        this.heightmap = rTerrain.heightmap;
        this.scale = rTerrain.scale;
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render || !super.render(renderContext, z)) {
            return false;
        }
        if (!this.inited) {
            init();
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.patches.length) {
                return true;
            }
            int i3 = 0;
            while (true) {
                int i4 = i3;
                if (i4 >= this.patches[i2].length) {
                    break;
                }
                C5155b bVar = this.patches[i2][i4];
                float aC = renderContext.getCamera().getPosition().mo9486aC(bVar.center);
                int i5 = 0;
                if (aC > 1000000.0f) {
                    i5 = 5;
                } else if (aC > 500000.0f) {
                    i5 = 4;
                } else if (aC > 100000.0f) {
                    i5 = 3;
                } else if (aC > 200000.0f) {
                    i5 = 2;
                } else if (aC > 50000.0f) {
                    i5 = 1;
                }
                renderContext.addPrimitive(this.material, bVar.fMX.get(i5).mesh, this.transform, this.primitiveColor, (OcclusionQuery) null, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
                i3 = i4 + 1;
            }
            i = i2 + 1;
        }
    }

    private Vec3f nearestPoint(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        Vec3f j = vec3f3.mo23506j(vec3f);
        Vec3f dfO = vec3f2.mo23506j(vec3f).dfO();
        return vec3f.mo23503i(dfO.mo23510mS(j.dot(dfO)));
    }

    public void init() {
        float f;
        this.inited = true;
        if (this.heightmap == null) {
            log.warn("Missing heightmap for " + this);
            return;
        }
        TextureData textureData = this.heightmap.getTextureData(0);
        Buffer buffer = textureData.getBuffer();
        Class cls = buffer instanceof FloatBuffer ? Float.class : buffer instanceof ByteBuffer ? Byte.class : null;
        if (cls == null) {
            throw new RuntimeException("Unsupported buffer type " + buffer.getClass());
        }
        int width = textureData.getWidth() + 1;
        ExpandableVertexData expandableVertexData = new ExpandableVertexData(new VertexLayout(true, false, false, false, 0), width * (textureData.getHeight() + 1));
        int i = 0;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= textureData.getHeight()) {
                break;
            }
            float f2 = 0.0f;
            int i4 = 0;
            int i5 = i;
            while (i4 < textureData.getWidth()) {
                if (cls == Float.class) {
                    f = ((FloatBuffer) buffer).get();
                } else if (cls == Byte.class) {
                    ByteBuffer byteBuffer = (ByteBuffer) buffer;
                    byteBuffer.get();
                    byteBuffer.get();
                    f = (float) (byteBuffer.get() & 255);
                } else {
                    f = f2;
                }
                expandableVertexData.setPosition(i5, ((float) i4) * this.scale.x, this.scale.y * f, ((float) i3) * this.scale.z);
                i4++;
                f2 = f;
                i5++;
            }
            i = i5 + 1;
            expandableVertexData.setPosition(i5, ((float) textureData.getWidth()) * this.scale.x, this.scale.y * f2, ((float) i3) * this.scale.z);
            i2 = i3 + 1;
        }
        int i6 = 0;
        while (i6 < textureData.getWidth()) {
            expandableVertexData.setPosition(i, ((float) i6) * this.scale.x, this.scale.y * 0.0f, ((float) textureData.getHeight()) * this.scale.z);
            i6++;
            i++;
        }
        int i7 = i + 1;
        expandableVertexData.setPosition(i, ((float) textureData.getWidth()) * this.scale.x, this.scale.y * 0.0f, ((float) textureData.getHeight()) * this.scale.z);
        int height = textureData.getHeight() / 32;
        int width2 = textureData.getWidth() / 32;
        this.patches = (C5155b[][]) Array.newInstance(C5155b.class, new int[]{height, width2});
        for (int i8 = 0; i8 < height; i8++) {
            for (int i9 = 0; i9 < width2; i9++) {
                C5155b bVar = new C5155b(this, (C5155b) null);
                bVar.center = new Vec3d((double) (((float) ((i9 * 32) + 16)) * this.scale.x), ScriptRuntime.NaN, (double) (((float) ((i8 * 32) + 16)) * this.scale.z));
                int i10 = 32;
                int i11 = 1;
                while (i10 > 0) {
                    Mesh mesh = new Mesh();
                    mesh.setGeometryType(GeometryType.TRIANGLES);
                    mesh.setVertexData(expandableVertexData);
                    ExpandableIndexData expandableIndexData = new ExpandableIndexData(i10 * i10 * 2);
                    mesh.setIndexes(expandableIndexData);
                    int i12 = 0;
                    int i13 = 0;
                    while (true) {
                        int i14 = i13;
                        if (i14 >= 32) {
                            break;
                        }
                        for (int i15 = 0; i15 < 32; i15 += i11) {
                            int i16 = (i9 * 32) + i15 + (((i8 * 32) + i14) * width);
                            int i17 = i16 + i11;
                            int i18 = (width * i11) + i16;
                            int i19 = i12 + 1;
                            expandableIndexData.setIndex(i12, i16);
                            int i20 = i19 + 1;
                            expandableIndexData.setIndex(i19, i18);
                            int i21 = i20 + 1;
                            expandableIndexData.setIndex(i20, i17);
                            int i22 = i21 + 1;
                            expandableIndexData.setIndex(i21, i17);
                            int i23 = i22 + 1;
                            expandableIndexData.setIndex(i22, i18);
                            i12 = i23 + 1;
                            expandableIndexData.setIndex(i23, (width * i11) + i16 + i11);
                        }
                        i13 = i14 + i11;
                    }
                    expandableIndexData.compact();
                    C5154a aVar = new C5154a(this, (C5154a) null);
                    aVar.mesh = mesh;
                    bVar.fMX.add(aVar);
                    i10 /= 2;
                    i11 *= 2;
                }
                this.patches[i8][i9] = bVar;
            }
        }
    }

    public void setScale(Vec3f vec3f) {
        this.scale = vec3f;
    }

    public Texture getHeightmap() {
        return this.heightmap;
    }

    public void setHeightmap(Texture texture) {
        this.heightmap = texture;
    }

    public RenderAsset cloneAsset() {
        return new RTerrain(this);
    }

    /* renamed from: taikodom.render.scene.RTerrain$a */
    private class C5154a {
        float awF;
        Mesh mesh;

        private C5154a() {
        }

        /* synthetic */ C5154a(RTerrain rTerrain, C5154a aVar) {
            this();
        }
    }

    /* renamed from: taikodom.render.scene.RTerrain$b */
    /* compiled from: a */
    private class C5155b {
        Vec3d center;
        List<C5154a> fMX;

        private C5155b() {
            this.fMX = new ArrayList();
        }

        /* synthetic */ C5155b(RTerrain rTerrain, C5155b bVar) {
            this();
        }
    }
}
