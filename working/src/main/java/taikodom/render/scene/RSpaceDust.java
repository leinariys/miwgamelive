package taikodom.render.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import game.geometry.Vector2fWrap;
import game.geometry.Vector4fWrap;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;
import taikodom.render.camera.Camera;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.enums.GeometryType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Mesh;
import taikodom.render.textures.BaseTexture;

import javax.vecmath.Tuple3f;

/* compiled from: a */
public class RSpaceDust extends RenderObject {

    /* renamed from: Y */
    static final Vec3f f10365Y = new Vec3f();

    /* renamed from: Z */
    static final Vec3f f10366Z = new Vec3f();
    static final Vec3f camPointDist = new Vec3f();
    static final Vec3f csp0 = new Vec3f();

    /* renamed from: d1 */
    static final Vec3f f10367d1 = new Vec3f();

    /* renamed from: d2 */
    static final Vec3f f10368d2 = new Vec3f();

    /* renamed from: d3 */
    static final Vec3f f10369d3 = new Vec3f();

    /* renamed from: d4 */
    static final Vec3f f10370d4 = new Vec3f();
    static final Vector2fWrap tex1 = new Vector2fWrap(0.0f, 0.0f);
    static final Vector2fWrap tex2 = new Vector2fWrap(1.0f, 0.0f);
    static final Vector2fWrap tex3 = new Vector2fWrap(1.0f, 1.0f);
    static final Vector2fWrap tex4 = new Vector2fWrap(0.0f, 1.0f);
    static final TransformWrap ticamera = new TransformWrap();
    /* renamed from: v1 */
    static final Vec3f f10371v1 = new Vec3f();
    /* renamed from: v2 */
    static final Vec3f f10372v2 = new Vec3f();
    static final Color white = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    private static final Vec3f tempVect = new Vec3f();
    private static double speedModificator = 4.340000152587891d;
    final Vec3d dislocation;
    final Vec3d lastCamPos;
    private final Vector2fWrap billboardSize;
    C3778uv random;
    private Mesh expandableMesh;
    private float fadeIn;
    private float fadeOut;
    private float halfRegionSize;
    private ExpandableIndexData indexes;
    private int lastVertex;
    private float numBillboards;
    private SceneObject referenceObject;
    private float regionSize;
    private double speedCap;
    private ExpandableVertexData vertexes;

    public RSpaceDust() {
        this.speedCap = 20.0d;
        this.lastCamPos = new Vec3d();
        this.dislocation = new Vec3d();
        this.fadeIn = 0.2f;
        this.fadeOut = 0.2f;
        this.numBillboards = 50.0f;
        this.regionSize = 200.0f;
        this.halfRegionSize = this.regionSize / 2.0f;
        this.expandableMesh = null;
        this.billboardSize = new Vector2fWrap(10.0f, 10.0f);
        this.random = new C3778uv(10);
        this.vertexes = new ExpandableVertexData(new VertexLayout(true, true, false, false, 1), 100);
        this.indexes = new ExpandableIndexData(400);
        this.expandableMesh = new Mesh();
        this.expandableMesh.setName("Expandable mesh for a RSpacedust");
        this.expandableMesh.setGeometryType(GeometryType.QUADS);
        this.expandableMesh.setVertexData(this.vertexes);
        this.expandableMesh.setIndexes(this.indexes);
    }

    RSpaceDust(RSpaceDust rSpaceDust) {
        this.speedCap = 20.0d;
        this.lastCamPos = new Vec3d();
        this.dislocation = new Vec3d();
        this.fadeIn = rSpaceDust.fadeIn;
        this.fadeOut = rSpaceDust.fadeOut;
        this.numBillboards = rSpaceDust.numBillboards;
        this.regionSize = rSpaceDust.regionSize;
        this.halfRegionSize = this.regionSize / 2.0f;
        this.expandableMesh = null;
        this.billboardSize = new Vector2fWrap(rSpaceDust.billboardSize.x, rSpaceDust.billboardSize.y);
        this.random = new C3778uv(10);
        this.material = rSpaceDust.getMaterial();
        this.primitiveColor.set(rSpaceDust.primitiveColor);
        this.vertexes = new ExpandableVertexData(new VertexLayout(true, true, false, false, 1), 100);
        this.indexes = new ExpandableIndexData(400);
        this.expandableMesh = new Mesh();
        this.expandableMesh.setName("Expandable mesh for a RSpacedust");
        this.expandableMesh.setGeometryType(GeometryType.QUADS);
        this.expandableMesh.setVertexData(this.vertexes);
        this.expandableMesh.setIndexes(this.indexes);
    }

    static double dpmodd(double d, double d2) {
        return ((d % d2) + d2) % d2;
    }

    public void dispose() {
    }

    public void releaseReferences() {
        super.releaseReferences();
        this.indexes.clear();
        this.vertexes.data().clear();
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render) {
            return false;
        }
        this.indexes.clear();
        this.indexes.setSize(0);
        this.vertexes.data().clear();
        this.lastVertex = 0;
        for (int i = 0; i < this.material.textureCount(); i++) {
            BaseTexture texture = this.material.getTexture(i);
            if (texture != null) {
                texture.addArea(1024.0f);
            }
        }
        if (!super.render(renderContext, z)) {
            return false;
        }
        if (this.referenceObject == null) {
            processSpaceDust(renderContext);
            if (renderContext.getSceneViewQuality().getEnvFxQuality() > 0) {
                return true;
            }
            renderContext.addPrimitive(this.material, this.expandableMesh, this.transform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
        } else {
            processSpaceDustForReference(renderContext);
            renderContext.addPrimitive(this.material, this.expandableMesh, this.transform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
        }
        return true;
    }

    public int step(StepContext stepContext) {
        super.step(stepContext);
        return 0;
    }

    public void setReferenceObject(SceneObject sceneObject) {
        this.referenceObject = sceneObject;
    }

    /* access modifiers changed from: package-private */
    public void processSpaceDust(RenderContext renderContext) {
        float f;
        Camera camera = renderContext.getCamera();
        TransformWrap transform = camera.getTransform();
        Vec3d ajr = renderContext.vec3dTemp0;
        ajr.mo9484aA(camera.getPosition());
        ajr.sub(this.lastCamPos);
        ajr.scale(speedModificator);
        double length = ajr.length();
        if (length > this.speedCap) {
            ajr.scale(this.speedCap / length);
        }
        this.dislocation.add(ajr);
        this.lastCamPos.mo9484aA(camera.getPosition());
        this.transform.setIdentity();
        this.transform.setTranslation(camera.getPosition());
        transform.mo17336a(ticamera);
        float f2 = this.regionSize;
        Vector2fWrap aka = this.billboardSize;
        float nearPlane = (this.fadeOut * this.halfRegionSize * 0.5f) + camera.getNearPlane();
        float nearPlane2 = ((1.0f - this.fadeIn) * this.halfRegionSize * 0.5f) + camera.getNearPlane();
        float f3 = getPrimitiveColor().w;
        float nearPlane3 = 1.0f / (this.halfRegionSize - (nearPlane2 - camera.getNearPlane()));
        float f4 = 1.0f / nearPlane;
        this.random.mo22467eP(hashCode());
        Vec3f vectorX = transform.getVectorX();
        Vec3f vectorY = transform.getVectorY();
        f10367d1.x = (-(vectorX.x * aka.x)) - (vectorY.x * aka.y);
        f10367d1.y = (-(vectorX.y * aka.x)) - (vectorY.y * aka.y);
        f10367d1.z = (-(vectorX.z * aka.x)) - (vectorY.z * aka.y);
        f10368d2.x = (vectorX.x * aka.x) - (vectorY.x * aka.y);
        f10368d2.y = (vectorX.y * aka.x) - (vectorY.y * aka.y);
        f10368d2.z = (vectorX.z * aka.x) - (vectorY.z * aka.y);
        f10369d3.x = (vectorX.x * aka.x) + (vectorY.x * aka.y);
        f10369d3.y = (vectorX.y * aka.x) + (vectorY.y * aka.y);
        f10369d3.z = (vectorX.z * aka.x) + (vectorY.z * aka.y);
        f10370d4.x = (-(vectorX.x * aka.x)) + (vectorY.x * aka.y);
        f10370d4.y = (-(vectorX.y * aka.x)) + (vectorY.y * aka.y);
        f10370d4.z = (vectorY.z * aka.y) + (-(vectorX.z * aka.x));
        for (int i = 0; ((float) i) < this.numBillboards; i++) {
            csp0.x = (float) (dpmodd((((double) this.random.mo22468j(0.0f, f2)) - this.dislocation.x) + ((double) this.halfRegionSize), (double) f2) - ((double) this.halfRegionSize));
            csp0.y = (float) (dpmodd((((double) this.random.mo22468j(0.0f, f2)) - this.dislocation.y) + ((double) this.halfRegionSize), (double) f2) - ((double) this.halfRegionSize));
            csp0.z = (float) (dpmodd((((double) this.random.mo22468j(0.0f, f2)) - this.dislocation.z) + ((double) this.halfRegionSize), (double) f2) - ((double) this.halfRegionSize));
            float f5 = -((csp0.x * ticamera.orientation.m20) + (csp0.y * ticamera.orientation.m21) + (csp0.z * ticamera.orientation.m22));
            if (f5 >= 1.0f) {
                if (f5 < nearPlane) {
                    f = (f5 - camera.getNearPlane()) * f4;
                } else if (f5 > nearPlane2) {
                    f = 1.0f - ((f5 - nearPlane2) * nearPlane3);
                    if (f > 1.0f) {
                        f = 1.0f;
                    }
                } else {
                    f = 1.0f;
                }
                if (f <= 1.0f && f > 0.0f) {
                    white.x = this.primitiveColor.x * f;
                    white.y = this.primitiveColor.y * f;
                    white.z = this.primitiveColor.z * f;
                    white.w = f * f3;
                    tempVect.set(csp0);
                    tempVect.mo23515p((Tuple3f) f10369d3);
                    addPoint(tempVect, tex3, white);
                    tempVect.set(csp0);
                    tempVect.mo23515p((Tuple3f) f10370d4);
                    addPoint(tempVect, tex4, white);
                    tempVect.set(csp0);
                    tempVect.mo23515p((Tuple3f) f10367d1);
                    addPoint(tempVect, tex1, white);
                    tempVect.set(csp0);
                    tempVect.mo23515p((Tuple3f) f10368d2);
                    addPoint(tempVect, tex2, white);
                }
            }
        }
    }

    private void addPoint(Vec3f vec3f, Vector2fWrap aka, Vector4fWrap ajf) {
        this.vertexes.setPosition(this.lastVertex, vec3f.x, vec3f.y, vec3f.z);
        this.vertexes.setColor(this.lastVertex, ajf.x, ajf.y, ajf.z, ajf.w);
        this.vertexes.setTexCoord(this.lastVertex, 0, aka.x, aka.y);
        this.indexes.setIndex(this.lastVertex, this.lastVertex);
        this.indexes.setSize(this.lastVertex);
        this.lastVertex++;
    }

    /* access modifiers changed from: package-private */
    public void processSpaceDustForReference(RenderContext renderContext) {
        Camera camera = renderContext.getCamera();
        TransformWrap transform = camera.getTransform();
        Vec3d ajr = renderContext.vec3dTemp0;
        ajr.mo9484aA(camera.getPosition());
        ajr.sub(this.lastCamPos);
        ajr.scale(speedModificator);
        double length = ajr.length();
        if (length > this.speedCap) {
            ajr.scale(this.speedCap / length);
        }
        this.dislocation.add(ajr);
        this.lastCamPos.mo9484aA(camera.getPosition());
        this.transform.setIdentity();
        this.transform.setTranslation(camera.getPosition());
        transform.mo17336a(ticamera);
        float f = this.regionSize;
        float f2 = f / 2.0f;
        this.random.mo22467eP(hashCode());
        white.set(this.primitiveColor);
        this.referenceObject.getGlobalTransform().multiply3x3(this.referenceObject.getVelocity(), f10366Z);
        if (f10366Z.length() == 0.0f) {
            this.referenceObject.getGlobalTransform().getZ(f10366Z);
            f10366Z.normalize();
        } else {
            f10366Z.normalize();
        }
        float length2 = this.referenceObject.getVelocity().length() / 100.0f;
        if (length2 > 0.001f) {
            renderContext.getCamera().getTransform().mo17336a(ticamera);
            int i = (int) (this.numBillboards + (this.numBillboards * length2 * 0.1f));
            if (i > 300) {
                i = 300;
            }
            for (int i2 = 0; i2 < i; i2++) {
                csp0.x = (float) (dpmodd((((double) this.random.mo22468j(0.0f, f)) - this.dislocation.x) + ((double) f2), (double) f) - ((double) f2));
                csp0.y = (float) (dpmodd((((double) this.random.mo22468j(0.0f, f)) - this.dislocation.y) + ((double) f2), (double) f) - ((double) f2));
                csp0.z = (float) (dpmodd((((double) this.random.mo22468j(0.0f, f)) - this.dislocation.z) + ((double) f2), (double) f) - ((double) f2));
                float f3 = -((csp0.x * ticamera.orientation.m20) + (csp0.y * ticamera.orientation.m21) + (csp0.z * ticamera.orientation.m22));
                if (f3 >= 50.0f) {
                    camPointDist.set(camera.getPosition());
                    camPointDist.sub(csp0);
                    f10365Y.set(camPointDist);
                    f10365Y.mo23483c(f10366Z);
                    f10365Y.normalize();
                    float cos = (float) ((1.0d - Math.cos(((double) (((Math.abs(f3) - 50.0f) / (f - 50.0f)) * 2.0f)) * 3.141592653589793d)) * 0.5d);
                    white.w = this.primitiveColor.w * cos;
                    white.x = this.primitiveColor.x * cos;
                    white.y = this.primitiveColor.y * cos;
                    white.z = cos * this.primitiveColor.z;
                    f10371v1.x = f10365Y.x * this.billboardSize.y;
                    f10371v1.y = f10365Y.y * this.billboardSize.y;
                    f10371v1.z = f10365Y.z * this.billboardSize.y;
                    f10372v2.x = f10366Z.x * this.billboardSize.x;
                    f10372v2.y = f10366Z.y * this.billboardSize.x;
                    f10372v2.z = f10366Z.z * this.billboardSize.x;
                    f10367d1.x = (f10372v2.x * (-length2)) - f10371v1.x;
                    f10367d1.y = (f10372v2.y * (-length2)) - f10371v1.y;
                    f10367d1.z = (f10372v2.z * (-length2)) - f10371v1.z;
                    f10368d2.x = (f10372v2.x * length2) - f10371v1.x;
                    f10368d2.y = (f10372v2.y * length2) - f10371v1.y;
                    f10368d2.z = (f10372v2.z * length2) - f10371v1.z;
                    if (f10367d1.isValid() && f10368d2.isValid()) {
                        tempVect.set(csp0);
                        tempVect.mo23515p((Tuple3f) f10367d1);
                        addPoint(tempVect, tex1, white);
                        tempVect.set(csp0);
                        tempVect.mo23515p((Tuple3f) f10368d2);
                        addPoint(tempVect, tex2, white);
                        tempVect.set(csp0);
                        tempVect.mo23513o(f10367d1);
                        addPoint(tempVect, tex3, white);
                        tempVect.set(csp0);
                        tempVect.mo23513o(f10368d2);
                        addPoint(tempVect, tex4, white);
                    }
                }
            }
        }
    }

    public RenderAsset cloneAsset() {
        return new RSpaceDust(this);
    }

    public Vector2fWrap getBillboardSize() {
        return this.billboardSize;
    }

    public void setBillboardSize(Vector2fWrap aka) {
        this.billboardSize.set(aka);
    }

    public float getFadeOut() {
        return this.fadeOut;
    }

    public void setFadeOut(float f) {
        this.fadeOut = f;
    }

    public float getFadeIn() {
        return this.fadeIn;
    }

    public void setFadeIn(float f) {
        this.fadeIn = f;
    }

    public float getRegionSize() {
        return this.regionSize;
    }

    public void setRegionSize(float f) {
        this.regionSize = f;
    }

    public void validate(SceneLoader sceneLoader) {
        super.validate(sceneLoader);
        this.material.setShader(sceneLoader.getDefaultSpaceDustShader());
    }
}
