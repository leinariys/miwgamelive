package taikodom.render.scene;

/* compiled from: a */
public abstract class SoundObject extends SceneObject {
    public SoundObject() {
    }

    public SoundObject(SoundObject soundObject) {
        super(soundObject);
    }

    public void releaseReferences() {
    }

    public boolean isNeedToStep() {
        return true;
    }

    public int getLoopCount() {
        return 1;
    }

    public void setLoopCount(int i) {
    }

    public float getGain() {
        return 1.0f;
    }

    public void setGain(float f) {
    }

    public void setPitch(float f) {
    }

    public boolean play() {
        return false;
    }

    public boolean stop() {
        return false;
    }

    public boolean isPlaying() {
        return false;
    }

    public boolean fadeOut(float f) {
        return false;
    }

    public boolean fadeIn(float f) {
        return false;
    }

    public boolean isValid() {
        return false;
    }

    public SoundSource getSource() {
        return null;
    }

    public void setControllerValue(float f) {
    }

    public boolean isAlive() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public float getAudibleRadius() {
        return 0.0f;
    }

    /* access modifiers changed from: package-private */
    public void setAudibleRadius(float f) {
    }

    public float getSamplePosition() {
        return 0.0f;
    }

    public float getTimeLenght() {
        return 0.0f;
    }
}
