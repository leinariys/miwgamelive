package taikodom.render.scene;

import com.hoplon.geometry.Color;
import game.geometry.Vec3d;
import game.geometry.aLH;
import taikodom.render.RenderContext;
import taikodom.render.enums.FogType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.textures.Texture;

/* compiled from: a */
public class RSceneAreaInfo extends SceneObject {
    public final aLH aabb = new aLH();
    public final Color ambientColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
    public final Color fogColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
    public Texture diffuseCubemap;
    public float fogEnd = 1000.0f;
    public float fogExp = 1.0f;
    public float fogStart = 1.0f;
    public FogType fogType = FogType.FOG_LINEAR;
    public Texture reflectiveCubemap;

    public RSceneAreaInfo() {
    }

    public RSceneAreaInfo(RSceneAreaInfo rSceneAreaInfo) {
        super(rSceneAreaInfo);
        this.fogType = rSceneAreaInfo.fogType;
        this.fogColor.set(rSceneAreaInfo.fogColor);
        this.fogStart = rSceneAreaInfo.fogStart;
        this.fogEnd = rSceneAreaInfo.fogEnd;
        this.fogExp = rSceneAreaInfo.fogExp;
        this.ambientColor.set(rSceneAreaInfo.ambientColor);
        this.aabb.mo9867g(rSceneAreaInfo.aabb);
        this.diffuseCubemap = (Texture) smartClone(rSceneAreaInfo.diffuseCubemap);
        this.reflectiveCubemap = (Texture) smartClone(rSceneAreaInfo.reflectiveCubemap);
    }

    public FogType getFogType() {
        return this.fogType;
    }

    public void setFogType(FogType fogType2) {
        this.fogType = fogType2;
    }

    public Vec3d getSize() {
        return this.aabb.din().mo9476Z(2.0d);
    }

    public void setSize(Vec3d ajr) {
        this.aabb.mo9839aD(ajr.mo9476Z(-0.5d).dfZ());
        this.aabb.mo9839aD(ajr.mo9476Z(0.5d).dfZ());
    }

    public Color getFogColor() {
        return this.fogColor;
    }

    public void setFogColor(Color color) {
        this.fogColor.set(color);
    }

    public float getFogStart() {
        return this.fogStart;
    }

    public void setFogStart(float f) {
        this.fogStart = f;
    }

    public float getFogEnd() {
        return this.fogEnd;
    }

    public void setFogEnd(float f) {
        this.fogEnd = f;
    }

    public float getFogExp() {
        return this.fogExp;
    }

    public void setFogExp(float f) {
        this.fogExp = f;
    }

    public Color getAmbientColor() {
        return this.ambientColor;
    }

    public void setAmbientColor(Color color) {
        this.ambientColor.set(color);
    }

    public aLH getAabb() {
        return this.aabb;
    }

    public Texture getDiffuseCubemap() {
        return this.diffuseCubemap;
    }

    public void setDiffuseCubemap(Texture texture) {
        this.diffuseCubemap = texture;
    }

    public Texture getReflectiveCubemap() {
        return this.reflectiveCubemap;
    }

    public void setReflectiveCubemap(Texture texture) {
        this.reflectiveCubemap = texture;
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.firstFrame) {
            return true;
        }
        renderContext.getCurrentScene().addSceneAreaInfo(this);
        this.firstFrame = false;
        return true;
    }

    public RenderAsset cloneAsset() {
        return new RSceneAreaInfo(this);
    }
}
