package taikodom.render.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import taikodom.render.RenderContext;
import taikodom.render.enums.LightType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.Light;

/* compiled from: a */
public class RLight extends SceneObject {
    public final Vec3f direction;
    public Light light;
    public Light renderLight;

    public RLight() {
        this.direction = new Vec3f(0.0f, 1.0f, 0.0f);
        this.light = new Light();
    }

    public RLight(RLight rLight) {
        super(rLight);
        this.direction = new Vec3f(0.0f, 1.0f, 0.0f);
        this.light = (Light) rLight.light.cloneAsset();
        this.direction.set(rLight.direction);
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render || !super.render(renderContext, z)) {
            return false;
        }
        this.light.setPosition(getGlobalTransform().position);
        this.globalTransform.orientation.transform(this.direction, this.light.getDirection());
        if (!z || this.light.getType() == LightType.DIRECTIONAL_LIGHT || (renderContext.getFrustum().mo1161a(this.light.getBoundingSphere()) && (renderContext.getSizeOnScreen(this.transform.position, (double) this.light.getRadius()) > 4.0f || !renderContext.isCullOutSmallObjects()))) {
            renderContext.addLight(this.light);
            if (isMainLight()) {
                renderContext.setMainLight(this.light);
            }
        }
        return true;
    }

    public RenderAsset cloneAsset() {
        return new RLight(this);
    }

    public LightType getType() {
        return this.light.getLightType();
    }

    public void setType(LightType lightType) {
        this.light.setType(lightType);
    }

    public Light getLight() {
        return this.light;
    }

    public void setLight(Light light2) {
        this.light = light2;
    }

    public Light getRenderLight() {
        return this.renderLight;
    }

    public void setRenderLight(Light light2) {
        this.renderLight = light2;
    }

    public Color getDiffuseColor() {
        return this.light.getDiffuseColor();
    }

    public void setDiffuseColor(Color color) {
        this.light.setDiffuseColor(color);
    }

    public Color getSpecularColor() {
        return this.light.getSpecularColor();
    }

    public void setSpecularColor(Color color) {
        this.light.setSpecularColor(color);
    }

    public Vec3f getDirection() {
        return this.direction;
    }

    public void setDirection(Vec3f vec3f) {
        this.direction.set(vec3f);
    }

    public float getExponent() {
        return this.light.getExponent();
    }

    public void setExponent(float f) {
        this.light.setExponent(f);
    }

    public float getCutoff() {
        return this.light.getCutoff();
    }

    public void setCutoff(float f) {
        this.light.setCutoff(f);
    }

    public boolean isMainLight() {
        return this.light.isMainLight();
    }

    public void setMainLight(boolean z) {
        this.light.setMainLight(z);
    }

    public float getRadius() {
        return this.light.getRadius();
    }

    public void setRadius(float f) {
        this.light.setRadius(f);
    }
}
