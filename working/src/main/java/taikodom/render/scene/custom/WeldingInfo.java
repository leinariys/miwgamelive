package taikodom.render.scene.custom;

import java.io.Serializable;

/* compiled from: a */
public class WeldingInfo implements Serializable {
    private static final long serialVersionUID = -4351523408114940701L;
    private int[] info;
    private int maxDuplicates;
    private int uniqueCount;
    private boolean[] weldTangents;

    public WeldingInfo(int i, int i2, int[] iArr, boolean[] zArr) {
        this.uniqueCount = i;
        this.maxDuplicates = i2;
        this.info = iArr;
        this.weldTangents = zArr;
    }

    public int[] getInfo() {
        return this.info;
    }

    public int getMaxDuplicates() {
        return this.maxDuplicates;
    }

    public int getUniqueCount() {
        return this.uniqueCount;
    }

    public boolean[] getWeldTangents() {
        return this.weldTangents;
    }
}
