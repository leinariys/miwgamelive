package taikodom.render.scene;

import com.hoplon.geometry.Vec3f;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import game.geometry.Vector2fWrap;
import org.mozilla.javascript.ScriptRuntime;
import taikodom.render.SoundBuffer;
import taikodom.render.StepContext;
import taikodom.render.enums.ShapeTypes;
import taikodom.render.loader.RenderAsset;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.Material;

import java.util.*;

/* compiled from: a */
public class SSoundGroup extends SoundObject {
    private static final TransformWrap mirrorXTransform = new TransformWrap(-1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
    public final List<SoundBuffer> buffers = new ArrayList();
    public final Map<SSourceElement, SSoundSource> sources = new HashMap();
    final Vec3f position = new Vec3f();
    final Vec3f velocity = new Vec3f();
    private RShape debugShape;
    private float gain;
    private int loopCount;
    private boolean mirrorX = false;

    public SSoundGroup(SSoundGroup sSoundGroup) {
        this.buffers.addAll(sSoundGroup.buffers);
        for (SSourceElement sSourceElement : sSoundGroup.sources.keySet()) {
            this.sources.put(new SSourceElement(sSourceElement), new SSoundSource());
        }
        for (SSoundSource addChild : this.sources.values()) {
            addChild(addChild);
        }
        setGain(sSoundGroup.getGain());
        setLoopCount(sSoundGroup.getLoopCount());
        if (debug()) {
            if (sSoundGroup.debugShape == null) {
                sSoundGroup.debugShape = new RShape();
                sSoundGroup.debugShape.setMaterial(new Material());
                sSoundGroup.debugShape.getMaterial().setShader(new Shader());
                sSoundGroup.debugShape.getMaterial().getShader().addPass(new ShaderPass());
                sSoundGroup.debugShape.setRender(true);
            }
            this.debugShape = new RShape(sSoundGroup.debugShape);
            this.debugShape.setShapeType(ShapeTypes.SHAPE_SPHERE);
            this.debugShape.setRender(true);
        }
        this.disposeWhenEmpty = true;
    }

    public SSoundGroup() {
        if (debug()) {
            this.debugShape = new RShape();
            this.debugShape.setMaterial(new Material());
            this.debugShape.getMaterial().setShader(new Shader());
            this.debugShape.getMaterial().getShader().addPass(new ShaderPass());
            this.debugShape.setRender(true);
        }
        this.gain = 1.0f;
        this.loopCount = 1;
        this.disposeWhenEmpty = true;
    }

    private boolean debug() {
        return false;
    }

    public void removeBuffer(SoundBuffer soundBuffer) {
        this.buffers.remove(soundBuffer);
    }

    public void addBuffer(SoundBuffer soundBuffer) {
        if (soundBuffer == null) {
            System.out.println("Trying to add a NULL buffer in object " + this.name);
        }
        this.buffers.add(soundBuffer);
        this.firstFrame = true;
    }

    public SoundBuffer getBuffer(int i) {
        return this.buffers.get(i);
    }

    public int bufferCount() {
        return this.buffers.size();
    }

    public void setBuffer(int i, SoundBuffer soundBuffer) {
        addBuffer(soundBuffer);
    }

    public void setSource(int i, SSourceElement sSourceElement) {
        addSource(sSourceElement);
    }

    public SSourceElement getSource(int i) {
        int i2 = 0;
        Iterator<SSourceElement> it = this.sources.keySet().iterator();
        while (true) {
            int i3 = i2;
            if (!it.hasNext()) {
                return null;
            }
            SSourceElement next = it.next();
            if (i3 == i) {
                return next;
            }
            i2 = i3 + 1;
        }
    }

    public void removeSource(SSourceElement sSourceElement) {
        this.sources.remove(sSourceElement);
    }

    public void addSource(SSourceElement sSourceElement) {
        SSoundSource sSoundSource = new SSoundSource();
        this.sources.put(sSourceElement, sSoundSource);
        addChild(sSoundSource);
    }

    public void releaseReferences() {
        super.releaseReferences();
        for (SSoundSource releaseReferences : this.sources.values()) {
            releaseReferences.releaseReferences();
        }
        for (SoundBuffer releaseReferences2 : this.buffers) {
            releaseReferences2.releaseReferences();
        }
    }

    public int getLoopCount() {
        return this.loopCount;
    }

    public void setLoopCount(int i) {
        this.loopCount = i;
        for (SSoundSource loopCount2 : this.sources.values()) {
            loopCount2.setLoopCount(i);
        }
    }

    public float getGain() {
        return this.gain;
    }

    public void setGain(float f) {
        this.gain = f;
        for (SSoundSource gain2 : this.sources.values()) {
            gain2.setGain(f);
        }
    }

    public RenderAsset cloneAsset() {
        return new SSoundGroup(this);
    }

    public boolean isNeedToStep() {
        return true;
    }

    public int step(StepContext stepContext) {
        if (this.buffers.size() <= 0) {
            return 0;
        }
        if (this.firstFrame) {
            play();
            this.firstFrame = false;
        }
        int i = 0;
        for (SSoundSource next : this.sources.values()) {
            int step = next.step(stepContext);
            if (!next.isAlive() || step == 1) {
                next.releaseReferences();
                next.dispose();
                i++;
            }
        }
        if (i < this.sources.size()) {
            return 0;
        }
        for (SceneObject next2 : getChildren()) {
            next2.setRender(false);
            next2.releaseReferences();
        }
        dispose();
        return 1;
    }

    public boolean play() {
        if (debug()) {
            addChild(this.debugShape);
        }
        ArrayList arrayList = new ArrayList(this.buffers);
        boolean z = false;
        for (Map.Entry next : this.sources.entrySet()) {
            SSoundSource sSoundSource = (SSoundSource) next.getValue();
            SSourceElement sSourceElement = (SSourceElement) next.getKey();
            if (sSoundSource != null && !sSoundSource.isPlaying() && !sSoundSource.isDisposed()) {
                SoundBuffer soundBuffer = null;
                if (arrayList.size() > 0) {
                    soundBuffer = (SoundBuffer) arrayList.get((int) Math.floor(Math.random() * ((double) arrayList.size())));
                    arrayList.remove(soundBuffer);
                } else if (this.buffers.size() > 0) {
                    soundBuffer = this.buffers.get((int) Math.floor(Math.random() * ((double) this.buffers.size())));
                }
                if (soundBuffer != null) {
                    sSoundSource.setBuffer(soundBuffer);
                    sSoundSource.setIs3d(true);
                    if (this.mirrorX) {
                        Vec3d ajr = new Vec3d(sSourceElement.getPosition());
                        mirrorXTransform.mo17331a(ajr);
                        sSoundSource.transform.position.mo9484aA(ajr);
                    } else {
                        sSoundSource.transform.position.mo9484aA(sSourceElement.getPosition());
                    }
                    if (debug()) {
                        RShape rShape = new RShape(this.debugShape);
                        if (this.mirrorX) {
                            Vec3d ajr2 = new Vec3d(sSourceElement.getPosition());
                            mirrorXTransform.mo17331a(ajr2);
                            rShape.transform.position.mo9484aA(ajr2);
                        } else {
                            rShape.transform.position.mo9484aA(sSourceElement.getPosition());
                        }
                        rShape.setScaling(2.0f);
                        rShape.setRender(true);
                        rShape.setPrimitiveColor(1.0f, 0.0f, 0.0f, 0.0f);
                        addChild(rShape);
                    }
                    sSoundSource.setLoopCount(this.loopCount);
                    sSoundSource.setGain(this.gain);
                    sSoundSource.setDistances(sSourceElement.getDistances());
                    z = true;
                }
            }
        }
        return z;
    }

    public void reset() {
        play();
    }

    public boolean stop() {
        boolean z = true;
        Iterator<SSoundSource> it = this.sources.values().iterator();
        while (true) {
            boolean z2 = z;
            if (!it.hasNext()) {
                return z2;
            }
            z = it.next().stop() & z2;
        }
    }

    public void onRemove() {
        super.onRemove();
        for (SSoundSource releaseReferences : this.sources.values()) {
            releaseReferences.releaseReferences();
        }
    }

    public void dispose() {
        for (SSoundSource next : this.sources.values()) {
            next.releaseReferences();
            next.dispose();
        }
        this.sources.clear();
        this.buffers.clear();
        this.disposed = true;
    }

    public boolean isAlive() {
        return isPlaying();
    }

    public boolean isPlaying() {
        for (SSoundSource next : this.sources.values()) {
            if (next != null && next.isPlaying()) {
                return true;
            }
        }
        return false;
    }

    public boolean isValid() {
        for (SSoundSource next : this.sources.values()) {
            if (next != null && next.isValid()) {
                return true;
            }
        }
        return false;
    }

    public int getTotalMilesSounds() {
        int i = 0;
        Iterator<SSoundSource> it = this.sources.values().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = it.next().getTotalMilesSounds() + i2;
        }
    }

    public void setSquaredSoundSource(SceneObject sceneObject) {
        throw new RuntimeException("Must implement this");
    }

    public boolean fadeOut(float f) {
        boolean z = false;
        Iterator<SSoundSource> it = this.sources.values().iterator();
        while (true) {
            boolean z2 = z;
            if (!it.hasNext()) {
                return z2;
            }
            z = it.next().fadeOut(f) | z2;
        }
    }

    public void setMirrorX(boolean z) {
        this.mirrorX = z;
    }

    public void updateInternalGeometry(SceneObject sceneObject) {
        super.updateInternalGeometry(sceneObject);
        for (SSoundSource updateInternalGeometry : this.sources.values()) {
            updateInternalGeometry.updateInternalGeometry(this);
        }
    }

    public void setDistances(Vector2fWrap aka) {
        for (SSourceElement distances : this.sources.keySet()) {
            distances.setDistances(aka);
        }
    }

    public void setDistances(Vector2fWrap aka, SSoundSource sSoundSource) {
        for (SSoundSource next : this.sources.values()) {
            if (next.getName() == sSoundSource.getName()) {
                next.setDistances(aka);
                return;
            }
        }
    }
}
