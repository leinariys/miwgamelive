package taikodom.render.scene;

/* compiled from: a */
public class Socket extends SceneObject {
    private SocketHub hub;
    private String hubName = "";
    private RModel rmodel;

    public Socket(Socket socket) {
        super(socket);
        this.rmodel = socket.rmodel;
        this.hub = socket.hub;
        this.hubName = socket.hubName;
    }

    public Socket() {
    }

    public Socket(String str) {
        this.hubName = str;
    }

    public Socket(RModel rModel, String str) {
        this.hubName = str;
        rModel.addSocket(this);
    }

    public SocketHub getHub() {
        return this.hub;
    }

    private void setHub(SocketHub socketHub) {
        if (this.hub != socketHub) {
            if (this.hub != null) {
                this.hub.removeSocket(this);
            }
            this.hub = socketHub;
            if (this.hub != null) {
                this.hub.addChild(this);
            }
        }
    }

    public String getHubName() {
        return this.hubName;
    }

    public void setHubName(String str) {
        if (!str.isEmpty()) {
            this.hubName = str;
            if (this.rmodel != null) {
                setHub(this.rmodel.getHub(str));
            }
        }
    }

    public void setRModel(RModel rModel) {
        if (this.rmodel != rModel) {
            if (this.rmodel != null) {
                this.rmodel.removeSocket(this);
            }
            this.rmodel = rModel;
            if (this.rmodel != null) {
                setHubName(this.hubName);
            } else {
                setHub((SocketHub) null);
            }
        }
    }

    public RModel gerRModel() {
        return this.rmodel;
    }

    public void unlink() {
        this.rmodel = null;
        setHub((SocketHub) null);
    }
}
