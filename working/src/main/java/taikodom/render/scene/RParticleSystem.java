package taikodom.render.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import game.geometry.aLH;
import org.mozilla.javascript.ScriptRuntime;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;
import taikodom.render.camera.Camera;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.enums.BillboardAlignment;
import taikodom.render.enums.GeometryType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Mesh;
import taikodom.render.textures.BaseTexture;
import util.Maths;

import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: a */
public class RParticleSystem extends RenderObject {
    private static final int MAX_ALIVE_PARTICLES = 5000;
    private static /* synthetic */ int[] $SWITCH_TABLE$taikodom$render$enums$BillboardAlignment = null;
    private static int globalAliveParticles = 0;
    public final Vec3f alignmentDirection;
    public final Vec3d lastPosition;
    public final Vec3d particlesAcceleration;
    public final Color particlesColorVariation;
    public final Color particlesFinalColor;
    public final Color particlesInitialColor;
    public final Vec3d particlesVelocity;
    public final Vec3d particlesVelocityVariation;
    public final Vec3d transformedParticlesVelocity;
    private final TransformWrap cameraSpaceTransform;
    public boolean active;
    public boolean alignToHorizon;
    public boolean alignToMovement;
    public BillboardAlignment alignment;
    public int currentAliveParticles;
    public float currentDelay;
    public float currentLifeTime;
    public float currentParticlesToSpawn;
    public boolean followEmitter;
    public int initialBurst;
    public float initialDelay;
    public float initialMaxAngle;
    public float initialMinAngle;
    public boolean isDead;
    public int maxParticles;
    public float newPartsSecond;
    public float particlesFinalSize;
    public float particlesFinalSizeVariation;
    public float particlesInitialSize;
    public float particlesInitialSizeVariation;
    public float particlesLifeTimeVariation;
    public int particlesLimit;
    public float particlesMaxLifeTime;
    public boolean scaleable;
    public float systemLifeTime;
    private ExpandableIndexData indexes;
    private float maxRotationSpeed;
    private double maxSpawnDistance;
    private Mesh mesh;
    private float minRotationSpeed;
    private ArrayList<C5153a> particlesPool;
    private double spawnLimitEnd;
    private double spawnLimitStart;
    private ExpandableVertexData vertexes;

    public RParticleSystem() {
        this.particlesPool = new ArrayList<>();
        this.alignmentDirection = new Vec3f(0.0f, 1.0f, 0.0f);
        this.lastPosition = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
        this.active = true;
        this.transformedParticlesVelocity = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
        this.particlesVelocity = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
        this.particlesAcceleration = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
        this.particlesVelocityVariation = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
        this.particlesInitialColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.particlesFinalColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        this.particlesColorVariation = new Color(0.1f, 0.1f, 0.1f, 0.1f);
        this.cameraSpaceTransform = new TransformWrap();
        this.spawnLimitStart = Double.MAX_VALUE;
        this.spawnLimitEnd = Double.MAX_VALUE;
        this.maxSpawnDistance = 5500000.0d;
        this.alignment = BillboardAlignment.VIEW_ALIGNED;
        this.alignmentDirection.z = 1.0f;
        this.newPartsSecond = 60.0f;
        this.currentParticlesToSpawn = 0.0f;
        this.particlesMaxLifeTime = 1.0f;
        this.particlesInitialSize = 1.0f;
        this.particlesFinalSize = 1.0f;
        this.particlesInitialColor.set(1.0f, 1.0f, 1.0f, 1.0f);
        this.particlesLifeTimeVariation = 0.0f;
        this.particlesInitialSizeVariation = 0.0f;
        this.particlesFinalSizeVariation = 0.0f;
        this.minRotationSpeed = 0.0f;
        this.maxRotationSpeed = 0.0f;
        this.initialBurst = 0;
        this.currentLifeTime = 0.0f;
        this.systemLifeTime = -1.0f;
        this.isDead = false;
        this.initialMaxAngle = 0.0f;
        this.initialMinAngle = 0.0f;
        this.followEmitter = false;
        this.currentAliveParticles = 0;
        this.initialDelay = 0.0f;
        this.currentDelay = 0.0f;
        this.scaleable = true;
        this.alignToMovement = false;
        this.alignToHorizon = false;
        initRenderStructs();
        setMaxParticles(100);
    }

    public RParticleSystem(RParticleSystem rParticleSystem) {
        super(rParticleSystem);
        this.particlesPool = new ArrayList<>();
        this.alignmentDirection = new Vec3f(0.0f, 1.0f, 0.0f);
        this.lastPosition = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
        this.active = true;
        this.transformedParticlesVelocity = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
        this.particlesVelocity = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
        this.particlesAcceleration = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
        this.particlesVelocityVariation = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
        this.particlesInitialColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.particlesFinalColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        this.particlesColorVariation = new Color(0.1f, 0.1f, 0.1f, 0.1f);
        this.cameraSpaceTransform = new TransformWrap();
        this.spawnLimitStart = Double.MAX_VALUE;
        this.spawnLimitEnd = Double.MAX_VALUE;
        this.maxSpawnDistance = 5500000.0d;
        this.followEmitter = rParticleSystem.followEmitter;
        this.alignment = rParticleSystem.alignment;
        this.alignmentDirection.set(rParticleSystem.alignmentDirection);
        this.newPartsSecond = rParticleSystem.newPartsSecond;
        this.currentParticlesToSpawn = 0.0f;
        this.particlesMaxLifeTime = rParticleSystem.particlesMaxLifeTime;
        this.particlesInitialSize = rParticleSystem.particlesInitialSize;
        this.particlesFinalSize = rParticleSystem.particlesFinalSize;
        this.particlesFinalColor.set(rParticleSystem.particlesFinalColor);
        this.particlesColorVariation.set(rParticleSystem.particlesColorVariation);
        this.particlesInitialColor.set(rParticleSystem.particlesInitialColor);
        this.particlesLifeTimeVariation = rParticleSystem.particlesLifeTimeVariation;
        this.particlesInitialSizeVariation = rParticleSystem.particlesInitialSizeVariation;
        this.particlesFinalSizeVariation = rParticleSystem.particlesFinalSizeVariation;
        this.particlesVelocity.mo9484aA(rParticleSystem.particlesVelocity);
        this.particlesAcceleration.mo9484aA(rParticleSystem.particlesAcceleration);
        this.particlesVelocityVariation.mo9484aA(rParticleSystem.particlesVelocityVariation);
        setInitialBurst(rParticleSystem.initialBurst);
        this.currentLifeTime = 0.0f;
        this.systemLifeTime = rParticleSystem.systemLifeTime;
        this.isDead = false;
        this.alignToMovement = rParticleSystem.alignToMovement;
        this.alignToHorizon = rParticleSystem.alignToHorizon;
        this.initialMaxAngle = rParticleSystem.initialMaxAngle;
        this.initialMinAngle = rParticleSystem.initialMinAngle;
        this.initialDelay = rParticleSystem.initialDelay;
        this.currentDelay = 0.0f;
        this.currentAliveParticles = 0;
        this.lastPosition.mo9484aA(getPosition());
        this.localAabb.mo9866g(-this.particlesFinalSize, -this.particlesFinalSize, -this.particlesFinalSize, this.particlesFinalSize, this.particlesFinalSize, this.particlesFinalSize);
        this.scaleable = rParticleSystem.scaleable;
        this.minRotationSpeed = rParticleSystem.minRotationSpeed;
        this.maxRotationSpeed = rParticleSystem.maxRotationSpeed;
        initRenderStructs();
        setMaxParticles(rParticleSystem.getMaxParticles());
    }

    static /* synthetic */ int[] $SWITCH_TABLE$taikodom$render$enums$BillboardAlignment() {
        int[] iArr = $SWITCH_TABLE$taikodom$render$enums$BillboardAlignment;
        if (iArr == null) {
            iArr = new int[BillboardAlignment.values().length];
            try {
                iArr[BillboardAlignment.AXIS_ALIGNED.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[BillboardAlignment.SCREEN_ALIGNED.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[BillboardAlignment.USER_ALIGNED.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[BillboardAlignment.VIEW_ALIGNED.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$taikodom$render$enums$BillboardAlignment = iArr;
        }
        return iArr;
    }

    public static int getGlobalAliveParticles() {
        return globalAliveParticles;
    }

    public aLH computeWorldSpaceAABB() {
        addChildrenWorldSpaceAABB();
        this.aabbWSLenght = this.aabbWorldSpace.din().mo9491ax(this.aabbWorldSpace.dim());
        return this.aabbWorldSpace;
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render || !super.render(renderContext, z)) {
            return false;
        }
        this.cameraSpaceTransform.setIdentity();
        this.cameraSpaceTransform.setTranslation(renderContext.getCamera().getPosition());
        if (!z || renderContext.getFrustum().mo1160a(this.aabbWorldSpace)) {
            generateMesh(renderContext);
            if (this.mesh.getIndexes().size() > 3) {
                float sizeOnScreen = renderContext.getSizeOnScreen(this.globalTransform.position, this.aabbWSLenght);
                for (int i = 0; i < this.material.textureCount(); i++) {
                    BaseTexture texture = this.material.getTexture(i);
                    if (texture != null) {
                        texture.addArea(sizeOnScreen);
                    }
                }
                renderContext.addPrimitive(this.material, this.mesh, this.cameraSpaceTransform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
                renderContext.incParticleSystemsRendered();
            }
        }
        return true;
    }

    public RenderAsset cloneAsset() {
        return new RParticleSystem(this);
    }

    public float getNewPartsSecond() {
        return this.newPartsSecond;
    }

    public void setNewPartsSecond(float f) {
        this.newPartsSecond = f;
    }

    public int getMaxParticles() {
        return this.maxParticles;
    }

    public void setMaxParticles(int i) {
        if (i == 0) {
            i = 2;
        }
        this.maxParticles = i;
        this.particlesPool.clear();
        this.particlesPool.ensureCapacity(i);
        for (int i2 = 0; i2 < i; i2++) {
            this.particlesPool.add(new C5153a());
        }
        this.indexes.ensure(i * 4);
        this.vertexes.ensure(i * 4);
        this.indexes.clear();
        int i3 = 0;
        for (int i4 = 0; i4 < i; i4++) {
            this.indexes.setIndex(i3, i3);
            int i5 = i3 + 1;
            this.vertexes.setTexCoord(i3, 0, 0.0f, 0.0f);
            this.indexes.setIndex(i5, i5);
            int i6 = i5 + 1;
            this.vertexes.setTexCoord(i5, 0, 1.0f, 0.0f);
            this.indexes.setIndex(i6, i6);
            int i7 = i6 + 1;
            this.vertexes.setTexCoord(i6, 0, 1.0f, 1.0f);
            this.indexes.setIndex(i7, i7);
            i3 = i7 + 1;
            this.vertexes.setTexCoord(i7, 0, 0.0f, 1.0f);
        }
    }

    public float getParticlesMaxLifeTime() {
        return this.particlesMaxLifeTime;
    }

    public void setParticlesMaxLifeTime(float f) {
        this.particlesMaxLifeTime = f;
    }

    public float getParticlesLifeTimeVariation() {
        return this.particlesLifeTimeVariation;
    }

    public void setParticlesLifeTimeVariation(float f) {
        this.particlesLifeTimeVariation = f;
    }

    public float getParticlesInitialSize() {
        return this.particlesInitialSize;
    }

    public void setParticlesInitialSize(float f) {
        this.particlesInitialSize = f;
    }

    public float getParticlesFinalSize() {
        return this.particlesFinalSize;
    }

    public void setParticlesFinalSize(float f) {
        this.particlesFinalSize = f;
    }

    public float getParticlesInitialSizeVariation() {
        return this.particlesInitialSizeVariation;
    }

    public void setParticlesInitialSizeVariation(float f) {
        this.particlesInitialSizeVariation = f;
    }

    public float getParticlesFinalSizeVariation() {
        return this.particlesFinalSizeVariation;
    }

    public void setParticlesFinalSizeVariation(float f) {
        this.particlesFinalSizeVariation = f;
    }

    public boolean isFollowEmitter() {
        return this.followEmitter;
    }

    public void setFollowEmitter(boolean z) {
        this.followEmitter = z;
    }

    public Vec3d getParticlesVelocity() {
        return this.particlesVelocity;
    }

    public void setParticlesVelocity(Vec3d ajr) {
        this.particlesVelocity.mo9484aA(ajr);
    }

    public Color getParticlesInitialColor() {
        return this.particlesInitialColor;
    }

    public void setParticlesInitialColor(Color color) {
        this.particlesInitialColor.set(color);
    }

    public Color getParticlesFinalColor() {
        return this.particlesFinalColor;
    }

    public void setParticlesFinalColor(Color color) {
        this.particlesFinalColor.set(color);
    }

    public float getInitialMaxAngle() {
        return this.initialMaxAngle;
    }

    public void setInitialMaxAngle(float f) {
        this.initialMaxAngle = f;
    }

    public Vec3d getParticlesVelocityVariation() {
        return this.particlesVelocityVariation;
    }

    public void setParticlesVelocityVariation(Vec3d ajr) {
        this.particlesVelocityVariation.mo9484aA(ajr);
    }

    private void initRenderStructs() {
        this.vertexes = new ExpandableVertexData(new VertexLayout(true, true, false, false, 1), 100);
        this.indexes = new ExpandableIndexData(100);
        this.mesh = new Mesh();
        this.mesh.setName("RParticleSystem_mesh");
        this.mesh.setGeometryType(GeometryType.QUADS);
        this.mesh.setVertexData(this.vertexes);
        this.mesh.setIndexes(this.indexes);
    }

    public float getMinRotationSpeed() {
        return this.minRotationSpeed;
    }

    public void setMinRotationSpeed(float f) {
        this.minRotationSpeed = f;
    }

    public float getMaxRotationSpeed() {
        return this.maxRotationSpeed;
    }

    public void setMaxRotationSpeed(float f) {
        this.maxRotationSpeed = f;
    }

    public BillboardAlignment getAlignment() {
        return this.alignment;
    }

    public void setAlignment(BillboardAlignment billboardAlignment) {
        this.alignment = billboardAlignment;
    }

    public Vec3f getAlignmentDirection() {
        return this.alignmentDirection;
    }

    public void setAlignmentDirection(Vec3f vec3f) {
        this.alignmentDirection.set(vec3f);
    }

    public int getInitialBurst() {
        return this.initialBurst;
    }

    public void setInitialBurst(int i) {
        this.initialBurst = i;
        this.currentParticlesToSpawn += (float) i;
    }

    public float getSystemLifeTime() {
        return this.systemLifeTime;
    }

    public void setSystemLifeTime(float f) {
        this.systemLifeTime = f;
    }

    public float getInitialDelay() {
        return this.initialDelay;
    }

    public void setInitialDelay(float f) {
        this.initialDelay = f;
    }

    public boolean isAlignToMovement() {
        return this.alignToMovement;
    }

    public void setAlignToMovement(boolean z) {
        this.alignToMovement = z;
    }

    public boolean isAlignToHorizon() {
        return this.alignToHorizon;
    }

    public void setAlignToHorizon(boolean z) {
        this.alignToHorizon = z;
    }

    public float getInitialMinAngle() {
        return this.initialMinAngle;
    }

    public void setInitialMinAngle(float f) {
        this.initialMinAngle = f;
    }

    private void updateParticlesLifeTime(StepContext stepContext) {
        float f;
        this.indexes.clear();
        this.mesh.getAabb().reset();
        float deltaTime = stepContext.getDeltaTime();
        this.currentAliveParticles = 0;
        this.currentDelay += deltaTime;
        if (this.currentDelay >= this.initialDelay) {
            this.currentParticlesToSpawn += this.newPartsSecond * deltaTime;
            int jv = Maths.m22672jv(this.currentParticlesToSpawn);
            this.currentParticlesToSpawn -= (float) jv;
            this.particlesLimit = this.maxParticles;
            if (jv > this.particlesLimit) {
                jv = this.particlesLimit;
            }
            if (this.currentSquaredDistanceToCamera > this.spawnLimitStart) {
                double d = 1.0d - ((this.currentSquaredDistanceToCamera - this.spawnLimitStart) / (this.spawnLimitEnd * 0.8d));
                if (d < ScriptRuntime.NaN) {
                    this.particlesLimit = 3;
                } else {
                    this.particlesLimit = (int) (d * ((double) this.particlesLimit));
                }
            }
            if (this.particlesLimit < 0 || this.currentSquaredDistanceToCamera > this.spawnLimitEnd) {
                this.particlesLimit = 0;
            }
            if (this.systemLifeTime > 0.0f) {
                this.currentLifeTime += deltaTime;
                if (this.currentLifeTime > this.systemLifeTime) {
                    this.currentParticlesToSpawn = 0.0f;
                    jv = 0;
                    this.isDead = true;
                }
            }
            float f2 = 1.0f / ((float) jv);
            int i = this.particlesLimit;
            Iterator<C5153a> it = this.particlesPool.iterator();
            int i2 = jv;
            while (it.hasNext()) {
                C5153a next = it.next();
                i--;
                if (i <= 0) {
                    break;
                } else if (next.imi) {
                    float f3 = next.lifeTime - deltaTime;
                    next.lifeTime = f3;
                    if (f3 <= 0.0f) {
                        next.imi = false;
                        globalAliveParticles--;
                    } else {
                        next.position.x += next.imd.x * ((double) deltaTime);
                        next.position.y += next.imd.y * ((double) deltaTime);
                        next.position.z += next.imd.z * ((double) deltaTime);
                        next.imd.x += next.ime.x * ((double) deltaTime);
                        next.imd.y += next.ime.y * ((double) deltaTime);
                        next.imd.z += next.ime.z * ((double) deltaTime);
                        next.size += next.imh * deltaTime;
                        next.color.x += next.imf.x * deltaTime;
                        next.color.y += next.imf.y * deltaTime;
                        next.color.z += next.imf.z * deltaTime;
                        next.color.w += next.imf.w * deltaTime;
                        next.deX += next.img * deltaTime;
                        if (next.color.x < 0.0f) {
                            next.color.x = 0.0f;
                        }
                        if (next.color.y < 0.0f) {
                            next.color.y = 0.0f;
                        }
                        if (next.color.z < 0.0f) {
                            next.color.z = 0.0f;
                        }
                        if (next.color.w < 0.0f) {
                            next.color.w = 0.0f;
                        }
                        this.currentAliveParticles++;
                        this.mesh.getAabb().mo9842aG(next.position);
                    }
                } else if (i2 > 0 && this.active && globalAliveParticles < 5000 && this.currentSquaredDistanceToCamera < this.maxSpawnDistance) {
                    spawnParticle(next, ((float) i2) * f2);
                    globalAliveParticles++;
                    i2--;
                }
            }
            if (this.isDead && this.currentAliveParticles == 0 && !stepContext.getEditMode()) {
                dispose();
            } else if (this.currentAliveParticles > 0) {
                if (this.particlesFinalSize > this.particlesInitialSize) {
                    f = this.particlesFinalSize;
                } else {
                    f = this.particlesInitialSize;
                }
                this.mesh.getAabb().mo9826C(this.mesh.getAabb().din().x + ((double) f), this.mesh.getAabb().din().y + ((double) f), this.mesh.getAabb().din().z + ((double) f));
                this.mesh.getAabb().mo9826C(this.mesh.getAabb().dim().x - ((double) f), this.mesh.getAabb().dim().y - ((double) f), this.mesh.getAabb().dim().z - ((double) f));
            }
        }
    }

    private void spawnParticle(C5153a aVar, float f) {
        aVar.imi = true;
        aVar.lifeTime = this.particlesMaxLifeTime + (Maths.random2Min1() * this.particlesLifeTimeVariation);
        aVar.imd.x = this.transformedParticlesVelocity.x + (this.particlesVelocityVariation.x * ((double) Maths.random2Min1()));
        aVar.imd.y = this.transformedParticlesVelocity.y + (this.particlesVelocityVariation.y * ((double) Maths.random2Min1()));
        aVar.imd.z = this.transformedParticlesVelocity.z + (this.particlesVelocityVariation.z * ((double) Maths.random2Min1()));
        aVar.ime.mo9484aA(this.particlesAcceleration);
        aVar.color.x = this.particlesInitialColor.x * ((Maths.random2Min1() * this.particlesColorVariation.x) + 1.0f);
        aVar.color.y = this.particlesInitialColor.y * ((Maths.random2Min1() * this.particlesColorVariation.y) + 1.0f);
        aVar.color.z = this.particlesInitialColor.z * ((Maths.random2Min1() * this.particlesColorVariation.z) + 1.0f);
        aVar.color.w = this.particlesInitialColor.w * ((Maths.random2Min1() * this.particlesColorVariation.w) + 1.0f);
        aVar.imf.x = (this.particlesFinalColor.x - aVar.color.x) / aVar.lifeTime;
        aVar.imf.y = (this.particlesFinalColor.y - aVar.color.y) / aVar.lifeTime;
        aVar.imf.z = (this.particlesFinalColor.z - aVar.color.z) / aVar.lifeTime;
        aVar.imf.w = (this.particlesFinalColor.w - aVar.color.w) / aVar.lifeTime;
        aVar.size = this.particlesInitialSize + (Maths.random2Min1() * this.particlesInitialSizeVariation);
        aVar.imh = ((this.particlesFinalSize + (Maths.random2Min1() * this.particlesFinalSizeVariation)) - aVar.size) / aVar.lifeTime;
        aVar.deX = this.initialMinAngle + ((this.initialMaxAngle - this.initialMinAngle) * Maths.random());
        aVar.img = this.minRotationSpeed + ((this.maxRotationSpeed - this.minRotationSpeed) * Maths.random());
        if (this.followEmitter) {
            float f2 = aVar.lifeTime * (1.0f - f);
            aVar.lifeTime *= f;
            aVar.position.x = aVar.imd.x * ((double) f2);
            aVar.position.y = aVar.imd.y * ((double) f2);
            aVar.position.z = aVar.imd.z * ((double) f2);
            aVar.color.x += aVar.imf.x * f2;
            aVar.color.y += aVar.imf.y * f2;
            aVar.color.z += aVar.imf.z * f2;
            aVar.color.w += aVar.imf.w * f2;
            aVar.size = (f2 * aVar.imh) + aVar.size;
            return;
        }
        aVar.position.x = ((this.globalTransform.position.x - this.lastPosition.x) * ((double) f)) + this.lastPosition.x;
        aVar.position.y = ((this.globalTransform.position.y - this.lastPosition.y) * ((double) f)) + this.lastPosition.y;
        aVar.position.z = ((this.globalTransform.position.z - this.lastPosition.z) * ((double) f)) + this.lastPosition.z;
    }

    public int step(StepContext stepContext) {
        if (this.firstFrame) {
            this.firstFrame = false;
            this.lastPosition.mo9484aA(this.globalTransform.position);
        }
        if (stepContext.getCamera() != null) {
            this.currentSquaredDistanceToCamera = this.globalTransform.mo17352g(stepContext.getCamera().globalTransform);
        }
        updateParticlesLifeTime(stepContext);
        this.globalTransform.multiply3x3(this.particlesVelocity, this.transformedParticlesVelocity);
        if (this.currentAliveParticles > 0) {
            if (this.followEmitter) {
                this.mesh.getAabb().mo9834a(this.globalTransform, this.aabbWorldSpace);
                this.aabbWorldSpace.mo9867g(this.mesh.getAabb());
                this.aabbWorldSpace.din().add(this.globalTransform.position);
                this.aabbWorldSpace.dim().add(this.globalTransform.position);
            } else {
                this.aabbWorldSpace.mo9867g(this.mesh.getAabb());
            }
        }
        this.lastPosition.mo9484aA(this.globalTransform.position);
        stepContext.incParticleSystemStepped();
        if (this.disposed) {
            return 1;
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public void generateMeshDefaultAligned(Vec3f vec3f, Vec3f vec3f2, Vec3d ajr) {
        this.mesh.getAabb().reset();
        double d = ScriptRuntime.NaN;
        double d2 = ScriptRuntime.NaN;
        double d3 = ScriptRuntime.NaN;
        if (this.followEmitter) {
            d = this.globalTransform.position.x;
            d2 = this.globalTransform.position.y;
            d3 = this.globalTransform.position.z;
        }
        double d4 = d - ajr.x;
        double d5 = d2 - ajr.y;
        double d6 = d3 - ajr.z;
        int i = 0;
        int i2 = 0;
        int i3 = this.particlesLimit;
        this.indexes.clear();
        Iterator<C5153a> it = this.particlesPool.iterator();
        int i4 = i3;
        while (it.hasNext()) {
            C5153a next = it.next();
            if (next.imi) {
                int i5 = i4 - 1;
                if (i5 <= 0) {
                    break;
                }
                int i6 = i + 1;
                double d7 = next.position.x + d4;
                double d8 = next.position.y + d5;
                double d9 = next.position.z + d6;
                this.mesh.getAabb().mo9826C(d7, d8, d9);
                double d10 = (double) (vec3f.x * next.size);
                double d11 = (double) (vec3f.y * next.size);
                double d12 = (double) (vec3f.z * next.size);
                double d13 = (double) (vec3f2.x * next.size);
                double d14 = (double) (vec3f2.y * next.size);
                double d15 = (double) (vec3f2.z * next.size);
                float cos = 1.4142137f * ((float) Math.cos((double) ((next.deX + 45.0f) * 0.017453292f)));
                float sin = 1.4142137f * ((float) Math.sin((double) ((next.deX + 45.0f) * 0.017453292f)));
                this.vertexes.setPosition(i2, (float) ((((double) cos) * d10) + (((double) sin) * d13) + d7), (float) ((((double) cos) * d11) + (((double) sin) * d14) + d8), (float) ((((double) cos) * d12) + (((double) sin) * d15) + d9));
                this.vertexes.setColor(i2, next.color.x, next.color.y, next.color.z, next.color.w);
                int i7 = i2 + 1;
                float f = -sin;
                this.vertexes.setPosition(i7, (float) ((((double) f) * d10) + (((double) cos) * d13) + d7), (float) ((((double) f) * d11) + (((double) cos) * d14) + d8), (float) ((((double) f) * d12) + (((double) cos) * d15) + d9));
                this.vertexes.setColor(i7, next.color.x, next.color.y, next.color.z, next.color.w);
                int i8 = i7 + 1;
                float f2 = -cos;
                this.vertexes.setPosition(i8, (float) ((((double) f2) * d10) + (((double) f) * d13) + d7), (float) ((((double) f2) * d11) + (((double) f) * d14) + d8), (float) ((((double) f2) * d12) + (((double) f) * d15) + d9));
                this.vertexes.setColor(i8, next.color.x, next.color.y, next.color.z, next.color.w);
                int i9 = i8 + 1;
                float f3 = -f;
                this.vertexes.setPosition(i9, (float) (d7 + (((double) f3) * d10) + (((double) f2) * d13)), (float) (d8 + (((double) f3) * d11) + (((double) f2) * d14)), (float) ((((double) f3) * d12) + (((double) f2) * d15) + d9));
                this.vertexes.setColor(i9, next.color.x, next.color.y, next.color.z, next.color.w);
                i2 = i9 + 1;
                i4 = i5;
                i = i6;
            }
        }
        this.indexes.setSize(i * 4);
    }

    /* access modifiers changed from: package-private */
    public void generateMesh(RenderContext renderContext) {
        Camera camera = renderContext.getCamera();
        camera.getTransform().getX(renderContext.vec3fTemp0);
        camera.getTransform().getY(renderContext.vec3fTemp1);
        camera.getTransform().getZ(renderContext.vec3fTemp2);
        camera.getTransform().getTranslation(renderContext.vec3dTemp0);
        if (this.alignToHorizon) {
            generateMeshHorizonAligned(renderContext.vec3fTemp0, renderContext.vec3fTemp2, renderContext.vec3dTemp0);
        } else if (this.alignToMovement) {
            generateMeshMovementAligned(renderContext.vec3fTemp2, renderContext.vec3dTemp0);
        } else {
            switch ($SWITCH_TABLE$taikodom$render$enums$BillboardAlignment()[this.alignment.ordinal()]) {
                case 1:
                case 3:
                    if (this.material != null && this.material.getShader() != null && this.material.getShader().getBestShader(renderContext.getDc()).getPass(0).getProgram() != null) {
                        generateMeshForGPUAlignment(renderContext.vec3dTemp0);
                        break;
                    } else {
                        generateMeshDefaultAligned(renderContext.vec3fTemp0, renderContext.vec3fTemp1, renderContext.vec3dTemp0);
                        break;
                    }
                case 2:
                    generateMeshAxisAligned(renderContext.vec3fTemp2, renderContext.vec3dTemp0);
                    break;
                case 4:
                    this.globalTransform.getX(renderContext.vec3fTemp0);
                    this.globalTransform.getY(renderContext.vec3fTemp1);
                    generateMeshHorizonAligned(renderContext.vec3fTemp0, renderContext.vec3fTemp1, renderContext.vec3dTemp0);
                    break;
                default:
                    generateMeshDefaultAligned(renderContext.vec3fTemp0, renderContext.vec3fTemp1, renderContext.vec3dTemp0);
                    break;
            }
            if (this.mesh != null) {
                this.localAabb.reset();
                if (!this.mesh.getAabb().dim().anA() || !this.mesh.getAabb().din().anA()) {
                    this.mesh.getAabb().mo9826C(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
                }
                this.localAabb.mo9868h(this.mesh.getAabb());
            }
        }
    }

    private void generateMeshMovementAligned(Vec3f vec3f, Vec3d ajr) {
        double d = ScriptRuntime.NaN;
        double d2 = ScriptRuntime.NaN;
        double d3 = ScriptRuntime.NaN;
        if (this.followEmitter) {
            d = this.globalTransform.position.x;
            d2 = this.globalTransform.position.y;
            d3 = this.globalTransform.position.z;
        }
        double d4 = d - ajr.x;
        double d5 = d2 - ajr.y;
        double d6 = d3 - ajr.z;
        int i = 0;
        Vec3f vec3f2 = new Vec3f();
        Vec3f vec3f3 = new Vec3f();
        this.indexes.clear();
        Iterator<C5153a> it = this.particlesPool.iterator();
        int i2 = 0;
        while (it.hasNext()) {
            C5153a next = it.next();
            if (next.imi) {
                double d7 = next.position.x + d4;
                double d8 = next.position.y + d5;
                double d9 = next.position.z + d6;
                vec3f2.x = (float) next.imd.x;
                vec3f2.y = (float) next.imd.y;
                vec3f2.z = (float) next.imd.z;
                vec3f2.normalize();
                vec3f3.cross(vec3f2, vec3f);
                vec3f3.normalize();
                double d10 = (double) (vec3f3.x * next.size);
                double d11 = (double) (vec3f3.y * next.size);
                double d12 = (double) (vec3f3.z * next.size);
                double d13 = (double) (vec3f2.x * next.size);
                double d14 = (double) (vec3f2.y * next.size);
                double d15 = (double) (vec3f2.z * next.size);
                this.vertexes.setPosition(i, (float) (d7 - (d10 + d13)), (float) (d8 - (d11 + d14)), (float) (d9 - (d12 + d15)));
                this.vertexes.setColor(i, next.color.x, next.color.y, next.color.z, next.color.w);
                int i3 = i + 1;
                this.vertexes.setPosition(i3, (float) ((d10 - d13) + d7), (float) ((d11 - d14) + d8), (float) ((d12 - d15) + d9));
                this.vertexes.setColor(i3, next.color.x, next.color.y, next.color.z, next.color.w);
                int i4 = i3 + 1;
                this.vertexes.setPosition(i4, (float) (d10 + d13 + d7), (float) (d11 + d14 + d8), (float) (d12 + d15 + d9));
                this.vertexes.setColor(i4, next.color.x, next.color.y, next.color.z, next.color.w);
                int i5 = i4 + 1;
                this.vertexes.setPosition(i5, (float) ((-d10) + d13 + d7), (float) ((-d11) + d14 + d8), (float) ((-d12) + d15 + d9));
                this.vertexes.setColor(i5, next.color.x, next.color.y, next.color.z, next.color.w);
                i = i5 + 1;
                i2++;
            }
        }
        this.indexes.setSize(i2 * 4);
    }

    private void generateMeshHorizonAligned(Vec3f vec3f, Vec3f vec3f2, Vec3d ajr) {
        double d = ScriptRuntime.NaN;
        double d2 = ScriptRuntime.NaN;
        double d3 = ScriptRuntime.NaN;
        if (this.followEmitter) {
            d = this.globalTransform.position.x;
            d2 = this.globalTransform.position.y;
            d3 = this.globalTransform.position.z;
        }
        double d4 = d - ajr.x;
        double d5 = d2 - ajr.y;
        double d6 = d3 - ajr.z;
        int i = 0;
        int i2 = 0;
        int i3 = this.particlesLimit;
        Iterator<C5153a> it = this.particlesPool.iterator();
        int i4 = i3;
        while (it.hasNext()) {
            C5153a next = it.next();
            if (next.imi) {
                int i5 = i4 - 1;
                if (i5 <= 0) {
                    break;
                }
                double d7 = next.position.x + d4;
                double d8 = next.position.y + d5;
                double d9 = next.position.z + d6;
                double d10 = (double) (vec3f.x * next.size);
                double d11 = (double) (vec3f.y * next.size);
                double d12 = (double) (vec3f.z * next.size);
                double d13 = (double) (vec3f2.x * next.size);
                double d14 = (double) (vec3f2.y * next.size);
                double d15 = (double) (vec3f2.z * next.size);
                this.vertexes.setPosition(i2, (float) ((d7 - d10) - d13), (float) ((d8 - d11) - d14), (float) ((d9 - d12) - d15));
                this.vertexes.setColor(i2, next.color.x, next.color.y, next.color.z, next.color.w);
                int i6 = i2 + 1;
                this.vertexes.setPosition(i6, (float) ((d7 + d10) - d13), (float) ((d8 + d11) - d14), (float) ((d9 + d12) - d15));
                this.vertexes.setColor(i6, next.color.x, next.color.y, next.color.z, next.color.w);
                int i7 = i6 + 1;
                this.vertexes.setPosition(i7, (float) (d7 + d10 + d13), (float) (d8 + d11 + d14), (float) (d9 + d12 + d15));
                this.vertexes.setColor(i7, next.color.x, next.color.y, next.color.z, next.color.w);
                int i8 = i7 + 1;
                this.vertexes.setPosition(i8, (float) ((d7 - d10) + d13), (float) ((d8 - d11) + d14), (float) ((d9 - d12) + d15));
                this.vertexes.setColor(i8, next.color.x, next.color.y, next.color.z, next.color.w);
                i2 = i8 + 1;
                i4 = i5;
                i++;
            }
        }
        this.indexes.setSize(i * 4);
    }

    private void generateMeshAxisAligned(Vec3f vec3f, Vec3d ajr) {
        this.mesh.getAabb().reset();
    }

    private void generateMeshForGPUAlignment(Vec3d ajr) {
        double d = ScriptRuntime.NaN;
        double d2 = ScriptRuntime.NaN;
        double d3 = ScriptRuntime.NaN;
        if (this.followEmitter) {
            d = this.globalTransform.position.x;
            d2 = this.globalTransform.position.y;
            d3 = this.globalTransform.position.z;
        }
        double d4 = d - ajr.x;
        double d5 = d2 - ajr.y;
        double d6 = d3 - ajr.z;
        this.indexes.clear();
        int i = 0;
        Iterator<C5153a> it = this.particlesPool.iterator();
        int i2 = 0;
        while (it.hasNext()) {
            C5153a next = it.next();
            if (next.imi) {
                int i3 = i2 + 1;
                double d7 = next.position.x + d4;
                double d8 = next.position.y + d5;
                double d9 = next.position.z + d6;
                this.vertexes.setPosition(i, (float) d7, (float) d8, (float) d9);
                this.vertexes.setPosition(i + 1, (float) d7, (float) d8, (float) d9);
                this.vertexes.setPosition(i + 2, (float) d7, (float) d8, (float) d9);
                this.vertexes.setPosition(i + 3, (float) d7, (float) d8, (float) d9);
                float ju = (next.color.y * 0.255f) + Maths.m22671ju(next.color.x * 255.0f);
                float ju2 = (next.color.w * 0.255f) + Maths.m22671ju(next.color.z * 255.0f);
                this.vertexes.setColor(i, ju, ju2, next.size, next.deX);
                this.vertexes.setColor(i + 1, ju, ju2, next.size, next.deX);
                this.vertexes.setColor(i + 2, ju, ju2, next.size, next.deX);
                this.vertexes.setColor(i + 3, ju, ju2, next.size, next.deX);
                i += 4;
                i2 = i3;
            }
        }
        this.indexes.setSize(i2 * 4);
    }

    public void releaseReferences() {
        super.releaseReferences();
        this.mesh = null;
        this.indexes = null;
        this.vertexes = null;
        releaseParticles();
    }

    public boolean isNeedToStep() {
        return true;
    }

    public void dispose() {
        super.dispose();
        releaseParticles();
    }

    private void releaseParticles() {
        Iterator<C5153a> it = this.particlesPool.iterator();
        while (it.hasNext()) {
            C5153a next = it.next();
            if (next.imi) {
                globalAliveParticles--;
                next.imi = false;
            }
        }
        this.particlesPool.clear();
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        releaseParticles();
    }

    public void validate(SceneLoader sceneLoader) {
        super.validate(sceneLoader);
        if (this.material.getShader() == null) {
            this.material.setShader(sceneLoader.getDefaultTrailShader());
        }
    }

    public Vec3d getParticlesAcceleration() {
        return this.particlesAcceleration;
    }

    public void setParticlesAcceleration(Vec3d ajr) {
        this.particlesAcceleration.mo9484aA(ajr);
    }

    public Color getParticlesColorVariation() {
        return this.particlesColorVariation;
    }

    public void setParticlesColorVariation(Color color) {
        this.particlesColorVariation.set(color);
    }

    public aLH computeLocalSpaceAABB() {
        return this.localAabb;
    }

    public double getSpawnLimitStart() {
        return this.spawnLimitStart;
    }

    public void setSpawnLimitStart(double d) {
        this.spawnLimitStart = d;
    }

    public double getSpawnLimitEnd() {
        return this.spawnLimitEnd;
    }

    public void setSpawnLimitEnd(double d) {
        this.spawnLimitEnd = d;
    }

    public void onRemove() {
        super.onRemove();
        releaseParticles();
    }

    /* renamed from: taikodom.render.scene.RParticleSystem$a */
    class C5153a {
        final Color color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        final Vec3d imc = new Vec3d();
        final Vec3d imd = new Vec3d();
        final Vec3d ime = new Vec3d();
        final Color imf = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        final Vec3d position = new Vec3d();
        float deX = 0.0f;
        float img = 0.0f;
        float imh = 0.0f;
        boolean imi = false;
        float lifeTime = 0.0f;
        float size = 1.0f;

        public C5153a() {
        }
    }
}
