package taikodom.render.scene;

import taikodom.render.StepContext;
import taikodom.render.loader.RenderAsset;

import java.util.Iterator;
import java.util.Vector;

/* compiled from: a */
public class SMultiLayerMusic extends SoundObject {
    int currentLayer;
    float gain;
    Vector<SMusic> musics;
    Vector<VolumeInterpolator> volumeInterpolators;

    public SMultiLayerMusic() {
        this.currentLayer = 0;
        this.gain = 1.0f;
        this.musics = new Vector<>();
        this.volumeInterpolators = new Vector<>();
    }

    public SMultiLayerMusic(SMultiLayerMusic sMultiLayerMusic) {
        this.currentLayer = sMultiLayerMusic.currentLayer;
        this.gain = sMultiLayerMusic.gain;
        int musicCount = sMultiLayerMusic.musicCount();
        for (int i = 0; i < musicCount; i++) {
            addMusic(sMultiLayerMusic.musics.get(i));
        }
    }

    public void releaseReferences() {
        super.releaseReferences();
        this.musics.clear();
    }

    public void dispose() {
        stop();
        this.disposed = true;
    }

    public int step(StepContext stepContext) {
        int i = 1;
        Iterator<VolumeInterpolator> it = this.volumeInterpolators.iterator();
        while (it.hasNext()) {
            VolumeInterpolator next = it.next();
            boolean z = !next.step(stepContext.getDeltaTime());
            SMusic music = next.getMusic();
            if (music != null) {
                music.setGain(next.getValue() * this.gain);
            }
            if (z) {
                if (music != null && next.getValue() <= 0.0f) {
                    music.stop();
                }
                it.remove();
            }
        }
        for (int i2 = 0; i2 < this.musics.size(); i2++) {
            i *= this.musics.get(i2).step(stepContext);
        }
        return i;
    }

    public boolean play() {
        if (this.currentLayer >= this.musics.size()) {
            return false;
        }
        this.musics.get(this.currentLayer).play();
        this.musics.get(this.currentLayer).setGain(this.gain);
        return true;
    }

    public boolean stop() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.musics.size()) {
                return true;
            }
            this.musics.get(i2).stop();
            i = i2 + 1;
        }
    }

    public boolean pause() {
        if (this.currentLayer >= this.musics.size()) {
            return false;
        }
        this.musics.get(this.currentLayer).pause();
        return true;
    }

    public boolean fadeOut(float f) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.musics.size()) {
                return true;
            }
            this.musics.get(i2).fadeOut(f);
            i = i2 + 1;
        }
    }

    public boolean fadeIn(float f) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.musics.size()) {
                return true;
            }
            this.musics.get(i2).fadeIn(f);
            i = i2 + 1;
        }
    }

    public void changeLayer(int i, float f) {
        float f2;
        if (this.currentLayer != i && i < this.musics.size()) {
            VolumeInterpolator volumeInterpolator = new VolumeInterpolator();
            float f3 = this.gain;
            int i2 = 0;
            SMusic sMusic = this.musics.get(this.currentLayer);
            if (sMusic != null) {
                f3 = sMusic.getGain();
                i2 = sMusic.getSamplePositionMs();
            }
            volumeInterpolator.init(f3, 0.0f, f, sMusic);
            this.volumeInterpolators.add(volumeInterpolator);
            VolumeInterpolator volumeInterpolator2 = new VolumeInterpolator();
            SMusic sMusic2 = this.musics.get(i);
            if (sMusic2 != null) {
                float gain2 = sMusic2.getGain();
                sMusic2.play();
                sMusic2.setGain(0.0f);
                sMusic2.setSamplePositionMs(i2);
                f2 = gain2;
            } else {
                f2 = 0.0f;
            }
            volumeInterpolator2.init(f2, this.gain, f, sMusic2);
            this.volumeInterpolators.add(volumeInterpolator2);
            this.currentLayer = i;
        }
    }

    public void addMusic(SMusic sMusic) {
        this.musics.add(sMusic);
    }

    public int musicCount() {
        return this.musics.size();
    }

    public SMusic getMusic(int i) {
        return this.musics.get(i);
    }

    public void setMusic(int i, SMusic sMusic) {
        if (this.musics.size() <= i || i < 0) {
            this.musics.add(sMusic);
        } else {
            this.musics.set(i, sMusic);
        }
    }

    public float getGain() {
        return this.gain;
    }

    public void setGain(float f) {
        this.gain = f;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < musicCount()) {
                this.musics.get(i2).setGain(f);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public RenderAsset cloneAsset() {
        return this;
    }
}
