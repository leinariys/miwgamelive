package taikodom.render.scene;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vector2fWrap;
import taikodom.render.MilesSoundSource;
import taikodom.render.SoundBuffer;
import taikodom.render.StepContext;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public class SSoundSource extends SoundObject {
    public static float MIN_AUDIBLE_VOLUME = -35.0f;
    final Vector2fWrap distances;
    final Vec3f position;
    final Vec3f velocity;
    float audibleRadius;
    boolean disposeAfterPlaying;
    float gain;
    boolean is3d;
    float pitch;
    private int loopCount;
    private SoundSource source;

    private SSoundSource(SSoundSource sSoundSource) {
        super(sSoundSource);
        this.distances = new Vector2fWrap();
        this.velocity = new Vec3f();
        this.position = new Vec3f();
        this.disposeAfterPlaying = true;
        this.source = null;
        this.loopCount = sSoundSource.loopCount;
        this.gain = sSoundSource.gain;
        this.distances.set(sSoundSource.distances);
        this.velocity.set(sSoundSource.velocity);
        this.pitch = sSoundSource.pitch;
        this.audibleRadius = sSoundSource.audibleRadius;
        this.is3d = sSoundSource.is3d;
        this.source = new MilesSoundSource();
        this.source.setBuffer(sSoundSource.getBuffer());
        setBuffer(sSoundSource.getBuffer());
        this.disposeAfterPlaying = sSoundSource.disposeAfterPlaying;
    }

    public SSoundSource() {
        this.distances = new Vector2fWrap();
        this.velocity = new Vec3f();
        this.position = new Vec3f();
        this.disposeAfterPlaying = true;
        this.source = null;
        this.loopCount = 1;
        this.gain = 1.0f;
        this.distances.x = 50.0f;
        this.distances.y = 1000.0f;
        this.pitch = 1.0f;
        this.audibleRadius = 1000.0f;
        this.is3d = false;
        this.source = new MilesSoundSource();
    }

    public void setDisposeAfterPlaying(boolean z) {
        this.disposeAfterPlaying = z;
    }

    public void releaseReferences() {
        super.releaseReferences();
        stop();
        if (this.source != null) {
            this.source.releaseReferences();
            this.source = null;
        }
    }

    public void dispose() {
        stop();
        this.disposed = true;
    }

    public int getLoopCount() {
        return this.loopCount;
    }

    public void setLoopCount(int i) {
        this.loopCount = i;
        if (this.source != null) {
            this.source.setLoop(this.loopCount);
        }
    }

    public float getGain() {
        return this.gain;
    }

    public void setGain(float f) {
        this.gain = f;
        if (this.source != null) {
            this.source.setGain(f);
        }
    }

    public void setPitch(float f) {
        this.pitch = f;
        if (this.source != null) {
            this.source.setPitch(f);
        }
    }

    public boolean play() {
        if (this.source == null || this.source.getBuffer() == null) {
            return false;
        }
        this.firstFrame = false;
        if (this.source.isPlaying()) {
            return this.source.play(this.loopCount);
        }
        if (!this.source.create()) {
            return false;
        }
        this.source.setGain(this.gain);
        this.source.setPitch(this.pitch);
        this.source.setMinMaxDistance(this.distances.x, this.distances.y);
        this.source.setLoop(this.loopCount);
        if (this.is3d) {
            this.transform.getTranslation(this.position);
            this.source.setPosition(this.position);
            this.source.setVelocity(this.velocity);
        }
        this.source.play(this.loopCount);
        return true;
    }

    public boolean fadeOut(float f) {
        if (this.source != null) {
            return this.source.fadeOut(f);
        }
        return false;
    }

    public boolean stop() {
        if (this.source != null) {
            return this.source.stop();
        }
        return false;
    }

    public boolean isPlaying() {
        if (this.source != null) {
            return this.source.isPlaying();
        }
        return false;
    }

    public boolean isValid() {
        if (this.source != null) {
            return this.source.isValid();
        }
        return false;
    }

    public SoundSource getSource() {
        return this.source;
    }

    public void setSource(SoundSource soundSource) {
        this.source = soundSource;
    }

    public SoundBuffer getBuffer() {
        return this.source.getBuffer();
    }

    public void setBuffer(SoundBuffer soundBuffer) {
        this.source.setBuffer(soundBuffer);
    }

    public boolean isAlive() {
        return isPlaying();
    }

    public float getAudibleRadius() {
        return this.audibleRadius;
    }

    public void setAudibleRadius(float f) {
        this.audibleRadius = f;
    }

    public int step(StepContext stepContext) {
        if (this.firstFrame) {
            if (this.is3d && this.loopCount != 0) {
                double g = stepContext.getCamera().getGlobalTransform().mo17352g(this.globalTransform);
                float maxListenDistance = getMaxListenDistance();
                if (g > ((double) (maxListenDistance * maxListenDistance))) {
                    if (!this.disposeAfterPlaying) {
                        return 1;
                    }
                    dispose();
                    releaseReferences();
                    return 1;
                }
            }
            play();
            this.firstFrame = false;
        }
        if (this.source == null) {
            return 1;
        }
        if (this.is3d) {
            this.position.x = (float) (this.globalTransform.position.x - stepContext.getCamera().getGlobalTransform().position.x);
            this.position.y = (float) (this.globalTransform.position.y - stepContext.getCamera().getGlobalTransform().position.y);
            this.position.z = (float) (this.globalTransform.position.z - stepContext.getCamera().getGlobalTransform().position.z);
            this.source.setPosition(this.position);
            this.source.setVelocity(this.velocity);
        }
        if (this.source.step(stepContext) == 0) {
            return 0;
        }
        if (!this.disposeAfterPlaying) {
            return 1;
        }
        dispose();
        releaseReferences();
        return 1;
    }

    public void reset() {
        play();
    }

    public Vector2fWrap getDistances() {
        return this.distances;
    }

    public void setDistances(Vector2fWrap aka) {
        this.distances.set(aka);
        if (this.distances.x <= 0.0f) {
            this.distances.x = 0.01f;
        }
        if (this.source != null) {
            this.source.setMinMaxDistance(this.distances.x, this.distances.y);
        }
    }

    public boolean getIs3d() {
        return this.is3d;
    }

    public void setIs3d(boolean z) {
        this.is3d = z;
    }

    public void onRemove() {
        super.onRemove();
        stop();
    }

    public int getTotalMilesSounds() {
        if (this.source != null) {
            return this.source.getTotalMilesSounds();
        }
        return 0;
    }

    public RenderAsset cloneAsset() {
        return new SSoundSource(this);
    }

    public void setSquaredSoundSource(SceneObject sceneObject) {
        throw new RuntimeException("Must implement this");
    }

    public float getMaxListenDistance() {
        if (!this.is3d) {
            return -1.0f;
        }
        return this.distances.x / ((float) Math.pow(10.0d, (double) (MIN_AUDIBLE_VOLUME / 20.0f)));
    }

    public float getTimeLenght() {
        if (this.source != null) {
            return this.source.getTimeLenght();
        }
        return 0.0f;
    }

    public float getSamplePosition() {
        if (this.source != null) {
            return (float) this.source.getSamplePositionMs();
        }
        return 0.0f;
    }
}
