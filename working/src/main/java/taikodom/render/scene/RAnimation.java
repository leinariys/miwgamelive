package taikodom.render.scene;

import logic.render.granny.AdapterGrannyJNI;
import logic.render.granny.C3242pY;
import logic.render.granny.C5801aah;
import taikodom.render.StepContext;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public class RAnimation extends RenderAsset {
    private float animationSpeed = 1.0f;
    private boolean disposed = false;
    private float fadeTime = 0.3f;
    private C3242pY grannyAnimation = null;
    private C5801aah grannyControl = null;
    private boolean initOnce = true;
    private RModel model = null;
    private boolean removeOnEnd = false;
    private boolean stopping = false;

    public RAnimation() {
    }

    public RAnimation(C3242pY pYVar) {
        this.grannyAnimation = pYVar;
    }

    public RAnimation(RAnimation rAnimation) {
        super(rAnimation);
        this.removeOnEnd = rAnimation.removeOnEnd;
        this.grannyAnimation = rAnimation.grannyAnimation;
        this.fadeTime = rAnimation.fadeTime;
        this.animationSpeed = rAnimation.animationSpeed;
    }

    public RenderAsset cloneAsset() {
        return new RAnimation(this);
    }

    public float getAnimationSpeed() {
        return this.animationSpeed;
    }

    public void setAnimationSpeed(float f) {
        this.animationSpeed = f;
        if (this.grannyControl != null) {
            AdapterGrannyJNI.m4861f(this.grannyControl, this.animationSpeed);
        }
    }

    public C5801aah getControl() {
        return this.grannyControl;
    }

    public float getFadeTime() {
        return this.fadeTime;
    }

    public void setFadeTime(float f) {
        this.fadeTime = f;
    }

    public C3242pY getGrannyAnimation() {
        return this.grannyAnimation;
    }

    public void setGrannyAnimation(C3242pY pYVar) {
        this.grannyAnimation = pYVar;
    }

    public RModel getModel() {
        return this.model;
    }

    public void setModel(RModel rModel) {
        this.model = rModel;
    }

    public float getRemainingTimeSeconds() {
        if (this.grannyControl != null) {
            return AdapterGrannyJNI.m5098o(this.grannyControl);
        }
        return 0.0f;
    }

    public boolean isInitOnce() {
        return this.initOnce;
    }

    public void setInitOnce(boolean z) {
        this.initOnce = z;
    }

    public boolean isPlaying() {
        return this.grannyControl != null;
    }

    public boolean isRemoveOnEnd() {
        return this.removeOnEnd;
    }

    public void setRemoveOnEnd(boolean z) {
        this.removeOnEnd = z;
    }

    public void play() {
        play(0);
    }

    public void play(int i) {
        if (this.model != null && this.model.getGrannyModelInstance() != null && this.grannyAnimation != null) {
            if (this.grannyControl != null) {
                AdapterGrannyJNI.m4766c(this.grannyControl);
            }
            this.initOnce = true;
            this.stopping = false;
            this.grannyControl = AdapterGrannyJNI.m3829a(this.model.getGameClock(), this.grannyAnimation, this.model.getGrannyModelInstance());
            if (this.fadeTime > 0.0f) {
                AdapterGrannyJNI.m4568b(this.grannyControl, this.fadeTime, false);
            }
            AdapterGrannyJNI.m4237a(this.grannyControl, i);
            AdapterGrannyJNI.m4861f(this.grannyControl, this.animationSpeed);
        }
    }

    public void reset() {
        play();
    }

    public void step(StepContext stepContext) {
        if (this.grannyControl != null) {
            float o = AdapterGrannyJNI.m5098o(this.grannyControl);
            float t = AdapterGrannyJNI.m5122t(this.grannyControl);
            if (o <= 0.0f || (t <= 0.0f && this.stopping)) {
                AdapterGrannyJNI.m4766c(this.grannyControl);
                this.grannyControl = null;
                if (this.removeOnEnd && !stepContext.getEditMode()) {
                    dispose();
                    return;
                }
                return;
            }
            this.model.setHasActiveAnimation(true);
        } else if (this.removeOnEnd && !stepContext.getEditMode()) {
            dispose();
        }
    }

    private void dispose() {
        this.disposed = true;
    }

    public void stop() {
        if (this.grannyControl != null) {
            this.stopping = true;
            if (this.fadeTime > 0.0f) {
                AdapterGrannyJNI.m4646b(this.grannyControl, AdapterGrannyJNI.m4883h(this.grannyControl, this.fadeTime));
                return;
            }
            AdapterGrannyJNI.m4766c(this.grannyControl);
            this.grannyControl = null;
        }
    }

    public boolean isDisposed() {
        return this.disposed;
    }
}
