package taikodom.render.scene;

import com.hoplon.geometry.Color;
import game.geometry.Vec3d;
import gnu.trove.THashSet;
import lombok.extern.slf4j.Slf4j;
import taikodom.render.RenderContext;
import taikodom.render.RenderView;
import taikodom.render.StepContext;
import taikodom.render.partitioning.TkdWorld;
import taikodom.render.primitives.RenderAreaInfo;

import javax.media.opengl.GL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/* compiled from: a */
@Slf4j
public class Scene {
    private float colorIntensity = 1.0f;
    private float colorIntensityResponse = 0.65f;
    private float currentColorCorrection = 0.0f;
    private boolean enabled = true;
    private String name = null;
    private Set<SceneObject> objects = new THashSet();
    private List<SceneObject> postPonedRemovalList = new ArrayList();
    @Deprecated
    private List<RSceneAreaInfo> sceneAreaInfos = new ArrayList();
    private SceneConfig sceneConfig = new SceneConfig();
    private TkdWorld tkdWorld = null;
    private boolean useTkdWorld = false;

    public Scene() {
    }

    public Scene(String str) {
        this.name = str;
    }

    public void createPartitioningStructure() {
        if (this.tkdWorld == null) {
            this.tkdWorld = new TkdWorld();
            synchronized (this.objects) {
                for (SceneObject addChild : this.objects) {
                    this.tkdWorld.addChild(addChild);
                }
            }
            this.useTkdWorld = true;
        }
    }

    public boolean addChild(SceneObject sceneObject) {
        boolean add;
        if (sceneObject == null) {
            throw new NullPointerException("Cannot add a null SceneObject");
        }
        sceneObject.updateInternalGeometry((SceneObject) null);
        synchronized (sceneObject) {
            add = this.objects.add(sceneObject);
            if (this.tkdWorld != null) {
                this.tkdWorld.addChild(sceneObject);
            }
        }
        return add;
    }

    public void removeAllChildren() {
        synchronized (this.objects) {
            for (SceneObject onRemove : this.objects) {
                onRemove.onRemove();
            }
            this.objects.clear();
            this.sceneAreaInfos.clear();
            if (this.tkdWorld != null) {
                this.tkdWorld.removeAll();
            }
        }
    }

    public boolean removeChild(SceneObject sceneObject) {
        boolean remove;
        if (sceneObject == null) {
            log.debug("Trying to remove a null child.");
            return false;
        }
        synchronized (this.objects) {
            remove = this.objects.remove(sceneObject);
            sceneObject.onRemove();
            if (this.tkdWorld != null) {
                this.tkdWorld.removeChild(sceneObject);
            }
        }
        return remove;
    }

    public void render(RenderContext renderContext) {
        if (!this.useTkdWorld || this.tkdWorld == null) {
            boolean isOrthogonal = renderContext.getCamera().isOrthogonal();
            synchronized (this.objects) {
                for (SceneObject fillRenderQueue : this.objects) {
                    fillRenderQueue.fillRenderQueue(renderContext, !isOrthogonal);
                }
            }
            return;
        }
        this.tkdWorld.cullZone(renderContext, true);
    }

    private boolean stepObject(SceneObject sceneObject, StepContext stepContext) {
        if (sceneObject.isDisposed() || sceneObject.step(stepContext) != 0) {
            return true;
        }
        int childCount = sceneObject.childCount();
        sceneObject.setCanRemoveChildren(false);
        for (int i = 0; i < childCount; i++) {
            if (stepObject(sceneObject.getChild(i), stepContext)) {
                sceneObject.removeChild(sceneObject.getChild(i));
            }
        }
        sceneObject.setCanRemoveChildren(true);
        if (sceneObject instanceof RLod) {
            RLod rLod = (RLod) sceneObject;
            if (rLod.getObjectLod0() != null) {
                stepObject(rLod.getObjectLod0(), stepContext);
            }
            if (rLod.getObjectLod1() != null) {
                stepObject(rLod.getObjectLod1(), stepContext);
            }
            if (rLod.getObjectLod2() != null) {
                stepObject(rLod.getObjectLod2(), stepContext);
            }
        }
        return false;
    }

    public void step(StepContext stepContext) {
        if (!this.useTkdWorld || this.tkdWorld == null) {
            stepContext.incObjectSteps((long) this.objects.size());
            synchronized (this.objects) {
                for (SceneObject next : this.objects) {
                    if (stepObject(next, stepContext)) {
                        this.postPonedRemovalList.add(next);
                    }
                }
                if (this.postPonedRemovalList.size() > 0) {
                    this.objects.removeAll(this.postPonedRemovalList);
                    this.postPonedRemovalList.clear();
                }
            }
        } else {
            this.tkdWorld.stepObjects(stepContext);
        }
        updateColorIntensity(stepContext.getDeltaTime());
    }

    public Set<SceneObject> getChildren() {
        return this.objects;
    }

    public SceneConfig getSceneConfig() {
        return this.sceneConfig;
    }

    public void setSceneConfig(SceneConfig sceneConfig2) {
        this.sceneConfig = sceneConfig2;
    }

    public RenderAreaInfo fillRenderAreaInfo(double d, double d2, double d3, RenderAreaInfo renderAreaInfo) {
        RSceneAreaInfo rSceneAreaInfo;
        if (this.sceneAreaInfos.size() > 0 && (rSceneAreaInfo = this.sceneAreaInfos.get(0)) != null) {
            renderAreaInfo.ambientColor.set(rSceneAreaInfo.ambientColor);
            renderAreaInfo.diffuseCubemap = rSceneAreaInfo.diffuseCubemap;
            renderAreaInfo.reflectiveCubemap = rSceneAreaInfo.reflectiveCubemap;
        }
        return renderAreaInfo;
    }

    @Deprecated
    public void addSceneAreaInfo(RSceneAreaInfo rSceneAreaInfo) {
        if (rSceneAreaInfo != null) {
            this.sceneAreaInfos.add(rSceneAreaInfo);
        }
    }

    @Deprecated
    public RSceneAreaInfo getSceneAreaInfo(Vec3d ajr) {
        if (this.sceneAreaInfos.size() > 0) {
            return this.sceneAreaInfos.get(0);
        }
        return null;
    }

    public void fillRenderViewAreaInfo(Vec3d ajr, RenderView renderView) {
        if (this.sceneAreaInfos.size() > 0) {
            RSceneAreaInfo rSceneAreaInfo = this.sceneAreaInfos.get(0);
            if (rSceneAreaInfo != null) {
                renderView.setFogColor(rSceneAreaInfo.fogColor);
                renderView.setFogEnd(rSceneAreaInfo.fogEnd);
                renderView.setFogStart(rSceneAreaInfo.fogStart);
                renderView.setFogExp(rSceneAreaInfo.fogExp);
                renderView.setFogType(rSceneAreaInfo.fogType.glEquivalent());
                return;
            }
            renderView.setFogColor(new Color(0.0f, 0.0f, 0.0f, 0.0f));
            renderView.setFogEnd(100000.0f);
            renderView.setFogStart(50000.0f);
        }
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean z) {
        this.enabled = z;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String toString() {
        return "Scene: " + this.name;
    }

    public boolean isUseTkdWorld() {
        return this.useTkdWorld;
    }

    public void setUseTkdWorld(boolean z) {
        this.useTkdWorld = z;
    }

    public void addStaticChild(SceneObject sceneObject) {
        if (sceneObject == null) {
            throw new NullPointerException("Cannot add a null SceneObject");
        }
        sceneObject.updateInternalGeometry((SceneObject) null);
        synchronized (sceneObject) {
            this.objects.add(sceneObject);
            if (this.tkdWorld != null) {
                this.tkdWorld.addStaticChild(sceneObject);
            }
        }
    }

    public void renderAABBs(GL gl, Vec3d ajr) {
        if (this.tkdWorld != null) {
            this.tkdWorld.renderGL(gl, ajr);
        }
    }

    public boolean hasChild(SceneObject sceneObject) {
        synchronized (this.objects) {
            for (SceneObject sceneObject2 : this.objects) {
                if (sceneObject2 == sceneObject) {
                    return true;
                }
            }
            return false;
        }
    }

    public void addColorIntensity(float f) {
        this.colorIntensity += f;
    }

    public float getColorIntensity() {
        return this.colorIntensity;
    }

    public float getColorCorrection() {
        return this.currentColorCorrection;
    }

    public void updateColorIntensity(float f) {
        float f2;
        float f3 = 1.0f;
        float f4 = this.colorIntensityResponse * f;
        float f5 = 1.0f - this.colorIntensity;
        if (f5 < 0.0f) {
            f5 = 0.0f;
        } else if (f5 > 1.0f) {
            f5 = 1.0f;
        }
        float f6 = f5 - this.currentColorCorrection;
        if (f6 > 0.0f) {
            this.currentColorCorrection += f6 * f4;
        } else {
            if (f6 < 0.0f) {
                f2 = (-0.5f * this.colorIntensityResponse) / f6;
            } else {
                f2 = 1.0f;
            }
            if (f2 <= 1.0f) {
                f3 = f2;
            }
            this.currentColorCorrection += f3 * f6 * f4;
        }
        this.colorIntensity = 0.0f;
    }
}
