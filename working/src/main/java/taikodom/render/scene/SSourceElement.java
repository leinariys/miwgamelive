package taikodom.render.scene;

import game.geometry.Vec3d;
import game.geometry.Vector2fWrap;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public class SSourceElement extends RenderAsset {
    private Vector2fWrap distances;
    private Vec3d position;

    public SSourceElement() {
        this.position = new Vec3d();
        this.distances = new Vector2fWrap();
    }

    public SSourceElement(SSourceElement sSourceElement) {
        this();
        this.position.mo9484aA(sSourceElement.position);
        this.distances.set(sSourceElement.distances);
    }

    public Vec3d getPosition() {
        return this.position;
    }

    public void setPosition(Vec3d ajr) {
        this.position.mo9484aA(ajr);
    }

    public Vector2fWrap getDistances() {
        return this.distances;
    }

    public void setDistances(Vector2fWrap aka) {
        this.distances.set(aka);
    }

    public RenderAsset cloneAsset() {
        return new SSourceElement(this);
    }
}
