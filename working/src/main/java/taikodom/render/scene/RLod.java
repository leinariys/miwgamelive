package taikodom.render.scene;

import game.geometry.aLH;
import taikodom.render.RenderContext;
import taikodom.render.helpers.RenderVisitor;
import taikodom.render.loader.RenderAsset;
import taikodom.render.raytrace.RayTraceSceneObjectInfoD;

/* compiled from: a */
public class RLod extends SceneObject {
    public SceneObject currentLodObject = null;
    public float distanceLod0;
    public float distanceLod1;
    public float distanceLod2;
    public SceneObject objectLod0 = null;
    public SceneObject objectLod1 = null;
    public SceneObject objectLod2 = null;
    public float squaredDistanceLod0;
    public float squaredDistanceLod1;
    public float squaredDistanceLod2;

    public RLod() {
    }

    public RLod(RLod rLod) {
        super(rLod);
        this.objectLod0 = (SceneObject) smartClone(rLod.objectLod0);
        this.objectLod1 = (SceneObject) smartClone(rLod.objectLod1);
        this.objectLod2 = (SceneObject) smartClone(rLod.objectLod2);
        setDistanceLod0(rLod.distanceLod0);
        setDistanceLod1(rLod.distanceLod1);
        setDistanceLod2(rLod.distanceLod2);
    }

    public void setDisposeWhenEmpty(boolean z) {
        super.setDisposeWhenEmpty(z);
        if (this.objectLod0 != null) {
            this.objectLod0.setDisposeWhenEmpty(z);
        }
        if (this.objectLod1 != null) {
            this.objectLod1.setDisposeWhenEmpty(z);
        }
        if (this.objectLod2 != null) {
            this.objectLod2.setDisposeWhenEmpty(z);
        }
    }

    public boolean isDisposed() {
        boolean z;
        if (this.objectLod0 == null || this.objectLod0.isDisposed()) {
            z = true;
        } else {
            z = false;
        }
        if (this.objectLod1 != null && !this.objectLod1.isDisposed()) {
            z = false;
        }
        if (this.objectLod2 != null && !this.objectLod2.isDisposed()) {
            z = false;
        }
        if (this.disposed || z) {
            return true;
        }
        return false;
    }

    public void checkEmpty() {
        boolean z = true;
        if (this.objectLod0 != null && !this.objectLod0.isDisposed()) {
            z = false;
        }
        if (this.objectLod1 != null && !this.objectLod1.isDisposed()) {
            z = false;
        }
        if (this.objectLod2 != null && !this.objectLod2.isDisposed()) {
            z = false;
        }
        if (this.objects.size() == 0 && this.disposeWhenEmpty && z) {
            dispose();
        }
    }

    public void onRemove() {
        super.onRemove();
        if (this.objectLod0 != null) {
            this.objectLod0.onRemove();
        }
        if (this.objectLod1 != null) {
            this.objectLod1.onRemove();
        }
        if (this.objectLod2 != null) {
            this.objectLod2.onRemove();
        }
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render || !super.render(renderContext, z)) {
            return false;
        }
        double g = this.globalTransform.mo17352g(renderContext.getCamera().getTransform()) / ((double) ((renderContext.getCamera().getTanHalfFovY() / 0.75f) / (1.0f + (renderContext.getSceneViewQuality().getLodQuality() * 0.5f))));
        SceneObject sceneObject = this.objectLod2;
        if (g < ((double) this.squaredDistanceLod0)) {
            sceneObject = this.objectLod0;
        } else if (g < ((double) this.squaredDistanceLod1)) {
            sceneObject = this.objectLod1;
        }
        if (sceneObject == null) {
            return true;
        }
        if (this.currentLodObject != sceneObject) {
            this.currentLodObject = sceneObject;
            this.currentLodObject.updateInternalGeometry(this);
        }
        this.currentLodObject.render(renderContext, z);
        return true;
    }

    public RenderAsset cloneAsset() {
        return new RLod(this);
    }

    public SceneObject getObjectLod0() {
        return this.objectLod0;
    }

    public void setObjectLod0(SceneObject sceneObject) {
        this.objectLod0 = sceneObject;
        updateInternalGeometry((SceneObject) null);
    }

    public SceneObject getObjectLod1() {
        return this.objectLod1;
    }

    public void setObjectLod1(SceneObject sceneObject) {
        this.objectLod1 = sceneObject;
        updateInternalGeometry((SceneObject) null);
    }

    public SceneObject getObjectLod2() {
        return this.objectLod2;
    }

    public void setObjectLod2(SceneObject sceneObject) {
        this.objectLod2 = sceneObject;
        updateInternalGeometry((SceneObject) null);
    }

    public float getDistanceLod0() {
        return this.distanceLod0;
    }

    public void setDistanceLod0(float f) {
        this.distanceLod0 = f;
        this.squaredDistanceLod0 = f * f;
    }

    public float getDistanceLod1() {
        return this.distanceLod1;
    }

    public void setDistanceLod1(float f) {
        this.distanceLod1 = f;
        this.squaredDistanceLod1 = f * f;
    }

    public float getDistanceLod2() {
        return this.distanceLod2;
    }

    public void setDistanceLod2(float f) {
        this.distanceLod2 = f;
        this.squaredDistanceLod2 = f * f;
    }

    public aLH getAABB() {
        return this.localAabb;
    }

    public aLH computeWorldSpaceAABB() {
        this.aabbWorldSpace.reset();
        if (this.currentLodObject != null) {
            this.currentLodObject.updateInternalGeometry(this);
            this.aabbWorldSpace.mo9868h(this.objectLod0.getAabbWorldSpace());
        } else {
            if (this.objectLod0 != null) {
                this.objectLod0.updateInternalGeometry(this);
                this.aabbWorldSpace.mo9868h(this.objectLod0.getAabbWorldSpace());
            }
            if (this.objectLod1 != null) {
                this.objectLod1.updateInternalGeometry(this);
                this.aabbWorldSpace.mo9868h(this.objectLod1.getAabbWorldSpace());
            }
            if (this.objectLod2 != null) {
                this.objectLod2.updateInternalGeometry(this);
                this.aabbWorldSpace.mo9868h(this.objectLod2.getAabbWorldSpace());
            }
        }
        addChildrenWorldSpaceAABB();
        this.aabbWSLenght = this.aabbWorldSpace.din().mo9491ax(this.aabbWorldSpace.dim());
        return this.aabbWorldSpace;
    }

    public aLH computeLocalSpaceAABB() {
        super.computeLocalSpaceAABB();
        if (this.objectLod0 != null) {
            this.localAabb.mo9868h(this.objectLod0.computeLocalSpaceAABB());
        }
        this.aabbLSLenght = this.localAabb.din().mo9491ax(this.localAabb.dim());
        return this.localAabb;
    }

    public RayTraceSceneObjectInfoD rayIntersects(RayTraceSceneObjectInfoD rayTraceSceneObjectInfoD, boolean z, boolean z2) {
        rayTraceSceneObjectInfoD.setChanged(false);
        super.rayIntersects(rayTraceSceneObjectInfoD, z, z2);
        if (!rayTraceSceneObjectInfoD.isIntersected() || !rayTraceSceneObjectInfoD.isChanged()) {
            SceneObject[] sceneObjectArr = {this.currentLodObject, this.objectLod2, this.objectLod1, this.objectLod0};
            int i = 0;
            while (true) {
                if (i >= sceneObjectArr.length) {
                    break;
                }
                if (sceneObjectArr[i] != null && (i <= 0 || sceneObjectArr[i] != this.currentLodObject)) {
                    rayTraceSceneObjectInfoD.setChanged(false);
                    sceneObjectArr[i].rayIntersects(rayTraceSceneObjectInfoD, z, z2);
                    if (rayTraceSceneObjectInfoD.isIntersected() && rayTraceSceneObjectInfoD.isChanged()) {
                        rayTraceSceneObjectInfoD.setIntersectedObject(this);
                        break;
                    }
                }
                i++;
            }
        }
        return rayTraceSceneObjectInfoD;
    }

    public boolean visitPreOrder(RenderVisitor renderVisitor) {
        if (!renderVisitor.visit(this)) {
            return false;
        }
        if (this.objectLod0 != null && !this.objectLod0.visitPreOrder(renderVisitor)) {
            return false;
        }
        if (this.objectLod1 != null && !this.objectLod1.visitPreOrder(renderVisitor)) {
            return false;
        }
        if (this.objectLod2 != null && !this.objectLod2.visitPreOrder(renderVisitor)) {
            return false;
        }
        for (SceneObject visitPreOrder : this.objects) {
            if (!visitPreOrder.visitPreOrder(renderVisitor)) {
                return false;
            }
        }
        return true;
    }

    public void releaseReferences() {
        super.releaseReferences();
        if (this.objectLod0 != null) {
            this.objectLod0.releaseReferences();
        }
        if (this.objectLod1 != null) {
            this.objectLod1.releaseReferences();
        }
        if (this.objectLod2 != null) {
            this.objectLod2.releaseReferences();
        }
        this.objectLod0 = null;
        this.objectLod1 = null;
        this.objectLod2 = null;
    }
}
