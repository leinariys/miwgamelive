package taikodom.render.scene;

/* compiled from: a */
public class Viewport {
    public int height;
    public int width;

    /* renamed from: x */
    public int x;

    /* renamed from: y */
    public int y;

    public Viewport() {
    }

    public Viewport(Viewport viewport) {
        set(viewport);
    }

    public Viewport(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public void set(Viewport viewport) {
        this.x = viewport.x;
        this.y = viewport.y;
        this.width = viewport.width;
        this.height = viewport.height;
    }
}
