package taikodom.render.scene;

import com.hoplon.geometry.Vec3f;
import taikodom.render.SoundBuffer;
import taikodom.render.StepContext;

/* compiled from: a */
public abstract class SoundSource {
    public static int numSoundSources;

    public static int getNumSoundSources() {
        return numSoundSources;
    }

    public abstract boolean play(int i);

    public abstract void setPosition(Vec3f vec3f);

    public abstract void setVelocity(Vec3f vec3f);

    public boolean setBuffer(SoundBuffer soundBuffer) {
        return false;
    }

    public SoundBuffer getBuffer() {
        return null;
    }

    public boolean isValid() {
        return false;
    }

    public boolean create() {
        return false;
    }

    public boolean setLoop(int i) {
        return false;
    }

    public boolean stop() {
        return false;
    }

    public boolean resume() {
        return false;
    }

    public void setPosition(float f, float f2, float f3) {
    }

    public boolean setGain(float f) {
        return false;
    }

    public boolean setPitch(float f) {
        return false;
    }

    public boolean isPlaying() {
        return false;
    }

    public boolean fadeOut(float f) {
        return false;
    }

    public boolean setMinMaxDistance(float f, float f2) {
        return false;
    }

    public int getTotalMilesSounds() {
        return 0;
    }

    public int getSamplePositionMs() {
        return 0;
    }

    public void setSamplePositionMs(int i) {
    }

    public boolean releaseSample() {
        return false;
    }

    public void releaseReferences() {
    }

    public int step(StepContext stepContext) {
        return 0;
    }

    public float getTimeLenght() {
        return 0.0f;
    }
}
