package taikodom.render.scene;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.*;
import org.mozilla.javascript.ScriptRuntime;
import taikodom.render.RenderContext;
import taikodom.render.enums.BillboardAlignment;
import taikodom.render.enums.CullFace;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Billboard;
import taikodom.render.primitives.OcclusionQuery;
import taikodom.render.raytrace.RayTraceSceneObjectInfoD;
import taikodom.render.shaders.ShaderPass;

import javax.vecmath.Vector3d;

/* compiled from: a */
public class RBillboard extends RenderObject {
    public static final Billboard billboard = new Billboard();
    private static final int MIN_SCREEN_SIZE = 3;
    public final Vec3f alignmentDirection = new Vec3f(0.0f, 1.0f, 0.0f);
    public BillboardAlignment alignment = BillboardAlignment.VIEW_ALIGNED;
    public double maxSizeGlobal = ScriptRuntime.NaN;
    public Vector2fWrap size = new Vector2fWrap();
    private boolean disableDepthWrite = false;
    private TransformWrap localTransform = new TransformWrap();

    public RBillboard() {
        this.size.set(128.0f, 128.0f);
    }

    public RBillboard(RBillboard rBillboard) {
        super(rBillboard);
        this.size.set(rBillboard.size);
        this.alignment = rBillboard.alignment;
    }

    public Vector2fWrap getSize() {
        return this.size;
    }

    public void setSize(Vector2fWrap aka) {
        this.size.set(aka);
    }

    public void setSize(float f, float f2) {
        this.size.set(f, f2);
    }

    public Billboard getBillboard() {
        return billboard;
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render || !super.render(renderContext, z)) {
            return false;
        }
        doAlignment(renderContext);
        float sizeOnScreen = renderContext.getSizeOnScreen(this.globalTransform.position, this.maxSizeGlobal);
        if (sizeOnScreen >= 3.0f || !renderContext.isCullOutSmallObjects() || renderContext.getCamera().isOrthogonal()) {
            ShaderPass pass = this.material.getShader().getPass(0);
            if (pass.isCullFaceEnabled() && !renderContext.getCamera().isOrthogonal() && this.alignment == BillboardAlignment.USER_ALIGNED) {
                renderContext.vec3fTemp0.sub(renderContext.getCamera().getGlobalTransform().position, this.globalTransform.position);
                this.globalTransform.getZ(renderContext.vec3fTemp1);
                double dot = (double) renderContext.vec3fTemp1.dot(renderContext.vec3fTemp0);
                CullFace cullFace = pass.getCullFace();
                if (cullFace.equals(CullFace.BACK) && dot < ScriptRuntime.NaN) {
                    return true;
                }
                if (cullFace.equals(CullFace.FRONT) && dot > ScriptRuntime.NaN) {
                    return true;
                }
            }
            if (!z || renderContext.getFrustum().mo1160a(this.aabbWorldSpace)) {
                for (int i = 0; i < this.material.textureCount(); i++) {
                    if (this.material.getTexture(i) != null) {
                        this.material.getTexture(i).addArea(sizeOnScreen);
                    }
                }
                renderContext.addPrimitive(this.material, billboard, this.localTransform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
                renderContext.incBillboardsRendered();
            }
        }
        return true;
    }

    private void doAlignment(RenderContext renderContext) {
        if (this.alignment == BillboardAlignment.SCREEN_ALIGNED) {
            renderContext.getCamera().getTransform().getX(renderContext.vec3fTemp0);
            renderContext.vec3fTemp0.scale(this.size.x);
            renderContext.getCamera().getTransform().getY(renderContext.vec3fTemp1);
            renderContext.vec3fTemp1.scale(this.size.y);
            renderContext.getCamera().getTransform().getZ(renderContext.vec3fTemp2);
            this.localTransform.setX(renderContext.vec3fTemp0);
            this.localTransform.setY(renderContext.vec3fTemp1);
            this.localTransform.setZ(renderContext.vec3fTemp2);
            this.transform.getTranslation(renderContext.vec3dTemp0);
            this.localTransform.setTranslation(renderContext.vec3dTemp0);
        } else if (this.alignment == BillboardAlignment.AXIS_ALIGNED) {
            this.globalTransform.multiply3x3(this.alignmentDirection, renderContext.vec3fTemp0);
            renderContext.getCamera().getTransform().getTranslation(renderContext.vec3dTemp1);
            this.globalTransform.getTranslation(renderContext.vec3dTemp2);
            renderContext.vec3dTemp1.sub(renderContext.vec3dTemp2);
            renderContext.vec3dTemp1.dfX();
            renderContext.vec3fTemp1.set(renderContext.vec3dTemp1);
            renderContext.vec3fTemp2.cross(renderContext.vec3fTemp1, renderContext.vec3fTemp0);
            renderContext.vec3fTemp2.dfT();
            renderContext.vec3fTemp2.dfP();
            renderContext.vec3fTemp1.cross(renderContext.vec3fTemp0, renderContext.vec3fTemp2);
            renderContext.vec3fTemp1.negate();
            renderContext.vec3fTemp2.scale(this.size.x);
            renderContext.vec3fTemp0.scale(this.size.y);
            this.localTransform.setX(renderContext.vec3fTemp2);
            this.localTransform.setY(renderContext.vec3fTemp0);
            this.localTransform.setZ(renderContext.vec3fTemp1);
            this.globalTransform.getTranslation(renderContext.vec3dTemp0);
            this.localTransform.setTranslation(renderContext.vec3dTemp0);
        } else if (this.alignment == BillboardAlignment.VIEW_ALIGNED) {
            renderContext.getCamera().getTransform().getTranslation(renderContext.vec3dTemp1);
            this.globalTransform.getTranslation(renderContext.vec3dTemp2);
            renderContext.vec3dTemp1.sub(renderContext.vec3dTemp2);
            renderContext.vec3dTemp1.dfX();
            renderContext.vec3fTemp1.set(renderContext.vec3dTemp1);
            renderContext.getCamera().getTransform().getY(renderContext.vec3fTemp0);
            renderContext.vec3fTemp2.cross(renderContext.vec3fTemp1, renderContext.vec3fTemp0);
            renderContext.vec3fTemp2.dfP();
            renderContext.vec3fTemp0.scale(this.size.y);
            renderContext.vec3fTemp2.scale(-this.size.x);
            this.localTransform.setX(renderContext.vec3fTemp2);
            this.localTransform.setY(renderContext.vec3fTemp0);
            this.localTransform.setZ(renderContext.vec3fTemp1);
            this.localTransform.setTranslation(this.globalTransform.position);
        }
    }

    public RenderAsset cloneAsset() {
        return new RBillboard(this);
    }

    public BillboardAlignment getAlignment() {
        return this.alignment;
    }

    public void setAlignment(BillboardAlignment billboardAlignment) {
        this.alignment = billboardAlignment;
    }

    public Vec3f getAlignmentDirection() {
        return this.alignmentDirection;
    }

    public void setAlignmentDirection(Vec3f vec3f) {
        this.alignmentDirection.set(vec3f);
    }

    public OcclusionQuery getOcclusionQuery() {
        return this.occlusionQuery;
    }

    public aLH computeWorldSpaceAABB() {
        float f = this.size.x;
        if (this.size.y > f) {
            f = this.size.y;
        }
        this.aabbWorldSpace.mo9866g(-f, -f, -f, f, f, f);
        this.aabbWorldSpace.mo9834a(this.globalTransform, this.aabbWorldSpace);
        addChildrenWorldSpaceAABB();
        this.aabbWSLenght = this.aabbWorldSpace.din().mo9491ax(this.aabbWorldSpace.dim());
        return this.aabbWorldSpace;
    }

    public aLH computeLocalSpaceAABB() {
        super.computeLocalSpaceAABB();
        this.localAabb.mo9826C((double) (-this.size.x), (double) (-this.size.y), ScriptRuntime.NaN);
        this.localAabb.mo9826C((double) this.size.x, (double) this.size.y, ScriptRuntime.NaN);
        return this.localAabb;
    }

    public RayTraceSceneObjectInfoD rayIntersects(RayTraceSceneObjectInfoD rayTraceSceneObjectInfoD, boolean z, boolean z2) {
        rayTraceSceneObjectInfoD.setChanged(false);
        super.rayIntersects(rayTraceSceneObjectInfoD, z, z2);
        if ((!rayTraceSceneObjectInfoD.isIntersected() || !rayTraceSceneObjectInfoD.isChanged()) && billboard != null) {
            Vec3d ajr = new Vec3d();
            Vec3d ajr2 = new Vec3d();
            ajr.sub(rayTraceSceneObjectInfoD.getStartPos(), this.globalTransform.position);
            ajr2.sub(rayTraceSceneObjectInfoD.getEndPos(), this.globalTransform.position);
            this.globalTransform.orientation.mo13944a((Vector3d) ajr);
            this.globalTransform.orientation.mo13944a((Vector3d) ajr2);
            C2567gu rayIntersects = billboard.rayIntersects(ajr.dfV(), ajr2.dfV());
            if (rayIntersects.isIntersected() && (!z || ((double) rayIntersects.mo19142vM()) < rayTraceSceneObjectInfoD.getParamIntersection())) {
                rayTraceSceneObjectInfoD.set(rayIntersects);
                rayTraceSceneObjectInfoD.setChanged(true);
                rayTraceSceneObjectInfoD.setIntersectedObject(this);
            }
        }
        return rayTraceSceneObjectInfoD;
    }

    public void updateInternalGeometry(SceneObject sceneObject) {
        super.updateInternalGeometry(sceneObject);
        double gk = this.globalTransform.mo17361gk() * ((double) this.size.x);
        double gm = this.globalTransform.mo17362gm() * ((double) this.size.y);
        this.maxSizeGlobal = gk;
        if (gm > gk) {
            this.maxSizeGlobal = gm;
        }
        if (this.alignment == BillboardAlignment.USER_ALIGNED) {
            this.localTransform.mo17344b(this.globalTransform);
            this.localTransform.mo17324B(this.size.x);
            this.localTransform.mo17325C(this.size.y);
        }
    }

    public boolean getDisableDepthWrite() {
        return this.disableDepthWrite;
    }

    public void setDisableDepthWrite(boolean z) {
        this.disableDepthWrite = z;
    }

    public void validate(SceneLoader sceneLoader) {
        super.validate(sceneLoader);
        if (this.material.getShader() != null) {
            return;
        }
        if (this.disableDepthWrite) {
            this.material.setShader(sceneLoader.getDefaultBillboardShaderNoDepth());
        } else {
            this.material.setShader(sceneLoader.getDefaultBillboardShader());
        }
    }

    public aLH getTransformedAABB() {
        float f = this.size.x;
        if (this.size.y > f) {
            f = this.size.y;
        }
        float f2 = f * 0.5f;
        new BoundingBox(-f2, -f2, -f2, f2, f2, f2).mo23421a(getGlobalTransform(), this.aabbWorldSpace);
        return this.aabbWorldSpace;
    }
}
