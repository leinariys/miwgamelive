package taikodom.render.scene;

/* compiled from: a */
public class VolumeInterpolator {
    float currentTime = 0.0f;
    float finalVal = 1.0f;
    float initVal = 0.0f;
    SMusic music = null;
    float result = 0.0f;
    float totalTime = 1.0f;

    VolumeInterpolator() {
    }

    /* access modifiers changed from: package-private */
    public boolean step(float f) {
        this.result = ((this.currentTime / this.totalTime) * (this.finalVal - this.initVal)) + this.initVal;
        this.currentTime += f;
        if (this.currentTime <= this.totalTime) {
            return true;
        }
        this.result = this.finalVal;
        return false;
    }

    /* access modifiers changed from: package-private */
    public void init(float f, float f2, float f3, SMusic sMusic) {
        this.music = sMusic;
        this.initVal = f;
        this.finalVal = f2;
        this.totalTime = f3;
        this.result = f;
        this.currentTime = 0.0f;
        if (this.totalTime <= 0.0f) {
            this.totalTime = 1.0E-4f;
        }
    }

    /* access modifiers changed from: package-private */
    public float getValue() {
        return this.result;
    }

    /* access modifiers changed from: package-private */
    public SMusic getMusic() {
        return this.music;
    }
}
