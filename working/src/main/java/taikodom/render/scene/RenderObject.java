package taikodom.render.scene;

import com.hoplon.geometry.Color;
import taikodom.render.NotExported;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.OcclusionQuery;
import taikodom.render.primitives.RenderAreaInfo;
import taikodom.render.shaders.Shader;
import taikodom.render.textures.Material;

/* compiled from: a */
public class RenderObject extends SceneObject {
    public final RenderAreaInfo areaInfo = new RenderAreaInfo();
    public final Color emissiveColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    public final Color primitiveColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    public long lastFrameRendered;
    public Material material;
    public OcclusionQuery occlusionQuery;
    public boolean selected;
    public boolean selectedRecursive;

    public RenderObject() {
    }

    public RenderObject(RenderObject renderObject) {
        super(renderObject);
        this.material = (Material) smartClone(renderObject.material);
        this.primitiveColor.set(renderObject.primitiveColor);
        this.selected = renderObject.selected;
        this.rayTraceable = renderObject.rayTraceable;
        this.lastFrameRendered = renderObject.lastFrameRendered;
        this.selectedRecursive = renderObject.selectedRecursive;
    }

    public RenderAsset cloneAsset() {
        return new RenderObject(this);
    }

    public Material getMaterial() {
        return this.material;
    }

    public void setMaterial(Material material2) {
        this.material = material2;
    }

    public Color getPrimitiveColor() {
        return this.primitiveColor;
    }

    public void setPrimitiveColor(Color color) {
        this.primitiveColor.set(color);
    }

    public void setPrimitiveColor(float f, float f2, float f3, float f4) {
        this.primitiveColor.set(f, f2, f3, f4);
    }

    public boolean isSelected() {
        return this.selected;
    }

    public void setSelected(boolean z) {
        this.selected = z;
    }

    public boolean isOcclusionQueryEnabled() {
        return this.occlusionQuery != null;
    }

    public void setOcclusionQueryEnabled(boolean z) {
        if (z && this.occlusionQuery == null) {
            this.occlusionQuery = new OcclusionQuery();
        }
    }

    @NotExported
    public long getLastFrameRendered() {
        return this.lastFrameRendered;
    }

    @NotExported
    public void setLastFrameRendered(long j) {
        this.lastFrameRendered = j;
    }

    public boolean isSelectedRecursive() {
        return this.selectedRecursive;
    }

    public void setSelectedRecursive(boolean z) {
        this.selectedRecursive = z;
    }

    @Deprecated
    public Shader getShader() {
        return this.material.getShader();
    }

    @Deprecated
    public void setShader(Shader shader) {
        if (this.material != null) {
            this.material.setShader(shader);
        }
    }

    public RenderAreaInfo getRenderAreaInfo() {
        return this.areaInfo;
    }

    public void validate(SceneLoader sceneLoader) {
        super.validate(sceneLoader);
        if (this.material == null) {
            this.material = sceneLoader.getDefaultMaterial();
        }
    }

    public Color getEmissiveColor() {
        return this.emissiveColor;
    }

    public void setEmissiveColor(Color color) {
        this.emissiveColor.set(this.primitiveColor);
    }
}
