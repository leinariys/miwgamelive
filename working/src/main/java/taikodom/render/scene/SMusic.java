package taikodom.render.scene;

import taikodom.render.MilesSoundStream;
import taikodom.render.StepContext;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public class SMusic extends SoundObject {
    MilesSoundStream buffer;
    float gain;
    boolean isPaused;
    int loopCount;
    float loopDelay;
    float pitch;
    boolean shouldPlay;
    private float accum;
    private boolean onHold;

    public SMusic() {
        this.accum = 0.0f;
        this.onHold = false;
        this.buffer = null;
        this.loopCount = 0;
        this.loopDelay = -1.0f;
        this.gain = 1.0f;
        this.pitch = 1.0f;
        this.isPaused = false;
        this.shouldPlay = false;
    }

    public SMusic(SMusic sMusic) {
        this.accum = 0.0f;
        this.onHold = false;
        this.buffer = null;
        this.loopCount = sMusic.loopCount;
        this.gain = sMusic.gain;
        this.pitch = sMusic.pitch;
        this.isPaused = false;
        setBuffer(sMusic.buffer);
    }

    public void releaseReferences() {
        super.releaseReferences();
        if (this.buffer != null) {
            this.buffer.releaseReferences();
            this.buffer = null;
        }
    }

    public void dispose() {
        stop();
        this.disposed = true;
    }

    public int getLoopCount() {
        return this.loopCount;
    }

    public void setLoopCount(int i) {
        if (i == 0) {
            this.loopDelay = -1.0f;
        }
        this.loopCount = i;
        if (this.buffer != null) {
            this.buffer.setLoopCount(this.loopCount);
        }
    }

    public float getLoopDelay() {
        return this.loopDelay;
    }

    public void setLoopDelay(float f) {
        setLoopCount(1);
        this.loopDelay = f;
    }

    public float getGain() {
        return this.gain;
    }

    public void setGain(float f) {
        this.gain = f;
        this.buffer.setGain(f);
    }

    public boolean play() {
        if (this.buffer == null) {
            return false;
        }
        this.shouldPlay = true;
        if (this.isPaused) {
            this.isPaused = false;
            return this.buffer.play();
        } else if (!this.buffer.play(this.loopCount)) {
            return false;
        } else {
            this.buffer.setGain(this.gain);
            return true;
        }
    }

    public boolean stop() {
        this.isPaused = false;
        this.shouldPlay = false;
        this.onHold = false;
        this.accum = 0.0f;
        if (this.buffer != null) {
            this.buffer.stop();
        }
        return false;
    }

    public boolean pause() {
        this.isPaused = true;
        if (this.buffer == null) {
            return false;
        }
        this.buffer.pause();
        return true;
    }

    public boolean isPlaying() {
        if (this.buffer != null) {
            return this.buffer.isPlaying();
        }
        return false;
    }

    public boolean fadeOut(float f) {
        this.onHold = false;
        this.accum = 0.0f;
        this.shouldPlay = false;
        if (this.buffer != null) {
            return this.buffer.fadeOut(f);
        }
        return false;
    }

    public boolean fadeIn(float f) {
        play();
        if (this.buffer != null) {
            return this.buffer.fadeIn(f);
        }
        return false;
    }

    public MilesSoundStream getBuffer() {
        return this.buffer;
    }

    public void setBuffer(MilesSoundStream milesSoundStream) {
        this.buffer = milesSoundStream;
    }

    public boolean isAlive() {
        return isPlaying() || this.isPaused;
    }

    public int step(StepContext stepContext) {
        if (this.buffer != null && !this.isPaused && !this.onHold && this.buffer.step(stepContext) != 0) {
            if (this.loopDelay == -1.0f || !this.shouldPlay) {
                return 1;
            }
            if (this.shouldPlay) {
                this.accum = 0.0f;
                this.onHold = true;
                return 0;
            }
        }
        if (!this.onHold || !this.shouldPlay) {
            return 0;
        }
        this.accum += stepContext.getDeltaTime();
        if (this.accum < this.loopDelay) {
            return 0;
        }
        this.accum = 0.0f;
        this.onHold = false;
        play();
        return 0;
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public int getSamplePositionMs() {
        if (this.buffer == null) {
            return 0;
        }
        return this.buffer.getSamplePositionMs();
    }

    public void setSamplePositionMs(int i) {
        if (this.buffer != null) {
            this.buffer.setSamplePositionMs(i);
        }
    }
}
