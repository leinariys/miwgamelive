package taikodom.render;

import com.hoplon.geometry.Vec3f;
import logic.render.miles.AdapterMilesBridgeJNI;
import logic.render.miles.C1467Vb;
import logic.render.miles.C5297aDx;
import lombok.extern.slf4j.Slf4j;
import org.lwjgl.BufferUtils;
import taikodom.render.scene.SoundSource;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/* compiled from: a */
@Slf4j
public class MilesSoundSource extends SoundSource {
    public static final long SMP_DONE = 2;
    public static final long SMP_PLAYING = 4;
    public static final long SMP_STOPPED = 8;
    public C1467Vb milesSample;
    public FloatBuffer tempFloat = BufferUtils.createFloatBuffer(1);
    public IntBuffer tempInt = BufferUtils.createIntBuffer(1);
    boolean isFadingOut;
    int loop;
    boolean outRanged;
    MilesSoundBuffer soundBuffer;
    int xOffset;
    int yOffset;

    public void finalize() {
        releaseReferences();
    }

    public void releaseReferences() {
        releaseSample();
    }

    public boolean setBuffer(SoundBuffer soundBuffer2) {
        this.soundBuffer = (MilesSoundBuffer) soundBuffer2;
        return true;
    }

    public SoundBuffer getBuffer() {
        return this.soundBuffer;
    }

    private boolean shouldPlay() {
        if (((float) numSoundSources) > ((float) SoundMiles.getInstance().getMaxSourceNumber()) * 0.85f) {
            if (this.soundBuffer == null || this.soundBuffer.getTimeLength() >= 1.0f) {
                return false;
            }
            return true;
        } else if (((float) numSoundSources) <= ((float) SoundMiles.getInstance().getMaxSourceNumber()) * 0.7f) {
            return true;
        } else {
            if (this.soundBuffer == null || this.soundBuffer.getTimeLength() >= 2.5f) {
                return false;
            }
            return true;
        }
    }

    public boolean create() {
        if (this.soundBuffer == null || numSoundSources >= SoundMiles.getInstance().getMaxSourceNumber() || !shouldPlay()) {
            return false;
        }
        if (this.milesSample == null) {
            try {
                this.milesSample = AdapterMilesBridgeJNI.m21900h(SoundMiles.getInstance().getDig());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (this.milesSample == null) {
            return false;
        }
        numSoundSources++;
        if (AdapterMilesBridgeJNI.m21674a(this.milesSample, this.soundBuffer.getExtension(), this.soundBuffer.getData(), (long) this.soundBuffer.getLength(), 0) == 0) {
            AdapterMilesBridgeJNI.m21665a(this.milesSample, 0, 0);
            AdapterMilesBridgeJNI.m21757a(this.milesSample, this.soundBuffer.getData(), (long) this.soundBuffer.getLength());
            AdapterMilesBridgeJNI.m21746a(this.milesSample, 44100);
        }
        return true;
    }

    public boolean play(int i) {
        if (this.milesSample == null) {
            log.debug("trying to play a sound without create it before.");
            return false;
        }
        AdapterMilesBridgeJNI.m21816b(this.milesSample, i);
        this.loop = i;
        if (!isOutRange()) {
            AdapterMilesBridgeJNI.m21888f(this.milesSample);
        }
        return true;
    }

    public boolean setLoop(int i) {
        if (this.milesSample == null) {
            return false;
        }
        AdapterMilesBridgeJNI.m21816b(this.milesSample, i);
        return true;
    }

    public boolean stop() {
        if (this.milesSample == null) {
            return false;
        }
        AdapterMilesBridgeJNI.m21909i(this.milesSample);
        return true;
    }

    public boolean fadeOut(float f) {
        if (this.milesSample == null || AdapterMilesBridgeJNI.AIL_find_filter("Volume Ramp Filter", this.tempInt) == 0) {
            return false;
        }
        this.isFadingOut = true;
        int i = this.tempInt.get(0);
        AdapterMilesBridgeJNI.m21705a(this.milesSample, C5297aDx.hBQ, (long) i);
        AdapterMilesBridgeJNI.m21705a(this.milesSample, C5297aDx.hBQ, (long) i);
        AdapterMilesBridgeJNI.m21758a(this.milesSample, (Buffer) this.tempFloat, (Buffer) null);
        AdapterMilesBridgeJNI.m21668a(this.milesSample, C5297aDx.hBQ, this.tempFloat.get(0));
        AdapterMilesBridgeJNI.m21800b(this.milesSample, C5297aDx.hBQ, 1000.0f * f);
        AdapterMilesBridgeJNI.m21668a(this.milesSample, C5297aDx.hBQ, 0.0f);
        return true;
    }

    public boolean resume() {
        if (this.milesSample == null) {
            return false;
        }
        AdapterMilesBridgeJNI.m21901h(this.milesSample);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean isOutRange() {
        return false;
    }

    public void ValidadePosition() {
    }

    public void setPosition(float f, float f2, float f3) {
        AdapterMilesBridgeJNI.m21814b(this.milesSample, f, f2, f3);
    }

    public void setPosition(Vec3f vec3f) {
        setPosition(vec3f.x, vec3f.y, vec3f.z);
    }

    public void setVelocity(Vec3f vec3f) {
        if (this.milesSample != null) {
            AdapterMilesBridgeJNI.m21847c(this.milesSample, vec3f.x, vec3f.y, vec3f.z);
        }
    }

    public boolean setGain(float f) {
        if (this.milesSample == null) {
            return false;
        }
        AdapterMilesBridgeJNI.m21813b(this.milesSample, f, f);
        return true;
    }

    public boolean setMinMaxDistance(float f, float f2) {
        if (this.milesSample == null) {
            return false;
        }
        AdapterMilesBridgeJNI.m21745a(this.milesSample, f2, f, 1);
        return true;
    }

    public boolean setPitch(float f) {
        if (this.milesSample == null || this.soundBuffer == null) {
            return false;
        }
        AdapterMilesBridgeJNI.m21746a(this.milesSample, (int) (((float) this.soundBuffer.getSampleRate()) * f));
        return true;
    }

    public boolean isPlaying() {
        if (this.milesSample != null && AdapterMilesBridgeJNI.m21912j(this.milesSample) == 4) {
            return true;
        }
        return false;
    }

    public int step(StepContext stepContext) {
        if (this.milesSample == null) {
            return 1;
        }
        if (((long) ((int) AdapterMilesBridgeJNI.m21912j(this.milesSample))) == 2) {
            releaseSample();
            return 1;
        }
        if (this.isFadingOut) {
            this.tempFloat.put(0, 1.0f);
            AdapterMilesBridgeJNI.m21670a(this.milesSample, C5297aDx.hBQ, "Ramp At", (Buffer) this.tempFloat, (Buffer) null, (Buffer) null);
            if (this.tempFloat.get(0) <= 0.0f) {
                releaseSample();
                this.isFadingOut = false;
                return 1;
            }
        }
        return 0;
    }

    public int getSamplePositionMs() {
        if (this.milesSample == null) {
            return 0;
        }
        AdapterMilesBridgeJNI.m21821b(this.milesSample, (Buffer) null, (Buffer) this.tempInt);
        return this.tempInt.get(0);
    }

    public void setSamplePositionMs(int i) {
        if (this.milesSample != null) {
            AdapterMilesBridgeJNI.m21865d(this.milesSample, i);
        }
    }

    public boolean releaseSample() {
        if (this.milesSample == null) {
            return false;
        }
        AdapterMilesBridgeJNI.m21909i(this.milesSample);
        AdapterMilesBridgeJNI.m21877e(this.milesSample);
        this.milesSample = null;
        numSoundSources--;
        return true;
    }

    public void setMilesSample(C1467Vb vb) {
        this.milesSample = vb;
    }

    public float getTimeLenght() {
        if (this.soundBuffer != null) {
            return this.soundBuffer.getTimeLength();
        }
        return 0.0f;
    }
}
