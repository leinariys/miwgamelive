package taikodom.render.loader;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import game.geometry.Vector2fWrap;
import lombok.extern.slf4j.Slf4j;
import taikodom.render.NotExported;
import taikodom.render.loader.descriptors.ClassDescriptor;
import taikodom.render.loader.descriptors.PropertyDescriptor;
import util.Syst;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/* compiled from: a */
@Slf4j
public class ReflectiveClassDescriptor extends ClassDescriptor {
    static final IParser colorParser = new ColorParser();
    static final IParser matrix4fparser = new Matrix4fParser();
    static final IParser transformParser = new TransformParser();
    static final IParser vec2Parser = new Vec2Parser();
    static final IParser vec3dParser = new Vec3dParser();
    static final IParser vec3fParser = new Vec3fParser();
    /* access modifiers changed from: private */
    private final Class<?> clazz;
    private final String name;
    private Map<String, PropertyDescriptor> props = new HashMap();

    public ReflectiveClassDescriptor(String str, Class<?> cls) {
        this.name = str;
        this.clazz = cls;
        getClassMethods(cls);
    }

    private static boolean isMethodValid(Method method) {
        if (method.getName().length() < 4 || method.getAnnotation(NotExported.class) != null) {
            return false;
        }
        return true;
    }

    private void getClassMethods(Class<?> cls) {
        String substring;
        String u;
        for (Method method : cls.getMethods()) {
            if (isMethodValid(method)) {
                Class<?> returnType = method.getReturnType();
                if ((method.getName().startsWith("get") || method.getName().startsWith("is")) && method.getParameterTypes().length == 0 && !returnType.equals(Void.TYPE)) {
                    try {
                        if (method.getName().startsWith("get")) {
                            substring = method.getName().substring(3);
                            u = Syst.m15909u(substring);
                        } else {
                            substring = method.getName().substring(2);
                            u = Syst.m15909u(substring);
                        }
                        Method method2 = cls.getMethod("set" + substring, new Class[]{returnType});
                        addPD(false, u, returnType, method, (Method) null);
                        addPD(false, u, method2.getParameterTypes()[0], (Method) null, method2);
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e2) {
                    }
                } else if (method.getName().startsWith("get") && method.getParameterTypes().length == 1 && !returnType.equals(Void.TYPE)) {
                    String substring2 = method.getName().substring(3);
                    String u2 = Syst.m15909u(substring2);
                    try {
                        Method method3 = cls.getMethod("set" + substring2, new Class[]{Integer.TYPE, returnType});
                        addPD(true, String.valueOf(u2) + "[]", returnType, method, (Method) null);
                        addPD(true, String.valueOf(u2) + "[]", method3.getParameterTypes()[1], (Method) null, method3);
                    } catch (SecurityException e3) {
                        e3.printStackTrace();
                    } catch (NoSuchMethodException e4) {
                    }
                }
            }
        }
    }

    private ReflectivePropertyDescriptor addPD(boolean z, String str, Class<?> cls, Method method, Method method2) {
        ReflectivePropertyDescriptor reflectivePropertyDescriptor = (ReflectivePropertyDescriptor) this.props.get(str);
        if (reflectivePropertyDescriptor == null) {
            reflectivePropertyDescriptor = new ReflectivePropertyDescriptor(parserFor(cls), cls.getSimpleName(), str, z, RenderAsset.class.isAssignableFrom(cls));
            this.props.put(str, reflectivePropertyDescriptor);
        }
        if (method2 != null) {
            reflectivePropertyDescriptor.setSetter(method2);
        }
        if (method != null) {
            reflectivePropertyDescriptor.setGetter(method);
        }
        return reflectivePropertyDescriptor;
    }

    private IParser parserFor(Class<?> cls) {
        if (cls.isEnum()) {
            return new C5145a(cls);
        }
        if (cls == String.class) {
            return new C5146b();
        }
        if (cls == Vec3f.class) {
            return vec3fParser;
        }
        if (cls == Vector2fWrap.class) {
            return vec2Parser;
        }
        if (cls == TransformWrap.class) {
            return transformParser;
        }
        if (cls == Matrix4fWrap.class) {
            return matrix4fparser;
        }
        if (cls == Color.class) {
            return colorParser;
        }
        if (cls == Vec3d.class) {
            return vec3dParser;
        }
        return C2288de.m29163c(cls);
    }

    public String getName() {
        return this.name;
    }

    public Object createInstance() {
        try {
            return this.clazz.newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException(e2);
        }
    }

    public PropertyDescriptor getProperty(String str) {
        return this.props.get(str);
    }

    public Collection<PropertyDescriptor> getProperties() {
        return this.props.values();
    }

    /* renamed from: taikodom.render.loader.ReflectiveClassDescriptor$a */
    class C5145a extends C2288de {
        private final /* synthetic */ Class exy;

        C5145a(Class cls) {
            this.exy = cls;
        }

        /* renamed from: O */
        public Object mo2718O(String str) {
            try {
                return this.exy.getField(str).get((Object) null);
            } catch (Exception e) {
                Exception exc = e;
                int parseInt = Integer.parseInt(str);
                for (Enum enumR : (Enum[]) this.exy.getEnumConstants()) {
                    if (enumR.ordinal() == parseInt) {
                        return enumR;
                    }
                }
                ReflectiveClassDescriptor.log.error("Unable to find a enum value at " + this.exy.getName() + ", corresponding to " + str);
                throw new RuntimeException(exc);
            }
        }
    }

    /* renamed from: taikodom.render.loader.ReflectiveClassDescriptor$b */
    /* compiled from: a */
    class C5146b extends C2288de {
        C5146b() {
        }

        /* renamed from: O */
        public Object mo2718O(String str) {
            return str;
        }
    }
}
