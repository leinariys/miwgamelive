package taikodom.render.loader.descriptors;

import lombok.extern.slf4j.Slf4j;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;

import java.util.List;

/* compiled from: a */
@Slf4j
public class ObjectUnassignedProperty extends UnassignedPropertyBase {

    public List<UnassignedPropertyBase> subProperties;

    public ObjectUnassignedProperty(PropertyDescriptor propertyDescriptor, String str, boolean z, int i, RenderAsset renderAsset, boolean z2) {
        super(propertyDescriptor, str, i);
    }

    public boolean doAssign(SceneLoader sceneLoader, Object obj) {
        return this.descriptor.set(obj, this.index, sceneLoader.getAsset(this.value));
    }
}
