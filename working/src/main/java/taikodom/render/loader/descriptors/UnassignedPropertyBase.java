package taikodom.render.loader.descriptors;

import taikodom.render.loader.SceneLoader;

/* compiled from: a */
public class UnassignedPropertyBase extends UnassignedProperty {
    public final PropertyDescriptor descriptor;
    public final int index;
    public final String value;

    public UnassignedPropertyBase(PropertyDescriptor propertyDescriptor) {
        this(propertyDescriptor, (String) null, -1);
    }

    public UnassignedPropertyBase(PropertyDescriptor propertyDescriptor, int i) {
        this(propertyDescriptor, (String) null, i);
    }

    public UnassignedPropertyBase(PropertyDescriptor propertyDescriptor, String str) {
        this(propertyDescriptor, str, -1);
    }

    public UnassignedPropertyBase(PropertyDescriptor propertyDescriptor, String str, int i) {
        this.descriptor = propertyDescriptor;
        this.value = str;
        this.index = i;
    }

    public boolean doAssign(SceneLoader sceneLoader, Object obj) {
        return this.descriptor.setAsString(obj, this.value);
    }

    public boolean doAssignToNewInstance(SceneLoader sceneLoader, Object obj) {
        return false;
    }

    public String getName() {
        return this.descriptor.getName();
    }

    public String getValue() {
        return this.value;
    }

    public String toString() {
        return String.valueOf(getClass().getSimpleName()) + ": " + this.descriptor.getName() + "=" + this.value;
    }
}
