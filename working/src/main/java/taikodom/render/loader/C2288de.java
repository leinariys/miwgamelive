package taikodom.render.loader;

import org.mozilla.javascript.ScriptRuntime;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.de */
/* compiled from: a */
public class C2288de implements IParser {

    /* renamed from: yj */
    private static Map<Class<?>, C2288de> f6575yj = new HashMap();

    static {
        f6575yj.put(Boolean.TYPE, new C0623Ip());
        f6575yj.put(Integer.TYPE, new C0625Ir());
        f6575yj.put(Byte.TYPE, new C0618Il());
        f6575yj.put(Short.TYPE, new C0620In());
        f6575yj.put(Long.TYPE, new C0579IA());
        f6575yj.put(Double.TYPE, new C0634Iz());
        f6575yj.put(Float.TYPE, new C0630Iw());
        f6575yj.put(Boolean.class, f6575yj.get(Boolean.TYPE));
        f6575yj.put(Integer.class, f6575yj.get(Integer.TYPE));
        f6575yj.put(Byte.class, f6575yj.get(Byte.TYPE));
        f6575yj.put(Short.class, f6575yj.get(Short.TYPE));
        f6575yj.put(Long.class, f6575yj.get(Long.TYPE));
        f6575yj.put(Double.class, f6575yj.get(Double.TYPE));
        f6575yj.put(Float.class, f6575yj.get(Float.TYPE));
    }

    public Class<?> clazz;
    /* renamed from: yi */
    public Constructor<?> f6576yi;

    public C2288de() {
    }

    public C2288de(Class<?> cls) throws NoSuchMethodException {
        this.clazz = cls;
        this.f6576yi = cls.getConstructor(new Class[]{String.class});
    }

    /* renamed from: a */
    public static void m29162a(Class<?> cls, C2288de deVar) {
        f6575yj.put(cls, deVar);
    }

    /* renamed from: c */
    public static C2288de m29163c(Class<?> cls) {
        return f6575yj.get(cls);
    }

    /* renamed from: O */
    public Object mo2718O(String str) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        try {
            return this.f6576yi.newInstance(new Object[]{str});
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException e) {
            if (Number.class.isAssignableFrom(this.clazz) && e.getCause() != null && (e.getCause() instanceof NumberFormatException)) {
                double parseDouble = Double.parseDouble(str);
                if (parseDouble % 1.0d == ScriptRuntime.NaN) {
                    return this.f6576yi.newInstance(new Object[]{String.valueOf((long) parseDouble)});
                }
            }
            throw e;
        }
    }
}
