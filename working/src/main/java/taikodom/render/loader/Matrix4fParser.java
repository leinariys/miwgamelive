package taikodom.render.loader;

import game.geometry.Matrix4fWrap;
import lombok.extern.slf4j.Slf4j;

/* renamed from: a.Ws */
/* compiled from: a */
@Slf4j
public class Matrix4fParser implements IParser {
    /* renamed from: O */
    public Object mo2718O(String str) {
        String[] split = str.split("[ \t\r\n]+");
        if (split.length == 3) {
            if (split.length == 3) {
                Matrix4fWrap ajk = new Matrix4fWrap();
                ajk.rotateX(Float.parseFloat(split[3]));
                ajk.rotateY(Float.parseFloat(split[4]));
                ajk.rotateZ(Float.parseFloat(split[5]));
                ajk.mo14049p(Float.parseFloat(split[0]), Float.parseFloat(split[1]), Float.parseFloat(split[2]));
                return ajk;
            }
        } else if (split.length == 6) {
            Matrix4fWrap ajk2 = new Matrix4fWrap();
            ajk2.mo14049p(Float.parseFloat(split[0]), Float.parseFloat(split[1]), Float.parseFloat(split[2]));
            return ajk2;
        }
        if (split.length == 16) {
            return new Matrix4fWrap(Float.parseFloat(split[0]), Float.parseFloat(split[4]), Float.parseFloat(split[8]), Float.parseFloat(split[12]), Float.parseFloat(split[1]), Float.parseFloat(split[5]), Float.parseFloat(split[9]), Float.parseFloat(split[13]), Float.parseFloat(split[2]), Float.parseFloat(split[6]), Float.parseFloat(split[10]), Float.parseFloat(split[14]), Float.parseFloat(split[3]), Float.parseFloat(split[7]), Float.parseFloat(split[11]), Float.parseFloat(split[15]));
        }
        log.error("Parsing a Matrix4f with wrong format: " + str);
        return new Matrix4fWrap();
    }
}
