package taikodom.render.loader;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.Wq */
/* compiled from: a */
public class Vec3fParser implements IParser {
    /* renamed from: O */
    public Object mo2718O(String str) {
        String[] split = str.split("[ \t\r\n]+");
        return new Vec3f(Float.parseFloat(split[0]), Float.parseFloat(split[1]), Float.parseFloat(split[2]));
    }
}
