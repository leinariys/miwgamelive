package taikodom.render.loader;

import gnu.trove.THashSet;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Set;

/* compiled from: a */
public abstract class FormatProvider {
    public SceneLoader loader;
    public Set<String> supportedFormats = new THashSet();

    public FormatProvider(SceneLoader sceneLoader, String... strArr) {
        this.loader = sceneLoader;
        this.supportedFormats.addAll(Arrays.asList(strArr));
    }

    public abstract void loadFile(InputStream inputStream, String str, String str2) throws IOException;

    /* access modifiers changed from: protected */
    public void addStockFile(StockFile stockFile) {
        this.loader.addStockFile(stockFile);
    }

    /* access modifiers changed from: protected */
    public StockEntry addEntry(StockFile stockFile, String str, RenderAsset renderAsset) {
        return this.loader.addEntry(stockFile, str, renderAsset);
    }

    public boolean check(String str) {
        return this.supportedFormats.contains(str);
    }
}
