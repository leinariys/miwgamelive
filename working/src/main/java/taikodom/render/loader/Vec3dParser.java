package taikodom.render.loader;

import game.geometry.Vec3d;

/* renamed from: a.Wp */
/* compiled from: a */
public class Vec3dParser implements IParser {
    /* renamed from: O */
    public Object mo2718O(String str) {
        String[] split = str.split("[ \t\r\n]+");
        return new Vec3d(Double.parseDouble(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]));
    }
}
