package taikodom.render.loader;

import java.util.*;

/* compiled from: a */
public class StockFile {
    public List<StockEntry> entries = new ArrayList();
    public Set<String> includes = new TreeSet(String.CASE_INSENSITIVE_ORDER);
    public String name;

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public StockEntry getEntry(String str) {
        for (StockEntry next : this.entries) {
            if (next.getName().equals(str)) {
                return next;
            }
        }
        return null;
    }

    public Collection<String> getIncludes() {
        return this.includes;
    }

    public void setIncludes(Collection<String> collection) {
        this.includes.clear();
        this.includes.addAll(collection);
    }

    public List<StockEntry> getEntries() {
        return this.entries;
    }

    public void setEntries(List<StockEntry> list) {
        this.entries = list;
    }

    public void addEntry(StockEntry stockEntry) {
        stockEntry.setStockFile(this);
        this.entries.add(stockEntry);
    }

    public int entryCount() {
        return this.entries.size();
    }

    public StockEntry getEntry(int i) {
        return this.entries.get(i);
    }

    public void removeInclude(String str) {
        this.includes.remove(str);
    }
}
