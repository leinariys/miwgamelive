package taikodom.render.loader;

import taikodom.render.loader.descriptors.UnassignedPropertyBase;

import java.util.ArrayList;
import java.util.List;

/* compiled from: a */
public class StockEntry {
    public boolean completing;
    public List<UnassignedPropertyBase> unassignedProperties = new ArrayList();
    public String clonesFrom;
    public boolean complete;
    public String name;
    public StockFile stockFile;
    public boolean unloaded;
    private RenderAsset asset;

    public StockEntry(String str) {
        this.name = str;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public StockFile getStockFile() {
        return this.stockFile;
    }

    public void setStockFile(StockFile stockFile2) {
        this.stockFile = stockFile2;
    }

    public boolean isComplete() {
        return this.complete;
    }

    public void setComplete(boolean z) {
        this.complete = z;
    }

    public String getClonesFrom() {
        return this.clonesFrom;
    }

    public void setClonesFrom(String str) {
        this.clonesFrom = str;
    }

    public boolean isUnloaded() {
        return this.unloaded;
    }

    public void setUnloaded(boolean z) {
        this.unloaded = z;
    }

    public RenderAsset getAsset() {
        return this.asset;
    }

    public void setAsset(RenderAsset renderAsset) {
        this.asset = renderAsset;
    }
}
