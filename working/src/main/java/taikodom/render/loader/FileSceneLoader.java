package taikodom.render.loader;

import logic.res.FileControl;
import taikodom.render.loader.provider.C6223ain;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: a */
public class FileSceneLoader extends SceneLoader {
    public C6223ain baseDir;

    public FileSceneLoader(String str) {
        this((C6223ain) new FileControl(str));
    }

    public FileSceneLoader(C6223ain ain) {
        setBaseDir(ain.mo2253BF());
    }

    public C6223ain getBaseDir() {
        return this.baseDir;
    }

    public void setBaseDir(File file) {
        setBaseDir((C6223ain) new FileControl(file));
    }

    public void setBaseDir(C6223ain ain) {
        this.baseDir = ain;
        this.imageLoader.setBasePath(ain);
    }

    public void loadFile(String str) throws IOException {
        loadFile(this.baseDir.concat(str));
    }

    public void loadFile(C6223ain ain) throws IOException {
        String path = ain.getPath();
        String replace = ain.getPath().substring(this.baseDir.getPath().length() + 1).replace('\\', '/');
        InputStream openInputStream = ain.openInputStream();
        loadFile(openInputStream, replace, path);
        openInputStream.close();
    }

    public void loadMissing() throws IOException {
        while (getMissingIncludes().size() > 0) {
            loadFile(getMissingIncludes().iterator().next());
        }
    }

    public void removeEntry(StockEntry stockEntry) {
        this.stockEntryCache.remove(stockEntry);
    }
}
