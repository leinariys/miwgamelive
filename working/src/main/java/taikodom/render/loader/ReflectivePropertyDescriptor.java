package taikodom.render.loader;

import taikodom.render.loader.descriptors.PropertyDescriptor;

import java.lang.reflect.Method;

/* compiled from: a */
public class ReflectivePropertyDescriptor extends PropertyDescriptor {
    private Method getter;
    private Method setter;
    private IParser valueParser;

    public ReflectivePropertyDescriptor(IParser dsVar, String str, String str2, boolean z, boolean z2) {
        this.isArray = z;
        this.isObject = z2;
        this.valueParser = dsVar;
        this.propertyTypeName = str;
        this.name = str2;
        this.isObject = z2;
    }

    public int arrayItemCount(RenderAsset renderAsset) {
        return 0;
    }

    public Object getAsObject(RenderAsset renderAsset) {
        try {
            return this.getter.invoke(renderAsset, new Object[0]);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }

    public String getAsString(RenderAsset renderAsset) {
        return String.valueOf(getAsObject(renderAsset));
    }

    public RenderAsset getObjectArrayItem(RenderAsset renderAsset, int i) {
        return null;
    }

    public boolean hasDefaultValue(RenderAsset renderAsset) {
        return false;
    }

    public String getName() {
        return this.name;
    }

    public void setGetter(Method method) {
        this.getter = method;
    }

    public void setSetter(Method method) {
        this.setter = method;
    }

    public boolean setAsString(Object obj, String str) {
        return setAsString(obj, 0, str);
    }

    public boolean setAsString(Object obj, int i, String str) {
        try {
            if (this.valueParser == null) {
                return false;
            }
            Object O = this.valueParser.mo2718O(str);
            if (this.isArray) {
                this.setter.invoke(obj, new Object[]{Integer.valueOf(i), O});
            } else {
                this.setter.invoke(obj, new Object[]{O});
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean set(Object obj, int i, Object obj2) {
        try {
            if (this.isArray) {
                this.setter.invoke(obj, new Object[]{Integer.valueOf(i), obj2});
                return true;
            }
            this.setter.invoke(obj, new Object[]{obj2});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String toString() {
        return "property " + this.propertyTypeName + " " + getName();
    }
}
