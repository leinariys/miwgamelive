package taikodom.render.loader;

import game.geometry.TransformWrap;
import lombok.extern.slf4j.Slf4j;

/* renamed from: a.Wx */
/* compiled from: a */
@Slf4j
public class TransformParser implements IParser {
    /* renamed from: O */
    public Object mo2718O(String str) {
        String[] split = str.split("[ \t\r\n]+");
        if (split.length == 3) {
            TransformWrap bcVar = new TransformWrap();
            bcVar.setTranslation(Double.parseDouble(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]));
            return bcVar;
        } else if (split.length == 6) {
            TransformWrap bcVar2 = new TransformWrap();
            bcVar2.mo17391y(Float.parseFloat(split[3]));
            bcVar2.mo17392z(Float.parseFloat(split[4]));
            bcVar2.mo17323A(Float.parseFloat(split[5]));
            bcVar2.setTranslation(Double.parseDouble(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]));
            return bcVar2;
        } else if (split.length == 16) {
            TransformWrap bcVar3 = new TransformWrap(Float.parseFloat(split[0]), Float.parseFloat(split[1]), Float.parseFloat(split[2]), Float.parseFloat(split[4]), Float.parseFloat(split[5]), Float.parseFloat(split[6]), Float.parseFloat(split[8]), Float.parseFloat(split[9]), Float.parseFloat(split[10]), Double.parseDouble(split[3]), Double.parseDouble(split[7]), Double.parseDouble(split[11]));
            bcVar3.orientation.normalize();
            return bcVar3;
        } else {
            log.error("Parsing a transform with wrong format: " + str);
            return new TransformWrap();
        }
    }
}
