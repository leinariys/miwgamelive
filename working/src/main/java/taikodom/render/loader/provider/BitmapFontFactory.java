package taikodom.render.loader.provider;

import logic.res.FileControl;
import lombok.extern.slf4j.Slf4j;
import taikodom.render.textures.BitmapFont;
import taikodom.render.textures.Font;
import taikodom.render.textures.WidthData;
import util.Syst;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.concurrent.Semaphore;

/* compiled from: a */
@Slf4j
public class BitmapFontFactory {
    public static final String CACHE_DIRECTORY = "data/fontcache/";
    private static final String formatVersion = "1.0";
    private static final String SEPARATOR = "/";
    /* access modifiers changed from: private */
    public static Semaphore fontConcurrentLock = new Semaphore(3);

    private static String rootPath;

    public static void setRootPath(String str) {
        rootPath = str;
    }

    public static BitmapFont getBitmapFont(java.awt.Font font) {
        BitmapFont bitmapFont = new BitmapFont();
        C5147a aVar = new C5147a(bitmapFont, font);
        aVar.setDaemon(true);
        aVar.start();
        return bitmapFont;
    }

    public static BufferedImage readSnapshot(BitmapFont bitmapFont) {
        try {
            java.awt.Font font = bitmapFont.getFont();
            FileControl avr = new FileControl(String.valueOf(String.valueOf(rootPath) + SEPARATOR + CACHE_DIRECTORY + Syst.getMd5(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf("") + font.getFamily()) + font.getFontName()) + font.getName()) + font.getSize()) + font.getStyle()) + formatVersion)) + ".bfd");
            if (!avr.exists()) {
                return null;
            }
            FileInputStream fileInputStream = new FileInputStream(avr.getFile());
            DataInputStream dataInputStream = new DataInputStream(fileInputStream);
            bitmapFont.setTextureResolution(dataInputStream.readInt());
            bitmapFont.setNumOfValidChars(dataInputStream.readInt());
            int readInt = dataInputStream.readInt();
            for (int i = 0; i < readInt; i++) {
                bitmapFont.getCharWidths().add(new WidthData(dataInputStream.readInt(), dataInputStream.readInt()));
            }
            BufferedImage read = ImageIO.read(fileInputStream);
            fileInputStream.close();
            return read;
        } catch (Exception e) {
            log.warn("Font snapshot invalid. creating a new one.");
            return null;
        }
    }

    public static void writeSnapshot(BufferedImage bufferedImage, int i, BitmapFont bitmapFont) {
        try {
            java.awt.Font font = bitmapFont.getFont();
            String str = String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf("") + font.getFamily()) + font.getFontName()) + font.getName()) + font.getSize()) + font.getStyle()) + formatVersion;
            File file = new File(String.valueOf(rootPath) + SEPARATOR + CACHE_DIRECTORY);
            if (!file.exists()) {
                file.mkdirs();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(new FileControl(String.valueOf(String.valueOf(rootPath) + SEPARATOR + CACHE_DIRECTORY + Syst.getMd5(str)) + ".bfd").getFile());
            DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream);
            dataOutputStream.writeInt((int) bitmapFont.getTextureResolution());
            dataOutputStream.writeInt(bitmapFont.getNumOfValidChars());
            dataOutputStream.writeInt(bitmapFont.getCharWidths().size());
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= bitmapFont.getCharWidths().size()) {
                    ImageIO.write(bufferedImage, "png", fileOutputStream);
                    fileOutputStream.close();
                    return;
                }
                dataOutputStream.writeInt(bitmapFont.getCharWidths().get(i3).width);
                dataOutputStream.writeInt(bitmapFont.getCharWidths().get(i3).realWidth);
                i2 = i3 + 1;
            }
        } catch (IOException e) {
            log.warn("Font-cache could not be writed.");
        }
    }

    /* access modifiers changed from: package-private */
    public Font createBitmapFont(java.awt.Font font) {
        return null;
    }

    /* renamed from: taikodom.render.loader.provider.BitmapFontFactory$a */
    static class C5147a extends Thread {
        private final /* synthetic */ BitmapFont goN;
        private final /* synthetic */ java.awt.Font goO;

        C5147a(BitmapFont bitmapFont, java.awt.Font font) {
            this.goN = bitmapFont;
            this.goO = font;
        }

        public void run() {
            try {
                BitmapFontFactory.fontConcurrentLock.acquire();
                this.goN.initialize(this.goO);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                BitmapFontFactory.fontConcurrentLock.release();
            }
        }
    }
}
