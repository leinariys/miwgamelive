package taikodom.render.loader.provider;

import logic.render.bink.C0972OJ;
import taikodom.render.BinkTexture;
import taikodom.render.loader.FormatProvider;
import taikodom.render.loader.SceneLoader;
import taikodom.render.loader.StockFile;
import taikodom.render.textures.BinkDataSource;

import java.io.InputStream;

/* compiled from: a */
public class BinkLoader extends FormatProvider {
    public BinkLoader(SceneLoader sceneLoader, String... strArr) {
        super(sceneLoader, strArr);
    }

    public void loadFile(InputStream inputStream, String str, String str2) {
        BinkDataSource binkDataSource = new BinkDataSource(C0972OJ.m7939d(str2, 0), 0);
        binkDataSource.setFileName(str2);
        BinkTexture binkTexture = new BinkTexture(binkDataSource);
        StockFile stockFile = new StockFile();
        stockFile.setName(str);
        addEntry(stockFile, str, binkTexture);
        addStockFile(stockFile);
    }
}
