package taikodom.render.loader.provider;

import taikodom.render.textures.Texture;

/* compiled from: a */
public class ImageLoaderEntry {
    int mipMapLevel;
    int mipMapResolution;
    boolean singleLevel;
    Texture texture;
}
