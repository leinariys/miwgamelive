package taikodom.render.loader.provider;

import taikodom.render.graphics2d.C0559Hm;
import taikodom.render.loader.FormatProvider;
import taikodom.render.loader.SceneLoader;
import taikodom.render.loader.StockFile;
import taikodom.render.textures.FlashTexture;

import java.io.File;
import java.io.InputStream;

/* compiled from: a */
public class FlashLoader extends FormatProvider {
    public FlashLoader(SceneLoader sceneLoader, String... strArr) {
        super(sceneLoader, strArr);
    }

    public void loadFile(InputStream inputStream, String str, String str2) {
        File file = new File(str2);
        C3176ol olVar = new C3176ol(C0559Hm.m5263p(file), file.length());
        olVar.setName(String.valueOf(str) + "/player");
        FlashTexture flashTexture = new FlashTexture(olVar);
        flashTexture.setName(str);
        StockFile stockFile = new StockFile();
        stockFile.setName(str);
        addEntry(stockFile, str, flashTexture);
        addEntry(stockFile, String.valueOf(str) + "/player", olVar);
        addStockFile(stockFile);
    }
}
