package taikodom.render.loader.provider;

import taikodom.render.graphics2d.C0559Hm;
import taikodom.render.loader.FormatProvider;
import taikodom.render.loader.SceneLoader;
import taikodom.render.loader.StockFile;
import taikodom.render.shaders.ShaderProgramObject;

import java.io.InputStream;

/* compiled from: a */
public class ShaderLoader extends FormatProvider {
    public ShaderLoader(SceneLoader sceneLoader, String... strArr) {
        super(sceneLoader, strArr);
    }

    public void loadFile(InputStream inputStream, String str, String str2) {
        int i;
        if (str.toLowerCase().endsWith(".vert")) {
            i = 35633;
        } else {
            i = 35632;
        }
        ShaderProgramObject shaderProgramObject = new ShaderProgramObject(i, C0559Hm.m5258c(inputStream, "utf-8"));
        shaderProgramObject.setName(str);
        StockFile stockFile = new StockFile();
        stockFile.setName(str);
        addEntry(stockFile, str, shaderProgramObject);
        addStockFile(stockFile);
    }
}
