package taikodom.render.loader.provider;

import com.hoplon.geometry.Vec3f;
import game.geometry.aLH;
import logic.render.aCF;
import logic.render.granny.*;
import logic.render.granny.dell.C6917awF;
import lombok.extern.slf4j.Slf4j;
import org.lwjgl.BufferUtils;
import taikodom.render.data.*;
import taikodom.render.enums.GeometryType;
import taikodom.render.graphics2d.C0559Hm;
import taikodom.render.loader.FormatProvider;
import taikodom.render.loader.SceneLoader;
import taikodom.render.loader.StockEntry;
import taikodom.render.loader.StockFile;
import taikodom.render.primitives.Mesh;
import taikodom.render.scene.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

/* compiled from: a */
@Slf4j
public class GrannyLoader extends FormatProvider {

    private static final String SEPARATOR = "/";

    public GrannyLoader(SceneLoader sceneLoader, String... strArr) {
        super(sceneLoader, strArr);
    }

    public void loadFile(InputStream inputStream, String str, String str2) throws IOException {
        C6298akK akk = new C6298akK();
        C0559Hm.copyStream(inputStream, akk);
        ByteBuffer chm = akk.chm();
        chm.flip();
        C6396amE a = AdapterGrannyJNI.m3853a(chm.limit(), (Buffer) chm);
        C2267dS e = AdapterGrannyJNI.m4834e(a);
        StockFile stockFile = new StockFile();
        stockFile.setName(str);
        if (e.mo17844kK() > 0) {
            loadGrannyAnimations(stockFile, e);
        }
        if (e.mo17840kG() > 0) {
            loadGrannyMeshs(stockFile, a, e);
            loadGrannyModels(stockFile, a, e);
        }
        addStockFile(stockFile);
    }

    private void loadGrannyMeshs(StockFile stockFile, C6396amE ame, C2267dS dSVar) {
        RGroup rGroup = new RGroup();
        rGroup.setName(correctGrannyFilePath(stockFile, "rgroup", "mainGroup"));
        C5148a aVar = new C5148a((C5148a) null);
        int kG = dSVar.mo17840kG();
        for (int i = 0; i < kG; i++) {
            C5975adz a = AdapterGrannyJNI.m3836a(dSVar.mo17841kH(), i);
            RModel rModel = new RModel(a);
            rModel.setName(correctGrannyFilePath(stockFile, "rmodel", a.getName()));
            addEntry(stockFile, rModel.getName(), rModel);
            for (int i2 = 0; i2 < a.bRs(); i2++) {
                loadGrannyMesh(stockFile, rGroup, rModel, a.bRt().mo8389xD(i2).cWp(), aVar);
            }
        }
        addEntry(stockFile, rGroup.getName(), rGroup);
    }

    private void loadGrannyMesh(StockFile stockFile, RGroup rGroup, RModel rModel, C1126QZ qz, C5148a aVar) {
        float f;
        C0251DE bqH = qz.bqH();
        boolean hasVextexComponent = hasVextexComponent(qz, aCF.position);
        boolean hasVextexComponent2 = hasVextexComponent(qz, aCF.diffuseColor);
        boolean hasVextexComponent3 = hasVextexComponent(qz, aCF.tangent);
        boolean hasVextexComponent4 = hasVextexComponent(qz, aCF.normal);
        int i = 0;
        for (int i2 = 0; i2 < 32; i2++) {
            if (hasVextexComponent(qz, String.valueOf(aCF.textureCoordinates) + Integer.toString(i2))) {
                i++;
            }
        }
        VertexLayout vertexLayout = new VertexLayout(hasVextexComponent, hasVextexComponent2, hasVextexComponent3, hasVextexComponent4, i);
        int aGU = bqH.aGU();
        VertexDataImpl vertexDataImpl = new VertexDataImpl(vertexLayout, aGU);
        if (hasVextexComponent) {
            getVextexComponent(qz, aCF.position, 3, vertexDataImpl.getVertices().buffer());
        }
        if (hasVextexComponent2) {
            getVextexComponent(qz, aCF.diffuseColor, 3, vertexDataImpl.getColors().buffer());
        }
        if (hasVextexComponent4) {
            getVextexComponent(qz, aCF.normal, 3, vertexDataImpl.getNormals().buffer());
        }
        if (hasVextexComponent3) {
            getVextexComponent(qz, aCF.tangent, 4, vertexDataImpl.getTangents().buffer());
            FloatBuffer vextexComponent = getVextexComponent(qz, aCF.binormal, 3);
            Vec4fData tangents = vertexDataImpl.getTangents();
            if (vextexComponent == null || vertexDataImpl.getNormals() == null) {
                for (int i3 = 0; i3 < aGU; i3++) {
                    tangents.setW(i3, 1.0f);
                }
            } else {
                Vec3fData normals = vertexDataImpl.getNormals();
                Vec3fData vec3fData = new Vec3fData(vextexComponent);
                Vec3f vec3f = new Vec3f();
                Vec3f vec3f2 = new Vec3f();
                Vec3f vec3f3 = new Vec3f();
                Vec3f vec3f4 = new Vec3f();
                for (int i4 = 0; i4 < aGU; i4++) {
                    normals.get(i4, vec3f);
                    vec3fData.get(i4, vec3f2);
                    tangents.getVec3(i4, vec3f4);
                    vec3f3.cross(vec3f, vec3f4);
                    if (vec3f3.dot(vec3f2) > 0.0f) {
                        f = -1.0f;
                    } else {
                        f = 1.0f;
                    }
                    tangents.setW(i4, f);
                }
            }
        }
        int i5 = 0;
        for (int i6 = 0; i6 < 32; i6++) {
            String str = String.valueOf(aCF.textureCoordinates) + Integer.toString(i6);
            if (hasVextexComponent(qz, str)) {
                getVextexComponent(qz, str, 2, vertexDataImpl.getTexCoords()[i5].buffer());
                i5++;
            }
        }
        ShortIndexData shortIndexData = new ShortIndexData(AdapterGrannyJNI.m4884h(qz));
        AdapterGrannyJNI.m4054a(qz, 2, shortIndexData.buffer());
        boolean g = AdapterGrannyJNI.m4880g(qz);
        if (qz.bqK().aWr() > 1) {
            RGroup rGroup2 = new RGroup();
            rGroup2.setName(correctGrannyFilePath(stockFile, "rgroup", qz.getName()));
            C3152oP aWs = qz.bqK().aWs();
            int i7 = 0;
            while (i7 < qz.bqK().aWr()) {
                C3152oP dF = aWs.mo20986dF(i7);
                Mesh mesh = new Mesh();
                mesh.setVertexData(vertexDataImpl);
                mesh.setGeometryType(GeometryType.TRIANGLES);
                mesh.setIndexes(shortIndexData.subSet(dF.mo20980UA() * 3, dF.mo20981UB() * 3));
                mesh.getIndexes().setUseVbo(true);
                mesh.computeLocalAabb();
                mesh.setGrannyMesh(qz);
                mesh.setGrannyBinding(AdapterGrannyJNI.m3879a(qz, rModel.getSkeleton(), rModel.getSkeleton()));
                if (AdapterGrannyJNI.m4880g(qz)) {
                    mesh.setGrannyDeformer((C6604aqE) null);
                } else {
                    g = false;
                    mesh.setGrannyDeformer(AdapterGrannyJNI.m3867a(AdapterGrannyJNI.m4799d(qz), AdapterGrannyJNI.aVf(), C6304akQ.fUv, C1622Xl.eIx));
                }
                mesh.setName(correctGrannyFilePath(stockFile, "mesh", getGroupName(qz.getName(), aVar.cIE)));
                mesh.setGrannyMaterialIndex(dF.mo20982Uz());
                log.debug("Loading " + mesh.getName());
                mesh.setGrannySkeleton(rModel.getSkeleton());
                RMesh rMesh = new RMesh();
                rMesh.setMesh(mesh);
                rMesh.setName(correctGrannyFilePath(stockFile, "rmesh", getGroupName(qz.getName(), aVar.cIE)));
                rGroup2.addChild(rMesh);
                rModel.addSubMesh(rMesh);
                rModel.checkMaxMutableVertexCount(AdapterGrannyJNI.m4827e(qz));
                addEntry(stockFile, rMesh.getName(), rMesh);
                addEntry(stockFile, mesh.getName(), mesh);
                i7++;
                aVar.cIE++;
            }
            loadBonesIntoRenderObjects(rModel.getGrannyModel(), rGroup2, stockFile);
            rGroup.addChild(rGroup2);
            addEntry(stockFile, rGroup2.getName(), rGroup2);
        }
        vertexDataImpl.setUseVbo(g);
        Mesh mesh2 = new Mesh();
        mesh2.setVertexData(vertexDataImpl);
        mesh2.setGeometryType(GeometryType.TRIANGLES);
        mesh2.setIndexes(shortIndexData);
        shortIndexData.setUseVbo(true);
        mesh2.setName(correctGrannyFilePath(stockFile, "mesh", qz.getName()));
        mesh2.setGrannyMesh(qz);
        mesh2.setGrannyBinding(AdapterGrannyJNI.m3879a(qz, rModel.getSkeleton(), rModel.getSkeleton()));
        if (AdapterGrannyJNI.m4880g(qz)) {
            mesh2.setGrannyDeformer((C6604aqE) null);
        } else {
            mesh2.setGrannyDeformer(AdapterGrannyJNI.m3867a(AdapterGrannyJNI.m4799d(qz), AdapterGrannyJNI.aVf(), C6304akQ.fUv, C1622Xl.eIx));
        }
        addEntry(stockFile, mesh2.getName(), mesh2);
        aLH aabb = mesh2.getAabb();
        Vec3f vec3f5 = new Vec3f();
        for (int i8 = 0; i8 < shortIndexData.size(); i8++) {
            vertexDataImpl.getVertices().get(shortIndexData.getIndex(i8), vec3f5);
            aabb.addPoint(vec3f5);
        }
        RMesh rMesh2 = new RMesh();
        rMesh2.setMesh(mesh2);
        rMesh2.setName(correctGrannyFilePath(stockFile, "rmesh", qz.getName()));
        loadBonesIntoRenderObjects(rModel.getGrannyModel(), rMesh2, stockFile);
        rGroup.addChild(rMesh2);
        if (qz.bqK().aWr() == 1) {
            rModel.addSubMesh(rMesh2);
            rModel.checkMaxMutableVertexCount(AdapterGrannyJNI.m4827e(qz));
        }
        addEntry(stockFile, rMesh2.getName(), rMesh2);
        log.debug("Finished loading " + mesh2.getName());
    }

    private void loadGrannyModels(StockFile stockFile, C6396amE ame, C2267dS dSVar) {
        int kG = dSVar.mo17840kG();
        for (int i = 0; i < kG; i++) {
            C5975adz a = AdapterGrannyJNI.m3836a(dSVar.mo17841kH(), i);
            String name = a.getName();
            int bRs = a.bRs();
            if (bRs > 1) {
                aDP bRt = a.bRt();
                RGroup rGroup = new RGroup();
                rGroup.setName(correctGrannyFilePath(stockFile, "rgroup", name));
                for (int i2 = 0; i2 < bRs; i2++) {
                    C1126QZ cWp = bRt.mo8389xD(i2).cWp();
                    StockEntry entry = stockFile.getEntry(correctGrannyFilePath(stockFile, "rmesh", cWp.getName()));
                    if (entry == null) {
                        rGroup.addChild((SceneObject) this.loader.getAsset(correctGrannyFilePath(stockFile, "rgroup", cWp.getName())));
                    } else {
                        rGroup.addChild((SceneObject) entry.getAsset());
                    }
                }
                addEntry(stockFile, rGroup.getName(), rGroup);
            } else {
                RGroup rGroup2 = new RGroup();
                rGroup2.setName(correctGrannyFilePath(stockFile, "rgroup", name));
                loadBonesIntoRenderObjects(a, rGroup2, stockFile);
                addEntry(stockFile, rGroup2.getName(), rGroup2);
            }
        }
    }

    private void loadGrannyAnimations(StockFile stockFile, C2267dS dSVar) {
        C6917awF kL = dSVar.mo17845kL();
        int kK = dSVar.mo17844kK();
        for (int i = 0; i < kK; i++) {
            RAnimation rAnimation = new RAnimation(AdapterGrannyJNI.m3910a(kL, i));
            rAnimation.setName(correctGrannyFilePath(stockFile, "anim", Integer.toString(i)));
            addEntry(stockFile, rAnimation.getName(), rAnimation);
        }
    }

    private void loadBonesIntoRenderObjects(C5975adz adz, SceneObject sceneObject, StockFile stockFile) {
        int bkG = adz.getSkeleton().bkG();
        C5767aVz bkH = adz.getSkeleton().bkH();
        if (bkG > 1) {
            for (int i = 0; i < bkG; i++) {
                C5767aVz Bg = bkH.mo11989Bg(i);
                RenderObject renderObject = new RenderObject();
                renderObject.setName(String.valueOf(Bg.getName()) + "_bone");
                FloatMatrixData floatMatrixData = new FloatMatrixData();
                AdapterGrannyJNI.m4168a(Bg.dBW(), (Buffer) floatMatrixData.buffer());
                renderObject.setTransform(floatMatrixData.getTransform());
                addEntry(stockFile, renderObject.getName(), renderObject);
                sceneObject.addChild(renderObject);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String getGroupName(String str, int i) {
        return String.valueOf(str) + SEPARATOR + i;
    }

    /* access modifiers changed from: protected */
    public FloatBuffer getVextexComponent(C1126QZ qz, String str, int i) {
        int i2 = 0;
        C1731Za aGT = qz.bqH().aGT();
        while (!C3278pt.aRH.equals(aGT.bHY())) {
            if (str.equals(aGT.getName())) {
                C1731Za ht = AdapterGrannyJNI.m4919ht(2);
                try {
                    ht.mo7433a(0, aGT);
                    ht.mo7447pi(i);
                    ht.mo7448pj(1).mo7441c(C3278pt.aRH);
                    FloatBuffer createFloatBuffer = BufferUtils.createFloatBuffer(AdapterGrannyJNI.m4827e(qz) * i);
                    AdapterGrannyJNI.m4055a(qz, ht, (Buffer) createFloatBuffer);
                    return createFloatBuffer;
                } finally {
                    ht.delete();
                }
            } else {
                i2++;
                aGT = aGT.bId();
            }
        }
        return null;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public boolean getVextexComponent(C1126QZ qz, String str, int i, FloatBuffer floatBuffer) {
        C1731Za aGT = qz.bqH().aGT();
        int i2 = 0;
        while (!C3278pt.aRH.equals(aGT.bHY())) {
            if (str.equals(aGT.getName())) {
                C1731Za ht = AdapterGrannyJNI.m4919ht(2);
                try {
                    ht.mo7433a(0, aGT);
                    ht.mo7447pi(i);
                    ht.mo7448pj(1).mo7441c(C3278pt.aRH);
                    AdapterGrannyJNI.m4055a(qz, ht, (Buffer) floatBuffer);
                    ht.delete();
                    return true;
                } catch (Throwable th) {
                    ht.delete();
                    throw th;
                }
            } else {
                i2++;
                aGT = aGT.bId();
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public String correctGrannyFilePath(StockFile stockFile, String str, String str2) {
        return String.valueOf(stockFile.getName()) + SEPARATOR + str + SEPARATOR + new File(str2).getName();
    }

    /* access modifiers changed from: protected */
    public boolean hasVextexComponent(C1126QZ qz, String str) {
        int i = 0;
        for (C1731Za aGT = qz.bqH().aGT(); !C3278pt.aRH.equals(aGT.bHY()); aGT = aGT.bId()) {
            if (str.equals(aGT.getName())) {
                return true;
            }
            i++;
        }
        return false;
    }

    /* renamed from: taikodom.render.loader.provider.GrannyLoader$a */
    private static class C5148a {
        int cIE;

        private C5148a() {
            this.cIE = 0;
        }

        /* synthetic */ C5148a(C5148a aVar) {
            this();
        }
    }
}
