package taikodom.render.loader.provider;

import logic.render.gameswf.*;
import logic.render.miles.aNZ;
import taikodom.render.SoundMiles;
import taikodom.render.loader.RenderAsset;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;

/* renamed from: a.ol */
/* compiled from: a */
public class C3176ol extends RenderAsset {
    public static final boolean aNp = false;

    static {
        aHY.m15231b(aHY.ddZ());
        try {
            Field declaredField = aNZ.class.getDeclaredField("swigCPtr");
            declaredField.setAccessible(true);
            aHY.m15226b(aHY.m15225B((int) ((Long) declaredField.get(SoundMiles.getInstance().getDig())).longValue(), 44100, 16));
        } catch (Exception e) {
            e.printStackTrace();
        }
        m36838cA(0.1f);
        aHY.clear_log_callback();
    }

    private float aNA = 0.0f;
    private boolean aNB = true;
    private boolean aNC = true;
    private boolean aND = true;
    private int aNE = 0;
    private long aNq;
    private String aNr;
    private C2781ju aNs;
    private long aNt;
    private long aNu;
    private int aNv;
    private int aNw;
    private int aNx;
    private C1831aX aNy = null;
    private C3840ve aNz = null;
    private ByteBuffer fileBuffer;
    private boolean finished = false;
    private boolean initialized = false;

    public C3176ol(ByteBuffer byteBuffer, long j) {
        this.fileBuffer = byteBuffer;
        this.aNq = j;
        m36837a(byteBuffer, j);
    }

    public C3176ol(String str) {
        this.aNr = str;
        initialize(str);
    }

    /* renamed from: SB */
    public static float m36836SB() {
        return aHY.get_curve_max_pixel_error();
    }

    /* renamed from: cA */
    public static void m36838cA(float f) {
        aHY.set_curve_max_pixel_error(f);
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public void destroy() {
        this.aNs = null;
        if (this.aNz != null) {
            this.aNz.delete();
            this.aNz = null;
        }
        if (this.aNy != null) {
            this.aNy.mo12113fJ();
            aHY.m15228b(this.aNy);
            this.aNy.delete();
            this.aNy = null;
        }
        aHY.clear_movie();
        this.initialized = false;
    }

    public int getCurrentFrame() {
        return this.aNz.aku();
    }

    /* renamed from: SC */
    public C2781ju mo21035SC() {
        return this.aNs;
    }

    /* renamed from: SD */
    public float mo21036SD() {
        return this.aNA;
    }

    /* renamed from: dv */
    public void mo21048dv(int i) {
        this.aNz.mo22649eW(Math.min(Math.max(0, i), this.aNy.mo12110fG()));
    }

    private void load(String str) {
        this.aNy = aHY.m15234mL(str);
        if (this.aNy == null) {
            System.err.println("Can't create a flash movie from '" + str + "'.");
            System.exit(1);
        }
        this.aNz = this.aNy.mo12112fI();
        if (this.aNz == null) {
            System.err.println("Can't create flash movie instance.");
            System.exit(1);
        }
        aHY.m15232b(this.aNz);
        this.aNs = new C2781ju(str, this.aNz.akC(), this.aNz.akD(), this.aNy.mo12110fG(), this.aNz.akE(), this.aNz.akB());
        this.aNt = System.nanoTime();
        this.aNu = this.aNt;
        this.aNz.mo22622a(0, 0, this.aNs.getWidth(), this.aNs.getHeight());
        this.aNz.mo22645dS(this.aNC ? 1.0f : 0.05f);
        this.initialized = true;
    }

    public boolean isFinished() {
        return this.finished;
    }

    public boolean isPlaying() {
        if (this.aNz != null) {
            return C3840ve.C3841a.iGr.equals(this.aNz.akw());
        }
        return false;
    }

    /* renamed from: SE */
    public boolean mo21037SE() {
        return this.aNB;
    }

    /* renamed from: SF */
    public boolean mo21038SF() {
        return this.aNC;
    }

    /* renamed from: a */
    private void m36837a(ByteBuffer byteBuffer, long j) {
        aHY.register_memory_file_opener_callback();
        C5681aSr.m18249c((int) j, byteBuffer);
        load("");
    }

    private void initialize(String str) {
        aHY.register_default_file_opener_callback();
        load(str);
    }

    /* renamed from: a */
    public void mo21040a(C2214cp cpVar, boolean z) {
        if (this.aNz != null) {
            this.aNz.mo22639b(cpVar, z);
        }
    }

    public void play() {
        if (!this.initialized) {
            reload();
        }
        this.aNE = 0;
        mo21048dv(0);
        this.finished = false;
        if (this.aNz != null) {
            this.aNz.mo22624a(C3840ve.C3841a.iGr);
        }
    }

    private void reload() {
        if (this.aNr == null || this.aNr.isEmpty()) {
            m36837a(this.fileBuffer, this.aNq);
        } else {
            initialize(this.aNr);
        }
    }

    /* renamed from: b */
    public void mo21043b(int i, int i2, int i3) {
        if (i != this.aNw || i2 != this.aNx || this.aNv != i3) {
            this.aND = true;
            this.aNw = i;
            this.aNx = i2;
            this.aNv = i3;
        }
    }

    /* renamed from: cB */
    public void mo21044cB(float f) {
        this.aNA = f;
    }

    /* renamed from: aL */
    public void mo21041aL(boolean z) {
        this.aNB = z;
    }

    public void setSize(int i, int i2) {
        this.aNs.setWidth(i);
        this.aNs.setHeight(i2);
        this.aNz.mo22622a(0, 0, this.aNs.getWidth(), this.aNs.getHeight());
    }

    /* renamed from: aM */
    public void mo21042aM(boolean z) {
        this.aNC = z;
    }

    /* renamed from: SG */
    public boolean mo21039SG() {
        if (this.finished || !isPlaying()) {
            return false;
        }
        if (this.aNB || (getCurrentFrame() + 1 < this.aNy.mo12110fG() && getCurrentFrame() >= this.aNE)) {
            long nanoTime = System.nanoTime();
            float f = (float) (nanoTime - this.aNu);
            this.aNu = nanoTime;
            if (this.aNA <= 0.0f || ((float) (nanoTime - this.aNt)) <= this.aNA * 1.0E9f) {
                if (this.aND) {
                    this.aNz.mo22648e(this.aNw, this.aNx, this.aNv);
                    this.aND = false;
                }
                this.aNE = getCurrentFrame();
                this.aNz.mo22644dR(1.0E-9f * f);
                if (getCurrentFrame() != this.aNE) {
                    return true;
                }
                return false;
            }
            this.finished = true;
            return false;
        }
        this.finished = true;
        return false;
    }

    public void stop() {
        if (this.aNz != null) {
            this.aNz.mo22624a(C3840ve.C3841a.iGs);
        }
    }

    public void draw() {
        this.aNz.display();
    }
}
