package taikodom.render.loader.provider;

/* renamed from: a.ju */
/* compiled from: a */
public class C2781ju {
    private int akl;
    private float akm;
    private int height;
    private String path;
    private int version;
    private int width;

    public C2781ju(String str, int i, int i2, int i3, float f, int i4) {
        this.path = str;
        this.width = i;
        this.height = i2;
        this.akl = i3;
        this.akm = f;
        this.version = i4;
    }

    /* renamed from: Fq */
    public int mo19989Fq() {
        return this.akl;
    }

    /* renamed from: Fr */
    public float mo19990Fr() {
        return this.akm;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int i) {
        this.height = i;
    }

    public String getPath() {
        return this.path;
    }

    public int getVersion() {
        return this.version;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int i) {
        this.width = i;
    }
}
