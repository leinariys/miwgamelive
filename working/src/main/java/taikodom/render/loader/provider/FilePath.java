package taikodom.render.loader.provider;

import java.io.*;
import java.nio.ByteBuffer;

/* renamed from: a.ain  reason: case insensitive filesystem */
/* compiled from: a */
public interface FilePath {
    /* renamed from: BB */
    FilePath mo2249BB();

    /* renamed from: BC */
    FilePath[] mo2250BC();

    /* renamed from: BD */
    void mo2251BD();

    /* renamed from: BE */
    PrintWriter mo2252BE();

    /* renamed from: BF */
    FilePath mo2253BF();

    /* renamed from: BG */
    File getFile();

    /* renamed from: BH */
    byte[] mo2255BH();

    /* renamed from: BI */
    ByteBuffer mo2256BI();

    /* renamed from: a */
    FilePath[] mo2258a(C0399FY fy);

    /* renamed from: aC */
    FilePath concat(String str);

    /* renamed from: d */
    void mo2260d(byte[] bArr);

    /* renamed from: d */
    boolean mo2261d(FilePath ain);

    boolean delete();

    boolean exists();

    String getName();

    String getPath();

    boolean isDir();

    long lastModified();

    long length();

    InputStream openInputStream() throws FileNotFoundException;

    OutputStream openOutputStream();
}
