package taikodom.render;

import com.hoplon.geometry.Vec3f;
import com.sun.opengl.util.BufferUtil;
import game.geometry.Matrix4fWrap;
import game.geometry.Vec3d;
import org.lwjgl.BufferUtils;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.primitives.Light;
import taikodom.render.primitives.Primitive;
import taikodom.render.primitives.TreeRecord;
import taikodom.render.scene.LightRecordList;
import taikodom.render.scene.SceneConfig;
import taikodom.render.scene.Viewport;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.Material;
import taikodom.render.textures.PBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.GLContext;
import javax.media.opengl.glu.GLU;
import java.nio.FloatBuffer;
import java.util.LinkedList;
import java.util.List;

/* compiled from: a */
public class DrawContext {
    private static PBuffer[] auxPBuffer;
    private static boolean useVbo = false;
    public final Matrix4fWrap camProjection = new Matrix4fWrap();
    public final Matrix4fWrap gpuCamAffine = new Matrix4fWrap();
    public final Matrix4fWrap gpuCamTransform = new Matrix4fWrap();
    public final Vec3d gpuOffset = new Vec3d();
    final Viewport viewport = new Viewport();
    public Material currentMaterial;
    public ShaderPass currentPass;
    public Primitive currentPrimitive;
    public List<SceneEvent> eventList;
    public FloatBuffer floatBuffer60Temp0 = BufferUtils.createFloatBuffer(16);
    public MaterialDrawingStates materialState = new MaterialDrawingStates();
    public Matrix4fWrap matrix4fTemp0 = new Matrix4fWrap();
    public Matrix4fWrap matrix4fTemp1 = new Matrix4fWrap();
    public Matrix4fWrap matrix4fTemp2 = new Matrix4fWrap();
    public PrimitiveDrawingState primitiveState = new PrimitiveDrawingState();
    public RenderView renderView;
    public SceneConfig sceneConfig = new SceneConfig();
    public ShaderParametersRenderingContext shaderParamContext = new ShaderParametersRenderingContext();
    public FloatBuffer tempBuffer0 = BufferUtil.newFloatBuffer(32);
    public Vec3d vec3dTemp0 = new Vec3d();
    public Vec3d vec3dTemp1 = new Vec3d();
    public Vec3d vec3dTemp2 = new Vec3d();
    public Vec3f vec3fTemp0 = new Vec3f();
    public Vec3f vec3fTemp1 = new Vec3f();
    public Vec3f vec3fTemp2 = new Vec3f();
    long currentFrame;
    LightRecordList.LightRecord currentLight;
    TreeRecord currentRecord;
    long drawCalls;
    float elapsedTime;
    int maxShaderQuality;
    long meshesBind;
    long numBillboards;
    long numImpostorsRendered;
    long numImpostorsUpdated;
    long numLights;
    long numMaterialsBind;
    long numMeshesBinds;
    long numObjectsStepped;
    long numPrimitiveBind;
    long numShadersBind;
    long numTexturesBind;
    long numTriangles;
    long octreeNodesVisited;
    boolean rendering;
    String roll = ".:|:";
    long scissorHeight;
    long scissorOffsetX;
    long scissorOffsetY;
    long scissorWidth;
    long textureMemoryBinded;
    double totalTime;
    /* renamed from: gl */
    private GL f10331gl;
    private GLContext glContext;
    private GLU glu = new GLU();
    private Light mainLight;

    public PBuffer getAuxPBuffer() {
        char c = 1;
        if (auxPBuffer == null) {
            auxPBuffer = new PBuffer[2];
            auxPBuffer[0] = new PBuffer();
            auxPBuffer[0].create(this, 1024, 1024);
            auxPBuffer[1] = new PBuffer();
            auxPBuffer[1].create(this, 1024, 1024);
        }
        if (!auxPBuffer[0].isActive()) {
            c = 0;
        }
        return auxPBuffer[c];
    }

    public GLU glu() {
        return this.glu;
    }

    /* renamed from: gl */
    public GL getGl() {
        return this.f10331gl;
    }

    public void incNumMeshesBinds() {
        this.meshesBind++;
    }

    public void incNumDrawCalls() {
        this.drawCalls++;
    }

    public long getNumDrawCalls() {
        return this.drawCalls;
    }

    public void incNumTriangles(int i) {
        this.numTriangles += (long) i;
    }

    public long getNumTriangles() {
        return this.numTriangles;
    }

    public void setGL(GL gl) {
        this.f10331gl = gl;
    }

    public long getCurrentFrame() {
        return this.currentFrame;
    }

    public void setCurrentFrame(long j) {
        this.currentFrame = j;
    }

    public double getTotalTime() {
        return this.totalTime;
    }

    public void setTotalTime(double d) {
        this.totalTime = d;
    }

    public float getElapsedTime() {
        return this.elapsedTime;
    }

    public void setElapsedTime(float f) {
        this.elapsedTime = f;
    }

    public void reset() {
        this.drawCalls = 0;
        this.numTexturesBind = 0;
        this.numPrimitiveBind = 0;
        this.numMeshesBinds = 0;
        this.numTriangles = 0;
        this.numImpostorsRendered = 0;
        this.numImpostorsUpdated = 0;
        this.meshesBind = 0;
        this.numMaterialsBind = 0;
        this.numShadersBind = 0;
        this.octreeNodesVisited = 0;
        this.mainLight = null;
    }

    public void incCurrentFrame() {
        this.currentFrame++;
    }

    public Light getMainLight() {
        return this.mainLight;
    }

    public void setMainLight(Light light) {
        this.mainLight = light;
    }

    public TreeRecord currentRecord() {
        return this.currentRecord;
    }

    public void setCurrentLight(LightRecordList.LightRecord lightRecord) {
        this.currentLight = lightRecord;
    }

    public boolean debugDraw() {
        return this.currentFrame < 3;
    }

    /* access modifiers changed from: protected */
    public void doLog(String str) {
        int length = new Exception().getStackTrace().length;
        for (int i = 0; i < length; i++) {
            System.out.print(this.roll.charAt(i % this.roll.length()));
        }
        System.out.println(str);
    }

    public void logDraw(Object obj) {
        doLog("draw " + obj);
    }

    public void logBind(Object obj) {
        doLog("bind " + obj);
    }

    public void logCompile(Object obj) {
        doLog("compile " + obj);
    }

    public void logUnbind(Object obj) {
        doLog("unbind " + obj);
    }

    public void log(String str) {
        doLog(str);
    }

    public RenderView getRenderView() {
        return this.renderView;
    }

    public void setRenderView(RenderView renderView2) {
        this.renderView = renderView2;
    }

    public void postSceneEvent(String str, Object obj) {
        if (this.eventList == null) {
            this.eventList = new LinkedList();
        }
        this.eventList.add(new SceneEvent(str, obj));
    }

    public void setGpuOffset(Vec3d ajr) {
        this.gpuOffset.mo9484aA(ajr);
    }

    public int getScreenOffsetX() {
        return this.viewport.x;
    }

    public int getScreenOffsetY() {
        return this.viewport.y;
    }

    public int getScreenWidth() {
        return this.viewport.width;
    }

    public int getScreenHeight() {
        return this.viewport.height;
    }

    public void setViewport(int i, int i2, int i3, int i4) {
        this.viewport.x = i;
        this.viewport.y = i2;
        this.viewport.width = i3;
        this.viewport.height = i4;
    }

    public long getOctreeNodesVisited() {
        return this.octreeNodesVisited;
    }

    public void incOctreeNodesVisited() {
        this.octreeNodesVisited++;
    }

    public GLContext getGlContext() {
        return this.glContext;
    }

    public void setGlContext(GLContext gLContext) {
        this.glContext = gLContext;
    }

    public long getNumShadersBind() {
        return this.numShadersBind;
    }

    public void incNumShadersBind() {
        this.numShadersBind++;
    }

    public void incNumMaterialBinds() {
        this.numMaterialsBind++;
    }

    public long getNumMaterarialsBind() {
        return this.numMaterialsBind;
    }

    public void incNumImpostorsRendered() {
        this.numImpostorsRendered++;
    }

    public long getNumImpostorsRendered() {
        return this.numImpostorsRendered;
    }

    public void incNumImpostorsUpdated() {
        this.numImpostorsUpdated++;
    }

    public long getNumImpostorsUpdated() {
        return this.numImpostorsUpdated;
    }

    public int getMaxShaderQuality() {
        return this.maxShaderQuality;
    }

    public void setMaxShaderQuality(int i) {
        this.maxShaderQuality = i;
    }

    public boolean isUseVbo() {
        return useVbo;
    }

    public void setUseVbo(boolean z) {
        useVbo = z && GLSupportedCaps.isVbo();
    }

    public long getNumPrimitiveBind() {
        return this.numPrimitiveBind;
    }

    public void incNumPrimitiveBind() {
        this.numPrimitiveBind++;
    }

    public long getNumTextureBind() {
        return this.numTexturesBind;
    }

    public void incNumTextureBind() {
        this.numTexturesBind++;
    }

    public Viewport getViewport() {
        return this.viewport;
    }

    public void setViewport(Viewport viewport2) {
        this.viewport.set(viewport2);
    }
}
