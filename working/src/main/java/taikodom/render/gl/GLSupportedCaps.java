package taikodom.render.gl;

/* renamed from: taikodom.render.gl.GLSupportedCaps */
/* compiled from: a */
public class GLSupportedCaps {
    private static boolean dds;
    private static boolean drawRangeElements;
    private static boolean fbo;
    private static boolean glsl;
    private static int maxTexChannels;
    private static boolean multitexture;
    private static boolean occlusionQuery;
    private static boolean pbuffer;
    private static boolean textureNPOT;
    private static boolean textureRectangle;
    private static boolean vbo;
    private static boolean vertexAttrib;
    private static boolean vertexShader;

    public static boolean isGlsl() {
        return glsl;
    }

    public static void setGlsl(boolean z) {
        glsl = z;
    }

    public static boolean isMultitexture() {
        return multitexture;
    }

    public static void setMultitexture(boolean z) {
        multitexture = z;
    }

    public static boolean isVbo() {
        return vbo;
    }

    public static void setVbo(boolean z) {
        vbo = z;
    }

    public static boolean isDds() {
        return dds;
    }

    public static void setDds(boolean z) {
        dds = z;
    }

    public static boolean isDrawRangeElements() {
        return drawRangeElements;
    }

    public static void setDrawRangeElements(boolean z) {
        drawRangeElements = z;
    }

    public static boolean isFbo() {
        return fbo;
    }

    public static void setFbo(boolean z) {
        fbo = z;
    }

    public static boolean isVertexShader() {
        return vertexShader;
    }

    public static void setVertexShader(boolean z) {
        vertexShader = z;
    }

    public static boolean isPbuffer() {
        return pbuffer;
    }

    public static void setPbuffer(boolean z) {
        pbuffer = z;
    }

    public static boolean isTextureRectangle() {
        return textureRectangle;
    }

    public static void setTextureRectangle(boolean z) {
        textureRectangle = z;
    }

    public static boolean isVertexAttrib() {
        return vertexAttrib;
    }

    public static void setVertexAttrib(boolean z) {
        vertexAttrib = z;
    }

    public static boolean isTextureNPOT() {
        return textureNPOT;
    }

    public static void setTextureNPOT(boolean z) {
        textureNPOT = z;
    }

    public static boolean isOcclusionQuery() {
        return occlusionQuery;
    }

    public static void setOcclusionQuery(boolean z) {
        occlusionQuery = z;
    }

    public static int getMaxTexChannels() {
        return maxTexChannels;
    }

    public static void setMaxTexChannels(int i) {
        maxTexChannels = i;
    }
}
