package taikodom.render.camera;

import game.geometry.Matrix3fWrap;
import game.geometry.Vec3d;
import org.mozilla.javascript.ScriptRuntime;
import taikodom.render.StepContext;
import taikodom.render.scene.SceneObject;

/* compiled from: a */
public class OrbitalCamera extends Camera {
    public final Vec3d distance = new Vec3d();
    public float maxPitch;
    public float pitch;
    public float roll;
    public float yaw;
    double maxZoomOut;
    double zoom;
    double zoomFactor;
    private SceneObject target;

    public OrbitalCamera() {
        this.distance.set(ScriptRuntime.NaN, 30.0d, 60.0d);
        this.maxPitch = 60.0f;
        this.maxZoomOut = 10.0d;
        this.zoomFactor = 1.0d;
    }

    public boolean isNeedToStep() {
        return true;
    }

    public Vec3d getDistance() {
        return this.distance;
    }

    public void setDistance(Vec3d ajr) {
        if (ajr.lengthSquared() < 0.01d) {
            this.distance.set(ScriptRuntime.NaN, ScriptRuntime.NaN, 1.0d);
        }
        this.distance.mo9484aA(ajr);
    }

    public double getZoomFactor() {
        return this.zoomFactor;
    }

    public void setZoomFactor(double d) {
        this.zoomFactor = d;
    }

    public float getRoll() {
        return this.roll;
    }

    public void setRoll(float f) {
        this.roll = f;
    }

    public float getMaxPitch() {
        return this.maxPitch;
    }

    public void setMaxPitch(float f) {
        this.maxPitch = f;
    }

    public double getZoom() {
        return this.zoom;
    }

    public void setZoom(double d) {
        this.zoom = d;
    }

    public float getYaw() {
        return this.yaw;
    }

    public void setYaw(float f) {
        this.yaw = f;
    }

    public float getPitch() {
        return this.pitch;
    }

    public void setPitch(float f) {
        this.pitch = f;
    }

    public SceneObject getTarget() {
        return this.target;
    }

    public void setTarget(SceneObject sceneObject) {
        this.target = sceneObject;
    }

    public double getMaxZoomOut() {
        return this.maxZoomOut;
    }

    public void setMaxZoomOut(double d) {
        this.maxZoomOut = d;
    }

    public int step(StepContext stepContext) {
        if (this.target != null) {
            computeTransformation(stepContext.vec3dTemp0, stepContext.matrix3fTemp0, stepContext.matrix3fTemp1);
        }
        return 0;
    }

    public void computeTransformation() {
        computeTransformation((Vec3d) null, (Matrix3fWrap) null, (Matrix3fWrap) null);
    }

    public void computeTransformation(Vec3d ajr, Matrix3fWrap ajd, Matrix3fWrap ajd2) {
        if (ajr == null) {
            ajr = new Vec3d();
        }
        if (ajd == null) {
            ajd = new Matrix3fWrap();
        }
        if (ajd2 == null) {
            ajd2 = new Matrix3fWrap();
        }
        ajr.mo9484aA(this.distance);
        ajr.normalize();
        ajr.scale(this.zoom * this.zoomFactor);
        ajr.add(this.distance);
        ajd.setIdentity();
        ajd2.setIdentity();
        ajd2.rotateY(this.yaw * 0.017453292f);
        ajd.rotateX(this.pitch * 0.017453292f);
        ajd2.mul(ajd);
        ajd2.transform(ajr);
        this.transform.orientation.mul(this.target.getGlobalTransform().orientation, ajd2);
        this.target.getGlobalTransform().mo17343b(ajr, this.transform.position);
        updateInternalGeometry((SceneObject) null);
    }
}
