package taikodom.render;

import logic.render.miles.AdapterMilesBridgeJNI;
import logic.render.miles.C1467Vb;
import logic.render.miles.C2689ic;
import logic.render.miles.C5297aDx;
import org.lwjgl.BufferUtils;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.SoundSource;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/* compiled from: a */
public class MilesSoundStream extends RenderAsset {
    public static final long SMP_DONE = 2;
    public static final long SMP_PLAYING = 4;
    public static final long SMP_STOPPED = 8;
    private static int openedStreams = 0;
    public String fileName = "";
    public FloatBuffer tempFloat = BufferUtils.createFloatBuffer(1);
    public IntBuffer tempInt = BufferUtils.createIntBuffer(1);
    float fadeInValue;
    private boolean isFadingIn;
    private boolean isFadingOut = false;
    private C2689ic stream;

    public static int getOpenedStreams() {
        return openedStreams;
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public boolean play() {
        if (this.stream != null) {
            AdapterMilesBridgeJNI.m21834b(this.stream, 0);
        } else if (!loadStream()) {
            return false;
        } else {
            openedStreams++;
            SoundSource.numSoundSources++;
            AdapterMilesBridgeJNI.m21858c(this.stream, 0);
            AdapterMilesBridgeJNI.m21870d(this.stream);
        }
        return true;
    }

    public boolean play(int i) {
        if (this.stream != null) {
            AdapterMilesBridgeJNI.m21834b(this.stream, 0);
        } else if (!loadStream()) {
            return false;
        } else {
            openedStreams++;
            SoundSource.numSoundSources++;
            AdapterMilesBridgeJNI.m21858c(this.stream, i);
            AdapterMilesBridgeJNI.m21870d(this.stream);
        }
        return true;
    }

    private boolean loadStream() {
        try {
            this.stream = AdapterMilesBridgeJNI.m21728a(SoundMiles.getInstance().getDig(), this.fileName, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (this.stream == null) {
            return false;
        }
        return true;
    }

    public void stop() {
        releaseReferences();
    }

    public void pause() {
        if (this.stream != null) {
            AdapterMilesBridgeJNI.m21834b(this.stream, 1);
        }
    }

    public void releaseReferences() {
        if (this.stream != null) {
            AdapterMilesBridgeJNI.m21833b(this.stream);
            openedStreams--;
            SoundSource.numSoundSources--;
            this.stream = null;
        }
    }

    public void setLoopCount(int i) {
        if (this.stream != null) {
            AdapterMilesBridgeJNI.m21858c(this.stream, i);
        }
    }

    public int getStatus() {
        if (this.stream != null) {
            return AdapterMilesBridgeJNI.m21884f(this.stream);
        }
        return 0;
    }

    public boolean isPlaying() {
        if (this.stream == null || AdapterMilesBridgeJNI.m21884f(this.stream) != 4) {
            return false;
        }
        return true;
    }

    public C1467Vb getMilesSample() {
        if (this.stream != null) {
            return AdapterMilesBridgeJNI.m21842c(this.stream);
        }
        return null;
    }

    public void setGain(float f) {
        C1467Vb c;
        if (this.stream != null && (c = AdapterMilesBridgeJNI.m21842c(this.stream)) != null) {
            AdapterMilesBridgeJNI.m21813b(c, f, f);
        }
    }

    public boolean fadeOut(float f) {
        C1467Vb milesSample;
        if (this.stream == null || (milesSample = getMilesSample()) == null || AdapterMilesBridgeJNI.AIL_find_filter("Volume Ramp Filter", this.tempInt) == 0) {
            return false;
        }
        this.isFadingOut = true;
        this.isFadingIn = false;
        int i = this.tempInt.get(0);
        AdapterMilesBridgeJNI.m21705a(milesSample, C5297aDx.hBQ, (long) i);
        AdapterMilesBridgeJNI.m21705a(milesSample, C5297aDx.hBQ, (long) i);
        AdapterMilesBridgeJNI.m21758a(milesSample, (Buffer) this.tempFloat, (Buffer) null);
        AdapterMilesBridgeJNI.m21668a(milesSample, C5297aDx.hBQ, 1.0f);
        AdapterMilesBridgeJNI.m21800b(milesSample, C5297aDx.hBQ, f);
        AdapterMilesBridgeJNI.m21668a(milesSample, C5297aDx.hBQ, 0.0f);
        return true;
    }

    public boolean fadeIn(float f) {
        C1467Vb milesSample;
        if (this.stream == null || (milesSample = getMilesSample()) == null || AdapterMilesBridgeJNI.AIL_find_filter("Volume Ramp Filter", this.tempInt) == 0) {
            return false;
        }
        this.isFadingIn = true;
        this.isFadingOut = false;
        int i = this.tempInt.get(0);
        AdapterMilesBridgeJNI.m21705a(milesSample, C5297aDx.hBQ, (long) i);
        AdapterMilesBridgeJNI.m21705a(milesSample, C5297aDx.hBQ, (long) i);
        AdapterMilesBridgeJNI.m21668a(milesSample, C5297aDx.hBQ, 0.0f);
        AdapterMilesBridgeJNI.m21800b(milesSample, C5297aDx.hBQ, f);
        AdapterMilesBridgeJNI.m21758a(milesSample, (Buffer) this.tempFloat, (Buffer) null);
        this.fadeInValue = 1.0f;
        AdapterMilesBridgeJNI.m21668a(milesSample, C5297aDx.hBQ, 1.0f);
        return true;
    }

    public int step(StepContext stepContext) {
        C1467Vb milesSample = getMilesSample();
        if (milesSample == null) {
            return 1;
        }
        if (((long) getStatus()) == 2) {
            releaseReferences();
            return 1;
        }
        if (this.isFadingOut) {
            this.tempFloat.put(0, 1.0f);
            AdapterMilesBridgeJNI.m21670a(milesSample, C5297aDx.hBQ, "Ramp At", (Buffer) this.tempFloat, (Buffer) null, (Buffer) null);
            if (this.tempFloat.get(0) <= 0.005f) {
                releaseReferences();
                this.isFadingOut = false;
                return 1;
            }
        } else if (this.isFadingIn) {
            this.tempFloat.put(0, 1.0f);
            AdapterMilesBridgeJNI.m21670a(milesSample, C5297aDx.hBQ, "Ramp At", (Buffer) this.tempFloat, (Buffer) null, (Buffer) null);
            if (this.tempFloat.get(0) >= this.fadeInValue - 0.005f) {
                this.isFadingIn = false;
                return 0;
            }
        }
        return 0;
    }

    public int getSamplePositionMs() {
        if (this.stream == null) {
            return 0;
        }
        AdapterMilesBridgeJNI.m21794a(this.stream, (Buffer) null, (Buffer) this.tempInt);
        return this.tempInt.get(0);
    }

    public void setSamplePositionMs(int i) {
        if (this.stream != null) {
            AdapterMilesBridgeJNI.m21889f(this.stream, i);
        }
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String str) {
        this.fileName = str;
    }
}
