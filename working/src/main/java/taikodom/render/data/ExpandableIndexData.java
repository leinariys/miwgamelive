package taikodom.render.data;

import com.sun.opengl.util.BufferUtil;

import java.nio.IntBuffer;

/* compiled from: a */
public class ExpandableIndexData extends IntIndexData {
    private int used;

    public ExpandableIndexData(int i) {
        super(i);
    }

    public ExpandableIndexData(ExpandableIndexData expandableIndexData) {
        super((IntIndexData) expandableIndexData);
        this.used = expandableIndexData.used;
    }

    public void setIndex(int i, int i2) {
        ensure(i);
        this.data.put(i, i2);
    }

    public int size() {
        return this.used;
    }

    public void clear() {
        this.data.clear();
        this.used = 0;
    }

    public void ensure(int i) {
        if (this.size <= i) {
            int max = Math.max((this.size * 3) / 2, i) + 1;
            IntBuffer newIntBuffer = BufferUtil.newIntBuffer(max);
            this.data.position(0);
            this.data.limit(this.used);
            newIntBuffer.put(this.data);
            newIntBuffer.flip();
            newIntBuffer.position(0);
            newIntBuffer.limit(max);
            this.data = newIntBuffer;
            this.size = max;
            this.used = i + 1;
        } else if (this.used <= i) {
            this.used = i + 1;
        }
    }

    public void compact() {
        IntBuffer newIntBuffer = BufferUtil.newIntBuffer(this.used);
        this.data.position(0);
        this.data.limit(this.used);
        newIntBuffer.put(this.data);
        newIntBuffer.position(0);
        newIntBuffer.limit(this.used);
        this.data = newIntBuffer;
        this.size = this.used;
    }

    public void setSize(int i) {
        this.used = i;
    }

    public IndexData duplicate() {
        return new ExpandableIndexData(this);
    }
}
