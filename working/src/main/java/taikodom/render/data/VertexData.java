package taikodom.render.data;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vector2fWrap;
import game.geometry.Vector4fWrap;
import taikodom.render.DrawContext;

/* compiled from: a */
public abstract class VertexData {
    public abstract void bind(DrawContext drawContext);

    public abstract void disposeVBO(DrawContext drawContext);

    public abstract VertexData duplicate();

    public abstract VertexLayout getLayout();

    public abstract void getNormal(int i, Vec3f vec3f);

    public abstract void getPosition(int i, Vec3f vec3f);

    public abstract void getTangents(int i, Vector4fWrap ajf);

    public abstract void getTexCoord(int i, int i2, Vector2fWrap aka);

    public abstract boolean isUseVBO();

    public abstract void setColor(int i, float f, float f2, float f3, float f4);

    public abstract void setNormal(int i, float f, float f2, float f3);

    public abstract void setPosition(int i, float f, float f2, float f3);

    public abstract void setTangents(int i, float f, float f2, float f3);

    public abstract void setTangents(int i, float f, float f2, float f3, float f4);

    public abstract void setTexCoord(int i, int i2, float f, float f2);

    public abstract void setUseVbo(boolean z);

    public abstract int size();

    public void setPosition(int i, Vec3f vec3f) {
        setPosition(i, vec3f.x, vec3f.y, vec3f.z);
    }

    public void setTangents(int i, Vector4fWrap ajf) {
        setTangents(i, ajf.x, ajf.y, ajf.z, ajf.w);
    }

    public void setNormal(int i, Vec3f vec3f) {
        setNormal(i, vec3f.x, vec3f.y, vec3f.z);
    }
}
