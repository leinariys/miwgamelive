package taikodom.render.data;

import com.sun.opengl.util.BufferUtil;
import taikodom.render.DrawContext;

import javax.media.opengl.GL;
import java.nio.Buffer;
import java.nio.ShortBuffer;

/* compiled from: a */
public class ShortIndexData extends IndexData {
    private int VBO;
    private ShortBuffer data;
    private int glType = 5123;
    private boolean vboCreated = false;

    public ShortIndexData(int i) {
        this.data = BufferUtil.newShortBuffer(i);
        this.size = i;
    }

    public ShortIndexData(ShortIndexData shortIndexData) {
        this.size = shortIndexData.size;
        this.data = BufferUtil.newShortBuffer(this.size);
        shortIndexData.data.clear();
        this.data.clear();
        this.data.put(shortIndexData.data);
        shortIndexData.data.clear();
        this.data.clear();
    }

    public void setIndex(int i, int i2) {
        this.data.put(i, (short) i2);
    }

    public int getIndex(int i) {
        return this.data.get(i);
    }

    public int getGLType() {
        return this.glType;
    }

    public Buffer buffer() {
        if (!this.useVbo || !this.vboCreated) {
            return this.data;
        }
        return null;
    }

    public IndexData subSet(int i, int i2) {
        ShortIndexData shortIndexData = new ShortIndexData(i2);
        this.data.position(i);
        this.data.limit(i + i2);
        shortIndexData.data.put(this.data);
        shortIndexData.data.clear();
        this.data.clear();
        return shortIndexData;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.data.capacity(); i++) {
            sb.append(this.data.get(i)).append(", ");
        }
        return sb.toString();
    }

    public void resize(int i) {
        if (i > this.data.capacity()) {
            ShortBuffer newShortBuffer = BufferUtil.newShortBuffer(i);
            this.size = i;
            newShortBuffer.put(this.data);
            newShortBuffer.clear();
            this.data = newShortBuffer;
        }
    }

    public void clear() {
        this.data.clear();
        this.size = 0;
    }

    public void bind(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        if (!drawContext.isUseVbo()) {
            this.useVbo = false;
        }
        if (this.useVbo) {
            if (!this.vboCreated) {
                buildVBO(drawContext);
            }
            gl.glBindBufferARB(34963, this.VBO);
            return;
        }
        gl.glBindBufferARB(34963, 0);
    }

    private void buildVBO(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        if (this.data != null) {
            int[] iArr = new int[1];
            gl.glGenBuffersARB(1, iArr, 0);
            this.VBO = iArr[0];
            this.vboCreated = true;
            gl.glBindBufferARB(34963, iArr[0]);
            gl.glBufferDataARB(34963, this.size * 2, this.data, 35044);
            gl.glBindBufferARB(34963, 0);
            this.vboCreated = true;
            return;
        }
        this.useVbo = false;
    }

    public IndexData duplicate() {
        return new ShortIndexData(this);
    }

    public boolean isVBO() {
        return this.VBO != 0;
    }
}
