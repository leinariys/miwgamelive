package taikodom.render.data;

/* compiled from: a */
public class VertexLayout {
    public static final VertexLayout defaultLayout = new VertexLayout(true, true, true, true, 1);
    private final int binormalsOffset;
    private final int colorOffset;
    private final boolean hasColor;
    private final boolean hasNormals;
    private final boolean hasPosition;
    private final boolean hasTangents;
    private final int normalsOffset;
    private final int positionOffset;
    private final int stride;
    private final int tangentsOffset;
    private final int texCoordCount;
    private final int texCoordOffset;

    public VertexLayout(boolean z, boolean z2, boolean z3, boolean z4, int i) {
        int i2 = 0;
        this.hasPosition = z;
        this.hasColor = z2;
        this.hasTangents = z3;
        this.hasNormals = z4;
        this.texCoordCount = i;
        if (z) {
            this.positionOffset = 0;
            i2 = 12;
        } else {
            this.positionOffset = -1;
        }
        if (z2) {
            this.colorOffset = i2;
            i2 += 16;
        } else {
            this.colorOffset = -1;
        }
        if (z3) {
            this.tangentsOffset = i2;
            i2 += 16;
        } else {
            this.tangentsOffset = -1;
        }
        if (z4) {
            this.normalsOffset = i2;
            i2 += 12;
        } else {
            this.normalsOffset = -1;
        }
        if (i > 0) {
            this.texCoordOffset = i2;
            i2 += i * 8;
        } else {
            this.texCoordOffset = -1;
        }
        this.binormalsOffset = -1;
        this.stride = i2;
    }

    public int stride() {
        return this.stride;
    }

    public int positionOffset() {
        return this.positionOffset;
    }

    public int colorOffset() {
        return this.colorOffset;
    }

    public int tangentsOffset() {
        return this.tangentsOffset;
    }

    public int normalsOffset() {
        return this.normalsOffset;
    }

    public int texCoordOffset() {
        return this.texCoordOffset;
    }

    public int texCoordCount() {
        return this.texCoordCount;
    }

    public int colorSize() {
        return 4;
    }

    public int texCoordOffset(int i) {
        return (i * 4 * 2) + this.texCoordOffset;
    }

    public boolean isHasPosition() {
        return this.hasPosition;
    }

    public boolean isHasColor() {
        return this.hasColor;
    }

    public boolean isHasTangents() {
        return this.hasTangents;
    }

    public boolean hasNormals() {
        return this.hasNormals;
    }
}
