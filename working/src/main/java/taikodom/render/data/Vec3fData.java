package taikodom.render.data;

import com.hoplon.geometry.Vec3f;

import java.nio.FloatBuffer;

/* compiled from: a */
public class Vec3fData extends Data {
    public Vec3fData(int i) {
        super(i * 3);
    }

    public Vec3fData(FloatBuffer floatBuffer) {
        super(floatBuffer);
    }

    public void set(int i, float f, float f2, float f3) {
        int i2 = i * 3;
        this.data.put(i2, f);
        this.data.put(i2 + 1, f2);
        this.data.put(i2 + 2, f3);
    }

    public void set(int i, Vec3f vec3f) {
        int i2 = i * 3;
        this.data.put(i2, vec3f.x);
        this.data.put(i2 + 1, vec3f.y);
        this.data.put(i2 + 2, vec3f.z);
    }

    public void get(int i, Vec3f vec3f) {
        int i2 = i * 3;
        vec3f.x = this.data.get(i2);
        vec3f.y = this.data.get(i2 + 1);
        vec3f.z = this.data.get(i2 + 2);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.data.capacity(); i += 3) {
            sb.append("(");
            sb.append(this.data.get(i)).append(", ");
            sb.append(this.data.get(i + 1)).append(", ");
            sb.append(this.data.get(i + 2));
            sb.append("), ");
        }
        return sb.toString();
    }

    public Vec3fData duplicate() {
        this.data.clear();
        Vec3fData vec3fData = new Vec3fData(this.data.capacity() / 3);
        vec3fData.buffer().put(this.data);
        vec3fData.buffer().clear();
        this.data.clear();
        return vec3fData;
    }
}
