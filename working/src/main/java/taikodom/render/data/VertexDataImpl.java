package taikodom.render.data;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vector2fWrap;
import game.geometry.Vector4fWrap;
import taikodom.render.DrawContext;
import taikodom.render.PrimitiveDrawingState;

import javax.media.opengl.GL;
import java.nio.Buffer;

/* compiled from: a */
public class VertexDataImpl extends VertexData {
    private final VertexLayout layout;
    public Vec4fData colors;
    public int colorsOffset;
    public Vec3fData normals;
    public int normalsOffset;
    public Vec4fData tangents;
    public int tangentsOffset;
    public Vec2fData[] texCoords;
    public int texCoordsOffset;
    public int vertexCount;
    public Vec3fData vertices;
    public int verticesOffset;
    private int VBO;
    private int[] texCoordsOffsets;
    private int totalAllocatedBytes;
    private boolean useVBO = false;
    private boolean vboCreated;

    public VertexDataImpl(VertexLayout vertexLayout, int i) {
        int i2;
        this.layout = vertexLayout;
        this.vertexCount = i;
        if (vertexLayout.isHasPosition()) {
            this.vertices = new Vec3fData(i);
            i2 = (this.vertices.buffer().capacity() * 4) + 0;
        } else {
            i2 = 0;
        }
        if (vertexLayout.isHasTangents()) {
            this.tangents = new Vec4fData(i);
            i2 += this.tangents.buffer().capacity() * 4;
        }
        if (vertexLayout.hasNormals()) {
            this.normals = new Vec3fData(i);
            i2 += this.normals.buffer().capacity() * 4;
        }
        if (vertexLayout.isHasColor()) {
            this.colors = new Vec4fData(i);
            i2 += this.colors.buffer().capacity() * 4;
        }
        this.texCoords = new Vec2fData[vertexLayout.texCoordCount()];
        for (int i3 = 0; i3 < vertexLayout.texCoordCount(); i3++) {
            this.texCoords[i3] = new Vec2fData(i);
            i2 += this.texCoords[i3].buffer().capacity() * 4;
        }
        this.totalAllocatedBytes = i2;
    }

    public VertexDataImpl(VertexDataImpl vertexDataImpl) {
        int i;
        this.layout = vertexDataImpl.getLayout();
        this.vertexCount = vertexDataImpl.size();
        if (this.layout.isHasPosition()) {
            this.vertices = vertexDataImpl.getVertices().duplicate();
            i = (this.vertices.buffer().capacity() * 4) + 0;
        } else {
            i = 0;
        }
        if (this.layout.isHasTangents()) {
            this.tangents = vertexDataImpl.getTangents().duplicate();
            i += this.tangents.buffer().capacity() * 4;
        }
        if (this.layout.hasNormals()) {
            this.normals = vertexDataImpl.getNormals().duplicate();
            i += this.normals.buffer().capacity() * 4;
        }
        if (this.layout.isHasColor()) {
            this.colors = vertexDataImpl.getColors().duplicate();
            i += this.colors.buffer().capacity() * 4;
        }
        this.texCoords = new Vec2fData[this.layout.texCoordCount()];
        for (int i2 = 0; i2 < this.layout.texCoordCount(); i2++) {
            this.texCoords[i2] = vertexDataImpl.getTexCoords()[i2].duplicate();
            i += this.texCoords[i2].buffer().capacity() * 4;
        }
        this.totalAllocatedBytes = i;
    }

    public void bind(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        PrimitiveDrawingState primitiveDrawingState = drawContext.primitiveState;
        if (!drawContext.isUseVbo()) {
            this.useVBO = false;
        }
        if (this.useVBO) {
            if (!this.vboCreated) {
                buildVBO(drawContext);
            }
            gl.glBindBufferARB(34962, this.VBO);
            if (this.layout.positionOffset() >= 0) {
                if (this.verticesOffset != -1) {
                    gl.glVertexPointer(3, 5126, 0, (long) this.verticesOffset);
                } else {
                    throw new RuntimeException("This mesh doesnt have position");
                }
            }
            if (primitiveDrawingState.isUseNormalArray()) {
                if (this.normalsOffset != -1) {
                    gl.glNormalPointer(5126, 0, (long) this.normalsOffset);
                } else {
                    throw new RuntimeException("This mesh doesnt have normals");
                }
            }
            if (primitiveDrawingState.isUseColorArray()) {
                if (this.colorsOffset != -1) {
                    gl.glColorPointer(this.layout.colorSize(), 5126, 0, (long) this.colorsOffset);
                } else {
                    throw new RuntimeException("This mesh doesnt have colors");
                }
            }
            if (primitiveDrawingState.isUseTangentArray()) {
                if (this.tangentsOffset != -1) {
                    gl.glVertexAttribPointer(primitiveDrawingState.getTangentChannel(), 4, 5126, false, 0, (long) this.tangentsOffset);
                } else {
                    throw new RuntimeException("This mesh doesnt have tangents");
                }
            }
            if (this.layout.texCoordCount() > 0) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= primitiveDrawingState.getNumTextures()) {
                        gl.glClientActiveTexture(33984);
                        return;
                    }
                    int channelMapping = primitiveDrawingState.channelMapping(i2);
                    int texCoordsSets = primitiveDrawingState.texCoordsSets(channelMapping);
                    gl.glClientActiveTexture(channelMapping + 33984);
                    if (this.layout.texCoordCount() > texCoordsSets && this.texCoordsOffsets[texCoordsSets] != -1) {
                        gl.glTexCoordPointer(2, 5126, 0, (long) this.texCoordsOffsets[texCoordsSets]);
                    } else if (this.texCoordsOffsets[0] != -1) {
                        gl.glTexCoordPointer(2, 5126, 0, (long) this.texCoordsOffsets[0]);
                    }
                    i = i2 + 1;
                }
            }
        } else if (this.vertices != null) {
            gl.glVertexPointer(3, 5126, 0, this.vertices.buffer());
            if (primitiveDrawingState.isUseNormalArray()) {
                if (this.normals != null) {
                    gl.glNormalPointer(5126, 0, this.normals.buffer());
                } else {
                    throw new RuntimeException("This mesh doesnt have normals");
                }
            }
            if (primitiveDrawingState.isUseColorArray()) {
                if (this.colors != null) {
                    gl.glColorPointer(4, 5126, 0, this.colors.buffer());
                } else {
                    throw new RuntimeException("This mesh doesnt have colors");
                }
            }
            if (primitiveDrawingState.isUseTangentArray()) {
                if (this.tangents != null) {
                    gl.glVertexAttribPointer(primitiveDrawingState.getTangentChannel(), 4, 5126, false, 0, this.tangents.buffer());
                } else {
                    throw new RuntimeException("This mesh doesnt have tangents");
                }
            }
            if (this.texCoords != null && this.texCoords.length > 0) {
                for (int i3 = 0; i3 < primitiveDrawingState.getNumTextures(); i3++) {
                    int channelMapping2 = primitiveDrawingState.channelMapping(i3);
                    int texCoordsSets2 = primitiveDrawingState.texCoordsSets(channelMapping2);
                    gl.glClientActiveTexture(channelMapping2 + 33984);
                    if (this.layout.texCoordCount() <= texCoordsSets2 || i3 >= this.texCoords.length || this.texCoords[i3] == null) {
                        gl.glTexCoordPointer(2, 5126, 0, this.texCoords[0].buffer());
                    } else {
                        gl.glTexCoordPointer(2, 5126, 0, this.texCoords[i3].buffer());
                    }
                }
                gl.glClientActiveTexture(33984);
            }
        } else {
            throw new RuntimeException("This mesh doesnt have position");
        }
    }

    public void setColor(int i, float f, float f2, float f3, float f4) {
        this.colors.set(i, f, f2, f3, f4);
    }

    public void setNormal(int i, float f, float f2, float f3) {
        this.normals.set(i, f, f2, f3);
    }

    public void setPosition(int i, float f, float f2, float f3) {
        this.vertices.set(i, f, f2, f3);
    }

    public void setTangents(int i, float f, float f2, float f3, float f4) {
        this.tangents.set(i, f, f2, f3, f4);
    }

    public void setTangents(int i, float f, float f2, float f3) {
        this.tangents.set(i, f, f2, f3);
    }

    public void setTexCoord(int i, int i2, float f, float f2) {
        this.texCoords[i2].set(i, f, f2);
    }

    public int size() {
        return this.vertexCount;
    }

    public void getPosition(int i, Vec3f vec3f) {
        this.vertices.get(i, vec3f);
    }

    public Vec3fData getVertices() {
        return this.vertices;
    }

    public Vec3fData getNormals() {
        return this.normals;
    }

    public void setNormals(Vec3fData vec3fData) {
        this.totalAllocatedBytes -= vec3fData.buffer().capacity() * 4;
        this.normals = vec3fData;
        this.totalAllocatedBytes += vec3fData.buffer().capacity() * 4;
    }

    public Vec4fData getTangents() {
        return this.tangents;
    }

    public void setTangents(Vec4fData vec4fData) {
        this.totalAllocatedBytes -= vec4fData.buffer().capacity() * 4;
        this.tangents = vec4fData;
        this.totalAllocatedBytes += vec4fData.buffer().capacity() * 4;
    }

    public Vec4fData getColors() {
        return this.colors;
    }

    public Vec2fData[] getTexCoords() {
        return this.texCoords;
    }

    private void buildVBO(DrawContext drawContext) {
        int i;
        GL gl = drawContext.getGl();
        int[] iArr = new int[1];
        gl.glGenBuffersARB(1, iArr, 0);
        this.VBO = iArr[0];
        this.vboCreated = true;
        gl.glBindBufferARB(34962, iArr[0]);
        gl.glBufferDataARB(34962, this.totalAllocatedBytes, (Buffer) null, 35044);
        if (this.vertices != null) {
            gl.glBufferSubDataARB(34962, 0, this.vertices.byteSize(), this.vertices.buffer());
            this.verticesOffset = 0;
            i = this.vertices.byteSize() + 0;
        } else {
            i = 0;
        }
        if (this.normals != null) {
            gl.glBufferSubDataARB(34962, i, this.normals.byteSize(), this.normals.buffer());
            this.normalsOffset = i;
            i += this.normals.byteSize();
        }
        if (this.tangents != null) {
            gl.glBufferSubDataARB(34962, i, this.tangents.byteSize(), this.tangents.buffer());
            this.tangentsOffset = i;
            i += this.tangents.byteSize();
        }
        if (this.colors != null) {
            gl.glBufferSubDataARB(34962, i, this.colors.byteSize(), this.colors.buffer());
            this.colorsOffset = i;
            i += this.colors.byteSize();
        }
        if (this.texCoords != null && this.texCoords.length > 0) {
            this.texCoordsOffsets = new int[this.texCoords.length];
            int i2 = i;
            for (int i3 = 0; i3 < this.texCoordsOffsets.length; i3++) {
                if (this.texCoords[i3] != null) {
                    gl.glBufferSubDataARB(34962, i2, this.texCoords[i3].byteSize(), this.texCoords[i3].buffer());
                    this.texCoordsOffsets[i3] = i2;
                    i2 += this.texCoords[i3].byteSize();
                } else {
                    this.texCoordsOffsets[i3] = -1;
                }
            }
        }
        gl.glBindBufferARB(34962, 0);
    }

    public void disposeVBO(DrawContext drawContext) {
        if (this.vboCreated) {
            drawContext.getGl().glDeleteBuffersARB(1, new int[]{this.VBO}, 0);
            this.vboCreated = false;
            this.VBO = 0;
        }
    }

    public void setUseVbo(boolean z) {
        this.useVBO = z;
    }

    public boolean isUseVBO() {
        return this.useVBO;
    }

    public void getNormal(int i, Vec3f vec3f) {
        this.normals.get(i, vec3f);
    }

    public void getTexCoord(int i, int i2, Vector2fWrap aka) {
        this.texCoords[i2].get(i, aka);
    }

    public void getTangents(int i, Vector4fWrap ajf) {
        this.tangents.get(i, ajf);
    }

    public VertexLayout getLayout() {
        return this.layout;
    }

    public VertexData duplicate() {
        return new VertexDataImpl(this);
    }
}
