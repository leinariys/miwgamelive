package taikodom.render.data;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vector4fWrap;

/* compiled from: a */
public class Vec4fData extends Data {
    public Vec4fData(int i) {
        super(i * 4);
    }

    public void set(int i, float f, float f2, float f3, float f4) {
        int i2 = i * 4;
        this.data.put(i2, f);
        this.data.put(i2 + 1, f2);
        this.data.put(i2 + 2, f3);
        this.data.put(i2 + 3, f4);
    }

    public void set(int i, Vector4fWrap ajf) {
        int i2 = i * 4;
        this.data.put(i2, ajf.x);
        this.data.put(i2 + 1, ajf.y);
        this.data.put(i2 + 2, ajf.z);
        this.data.put(i2 + 3, ajf.w);
    }

    public void set(int i, float f, float f2, float f3) {
        int i2 = i * 4;
        this.data.put(i2, f);
        this.data.put(i2 + 1, f2);
        this.data.put(i2 + 2, f3);
    }

    public void setW(int i, float f) {
        this.data.put((i * 4) + 3, f);
    }

    public void get(int i, Vector4fWrap ajf) {
        int i2 = i * 4;
        ajf.x = this.data.get(i2);
        ajf.y = this.data.get(i2 + 1);
        ajf.z = this.data.get(i2 + 2);
        ajf.w = this.data.get(i2 + 3);
    }

    public void getVec3(int i, Vec3f vec3f) {
        int i2 = i * 4;
        vec3f.x = this.data.get(i2);
        vec3f.y = this.data.get(i2 + 1);
        vec3f.z = this.data.get(i2 + 2);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.data.capacity(); i += 4) {
            sb.append("(");
            sb.append(this.data.get(i)).append(", ");
            sb.append(this.data.get(i + 1)).append(", ");
            sb.append(this.data.get(i + 2)).append(", ");
            sb.append(this.data.get(i + 3));
            sb.append("), ");
        }
        return sb.toString();
    }

    public Vec4fData duplicate() {
        this.data.clear();
        Vec4fData vec4fData = new Vec4fData(this.data.capacity() / 4);
        vec4fData.buffer().put(this.data);
        vec4fData.buffer().clear();
        this.data.clear();
        return vec4fData;
    }
}
