package taikodom.render.partitioning;

import game.geometry.Vec3d;
import taikodom.render.RenderContext;
import taikodom.render.scene.SceneObject;

import javax.media.opengl.GL;

/* compiled from: a */
public abstract class RenderZone {
    public abstract void addChild(SceneObject sceneObject);

    public abstract void addStaticChild(SceneObject sceneObject);

    public abstract void cullZone(RenderContext renderContext, boolean z);

    public abstract void findStepableObjects(SceneObject sceneObject);

    public abstract void onObjectTransform(SceneObject sceneObject);

    public abstract void rebuildUp();

    public abstract void removeAll();

    public abstract void removeChild(SceneObject sceneObject);

    public abstract void removeStepableObjects(SceneObject sceneObject);

    public abstract void renderGL(GL gl, Vec3d ajr);
}
