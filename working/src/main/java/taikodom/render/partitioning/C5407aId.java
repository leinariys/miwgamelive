package taikodom.render.partitioning;

import taikodom.render.helpers.RenderVisitor;
import taikodom.render.scene.SceneObject;

/* renamed from: a.aId  reason: case insensitive filesystem */
/* compiled from: a */
public class C5407aId implements RenderVisitor {
    final /* synthetic */ TkdWorld iaK;

    public C5407aId(TkdWorld tkdWorld) {
        this.iaK = tkdWorld;
    }

    public boolean visit(SceneObject sceneObject) {
        if (!sceneObject.isNeedToStep()) {
            return true;
        }
        this.iaK.stepableObjects.remove(sceneObject);
        return true;
    }
}
