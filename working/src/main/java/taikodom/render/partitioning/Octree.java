package taikodom.render.partitioning;

import game.geometry.Vec3d;
import game.geometry.aLH;
import taikodom.render.RenderContext;
import taikodom.render.scene.SceneObject;

import javax.media.opengl.GL;
import javax.vecmath.Tuple3d;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* compiled from: a */
public class Octree extends RenderZone {
    private final aLH aabb = new aLH();
    private final Vec3d center = new Vec3d();
    private final Vec3d defaultHalfSize = new Vec3d();
    private Octree[] children = new Octree[8];
    private int depth;
    private boolean empty;
    private boolean leaf;
    private boolean mainNode = false;
    private List<SceneObject> objects = new ArrayList();
    private RenderZone parent;

    public Octree(RenderZone renderZone, int i, Vec3d ajr, Vec3d ajr2) {
        this.parent = renderZone;
        this.center.mo9484aA(ajr);
        this.defaultHalfSize.mo9484aA(ajr2);
        this.defaultHalfSize.scale(0.5d);
        this.empty = true;
        this.leaf = false;
        this.depth = i;
        if (i == 0) {
            this.leaf = true;
            return;
        }
        int i2 = i - 1;
        Vec3d ajr3 = new Vec3d(this.defaultHalfSize);
        double d = ajr2.x / 4.0d;
        double d2 = ajr2.y / 4.0d;
        double d3 = ajr2.z / 4.0d;
        this.children[0] = new Octree(this, i2, ajr.mo9557y(-d, -d2, -d3), ajr3);
        this.children[1] = new Octree(this, i2, ajr.mo9557y(d, -d2, -d3), ajr3);
        this.children[2] = new Octree(this, i2, ajr.mo9557y(d, -d2, d3), ajr3);
        this.children[3] = new Octree(this, i2, ajr.mo9557y(-d, -d2, d3), ajr3);
        this.children[4] = new Octree(this, i2, ajr.mo9557y(-d, d2, -d3), ajr3);
        this.children[5] = new Octree(this, i2, ajr.mo9557y(d, d2, -d3), ajr3);
        this.children[6] = new Octree(this, i2, ajr.mo9557y(d, d2, d3), ajr3);
        this.children[7] = new Octree(this, i2, ajr.mo9557y(-d, d2, d3), ajr3);
    }

    public void addChild(SceneObject sceneObject) {
        if (this.leaf) {
            this.empty = false;
            this.objects.add(sceneObject);
            sceneObject.setRenderZone(this);
            rebuildUp();
            return;
        }
        Vec3d zO = sceneObject.getAabbWorldSpace().mo9875zO();
        if (zO.x < this.center.x) {
            if (zO.y < this.center.y) {
                if (zO.z < this.center.z) {
                    this.children[0].addChild(sceneObject);
                } else {
                    this.children[3].addChild(sceneObject);
                }
            } else if (zO.z < this.center.z) {
                this.children[4].addChild(sceneObject);
            } else {
                this.children[7].addChild(sceneObject);
            }
        } else if (zO.y < this.center.y) {
            if (zO.z < this.center.z) {
                this.children[1].addChild(sceneObject);
            } else {
                this.children[2].addChild(sceneObject);
            }
        } else if (zO.z < this.center.z) {
            this.children[5].addChild(sceneObject);
        } else {
            this.children[6].addChild(sceneObject);
        }
    }

    public void addStaticChild(SceneObject sceneObject) {
        addChild(sceneObject);
    }

    public void cullZone(RenderContext renderContext, boolean z) {
        boolean z2;
        boolean z3 = false;
        renderContext.getDc().incOctreeNodesVisited();
        if (this.leaf) {
            int size = this.objects.size() - 1;
            while (size >= 0) {
                SceneObject sceneObject = this.objects.get(size);
                if (sceneObject.isDisposed()) {
                    removeButNotRebuild(sceneObject);
                    z2 = true;
                } else {
                    sceneObject.fillRenderQueue(renderContext, z);
                    z2 = z3;
                }
                size--;
                z3 = z2;
            }
            if (z3) {
                rebuildUp();
                return;
            }
            return;
        }
        for (int i = 0; i < 8; i++) {
            Octree octree = this.children[i];
            if (!octree.isEmpty()) {
                if (z) {
                    int b = renderContext.getFrustum().mo1162b(octree.getAABB());
                    if (b == 1) {
                        octree.cullZone(renderContext, true);
                    } else if (b == 2) {
                        octree.cullZone(renderContext, false);
                    }
                } else {
                    octree.cullZone(renderContext, false);
                }
            }
        }
    }

    private boolean removeButNotRebuild(SceneObject sceneObject) {
        sceneObject.setRenderZone((RenderZone) null);
        if (!this.objects.remove(sceneObject)) {
            return false;
        }
        if (this.objects.size() != 0) {
            return true;
        }
        this.empty = true;
        return true;
    }

    private void rebuildNode() {
        this.aabb.reset();
        if (!this.leaf) {
            this.empty = true;
            for (int i = 0; i < 8; i++) {
                if (!this.children[i].isEmpty()) {
                    this.aabb.mo9868h(this.children[i].getAABB());
                    this.empty = false;
                }
            }
        }
        if (!this.empty) {
            LinkedList linkedList = new LinkedList();
            for (int i2 = 0; i2 < this.objects.size(); i2++) {
                linkedList.add(this.objects.get(i2));
            }
            for (int i3 = 0; i3 < linkedList.size(); i3++) {
                SceneObject sceneObject = (SceneObject) linkedList.get(i3);
                for (int i4 = 0; i4 < sceneObject.childCount(); i4++) {
                    linkedList.add(sceneObject.getChild(i4));
                }
                this.aabb.mo9868h(((SceneObject) linkedList.get(i3)).getAabbWorldSpace());
            }
            linkedList.clear();
        }
    }

    public void findStepableObjects(SceneObject sceneObject) {
        if (this.parent != null) {
            this.parent.findStepableObjects(sceneObject);
        }
    }

    public void removeStepableObjects(SceneObject sceneObject) {
        if (this.parent != null) {
            this.parent.removeStepableObjects(sceneObject);
        }
    }

    public void onObjectTransform(SceneObject sceneObject) {
        Vec3d f = sceneObject.getAabbWorldSpace().mo9875zO().mo9517f((Tuple3d) this.center);
        if (Math.abs(f.x) > this.defaultHalfSize.x || Math.abs(f.y) > this.defaultHalfSize.y || Math.abs(f.z) > this.defaultHalfSize.z) {
            if (sceneObject.getRenderZone() == this) {
                removeButNotRebuild(sceneObject);
            }
            rebuildNode();
            if (!this.mainNode && this.parent != null) {
                this.parent.onObjectTransform(sceneObject);
            } else if (this.parent != null) {
                this.parent.addStaticChild(sceneObject);
            } else {
                addStaticChild(sceneObject);
            }
        } else if (sceneObject.getRenderZone() == this) {
            rebuildUp();
        } else {
            addChild(sceneObject);
        }
    }

    public void rebuildUp() {
        rebuildNode();
        if (this.parent != null) {
            this.parent.rebuildUp();
        }
    }

    public void removeAll() {
        for (int i = 0; i < this.objects.size(); i++) {
            this.objects.get(i).setRenderZone((RenderZone) null);
        }
        this.objects.clear();
        this.empty = true;
        if (!this.leaf) {
            for (int i2 = 0; i2 < 8; i2++) {
                if (!this.children[i2].isEmpty()) {
                    this.children[i2].removeAll();
                }
            }
        }
    }

    public void removeChild(SceneObject sceneObject) {
        if (removeButNotRebuild(sceneObject)) {
            rebuildUp();
        }
    }

    public boolean isPointInside(Vec3d ajr) {
        Vec3d f = this.center.mo9517f((Tuple3d) ajr);
        if (Math.abs(f.x) >= this.defaultHalfSize.x || Math.abs(f.y) >= this.defaultHalfSize.y || Math.abs(f.z) >= this.defaultHalfSize.z) {
            return false;
        }
        return true;
    }

    public void setMainNode(boolean z) {
        this.mainNode = z;
    }

    public void dispose() {
        removeAll();
        if (!this.leaf) {
            for (int i = 0; i < 8; i++) {
                this.children[i].dispose();
            }
        }
    }

    public boolean isEmpty() {
        return this.empty;
    }

    public aLH getAABB() {
        return this.aabb;
    }

    public void renderGL(GL gl, Vec3d ajr) {
        if (!this.empty) {
            gl.glColor3f(1.0f, 0.5f + (((float) this.depth) * 0.1f), ((float) this.depth) * 0.2f);
            Vec3d f = this.aabb.dim().mo9517f((Tuple3d) ajr);
            Vec3d f2 = this.aabb.din().mo9517f((Tuple3d) ajr);
            gl.glVertex3d(f.x, f.y, f.z);
            gl.glVertex3d(f2.x, f.y, f.z);
            gl.glVertex3d(f.x, f.y, f.z);
            gl.glVertex3d(f.x, f2.y, f.z);
            gl.glVertex3d(f.x, f.y, f.z);
            gl.glVertex3d(f.x, f.y, f2.z);
            gl.glVertex3d(f2.x, f2.y, f2.z);
            gl.glVertex3d(f.x, f2.y, f2.z);
            gl.glVertex3d(f2.x, f2.y, f2.z);
            gl.glVertex3d(f2.x, f.y, f2.z);
            gl.glVertex3d(f2.x, f2.y, f2.z);
            gl.glVertex3d(f2.x, f2.y, f.z);
            gl.glVertex3d(f.x, f2.y, f.z);
            gl.glVertex3d(f2.x, f2.y, f.z);
            gl.glVertex3d(f.x, f2.y, f.z);
            gl.glVertex3d(f.x, f2.y, f2.z);
            gl.glVertex3d(f.x, f2.y, f2.z);
            gl.glVertex3d(f.x, f.y, f2.z);
            gl.glVertex3d(f.x, f.y, f2.z);
            gl.glVertex3d(f2.x, f.y, f2.z);
            gl.glVertex3d(f2.x, f.y, f.z);
            gl.glVertex3d(f2.x, f.y, f2.z);
            gl.glVertex3d(f2.x, f.y, f.z);
            gl.glVertex3d(f2.x, f2.y, f.z);
            if (!this.leaf) {
                for (int i = 0; i < 8; i++) {
                    this.children[i].renderGL(gl, ajr);
                }
            }
            if (this.mainNode) {
                gl.glColor3f(0.0f, 1.0f, 1.0f);
                Vec3d f3 = this.center.mo9517f((Tuple3d) this.defaultHalfSize);
                f3.sub(ajr);
                Vec3d d = this.center.mo9504d((Tuple3d) this.defaultHalfSize);
                d.sub(ajr);
                gl.glVertex3d(f3.x, f3.y, f3.z);
                gl.glVertex3d(d.x, f3.y, f3.z);
                gl.glVertex3d(f3.x, f3.y, f3.z);
                gl.glVertex3d(f3.x, d.y, f3.z);
                gl.glVertex3d(f3.x, f3.y, f3.z);
                gl.glVertex3d(f3.x, f3.y, d.z);
                gl.glVertex3d(d.x, d.y, d.z);
                gl.glVertex3d(f3.x, d.y, d.z);
                gl.glVertex3d(d.x, d.y, d.z);
                gl.glVertex3d(d.x, f3.y, d.z);
                gl.glVertex3d(d.x, d.y, d.z);
                gl.glVertex3d(d.x, d.y, f3.z);
                gl.glVertex3d(f3.x, d.y, f3.z);
                gl.glVertex3d(d.x, d.y, f3.z);
                gl.glVertex3d(f3.x, d.y, f3.z);
                gl.glVertex3d(f3.x, d.y, d.z);
                gl.glVertex3d(f3.x, d.y, d.z);
                gl.glVertex3d(f3.x, f3.y, d.z);
                gl.glVertex3d(f3.x, f3.y, d.z);
                gl.glVertex3d(d.x, f3.y, d.z);
                gl.glVertex3d(d.x, f3.y, f3.z);
                gl.glVertex3d(d.x, f3.y, d.z);
                gl.glVertex3d(d.x, f3.y, f3.z);
                gl.glVertex3d(d.x, d.y, f3.z);
            }
        }
    }
}
