package taikodom.render.partitioning;

import game.geometry.Vec3d;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;
import taikodom.render.helpers.RenderVisitor;
import taikodom.render.scene.SceneObject;

import javax.media.opengl.GL;
import java.util.ArrayList;
import java.util.List;

/* compiled from: a */
public class TkdWorld extends RenderZone {
    /* access modifiers changed from: private */
    public List<SceneObject> stepableObjects = new ArrayList();
    RenderVisitor findSteppablesVisitor = new C5406aIc(this);
    RenderVisitor removeSteppablesVisitor = new C5407aId(this);
    private int cellDepth = 2;
    private float cellSize = 20000.0f;
    private List<SceneObject> renderableObjects = new ArrayList();
    private List<Octree> sparseCells = new ArrayList();

    private Octree findCell(Vec3d ajr) {
        for (Octree next : this.sparseCells) {
            if (next.isPointInside(ajr)) {
                return next;
            }
        }
        Octree octree = new Octree(this, this.cellDepth, new Vec3d(((double) this.cellSize) * Math.floor((ajr.x / ((double) this.cellSize)) + 0.5d), ((double) this.cellSize) * Math.floor((ajr.y / ((double) this.cellSize)) + 0.5d), ((double) this.cellSize) * Math.floor((ajr.z / ((double) this.cellSize)) + 0.5d)), new Vec3d((double) this.cellSize, (double) this.cellSize, (double) this.cellSize));
        octree.setMainNode(true);
        this.sparseCells.add(octree);
        return octree;
    }

    private void destroyCells() {
        for (Octree dispose : this.sparseCells) {
            dispose.dispose();
        }
        this.sparseCells.clear();
    }

    public void addChild(SceneObject sceneObject) {
        this.renderableObjects.add(sceneObject);
        sceneObject.setRenderZone(this);
        findStepableObjects(sceneObject);
    }

    public void addStaticChild(SceneObject sceneObject) {
        Octree findCell = findCell(sceneObject.getAabbWorldSpace().mo9875zO());
        if (findCell != null) {
            findCell.addStaticChild(sceneObject);
        } else {
            addChild(sceneObject);
        }
        findStepableObjects(sceneObject);
    }

    public void cullZone(RenderContext renderContext, boolean z) {
        for (Octree next : this.sparseCells) {
            next.isEmpty();
            int b = renderContext.getFrustum().mo1162b(next.getAABB());
            if (b == 1) {
                next.cullZone(renderContext, true);
            } else if (b == 2) {
                next.cullZone(renderContext, false);
            }
        }
        for (int size = this.renderableObjects.size() - 1; size >= 0; size--) {
            SceneObject sceneObject = this.renderableObjects.get(size);
            if (sceneObject.isDisposed()) {
                sceneObject.setRenderZone((RenderZone) null);
                this.renderableObjects.remove(size);
            } else {
                sceneObject.fillRenderQueue(renderContext, true);
            }
        }
    }

    public void findStepableObjects(SceneObject sceneObject) {
        sceneObject.visitPreOrder(this.findSteppablesVisitor);
    }

    public final void onObjectTransform(SceneObject sceneObject) {
    }

    public void rebuildUp() {
    }

    public void removeAll() {
        this.stepableObjects.clear();
        this.renderableObjects.clear();
        destroyCells();
    }

    public void removeChild(SceneObject sceneObject) {
        this.renderableObjects.remove(sceneObject);
        removeStepableObjects(sceneObject);
    }

    public void stepObjects(StepContext stepContext) {
        int i;
        int size = this.stepableObjects.size();
        stepContext.incObjectSteps((long) size);
        int i2 = 0;
        while (i2 < size) {
            SceneObject sceneObject = this.stepableObjects.get(i2);
            if (sceneObject.isDisposed() || sceneObject.step(stepContext) != 0) {
                this.stepableObjects.remove(i2);
                i = i2 - 1;
                size--;
            } else {
                i = i2;
            }
            i2 = i + 1;
        }
    }

    public void renderGL(GL gl, Vec3d ajr) {
        gl.glEnable(3042);
        gl.glBlendFunc(770, 771);
        gl.glBegin(1);
        for (Octree renderGL : this.sparseCells) {
            renderGL.renderGL(gl, ajr);
        }
        gl.glEnd();
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        gl.glDisable(3042);
        gl.glBlendFunc(1, 1);
    }

    public void removeStepableObjects(SceneObject sceneObject) {
        sceneObject.visitPreOrder(this.removeSteppablesVisitor);
    }
}
