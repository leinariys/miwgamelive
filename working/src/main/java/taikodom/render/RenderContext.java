package taikodom.render;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.*;
import lombok.extern.slf4j.Slf4j;
import taikodom.render.camera.Camera;
import taikodom.render.impostor.ImpostorManager;
import taikodom.render.primitives.*;
import taikodom.render.scene.*;
import taikodom.render.shaders.Shader;
import taikodom.render.textures.Material;
import taikodom.render.textures.RenderTarget;
import taikodom.render.textures.Texture;

import javax.vecmath.Matrix3f;

/* compiled from: a */
@Slf4j
public class RenderContext {
    public final Matrix4fWrap cameraAffineTransform = new Matrix4fWrap();
    public final Matrix4fWrap cameraTransform = new Matrix4fWrap();
    public final TransformWrap transformTemp0 = new TransformWrap();
    public final Vector2fWrap vec2fTemp0 = new Vector2fWrap();
    public final Vector2fWrap vec2fTemp1 = new Vector2fWrap();
    public final Vector2fWrap vec2fTemp2 = new Vector2fWrap();
    public final Vec3d vec3dTemp0 = new Vec3d();
    public final Vec3d vec3dTemp1 = new Vec3d();
    public final Vec3d vec3dTemp2 = new Vec3d();
    public final Vec3f vec3fTemp0 = new Vec3f();
    public final Vec3f vec3fTemp1 = new Vec3f();
    public final Vec3f vec3fTemp2 = new Vec3f();
    public final Vec3f vec3fTemp3 = new Vec3f();
    public final SceneViewQuality sceneViewQuality = new SceneViewQuality();
    private final C3862vv frustum = new C0216Ch();
    private final Vec3d gpuOffset = new Vec3d();
    public double absoluteTime;
    public boolean render;
    public Color ambientLight;
    public int clearBuffers;
    public Color clearColor;
    public Camera currentCamera;
    public Scene currentScene;
    public float deltaTimeInSeconds;
    public boolean forceSimpleShader;
    public LightRecordList lights = new LightRecordList();
    public SceneObject parentObject;
    public TreeRecordList records = new TreeRecordList();
    public RenderTarget renderTarget;
    private boolean allowImpostors = false;
    private int billboardsRendered;
    private boolean cullOutSmallObjects = true;
    private long currentFrame;
    /* renamed from: dc */
    private DrawContext f10340dc;
    private ImpostorManager impostorManager;
    private int meshesRendered;
    private int numImpostorsUpdated;
    private int particleSystemsRendered;
    private int trailsRendered;

    public Scene getCurrentScene() {
        return this.currentScene;
    }

    public void setCurrentScene(Scene scene) {
        this.currentScene = scene;
    }

    public void addLight(Light light) {
        LightRecordList.LightRecord add = this.lights.add();
        add.light = light;
        add.cameraSpaceBoundingSphere.bny().sub(light.getPosition(), this.gpuOffset);
        add.cameraSpaceBoundingSphere.setRadius(light.getRadius());
    }

    private TreeRecord addPrimitive(Shader shader, Material material, Primitive primitive, TransformWrap bcVar, Color color, aLH alh, OcclusionQuery occlusionQuery, int i, RenderAreaInfo renderAreaInfo, double d) {
        TreeRecord add = this.records.add();
        add.shader = shader.getBestShader(this.f10340dc);
        add.material = material;
        add.primitive = primitive;
        add.transform.mo17344b(bcVar);
        add.gpuTransform.mo13985a((Matrix3f) bcVar.orientation);
        add.gpuTransform.m03 = (float) (bcVar.position.x - this.gpuOffset.x);
        add.gpuTransform.m13 = (float) (bcVar.position.y - this.gpuOffset.y);
        add.gpuTransform.m23 = (float) (bcVar.position.z - this.gpuOffset.z);
        add.squaredDistanceToCamera = d;
        if (alh != null) {
            add.cameraSpaceBoundingBox.din().sub(alh.din(), this.gpuOffset);
            add.cameraSpaceBoundingBox.dim().sub(alh.dim(), this.gpuOffset);
        } else {
            add.cameraSpaceBoundingBox.reset();
        }
        add.color.set(color);
        add.occlusionQuery = occlusionQuery;
        add.entryPriority = i;
        add.areaInfo = renderAreaInfo;
        return add;
    }

    public TreeRecord addPrimitive(Material material, Primitive primitive, TransformWrap bcVar, Color color, OcclusionQuery occlusionQuery, int i, RenderObject renderObject, aLH alh, double d) {
        if (primitive == null) {
            log.error("Trying to add a null primitive to TreeRecord.");
            return null;
        }
        if (this.currentFrame > renderObject.getLastFrameRendered()) {
            renderObject.setLastFrameRendered(this.currentFrame);
        }
        if (material == null) {
            log.error("Null material for primitive " + primitive);
            return null;
        } else if (material.getShader() == null) {
            log.error("Null shader for material " + material.getName());
            return null;
        } else {
            RenderAreaInfo renderAreaInfo = renderObject.getRenderAreaInfo();
            if (this.currentScene != null) {
                renderAreaInfo = this.currentScene.fillRenderAreaInfo(bcVar.position.x, bcVar.position.y, bcVar.position.z, renderObject.getRenderAreaInfo());
            }
            return addPrimitive(material.getShader(), material, primitive, bcVar, color, alh, occlusionQuery, i, renderAreaInfo, d);
        }
    }

    public void clearRecords() {
        this.records.clear();
        this.numImpostorsUpdated = 0;
        this.trailsRendered = 0;
        this.meshesRendered = 0;
        this.billboardsRendered = 0;
        this.particleSystemsRendered = 0;
    }

    public void clearRecordsAndLights() {
        clearRecords();
        this.lights.clear();
    }

    public void createRenderTargetAndTexture(int i, int i2, String str) {
    }

    public Camera getCamera() {
        return this.currentCamera;
    }

    public void setCamera(Camera camera) {
        this.currentCamera = camera;
    }

    public TreeRecordList getRecords() {
        return this.records;
    }

    public Texture getRenderTargetTexture() {
        return null;
    }

    public void setCamera(Matrix4fWrap ajk, float f, float f2, float f3, float f4) {
    }

    public void setCameraOrientation(Matrix4fWrap ajk) {
    }

    public void setClearBuffers(boolean z, boolean z2, boolean z3) {
    }

    public void setFogParameters(int i, float f, float f2, float f3, Color color) {
    }

    public void setupGLFog() {
    }

    public void setupGLLights() {
    }

    public void sortLights() {
    }

    public void sortSubTree() {
    }

    public LightRecordList getLights() {
        return this.lights;
    }

    public float getDeltaTime() {
        return this.deltaTimeInSeconds;
    }

    public void setDeltaTime(float f) {
        this.deltaTimeInSeconds = f;
    }

    public Vec3d getGpuOffset() {
        return this.gpuOffset;
    }

    public void setGpuOffset(Vec3d ajr) {
        this.gpuOffset.mo9484aA(ajr);
    }

    public DrawContext getDc() {
        return this.f10340dc;
    }

    public void setDc(DrawContext drawContext) {
        this.f10340dc = drawContext;
    }

    public C3862vv getFrustum() {
        return this.frustum;
    }

    public void incParticleSystemsRendered() {
        this.particleSystemsRendered++;
    }

    public void incBillboardsRendered() {
        this.billboardsRendered++;
    }

    public void incMeshesRendered() {
        this.meshesRendered++;
    }

    public void incTrailsRendered() {
        this.trailsRendered++;
    }

    public int getParticleSystemsRendered() {
        return this.particleSystemsRendered;
    }

    public int getBillboardsRendered() {
        return this.billboardsRendered;
    }

    public int getMeshesRendered() {
        return this.meshesRendered;
    }

    public int getTrailsRendered() {
        return this.trailsRendered;
    }

    public void setMainLight(Light light) {
        this.f10340dc.setMainLight(light);
    }

    public int getNumLights() {
        return this.lights.size();
    }

    public int getNumVisibleObjects() {
        return this.records.size();
    }

    public float getSizeOnScreen(Vec3d ajr, double d) {
        float ax = (float) ajr.mo9491ax(this.currentCamera.getPosition());
        if (ax == 0.0f) {
            return (float) this.f10340dc.getScreenWidth();
        }
        return (float) (((double) (((float) this.f10340dc.getScreenWidth()) / (ax * (2.0f * this.currentCamera.getTanHalfFovY())))) * d);
    }

    public double getDistanceForSize(double d, float f) {
        return (((double) this.f10340dc.getScreenWidth()) / (((double) f) / d)) / ((double) (2.0f * this.currentCamera.getTanHalfFovY()));
    }

    public boolean isCullOutSmallObjects() {
        return this.cullOutSmallObjects;
    }

    public void setCullOutSmallObjects(boolean z) {
        this.cullOutSmallObjects = z;
    }

    public SceneViewQuality getSceneViewQuality() {
        return this.sceneViewQuality;
    }

    public void setSceneViewQuality(SceneViewQuality sceneViewQuality2) {
        this.sceneViewQuality.set(sceneViewQuality2);
    }

    public boolean isAllowImpostors() {
        return this.allowImpostors && this.impostorManager != null;
    }

    public void setAllowImpostors(boolean z) {
        this.allowImpostors = z;
    }

    public void addImpostor(SceneObject sceneObject) {
        this.impostorManager.addImpostor(sceneObject);
    }

    public ImpostorManager getImpostorManager() {
        return this.impostorManager;
    }

    public void setImpostorManager(ImpostorManager impostorManager2) {
        this.impostorManager = impostorManager2;
    }

    public void incNumImpostorsUpdated() {
        this.numImpostorsUpdated++;
    }

    public int getNumImpostorsUpdated() {
        return this.numImpostorsUpdated;
    }
}
