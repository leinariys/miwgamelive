package taikodom.render.graphics2d;

import logic.ui.C3783v;

import java.util.EnumSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: a.acN  reason: case insensitive filesystem */
/* compiled from: a */
public class C5885acN {
    private static C3783v<Object, Map<Object, Object>> ffa = new C3783v<>(16, 0.75f, 16, C3783v.C3792i.WEAK, C3783v.C3792i.STRONG, EnumSet.of(C3783v.C3791h.IDENTITY_COMPARISONS));

    public static <V> V get(Object obj, Object obj2) {
        Map map = ffa.get(obj);
        if (map == null) {
            return null;
        }
        return (V) map.get(obj2);
    }

    /* renamed from: ak */
    public static Map<Object, Object> m20331ak(Object obj) {
        return ffa.get(obj);
    }

    public static <V> V get(Object obj, Object obj2, V v) {
        V v2;
        Map map = ffa.get(obj);
        if (map == null || (v2 = (V) map.get(obj2)) == null) {
            return v;
        }
        return v2;
    }

    public static <K, V> V put(Object obj, K k, V v) {
        Map map = ffa.get(obj);
        if (map == null) {
            synchronized (C5885acN.class) {
                map = ffa.get(obj);
                if (map == null) {
                    map = new ConcurrentHashMap();
                    ffa.put(obj, map);
                }
            }
        }
        V v2 = (V) map.get(k);
        if (v != null) {
            if (v2 != null) {
                map.remove(k);
                map.put(k, v);
            } else {
                map.put(k, v);
            }
        } else if (v2 != null) {
            map.remove(k);
        }
        return v2;
    }
}
