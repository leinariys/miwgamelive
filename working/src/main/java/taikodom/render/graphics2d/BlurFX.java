package taikodom.render.graphics2d;

import taikodom.render.DrawContext;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.shaders.ShaderProgram;
import taikodom.render.shaders.ShaderProgramObject;
import taikodom.render.textures.ChannelInfo;
import taikodom.render.textures.DDSLoader;
import taikodom.render.textures.RenderTarget;

import javax.media.opengl.GL;
import javax.swing.*;
import java.awt.*;

/* compiled from: a */
public class BlurFX {
    public float glowIntensity = 1.0f;
    private Shader defaultShader;
    private ShaderProgram horProgram;
    private boolean initialized = false;
    private boolean isSupported = false;
    private int nFilterPasses = 2;
    private ShaderProgram verProgram;

    public boolean init(DrawContext drawContext) {
        if (!GLSupportedCaps.isGlsl()) {
            this.initialized = true;
            return false;
        }
        this.initialized = true;
        this.defaultShader = new Shader();
        this.defaultShader.setName("Glow shader");
        ShaderPass shaderPass = new ShaderPass();
        shaderPass.setName("Glow shader pass");
        ChannelInfo channelInfo = new ChannelInfo();
        channelInfo.setTexChannel(10);
        shaderPass.setChannelTextureSet(0, channelInfo);
        this.defaultShader.addPass(shaderPass);
        createShaderPrograms(drawContext);
        return true;
    }

    private void createShaderPrograms(DrawContext drawContext) {
        ShaderProgramObject shaderProgramObject = new ShaderProgramObject(35633, "void main(void){gl_Position = ftransform();gl_TexCoord[0] = gl_MultiTexCoord0;}");
        String c = C0559Hm.m5258c(getClass().getResourceAsStream("blur-vertical.frag"), "utf-8");
        String c2 = C0559Hm.m5258c(getClass().getResourceAsStream("blur-horizontal.frag"), "utf-8");
        ShaderProgramObject shaderProgramObject2 = new ShaderProgramObject(35632, c);
        ShaderProgramObject shaderProgramObject3 = new ShaderProgramObject(35632, c2);
        this.verProgram = new ShaderProgram();
        this.verProgram.addProgramObject(shaderProgramObject);
        this.verProgram.addProgramObject(shaderProgramObject2);
        this.horProgram = new ShaderProgram();
        this.horProgram.addProgramObject(shaderProgramObject);
        this.horProgram.addProgramObject(shaderProgramObject3);
        if (!this.verProgram.compile((ShaderPass) null, drawContext)) {
            this.verProgram = null;
            this.horProgram = null;
        } else if (!this.horProgram.compile((ShaderPass) null, drawContext)) {
            this.verProgram = null;
            this.horProgram = null;
        } else {
            this.isSupported = true;
        }
    }

    public void blur(RenderTarget renderTarget, DrawContext drawContext, int i, int i2, int i3, int i4) {
        boolean z = true;
        if (this.initialized || init(drawContext)) {
            if (!this.isSupported || drawContext.getMaxShaderQuality() > 0) {
                z = false;
            }
            if (renderTarget != null && drawContext.getMaxShaderQuality() <= 1) {
                GL gl = drawContext.getGl();
                gl.glEnable(3553);
                renderTarget.bindTexture(drawContext);
                Rectangle rectangle = new Rectangle(i, i2, i3, i4);
                SwingUtilities.computeIntersection(0, 0, renderTarget.getWidth(), renderTarget.getHeight(), rectangle);
                int i5 = rectangle.x;
                int i6 = rectangle.y;
                int i7 = rectangle.width;
                int i8 = rectangle.height;
                int height = (renderTarget.getHeight() - i6) - i8;
                gl.glDisable(3042);
                if (z) {
                    int i9 = 0;
                    while (true) {
                        int i10 = i9;
                        if (i10 >= this.nFilterPasses) {
                            break;
                        }
                        renderTarget.copyBufferAreaToTexture(drawContext, i5, height, i5, height, i7, i8);
                        this.verProgram.bind(drawContext);
                        this.verProgram.updateMaterialProgramAttribs(drawContext);
                        drawQuad(renderTarget, drawContext, i5, i6, i7, i8);
                        this.verProgram.unbind(drawContext);
                        renderTarget.copyBufferAreaToTexture(drawContext, i5, height, i5, height, i7, i8);
                        this.horProgram.bind(drawContext);
                        this.horProgram.updateMaterialProgramAttribs(drawContext);
                        drawQuad(renderTarget, drawContext, i5, i6, i7, i8);
                        this.horProgram.unbind(drawContext);
                        i9 = i10 + 1;
                    }
                } else {
                    gl.glPushAttrib(DDSLoader.DDSD_LINEARSIZE);
                    int width = renderTarget.getWidth() / 512;
                    renderTarget.copyBufferAreaToTexture(drawContext, i5, height, i5, height, i7, i8);
                    renderTarget.bindTexture(drawContext);
                    drawShrinked(renderTarget, drawContext, i5, i6, i7, i8, width);
                    renderTarget.copyBufferAreaToTexture(drawContext, i5, height, i5, height, i7, i8);
                    renderTarget.bindTexture(drawContext);
                    drawQuadStretchedArea(renderTarget, drawContext, i5, i6, i7, i8, (float) width, 512);
                    int width2 = renderTarget.getWidth() / 256;
                    renderTarget.copyBufferAreaToTexture(drawContext, i5, height, i5, height, i7, i8);
                    renderTarget.bindTexture(drawContext);
                    drawShrinked(renderTarget, drawContext, i5, i6, i7, i8, width2);
                    renderTarget.copyBufferAreaToTexture(drawContext, i5, height, i5, height, i7, i8);
                    renderTarget.bindTexture(drawContext);
                    drawQuadStretchedArea(renderTarget, drawContext, i5, i6, i7, i8, (float) width2, 512);
                    gl.glPopAttrib();
                }
                gl.glEnable(3042);
                gl.glBlendFunc(1, 771);
            }
        }
    }

    private void drawQuadStretchedArea(RenderTarget renderTarget, DrawContext drawContext, int i, int i2, int i3, int i4, float f, int i5) {
        GL gl = drawContext.getGl();
        gl.glEnable(3089);
        gl.glScissor(i, (renderTarget.getHeight() - i2) - i4, i3, i4);
        if (((float) i) % f != 0.0f) {
            i = (int) (((float) i) + (f - (((float) i) % f)));
        }
        if (((float) i2) % f != 0.0f) {
            i2 = (int) (((float) i2) + (f - (((float) i2) % f)));
        }
        if (((float) i3) % f != 0.0f) {
            i3 = (int) (((float) i3) + (f - (((float) i3) % f)));
        }
        if (((float) i4) % f != 0.0f) {
            i4 = (int) (((float) i4) + (f - (((float) i4) % f)));
        }
        int sizeX = renderTarget.getTexture().getSizeX();
        int sizeY = renderTarget.getTexture().getSizeY();
        int height = renderTarget.getHeight();
        float f2 = ((float) i) / ((float) sizeX);
        float f3 = ((float) (height - i2)) / ((float) sizeY);
        float f4 = (((float) i) + (((float) i3) / f)) / ((float) sizeX);
        float f5 = (((float) (height - i2)) - (((float) i4) / f)) / ((float) sizeY);
        gl.glBegin(7);
        gl.glTexCoord2f(f2, f3);
        gl.glVertex2f((float) i, (float) i2);
        gl.glTexCoord2f(f4, f3);
        gl.glVertex2f((float) (i + i3), (float) i2);
        gl.glTexCoord2f(f4, f5);
        gl.glVertex2f((float) (i + i3), (float) (i2 + i4));
        gl.glTexCoord2f(f2, f5);
        gl.glVertex2f((float) i, (float) (i2 + i4));
        gl.glEnd();
        gl.glDisable(3089);
    }

    private void drawShrinked(RenderTarget renderTarget, DrawContext drawContext, int i, int i2, int i3, int i4, int i5) {
        GL gl = drawContext.getGl();
        if (i % i5 != 0) {
            i += i5 - (i % i5);
        }
        if (i2 % i5 != 0) {
            i2 += i5 - (i2 % i5);
        }
        if (i3 % i5 != 0) {
            i3 += i5 - (i3 % i5);
        }
        if (i4 % i5 != 0) {
            i4 += i5 - (i4 % i5);
        }
        int sizeX = renderTarget.getTexture().getSizeX();
        int sizeY = renderTarget.getTexture().getSizeY();
        int height = renderTarget.getHeight();
        gl.glEnable(3089);
        gl.glScissor(i, (height - i2) - i4, i3, i4);
        float f = ((float) i) / ((float) sizeX);
        float f2 = ((float) (height - i2)) / ((float) sizeY);
        float f3 = ((float) (i + i3)) / ((float) sizeX);
        float f4 = ((float) ((height - i2) - i4)) / ((float) sizeY);
        int i6 = i3 / i5;
        int i7 = i4 / i5;
        gl.glBegin(7);
        gl.glTexCoord2f(f, f2);
        gl.glVertex2f((float) i, (float) i2);
        gl.glTexCoord2f(f3, f2);
        gl.glVertex2f((float) (i + i6), (float) i2);
        gl.glTexCoord2f(f3, f4);
        gl.glVertex2f((float) (i + i6), (float) (i2 + i7));
        gl.glTexCoord2f(f, f4);
        gl.glVertex2f((float) i, (float) (i2 + i7));
        gl.glEnd();
        gl.glDisable(3089);
    }

    private void drawQuad(RenderTarget renderTarget, DrawContext drawContext, int i, int i2, int i3, int i4) {
        GL gl = drawContext.getGl();
        int sizeX = renderTarget.getTexture().getSizeX();
        int sizeY = renderTarget.getTexture().getSizeY();
        int height = renderTarget.getHeight();
        float f = ((float) i) / ((float) sizeX);
        float f2 = ((float) (height - i2)) / ((float) sizeY);
        float f3 = ((float) ((i + i3) - 1)) / ((float) sizeX);
        float f4 = ((float) (((height - i2) - i4) + 1)) / ((float) sizeY);
        gl.glBegin(7);
        gl.glTexCoord2f(f, f2);
        gl.glVertex2f((float) i, (float) i2);
        gl.glTexCoord2f(f, f4);
        gl.glVertex2f((float) i, (float) ((i2 + i4) - 1));
        gl.glTexCoord2f(f3, f4);
        gl.glVertex2f((float) ((i + i3) - 1), (float) ((i2 + i4) - 1));
        gl.glTexCoord2f(f3, f2);
        gl.glVertex2f((float) ((i + i3) - 1), (float) i2);
        gl.glEnd();
    }

    public void releaseReferences() {
        if (this.defaultShader != null) {
            this.defaultShader.releaseReferences();
        }
        if (this.verProgram != null) {
            this.verProgram.releaseReferences();
        }
        if (this.horProgram != null) {
            this.horProgram.releaseReferences();
        }
        this.horProgram = null;
        this.verProgram = null;
        this.defaultShader = null;
    }
}
