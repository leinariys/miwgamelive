package taikodom.render.graphics2d;

import taikodom.render.DrawContext;
import taikodom.render.textures.Font;

import javax.media.opengl.GL;
import java.awt.*;

/* compiled from: a */
public class TextDraw extends DrawNode {
    private Font font;
    private String text;

    /* renamed from: x */
    private float f10348x;

    /* renamed from: y */
    private float f10349y;

    public TextDraw(Font font2, Color color, String str, float f, float f2) {
        this.font = font2;
        this.text = str;
        this.f10348x = f;
        this.f10349y = f2;
    }

    public void draw(DrawContext drawContext, RGuiScene rGuiScene) {
        GL gl = drawContext.getGl();
        this.font.drawString(drawContext, this.text, this.f10348x, this.f10349y);
        gl.glDisable(3553);
    }
}
