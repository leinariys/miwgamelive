package taikodom.render.graphics2d;

import taikodom.render.textures.Texture;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.image.ImageObserver;
import java.util.Arrays;

/* compiled from: a */
public class TexPolygon extends Polygon {
    private static final int MIN_LENGTH = 4;

    public float[] upoints = new float[4];
    public float[] vpoints = new float[4];
    private Image image;

    public TexPolygon(Image image2) {
        this.image = image2;
    }

    public void addPoint(int i, int i2) {
        TexPolygon.super.addPoint(i, i2);
        addUV(0.0f, 0.0f);
    }

    public void addPoint(int i, int i2, float f, float f2) {
        TexPolygon.super.addPoint(i, i2);
        addUV(f, f2);
    }

    private void addUV(float f, float f2) {
        int i = 4;
        if (this.npoints >= this.upoints.length || this.npoints >= this.vpoints.length) {
            int i2 = this.npoints * 2;
            if (i2 >= 4) {
                i = ((i2 + -1) & i2) != 0 ? Integer.highestOneBit(i2) : i2;
            }
            this.upoints = Arrays.copyOf(this.upoints, i);
            this.vpoints = Arrays.copyOf(this.vpoints, i);
        }
        this.upoints[this.npoints - 1] = f;
        this.vpoints[this.npoints - 1] = f2;
    }

    public Image getImage() {
        return this.image;
    }

    public PathIterator getPathIterator(AffineTransform affineTransform) {
        return new C5136b(this, affineTransform);
    }

    public PathIterator getPathIterator(AffineTransform affineTransform, double d) {
        return getPathIterator(affineTransform);
    }

    public void createOctagon(int i) {
        int width = this.image.getWidth((ImageObserver) null);
        int height = this.image.getHeight((ImageObserver) null);
        if (width == -1 || height == -1) {
            C5135a aVar = new C5135a(this, i);
            width = this.image.getWidth(aVar);
            height = this.image.getHeight(aVar);
        }
        if (width != -1 && height != -1) {
            createOctagon(i, width, height);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void createOctagon(int i, int i2, int i3) {
        if (this.npoints <= 0) {
            if ((Texture.isPowerOf2(i2) && Texture.isPowerOf2(i3)) || Texture.haveNPOT()) {
                float f = ((float) i) / ((float) i2);
                float f2 = ((float) i) / ((float) i3);
                addPoint(i, 0, f, 0.0f);
                addPoint(i2 - i, 0, 1.0f - f, 0.0f);
                addPoint(i2, i, 1.0f, f2);
                addPoint(i2, i3 - i, 1.0f, 1.0f - f2);
                addPoint(i2 - i, i3, 1.0f - f, 1.0f);
                addPoint(i, i3, f, 1.0f);
                addPoint(0, i3 - i, 0.0f, 1.0f - f2);
                addPoint(0, i, 0.0f, f2);
            } else if (Texture.haveTexRect()) {
                float f3 = (float) i;
                float f4 = (float) i;
                addPoint(i, 0, f3, 0.0f);
                addPoint(i2 - i, 0, ((float) i2) - f3, 0.0f);
                addPoint(i2, i, (float) i2, f4);
                addPoint(i2, i3 - i, (float) i2, ((float) i3) - f4);
                addPoint(i2 - i, i3, ((float) i2) - f3, (float) i3);
                addPoint(i, i3, f3, (float) i3);
                addPoint(0, i3 - i, 0.0f, ((float) i2) - f4);
                addPoint(0, i, 0.0f, f4);
            } else {
                float f5 = ((float) i) / ((float) i2);
                float f6 = ((float) i) / ((float) i3);
                float roundToPowerOf2 = ((float) i2) / ((float) Texture.roundToPowerOf2(i2));
                float roundToPowerOf22 = ((float) i3) / ((float) Texture.roundToPowerOf2(i3));
                addPoint(i, 0, f5 * roundToPowerOf2, 0.0f);
                addPoint(i2 - i, 0, (1.0f - f5) * roundToPowerOf2, 0.0f);
                addPoint(i2, i, 1.0f * roundToPowerOf2, f6 * roundToPowerOf22);
                addPoint(i2, i3 - i, 1.0f * roundToPowerOf2, (1.0f - f6) * roundToPowerOf22);
                addPoint(i2 - i, i3, (1.0f - f5) * roundToPowerOf2, 1.0f * roundToPowerOf22);
                addPoint(i, i3, f5 * roundToPowerOf2, 1.0f * roundToPowerOf22);
                addPoint(0, i3 - i, 0.0f, roundToPowerOf22 * (1.0f - f6));
                addPoint(0, i, 0.0f, f6);
            }
        }
    }

    /* renamed from: taikodom.render.graphics2d.TexPolygon$a */
    private static final class C5135a implements ImageObserver {
        private TexPolygon hdK;
        private int hdL;

        public C5135a(TexPolygon texPolygon, int i) {
            this.hdK = texPolygon;
            this.hdL = i;
        }

        public boolean imageUpdate(Image image, int i, int i2, int i3, int i4, int i5) {
            this.hdK.createOctagon(this.hdL, i4, i5);
            return false;
        }
    }

    /* renamed from: taikodom.render.graphics2d.TexPolygon$b */
    /* compiled from: a */
    class C5136b implements PathIterator {
        TexPolygon iXG;
        int index;
        AffineTransform transform;

        public C5136b(TexPolygon texPolygon, AffineTransform affineTransform) {
            this.iXG = texPolygon;
            this.transform = affineTransform;
            if (texPolygon.npoints == 0) {
                this.index = 1;
            }
        }

        public int getWindingRule() {
            return 0;
        }

        public boolean isDone() {
            return this.index > this.iXG.npoints;
        }

        public void next() {
            this.index++;
        }

        public int currentSegment(float[] fArr) {
            if (this.index >= this.iXG.npoints) {
                return 4;
            }
            fArr[0] = (float) this.iXG.xpoints[this.index];
            fArr[1] = (float) this.iXG.ypoints[this.index];
            fArr[2] = this.iXG.upoints[this.index];
            fArr[3] = this.iXG.vpoints[this.index];
            if (this.transform != null) {
                this.transform.transform(fArr, 0, fArr, 0, 1);
            }
            if (this.index != 0) {
                return 1;
            }
            return 0;
        }

        public int currentSegment(double[] dArr) {
            if (this.index >= this.iXG.npoints) {
                return 4;
            }
            dArr[0] = (double) this.iXG.xpoints[this.index];
            dArr[1] = (double) this.iXG.ypoints[this.index];
            dArr[2] = (double) this.iXG.upoints[this.index];
            dArr[3] = (double) this.iXG.vpoints[this.index];
            if (this.transform != null) {
                this.transform.transform(dArr, 0, dArr, 0, 1);
            }
            if (this.index != 0) {
                return 1;
            }
            return 0;
        }
    }
}
