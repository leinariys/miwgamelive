package taikodom.render.graphics2d;

import taikodom.render.DrawContext;
import taikodom.render.textures.BaseTexture;

import javax.media.opengl.GL;

/* compiled from: a */
public class ImageDraw extends DrawNode {
    private int bufferPosition;
    private int count;
    private BaseTexture texture;

    public ImageDraw(BaseTexture baseTexture, int i, int i2) {
        this.bufferPosition = i;
        this.count = i2;
        this.texture = baseTexture;
    }

    public ImageDraw() {
    }

    public void draw(DrawContext drawContext, RGuiScene rGuiScene) {
        this.texture.bind(drawContext);
        GL gl = drawContext.getGl();
        drawElements(gl, rGuiScene.getIndexes(), 7, this.bufferPosition, this.count);
        gl.glDisable(this.texture.getTarget());
    }
}
