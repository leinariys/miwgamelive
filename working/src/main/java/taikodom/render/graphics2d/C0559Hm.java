package taikodom.render.graphics2d;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.*;

/* renamed from: a.Hm */
/* compiled from: a */
public class C0559Hm {
    public static final Charset igv = Charset.forName("UTF-8");
    private static String SEPARATOR = System.getProperty("line.separator");

    /* renamed from: a */
    public static List<File> m5246a(String str, FilenameFilter filenameFilter) throws FileNotFoundException {
        return m5256b(new File(str), filenameFilter);
    }

    /* renamed from: a */
    public static File[] m5253a(File file, String str) {
        return file.listFiles(new C0562b(str));
    }

    /* renamed from: b */
    public static List<File> m5256b(File file, FilenameFilter filenameFilter) throws FileNotFoundException {
        if (file.exists()) {
            return m5259c(file, filenameFilter);
        }
        throw new FileNotFoundException("The file or directory " + file.getAbsolutePath() + " was not found");
    }

    /* renamed from: c */
    private static List<File> m5259c(File file, FilenameFilter filenameFilter) {
        if (file.isFile()) {
            if (filenameFilter.accept(file, file.getName())) {
                return Collections.singletonList(file);
            }
            return Collections.emptyList();
        } else if (file.getName().equals("CVS")) {
            return Collections.emptyList();
        } else {
            ArrayList arrayList = new ArrayList();
            for (File c : file.listFiles()) {
                arrayList.addAll(m5259c(c, filenameFilter));
            }
            return arrayList;
        }
    }

    /* renamed from: a */
    public static boolean m5251a(File file, String str, Charset charset) throws IOException {
        boolean z = false;
        if (!file.exists()) {
            return true;
        }
        InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(file), charset);
        StringBuffer stringBuffer = new StringBuffer();
        char[] cArr = new char[512];
        while (true) {
            int read = inputStreamReader.read(cArr);
            if (read <= 0) {
                break;
            }
            stringBuffer.append(cArr, 0, read);
        }
        inputStreamReader.close();
        if (!str.equals(stringBuffer.toString())) {
            z = true;
        }
        return z;
    }

    /* renamed from: t */
    public static File m5265t(String str, String str2, String str3) {
        File file = new File(str, m5262mY(str2));
        if (!file.isDirectory()) {
            file.mkdirs();
        }
        return new File(file.getAbsoluteFile(), String.valueOf(str3) + ".java");
    }

    /* renamed from: mY */
    public static String m5262mY(String str) {
        StringTokenizer stringTokenizer = new StringTokenizer(str, ".");
        StringBuffer stringBuffer = new StringBuffer();
        while (stringTokenizer.hasMoreElements()) {
            stringBuffer.append(stringTokenizer.nextToken());
            if (stringTokenizer.hasMoreElements()) {
                stringBuffer.append(File.separatorChar);
            }
        }
        return stringBuffer.toString();
    }

    /* renamed from: a */
    public static String m5244a(File file, Charset charset) throws IOException {
        if (!file.exists()) {
            return null;
        }
        InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(file), charset);
        StringBuffer stringBuffer = new StringBuffer();
        char[] cArr = new char[512];
        while (true) {
            int read = inputStreamReader.read(cArr);
            if (read <= 0) {
                inputStreamReader.close();
                return stringBuffer.toString();
            }
            stringBuffer.append(cArr, 0, read);
        }
    }

    /* renamed from: a */
    public static String m5245a(InputStream inputStream, Charset charset) throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, charset);
        StringBuffer stringBuffer = new StringBuffer();
        char[] cArr = new char[512];
        while (true) {
            int read = inputStreamReader.read(cArr);
            if (read <= 0) {
                inputStreamReader.close();
                return stringBuffer.toString();
            }
            stringBuffer.append(cArr, 0, read);
        }
    }

    /* renamed from: b */
    public static void m5257b(File file, String str) throws IOException {
        m5247a(file, (Charset) null, str);
    }

    /* renamed from: a */
    public static void m5247a(File file, Charset charset, String str) throws IOException {
        OutputStreamWriter outputStreamWriter;
        if (charset != null) {
            outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file), charset);
        } else {
            outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file));
        }
        outputStreamWriter.write(str);
        outputStreamWriter.flush();
        outputStreamWriter.close();
    }

    /* renamed from: a */
    public static boolean m5250a(File file, File file2, boolean z) throws IOException {
        boolean z2 = true;
        if (file.isDirectory()) {
            file2.mkdirs();
            File[] listFiles = file.listFiles();
            if (listFiles == null) {
                return true;
            }
            for (File file3 : listFiles) {
                z2 &= m5250a(file3, new File(file2, file3.getName()), z);
            }
            return z2;
        }
        try {
            copyFile(file, file2, true);
            return true;
        } catch (IOException e) {
            if (z) {
                return false;
            }
            throw e;
        }
    }

    public static void copyFile(File file, File file2) throws IOException {
        copyFile(file, file2, false);
    }

    public static void copyFile(File file, File file2, boolean z) throws IOException {
        FileOutputStream fileOutputStream;
        if (file2.getParent() != null && z) {
            file2.getParentFile().mkdirs();
        }
        FileInputStream fileInputStream = new FileInputStream(file);
        try {
            fileOutputStream = new FileOutputStream(file2);
            try {
                byte[] bArr = new byte[4096];
                while (true) {
                    int read = fileInputStream.read(bArr);
                    if (read == -1) {
                        try {
                            break;
                        } finally {
                            if (fileOutputStream != null) {
                                fileOutputStream.close();
                            }
                        }
                    } else {
                        fileOutputStream.write(bArr, 0, read);
                    }
                }
                fileInputStream.close();
            } catch (Throwable th) {
                th = th;
            }
        } catch (Throwable th2) {
            fileOutputStream = null;
            try {
                fileInputStream.close();
            } finally {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            }
        }
    }

    public static void copyStream(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bArr = new byte[4096];
        while (true) {
            int read = inputStream.read(bArr);
            if (read >= 0) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    public static boolean m5252a(File file, boolean z) {
        boolean z2;
        File[] listFiles;
        if (!file.isDirectory() || !z || (listFiles = file.listFiles()) == null) {
            z2 = true;
        } else {
            z2 = true;
            for (File a : listFiles) {
                z2 &= m5252a(a, true);
            }
        }
        if (!z2 || !file.delete()) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public static void m5249a(Properties properties, File file, String str) throws IOException {
        C0560a aVar = new C0560a();
        aVar.putAll(properties);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        aVar.store(byteArrayOutputStream, str);
        String byteArrayOutputStream2 = byteArrayOutputStream.toString("8859_1");
        int i = 0;
        if (str != null) {
            i = byteArrayOutputStream2.indexOf(SEPARATOR);
        }
        String substring = byteArrayOutputStream2.substring(byteArrayOutputStream2.indexOf(SEPARATOR, i + 1) + SEPARATOR.length());
        if (m5251a(file, substring, Charset.forName("8859_1"))) {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(substring.getBytes("8859_1"));
            fileOutputStream.close();
        }
    }

    /* renamed from: aU */
    public static boolean m5254aU(String str, String str2) throws IOException {
        return m5260d(new File(str), new File(str2));
    }

    /* renamed from: d */
    public static boolean m5260d(File file, File file2) throws IOException {
        return file2.getCanonicalPath().startsWith(String.valueOf(file.getCanonicalPath()) + File.separator);
    }

    /* renamed from: aV */
/*
    public static void m5255aV(String str, String str2) {
        Assert.assertTrue(Arrays.equals(m5264q(new File(str)), m5264q(new File(str2))));
    }
*/
    /* renamed from: p */
    public static ByteBuffer m5263p(File file) {
        int length = (int) file.length();
        ByteBuffer order = ByteBuffer.allocateDirect(length).order(ByteOrder.nativeOrder());
        try {
            FileChannel channel = new FileInputStream(file).getChannel();
            int i = length;
            while (i > 0) {
                try {
                    int read = channel.read(order);
                    if (read < 0) {
                        throw new EOFException();
                    }
                    i -= read;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        }
        return order;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: q */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] m5264q(File file) {
        byte[] fileContent = new byte[0];
        try {
            fileContent = Files.readAllBytes(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileContent;
    }

    /* renamed from: e */
    public static byte[] m5261e(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        copyStream(inputStream, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static void readFully(InputStream inputStream, byte[] bArr) {
        int i = 0;
        while (true) {
            try {
                int read = inputStream.read(bArr, i, bArr.length - i);
                if (read > 0) {
                    i += read;
                } else {
                    return;
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /* renamed from: c */
    public static String m5258c(InputStream inputStream, String str) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr = new byte[4096];
            while (true) {
                int read = inputStream.read(bArr);
                if (read <= 0) {
                    return new String(byteArrayOutputStream.toByteArray(), str);
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: a */
    public static void m5248a(File file, byte[] bArr) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(bArr);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: a.Hm$b */
    /* compiled from: a */
    static class C0562b implements FilenameFilter {
        private final /* synthetic */ String dbv;

        C0562b(String str) {
            this.dbv = str;
        }

        public boolean accept(File file, String str) {
            return str.startsWith(this.dbv);
        }
    }

    /* renamed from: a.Hm$a */
    static class C0560a extends Properties {
        C0560a() {
        }

        public synchronized Enumeration<Object> keys() {
            TreeSet treeSet;
            treeSet = new TreeSet();
            treeSet.addAll(super.keySet());
            return new C0561a(treeSet.iterator());
        }

        /* renamed from: a.Hm$a$a */
        class C0561a implements Enumeration<Object> {
            private final /* synthetic */ Iterator dDu;

            C0561a(Iterator it) {
                this.dDu = it;
            }

            public boolean hasMoreElements() {
                return this.dDu.hasNext();
            }

            public Object nextElement() {
                return this.dDu.next();
            }
        }
    }
}
