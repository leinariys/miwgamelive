package taikodom.render.graphics2d;

import java.util.ArrayList;

/* compiled from: a */
public abstract class StackList<T> {
    private final ArrayList<T> list = new ArrayList<>();
    private int pos = 0;
    private int[] stack = new int[512];
    private int stackCount = 0;

    /* access modifiers changed from: protected */
    public abstract void copy(T t, T t2);

    /* access modifiers changed from: protected */
    public abstract T create();

    public final void push() {
        int[] iArr = this.stack;
        int i = this.stackCount;
        this.stackCount = i + 1;
        iArr[i] = this.pos;
    }

    public final void pop() {
        int[] iArr = this.stack;
        int i = this.stackCount - 1;
        this.stackCount = i;
        this.pos = iArr[i];
    }

    public T get() {
        if (this.pos == this.list.size()) {
            expand();
        }
        ArrayList<T> arrayList = this.list;
        int i = this.pos;
        this.pos = i + 1;
        return arrayList.get(i);
    }

    private void expand() {
        this.list.add(create());
    }
}
