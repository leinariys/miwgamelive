package taikodom.render.graphics2d;

import taikodom.render.DrawContext;

import java.awt.*;

/* renamed from: a.Pq */
/* compiled from: a */
public class C1082Pq extends DrawNode {
    private final Color color;

    public C1082Pq(Color color2) {
        this.color = color2;
    }

    public void draw(DrawContext drawContext, RGuiScene rGuiScene) {
        drawContext.getGl().glColor4f(((float) this.color.getRed()) / 255.0f, ((float) this.color.getGreen()) / 255.0f, ((float) this.color.getBlue()) / 255.0f, ((float) this.color.getAlpha()) / 255.0f);
    }
}
