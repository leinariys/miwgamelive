package taikodom.render.graphics2d;

import taikodom.render.DrawContext;
import taikodom.render.textures.BaseTexture;

/* compiled from: a */
public class TexPolyDraw extends DrawNode {
    public int bufferPosition;
    public int count;
    public int elementType;
    private BaseTexture texture;

    public TexPolyDraw(BaseTexture baseTexture, int i, int i2, int i3) {
        this.texture = baseTexture;
        this.bufferPosition = i2;
        this.count = i3;
        this.elementType = i;
    }

    public void draw(DrawContext drawContext, RGuiScene rGuiScene) {
        this.texture.bind(drawContext);
        rGuiScene.getIndexes().buffer().position(this.bufferPosition);
        drawContext.getGl().glDrawElements(this.elementType, this.count, 5125, rGuiScene.getIndexes().buffer());
        drawContext.getGl().glDisable(this.texture.getTarget());
    }
}
