package taikodom.render.graphics2d;

import game.geometry.Matrix4fWrap;
import taikodom.render.DrawContext;
import taikodom.render.TexBackedImage;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.textures.BaseTexture;
import taikodom.render.textures.RenderTarget;
import taikodom.render.textures.Texture;

import javax.media.opengl.GL;
import javax.swing.*;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.*;
import java.awt.image.*;
import java.awt.image.renderable.RenderableImage;
import java.text.AttributedCharacterIterator;
import java.util.Map;

/* compiled from: a */
public class RGraphics2 extends Graphics2D {
    public static final int BOTTOM = 8;
    public static final int LEFT = 1;
    public static final int RIGHT = 4;
    public static final int TOP = 2;
    private static Point2D tempP1 = new Point2D.Float();
    private static Point2D tempP2 = new Point2D.Float();
    private static Point2D tempP3 = new Point2D.Float();
    private static Point2D tempP4 = new Point2D.Float();
    private Color backgroundColor;
    private boolean clippingOn;
    private Color color;
    private Color colorMultiplier;
    private Color composedColorMultiplier;
    private Composite composite;
    private RGraphicsContext context;
    private Font font;
    private taikodom.render.textures.Font graphicalFont;
    private RGuiScene guiScene;
    private RenderingHints hints;
    private float lineWidth;
    private Paint paint;
    private RGraphics2 parent;
    private Color parentColorMultiplier;
    private Stroke stroke;
    private AffineTransform transform;
    private Rectangle untClip;

    public RGraphics2(RGraphics2 rGraphics2) {
        init(rGraphics2);
    }

    @Deprecated
    public RGraphics2(DrawContext drawContext) {
        this(new RGraphicsContext(drawContext));
    }

    public RGraphics2(RGraphicsContext rGraphicsContext) {
        this.context = rGraphicsContext;
        this.guiScene = new RGuiScene(rGraphicsContext);
        this.untClip = new Rectangle(0, 0, 0, 0);
        this.backgroundColor = Color.WHITE;
        this.color = Color.WHITE;
        this.composedColorMultiplier = Color.WHITE;
        this.parentColorMultiplier = Color.WHITE;
        this.transform = new AffineTransform();
        this.lineWidth = 1.0f;
        this.clippingOn = false;
    }

    public static Shape transformShape(AffineTransform affineTransform, Shape shape) {
        if (shape == null) {
            return null;
        }
        if ((shape instanceof Rectangle2D) && (affineTransform.getType() & 48) == 0) {
            Rectangle2D rectangle2D = (Rectangle2D) shape;
            double[] dArr = {rectangle2D.getX(), rectangle2D.getY(), rectangle2D.getMaxX(), rectangle2D.getMaxY()};
            affineTransform.transform(dArr, 0, dArr, 0, 2);
            Rectangle2D.Float floatR = new Rectangle2D.Float();
            floatR.setFrameFromDiagonal(dArr[0], dArr[1], dArr[2], dArr[3]);
            return floatR;
        } else if (affineTransform.isIdentity()) {
            return cloneShape(shape);
        } else {
            return affineTransform.createTransformedShape(shape);
        }
    }

    public static Shape cloneShape(Shape shape) {
        return new GeneralPath(shape);
    }

    public void reset() {
        this.clippingOn = false;
        this.guiScene.reset();
    }

    public void render() {
        this.guiScene.draw((RenderTarget) null);
    }

    public void init(RGraphics2 rGraphics2) {
        this.parent = rGraphics2;
        this.context = rGraphics2.context;
        this.backgroundColor = rGraphics2.backgroundColor;
        this.color = rGraphics2.color;
        this.lineWidth = 1.0f;
        this.clippingOn = false;
        if (this.transform == null) {
            this.transform = (AffineTransform) rGraphics2.transform.clone();
        } else {
            this.transform.setTransform(rGraphics2.transform);
        }
        if (this.untClip == null) {
            this.untClip = new Rectangle(-10000, -10000, 20000, 20000);
        }
        this.composedColorMultiplier = rGraphics2.composedColorMultiplier;
        this.parentColorMultiplier = rGraphics2.composedColorMultiplier;
        this.untClip.setBounds(rGraphics2.untClip);
        this.hints = rGraphics2.hints;
        this.clippingOn = rGraphics2.clippingOn;
        this.font = rGraphics2.font;
        this.paint = rGraphics2.paint;
        this.guiScene = rGraphics2.guiScene;
    }

    public RGraphicsContext getContext() {
        return this.context;
    }

    public void addRenderingHints(Map<?, ?> map) {
    }

    public void clip(Shape shape) {
        if (shape == null) {
            this.clippingOn = false;
            return;
        }
        Rectangle bounds = shape.getBounds();
        clipRect(bounds.x, bounds.y, bounds.width, bounds.height);
    }

    public void draw(Shape shape) {
        if (this.color.getAlpha() != 0) {
            float[] fArr = new float[6];
            this.guiScene.setLineWidth((float) (((double) this.lineWidth) * this.transform.getScaleX()));
            int lastIdx = this.guiScene.getLastIdx();
            this.guiScene.setBlendFunc(770, 771);
            this.guiScene.setClip(this);
            this.guiScene.setColor(this.color, this.composedColorMultiplier);
            FlatteningPathIterator flatteningPathIterator = new FlatteningPathIterator(shape.getPathIterator(this.transform), 1.0d);
            float f = 0.0f;
            float f2 = 0.0f;
            while (!flatteningPathIterator.isDone()) {
                switch (flatteningPathIterator.currentSegment(fArr)) {
                    case 0:
                        f2 = fArr[0];
                        f = fArr[1];
                        if (this.guiScene.getLastIdx() - lastIdx >= 2) {
                            this.guiScene.add(new C3107nu(3, lastIdx, this.guiScene.getLastIdx() - lastIdx));
                        }
                        lastIdx = this.guiScene.getLastIdx();
                        break;
                    case 1:
                        if (this.guiScene.getLastIdx() == lastIdx) {
                            this.guiScene.addVertex(f2, f, 0.0f);
                        }
                        f2 = fArr[0];
                        f = fArr[1];
                        this.guiScene.addVertex(f2, f, 0.0f);
                        break;
                    case 4:
                        if (this.guiScene.getLastIdx() - lastIdx >= 2) {
                            this.guiScene.add(new C3107nu(2, lastIdx, this.guiScene.getLastIdx() - lastIdx));
                        }
                        lastIdx = this.guiScene.getLastIdx();
                        break;
                }
                flatteningPathIterator.next();
            }
            if (this.guiScene.getLastIdx() - lastIdx >= 2) {
                this.guiScene.add(new C3107nu(3, lastIdx, this.guiScene.getLastIdx() - lastIdx));
            }
        }
    }

    public void drawGlyphVector(GlyphVector glyphVector, float f, float f2) {
    }

    public boolean drawImage(Image image, AffineTransform affineTransform, ImageObserver imageObserver) {
        drawImage(image, (int) affineTransform.getTranslateX(), (int) affineTransform.getTranslateY(), imageObserver);
        return false;
    }

    public void drawImage(BufferedImage bufferedImage, BufferedImageOp bufferedImageOp, int i, int i2) {
    }

    public void drawRenderableImage(RenderableImage renderableImage, AffineTransform affineTransform) {
    }

    public void drawRenderedImage(RenderedImage renderedImage, AffineTransform affineTransform) {
    }

    public void drawString(String str, int i, int i2) {
        drawString(str, (float) i, (float) i2);
    }

    public void drawString(String str, float f, float f2) {
        if (str != null && str.length() != 0) {
            this.guiScene.setClip(this);
            this.guiScene.setColor(this.color, this.composedColorMultiplier);
            this.graphicalFont = this.context.getFont(this.font);
            if (this.transform.getType() == 0 || this.transform.getType() == 1) {
                this.guiScene.add(new TextDraw(this.graphicalFont, this.color, str, (float) (((double) f) + this.transform.getTranslateX()), (float) (((double) f2) + this.transform.getTranslateY())));
                return;
            }
            double[] dArr = new double[16];
            this.transform.getMatrix(dArr);
            dArr[15] = 1.0d;
            dArr[13] = dArr[5];
            dArr[12] = dArr[4];
            dArr[5] = dArr[3];
            dArr[4] = dArr[2];
            dArr[2] = 0.0d;
            dArr[3] = 0.0d;
            this.guiScene.add(new C5130a(this.graphicalFont, this.color, str, f, f2, dArr));
        }
    }

    public void drawString(AttributedCharacterIterator attributedCharacterIterator, int i, int i2) {
    }

    public void drawString(AttributedCharacterIterator attributedCharacterIterator, float f, float f2) {
    }

    private void drawTexPolygon(TexPolygon texPolygon) {
        Image image = texPolygon.getImage();
        this.guiScene.setClip(this);
        AffineTransform affineTransform = new AffineTransform(this.transform);
        float[] fArr = new float[6];
        int lastIdx = this.guiScene.getLastIdx();
        BaseTexture texture = this.context.getTexture(image);
        float f = 0.0f;
        float f2 = 0.0f;
        float f3 = 0.0f;
        float f4 = 0.0f;
        boolean z = false;
        PathIterator pathIterator = texPolygon.getPathIterator(affineTransform);
        while (!pathIterator.isDone()) {
            switch (pathIterator.currentSegment(fArr)) {
                case 0:
                    f = fArr[0];
                    f2 = fArr[1];
                    f3 = fArr[2];
                    f4 = fArr[3];
                    z = false;
                    break;
                case 1:
                    if (!z) {
                        this.guiScene.addVertex(f, f2, 0.0f);
                        this.guiScene.setCurrentTexCoord(0, f3, f4);
                    }
                    f = fArr[0];
                    f2 = fArr[1];
                    f3 = fArr[2];
                    f4 = fArr[3];
                    this.guiScene.addVertex(f, f2, 0.0f);
                    this.guiScene.setCurrentTexCoord(0, f3, f4);
                    z = true;
                    break;
                default:
                    z = false;
                    break;
            }
            pathIterator.next();
        }
        if (this.guiScene.getLastIdx() - lastIdx >= 3) {
            this.guiScene.setBlendFunc(770, 771);
            this.guiScene.add(new TexPolyDraw(texture, 6, lastIdx, this.guiScene.getLastIdx() - lastIdx));
        }
        if (!(this.paint instanceof Color)) {
            this.guiScene.setEnableColorArray(false);
        }
        this.guiScene.setPaintContext((PaintContext) null, false);
    }

    public void fill(Shape shape) {
        if (this.color.getAlpha() != 0) {
            if (shape instanceof TexPolygon) {
                drawTexPolygon((TexPolygon) shape);
                return;
            }
            int lastIdx = this.guiScene.getLastIdx();
            float[] fArr = new float[6];
            Rectangle2D bounds2D = shape.getBounds2D();
            Point2D.Double doubleR = new Point2D.Double(bounds2D.getCenterX(), bounds2D.getCenterY());
            AffineTransform affineTransform = new AffineTransform(this.transform);
            Point2D transform2 = affineTransform.transform(doubleR, doubleR);
            this.guiScene.setClip(this);
            this.guiScene.setColor(this.color, this.composedColorMultiplier);
            if (this.paint != null && !(this.paint instanceof Color)) {
                this.guiScene.setEnableColorArray(true);
                this.guiScene.setPaintContext(this.paint.createContext(ColorModel.getRGBdefault(), new Rectangle(0, 0, 1000, 1000), new Rectangle(0, 0, 0, 0), affineTransform, (RenderingHints) null), this.paint.getTransparency() != 1);
            }
            this.guiScene.addVertex((float) transform2.getX(), (float) transform2.getY(), 0.0f);
            FlatteningPathIterator flatteningPathIterator = new FlatteningPathIterator(shape.getPathIterator(affineTransform), 1.0d);
            boolean z = false;
            float f = 0.0f;
            float f2 = 0.0f;
            while (!flatteningPathIterator.isDone()) {
                switch (flatteningPathIterator.currentSegment(fArr)) {
                    case 0:
                        f2 = fArr[0];
                        f = fArr[1];
                        z = false;
                        break;
                    case 1:
                        if (!z) {
                            this.guiScene.addVertex(f2, f, 0.0f);
                        }
                        f2 = fArr[0];
                        f = fArr[1];
                        this.guiScene.addVertex(f2, f, 0.0f);
                        z = true;
                        break;
                    default:
                        z = false;
                        break;
                }
                flatteningPathIterator.next();
            }
            if (this.guiScene.getLastIdx() - lastIdx >= 3) {
                this.guiScene.setBlendFunc(770, 771);
                this.guiScene.add(new C3107nu(6, lastIdx, this.guiScene.getLastIdx() - lastIdx));
            }
            if (!(this.paint instanceof Color)) {
                this.guiScene.setEnableColorArray(false);
            }
            this.guiScene.setPaintContext((PaintContext) null, false);
        }
    }

    public Color getBackground() {
        return this.backgroundColor;
    }

    public void setBackground(Color color2) {
        this.backgroundColor = color2;
    }

    public Composite getComposite() {
        return this.composite;
    }

    public void setComposite(Composite composite2) {
        this.composite = composite2;
    }

    public GraphicsConfiguration getDeviceConfiguration() {
        return null;
    }

    public FontRenderContext getFontRenderContext() {
        return null;
    }

    public Paint getPaint() {
        return this.paint;
    }

    public void setPaint(Paint paint2) {
        this.paint = paint2;
        if (paint2 instanceof Color) {
            this.color = (Color) paint2;
        }
    }

    public Object getRenderingHint(RenderingHints.Key key) {
        if (this.hints == null) {
            return null;
        }
        return this.hints.get(key);
    }

    public RenderingHints getRenderingHints() {
        return this.hints;
    }

    public void setRenderingHints(Map<?, ?> map) {
    }

    public Stroke getStroke() {
        return this.stroke;
    }

    public void setStroke(Stroke stroke2) {
        if (stroke2 != null) {
            this.lineWidth = ((BasicStroke) stroke2).getLineWidth();
        }
    }

    public AffineTransform getTransform() {
        return this.transform;
    }

    public void setTransform(AffineTransform affineTransform) {
        this.transform.setTransform(affineTransform);
    }

    public void setTransform(Matrix4fWrap ajk) {
    }

    public AffineTransform internalGetTransform() {
        return this.transform;
    }

    public boolean hit(Rectangle rectangle, Shape shape, boolean z) {
        return false;
    }

    public boolean hitClip(int i, int i2, int i3, int i4) {
        Rectangle rectangle = this.untClip;
        if (!this.clippingOn || rectangle == null) {
            return true;
        }
        if (this.transform.getType() == 1) {
            return rectangle.intersects(this.transform.getTranslateX() + ((double) i), this.transform.getTranslateY() + ((double) i2), (double) i3, (double) i4);
        } else if (this.transform.getType() == 0) {
            return rectangle.intersects((double) i, (double) i2, (double) i3, (double) i4);
        } else {
            return true;
        }
    }

    public void rotate(double d) {
        this.transform.rotate(d);
    }

    public void rotate(double d, double d2, double d3) {
        this.transform.rotate(d, d2, d3);
    }

    public void scale(double d, double d2) {
        this.transform.scale(d, d2);
    }

    public void setRenderingHint(RenderingHints.Key key, Object obj) {
        if (this.hints == null) {
            this.hints = new RenderingHints(key, obj);
        } else {
            this.hints.put(key, obj);
        }
    }

    public void unsetTransform() {
    }

    public void shear(double d, double d2) {
        this.transform.shear(d, d2);
    }

    public void transform(AffineTransform affineTransform) {
        this.transform.concatenate(affineTransform);
    }

    public void translate(int i, int i2) {
        translate((double) i, (double) i2);
    }

    public void translate(double d, double d2) {
        this.transform.translate(d, d2);
    }

    public void clearRect(int i, int i2, int i3, int i4) {
        fillRect((int) this.transform.getTranslateX(), (int) this.transform.getTranslateY(), i3, i4);
    }

    public void clipRect(int i, int i2, int i3, int i4) {
        if (!this.clippingOn) {
            setClip(i, i2, i3, i4);
            return;
        }
        Rectangle bounds = this.transform.createTransformedShape(new Rectangle(i, i2, i3, i4)).getBounds();
        SwingUtilities.computeIntersection(bounds.x, bounds.y, bounds.width, bounds.height, this.untClip);
    }

    public void copyArea(int i, int i2, int i3, int i4, int i5, int i6) {
    }

    public Graphics create() {
        RGraphics2 createGraphics = this.context.createGraphics();
        createGraphics.init(this);
        return createGraphics;
    }

    public void dispose() {
    }

    public void drawArc(int i, int i2, int i3, int i4, int i5, int i6) {
        draw(new Arc2D.Float((float) i, (float) i2, (float) i3, (float) i4, (float) i5, (float) i6, 0));
    }

    public boolean drawImage(Image image, int i, int i2, ImageObserver imageObserver) {
        return drawImage(image, i, i2, i + image.getWidth(imageObserver), i2 + image.getHeight(imageObserver), 0, 0, image.getWidth(imageObserver), image.getHeight(imageObserver), this.backgroundColor, imageObserver);
    }

    public boolean drawImage(Image image, int i, int i2, Color color2, ImageObserver imageObserver) {
        return drawImage(image, i, i2, i + image.getWidth(imageObserver), i2 + image.getHeight(imageObserver), 0, 0, image.getWidth(imageObserver), image.getHeight(imageObserver), color2, imageObserver);
    }

    public boolean drawImage(Image image, int i, int i2, int i3, int i4, ImageObserver imageObserver) {
        return drawImage(image, i, i2, i + i3, i2 + i4, 0, 0, image.getWidth(imageObserver), image.getHeight(imageObserver), this.backgroundColor, imageObserver);
    }

    public boolean drawImage(Image image, int i, int i2, int i3, int i4, Color color2, ImageObserver imageObserver) {
        return drawImage(image, i, i2, i + i3, i2 + i4, 0, 0, image.getWidth(imageObserver), image.getHeight(imageObserver), color2, imageObserver);
    }

    public boolean drawImage(Image image, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, ImageObserver imageObserver) {
        return drawImage(image, i, i2, i3, i4, i5, i6, i7, i8, (Color) null, imageObserver);
    }

    public boolean drawImage(Image image, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, Color color2, ImageObserver imageObserver) {
        BaseTexture texture;
        if (image == null || image.getWidth((ImageObserver) null) == -1 || image.getHeight((ImageObserver) null) == -1) {
            return false;
        }
        if ((color2 != null && color2.getAlpha() == 0) || (texture = this.context.getTexture(image)) == null) {
            return false;
        }
        tempP1.setLocation((double) i3, (double) i2);
        tempP2.setLocation((double) i, (double) i2);
        tempP3.setLocation((double) i, (double) i4);
        tempP4.setLocation((double) i3, (double) i4);
        this.transform.transform(tempP1, tempP1);
        this.transform.transform(tempP2, tempP2);
        this.transform.transform(tempP3, tempP3);
        this.transform.transform(tempP4, tempP4);
        int lastIdx = this.guiScene.getLastIdx();
        float f = 1.0f;
        float f2 = 1.0f;
        if (image instanceof TexBackedImage) {
            TexBackedImage texBackedImage = (TexBackedImage) image;
            f = texBackedImage.getXmul();
            f2 = texBackedImage.getYmul();
        } else if (texture.getTarget() == 34037) {
            f = (float) image.getWidth(imageObserver);
            f2 = (float) image.getHeight(imageObserver);
        } else if ((!Texture.isPowerOf2(image.getWidth(imageObserver)) || !Texture.isPowerOf2(image.getHeight(imageObserver))) && !GLSupportedCaps.isTextureNPOT()) {
            int roundToPowerOf2 = Texture.roundToPowerOf2(image.getWidth(imageObserver));
            int roundToPowerOf22 = Texture.roundToPowerOf2(image.getHeight(imageObserver));
            f = ((float) image.getWidth(imageObserver)) / ((float) roundToPowerOf2);
            f2 = ((float) image.getHeight(imageObserver)) / ((float) roundToPowerOf22);
        }
        float width = (float) image.getWidth(imageObserver);
        float height = (float) image.getHeight(imageObserver);
        this.guiScene.setClip(this);
        this.guiScene.setColor(color2, this.composedColorMultiplier);
        this.guiScene.addVertex((float) tempP1.getX(), (float) tempP1.getY(), 0.0f);
        this.guiScene.setCurrentTexCoord(0, (((float) i7) / width) * f, (((float) i6) / height) * f2);
        this.guiScene.addVertex((float) tempP2.getX(), (float) tempP2.getY(), 0.0f);
        this.guiScene.setCurrentTexCoord(0, (((float) i5) / width) * f, (((float) i6) / height) * f2);
        this.guiScene.addVertex((float) tempP3.getX(), (float) tempP3.getY(), 0.0f);
        this.guiScene.setCurrentTexCoord(0, (((float) i5) / width) * f, (((float) i8) / height) * f2);
        this.guiScene.addVertex((float) tempP4.getX(), (float) tempP4.getY(), 0.0f);
        this.guiScene.setCurrentTexCoord(0, f * (((float) i7) / width), f2 * (((float) i8) / height));
        this.guiScene.setEnableColorArray(false);
        this.guiScene.setBlendFunc(1, 771);
        this.guiScene.add(new ImageDraw(texture, lastIdx, 4));
        return true;
    }

    /* access modifiers changed from: package-private */
    public int outcode(float f, float f2) {
        int i = 0;
        if (f2 < ((float) this.untClip.y)) {
            i = 8;
        } else if (f2 > ((float) (this.untClip.y + this.untClip.height))) {
            i = 2;
        }
        if (f > ((float) (this.untClip.x + this.untClip.width))) {
            return i | 4;
        }
        if (f < ((float) this.untClip.x)) {
            return i | 1;
        }
        return i;
    }

    public void drawLine(int i, int i2, int i3, int i4) {
        draw(new Line2D.Float((float) i, (float) i2, (float) i3, (float) i4));
    }

    public void drawOval(int i, int i2, int i3, int i4) {
        draw(new Ellipse2D.Float((float) i, (float) i2, (float) i3, (float) i4));
    }

    public void drawPolygon(int[] iArr, int[] iArr2, int i) {
        draw(new Polygon(iArr, iArr2, i));
    }

    public void drawPolyline(int[] iArr, int[] iArr2, int i) {
        for (int i2 = 0; i2 < i - 1; i2++) {
            drawLine(iArr[i2], iArr2[i2], iArr[i2 + 1], iArr2[i2 + 1]);
        }
    }

    public void drawRoundRect(int i, int i2, int i3, int i4, int i5, int i6) {
        draw(new RoundRectangle2D.Float((float) i, (float) i2, (float) i3, (float) i4, (float) i5, (float) i6));
    }

    public void fillArc(int i, int i2, int i3, int i4, int i5, int i6) {
        draw(new Arc2D.Float((float) i, (float) i2, (float) i3, (float) i4, (float) i5, (float) i6, 2));
    }

    public void fillOval(int i, int i2, int i3, int i4) {
        fill(new Ellipse2D.Float((float) i, (float) i2, (float) i3, (float) i4));
    }

    public void fillPolygon(int[] iArr, int[] iArr2, int i) {
        fill(new Polygon(iArr, iArr2, i));
    }

    public void fillRect(int i, int i2, int i3, int i4) {
        fill(new Rectangle2D.Float((float) i, (float) i2, (float) i3, (float) i4));
    }

    public void fillRoundRect(int i, int i2, int i3, int i4, int i5, int i6) {
        fill(new RoundRectangle2D.Float((float) i, (float) i2, (float) i3, (float) i4, (float) i5, (float) i6));
    }

    public Color getColor() {
        return this.color;
    }

    public void setColor(Color color2) {
        this.color = color2;
    }

    public Font getFont() {
        return this.font;
    }

    public void setFont(Font font2) {
        if (font2 != this.font) {
            this.font = font2;
            this.graphicalFont = this.context.getFont(font2);
        }
    }

    public taikodom.render.textures.Font getGraphicalFont() {
        return this.graphicalFont;
    }

    public FontMetrics getFontMetrics(Font font2) {
        return this.context.getFontMetrics(font2);
    }

    public void setClip(int i, int i2, int i3, int i4) {
        setClip(new Rectangle(i, i2, i3, i4));
    }

    private Shape untransformShape(Shape shape) {
        if (shape == null) {
            return null;
        }
        try {
            return transformShape(this.transform.createInverse(), shape);
        } catch (NoninvertibleTransformException e) {
            throw new RuntimeException(e);
        }
    }

    public Shape getClip() {
        return untransformShape(this.untClip);
    }

    public void setClip(Shape shape) {
        if (shape == null) {
            this.clippingOn = false;
            this.untClip.setBounds(-10000, -10000, 20000, 20000);
            return;
        }
        this.clippingOn = true;
        this.untClip.setBounds(this.transform.createTransformedShape(shape).getBounds());
    }

    public Rectangle getClipBounds() {
        if (!this.clippingOn) {
            return null;
        }
        return getClip().getBounds();
    }

    public Rectangle getClipBounds(Rectangle rectangle) {
        if (rectangle == null) {
            throw new NullPointerException("null rectangle parameter");
        }
        if (this.clippingOn) {
            rectangle.setBounds(getClipBounds());
        }
        return rectangle;
    }

    public void drawRect(int i, int i2, int i3, int i4) {
        if (this.color.getAlpha() != 0 && i3 >= 0 && i4 >= 0) {
            draw(new Rectangle(i, i2, i3, i4));
        }
    }

    public void setPaintMode() {
    }

    public void setXORMode(Color color2) {
    }

    public Color getBackgroundColor() {
        return this.backgroundColor;
    }

    public void setBackgroundColor(Color color2) {
        this.backgroundColor = color2;
    }

    public Graphics create(int i, int i2, int i3, int i4) {
        Graphics create = create();
        if (create == null) {
            return null;
        }
        create.translate(i, i2);
        create.clipRect(0, 0, i3, i4);
        return create;
    }

    public RGuiScene getGuiScene() {
        return this.guiScene;
    }

    public void untransformedClipBounds(Rectangle rectangle) {
        rectangle.setBounds(this.untClip);
    }

    public Color getColorMultiplier() {
        return this.colorMultiplier;
    }

    public void setColorMultiplier(Color color2) {
        this.colorMultiplier = color2;
        if (color2 == null || Color.WHITE.equals(color2)) {
            this.composedColorMultiplier = this.parent.composedColorMultiplier;
        } else {
            this.composedColorMultiplier = new Color((color2.getRed() * this.parentColorMultiplier.getRed()) / 255, (color2.getGreen() * this.parentColorMultiplier.getGreen()) / 255, (color2.getBlue() * this.parentColorMultiplier.getBlue()) / 255, (color2.getAlpha() * this.parentColorMultiplier.getAlpha()) / 255);
        }
    }

    public void blur(int i, int i2, int i3, int i4) {
        int translateY = (int) (((double) i2) + this.transform.getTranslateY());
        this.guiScene.setClip(this);
        this.guiScene.addBlur((int) (((double) i) + this.transform.getTranslateX()), translateY, i3, i4);
    }

    public RGraphics2 getParent() {
        return this.parent;
    }

    /* renamed from: taikodom.render.graphics2d.RGraphics2$a */
    class C5130a extends TextDraw {
        private final /* synthetic */ double[] eXO;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C5130a(taikodom.render.textures.Font font, Color color, String str, float f, float f2, double[] dArr) {
            super(font, color, str, f, f2);
            this.eXO = dArr;
        }

        public void draw(DrawContext drawContext, RGuiScene rGuiScene) {
            GL gl = drawContext.getGl();
            gl.glPushMatrix();
            gl.glMultMatrixd(this.eXO, 0);
            super.draw(drawContext, rGuiScene);
            gl.glPopMatrix();
        }
    }
}
