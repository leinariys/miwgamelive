package taikodom.render.helpers;

import taikodom.render.RenderView;

/* compiled from: a */
public interface RenderTask {
    void run(RenderView renderView);
}
