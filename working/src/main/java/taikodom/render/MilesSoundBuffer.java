package taikodom.render;

import logic.render.miles.AdapterMilesBridgeJNI;
import logic.render.miles.aHS;
import lombok.extern.slf4j.Slf4j;
import taikodom.render.loader.provider.SoundBufferLoader;

import java.nio.Buffer;

/* compiled from: a */
@Slf4j
public class MilesSoundBuffer extends SoundBuffer {
    static int totalMilesBufferUsage = 0;
    private String extension = "";
    private String fileName = "";
    private aHS soundInfo = new aHS();

    public static int getTotalBytes() {
        return totalMilesBufferUsage;
    }

    public boolean setData(Buffer buffer, int i) {
        if (buffer == null) {
            return false;
        }
        synchronized (this) {
            this.length = i;
            this.data = buffer;
            totalMilesBufferUsage += i;
            fillInternalBuffer(getSoundInfo());
        }
        return true;
    }

    public int getLength() {
        return this.length;
    }

    public String getExtension() {
        return this.extension;
    }

    public void setExtension(String str) {
        this.extension = str;
    }

    public Buffer getData() {
        if (this.data == null) {
            SoundBufferLoader.addBufferRequest(this);
        }
        return this.data;
    }

    public aHS getSoundInfo() {
        fillInternalBuffer(this.soundInfo);
        return this.soundInfo;
    }

    private void fillInternalBuffer(aHS ahs) {
        if (ahs.ddK() == null && this.extension.equals(".wav")) {
            if (AdapterMilesBridgeJNI.m21703a(this.data, ahs) == 0) {
                log.warn("Unable to parse WAV info from file " + this.fileName);
                return;
            }
            this.sampleRate = (int) ahs.ddM();
            this.bitDepth = ahs.mo9188DH();
            this.channels = ahs.getChannels();
            this.dataLenght = ahs.ddL();
            this.timeLenght = (8.0f * ((float) this.dataLenght)) / ((float) ((this.bitDepth * this.channels) * this.sampleRate));
        }
    }

    public int getSampleRate() {
        return this.sampleRate;
    }

    public int getBitDepth() {
        return this.bitDepth;
    }

    public int getBufferId() {
        return 0;
    }

    public int getChannels() {
        return this.channels;
    }

    public long getSampleCount() {
        return 1;
    }

    public float getTimeLength() {
        return this.timeLenght;
    }

    public boolean isValid() {
        return true;
    }

    public MilesSoundBuffer cloneAsset() {
        return this;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String str) {
        this.fileName = str;
        this.extension = str.substring(str.lastIndexOf("."));
    }

    public void forceSyncLoading() {
        if (this.data == null) {
            SoundBufferLoader.syncLoadBuffer(this);
        }
    }
}
