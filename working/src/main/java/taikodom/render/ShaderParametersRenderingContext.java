package taikodom.render;

import taikodom.render.camera.Camera;
import taikodom.render.primitives.Light;
import taikodom.render.primitives.TreeRecord;

import javax.media.opengl.GL;

/* compiled from: a */
public class ShaderParametersRenderingContext {
    private Camera camera;

    /* renamed from: gl */
    private GL f10341gl;
    private Light light;
    private DrawContext renderContext;
    private TreeRecord treeRecord;

    /* renamed from: gl */
    public GL mo25337gl() {
        return this.f10341gl;
    }

    public TreeRecord getTreeRecord() {
        return this.treeRecord;
    }

    public void setTreeRecord(TreeRecord treeRecord2) {
        this.treeRecord = treeRecord2;
    }

    public Light getLight() {
        return this.light;
    }

    public void setLight(Light light2) {
        this.light = light2;
    }

    public DrawContext getRenderContext() {
        return this.renderContext;
    }

    public void setRenderContext(DrawContext drawContext) {
        this.renderContext = drawContext;
    }

    public Camera getCamera() {
        return this.camera;
    }

    public void setCamera(Camera camera2) {
        this.camera = camera2;
    }
}
