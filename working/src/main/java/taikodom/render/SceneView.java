package taikodom.render;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.*;
import org.mozilla.javascript.ScriptRuntime;
import taikodom.render.camera.Camera;
import taikodom.render.postProcessing.PostProcessingFX;
import taikodom.render.raytrace.RayTraceSceneObjectInfoD;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;
import taikodom.render.scene.SceneViewQuality;
import taikodom.render.scene.Viewport;
import taikodom.render.textures.RenderTarget;

import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3f;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/* compiled from: a */
public class SceneView {
    public static TransformWrap tempTransform = new TransformWrap();
    public static Vector4dWrap tempVec4d = new Vector4dWrap();
    public static Vector4dWrap tempVec4d2 = new Vector4dWrap();
    private final SceneViewQuality sceneViewQuality;
    public List<PostProcessingFX> postProcessingFX;
    private boolean allowImpostors;
    private Camera camera;
    private boolean enabled;
    private String handle;
    private int priority;
    private RenderInfo renderInfo;
    private RenderTarget renderTarget;
    private Scene scene;
    private Viewport viewport;

    public SceneView() {
        this(new Scene(), new Camera());
    }

    public SceneView(Scene scene2, Camera camera2) {
        this.viewport = new Viewport(0, 0, 1024, 768);
        this.renderInfo = new RenderInfo();
        this.sceneViewQuality = new SceneViewQuality();
        this.postProcessingFX = new ArrayList();
        this.enabled = true;
        this.scene = scene2;
        this.camera = camera2;
    }

    public SceneView(SceneView sceneView) {
        this.viewport = new Viewport(0, 0, 1024, 768);
        this.renderInfo = new RenderInfo();
        this.sceneViewQuality = new SceneViewQuality();
        this.postProcessingFX = new ArrayList();
        this.enabled = true;
        set(sceneView);
    }

    public void set(SceneView sceneView) {
        this.scene = sceneView.scene;
        this.camera = sceneView.camera;
        this.viewport.set(sceneView.viewport);
        this.renderInfo.set(sceneView.renderInfo);
        this.sceneViewQuality.set(sceneView.sceneViewQuality);
        this.postProcessingFX.clear();
        this.postProcessingFX.addAll(sceneView.postProcessingFX);
        this.priority = sceneView.priority;
        this.enabled = sceneView.enabled;
        this.renderTarget = sceneView.renderTarget;
        this.allowImpostors = sceneView.allowImpostors;
    }

    public Viewport getViewport() {
        return this.viewport;
    }

    public void setViewport(Viewport viewport2) {
        this.viewport = viewport2;
    }

    public void setViewport(int i, int i2, int i3, int i4) {
        this.viewport.x = i;
        this.viewport.y = i2;
        this.viewport.width = i3;
        this.viewport.height = i4;
    }

    public RenderInfo getRenderInfo() {
        return this.renderInfo;
    }

    public void setRenderInfo(RenderInfo renderInfo2) {
        this.renderInfo = renderInfo2;
    }

    public Scene getScene() {
        return this.scene;
    }

    public void setScene(Scene scene2) {
        this.scene = scene2;
    }

    public Camera getCamera() {
        return this.camera;
    }

    public void setCamera(Camera camera2) {
        this.camera = camera2;
    }

    public Color getClearColor() {
        return this.renderInfo.getClearColor();
    }

    public Color getFogColor() {
        return this.renderInfo.getFogColor();
    }

    public void setFogColor(Color color) {
        this.renderInfo.setFogColor(color);
    }

    public float getFogEnd() {
        return this.renderInfo.getFogEnd();
    }

    public void setFogEnd(float f) {
        this.renderInfo.setFogEnd(f);
    }

    public float getFogExp() {
        return this.renderInfo.getFogExp();
    }

    public void setFogExp(float f) {
        this.renderInfo.setFogExp(f);
    }

    public float getFogStart() {
        return this.renderInfo.getFogStart();
    }

    public void setFogStart(float f) {
        this.renderInfo.setFogStart(f);
    }

    public int getFogType() {
        return this.renderInfo.getFogType();
    }

    public void setFogType(int i) {
        this.renderInfo.setFogType(i);
    }

    public boolean isClearColorBuffer() {
        return this.renderInfo.isClearColorBuffer();
    }

    public void setClearColorBuffer(boolean z) {
        this.renderInfo.setClearColorBuffer(z);
    }

    public boolean isClearDepthBuffer() {
        return this.renderInfo.isClearDepthBuffer();
    }

    public void setClearDepthBuffer(boolean z) {
        this.renderInfo.setClearDepthBuffer(z);
    }

    public boolean isClearStencilBuffer() {
        return this.renderInfo.isClearStencilBuffer();
    }

    public void setClearStencilBuffer(boolean z) {
        this.renderInfo.setClearStencilBuffer(z);
    }

    public boolean isWireframeOnly() {
        return this.renderInfo.isWireframeOnly();
    }

    public void setWireframeOnly(boolean z) {
        this.renderInfo.setWireframeOnly(z);
    }

    public RayTraceSceneObjectInfoD rayTraceMouseOverScene(Vector2fWrap aka) {
        return rayTraceMouseOverScene(aka, true);
    }

    public RayTraceSceneObjectInfoD rayTraceMouseOverScene(Vector2fWrap aka, boolean z) {
        Vec3f vec3f = new Vec3f();
        vec3f.x = aka.x / ((float) getViewport().width);
        vec3f.y = aka.y / ((float) getViewport().height);
        vec3f.z = 0.0f;
        Vec3d normScreenPosToWorldPos = getNormScreenPosToWorldPos(vec3f);
        vec3f.z = 1.0f;
        Vec3d normScreenPosToWorldPos2 = getNormScreenPosToWorldPos(vec3f);
        if (this.camera.isOrthogonal()) {
            normScreenPosToWorldPos.z *= -1.0d;
            normScreenPosToWorldPos2.z *= -1.0d;
        }
        RayTraceSceneObjectInfoD rayTraceSceneObjectInfoD = new RayTraceSceneObjectInfoD(normScreenPosToWorldPos, normScreenPosToWorldPos2);
        Stack stack = new Stack();
        synchronized (this.scene.getChildren()) {
            for (SceneObject add : this.scene.getChildren()) {
                stack.add(add);
            }
        }
        while (!stack.empty()) {
            SceneObject sceneObject = (SceneObject) stack.pop();
            if (sceneObject.isAllowSelection()) {
                if (!z) {
                    synchronized (sceneObject.getChildren()) {
                        for (SceneObject add2 : sceneObject.getChildren()) {
                            stack.add(add2);
                        }
                    }
                }
                aDM a = sceneObject.getAabbWorldSpace().mo9831a(normScreenPosToWorldPos, normScreenPosToWorldPos2, true);
                if (a.isIntersected()) {
                    rayTraceSceneObjectInfoD.setChanged(false);
                    sceneObject.rayIntersects(rayTraceSceneObjectInfoD, true, z);
                    if (rayTraceSceneObjectInfoD.isIntersected() && rayTraceSceneObjectInfoD.isChanged()) {
                        rayTraceSceneObjectInfoD.setIntersectedPoint(normScreenPosToWorldPos.mo9504d((Tuple3d) rayTraceSceneObjectInfoD.getRayDelta().mo9476Z(rayTraceSceneObjectInfoD.getParamIntersection())));
                        rayTraceSceneObjectInfoD.getPlane().setNormal(a.getNormal());
                        rayTraceSceneObjectInfoD.getPlane().mo718ac(-rayTraceSceneObjectInfoD.getNormal().mo9493az(rayTraceSceneObjectInfoD.getIntersectedPoint()));
                        rayTraceSceneObjectInfoD.setIntersectedObject(sceneObject);
                    }
                }
            }
        }
        return rayTraceSceneObjectInfoD;
    }

    public Vec3d getScreenPosToWorldPos(Vec3f vec3f) {
        Vec3f vec3f2 = new Vec3f((Vector3f) vec3f);
        vec3f2.x = vec3f.x / ((float) getViewport().width);
        vec3f2.y = vec3f.y / ((float) getViewport().height);
        return getNormScreenPosToWorldPos(vec3f2);
    }

    public double getDistanceForWidthSize(double d, float f) {
        return (((double) this.viewport.width) / (((double) (this.camera.getAspect() * f)) / d)) / ((double) (2.0f * this.camera.getTanHalfFovY()));
    }

    public double getDistanceForHeightSize(double d, float f) {
        return (((double) this.viewport.height) / (((double) f) / d)) / ((double) (2.0f * this.camera.getTanHalfFovY()));
    }

    public float getSizeOnScreen(Vec3d ajr, double d) {
        float ax = (float) ajr.mo9491ax(this.camera.getPosition());
        if (ax == 0.0f) {
            return (float) this.viewport.width;
        }
        return (float) (((double) (((float) this.viewport.width) / (ax * (2.0f * this.camera.getTanHalfFovY())))) * d);
    }

    public Vec3d getNormScreenPosToWorldPos(Vec3f vec3f) {
        Vector4fWrap ajf = new Vector4fWrap();
        ajf.x = (float) (((double) (vec3f.x * 2.0f)) - 1.0d);
        ajf.y = (float) (((double) (vec3f.y * 2.0f)) - 1.0d);
        ajf.z = (float) (((double) (vec3f.z * 2.0f)) - 1.0d);
        ajf.w = 1.0f;
        Matrix4fWrap ajk = new Matrix4fWrap();
        ajk.invert(this.camera.getProjection());
        ajk.transform(ajf);
        if (ajf.w == 0.0f) {
            return vec3f.dfR();
        }
        Vec3d ajr = new Vec3d((double) ajf.x, (double) ajf.y, (double) ajf.z);
        ajr.mo9487aa((double) (1.0f / ajf.w));
        this.camera.getTransform().mo17331a(ajr);
        return ajr;
    }

    public boolean getWindowCoords(Vec3d ajr, Vec3d ajr2) {
        Camera camera2 = this.camera;
        Vector4dWrap ajd = tempVec4d;
        Vector4dWrap ajd2 = tempVec4d2;
        ajd.x = ajr.x - camera2.getTransform().position.x;
        ajd.y = ajr.y - camera2.getTransform().position.y;
        ajd.z = ajr.z - camera2.getTransform().position.z;
        ajd.w = 1.0d;
        tempTransform.mo17334a(camera2.getTransform().orientation);
        tempTransform.mo17341b(ajd, ajd2);
        camera2.getProjection().mo14005c(ajd2, ajd);
        if (ajd.w == ScriptRuntime.NaN) {
            return false;
        }
        ajd.x /= ajd.w;
        ajd.y /= ajd.w;
        ajd.z /= ajd.w;
        ajr2.x = ((double) this.viewport.x) + (((ajd.x + 1.0d) * ((double) this.viewport.width)) / 2.0d);
        ajr2.y = ((double) this.viewport.y) + (((ajd.y + 1.0d) * ((double) this.viewport.height)) / 2.0d);
        ajr2.z = (ajd.z + 1.0d) / 2.0d;
        return true;
    }

    public void setNoShading(boolean z) {
        this.renderInfo.setNoShading(z);
    }

    public void setDrawAABBs(boolean z) {
        this.renderInfo.setDrawAABBs(z);
    }

    public void setDrawLights(boolean z) {
        this.renderInfo.setDrawLights(z);
    }

    public int getPriority() {
        return this.priority;
    }

    public void setPriority(int i) {
        this.priority = i;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean z) {
        this.enabled = z;
    }

    public void enable() {
        setEnabled(true);
    }

    public void disable() {
        setEnabled(false);
    }

    public SceneViewQuality getSceneViewQuality() {
        return this.sceneViewQuality;
    }

    public void setSceneViewQuality(SceneViewQuality sceneViewQuality2) {
        this.sceneViewQuality.set(sceneViewQuality2);
    }

    public RenderTarget getRenderTarget() {
        return this.renderTarget;
    }

    public void setRenderTarget(RenderTarget renderTarget2) {
        this.renderTarget = renderTarget2;
    }

    public void addPostProcessingFX(PostProcessingFX postProcessingFX2) {
        this.postProcessingFX.add(postProcessingFX2);
    }

    public void removePostProcessingFX(PostProcessingFX postProcessingFX2) {
        this.postProcessingFX.remove(postProcessingFX2);
    }

    public List<PostProcessingFX> getPostProcessingFXs() {
        return this.postProcessingFX;
    }

    public boolean isAllowImpostors() {
        return this.allowImpostors;
    }

    public void setAllowImpostors(boolean z) {
        this.allowImpostors = z;
    }

    public String getHandle() {
        return this.handle;
    }

    public void setHandle(String str) {
        this.handle = str;
    }
}
