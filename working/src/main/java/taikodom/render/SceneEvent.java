package taikodom.render;

/* compiled from: a */
public class SceneEvent {
    String message;
    Object object;

    public SceneEvent(String str, Object obj) {
        this.message = str;
        this.object = obj;
    }

    public String getMessage() {
        return this.message;
    }

    public Object getObject() {
        return this.object;
    }
}
