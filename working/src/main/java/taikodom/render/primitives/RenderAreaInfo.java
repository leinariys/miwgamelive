package taikodom.render.primitives;

import com.hoplon.geometry.Color;
import taikodom.render.textures.Texture;

/* compiled from: a */
public class RenderAreaInfo {
    public final Color ambientColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
    public Texture diffuseCubemap;
    public Texture reflectiveCubemap;
}
