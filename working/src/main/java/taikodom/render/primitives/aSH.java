package taikodom.render.primitives;

import com.hoplon.geometry.Vec3f;
import game.geometry.C2567gu;

import javax.vecmath.Vector3f;

/* renamed from: a.aSH */
/* compiled from: a */
public class aSH {

    /* renamed from: Oj */
    public Vec3f f3735Oj;
    public Vec3f iNl;

    /* renamed from: p0 */
    public Vec3f f3736p0;

    /* renamed from: p1 */
    public Vec3f f3737p1;

    /* renamed from: p2 */
    public Vec3f f3738p2;

    public aSH() {
        this.f3736p0 = new Vec3f();
        this.f3737p1 = new Vec3f();
        this.f3738p2 = new Vec3f();
    }

    public aSH(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        this.f3736p0 = vec3f;
        this.f3737p1 = vec3f2;
        this.f3738p2 = vec3f3;
    }

    public Vec3f duL() {
        return this.f3736p0;
    }

    /* renamed from: bS */
    public void mo11339bS(Vec3f vec3f) {
        this.f3736p0 = vec3f;
        this.f3735Oj = null;
        this.iNl = null;
    }

    /* renamed from: I */
    public void mo11335I(float f, float f2, float f3) {
        this.f3736p0.set(f, f2, f3);
        this.f3735Oj = null;
        this.iNl = null;
    }

    public Vec3f duM() {
        return this.f3737p1;
    }

    /* renamed from: bT */
    public void mo11340bT(Vec3f vec3f) {
        this.f3737p1 = vec3f;
        this.f3735Oj = null;
        this.iNl = null;
    }

    /* renamed from: J */
    public void mo11336J(float f, float f2, float f3) {
        this.f3737p1.set(f, f2, f3);
        this.f3735Oj = null;
        this.iNl = null;
    }

    public Vec3f duN() {
        return this.f3738p2;
    }

    /* renamed from: bU */
    public void mo11341bU(Vec3f vec3f) {
        this.f3738p2 = vec3f;
        this.f3735Oj = null;
        this.iNl = null;
    }

    /* renamed from: K */
    public void mo11338K(float f, float f2, float f3) {
        this.f3738p2.set(f, f2, f3);
        this.f3735Oj = null;
        this.iNl = null;
    }

    /* renamed from: q */
    public void mo11347q(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        this.f3736p0 = vec3f;
        this.f3737p1 = vec3f2;
        this.f3738p2 = vec3f3;
        this.f3735Oj = null;
        this.iNl = null;
    }

    public String toString() {
        return String.format("%s %s %s", new Object[]{this.f3736p0, this.f3737p1, this.f3738p2});
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof aSH)) {
            return false;
        }
        aSH ash = (aSH) obj;
        if (!ash.f3736p0.equals(this.f3736p0) || !ash.f3737p1.equals(this.f3737p1) || !ash.f3738p2.equals(this.f3738p2)) {
            return false;
        }
        return true;
    }

    public C2567gu rayIntersects(Vec3f vec3f, Vec3f vec3f2) {
        return rayIntersects(new C2567gu(vec3f, vec3f2));
    }

    public C2567gu rayIntersects(C2567gu guVar) {
        float f;
        float f2;
        float f3;
        float f4;
        float f5;
        float f6;
        Vec3f b = this.f3737p1.mo23506j(this.f3736p0).mo23476b(this.f3738p2.mo23506j(this.f3737p1));
        float dot = b.dot(guVar.mo19147vR());
        if (dot < 0.0f) {
            float dot2 = b.dot(this.f3736p0) - b.dot(guVar.mo19145vP());
            if (dot2 <= 0.0f && dot2 >= dot) {
                float f7 = dot2 / dot;
                Vec3f i = guVar.mo19145vP().mo23503i(guVar.mo19147vR().mo23510mS(f7));
                if (Math.abs(b.x) > Math.abs(b.y)) {
                    if (Math.abs(b.x) > Math.abs(b.z)) {
                        f = i.y - this.f3736p0.y;
                        f2 = this.f3737p1.y - this.f3736p0.y;
                        f3 = this.f3738p2.y - this.f3736p0.y;
                        f4 = i.z - this.f3736p0.z;
                        f5 = this.f3737p1.z - this.f3736p0.z;
                        f6 = this.f3738p2.z - this.f3736p0.z;
                    } else {
                        f = i.x - this.f3736p0.x;
                        f2 = this.f3737p1.x - this.f3736p0.x;
                        f3 = this.f3738p2.x - this.f3736p0.x;
                        f4 = i.y - this.f3736p0.y;
                        f5 = this.f3737p1.y - this.f3736p0.y;
                        f6 = this.f3738p2.y - this.f3736p0.y;
                    }
                } else if (Math.abs(b.y) > Math.abs(b.z)) {
                    f = i.x - this.f3736p0.x;
                    f2 = this.f3737p1.x - this.f3736p0.x;
                    f3 = this.f3738p2.x - this.f3736p0.x;
                    f4 = i.z - this.f3736p0.z;
                    f5 = this.f3737p1.z - this.f3736p0.z;
                    f6 = this.f3738p2.z - this.f3736p0.z;
                } else {
                    f = i.x - this.f3736p0.x;
                    f2 = this.f3737p1.x - this.f3736p0.x;
                    f3 = this.f3738p2.x - this.f3736p0.x;
                    f4 = i.y - this.f3736p0.y;
                    f5 = this.f3737p1.y - this.f3736p0.y;
                    f6 = this.f3738p2.y - this.f3736p0.y;
                }
                float f8 = (f2 * f6) - (f5 * f3);
                if (f8 != 0.0f) {
                    float f9 = 1.0f / f8;
                    float f10 = ((f6 * f) - (f3 * f4)) * f9;
                    if (f10 >= 0.0f) {
                        float f11 = ((f4 * f2) - (f5 * f)) * f9;
                        if (f11 >= 0.0f && (1.0f - f10) - f11 >= 0.0f) {
                            guVar.setIntersected(true);
                            guVar.mo19134ac(f7);
                            guVar.mo19138l(b.dfO());
                        }
                    }
                }
            }
        }
        return guVar;
    }

    /* renamed from: Jh */
    public Vec3f mo11337Jh() {
        if (this.f3735Oj == null) {
            this.f3735Oj = this.f3736p0.mo23506j(this.f3737p1).mo23489d((Vector3f) this.f3736p0.mo23506j(this.f3738p2)).dfP();
        }
        return this.f3735Oj;
    }

    public Vec3f duO() {
        if (this.iNl == null) {
            this.iNl = new Vec3f(((this.f3736p0.x + this.f3737p1.x) + this.f3738p2.x) / 3.0f, ((this.f3736p0.y + this.f3737p1.y) + this.f3738p2.y) / 3.0f, ((this.f3736p0.z + this.f3737p1.z) + this.f3738p2.z) / 3.0f);
        }
        return this.iNl;
    }
}
