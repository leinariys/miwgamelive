package taikodom.render.textures;

import game.geometry.Matrix4fWrap;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public abstract class BaseTexture extends RenderAsset {
    public static final Matrix4fWrap FLIP_Y_MATRIX = new Matrix4fWrap(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f);
    public static int textureMemoryUsed;
    public final Matrix4fWrap transform = new Matrix4fWrap(FLIP_Y_MATRIX);
    public int bpp;
    public int height;
    public int target = 3553;
    public int width;
    private float screenArea;
    private float tempScreenArea;

    public static int getTextureMemoryUsed() {
        return textureMemoryUsed;
    }

    public static void addTextureMemoryUsed(int i) {
        textureMemoryUsed += i;
    }

    public abstract void bind(int i, DrawContext drawContext);

    public abstract void bind(DrawContext drawContext);

    public Matrix4fWrap getTransform() {
        return this.transform;
    }

    public void setTransform(Matrix4fWrap ajk) {
        this.transform.set(FLIP_Y_MATRIX);
        Matrix4fWrap.m22816c(FLIP_Y_MATRIX, ajk, this.transform);
    }

    public int getSizeX() {
        return this.width;
    }

    public void setSizeX(int i) {
        this.width = i;
    }

    public int getSizeY() {
        return this.height;
    }

    public void setSizeY(int i) {
        this.height = i;
    }

    public int getBpp() {
        return this.bpp;
    }

    public void setBpp(int i) {
        this.bpp = i;
    }

    public float getTransformedWidth() {
        return Math.abs(((float) this.width) * this.transform.m00);
    }

    public float getTransformedHeight() {
        return Math.abs(((float) this.height) * this.transform.m11);
    }

    public void addArea(float f) {
        if (f > this.tempScreenArea) {
            this.tempScreenArea = f;
        }
    }

    public void resetTextureArea() {
        this.screenArea = this.tempScreenArea;
        this.tempScreenArea = 0.0f;
    }

    public float getScreenArea() {
        return this.screenArea > this.tempScreenArea ? this.screenArea : this.tempScreenArea;
    }

    public int getTexId() {
        return 0;
    }

    public int getTarget() {
        return this.target;
    }

    public float getXScaling() {
        return this.transform.m00;
    }

    public float getYScaling() {
        return this.transform.m11;
    }

    public int getMemorySize() {
        return 0;
    }
}
