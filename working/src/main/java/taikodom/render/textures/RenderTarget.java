package taikodom.render.textures;

import com.hoplon.geometry.Color;
import taikodom.render.DrawContext;
import taikodom.render.enums.FBOAttachTarget;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.loader.RenderAsset;

import javax.media.opengl.GL;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLException;

/* compiled from: a */
public class RenderTarget extends RenderAsset {
    private static final GLCapabilities PBUFFER_CAPS = new GLCapabilities();

    static {
        if (!GLSupportedCaps.isPbuffer()) {
            throw new GLException("PBuffers not supported with this graphics card");
        }
        PBUFFER_CAPS.setDoubleBuffered(false);
        PBUFFER_CAPS.setHardwareAccelerated(true);
        PBUFFER_CAPS.setPbufferRenderToTexture(false);
        PBUFFER_CAPS.setAlphaBits(8);
        PBUFFER_CAPS.setGreenBits(8);
        PBUFFER_CAPS.setRedBits(8);
        PBUFFER_CAPS.setBlueBits(8);
        PBUFFER_CAPS.setDepthBits(24);
        PBUFFER_CAPS.setStencilBits(8);
    }

    private FrameBufferObject fbo;
    /* renamed from: gl */
    private GL f10383gl;
    private int height;
    private boolean isBound = false;
    private boolean isOffscreen = false;
    private PBuffer pbuffer;
    private int target = 3553;
    private SimpleTexture texture;
    private int width;

    public void bind(DrawContext drawContext) {
        if (this.isBound) {
            throw new RuntimeException("RenderTarget already bound");
        }
        this.isBound = true;
        if (this.isOffscreen) {
            this.pbuffer = drawContext.getAuxPBuffer();
            this.pbuffer.activate(drawContext);
            GL gl = drawContext.getGl();
            gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            gl.glClear(DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ);
        } else if (this.fbo != null) {
            this.fbo.bind(drawContext);
        }
    }

    public void releaseReferences() {
        if (this.texture != null) {
            this.texture.releaseReferences();
            this.texture = null;
        }
        if (this.fbo != null) {
            this.fbo.releaseReferences();
            this.fbo = null;
        }
    }

    public void releaseReferences(GL gl) {
        if (this.texture != null) {
            this.texture.releaseReferences(gl);
            this.texture = null;
        }
    }

    public void createAsFBO(int i, int i2, DrawContext drawContext) {
        releaseReferences(drawContext.getGl());
        if (!GLSupportedCaps.isFbo()) {
            throw new RuntimeException("FBO extension not supported");
        }
        this.fbo = new FrameBufferObject();
        this.fbo.create(drawContext, i, i2, true);
        this.texture = new SimpleTexture();
        this.texture.setName("Simple texture for RT " + getName());
        this.texture.createRectangleEmpty(i, i2, 32);
        this.texture.fillWithColor(new Color(0.0f, 0.0f, 0.0f, 0.0f));
        this.fbo.addColorAttachTexture(drawContext, this.texture, FBOAttachTarget.TEXTURE_RECTANGLE);
    }

    public void createAsPBO(int i, int i2, DrawContext drawContext) {
        releaseReferences(drawContext.getGl());
        this.texture = new SimpleTexture();
        this.texture.setName("Simple texture for RT " + getName());
        this.texture.createEmptyRGBA(i, i2, 32);
        this.texture.fillWithColor(new Color(0.0f, 0.0f, 0.0f, 0.0f));
        this.isOffscreen = true;
    }

    public void create(int i, int i2, boolean z, DrawContext drawContext, boolean z2) {
        releaseReferences();
        this.f10383gl = drawContext.getGl();
        if (!z) {
            this.texture = new SimpleTexture();
            this.texture.setName("Simple texture for RT " + getName());
            this.texture.createEmptyRGBA(i, i2, 32);
            this.texture.fillWithColor(new Color(0.0f, 0.0f, 0.0f, 0.0f));
        } else if (GLSupportedCaps.isFbo() && !z2) {
            createAsFBO(i, i2, drawContext);
        } else if (GLSupportedCaps.isPbuffer()) {
            createAsPBO(i, i2, drawContext);
        } else {
            throw new GLException("pBuffers not supported with this graphics card");
        }
    }

    public void unbind(DrawContext drawContext) {
        if (!this.isBound) {
            throw new RuntimeException("RenderTarget not bound");
        }
        this.isBound = false;
        if (this.pbuffer != null) {
            this.pbuffer.deactivate(drawContext);
        } else if (this.fbo != null) {
            FrameBufferObject.unbind(drawContext);
        } else {
            this.texture.copyFromCurrentBuffer(drawContext.getGl());
        }
    }

    public void bindTexture(DrawContext drawContext) {
        this.texture.bind(drawContext);
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int i) {
        this.width = i;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int i) {
        this.height = i;
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public BaseTexture getTexture() {
        return this.texture;
    }

    public void setTexture(SimpleTexture simpleTexture) {
        this.texture = simpleTexture;
    }

    public void copyBufferToTexture(DrawContext drawContext) {
        if (this.fbo == null) {
            this.texture.copyFromCurrentBuffer(drawContext.getGl());
        }
    }

    public void copyBufferAreaToTexture(DrawContext drawContext, int i, int i2, int i3, int i4, int i5, int i6) {
        if (this.fbo == null) {
            this.texture.copyFromCurrentBuffer(this.f10383gl, i, i2, i3, i4, i5, i6);
        }
    }
}
