package taikodom.render.textures;

import com.sun.opengl.util.texture.TextureData;

/* compiled from: a */
public class TextureDataSource extends DataResource {
    public int height;
    public int width;
    private float actualAreaSize;
    private long lastRequestTime;

    public long getLastRequestTime() {
        return this.lastRequestTime;
    }

    public void setLastRequestTime(long j) {
        this.lastRequestTime = j;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int i) {
        this.width = i;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int i) {
        this.height = i;
    }

    public int getBpp() {
        return 0;
    }

    public float getActualAreaSize() {
        return this.actualAreaSize;
    }

    public void setActualAreaSize(float f) {
        this.actualAreaSize = f;
    }

    public TextureData[] getData() {
        return null;
    }

    public void setData(TextureData textureData) {
    }

    public TextureData getData(int i) {
        return null;
    }

    public void clearData() {
    }
}
