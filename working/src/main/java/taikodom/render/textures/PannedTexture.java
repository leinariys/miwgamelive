package taikodom.render.textures;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import taikodom.render.DrawContext;

/* compiled from: a */
public class PannedTexture extends Texture {
    public final Matrix4fWrap panMatrix = new Matrix4fWrap();
    public Texture texture;

    public void addArea(float f) {
        if (this.texture != null) {
            this.texture.addArea(f);
        }
    }

    public Texture getTexture() {
        return this.texture;
    }

    public void setTexture(Texture texture2) {
        this.texture = texture2;
    }

    public Vec3f getPanSpeed() {
        return new Vec3f(this.panMatrix.m03, this.panMatrix.m13, this.panMatrix.m23);
    }

    public void setPanSpeed(Vec3f vec3f) {
        this.panMatrix.m03 = vec3f.x;
        this.panMatrix.m13 = vec3f.y;
        this.panMatrix.m23 = vec3f.z;
    }

    public Vec3f getPanSpeed(Vec3f vec3f) {
        vec3f.x = this.panMatrix.m03;
        vec3f.y = this.panMatrix.m13;
        vec3f.z = this.panMatrix.m23;
        return vec3f;
    }

    public void bind(DrawContext drawContext) {
        if (this.texture != null) {
            this.texture.bind(drawContext);
        }
        drawContext.matrix4fTemp0.set(this.panMatrix);
        drawContext.matrix4fTemp0.m03 *= (float) drawContext.getTotalTime();
        drawContext.matrix4fTemp0.m13 *= (float) drawContext.getTotalTime();
        drawContext.matrix4fTemp0.m23 *= (float) drawContext.getTotalTime();
        drawContext.getGl().glMultMatrixf(drawContext.matrix4fTemp0.mo13998b(drawContext.tempBuffer0));
    }

    public void bind(int i, DrawContext drawContext) {
        if (this.texture != null) {
            this.texture.bind(i, drawContext);
        }
        drawContext.matrix4fTemp0.set(this.panMatrix);
        drawContext.matrix4fTemp0.m03 *= (float) drawContext.getTotalTime();
        drawContext.matrix4fTemp0.m13 *= (float) drawContext.getTotalTime();
        drawContext.matrix4fTemp0.m23 *= (float) drawContext.getTotalTime();
        drawContext.getGl().glMultMatrixf(drawContext.matrix4fTemp0.mo13998b(drawContext.tempBuffer0));
    }
}
