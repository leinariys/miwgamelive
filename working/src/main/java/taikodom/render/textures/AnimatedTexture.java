package taikodom.render.textures;

import org.mozilla.javascript.ScriptRuntime;
import taikodom.render.DrawContext;

/* compiled from: a */
public class AnimatedTexture extends Texture {
    public float fps = 1.0f;
    public int horizontalFrames = 1;
    public boolean looped = true;
    public double startTime = -1.0d;
    public Texture texture;
    public int totalFrames = 1;
    public int verticalFrames = 1;

    public void addArea(float f) {
        if (this.texture != null) {
            this.texture.addArea(f);
        }
    }

    public Texture getTexture() {
        return this.texture;
    }

    public void setTexture(Texture texture2) {
        this.texture = texture2;
    }

    public boolean getIsLooped() {
        return this.looped;
    }

    public void setIsLooped(boolean z) {
        this.looped = z;
    }

    public float getFps() {
        return this.fps;
    }

    public void setFps(float f) {
        this.fps = f;
    }

    public int getHorizontalFrames() {
        return this.horizontalFrames;
    }

    public void setHorizontalFrames(int i) {
        this.horizontalFrames = i;
        this.totalFrames = this.verticalFrames * i;
    }

    public int getVerticalFrames() {
        return this.verticalFrames;
    }

    public void setVerticalFrames(int i) {
        this.verticalFrames = i;
        this.totalFrames = this.horizontalFrames * i;
    }

    public void bind(DrawContext drawContext) {
        if (this.texture != null) {
            this.texture.bind(drawContext);
        }
        applyMatrixToRender(drawContext);
    }

    public void bind(int i, DrawContext drawContext) {
        if (this.texture != null) {
            this.texture.bind(i, drawContext);
        }
        applyMatrixToRender(drawContext);
    }

    private void applyMatrixToRender(DrawContext drawContext) {
        if (this.startTime < ScriptRuntime.NaN) {
            this.startTime = drawContext.getTotalTime();
        }
        int totalTime = (int) (((drawContext.getTotalTime() - this.startTime) * ((double) this.fps)) % ((double) this.totalFrames));
        if (!this.looped && totalTime >= this.totalFrames) {
            totalTime = this.totalFrames - 1;
        }
        int i = totalTime % this.verticalFrames;
        int floor = (int) Math.floor((double) (totalTime / this.horizontalFrames));
        drawContext.matrix4fTemp0.ceM();
        drawContext.matrix4fTemp0.m00 = 1.0f / ((float) this.verticalFrames);
        drawContext.matrix4fTemp0.m11 = 1.0f / ((float) this.horizontalFrames);
        drawContext.matrix4fTemp0.m03 = ((float) i) * drawContext.matrix4fTemp0.m00;
        drawContext.matrix4fTemp0.m13 = ((float) ((this.horizontalFrames - floor) - 1)) * drawContext.matrix4fTemp0.m11;
        drawContext.getGl().glMultMatrixf(drawContext.matrix4fTemp0.mo13998b(drawContext.tempBuffer0));
    }
}
