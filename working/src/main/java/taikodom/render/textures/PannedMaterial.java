package taikodom.render.textures;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;

/* compiled from: a */
public class PannedMaterial extends Material {
    public final Matrix4fWrap panMatrix = new Matrix4fWrap();
    public Material material;

    public Material getMaterial() {
        return this.material;
    }

    public void setMaterial(Material material2) {
        this.material = material2;
    }

    public Vec3f getPanSpeed() {
        return new Vec3f(this.panMatrix.m03, this.panMatrix.m13, this.panMatrix.m23);
    }

    public void setPanSpeed(Vec3f vec3f) {
        this.panMatrix.m03 = vec3f.x;
        this.panMatrix.m13 = vec3f.y;
        this.panMatrix.m23 = vec3f.z;
    }

    public Vec3f getPanSpeed(Vec3f vec3f) {
        vec3f.x = this.panMatrix.m03;
        vec3f.y = this.panMatrix.m13;
        vec3f.z = this.panMatrix.m23;
        return vec3f;
    }
}
