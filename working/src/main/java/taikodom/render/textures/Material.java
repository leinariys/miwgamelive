package taikodom.render.textures;

import com.hoplon.geometry.Color;
import game.geometry.Matrix4fWrap;
import game.geometry.Vector2fWrap;
import taikodom.render.DrawContext;
import taikodom.render.MaterialDrawingStates;
import taikodom.render.loader.Property;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.shaders.ShaderProgram;
import taikodom.render.shaders.parameters.ShaderParameter;

import javax.media.opengl.GL;
import java.util.ArrayList;
import java.util.List;

/* compiled from: a */
public class Material extends RenderAsset {
    public static final int DIFFUSE_CHANNEL = 8;
    public static final int DIFFUSE_DETAIL_CHANNEL = 9;
    public static final int FRAMEBUFFER_CHANNEL = 14;
    public static final int LIGHTMAP_CHANNEL = 7;
    public static final int MAX_MATERIAL_TEX = 16;
    public static final int NORMAL_CHANNEL = 12;
    public static final int SELFILUM_CHANNEL = 10;
    public final Color specular;
    public final List<BaseTexture> textures;
    public final Matrix4fWrap transform;
    public ShaderParameter[][] internalShaderParameters;
    public List<ShaderParameter> shaderParameters;
    public boolean shaderParametersUpdated;
    public int shaderPassCount;
    public float shininess;
    private Shader shader;
    private boolean useFramebufferRGB;

    public Material() {
        this.textures = new ArrayList();
        this.shininess = 32.0f;
        this.specular = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.transform = new Matrix4fWrap();
        this.shaderParameters = new ArrayList();
        for (int i = 0; i < 16; i++) {
            this.textures.add((BaseTexture) null);
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Material(Material material) {
        super(material);
        this.textures = new ArrayList();
        this.shininess = 32.0f;
        this.specular = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.transform = new Matrix4fWrap();
        this.shaderParameters = new ArrayList();
        this.shader = null;
        this.shaderPassCount = 0;
        this.shaderParametersUpdated = false;
        this.internalShaderParameters = null;
        for (int i = 0; i < material.textures.size(); i++) {
            BaseTexture baseTexture = material.textures.get(i);
            if (baseTexture != null) {
                this.textures.add((BaseTexture) baseTexture.cloneAsset());
            } else {
                this.textures.add((BaseTexture) null);
            }
        }
        this.shininess = material.shininess;
        this.specular.set(material.specular);
        if (material.shader != null) {
            setShader((Shader) material.shader.cloneAsset());
        }
        this.transform.set(material.transform);
        this.useFramebufferRGB = material.useFramebufferRGB;
        for (int i2 = 0; i2 < material.shaderParameters.size(); i2++) {
            ShaderParameter shaderParameter = material.shaderParameters.get(i2);
            if (shaderParameter != null) {
                this.shaderParameters.add((ShaderParameter) shaderParameter.cloneAsset());
            } else {
                this.shaderParameters.add((ShaderParameter) null);
            }
        }
    }

    public float getShininess() {
        return this.shininess;
    }

    public void setShininess(float f) {
        this.shininess = f;
    }

    public Color getSpecular() {
        return this.specular;
    }

    public void setSpecular(Color color) {
        this.specular.set(color);
    }

    public void setTexture(int i, BaseTexture baseTexture) {
        if (i == -1) {
            this.textures.add(baseTexture);
            return;
        }
        while (this.textures.size() <= i) {
            this.textures.add((BaseTexture) null);
        }
        this.textures.set(i, baseTexture);
    }

    public void bind(DrawContext drawContext) {
        if (drawContext.debugDraw()) {
            drawContext.logBind(this);
        }
        if (!this.shaderParametersUpdated && this.shader != null) {
            this.shader.compile(drawContext);
            populateShaderParameters();
        }
        drawContext.incNumMaterialBinds();
        GL gl = drawContext.getGl();
        gl.glMaterialf(1028, 5633, this.shininess);
        MaterialDrawingStates materialDrawingStates = drawContext.materialState;
        if (drawContext.currentMaterial != null) {
            drawContext.currentMaterial.unbind(drawContext);
        }
        for (int i = 0; i < materialDrawingStates.getStates().size(); i++) {
            ChannelInfo texInfo = materialDrawingStates.getTexInfo(i);
            int i2 = texInfo.channelMapping;
            gl.glActiveTexture(33984 + i2);
            gl.glLoadIdentity();
            bindTexture(i2, texInfo.texChannel, drawContext, (BaseTexture) null);
        }
    }

    private void bindTexture(int i, int i2, DrawContext drawContext, BaseTexture baseTexture) {
        updateTextureMatrix(i, i2, drawContext);
        BaseTexture baseTexture2 = this.textures.get(i2);
        if (baseTexture2 != null) {
            baseTexture2.bind(drawContext);
        }
    }

    private void updateTextureMatrix(int i, int i2, DrawContext drawContext) {
        drawContext.getGl().glLoadMatrixf(this.transform.mo13998b(drawContext.floatBuffer60Temp0));
        BaseTexture texture = getTexture(i2);
        if (texture != null) {
            drawContext.getGl().glMultMatrixf(texture.getTransform().mo13998b(drawContext.floatBuffer60Temp0));
        }
    }

    private void populateShaderParameters() {
        if (this.shader != null) {
            int passCount = this.shader.passCount();
            this.internalShaderParameters = new ShaderParameter[passCount][];
            this.shaderPassCount = passCount;
            for (int i = 0; i < passCount; i++) {
                ShaderProgram program = this.shader.getPass(i).getProgram();
                if (program == null) {
                    this.internalShaderParameters[i] = null;
                } else if (program.getManualParamsCount() > 0) {
                    this.internalShaderParameters[i] = new ShaderParameter[program.getManualParamsCount()];
                    for (int i2 = 0; i2 < program.getManualParamsCount(); i2++) {
                        this.internalShaderParameters[i][i2] = (ShaderParameter) program.getManualParameter(i2).cloneAsset();
                    }
                }
            }
            for (int i3 = 0; i3 < passCount; i3++) {
                ShaderProgram program2 = this.shader.getPass(i3).getProgram();
                if (program2 != null) {
                    for (int i4 = 0; i4 < program2.getManualParamsCount(); i4++) {
                        for (int i5 = 0; i5 < this.shaderParameters.size(); i5++) {
                            ShaderParameter shaderParameter = this.shaderParameters.get(i5);
                            ShaderParameter shaderParameter2 = this.internalShaderParameters[i3][i4];
                            if (shaderParameter.getParameterName().equals(shaderParameter2.getParameterName())) {
                                shaderParameter2.assignValue(shaderParameter);
                                shaderParameter.setLocationId(shaderParameter2.getLocationId());
                                this.internalShaderParameters[i3][i4] = shaderParameter;
                            }
                        }
                    }
                }
            }
            this.shaderParametersUpdated = true;
        }
    }

    public Shader getShader() {
        return this.shader;
    }

    public void setShader(Shader shader2) {
        this.shader = shader2;
    }

    public BaseTexture getTexture(int i) {
        return this.textures.get(i);
    }

    public RenderAsset cloneAsset() {
        return new Material(this);
    }

    public BaseTexture getNormalTexture() {
        return getTexture(12);
    }

    public void setNormalTexture(BaseTexture baseTexture) {
        setTexture(12, baseTexture);
    }

    public BaseTexture getSelfIluminationTexture() {
        return getTexture(10);
    }

    public void setSelfIluminationTexture(BaseTexture baseTexture) {
        setTexture(10, baseTexture);
    }

    public BaseTexture getDiffuseTexture() {
        return getTexture(8);
    }

    public void setDiffuseTexture(BaseTexture baseTexture) {
        setTexture(8, baseTexture);
    }

    @Property(name = "diffuseDetailTexture", persisted = false)
    public BaseTexture getDiffuseDetailTexture() {
        return getTexture(9);
    }

    @Property(name = "diffuseDetailTexture", persisted = false)
    public void setDiffuseDetailTexture(BaseTexture baseTexture) {
        setTexture(9, baseTexture);
    }

    public void setNormalTexture(int i, BaseTexture baseTexture) {
        setTexture(i + 12, baseTexture);
    }

    public void setDiffuseDetailTexture(int i, BaseTexture baseTexture) {
        setTexture(i + 9, baseTexture);
    }

    public void setSelfIluminationTexture(int i, BaseTexture baseTexture) {
        setTexture(i + 10, baseTexture);
    }

    public void setDiffuseTexture(int i, BaseTexture baseTexture) {
        setTexture(i + 8, baseTexture);
    }

    public void setShaderParameter(int i, ShaderParameter shaderParameter) {
        addShaderParameter(shaderParameter);
    }

    public void addShaderParameter(ShaderParameter shaderParameter) {
        this.shaderParameters.add(shaderParameter);
    }

    public Matrix4fWrap getTransform() {
        return this.transform;
    }

    public void setTransform(Matrix4fWrap ajk) {
        this.transform.set(ajk);
    }

    public void updateShaderParameters(int i, DrawContext drawContext) {
        if (this.shader != null && this.shader == drawContext.currentRecord().shader && this.internalShaderParameters != null && this.internalShaderParameters[i] != null) {
            if (i >= this.shaderPassCount) {
                this.shaderParametersUpdated = false;
                importShaderParametersFromShader();
            }
            ShaderPass pass = this.shader.getPass(i);
            if (pass != null) {
                for (int i2 = 0; i2 < pass.getProgram().getManualParamsCount(); i2++) {
                    this.internalShaderParameters[i][i2].bind(drawContext);
                }
            }
        }
    }

    public ShaderParameter getShaderParameter(int i) {
        return this.shaderParameters.get(i);
    }

    public void importShaderParametersFromShader() {
    }

    public void validate(SceneLoader sceneLoader) {
        super.validate(sceneLoader);
        if (getDiffuseTexture() == null) {
            setDiffuseTexture(sceneLoader.getDefaultDiffuseTexture());
        }
        if (getNormalTexture() == null) {
            setNormalTexture(sceneLoader.getDefaultNormalTexture());
        }
        if (getSelfIluminationTexture() == null) {
            setSelfIluminationTexture(sceneLoader.getDefaultSelfIluminationTexture());
        }
    }

    public int shaderParameterCount() {
        return this.shaderParameters.size();
    }

    public int textureCount() {
        return this.textures.size();
    }

    public Vector2fWrap getScaling() {
        Vector2fWrap aka = new Vector2fWrap();
        aka.x = (float) Math.sqrt((double) ((this.transform.m00 * this.transform.m00) + (this.transform.m10 * this.transform.m10) + (this.transform.m20 * this.transform.m20)));
        aka.y = (float) Math.sqrt((double) ((this.transform.m01 * this.transform.m01) + (this.transform.m11 * this.transform.m11) + (this.transform.m21 * this.transform.m21)));
        return aka;
    }

    public float getXScaling() {
        return (float) Math.sqrt((double) ((this.transform.m00 * this.transform.m00) + (this.transform.m10 * this.transform.m10) + (this.transform.m20 * this.transform.m20)));
    }

    public float getYScaling() {
        return (float) Math.sqrt((double) ((this.transform.m01 * this.transform.m01) + (this.transform.m11 * this.transform.m11) + (this.transform.m21 * this.transform.m21)));
    }

    public float getTransformedHeight() {
        return Math.abs(getDiffuseTexture().getTransformedHeight() * this.transform.m11);
    }

    public void releaseReferences() {
        super.releaseReferences();
        this.textures.clear();
        this.shader = null;
    }

    public void unbind(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        MaterialDrawingStates materialDrawingStates = drawContext.materialState;
        for (int i = 0; i < materialDrawingStates.getStates().size(); i++) {
            ChannelInfo texInfo = materialDrawingStates.getTexInfo(i);
            gl.glActiveTexture(texInfo.channelMapping + 33984);
            BaseTexture texture = getTexture(texInfo.texChannel);
            if (texture != null) {
                gl.glDisable(texture.getTarget());
            }
        }
    }

    public boolean isUseFramebufferRGB() {
        return this.useFramebufferRGB;
    }

    public void setUseFramebufferRGB(boolean z) {
        this.useFramebufferRGB = z;
    }
}
