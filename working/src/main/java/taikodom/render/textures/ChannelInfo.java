package taikodom.render.textures;

import game.geometry.Vector4fWrap;
import taikodom.render.enums.TexEnvMode;

/* compiled from: a */
public class ChannelInfo {
    public int channelMapping;
    public int texChannel;
    public int texCoord;
    public TexEnvMode texEnvMode = TexEnvMode.MODULATE;
    private Vector4fWrap qEquation = new Vector4fWrap(0.0f, 0.0f, 0.0f, 1.0f);
    private boolean qTexCoordGeneration = false;
    private Vector4fWrap rEquation = new Vector4fWrap(0.0f, 0.0f, 1.0f, 1.0f);
    private boolean rTexCoordGeneration = false;
    private Vector4fWrap sEquation = new Vector4fWrap(1.0f, 0.0f, 0.0f, 1.0f);
    private boolean sTexCoordGeneration = false;
    private Vector4fWrap tEquation = new Vector4fWrap(0.0f, 1.0f, 0.0f, 1.0f);
    private boolean tTexCoordGeneration = false;

    public boolean isAutoCoord() {
        return this.sTexCoordGeneration || this.tTexCoordGeneration || this.rTexCoordGeneration || this.qTexCoordGeneration;
    }

    public int getChannelMapping() {
        return this.channelMapping;
    }

    public void setChannelMapping(int i) {
        this.channelMapping = i;
    }

    @Deprecated
    public int getGlChannel() {
        return this.channelMapping;
    }

    @Deprecated
    public void setGlChannel(int i) {
        this.channelMapping = i;
    }

    public int getTexCoord() {
        return this.texCoord;
    }

    public void setTexCoord(int i) {
        this.texCoord = i;
    }

    @Deprecated
    public int getTexCoordSet() {
        return this.texCoord;
    }

    @Deprecated
    public void setTexCoordSet(int i) {
        this.texCoord = i;
    }

    public TexEnvMode getTexEnvMode() {
        return this.texEnvMode;
    }

    public void setTexEnvMode(TexEnvMode texEnvMode2) {
        this.texEnvMode = texEnvMode2;
    }

    @Deprecated
    public TexEnvMode getEnvMode() {
        return this.texEnvMode;
    }

    @Deprecated
    public void setEnvMode(TexEnvMode texEnvMode2) {
        this.texEnvMode = texEnvMode2;
    }

    public int getTexChannel() {
        return this.texChannel;
    }

    public void setTexChannel(int i) {
        this.texChannel = i;
    }

    public void assign(ChannelInfo channelInfo) {
        this.channelMapping = channelInfo.channelMapping;
        this.texCoord = channelInfo.texCoord;
        this.texEnvMode = channelInfo.texEnvMode;
        this.texChannel = channelInfo.texChannel;
        this.sTexCoordGeneration = channelInfo.sTexCoordGeneration;
        this.tTexCoordGeneration = channelInfo.tTexCoordGeneration;
        this.rTexCoordGeneration = channelInfo.rTexCoordGeneration;
        this.sEquation.set(channelInfo.sEquation);
        this.tEquation.set(channelInfo.tEquation);
        this.rEquation.set(channelInfo.rEquation);
        this.qEquation.set(channelInfo.rEquation);
    }

    public boolean isSTexCoordGeneration() {
        return this.sTexCoordGeneration;
    }

    public void setSTexCoordGeneration(boolean z) {
        this.sTexCoordGeneration = z;
    }

    public boolean isTTexCoordGeneration() {
        return this.tTexCoordGeneration;
    }

    public void setTTexCoordGeneration(boolean z) {
        this.tTexCoordGeneration = z;
    }

    public Vector4fWrap getSEquation() {
        return this.sEquation;
    }

    public void setSEquation(Vector4fWrap ajf) {
        this.sEquation.set(ajf);
    }

    public Vector4fWrap getTEquation() {
        return this.tEquation;
    }

    public void setTEquation(Vector4fWrap ajf) {
        this.tEquation.set(ajf);
    }

    public boolean isRTexCoordGeneration() {
        return this.rTexCoordGeneration;
    }

    public void setRTexCoordGeneration(boolean z) {
        this.rTexCoordGeneration = z;
    }

    public Vector4fWrap getREquation() {
        return this.rEquation;
    }

    public void setREquation(Vector4fWrap ajf) {
        this.rEquation.set(ajf);
    }

    public boolean isQTexCoordGeneration() {
        return this.qTexCoordGeneration;
    }

    public void setQTexCoordGeneration(boolean z) {
        this.qTexCoordGeneration = z;
    }

    public Vector4fWrap getQEquation() {
        return this.qEquation;
    }

    public void setQEquation(Vector4fWrap ajf) {
        this.qEquation.set(ajf);
    }
}
