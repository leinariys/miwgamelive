package taikodom.render.textures;

import game.geometry.Matrix4fWrap;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.provider.C2781ju;
import taikodom.render.loader.provider.C3176ol;

import javax.media.opengl.GL;
import java.util.ArrayList;
import java.util.List;

/* compiled from: a */
public class FlashTexture extends BaseTexture {
    private static float maxCurveError = 20.0f;
    private static int maxHeight = 1024;
    private static int maxWidth = 1024;
    private static float minCurveError = 0.05f;
    private static int minHeight = 64;
    private static int minWidth = 64;
    private float curveError = 0.1f;
    private List<C5194a> lodLevels = new ArrayList();
    private PBuffer pbuffer;
    private C3176ol player;
    private float quality = 0.5f;
    private float scaleX = 1.0f;
    private float scaleY = 1.0f;
    private SimpleTexture texture;

    FlashTexture() {
    }

    public FlashTexture(C3176ol olVar) {
        this.player = olVar;
        C2781ju SC = olVar.mo21035SC();
        this.width = Texture.roundToPowerOf2(SC.getWidth());
        this.height = Texture.roundToPowerOf2(SC.getHeight());
        this.scaleX = ((float) SC.getWidth()) / ((float) this.width);
        this.scaleY = ((float) SC.getHeight()) / ((float) this.height);
        setupLOD();
        C3176ol.m36838cA(this.curveError);
    }

    public static void setQualityLimits(int i, int i2, int i3, int i4, float f, float f2) {
        minCurveError = f;
        maxCurveError = f2;
        minWidth = i;
        minHeight = i2;
        maxWidth = i3;
        maxHeight = i4;
    }

    private void setupLOD() {
        if (minWidth != maxWidth) {
            int width = this.player.mo21035SC().getWidth();
            int height = this.player.mo21035SC().getHeight();
            this.lodLevels.add(new C5194a(width, height));
            int i = width * 2;
            int i2 = height * 2;
            int i3 = 0;
            while (true) {
                if ((width < height || i > maxWidth) && (width >= height || i2 > maxHeight)) {
                    int i4 = width / 2;
                    int i5 = height / 2;
                    int i6 = 0;
                } else {
                    this.lodLevels.add(new C5194a(i, i2));
                    i *= 2;
                    i2 *= 2;
                    i3++;
                }
            }
            /*
            int i42 = width / 2;
            int i52 = height / 2;
            int i62 = 0;
            while (true) {
                if ((width < height || i42 < minWidth) && (width >= height || i52 < minHeight)) {
                    this.quality = (((float) i62) + 1.0f) / ((((float) i62) + 1.0f) + ((float) i3));
                } else {
                    this.lodLevels.add(0, new C5194a(i42, i52));
                    i62++;
                    i42 /= 2;
                    i52 /= 2;
                }
            }
            this.quality = (((float) i62) + 1.0f) / ((((float) i62) + 1.0f) + ((float) i3));
            */
        }
    }

    public void releaseReferences() {
        this.pbuffer.destroy();
        this.texture.releaseReferences();
        this.player.destroy();
        this.player = null;
    }

    private void createPBuffer(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        if (this.pbuffer != null) {
            this.pbuffer.destroy();
            this.pbuffer = null;
            this.texture.releaseReferences();
        }
        this.pbuffer = new PBuffer();
        this.pbuffer.create(drawContext, this.width, this.height);
        this.texture = new SimpleTexture();
        this.texture.createEmptyRGBA(this.width, this.height, 32);
        setTransform(new Matrix4fWrap(this.scaleX, 0.0f, 0.0f, 0.0f, 0.0f, this.scaleY, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f - this.scaleY, 0.0f, 1.0f));
        this.pbuffer.activate(drawContext);
        gl.glClear(DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ);
        this.pbuffer.deactivate(drawContext);
        this.texture.bind(drawContext);
    }

    public void bind(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        drawContext.incNumTextureBind();
        if (this.pbuffer == null) {
            createPBuffer(drawContext);
        }
        if (this.player.mo21039SG()) {
            this.pbuffer.activate(drawContext);
            try {
                this.player.draw();
            } finally {
                this.texture.copyFromCurrentBuffer(gl);
                this.pbuffer.deactivate(drawContext);
            }
        } else if (this.player.isFinished()) {
            drawContext.postSceneEvent("endVideo", this);
        }
        this.texture.bind(drawContext);
    }

    public C3176ol getPlayer() {
        return this.player;
    }

    public float getQuality() {
        return this.quality;
    }

    public void setTarget(int i) {
        this.target = i;
    }

    public void setQuality(DrawContext drawContext, float f) {
        if (minWidth != maxWidth) {
            this.quality = Math.min(Math.max(0.0f, f), 1.0f);
            this.curveError = ((1.0f - this.quality) * (maxCurveError - minCurveError)) + minCurveError;
            int ceil = (int) Math.ceil((double) (this.quality * (((float) this.lodLevels.size()) - 1.0f)));
            int i = this.lodLevels.get(ceil).width;
            int i2 = this.lodLevels.get(ceil).height;
            if (this.player.mo21035SC().getWidth() != i) {
                this.player.setSize(i, i2);
                this.width = Texture.roundToPowerOf2(i);
                this.height = Texture.roundToPowerOf2(i2);
                createPBuffer(drawContext);
            }
        }
    }

    public void bind(int i, DrawContext drawContext) {
        drawContext.getGl().glActiveTexture(i);
        bind(i, drawContext);
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public float getScaleX() {
        return this.scaleX;
    }

    public float getScaleY() {
        return this.scaleY;
    }

    /* renamed from: taikodom.render.textures.FlashTexture$a */
    private class C5194a {
        public int height;
        public int width;

        public C5194a(int i, int i2) {
            this.width = i;
            this.height = i2;
        }
    }
}
