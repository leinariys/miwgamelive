package taikodom.render.textures;

import logic.render.bink.C0972OJ;
import logic.render.bink.C2427fN;

/* compiled from: a */
public class BinkDataSource extends TextureDataSource {
    long flags;
    private C2427fN bink;

    public BinkDataSource(C2427fN fNVar, long j) {
        setBink(fNVar);
        this.flags = j;
    }

    public C2427fN getBink() {
        if (this.bink == null) {
            long nanoTime = System.nanoTime();
            setBink(C0972OJ.m7939d(this.fileName, this.flags));
            System.out.println("Reload bink time:" + (System.nanoTime() - nanoTime));
        }
        return this.bink;
    }

    public void setBink(C2427fN fNVar) {
        if (fNVar != null) {
            this.bink = fNVar;
            this.width = (int) fNVar.mo18543sN();
            this.height = (int) fNVar.mo18544sO();
        }
    }

    public void flush() {
        C0972OJ.m7943e(this.bink);
        this.bink = null;
    }
}
