package taikodom.render.textures;

import com.hoplon.geometry.Color;
import game.geometry.Matrix4fWrap;
import org.lwjgl.BufferUtils;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;
import util.Maths;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

/* compiled from: a */
public class SimpleTexture extends BaseTexture {
    public int internalFormat = 6408;
    public int internalHeight;
    public int internalWidth;
    public int texID;
    public C5195a type = C5195a.RGBA;

    public void releaseReferences() {
        GL currentGL = GLU.getCurrentGL();
        if (currentGL.glIsTexture(this.texID)) {
            currentGL.glDeleteTextures(1, new int[]{this.texID}, 0);
            addTextureMemoryUsed(-(((this.width * this.height) * this.bpp) / 8));
            this.texID = 0;
            this.height = 0;
            this.width = 0;
        }
    }

    public void releaseReferences(GL gl) {
        if (gl.glIsTexture(this.texID)) {
            gl.glDeleteTextures(1, new int[]{this.texID}, 0);
            addTextureMemoryUsed(-(((this.width * this.height) * this.bpp) / 8));
            this.texID = 0;
            this.height = 0;
            this.width = 0;
        }
    }

    public void createRectangleEmpty(int i, int i2, int i3) {
        if (this.width != i || this.height != i2 || this.type != C5195a.RGBA) {
            releaseReferences();
            this.type = C5195a.RGBA;
            if (i != 0 && i2 != 0) {
                this.internalWidth = i;
                this.internalHeight = i2;
                GL currentGL = GLU.getCurrentGL();
                IntBuffer createIntBuffer = BufferUtils.createIntBuffer(1);
                this.target = 34037;
                currentGL.glEnable(this.target);
                currentGL.glGenTextures(1, createIntBuffer);
                this.texID = createIntBuffer.get(0);
                this.width = i;
                this.height = i2;
                addTextureMemoryUsed(((this.width * this.height) * i3) / 8);
                if (i3 == 32) {
                    this.internalFormat = 6408;
                } else if (i3 == 24) {
                    this.internalFormat = 6407;
                } else {
                    this.internalFormat = 6406;
                }
                this.bpp = i3;
                currentGL.glActiveTexture(33984);
                currentGL.glEnable(this.target);
                currentGL.glBindTexture(this.target, this.texID);
                currentGL.glTexImage2D(this.target, 0, 6408, this.width, this.height, 0, 6408, 5121, (Buffer) null);
                currentGL.glTexParameteri(this.target, 10241, 9729);
                currentGL.glTexParameteri(this.target, 10240, 9729);
                currentGL.glTexParameteri(this.target, 10242, 33071);
                currentGL.glTexParameteri(this.target, 10243, 33071);
                currentGL.glDisable(this.target);
                setTransform(new Matrix4fWrap((float) this.width, 0.0f, 0.0f, 0.0f, 0.0f, -((float) this.height), 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f));
            }
        }
    }

    public void createEmptyDepth(int i, int i2, int i3) {
        if (this.width != i || this.height != i2 || this.type != C5195a.DEPTH) {
            releaseReferences();
            this.type = C5195a.DEPTH;
            this.target = 3553;
            if (i != 0 && i2 != 0) {
                this.internalWidth = i;
                this.internalHeight = i2;
                GL currentGL = GLU.getCurrentGL();
                IntBuffer createIntBuffer = BufferUtils.createIntBuffer(1);
                currentGL.glEnable(this.target);
                currentGL.glGenTextures(1, createIntBuffer);
                this.texID = createIntBuffer.get(0);
                this.width = Maths.doubling(i);
                this.height = Maths.doubling(i2);
                this.bpp = i3;
                addTextureMemoryUsed(((this.width * this.height) * i3) / 8);
                currentGL.glActiveTexture(33984);
                currentGL.glEnable(this.target);
                currentGL.glBindTexture(this.target, this.texID);
                currentGL.glTexImage2D(3553, 0, 6402, this.width, this.height, 0, 6402, 5126, (Buffer) null);
                currentGL.glTexParameteri(this.target, 10241, 9729);
                currentGL.glTexParameteri(this.target, 10240, 9729);
                currentGL.glTexParameteri(this.target, 10242, 33071);
                currentGL.glTexParameteri(this.target, 10243, 33071);
                currentGL.glDisable(this.target);
                setTransform(new Matrix4fWrap());
            }
        }
    }

    public void createEmptyRGBA(int i, int i2, int i3) {
        if (this.width != i || this.height != i2 || this.type != C5195a.RGBA) {
            releaseReferences();
            this.type = C5195a.RGBA;
            this.target = 3553;
            if (i != 0 && i2 != 0) {
                this.internalWidth = i;
                this.internalHeight = i2;
                GL currentGL = GLU.getCurrentGL();
                IntBuffer createIntBuffer = BufferUtils.createIntBuffer(1);
                currentGL.glEnable(this.target);
                currentGL.glGenTextures(1, createIntBuffer);
                this.texID = createIntBuffer.get(0);
                this.width = Maths.doubling(i);
                this.height = Maths.doubling(i2);
                addTextureMemoryUsed(((this.width * this.height) * i3) / 8);
                if (i3 == 32) {
                    this.internalFormat = 6408;
                } else if (i3 == 24) {
                    this.internalFormat = 6407;
                } else {
                    this.internalFormat = 6406;
                }
                this.bpp = i3;
                currentGL.glActiveTexture(33984);
                currentGL.glEnable(this.target);
                currentGL.glBindTexture(this.target, this.texID);
                currentGL.glTexImage2D(this.target, 0, 6408, this.width, this.height, 0, 6408, 5121, (Buffer) null);
                currentGL.glTexParameteri(this.target, 10241, 9729);
                currentGL.glTexParameteri(this.target, 10240, 9729);
                currentGL.glTexParameteri(this.target, 10242, 33071);
                currentGL.glTexParameteri(this.target, 10243, 33071);
                currentGL.glDisable(this.target);
                setTransform(new Matrix4fWrap(((float) this.internalWidth) / ((float) this.width), 0.0f, 0.0f, 0.0f, 0.0f, -(((float) this.internalHeight) / ((float) this.height)), 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f));
            }
        }
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public void bind(DrawContext drawContext) {
        if (this.width != 0 && this.height != 0) {
            drawContext.incNumTextureBind();
            drawContext.getGl().glEnable(this.target);
            drawContext.getGl().glBindTexture(this.target, this.texID);
        }
    }

    public void bind(int i, DrawContext drawContext) {
        drawContext.getGl().glActiveTexture(i);
        bind(drawContext);
    }

    public void setSubData(int i, int i2, int i3, int i4, Buffer buffer) {
        if (i + i3 <= this.width && i2 + i4 <= this.height) {
            GL currentGL = GLU.getCurrentGL();
            currentGL.glEnable(this.target);
            currentGL.glBindTexture(this.target, this.texID);
            currentGL.glTexSubImage2D(this.target, 0, i, i2, i3, i4, this.internalFormat, 5121, buffer);
            currentGL.glDisable(this.target);
        }
    }

    public void copyFromCurrentBuffer(GL gl) {
        gl.glActiveTexture(33984);
        gl.glEnable(this.target);
        gl.glBindTexture(this.target, this.texID);
        gl.glCopyTexSubImage2D(this.target, 0, 0, 0, 0, 0, this.internalWidth, this.internalHeight);
        gl.glDisable(this.target);
    }

    public void copyFromCurrentBuffer(GL gl, int i, int i2, int i3, int i4) {
        gl.glActiveTexture(33984);
        gl.glEnable(this.target);
        gl.glBindTexture(this.target, this.texID);
        gl.glCopyTexSubImage2D(this.target, 0, i, i2, i3, i4, this.internalWidth, this.internalHeight);
        gl.glDisable(this.target);
    }

    public void copyFromCurrentBuffer(GL gl, int i, int i2, int i3, int i4, int i5, int i6) {
        gl.glActiveTexture(33984);
        gl.glEnable(this.target);
        gl.glBindTexture(this.target, this.texID);
        gl.glCopyTexSubImage2D(this.target, 0, i, i2, i3, i4, i5, i6);
        gl.glDisable(this.target);
    }

    public void fillWithColor(Color color) {
        ByteBuffer allocate = ByteBuffer.allocate(((this.width * this.height) * this.bpp) / 8);
        if (this.bpp == 32) {
            for (int i = 0; i < ((this.width * this.height) * this.bpp) / 8; i += 4) {
                allocate.put((byte) ((int) (color.x * 255.0f)));
                allocate.put((byte) ((int) (color.y * 255.0f)));
                allocate.put((byte) ((int) (color.z * 255.0f)));
                allocate.put((byte) ((int) (color.w * 255.0f)));
            }
        } else if (this.bpp == 24) {
            for (int i2 = 0; i2 < ((this.width * this.height) * this.bpp) / 8; i2 += 3) {
                allocate.put((byte) ((int) (color.x * 255.0f)));
                allocate.put((byte) ((int) (color.y * 255.0f)));
                allocate.put((byte) ((int) (color.z * 255.0f)));
            }
        } else {
            throw new RuntimeException("Dont know how to fill an texture with bpp=" + this.bpp);
        }
        allocate.position(0);
        setSubData(0, 0, this.width, this.height, allocate);
    }

    public int getTexId() {
        return this.texID;
    }

    /* renamed from: taikodom.render.textures.SimpleTexture$a */
    enum C5195a {
        RGBA,
        DEPTH
    }
}
