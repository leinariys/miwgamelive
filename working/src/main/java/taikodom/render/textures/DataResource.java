package taikodom.render.textures;

/* compiled from: a */
public class DataResource {
    int degradationLevel = 0;
    String fileName;
    boolean isReady = false;
    int lifeTime;

    public int getDegradationLevel() {
        return this.degradationLevel;
    }

    public void setDegradationLevel(int i) {
        this.degradationLevel = i;
    }

    public void incDegradationLevel() {
        this.degradationLevel++;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String str) {
        this.fileName = str;
    }

    public boolean isReady() {
        return this.isReady;
    }

    public void setReady(boolean z) {
        this.isReady = z;
    }

    public int getLifeTime() {
        return this.lifeTime;
    }

    public void setLifeTime(int i) {
        this.lifeTime = i;
    }

    public void incLifeTime(int i) {
        this.lifeTime += i;
    }
}
