package taikodom.render.textures;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/* compiled from: a */
public class DDSLoader {
    public static final int D3DFMT_A8R8G8B8 = 21;
    public static final int D3DFMT_DXT1 = 827611204;
    public static final int D3DFMT_DXT2 = 844388420;
    public static final int D3DFMT_DXT3 = 861165636;
    public static final int D3DFMT_DXT4 = 877942852;
    public static final int D3DFMT_DXT5 = 894720068;
    public static final int D3DFMT_R8G8B8 = 20;
    public static final int D3DFMT_UNKNOWN = 0;
    public static final int D3DFMT_X8R8G8B8 = 22;
    public static final int DDPF_ALPHA = 2;
    public static final int DDPF_ALPHAPIXELS = 1;
    public static final int DDPF_COMPRESSED = 128;
    public static final int DDPF_FOURCC = 4;
    public static final int DDPF_PALETTEINDEXED4 = 8;
    public static final int DDPF_PALETTEINDEXED8 = 32;
    public static final int DDPF_PALETTEINDEXEDTO8 = 16;
    public static final int DDPF_RGB = 64;
    public static final int DDPF_RGBTOYUV = 256;
    public static final int DDSCAPS2_CUBEMAP = 512;
    public static final int DDSCAPS2_CUBEMAP_NEGATIVEX = 2048;
    public static final int DDSCAPS2_CUBEMAP_NEGATIVEY = 8192;
    public static final int DDSCAPS2_CUBEMAP_NEGATIVEZ = 32768;
    public static final int DDSCAPS2_CUBEMAP_POSITIVEX = 1024;
    public static final int DDSCAPS2_CUBEMAP_POSITIVEY = 4096;
    public static final int DDSCAPS2_CUBEMAP_POSITIVEZ = 16384;
    public static final int DDSCAPS_COMPLEX = 8;
    public static final int DDSCAPS_MIPMAP = 4194304;
    public static final int DDSCAPS_TEXTURE = 4096;
    public static final int DDSD_ALPHABITDEPTH = 128;
    public static final int DDSD_BACKBUFFERCOUNT = 32;
    public static final int DDSD_CAPS = 1;
    public static final int DDSD_DEPTH = 8388608;
    public static final int DDSD_HEIGHT = 2;
    public static final int DDSD_LINEARSIZE = 524288;
    public static final int DDSD_LPSURFACE = 2048;
    public static final int DDSD_MIPMAPCOUNT = 131072;
    public static final int DDSD_PITCH = 8;
    public static final int DDSD_PIXELFORMAT = 4096;
    public static final int DDSD_WIDTH = 4;
    public static final int DDSD_ZBUFFERBITDEPTH = 64;
    boolean compressed;
    private DDSHeader header;

    public static String getCompressionFormatName(int i) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < 4; i2++) {
            stringBuffer.append((char) (i & 255));
            i >>= 8;
        }
        return stringBuffer.toString();
    }

    private static int computeCompressedBlockSize(int i, int i2, int i3, int i4) {
        int i5 = ((i + 3) / 4) * ((i2 + 3) / 4) * ((i3 + 3) / 4);
        switch (i4) {
            case D3DFMT_DXT1 /*827611204*/:
                return i5 * 8;
            default:
                return i5 * 16;
        }
    }

    private boolean isCompressed() {
        return isPixelFormatFlagSet(4);
    }

    public boolean isSurfaceDescFlagSet(int i) {
        return (this.header.flags & i) != 0;
    }

    private int getCompressionFormat() {
        return this.header.getPfFourCC();
    }

    public int getRealWidth() {
        return this.header.getWidth();
    }

    public int getRealHeight() {
        return this.header.getHeight();
    }

    private void fixupHeader() {
        if (isCompressed() && !isSurfaceDescFlagSet(DDSD_LINEARSIZE)) {
            int i = this.header.backBufferCountOrDepth;
            if (i == 0) {
                i = 1;
            }
            this.header.pitchOrLinearSize = computeCompressedBlockSize(getRealWidth(), getRealHeight(), i, getCompressionFormat());
            this.header.flags |= DDSD_LINEARSIZE;
        }
    }

    public void loadFromFile(InputStream inputStream) {
        this.header = new DDSHeader();
        try {
            this.header.read(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isPixelFormatFlagSet(int i) {
        return (this.header.pfFlags & i) != 0;
    }


    public int getPixelFormat() {
        if (isCompressed()) {
            return getCompressionFormat();
        }
        if (isPixelFormatFlagSet(64)) {
            if (isPixelFormatFlagSet(1)) {
                if ((getDepth() == 32) && (this.header.pfRBitMask == 16711680) &&
                        (this.header.pfGBitMask == 65280) &&
                        (this.header.pfBBitMask == 255) &&
                        (this.header.pfABitMask == -16777216)) {
                    return 21;
                }
            } else {
                if ((getDepth() == 24) && (this.header.pfRBitMask == 16711680) &&
                        (this.header.pfGBitMask == 65280) &&
                        (this.header.pfBBitMask == 255)) {
                    return 20;
                }
                if ((getDepth() == 32) &&
                        (this.header.pfRBitMask == 16711680) &&
                        (this.header.pfGBitMask == 65280) &&
                        (this.header.pfBBitMask == 255)) {
                    return 22;
                }
            }
        }
        return 0;
    }

    public boolean isCubemap() {
        return ((this.header.ddsCaps1 & 8) == 0 || (this.header.ddsCaps2 & 512) == 0) ? false : true;
    }

    public boolean isCubemapSidePresent(int i) {
        return isCubemap() && (this.header.ddsCaps2 & i) != 0;
    }

    public int getNumMipMaps() {
        if (!isSurfaceDescFlagSet(DDSD_MIPMAPCOUNT)) {
            return 0;
        }
        return this.header.mipMapCountOrAux;
    }

    public int mipMapWidth(int i) {
        int i2 = this.header.width;
        for (int i3 = 0; i3 < i; i3++) {
            i2 >>= 1;
        }
        return Math.max(i2, 1);
    }

    public int getBpp() {
        return this.header.pfSize;
    }

    public int getDepth() {
        return this.header.pfRGBBitCount;
    }

    public int mipMapHeight(int i) {
        int i2 = this.header.height;
        for (int i3 = 0; i3 < i; i3++) {
            i2 >>= 1;
        }
        return Math.max(i2, 1);
    }

    private int mipMapSizeInBytes(int i) {
        int mipMapWidth = mipMapWidth(i);
        int mipMapHeight = mipMapHeight(i);
        if (!isCompressed()) {
            return mipMapWidth * mipMapHeight * (getDepth() / 8);
        }
        return (getCompressionFormat() == 827611204 ? 8 : 16) * ((mipMapWidth + 3) / 4) * ((mipMapHeight + 3) / 4);
    }

    private int sideSizeInBytes() {
        int numMipMaps = getNumMipMaps();
        if (numMipMaps == 0) {
            numMipMaps = 1;
        }
        int i = 0;
        int i2 = 0;
        while (i < numMipMaps) {
            i++;
            i2 = mipMapSizeInBytes(i) + i2;
        }
        return i2;
    }

    private int sideShiftInBytes(int i) {
        int i2 = 0;
        int[] iArr = {1024, 2048, 4096, DDSCAPS2_CUBEMAP_NEGATIVEY, DDSCAPS2_CUBEMAP_POSITIVEZ, DDSCAPS2_CUBEMAP_NEGATIVEZ};
        int sideSizeInBytes = sideSizeInBytes();
        int i3 = 0;
        while (true) {
            int i4 = i2;
            if (i3 >= iArr.length) {
                throw new RuntimeException("Illegal side: " + i);
            } else if ((iArr[i3] & i) != 0) {
                return i4;
            } else {
                i2 = i4 + sideSizeInBytes;
                i3++;
            }
        }
    }

    public DDSInfo getMipMaps(int i, int i2, int i3, InputStream inputStream) {
        if (i2 == i3) {
            return getMipMap(i, i2, inputStream);
        }
        if (!isCubemap() && i != 0) {
            throw new RuntimeException("Illegal side for 2D texture: " + i);
        } else if (isCubemap() && !isCubemapSidePresent(i)) {
            throw new RuntimeException("Illegal side, side not present: " + i);
        } else if (getNumMipMaps() <= 0 || (i2 >= 0 && i2 < getNumMipMaps())) {
            int writtenSize = DDSHeader.writtenSize();
            if (isCubemap()) {
                writtenSize += sideShiftInBytes(i);
            }
            int i4 = 0;
            int i5 = writtenSize;
            while (i4 < i2) {
                i5 += mipMapSizeInBytes(i4);
                i4++;
            }
            long j = (long) i5;
            int i6 = i4;
            while (i6 <= i3) {
                i6++;
                i5 = mipMapSizeInBytes(i6) + i5;
            }
            long j2 = (long) i5;
            FileChannel channel = ((FileInputStream) inputStream).getChannel();
            ByteBuffer allocate = ByteBuffer.allocate((int) (j2 - j));
            try {
                channel.position(j);
                channel.read(allocate);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (i3 < i2) {
                return null;
            }
            ByteBuffer[] byteBufferArr = new ByteBuffer[(i3 - i2)];
            int i7 = 0;
            for (int i8 = 0; i8 < i3 - i2; i8++) {
                int mipMapSizeInBytes = mipMapSizeInBytes(i8 + i2);
                allocate.position(i7);
                allocate.limit(i7 + mipMapSizeInBytes);
                byteBufferArr[i8] = allocate.slice();
                byteBufferArr[i8].position(0);
                i7 += mipMapSizeInBytes;
            }
            DDSInfo dDSInfo = new DDSInfo(byteBufferArr, mipMapWidth(i2), mipMapHeight(i2), isCompressed(), getCompressionFormat());
            allocate.clear();
            return dDSInfo;
        } else {
            throw new RuntimeException("Illegal mipmap number " + i2 + " (0.." + (getNumMipMaps() - 1) + ")");
        }
    }

    public DDSInfo getMipMap(int i, int i2, InputStream inputStream) {
        if (!isCubemap() && i != 0) {
            throw new RuntimeException("Illegal side for 2D texture: " + i);
        } else if (isCubemap() && !isCubemapSidePresent(i)) {
            throw new RuntimeException("Illegal side, side not present: " + i);
        } else if (getNumMipMaps() <= 0 || (i2 >= 0 && i2 < getNumMipMaps())) {
            int writtenSize = DDSHeader.writtenSize();
            if (isCubemap()) {
                writtenSize += sideShiftInBytes(i);
            }
            int i3 = writtenSize;
            for (int i4 = 0; i4 < i2; i4++) {
                i3 += mipMapSizeInBytes(i4);
            }
            int mipMapSizeInBytes = mipMapSizeInBytes(i2);
            FileChannel channel = ((FileInputStream) inputStream).getChannel();
            ByteBuffer[] byteBufferArr = {ByteBuffer.allocateDirect(mipMapSizeInBytes)};
            try {
                channel.position((long) i3);
                channel.read(byteBufferArr);
            } catch (IOException e) {
                e.printStackTrace();
            }
            byteBufferArr[0].position(0);
            return new DDSInfo(byteBufferArr, mipMapWidth(i2), mipMapHeight(i2), isCompressed(), getCompressionFormat());
        } else {
            throw new RuntimeException("Illegal mipmap number " + i2 + " (0.." + (getNumMipMaps() - 1) + ")");
        }
    }

    public int getMipMapIDByResolution(int i) {
        if (i >= this.header.width) {
            return 0;
        }
        int i2 = this.header.width;
        int i3 = 0;
        while (i2 > i) {
            i2 >>= 1;
            i3++;
        }
        if (i2 > i) {
            i3--;
        }
        if (i3 >= getNumMipMaps()) {
            i3 = getNumMipMaps() - 1;
        }
        if (i3 < 0) {
            i3 = 0;
        }
        return i3;
    }
}
