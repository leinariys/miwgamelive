package taikodom.render.textures;

import com.sun.opengl.impl.Debug;
import com.sun.opengl.util.texture.TextureData;
import com.sun.opengl.util.texture.TextureIO;
import game.geometry.Vector2fWrap;
import gnu.trove.TIntObjectHashMap;
import gnu.trove.TIntObjectIterator;
import taikodom.render.DrawContext;
import taikodom.render.enums.TexMagFilter;
import taikodom.render.enums.TexMinFilter;
import taikodom.render.enums.TexWrap;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.provider.ImageLoader;
import taikodom.render.loader.provider.ResourceCleaner;

import javax.media.opengl.GL;

/* compiled from: a */
public class Texture extends BaseTexture {
    private static final boolean disableNPOT = Debug.isPropertyDefined("jogl.texture.nonpot");
    public TextureDataSource dataSource;
    public boolean firstBindTextureRequested = false;
    public boolean initialized = false;
    public int internalFormat = 6408;
    public boolean isDead = false;
    public TexMagFilter magFilter = TexMagFilter.NONE;
    public TexMinFilter minFilter = TexMinFilter.NONE;
    public boolean onlyUpdateNeeded = false;
    public boolean transformed = false;
    public boolean waitingForUpdate = false;
    public TexWrap wrapS = TexWrap.REPEAT;
    public TexWrap wrapT = TexWrap.REPEAT;
    Vector2fWrap ScreenSize;
    private boolean firstBind = true;
    private boolean hasMipmap = false;
    private long lastAdaptationTime;
    private long lastBindTime;
    private float persistencePriority;
    private com.sun.opengl.util.texture.Texture tex = null;
    private TIntObjectHashMap<TextureData> textureData = new TIntObjectHashMap<>(1);
    private boolean updatingData = false;

    public static boolean haveNPOT() {
        return !disableNPOT && GLSupportedCaps.isTextureNPOT();
    }

    public static boolean haveTexRect() {
        return TextureIO.isTexRectEnabled() && GLSupportedCaps.isTextureRectangle();
    }

    public static boolean isPowerOf2(int i) {
        return ((i + -1) & i) == 0;
    }

    public static int roundToPowerOf2(int i) {
        return (int) Math.pow(2.0d, Math.ceil(Math.log10((double) i) / Math.log10(2.0d)));
    }

    public void setTextureData(int i, TextureData textureData2) {
        if (textureData2 == null) {
            this.textureData.remove(i);
        } else {
            this.hasMipmap = textureData2.getMipmap();
            this.textureData.put(i, textureData2);
            if (i == 0) {
                this.width = textureData2.getWidth();
                this.height = textureData2.getHeight();
            }
        }
        if (this.textureData.size() > 1) {
        }
        if (this.textureData.size() == 1) {
            if ((!isPowerOf2(((TextureData) this.textureData.get(0)).getWidth()) || !isPowerOf2(((TextureData) this.textureData.get(0)).getHeight())) && !haveNPOT() && haveTexRect()) {
            }
        }
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public void bind(DrawContext drawContext) {
        this.lastBindTime = System.currentTimeMillis();
        if (this.persistencePriority < 65000.0f) {
            this.persistencePriority += drawContext.getElapsedTime();
        }
        GL gl = drawContext.getGl();
        drawContext.incNumTextureBind();
        if (!this.waitingForUpdate && (this.dataSource instanceof DDSDataSource)) {
            DDSDataSource dDSDataSource = (DDSDataSource) this.dataSource;
            if (this.dataSource.getDegradationLevel() != 0 && dDSDataSource.getMipMapResolution() < ImageLoader.getMaxMipMapSize() && getScreenArea() > ((float) dDSDataSource.getMipMapResolution())) {
                this.waitingForUpdate = ImageLoader.addTexture(this, (int) Math.ceil((double) getScreenArea()), false);
            }
        }
        if (this.tex == null) {
            this.updatingData = true;
            if (this.dataSource != null) {
                TextureData[] data = this.dataSource.getData();
                if (data.length > 1) {
                    this.tex = TextureIO.newTexture(34067);
                    if (this.dataSource instanceof DDSDataSource) {
                        this.width = ((DDSDataSource) this.dataSource).getRealWidth();
                        this.height = ((DDSDataSource) this.dataSource).getRealHeight();
                    } else {
                        this.width = this.dataSource.getWidth();
                        this.height = this.dataSource.getHeight();
                    }
                    for (int i = 0; i < data.length; i++) {
                        this.tex.updateImage(data[i], ImageLoader.cubeMapTargets[i]);
                    }
                } else {
                    this.tex = TextureIO.newTexture(data[0]);
                    if (this.dataSource instanceof DDSDataSource) {
                        this.width = ((DDSDataSource) this.dataSource).getRealWidth();
                        this.height = ((DDSDataSource) this.dataSource).getRealHeight();
                    } else {
                        this.width = this.dataSource.getWidth();
                        this.height = this.dataSource.getHeight();
                    }
                }
                this.dataSource.clearData();
            } else if (this.textureData.get(0) != null) {
                if (this.textureData.size() > 1) {
                    this.tex = TextureIO.newTexture(34067);
                    this.width = this.tex.getWidth();
                    this.height = this.tex.getWidth();
                    TIntObjectIterator it = this.textureData.iterator();
                    while (it.hasNext()) {
                        it.advance();
                        this.tex.updateImage((TextureData) it.value(), it.key());
                    }
                } else {
                    this.tex = TextureIO.newTexture((TextureData) this.textureData.get(0));
                    if (((TextureData) this.textureData.get(0)).getMipmap()) {
                        gl.glTexParameteri(getTarget(), 10241, 9987);
                    }
                }
                this.textureData = null;
            } else {
                return;
            }
            this.target = this.tex.getTarget();
            if (this.target != 34037) {
                gl.glTexParameteri(this.target, 10242, this.wrapS.glEquivalent());
                gl.glTexParameteri(this.target, 10243, this.wrapT.glEquivalent());
            }
            if (this.target == 34067) {
                gl.glTexParameteri(this.target, 10242, 33071);
                gl.glTexParameteri(this.target, 10243, 33071);
                gl.glTexParameteri(this.target, 32882, 33071);
            }
            if (this.minFilter != TexMinFilter.NONE) {
                gl.glTexParameteri(this.target, 10241, this.minFilter.glEquivalent());
            }
            if (this.magFilter != TexMagFilter.NONE) {
                gl.glTexParameteri(this.target, 10240, this.magFilter.glEquivalent());
            }
            this.updatingData = false;
            gl.glEnable(this.target);
            this.tex.bind();
            return;
        }
        if (!this.initialized) {
            ResourceCleaner.addTexture(this);
            addTextureMemoryUsed(this.tex.getEstimatedMemorySize());
            this.initialized = true;
        }
        if (this.onlyUpdateNeeded) {
            if (this.dataSource != null && !this.dataSource.isReady()) {
                ImageLoader.reAddTexture(this);
            }
            this.waitingForUpdate = false;
            this.onlyUpdateNeeded = false;
            this.updatingData = true;
            addTextureMemoryUsed(-this.tex.getEstimatedMemorySize());
            TextureData[] data2 = this.dataSource.getData();
            if (data2.length > 1) {
                for (int i2 = 0; i2 < data2.length; i2++) {
                    this.tex.updateImage(data2[i2], ImageLoader.cubeMapTargets[i2]);
                }
            } else {
                this.tex.updateImage(data2[0], 0);
                if ((this.dataSource instanceof DDSDataSource) && this.dataSource.getData(0).getMipmap()) {
                    this.minFilter = TexMinFilter.LINEAR_MIPMAP_LINEAR;
                }
            }
            if (this.target == 34037) {
                if (this.wrapS == TexWrap.REPEAT || this.wrapS == TexWrap.MIRRORED_REPEAT) {
                    this.wrapS = TexWrap.CLAMP_TO_EDGE;
                }
                if (this.wrapT == TexWrap.REPEAT || this.wrapT == TexWrap.MIRRORED_REPEAT) {
                    this.wrapT = TexWrap.CLAMP_TO_EDGE;
                }
            }
            if (this.minFilter != TexMinFilter.NONE) {
                gl.glTexParameteri(this.target, 10241, this.minFilter.glEquivalent());
            }
            if (this.magFilter != TexMagFilter.NONE) {
                gl.glTexParameteri(this.target, 10240, this.magFilter.glEquivalent());
            }
            if (this.target == 34067) {
                gl.glTexParameteri(this.target, 10242, 33071);
                gl.glTexParameteri(this.target, 10243, 33071);
                gl.glTexParameteri(this.target, 32882, 33071);
            } else {
                gl.glTexParameteri(this.target, 10242, this.wrapS.glEquivalent());
                gl.glTexParameteri(this.target, 10243, this.wrapT.glEquivalent());
            }
            if (this.minFilter != TexMinFilter.NONE) {
                gl.glTexParameteri(this.target, 10241, this.minFilter.glEquivalent());
            }
            if (this.magFilter != TexMagFilter.NONE) {
                gl.glTexParameteri(this.target, 10240, this.magFilter.glEquivalent());
            }
            this.dataSource.clearData();
            this.updatingData = false;
            addTextureMemoryUsed(this.tex.getEstimatedMemorySize());
        }
        gl.glEnable(this.target);
        this.tex.bind();
    }

    public void bind(int i, DrawContext drawContext) {
        drawContext.getGl().glActiveTexture(i);
        bind(drawContext);
    }

    public com.sun.opengl.util.texture.Texture getInternalTexture() {
        return this.tex;
    }

    public TextureData getTextureData(int i) {
        return (TextureData) this.textureData.get(i);
    }

    public TexWrap getWrapS() {
        return this.wrapS;
    }

    public void setWrapS(TexWrap texWrap) {
        this.wrapS = texWrap;
    }

    public TexWrap getWrapT() {
        return this.wrapT;
    }

    public void setWrapT(TexWrap texWrap) {
        this.wrapT = texWrap;
    }

    @Deprecated
    public TexWrap getWrap() {
        return this.wrapS;
    }

    @Deprecated
    public void setWrap(TexWrap texWrap) {
        setWrapS(texWrap);
        setWrapT(texWrap);
    }

    public TexMagFilter getMagFilter() {
        return this.magFilter;
    }

    public void setMagFilter(TexMagFilter texMagFilter) {
        this.magFilter = texMagFilter;
    }

    public TexMinFilter getMinFilter() {
        return this.minFilter;
    }

    public void setMinFilter(TexMinFilter texMinFilter) {
        this.minFilter = texMinFilter;
    }

    public int getTexId() {
        if (this.tex != null) {
            return this.tex.getTextureObject();
        }
        return 0;
    }

    public boolean isHasMipmap() {
        return this.hasMipmap;
    }

    public void releaseReferences() {
        if (this.tex != null) {
            addTextureMemoryUsed(-this.tex.getEstimatedMemorySize());
            this.tex.dispose();
            this.tex = null;
        }
    }

    public float getTransformedWidth() {
        return Math.abs(((float) this.width) * this.transform.m00);
    }

    public float getTransformedHeight() {
        return Math.abs(((float) this.height) * this.transform.m11);
    }

    public TextureDataSource getDataSource() {
        return this.dataSource;
    }

    public void setDataSource(TextureDataSource textureDataSource) {
        this.dataSource = textureDataSource;
    }

    public long getLastBindTime() {
        return this.lastBindTime;
    }

    public void setLastBindTime(long j) {
        this.lastBindTime = j;
    }

    public boolean isInitialized() {
        return this.initialized;
    }

    public void setInitialized(boolean z) {
        this.initialized = z;
    }

    public boolean isOnlyUpdateNeeded() {
        return this.onlyUpdateNeeded;
    }

    public void setOnlyUpdateNeeded(boolean z) {
        this.onlyUpdateNeeded = z;
    }

    public boolean isUpdatingData() {
        return this.updatingData;
    }

    public boolean isFirstBind() {
        return this.firstBind;
    }

    public void setFirstBind(boolean z) {
        this.firstBind = z;
    }

    public float getPersistencePriority() {
        return this.persistencePriority;
    }

    public void setPersistencePriority(float f) {
        this.persistencePriority = f;
    }

    public boolean isFirstBindTextureRequested() {
        return this.firstBindTextureRequested;
    }

    public void setFirstBindTextureRequested(boolean z) {
        this.firstBindTextureRequested = z;
    }

    public void destroyInternalTexture() {
        if (this.tex != null) {
            ImageLoader.destroyTexture(this);
        }
    }

    public long getLastDegradationTime() {
        return this.lastAdaptationTime;
    }

    public void setLastDegradationTime(long j) {
        this.lastAdaptationTime = j;
    }

    public void updateImage(TextureData textureData2, int i) {
        if (this.tex != null) {
            this.tex.updateImage(textureData2, i);
        }
    }

    public void updateImage(DrawContext drawContext, TextureData textureData2) {
        if (this.tex != null) {
            this.tex.bind();
            drawContext.getGl().glTexImage2D(this.tex.getTarget(), 0, 0, 8, 8, 0, 6408, 5121, textureData2.getBuffer());
        }
    }

    public int getMemorySize() {
        return this.tex.getEstimatedMemorySize();
    }

    public void setTarget(int i) {
        this.target = i;
    }

    public boolean isWaitingForUpdate() {
        return this.waitingForUpdate;
    }

    public void setWaitingForUpdate(boolean z) {
        this.waitingForUpdate = z;
    }
}
