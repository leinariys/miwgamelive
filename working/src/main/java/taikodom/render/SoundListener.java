package taikodom.render;

import taikodom.render.scene.SceneObject;

/* compiled from: a */
public abstract class SoundListener extends SceneObject {
    public float gain = 1.0f;

    SoundListener() {
    }

    /* access modifiers changed from: package-private */
    public float getGain() {
        return this.gain;
    }

    /* access modifiers changed from: package-private */
    public void setGain(float f) {
        this.gain = f;
    }

    public boolean isNeedToStep() {
        return true;
    }
}
