package com.hoplon.geometry;

import game.geometry.Vector4fWrap;

import javax.vecmath.*;
import java.nio.FloatBuffer;

/* compiled from: a */
public class Color extends Vector4fWrap {

    @Deprecated
    public Color() {
        super(0.0f, 0.0f, 0.0f, 0.0f);
    }

    public Color(float f, float f2, float f3, float f4) {
        super(f, f2, f3, f4);
    }

    public Color(float[] fArr) {
        super(fArr);
    }

    public Color(Tuple3f tuple3f) {
        super(tuple3f);
    }

    public Color(Tuple4d tuple4d) {
        super(tuple4d);
    }

    public Color(Tuple4f tuple4f) {
        super(tuple4f);
    }

    public Color(Vector4d vector4d) {
        super(vector4d);
    }

    public Color(Vector4f vector4f) {
        super(vector4f);
    }

    public static Color interpolate(Color color, Color color2, float f) {
        Color color3 = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        color3.x = (color2.x * f) + (color.x * (1.0f - f));
        color3.y = (color2.y * f) + (color.y * (1.0f - f));
        color3.z = (color2.z * f) + (color.z * (1.0f - f));
        color3.w = (color2.w * f) + (color.w * (1.0f - f));
        return color3;
    }

    public float getRed() {
        return this.x;
    }

    public void setRed(float f) {
        this.x = f;
    }

    public float getGreen() {
        return this.y;
    }

    public void setGreen(float f) {
        this.y = f;
    }

    public float getBlue() {
        return this.z;
    }

    public void setBlue(float f) {
        this.z = f;
    }

    public float getAlpha() {
        return this.w;
    }

    public void setAlpha(float f) {
        this.z = f;
    }

    public Color multiply(Color color) {
        if (color == null) {
            return this;
        }
        Color color2 = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        color2.x = this.x * color.x;
        color2.y = this.y * color.y;
        float f = color.z;
        this.z = f;
        color2.z = f;
        float f2 = color.w;
        this.w = f2;
        color2.w = f2;
        return color2;
    }

    public FloatBuffer fillFloatBuffer(FloatBuffer floatBuffer) {
        floatBuffer.clear();
        floatBuffer.put(this.x);
        floatBuffer.put(this.y);
        floatBuffer.put(this.z);
        floatBuffer.put(this.w);
        floatBuffer.flip();
        return floatBuffer;
    }
}
