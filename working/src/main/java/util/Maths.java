package util;

import org.mozilla.javascript.ScriptRuntime;

/* renamed from: a.aip  reason: case insensitive filesystem */
/* compiled from: a */
public class Maths {
    public static final double pi180 = 0.01745329238474369d;//пи / 180

    public static float clamp(float f, float f2, float f3) {
        if (f3 < f) {
            return f;
        }
        if (f3 > f2) {
            return f2;
        }
        return f3;
    }

    /* renamed from: c */
    public static long clamp(long j, long j2, long j3) {
        if (j3 < j) {
            return j;
        }
        if (j3 > j2) {
            return j2;
        }
        return j3;
    }

    public static int clamp(int i, int i2, int i3) {
        if (i3 < i) {
            return i;
        }
        if (i3 > i2) {
            return i2;
        }
        return i3;
    }

    public static float random2Min1() {
        return (float) ((Math.random() * 2.0d) - 1.0d);
    }

    public static float random() {
        return (float) Math.random();
    }

    /* renamed from: L */
    public static float m22666L(double d) {
        if (d >= ScriptRuntime.NaN) {
            return (float) ((long) d);
        }
        double d2 = (double) ((long) d);
        if (d2 != d) {
            return (float) (d2 - 1.0d);
        }
        return (float) d2;
    }

    /* renamed from: ju */
    public static float m22671ju(float f) {
        if (f >= 0.0f) {
            return (float) ((long) f);
        }
        float f2 = (float) ((long) f);
        if (f2 != f) {
            return f2 - 1.0f;
        }
        return f2;
    }

    public static double floor(double d) {
        if (d >= ScriptRuntime.NaN) {
            return (double) ((long) d);
        }
        double d2 = (double) ((long) d);
        if (d2 != d) {
            return d2 - 1.0d;
        }
        return d2;
    }

    /* renamed from: jv */
    public static int m22672jv(float f) {
        if (f >= 0.0f) {
            return (int) f;
        }
        float f2 = (float) ((long) f);
        if (f2 != f) {
            return (int) (f2 - 1.0f);
        }
        return (int) f2;
    }

    /* renamed from: s */
    public static float m22677s(float f, float f2) {
        return (float) Math.sqrt((double) ((f * f) + (f2 * f2)));
    }

    public static double ceil(double d) {
        if (d >= ScriptRuntime.NaN) {
            return (double) ((long) d);
        }
        double d2 = (double) ((long) d);
        if (d2 != d) {
            return d2 - 1.0d;
        }
        return d2;
    }

    /* renamed from: jw */
    public static double m22673jw(float f) {
        if (f <= 0.0f) {
            return (double) ((long) f);
        }
        float f2 = (float) ((long) f);
        if (f2 != f) {
            return (double) (f2 + 1.0f);
        }
        return (double) f2;
    }

    /* renamed from: jx */
    public static float m22674jx(float f) {
        if (f <= 0.0f) {
            return (float) ((long) f);
        }
        float f2 = (float) ((long) f);
        if (f2 != f) {
            return f2 + 1.0f;
        }
        return f2;
    }

    /* renamed from: M */
    public static double cosPi180(double d) {
        return Math.cos(pi180 * d);
    }

    /* renamed from: N */
    public static double sinPi180(double d) {
        return Math.sin(pi180 * d);
    }

    /* renamed from: O */
    public static double tanPi180(double d) {
        return Math.tan(pi180 * d);
    }

    /* renamed from: ra */
    public static int doubling(int i) {
        int i2 = 1;
        while (i2 < i) {
            i2 *= 2;
        }
        return i2;
    }

    /* renamed from: rb */
    public static int m22676rb(int i) {
        int i2 = 0;
        for (int i3 = 1; i > i3; i3 *= 2) {
            i2++;
        }
        return i2;
    }
}
