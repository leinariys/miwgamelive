package test;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/* renamed from: test.a.TY */
/* compiled from: test.a */
public class AwtSwingTest {
    /* access modifiers changed from: private */
    public static Frame emy;
    /* access modifiers changed from: private */
    public static JFrame emz;

    public static void main(String[] strArr) {
        emz = new C1339e(); // swing
        emz.enableInputMethods(true);
        emz.setDefaultCloseOperation(3);
        emz.setBounds(100, 100, 1050, 800);
        emz.setLayout(new BorderLayout());
        emy = new C1340f(); //awt
        emy.setBounds(10, 10, 400, 400);
        JButton jButton = new JButton("test");
        JPanel jPanel = new JPanel();
        emy.add(jPanel);
        jPanel.setLayout(new GridLayout(0, 1));
        jPanel.setBounds(0, 0, 400, 400);
        jPanel.add(jButton);
        new JTextField().setText("teste");
        emy.setFocusableWindowState(false);
        emy.setUndecorated(true);
        emy.setVisible(true);//добавил
        C1338d dVar = new C1338d();
        emz.addMouseListener(new C1337c());
        emz.addMouseMotionListener(new C1336b());
        emz.setLayout(new BorderLayout());
        emz.add(dVar);
        emz.setVisible(true);
        new Timer(10, new C1335a()).start();
    }

    /* access modifiers changed from: private */
    /* renamed from: test.a */
    public static Component m10027a(int i, int i2, Component component, boolean z) {
        Component component2;
        Component component3;
        if (!z || !(component instanceof JRootPane)) {
            component2 = component;
        } else {
            component2 = ((JRootPane) component).getContentPane();
        }
        if (!component2.contains(i, i2)) {
            component3 = null;
        } else {
            synchronized (component2.getTreeLock()) {
                if (component2 instanceof Container) {
                    Container container = (Container) component2;
                    int componentCount = container.getComponentCount();
                    int i3 = 0;
                    while (true) {
                        if (i3 < componentCount) {
                            Component component4 = container.getComponent(i3);
                            if (component4 != null && component4.isVisible() && component4.contains(i - component4.getX(), i2 - component4.getY())) {
                                component3 = component4;
                                break;
                            }
                            i3++;
                        } else {
                            break;
                        }
                    }
                }
                component3 = component2;
            }
        }
        if (component3 != null) {
            if ((component2 instanceof JTabbedPane) && component3 != component2) {
                component3 = ((JTabbedPane) component2).getSelectedComponent();
            }
            i -= component3.getX();
            i2 -= component3.getY();
        }
        if (component3 == component2 || component3 == null) {
            return component3;
        }
        return m10027a(i, i2, component3, z);
    }

    /* renamed from: test.a.TY$e */
    /* compiled from: test.a */
    static class C1339e extends JFrame {
        C1339e() {
        }

        /* access modifiers changed from: protected */
        public void processEvent(AWTEvent aWTEvent) {
            if (aWTEvent instanceof MouseEvent) {
                MouseEvent mouseEvent = (MouseEvent) aWTEvent;
                Component b = AwtSwingTest.m10027a(mouseEvent.getX() - AwtSwingTest.emz.getInsets().left, mouseEvent.getY() - AwtSwingTest.emz.getInsets().top, AwtSwingTest.emy, true);
                if (b != null) {
                    b.dispatchEvent(new MouseEvent(b, mouseEvent.getID(), mouseEvent.getWhen(), mouseEvent.getModifiersEx() | mouseEvent.getModifiers(), mouseEvent.getX() - AwtSwingTest.emz.getInsets().left, mouseEvent.getY() - AwtSwingTest.emz.getInsets().top, mouseEvent.getXOnScreen(), mouseEvent.getYOnScreen(), mouseEvent.getClickCount(), mouseEvent.isPopupTrigger(), mouseEvent.getButton()));
                } else {
                    return;
                }
            }
            boolean z = aWTEvent instanceof KeyEvent;
        }
    }

    /* renamed from: test.a.TY$f */
    /* compiled from: test.a */
    static class C1340f extends Frame {


        C1340f() {
        }

        public boolean isShowing() {
            return true;
        }

        public boolean isVisible() {
            if (!isFocusableWindow() || !new Throwable().getStackTrace()[1].getMethodName().startsWith("requestFocus")) {
                return true;
            }
            return false;
        }

        public boolean isFocused() {
            return true;
        }
    }

    /* renamed from: test.a.TY$d */
    /* compiled from: test.a */
    static class C1338d extends JPanel {
        C1338d() {
        }

        public void paint(Graphics graphics) {
            graphics.translate(AwtSwingTest.emy.getComponent(0).getX(), AwtSwingTest.emy.getComponent(0).getY());
            AwtSwingTest.emy.getComponent(0).paint(graphics);
        }
    }

    /* renamed from: test.a.TY$c */
    /* compiled from: test.a */
    static class C1337c extends MouseAdapter {
        C1337c() {
        }
    }

    /* renamed from: test.a.TY$b */
    /* compiled from: test.a */
    static class C1336b extends MouseMotionAdapter {
        C1336b() {
        }
    }

    /* renamed from: test.a.TY$test.a */
    static class C1335a implements ActionListener {
        C1335a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            AwtSwingTest.emz.repaint();
        }
    }
}
