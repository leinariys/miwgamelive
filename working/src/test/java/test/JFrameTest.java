package test;

import javax.swing.*;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;

/* renamed from: a.mZ */
/* compiled from: a */
public class JFrameTest extends JFrame {
    public JFrameTest() {
        setVisible(true);
        add(new C2981a());
        setBounds(100, 100, 400, 400);
    }

    public static void main(String[] strArr) {
        new JFrameTest();
    }

    /* renamed from: a.mZ$a */
    class C2981a extends JPanel {
        C2981a() {
        }

        public void paint(Graphics graphics) {
            Graphics2D graphics2D = (Graphics2D) graphics;
            graphics2D.setColor(Color.red);
            graphics2D.draw(new Rectangle(10, 10, 200, 200));
            Font font = getFont();
            graphics2D.setFont(font);
            graphics2D.drawString("exp", 0, 50);
            FontRenderContext fontRenderContext = graphics2D.getFontRenderContext();
            graphics2D.getFontMetrics();
            GlyphVector createGlyphVector = font.createGlyphVector(fontRenderContext, "teste".toCharArray());
            graphics2D.drawGlyphVector(createGlyphVector, 20.0f, 20.0f);
            graphics2D.draw(new Rectangle(10, 10, 200, 200));
            graphics2D.translate(100, 100);
            graphics2D.scale(10.0d, 10.0d);
            graphics2D.setStroke(new BasicStroke(0.2f));
            graphics2D.draw(createGlyphVector.getOutline());
            graphics2D.translate(0, 10);
            graphics2D.fill(createGlyphVector.getOutline());
        }
    }
}
