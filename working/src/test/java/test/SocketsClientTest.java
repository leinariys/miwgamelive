package test;

import util.thread.PoolThread;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* renamed from: d */
/* compiled from: test.a */
public class SocketsClientTest {

    private static ExecutorService executorService = Executors.newSingleThreadExecutor();


    public static void main(String[] strArr) throws IOException, InterruptedException {
        if (strArr == null) {
            executorService.submit(new ServerService());
        } else {
            PoolThread.init(new ServerService(), "ServerService--Test");
            PoolThread.sleep(1000);
        }
        new ClientService();
    }

    /* renamed from: d$c */
    /* compiled from: test.a */
    public static class ClientService {
        public ClientService() throws IOException, InterruptedException {
            Socket socket = new Socket("localhost", 8989);
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();
            long currentTimeMillis = System.currentTimeMillis();
            long j = 0;
            while (true) {
                Thread.sleep(5000);
                int mas = 5;
                System.out.println("Kleint otpraavil " + mas);
                outputStream.write(mas);
                outputStream.flush();

                int read = inputStream.read();
                System.out.println("Kleint poluchil " + read);
                j++;
                long currentTimeMillis2 = System.currentTimeMillis();
                if (currentTimeMillis2 - currentTimeMillis > 1000) {
                    System.out.println("---> " + j);
                    currentTimeMillis = currentTimeMillis2;
                    j = 0;
                }
            }
        }
    }

    /* renamed from: d$test.a */
    static class ServerService implements Runnable {
        public void run() {
            try {
                Socket accept = new ServerSocket(8989, 100).accept();
                InputStream inputStream = accept.getInputStream();
                OutputStream outputStream = accept.getOutputStream();
                while (true) {
                    //Ожидание сообщения
                    int read = inputStream.read();
                    System.out.println("Server poluchil " + read);


                    //Отвечаем
                    int mas = 5000;
                    System.out.println("Server otpravil " + mas);
                    outputStream.write(5000);
                    outputStream.flush();

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
