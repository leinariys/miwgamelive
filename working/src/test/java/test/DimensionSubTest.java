package test;

import java.awt.*;

/* renamed from: a.CG */
/* compiled from: a */
public class DimensionSubTest {
    /* renamed from: a */
    public static Component m1512a(Component component, String str) {
        String name = component.getName();
        if (name != null && name.equals(str)) {
            return component;
        }
        if (component instanceof Container) {
            return m1513b((Container) component, str);
        }
        return null;
    }

    /* renamed from: b */
    private static Component m1513b(Container container, String str) {
        Component b;
        int componentCount = container.getComponentCount();
        for (int i = 0; i < componentCount; i++) {
            Component component = container.getComponent(i);
            String name = component.getName();
            if (name != null && name.equals(str)) {
                return component;
            }
        }
        for (int i2 = 0; i2 < componentCount; i2++) {
            Container component2 = (Container) container.getComponent(i2);
            if ((component2 instanceof Container) && (b = m1513b(component2, str)) != null) {
                return b;
            }
        }
        return null;
    }
}
