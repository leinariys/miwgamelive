package test;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.Channel;
import java.nio.channels.FileChannel;

public class FileChannelTest {

    private static int metricWriteByte;
    private static int f5761ph;
    private static int f5760pg;
    private static ByteBuffer bufWrite;
    private static boolean f5759pf;

    public static void main(String[] args) throws IOException {
        String nioData = "working/src/test/resources/test/nio-data.txt";
        File df = new File(nioData);
        df.exists();
        RandomAccessFile aFile = new RandomAccessFile(nioData, "rw");
        FileChannel inChannel = aFile.getChannel();

        ByteBuffer buf = ByteBuffer.allocate(10);


        int bytesRead = inChannel.read(buf);
        while (bytesRead != -1) {

            System.out.println("Read " + bytesRead);
            buf.flip();

            while (buf.hasRemaining()) {
                System.out.print((char) buf.get());
            }

            buf.clear();
            bytesRead = inChannel.read(buf);
        }

        bufWrite = ByteBuffer.allocate(48);

        bufWrite.put(buf);

        inChannel.write(bufWrite);

        mo7817a(inChannel);

        aFile.close();
    }


    public static boolean mo7817a(Channel channel) throws IOException {
        int write = ((ByteChannel) channel).write(bufWrite);
        if (write > 0) {
            metricWriteByte += write;
            f5761ph++;
        } else {
            f5760pg++;
        }
        if (!bufWrite.hasRemaining()) {
            bufWrite.clear();
            f5759pf = false;
        }
        if (write > 0) {
            return true;
        }
        return false;
    }

}
