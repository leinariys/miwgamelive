package test;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: test.a */
public class DeletingLogTest {
    public static void main(String[] strArr) {
        System.out.println("Transaction logs:");
        System.out.println("--------------------------------------");
        new File("data").list(new C0001a());
        System.out.println();
        System.out.println("App logs:");
        System.out.println("--------------------------------------");
        new File("log").list(new C0002b());
        System.out.println();
        System.out.println("Chat logs:");
        System.out.println("--------------------------------------");
        new File("log").list(new C0003c());
    }

    /* renamed from: test.a$test.a */
    static class C0001a implements FilenameFilter {
        C0001a() {
        }

        public boolean accept(File file, String str) {
            if (!str.endsWith(".log")) {
                return false;
            }
            File file2 = new File(file, str);
            System.out.println("deleting " + file2);
            file2.delete();
            return false;
        }
    }

    /* renamed from: test.a$b */
    static class C0002b implements FilenameFilter {
        C0002b() {
        }

        public boolean accept(File file, String str) {
            if (!str.endsWith(".log.csv")) {
                return false;
            }
            File file2 = new File(file, str);
            System.out.println("deleting " + file2);
            file2.delete();
            return false;
        }
    }

    /* renamed from: test.a$c */
    static class C0003c implements FilenameFilter {
        C0003c() {
        }

        public boolean accept(File file, String str) {
            if (!str.startsWith("chat") || !str.endsWith(".log")) {
                return false;
            }
            File file2 = new File(file, str);
            System.out.println("deleting " + file2);
            file2.delete();
            return false;
        }
    }
}
