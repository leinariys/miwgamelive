package test;

import jdbm.RecordManager;
import jdbm.RecordManagerFactory;
import jdbm.helper.FastIterator;
import jdbm.htree.HTree;

import java.io.IOException;
import java.util.Properties;

public class FruitBasket {
    RecordManager recman;
    HTree hashtable;
    FastIterator iter;
    String fruit;
    String color;


    public FruitBasket()
        throws IOException {
        // create or open fruits record manager
        Properties props = new Properties();
        recman = RecordManagerFactory.createRecordManager("working/src/test/resources/test/FruitBasket/environment", props);

        // create or load fruit basket (hashtable of fruits)
        long recid = recman.getNamedObject("Корзина");
        if (recid != 0) {
            System.out.println("Reloading existing fruit basket...");
            hashtable = HTree.load(recman, recid);
            System.out.print("Корзина с фруктами содержит: ");
            showBasket();
        } else {
            System.out.println("Creating new fruit basket...");
            hashtable = HTree.createInstance(recman);
            recman.setNamedObject("Корзина", hashtable.getRecid());
        }
    }

    public static void main(String[] args) {
        try {
            FruitBasket basket = new FruitBasket();
            basket.runDemo();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void runDemo()
        throws IOException {
        // insert keys and values
        System.out.println();
        System.out.println("Adding fruits to the basket...");
        hashtable.put("Бананы", "yellow");
        hashtable.put("Флаг", "red");
        hashtable.put("Кружка", "green");

        System.out.print("Корзина с фруктами содержит: ");
        showBasket();


        // display color of a specific fruit
        System.out.println();
        System.out.println("Получить цвет бананов...");
        String bananasColor = (String) hashtable.get("Бананы");
        System.out.println("Бананы are " + bananasColor);

        recman.commit();

        try {
            // Thread.sleep( 10 * 1000 );
        } catch (Exception except) {
            // ignore
        }

        // remove a specific fruit from hashtable
        System.out.println();
        System.out.print("Извлечение бананов из корзины...");
        hashtable.remove("Бананы");
        recman.commit();
        System.out.println("Сделано.");

        // iterate over remaining objects
        System.out.println();
        System.out.println("Остальные цвета фруктов:");
        showBasket();

        // cleanup
        recman.close();
    }

    public void showBasket()
        throws IOException {
        // Display content of fruit basket
        System.out.println();
        iter = hashtable.keys();
        fruit = (String) iter.next();
        while (fruit != null) {
            color = (String) hashtable.get(fruit);
            System.out.println(fruit + " are " + color);
            fruit = (String) iter.next();
        }
        System.out.println();
    }

}
