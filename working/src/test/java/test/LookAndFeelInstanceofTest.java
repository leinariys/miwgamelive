package test;

import logic.swing.BasicLookAndFeelSpaceEngine;
import logic.swing.IComponentUi;
import org.junit.jupiter.api.Assertions;

import javax.swing.*;

/* renamed from: a.ajt  reason: case insensitive filesystem */
/* compiled from: a */
public class LookAndFeelInstanceofTest {
    //  -XDignore.symbol.file

    public static void main(String[] args) throws UnsupportedLookAndFeelException {
        cek();
    }


    //  @Test
    public static void cek() throws UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel(new BasicLookAndFeelSpaceEngine());

        m23100an(new JButton().getUI());
        m23100an(new JLabel().getUI());
        m23100an(new JPanel().getUI());
        m23100an(new JTextArea().getUI());
        m23100an(new JTextField().getUI());
        m23100an(new JProgressBar().getUI());
    }

    /* renamed from: an */

    /**
     * Проверяем что  унаследованно IComponentUi
     *
     * @param obj
     */
    private static void m23100an(Object obj) {
        if (!(obj instanceof IComponentUi)) {
            Assertions.fail(obj.getClass() + " does not implements TaikodomUI");
        }
    }
}
