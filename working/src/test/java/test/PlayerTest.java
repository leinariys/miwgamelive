package test;

import logic.res.FileControl;
import logic.res.code.C6145ahN;
import org.mozilla.javascript.*;
import util.C1646YF;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* renamed from: test.a.dK */
/* compiled from: test.a */
public class PlayerTest {
    /* access modifiers changed from: private */

    /* renamed from: Af */
    private final String playerName;
    /* renamed from: Aa */
    public Scriptable f6460Aa;
    /* renamed from: zZ */
    public Context f6467zZ;
    /* renamed from: Ab */
    private long f6461Ab;
    /* renamed from: Ac */
    private Reader jsReader;
    /* renamed from: Ad */
    private C2245a f6463Ad;
    /* renamed from: Ae */
    private Runnable f6464Ae;
    /* access modifiers changed from: private */
    /* renamed from: zY */
    private boolean f6466zY;

    public PlayerTest(String playerName) {
        this.playerName = playerName;
    }

    public static void main(String[] strArr) {
        try {
            FileControl avr = new FileControl("working/src/test/resources/test.js");
            long oldModif = 0;
            C6145ahN ahn = new C6145ahN(10.0f);
            ArrayList<PlayerTest> arrayList = new ArrayList<>();
            arrayList.add(new PlayerTest("player 1"));
            arrayList.add(new PlayerTest("player 2"));
            while (true) {
                long caT = ahn.caT();
                long lastModified = avr.lastModified();
                boolean isModif = oldModif != lastModified;
                for (PlayerTest dKVar : arrayList) {
                    if (isModif) {
                        dKVar.mo17736kf();
                        dKVar.mo17735a((Reader) new InputStreamReader(avr.openInputStream()));
                    }
                    dKVar.step(((float) caT) / 1000.0f);
                }
                oldModif = lastModified;
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    /* renamed from: test.a */
    public void mo17735a(Reader reader) {
        this.f6466zY = true;
        this.jsReader = reader;
    }

    /* renamed from: test.a */
    //обрабатывает исключения JavaScriptException, возникающие в момент выполнения JavaScript-кода.
    private void m28780a(JavaScriptException dxVar) {
        String obj = dxVar.getValue().toString();
        log("op: " + obj);
        if ("_x_cont".equals(obj)) {
            this.f6466zY = true;
            double doubleValue = ((Double) this.f6460Aa.get("_x_sleep", this.f6460Aa)).doubleValue();
            if (doubleValue != ScriptRuntime.NaN) {
                this.f6461Ab = (long) (doubleValue + ((double) System.currentTimeMillis()));
            } else {
                this.f6461Ab = 0;
            }
        } else if (obj.startsWith("_x_cont: ")) {
            String[] split = obj.split(":");
            if ("nextState".equals(split[1].trim())) {
                this.f6466zY = true;
                this.f6464Ae = this.f6463Ad.mo17739gO(split[2].trim());
                return;
            }
            log("Unknown command: " + obj);
        } else {
            dxVar.printStackTrace();
        }
    }

    /* renamed from: b */
    private void m28783b(Reader reader) throws IOException {
        this.f6467zZ = Context.enter();

        Context.call(new C1112c());
        this.f6467zZ.setOptimizationLevel(-2);
        this.f6460Aa = this.f6467zZ.initStandardObjects((ScriptableObject) null);
        this.f6460Aa.put("util", this.f6460Aa, (Object) new C1646YF());
        this.f6463Ad = new C2245a(this.playerName);
        this.f6460Aa.put("player", this.f6460Aa, (Object) this.f6463Ad);

        //final File f = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
        //System.out.println("getAbsolutePath = " + f.getAbsolutePath());
        //System.out.println("java.class.path = " + System.getProperty("java.class.path"));

        InputStream asStream = getClass().getResourceAsStream("bootstrap.js");
        InputStreamReader inputStreamReader = new InputStreamReader(asStream);
        this.f6467zZ.evaluateReader(this.f6460Aa, (Reader) inputStreamReader, "bootstrap.js", 0, (Object) null);
        inputStreamReader.close();
        try {
            this.f6467zZ.evaluateReader(this.f6460Aa, reader, "<game.script>", 0, (Object) null);
        } catch (JavaScriptException e) {
            m28780a(e);
        }
    }

    /* renamed from: kf */
    public void mo17736kf() {
        m28784kg();
        if (this.jsReader != null) {
            try {
                this.jsReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: test.a */
    private void m28781a(Runnable runnable) {
        log("continuing: " + runnable);
        m28784kg();
        try {
            runnable.run();
        } catch (JavaScriptException e) {
            m28780a(e);
        }
    }

    public void step(float f) {
        if (this.f6466zY) {
            try {
                if (this.f6464Ae != null) {
                    m28781a((Runnable) new C2246b(this.f6464Ae));
                }
                if (this.jsReader != null) {
                    this.f6466zY = false;
                    try {
                        m28783b(this.jsReader);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    this.jsReader = null;
                }
                if (this.f6461Ab != 0 && this.f6461Ab <= System.currentTimeMillis()) {
                    m28781a((Runnable) new C2247c());
                }
            } catch (RhinoException e3) {
                e3.printStackTrace();
                mo17736kf();
            }
        }
    }

    private void log(String str) {
        System.out.println(String.valueOf(this.f6463Ad.getName()) + ": " + str);
    }

    /* renamed from: kg */
    private void m28784kg() {
        this.f6461Ab = 0;
        this.f6466zY = false;
        this.f6464Ae = null;
        if (this.f6460Aa != null) {
            this.f6460Aa.put("_x_sleep", this.f6460Aa, (Object) 0);
            this.f6460Aa.put("_x_waitForSpawn", this.f6460Aa, (Object) false);
        }
    }

    /* renamed from: a.QL$c */
    /* compiled from: a */
    class C1112c implements ContextAction {
        public Object run(Context lhVar) {
            return true;
        }
    }

    /* renamed from: test.a.dK$test.a */
    public class C2245a {
        private final String name;
        private Map<String, Runnable> erj = new HashMap();

        public C2245a(String str) {
            this.name = str;
        }

        public String getName() {
            return this.name;
        }

        /* renamed from: test.a */
        public void mo17738a(String str, Runnable runnable) {
            this.erj.put(str, runnable);
        }

        /* renamed from: gO */
        public Runnable mo17739gO(String str) {
            return this.erj.get(str);
        }

        /* renamed from: gP */
        public void mo17740gP(String str) {
            throw new JavaScriptException("_x_cont: nextState:" + str, "<java>", 0);
        }
    }

    /* renamed from: test.a.dK$b */
    /* compiled from: test.a */
    class C2246b implements Runnable {
        private final /* synthetic */ Runnable fOd;

        C2246b(Runnable runnable) {
            this.fOd = runnable;
        }

        public void run() {
            this.fOd.run();
        }

        public String toString() {
            return "gotoState";
        }
    }

    /* renamed from: test.a.dK$c */
    /* compiled from: test.a */
    class C2247c implements Runnable {
        C2247c() {
        }

        public void run() {
            PlayerTest.this.f6467zZ.evaluateString(PlayerTest.this.f6460Aa, "_x_cont()", "<game.script>", 0, (Object) null);
        }

        public String toString() {
            return "_x_cont()";
        }
    }
}
