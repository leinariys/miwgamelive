package test;

import logic.res.html.aKU;

import java.io.*;
import java.util.List;

/* renamed from: a.mn */
/* compiled from: a */
public class FilenameFilterClassTest {


    public static void main(String[] strArr) {

        String[] strArr1 = {"@C:\\Projects\\Java\\miwgamelive\\README.md"};

        List listFilesExtensionClass = aKU.dgK();
        for (String str : strArr1) {
            if (str.startsWith("@")) {
                try {
                    BufferedReader bufferedReader = new BufferedReader(new FileReader(str.substring(1)));
                    while (true) {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        } else if (readLine.length() > 0) {
                            m35838a((List<File>) listFilesExtensionClass, new File(readLine));
                        }
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            } else {
                m35838a((List<File>) listFilesExtensionClass, new File(str));
            }
        }
    }

    /* renamed from: a */
    private static void m35838a(List<File> list, File file) {
        if (!file.isDirectory()) {
            list.add(file);
        } else {
            list.addAll(m35837a(file, (FilenameFilter) new FilenameFilterClass()));
        }
    }

    /* renamed from: a */
    public static List<File> m35837a(File file, FilenameFilter filenameFilter) {
        List<File> dgK = aKU.dgK();
        File[] listFiles = file.listFiles(filenameFilter);
        File[] listFiles2 = file.listFiles(new FileFilterIsDirectory());
        for (File add : listFiles) {
            dgK.add(add);
        }
        for (File a : listFiles2) {
            dgK.addAll(m35837a(a, filenameFilter));
        }
        return dgK;
    }

    /* renamed from: a.mn$b */
    /* compiled from: a */
    static class FileFilterIsDirectory implements FileFilter {
        FileFilterIsDirectory() {
        }

        public boolean accept(File file) {
            return file.isDirectory();
        }
    }

    /* renamed from: a.mn$a */
    static class FilenameFilterClass implements FilenameFilter {
        FilenameFilterClass() {
        }

        public boolean accept(File file, String str) {
            return str.endsWith(".class");
        }
    }
}
