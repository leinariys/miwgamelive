package test;

import a.XG;
import jdbm.RecordManager;
import jdbm.RecordManagerFactory;
import jdbm.btree.BTree;
import jdbm.helper.*;
import jdbm.htree.HTree;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class RecordManagerFactoryTest {
    private static final String TEST = "test";
    private static final String REPOSITORY = "repository";
    private static final String REPOSITORY_TEMP = "repository-temp";
    private static final String REFERENCE_TREE = "reference-tree";
    private static final String REFERENCE_TREE_TEMP = "reference-tree-temp";
    private static final String REPOSITORY_LAST_ID = "repository.lastId";


    static RecordManager recordManager;

    public static void main(String[] args) {
        BTree bTree;

        try {
            String path = new File("working/src/test/resources/environment").getAbsolutePath();

            //Opening repository: .\data\environment
            recordManager = RecordManagerFactory.createRecordManager(path, new Properties());
/*
            long recid = recordManager.getNamedObject(REPOSITORY_LAST_ID);
            recid = recordManager.insert(new StringBuilder());
            recordManager.setNamedObject(REPOSITORY_LAST_ID, recid);
            recordManager.commit();

            if (recid != 0) {
                recid = recordManager.getNamedObject(REPOSITORY_LAST_ID);
                Object fetch = recordManager.fetch(recid);
                System.out.println(fetch);
            } else {

               recid = recordManager.insert(Long.valueOf(456));
               recordManager.setNamedObject(REPOSITORY_LAST_ID, recid);
               recordManager.commit();
            }
*/


            bTree = getBTree(TEST);
            showTree(bTree);
            bTree = getBTree(REPOSITORY);
            showTree(bTree);
            bTree = getBTree(REPOSITORY_TEMP);
            showTree(bTree);
            bTree = getBTree(REFERENCE_TREE);
            showTree(bTree);
            bTree = getBTree(REFERENCE_TREE_TEMP);
            showTree(bTree);
            Object fetch = getLong(REPOSITORY_LAST_ID);
            showObject(fetch);


            // bTree = BTree.load( recman, recid );


            //recman.commit();
        } catch (IOException e3) {
            throw new RuntimeException(e3);
        }
    }

    private static Object getLong(String folder) throws IOException {
        Object fetch = null;
        long recid = recordManager.getNamedObject(folder);
        if (recid != 0) {
            fetch = recordManager.fetch(recid);
            System.out.println("Directory: " + folder + "   recid: " + recid + " Дерево загружено из БД, fetch: " + fetch);
        }
        return fetch;
    }

    private static BTree getBTree(String folder) throws IOException {
        BTree bTree;
        long recid = recordManager.getNamedObject(folder);
        if (recid != 0) {
            bTree = BTree.load(recordManager, recid);
            System.out.println("Directory: " + folder + "   recid: " + recid + " Дерево загружено из БД, size: " + bTree.size());
        } else {
            // создайте новую структуру данных B+Tree и используйте StringComparator для упорядочения записей на основе имен людей.
            bTree = BTree.createInstance(recordManager, new LongComparator(), new XG(), new ByteArraySerializer(), 256);
            recordManager.setNamedObject(folder, bTree.getRecid());
            System.out.println("Directory: " + folder + "   recid: " + recid + " Создано новое дерево BTree");
        }
        return bTree;
    }

    private static HTree getHTree(String folder) throws IOException {
        HTree hTree;
        long recid = recordManager.getNamedObject(folder);
        System.out.println();
        if (recid != 0) {
            hTree = HTree.load(recordManager, recid);
            System.out.println("Дерево загружено из БД, getRecid: " + hTree.getRecid());
        } else {
            hTree = HTree.createInstance(recordManager);
            recordManager.setNamedObject(folder, hTree.getRecid());
            System.out.println("Создано новое дерево HTree");
        }
        return hTree;
    }

    public static void showObject(Object object) throws IOException {
        System.out.println("Class:'" + object.getClass() + "'   toString:'" + object + "'");
    }

    // Показать содержимое
    public static void showTree(BTree bTree) throws IOException {
        TupleBrowser tupleBrowser = bTree.browse();
        Tuple tuple = new Tuple();
        while (tupleBrowser.getNext(tuple)) {
            System.out.println("Class:'" + tuple.getClass() + "'   toString:'" + tuple.getKey() + " - " + tuple.getValue());
        }
    }

    public static void showTree(HTree hashtable) throws IOException {
        // Показать содержимое
        FastIterator iter = hashtable.keys();
        Object key = iter.next();
        while (key != null) {
            Object value = hashtable.get(key);
            System.out.println("Class key:'" + key.getClass() + " Class value:'" + value.getClass() + "'   key:'" + key + "' value:'" + value + "'");
            key = iter.next();
        }
    }
}
