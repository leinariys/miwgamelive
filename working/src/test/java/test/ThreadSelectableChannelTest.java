package test;


import game.network.channel.Connect;
import game.network.channel.NetworkChannel;
import game.network.channel.client.ClientConnect;
import game.network.channel.server.OpenSocketNioServer;
import game.network.message.*;
import lombok.extern.slf4j.Slf4j;

import java.io.EOFException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UTFDataFormatException;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

@Slf4j
public class ThreadSelectableChannelTest {

    static final String MESS_TEST_RU = "ТЕстовое сообщение 1";
    static final String MESS_TEST_EN = "Test Message 11";
    static ByteMessageCompres mess_RU_UTF_8 = new ByteMessageCompres(MESS_TEST_RU.getBytes(StandardCharsets.UTF_8));
    static ByteMessageCompres mess_EN_UTF_8 = new ByteMessageCompres(MESS_TEST_EN.getBytes(StandardCharsets.UTF_8));
    static ByteMessageCompres mess_RU_US_ASCII = new ByteMessageCompres(MESS_TEST_RU.getBytes(StandardCharsets.US_ASCII));
    static ByteMessageCompres mess_EN_US_ASCII = new ByteMessageCompres(MESS_TEST_EN.getBytes(StandardCharsets.US_ASCII));

    //0422   Т
//0000 0100 0010 0010
//-48 -94 -> 208 162
//1101 0000  1010 0010
    public static void main(String[] args) throws IOException {

        testChannelBuffer();
        //testMessage();
        //testStart();
    }

    private static void testChannelBuffer() throws IOException {
        //67108863;//67108864  = 2^26 = 8 Mbit
        ChannelBufferWriter bufferWriter = new ChannelWriterCompression(2048, 67108863);
        ChannelBufferWriter bufferWriterNot = new ChannelWriterNotCompression(2048, 67108863);
        ChannelBufferReader bufferReader = new ChannelReaderCompression(2048, 67108863);
        ChannelBufferReader bufferReaderNot = new ChannelReaderNotCompression(2048, 67108863);


        processByteMessageWriter(bufferWriter, mess_RU_UTF_8, "mess_RU_UTF_8");
        processByteMessageWriter(bufferWriterNot, mess_EN_UTF_8, "mess_EN_UTF_8");
        processByteMessageWriter(bufferWriter, mess_RU_US_ASCII, "mess_RU_US_ASCII");
        processByteMessageWriter(bufferWriterNot, mess_EN_US_ASCII, "mess_EN_US_ASCII");

        processByteMessageReader(bufferReader, mess_RU_UTF_8, "mess_RU_UTF_8");
        processByteMessageReader(bufferReaderNot, mess_EN_UTF_8, "mess_EN_UTF_8");
        processByteMessageReader(bufferReader, mess_RU_US_ASCII, "mess_RU_US_ASCII");
        processByteMessageReader(bufferReaderNot, mess_EN_US_ASCII, "mess_EN_US_ASCII");
    }


    private static void processByteMessageWriter(ChannelBufferWriter bufferWriter, ByteMessageCompres messRuUtf8, String mess_en_utf_8) throws IOException {
        log.info("------------------Writer {} ------------------ ", mess_en_utf_8);
        FileChannel inChannelBufferWriter = new RandomAccessFile("working/src/test/resources/test/nio-data-" + mess_en_utf_8 + ".txt", "rw").getChannel();
        bufferWriter.writeMessageToBuffer(messRuUtf8);
        //bufferWriter.prepareForReading();
        bufferWriter.writeMessageToBuffer(messRuUtf8);
        bufferWriter.prepareForReading();
        log.info("1 CountWriteMessage: {}", bufferWriter.getCountWriteMessage());
        log.info("1 MetricMessageFilled: {}", bufferWriter.getMetricMessageFilled());
        log.info("1 MetricWriteByte: {}", bufferWriter.getMetricWriteByte());
        log.info("1 AllOriginalByteLength: {}", bufferWriter.getAllOriginalByteLength());
        log.info("1 MetricМessagesEmpty: {}", bufferWriter.getMetricМessagesEmpty());
        log.info("1 BufferReadyToBeRead: {}", bufferWriter.isBufferReadyToBeRead());
        bufferWriter.writeBufferToChannel(inChannelBufferWriter);
        //bufferWriter.writeBufferToChannel(inChannelBufferWriter);
        log.info("2 CountWriteMessage: {}", bufferWriter.getCountWriteMessage());
        log.info("2 MetricMessageFilled: {}", bufferWriter.getMetricMessageFilled());
        log.info("2 MetricWriteByte: {}", bufferWriter.getMetricWriteByte());
        log.info("2 AllOriginalByteLength: {}", bufferWriter.getAllOriginalByteLength());
        log.info("2 MetricМessagesEmpty: {}", bufferWriter.getMetricМessagesEmpty());
        log.info("2 BufferReadyToBeRead: {}", bufferWriter.isBufferReadyToBeRead());
    }

    private static void processByteMessageReader(ChannelBufferReader bufferReader, ByteMessage mess_RU_UTF_8, String mess_ru_utf_8) throws IOException {
        log.info("------------------Reader {} ------------------", mess_ru_utf_8);
        FileChannel inChannelBufferReader = new RandomAccessFile("working/src/test/resources/test/nio-data-" + mess_ru_utf_8 + ".txt", "rw").getChannel();
        log.info("1 CountWriteMessage: {}", bufferReader.getCountReadMessage());
        log.info("1 MetricMessageFilled: {}", bufferReader.getByteLength());
        log.info("1 MetricWriteByte: {}", bufferReader.getMetricReadByte());
        log.info("1 AllOriginalByteLength: {}", bufferReader.isNotEmpty());
        bufferReader.readerChannelToBuffer(inChannelBufferReader);
        log.info("2 CountWriteMessage: {}", bufferReader.getCountReadMessage());
        log.info("2 MetricMessageFilled: {}", bufferReader.getByteLength());
        log.info("2 MetricWriteByte: {}", bufferReader.getMetricReadByte());
        log.info("2 AllOriginalByteLength: {}", bufferReader.isNotEmpty());
        ByteMessageCompres dfg = bufferReader.getMessage();
        log.info("2 ByteMessageCompres: {}", dfg);

    }


    private static void testMessage() throws IOException {
        processByteMessage(mess_RU_UTF_8, MESS_TEST_RU);
        processByteMessage(mess_EN_UTF_8, MESS_TEST_EN);
        processByteMessage(mess_RU_US_ASCII, MESS_TEST_RU);
        processByteMessage(mess_EN_US_ASCII, MESS_TEST_EN);
    }

    private static void processByteMessage(ByteMessage byteMessage, String messTest) throws UTFDataFormatException, EOFException {
        log.info("--------------{}--------------", messTest);
        log.info("getMessage: {}", Objects.equals(byteMessage.getMessage(), messTest.getBytes()));
        log.info("getMessageId: {}", byteMessage.getMessageId());
        log.info("getFromClientConnect: {}", byteMessage.getFromClientConnect());
        log.info("getLength: {} {}", byteMessage.getLength(), messTest.length());
        log.info("getInClientConnect: {}", byteMessage.getInClientConnect());
        log.info("getOffset: {}", byteMessage.getOffset());
        log.info("toString: \n{}", byteMessage.toString());

        ByteMessageReader messageReader = new ByteMessageReader(byteMessage.getMessage(), byteMessage.getOffset(), byteMessage.getLength());
        ByteMessageReader cloneByteMessageReader = messageReader.cloneByteMessageReader();
        log.info("equals clone: {}", Objects.equals(messageReader, cloneByteMessageReader));
        log.info("messageReader read: {}", messageReader.read());
        log.info("messageReader readBits: {}", messageReader.readBits(1));
        byte[] bytes = new byte[90];
        log.info("messageReader read: {}", messageReader.read(bytes, 1, 4));
        log.info("messageReader bytes: {}", bytes);
        messageReader.readFully(bytes);
        log.info("messageReader readFully: {}", bytes);
        messageReader.readFully(bytes, 1, 10);
        log.info("messageReader readFully,1,10: {}", bytes);
        log.info("messageReader skipBytes: {}", messageReader.skipBytes(2));
        log.info("messageReader readBoolean: {}", messageReader.readBoolean());
        log.info("messageReader readByte: {}", cloneByteMessageReader.readByte());
        log.info("messageReader readUnsignedByte: {}", cloneByteMessageReader.readUnsignedByte());
        log.info("messageReader readShort: {}", cloneByteMessageReader.readShort());
        log.info("messageReader readShort0: {}", cloneByteMessageReader.readShort0());
        log.info("messageReader readUnsignedShort: {}", cloneByteMessageReader.readUnsignedShort());
        log.info("messageReader readUnsignedShort0: {}", cloneByteMessageReader.readUnsignedShort0());
        log.info("messageReader readChar: {}", cloneByteMessageReader.readChar());
        log.info("messageReader readInt: {}", cloneByteMessageReader.readInt());
        log.info("messageReader readPositiveInt: {}", cloneByteMessageReader.readPositiveInt());
        log.info("messageReader readInt0: {}", cloneByteMessageReader.readInt0());
        log.info("messageReader read3bytes: {}", cloneByteMessageReader.read3bytes());

        cloneByteMessageReader = new ByteMessageReader(byteMessage.getMessage(), byteMessage.getOffset(), byteMessage.getLength());
        log.info("messageReader readLong: {}", cloneByteMessageReader.readLong());
        log.info("messageReader readLong0: {}", cloneByteMessageReader.readLong0());
        log.info("messageReader readFloat: {}", cloneByteMessageReader.readFloat());
        log.info("messageReader readDouble: {}", cloneByteMessageReader.readDouble());
        log.info("messageReader readLine: {}", cloneByteMessageReader.readLine());
        cloneByteMessageReader = new ByteMessageReader(byteMessage.getMessage(), byteMessage.getOffset(), byteMessage.getLength());
        //log.info("messageReader readUTF: {}", cloneByteMessageReader.readUTF());
    }


    private static void testStart() {
        int sdf = (int) (Math.random() * ((double) new Object().hashCode()) * ((double) Thread.currentThread().hashCode()));

        NetworkChannel serverSocket = new OpenSocketNioServer(15226, "1.0.4933.55");
        serverSocket.creatListenerChannel(new ConnectTest());
        try {
            serverSocket.buildListenerChannel(15225, (int[]) null, (int[]) null, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                //System.out.println(System.getProperty("nio.savemessages"));
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        /*
        ServerSocketChannel open = ServerSocketChannel.open();
        open.configureBlocking(false);
        open.socket().bind(new InetSocketAddress(15115));
        ConfigServer configServer = new ConfigServer(open, ElementConnect.Attitude.CHILD, this, (int[]) null, (int[]) null, false);
        this.threadSelectableChannel.addChannel((WraperSelectable) configServer);

*/
    }

    static class ConnectTest implements Connect {
        @Override
        public void chechUser(ClientConnect bVar, String userName, String pass, boolean isForcingNewUser) {
        }

        @Override
        public void userDisconnected(ClientConnect bVar) {
        }
    }
}
