package test;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.ParameterizedType;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
/* renamed from: c */
/* compiled from: test.a */
public class ClassTest {

    public static ExecutorService executorService = Executors.newSingleThreadExecutor();


    public static void main(String[] strArr) {
        Sclass cVar = new Sclass();
        System.out.println(cVar.getClass().getTypeParameters().length);
        System.out.println(Aclass.class.getTypeParameters()[0].getBounds()[0]);
        System.out.println(cVar.getClass().getSuperclass().getTypeParameters()[0].getBounds()[0]);
        System.out.println(cVar.getClass().getGenericSuperclass());
        System.out.println(((ParameterizedType) cVar.getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
        System.out.println(((ParameterizedType) cVar.getClass().getGenericSuperclass()).getActualTypeArguments()[0]);


        executorService.submit(new C1897b());


    }

    /* renamed from: c$b */
    /* compiled from: test.a */
    public interface C4139b {
        Class<? extends Number> getClllllass();
    }

    /* renamed from: c$test.a */
    public static abstract class Aclass<T extends Number> implements C4139b {
        private Class<T> dgw;
    }

    /* renamed from: c$c */
    /* compiled from: test.a */
    public static class Sclass extends Aclass<Long> {
        public Class<? extends Number> getClllllass() {
            return null;
        }
    }


    public static class T1 {
        public String getF1() {
            return "000";
        }
    }

    public static class T2 {
        public T1 f1 = null;

        public T1 getF1() {
            System.out.println("1111");
            if (f1 == null) {
                throw new NullPointerException();
            }
            return f1;
        }
    }

    public static class T3 {
        public T2 f2 = new T2();

        public T2 getF2() {
            return f2;
        }
    }


    static class C1897b implements Runnable {
        public void run() {
            try {
                System.out.println("----------");
                new T3().getF2().getF1().getF1();
                System.out.println(new T3().getF2().getF1().getF1());
            } catch (Throwable th) {
                log.warn("Exception while executing a task", th);
            }
        }
    }

}
