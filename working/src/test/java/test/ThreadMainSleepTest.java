package test;

/* renamed from: h */
/* compiled from: test.a */
public class ThreadMainSleepTest {
    public static void main(String[] strArr) throws InterruptedException {
        long nanoTime = System.nanoTime();


        for (int ticks = 1; ticks < 2000; ticks++) {
            Thread.sleep(1000);

            double seconds = (System.nanoTime() - nanoTime) * 1.0E-9d;

            System.out.printf("Elapsed: %f seconds %d ticks\n", seconds, ticks);
        }


    }
}
