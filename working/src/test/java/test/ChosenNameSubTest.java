package test;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/* renamed from: a.BD */
/* compiled from: a */
public class ChosenNameSubTest extends JDialog implements ActionListener {
    private static ChosenNameSubTest cnO;
    private static String value = "";
    private JList list;

    private ChosenNameSubTest(Frame frame, Component component, String str, String str2, Object[] objArr, String str3, String str4) {
        super(frame, str2, true);
        JButton jButton = new JButton("Cancel");
        jButton.addActionListener(this);
        JButton jButton2 = new JButton("Set");
        jButton2.setActionCommand("Set");
        jButton2.addActionListener(this);
        getRootPane().setDefaultButton(jButton2);
        this.list = new C0091a(objArr);
        this.list.setSelectionMode(1);
        if (str4 != null) {
            this.list.setPrototypeCellValue(str4);
        }
        this.list.setLayoutOrientation(2);
        this.list.setVisibleRowCount(-1);
        this.list.addMouseListener(new C0092b(jButton2));
        JScrollPane jScrollPane = new JScrollPane(this.list);
        jScrollPane.setPreferredSize(new Dimension(250, 80));
        jScrollPane.setAlignmentX(0.0f);
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BoxLayout(jPanel, 3));
        JLabel jLabel = new JLabel(str);
        jLabel.setLabelFor(this.list);
        jPanel.add(jLabel);
        jPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        jPanel.add(jScrollPane);
        jPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        JPanel jPanel2 = new JPanel();
        jPanel2.setLayout(new BoxLayout(jPanel2, 2));
        jPanel2.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
        jPanel2.add(Box.createHorizontalGlue());
        jPanel2.add(jButton);
        jPanel2.add(Box.createRigidArea(new Dimension(10, 0)));
        jPanel2.add(jButton2);
        Container contentPane = getContentPane();
        contentPane.add(jPanel, "Center");
        contentPane.add(jPanel2, "Last");
        setValue(str3);
        pack();
        setLocationRelativeTo(component);
    }

    /* renamed from: a */
    public static String m838a(Component component, Component component2, String str, String str2, String[] strArr, String str3, String str4) {
        cnO = new ChosenNameSubTest(JOptionPane.getFrameForComponent(component), component2, str, str2, strArr, str3, str4);
        cnO.setVisible(true);
        return value;
    }

    private void setValue(String str) {
        value = str;
        this.list.setSelectedValue(value, true);
    }

    public void actionPerformed(ActionEvent actionEvent) {
        if ("Set".equals(actionEvent.getActionCommand())) {
            value = (String) this.list.getSelectedValue();
        }
        cnO.setVisible(false);
    }

    /* renamed from: a.BD$a */
    class C0091a extends JList {
        C0091a(Object[] objArr) {
            super(objArr);
        }

        public int getScrollableUnitIncrement(Rectangle rectangle, int i, int i2) {
            int firstVisibleIndex;
            if (i == 1 && i2 < 0 && (firstVisibleIndex = getFirstVisibleIndex()) != -1) {
                Rectangle cellBounds = getCellBounds(firstVisibleIndex, firstVisibleIndex);
                if (cellBounds.y == rectangle.y && firstVisibleIndex != 0) {
                    Point location = cellBounds.getLocation();
                    location.y--;
                    int locationToIndex = locationToIndex(location);
                    Rectangle cellBounds2 = getCellBounds(locationToIndex, locationToIndex);
                    if (cellBounds2 == null || cellBounds2.y >= cellBounds.y) {
                        return 0;
                    }
                    return cellBounds2.height;
                }
            }
            return super.getScrollableUnitIncrement(rectangle, i, i2);
        }
    }

    /* renamed from: a.BD$b */
    /* compiled from: a */
    class C0092b extends MouseAdapter {
        private final /* synthetic */ JButton enS;

        C0092b(JButton jButton) {
            this.enS = jButton;
        }

        public void mouseClicked(MouseEvent mouseEvent) {
            if (mouseEvent.getClickCount() == 2) {
                this.enS.doClick();
            }
        }
    }
}
