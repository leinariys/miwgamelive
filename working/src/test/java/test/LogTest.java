package test;

import logic.thred.LogPrinter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class LogTest {
    public static final byte eqK = 2;
    public static final byte eqL = 4;
    public static final byte eqM = 8;
    public static final byte eqN = 16;
    public static final byte eqO = 32;
    public static final byte eqP = 64;
    public static final byte eqQ = 126;
    public static final byte eqR = 120;
    public static byte eqT = 0;
    static Properties eqX;
    static String property;
    private static LogPrinter logger = LogPrinter.m10275K(LogTest.class);

    public static void main(String[] args) throws IOException {
        File file = new File("taikodomlogger.properties");
        eqX = new Properties();
        eqX.load(new FileInputStream(file));

        logger.fatal("111111111111111111111111111111111111");
        logger.error("22222222222222222222222222222222222222");
        logger.info("3333333333333333333333333333333333333");
        logger.trace("44444444444444444444444444444444444444444");
        logger.warn("555555555555555555555555555555555555555");


        byte gM = m10279gM("levels");
        if (gM == 0) {
            gM = eqR;
        }
        eqT = gM;


        property = System.getProperty("TaikodomLogger.DISABLE");
        boolean eqU = property != null && property.equalsIgnoreCase("true");


        System.out.println("dfsdf");
    }

    private static byte m10279gM(String str) {
        String property;
        byte b = 0;
        if (eqX == null || (property = eqX.getProperty(str, (String) null)) == null) {
            return 0;
        }
        String[] split = property.split(",");
        int i = 0;
        while (true) {
            byte b2 = b;
            if (i >= split.length) {
                return b2;
            }
            String str2 = split[i];
            if (str2.equals("trace")) {
                b = (byte) (b2 | 2);
            } else if (str2.equals("debug")) {
                b = (byte) (b2 | 4);
            } else if (str2.equals("info")) {
                b = (byte) (b2 | 8);
            } else if (str2.equals("warn")) {
                b = (byte) (b2 | eqN);
            } else if (str2.equals("error")) {
                b = (byte) (b2 | eqO);
            } else {
                b = str2.equals("fatal") ? (byte) (b2 | eqP) : b2;
            }
            i++;
        }
    }
}
