package test;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/* renamed from: test.a.ei */
/* compiled from: test.a */
public class ChosenNameTest {

    /* renamed from: BE */
    static JFrame jFrameRoot;
    static String[] names = {"Arlo", "Cosmo", "Elmo", "Hugo", "Jethro", "Laszlo", "Milo", "Nemo", "Otto", "Ringo", "Rocco", "Rollo"};

    /* renamed from: kW */
    public static JPanel pickNewName() {
        JLabel jLabel = new JLabel("The chosen name:");
        JLabel jLabel2 = new JLabel(names[1]);
        jLabel.setLabelFor(jLabel2);
        jLabel2.setFont(m29975kX());
        JButton jButton = new JButton("Pick new name...");
        jButton.addActionListener(new C2388b(jButton, jLabel2));
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BoxLayout(jPanel, 3));
        jPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 10, 20));
        jLabel.setAlignmentX(0.5f);
        jLabel2.setAlignmentX(0.5f);
        jButton.setAlignmentX(0.5f);
        jPanel.add(jLabel);
        jPanel.add(Box.createVerticalStrut(5));
        jPanel.add(jLabel2);
        jPanel.add(Box.createRigidArea(new Dimension(150, 10)));
        jPanel.add(jButton);
        return jPanel;
    }

    /* renamed from: kX */
    public static Font m29975kX() {
        String[] strArr;
        String str = null;
        String[] strArr2 = {"French Script", "FrenchScript", "Script"};
        String[] strArr3 = null;
        GraphicsEnvironment localGraphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        if (localGraphicsEnvironment != null) {
            strArr = localGraphicsEnvironment.getAvailableFontFamilyNames();
        } else {
            strArr = strArr3;
        }
        if (!(strArr == null || strArr2 == null)) {
            int i = 0;
            while (str == null && i < strArr2.length) {
                int i2 = 0;
                String str2 = str;
                while (str2 == null && i2 < strArr.length) {
                    if (strArr[i2].startsWith(strArr2[i]) && new Font(strArr[i2], 0, 1).canDisplay('A')) {
                        str2 = strArr[i2];
                        System.out.println("Using font: " + str2);
                    }
                    i2++;
                }
                i++;
                str = str2;
            }
        }
        if (str != null) {
            return new Font(str, 0, 36);
        }
        return new Font("Serif", 2, 36);
    }

    /* access modifiers changed from: private */
    /* renamed from: kY */
    public static void creatJFrame() {
        jFrameRoot = new JFrame("Name That Baby");
        jFrameRoot.setDefaultCloseOperation(3);
        JPanel pickNewName = pickNewName();
        pickNewName.setOpaque(true);
        jFrameRoot.setContentPane(pickNewName);
        jFrameRoot.pack();
        jFrameRoot.setVisible(true);
    }

    public static void main(String[] strArr) {
        SwingUtilities.invokeLater(new JFrameRunnable());
    }

    /* renamed from: test.a.ei$b */
    /* compiled from: test.a */
    static class C2388b implements ActionListener {
        private final JButton button;
        private final JLabel label;

        C2388b(JButton jButton, JLabel jLabel) {
            this.button = jButton;
            this.label = jLabel;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            this.label.setText(ChosenNameSubTest.m838a(ChosenNameTest.jFrameRoot, this.button, "Baby names ending in O:", "Name Chooser", ChosenNameTest.names, this.label.getText(), "Cosmo  "));
        }
    }

    /* renamed from: test.a.ei$test.a */
    static class JFrameRunnable implements Runnable {
        JFrameRunnable() {
        }

        public void run() {
            ChosenNameTest.creatJFrame();
        }
    }
}
