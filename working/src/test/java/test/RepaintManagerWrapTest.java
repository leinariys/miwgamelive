package test;

import logic.render.RepaintManagerWrap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.aqB  reason: case insensitive filesystem */
/* compiled from: a */
public class RepaintManagerWrapTest {

    public static void main(String[] args) {
        JFrame jFrame = new JFrame();
        jFrame.setVisible(true);
        jFrame.setSize(new Dimension(500, 500));

        JPanel jPanel = new JPanel();
        jPanel.setVisible(true);
        jFrame.setLayout((LayoutManager) null);
        jFrame.add(jPanel);
        jPanel.setLayout((LayoutManager) null);
        jPanel.setBounds(100, 100, 200, 200);
        jPanel.setBackground(Color.BLUE);


        JPanel jPanel2 = new JPanel();
        jPanel2.setVisible(true);
        jPanel2.setBackground(Color.RED);
        jPanel.add(jPanel2);
        jPanel2.setBounds(10, 20, 33, 55);

        RepaintManagerWrap repaintManagerWrap = new RepaintManagerWrap();
        repaintManagerWrap.addDirtyRegion((JComponent) jPanel2, 0, 0, jPanel2.getWidth(), jPanel2.getHeight());
        repaintManagerWrap.addDirtyRegion((JComponent) jPanel2, 30, 0, jPanel2.getWidth(), jPanel2.getHeight());
    }


    @Test
    public void crv() {
        JFrame jFrame = new JFrame();
        jFrame.setVisible(true);
        JPanel jPanel = new JPanel();
        jFrame.setLayout((LayoutManager) null);
        jFrame.add(jPanel);
        jPanel.setLayout((LayoutManager) null);
        jPanel.setBounds(100, 100, 1000, 1000);
        JPanel jPanel2 = new JPanel();
        jPanel.add(jPanel2);
        jPanel2.setBounds(10, 20, 33, 55);
        RepaintManagerWrap repaintManagerWrap = new RepaintManagerWrap();
        repaintManagerWrap.addDirtyRegion((JComponent) jPanel2, 0, 0, jPanel2.getWidth(), jPanel2.getHeight());
        Assertions.assertEquals(new Rectangle(10, 20, 33, 55), repaintManagerWrap.mo14780p(jPanel));
        repaintManagerWrap.addDirtyRegion((JComponent) jPanel2, 30, 0, jPanel2.getWidth(), jPanel2.getHeight());
        Assertions.assertEquals(new Rectangle(40, 20, 3, 55), repaintManagerWrap.mo14780p(jPanel));
    }
}
