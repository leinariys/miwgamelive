package test;

import com.sun.opengl.util.Animator;
import game.geometry.Quat4fWrap;
import game.geometry.Vector2fWrap;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.exec.Watchdog;
import taikodom.render.RenderGui;
import taikodom.render.RenderView;
import taikodom.render.SceneView;
import taikodom.render.StepContext;
import taikodom.render.camera.Camera;
import taikodom.render.enums.BlendType;
import taikodom.render.enums.GeometryType;
import taikodom.render.gui.GLine;
import taikodom.render.gui.GuiScene;
import taikodom.render.loader.FileSceneLoader;
import taikodom.render.scene.Scene;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.ChannelInfo;
import taikodom.render.textures.Material;
import taikodom.render.textures.Texture;

import javax.media.opengl.*;
import java.awt.*;
import java.awt.event.*;

/* compiled from: a */
@Slf4j
public class GLineTest extends Frame implements MouseListener, MouseMotionListener, MouseWheelListener, GLEventListener {
    private static final long serialVersionUID = -1252909773268949583L;
    static GLineTest demo = null;
    /* access modifiers changed from: private */
    public Vector2fWrap center;
    int displayCount = 0;
    Texture dustTex;
    int fps;
    boolean isRendering = false;
    GLine line;
    boolean lockedTarget;
    int mousex;
    int mousey;
    SceneView sceneView;
    int secondCounter;
    StepContext stepContext;
    long timeToDisplay = 0;
    Texture trailTex;
    private long before = System.nanoTime();
    private Camera camera;
    private Quat4fWrap cameraOrientation;
    private Quat4fWrap finalCameraOrientation;
    private GuiScene guiScene = new GuiScene();
    private FileSceneLoader loader;
    private RenderGui renderGui = new RenderGui();
    private String resourceDir;
    /* renamed from: rv */
    private RenderView f10352rv;
    private Scene scene;
    private long startTime = System.nanoTime();
    private Watchdog watchDog;

    public GLineTest() {
        super("Basic GLine Demo");
        setLayout(new BorderLayout());
        setSize(720, 640);
        setLocation(40, 40);
        setAlwaysOnTop(true);
        setVisible(true);
        setupJOGL();
        addWindowListener(new C5137a());
    }

    public static void main(String[] strArr) {
        demo = new GLineTest();
        demo.resourceDir = "../taikodom.client.resource/";
        if (strArr.length > 0) {
            demo.resourceDir = strArr[0];
        }
        demo.setVisible(true);
    }

    public void init(GLAutoDrawable gLAutoDrawable) {
        try {
            this.f10352rv = new RenderView(gLAutoDrawable.getContext());
            this.f10352rv.setViewport(0, 0, gLAutoDrawable.getWidth(), gLAutoDrawable.getHeight());
            gLAutoDrawable.setGL(new DebugGL(gLAutoDrawable.getGL()));
            this.f10352rv.getDrawContext().setCurrentFrame(1000);
            this.f10352rv.setGL(gLAutoDrawable.getGL());
            this.renderGui.getGuiContext().setDc(this.f10352rv.getDrawContext());
            this.f10352rv.getDrawContext().setRenderView(this.f10352rv);
            this.loader = new FileSceneLoader(this.resourceDir);
            System.loadLibrary("granny2");
            System.loadLibrary("mss32");
            System.loadLibrary("binkw32");
            System.loadLibrary("utaikodom.render");
            this.loader.loadFile("data/shaders/light_shader.pro");
            this.loader.loadFile("data/models/fig_spa_bullfrog.pro");
            this.loader.loadFile("data/models/fig_spa_barracuda.pro");
            this.loader.loadFile("data/scene/nod_spa_mars.pro");
            this.loader.loadFile("data/trails.pro");
            this.loader.loadFile("data/effects.pro");
            this.loader.loadFile("data/models/amm_min_contramed1.pro");
            this.loader.loadFile("data/hud.pro");
            this.loader.loadMissing();
            this.sceneView = new SceneView();
            this.camera = this.sceneView.getCamera();
            this.camera.setFarPlane(400000.0f);
            this.camera.setNearPlane(1.0f);
            this.camera.setFovY(75.0f);
            this.camera.setAspect(1.3333f);
            this.camera.getTransform().setTranslation(20.0d, 20.0d, 20.0d);
            Shader shader = new Shader();
            shader.setName("Default billboard shader");
            ShaderPass shaderPass = new ShaderPass();
            shaderPass.setName("Default billboard pass");
            ChannelInfo channelInfo = new ChannelInfo();
            channelInfo.setTexChannel(8);
            shaderPass.setChannelTextureSet(0, channelInfo);
            shaderPass.setBlendEnabled(true);
            shaderPass.setAlphaTestEnabled(true);
            shaderPass.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
            shaderPass.setSrcBlend(BlendType.ONE);
            shaderPass.setCullFaceEnabled(true);
            shaderPass.getRenderStates().setCullFaceEnabled(true);
            shader.addPass(shaderPass);
            Material material = new Material();
            ShaderPass shaderPass2 = new ShaderPass();
            shaderPass2.setName("Default billboard pass");
            ChannelInfo channelInfo2 = new ChannelInfo();
            channelInfo2.setTexChannel(8);
            shaderPass2.setChannelTextureSet(0, channelInfo2);
            shaderPass2.setBlendEnabled(true);
            shaderPass2.setAlphaTestEnabled(true);
            shaderPass2.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
            shaderPass2.setSrcBlend(BlendType.ONE);
            shaderPass2.setCullFaceEnabled(true);
            shaderPass2.getRenderStates().setCullFaceEnabled(true);
            material.setShader(shader);
            this.scene = this.sceneView.getScene();
            this.center = new Vector2fWrap(320.0f, 310.0f);
            this.cameraOrientation = new Quat4fWrap();
            this.finalCameraOrientation = new Quat4fWrap();
            this.line = new GLine();
            this.line.setMaterial(material);
            this.line.createCircle(this.center, 100.0f);
            this.line.createSquare(this.center, 150.0f);
            this.line.createRectangle(this.center, 200.0f, 250.0f);
            this.line.createElipse(this.center, 100.0f, 250.0f);
            this.line.setGeometryType(GeometryType.LINES);
            this.guiScene.addChild(this.line);
            System.nanoTime();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    public void display(GLAutoDrawable gLAutoDrawable) {
        try {
            long currentTimeMillis = System.currentTimeMillis();
            if (this.watchDog == null) {
                this.watchDog = new Watchdog(5000);
                /*
                PrintWriter M = LogPrinter.createLogFile("client-watchdog", "log");
                M.println("Starting GLine demo");
                this.watchDog.setPrintWriter(M);
                this.watchDog.mo14458ht(20);
                this.watchDog.mo14453K(2.0E10d);
                */
                this.watchDog.start();
            }
            this.f10352rv.getDrawContext().setViewport(0, 0, getSize().width, getSize().height);
            this.f10352rv.getRenderInfo().setDrawAABBs(false);
            long j = currentTimeMillis - this.startTime;
            this.camera.getTransform().setIdentity();
            this.cameraOrientation.mo15207a(this.cameraOrientation, this.finalCameraOrientation, 0.1f);
            this.camera.setOrientation(this.cameraOrientation);
            this.camera.calculateProjectionMatrix();
            if (this.stepContext == null) {
                this.stepContext = new StepContext();
            }
            this.stepContext.setCamera(this.camera);
            this.stepContext.setDeltaTime((float) (currentTimeMillis - this.before));
            this.scene.step(this.stepContext);
            this.guiScene.step(this.stepContext);
            this.timeToDisplay += currentTimeMillis - this.before;
            this.f10352rv.render(this.sceneView, (double) (((float) j) * 1.0E-9f), ((float) (currentTimeMillis - this.before)) * 1.0E-9f);
            this.renderGui.render(this.guiScene);
            this.before = currentTimeMillis;
            if (this.timeToDisplay > 1000000000) {
                this.timeToDisplay = 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    private void setupJOGL() {
        GLCapabilities gLCapabilities = new GLCapabilities();
        gLCapabilities.setDoubleBuffered(true);
        gLCapabilities.setHardwareAccelerated(true);
        GLCanvas gLCanvas = new GLCanvas(gLCapabilities);
        gLCanvas.enableInputMethods(true);
        gLCanvas.addGLEventListener(this);
        gLCanvas.addMouseMotionListener(this);
        gLCanvas.addMouseListener(this);
        gLCanvas.addMouseWheelListener(this);
        add(gLCanvas, "Center");
        gLCanvas.addMouseMotionListener(new C5138b());
        gLCanvas.addKeyListener(new C5139c());
        new Animator(gLCanvas).start();
    }

    public void reshape(GLAutoDrawable gLAutoDrawable, int i, int i2, int i3, int i4) {
        if (this.camera != null) {
            this.camera.setAspect(((float) i3) / ((float) i4));
        }
        this.sceneView.setViewport(0, 0, i3, i4);
        this.renderGui.resize(i, i2, i3, i4);
    }

    public void displayChanged(GLAutoDrawable gLAutoDrawable, boolean z, boolean z2) {
        this.f10352rv.setViewport(0, 0, gLAutoDrawable.getWidth(), gLAutoDrawable.getHeight());
    }

    public void mouseMoved(MouseEvent mouseEvent) {
    }

    public void mouseClicked(MouseEvent mouseEvent) {
    }

    public void mouseEntered(MouseEvent mouseEvent) {
    }

    public void mouseExited(MouseEvent mouseEvent) {
    }

    public void mousePressed(MouseEvent mouseEvent) {
    }

    public void mouseReleased(MouseEvent mouseEvent) {
    }

    public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
    }

    public void mouseDragged(MouseEvent mouseEvent) {
    }

    /* renamed from: test.GLineTest$a */
    class C5137a extends WindowAdapter {
        C5137a() {
        }

        public void windowClosing(WindowEvent windowEvent) {
            System.exit(0);
        }

        public void windowClosed(WindowEvent windowEvent) {
            System.exit(0);
        }
    }

    /* renamed from: test.GLineTest$b */
    /* compiled from: a */
    class C5138b implements MouseMotionListener {
        C5138b() {
        }

        public void mouseDragged(MouseEvent mouseEvent) {
        }

        public void mouseMoved(MouseEvent mouseEvent) {
            GLineTest.this.mousex = mouseEvent.getX();
            GLineTest.this.mousey = mouseEvent.getY();
        }
    }

    /* renamed from: test.GLineTest$c */
    /* compiled from: a */
    class C5139c implements KeyListener {
        C5139c() {
        }

        public void keyPressed(KeyEvent keyEvent) {
            float f = 0.0f;
            if (keyEvent.getKeyCode() == 87) {
                GLineTest.this.center.y += 1.0f;
            }
            if (keyEvent.getKeyCode() == 83) {
                GLineTest.this.center.y -= 1.0f;
            }
            if (keyEvent.getKeyCode() == 65) {
                GLineTest.this.center.x -= 1.0f;
            }
            if (keyEvent.getKeyCode() == 68) {
                GLineTest.this.center.x += 1.0f;
            }
            GLineTest.this.center.x = GLineTest.this.center.x < 0.0f ? 0.0f : GLineTest.this.center.x;
            Vector2fWrap access$0 = GLineTest.this.center;
            if (GLineTest.this.center.y >= 0.0f) {
                f = GLineTest.this.center.y;
            }
            access$0.y = f;
        }

        public void keyReleased(KeyEvent keyEvent) {
        }

        public void keyTyped(KeyEvent keyEvent) {
        }
    }
}
