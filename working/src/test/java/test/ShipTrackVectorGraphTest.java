package test;

import com.hoplon.geometry.Vec3f;

import javax.swing.*;
import javax.vecmath.Vector3f;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: a.wx */
/* compiled from: a */
public class ShipTrackVectorGraphTest {

    /* renamed from: BE */
    public static JFrame jFrame;
    public static ShipTrackVectorGraphTest currentClass;

    static Vec3f line_cur = new Vec3f();
    static Vec3f line_des = new Vec3f();
    static Vec3f line_force1 = new Vec3f();
    static Vec3f line_force2 = new Vec3f();
    static Vec3f line_x = new Vec3f();
    static Vec3f line_y = new Vec3f();
    static Vec3f line_z = new Vec3f();
    static String text = "";
    private static ClassLoader classLoader = ShipTrackVectorGraphTest.class.getClassLoader();
    private static String pattrn = "cur=\\(([E0-9.-]+), ([E0-9.-]+), ([E0-9.-]+)\\) des=\\(([E0-9.-]+), ([E0-9.-]+), ([E0-9.-]+)\\) force=\\(([E0-9.-]+), ([E0-9.-]+), ([E0-9.-]+)\\) \\(([E0-9.-]+), ([E0-9.-]+), ([E0-9.-]+)\\) X=\\(([E0-9.-]+), ([E0-9.-]+), ([E0-9.-]+)\\) Y=\\(([E0-9.-]+), ([E0-9.-]+), ([E0-9.-]+)\\) Z=\\(([E0-9.-]+), ([E0-9.-]+), ([E0-9.-]+)\\) yawAng=([E0-9.-]+) pitchAng=([E0-9.-]+)";
    private GraphicsComponent graphicsComponent;
    private Timer timer = new Timer(1000, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            graphicsComponent.repaint();
        }
    });


    public ShipTrackVectorGraphTest() {
        this.timer.start();
    }

    public static void main(String[] strArr) throws IOException {
        File fileVectorGraphTest = new File(classLoader.getResource("VectorGraphTest.xml").getFile());
        System.out.println(pattrn);
        VectorGraphSwingTest vectorGraph = new VectorGraphSwingTest();
        vectorGraph.setDaemon(true);
        vectorGraph.start();
        LineNumberReader lineNumberReader = new LineNumberReader(new FileReader(fileVectorGraphTest));
        Pattern compile = Pattern.compile(pattrn);

        while (true) {
            String readLine = lineNumberReader.readLine();
            if (readLine != null) {
                Matcher matcher = compile.matcher(readLine);
                if (matcher.matches()) {
                    line_cur = new Vec3f(Float.parseFloat(matcher.group(1)), Float.parseFloat(matcher.group(2)), Float.parseFloat(matcher.group(3)));
                    line_des = new Vec3f(Float.parseFloat(matcher.group(4)), Float.parseFloat(matcher.group(5)), Float.parseFloat(matcher.group(6)));
                    line_force1 = new Vec3f(Float.parseFloat(matcher.group(7)), Float.parseFloat(matcher.group(8)), Float.parseFloat(matcher.group(9)));
                    line_force2 = new Vec3f(Float.parseFloat(matcher.group(10)), Float.parseFloat(matcher.group(11)), Float.parseFloat(matcher.group(12)));
                    line_x = new Vec3f(Float.parseFloat(matcher.group(13)), Float.parseFloat(matcher.group(14)), Float.parseFloat(matcher.group(15)));
                    line_y = new Vec3f(Float.parseFloat(matcher.group(16)), Float.parseFloat(matcher.group(17)), Float.parseFloat(matcher.group(18)));
                    line_z = new Vec3f(Float.parseFloat(matcher.group(19)), Float.parseFloat(matcher.group(20)), Float.parseFloat(matcher.group(21)));
                    text = readLine.replaceAll("\\) ", ")\n");
                    SwingUtilities.invokeLater(new RunGraphicsComponent());
                }
            } else {
                return;
            }
        }
    }

    public void repaint() {
        this.graphicsComponent.repaint();
    }

    /* renamed from: a */
    public void mo22827a(Container container) {
        container.setLayout(new BoxLayout(container, 3));
        this.graphicsComponent = new GraphicsComponent();
        container.add(this.graphicsComponent);
        this.graphicsComponent.setAlignmentX(0.0f);
    }

    /* renamed from: a.wx$a */
    static class VectorGraphSwingTest extends Thread {

        public void run() {
            SwingUtilities.invokeLater(new C3936a());
        }

        /* renamed from: a.wx$a$a */
        class C3936a implements Runnable {

            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (Exception e) {
                }
                ShipTrackVectorGraphTest.jFrame = new JFrame("ShipTrack");
                ShipTrackVectorGraphTest.jFrame.setDefaultCloseOperation(3);
                ShipTrackVectorGraphTest.currentClass = new ShipTrackVectorGraphTest();
                ShipTrackVectorGraphTest.currentClass.mo22827a(ShipTrackVectorGraphTest.jFrame.getContentPane());
                ShipTrackVectorGraphTest.jFrame.pack();
                ShipTrackVectorGraphTest.jFrame.setVisible(true);
            }
        }
    }

    /* renamed from: a.wx$b */
    /* compiled from: a */
    static class RunGraphicsComponent implements Runnable {

        public void run() {
            ShipTrackVectorGraphTest.currentClass.repaint();
        }
    }

    /* renamed from: a.wx$c */
    /* compiled from: a */
    class GraphicsComponent extends JComponent {
        private final float kZ = 0.70710677f;
        private final float kXY = 0.033333335f;
        private final int widthPoint = 15;
        private final int heightPoint = 15;
        Color gridColor;
        int ifJ;
        int ifK;
        Dimension preferredSize = new Dimension(600, 600);

        public GraphicsComponent() {
            setBackground(Color.WHITE);
            setOpaque(true);
        }

        public Dimension getPreferredSize() {
            return this.preferredSize;
        }

        /* access modifiers changed from: protected */
        public void paintComponent(Graphics graphics) {
            if (isOpaque()) {
                graphics.setColor(getBackground());
                graphics.fillRect(0, 0, getWidth(), getHeight());
            }
            graphics.setColor(Color.BLACK);

            int min = (Math.min(getSize().width, getSize().height) - 100) / 2;
            paintVector(graphics, new Vec3f(((float) (-min)) / kXY, 0.0f, 0.0f), new Vec3f(((float) min) / kXY, 0.0f, 0.0f), "X");
            paintVector(graphics, new Vec3f(0.0f, ((float) (-min)) / kXY, 0.0f), new Vec3f(0.0f, ((float) min) / kXY, 0.0f), "Y");
            paintVector(graphics, new Vec3f(0.0f, 0.0f, ((float) (-min)) / kXY), new Vec3f(0.0f, 0.0f, ((float) min) / kXY), "Z");

            graphics.setColor(Color.RED);
            Vec3f vec3f = new Vec3f((Vector3f) ShipTrackVectorGraphTest.line_force2);
            vec3f.scale(10.0f);
            paintAxisProjection(graphics, vec3f);

            graphics.setColor(Color.BLUE);
            Vec3f vec3f2 = new Vec3f((Vector3f) ShipTrackVectorGraphTest.line_x);
            vec3f2.scale(500.0f);
            paintVector(graphics, vec3f, vec3f.mo23503i(vec3f2), "x");
            Vec3f vec3f3 = new Vec3f((Vector3f) ShipTrackVectorGraphTest.line_y);
            vec3f3.scale(500.0f);
            paintVector(graphics, vec3f, vec3f.mo23503i(vec3f3), "y");
            Vec3f vec3f4 = new Vec3f((Vector3f) ShipTrackVectorGraphTest.line_z);
            vec3f4.scale(500.0f);
            paintVector(graphics, vec3f, vec3f.mo23503i(vec3f4), "z");

            graphics.setColor(Color.MAGENTA);
            Vec3f vec3f5 = new Vec3f((Vector3f) ShipTrackVectorGraphTest.line_des);
            vec3f5.scale(5.0f);
            paintVector(graphics, vec3f, vec3f.mo23503i(vec3f5), "line_des");
            paintAxisProjection(graphics, vec3f.mo23503i(vec3f5));

            graphics.setColor(Color.BLACK);
            int i = 10;
            for (String drawString : ShipTrackVectorGraphTest.text.split("\n")) {
                graphics.drawString(drawString, 10, i);
                i += graphics.getFontMetrics().getHeight();
            }
        }

        /* renamed from: a */
        private void paintAxisProjection(Graphics graphics, Vec3f vec3f) {
            Point bE = paintPoint(vec3f);
            Color color = graphics.getColor();
            graphics.setColor(new Color(Math.min(color.getRed() + ((255 - color.getRed()) / 2), 255), Math.min(color.getGreen() + ((255 - color.getGreen()) / 2), 255), Math.min(color.getBlue() + ((255 - color.getBlue()) / 2), 255)));
            paintLine(graphics, new Vec3f(vec3f.x, -5.0f, 0.0f), new Vec3f(vec3f.x, 5.0f, 0.0f));
            paintLine(graphics, new Vec3f(-5.0f, vec3f.y, 0.0f), new Vec3f(5.0f, vec3f.y, 0.0f));
            paintLine(graphics, new Vec3f(0.0f, -5.0f, vec3f.z), new Vec3f(0.0f, 5.0f, vec3f.z));
            paintLine(graphics, new Vec3f(vec3f.x, 0.0f, 0.0f), new Vec3f(vec3f.x, 0.0f, vec3f.z));
            paintLine(graphics, new Vec3f(0.0f, 0.0f, vec3f.z), new Vec3f(vec3f.x, 0.0f, vec3f.z));
            paintLine(graphics, new Vec3f(vec3f.x, 0.0f, vec3f.z), new Vec3f(vec3f.x, vec3f.y, vec3f.z));
            graphics.setColor(color);
            graphics.drawOval(bE.x - widthPoint / 2, bE.y - heightPoint / 2, widthPoint, heightPoint);
        }

        /* renamed from: a */
        private void paintLine(Graphics graphics, Vec3f vec3f, Vec3f vec3f2) {
            Point bE = paintPoint(vec3f);
            Point bE2 = paintPoint(vec3f2);
            graphics.drawLine(bE.x, bE.y, bE2.x, bE2.y);
        }

        /* renamed from: a */
        private void paintVector(Graphics graphics, Vec3f vec3f, Vec3f vec3f2, String str) {
            paintLine(graphics, vec3f, vec3f2);
            Point bE = paintPoint(vec3f2);
            graphics.drawOval(bE.x - widthPoint / 2, bE.y - heightPoint / 2, widthPoint, heightPoint);
            if (str != null) {
                graphics.drawString(str, bE.x + 10, bE.y);
            }
        }

        /**
         * Рисуем точку
         *
         * @param vec3f
         * @return
         */
        /* renamed from: bE */
        private Point paintPoint(Vec3f vec3f) {
            Point point = new Point();
            point.x = (int) (((float) (getSize().width / 2)) + (vec3f.x * kXY));
            point.y = (int) (((float) (getSize().height / 2)) - (vec3f.y * kXY));
            if (vec3f.z != 0.0f) {
                point.x = (int) (((float) point.x) - ((vec3f.z * kXY) * kZ));
                point.y = (int) (((float) point.y) + (vec3f.z * kXY * kZ));
            }
            return point;
        }
    }
}
