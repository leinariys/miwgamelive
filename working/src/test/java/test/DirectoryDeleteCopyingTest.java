package test;

import taikodom.render.graphics2d.C0559Hm;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/* renamed from: a.uc */
/* compiled from: a */
public class DirectoryDeleteCopyingTest {
    public static void main(String[] strArr) throws IOException {

        //String[] arr = { "C:\\Projects\\Java\\miwgamelive\\working\\libs\\natives", "C:\\Projects\\Java\\miwgamelive\\working\\libs\\natives" } ;
        new DirectoryDeleteCopyingTest().run(strArr);
    }

    private void run(String[] strArr) throws IOException {
        File file;
        File file2 = new File(strArr[0]);
        File file3 = new File(strArr[1]);
        File[] listFiles = file2.listFiles();
        if (listFiles != null) {
            Arrays.sort(listFiles);
            ArrayList arrayList = new ArrayList(Arrays.asList(listFiles));
            Iterator it = arrayList.iterator();
            while (true) {
                if (it.hasNext()) {
                    file = (File) it.next();
                    if (file.getName().matches(".*utaikodom\\.game.*")) {
                        break;
                    }
                } else {
                    file = null;
                    break;
                }
            }
            if (file == null) {
                throw new IllegalStateException("Did not find utaikodom.game");
            }

            arrayList.remove(file);
            arrayList.add(file);
            arrayList.toArray(listFiles);


            m40079d(file3);
            String str = strArr.length > 2 ? strArr[2] : "bin";
            for (File file4 : listFiles) {
                File file5 = new File(file4, str);
                if (file5.isDirectory()) {
                    m40078a(file5, file3);
                }
            }
        }
    }

    /* renamed from: a */
    private void m40078a(File file, File file2) throws IOException {
        System.out.println("Copying " + file + " to " + file2);
        C0559Hm.m5250a(file, file2, true);
    }

    /* renamed from: d */
    private void m40079d(File file) {
        System.out.println("Cleaning up " + file);
        C0559Hm.m5252a(file, true);
    }
}
