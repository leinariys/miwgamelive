package test;

import jdbm.RecordManager;
import jdbm.RecordManagerFactory;
import jdbm.btree.BTree;
import jdbm.helper.StringComparator;
import jdbm.helper.Tuple;
import jdbm.helper.TupleBrowser;

import java.io.IOException;
import java.util.Properties;

public class FamousPeople {

    static String DATABASE = "working/src/test/resources/test/FruitBasket/environment";
    static String BTREE_NAME = "FamousPeople";

    static String[] people =
        {"Greenspan, Alan",
            "Williams-Byrd, Julie",
            "Picasso, Pablo",
            "Stallman, Richard",
            "Fort, Paul",
            "Søndergaard, Ole",
            "Schwarzenegger, Arnold",
            "Dulkinys, Susanna"};

    static String[] occupations =
        {"Federal Reserve Board Chairman",
            "Engineer",
            "Painter",
            "Programmer",
            "Poet",
            "Typographer",
            "Actor",
            "Designer"};

    static String PREFIX = "S";

    /**
     * Example main entrypoint.
     */
    public static void main(String[] args) {
        RecordManager recman;
        long recid;
        Tuple tuple = new Tuple();
        TupleBrowser tupleBrowser;
        BTree bTree;
        Properties props;

        props = new Properties();

        try {
            // открыть базу данных и настроить кеш объектов
            recman = RecordManagerFactory.createRecordManager(DATABASE, props);

            // попробуйте перезагрузить существующее дерево B+Tree
            recid = recman.getNamedObject(BTREE_NAME);
            if (recid != 0) {
                bTree = BTree.load(recman, recid);
                System.out.println("Reloaded existing BTree with " + bTree.size()
                    + " famous people.");
            } else {
                // создайте новую структуру данных B+Tree и используйте StringComparator для упорядочения записей на основе имен людей.
                bTree = BTree.createInstance(recman, new StringComparator());
                recman.setNamedObject(BTREE_NAME, bTree.getRecid());
                System.out.println("Created a new empty BTree");
            }

            // вставьте людей с их соответствующей профессией
            System.out.println();
            for (int i = 0; i < people.length; i++) {
                System.out.println("Insert: " + people[i]);
                bTree.insert(people[i], occupations[i], false);
            }

            // сделать данные постоянными в базе данных
            recman.commit();

            // показать список людей с их профессией
            System.out.println();
            System.out.println("Person                   Occupation       ");
            System.out.println("------------------       ------------------");

            // проходить людей по порядку
            showTree(bTree);

            // проходить людей в обратном порядке
            System.out.println();
            System.out.println("Reverse order:");
            tupleBrowser = bTree.browse(null); // позиционировать браузер в конце списка

            while (tupleBrowser.getPrevious(tuple)) {
                print(tuple);
            }


            // display people whose name start with PREFIX range
            System.out.println();
            System.out.println("All people whose name start with '" + PREFIX + "':");

            tupleBrowser = bTree.browse(PREFIX);
            while (tupleBrowser.getNext(tuple)) {
                String key = (String) tuple.getKey();
                if (key.startsWith(PREFIX)) {
                    print(tuple);
                } else {
                    break;
                }
            }

        } catch (Exception except) {
            except.printStackTrace();
        }
    }

    // Показать содержимое
    public static void showTree(BTree bTree) throws IOException {
        TupleBrowser tupleBrowser = bTree.browse();
        Tuple tuple = new Tuple();
        while (tupleBrowser.getNext(tuple)) {
            System.out.println(tuple.getKey() + " - " + tuple.getValue());
        }
    }

    /**
     * Print a Tuple containing a ( Person, Occupation ) pair.
     */
    static void print(Tuple tuple) {
        String person = (String) tuple.getKey();
        String occupation = (String) tuple.getValue();
        System.out.println(pad(person, 25) + occupation);
    }


    /**
     * Pad a string with spaces on the right.
     *
     * @param str   String to add spaces
     * @param width Width of string after padding
     */
    static String pad(String str, int width) {
        StringBuffer buf = new StringBuffer(str);
        int space = width - buf.length();
        while (space-- > 0) {
            buf.append(' ');
        }
        return buf.toString();
    }

}
