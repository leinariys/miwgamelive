package test;

import logic.ui.BaseUiTegXml;
import logic.ui.item.GBox;
import logic.ui.item.HBox;
import logic.ui.item.VBox;

import java.awt.*;

/* renamed from: a.Fp */
/* compiled from: a */
public class DimensionTest {

    public static void main(String[] args) {
        DimensionTest sdsf = new DimensionTest();
        sdsf.aPp();
        sdsf.aPq();
        sdsf.aPr();
        sdsf.aPs();
        sdsf.aPt();
        sdsf.aPu();
    }


    public void aPp() {
        System.out.println("test: " + (new BaseUiTegXml().createJComponentAndAddInRootPane((Object) this, "<vbox/>") instanceof VBox));
    }


    public void aPq() {
        System.out.println("test: " + (new BaseUiTegXml().createJComponentAndAddInRootPane((Object) this, "<hbox/>") instanceof HBox));
    }


    public void aPr() {
        System.out.println("test: " + (new BaseUiTegXml().createJComponentAndAddInRootPane((Object) this, "<gbox/>") instanceof GBox));
    }


    public void aPs() {
        Component e = new BaseUiTegXml().createJComponentAndAddInRootPane((Object) this,
            "<vbox name='v0' style='width:100px; height:100px'>" +
                "<vbox name='v1' style='width:100%; height:50%'/>" +
                "<vbox name='v2' style='width:100%; height:50%'/>" +
                "</vbox>");
        System.out.println("test: " + (e instanceof VBox));
        System.out.println("test: " + new Dimension(100, 100) + e.getPreferredSize());
        e.setSize(e.getPreferredSize());
        e.doLayout();
        System.out.println("test: " + new Dimension(100, 100) + e.getSize());
        System.out.println("test: " + new Dimension(100, 50) + DimensionSubTest.m1512a(e, "v1").getSize());
        System.out.println("test: " + new Dimension(100, 50) + DimensionSubTest.m1512a(e, "v2").getSize());
    }


    public void aPt() {
        Component e = new BaseUiTegXml().createJComponentAndAddInRootPane((Object) this, "<hbox name='v0' style='width:100px; height:100px'><hbox name='v1' style='width:50%; height:100%'/><hbox name='v2' style='width:50%; height:100%'/></hbox>");
        System.out.println("test: " + (e instanceof HBox));
        System.out.println("test: " + new Dimension(100, 100) + e.getPreferredSize());
        e.setSize(e.getPreferredSize());
        Component a = DimensionSubTest.m1512a(e, "v1");
        Component a2 = DimensionSubTest.m1512a(e, "v2");
        System.out.println("test: " + new Dimension(50, 100) + a.getPreferredSize());
        System.out.println("test: " + new Dimension(50, 100) + a2.getPreferredSize());
        e.doLayout();
        System.out.println("test: " + new Dimension(100, 100) + e.getSize());
        System.out.println("test: " + new Dimension(50, 100) + a.getSize());
        System.out.println("test: " + new Dimension(50, 100) + a2.getSize());
    }


    public void aPu() {
        Component e = new BaseUiTegXml().createJComponentAndAddInRootPane((Object) this, "<gbox cols='2' name='c0' style='width:100px; height:100px'><gbox name='c1' style='width:10px; height:10px'/><gbox name='c2' style='width:100%; height:20px'/><gbox name='c3' style='width:20px; height:10px'/><gbox name='c4' style='width:50%; height:20px'/></gbox>");
        System.out.println("test: " + (e instanceof GBox));
        System.out.println("test: " + new Dimension(100, 100) + e.getPreferredSize());
        e.setSize(e.getPreferredSize());
        Component a = DimensionSubTest.m1512a(e, "c1");
        Component a2 = DimensionSubTest.m1512a(e, "c2");
        Component a3 = DimensionSubTest.m1512a(e, "c3");
        Component a4 = DimensionSubTest.m1512a(e, "c4");
        System.out.println("test: " + new Dimension(10, 10) + a.getPreferredSize());
        System.out.println("test: " + new Dimension(20, 10) + a3.getPreferredSize());
        e.doLayout();
        System.out.println("test: " + new Dimension(100, 100) + e.getSize());
        System.out.println("test: " + new Dimension(10, 10) + a.getSize());
        System.out.println("test: " + new Dimension(20, 10) + a3.getSize());
        System.out.println("test: " + new Dimension(50, 20) + a4.getSize());
        System.out.println("test: " + new Dimension(80, 20) + a2.getSize());
    }

}
