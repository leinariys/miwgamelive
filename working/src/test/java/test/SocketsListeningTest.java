package test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

/* renamed from: f */
/* compiled from: test.a */
public class SocketsListeningTest {
    private static byte[] data = new byte[255];

    public static void main(String[] strArr) throws IOException {
        for (int i = 0; i < data.length; i++) {
            data[i] = (byte) i;
        }

        ServerSocketChannel open = ServerSocketChannel.open();//TCP
        open.configureBlocking(false);
        open.socket().bind(new InetSocketAddress(9000));

        Selector open2 = Selector.open();
        open.register(open2, 16);

        while (true) {
            open2.select();
            Iterator<SelectionKey> it = open2.selectedKeys().iterator();

            while (it.hasNext()) {
                SelectionKey next = it.next();
                it.remove();
                try {
                    //Проверяет, готов ли канал этого ключа принять новый сокет
                    //  подключение.
                    if (next.isAcceptable()) {

                        SocketChannel accept = open.accept();//Получение сокета
                        System.out.println("Accepted connection from " + accept);
                        accept.configureBlocking(false);
                        //Регистрирует этот канал с помощью данного селектора, возвращая ключ выбора.
                        accept.register(open2, 4).attach(ByteBuffer.wrap(data));

                    } else if (next.isWritable()) { //Проверяет, готов ли канал этого ключа к записи.
                        SocketChannel socketChannel = (SocketChannel) next.channel();
                        ByteBuffer byteBuffer = (ByteBuffer) next.attachment();

                        //Сообщает, есть ли какие-либо элементы между текущей позицией и пределом.
                        if (!byteBuffer.hasRemaining()) {
                            byteBuffer.rewind();
                        }
                        socketChannel.write(byteBuffer);
                    }

                } catch (IOException e) {
                    next.cancel();
                    try {
                        next.channel().close();
                    } catch (IOException e2) {
                    }
                }
            }
        }
    }
}
