package test;

// AWTPainting.java
// Процесс рисования в AWT очень прост

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class AWTPainting extends Frame {

    public AWTPainting() {
        super("AWTPainting");
        // выход при закрытии окна
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        setLayout(new FlowLayout());
        // попробуем закрасить часть кнопки
        add(new Button("Перерисуем кнопку!") {
            @Override
            public void paint(Graphics g) {
                g.setColor(Color.BLUE);
                g.fillRect(2, 2, getWidth() - 5, getHeight() - 5);
            }
        });
        setSize(200, 200);
    }

    public static void main(String[] args) {
        new AWTPainting().setVisible(true);
    }

    // в этом методе производится рисование
    @Override
    public void paint(Graphics g) {
        // заполняем все красным цветом
        g.setColor(Color.RED);
        g.fillRect(0, 0, getWidth(), getHeight());
    }
}