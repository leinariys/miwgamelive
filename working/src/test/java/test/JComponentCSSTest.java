package test;

import logic.res.KeyCode;
import logic.swing.BasicLookAndFeelSpaceEngine;
import logic.swing.BorderWrapper;
import logic.ui.BaseUiTegXml;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

/* renamed from: a.Ce */
/* compiled from: a */
public class JComponentCSSTest {


    public static void main(String[] args) throws UnsupportedLookAndFeelException {

        new JComponentCSSTest().start();
    }


    public void start() throws UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel(new BasicLookAndFeelSpaceEngine());

        Border border = new JTextField().getBorder();
        Assertions.assertTrue(border instanceof BorderWrapper, border.getClass() + " is not TaikodomBorder");


        JTextField e = (JTextField) new BaseUiTegXml().createJComponentAndAddInRootPane((Object) this, "<textfield text='test' style='width:300px; height:200px'/>");
        Assertions.assertTrue(e instanceof JTextField);

        Assertions.assertEquals(new Dimension(20, 16), e.getPreferredSize());


        JTextArea e2 = (JTextArea) new BaseUiTegXml().createJComponentAndAddInRootPane((Object) this, "<textarea text='test' style='width:300px; height:200px'/>");
        Assertions.assertTrue(e2 instanceof JTextArea);
        Assertions.assertEquals(new Dimension(20, 16), e2.getPreferredSize());


    }


    @BeforeAll
    public void setUp() throws UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel(new BasicLookAndFeelSpaceEngine());
    }


    @Test
    public void aBl() {
        Border border = new JTextField().getBorder();
        Assertions.assertTrue(border instanceof BorderWrapper, border.getClass() + " is not TaikodomBorder");
    }

    @Test
    public void aBm() {
        JTextField e = (JTextField) new BaseUiTegXml().createJComponentAndAddInRootPane((Object) this, "<textfield text='test' style='width:300px; height:200px'/>");
        Assertions.assertTrue(e instanceof JTextField);
        Assertions.assertEquals(new Dimension(KeyCode.cua, 200), e.getPreferredSize());
    }

    @Test
    public void aBn() {
        JTextArea e = (JTextArea) new BaseUiTegXml().createJComponentAndAddInRootPane((Object) this, "<textarea text='test' style='width:300px; height:200px'/>");
        Assertions.assertTrue(e instanceof JTextArea);
        Assertions.assertEquals(new Dimension(KeyCode.cua, 200), e.getPreferredSize());
    }

}
