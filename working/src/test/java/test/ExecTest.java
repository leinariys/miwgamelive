package test;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * Ожидает первое сообщение от сервер, затем посылает команды и ожидает ответы
 */

/* renamed from: a.arO  reason: case insensitive filesystem */
/* compiled from: a */
public class ExecTest {
    public static Map<String, ExecTest> listOfConnections = new HashMap();
    private boolean isFirstSends;
    private String host;
    private String messageEnds;
    private Socket socket;

    public ExecTest(String str, String str2) throws IOException {
        this(str);
        this.messageEnds = str2;
    }

    public ExecTest(String host) throws IOException {
        this.messageEnds = "tkd>";
        this.isFirstSends = false;
        this.host = host;
        this.socket = new Socket(host, 8989);
    }

    /* renamed from: au */
    public static ExecTest m25422au(String host, String message) throws IOException {
        ExecTest execTest = getConnection(host);
        execTest.messageEnds = message;
        return execTest;
    }

    /* renamed from: jN */
    public static ExecTest getConnection(String str) throws IOException {
        try {
            ExecTest execTest = listOfConnections.get(str);
            if (execTest != null && !execTest.socket.isClosed()) {
                return execTest;
            }
            execTest = new ExecTest(str);
            listOfConnections.put(str, execTest);
            return execTest;
        } catch (Exception e) {
            throw e;
        }
    }

    public static void main(String[] strArr) throws IOException, InterruptedException {
        ExecTest execTest = getConnection("localhost");
        System.out.println(execTest.requst("exec config.ConsoleTag tag=tkd>"));
        System.out.println(execTest.requst("exec config.ListMode type=html"));
        System.out.println(execTest.requst("exec list.Stations"));
    }

    /* renamed from: b */
    public String readInputStream(InputStream inputStream, String str) throws IOException, InterruptedException {
        int read;
        byte[] bArr = new byte[1024];
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            if (inputStream.available() > 0 && (read = inputStream.read(bArr, 0, 1024)) >= 0) {
                sb.append(new String(bArr, 0, read));
                if (str != null) {
                    String sb2 = sb.toString();
                    if (sb2.endsWith(str)) {
                        return sb2;
                    }
                } else {
                    continue;
                }
            } else if (i > 20) {
                try {
                    return sb.toString();
                } catch (Exception e) {
                }
            } else {
                Thread.sleep(100);
                i++;
            }
        }
    }

    /* renamed from: jO */
    public String requst(String str) throws IOException, InterruptedException {
        return requst(str, this.messageEnds);
    }

    /* renamed from: av */
    public String requst(String str, String str2) throws IOException, InterruptedException {
        String readMessage = "";
        OutputStream outputStream = this.socket.getOutputStream();
        InputStream inputStream = this.socket.getInputStream();
        if (this.isFirstSends) {
            readMessage = readInputStream(inputStream, str2) + "\n";
            this.isFirstSends = false;
        }
        outputStream.write((str + "\n").getBytes());
        return "\nFirst readMessage:'" + readMessage + "'" +
                "\nwriteMessage:'" + str + "'" +
                "\nreadMessage:'" + readInputStream(inputStream, str2) + "'";
    }
}
