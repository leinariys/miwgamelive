package test;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/* renamed from: a.xD */
/* compiled from: a */
public class ButtonHideTest {
    public static void main(String[] strArr) {
        JFrame jFrame = new JFrame("bla");
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//выход из приложения

        JPanel jPanel = new JPanel();
        JButton jButton = new JButton("bt");
        JButton jButton2 = new JButton("bt2");


        jButton.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        jPanel.remove(jButton2);
                        jPanel.updateUI();//Добавал, иначе оставалась картинка кнопки
                    }
                }
        );
        jFrame.setLayout(new BorderLayout());
        jPanel.setLayout(new FlowLayout());
        jPanel.add(jButton);
        jPanel.add(jButton2);
        jFrame.add(jPanel);
        jFrame.setBounds(400, 400, 200, 200);
        jFrame.show();
        jFrame.validate();
        jPanel.validate();
        jPanel.setLayout((LayoutManager) null);
    }

}
