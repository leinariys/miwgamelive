package test;

import logic.ui.layout.*;
import org.mozilla.javascript.ScriptRuntime;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

/* renamed from: a.wK   C3888wK*/
/* compiled from: a */
public class GridLayoutTest {


    public static void main(String[] args) {
        GridLayoutTest sdf = new GridLayoutTest();
        sdf.ani();
        sdf.anj();
        sdf.ank();
        sdf.anm();
        sdf.ann();

    }
    

    public void ani() {
        GridLayoutCustom adh = new GridLayoutCustom();
        GridLayout a = (GridLayout) adh.createLayout(new JPanel(), " { columns: 3; rows: 5; hgap: 7; vgap: 11; } ");
        System.out.println("test:" + 3 + " - " + (long) a.getColumns());
        System.out.println("test:" + 5 + " - " + (long) a.getRows());
        System.out.println("test:" + 7 + " - " + (long) a.getHgap());
        System.out.println("test:" + 11 + " - " + (long) a.getVgap());
        GridLayout a2 = (GridLayout) adh.createLayout(new JPanel(), " { columns: 3; vgap: 11; } ");
        System.out.println("test:" + 3 + " - " + (long) a2.getColumns());
        System.out.println("test:" + 1 + " - " + (long) a2.getRows());
        System.out.println("test:" + 0 + " - " + (long) a2.getHgap());
        System.out.println("test:" + 11 + " - " + (long) a2.getVgap());
    }


    public void anj() {
        GridBagLayoutCustom adb = new GridBagLayoutCustom();
        GridBagLayout a = (GridBagLayout) adb.createLayout(new JPanel(), "{columnWeights: 3; columnWidths: 2 5; rowHeights: 7 9; rowWeights: 3 11;}");
        System.out.println("test:" + Arrays.equals(new double[]{3.0d}, a.columnWeights));
        System.out.println("test:" + Arrays.equals(new int[]{2, 5}, a.columnWidths));
        System.out.println("test:" + Arrays.equals(new int[]{7, 9}, a.rowHeights));
        System.out.println("test:" + Arrays.equals(new double[]{3.0d, 11.0d}, a.rowWeights));
        GridBagConstraints gridBagConstraints = (GridBagConstraints) adb.mo8427iw("gridx: 3; gridy: 5; gridwidth: 7; gridheight: 9; weightx: 11; weighty: 13; anchor: above-baseline-leading; fill: horizontal; ipadx: 23; ipady: 27; insets: 29 31 33 37;");
        System.out.println("test:" + 3 + " - " + (long) gridBagConstraints.gridx);
        System.out.println("test:" + 5 + " - " + (long) gridBagConstraints.gridy);
        System.out.println("test:" + 7 + " - " + (long) gridBagConstraints.gridwidth);
        System.out.println("test:" + 9 + " - " + (long) gridBagConstraints.gridheight);
        System.out.println("test:" + 11.0d + " - " + gridBagConstraints.weightx + " - " + ScriptRuntime.NaN);
        System.out.println("test:" + 13.0d + " - " + gridBagConstraints.weighty + " - " + ScriptRuntime.NaN);
        System.out.println("test:" + 1280 + " - " + (long) gridBagConstraints.anchor);
        System.out.println("test:" + 2 + " - " + (long) gridBagConstraints.fill);
        System.out.println("test:" + 23 + " - " + (long) gridBagConstraints.ipadx);
        System.out.println("test:" + 27 + " - " + (long) gridBagConstraints.ipady);
        System.out.println("test:" + new Insets(29, 31, 33, 37) + " - " + gridBagConstraints.insets);
    }


    public void ank() {
        BoxLayoutCustom bIVar = new BoxLayoutCustom();
        System.out.println("test:" + 1 + " - " + bIVar.createLayout(new JPanel(), " { axis: y-axis } "));
        System.out.println("test:" + 0 + " - " + bIVar.createLayout(new JPanel(), " { axis: x-axis } "));
    }


    public void anl() {
        FlowLayoutCustom eNVar = new FlowLayoutCustom();
        FlowLayout a = (FlowLayout) eNVar.createLayout(new JPanel(), " { alignment: center; vgap:3; hgap:7; } ");
        System.out.println("test:" + 3 + " - " + (long) a.getVgap());
        System.out.println("test:" + 7 + " - " + (long) a.getHgap());
        System.out.println("test:" + 1 + " - " + (long) a.getAlignment());
        FlowLayout a2 = (FlowLayout) eNVar.createLayout(new JPanel(), " { alignment: right; vgap:23; hgap:27; } ");
        System.out.println("test:" + 23 + " - " + (long) a2.getVgap());
        System.out.println("test:" + 27 + " - " + (long) a2.getHgap());
        System.out.println("test:" + 2 + " - " + (long) a2.getAlignment());
    }


    public void anm() {
        BorderLayoutCustom ada = new BorderLayoutCustom();
        BorderLayout a = (BorderLayout) ada.createLayout(new JPanel(), " { alignment: center; vgap:3; hgap:7; } ");
        System.out.println("test:" + 3 + " - " + (long) a.getVgap());
        System.out.println("test:" + 7 + " - " + (long) a.getHgap());
        BorderLayout a2 = (BorderLayout) ada.createLayout(new JPanel(), "");
    }


    public void ann() {
        CardLayoutCustom asc = new CardLayoutCustom();
        CardLayout a = (CardLayout) asc.createLayout(new JPanel(), " { vgap:3; hgap:7; } ");
        System.out.println("test:" + 3 + " - " + (long) a.getVgap());
        System.out.println("test:" + 7 + " - " + (long) a.getHgap());
        CardLayout a2 = (CardLayout) asc.createLayout(new JPanel(), "");
    }
}
