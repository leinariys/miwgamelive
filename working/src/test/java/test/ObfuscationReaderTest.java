package test;

import logic.obfuscation.read.ObfuscationReader;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

public class ObfuscationReaderTest {

    public static void main(String[] strArr) throws IOException {
        Map<String, String> stringMap = new ObfuscationReader(new FileInputStream("working\\src\\test\\resources/ob-client.map")).getMap();
        System.out.println("stringMap:" + stringMap.toString());
    }

}
