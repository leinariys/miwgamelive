package test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

public class InetSocketAddressTest {

    private static ServerThread serverThread;
    private static boolean daemon = true;
    private static SocketAddress socketAddress;
    private static DatagramChannel datagramChannelServer;
    private static DatagramChannel datagramChannelClient;
    private static ByteBuffer buffer;


    public static void startReceiver() throws IOException {
        socketAddress = new InetSocketAddress("localhost", 11333);
        datagramChannelServer = DatagramChannel.open();
        datagramChannelServer.bind(socketAddress);//Прослушивание порта
        System.out.println("Receiver started at #" + socketAddress);
    }

    public static void startClient() throws IOException {
        socketAddress = new InetSocketAddress("localhost", 11333);
        datagramChannelClient = DatagramChannel.open();
        datagramChannelClient.bind(null);
        System.out.println("Receiver started at #" + socketAddress);
    }

    public static void main(String[] args) throws IOException {
        //InetSocketAddressTest socketAddressTest = new InetSocketAddressTest(11333);
        //socketAddressTest.start();

        startReceiver();
        startClient();
        while (true) {
            sendMessage(datagramChannelClient, "Bye!", socketAddress);
            String message = receiveMessage(datagramChannelServer);

            if ("Bye!".equals(message)) {
                break;
            }
        }
        datagramChannelServer.close();
        datagramChannelClient.close();
        System.out.println("Receiver closed!");
    }

    public static String receiveMessage(DatagramChannel receiver) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        SocketAddress senderAddress = receiver.receive(buffer);
        String message = extractMessage(buffer);
        System.out.println("Received message from sender: " + senderAddress);
        System.out.println("Message: " + message);
        return message;
    }

    private static String extractMessage(ByteBuffer buffer) {
        buffer.flip();
        byte[] bytes = new byte[buffer.remaining()];
        buffer.get(bytes);
        String msg = new String(bytes);
        return msg;
    }

    public static void sendMessage(DatagramChannel sender, String msg,
                                   SocketAddress receiverAddress) throws IOException {
        ByteBuffer buffer = ByteBuffer.wrap(msg.getBytes());
        sender.send(buffer, receiverAddress);
    }

    public void start() throws IOException {
        this.datagramChannelServer = DatagramChannel.open();
        this.datagramChannelServer.bind(this.socketAddress);

        this.buffer = ByteBuffer.allocateDirect(64);
        this.serverThread = new ServerThread();
        this.serverThread.setDaemon(this.daemon);
        this.serverThread.start();
    }

    /* access modifiers changed from: private */
    /* renamed from: Ys */
    public void run() {
        try {
            this.buffer.clear();
            SocketAddress receive = this.datagramChannelServer.receive(this.buffer);
            if (this.buffer.position() == 16) {
                this.buffer.putLong(11111);
                this.buffer.putLong(2222222);
                this.buffer.flip();
                this.datagramChannelClient.send(this.buffer, receive);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public class ServerThread extends Thread {
        public ServerThread() {
            super("Time server");
        }

        public void run() {
            while (!Thread.interrupted()) {
                InetSocketAddressTest.this.run();
            }
        }
    }
}
