package test;

import game.server.IServerConsole;
import game.server.ServerConsoleConnect;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import java.util.Stack;

/* renamed from: a.sB */
/* compiled from: a */
public class ServerConsoleTest extends JFrame implements IServerConsole {
    /* access modifiers changed from: private */
    public JTextArea consoleText;
    /* access modifiers changed from: private */
    public LinkedList<String> consoleHistory = new LinkedList<>();
    /* access modifiers changed from: private */
    public int lineCommands = -1;
    private JTextArea terminalText;
    private ServerConsoleConnect consoleConnect = new ServerConsoleConnect();


    public ServerConsoleTest() {
        this.consoleConnect.setServerConsole((IServerConsole) this);
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout());
        this.terminalText = new JTextArea();
        this.terminalText.setFont(new Font(null, 0, 15));
        this.terminalText.setEditable(false);
        this.terminalText.setBackground(Color.BLACK);
        this.terminalText.setForeground(Color.WHITE);
        JScrollPane jScrollPane = new JScrollPane(this.terminalText);

        this.consoleText = new JTextArea();
        this.consoleText.setFont(new Font(null, 0, 15));
        this.consoleText.setBackground(Color.BLACK);
        this.consoleText.setForeground(Color.WHITE);
        this.consoleText.setCaretColor(Color.WHITE);
        this.consoleText.addKeyListener(new KeyListenerWrap());

        jPanel.add(jScrollPane, "Center");
        jPanel.add(this.consoleText, "South");

        setTitle("Server Console (rhino)");
        setContentPane(jPanel);
        setVisible(true);
        setDefaultCloseOperation(3);
        setBounds(100, 0, 800, 600);
    }

    public static void main(String[] strArr) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
        }

        new ServerConsoleTest();
    }

    private void executeWriteSocket(String inputMes) {
        try {
            printTerminal(inputMes);
            ServerConsoleTest.this.consoleConnect.writeSocketUTF8(inputMes.replaceFirst(">\\s*", ""));
            ServerConsoleTest.this.consoleText.setText("");
            ServerConsoleTest.this.consoleHistory.add(0, inputMes);
        } catch (Exception e) {
            printTerminal(e.toString());
        }
        cookConsole(inputMes);
    }

    /* access modifiers changed from: protected */
    /* renamed from: iP */
    public void executeJs(String inputMes) {
        if (checPairedBrackets(inputMes)) {
            printTerminal(inputMes);
            try {
                Object result = this.consoleConnect.runJs(inputMes);
                if (result != null) {
                    printTerminal(String.valueOf(result));
                }
            } catch (Exception e) {
                printTerminal(e.toString());
            }
            cookConsole(inputMes);
        }
    }

    private void cookConsole(String inputMes) {
        this.consoleText.setText("");
        this.consoleHistory.add(0, inputMes);
        this.lineCommands = 0;
        if (this.consoleHistory.size() > 1000) {
            this.consoleHistory = (LinkedList<String>) this.consoleHistory.subList(0, 1000);
        }
    }

    /**
     * Контроль парности скобок
     *
     * @param inputMes
     * @return
     */
    private boolean checPairedBrackets(String inputMes) {
        Stack stack = new Stack();
        for (char c : inputMes.toCharArray()) {//Контроль парности скобок
            if (c == '{' || c == '[' || c == '(') {
                stack.push(1);
            }
            if ((c == '}' || c == ']' || c == ')') && !stack.isEmpty()) {
                stack.pop();
            }
        }
        return stack.isEmpty();
    }


    /* renamed from: m */
    public void printTerminal(String inputMes) {
        Document document = this.terminalText.getDocument();
        String str2 = inputMes.trim() + "\n";
        if (document != null) {
            try {
                document.insertString(document.getLength(), str2, (AttributeSet) null);
                this.terminalText.setCaretPosition(document.getLength());
            } catch (BadLocationException e) {
            }
        }
    }

    /* renamed from: aE */
    public void setMessageConsole(String str) {
        printTerminal(str);
    }

    /* renamed from: BN */
    public void clearConsole() {
        this.consoleText.setText("");
    }

    /* renamed from: a.sB$a */
    class KeyListenerWrap implements KeyListener {
        @Override
        public void keyPressed(KeyEvent keyEvent) {
            if (keyEvent.getKeyCode() == KeyEvent.VK_UP) {
                //Когда вставили в консоль несколько строк
                if ((ServerConsoleTest.this.consoleText.getCaretPosition() == 0 || !ServerConsoleTest.this.consoleText.getText().contains("\n")) && ServerConsoleTest.this.consoleHistory.size() - 1 > ServerConsoleTest.this.lineCommands) {
                    JTextArea consoleText = ServerConsoleTest.this.consoleText;
                    LinkedList consoleMes = ServerConsoleTest.this.consoleHistory;
                    ServerConsoleTest.this.lineCommands++;
                    consoleText.setText((String) consoleMes.get(ServerConsoleTest.this.lineCommands));
                }
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_DOWN && !ServerConsoleTest.this.consoleText.getText().contains("\n") && ServerConsoleTest.this.lineCommands > 0) {
                JTextArea consoleText = ServerConsoleTest.this.consoleText;
                LinkedList consoleMes = ServerConsoleTest.this.consoleHistory;
                ServerConsoleTest.this.lineCommands++;
                consoleText.setText((String) consoleMes.get(ServerConsoleTest.this.lineCommands));
            }
        }

        @Override
        public void keyReleased(KeyEvent keyEvent) {
            if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
                String trim = ServerConsoleTest.this.consoleText.getText().trim();
                if (trim.startsWith(">")) {//Прямая команда в сокет
                    ServerConsoleTest.this.executeWriteSocket(trim);
                } else {//команда
                    ServerConsoleTest.this.executeJs(trim);
                }
                ServerConsoleTest.this.lineCommands = -1;
            }
        }

        public void keyTyped(KeyEvent keyEvent) {
        }
    }


}
