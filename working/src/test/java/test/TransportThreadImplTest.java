package test;


import game.network.Transport;
import game.network.build.ManagerMessageTransport;
import game.network.build.TransportThread;
import game.network.channel.Connect;
import game.network.channel.NetworkChannel;
import game.network.channel.client.ClientConnect;
import game.network.channel.server.OpenSocketNioServer;
import game.network.message.ByteMessageReader;
import game.network.message.TransportCommand;
import game.network.message.TransportResponse;
import game.network.message.externalizable.C6302akO;
import game.network.message.externalizable.ObjectId;
import game.network.message.externalizable.ObjectIdImpl;
import game.network.message.serializable.ReceivedTime;
import game.network.thread.TransportThreadImpl;
import lombok.extern.slf4j.Slf4j;
import taikodom.render.textures.DDSLoader;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;

@Slf4j
public class TransportThreadImplTest {

    public static void main(String[] args) {
        testStart();
    }

    private static void testStart() {
        NetworkChannel serverSocket = new OpenSocketNioServer(15226, "1.0.4933.55");
        serverSocket.creatListenerChannel(new ConnectTest());
        try {
            serverSocket.buildListenerChannel(15225, (int[]) null, (int[]) null, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ManagerMessageTransport messageTransport = new ManagerMessageTransportImpl(serverSocket);


        TransportThreadImpl transportThread = new TransportThreadImpl(messageTransport, serverSocket.getClientConnect(), serverSocket.getMessageProcessing(), serverSocket.getProcessSendMessage());


        while (true) {
            try {
                //System.out.println(System.getProperty("nio.savemessages"));
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        /*
        ServerSocketChannel open = ServerSocketChannel.open();
        open.configureBlocking(false);
        open.socket().bind(new InetSocketAddress(15115));
        ConfigServer configServer = new ConfigServer(open, ElementConnect.Attitude.CHILD, this, (int[]) null, (int[]) null, false);
        this.threadSelectableChannel.addChannel((WraperSelectable) configServer);
        */
    }


    static class ConnectTest implements Connect {

        @Override
        public void chechUser(ClientConnect bVar, String userName, String pass, boolean isForcingNewUser) {

        }

        @Override
        public void userDisconnected(ClientConnect bVar) {

        }
    }

    private static class ManagerMessageTransportImpl implements ManagerMessageTransport {
        public TransportThread transportThread;
        private ArrayBlockingQueue<Runnable> poolExecuterCommand = new ArrayBlockingQueue<>(DDSLoader.DDSD_MIPMAPCOUNT);

        ManagerMessageTransportImpl(NetworkChannel serverSocket) {

            this.transportThread = new TransportThreadImpl(this, serverSocket.getClientConnect(), serverSocket.getMessageProcessing(), serverSocket.getProcessSendMessage());
            this.transportThread.setIsServer(true);

        }


        @Override
        public TransportResponse preparingResponseMessage(ClientConnect bVar, TransportCommand transportCommand) {

            ByteMessageReader inputStream = transportCommand.getByteMessageReader();

            C6302akO d = new ObjectIdImpl(new ObjectId(159L));

            TransportResponse createResponse = transportCommand.createResponse();
            addSerializeDataReceivedTime((Transport) createResponse, (ReceivedTime) d);
            return createResponse;
        }

        private void addSerializeDataReceivedTime(Transport transport, ReceivedTime receivedTime) {
           /*
            C0387FM fm = new C0387FM(this.dataGameEvent, transport.getOutputStream(), false, true, 0, false);

            fm.writeObject(receivedTime);

            fm.flush();
*/
        }

        @Override
        public void addToPoolExecuterCommand(ClientConnect clientConnect, TransportCommand transportCommand) {
            TransportResponse response = ManagerMessageTransportImpl.this.preparingResponseMessage(clientConnect, transportCommand);
            if (response != null) {
                ManagerMessageTransportImpl.this.transportThread.sendTransportResponse(clientConnect, response);
            }
        }


    }
}
